#!/bin/bash

ROOT=$PWD
#TAG=registry.bst.ai:5000/bst-ehub-core-ui
TAG=frontend_container
NAME=FrontEnd_Space
## PRINT Codes

GREEN='\033[0;32m'
NC='\033[0m' # No Color
printf "${GREEN}Starting the Build. \n ${NC}\n"

# Build the image if we need to
docker build  --tag $TAG $PWD --no-cache
#docker build --tag $TAG $PWD
printf "${GREEN}Docker image build successful${NC}\n"

