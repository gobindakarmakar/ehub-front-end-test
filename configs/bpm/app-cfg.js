/*
 * Copyright 2005-2015 Alfresco Software, Ltd. All rights reserved.
 * License rights for this program may be obtained from Alfresco Software, Ltd.
 * pursuant to a written agreement and any use of this program without such an
 * agreement is prohibited.
 */
'use strict';

var ACTIVITI = ACTIVITI || {};
var mainPath = (window.location.pathname).split('/')[1];

var ehubObj  =(JSON.parse(decodeURIComponent(window.location.href.split('id=')[1])));
window.localStorage.setItem("ehubObject",ehubObj)

ACTIVITI.CONFIG = {
	'onPremise' : true,
	'contextRoot' : '/' + mainPath,
	'webContextRoot' : '/' + mainPath
};
ACTIVITI.API = {
	/*---  For ING  ---*/ 
	/*---'path': 'http://ing.ui.xara.ai/ehubrest',---*/ 
	/*---  For CERI  ---*/ 
	/*---'path': 'http://ceri.ui.xara.ai/ehubrest',---*/ 
	/*---  For DEMO  ---*/  
	/*---'path': 'http://ing.ui.xara.ai/ehubrest',---*/ 
	/*'path': 'http://dev.element.xara.ai:8080/ehubrest',*/
	'path': EHUB_API+'/ehubrest',
	'token':  window.localStorage.getItem('ehubObject') !== null ? JSON.parse( window.localStorage.getItem('ehubObject')).token : null,
	'userId': window.localStorage.getItem('ehubObject') !== null ? JSON.parse( window.localStorage.getItem('ehubObject')).userId : null
}
/*
 * Copyright 2005-2015 Alfresco Software, Ltd. All rights reserved.
 * License rights for this program may be obtained from Alfresco Software, Ltd.
 * pursuant to a written agreement and any use of this program without such an
 * agreement is prohibited.
 */
//
// DEV ENV OVERRIDES
//

