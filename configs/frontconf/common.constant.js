'use strict';

/*---  For PROD ENV ---*/ 
elementApp       
            .constant('FLASH_ALERT_DURATION',5000)
            .constant('FLASH_ALERT_CLOSE',true)
            .constant('UPLOADING_FILES_NUM',12)
            .constant('UPLOADING_KYC_FILES_NUM',6)
//for 6060
.constant('Onboarding_Survey_URL', KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/{OnBoardingSurveyID}?newtest=Y&lang=en&userToken=')
.constant('Generate_report_Survey_URL', KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/{serveyID}?newtest=Y&lang=en&token=');




