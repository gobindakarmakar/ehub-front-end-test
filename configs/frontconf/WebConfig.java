package element.bst.elementexploration.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * 
 * @author Amit Patel
 *
 */

@ComponentScan({ "element.bst.elementexploration" })
@Configuration
@EnableWebMvc
@Import({ WebSecurityConfig.class })

@PropertySources({ 
	@PropertySource(value = "file:///${app.conf.dir}/ehub-ui.properties")
})

public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configuration) {

		configuration.enable();
	}

	/*
	 * @Bean public static PropertySourcesPlaceholderConfigurer
	 * placeHolderConfigurer() throws IOException {
	 * 
	 * PropertySourcesPlaceholderConfigurer config = new
	 * PropertySourcesPlaceholderConfigurer(); config.setLocations(new
	 * PathMatchingResourcePatternResolver().getResources("file:///" +
	 * System.getProperty("app.conf.dir") + "/log_configuration.properties"));
	 * 
	 * return config; }
	 */

	@Bean
	public static RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public InternalResourceViewResolver getInternalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
		registry.addResourceHandler("/scripts/**").addResourceLocations("/scripts/");
		registry.addResourceHandler("/vendor/**").addResourceLocations("/vendor/");
	}
}