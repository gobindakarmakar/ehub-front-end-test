'use strict';


var paths = {
		'EHUB_API':EHUB_API+'/ehubrest/api/',
		'EHUB_FE_API':EHUB_FE_API+'/element/',
		'TI_API':EHUB_API+'/transaction/',
		'ACTIVITI_FE_PATH':EHUB_API+'/elementbpm/#/',
		'KYC_QUESTIONNAIRE_PATH':KYC_QUESTIONNAIRE_PATH+'/element-questionnaire-builder/index.php/admin',
		'POLICY_ENFORCEMENT_PATH': POLICY_ENFORCEMENT_PATH+'/policyEnforcement/'
		
}

elementApp 
	.constant('EHUB_API',paths.EHUB_API )
	.constant('EHUB_FE_API', paths.EHUB_FE_API)
	.constant('TI_API', paths.TI_API)
	.constant('ACTIVITI_FE_PATH', paths.ACTIVITI_FE_PATH)
	.constant('KYC_QUESTIONNAIRE_PATH', paths.KYC_QUESTIONNAIRE_PATH)
	.constant('POLICY_ENFORCEMENT_PATH',paths.POLICY_ENFORCEMENT_PATH);

window.localStorage.setItem("paths",(JSON.stringify(paths)));
