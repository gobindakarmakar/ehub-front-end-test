#!/usr/bin/python
import os
import glob

CONFIG_PATH = os.environ['CONFIG_PATH'] if 'CONFIG_PATH' in os.environ else '/opt/bst-ehub-front/conf'
config_files = glob.glob(CONFIG_PATH+"/*.*")
envs = os.environ

for file_name in config_files:
    file = open(file_name, "r")
    configs =  file.read()
    for env in list(envs):
        configs = configs.replace("@__ENV_"+env+"__",envs[env])
    
    file_new = open(file_name,"w")
    file_new.write(configs) 
    file_new.close()
