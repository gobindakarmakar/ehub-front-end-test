#!/bin/sh

echo Welcome to BST Computing Engine
/etc/init.d/ssh start
python /root/set_config.py
cat /opt/bst-ehub-front/conf/ehub-ui.properties

sudo -H -u tomcat bash -c /opt/tomcat/bin/startup.sh
tail -f /opt/tomcat/logs/catalina.out
