1.7.0.9 release(date)

Deprecation Warning:


Bug Fixes:

	Docparser:
		1)Loader should be enabled on click of save/update button before api call.
		2)delete functionality issue for checkboxes.

	Front-end:
		1)Application running issues in IE browser.

New Features:


Performance Improvements:

	Orchestration:
		1)Code cleanup and commentation for Orchestration module.

    Login module:
	    1)New Updated design for the Login screen and Forgot password screen.
		
Breaking Changes:


1.7.1.2 release(7 jan 2019)

Performance:
1.Moved  to v2 API for search and  ehanced the performance.


New Feature:
1.Added the new module called 'Source creditability' where user can define the credibility for each source related to each entity attribute via Element.

v1.7.1.3
New Features:
		1)Feature of Adding the company screening in the screening section and report.
		2)Custom sentiment for Adverse/Finance news Where users can his/her own sentiment about the news
		3)Feature od Showing the Country flag
		4)New tooltip for organisation chart which shows the details like UBO/IBO,country,Screening,Percentage,Source information.
		
