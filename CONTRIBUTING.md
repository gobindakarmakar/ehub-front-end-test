#Code conventions
Coding conventions are style guidelines for programming. They typically cover:

* Naming and declaration rules for variables and functions.
* Rules for the use of white space, indentation, and comments.
* Programming practices and principles

Coding conventions secure quality:

* Improves code readability
* Make code maintenance easier

#Performance
* Coding conventions are not used by computers. Most rules have little impact on the execution of programs.
* Indentation and extra spaces are not significant in small scripts.
* For code in development, readability should be preferred. Larger production scripts should be minified.

#HTML 5 code style and conventions
##Document type
Strictly use HTML5 as markup language and declare it's use in the file header as following (all in lower case):

    <!doctype html>

##Element names
Strictly use lowercase element names.

##Element closure
1. Always keep your code tidy, clean and well-formed.
2. Use a well-formed-"close to XHTML" syntax.
3. Close all elements including the empty elements.

##Mandatory tags and viewport
1. **html**, **head**, **title** and **body** tags should always be present in a final html document (included jsps may need to omit the tags however these tags should be present in the final page). Make the **title** as meaningful as possible.
2. **meta** viewport element should be set in all web pages to give the browser instructions on how to control the page's dimensions and scaling. An example follows:

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

##Attrribute names
Always use lower case attribute names.

__Bad__

    <div CLASS="menu">

__Good__

    <div class="menu">

##Attribute values
Always quote the attribute values.

__Bad__

    <table class=striped>

__Good__

    <table class="striped">

##Image attributes
Always add the alt attribute to images. This attribute is important when the image for some reason cannot be displayed. Also, always define image width and height. It reduces flickering because the browser can reserve space for the image before loading.

##Equal signs
Do not use space around equal signs for better code readability.

__Bad__

    <link rel = "stylesheet" href = "styles.css">

__Good__

    <link rel="stylesheet" href="styles.css">

##Code line length
Avoid code lines longer than 80 characters and divide into multi line code with proper indentation if necessary. This is needed to ensure code readability and consistent coding style.


##Blank lines and indentation
1. Add blank lines to separate large or logical code blocks.
2. For readability, add two spaces of indentation. Do not use the tab key.
3. Do not indent the raw text value inside a block or inline elements.

##HTML comments
* Short comments should be written in one line like this.

    <!-- This is a comment -->

* Comments that spans more than one line, should be written like this

>`<!--`

>  Long comment indented by two spaces (indentation not visible here).

>  Long comment indented by two spaces (indentation not visible here).

>`-->`

##Style sheets
* Use simple syntax for linking to style sheets(the type attribute is not necessary).

    <link rel="stylesheet" href="styles.css">

* Style sheet indentation rules follow (to be used in web pages as well as in CSS files):
    1. Place the opening bracket on the same line as the selector
    2. Use one space before the opening bracket
    3. Use two spaces of indentation
    4. Use semicolon after each property-value pair, including the last
    5. Only use quotes around values if the value contains spaces
    6. Place the closing bracket on a new line, without leading spaces
    7. Avoid lines over 80 characters

##Loading javascript
Use simple syntax for loading external scripts (the type attribute is not necessary)

    <script src="myscript.js">

#Javascript code style and conventions
##Variable names
* use **camelCase** for identifier names (variables and functions).
* All names start with a **letter**.

##Space and indentation
* Always put spaces around operators `( = + - * / )`, and after **commas**.
* Always use 4 spaces for indentation of code blocks.

##Statement rules
* Always end a simple statement with a semicolon.
* Rules for compound statements follow:
    1. Put the opening bracket at the end of the first line.
    2. Use one space before the opening bracket.
    3. Put the closing bracket on a new line, without leading spaces.
    4. Do not end a complex statement with a semicolon (use braces instead).

##Object rules
* General rules for object definition follow:
    1. Place the opening bracket on the same line as the object name.
    2. Use colon plus one space between each property and its value.
    3. Use quotes around string values, not around numeric values.
    4. Do not add a comma after the last property-value pair.
    5. Place the closing bracket on a new line, without leading spaces.
    6. Always end an object definition with a semicolon.

* Short objects can be written compressed, on one line, using spaces only between properties, like this:

    var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};

##Line length
* For readability, avoid lines longer than 80 characters.
* If a JavaScript statement does not fit on one line, the best place to break it, is after an operator or a comma.

##Standard practices
These guidelines should be checked before the code moves into the repository:

* Avoid global variables.
* Always declare local variables.
* Declare variables before their use.
* Initialize the variables at the point of their declaration to some default values.
* Always treat numbers, strings, or booleans as primitive values. Not as objects.
* Don't use `new Object()`, use following instead:
    1. Use `{}` instead of `new Object()`
    2. Use `""` instead of `new String()`
    3. Use `0` instead of `new Number()`
    4. Use `false` instead of `new Boolean()`
    5. Use `[]` instead of `new Array()`
    6. Use `/()/` instead of `new RegExp()`
    7. Use `function (){}` instead of `new Function()`
* Beware of automatic type conversions, e.g. numbers can accidentally be converted to strings or `NaN`.
* Use `===` comparison
* Use parameter defaults, assign default values to arguments.
* Always end switch blocks with default.
* Do not use `eval()`.

#Naming conventions
Naming conventions should be consistent across all static and dynamic web components.

##General conventions
* File names should be nouns or verbs and be constituted with lowercase ASCII characters and numeric digits. Avoid separators however use of underscore is permitted.
* Variable and function names written as camelCase
* Global variables written in UPPERCASE
* Constants (like PI) written in UPPERCASE
* Do not start names with a `$` sign. It will put you in conflict with many JavaScript library names.

##Hyphens and underscores
* HTML5 attributes can start with data- (data-quantity, data-price).
* CSS uses hyphens in property-names (font-size).
* Prefer underscore over hyphen otherwise.

##File extensions
* **.html** for static html files.
* Native (eg. jsp) for dynamic web pages.
* **.css** for css files.
* **.js** for javascript files.

#File encoding
* Default encoding for all static and dynamic web components is **UTF-8**.
* Declare the encoding in CSS and JSON files explicitly. Declaration of encoding in CSS file header:

    `@charset "UTF-8";`

#Testing
* All javascript code should be declared as **strict** and formed with strict syntax.
    "use strict";
* Configure the build automation tool to validate Javascript(may use Jshint), CSS(may use CSSLint) and the html pages (many build tools have native support) as a part of the build process. Use external libraries like wro4j or equivalent to achieve this if required.
* Build should fail early on the first validation error however should report all the existing issues.

#Accessibility guidelines
The Web Content Accessibility Guidelines (WCAG, currently version 2.0 published in December 2008) published by the Web Accessibility Initiative (WAI) of the World Wide Web Consortium (W3C) must be complied with to the extent possible. Exact scope and coverage should be discussed.



