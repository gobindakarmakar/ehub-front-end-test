#!/bin/bash

ROOT=$PWD
#TAG=899719272550.dkr.ecr.eu-west-1.amazonaws.com/be-ui/explorer:1.7.1.6
#TAG=registry.bst.ai:5000/bst-ehub-core-ui
TAG=frontend_container
NAME=FrontEnd_Space
## PRINT Codes

GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf "${GREEN}Stopping Running Containers${NC}\n"
# Check if a container with the same image tag, already exists
CONTAINER=$(docker ps -aq -f name=$NAME)
if [ ! -z "$CONTAINER" ]; then
  # Check if the container is currently running
  STATUS=$(docker inspect -f {{.State.Status}} $CONTAINER)
  if [ "$STATUS" == "running" ]; then
    docker stop $CONTAINER
  fi

  # Remove the old container
  docker rm --force $CONTAINER
fi
printf "${GREEN}Starting the Container${NC}\n"



docker run --name $NAME -it --hostname $NAME -d  \
-p 80:8080 -e "container=docker" \
-e "ELEMENT_MAIN_URL=http://3.248.195.168" \
-e "POLICY_ENFORCEMENT_URL=http://3.248.195.168:7070" \
-e "QUESTIONNAIRE_URL=http://3.248.195.168:90" \
-e "ACTIVITI_URL=http://3.248.195.168:8080" \
-e "ELEMENT_REST_DOMAIN=http://3.248.195.168:8080" \
--privileged=true  \
--restart=always \
--net bst  $TAG tail -f /dev/null
printf "${GREEN}Starting Success, you can access the docker container by {docker exec -it $NAME /bin/bash}${NC}\n"

