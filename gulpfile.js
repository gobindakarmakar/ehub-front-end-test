/*
 ================================================================================
 *********************   Gulp Setup For The Site      ***************************
 ================================================================================
 */

 /*--  Variables  --*/
 var gulp = require('gulp');
 var sass = require('gulp-sass');
 var minifyCSS = require('gulp-minify-css');
 var concat = require('gulp-concat');
 var babel = require('gulp-babel');
 var minify = require("gulp-babel-minify");
 var removeUseStrict = require("gulp-remove-use-strict");
//var uglify = require('gulp-uglify');

var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var batch = require('gulp-batch');
//var uglify = require('gulp-uglify-es');
var uglifyyes = require('gulp-uglifyes');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var pump = require('pump');
var saveLicense = require('uglify-save-license');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

/*
================================================================================
*************************   Development Tasks      *****************************
================================================================================
*/

/*
================================================================================
*************************   Stream Tasks Starts    *****************************
================================================================================
*/

/*--  Vendor Stream Task  --*/
gulp.task('vendorStyles', function() {
    return gulp.src([
        'src/main/webapp/vendor/jquery/css/jquery.mThumbnailScroller.min.css',
        'src/main/webapp/vendor/jquery/css/jquery.mCustomScrollbar.css',
        'src/main/webapp/vendor/jquery/css/jquery.dataTables.min.css',
        'src/main/webapp/vendor/jquery/css/responsive.dataTables.min.css',
        'src/main/webapp/vendor/jquery/css/jquery-ui.css',
        'src/main/webapp/vendor/jquery/css/rzslider.css',
        'src/main/webapp/vendor/angular/css/angular-gridster.min.css',
        'src/main/webapp/vendor/angular/css/angular-flash.css',
        'src/main/webapp/vendor/angular/css/angular-timeline.css',
        'src/main/webapp/vendor/jquery/css/rangedatepicker.css',
        'src/main/webapp/vendor/jquery/css/displacy-entity.css',
        'src/main/webapp/charts/leaflet/leaflet.css',
        'src/main/webapp/charts/WorldMap/css/Worldmap.css',
        'src/main/webapp/scripts/VLA/css/animate.css',
        'src/main/webapp/scripts/VLA/css/libs-bundle.css',
        'src/main/webapp/scripts/VLA/css/slider.css',
        'src/main/webapp/workflow/assets/css/joint.css',
        'src/main/webapp/workflow/assets/css/jqx.base.css',
        'src/main/webapp/workflow/assets/css/chosen.min.css'
        ])
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Custom Stream Task  --*/
gulp.task('customStyles', function() {
    return gulp.src(['src/main/webapp/assets/scss/theme/styles.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Vendor Stream Task  --*/
gulp.task('commonVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/vendor/jquery/css/jquery.mThumbnailScroller.min.css',
        'src/main/webapp/vendor/jquery/css/jquery.mCustomScrollbar.css',
        'src/main/webapp/vendor/jquery/css/jquery.dataTables.min.css',
        'src/main/webapp/vendor/jquery/css/responsive.dataTables.min.css',
        'src/main/webapp/vendor/jquery/css/jquery-ui.css',
        'src/main/webapp/vendor/jquery/css/jquery.scrollbar.css',
        'src/main/webapp/vendor/jquery/css/rzslider.css',
        'src/main/webapp/vendor/angular/css/angular-gridster.min.css',
        'src/main/webapp/vendor/angular/css/angular-flash.css',
        'src/main/webapp/vendor/angular/css/angular-timeline.css',
        'src/main/webapp/vendor/jquery/css/rangedatepicker.css',
        'src/main/webapp/vendor/jquery/css/displacy-entity.css',
        'src/main/webapp/charts/leaflet/leaflet.css',
        'src/main/webapp/charts/WorldMap/css/Worldmap.css',
        'src/main/webapp/workflow/assets/css/joint.css',
        'src/main/webapp/workflow/assets/css/jqx.base.css',
        'src/main/webapp/workflow/assets/css/chosen.min.css'
        ])
    .pipe(concat('common-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('common-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Common Stream Task  --*/
gulp.task('commonStyles', function() {
    return gulp.src(['src/main/webapp/assets/scss/theme/path/common-styles.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('common-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('common-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
*********************   Predict Module Stream Tasks      ***********************
================================================================================
*/
/*--  Predict Vendor Stream Task  --*/
gulp.task('predictVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('predict-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/predict/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('predict-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/predict/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Predict Stream Task  --*/
gulp.task('predictStyles', function() {
    return gulp.src(['src/main/webapp/predict/assets/scss/predict.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('predict-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/predict/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('predict-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/predict/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
**********************   Workflow Module Stream Tasks      *********************
================================================================================
*/
/*--  Workflow Vendor Stream Task  --*/
gulp.task('workflowVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css',
        'src/main/webapp/workflow/assets/css/joint.css',
        'src/main/webapp/workflow/assets/css/jqx.base.css',
        'src/main/webapp/workflow/assets/css/chosen.min.css'
        ])
    .pipe(concat('workflow-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('workflow-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Work-flow Stream Task  --*/
gulp.task('workflowStyles', function() {
    return gulp.src(['src/main/webapp/workflow/assets/scss/workflow.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('workflow-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('workflow-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
**********************   Entity Module Stream Tasks      ***********************
================================================================================
*/
/*--  Entity Vendor Stream Task  --*/
gulp.task('entityVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('entity-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/entity/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('entity-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/entity/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Entity Stream Task  --*/
gulp.task('entityStyles', function() {
    return gulp.src(['src/main/webapp/entity/assets/scss/entity.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('entity-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/entity/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('entity-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/entity/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
************************   MIP Module Stream Tasks      ************************
================================================================================
*/

/*--  MIP Vendor Stream Task  --*/
gulp.task('mipVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('mip-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/mip/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('mip-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/mip/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  MIP Stream Task  --*/
gulp.task('mipStyles', function() {
    return gulp.src(['src/main/webapp/mip/assets/scss/mip.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('mip-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/mip/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('mip-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/mip/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
****************   Transaction Intelligence Module Tasks      ******************
================================================================================
*/
/*--  Transaction Intelligence Vendor Stream Task  --*/
gulp.task('transactionIntelligenceVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('transaction-intelligence-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('transaction-intelligence-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Transaction Intelligence Stream Task  --*/
gulp.task('transactionIntelligenceStyles', function() {
    return gulp.src(['src/main/webapp/transactionIntelligence/assets/scss/transaction-intelligence.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('transaction-intelligence-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('transaction-intelligence-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
************************  Lead Generation Module Tasks  ************************
================================================================================
*/
/*--  Lead Generation Vendor Stream Task  --*/
gulp.task('leadGenerationVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('lead-generation-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('lead-generation-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Lead Generation Stream Task  --*/
gulp.task('leadGenerationStyles', function() {
    return gulp.src(['src/main/webapp/leadGeneration/assets/scss/lead-generation.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('lead-generation-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('lead-generation-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
************************  Doc Parser Module Tasks  ************************
================================================================================
*/
/*--  Doc Parser Vendor Stream Task  --*/
gulp.task('docParserVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('doc-parser-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('doc-parser-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Doc Parser Stream Task  --*/
gulp.task('docParserStyles', function() {
    return gulp.src(['src/main/webapp/docparser/assets/scss/doc-parser.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('doc-parser-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('doc-parser-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
***************************  Due Module Tasks  ***************************
================================================================================
*/
/*--  Due Module Vendor Stream Task  --*/
gulp.task('dueVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('due-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('due-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Due Module Stream Task  --*/
gulp.task('dueStyles', function() {
    return gulp.src(['src/main/webapp/entitySearch/assets/scss/due.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('due-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('due-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
********************  Identity Management Module Tasks  ************************
================================================================================
*/
/*--  Identity Management Vendor Stream Task  --*/
gulp.task('identityManagementVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('identity-management-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('identity-management-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Identity Management Stream Task  --*/
gulp.task('identityManagementStyles', function() {
    return gulp.src(['src/main/webapp/identityManagement/assets/scss/identity-management.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('identity-management-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('identity-management-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
**********************  Upload Documents Module Tasks  *************************
================================================================================
*/
/*--  Identity Management Vendor Stream Task  --*/
gulp.task('uploadDocumentsVendorStyles', function() {
    return gulp.src([
        'src/main/webapp/assets/css/common-vendor.min.css'
        ])
    .pipe(concat('upload-documents-vendor.css'))
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('upload-documents-vendor.min.css'))
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*--  Identity Management Stream Task  --*/
gulp.task('uploadDocumentsStyles', function() {
    return gulp.src(['src/main/webapp/uploadDocuments/assets/scss/upload-documents.scss'])
    .pipe(sass({ errLogToConsole: true }))
    .on('error', function(err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(concat('upload-documents-styles.css'))
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/css'))
    .pipe(minifyCSS())
    .pipe(concat('upload-documents-styles.min.css'))
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/css'))
    .pipe(gulp.dest('./dist/css'));
});

/*
================================================================================
*************************   Stream Tasks Ends    *******************************
================================================================================
*/

/*
================================================================================
***************************   Js Tasks Starts    *******************************
================================================================================
*/

/*--  Identity Management Scripts Js Compress Task  --*/
gulp.task('IDMScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/identityManagement/services/identityManagement.api.service.js',
        'src/main/webapp/identityManagement/directives/custom.keys.directive.js',
        'src/main/webapp/identityManagement/directives/scroll.to.active.directive.js',
        'src/main/webapp/identityManagement/controllers/groups.controller.js',
        'src/main/webapp/identityManagement/controllers/personal.controller.js',
        'src/main/webapp/identityManagement/controllers/users.controller.js',
        'src/main/webapp/identityManagement/modal/controllers/changePasswordController.js',
        'src/main/webapp/identityManagement/modal/controllers/createGroupController.js',
        'src/main/webapp/identityManagement/modal/controllers/createUser.controller.js',
        'src/main/webapp/identityManagement/modal/controllers/deleteUser.controller.js',
        'src/main/webapp/identityManagement/modal/controllers/delete.group.controller.js',
        'src/main/webapp/identityManagement/modal/controllers/delete.group.member.controller.js'
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('IDM-scripts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Workflow Vendor Js Compress Task  --*/
gulp.task('workflowVendorJs', function () {
    return gulp.src([
        'src/main/webapp/vendor/jquery/js/underscore.min.js',
        'src/main/webapp/vendor/jquery/js/lodash.js',
        'src/main/webapp/vendor/jquery/js/backbone.js',
        'src/main/webapp/vendor/jquery/js/joint.js',
        'src/main/webapp/vendor/jquery/js/jqx-all.js',

        'src/main/webapp/vendor/jquery/js/jquery.resize.js',
        'src/main/webapp/vendor/jquery/js/moment.min.js',
        'src/main/webapp/vendor/jquery/js/chosen.jquery.min.js',
        
        'src/main/webapp/vendor/jquery/js/jquery.nicescroll.min.js',
        'src/main/webapp/vendor/jquery/js/jquery-ui-timepicker-addon.js',
        'src/main/webapp/vendor/jquery/js/jquery-ui-sliderAccess.js',
        'src/main/webapp/vendor/jquery/js/moment-timezone-with-data.min.js',
        'src/main/webapp/vendor/jquery/js/moment-timezone-utils.js',
        'src/main/webapp/vendor/jquery/js/moment-timezone.min.js',
        'src/main/webapp/vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js',
        
        'src/main/webapp/vendor/jquery/js/ElementQueries.js',
        'src/main/webapp/vendor/jquery/js/ResizeSensor.js',
        'src/main/webapp/vendor/jquery/js/newlodash.js',
        
        'src/main/webapp/vendor/angular/js/angular-datatables.min.js',
        'src/main/webapp/vendor/angular/js/angular-resource.min.js',
        
        'src/main/webapp/workflow/assets/codemirror/lib/codemirror.js',
        'src/main/webapp/workflow/assets/codemirror/mode/python/python.js',
        'src/main/webapp/workflow/assets/codemirror/mode/r/r.js'
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))    
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('workflow-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Workflow Scripts Js Compress Task  --*/
gulp.task('workflowScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/workflow/constants/workflow.constant.js',
        'src/main/webapp/workflow/controllers/workflow.dashboard.controller.js',
        'src/main/webapp/workflow/controllers/edit.workflow.controller.js',
        'src/main/webapp/workflow/services/workflow.dashboard.api.services.js'
        
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('workflow-scripts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Entity Vendor Js Compress Task  --*/
gulp.task('entityVendorJs', function () {
    return gulp.src([
        'src/main/webapp/vendor/jquery/js/moment.min.js',
        'src/main/webapp/vendor/jquery/js/dom-to-image.js',
        'src/main/webapp/vendor/jquery/js/html2pdf.bundle.min.js',
        'src/main/webapp/vendor/angular/js/ngStorage.js',
        'src/main/webapp/vendor/angular/js/angular-moment.min.js',
        'src/main/webapp/vendor/angular/js/socket.io.min.js',        
        'src/main/webapp/scripts/VLA/js/loadash.js',
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('entity-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entity/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Entity Scripts Js Compress Task  --*/
gulp.task('entityScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/scripts/common/constants/enrich.search.constant.js',
        'src/main/webapp/entity/constants/fetchers.list.constant.js',
        'src/main/webapp/entity/constants/entityCompany.constant.js',
        'src/main/webapp/scripts/enrich/constants/entity.search.constant.js',
        
        'src/main/webapp/entity/services/entity.graph.services.js',
        'src/main/webapp/scripts/enrich/services/enrich.graph.service.js',
        'src/main/webapp/scripts/enrich/services/entity.api.services.js',
        
        'src/main/webapp/entity/controllers/entity.company.new.controller.js',
        'src/main/webapp/entity/controllers/entity.person.new.controller.js',
        'src/main/webapp/entity/modal/controller/entityRIskModal.controller.js',
        'src/main/webapp/entity/modal/controller/overview.modal.controller.js',
        'src/main/webapp/entity/modal/controller/leadership.additional.modal.controller.js',
        'src/main/webapp/entity/modal/controller/twitter.followers.following.controller.js',
        'src/main/webapp/entity/modal/controller/bad.buyers.modal.controller.js',
        'src/main/webapp/entity/modal/controller/bad.buyers.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/comments.modal.controller.js',
        'src/main/webapp/entity/modal/controller/crimestoppers.uk.modal.controller.js',
        'src/main/webapp/entity/modal/controller/domain.company.modal.controller.js',
        'src/main/webapp/entity/modal/controller/employment.history.modal.controller.js',
        'src/main/webapp/entity/modal/controller/exclusions.fraud.modal.controller.js',
        'src/main/webapp/entity/modal/controller/fbi.list.modal.controller.js',
        'src/main/webapp/entity/modal/controller/followers.modal.controller.js',
        'src/main/webapp/entity/modal/controller/following.modal.controller.js',
        'src/main/webapp/entity/modal/controller/general.info.modal.controller.js',
        'src/main/webapp/entity/modal/controller/general.modal.controller.js',
        'src/main/webapp/entity/modal/controller/imsasllc.modal.controller.js',
        'src/main/webapp/entity/modal/controller/industry.modal.controller.js',
        'src/main/webapp/entity/modal/controller/key.relationships.modal.controller.js',
        'src/main/webapp/entity/modal/controller/key.staff.schools.modal.controller.js',
        'src/main/webapp/entity/modal/controller/latest.release.modal.controller.js',
        'src/main/webapp/entity/modal/controller/news.articles.modal.controller.js',
        'src/main/webapp/entity/modal/controller/news.list.modal.controller.js',
        'src/main/webapp/entity/modal/controller/adverse.news.modal.controller.js',      
        'src/main/webapp/entity/modal/controller/news.modal.controller.js',
        'src/main/webapp/entity/modal/controller/open.payments.modal.controller.js',
        'src/main/webapp/entity/modal/controller/organizations.modal.controller.js',
        'src/main/webapp/entity/modal/controller/person.education.modal.controller.js',
        'src/main/webapp/entity/modal/controller/person.interests.modal.controller.js',        
        'src/main/webapp/entity/modal/controller/previous.location.modal.controller.js',
        'src/main/webapp/entity/modal/controller/products.modal.controller.js',
        'src/main/webapp/entity/modal/controller/related.org.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/related.organization.modal.controller.js',
        'src/main/webapp/entity/modal/controller/related.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/related.person.org.controller.js',
        'src/main/webapp/entity/modal/controller/related.person.people.modal.controller.js',
        'src/main/webapp/entity/modal/controller/replies.modal.controller.js',
        'src/main/webapp/entity/modal/controller/sanctionssearch.list.modal.controller.js',
        'src/main/webapp/entity/modal/controller/scam.report.modal.controller.js',
        'src/main/webapp/entity/modal/controller/scam.website.modal.controller.js',
        'src/main/webapp/entity/modal/controller/sex.offenders.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/social.media.articles.modal.controller.js',        
        'src/main/webapp/entity/modal/controller/social.media.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/stocks.modal.controller.js',
        'src/main/webapp/entity/modal/controller/supplier.black.list.modal.controller.js',
        'src/main/webapp/entity/modal/controller/thumbnails.modal.controller.js',
        'src/main/webapp/entity/modal/controller/unlimitedcriminalchecks.modal.controller.js',
        'src/main/webapp/entity/modal/controller/reddit.comments.modal.controller.js',

        'src/main/webapp/entity/modal/controller/balance.sheet.modal.controller.js',
        'src/main/webapp/entity/modal/controller/income.sheet.modal.controller.js',
        'src/main/webapp/entity/modal/controller/cashflow.sheet.modal.controller.js',
        'src/main/webapp/entity/modal/controller/finance.modal.controller.js',

        'src/main/webapp/entity/modal/controller/question.answers.modal.controller.js',
        'src/main/webapp/entity/modal/controller/finance.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/overview.person.modal.controller.js',
        'src/main/webapp/entity/modal/controller/risk.personoffence.modal.controller.js',
        'src/main/webapp/entity/modal/controller/social.follwers.modal.controller.js',
        'src/main/webapp/entity/modal/controller/QuestionsPerAnswersStatusController.js'
        ])
.pipe(uglify({ output: { ascii_only: true } })
    .pipe(babel({
        "compact": false,
        presets: ['es2015']
    }))
    .on('error', function(e){
        console.log(e);
    }))
.pipe(concat('entity-scripts.min.js'))    
.pipe(gulp.dest('./src/main/webapp/entity/assets/assets/minifiedJs'))
.pipe(gulp.dest('dist/js'));
});

/*--  Entity Chart Js Compress Task  --*/
gulp.task('entityChartJs', function () {
    return gulp.src([
        'src/main/webapp/charts/groupedColumChart.js',
        'src/main/webapp/charts/timescaleBubble.js',
        'src/main/webapp/charts/timescaleBubble2.js',
        'src/main/webapp/charts/bubbleChart.js',
        'src/main/webapp/charts/pieChart.js',
        'src/main/webapp/charts/leaflet/leaflet.js',
        'src/main/webapp/charts/leaflet/TileLayer.Grayscale.js',
        'src/main/webapp/charts/hotTopics.js',
        'src/main/webapp/charts/topojson.min.js',
        'src/main/webapp/charts/cloud.js',
        'src/main/webapp/charts/ClusterBubble.js',
        'src/main/webapp/charts/caseTimeline.js',
        'src/main/webapp/charts/WorldMap/js/WorldChart.js',
        'src/main/webapp/charts/brush.js',
        'src/main/webapp/charts/collapse.js',
        'src/main/webapp/charts/sank.min.js',
        'src/main/webapp/charts/reusablePie.js',
        'src/main/webapp/charts/networkChart.js',
        'src/main/webapp/charts/networkChartEntity.js',
        'src/main/webapp/charts/lineChart.js',
        'src/main/webapp/charts/horizontalBarChart.js',
        'src/main/webapp/charts/verticalBarChart.js',
        'src/main/webapp/charts/cytoscape-cola.js',
//        'src/main/webapp/charts/datamaps.all.js',
'src/main/webapp/charts/brushMip.js',
'src/main/webapp/charts/timelineSimpleColumn.js',
'src/main/webapp/charts/bubbleEntity.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/temporal.js',
'src/main/webapp/charts/D3treeMap.js',
'src/main/webapp/charts/D3radarChart.js',
'src/main/webapp/charts/D3heatMap.js',
'src/main/webapp/charts/sankRiskScore.js',
'src/main/webapp/charts/ordinalLineChart.js',
'src/main/webapp/charts/timeformat.js',
'src/main/webapp/charts/timeformatsupport.js'

])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('entity-charts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entity/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Mip Vendor Js Compress Task  --*/
gulp.task('mipVendorJs', function () {
    return gulp.src([
        'src/main/webapp/scripts/VLA/js/typeahead.bundle.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js',
        'src/main/webapp/scripts/VLA/js/weaver.min.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-spread.js',
//        'src/main/webapp/vendor/jquery/js/leaflet-image.js',
'src/main/webapp/charts/leaflet/leaflet.js',
'src/main/webapp/charts/topojson.min.js',
//        'src/main/webapp/charts/datamaps.all.js',
'src/main/webapp/charts/WorldMap/js/WorldChart.js',
'src/main/webapp/charts/reusablePie.js',
'src/main/webapp/charts/horizontalBarChart.js',
'src/main/webapp/charts/verticalBarChart.js',
'src/main/webapp/charts/brushMip.js',
'src/main/webapp/charts/lineChart.js'
])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('mip-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Mip Scripts Js Compress Task  --*/
gulp.task('mipScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/mip/constants/mip.constant.js',

        'src/main/webapp/mip/services/mip.api.service.js',
        'src/main/webapp/mip/services/mip.service.js',

        'src/main/webapp/mip/controllers/mip.landing.controller.js',
        'src/main/webapp/mip/controllers/mip.controller.js',

        'src/main/webapp/mip/modal/controllers/mip.save.modal.controller.js'
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('mip-scripts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  TransactionIntelligence Scripts Js Compress Task  --*/
gulp.task('transactionIntelligenceScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/scripts/common/constants/enrich.search.constant.js',
        'src/main/webapp/scripts/enrich/services/enrich.graph.service.js',
        'src/main/webapp/scripts/enrich/services/entity.api.services.js',
        'src/main/webapp/entity/services/entity.graph.services.js',
        'src/main/webapp/transactionIntelligence/controllers/transactionIntelligence.landing.controller.js',
        'src/main/webapp/transactionIntelligence/controllers/transactionIntelligence.controller.js',
        'src/main/webapp/transactionIntelligence/controllers/alerts.dashboard.controller.js',
        'src/main/webapp/scripts/common/directives/context.menu.directive.js',

        'src/main/webapp/transactionIntelligence/modal/controllers/fullAnalysis.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/articleVisualizer.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/createAlert.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/resolveAnalysis.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/riskAlert.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/viewAllTrans.modal.controller.js',
        'src/main/webapp/transactionIntelligence/modal/controllers/sideRisk.modal.controller.js',
        'src/main/webapp/transactionIntelligence/services/transaction.analysis.api.service.js',
        'src/main/webapp/transactionIntelligence/services/transaction.graph.service.js',

        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('transactionIntelligence-scripts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  TransactionIntelligence Vendor Js Compress Task  --*/
gulp.task('transactionIntelligenceVendorJs', function () {
    return gulp.src([        
        'src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-navigator.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js',
        'src/main/webapp/scripts/VLA/js/weaver.min.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-spread.js',
        'src/main/webapp/scripts/VLA/js/random-color.js',
        'src/main/webapp/scripts/VLA/js/spectrum.js',
        'src/main/webapp/scripts/VLA/js/typeahead.bundle.js',
        'src/main/webapp/scripts/VLA/js/jquery.selectric.min.js',
        'src/main/webapp/scripts/VLA/js/custom_config.js',
        'src/main/webapp/scripts/VLA/js/app.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ua.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ar.js',
'src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js',
'src/main/webapp/scripts/VLA/js/bluebird.min.js',
'src/main/webapp/scripts/VLA/js/base64toblob.js',
'src/main/webapp/scripts/VLA/js/loadash.js',
'src/main/webapp/vendor/jquery/js/moment.min.js',
'src/main/webapp/vendor/jquery/js/displacy-entity.min.js',
'src/main/webapp/vendor/angular/js/angular-moment.min.js',
'src/main/webapp/vendor/angular/js/angular-datatables.min.js',
'src/main/webapp/vendor/angular/js/FileSaver.min.js',
'src/main/webapp/vendor/jquery/js/jquery-daterangepicker.js',
'src/main/webapp/vendor/jquery/js/raphael.min.js',
'src/main/webapp/vendor/jquery/js/raphael.icons.min.js',
'src/main/webapp/vendor/jquery/js/wheelnav.min.js'
])
    .pipe(uglify({ output: { ascii_only: true } })
    /*.pipe(babel({
        "compact": false,
        presets: ['es2015']
    }))*/
    .on('error', function(e){
        console.log(e);
    }))
    .pipe(concat('transactionIntelligence-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  TransactionIntelligence Charts Js Compress Task  --*/
gulp.task('transactionIntelligenceChartsJs', function () {
    return gulp.src([
        'src/main/webapp/charts/topojson.min.js',
//        'src/main/webapp/charts/datamaps.all.js'
'src/main/webapp/charts/WorldMap/js/WorldChart.js',
'src/main/webapp/charts/lineChart.js',
'src/main/webapp/charts/reusablePie.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/cloud.js',
'src/main/webapp/charts/hotTopics.js',
'src/main/webapp/charts/groupedColumChart.js',
'src/main/webapp/charts/bubbleEntity.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/timeformat.js',
'src/main/webapp/charts/timeformatsupport.js',
'src/main/webapp/charts/SimpleBarChart.js',
'src/main/webapp/charts/verticalNegativeBarChart.js',
'src/main/webapp/charts/ordinalLineChart.js'

])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('transactionIntelligence-charts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

///*--  Lead Generation Scripts Js Compress Task  --*/
gulp.task('leadGenerationScriptsJs', function () {
    return gulp.src([
        "src/main/webapp/leadGeneration/constants/leadGenaration.constant.js",
        "src/main/webapp/leadGeneration/services/leadGeneration.api.service.js",
        "src/main/webapp/leadGeneration/services/leadGeneration.graph.service.js",
        'src/main/webapp/entity/services/entity.graph.services.js',
//    'src/main/webapp/scripts/enrich/services/enrich.graph.service.js',
'src/main/webapp/scripts/enrich/services/entity.api.services.js',
'src/main/webapp/scripts/discover/services/discover.api.service.js',
"src/main/webapp/leadGeneration/controllers/leadGeneration_DualCardHolderDetails.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGeneration_GeneralCardHolderDetails.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGeneration_PredictingDualCardHolderDetails.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGenerationCluster.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGenerationCustomerPage.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGenerationEarlyAdopters.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGenerationLanding.controller.js",
"src/main/webapp/leadGeneration/controllers/leadGenerationCommonfilter.js",

])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('leadgeneration-scripts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Lead Generation Vendor Js Compress Task  --*/
gulp.task('leadgenerationVendorJs', function () {
    return gulp.src([        
        'src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-navigator.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js',
        'src/main/webapp/scripts/VLA/js/weaver.min.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-spread.js',
        'src/main/webapp/scripts/VLA/js/random-color.js',
        'src/main/webapp/scripts/VLA/js/spectrum.js',
        'src/main/webapp/scripts/VLA/js/typeahead.bundle.js',
        'src/main/webapp/scripts/VLA/js/jquery.selectric.min.js',
        'src/main/webapp/scripts/VLA/js/custom_config.js',
        'src/main/webapp/scripts/VLA/js/app.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ua.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ar.js',
'src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js',
'src/main/webapp/scripts/VLA/js/bluebird.min.js',
'src/main/webapp/scripts/VLA/js/base64toblob.js',
'src/main/webapp/scripts/VLA/js/loadash.js',
'src/main/webapp/vendor/jquery/js/moment.min.js',
//        'src/main/webapp/vendor/jquery/js/displacy-entity.min.js',
'src/main/webapp/vendor/angular/js/angular-moment.min.js',
'src/main/webapp/vendor/angular/js/angular-datatables.min.js',
'src/main/webapp/vendor/angular/js/FileSaver.min.js'
//        'src/main/webapp/vendor/jquery/js/jquery-daterangepicker.js'
])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('leadgeneration-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Lead Generation Charts Js Compress Task  --*/
gulp.task('leadgenerationChartsJs', function () {
    return gulp.src([
        'src/main/webapp/charts/topojson.min.js',
//        'src/main/webapp/charts/datamaps.all.js',
'src/main/webapp/charts/WorldMap/js/WorldChart.js',
'src/main/webapp/charts/lineChart.js',
'src/main/webapp/charts/reusablePie.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/cloud.js',
'src/main/webapp/charts/hotTopics.js',
'src/main/webapp/charts/groupedColumChart.js',
'src/main/webapp/charts/bubbleEntity.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/timeformat.js',
'src/main/webapp/charts/timeformatsupport.js',
'src/main/webapp/charts/SimpleBarChart.js',
'src/main/webapp/charts/verticalNegativeBarChart.js',
'src/main/webapp/charts/ordinalLineChart.js',
'src/main/webapp/charts/areaLineChart.js',
'src/main/webapp/charts/networkChartEntity.js'

])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('leadgeneration-charts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('datamapsScriptsJs',function(){
    return gulp.src([
        'src/main/webapp/charts/datamaps.all.js',
        'src/main/webapp/scripts/VLA/js/cola.v3.min.js',
        'src/main/webapp/vendor/jquery/js/leaflet-image.js'
        ])
    .pipe(uglify({output : { ascii_only : true}})
        .on('error',function(e){
            console.log(e);
        }))
    .pipe(concat('datamaps-scripts.min.js'))
    .pipe(gulp.dest('./src/main/webapp/vendor'))
    .pipe(gulp.dest('dist/js'));
});

// for datamaps.all.js
gulp.task('uploadDocumentsScriptsJs',function(){
    return gulp.src([
        'src/main/webapp/uploadDocuments/scripts/analyzeDocuments.controller.js',
        'src/main/webapp/uploadDocuments/scripts/uploadDocuments.controller.js',
        'src/main/webapp/uploadDocuments/modal/controllers/news.controller.js',
        'src/main/webapp/uploadDocuments/services/uploadDocuments.api.service.js'
        ])
    .pipe(uglify({output : { ascii_only : true}})
        .pipe(babel({presets:['es2015']}))
        .on('error',function(e){
            console.log(e);
        }))
    .pipe(concat('upload-documents-scripts.min.js'))
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Common Vendor Js Compress Task  --*/
gulp.task('commonVendorJs', function () {
    return gulp.src([
        'src/main/webapp/vendor/jquery/js/jquery.min.js',
        'src/main/webapp/vendor/jquery/js/jquery-ui.js',
        'src/main/webapp/vendor/jquery/js/jquery.mousewheel.min.js',
        'src/main/webapp/vendor/jquery/js/jquery.mThumbnailScroller.min.js',
        'src/main/webapp/vendor/jquery/js/jquery.mCustomScrollbar.js',
        'src/main/webapp/vendor/jquery/js/jquery.scrollbar.js',
        'src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js',
        'src/main/webapp/scripts/VLA/js/jquery.qtip.js',
        'src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js',
        'src/main/webapp/vendor/jquery/js/bootstrap.min.js',
        'src/main/webapp/vendor/jquery/js/jquery.ui.widget.js',
        'src/main/webapp/vendor/jquery/js/jquery.iframe-transport.js',
        'src/main/webapp/vendor/jquery/js/jquery.fileupload.js',
        'src/main/webapp/vendor/jquery/js/jquery-daterangepicker.js',
//        'src/main/webapp/vendor/jquery/js/bootstrap-daterangepicker.js',       

'src/main/webapp/vendor/angular/js/angular.min.js',
'src/main/webapp/vendor/angular/js/angular-ui-router.min.js',
'src/main/webapp/vendor/angular/js/ui-bootstrap-tpls-2.5.0.min.js',
'src/main/webapp/vendor/angular/js/html2canvas.js',
'src/main/webapp/vendor/angular/js/ng-file-upload.min.js',
'src/main/webapp/vendor/angular/js/angular-flash.min.js',

'src/main/webapp/charts/d3.v3.min.js',
'src/main/webapp/charts/d3.v4.min.js',
'src/main/webapp/charts/d3v3.js',
'src/main/webapp/charts/d3js.js',

//        'src/main/webapp/scripts/VLA/js/cola.v3.min.js',
'src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js',
'src/main/webapp/scripts/VLA/js/cytoscape-cola.js',
'src/main/webapp/scripts/VLA/js/cytoscape-qtip.js',
'src/main/webapp/vendor/angular/js/angular_multiselect.js',
'src/main/webapp/vendor/jquery/js/jquery.orgchart.js'

])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('common-vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/vendor'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Common Script Js Compress Task  --*/
gulp.task('commonScriptsJs', function () {
    return gulp.src([
        'src/main/webapp/scripts/common/constants/common.constant.js',
        'src/main/webapp/scripts/discover/constants/discover.constant.js',
        'src/main/webapp/scripts/act/constants/act.constant.js',
        
        'src/main/webapp/scripts/common/services/shared.service.js',
        'src/main/webapp/scripts/common/services/common.service.js',
        'src/main/webapp/scripts/common/services/top.panel.api.service.js',
        'src/main/webapp/scripts/common/services/upload.file.service.js',
        'src/main/webapp/scripts/discover/services/discover.api.service.js',
        'src/main/webapp/scripts/act/services/act.api.service.js',
        'src/main/webapp/scripts/act/services/act.graph.service.js',
        'src/main/webapp/scripts/manage/services/generalSettings.api.service.js',
        'src/main/webapp/scripts/common/services/common.service.js',
        'src/main/webapp/scripts/common/services/riskScore.service.js',

        'src/main/webapp/scripts/common/filters/common.filters.js',

        'src/main/webapp/scripts/common/js/submenu.controller.js',
        'src/main/webapp/scripts/common/js/top.panel.controller.js',
        'src/main/webapp/scripts/common/js/advanced.search.controller.js',
        'src/main/webapp/scripts/common/js/user.events.controller.js',
        'src/main/webapp/scripts/common/js/my.clipboard.controller.js',
        'src/main/webapp/scripts/common/js/notification.controller.js',
        'src/main/webapp/scripts/common/modal/js/entityRIskModal.controller.js',
        'src/main/webapp/scripts/common/modal/js/add.media.modal.controller.js',
        'src/main/webapp/scripts/common/modal/js/widget.capture.modal.controller.js', 
        'src/main/webapp/scripts/common/modal/js/maximize.modal.controller.js', 
        'src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js', 
        'src/main/webapp/scripts/common/modal/js/dataPopUp.modal.controller.js',

        'src/main/webapp/scripts/common/js/entity.visualiser.js',
        'src/main/webapp/scripts/common/js/chatbot.controller.js',

        'src/main/webapp/scripts/common/modal/js/onboarding.modal.controller.js',
        'src/main/webapp/scripts/common/modal/js/uploadquestionaire.controller.js',
        'src/main/webapp/scripts/common/modal/js/upload.documents.modal.controller.js',
        'src/main/webapp/scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js',
        'src/main/webapp/scripts/common/modal/js/idv.questionaire.controller.js',
        'src/main/webapp/scripts/common/modal/js/create.event.controller.js',
        'src/main/webapp/scripts/common/modal/js/participants.event.controller.js',

        'src/main/webapp/scripts/enrich/services/enrich.api.service.js',
        'src/main/webapp/scripts/common/modal/js/riskScore.modal.controller.js'
        ])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))    
    .pipe(concat('common-scripts.min.js'))
    .pipe(gulp.dest('./src/main/webapp/scripts'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Vendor Js Compress Task  --*/
gulp.task('vendorJs', function () {
    return gulp.src([
        'src/main/webapp/vendor/jquery/js/lodash.js',
        'src/main/webapp/vendor/jquery/js/raphael.min.js',
        'src/main/webapp/vendor/jquery/js/raphael.icons.min.js',
        'src/main/webapp/vendor/jquery/js/wheelnav.min.js',
        'src/main/webapp/vendor/jquery/js/jquery.resize.js',
        'src/main/webapp/vendor/jquery/js/moment.min.js',
        'src/main/webapp/vendor/jquery/js/underscore.min.js',
        'src/main/webapp/vendor/jquery/js/displacy-entity.min.js',
        'src/main/webapp/vendor/jquery/js/leaflet-image.js',
        
        'src/main/webapp/vendor/angular/js/angular-gridster.js',
        'src/main/webapp/vendor/angular/js/angular-datatables.min.js',
        'src/main/webapp/vendor/angular/js/FileSaver.min.js',
        'src/main/webapp/vendor/angular/js/socket.io.min.js',
        'src/main/webapp/vendor/angular/js/angular-resource.min.js',
        'src/main/webapp/vendor/angular/js/angular-timeline.js',
        'src/main/webapp/vendor/angular/js/angular-moment.min.js',
        'src/main/webapp/vendor/angular/js/ngStorage.js',
        'src/main/webapp/vendor/angular/js/angular-cookies.js',
        'src/main/webapp/vendor/jquery/js/jquery.nicescroll.min.js',
        'src/main/webapp/vendor/jquery/js/jquery-ui-timepicker-addon.js',
        'src/main/webapp/vendor/jquery/js/jquery-ui-sliderAccess.js',
        "src/main/webapp/vendor/jquery/js/moment-timezone-with-data.min.js",
        "src/main/webapp/vendor/jquery/js/dom-to-image.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone-utils.js",
        "src/main/webapp/vendor/jquery/js/angularjs-dropdown-multiselect.js"
        ])
    .pipe(uglify({ output: { ascii_only: true } })
//    .pipe(babel({
//        "compact": false,
//        presets: ['es2015']
//    }))
.on('error', function(e){
    console.log(e);
}))
    .pipe(concat('vendor.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/vendor'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Chart Js Compress Task  --*/
gulp.task('chartJs', function () {
    return gulp.src([
        'src/main/webapp/charts/groupedColumChart.js',
        'src/main/webapp/charts/timescaleBubble.js',
        'src/main/webapp/charts/timescaleBubble2.js',
        'src/main/webapp/charts/bubbleChart.js',
        'src/main/webapp/charts/pieChart.js',
        'src/main/webapp/charts/leaflet/leaflet.js',
        'src/main/webapp/charts/hotTopics.js',
        'src/main/webapp/charts/topojson.min.js',
        'src/main/webapp/charts/cloud.js',
        'src/main/webapp/charts/ClusterBubble.js',
        'src/main/webapp/charts/caseTimeline.js',
        'src/main/webapp/charts/WorldMap/js/WorldChart.js',
        'src/main/webapp/charts/brush.js',
        'src/main/webapp/charts/collapse.js',
        'src/main/webapp/charts/sank.min.js',
        'src/main/webapp/charts/reusablePie.js',
        'src/main/webapp/charts/networkChart.js',
        'src/main/webapp/charts/networkChartEntity.js',
        'src/main/webapp/charts/lineChart.js',
        'src/main/webapp/charts/horizontalBarChart.js',
        'src/main/webapp/charts/verticalBarChart.js',
        'src/main/webapp/charts/cytoscape-cola.js',
//        'src/main/webapp/charts/datamaps.all.js',
'src/main/webapp/charts/brushMip.js',
'src/main/webapp/charts/timelineSimpleColumn.js',
'src/main/webapp/charts/bubbleEntity.js',
'src/main/webapp/charts/stackedtime.js',
'src/main/webapp/charts/temporal.js',
'src/main/webapp/charts/sankRiskScore.js',
'src/main/webapp/charts/D3radarChart.js',
'src/main/webapp/charts/leaflet/TileLayer.Grayscale.js'
])
    .pipe(uglify({ output: { ascii_only: true } })
        .pipe(babel({
            "compact": false,
            presets: ['es2015']
        }))
        .on('error', function(e){
            console.log(e);
        }))
    .pipe(concat('charts.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/charts'))
    .pipe(gulp.dest('dist/js'));
});

/*--  Script Js Compress Task  --*/
gulp.task('scriptsJs', function () {
    return gulp.src([
        'src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-navigator.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js',
        'src/main/webapp/scripts/VLA/js/weaver.min.js',
        'src/main/webapp/scripts/VLA/js/cytoscape-spread.js',
        'src/main/webapp/scripts/VLA/js/random-color.js',
        'src/main/webapp/scripts/VLA/js/spectrum.js',
        'src/main/webapp/scripts/VLA/js/jquery.qtip.js',
        'src/main/webapp/scripts/VLA/js/typeahead.bundle.js',
        'src/main/webapp/scripts/VLA/js/jquery.selectric.min.js',
        'src/main/webapp/scripts/VLA/js/custom_config.js',
        'src/main/webapp/scripts/VLA/js/app.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ua.js',
//        'src/main/webapp/scripts/VLA/js/app_locale_ar.js',
'src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js',
'src/main/webapp/scripts/VLA/js/bluebird.min.js',
'src/main/webapp/scripts/VLA/js/base64toblob.js',
 //       'src/main/webapp/scripts/VLA/js/loadash.js',

 'src/main/webapp/scripts/app.js',
 'src/main/webapp/scripts/app.config.js',
 'src/main/webapp/scripts/app.run.js', 

 'src/main/webapp/scripts/common/constants/enrich.search.constant.js',
 'src/main/webapp/scripts/transactionMonitoring/constants/transmonitoring.constant.js',
//        'src/main/webapp/scripts/adverseNews/constants/adverse.news.constant.js',

'src/main/webapp/scripts/discover/services/discover.graph.service.js',
'src/main/webapp/scripts/enrich/services/enrich.graph.service.js',
'src/main/webapp/scripts/enrich/services/entity.api.services.js',

'src/main/webapp/scripts/transactionMonitoring/services/transmonitoring.api.service.js',
'src/main/webapp/scripts/transactionMonitoring/services/transmonitoring.graph.service.js',
//        'src/main/webapp/scripts/adverseNews/services/adverse.news.api.service.js',
//        'src/main/webapp/scripts/adverseNews/services/adverse.news.service.js',
'src/main/webapp/scripts/discover/services/investigation.landing.service.js',
'src/main/webapp/transactionIntelligence/services/transaction.analysis.api.service.js',
'src/main/webapp/transactionIntelligence/services/transaction.graph.service.js',

'src/main/webapp/scripts/discover/directives/work.diary.directive.js',  

'src/main/webapp/scripts/domain/js/domain.controller.js',
'src/main/webapp/scripts/process/js/process.controller.js',

'src/main/webapp/scripts/common/directives/context.menu.directive.js',
'src/main/webapp/scripts/common/modal/js/filter.case.modal.controller.js',

'src/main/webapp/scripts/predict/js/predict.dashboard.controller.js', 

'src/main/webapp/scripts/discover/js/workspace.controller.js',
'src/main/webapp/scripts/discover/js/transaction.monitoring.controller.js',
'src/main/webapp/scripts/discover/js/investigation.landing.controller.js',
'src/main/webapp/scripts/discover/js/discover.dashboard.controller.js',
'src/main/webapp/scripts/discover/js/gridster.controller.js',
'src/main/webapp/scripts/discover/js/my.case.diary.controller.js',
'src/main/webapp/scripts/discover/js/top.locations.controller.js',
'src/main/webapp/scripts/discover/js/your.case.metrics.controller.js',
'src/main/webapp/scripts/discover/js/case.in.focus.controller.js',
'src/main/webapp/scripts/discover/js/hot.topics.controller.js',
'src/main/webapp/scripts/discover/js/my.entities.controller.js',
'src/main/webapp/scripts/discover/js/entities.in.focus.controller.js',
'src/main/webapp/scripts/discover/js/your.work.diary.controller.js',
'src/main/webapp/scripts/discover/modal/js/create.case.modal.controller.js',

'src/main/webapp/scripts/act/js/act.dashboard.controller.js',
'src/main/webapp/scripts/act/js/case.page.landing.controller.js',
'src/main/webapp/scripts/act/js/audit.trail.controller.js',
'src/main/webapp/scripts/act/js/entity.page.controller.js',
'src/main/webapp/scripts/act/js/acthottopics.controller.js',
'src/main/webapp/scripts/act/js/acttoplocations.controller.js',
'src/main/webapp/scripts/act/js/associatedentities.controller.js',
'src/main/webapp/scripts/act/js/casetimeline.controller.js',
'src/main/webapp/scripts/act/js/entitiesinfocus.controller.js',
'src/main/webapp/scripts/act/js/gridster.controller.js',
'src/main/webapp/scripts/act/js/related.cases.controller.js',
'src/main/webapp/scripts/act/js/peer.group.controller.js',
'src/main/webapp/scripts/act/js/related.company.controller.js',        
'src/main/webapp/scripts/act/js/riskratio.controller.js',
'src/main/webapp/scripts/act/modal/js/reassign.modal.controller.js',
'src/main/webapp/scripts/act/js/linkanalysis.controller.js',
'src/main/webapp/scripts/act/modal/js/riskoverview.modal.controller.js',

'src/main/webapp/scripts/enrich/js/enrich.dashboard.controller.js',
'src/main/webapp/scripts/enrich/directives/enrich.directives.js',
'src/main/webapp/scripts/enrich/js/dfd.controller.js',
'src/main/webapp/scripts/enrich/js/social.live.feed.controller.js',

'src/main/webapp/scripts/transactionMonitoring/controllers/transaction.monitoring.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/geographic.distribution.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/grouped.peer.analysis.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/risky.transactions.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/transaction.linkage.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/transaction.per.period.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/transactions.journey.controller.js',
'src/main/webapp/scripts/transactionMonitoring/controllers/volume.monitor.controller.js', 

'src/main/webapp/scripts/manage/js/app.manager.controller.js',
'src/main/webapp/scripts/manage/js/data.management.controller.js',
'src/main/webapp/scripts/manage/js/general.settings.controller.js',
'src/main/webapp/scripts/manage/js/manage.dashboard.controller.js',
'src/main/webapp/scripts/manage/js/system.monitoring.controller.js',
'src/main/webapp/scripts/manage/js/user.management.controller.js',
'src/main/webapp/scripts/manage/modal/js/addUser.Modal.Controller.js',
'src/main/webapp/scripts/manage/js/users.controller.js',
'src/main/webapp/scripts/manage/js/groups.controller.js',
'src/main/webapp/scripts/manage/js/manageDb.controller.js',
'src/main/webapp/scripts/manage/js/deployments.controller.js',
'src/main/webapp/scripts/manage/js/jobs.controller.js',
'src/main/webapp/scripts/manage/js/administrations.controller.js',
'src/main/webapp/scripts/manage/js/crystalBall.controller.js',
'src/main/webapp/scripts/manage/js/activeProcess.controller.js',
'src/main/webapp/scripts/manage/js/suspendedProcess.controller.js',
'src/main/webapp/scripts/manage/js/dataSource.controller.js',
'src/main/webapp/scripts/manage/js/tasks.controller.js',
'src/main/webapp/scripts/manage/js/process.instance.controller.js',
'src/main/webapp/scripts/manage/js/reports.controller.js',
'src/main/webapp/scripts/manage/js/deployment.process.controller.js',
'src/main/webapp/scripts/manage/js/model.workspace.controller.js',
'src/main/webapp/scripts/manage/js/addSource.Modal.Controller.js',
'src/main/webapp/scripts/manage/modal/js/deploymentProcess.modal.controller.js',
'src/main/webapp/scripts/manage/modal/js/modelWorkspace.modal.controller.js',
'src/main/webapp/scripts/manage/modal/js/candidate.info.modal.controller.js',
'src/main/webapp/scripts/manage/js/addNewCuration.js',

//        'src/main/webapp/scripts/adverseNews/controllers/adverseNews.Controller.js',

'src/main/webapp/scripts/manage/js/addNewCuration.js',
'src/main/webapp/scripts/manage/js/staging.mip.controller.js',

'src/main/webapp/scripts/manage/services/dataCuration.api.service.js',
'src/main/webapp/scripts/manage/modal/js/relation.modal.controller.js',
'src/main/webapp/scripts/manage/modal/js/previewModal.modal.controller.js',
'src/main/webapp/vendor/jquery/js/chosen.jquery.min.js',

'src/main/webapp/vendor/jquery/js/jquery.nicescroll.min.js',
'src/main/webapp/vendor/jquery/js/jquery-ui-timepicker-addon.js',
'src/main/webapp/vendor/jquery/js/jquery-ui-sliderAccess.js',
"src/main/webapp/vendor/jquery/js/moment-timezone-with-data.min.js",
"src/main/webapp/vendor/jquery/js/dom-to-image.js",
"src/main/webapp/vendor/jquery/js/moment-timezone-utils.js",
"src/main/webapp/vendor/jquery/js/angularjs-dropdown-multiselect.js",
"src/main/webapp/scripts/uploadDocument.controller.js"
])
.pipe(uglify({ output: { ascii_only: true } })
    .pipe(babel({
        "compact": false,
        presets: ['es2015']
    }))
    .on('error', function(e){
        console.log(e);
    }))    
.pipe(concat('scripts.min.js'))
.pipe(gulp.dest('./src/main/webapp/scripts'))
.pipe(gulp.dest('dist/js'));
});

/*
================================================================================
*****************************   Js Tasks Ends    *******************************
================================================================================
*/

/*--  Watch Task  --*/
/*--  Watch Task  --*/
gulp.task('watch', function() {
    // gulp.watch('src/main/webapp/assets/scss/**/*.scss',  gulp.parallel(['vendorStyles', 'customStyles', 'commonVendorStyles', 'commonStyles', 'predictVendorStyles', 'predictStyles', 'workflowVendorStyles', 'workflowStyles', 'entityVendorStyles', 'entityStyles', 'mipVendorStyles', 'mipStyles', 'transactionIntelligenceVendorStyles', 'transactionIntelligenceStyles', 'leadGenerationVendorStyles', 'leadGenerationStyles', 'docParserVendorStyles','docParserStyles','dueVendorStyles', 'dueStyles', 'identityManagementVendorStyles', 'identityManagementStyles','uploadDocumentsVendorStyles','uploadDocumentsStyles']));
    // // gulp.watch('src/main/webapp/**/*.js',  gulp.parallel(['commonScriptsJs', 'scriptsJs', 'workflowScriptsJs', 'entityScriptsJs', 'mipScriptsJs', 'transactionIntelligenceScriptsJs', 'workflowVendorJs', 'entityVendorJs', 'entityChartJs', 'mipVendorJs', 'transactionIntelligenceVendorJs', 'transactionIntelligenceChartsJs', 'commonVendorJs', 'vendorJs', 'chartJs', 'IDMScriptsJs','datamapsScriptsJs']));
    // gulp.watch('src/main/webapp/assets/img/*',  gulp.parallel(['images']));
    // return
    const watcher = gulp.watch('src/main/webapp/assets/scss/**/*.scss');
    const watcher2 = gulp.watch('src/main/webapp/assets/img/*');
   
    watcher.on('change', gulp.parallel(['vendorStyles', 'customStyles', 'commonVendorStyles', 'commonStyles', 'predictVendorStyles', 'predictStyles', 'workflowVendorStyles', 'workflowStyles', 'entityVendorStyles', 'entityStyles', 'mipVendorStyles', 'mipStyles', 'transactionIntelligenceVendorStyles', 'transactionIntelligenceStyles', 'leadGenerationVendorStyles', 'leadGenerationStyles', 'docParserVendorStyles','docParserStyles','dueVendorStyles', 'dueStyles', 'identityManagementVendorStyles', 'identityManagementStyles','uploadDocumentsVendorStyles','uploadDocumentsStyles']));
    watcher2.on('change', gulp.parallel(['images']));
    return
});

/*
================================================================================
*************************   Deployment Tasks      ******************************
================================================================================
*/
/*--  Optimizing CSS and JavaScript  --*/
gulp.task('useref', function() {
    return gulp.src('src/main/webapp/WEB-INF/jsp/login.jsp')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'));
});

/*--  Coping Html Task  --*/
gulp.task('html', function() {
    return gulp.src('src/main/webapp/**/*')
    .pipe(gulp.dest('dist'))
});
/*--  Optimizing Images Task  --*/
gulp.task('images', function() {
    return gulp.src('src/main/webapp/assets/img/**/*.+(png|ico|jpg|jpeg|gif|svg)')
    /*--  Caching images that ran through imagemin  --*/
    .pipe(cache(imagemin({
        interlaced: true,
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe(gulp.dest('src/main/webapp/assets/images'))
});

/*--   Copying Fonts Task  --*/
gulp.task('fonts', function() {
    return gulp.src('src/main/webapp/assets/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
})

/*--  Cleaning  Cache Task  --*/
gulp.task('clean', function() {
    return del.sync('dist').then(function(cb) {
        return cache.clearAll(cb);
    });
})

/*--  Removing Dist  Task  --*/
gulp.task('clean:dist', function() {
    return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

/*-- Build Task Function  --*/
gulp.task('build', function(callback) {
    runSequence(
        'clean:dist',
        'html', 'sass', 'js', ['images', 'fonts'],
        callback
        )
});

/*-- Default Function  --*/
gulp.task('default', function() {
    /*--  place code for your default task here  --*/
    gulp.task('default', ['sass', 'js']);
});

gulp.task('commonVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui.js",
        "src/main/webapp/vendor/jquery/js/jquery.mThumbnailScroller.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.mCustomScrollbar.js",
        "src/main/webapp/vendor/jquery/js/bootstrap.min.js",
        "src/main/webapp/vendor/jquery/js/moment.min.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone-with-data.min.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone-utils.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone.min.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js",
        "src/main/webapp/vendor/jquery/js/jquery-daterangepicker.js",
        "src/main/webapp/vendor/jquery/js/bootstrap-daterangepicker.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui-timepicker-addon.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui-sliderAccess.js",
        "src/main/webapp/charts/d3.v3.min.js",
        "src/main/webapp/charts/d3.v4.min.js",
        "src/main/webapp/charts/d3v3.js",
        "src/main/webapp/charts/d3js.js",
        
        "src/main/webapp/vendor//angular/js/angular.min.js",
        "src/main/webapp/vendor//angular/js/angular-ui-router.min.js",
        "src/main/webapp/vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js",
        "src/main/webapp/vendor//angular/js/html2canvas.js",
        "src/main/webapp/vendor//angular/js/ng-file-upload.min.js",
        "src/main/webapp/vendor//angular/js/angular-flash.min.js",
        "src/main/webapp/vendor/angular/js/FileSaver.min.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('common.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/vendor'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('commonScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/constants/app.constant.js",
        "src/main/webapp/scripts/common/constants/common.constant.js",
        "src/main/webapp/scripts/discover/constants/discover.constant.js",
        "src/main/webapp/scripts/act/constants/act.constant.js",
        
        "src/main/webapp/scripts/common/services/upload.file.service.js",
        "src/main/webapp/scripts/common/services/common.service.js",
        "src/main/webapp/scripts/common/services/shared.service.js",
        "src/main/webapp/scripts/common/services/top.panel.api.service.js",
        "src/main/webapp/scripts/common/services/riskScore.service.js",
        "src/main/webapp/scripts/discover/services/discover.api.service.js",
        "src/main/webapp/scripts/act/services/act.api.service.js",
        "src/main/webapp/scripts/act/services/act.graph.service.js",

        "src/main/webapp/scripts/common/js/submenu.controller.js",
        "src/main/webapp/scripts/common/js/top.panel.controller.js",
        "src/main/webapp/scripts/common/js/advanced.search.controller.js",
        "src/main/webapp/scripts/common/js/user.events.controller.js",
        "src/main/webapp/scripts/common/modal/js/riskScore.modal.controller.js",
        "src/main/webapp/scripts/common/js/my.clipboard.controller.js",
        "src/main/webapp/scripts/common/js/notification.controller.js",
        "src/main/webapp/scripts/common/modal/js/add.media.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/widget.capture.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/create.event.controller.js",
        "src/main/webapp/scripts/common/modal/js/participants.event.controller.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('common.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/scripts'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('DocparserVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.mousewheel.min.js", 
        "src/main/webapp/vendor/jquery/js/jquery.ui.widget.js",
        "src/main/webapp/vendor/jquery/js/jquery.iframe-transport.js",
        "src/main/webapp/vendor/jquery/js/jquery.fileupload.js",
        "src/main/webapp/vendor/angular/js/lodash.min.js",
        
        ])
    .pipe(uglifyyes())
    .pipe(concat('docParser.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('docParserScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/modal/js/confirmationForDocparserPage.modal.js"

        ])
    .pipe(uglifyyes())
    .pipe(concat('docParser.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

// gulp.task('docParserChartsJs', function () {
//     gulp.src([
//         'src/main/webapp/charts/d3.v3.min.js',
//         'src/main/webapp/charts/d3.v4.min.js',
//         'src/main/webapp/charts/d3v3.js',
//         'src/main/webapp/charts/d3js.js',       
//         ])
//     .pipe(uglifyyes())
//     .pipe(concat('docparser.charts.min.js'))    
//     .pipe(gulp.dest('./src/main/webapp/charts'))
//     .pipe(gulp.dest('dist/js'));
// });

gulp.task('DocparserIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/docparser/docparser.app.js",
        "src/main/webapp/docparser/docparser.config.js",
        "src/main/webapp/docparser/docparser.run.js",
        "src/main/webapp/docparser/controllers/docparserLanding_controller.js",
        "src/main/webapp/docparser/controllers/docParserPage_controller.js",
        "src/main/webapp/docparser/modal/controller/uploadFiles.controller.js",
        "src/main/webapp/docparser/modal/controller/preview.modal.controller.js",
        "src/main/webapp/docparser/services/docParser.api.service.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('docParser.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/docparser/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('EntityVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",  

        "src/main/webapp/vendor/jquery/js/jquery.orgchart.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",
        "src/main/webapp/vendor/jquery/js/jQuery.highlight.js",

        "src/main/webapp/vendor/angular/js/angular-sanitize.min.js",
        "src/main/webapp/vendor/jquery/js/angularjs-dropdown-multiselect.js",

        "src/main/webapp/vendor/jquery/js/dom-to-image.js",
        "src/main/webapp/vendor/jquery/js/html2pdf.bundle.min.js",
        "src/main/webapp/vendor/jquery/js/primitive.min.js",
        "src/main/webapp/vendor//angular/js/ngStorage.js",
        "src/main/webapp/vendor//angular/js/angular-moment.min.js",
        "src/main/webapp/vendor//angular/js/socket.io.min.js",
        "src/main/webapp/scripts/VLA/js/loadash.js",

        "src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-navigator.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/random-color.js",
        "src/main/webapp/scripts/VLA/js/spectrum.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        "src/main/webapp/scripts/VLA/js/jquery.selectric.min.js",
        "src/main/webapp/scripts/VLA/js/custom_config.js",
        "src/main/webapp/scripts/VLA/js/app.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('entity.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entity/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('EntityChartsJsNew', function () {
    return gulp.src([
        "src/main/webapp/charts/groupedColumChart.js",
        "src/main/webapp/charts/timescaleBubble.js",
        "src/main/webapp/charts/timescaleBubble2.js",
        "src/main/webapp/charts/bubbleChart.js",
        "src/main/webapp/charts/pieChart.js",
        "src/main/webapp/charts/leaflet/leaflet.js",
        "src/main/webapp/charts/hotTopics.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/ClusterBubble.js",
        "src/main/webapp/charts/caseTimeline.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/brush.js",
        "src/main/webapp/charts/collapse.js",
        "src/main/webapp/charts/sank.min.js",
        "src/main/webapp/charts/reusablePie.js",
        "src/main/webapp/charts/networkChart.js",
        "src/main/webapp/charts/networkChartEntity.js",
        "src/main/webapp/charts/lineChart.js",
        "src/main/webapp/charts/horizontalBarChart.js",
        "src/main/webapp/charts/verticalBarChart.js",
        "src/main/webapp/charts/cytoscape-cola.js",
        "src/main/webapp/charts/datamaps.all.js",
        "src/main/webapp/charts/brushMip.js",
        "src/main/webapp/charts/timelineSimpleColumn.js",
        "src/main/webapp/charts/bubbleEntity.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/temporal.js",
        "src/main/webapp/charts/D3treeMap.js",
        "src/main/webapp/charts/D3radarChart.js",
        "src/main/webapp/charts/D3heatMap.js",
        "src/main/webapp/charts/sankRiskScore.js",
        
        ])
    .pipe(uglifyyes())
    .pipe(concat('entity.charts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entity/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('EntityScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/js/my.entity.clipboard.controller.js",
        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/dataPopUp.modal.controller.js",
        "src/main/webapp/entity/modal/controller/entity.datapopup.modal.controller.js",
        "src/main/webapp/scripts/common/js/entity.visualiser.js",
        "src/main/webapp/scripts/common/modal/js/onboarding.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadquestionaire.controller.js",
        "src/main/webapp/scripts/common/modal/js/upload.documents.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/idv.questionaire.controller.js",
        "src/main/webapp/scripts/common/modal/js/confirmDelSticky.controller.js",
        "src/main/webapp/scripts/enrich/services/enrich.api.service.js", 
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('entity.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entity/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});
// gulp.task('EntityIntializeJsNew', function () {
//     return gulp.src([
//         "src/main/webapp/scripts/app.js",
//         "src/main/webapp/entity/entity.app.js",
//         "src/main/webapp/entity/entity.config.js",
//         "src/main/webapp/entity/entity.run.js",
//         ])
//     .pipe(uglifyyes())
//     .pipe(concat('entity.intialize.new.min.js'))    
//     .pipe(gulp.dest('./src/main/webapp/entity'))
//     .pipe(gulp.dest('dist/js'));
// });

gulp.task('EntityIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/entity/entity.app.js",
        "src/main/webapp/entity/entity.config.js",
        "src/main/webapp/entity/entity.run.js",

        "src/main/webapp/scripts/common/constants/enrich.search.constant.js",
        "src/main/webapp/entity/constants/fetchers.list.constant.js",
        "src/main/webapp/entity/constants/entityCompany.constant.js",
        "src/main/webapp/scripts/enrich/constants/entity.search.constant.js",

        "src/main/webapp/entity/services/entity.graph.services.js",
        "src/main/webapp/scripts/enrich/services/entity.orgchart.service.js",
        "src/main/webapp/scripts/enrich/services/enrich.graph.service.js",
        "src/main/webapp/scripts/enrich/services/entity.api.services.js",

        "src/main/webapp/scripts/common/filters/common.filters.js",

        "src/main/webapp/entity/controllers/entity.company.new.controller.js",
        "src/main/webapp/entity/controllers/entity.person.new.controller.js",
        "src/main/webapp/entity/controllers/entity.company.shareHolderEvidence.controller.js",
        "src/main/webapp/entity/modal/controller/entityRIskModal.controller.js",
        "src/main/webapp/entity/modal/controller/overview.modal.controller.js",
        "src/main/webapp/entity/modal/controller/companySourceDetail.controller.js",
        "src/main/webapp/entity/modal/controller/leadership.additional.modal.controller.js",
        "src/main/webapp/entity/modal/controller/twitter.followers.following.controller.js",
        "src/main/webapp/entity/modal/controller/bad.buyers.modal.controller.js",
        "src/main/webapp/entity/modal/controller/bad.buyers.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/comments.modal.controller.js",
        "src/main/webapp/entity/modal/controller/crimestoppers.uk.modal.controller.js",
        "src/main/webapp/entity/modal/controller/domain.company.modal.controller.js",
        "src/main/webapp/entity/modal/controller/employment.history.modal.controller.js",
        "src/main/webapp/entity/modal/controller/exclusions.fraud.modal.controller.js",
        "src/main/webapp/entity/modal/controller/fbi.list.modal.controller.js",
        "src/main/webapp/entity/modal/controller/followers.modal.controller.js",
        "src/main/webapp/entity/modal/controller/following.modal.controller.js",
        "src/main/webapp/entity/modal/controller/general.info.modal.controller.js",
        "src/main/webapp/entity/modal/controller/general.modal.controller.js",
        "src/main/webapp/entity/modal/controller/imsasllc.modal.controller.js",
        "src/main/webapp/entity/modal/controller/industry.modal.controller.js",
        "src/main/webapp/entity/modal/controller/key.relationships.modal.controller.js",
        "src/main/webapp/entity/modal/controller/key.staff.schools.modal.controller.js",
        "src/main/webapp/entity/modal/controller/latest.release.modal.controller.js",
        "src/main/webapp/entity/modal/controller/news.articles.modal.controller.js",
        "src/main/webapp/entity/modal/controller/news.list.modal.controller.js",
        "src/main/webapp/entity/modal/controller/adverse.news.modal.controller.js",
        "src/main/webapp/entity/modal/controller/news.modal.controller.js",
        "src/main/webapp/entity/modal/controller/open.payments.modal.controller.js",
        "src/main/webapp/entity/modal/controller/organizations.modal.controller.js",
        "src/main/webapp/entity/modal/controller/person.education.modal.controller.js",
        "src/main/webapp/entity/modal/controller/person.interests.modal.controller.js",
        "src/main/webapp/entity/modal/controller/previous.location.modal.controller.js",
        "src/main/webapp/entity/modal/controller/products.modal.controller.js",
        "src/main/webapp/entity/modal/controller/related.org.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/related.organization.modal.controller.js",
        "src/main/webapp/entity/modal/controller/related.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/related.person.org.controller.js",
        "src/main/webapp/entity/modal/controller/related.person.people.modal.controller.js",
        "src/main/webapp/entity/modal/controller/replies.modal.controller.js",
        "src/main/webapp/entity/modal/controller/sanctionssearch.list.modal.controller.js",
        "src/main/webapp/entity/modal/controller/scam.report.modal.controller.js",
        "src/main/webapp/entity/modal/controller/scam.website.modal.controller.js",
        "src/main/webapp/entity/modal/controller/sex.offenders.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/social.media.articles.modal.controller.js",
        "src/main/webapp/entity/modal/controller/social.media.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/stocks.modal.controller.js",
        "src/main/webapp/entity/modal/controller/supplier.black.list.modal.controller.js",
        "src/main/webapp/entity/modal/controller/thumbnails.modal.controller.js",
        "src/main/webapp/entity/modal/controller/unlimitedcriminalchecks.modal.controller.js",
        "src/main/webapp/entity/modal/controller/reddit.comments.modal.controller.js",

        "src/main/webapp/entity/modal/controller/balance.sheet.modal.controller.js",
        "src/main/webapp/entity/modal/controller/income.sheet.modal.controller.js",
        "src/main/webapp/entity/modal/controller/cashflow.sheet.modal.controller.js",
        "src/main/webapp/entity/modal/controller/finance.modal.controller.js",

        "src/main/webapp/entity/modal/controller/question.answers.modal.controller.js",
        "src/main/webapp/entity/modal/controller/finance.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/overview.person.modal.controller.js",
        "src/main/webapp/entity/modal/controller/risk.personoffence.modal.controller.js",
        "src/main/webapp/entity/modal/controller/social.follwers.modal.controller.js",
        "src/main/webapp/entity/modal/controller/flowMaximizeModalController.js",
        "src/main/webapp/entity/modal/controller/ownership.strcture.update.js",
        "src/main/webapp/entity/modal/controller/QuestionsPerAnswersStatusController.js",
        "src/main/webapp/entity/modal/controller/add.officer.modal.controller.js",
        "src/main/webapp/entity/modal/controller/sourceChangeModalController.js",
        ])
.pipe(uglifyyes())
.pipe(concat('entity.intialize.new.min.js'))    
.pipe(gulp.dest('./src/main/webapp/entity/assets/minifiedJs'))
.pipe(gulp.dest('dist/js'));
});

gulp.task('entitySearchVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",
        "src/main/webapp/vendor/angular/js/angular_multiselect.js",
        "src/main/webapp/vendor/jquery/js/jquery.orgchart.js",
        "src/main/webapp/vendor/angular/js/angular-moment.min.js",
        "src/main/webapp/vendor/angular/js/angular-datatables.min.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('entitySearch.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('entitySearchScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",

        "src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-navigator.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/random-color.js",
        "src/main/webapp/scripts/VLA/js/spectrum.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        "src/main/webapp/scripts/VLA/js/jquery.selectric.min.js",
        "src/main/webapp/scripts/VLA/js/custom_config.js",
        "src/main/webapp/scripts/VLA/js/app.js",

        "src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js",
        "src/main/webapp/scripts/VLA/js/bluebird.min.js",
        "src/main/webapp/scripts/VLA/js/base64toblob.js",
        "src/main/webapp/scripts/VLA/js/loadash.js",

        "src/main/webapp/scripts/enrich/services/enrich.graph.service.js",
        "src/main/webapp/entity/services/entity.graph.services.js",
        "src/main/webapp/scripts/manage/services/dataCuration.api.service.js",

        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('entitySearch.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('entitySearchIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/entitySearch/entitySearch.app.js",

        "src/main/webapp/entitySearch/entitySearch.config.js",

        "src/main/webapp/entitySearch/entitySearch.run.js",

        "src/main/webapp/entitySearch/constants/entitySearch.constant.js",
        //"src/main/webapp/entitySearch/controllers/entitySearchLandingController.js",
        "src/main/webapp/entitySearch/controllers/entitySearchResultController.js",
        "src/main/webapp/entitySearch/services/entitySearch.api.service.js",
        "src/main/webapp/entitySearch/controllers/data.PopUp.controller.js",
        "src/main/webapp/entitySearch/modal/controllers/flowMaximizeModalController.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('entitySearch.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('entitySearchChartsJsNew', function () {
    return gulp.src([
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/datamaps.all.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/lineChart.js",
        "src/main/webapp/charts/reusablePie.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/hotTopics.js",
        "src/main/webapp/charts/groupedColumChart.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/bubbleEntity.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/timeformat.js",
        "src/main/webapp/charts/timeformatsupport.js",
        "src/main/webapp/charts/SimpleBarChart.js",
        "src/main/webapp/charts/verticalNegativeBarChart.js",
        "src/main/webapp/charts/ordinalLineChart.js",
        "src/main/webapp/charts/areaLineChart.js",
        "src/main/webapp/charts/networkChartEntity.js",
        "src/main/webapp/charts/verticalBarChart.js",
        "src/main/webapp/charts/NegativeGroupedColumnChart.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('entitySearch.charts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/entitySearch/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('identityManagementVendorJsNew', function () {
    return gulp.src([
        'src/main/webapp//vendor/angular/js/lodash.min.js'
        ])
    .pipe(uglifyyes())
    .pipe(concat('identityManagement.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('identityManagementIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/identityManagement/identityManagement.app.js",
        "src/main/webapp/identityManagement/identityManagement.config.js",
        "src/main/webapp/identityManagement/identityManagement.run.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('identityManagement.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('identityManagementModuleScripts', function () {
    return gulp.src([
        "src/main/webapp/identityManagement/services/identityManagement.api.service.js",
        "src/main/webapp/identityManagement/directives/custom.keys.directive.js",
        "src/main/webapp/identityManagement/directives/scroll.to.active.directive.js",
        "src/main/webapp/identityManagement/controllers/groups.controller.js",
        "src/main/webapp/identityManagement/controllers/personal.controller.js",
        "src/main/webapp/identityManagement/controllers/users.controller.js",
        "src/main/webapp/identityManagement/modal/controllers/changePasswordController.js",
        "src/main/webapp/identityManagement/modal/controllers/createGroupController.js",
        "src/main/webapp/identityManagement/modal/controllers/createUser.controller.js",
        "src/main/webapp/identityManagement/modal/controllers/deleteUser.controller.js",
        "src/main/webapp/identityManagement/modal/controllers/delete.group.controller.js",
        "src/main/webapp/identityManagement/modal/controllers/delete.group.member.controller.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('identityManagement.ModuleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/identityManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('leadGenerationVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/jquery/js/bootstrap.min.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",

        "src/main/webapp/vendor/angular/js/angular-touch.js",
        "src/main/webapp/vendor/angular/js/angular-animate.js",
        "src/main/webapp/vendor/angular/js/angular-aria.js",
        "src/main/webapp/vendor/angular/js/ui-grid.min.js",
        "src/main/webapp/vendor/angular/js/angular_multiselect.js",                
        "src/main/webapp/vendor/angular/js/angular-moment.min.js",
        "src/main/webapp/vendor/angular/js/angular-datatables.min.js",    
        ])
    .pipe(uglifyyes())
    .pipe(concat('leadGeneration.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('leadGenerationScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-navigator.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/random-color.js",
        "src/main/webapp/scripts/VLA/js/spectrum.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        "src/main/webapp/scripts/VLA/js/jquery.selectric.min.js",
        "src/main/webapp/scripts/VLA/js/custom_config.js",
        "src/main/webapp/scripts/VLA/js/app.js",

        "src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js",
        "src/main/webapp/scripts/VLA/js/bluebird.min.js",
        "src/main/webapp/scripts/VLA/js/base64toblob.js",
        "src/main/webapp/scripts/VLA/js/loadash.js",

        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js", 
        ///"src/main/webapp/scripts/enrich/services/enrich.graph.service.js"   
        ])
    .pipe(uglifyyes())
    .pipe(concat('leadGeneration.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('leadGenerationIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/leadGeneration/leadGeneration.app.js",
        "src/main/webapp/leadGeneration/leadGeneration.config.js",
        "src/main/webapp/leadGeneration/leadGeneration.run.js",
        "src/main/webapp/leadGeneration/constants/leadGenaration.constant.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('leadGeneration.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('leadGenerationChartsJsNew', function () {
    return gulp.src([
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/datamaps.all.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/lineChart.js",
        "src/main/webapp/charts/reusablePie.js",
        "src/main/webapp/charts/NegativeBarWithTooltip.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/hotTopics.js",
        "src/main/webapp/charts/groupedColumChart.js",
        "src/main/webapp/scripts/enrich/services/enrich.graph.service.js",
        "src/main/webapp/entity/services/entity.graph.services.js",

        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/bubbleEntity.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/timeformat.js",
        "src/main/webapp/charts/timeformatsupport.js",
        "src/main/webapp/charts/SimpleBarChart.js",
        "src/main/webapp/charts/verticalNegativeBarChart.js",
        "src/main/webapp/charts/ordinalLineChart.js",
        "src/main/webapp/charts/areaLineChart.js",
        "src/main/webapp/charts/networkChartEntity.js",
        "src/main/webapp/charts/sunBurstIcicle.js",
        "src/main/webapp/charts/comparisonProgressBar.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('leadGeneration.charts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('leadGenerationModuleScriptsNew', function () {
    return gulp.src([
        // "src/main/webapp/leadGeneration/services/leadGeneration.api.service.js",
        // "src/main/webapp/leadGeneration/services/leadGeneration.graph.service.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationLanding.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationCluster.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationUc2ClustersController.js",
        // "src/main/webapp/leadGeneration/controllers/leadGeneration_GeneralCardHolderDetails.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGeneration_DualCardHolderDetails.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationEarlyAdopters.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationCustomerPage.controller.js",
        // "src/main/webapp/leadGeneration/controllers/leadGenerationCommonfilter.js",
        // "src/main/webapp/leadGeneration/testing/leadGenerationCluster.test.js",
        
        "src/main/webapp/leadGeneration/services/leadGeneration.api.service.js",
        "src/main/webapp/leadGeneration/services/leadGeneration.graph.service.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationLanding.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationCluster.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationUc2ClustersController.js",
        "src/main/webapp/leadGeneration/controllers/leadGeneration_GeneralCardHolderDetails.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGeneration_DualCardHolderDetails.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationEarlyAdopters.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationCustomerPage.controller.js",
        "src/main/webapp/leadGeneration/controllers/leadGenerationCommonfilter.js",
        "src/main/webapp/leadGeneration/testing/leadGenerationCluster.test.js",
        
        ])
    .pipe(uglifyyes())
    .pipe(concat('leadGeneration.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/leadGeneration/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('sourceManagementVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.min.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui.js",
        "src/main/webapp/vendor/jquery/js/jquery.mThumbnailScroller.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.mCustomScrollbar.js",
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/jquery/js/bootstrap.min.js",

        "src/main/webapp/vendor//angular/js/angular.min.js",
        "src/main/webapp/vendor//angular/js/angular-ui-router.min.js",
        "src/main/webapp/vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js",
        "src/main/webapp/vendor//angular/js/ng-file-upload.min.js",

        "src/main/webapp/vendor/angular/js/angular-touch.js",
        "src/main/webapp/vendor/angular/js/angular-animate.js",
        "src/main/webapp/vendor/angular/js/angular-aria.js",
        "src/main/webapp/vendor/angular/js/csv.js",
        "src/main/webapp/vendor/angular/js/pdfmake.js",
        "src/main/webapp/vendor/angular/js/vfs_fonts.js",
        "src/main/webapp/vendor/angular/js/lodash.min.js",
        "src/main/webapp/vendor/angular/js/jszip.min.js",
        "src/main/webapp/vendor/angular/js/excel-builder.dist.js",
        "src/main/webapp/vendor/angular/js/ui-grid.min.js",

        "src/main/webapp/vendor//angular/js/angular-flash.min.js",
        "src/main/webapp/vendor/angular/js/angular_multiselect.js",

        "src/main/webapp/charts/d3.v3.min.js",
        "src/main/webapp/charts/d3.v4.min.js",
        "src/main/webapp/charts/d3v3.js",
        "src/main/webapp/charts/d3js.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('sourceManagement.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/sourceManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('sourceManagementScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",])
    .pipe(uglifyyes())
    .pipe(concat('sourceManagement.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/sourceManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('sourceManagementIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/sourceManagement/sourceManagement.app.js",
        "src/main/webapp/sourceManagement/sourceManagement.config.js",
        "src/main/webapp/sourceManagement/sourceManagement.run.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('sourceManagement.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/sourceManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});
gulp.task('sourceManagementModuleScriptsJsNew', function () {
    return gulp.src([
        'src/main/webapp/sourceManagement/services/sourceManagement.api.service.js',
        'src/main/webapp/sourceManagement/controllers/sourceManagement.controller.js',
        'src/main/webapp/sourceManagement/modal/js/addNewSourceModal.controller.js',
        'src/main/webapp/sourceManagement/modal/js/editSourceModalSourceManagement.controller.js',
        ])
    .pipe(uglifyyes())
    .pipe(concat('sourceManagement.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/sourceManagement/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('transactionIntelligenceVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/raphael.min.js",
        "src/main/webapp/vendor/jquery/js/raphael.icons.min.js",
        "src/main/webapp/vendor/jquery/js/wheelnav.min.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",
        "src/main/webapp/vendor/angular/js/angular_multiselect.js",
        "src/main/webapp/vendor/angular/js/angular-datatables.min.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('transactionIntelligence.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('transactionIntelligenceScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-navigator.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/random-color.js",
        "src/main/webapp/scripts/VLA/js/spectrum.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        "src/main/webapp/scripts/VLA/js/jquery.selectric.min.js",
        "src/main/webapp/scripts/VLA/js/custom_config.js",
        "src/main/webapp/scripts/VLA/js/app.js",
        "src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js",
        "src/main/webapp/scripts/VLA/js/bluebird.min.js",
        "src/main/webapp/scripts/VLA/js/base64toblob.js",
        "src/main/webapp/scripts/VLA/js/loadash.js",
        "src/main/webapp/scripts/common/js/entity.visualiser.js",

        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        "src/main/webapp/scripts/common/directives/context.menu.directive.js",
        "src/main/webapp/scripts/enrich/services/entity.api.services.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('transactionIntelligence.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('transactionIntelligenceChartsJsNew', function () {
    return gulp.src([
        "src/main/webapp/charts/topojson.min.js", 
        "src/main/webapp/charts/datamaps.all.js", 
        "src/main/webapp/charts/WorldMap/js/WorldChart.js", 
        "src/main/webapp/charts/lineChart.js", 
        "src/main/webapp/charts/reusablePie.js", 
        "src/main/webapp/charts/stackedtime.js", 
        "src/main/webapp/charts/cloud.js", 
        "src/main/webapp/charts/hotTopics.js", 
        "src/main/webapp/charts/groupedColumChart.js", 
        "src/main/webapp/scripts/enrich/services/enrich.graph.service.js",
        "src/main/webapp/entity/services/entity.graph.services.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/bubbleEntity.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/timeformat.js",
        "src/main/webapp/charts/timeformatsupport.js",
        "src/main/webapp/charts/SimpleBarChart.js",
        "src/main/webapp/charts/verticalNegativeBarChart.js",
        "src/main/webapp/charts/ordinalLineChart.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('transactionIntelligence.charts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('transactionIntelligenceIntializeJsNew', function () {
    return gulp.src([

        "src/main/webapp/scripts/app.js",
        "src/main/webapp/transactionIntelligence/transactionIntelligence.app.js",
        "src/main/webapp/transactionIntelligence/transactionIntelligence.config.js",
        "src/main/webapp/transactionIntelligence/transactionIntelligence.run.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('transactionIntelligence.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('transactionIntelligenceModuleScriptsJsNew', function () {
    return gulp.src([

        "src/main/webapp/scripts/common/constants/enrich.search.constant.js", 
        
        "src/main/webapp/transactionIntelligence/controllers/transactionIntelligence.landing.controller.js", 
        "src/main/webapp/transactionIntelligence/controllers/transactionIntelligence.controller.js",
        "src/main/webapp/transactionIntelligence/controllers/alerts.dashboard.controller.js", 
        
        "src/main/webapp/transactionIntelligence/modal/controllers/fullAnalysis.modal.controller.js",
        "src/main/webapp/transactionIntelligence/modal/controllers/createAlert.modal.controller.js", 
        "src/main/webapp/transactionIntelligence/modal/controllers/resolveAnalysis.modal.controller.js", 
        "src/main/webapp/transactionIntelligence/modal/controllers/articleVisualizer.modal.controller.js", 

        "src/main/webapp/transactionIntelligence/services/transaction.analysis.api.service.js", 
        "src/main/webapp/transactionIntelligence/services/transaction.graph.service.js", 
        "src/main/webapp/transactionIntelligence/modal/controllers/riskAlert.modal.controller.js",
        "src/main/webapp/transactionIntelligence/modal/controllers/corporateAlert.modal.controller.js",
        "src/main/webapp/transactionIntelligence/modal/controllers/counterParties.modal.controller.js",
        "src/main/webapp/transactionIntelligence/modal/controllers/viewAllTrans.modal.controller.js",
        "src/main/webapp/transactionIntelligence/modal/controllers/sideRisk.modal.controller.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('transactionIntelligence.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/transactionIntelligence/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('uploadDocumentsVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.ui.widget.js",
        "src/main/webapp/vendor/angular/js/lodash.min.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('uploadDocuments.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('uploadDocumentsScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/modal/js/confirmationForDocparserPage.modal.js"
        ])
    .pipe(uglifyyes())
    .pipe(concat('uploadDocuments.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('uploadDocumentsIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/uploadDocuments/uploadDocuments.app.js",
        "src/main/webapp/uploadDocuments/uploadDocuments.config.js",
        "src/main/webapp/uploadDocuments/uploadDocuments.run.js", 
        ])
    .pipe(uglifyyes())
    .pipe(concat('uploadDocuments.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('uploadDocumentsModuleScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/uploadDocuments/scripts/uploadDocuments.controller.js",
        "src/main/webapp/uploadDocuments/scripts/analyzeDocuments.controller.js",
        "src/main/webapp/uploadDocuments/modal/controllers/news.controller.js",
        "src/main/webapp/uploadDocuments/services/uploadDocuments.api.service.js",
        "src/main/webapp/scripts/enrich/services/enrich.api.service.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('uploadDocuments.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/uploadDocuments/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('workflowVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.scrollbar.js",
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/angular/js/lodash.min.js",

        "src/main/webapp/vendor/jquery/js/underscore.min.js",
        "src/main/webapp/vendor/jquery/js/lodash.js",
        "src/main/webapp/vendor/jquery/js/backbone.js",
        "src/main/webapp/vendor/jquery/js/joint.js",
        "src/main/webapp/vendor/jquery/js/jqx-all.js",

        "src/main/webapp/vendor/jquery/js/jquery.resize.js",
        "src/main/webapp/vendor/jquery/js/chosen.jquery.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.nicescroll.min.js",

        "src/main/webapp/vendor/jquery/js/ElementQueries.js",
        "src/main/webapp/vendor/jquery/js/ResizeSensor.js",
        "src/main/webapp/vendor/jquery/js/newlodash.js",

        "src/main/webapp/vendor/angular/js/angular-datatables.min.js",
        "src/main/webapp/vendor/angular/js/angular-resource.min.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('workflow.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('workflowScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",

        "src/main/webapp/scripts/common/js/entity.visualiser.js",

        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('workflow.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('workflowIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/workflow/workflow.app.js",
        "src/main/webapp/workflow/workflow.config.js",
        "src/main/webapp/workflow/workflow.run.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('workflow.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('workflowModuleScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/common/constants/app.constant.js",    

        "src/main/webapp/workflow/controllers/workflow.dashboard.controller.js",
        "src/main/webapp/workflow/controllers/edit.workflow.controller.js",
        "src/main/webapp/workflow/services/workflow.dashboard.api.services.js",
        "src/main/webapp/workflow/modal/controllers/case.modal.controller.js",

        "src/main/webapp/workflow/assets/codemirror/lib/codemirror.js",
        "src/main/webapp/workflow/assets/codemirror/mode/python/python.js",
        "src/main/webapp/workflow/assets/codemirror/mode/r/r.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('workflow.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/workflow/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('mipVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/angular/js/lodash.min.js",
        "src/main/webapp/vendor/jquery/js/leaflet-image.js",
        ])    
    .pipe(uglifyyes())
    .pipe(concat('mip.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('mipScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/common/js/entity.visualiser.js",

        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('mip.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('mipIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/mip/mip.app.js",
        "src/main/webapp/mip/mip.config.js",
        "src/main/webapp/mip/mip.run.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('mip.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('mipChartsJsNew', function () {
    return gulp.src([
        "src/main/webapp/charts/leaflet/leaflet.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/datamaps.all.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/networkChart.js",
        "src/main/webapp/charts/reusablePie.js",
        "src/main/webapp/charts/horizontalBarChart.js",
        "src/main/webapp/charts/verticalBarChart.js",
        "src/main/webapp/charts/brushMip.js",
        "src/main/webapp/charts/lineChart.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('mip.charts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('mipModuleJsNew', function () {
    return gulp.src([
        "src/main/webapp/mip/constants/mip.constant.js",
        
        "src/main/webapp/mip/services/mip.api.service.js",
        "src/main/webapp/mip/services/mip.service.js",

        "src/main/webapp/mip/controllers/mip.landing.controller.js",
        "src/main/webapp/mip/controllers/mip.controller.js",

        "src/main/webapp/mip/modal/controllers/mip.save.modal.controller.js",

        ])
    .pipe(uglifyyes())
    .pipe(concat('mip.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/mip/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('predictVendorJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.orgchart.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",
        "src/main/webapp/vendor/angular/js/angular-sanitize.min.js",
        "src/main/webapp/vendor/jquery/js/angularjs-dropdown-multiselect.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('predict.vendor.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/predict/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('predictScriptsJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        "src/main/webapp/scripts/common/js/entity.visualiser.js",
        
        "src/main/webapp/scripts/common/modal/js/onboarding.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadquestionaire.controller.js",
        "src/main/webapp/scripts/common/modal/js/upload.documents.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/idv.questionaire.controller.js",
        "src/main/webapp/scripts/enrich/services/enrich.api.service.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('predict.scripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/predict/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('predictIntializeJsNew', function () {
    return gulp.src([
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/predict/predict.app.js",
        "src/main/webapp/predict/predict.config.js",
        "src/main/webapp/predict/predict.run.js",
        ])
    .pipe(uglifyyes())
    .pipe(concat('predict.intialize.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/predict/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('predictModuleJsNew', function () {
    return gulp.src([
        "src/main/webapp/predict/controllers/predict.dashboard.controller.js"
        ])
    .pipe(uglifyyes())
    .pipe(concat('predict.moduleScripts.new.min.js'))    
    .pipe(gulp.dest('./src/main/webapp/predict/assets/minifiedJs'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('rootModuleJsNew', function () {
    return gulp.src([
        "src/main/webapp/vendor/jquery/js/jquery.min.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui.js",
        "src/main/webapp/vendor/jquery/js/jquery.mThumbnailScroller.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.mCustomScrollbar.js",
        "src/main/webapp/vendor/jquery/js/jquery.dataTables.min.js",
        "src/main/webapp/vendor/jquery/js/dataTables.responsive.min.js",
        "src/main/webapp/vendor/jquery/js/raphael.min.js",
        "src/main/webapp/vendor/jquery/js/raphael.icons.min.js",
        "src/main/webapp/vendor/jquery/js/wheelnav.min.js",
        "src/main/webapp/vendor/jquery/js/bootstrap.min.js",
        "src/main/webapp/vendor/jquery/js/jquery.resize.js",
        "src/main/webapp/vendor/jquery/js/moment.min.js",
        "src/main/webapp/vendor/jquery/js/underscore.min.js",
        "src/main/webapp/vendor/jquery/js/displacy-entity.min.js",
        "src/main/webapp/vendor/jquery/js/leaflet-image.js",
        "src/main/webapp/vendor/jquery/js/jquery.scrollbar.js",
        "src/main/webapp/vendor/jquery/js/chosen.jquery.min.js",
        "src/main/webapp/charts/timeformat.js",
        "src/main/webapp/charts/timeformatsupport.js",
        "src/main/webapp/charts/d3.v3.min.js",
        "src/main/webapp/charts/d3.v4.min.js",
        "src/main/webapp/charts/d3v3.js",
        "src/main/webapp/charts/d3js.js",
        "src/main/webapp/charts/groupedColumChart.js",
        "src/main/webapp/charts/timescaleBubble.js",
        "src/main/webapp/charts/timescaleBubble2.js",
        "src/main/webapp/charts/bubbleChart.js",
        "src/main/webapp/charts/pieChart.js",
        "src/main/webapp/charts/leaflet/leaflet.js",
        "src/main/webapp/charts/leaflet/TileLayer.Grayscale.js",
        "src/main/webapp/charts/leaflet/leaflet.search.js",
        "src/main/webapp/charts/hotTopics.js",
        "src/main/webapp/charts/topojson.min.js",
        "src/main/webapp/charts/cloud.js",
        "src/main/webapp/charts/ClusterBubble.js",
        "src/main/webapp/charts/caseTimeline.js",
        "src/main/webapp/charts/WorldMap/js/WorldChart.js",
        "src/main/webapp/charts/brush.js",
        "src/main/webapp/charts/collapse.js",
        "src/main/webapp/charts/sank.min.js",
        "src/main/webapp/charts/reusablePie.js",
        "src/main/webapp/charts/networkChart.js",
        "src/main/webapp/charts/networkChartEntity.js",
        "src/main/webapp/charts/lineChart.js",
        "src/main/webapp/charts/horizontalBarChart.js",
        "src/main/webapp/charts/verticalBarChart.js",
        "src/main/webapp/charts/cytoscape-cola.js",
        "src/main/webapp/charts/datamaps.all.js",
        "src/main/webapp/charts/brushMip.js",
        "src/main/webapp/charts/timelineSimpleColumn.js",
        "src/main/webapp/charts/bubbleEntity.js",
        "src/main/webapp/charts/stackedtime.js",
        "src/main/webapp/charts/temporal.js",
        "src/main/webapp/charts/sankRiskScore.js",
        "src/main/webapp/charts/D3radarChart.js",        
        "src/main/webapp/charts/ordinalLineChart.js",
        "src/main/webapp/vendor/angular/js/angular.min.js",
        "src/main/webapp/vendor/angular/js/angular-ui-router.min.js",
        "src/main/webapp/vendor/angular/js/ui-bootstrap-tpls-2.5.0.min.js",
        "src/main/webapp/vendor/angular/js/angular-gridster.js",
        "src/main/webapp/vendor/angular/js/angular-datatables.min.js",
        "src/main/webapp/vendor/angular/js/html2canvas.js",
        "src/main/webapp/vendor/angular/js/ng-file-upload.min.js",
        
        "src/main/webapp/vendor/angular/js/angular-flash.min.js",
        "src/main/webapp/vendor/angular/js/FileSaver.min.js",
        "src/main/webapp/vendor/angular/js/socket.io.min.js",
        "src/main/webapp/vendor/angular/js/angular-timeline.js",
        "src/main/webapp/vendor/angular/js/ngStorage.js",
        "src/main/webapp/vendor/angular/js/angular-moment.min.js",
        "src/main/webapp/vendor/angular/js/angular-cookies.js",
        "src/main/webapp/vendor/angular/js/rzSlider.js",
        "src/main/webapp/vendor/angular/js/angular-chosen.min.js",
        "src/main/webapp/scripts/app.js",
        "src/main/webapp/scripts/app.config.js",
        "src/main/webapp/scripts/app.run.js",
        "src/main/webapp/scripts/common/constants/app.constant.js",
        "src/main/webapp/scripts/common/constants/common.constant.js",
        "src/main/webapp/scripts/discover/constants/discover.constant.js",
        "src/main/webapp/scripts/act/constants/act.constant.js",
        "src/main/webapp/scripts/common/constants/enrich.search.constant.js",
        "src/main/webapp/scripts/transactionMonitoring/constants/transmonitoring.constant.js",
        "src/main/webapp/scripts/adverseNews/constants/adverse.news.constant.js",
        "src/main/webapp/scripts/enrich/constants/entity.search.constant.js",
        "src/main/webapp/scripts/common/services/shared.service.js",
        "src/main/webapp/scripts/common/services/common.service.js",
        "src/main/webapp/scripts/common/services/riskScore.service.js",
        "src/main/webapp/scripts/common/services/top.panel.api.service.js",
        "src/main/webapp/scripts/common/services/upload.file.service.js",
        "src/main/webapp/scripts/discover/services/discover.api.service.js",
        "src/main/webapp/scripts/discover/services/discover.graph.service.js",
        "src/main/webapp/scripts/enrich/services/enrich.api.service.js",
        "src/main/webapp/scripts/enrich/services/enrich.graph.service.js",
        "src/main/webapp/scripts/act/services/act.api.service.js",
        "src/main/webapp/scripts/act/services/act.graph.service.js",
        "src/main/webapp/scripts/manage/services/generalSettings.api.service.js",
        "src/main/webapp/scripts/transactionMonitoring/services/transmonitoring.api.service.js",
        "src/main/webapp/scripts/transactionMonitoring/services/transmonitoring.graph.service.js",
        "src/main/webapp/scripts/screeningTesting/services/screening.testing.api.service.js",
        "src/main/webapp/scripts/adverseNews/services/adverse.news.api.service.js",
        "src/main/webapp/scripts/adverseNews/services/adverse.news.service.js",
        "src/main/webapp/scripts/discover/services/investigation.landing.service.js",
        "src/main/webapp/scripts/enrich/services/entity.api.services.js",
        "src/main/webapp/transactionIntelligence/services/transaction.analysis.api.service.js",
        "src/main/webapp/scripts/discover/directives/work.diary.directive.js",
        "src/main/webapp/scripts/domain/js/domain.controller.js",    
        "src/main/webapp/scripts/process/js/process.controller.js",
        "src/main/webapp/scripts/common/js/submenu.controller.js",
        "src/main/webapp/scripts/common/js/entity.visualiser.js",
        "src/main/webapp/scripts/common/js/chatbot.controller.js",
        "src/main/webapp/scripts/common/modal/js/chatbot.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/confirmationForDocparserPage.modal.js",
        "src/main/webapp/scripts/common/modal/js/confirmDelSticky.controller.js",
        "src/main/webapp/scripts/common/js/top.panel.controller.js",
        "src/main/webapp/scripts/common/js/advanced.search.controller.js",
        "src/main/webapp/scripts/common/js/user.events.controller.js",
        "src/main/webapp/scripts/common/js/my.clipboard.controller.js",
        "src/main/webapp/scripts/common/js/my.entity.clipboard.controller.js",
        "src/main/webapp/scripts/common/js/notification.controller.js",
        "src/main/webapp/scripts/common/modal/js/entityRIskModal.controller.js",
        "src/main/webapp/scripts/common/modal/js/add.media.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/riskScore.modal.controller.js",
        
        "src/main/webapp/scripts/common/modal/js/widget.capture.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/maximize.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/dataPopUp.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/onboarding.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadquestionaire.controller.js",
        "src/main/webapp/scripts/common/modal/js/upload.documents.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/idv.questionaire.controller.js",
        "src/main/webapp/scripts/common/modal/js/create.event.controller.js",
        "src/main/webapp/scripts/common/modal/js/participants.event.controller.js",
        "src/main/webapp/scripts/common/directives/common.directives.js",
        "src/main/webapp/scripts/common/modal/js/screeningTesting.modal.controller.js",
        "src/main/webapp/scripts/common/filters/common.filters.js",
        "src/main/webapp/scripts/common/directives/context.menu.directive.js",
        "src/main/webapp/scripts/discover/js/workspace.controller.js",
        "src/main/webapp/scripts/discover/js/transaction.monitoring.controller.js",
        "src/main/webapp/scripts/discover/js/investigation.landing.controller.js",
        "src/main/webapp/scripts/discover/js/discover.dashboard.controller.js",
        "src/main/webapp/scripts/discover/js/gridster.controller.js",
        "src/main/webapp/scripts/discover/js/my.case.diary.controller.js",
        "src/main/webapp/scripts/discover/js/top.locations.controller.js",
        "src/main/webapp/scripts/discover/js/your.case.metrics.controller.js",
        "src/main/webapp/scripts/discover/js/case.in.focus.controller.js",
        "src/main/webapp/scripts/discover/js/hot.topics.controller.js",
        "src/main/webapp/scripts/discover/js/my.entities.controller.js",
        "src/main/webapp/scripts/discover/js/entities.in.focus.controller.js",
        "src/main/webapp/scripts/discover/js/your.work.diary.controller.js",
        "src/main/webapp/scripts/discover/modal/js/create.case.modal.controller.js",
        "src/main/webapp/scripts/common/modal/js/filter.case.modal.controller.js",

        "src/main/webapp/scripts/act/js/act.dashboard.controller.js",
        "src/main/webapp/scripts/act/js/case.page.landing.controller.js",
        "src/main/webapp/scripts/act/js/entity.page.controller.js",
        "src/main/webapp/scripts/act/js/acthottopics.controller.js",
        "src/main/webapp/scripts/act/js/acttoplocations.controller.js",
        "src/main/webapp/scripts/act/js/associatedentities.controller.js",
        "src/main/webapp/scripts/act/js/casetimeline.controller.js",
        "src/main/webapp/scripts/act/js/entitiesinfocus.controller.js",
        "src/main/webapp/scripts/act/js/gridster.controller.js",
        "src/main/webapp/scripts/act/js/related.cases.controller.js",
        "src/main/webapp/scripts/act/js/peer.group.controller.js",    
        "src/main/webapp/scripts/act/js/related.company.controller.js",                
        "src/main/webapp/scripts/act/js/riskratio.controller.js",
        "src/main/webapp/scripts/act/modal/js/reassign.modal.controller.js",
        "src/main/webapp/scripts/act/js/linkanalysis.controller.js",
        "src/main/webapp/scripts/act/modal/js/riskoverview.modal.controller.js",
        "src/main/webapp/scripts/act/js/audit.trail.controller.js",    
        "src/main/webapp/scripts/enrich/js/enrich.dashboard.controller.js",
        "src/main/webapp/scripts/enrich/js/dfd.controller.js",
        "src/main/webapp/scripts/enrich/js/social.live.feed.controller.js",
        "src/main/webapp/scripts/enrich/directives/enrich.directives.js",
        "src/main/webapp/scripts/manage/constants/manage.constants.js",
        "src/main/webapp/scripts/manage/js/app.manager.controller.js",
        "src/main/webapp/scripts/manage/js/data.management.controller.js",
        "src/main/webapp/scripts/manage/js/general.settings.controller.js",
        "src/main/webapp/scripts/manage/js/manage.dashboard.controller.js",
        "src/main/webapp/scripts/manage/js/system.monitoring.controller.js",
        "src/main/webapp/scripts/manage/js/user.management.controller.js",
        "src/main/webapp/scripts/manage/modal/js/addUser.Modal.Controller.js",
        "src/main/webapp/scripts/manage/js/users.controller.js",
        "src/main/webapp/scripts/manage/js/groups.controller.js",
        "src/main/webapp/scripts/manage/js/manageDb.controller.js",
        "src/main/webapp/scripts/manage/js/deployments.controller.js",
        "src/main/webapp/scripts/manage/js/jobs.controller.js",
        "src/main/webapp/scripts/manage/js/administrations.controller.js",
        "src/main/webapp/scripts/manage/js/crystalBall.controller.js",
        "src/main/webapp/scripts/manage/js/activeProcess.controller.js",
        "src/main/webapp/scripts/manage/js/suspendedProcess.controller.js",
        "src/main/webapp/scripts/manage/js/dataSource.controller.js",
        "src/main/webapp/scripts/manage/js/tasks.controller.js",
        "src/main/webapp/scripts/manage/js/process.instance.controller.js",
        "src/main/webapp/scripts/manage/js/reports.controller.js",
        "src/main/webapp/scripts/manage/js/deployment.process.controller.js",
        "src/main/webapp/scripts/manage/js/model.workspace.controller.js",
        "src/main/webapp/scripts/manage/modal/js/addSource.Modal.controller.js",
        "src/main/webapp/scripts/manage/modal/js/deploymentProcess.modal.controller.js",
        "src/main/webapp/scripts/manage/modal/js/modelWorkspace.modal.controller.js",
        "src/main/webapp/scripts/manage/modal/js/candidate.info.modal.controller.js", 
        "src/main/webapp/scripts/manage/modal/js/uploadCustomerlogoModal.controller.js",
        "src/main/webapp/scripts/manage/js/addNewCuration.js",
        "src/main/webapp/scripts/manage/js/staging.mip.controller.js",
        "src/main/webapp/scripts/manage/services/dataCuration.api.service.js",
        "src/main/webapp/scripts/manage/modal/js/relation.modal.controller.js",
        "src/main/webapp/scripts/manage/modal/js/previewModal.modal.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/transaction.monitoring.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/geographic.distribution.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/grouped.peer.analysis.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/risky.transactions.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/transaction.linkage.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/transaction.per.period.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/transactions.journey.controller.js",
        "src/main/webapp/scripts/transactionMonitoring/controllers/volume.monitor.controller.js",
        "src/main/webapp/scripts/adverseNews/controllers/adverseNews.Controller.js", 
        "src/main/webapp/scripts/screeningTesting/controllers/screeningTesting.Controller.js", 
        "src/main/webapp/scripts/discover/js/arc.selection.controller.js", 
        "src/main/webapp/scripts/VLA/js/cola.v3.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape_2.7.12.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cola.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cose-bilkent.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-cxtmenu.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-markov-cluster.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-navigator.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-ngraph.forcelayout.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-panzoom.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-qtip.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-undo-redo.js",
        "src/main/webapp/scripts/VLA/js/weaver.min.js",
        "src/main/webapp/scripts/VLA/js/cytoscape-spread.js",
        "src/main/webapp/scripts/VLA/js/random-color.js",
        "src/main/webapp/scripts/VLA/js/spectrum.js",
        "src/main/webapp/scripts/VLA/js/jquery.qtip.js",
        "src/main/webapp/scripts/VLA/js/typeahead.bundle.js",
        "src/main/webapp/scripts/VLA/js/jquery.selectric.min.js",
        "src/main/webapp/scripts/VLA/js/custom_config.js",
        "src/main/webapp/scripts/VLA/js/app.js",
        "src/main/webapp/scripts/VLA/tagcloud/js/tagcloud.js",
        "src/main/webapp/scripts/VLA/js/bluebird.min.js",
        "src/main/webapp/scripts/VLA/js/base64toblob.js",
        "src/main/webapp/scripts/VLA/js/loadash.js",        
        "src/main/webapp/scripts/createNotification.controller.js",
        "src/main/webapp/vendor/jquery/js/jquery.nicescroll.min.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui-timepicker-addon.js",
        "src/main/webapp/vendor/jquery/js/jquery-ui-sliderAccess.js",
        "src/main/webapp/vendor/jquery/js/jquery-daterangepicker.js",
        "src/main/webapp/vendor/jquery/js/bootstrap-daterangepicker.js",

        "src/main/webapp/vendor/jquery/js/moment-timezone-with-data.min.js",
        "src/main/webapp/vendor/jquery/js/dom-to-image.js",
        "src/main/webapp/vendor/jquery/js/moment-timezone-utils.js",
        "src/main/webapp/vendor/jquery/js/angularjs-dropdown-multiselect.js",    
        ])
.pipe(uglifyyes())
.pipe(concat('root.moduleScripts.new.min.js'))    
.pipe(gulp.dest('./src/main/webapp/assets/minifiedJs'))
.pipe(gulp.dest('dist/js'));
});

gulp.task('build-predict', gulp.parallel(['predictModuleJsNew','predictIntializeJsNew','predictScriptsJsNew','predictVendorJsNew']) , function(){
    return 
});

gulp.task('build-mip', gulp.parallel(['mipChartsJsNew','mipModuleJsNew','mipIntializeJsNew','mipScriptsJsNew','mipVendorJsNew']) , function(){
    return
});

gulp.task('build-workflow', gulp.parallel(['workflowVendorJsNew','workflowScriptsJsNew','workflowIntializeJsNew','workflowModuleScriptsJsNew']) , function(){
    return 
});
gulp.task('build-upload-documents', gulp.parallel(['uploadDocumentsVendorJsNew','uploadDocumentsScriptsJsNew','uploadDocumentsIntializeJsNew','uploadDocumentsModuleScriptsJsNew']) , function(){
    return 
});

gulp.task('build-transaction-intelligence', gulp.parallel(['transactionIntelligenceVendorJsNew','transactionIntelligenceScriptsJsNew','transactionIntelligenceChartsJsNew','transactionIntelligenceIntializeJsNew','transactionIntelligenceModuleScriptsJsNew']) , function(){
    return 
});

gulp.task('build-source-management', gulp.parallel(['sourceManagementVendorJsNew','sourceManagementModuleScriptsJsNew','sourceManagementScriptsJsNew','sourceManagementIntializeJsNew']) , function(){
    return 
});
gulp.task('build-lead-generation', gulp.parallel(['leadGenerationVendorJsNew','leadGenerationScriptsJsNew','leadGenerationIntializeJsNew','leadGenerationChartsJsNew','leadGenerationModuleScriptsNew']) , function(){
    return 
});

gulp.task('build-identity-management', gulp.parallel(['identityManagementVendorJsNew','identityManagementIntializeJsNew','identityManagementModuleScripts']) , function(){
    return 
});

gulp.task('build-entity-search', gulp.parallel(['entitySearchVendorJsNew','entitySearchScriptsJsNew','entitySearchIntializeJsNew','entitySearchChartsJsNew']) , function(){
    return 
});

gulp.task('build-entity', gulp.parallel(['EntityVendorJsNew','EntityChartsJsNew','EntityScriptsJsNew','EntityIntializeJsNew']) , function(){
    return 
});
gulp.task('build-doc-parser', gulp.parallel(['DocparserVendorJsNew','docParserScriptsJsNew','DocparserIntializeJsNew']) , function(){
    return 
});

gulp.task('build-element',gulp.parallel(['build-doc-parser',
    'build-entity',
    'build-entity-search',
    'build-identity-management',
    'build-lead-generation',
    'build-source-management',
    'build-transaction-intelligence',
    'build-upload-documents',
    'build-workflow',
    'build-predict','build-mip','rootModuleJsNew']),function(){
    return; 
})
