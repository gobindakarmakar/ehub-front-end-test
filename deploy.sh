#!/bin/bash


## PRINT Codes

GREEN='\033[0;32m'
RED='\033[0;35m'
NC='\033[0m' # No Color
printf "${GREEN}BST Frontend:Isracard${NC}\n"

printf "Patching Started############.\n"

printf "Updating Frontend Container.\n"
docker load -i bst-ehub-core-ui

printf "Restarting Frontend.\n"
./container.sh

printf "${GREEN}BST Frontend Deploy Completed${NC}\n"
