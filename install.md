# Minified Js using Gulp
    This is Manual setup for deploying a Minified js files on server.

## Gulp Setup

1) If you have already node and gulp installed run => npm install.
2) [nodejs](https://nodejs.org/en/) - Install NodeJs 
    Install GULP using command.
```
 npm install gulp -g
```

## Gulp Tasks

* gulp images 
```
Optimizing all images.
```

* gulp watch 
```
Watching all the changes for scss , js and images.
```

## Steps to update the server (8081)

* Go to app.constant.js file (scripts/common/constants/app.constant.js)
* Replace local path to server path i.e

```
    elementApp 
	.constant('EHUB_API', 'http://188.166.40.105:8081/ehubrest/api/')
	.constant('EHUB_FE_API', 'http://188.166.40.105:8081/element/')
	.constant('ACTIVITI_FE_PATH', 'http://188.166.40.105:8081/activiti-app/#/');
```	

* Change port number to 8081 at line number 33 in CustomAuthenticationProvider.java file.
* Comment all local paths in all index files and Uncomment all gulp files.
   i.e in 
   -entity Module
   -mip 
   -transactionIntelligence
   -workflow
   and all change main index file i.e under WEB-INF folder

* Change the content in WebConfig.java file
```
        --ehubui/src/main/java/element/bst/elementexploration/config/WebConfig.java    
        change line no. 28 
        	@PropertySource(value = "file:///${app.conf.dir}/ehub-ui.properties")
        to 
            	@PropertySource(value = "classpath:ehub-ui.properties")
```

* Change the content in ehub-ui.properties file
```        
        ehubui/src/main/resources/ehub-ui.properties,
    change url to respective ip
        element.api.url = http://188.166.40.105:8081/ehubrest/api/security/validateEmailPassword
```        

* run all gulp tasks
```
   gulp IDMScriptsJs workflowVendorJs workflowScriptsJs entityVendorJs entityScriptsJs entityChartJs mipVendorJs mipScriptsJs 		transactionIntelligenceScriptsJs transactionIntelligenceVendorJs transactionIntelligenceChartsJs 	    commonVendorJs commonScriptsJs vendorJs chartJs scriptsJs
```
   
* Now, Go to our ehubcorefrontend folder (project folder name) -> right click on it -> Go to export -> Then click on War file.

* Now, First uncheck the checkbox i.e Optimize for a specific server runtime.
* Click on browse option and set the path where you want to save the war file.
* Also save the file with element.war
* Now, Go to fileZilla and connect to 188.166.40.105 server -> navigate to apache-tomcat-8.5.9 -> webapps
* Replace newly generated war file with old file.
