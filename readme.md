# Blackswan Element

Element is Enterprise Cognitive Operating System to develop Enterprise AI driven applications centered on the concept of augmented human intelligence. Essentially Element is the foundation that can be used by enterprises across multiple industries to build robust AI applications & tools that would allow to better collect and organize the necessary information, gain competitive intelligence, improve accountability and compliance, drive new business opportunities and increase the predicting power � all while cutting costs, reducing errors and eliminating waste in a minimal time and effort

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See install.md for notes on how to deploy the project on a Server.

### Prerequisites

What things you need to install the software and how to install them

```
Eclipse Neon 3 - Eclipse IDE for Java EE Developers
MySQL Workbench 6 - latest version
Tomcat 8.5

```

### Installing
Download all of these three and install eclipse,MySQL Workbench.
Extract tomcat to specific location.
Need to copy the repositories in your local system from Bitbucket.
Import those maven in eclipse and configure the tomcat server for runing the project.


## Deployment

We can deploy project with the help of  bamboo or by manually (mentioned same in install.md).

## Built With

* [AngularJs](https://docs.angularjs.org/api) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Saas](https://sass-lang.com/) - Used to Styling CSS effectively
* [Bamboo](https://www.atlassian.com/software/bamboo) - Project Deployment


## For Deployment

Modify the "ehub-ui.properties" file with the urls of the element-backend
ex: change the IP of the existing url "http://52.211.59.166:8080/ehubrest/api/security/validateEmailPassword" with "yourIp:yourPort/ehubrest/api/security/validateEmailPassword" 
