'use strict';
angular.module('identityManagementApp')
	   .factory('IndentityManagementApiService', indentityManagementApiService);

		indentityManagementApiService.$inject = [
			'$http',
			'$q',
			'EHUB_API'
		];
		
		function indentityManagementApiService(
				$http,
				$q,
				EHUB_API
		) {
			return{
				getPersonalDetails: getPersonalDetails,
				getAssociatedUserGroups: getAssociatedUserGroups,
				updatePersonalDetails: updatePersonalDetails,
				getAllGroupsList: getAllGroupsList,
				getAllUsersList: getAllUsersList,
				createGroup: createGroup,
				createNewUser: createNewUser,
				filterUsers: filterUsers,
				getUserGroupDetails: getUserGroupDetails,
				deactivateUser: deactivateUser,
				activateUser: activateUser,
				updateUser: updateUser,
				listUsersByGroup: listUsersByGroup,
				getGroupDetails: getGroupDetails,
				updateGroup: updateGroup,
				deactivateGroup: deactivateGroup,
				activateGroup: activateGroup,
				associateUser: associateUser,
				deassociateUser: deassociateUser,
				getFullTextSearchGroup: getFullTextSearchGroup
			};
			
			/*
			 * purpose: get Personal Details
			 * created: 03 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function getPersonalDetails(params){
				var apiurl = EHUB_API + 'user/getUserProfile';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getPersonalDetailsSuccess)
						.catch(getPersonalDetailsError));
				
				/* getPersonalDetails Error function */
				function getPersonalDetailsError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getPersonalDetails Success function */
				function getPersonalDetailsSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: get Associated User Groups
			 * created: 02 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function getAssociatedUserGroups(params){
				var apiurl = EHUB_API + 'group/listUserGroups';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getAssociatedUserGroupsSuccess)
						.catch(getAssociatedUserGroupsError));
				
				/* getAssociatedUserGroups Error function */
				function getAssociatedUserGroupsError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getAssociatedUserGroups Success function */
				function getAssociatedUserGroupsSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: update Personal Details
			 * created: 02 apr 2018
			 * params: data(object), params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function updatePersonalDetails(data, params){
				var apiurl = EHUB_API + 'user/updateProfile';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(updatePersonalDetailsSuccess)
						.catch(updatePersonalDetailsError));
				
				/* updatePersonalDetails Error function */
				function updatePersonalDetailsError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* updatePersonalDetails Success function */
				function updatePersonalDetailsSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: get All Groups List
			 * created: 05 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function getAllGroupsList(params){
				var apiurl = EHUB_API + 'group/groupList';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getAllGroupsListSuccess)
						.catch(getAllGroupsListError));
				
				/* getAllGroupsList Error function */
				function getAllGroupsListError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getAllGroupsList Success function */
				function getAllGroupsListSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: create Group
			 * created: 05 apr 2018
			 * params: daat(object), params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function createGroup(data, params){
				var apiurl = EHUB_API + 'group/create';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(createGroupSuccess)
						.catch(createGroupError));
				
				/* createGroup Error function */
				function createGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* createGroup Success function */
				function createGroupSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: get user list
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function getAllUsersList(params){
				var apiurl = EHUB_API + 'user/getUserListing';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getAllUsersListSuccess)
						.catch(getAllUsersListError));
				
				/* getAllUsersList Error function */
				function getAllUsersListError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getAllUsersList Success function */
				function getAllUsersListSuccess(response){
					return(response);
				}
			}
			
			/*
			 * purpose: Create new user
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function createNewUser(params, data){
				var apiurl = EHUB_API + 'admin/register';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(createNewUserSuccess)
						.catch(createNewUserError));
				
				/* createNewUser Error function */
				function createNewUserError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* createNewUser Success function */
				function createNewUserSuccess(response){
					return(response);
				}
			}

			/*
			 * purpose: Filter Users
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function filterUsers(params) {
				var apiurl = EHUB_API + 'user/fullTextSearch';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params,
				});
				return (request
					.then(filterUsersSuccess)
					.catch(filterUsersError));

				/* filterUsers Error function */
				function filterUsersError(response) {
					if (!angular.isObject(response.data) || !(response.data.message)) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* filterUsers Success function */
				function filterUsersSuccess(response) {
					return (response);
				}
			}


			/*
			 * purpose: Get user group details
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function getUserGroupDetails(params) {
				var apiurl = EHUB_API + 'group/listUserGroups';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params,
				});
				return (request
					.then(getUserGroupDetailsSuccess)
					.catch(getUserGroupDetailsError));

				/* getUserGroupDetails Error function */
				function getUserGroupDetailsError(response) {
					if (!angular.isObject(response.data) || !(response.data.message)) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* getUserGroupDetails Success function */
				function getUserGroupDetailsSuccess(response) {
					return (response);
				}
			}
			
			/*
			 * purpose: deactivate user
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function deactivateUser(params) {
				var apiurl = EHUB_API + 'admin/deactivateUser';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
				});
				return (request
					.then(deleteUserSuccess)
					.catch(deleteUserError));

				/* deleteUser Error function */
				function deleteUserError(response) {
					if (!angular.isObject(response.data) || !(response.data.message)) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* deleteUser Success function */
				function deleteUserSuccess(response) {
					return (response);
				}
			}
			
			/*
			 * purpose: activate user
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function activateUser(params) {
				var apiurl = EHUB_API + 'admin/activateUser';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
				});
				return (request
					.then(activateUserSuccess)
					.catch(activateUserError));

				/* activateUser Error function */
				function activateUserError(response) {
					if (!angular.isObject(response.data) || !(response.data.message)) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* activateUser Success function */
				function activateUserSuccess(response) {
					return (response);
				}
			}
			
			/*
			 * purpose: update user
			 * created: 04 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */ 
			
			function updateUser(params, data) {
				var apiurl = EHUB_API + 'admin/updateUser';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
					data: data
				});
				return (request
					.then(updateUserSuccess)
					.catch(updateUserError));

				/* updateUser Error function */
				function updateUserError(response) {
					if (!angular.isObject(response.data) || !(response.data.message)) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* updateUser Success function */
				function updateUserSuccess(response) {
					return (response);
				}
			}
			/*
			 * purpose: get list Users By Group
			 * created: 06 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function listUsersByGroup(params){
				var apiurl = EHUB_API + 'group/listUsersByGroup';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(listUsersByGroupSuccess)
						.catch(listUsersByGroupError));
				
				/* listUsersByGroup Error function */
				function listUsersByGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* listUsersByGroup Success function */
				function listUsersByGroupSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: get Group Details
			 * created: 06 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function getGroupDetails(params){
				var apiurl = EHUB_API + 'group/getGroup';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getGroupDetailsSuccess)
						.catch(getGroupDetailsError));
				
				/* getGroupDetails Error function */
				function getGroupDetailsError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getGroupDetails Success function */
				function getGroupDetailsSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: update group Details
			 * created: 06 apr 2018
			 * params: data(object), params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function updateGroup(data, params){
				var apiurl = EHUB_API + 'group/update';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(updateGroupSuccess)
						.catch(updateGroupError));
				
				/* updateGroup Error function */
				function updateGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* updateGroup Success function */
				function updateGroupSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: deactivate group 
			 * created: 07 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function deactivateGroup(params){
				var apiurl = EHUB_API + 'group/deactivate';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params
				});
				return(request
						.then(deactivateGroupSuccess)
						.catch(deactivateGroupError));
				
				/* deactivateGroup Error function */
				function deactivateGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* deactivateGroup Success function */
				function deactivateGroupSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: activate group 
			 * created: 07 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function activateGroup(params){
				var apiurl = EHUB_API + 'group/activate';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params
				});
				return(request
						.then(activateGroupSuccess)
						.catch(activateGroupError));
				
				/* activateGroup Error function */
				function activateGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* activateGroup Success function */
				function activateGroupSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: associate User
			 * created: 09 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function associateUser(params){
				var apiurl = EHUB_API + 'group/addUser';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params
				});
				return(request
						.then(associateUserSuccess)
						.catch(associateUserError));
				
				/* associateUser Error function */
				function associateUserError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* associateUser Success function */
				function associateUserSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: deassociate User 
			 * created: 09 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function deassociateUser(params){
				var apiurl = EHUB_API + 'group/removeUser';
				var request = $http({
					url: apiurl,
					method: 'POST',
					params: params
				});
				return(request
						.then(deassociateUserSuccess)
						.catch(deassociateUserError));
				
				/* deassociateUser Error function */
				function deassociateUserError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* deassociateUser Success function */
				function deassociateUserSuccess(response){
					return(response);
				}
			}
			/*
			 * purpose: get Full Text Search Group
			 * created: 09 apr 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */ 
			function getFullTextSearchGroup(params){
				var apiurl = EHUB_API + 'group/groupListFullTextSearch';
				var request = $http({
					url: apiurl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getFullTextSearchGroupSuccess)
						.catch(getFullTextSearchGroupError));
				
				/* getFullTextSearchGroup Error function */
				function getFullTextSearchGroupError(response){
					if(!angular.isObject(response.data) || !(response.data.message)){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getFullTextSearchGroup Success function */
				function getFullTextSearchGroupSuccess(response){
					return(response);
				}
			}
		}