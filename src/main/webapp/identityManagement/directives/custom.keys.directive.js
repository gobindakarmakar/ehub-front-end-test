'use strict';
angular.module('identityManagementApp')
    .directive('customKeys', ["$parse", function ($parse) {
        var directive = {};
        directive.compile = function($element, attr) {
            var up, down, enter, escape;
            // key up event
            if(attr.upPressed) {
                up = $parse(attr.upPressed);
            }
            // key down event
            if(attr.downPressed) {
                down = $parse(attr.downPressed);
            }
            // key enter event
            if(attr.enterPressed) {
                enter = $parse(attr.enterPressed);
            }
            // key escape event
            if(attr.escapePressed) {
                escape = $parse(attr.escapePressed);
            }

            return function(scope, element) {
            	// scroll the users list up/down according to the key events
                element.on('keyup', function(e) {
                    if(e.keyCode === 38) {
                        scope.$apply(function() {
                            if(up) {
                                up(scope, {$event:e});
                                $("#selectUsersPopup").mCustomScrollbar("scrollTo","+=15",{scrollEasing:"linear", scrollInertia:200});
                                //$("#selectUsersPopup").mCustomScrollbar("scrollTo",$('#selectUsersPopup').find('.mCSB_container').find('li.active'));
                            }
                        });
                    } else if(e.keyCode === 40) {
                        scope.$apply(function() {
                            if(down) {
                                down(scope, {$event:e});
                                if(scope.popupModel.selectedIndex > 5){
                                	$("#selectUsersPopup").mCustomScrollbar("scrollTo","-=32", {scrollEasing:"linear", scrollInertia:200});
                                }
                                //$("#selectUsersPopup").mCustomScrollbar("scrollTo","-=15", {scrollEasing:"linear", scrollInertia:200});
                               //$("#selectUsersPopup").mCustomScrollbar("scrollTo",$('#selectUsersPopup').find('.mCSB_container').find('li.active'));
                            }
                        });
                    } else if(e.keyCode === 13) {
                        scope.$apply(function() {
                            if(enter) {
                                enter(scope, {$event:e});
                            }
                        });
                    } else if(e.keyCode === 27) {
                    scope.$apply(function() {
                        if(escape) {
                            escape(scope, {$event:e});
                        }
                    });
                }
                });

                element.on('keydown', element, function (e) {
                    if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 13 || e.keyCode === 27){
                        e.preventDefault();
                    }
                });
            };
    };
    return directive;
}]);
