/**
 * Directive that ensures the child-element with class .active is visible and scrolls if needed. Watches the value
 * of the directive and will re-apply if this value is changes.
 */
'use strict';
angular.module('identityManagementApp')
    .directive('scrollToActive', ['$timeout', function($timeout) {
        return {
            restrict: 'AC',
            scope: {
                toWatch: "=scrollToActiveModel"
            },
            compile: function () {
                return function ($scope, $element, $attrs) {
                    $scope.$watch('toWatch', function() {
                        $timeout(function() {
                            var useParent = $attrs.useParent;
                            var offsetTop = $attrs.offsetTop;
                            if(offsetTop) {
                                offsetTop = parseInt(offsetTop);
                                if(isNaN(offsetTop)) {
                                    offsetTop = 0;
                                }
                            }
                            if (!offsetTop) {
                                offsetTop = 0;
                            }

                            var selectedArr = $element.children('.active');
                            if(selectedArr && selectedArr.length > 0) {
                                var selected = angular.element(selectedArr[0]);

                                if(useParent) {
                                    $element = angular.element($element.parent());
                                }
                                var selectedTop = selected.position().top - $element.position().top + $element.scrollTop();
                                var selectedBottom = selectedTop + selected.outerHeight();
                                var elementBottom = $element.scrollTop() + $element.innerHeight();
                                var elementTop = elementBottom - $element.innerHeight();

                                if(selectedTop <= elementTop) {
                                    // scroll up
                                    $element.scrollTop(selectedTop - selected.outerHeight() - offsetTop);
                                } else if(selectedBottom > elementBottom) {
                                    // scroll down
                                    $element.scrollTop(elementTop + selected.outerHeight() - offsetTop);
                                }
                            }
                        }, 0);
                    });
                };
            }
        };
    }]);
