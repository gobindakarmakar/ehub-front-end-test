<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->


<!--  Identity Management Starts  -->
<div class="identity-management-wrapper">
    <!--  Main List Starts  -->
    <div class="main-list" id="groupsListWrapper">
        <div class="list-subheader clearfix">
            <div class="btn-group pull-right">
                <button class="btn" ng-click="showCreateGroupPopup(false)">{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"].CREATE}}&nbsp;</button>
            </div>
        </div>
        <div class="list-wrapper" auto-height>
            <div class="input-group-wrapper">
                <!-- <h4>{{jsnData["USER-MGMT"]["NAME-SEARCH"]}}</h4> -->
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-search"></i>
                    </span>
                    <input type="text" ng-model="searchGroup" class="form-control" ng-change="searchGroupByKeyWord(searchGroup)">                
                </div>
                <span class="message-text" ng-if="searchGroup.length != 0 && searchGroup.length < 3">Length of the search string should be at least 3 character.</span>
            </div>
        </div>
        <div class="list-wrapper" id="groupsList" auto-height>
            <div class="custom-spinner case-dairy-spinner" ng-show="groupsLoader">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <div class="list-wrapper" ng-show="model.groups.length > 0">
                <ul class="full-list" offset-top="100" id="listOfGroups">
                    <li ng-repeat="group in model.groups" ng-class="{'active': model.selectedGroup.userGroupId == group.userGroupId}" ng-click="selectGroup(group.userGroupId)">
                        <div>
                            <div class="title">
                                {{group.name}}
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="nothing-to-see" ng-show="model.tasks.length == 0">
                    <span>{{jsnData.IDM["TASK"]["MESSAGE"]["NO-TASKS"]}}</span>
                </div>
            </div>
            <div class="nothing-to-see" ng-if="model.groups.length == 0">
                <span>{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["NO-GROUPS"]}}</span>
            </div>
        </div>
    </div>
    <!--  Main List Starts  -->

    <!--  Main Content Wrapper Starts  -->
    <div class="main-content-wrapper" id="groupsContentContainer">

        <!--  Main Content Starts  -->
        <div class="main-content">

            <!--  Header Starts  -->
            <div class="header" ng-if="model.selectedGroup">
                <div class="pull-right">
                    <button class="btn btn-clean" ng-click="showCreateGroupPopup(true, model.selectedGroup)">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button class="btn btn-clean" ng-click="showDeleteGroupModal(model.selectedGroup)" ng-if="model.selectedGroup.active == 0">
                        <span class="glyphicon glyphicon-ok"></span>
                    </button>
                    <button class="btn btn-clean" ng-click="showDeleteGroupModal(model.selectedGroup)" ng-if="model.selectedGroup.active == 1">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </div>
                <h2>
                    <span>{{model.selectedGroup.name}}</span>
                    <span ng-if="model.selectedGroup.active == 0">({{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"].INACTIVE}})</span>
                </h2>
            </div>
            <!--  Header Ends  -->

            <!--  Content Starts  -->
            <div class="content" ng-show="model.selectedGroup" auto-height offset="70">

                <!-- Section Starts  -->
                <div class="section" ng-if="model.selectedGroup.active == 0">
                    <blockquote>
                        {{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["INACTIVE-DESCRIPTION"]}}
                    </blockquote>
                </div>
                <div class="section custom-data-table-wrapper" ng-if="model.selectedGroup.modifiedDate != null">
                    <h3 class="mar-b10">
                        {{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["SYNC-DETAILS"]}}
                    </h3>
                    <table class="table table-scroll two-col-equal">
                        <tr>
                            <td>{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["LAST-SYNC-TIME"]}}</td>
                            <td>{{model.selectedGroup.modifiedDate| date:'MM/dd/yyyy'}}</td>
                        </tr>
                    </table>
                </div>
                <!-- Section Ends  -->

                <!-- Section Starts  -->
                <div class="section">
                    <h3 class="mar-b10">
                        {{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"].MEMBERS}}
                        <span class="action">
                            <a popover-placement="bottom" uib-popover-template="myPopover.templateUrl" popover-is-open="myPopover.isOpen">{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["ADD-USER"]}}</a>
                        </span>
                    </h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group user-search-field">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-search"></i>
                                </span>
                                <input type="text" ng-model="model.userFilter" class="form-control" ng-change="searchUsers()">
                            </div>
                            <span class="message-text" ng-if="model.userFilter.length != 0 && model.userFilter.length < 3">Length of the search string should be at least 3 character.</span>
                        </div>
                        <div class="col-sm-8">
                            <div class="dropdown-subtle text-right width-100">
                                <div class="btn-group btn-group-sm mar-y15">
                                    <button type="button" class="btn btn-default dropdown-toggle" ng-click="openFilters()">{{jsnData.IDM.GENERAL.ACTION.Filters}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>           
                    <span>
                        <a ng-click="showPreviousUsers()" ng-show="model.userPage > 0">&laquo; {{jsnData.IDM["USER-MGMT"]["SHOW-PREVIOUS"]}} {{model.pageSize}}</a>
                        <span ng-show="model.userPage > 0 && model.moreUsers">|</span>
                        <a ng-click="showNextUsers()" ng-show="model.moreUsers">{{jsndata.IDM["USER-MGMT"]["SHOW-NEXT"]}} {{model.pageSize}} &raquo;</a>
                    </span>
                    <div class="custom-data-table-wrapper">
                        <table width="100%" id="groupsTable" class="table table-scroll four-col-end-action">
                            <thead class="clearfix">
                                <tr>
                                    <th width="10%">{{jsnData.IDM["USER-MGMT"].ID}}</th>
                                    <th width="30%">{{jsnData.IDM["USER-MGMT"].EMAIL}}</th>
                                    <th width="30%">{{jsnData.IDM["USER-MGMT"].NAME}}</th>
                                    <th width="30%"></th>
                                </tr>
                            </thead>
                            <tbody id="groupsTableBody" ng-show="model.users && model.users.length > 0">
                                <tr class="clearfix" ng-repeat="user in model.users">
                                    <td>{{user.userId}}</td>
                                    <td>{{user.emailAddress}}</td>
                                    <td>{{user.firstName}} {{user.lastName}}</td>
                                    <td align="center" ng-click="deleteUserFromGroup(user.userId, model.selectedGroup.userGroupId)">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="message-text text-center pad-t10" ng-if="!model.users || model.users.length == 0">
                            <span>{{jsnData.IDM["USER-MGMT"]["NO-MATCHING-USERS"]}}</span>
                        </div>
                        <div class="text-right" ng-hide="model.usersCount <= 10">
                            <ul class="case-diary-pagination" uib-pagination total-items="model.usersCount" items-per-page="10" ng-model="pageNum" ng-change="pageChanged(pageNum, model.selectedGroup.userGroupId)"
                                max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                    </div>
                </div>
                <!-- Section Ends  -->

                <!-- Custom Spinner Starts  -->
                <div class="custom-spinner case-dairy-spinner" ng-show="model.loadingUsers">
                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                </div>
                <!-- Custom Spinner Ends  -->
            </div>
            <!--  Content Ends  -->

            <div class="nothing-to-see" ng-if="model.groups.length > 0 && !model.selectedGroup">
                <span>{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["NO-SELECTION"]}}</span>
            </div>

        </div>
        <!--  Main Content Ends  -->

    </div>
    <!--  Main Contents Wrapper Ends  -->

</div>
<!--  Identity Management Ends  -->
