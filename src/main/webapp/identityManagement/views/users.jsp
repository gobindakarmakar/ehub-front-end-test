<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Identity Management Starts  -->
<div class="identity-management-wrapper">

    <!--  Main List Starts  -->
    <div class="main-list" id="usersList">
        <div class="list-subheader clearfix mar-t0">
            <div class="btn-group pull-right">
                <button class="btn" ng-click="createEditUser(false);">{{jsnData.IDM["USER-MGMT"]["CREATE-USER"]}}&nbsp;</button>
            </div>
        </div>
        <div class="list-wrapper" auto-height>
            <div class="input-group-wrapper">
                <h4>{{jsnData["USER-MGMT"]["NAME-SEARCH"]}}</h4>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-search"></i>
                    </span>
                    <input type="text" ng-model="searchUsers" class="form-control" ng-change="searchUser()">                
                </div>
                <span class="message-text mar-t5" ng-if="searchUsers.length != 0 && searchUsers.length < 3">Length of the search string should be at least 3 character.</span>
            </div>
        </div>
    </div>
    <!--  Main List Ends  -->

    <!--  Main Content Wrapper Starts  -->
    <div class="main-content-wrapper" id="usersContentContainer">
        <div class="main-content" ng-show="!model.loadingTenant">

            <!--  Header Starts  -->
            <div class="header">
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-toggle="dropdown" ng-disabled="countChecked() == 0 || countChecked() > 1">{{jsnData.IDM["USER-MGMT"].ACTIONS}}</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ng-disabled="countChecked() == 0 || countChecked() > 1">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li ng-click="createEditUser(true, currentUser);">
                                <a href="">{{jsnData.IDM["USER-MGMT"]["EDIT-USER"]}}</a>
                            </li>
                            <!--                         <li ng-click="editUserPassword()"> -->
                            <!--                             <a href="">{{jsnData.IDM["USER-MGMT"]["CHANGE-PASSWORD"]}}</a> -->
                            <!--                         </li> -->
                            <li ng-click="activateDeactivateUser(currentUser)" ng-if="userStatus == 0">
                                <a href="">{{jsnData.IDM["USER-MGMT"]["ACTIVATE-USER"]}}</a>
                            </li>
                            <li ng-click="activateDeactivateUser(currentUser)" ng-if="userStatus == 1">
                                <a href="">{{jsnData.IDM["USER-MGMT"]["DEACTIVATE-USER"]}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <h2>{{jsnData.IDM["USER-MGMT"].TITLE}}</h2>
            </div>
            <!--  Header Ends  -->

            <!--  Content Starts  -->
            <div class="content" auto-height offset="6">
                <div class="clearfix">
                    <div class="dropdown-subtle text-right width-100">
                        <div class="btn-group btn-group-sm mar-y5">
                            <button type="button" class="btn btn-default dropdown-toggle" ng-click="openFilters()">{{jsnData.IDM.GENERAL.ACTION.Filters}}
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li ng-repeat="sort in sortOptions">
                                    <a ng-click="sortUsers(sort)">{{sort.orderBy}} {{sort.sortOrder}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="message clearfix custom-data-table-wrapper">                                          
                        <table width="100%" class="table table-scroll five-col-checkbox" id="usersTable">
                            <thead class="clearfix">
                                <tr>
                                    <th></th>
                                    <th>{{jsnData.IDM["USER-MGMT"].ID}}</th>
                                    <th>{{jsnData.IDM["USER-MGMT"].EMAIL}}</th>
                                    <th>{{jsnData.IDM["USER-MGMT"].NAME}}</th>
                                </tr>
                            </thead>
                            <tbody id="usersTableBody" ng-show="userList && userList.length > 0">
                                <tr class="clearfix" ng-repeat="user in userList">
                                    <td class="control">
                                        <label class="custom-checkbox-input">
                                            <input type="checkbox" id="usrChkd" ng-click="toggleUserSelection($event, user)" ng-model="user.isselected">
                                            <span>&nbsp;</span>
                                        </label>
                                    </td>
                                    <td>{{user.userId}}</td>
                                    <td>{{user.emailAddress}}</td>
                                    <td>{{user.firstName + ' ' + user.lastName}}</td>
                                </tr>                           
                            </tbody>
                        </table>
                        <span class="message-text" ng-if="!userList || userList.length == 0">{{jsnData.IDM["USER-MGMT"]["NO-MATCHING-USERS"]}}</span> 
                        <div class="custom-spinner case-dairy-spinner" ng-show="usersLoader">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <div class="text-right" ng-show="totalResults > 10">
                            <ul class="case-diary-pagination" uib-pagination total-items="totalResults" items-per-page="10" ng-model="pageNum"
                                ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Ends  -->

        </div>
    </div>
    <!--  Main Content Wrapper Ends  -->

</div>
<!--  Identity Management Ends  -->