<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Identity Management Starts  -->
<div class="identity-management-wrapper">

    <!--  Main Contents Wrapper Starts  -->
    <div class="main-content-wrapper admin-main-content-wrapper" id="profileContentContainer">
        <div class="main-content">

            <!--  Header Starts  -->
            <div class="header clearfix">
                <div class="pull-right">
                    <button class="btn btn-clean" ng-click="showChangePasswordModal()">
                        <span>{{jsnData.IDM['PROFILE-MGMT']['CHANGE-PASSWORD']}}</span>
                    </button>
                </div>
                <div class="img-thumbnail-wrapper">
                    <img ng-src="{{restRootUrl()}}/app/rest/admin/profile-picture?{{cacheBuster()}}&token={{token}}" src="../assets/images/user-img.png"
                         class="img-thumbnail clickable" ng-click="showUploadPictureModal()">
                </div>
                <h2>{{model.profile.firstName}} {{model.profile.lastName}}</h2>
            </div>
            <!--  Header Ends  -->

            <!--  Content Starts  -->
            <div class="content" auto-height offset="6" id="personalContent">
                <div class="col-sm-12">
                    <h2>{{jsnData.IDM['PROFILE-MGMT']['DETAILS']}}</h2>
                    <h5>{{jsnData.IDM['PROFILE-MGMT']['DETAILS-DESCRIPTION']}}</h5>

                    <div class="section" id="section1">
                        <div class="custom-spinner case-dairy-spinner" ng-show="profileLoader">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <form class="custom-form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="control-label">{{jsnData.IDM['PROFILE-MGMT']['FIRST-NAME']}}</h4>
                                    <div ng-if="!model.editingFirstName" class="profile-detail" ng-click="model.editingFirstName = true; focusInput('#fName')">
                                        <span ng-if="model.profile.firstName != null && model.profile.firstName !== undefined">{{model.profile.firstName}}</span>
                                        <span ng-if="model.profile.firstName == null || model.profile.firstName == undefined || model.profile.firstName == ''" class="nothing-to-see">{{jsnData.IDM['PROFILE-MGMT']['NOTHING-SET']}}</span>
                                    </div>
                                    <div class="form-group" ng-if="model.editingFirstName">
                                        <input type="text" id="fName" class="form-control" autofocus ng-model="model.profile.firstName" ng-blur="firstNameChanged()" custom-keys
                                               enter-pressed="firstNameChanged()">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h4 class="control-label">{{jsnData.IDM['PROFILE-MGMT']['LAST-NAME']}}</h4>
                                    <div ng-if="!model.editingLastName" class="profile-detail" ng-click="model.editingLastName = true; focusInput('#lName')">
                                        <span ng-if="model.profile.lastName != null && model.profile.lastName !== undefined">{{model.profile.lastName}}</span>
                                        <span ng-if="model.profile.lastName == null || model.profile.lastName == undefined || model.profile.lastName == ''" class="nothing-to-see">{{jsnData.IDM['PROFILE-MGMT']['NOTHING-SET']}}</span>
                                    </div>
                                    <div class="form-group" ng-if="model.editingLastName">
                                        <input type="text" id="lName" class="form-control" auto-focus ng-model="model.profile.lastName" ng-blur="lastNameChanged()" custom-keys
                                               enter-pressed="lastNameChanged()">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h4 class="control-label">
                                        <span>{{jsnData.IDM['PROFILE-MGMT']['EMAIL']}}</span>
                                        <span ng-if="model.profile.emailErrorMessage" class="text-red">{{model.profile.emailErrorMessage}}</span>
                                    </h4>
                                    <div ng-if="!model.editingEmail" class="profile-detail" ng-click="model.editingEmail = true; focusInput('#email')">
                                        <span ng-if="model.profile.emailAddress != null && model.profile.emailAddress != undefined">{{model.profile.emailAddress}}</span>
                                        <span ng-if="model.profile.emailAddress == null || model.profile.emailAddress == undefined" class="nothing-to-see">{{jsnData.IDM['PROFILE-MGMT']['NOTHING-SET']}}</span>
                                        {{model.profile.emailErrorMessage}}
                                    </div>
                                    <div class="form-group" ng-if="model.editingEmail">
                                        <input type="text" id="email" class="form-control" auto-focus ng-model="model.profile.emailAddress" ng-blur="emailChanged()" custom-keys
                                               enter-pressed="emailChanged()">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="section" ng-show="groupsList && groupsList.length > 0">
                        <div class="custom-spinner case-dairy-spinner" ng-show="groupsLoader">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <h2 class="mar-b0">{{jsnData.IDM['PROFILE-MGMT']['GROUPS']}}</h2>
                        <ul class="simple-list" id="section">
                            <li ng-repeat="group in groupsList">
                                <span class="glyphicon glyphicon-folder-close mar-r5"></span>
                                <span>{{group.name}}</span>
                            </li>
                        </ul>
                    </div>
                </div>                    
            </div>
            <!--  Content Ends  -->

        </div>
    </div>
    <!--  Main Contents Wrapper Ends  -->

</div>
<!--  Identity Management Ends  -->