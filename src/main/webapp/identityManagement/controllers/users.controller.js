'use strict';
	angular.module('identityManagementApp')
		.controller('UsersController', usersController);

	usersController.$inject = [
		'$rootScope',
		'$scope',
		'IndentityManagementApiService',
		'$uibModal',
		'$timeout',
		'Flash',
		'HostPathService'
	];

	function usersController(
		$rootScope,
		$scope,
		IndentityManagementApiService,
		$uibModal,
		$timeout,
		Flash,
		HostPathService
	) {

		var params = {
			token: $rootScope.ehubObject.token
		};
		var openCreateEditUserModal;
		var openFiltersModal;
		var activateDeactivateUserModal;
		$scope.filters = {};
		/*
		 * @purpose: Get users list
		 * @created: 3rd April 2018
		 * @return: null
		 * @author: zameer
		 */
		$scope.fetchUserList = function(params) {
			$scope.usersLoader = true;
			IndentityManagementApiService.getAllUsersList(params).then(function(response) {
				$scope.usersLoader = false;
				$scope.userList = response.data.result;
				$scope.totalResults = response.data.paginationInformation.totalResults;
			}, function() {
				$scope.usersLoader = false;
			});
		};
		$scope.fetchUserList(params);
		
		/*
	     * @purpose: paginating the users list
	     * @created: 9th April 2018
	     * @params: page(number)
	     * @return: no
	     * @author: zameer
	    */ 
	    $scope.pageChanged = function (page) {
	    	//pass search params if search keyword length more than or equal 3
	    	if($scope.searchUsers && $scope.searchUsers.length >= 3){
	    		//check for order by and status(active/inactive)
	    		if($scope.filters.orderBy || $scope.filters.status){
		    		var paramss = {
						token: $rootScope.ehubObject.token,
						pageNumber : page,
						recordsPerPage : 10,
						status: $scope.filters.status,
						orderBy: $scope.filters.orderBy,
						keyword: $scope.searchUsers,
						orderIn: $scope.filters.orderIn
			    	};
	    		}else{
	    			var paramss = {
						token: $rootScope.ehubObject.token,
						keyword: $scope.searchUsers,
						pageNumber : page,
						recordsPerPage : 10 
					};
	    		}
	    		$scope.usersLoader = true;
				IndentityManagementApiService.filterUsers(paramss).then(function(response) {
					$scope.usersLoader = false;
					$scope.userList = response.data.result;
				}, function() {
					$scope.usersLoader = false;
				});
	    	}else{
	    		//check for order by and status(active/inactive)
	    		if($scope.filters.orderBy || $scope.filters.status){
		    		var params = {
						token: $rootScope.ehubObject.token,
						pageNumber : page,
						recordsPerPage : 10,
						status: $scope.filters.status,
						orderBy: $scope.filters.orderBy,
						orderIn: $scope.filters.orderIn
			    	};
		    	}else{
		    		// params without filters
		    		var params = {
		    			token: $rootScope.ehubObject.token,
		    			pageNumber : page,
		    	        recordsPerPage : 10      
		    		};
		    	}
		    	$scope.fetchUserList(params);
	    	}
	    };

		/*
		 * @purpose: Search User
		 * @created: 3rd April 2018
		 * @return: null
		 * @author: zameer
		 */

		$scope.searchUser = function() {
			var paramss = {
				token: $rootScope.ehubObject.token,
				keyword: $scope.searchUsers
			};
			//if search keyword length more than or equal 3
			if($scope.searchUsers.length >= 3){
				$scope.usersLoader = true;
				IndentityManagementApiService.filterUsers(paramss).then(function(response) {
					$scope.usersLoader = false;
					$scope.userList = response.data.result;
					$scope.pageNum = 1;
					$scope.totalResults = response.data.paginationInformation.totalResults;
				}, function() {
					$scope.usersLoader = false;
				});
			}
			//if there is no search keyword
        	if($scope.searchUsers.length == 0){
        		$scope.fetchUserList(params);
        		$scope.pageNum = 1;
        		$scope.filters = {};
        	}
		};
		
		 /*
		 * @purpose: createEditUser(create/Edit) modal
		 * @created: 3rd dec 2018
		 * @return: isEdit, userDetails
		 * @author: swathi
		 */
		$scope.createEditUser = function(isEdit, userDetails) {
			openCreateEditUserModal = $uibModal.open({
				templateUrl: 'modal/views/createUser.jsp',
				scope: $scope,
				backdrop: 'static',
				controller: 'CreateUserController',
				windowClass: 'custom-modal createcase-modal',
				resolve: {
					isEdit: function() {
						return isEdit;
					},
					userDetails: function() {
						return userDetails;
					}
				}
			});

			openCreateEditUserModal.result.then(function(response) {
				//create user modal
				if (response == 'success') {
					HostPathService.FlashSuccessMessage('CREATE USER', 'Successfully created user');
					$scope.fetchUserList(params);
				}
				//update user modal
				if (response == 'updated') {
					HostPathService.FlashSuccessMessage('UPDATE USER', 'Successfully updated user');
					$scope.fetchUserList(params);
				}
			}, function() {});
		};
		
		/*
		 * @purpose: apply filters to users
		 * @created: 12th April 2018
		 * @author: zameer
		 */
		$scope.openFilters = function(){
			openFiltersModal = $uibModal.open({
				templateUrl: 'modal/views/filterModal.jsp',
				scope: $scope,
				backdrop: 'static',
				windowClass: 'custom-modal createcase-modal'
			});
			openFiltersModal.result.then(function(response){
				if (response == 'success') {
					HostPathService.FlashSuccessMessage('APPLY FILTERS', 'Successfully applied filters');
					//$scope.fetchUserList(params);
				}
			},function(){
				
			});
		};
		
		/*
		 * @purpose: apply filters to users
		 * @created: 3rd dec 2018
		 * @author: swathi
		 */
		$scope.applyFilters = function(){
			//pass search params if search keyword length more than or equal 3
			if($scope.searchUsers && $scope.searchUsers.length >=3){
				var filterparams = {
					token: $rootScope.ehubObject.token,
					status: $scope.filters.status,
					orderBy: $scope.filters.orderBy,
					orderIn: $scope.filters.orderIn,
					keyword: $scope.searchUsers
				};
				IndentityManagementApiService.filterUsers(filterparams).then(function(response) {
					$scope.usersLoader = false;
					$scope.userList = response.data.result;
					openFiltersModal.close('success');// close filter modal after applying filters
					$scope.pageNum = 1;
				}, function() {
					$scope.usersLoader = false;
				});
			}else{
				var filterparams = {
					token: $rootScope.ehubObject.token,
					status: $scope.filters.status,
					orderBy: $scope.filters.orderBy,
					orderIn: $scope.filters.orderIn
				};
				$scope.fetchUserList(filterparams);
				openFiltersModal.close('success');// close filter modal after applying filters
				$scope.pageNum = 1;
			}
		};

		
		 /*
		 * @purpose: activate / Deactivate user modal
		 * @created: 3rd dec 2018
		 * @return: selectedUser
		 * @author: swathi
		 */
		$scope.activateDeactivateUser = function(selectedUser){
			activateDeactivateUserModal = $uibModal.open({
				templateUrl: 'modal/views/deleteUser.jsp',
				scope: $scope,
				backdrop: 'static',
				controller: 'DeleteUserController',
				windowClass: 'custom-modal createcase-modal',
				resolve: {
					selectedUser: function(){
						return selectedUser;
					}
				}
			});
			
			activateDeactivateUserModal.result.then(function(response){
				//activate user
				if (response == 'ACTIVATE') {
					HostPathService.FlashSuccessMessage('ACTIVATE USER', 'Successfully activated user');
					$scope.fetchUserList(params);
				}
				//deactivate user
				if (response == 'DEACTIVATE') {
					HostPathService.FlashSuccessMessage('DEACTIVATE USER', 'Successfully deactivated user');
					$scope.fetchUserList(params);
				}
			}, function(){});
		};
		
		/*
		 * @purpose: Enable/disable actions button when selecting a user
		 * @created: 4th April 2018
		 * @return: count
		 * @author: zameer
		 */
		$scope.countChecked = function() {
			var count = 0;
			/* if checkboxes checked count 0 or more than 1, disable 'select an action' button.
			   Enable 'select an action' button only if one checkbox checked */
			angular.forEach($scope.userList, function(value) {
				if (value.isselected) {
					count++;
				}
			});
			return count;
		};
		
		/*
		 * @purpose: toggle user selection
		 * @created: 3rd dec 2018
		 * @author: swathi
		 */
		$scope.toggleUserSelection = function() {
			/* Check for checkbox is selected or not
			   if it is already selected uncheck checkbox */
			angular.forEach($scope.userList, function(value) {
				if (value.isselected == true) {
					$scope.id = value.userId;
					$scope.userStatus = value.status;
					$scope.currentUser = value;
				}
			});
		};
		

		/*
		 * @purpose: deactivate user
		 * @created: 4th April 2018
		 * @return: success/error
		 * @author: zameer
		 */
		$scope.removeUserActivation = function() {
			var paramss = {
				token: $rootScope.ehubObject.token,
				userId: $scope.id
			};
			IndentityManagementApiService.deactivateUser(paramss).then(function() {
				HostPathService.FlashSuccessMessage('SUCCESSFUL USER DEACTIVATE', 'Successfully deactivated user');
				$scope.fetchUserList(params);
			}, function() {

			});
		};

		/*
		 * @purpose: activate user
		 * @created: 4th April 2018
		 * @return: success/error
		 * @author: zameer
		 */
		$scope.addUserActivation = function() {
			var paramss = {
				token: $rootScope.ehubObject.token,
				userId: $scope.id
			};
			IndentityManagementApiService.activateUser(paramss).then(function() {
				HostPathService.FlashSuccessMessage('SUCCESSFUL USER ACTIVATE', 'Successfully activated user');
				$scope.fetchUserList(params);
			}, function() {

			});
		};
		
		 /*
		 * @purpose: close modal
		 * @created: 3rd dec 2018
		 * @author: swathi
		 */
		$scope.closeModal = function(){
			$scope.filters = {};
			openFiltersModal.close('close');
		};
		
		/* 
         * purpose: Setting height of users tab
		 * created: 10 apr 2018
		 * author: zameer
         */
//        var topPanelHeight = 60;
//    	var subMenuHeight = 55;
//    	var topMargin = 15;
//    	var bottomMargin = 15;
//    	
//        var startTableInterval = setInterval(function(){
//        	setTableHeight();
//        },100);
//        
//
//    	function setTableHeight(){
//        	if($("#usersTable").length > 0 && $("#usersTableBody").length > 0){
//        		clearInterval(startTableInterval);
//        		
//        		var totalAvailable = jQuery(window).height() - (topPanelHeight + subMenuHeight + topMargin + bottomMargin);
//            	jQuery('#usersTable').height(totalAvailable - 170);
//                jQuery('#usersTableBody').height(totalAvailable - 185);
//        		$("#usersTableBody").mCustomScrollbar({
//     	        	axis : "y",
//     				theme : "minimal"
// 	        	});
//        	}
//        };
//        
//        jQuery(window).resize(resizewindow);
//        function resizewindow(){
//        	if($("#usersContentContainer").length > 0 && $("#usersList").length > 0){
//        		clearInterval(startResizeInterval);
//        		
//        		var totalAvailable = jQuery(window).height() - (topPanelHeight + subMenuHeight + topMargin + bottomMargin);
//                jQuery('#usersContentContainer').height(totalAvailable);
//                jQuery('#usersList').height(totalAvailable);
//                jQuery('#usersTableBody').height(totalAvailable - 185);
//                jQuery('#usersTable').height(totalAvailable - 170);
//        	}
//        }
//        
//        var startResizeInterval = setInterval(function(){
//        	resizewindow();
//        },100); 
//        
//        
//        function setCustomScoller(){
//        	if($("#usersTableBody").length > 0){
//        		$("#usersTableBody").mCustomScrollbar({
//    	        	axis : "y",
//    				theme : "minimal"
//    				
//    	        });	
//        	}
//        }
//        var startInterval = setInterval(function(){
//        	setCustomScoller();
//        },100);
        /* setting height ends */


	}