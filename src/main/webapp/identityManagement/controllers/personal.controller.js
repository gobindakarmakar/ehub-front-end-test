'use strict';
angular.module('identityManagementApp')
	   .controller('PersonalController', personalController);

		personalController.$inject = [
			'$scope',
			'$rootScope',
			'IndentityManagementApiService',
			'EHUB_API',
			'$uibModal',
			'$timeout'
		];
		
		function personalController(
			$scope,
			$rootScope,
			IndentityManagementApiService,
			EHUB_API,
			$uibModal,
			$timeout
		) {
		$scope.model = {};
		var params = {
			token: $rootScope.ehubObject.token
		};
		/*
		 * purpose: get list of user groups
		 * created: 03 apr 2018
		 * author: swathi
		 */ 
        $scope.loadGroupsList = function() {
        	$scope.groupsLoader = true;
        	params = {
				token: $rootScope.ehubObject.token,
				userId: $rootScope.ehubObject.userId
			};
        	IndentityManagementApiService.getAssociatedUserGroups(params).then(function (response) {
        		$scope.totalCount = response.data.paginationInformation.totalResults;
        		if($scope.totalCount <= 10){
        			$scope.groupsLoader = false;
	                $scope.groupsList = response.data.result;
	                if($scope.groupsList.length > 0){
                    	$('#section').mCustomScrollbar({
                    		axis : "y",
            				theme : "minimal"
                    	});
                    }
	                $scope.loadProfile();
        		}
        		else{
        			/* api will return at a time 10 results. 
       			 	if results count more than 10, call api with page number 1 and results count as recordsPerPage to get all the results.*/
	                var newParams = {
	            		token: $rootScope.ehubObject.token,
	    				userId: $rootScope.ehubObject.userId,
	    				recordsPerPage: $scope.totalCount,
	    				pageNumber: 1
	                };
	                IndentityManagementApiService.getAssociatedUserGroups(newParams).then(function (response) {
	            		$scope.groupsLoader = false;
	                    $scope.groupsList = response.data.result;
	                    if($scope.groupsList.length > 0){
	                    	$('#section').mCustomScrollbar({
	                    		axis : "y",
	            				theme : "minimal"
	                    	});
	                    }
	    				$scope.loadProfile();
	                },function(){
	                	$scope.groupsLoader = false;
	                });
        		}
            }, function(){
            	$scope.groupsLoader = false;
            });
        };
        $scope.loadGroupsList();
		/*
		 * purpose: get personal Details
		 * created: 02 apr 2018
		 * author: swathi
		 */ 
        $scope.loadProfile = function() {
        	$scope.profileLoader = true;
        	IndentityManagementApiService.getPersonalDetails(params).then(function (profileData) {
        		$scope.profileLoader = false;
                $scope.model.originalEmail = profileData.data.emailAddress; // Storing it extra, so we're able to reset
                $scope.model.profile = profileData.data;
            }, function(){
            	$scope.profileLoader = false;
            });
        };
        /*
		 * purpose: update email
		 * created: 03 Dec 2018
		 * author: swathi
		 */ 
        $scope.emailChanged = function() {
            $scope.model.profile.emailErrorMessage = undefined;
            if ($scope.model.profile.emailAddress !== null && $scope.model.profile.emailAddress !== undefined && $scope.model.profile.emailAddress !== '') {
            	$scope.profileLoader = true;
                IndentityManagementApiService.updatePersonalDetails($scope.model.profile, params).then(function() {
                	$scope.profileLoader = false;
                	$scope.model.editingEmail = false;
                    $scope.loadProfile(); // reload values from server
                }, function (data, status) {
                	$scope.profileLoader = false;
                    if (status === 409) {
                        $scope.model.profile.emailErrorMessage = data.message;
                    }
                });
            } else {
                // Reset if invalid value
                $scope.model.profile.emailAddress = $scope.model.originalEmail;
            }
        };
        /*
		 * purpose: update firstName
		 * created: 03 Dec 2018
		 * author: swathi
		 */ 
        $scope.firstNameChanged = function() {
        	$scope.profileLoader = true;
            IndentityManagementApiService.updatePersonalDetails($scope.model.profile, params).then(function() {
                $scope.model.editingFirstName = false;
                $scope.profileLoader = false;
            }, function(){
        		 $scope.model.editingFirstName = false;
        		 $scope.profileLoader = false;
            });
        };
        
        /*
		 * purpose: update lastName
		 * created: 03 Dec 2018
		 * author: swathi
		 */ 
        $scope.lastNameChanged = function() {
        	$scope.profileLoader = true;
            IndentityManagementApiService.updatePersonalDetails($scope.model.profile, params).then(function() {
	            $scope.model.editingLastName = false;
	            $scope.profileLoader = false;
            }, function(){
       		 	$scope.model.editingLastName = false;
       		 	$scope.profileLoader = false;
           });
        };
        
        /*
		 * purpose: update company
		 * created: 03 Dec 2018
		 * author: swathi
		 */ 
        $scope.companyChanged = function() {
        	$scope.profileLoader = true;
            IndentityManagementApiService.updatePersonalDetails($scope.model.profile, params).then(function() {
                $scope.model.editingCompany = false;
                $scope.profileLoader = false;
            }, function(){
       		 	$scope.model.editingCompany = false;
       		 	$scope.profileLoader = false;
           });
        };
        /*
		 * purpose: open change password modal
		 * created: 04 apr 2018
		 * author: swathi
		 */ 
        $scope.showChangePasswordModal = function(){
        	var openModalInstance = $uibModal.open({
        		templateUrl: 'modal/views/changePassword.jsp',
             	backdrop: 'static',
             	controller: 'ChangePasswordController',
			 	windowClass: 'custom-modal createcase-modal',
			 	resolve: {
			 		profile: function(){
			 			return $scope.model.profile;
			 		}
			 	}
        	});
        	openModalInstance.result.then(function(){
        		
        	}, function(){
        		
        	});
        };
        
        /*
		 * purpose: Focus the input field when user enters into it
		 * created: 03 Dec 2018
		 * author: swathi
		 */ 
        $scope.focusInput = function(id){
        	$timeout(function(){
        		$(id).focus();
        	},0); 
        };
        /* 
         * purpose: Setting height of personal tab
		 * created: 10 apr 2018
		 * author: zameer
         */
        
//        var topPanelHeight = 60;
//    	var subMenuHeight = 55;
//    	var topMargin = 15;
//    	var bottomMargin = 15;
//    	
//        var startTableInterval = setInterval(function(){
//        	setGroupsDivHeight();
//        },100);
//        
//
//    	function setGroupsDivHeight(){
//        	if($("#section").length > 0){
//        		clearInterval(startTableInterval);
//        		
//        		var totalAvailable = jQuery(window).height() - (topPanelHeight + subMenuHeight + topMargin + bottomMargin);
//        		jQuery('#section').height(totalAvailable - 480);
//        		$("#section").mCustomScrollbar({
//     	        	axis : "y",
//     				theme : "minimal"
// 	        	});
//        	}
//        };
//        
//        jQuery(window).resize(resizewindow);
//        function resizewindow(){
//        	if($("#profileContentContainer").length > 0 && $(".simple-list").length > 0){
//        		clearInterval(startResizeInterval);
//        		
//        		var totalAvailable = jQuery(window).height() - (topPanelHeight + subMenuHeight + topMargin + bottomMargin);
//                jQuery('#profileContentContainer').height(totalAvailable);
//                jQuery('#section').height(totalAvailable - 480);
//                
//        	}
//        }
//        
//        var startResizeInterval = setInterval(function(){
//        	resizewindow();
//        },100); 
//        
//        function setCustomScoller(){
//        	if($("#section").length > 0){
//        		$("#section").mCustomScrollbar({
//    	        	axis : "y",
//    				theme : "minimal"
//    	        });
//        	}
//        }
//        var startInterval = setInterval(function(){
//        	setCustomScoller();
//        },100);
        /* setting height ends */
	}