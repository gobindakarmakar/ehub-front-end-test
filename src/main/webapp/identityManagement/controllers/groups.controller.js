'use strict';
angular.module('identityManagementApp')
	   .controller('GroupsController', groupsController);

		groupsController.$inject = [
			'$scope',
			'$rootScope',
			'IndentityManagementApiService',
			'EHUB_API',
			'$uibModal',
			'Flash',
			'HostPathService'
		];
		
		function groupsController(
			$scope,
			$rootScope,
			IndentityManagementApiService,
			EHUB_API,
			$uibModal,
			Flash,
			HostPathService
		) {
	
		/*Initializing scope variables*/ 
		$scope.model = {};
		$scope.filters = {};
		$scope.popupModel = {};
        $scope.popupModel.selectedUser = undefined;
		$scope.popupModel.selectedIndex = -1;
		var params = {
			token: $rootScope.ehubObject.token
		};
		var openFiltersModal;
		
		/*
		 * purpose: get all groups
		 * created: 05 apr 2018
		 * author: swathi
		 */ 
        $scope.loadAllGroups = function() {
        	$scope.groupsLoader = true;
        	IndentityManagementApiService.getAllGroupsList(params).then(function (response) {
        		$scope.totalCount = response.data.paginationInformation.totalResults;
        		if($scope.totalCount <= 10){
        			$scope.groupsLoader = false;
            		$scope.model.groups = response.data.result;
//            		$("#listOfGroups").mCustomScrollbar({
//        	        	axis : "y",
//        				theme : "minimal"
//        	        });
        		}else{
        			/* api will return at a time 10 results. 
        			 if groups count more than 10, call api with page number 1 and groups count as recordsPerPage to get all the results.*/
	        		var newParams = {
        				token: $rootScope.ehubObject.token,
        				pageNumber: 1,
        				recordsPerPage: $scope.totalCount
	        		};
	        		IndentityManagementApiService.getAllGroupsList(newParams).then(function (response) {
	            		$scope.groupsLoader = false;
	            		$scope.model.groups = response.data.result;
	            		if($scope.model.groups.length > 0){
	                		$("#listOfGroups").mCustomScrollbar({
	            	        	axis : "y",
	            				theme : "minimal"
	            	        });
	                	}
	        		}, function(){
	        			$scope.groupsLoader = false;
	        		});
        		}	
            }, function(){
            	$scope.groupsLoader = false;
            });
        };
        $scope.loadAllGroups();
        /*
		 * purpose: get group details by selecting group
		 * created: 06 apr 2018
		 * author: swathi
		 */ 
        $scope.selectGroup = function(id) {
        	$scope.myPopover.close();
        	params = {
    			token: $rootScope.ehubObject.token,
    			groupId: id
        	};
        	$scope.model.loadingGroup = true;
        	IndentityManagementApiService.getGroupDetails(params).then(function (response) {
    			$scope.model.loadingGroup = false;
    			$scope.model.selectedGroup = response.data;
    			fetchUserPage(params);
            }, function(){
            	$scope.model.loadingGroup = false;
            });
        };
        
        /*
	     * @purpose: paginating the users in a group
	     * @created: 9th April 2018
	     * @params: page(number)
	     * @return: no
	     * @author: zameer
	    */ 
	    $scope.pageChanged = function (page, id) {
	    	//pass search params if search keyword length more than or equal 3
	    	if($scope.model.userFilter && $scope.model.userFilter.length >= 3){
	    		//check for order by and status(active/inactive)
	    		if($scope.filters.orderBy || $scope.filters.status){
		    		var paramss = {
		    			token: $rootScope.ehubObject.token,
		    			pageNumber : page,
		    	        recordsPerPage : 10,
		    	        groupId: id,
		    	        status: $scope.filters.status,
						orderBy: $scope.filters.orderBy,
						keyword: $scope.model.userFilter,
						orderIn: $scope.filters.orderIn
			    	};
	    		}else{
	    			var paramss = {
	    				token: $rootScope.ehubObject.token,
	    				keyword: $scope.model.userFilter,
	    				pageNumber : page,
	    				groupId: id,
	    				recordsPerPage : 10 
	    			};
	    		}
	    		$scope.model.loadingUsers = true;
				IndentityManagementApiService.filterUsers(paramss).then(function(response) {
					$scope.model.loadingUsers = false;
					$scope.model.users = response.data.result;
				}, function() {
					$scope.model.loadingUsers = false;
				});
	    	}else{
	    		//check for order by and status(active/inactive)
	    		if($scope.filters.orderBy || $scope.filters.status){
		    		var params = {
						token: $rootScope.ehubObject.token,
						groupId: id,
						pageNumber : page,
						recordsPerPage : 10,
						status: $scope.filters.status,
						orderBy: $scope.filters.orderBy,
						orderIn: $scope.filters.orderIn
			    	};
		    	}else{
		    		var params = {
		    			token: $rootScope.ehubObject.token,
		    			groupId: id,
		    			pageNumber : page,
		    	        recordsPerPage : 10      
		    		};
		    	}
	    		//pass search params if search keyword length more than or equal 3
		    	if($scope.model.userFilter && $scope.model.userFilter.length >= 3){
		    		params.keyword =  $scope.model.userFilter;
		    	}
		    	fetchUserPage(params);
		    }
    	};
	    
	    /*
		 * @purpose: apply filters to users
		 * @created: 12th April 2018
		 * @return: success/error
		 * @author: zameer
		 */
		/* open filters modal starts */
		$scope.openFilters = function(){
			openFiltersModal = $uibModal.open({
				templateUrl: 'modal/views/filterModal.jsp',
				scope: $scope,
				backdrop: 'static',
				windowClass: 'custom-modal createcase-modal'
			});
			openFiltersModal.result.then(function(response){
				if (response == 'success') {
					HostPathService.FlashSuccessMessage('APPLY FILTERS', 'Successfully applied filters');
				}
			},function(){
				
			});
		};
		/* open filters modal ends */
		
		/* handling filter data starts  */
		$scope.applyFilters = function(){
			//pass search params if search keyword length more than or equal 3
			if($scope.model.userFilter && $scope.model.userFilter.length >= 3){
				var filterparams = {
					token: $rootScope.ehubObject.token,
					status: $scope.filters.status,
					orderBy: $scope.filters.orderBy,
					orderIn: $scope.filters.orderIn,
					keyword: $scope.model.userFilter,
					groupId: $scope.model.selectedGroup.userGroupId
				};
				IndentityManagementApiService.filterUsers(filterparams).then(function(response) {
					$scope.model.loadingUsers = false;
					$scope.model.users = response.data.result;
					openFiltersModal.close('success');
					$scope.pageNum = 1;
				}, function() {
					$scope.model.loadingUsers = false;
				});
			}else{
				var filterparams = {
					token: $rootScope.ehubObject.token,
					status: $scope.filters.status,
					orderBy: $scope.filters.orderBy,
					orderIn: $scope.filters.orderIn,
					groupId: $scope.model.selectedGroup.userGroupId
				};
				IndentityManagementApiService.getAllUsersList(filterparams)
				.then(function(response) {
					$scope.model.loadingUsers = false;
					$scope.model.users = response.data.result;
					$scope.model.usersCount = response.data.paginationInformation.totalResults;
					$scope.pageNum = 1;
				}, function() {
					$scope.model.loadingUsers = false;
				});
				openFiltersModal.close('success');
			}
		};
		/* handling filter data ends  */
	    
		
		/* closing modal starts */
		$scope.closeModal = function(){
			$scope.filters = {};
			openFiltersModal.close('close');
		};
		/* closing modal ends */
	    
		
        /*
		 * purpose: show users list by selecting group
		 * created: 06 apr 2018
		 * author: swathi
		 */ 
        function fetchUserPage(params) {
            $scope.model.loadingUsers = true;
            IndentityManagementApiService.listUsersByGroup(params).then(function(response) {
            	$scope.model.usersCount = response.data.paginationInformation.totalResults;
            	$scope.model.users = response.data.result;
                $scope.model.loadingUsers = false;
            }, function(){
            	$scope.model.loadingUsers = false;
            });
        }

        /*
		 * purpose: search users from users table
		 * created: 13 apr 2018
		 * author: swathi
		 */ 
        $scope.searchUsers = function(){
        	var params = {
				token: $rootScope.ehubObject.token,
				keyword: $scope.model.userFilter,
				groupId: $scope.model.selectedGroup.userGroupId
			};
			if($scope.model.userFilter.length >= 3){
				$scope.model.loadingUsers = true;
				IndentityManagementApiService.filterUsers(params).then(function(response) {
					$scope.model.usersCount = response.data.paginationInformation.totalResults;
	            	$scope.model.users = response.data.result;
	            	$scope.model.loadingUsers = false;
				}, function() {
					$scope.model.loadingUsers = false;
				});
			}
        };
        
        // Add user popover
        $scope.myPopover = {
    		templateUrl: 'views/popover.html',
    		open: function open() {
    			$scope.myPopover.isOpen = true;
	        },
	        close: function close() {
	        	$scope.popupModel.userResults = [];
	        	$scope.myPopover.isOpen = false;
	        }
        };
        
        /*
		 * purpose: search user by key word
		 * created: 10 apr 2018
		 * author: swathi
		 */ 
        $scope.searchUserBykeyword = function(searchUser) {
			var params = {
				token: $rootScope.ehubObject.token,
				keyword: searchUser
			};
			//pass search params if search keyword length more than or equal 3
			if(searchUser.length >= 3){
				$scope.popoverLoader = true;
				IndentityManagementApiService.filterUsers(params).then(function(response) {
					// if total Results count less than or equal 10
					if(response.data.paginationInformation.totalResults <= 10){
						$scope.popoverLoader = false;
						$scope.popupModel.userResults = response.data.result;
						if($scope.popupModel.userResults.length > 0){
							jQuery(function($){
								setTimeout(function(){
									$("#selectUsersPopup").mCustomScrollbar({
										axis : "y",
										theme : "minimal"
									});
								}, 0);
							});
						}
					}
					else{
						/* api will return at a time 10 results. 
	        			 if results count more than 10, call api with page number 1 and results count as recordsPerPage to get all the results.*/

		        		var newParams = {
	        				token: $rootScope.ehubObject.token,
	        				keyword: searchUser,
	        				pageNumber: 1,
	        				recordsPerPage: response.data.paginationInformation.totalResults
		        		};
						IndentityManagementApiService.filterUsers(newParams).then(function(response) {
							$scope.popoverLoader = false;
							$scope.popupModel.userResults = response.data.result;
							if($scope.popupModel.userResults.length > 0){
								jQuery(function($){
									setTimeout(function(){
										$("#selectUsersPopup").mCustomScrollbar({
											axis : "y",
											theme : "minimal"
										});
									}, 0);
								});
							}
						}, function() {
							$scope.popoverLoader = false;
						});
					}
				}, function() {
					$scope.popoverLoader = false;
				});
			}
			if(searchUser.length == 0){
				//empty previous filtered results if search keyword is removed
				$scope.popupModel.userResults = [];
			}
		};
		 /*
		 * purpose: Add user to group
		 * created: 10 apr 2018
		 * author: swathi
		 */ 
        $scope.confirmUser = function(user) {
        	 if (!user) {
                 /* Selection is done with keyboard, use selection index
                   based on selected index retrieve user from users list */
                 var users = $scope.popupModel.userResults;
                 if ($scope.popupModel.selectedIndex >= 0 && $scope.popupModel.selectedIndex <users.length) {
                     user = users[$scope.popupModel.selectedIndex];
                 }
             }
			var params = {
				token: $rootScope.ehubObject.token,
				groupId: $scope.model.selectedGroup.userGroupId,
				userId: user.userId 
			};
			$scope.popoverLoader = true;
			IndentityManagementApiService.associateUser(params).then(function() {
				$scope.popoverLoader = false;
				$scope.myPopover.close();
				//fetch users list by selected group id
				params = {
	    			token: $rootScope.ehubObject.token,
	    			groupId: $scope.model.selectedGroup.userGroupId
	        	};
				fetchUserPage(params);
				HostPathService.FlashSuccessMessage('SUCCESSFUL ADD USER', 'Successfully added user');
			}, function(error) {
				$scope.popoverLoader = false;
				HostPathService.FlashErrorMessage('ERROR ADD USER', error.responseMessage);
			});
		};
		/*
		 * purpose: select previous user in Add user popover
		 * created: 10 apr 2018
		 * author: swathi
		 */ 
		$scope.previousUser = function(){
			 var users = $scope.popupModel.userResults;
			 //if selection made through keyboard
             if(users && users.length > 0 && $scope.popupModel.selectedIndex > 0) {
            	 // subtract by 1 to get previous user index
            	 $scope.popupModel.selectedIndex -= 1;
            	 $scope.popupModel.selectedUser = users[$scope.popupModel.selectedIndex];
             }
		};
		/*
		 * purpose: select next user in Add user popover
		 * created: 10 apr 2018
		 * author: swathi
		 */ 
		$scope.nextUser = function() {
             var users = $scope.popupModel.userResults;
             //if selection made through keyboard
             if(users && users.length > 0 && $scope.popupModel.selectedIndex < users.length -1) {
            	 // add by 1 to get next user index
            	 $scope.popupModel.selectedIndex += 1;
            	 $scope.popupModel.selectedUser = users[$scope.popupModel.selectedIndex];
             }
         };
        /*
		 * purpose: open create group modal
		 * created: 05 apr 2018
		 * author: swathi
		 */ 
        $scope.showCreateGroupPopup = function(isEdit, selectedGroup){
        	var openModalInstance = $uibModal.open({
        		templateUrl: 'modal/views/createGroup.jsp',
             	backdrop: 'static',
             	controller: 'CreateGroupController',
			 	windowClass: 'custom-modal createcase-modal',
			 	resolve: {
			 		isEdit: function(){
			 			return isEdit;
			 		},
			 		selectedGroup: function(){
			 			return selectedGroup;
			 		}
			 	}
        	});
        	openModalInstance.result.then(function(response){
        		//Successful create group modal
        	    if(response == 'success'){
        	    	HostPathService.FlashSuccessMessage('SUCCESSFUL CREATE GROUP', 'Successfully created group');
        	    	$scope.loadAllGroups();
        	    }
        	   //Successful update group modal
        	    if(response == 'update'){
        	    	HostPathService.FlashSuccessMessage('SUCCESSFUL UPDATE GROUP', 'Successfully updated group');
        	    	$scope.loadAllGroups();
        	    }
        	}, function(){
        		
        	});
        };
        /*
		 * purpose: open delete group modal
		 * created: 07 apr 2018
		 * author: swathi
		 */ 
        $scope.showDeleteGroupModal = function(selectedGroup){
        	var openModalInstance = $uibModal.open({
        		templateUrl: 'modal/views/deleteGroup.jsp',
             	backdrop: 'static',
             	controller: 'DeleteGroupController',
			 	windowClass: 'custom-modal createcase-modal',
			 	resolve: {
			 		selectedGroup: function(){
			 			return selectedGroup;
			 		}
			 	}
        	});
        	openModalInstance.result.then(function(response){
        	    if(response == 'DEACTIVATE'){
        	    	HostPathService.FlashSuccessMessage('SUCCESSFUL DEACTIVATE GROUP', 'Successfully deactivated group');
        	    	$scope.selectGroup(selectedGroup.userGroupId);
        	    }
        	    if(response == 'ACTIVATE'){
        	    	HostPathService.FlashSuccessMessage('SUCCESSFUL ACTIVATE GROUP', 'Successfully activated group');
        	    	$scope.selectGroup(selectedGroup.userGroupId);
        	    }
        	}, function(){
        		
        	});
        };
        /*
		 * purpose: open delete User From Group modal
		 * created: 09 apr 2018
		 * author: swathi
		 */ 
        $scope.deleteUserFromGroup = function(userId, groupId){
        	var openModalInstance = $uibModal.open({
        		templateUrl: 'modal/views/deleteGroupMember.jsp',
             	backdrop: 'static',
             	controller: 'DeleteGroupMemberController',
			 	windowClass: 'custom-modal createcase-modal',
			 	resolve: {
			 		userId: function(){
			 			return userId;
			 		},
			 		groupId: function(){
			 			return groupId;
			 		}
			 	}
        	});
        	openModalInstance.result.then(function(response){
        	    if(response == 'DELETE'){
        	    	HostPathService.FlashSuccessMessage('SUCCESSFUL DELETE USER FROM GROUP', 'Successfully deleted user from group');
        	    	$scope.selectGroup(groupId);
        	    }
        	}, function(){
        		
        	});
        };
        /*
		 * purpose: get group by search keyword
		 * created: 09 apr 2018
		 * author: swathi
		 */ 
        $scope.searchGroupByKeyWord = function(keyword) {
        	params = {
    			token: $rootScope.ehubObject.token,
    			keyword: keyword
        	};
        	//pass search params if search keyword length more than or equal 3
        	if(keyword.length >= 3){
	        	$scope.groupsLoader = true;
	        	IndentityManagementApiService.getFullTextSearchGroup(params).then(function (response) {
	        		if(response.data.paginationInformation.totalResults <= 10){
		        		$scope.groupsLoader = false;
		        		$scope.model.groups = response.data.result;
	        		}else{
	        			/* api will return at a time 10 results. 
	        			 if results count more than 10, call api with page number 1 and results count as recordsPerPage to get all the results.*/
		        		var newParams = {
	        				token: $rootScope.ehubObject.token,
	        				keyword: keyword,
	        				pageNumber: 1,
	        				recordsPerPage: response.data.paginationInformation.totalResults
		        		};
						IndentityManagementApiService.getFullTextSearchGroup(newParams).then(function(response) {
							$scope.groupsLoader = false;
			        		$scope.model.groups = response.data.result;
						}, function() {
							$scope.groupsLoader = false;
						});
					}
	            }, function(){
	            	$scope.groupsLoader = false;
	            });
        	}
        	if(keyword.length == 0){
        		//if search keyword is removed load all the existed groups.
        		$scope.loadAllGroups();
        	}
        };
}