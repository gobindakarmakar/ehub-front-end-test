'use strict';

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', function($stateProvider, $provide, $urlRouterProvider){
	
		//define module name
	   window.appName = 'identityManagement';
	   
	   // In order to use instances of $stateProvider, $urlRouterProvider in run file we are using  $provide factory
	   $provide.factory('$stateProvider', function () {
		   return $stateProvider;
	   });
	   $provide.factory('$urlRouterProvider', function () {
		   return $urlRouterProvider;
	   });
}]);