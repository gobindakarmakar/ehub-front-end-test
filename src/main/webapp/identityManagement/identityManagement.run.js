'use strict';
angular.module('identityManagementApp')
		.run(['$rootScope', 'UploadFileService', 'HostPathService', '$location', '$state','$stateProvider','$urlRouterProvider','$http','EHUB_FE_API', function($rootScope, UploadFileService, HostPathService, $location, $state, $stateProvider, $urlRouterProvider, $http, EHUB_FE_API){
			//get user session details from jsp function
			var userDetails = getAgainTokenBySession();
			
			var url;
			$rootScope.ehubObject = userDetails;
			HostPathService.setHostPath($location.absUrl());
			
			$rootScope.$on('$stateChangeStart', function(event, toState) {
				$(".Bubble_Chart_tooltip").css("display", "none");
				if(userDetails.fullName === "DataEntry User"){
					if(toState.name == 'dataCuration' || toState.name == 'addDataCuration'){
					}else{
						$location.path('/404');
					}
				}
			});
			
			//Storing JSON data from userDATA.json
			$http.get('data/userData.json').then(function(response){
				$rootScope.jsnData = response.data;
			});
			
			$stateProvider
			.state('identityUsers', {
				url: '/users',
				templateUrl: function(){
					return  EHUB_FE_API + appName + '/views/users.jsp';
				},
				controller : 'UsersController'
			})
			.state('identityGroups', {
				url: '/groups',
				templateUrl: function(){
					return  EHUB_FE_API + appName + '/views/groups.jsp';
				},
				controller : 'GroupsController'
			})
			.state('identityPersonal', {
				url: '/profile',
				templateUrl: function(){
					return  EHUB_FE_API + appName + '/views/personal.jsp';
				},
				controller : 'PersonalController'
			})
			.state('404', {
				url : '/404',
				templateUrl : function(){
					return EHUB_FE_API + 'error';
				}
			});
		   $urlRouterProvider.when('', '/profile');
		   $urlRouterProvider.otherwise(function(){
				return '/404';
			});

			/*
		     * @purpose: Get Submenu Data
		     * @created: 2 Feb 2018
		     * @params: params
		     * @return: no
		     * @author: Zameer
		    */
			function getSubmenu(){
				//check condition for main module pages url
				if(window.location.hash.indexOf("#!/") < 0 && window.location.hash.indexOf("#") >= 0){
					url = 'scripts/common/data/submenu.json';
				}
				//check condition for other modules pages url
		   		else{
					url = '../scripts/common/data/submenu.json';
				}
				UploadFileService.getSubmenuData(url).then(function(response){
					if($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)){
						angular.forEach(response.data.dashboarDropDownMenuItems,function(val){
							if(val.menu == "Manage"){
								angular.forEach(val.content,function(v){
									//Enable System Settings for admin user
									if(v.name == "System Settings" && $rootScope.ehubObject.adminUser){
										v.disabled ="no";
									}
									if(v.name == "Document Parsing"){
										v.disabled ="no";
									}
									if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
										v.disabled ="yes";
									}
								});
							}
						});
					}
					HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
					HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
				},function(error){
					HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
				});
			}
			getSubmenu();
			
			//Event is fired on state change success
			$rootScope.$on('$stateChangeSuccess', function(event, toState) {
				if (toState.name == '404') {
					$rootScope.topPanelPreview = false;
				} else {
					// Show top panel if token exist
					if($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== ""){
						$rootScope.topPanelPreview = true;
					}
				}
			});
		}]);