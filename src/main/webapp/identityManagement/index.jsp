<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="identityManagementApp">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="../assets/images/logo-new.png" />
		<title>IdentityManagement</title>

		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="./assets/css/identity-management-vendor.min.css" rel="stylesheet"/>
		<!-------------------------------  Vendor Styles Ends  --------------------------------->
		
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="./assets/css/identity-management-styles.min.css" rel="stylesheet"/>
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	
	<body ng-cloak>
		 <flash-message class="main-flash-wrapper"></flash-message>
		 <div ng-show="topPanelPreview">
			<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
		 </div>   
		<div ui-view></div>
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
		<script src='../vendor/jquery/js/jquery.min.js'></script>
        <script src='../vendor/jquery/js/jquery-ui.js'></script>
        <script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
        <script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
        <script src='../vendor/jquery/js/bootstrap.min.js'></script>
        <script src='../vendor/jquery/js/moment.min.js'></script>
		<script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
		<script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
	 	<script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>

        <script src='../vendor//angular/js/angular.min.js'></script>
        <script src='../vendor//angular/js/angular-ui-router.min.js'></script>
        <script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
        <script src='../vendor//angular/js/html2canvas.js'></script>
		<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
		<script src="../vendor/angular/js/lodash.min.js"></script>
        <script src='../vendor//angular/js/angular-flash.min.js'></script>
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		
		<!----------------------------  Initial Configuration JS Starts  ------------------------------->
		<script src='../scripts/app.js'></script>
		<script src="identityManagement.app.js"></script>
		<script src="identityManagement.config.js"></script>
		<script src="identityManagement.run.js"></script>
		<!----------------------------  Initial Configuration JS Ends  ------------------------------->
		
		<!--------------------------------  Common Scripts Js Starts  ------------------------------>
		<script src='../scripts/common/constants/app.constant.js'></script>
    	<script src='../scripts/common/constants/common.constant.js'></script>
    	<script src='../scripts/discover/constants/discover.constant.js'></script>
    	<script src='../scripts/act/constants/act.constant.js'></script>
    	
		<script src="../scripts/common/services/upload.file.service.js"></script>
		<script src="../scripts/common/services/common.service.js"></script>
		<script src="../scripts/common/services/shared.service.js"></script>
		<script src="../scripts/common/services/top.panel.api.service.js"></script>
		<script src='../scripts/common/services/riskScore.service.js'></script>
		<script src='../scripts/discover/services/discover.api.service.js'></script>
    	<script src='../scripts/act/services/act.api.service.js'></script>
    	<script src='../scripts/act/services/act.graph.service.js'></script>
    	
    	
    	<script src='../scripts/common/js/submenu.controller.js'></script>
    	<script src='../scripts/common/js/top.panel.controller.js'></script>
    	<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
    	<script src='../scripts/common/js/advanced.search.controller.js'></script>
    	<script src='../scripts/common/js/user.events.controller.js'></script>
    	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
    	<script src="../scripts/common/js/notification.controller.js"></script>
    	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
    	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
    	<script src="../scripts/common/modal/js/create.event.controller.js"></script>
    	<script src="../scripts/common/modal/js/participants.event.controller.js"></script> 
		<!--------------------------------  Common Scripts Js Ends  ------------------------------>
		
		<!--------------------------------  Identity Management Scripts Js Starts  ------------------------------>
		<script src="services/identityManagement.api.service.js"></script>
		<script src="directives/custom.keys.directive.js"></script>
		<script src="directives/scroll.to.active.directive.js"></script>
		<script src="controllers/groups.controller.js"></script>
		<script src="controllers/personal.controller.js"></script>
		<script src="controllers/users.controller.js"></script>
		<script src="modal/controllers/changePasswordController.js"></script>
		<script src="modal/controllers/createGroupController.js"></script>
		<script src="modal/controllers/createUser.controller.js"></script>
		<script src="modal/controllers/deleteUser.controller.js"></script>
		<script src="modal/controllers/delete.group.controller.js"></script>
		<script src="modal/controllers/delete.group.member.controller.js"></script>
		<!--------------------------------  Identity Management Scripts Js Ends  ------------------------------>
		
		
		
		<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/

		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
<!-- 		<script src="../vendor/common-vendor.min.js"></script> -->
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		<!----------------------------  Initial Configuration JS Starts  ------------------------------->
<!-- 		<script src='../scripts/app.js'></script> -->
<!-- 		<script src="identityManagement.app.js"></script> -->
<!-- 		<script src="identityManagement.config.js"></script> -->
<!-- 		<script src="identityManagement.run.js"></script> -->
		<!----------------------------  Initial Configuration JS Ends  ------------------------------->
		

		<!--------------------------------  Common Scripts Js Starts  ------------------------------>
<!-- 		<script src="../scripts/common-scripts.min.js"></script> -->
		<!--------------------------------  Common Scripts Js Ends   ------------------------------->

		<!--------------------------------  Identity Management Scripts Js Starts  ------------------------------>
<!-- 		<script src="./assets/minifiedJs/IDM-scripts.min.js"></script> -->
		<!--------------------------------  Identity Management Scripts Js Ends   ------------------------------->
<!--new minified Js start -->
<!-- <script src="../vendor/common.vendor.new.min.js"></script>
<script src="./assets/minifiedJs/identityManagement.vendor.new.min.js"></script>

<script src="./assets/minifiedJs/identityManagement.intialize.new.min.js"></script>
 
<script src="../scripts/common.scripts.new.min.js"></script>
<script src="./assets/minifiedJs/identityManagement.ModuleScripts.new.min.js"></script> -->
<!--new minified Js end -->

<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->
		
		
		
			<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('.chat-bot-wrapper').mCustomScrollbar({
           axis: "y"
       });
   });
</script>
	</body>
</html>
