'use strict';
angular.module('identityManagementApp')
	   .controller('ChangePasswordController', changePasswordController);

		changePasswordController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'IndentityManagementApiService',
			'EHUB_API',
			'Flash',
			'HostPathService',
			'profile'
		];
		
		function changePasswordController(
			$scope,
			$rootScope,
			$uibModalInstance,
			IndentityManagementApiService,
			EHUB_API,
			Flash,
			HostPathService,
			profile
		) {
		$scope.model = profile;
		$scope.model.error = false;
		var params = {
			token: $rootScope.ehubObject.token
		};
		
		//Disable confirm button if it is not satisfies the below condition
		$scope.isConfirmButtonDisabled = function() {
            return !$scope.model.originalPassword || $scope.model.originalPassword.length == 0 || !$scope.model.newPassword || $scope.model.newPassword.length === 0 || !$scope.model.newPassword2   || $scope.model.newPassword2.length === 0 || $scope.model.newPassword !== $scope.model.newPassword2;
        };
        
        //check for old and new passwords do not match 
        $scope.showPasswordsDontMatch = function() {
            return $scope.model.originalPassword && $scope.model.originalPassword.length > 0 && $scope.model.newPassword && $scope.model.newPassword.length > 0 && $scope.model.newPassword2 && $scope.model.newPassword2.length > 0 && $scope.model.newPassword !== $scope.model.newPassword2;
        };
		
		/*
		 * purpose: change password
		 * created: 13 apr 2018
		 * author: swathi
		 */ 
		$scope.changePassword = function(){
			$scope.changePasswordLoader = true;
			var data = {
				"userId": $scope.model.userId,
				"emailAddress": $scope.model.emailAddress,
				"firstName": $scope.model.firstName,
				"lastName": $scope.model.lastName,
				"middleName": $scope.model.middleName,
				"screenName": $scope.model.screenName,
				"dob": $scope.model.dob,
				"country": $scope.model.country,
                "oldPassword": $scope.model.originalPassword,
                "newPassword": $scope.model.newPassword,
                "retypePassword": $scope.model.newPassword2
			};			
			IndentityManagementApiService.updatePersonalDetails(data, params).then(function() {
				$scope.changePasswordLoader = false;
				HostPathService.FlashSuccessMessage('SUCCESSFUL CHANGE PASSWORD', 'Successfully changed password');
                $scope.closeModal();
            }, function(e) {
            	$scope.changePasswordLoader = false;
                $scope.model.error = true;
                HostPathService.FlashErrorMessage('ERROR CHANGE PASSWORD', e.responseMessage);
            });
		};
		
		/*
		 * purpose: close change password modal
		 * created: 04 apr 2018
		 * author: swathi
		 */ 	
		$scope.closeModal = function(){
			$uibModalInstance.close();
		};			
}