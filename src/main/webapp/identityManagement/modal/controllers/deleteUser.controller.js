'use strict';
angular.module('identityManagementApp')
	   .controller('DeleteUserController', deleteUserController);

		deleteUserController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'IndentityManagementApiService',
			'EHUB_API',
			'Flash',
			'HostPathService',
			'selectedUser'
		];
		
		function deleteUserController(
				$scope,
				$rootScope,
				$uibModalInstance,
				IndentityManagementApiService,
				EHUB_API,
				Flash,
				HostPathService,
				selectedUser
		){
			$scope.model = selectedUser;
			var params = {
				token: $rootScope.ehubObject.token,
				userId: $scope.model.userId
			};
			
			/*
			 * purpose: deactivate user
			 * created: 09 apr 2018
			 * author: zameer
			 */ 
			$scope.deactivateUser = function(){
				$scope.deactivateUserLoader = true;
				IndentityManagementApiService.deactivateUser(params).then(function() {
					$scope.deactivateGroupLoader = false;
					$uibModalInstance.close('DEACTIVATE');
	            }, function(e) {
	            	$scope.deactivateUserLoader = false;
	                HostPathService.FlashErrorMessage('ERROR DEACTIVATE GROUP', e.responseMessage);
	            });
			};
			
			/*
			 * purpose: activate user
			 * created: 09 apr 2018
			 * author: zameer
			 */ 
			$scope.activateUser = function(){
				$scope.deactivateUserLoader = true;
				IndentityManagementApiService.activateUser(params).then(function() {
					$scope.deactivateGroupLoader = false;
					$uibModalInstance.close('ACTIVATE');
	            }, function(e) {
	            	$scope.deactivateUserLoader = false;
	                HostPathService.FlashErrorMessage('ERROR ACTIVATE GROUP', e.responseMessage);
	            });
			};

			/*
			 * purpose: close delete user modal
			 * created: 09 apr 2018
			 * author: zameer
			 */ 	
			$scope.closeModal = function(){
				$uibModalInstance.close('close');
			};
			
		}
		