'use strict';
angular.module('identityManagementApp')
	   .controller('CreateUserController', createUserController);

		createUserController.$inject = [
			'$rootScope',
			'$scope',
			'IndentityManagementApiService',
			'$uibModalInstance',
			'isEdit',
			'userDetails'
		];
		
		function createUserController(
			$rootScope,
			$scope,
			IndentityManagementApiService,
			$uibModalInstance,
			isEdit,
			userDetails
		) {
			var params = {
		        token: $rootScope.ehubObject.token
		    };
			$scope.isEdit = isEdit;
			$scope.userDetails = {};
			
			/*
			 * purpose: create User
			 * created: 04 dec 2018
			 * @params: data(object)
			 * author: swathi
			 */ 	
			$scope.createUser = function(data){
				$scope.createUserLoader = true;
				IndentityManagementApiService.createNewUser(params, data).then(function(){
					$scope.createUserLoader = false;
					$uibModalInstance.close('success');
				}, function(){
					$scope.createUserLoader = false;
				});
			};
			
			/*
			 * purpose: edit User
			 * created: 04 dec 2018
			 * @params: data(object)
			 * author: swathi
			 */ 	
			$scope.editUser = function(data){
				$scope.createUserLoader = true;
				var updateparams = {
				        token: $rootScope.ehubObject.token,
				        userId: data.userId
				    };
				IndentityManagementApiService.updateUser(updateparams, data).then(function(){
					$scope.createUserLoader = false;
					$uibModalInstance.close('updated');
				}, function(){
					$scope.createUserLoader = false;
				});
			};
			/*
			 * purpose: close modal
			 * created: 04 dec 2018
			 * author: swathi
			 */ 	
			$scope.closeModal = function(){
				$uibModalInstance.close("close");
			};
			
			if(userDetails){
				$scope.userDetails = userDetails;
				//Converting date to iso string
				var d = new Date(userDetails.dob);
				$scope.userDetails.dob = d.toISOString().split('T')[0];
			}
			// datepicker function
			$( function() {
			    $( "#datepicker" ).datepicker({
			    	changeMonth: true,
			        changeYear: true
			    });
			    $( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
			    if(userDetails){
			    	//set user dob as default date
			    	$("#datepicker").datepicker( "setDate" , userDetails.dob );
			    }
			  } );
		
		}