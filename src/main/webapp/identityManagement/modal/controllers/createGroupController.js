'use strict';
angular.module('identityManagementApp')
	   .controller('CreateGroupController', createGroupController);

		createGroupController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'IndentityManagementApiService',
			'EHUB_API',
			'Flash',
			'HostPathService',
			'isEdit',
			'selectedGroup'
		];
		
		function createGroupController(
			$scope,
			$rootScope,
			$uibModalInstance,
			IndentityManagementApiService,
			EHUB_API,
			Flash,
			HostPathService,
			isEdit,
			selectedGroup
		) {
		/*Initializing scope variables*/ 
		$scope.model = {};
		$scope.isEdit = isEdit;
		if(selectedGroup){
			$scope.model.editedGroup = selectedGroup;
		}
		var params = {
			token: $rootScope.ehubObject.token
		};
		/*
		 * purpose: create group
		 * created: 05 apr 2018
		 * author: swathi
		 */ 
		$scope.createGroup = function(){
			$scope.createGroupLoader = true;
			var data = {
				"name": $scope.model.editedGroup.name,
			    "description": $scope.model.editedGroup.description,
			    "remarks": $scope.model.editedGroup.remarks
			};
			IndentityManagementApiService.createGroup(data, params).then(function() {
				$scope.createGroupLoader = false;
				$uibModalInstance.close('success');
            }, function(e) {
            	$scope.createGroupLoader = false;
                HostPathService.FlashErrorMessage('ERROR CREATE GROUP', e.responseMessage);
            });
		};
		/*
		 * purpose: update group
		 * created: 06 apr 2018
		 * author: swathi
		 */ 
		$scope.updateGroup = function(){
			$scope.createGroupLoader = true;
			params = {
				token: $rootScope.ehubObject.token,
				groupId: $scope.model.editedGroup.userGroupId
			};
			var data = {
				"name": $scope.model.editedGroup.name,
			    "description": $scope.model.editedGroup.description,
			    "remarks": $scope.model.editedGroup.remarks,
			    "userGroupId": $scope.model.editedGroup.userGroupId
			};
			IndentityManagementApiService.updateGroup(data, params).then(function() {
				$scope.createGroupLoader = false;
				$uibModalInstance.close('update');
            }, function(e) {
            	$scope.createGroupLoader = false;
                HostPathService.FlashErrorMessage('ERROR UPDATE GROUP', e.responseMessage);
            });
		};
		/*
		 * purpose: close group modal
		 * created: 05 apr 2018
		 * author: swathi
		 */ 	
		$scope.closeModal = function(){
			$uibModalInstance.close('close');
		};		
}