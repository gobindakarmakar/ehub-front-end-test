'use strict';
angular.module('identityManagementApp')
	   .controller('DeleteGroupController', deleteGroupController);

		deleteGroupController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'IndentityManagementApiService',
			'EHUB_API',
			'Flash',
			'HostPathService',
			'selectedGroup'
		];
		
		function deleteGroupController(
			$scope,
			$rootScope,
			$uibModalInstance,
			IndentityManagementApiService,
			EHUB_API,
			Flash,
			HostPathService,
			selectedGroup
		) {
			$scope.model = selectedGroup;
			var params = {
				token: $rootScope.ehubObject.token,
				groupId: $scope.model.userGroupId
			};
			/*
			 * purpose: deactivate group
			 * created: 07 apr 2018
			 * author: swathi
			 */ 
			$scope.deactivateGroup = function(){
				$scope.deactivateGroupLoader = true;
				IndentityManagementApiService.deactivateGroup(params).then(function() {
					$scope.deactivateGroupLoader = false;
					$uibModalInstance.close('DEACTIVATE');
	            }, function(e) {
	            	$scope.deactivateGroupLoader = false;
	                HostPathService.FlashErrorMessage('ERROR DEACTIVATE GROUP', e.responseMessage);
	            });
			};
			/*
			 * purpose: activate group
			 * created: 07 apr 2018
			 * author: swathi
			 */ 
			$scope.activateGroup = function(){
				$scope.deactivateGroupLoader = true;
				IndentityManagementApiService.activateGroup(params).then(function() {
					$scope.deactivateGroupLoader = false;
					$uibModalInstance.close('ACTIVATE');
	            }, function(e) {
	            	$scope.deactivateGroupLoader = false;
	                HostPathService.FlashErrorMessage('ERROR ACTIVATE GROUP', e.responseMessage);
	            });
			};
			/*
			 * purpose: close delete group modal
			 * created: 07 apr 2018
			 * author: swathi
			 */ 	
			$scope.closeModal = function(){
				$uibModalInstance.close('close');
			};	
		}