'use strict';
angular.module('identityManagementApp')
	   .controller('DeleteGroupMemberController', deleteGroupMemberController);

		deleteGroupMemberController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'IndentityManagementApiService',
			'EHUB_API',
			'Flash',
			'HostPathService',
			'userId',
			'groupId'
		];
		
		function deleteGroupMemberController(
			$scope,
			$rootScope,
			$uibModalInstance,
			IndentityManagementApiService,
			EHUB_API,
			Flash,
			HostPathService,
			userId,
			groupId
		) {
			var params = {
				token: $rootScope.ehubObject.token,
				groupId: groupId,
				userId: userId
			};
			/*
			 * purpose: delete user from group
			 * created: 09 apr 2018
			 * author: swathi
			 */ 
			$scope.deleteGroupMember = function(){
				$scope.deactivateGroupLoader = true;
				IndentityManagementApiService.deassociateUser(params).then(function() {
					$scope.deactivateGroupLoader = false;
					$uibModalInstance.close('DELETE');
	            }, function(e) {
	            	$scope.deactivateGroupLoader = false;
	                HostPathService.FlashErrorMessage('ERROR DELETE GROUP', e.responseMessage);
	            });
			};
			/*
			 * purpose: close activate/Deactivate User modal
			 * created: 07 apr 2018
			 * author: swathi
			 */ 	
			$scope.closeModal = function(){
				$uibModalInstance.close('close');
			};	
		}