<div class="custom-spinner case-dairy-spinner" ng-show="createGroupLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="modal-header">
    <button type="button" class="close" ng-click="closeModal()"><span>&#x2716;</span></button>
    <h4 class="modal-title" ng-if="!isEdit">
        {{jsnData.IDM['FUNCTIONAL-GROUP-MGMT']['POPUP']['CREATE-TITLE']}}
    </h4>
    <h4 class="modal-title" ng-if="isEdit">
        {{jsnData.IDM['FUNCTIONAL-GROUP-MGMT']['POPUP']['EDIT-TITLE']}}
    </h4>
</div>
<div class="modal-body">
    <form class="form">
        <div class="form-group">
            <label class="control-label">{{jsnData.IDM['FUNCTIONAL-GROUP-MGMT']['POPUP']['CREATE-NAME']}}</label>
            <input type="text" class="form-control" ng-model="model.editedGroup.name">
        </div>
        <div class="form-group">
            <label class="control-label">{{jsnData.IDM['FUNCTIONAL-GROUP-MGMT']['POPUP']['DESCRIPTION']}}</label>
            <input type="text" class="form-control" ng-model="model.editedGroup.description">
        </div>
        <div class="form-group">
            <label class="control-label">{{jsnData.IDM['FUNCTIONAL-GROUP-MGMT']['POPUP']['REMARKS']}}</label>
            <input type="text" class="form-control" ng-model="model.editedGroup.remarks">
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" ng-click="closeModal()">{{jsnData.IDM['GENERAL']['ACTION']['CANCEL']}}</button>
    <button type="button" ng-if="!isEdit" class="btn btn-primary" ng-disabled="!model.editedGroup.name || model.editedGroup.name.length == 0"
            ng-click="createGroup();">{{jsnData.IDM['GENERAL']['ACTION']['SAVE']}}</button>
    <button type="button" ng-if="isEdit" class="btn btn-primary" ng-disabled="!model.editedGroup.name || model.editedGroup.name.length == 0"
            ng-click="updateGroup();">{{jsnData.IDM['GENERAL']['ACTION']['SAVE']}}</button>
</div>
