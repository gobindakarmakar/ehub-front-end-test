<div class="modal-header">
    <button type="button" class="close" ng-click="closeModal()"><span>&#x2716;</span></button>
    <h4 class="modal-title">DELETE GROUP MEMBER</h4>
</div>
<div class="modal-body">
    <div class="mar-y15 f-14">
        Are you sure you want to delete this user from the group?
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" ng-click="closeModal()">{{jsnData.IDM["GENERAL"]["ACTION"]["CANCEL"]}}</button>
    <button type="button" class="btn btn-primary" ng-click="deleteGroupMember();">{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["POPUP"]["DELETE-CONFIRM"]}}</button>
</div>