<div class="modal-header">
    <button type="button" class="close" aria-hidden="true" ng-click="closeModal()">
        <span>&#x2716;</span>
    </button>
    <h4 class="modal-title">Select Filters</h4>
</div>
<div class="modal-body">
    <form class="form text-uppercase">
        <div class="form-group row mar-b5">
            <label class="col-sm-4 pad-t10">Order By:</label>
            <div class="col-sm-8">
                <select class="form-control  text-uppercase" ng-model="filters.orderBy">
                    <option value="firstName">FirstName</option>
                </select>
            </div>
        </div>
        <div class="form-group row mar-b0">
            <label class="col-sm-4 pad-t10">OrderIn:</label>
            <div class="col-sm-8">
                <select class="form-control  text-uppercase" ng-model="filters.orderIn">
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                </select>
            </div>
        </div>
        <div class="form-group row mar-b5">
            <label class="col-sm-4 pad-t10">Status:</label>
            <div class="col-sm-8">
                <select class="form-control  text-uppercase" ng-model="filters.status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
    </form>
</div>
<div class="custom-spinner case-dairy-spinner" ng-show="usersLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="modal-footer">
    <button type="button" ng-disabled="(!filters.orderBy || !filters.orderIn) && !filters.status" class="btn btn-update"
            ng-click="applyFilters()">Apply</button>
</div>