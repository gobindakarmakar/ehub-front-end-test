<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modal-header-wrapper">
    <div class="modal-header">
        <button type="button" class="close" ng-click="closeModal();"><span>&#x2716;</span></button>
        <h2 class="modal-title" ng-if="!isEdit">
            {{jsnData.IDM["USER-MGMT"].POPUP["CREATE-TITLE"]}}
        </h2>
        <h2 class="modal-title" ng-if="isEdit">
            {{jsnData.IDM["USER-MGMT"].POPUP["EDIT-TITLE"]}}
        </h2>
    </div>
</div>
<div  class="modal-body clearfix">
    <form class="form pad-0" name="newUserForm">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">Screen Name</label>
                <input name="screenName" type="text" class="form-control" auto-focus ng-model="userDetails.screenName">
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">Middle Name</label>
                <input name="middleName" type="text" class="form-control" auto-focus ng-model="userDetails.middleName">
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">dob</label>
                <input name="dob" uib-datepicker type="text" id="datepicker" class="form-control" auto-focus ng-model="userDetails.dob">
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">Country</label>
                <input name="country" type="text" class="form-control" auto-focus ng-model="userDetails.country">
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">{{jsnData.IDM["USER-MGMT"].POPUP["CREATE-EMAIL"]}}</label>
                <span style="color:white; background-color: red; padding: 0 5px 0 5px" ng-show="newUserForm.emailInput.$error.email">
                    {{jsnData.IDM["USER-MGMT"].POPUP["CREATE-INVALID-EMAIL"]}}
                </span>
                <input name="emailInput" type="email" class="form-control" ng-model="userDetails.emailAddress">
            </div>
        </div>
        <div class="col-xs-12" ng-if="!isEdit">
            <div class="form-group">
                <label class="control-label">{{jsnData.IDM["USER-MGMT"].POPUP["CREATE-PASSWORD"]}}</label>
                <input type="password" class="form-control" ng-model="userDetails.password">
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <label class="control-label">{{jsnData.IDM["USER-MGMT"].POPUP["CREATE-FIRSTNAME"]}}</label>
                <input type="text" class="form-control" ng-model="userDetails.firstName">
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <label class="control-label">{{jsnData.IDM["USER-MGMT"].POPUP["CREATE-LASTNAME"]}}</label>
                <input type="text" class="form-control" ng-model="userDetails.lastName">
            </div>
        </div>
        <div class="alert error" ng-if="model.errorMessage">
            {{model.errorMessage}}
        </div>
    </form>
</div>
<div class="custom-spinner case-dairy-spinner" ng-show="createUserLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="modal-footer-wrapper">
    <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="closeModal();">{{jsnData.IDM.GENERAL.ACTION.CANCEL}}</button>
        <button ng-if="!isEdit"
                ng-disabled = "!userDetails.firstName || !userDetails.lastName || !userDetails.emailAddress || !userDetails.password || !userDetails.country || !userDetails.dob || !userDetails.screenName"
                type="button"
                class="btn btn-primary"
                ng-click="createUser(userDetails);">{{jsnData.IDM.GENERAL.ACTION.SAVE}}
        </button>
        <button ng-if="isEdit"
                type="button"
                class="btn btn-primary"
                ng-click="editUser(userDetails);">{{jsnData.IDM.GENERAL.ACTION.UPDATE}}
        </button>
    </div>
</div>

