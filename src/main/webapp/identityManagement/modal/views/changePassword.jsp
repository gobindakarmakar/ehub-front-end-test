<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="custom-spinner case-dairy-spinner" ng-show="changePasswordLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="modal-header">
    <button type="button" class="close" ng-click="closeModal()"><span>&#x2716;</span></button>
    <h4 class="modal-title">{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-TITLE']}}</h4>
</div>
<div class="modal-body">
    <form class="form pad-0">
        <div clas="form-group">
            <label class="control-label">{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-ORIGINAL']}}</label>
            <input auto-focus type="password" class="form-control" ng-model="model.originalPassword">

            <label>{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-NEW-PASSWORD']}}</label>
            <input type="password" class="form-control" ng-model="model.newPassword">

            <label>{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-NEW-PASSWORD2']}}</label>
            <input type="password" class="form-control" ng-model="model.newPassword2">
        </div>
        <div ng-if="showPasswordsDontMatch()" style="color: red">
            <span>{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-NOT-MATCHING']}}</span>
        </div>
    </form>         
</div>
<div class="modal-footer">
    <div class="pull-left" ng-if="model.error" style="color: red">
        <span>{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-INVALID']}}</span>
    </div>

    <button type="button" class="btn btn-default" ng-click="closeModal()">{{jsnData.IDM['GENERAL']['ACTION']['CANCEL']}}</button>
    <button type="button" class="btn btn-primary" ng-disabled="isConfirmButtonDisabled()" ng-click="changePassword();">{{jsnData.IDM['PROFILE-MGMT']['POPUP']['CHANGE-PASSWORD-CONFIRM']}}</button>
</div>