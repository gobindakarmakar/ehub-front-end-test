<div class="custom-spinner case-dairy-spinner" ng-show="deactivateGroupLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="modal-header clearfix">
    <button type="button" class="close" ng-click="closeModal()"><span>&#x2716;</span></button>
    <h4 class="modal-title" ng-if="model.active == 0">
        ACTIVATE GROUP
    </h4>
    <h4 class="modal-title" ng-if="model.active == 1">
        {{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["POPUP"]["DEACTIVATE-TITLE"]}}
    </h4>
</div>
<div class="modal-body">
    <div ng-if="model.active == 0" class="mar-y15 f-14">
        Are you sure you want to activate this group?
    </div>
    <div ng-if="model.active == 1" class="mar-y15 f-14">
        Are you sure you want to deactivate this group?
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" ng-click="closeModal()">{{jsnData.IDM["GENERAL"]["ACTION"]["CANCEL"]}}</button>
    <button type="button" class="btn btn-primary" ng-click="deactivateGroup()" ng-if="model.active == 1">{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["POPUP"]["DELETE-CONFIRM"]}}</button>
    <button type="button" class="btn btn-primary" ng-click="activateGroup()" ng-if="model.active == 0">{{jsnData.IDM["FUNCTIONAL-GROUP-MGMT"]["POPUP"]["DELETE-CONFIRM"]}}</button>
</div>