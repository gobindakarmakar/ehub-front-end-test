'use strict';
angular.module('ehubMipApp')
	   .controller('MipLandingController', mipLandingController);

		mipLandingController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$window', 
			'$location',
			'$timeout',
			'MipApiService',
			'MipService'
		];
		
		function mipLandingController(
			$scope, 
			$rootScope,
			$state,
			$window,
			$location,
			$timeout,
			MipApiService,
			MipService) {
    
		
		var token = $rootScope.ehubObject.token;
	  	$scope.recentList = [];
	  	$scope.savedList = [];
	  	$scope.actualMipData = {};
	  	$scope.mipData = [];
	  	$scope.mipLandingFilter = '';
	  	$scope.showMipLandingLoader = false;
	  	var params = {
			token: token		
  		};
	  	
	  	/*get  all saved searches */
	  	$scope.getAllSavedSearches = function(){ 
	  		MipApiService.getAllSavedSearches(params).then(function(response){
	  			var todayDt = new Date().getTime();
	  			$timeout(function(){
		        	$scope.savedList = response.data.result;
		        	angular.forEach(response.data.result, function(d){
		        		/* 172800000 == 2 days */
		        		if(d.updatedOn && ((d.updatedOn-todayDt)<=172800000)){
		        			$scope.recentList.push(d);
		        		}else if((d.createdOn-todayDt)<=172800000){
		        			$scope.recentList.push(d);
		        		}
		        	});
	        	  },0);
	  		}, function(error){
	  			$scope.showMipLandingLoader = false;
	  		});
	  	}
	  	$scope.getAllSavedSearches();
	  	/*
	     * @purpose: open selected search
	     * @created: 17 oct 2017
	     * @params: data(object)
	     * @return: no
	     * @author: swathi
	    */
	  	$scope.openSelectedSearch = function(data){
	  		if(data){
	  			$state.go('mip',{id: data.searchId});
	  		}else{
		        $state.go('mip');
	  		}
	  	};
	  	/**
	  	 * Function to load data to show search filters
	  	 */
	  	$scope.loadMipData = function(){
	  		$scope.showMipLandingLoader = true;
	  		var data = {
	        	"q": "",
	        	"size": 0,
	        	"from": 0,
	        	"sort": "string",
	        	"timefilter": { 
	        	     "from":"",  
	        	     "to": "" 
	    	     } 
	  		};
	  		data = JSON.stringify(data);
	  		MipApiService.searchOrgWithFacts(params, data).then(function(response){
	  			$timeout(function(){
	        		$scope.showMipLandingLoader = false;
		        	$scope.actualMipData = response.data.facets;
		        	var datanew = {};
		    		angular.forEach(response.data.facets, function(d, i){
		    			if(!$scope.mipLandingFilter){
		    				$scope.mipLandingFilter = i;
		    			}
		    			$scope.mipData.push({
	    					id: i,
	    					val: d,
	    					title: i.replace("by-","").split("-").join(" ")
	    				});
		    		});
		    		$scope.getTypeAHead("initial");
	        	},0);
	  		}, function(error){
	  			$scope.showMipLandingLoader = false;
	  		});
	  	}
	  	$scope.loadMipData();
	  	/**
	  	 * Function to get type a head based on selected value
	  	 */
	  	$scope.getTypeAHead = function(initial){
	  		$("#mipLandingTypeAHead").val("");
	  		if(!initial)
	  			$('#mipLandingTypeAHead').typeahead("destroy");
	  		var data = [];	  
	  		angular.forEach($scope.actualMipData[$scope.mipLandingFilter], function(d, i){
				data.push(d.key);
			});
			
		  	$('#mipLandingTypeAHead').typeahead({
		  	  hint: true,
		  	  highlight: true,
		  	  minLength: 1
		  	},
		  	{
		  	  name: 'fiters',
		  	  source: MipService.substringMatcher(data)
		  	});
	  	}
	  	$('#mipLandingTypeAHead').on('typeahead:selected', function (e, datum) {
	  		var searchStr = ($scope.mipLandingFilter.replace("by-","").split("-").join(" "))+":"+datum;
	  		var url = $state.href('mip',{'searchSting': searchStr});
	  		window.open(url, "_self");
	  	});
};