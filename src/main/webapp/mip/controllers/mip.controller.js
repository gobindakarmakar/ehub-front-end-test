'use strict';
angular.module('ehubMipApp')
	   .controller('MipController', mipController);

		mipController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$timeout',
			'$uibModal',
			'MipService',
			'MipApiService',
			'MipConst'
		];
		
		function mipController(
				$scope, 
				$rootScope,
				$state,
				$timeout,
				$uibModal, 
				MipService,
				MipApiService,  
				MipConst) {
    
		   $scope.Mipcountries = MipConst.countriesInfo;// get mip constatnt here
		   $scope.selectedCountry = MipConst.selectedCountry;
		   
			var userId = $rootScope.ehubObject.token;
			var params = { 
				token: userId
			};
			$scope.showMipLoader = true;
			$scope.mipselect_view_type_value = "list";
			$scope.facetsPieData = [];
			$scope.facetsBarData = [];  
			$scope.resultData1 = [];
			$scope.mipResultRecords = 0; 
			$scope.topPersons = [];
			$scope.topSectors = [];
			$scope.topPlayers = [];
			$scope.topTechs = [];
			$scope.topInvestement = [];
			$scope.transactionsByInvestment;
			$scope.selectedCountryMapChart = "World" ;
			$scope.eventsMipSelect = "Investment";
			$scope.articlesMip = [];
			
			var div = d3.select("body").append("div").attr("class", "toolTip_horizontal").style("position", "absolute").style("z-index", 1000).style("background","rgb(27, 39, 53)").style("padding","5px 10px").style("border-radius","10px").style("font-size","10px").style("display","none");
		    $scope.isSavedSearchedChecked = false;/* flag to check whether loaded normally or loading from saved search */
			var mipdata = [];
			var filtersApplied = [];
			var filtersArr = [];
			var exptDataTable;
			var bombsData=[];
			var countryProjections = MipConst.countryProjections;
			var fieldMapping = {
				 "products": "products",
				 "areaServed": "address",
				 "location": "location",
				 "numberOfEmployees": "numberOfEmployees",
				 "businessStatus": "businesStatus",
				/* "by-operatingVerticals": [],
			     "by-investmentStatus": [],
			     "by-entity-classification": [],
			     "by-technology": [],
			     "by-isicV4": [],
			     "by-ownershipStatus": []*/
			};
			var data_default = {
			    "by-products": [],
			    "by-areaServed": [],
			    "by-location": [],
			    "by-numberOfEmployees": [],
			    "by-businessStatus": [],
			    "by-operatingVerticals": [],
			    "by-investmentStatus": [],
			    "by-entity-classification": [],
			    "by-technology": [],
			    "by-isicV4": [],
			    "by-ownershipStatus": []
			};
			angular.element(document).ready(function() {
				$(".top-filters-wrapper").mThumbnailScroller({
					axis : "x",
					speed : "10"
				});
				$('.applymscroll').mCustomScrollbar({
				 	axis : "y",
				 	theme : "minimal"
				});
			});
			/**
			 * show view by default text in mip content type selector
			 */
			
			$('#mipselect_view_type').on('change', function(){
			    $(this).find('option').text(function(i,t){
			        return this.selected ?( 'View By : ' + t) : t.replace ('View By : ', '');
			    });
			}).change();
			/**
			 * Function to load mip data
			 */
			$scope.loadmipdata = function(data, initial) {
				if(!data){
					data = data_default;
				}
				$scope.showMipLoader = true;
				var finalData = "";
				var finalData1 = [];
				
				angular.forEach(data, function(d, i){	
					angular.forEach(d, function(d1, i1){
						finalData1.push( i+":"+d1);
					});		
				});
				
				if(finalData1.length > 0){
					finalData = finalData1.join(" AND ");
				}
				getNewsForMIp(finalData1[0]);
				
				var data = {
					"q": finalData,
		        	"size": 500,
		        	"from": 0,
		        	"sort": "string",
		        	"timefilter": { 
		        	     "from": $('#datePicker1').val(),  
		        	     "to": $('#datePicker2').val() 
		        	 } 
				};
				data = JSON.stringify(data);
				
			    MipApiService.searchOrgWithFacts(params, data).then(function(response){
			    	if(!$scope.isSavedSearchedChecked){
		        		$scope.checkforSavedSearches();
		        		$scope.isSavedSearchedChecked = true;
		        	}
		            mipdata = response.data;
		            if(initial)
	            	plotCharts(mipdata.facets, mipdata);
		            $scope.mipResultRecords = 0;
		            $timeout(function(){
		            	$scope.mipResultRecords = mipdata.orgs.length;
			            $scope.resultData1 = mipdata.orgs;
			            $scope.loadSelectedView();
			            $timeout(function(){
			            	$scope.showMipLoader = false;
			            }, 10);
		            },0);
		  		}, function(error){
		  			$scope.showMipLoader = false;
		  		});
			};
			/**
			 * Function to get news for mip
			 */
			function getNewsForMIp(data){
				if(!data){
					data = "locations";/*default search to locations */
				}
				var dataInfo = {
					entity: data.split(":")[0],
					token: userId 
				};
				MipApiService.searchEntity(dataInfo).then(function(response){
					 $timeout(function(){
			       		$scope.articlesMip = response.data.articles;
	//			       		$(".applymscroll").mCustomScrollbar({   
	//			    			axis: "y",          
	//			    			theme: "minimal-dark"  
	//			    		});
			       	 },0);
				}, function(error){
					
				});
			}
			/* initialize date pickers */
			$('#datePicker1').datepicker();
			$('#datePicker2').datepicker();
			
			$scope.isSwitchStatus = false;
			/* toggle switch */
			$scope.toggleSwitch = function(){
				$scope.isSwitchStatus = !$scope.isSwitchStatus;
			};
			
			$scope.loadmipdata(data_default, "initial");/* call load function to load filters */
			/**
			 * Function to checked whether saved search is loading or not
			 */
			$scope.checkforSavedSearches = function(){
				/* if saved search apply saved search */
				if(MipService.getParameterByName("id")){
					$scope.showMipLoader = true; 
					/* get data for saved search */
					var dataParams = {
						searchId: MipService.getParameterByName("id"),
						token: userId
					};
					MipApiService.findSearchHistoryById(dataParams).then(function(response){
						updatefiltersAndSearch(response.data, "update")
					}, function(error){
						$scope.showMipLoader = false; 
					});
				}else if(MipService.getParameterByName("searchSting")){
					var str = MipService.getParameterByName("searchSting");
					var key = (str.split(":")[0]).trim();
					var data = {}; 
						data[key] = [(str.split(":")[1]).trim()];
					var r = {};  r.data = encodeURIComponent(JSON.stringify(data));
					updatefiltersAndSearch(r);
				}
			};
			/**
			 * Function to update search and filters
			 */
			function updatefiltersAndSearch(r, update){
				
				data_default = JSON.parse(decodeURIComponent(r.data));
				$scope.showMipLoader = false;
		   		
		   		angular.forEach(JSON.parse(decodeURIComponent(r.data)), function(val, i){
		   			if(val.length > 0){
		   				angular.forEach(val, function(valInner){
		   					filtersApplied.push(i + "->" + valInner)
		   				});
		   			}
		   		});
		   		$(".mipfiltersul").empty();
			    $(".mipfiltersul").append("<li><span class='text-uppercase'>Displaying</span></li>");	    	   
		   		
		    	if(filtersApplied.length>0){
		    		angular.forEach(filtersApplied, function (d, i) {
		   		        $(".mipfiltersul").append('<li><a class="text-uppercase filter-button" href="javascript:void(0);">  <span>' + d + '</span><button type="button" class="closeFilter"><i class="fa fa-times"></i></button> </a></li>');
		   		    });
		   		}
		    	/* update saved data */
		    	if(update)
				  $scope.saveSearchData(r.name, "fromload", r.searchId);
		    	  $scope.loadmipdata(data_default);
			}
		    /**
			 * Function to plot charts
			 */
			function plotCharts(data, mainData) {
				$scope.topPersons = mainData['top-persons'];
		   		$scope.topSectors = mainData['top-sectors'];
		   		$scope.topPlayers = mainData['top-players'];
		   		$scope.topTechs = mainData['top-technologies'];
		   		$scope.topInvestement = mainData['top-investement'];
		   		$scope.transactionsByInvestment = mainData['transactions-by-investments'];   		
//		   		$(".applymscroll").mCustomScrollbar({   
//					axis: "y",          
//					theme: "minimal-dark"  
//				});
				var datanew = {};
				angular.forEach(data, function(d, i){
					datanew[i] = {
						id: i,
						val: d,
						title: i.replace("by-","").split("-").join(" ")
					};
				});
				datanew['by-investmentStatus'] = {
					id: 'by-investmentStatus',
					val: [{doc_count:897,key:"DELOITTE VENTURES"},{doc_count:797,key:"ASSIST FINCORP"},{doc_count:997,key:"CHASE MORGAN"},{doc_count:697,key:"FILIP VENTURE"}],
					title: "investmentStatus"
				}
				$scope.facetsPieData = {
					"entities": datanew['by-ownershipStatus'],
					"economic": datanew['by-numberOfEmployees'],				
					"type": datanew['by-businessStatus'],
					"REGIONS": datanew['by-location']
				};
				$scope.facetsBarData = {
					"FocusedSectors":datanew['by-products'],
					"investments":datanew['by-investmentStatus']
				};
				
				var focusedSectormax = d3.max($scope.facetsBarData.FocusedSectors.val,function(d){
					return d.doc_count;
				})
				$.map($scope.facetsBarData.FocusedSectors.val,function(d, i){
					d.pct = (d.doc_count/focusedSectormax)*100;
					return d.pctInt = parseInt(d.pct);
				});
				var investorsmax = d3.max($scope.facetsBarData.investments.val,function(d){
					return d.doc_count;
				})
				$.map($scope.facetsBarData.investments.val,function(d, i){
					d.pct = (d.doc_count/investorsmax)*100;
					return d.pctInt = parseInt(d.pct);
				});
				setTimeout(function(){
					
				    /*Here your view content is fully loaded !! */
					angular.forEach($scope.facetsPieData,function(d, i){
						$scope.InitializeandPlotPie(d.val, d.id);
					});
					$(".result-graph-wrapper-main").mCustomScrollbar({   
				    	axis: "y",          
				    	theme: "minimal-dark"  
		    		});
//					angular.forEach($scope.facetsBarData,function(d, i){
//						MipService.InitializeandPlotBar(d.val, d.id, i);
//					});
					 var colors = d3_v3.scale.ordinal().range(["#1DBF6A", "#DD4040", "#3990CF"]);
					 var bombMap =   new Datamap({
					        element: document.getElementById('locationTrendMap'),
					        scope: 'world',			       
					        fills: {
					            'red': '#884BA2',
					            'green': '#54A063',
					            'blue': '#2CBCFA',
					            'seablue': '#37FEFF',
					            defaultFill: '#334851'
					        },
					        geographyConfig: {			           
					            highlightFillColor: '#1b2735',			            
					            highlightBorderOpacity: 0
					        },
					        bubblesConfig: {
					            borderWidth: 0,
					            borderOpacity: 0,
					            borderColor: 'transparent',

					        },
					         done: function (datamap) {
					            datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
					            function redraw() {
					                datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
					            }
					        }

					    });
					bombsData = mainData['location-trends'];
					MipService.plotCirclesOnMap(bombMap, "#locationTrendMap", bombsData);
					var investmentTrendChartData = mainData['investment-trends'];
					
					investmentTrendChartData.map(function(d){
						d.x = new Date(d.date);
						d.value = d.sum;
						return d.time = d.date;
					});
					 var lineData =[{
						"key": "",
						"values": mainData['investment-trends']
					 }];
					 
					 var options = {
					    container:"#investmentTrendChart",
					    height: '140',
					    axisX:true,
					    axisY:true,
					    gridy:true ,
					    data:lineData,
					    actualData:lineData[0].values,
					    strokeWidth:"1px"
					 };
					 new InitializeandPlotLine(options);
					 var growthlineData =[{
							"key": "",
							"values": mainData['growth-trend']
					 }];
					 /*load small line chart on top left corner*/
					 var options = {
					    container:"#trendsChart",
					    height: '40',
					    axisX:false,
					    axisY:false,
					    gridy:false ,
					    data:lineData,
					    actualData:lineData[0].values,
					    marginTop:0,
					    marginBottom:0,
						marginRight:10,
						marginLeft:10,
						strokeWidth:"1px"
					 };
					 InitializeandPlotLine(options);
				 },0);
			};
			/**
			 * Function to plot pie
			 */
			 $scope.InitializeandPlotPie= function(data, id) {
			    data.map(function (d) {
			        return d.value = d.doc_count;
			    });
			    var newData = [];
			    var keys = [];
			    angular.forEach(data, function(d, i){
			    	if(d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(),keys)== -1){
			    		newData.push(d);
			    		keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
			    	}
			    });
			    var colors = ["#5d96c8" , "#b753cd" , "#69ca6b" , "#69ca6b","#c1bd4f" , "#db3f8d" , "#669900"]
			    var options = {
			        container: "#" + id,
			        data: newData,
			        height:140,
			        colors:colors,
			        islegends:true,
			        islegendleft:true,
			        legendwidth:50,
			        page:"mip"

			    };
			    setTimeout(function () {
			        new reusablePie(options);
			    });
			};
			/**
			 * Function to apply filters frombar charts
			 */
			$scope.applyFilters =function(currentElement,id){
				
				 refresh(currentElement.key,id )
	               
			}
			 
			 /**
			  * Function to update map chart
			  */
			 window.updateMapChart = function(s, x, data, options, bombMap, mapid){
				 var sx = s.map(x.invert);
		         var x0 = parseInt(sx[0].getTime());
		         var x1 = parseInt(sx[1].getTime());

		         var newData = [];         
		         angular.forEach(data, function (d, i) {
		             if ((new Date(d.time) >= x0 && new Date(d.time) <= x1)) {
		            	 newData.push(d);
		             }
		         });
		         $(mapid).find(".datamaps-bubble").remove();
		         MipService.plotCirclesOnMap(bombMap, mapid, newData, "update")
			 }
			/**
			 * Function to refresh when filters applied
			 */
			window.refresh = function(val, divid, isremove) {
			    var type = divid.split("by-")[1];
			    if (isremove) {
			        var indx = filtersApplied.indexOf(type + "->" + val);
			        if (indx > -1) {
			            filtersApplied.splice(indx, 1);
			        }
			        var indx1 = data_default[type].indexOf(val);
			        if (indx1 > -1) {
			            data_default[type].splice(indx1, 1);
			        }
			    } else {
			        if ($.inArray(type + "->" + val, filtersApplied) == -1) {
			            filtersApplied.push(type + "->" + val);
			            if (!data_default[type]) {
			                data_default[type] = [];
			            }
			            data_default[type].push(val)
			        }
			    }

			    $(".mipfiltersul").empty();
			    $(".mipfiltersul").append("<li><span class='text-uppercase'>Displaying</span></li>")			    
			    angular.forEach(filtersApplied, function (d, i) {
			        $(".mipfiltersul").append('<li><a class="text-uppercase filter-button" href="javascript:void(0);">  <span>' + d + '</span><button type="button" class="closeFilter"><i class="fa fa-times"></i></button> </a></li>');
			    });
			    if(filtersApplied.length ==1){
			    	goToByScroll(".search-results-datatable")
			    }
			    $scope.loadmipdata(data_default);
			}
			//----------------------------------------------------------------
			/**
			 * Function to scroll page to particular div
			 */
			function goToByScroll(idOrClass){			    
			      // Scroll
			    $('html,body').animate({
			        scrollTop: $(idOrClass).offset().top},
			        'slow');
			}

			/**
			 * remove filter
			 */
			$("body").on("click", ".closeFilter", function () {
			    var curenttxt = ($(this).closest("a").text().trim().split("->"));
			    refresh(curenttxt[1], "by-" + curenttxt[0], true);
			});
			
			$(window).on("resize", function () {
				angular.forEach($scope.facetsPieData,function(d, i){
					$scope.InitializeandPlotPie(d.val, d.id);
				});
		    });
			/**
			 * Function to load the select view
			 */
			$scope.loadSelectedView = function(countryname, event){
				if(countryname){
					$scope.selectedCountry = countryname;
				}
				if(event){
					$(event.target).parent().parent().parent().find("li").removeClass("active");
					$(event.target).parent().parent().addClass('active');
				}
				if($scope.mipselect_view_type_value == "list"){
					/* no need to do anything as we are already loading the table */
				}else if($scope.mipselect_view_type_value =="networChart"){
					/* load network chart if not loaded */
					if($("#networkGraphMip").find("svg").length ==0){
						/* load mip chart */
						var options = {
			                url: "../vendor/data/mipnetworkMainData.json",
			                id: "networkGraphMip",
			                timelineid: "timeline_new_bs_mip"
			            };
						  $timeout(function(){
						$scope.loadDataAndPlotGraphForMIP(options);  },10);
					}
				}else if($scope.mipselect_view_type_value == "barChart"){
					  $timeout(function(){
						  $scope.loadDataAndPlotbarGraphForMIP();
					  },10);
				}else if($scope.mipselect_view_type_value == "mapChart"){
					if($("#mapChartMip").find("svg").length ==0){
						$timeout(function(){
							$scope.loadDataAndPlotMapGraphForMIP($scope.Mipcountries[0])
						},10);
					}
				}
			};
			/**
			 * Function to load data for network chart
			 */
			 $scope.loadDataAndPlotGraphForMIP = function(options){
			    $.ajax({
	               url: options.url,
	               success: function (r) {
	            	  var data_new = createGraphData(r);
	            	  handleGraphData(data_new, options);
	               },
	               error: function () {
	               }
			    });
		        function handleGraphData(data, options) {
				      var finalData = {};
				      var edges = [];
				      var nodeIDS = [];
				      var nodes = [];

				      angular.forEach(data.vertices, function ( val,i) {
				          nodeIDS.push(val.id)
				          nodes.push({
				              data: val
				          });
				      });
				      angular.forEach(data.edges, function ( val,i) {
				          if ($.inArray(val.from, nodeIDS) != -1 && $.inArray(val.to, nodeIDS) != -1)
				              edges.push({
				                  data: {
				                      source: val.from,
				                      target: val.to,
				                      labelE: val.labelE,
				                      id: val.id
				                  }});
				      });
				      finalData.nodes = nodes;
				      finalData.edges = edges

				      loadNetworkChart(finalData, options);
				      loadBrush(data.vertices, options, finalData);
				 }
			}
			
			/**
			 * Function to load data for network chart
			 */
			 $scope.loadDataAndPlotbarGraphForMIP = function(options){
				   var data = $scope.resultData1.slice(0,100);
				   var finalBarData = [];
				   var types = ["public","private"]
				   angular.forEach(data,function(d, i){
					   if(d.name){
						 finalBarData.push({
						   "x": d.name,
						   "y": (Math.floor(Math.random() * 1000))+1,
						   "type": types[(Math.floor(Math.random() * 1))]
						 })
					   }
				   });
			       var options = {
		    			id: "#barGraphMip",
		    			height: 300
			       };
			       new verticalBarGraph(finalBarData, options);
			 }
			 /**
			  * Function to create graph data 
			  */
			 function createGraphData(data){
				 var currentData = [];
					var currentMappingData = [];
					 if(!data_default['products'] || data_default['products'].length == 0){
						 currentMappingData = data['mapping']['Marketing'];
					 }else{
						 angular.forEach(data_default['products'],function(d1,i1){
							 angular.forEach(data['mapping'][d1],function(d2,i2){
								 currentMappingData.push(d2);
							 });
						 });
					 }
					
					 var indexs = MipService.getNRandomIndex(currentMappingData,2);
					 var currentDomains = [];
					 angular.forEach(indexs,function(d,i){
						 currentDomains.push(currentMappingData[d]);
					 });
					 
//					 currentDomains =currentMappingData 
					 var vertices = [],edges = [];
					 angular.forEach(currentDomains,function(d,i){
						var mainVertices = {
								"name":d,
								"id":"vertice_"+i,
								"labelV":d,
								"weight":10,
								"mainNode":true
						}
						vertices.push(mainVertices);
						angular.forEach(data['data'][d],function(d1,i1){
							edges.push({
								"id":"edge_"+(edges.length),
								"labelE":"edge_"+d,
								"from":"vertice_"+i,
								"to":"vertice_"+(vertices.length+currentDomains.length)					
							});
							vertices.push({
								"name":d1,
								"id":"vertice_"+(vertices.length+currentDomains.length),
								"labelV":d,
								"weight": (Math.floor(Math.random() * 10))+1,//to have minimum 1
								"time": MipService.randomDate(new Date(2015, 0, 1), new Date())
							});
						});
					 });
					 var finalData = {
					    "vertices": vertices,
					    "edges":edges
					 };
					
					 return finalData;
			 }
			 /**
			  * Function to view by map
			  */
			 var country; var leafletMapMIP
			 $scope.loadDataAndPlotMapGraphForMIP = function(countryData, event){
				 if(event){
					 $(event.target).parent().parent().parent().find("li").removeClass("active");
						$(event.target).parent().parent().addClass('active');
					}
				 if(countryData.id){
					 country = countryData.id;
					 $scope.selectedCountryMapChart = countryData.name;
					
				 }else{
					 country = countryData;
				 }
//				 $("#mapChartMip").empty();
				 //code for changing map starts here
				 var zoomOptions= {
					   minZoom: 2,
					   zoom: 2
				   };
				
				 if(country =="world"){
					 $("#mapChartMip").empty();
					 if(leafletMapMIP){
						 leafletMapMIP.remove();
					 }
					 leafletMapMIP= MipService.setWorldMap(countryData.data, zoomOptions);
				 }else{
					 if(countryData.latlon)
					 leafletMapMIP.setView(countryData.latlon, 4.5)
				 }
				 
				//code for changing map ends here
				 return;
				 $("#mapChartMip").empty();
				 var bomb_map;
				 if(country == "usa" || country =="world"){
					  bomb_map =  new Datamap({
					        element: document.getElementById("mapChartMip"),
					        scope: country,
					        geographyConfig: {			           
					            highlightFillColor: '#1b2735',			            
					            highlightBorderOpacity: 0
					        },
					        fills: {
					            'red': '#884BA2',
					            'green': '#54A063',
					            'blue': '#2CBCFA',
					            'seablue': '#37FEFF',
					            defaultFill: '#334851'
					        },
					        bubblesConfig: {
					            borderWidth: 0,
					            borderOpacity: 0,
					            borderColor: 'transparent',

					        },
					        done: function (datamap) {
					            datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
					            function redraw() {
					                datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
					            }
					        }
					    });
				 }else{
					 bomb_map =  new Datamap({
					        element: document.getElementById("mapChartMip"),
					        scope: country,
					        setProjection:countryProjections[country],
					        geographyConfig: {			           
					            highlightFillColor: '#1b2735',			            
					            highlightBorderOpacity: 0
					        },
					        fills: {
					            'red': '#884BA2',
					            'green': '#54A063',
					            'blue': '#2CBCFA',
					            'seablue': '#37FEFF',
					            defaultFill: '#334851'
					        },
					        bubblesConfig: {
					            borderWidth: 0,
					            borderOpacity: 0,
					            borderColor: 'transparent',

					        },
					        done: function (datamap) {
					            datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
					            function redraw() {
					                datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
					            }
					        }
					    });
					 
				 }
				 MipService.plotCirclesOnMap(bomb_map, "#mapChartMip", countryData.data);
			 }
			 /*
			  *@purpose: load MIP Entity modal
			  *@created: 18 Oct 2017
			  *@params: null
			  *@returns: none
			  *@author: swathi
			  */
			 $scope.showMipSaveModal = function(){
				 if(MipService.getParameterByName("id")){
					  $scope.saveSearchData($("#SearchTxtSpan").text(),"fromload", MipService.getParameterByName("id"));
				 }else{
					 var openMipSaveModal = $uibModal.open({
			            templateUrl: './modal/views/mip.save.modal.html',
			            controller: 'MipSaveController',
			            size: 'sm',
			            backdrop: 'static',
			            windowClass: 'custom-modal mip-save-modal mip-save-wrapper',
			            resolve: {
							mipSaveModalData: function(){
								return data_default;
							}
						}
			        });

				 	openMipSaveModal.result.then(function (response) {
				 		
			        }, function (reject) {
			        }); 
				 }		
			 };
			 /**
			  * function to save search
			  */
			 $scope.saveSearchData = function(name, isfromload, searchId){	
				 var data = {
					 "name": name,
					 "data": encodeURIComponent(JSON.stringify(data_default)),
					 "sourcePage": "mip"
				 };
				 data = JSON.stringify(data);
				 $("#SearchTxtSpan").text(name);
				 $("#curentSearchName").find("span").addClass("fa fa-times");
				 $scope.showMipLoader = true;
				 
				  var url = "mip/search/saveSearchHistory",
	  				  params = {
						  token: userId
			  		  };
				  if(isfromload){
						var url = "mip/search/updateSearchHistory",
						params = {
						  token: userId,
						  searchId: searchId
			  		  	};
						data = {
							 "name": name,
							 "data": encodeURIComponent(JSON.stringify(data_default)),
							 "sourcePage": "mip",
							 "searchId": searchId
						 };
					 }
				  MipApiService.saveOrUpdateSearchHistory(data, params, url).then(function(response){
					  $scope.showMipLoader = false;
					  if(!isfromload){
						 
					  }
				  }, function(error){
					  $scope.showMipLoader = false;
				  });
			 };
			 /**
			  * Function to remove search
			  */
			$scope.closeMipSearch = function(){
				 $("#curentSearchName").find("span").removeClass("fa fa-times");
				 var url = window.location.href.split('?')[0];
		         history.pushState({}, null, url);
			};
			/* navigate to EntityCompany page */
			$scope.navigateToEntityCompany = function(name){
				var indexOfbackSlash = window.location.href.split("/#!/")[0].lastIndexOf('/');
				var url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/entity/#!/company/"+name;
				window.open(url, '_blank');
			};
			
			/**
			 * Function to update network chart when brushed
			 */
			 window.updateNetworkChart = function(s,x,finalData,options){
		  	   var sx = s.map(x.invert);
		         var x0 = parseInt(sx[0].getTime());
		         var x1 = parseInt(sx[1].getTime());

		         var newNodes = [], newEdges = [];
		         var newNodeIDS = [];
		         var newfinalData = {};
		         angular.forEach(finalData.nodes, function (d,i) {
		             if ((new Date(d.data.time) >= x0 && new Date(d.data.time) <= x1) || (d.data.mainNode)) {
		                 newNodes.push(d);
		                 newNodeIDS.push(d.data.id)
		             }
		         });
		         angular.forEach(finalData.edges, function (val,i) {
		             if ($.inArray(val.data.source, newNodeIDS) != -1 && $.inArray(val.data.target, newNodeIDS) != -1)
		                 newEdges.push({
		                     data: {
		                         source: val.data.source,
		                         target: val.data.target,
		                         labelE: val.data.labelE,
		                         id: val.data.id
		                     }});
		         });
		         newfinalData.nodes = newNodes;
		         newfinalData.edges = newEdges

		         new loadNetworkChart(newfinalData,options);
		     }
}