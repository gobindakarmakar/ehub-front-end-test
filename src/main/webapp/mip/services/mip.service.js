'use strict';
angular.module('ehubMipApp')
	   .service('MipService', mipService);

		mipService.$inject = [
			'$location'
		];
		
		function mipService(
				$location) {
			
			
			var bombsData = [];
			var myService = this;
			
			myService.substringMatcher = function(strs) {
			  	  return function findMatches(q, cb) {
			  		  	var matches, substringRegex;
			  		   /* an array that will be populated with substring matches */
				  	    matches = [];
			
				  	    /* regex used to determine if a string contains the substring `q` */
				  	    var substrRegex = new RegExp(q, 'i');
			
				  	    /* iterate through the pool of strings and for any string that
				  	       contains the substring `q`, add it to the `matches` array */
				  	    angular.forEach(strs, function(str, i) {
					  	     if (substrRegex.test(str)) {
					  	        matches.push(str);
					  	     }
				  	    });
				  	    cb(matches);
			  	  };
		  	};
		  	/* get Parameter By Name */
		  	myService.getParameterByName = function(name, url) {
		        if (!url) {
		            url = window.location.href;
		        }
		        name = name.replace(/[\[\]]/g, "\\$&");
		        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		                results = regex.exec(url);
		        if (!results)
		            return null;
		        if (!results[2])
		            return '';
		        return decodeURIComponent(results[2].replace(/\+/g, " "));
		    };
			/* Initialize and plot bar chart */
		    myService.InitializeandPlotBar = function(data, id, index) {
			    data.map(function (d) {
			    	 if(id == "by-primary-business-sector"){
						 d.txtToShow = (d.doc_count);
					 }else if(id == "by-business-targets"){
						 d.txtToShow = abbreviateNumber(d.doc_count)
					 }
			        return d.count = d.doc_count;
			    });
				setTimeout(function(){
					barChart(data, id, index);
				});
				$(".result-graph-wrapper-main").mCustomScrollbar({   
			    	axis: "y",          
			    	theme: "minimal-dark"  
	    		});
			};
			/* abbreviate Number */
			function abbreviateNumber(value) {
			    var newValue = value;
			    if (value >= 1000) {
			        var suffixes = ["", "k", "m", "b","t"];
			        var suffixNum = Math.floor( (""+value).length/3 );
			        var shortValue = '';
			        for (var precision = 2; precision >= 1; precision--) {
			            shortValue = parseFloat( (suffixNum != 0 ? (value / Math.pow(1000,suffixNum) ) : value).toPrecision(precision));
			            var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g,'');
			            if (dotLessShortValue.length <= 2) { break; }
			        }
			        if (shortValue % 1 != 0)  shortNum = shortValue.toFixed(1);
			        newValue = shortValue+suffixes[suffixNum];
			    }
			    return newValue;
			}
			/**
			 * Function to plot circles on map
			 */
			myService.plotCirclesOnMap = function(bombMap, id, data, update){
				 //plotting circles on map
				 var currentData = [];
				 if(data){
					 currentData = data;
				}else{
					currentData = bombsData;
				}
				 var bombScale = d3.scaleLinear().domain(d3.extent(currentData,function(d){return d.count})).range([5,17])
				
				 if(currentData.length>0){
					 if(!update){
						 currentData.map(function(d){
							if(data){
								d.count = (Math.floor(Math.random() * 150))+1
							}
							 if(d.count<20){
								 d.fillKey = "green";
							 }else if(d.count<80){
								 d.fillKey = "seablue";
							 }else if(d.count<130){
								 d.fillKey = "blue";
							 }else {
								 d.fillKey = "red";
							 }				 
							 d.time  = myService.randomDate(new Date(2007, 0, 1), new Date());
							 d.weight = d.count;
							 return d.radius = bombScale(d.count);
						 })
						 if(data){
						 var options = {
							              
							                timelineid: "timeline_new_bs_mip_map"
							            }
						
						 loadBrush(currentData,options,"",bombMap,id);
						 }
				 }
					 //circles on world map
					 var bombs = currentData;
				//draw bubbles for bombs
				   bombMap.bubbles(bombs, {
				       popupTemplate: function (geo, d) {
				    	   if(d.type){
				    		   return ["<div class='tooltip_datamap_all_data text-uppercase'><div style='color:white;margin-bottom:15px'><h5>"+(d.location)+"</h5></div><div style='margin-bottom:15px'>Funding Received <h4 style='color:#3372A8;margin-top:0'>"+d.count+"BN</h4></div><div style='margin-bottom:15px'>Type <h6 style='color:#3372A8;margin-top:0'>"+d.type+"</h6></div></div>"].join('');
				    	   }else{
				    		   return ['<div class="hoverinfo">Investments: ' + d.count,
					               '</div>'].join('');
				    	   }   	 
				          
				       }
				   });
				   d3.select(id).selectAll(".datamaps-bubble").attr("r",function(d){		    	
				   	return d.radius
				   }).style("fill-opacity",0.5);
				 }
			};
			 /*======================sTART Mocking Trend Chart DATa=====================*/
			 /**
			  * Function to give random date
			  */
			myService.randomDate = function(start, end) {
			    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
			 };
			 /**
			  * Function to get random index
			  */
			 myService.getNRandomIndex = function(arr, n, indexArr){
				 if(!indexArr){
					 indexArr = [];
				 }
				 var index = Math.floor(Math.random() * arr.length);
				 if($.inArray(index,indexArr) == -1){
					 indexArr.push(index);
				 }
				 if(indexArr.length == n){
					 return  indexArr;
				 }else{
					return this.getNRandomIndex(arr, n, indexArr) 
				 }
			 };
			 /**
			 * Function to plot circles on map
			 */
			myService.setWorldMap = function(locationsData, zoomOptions){
				console.log(locationsData)
				var map = L.map( 'mapChartMip', {
			        center: [20.0, 5.0],
			        minZoom: zoomOptions.minZoom,
			        zoom: zoomOptions.zoom
			    });
				L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/hybrid.day/{z}/{x}/{y}/{size}/{format}?app_id=PnLGQmyD8SUwZws9DsaV&app_code=1bD_gbm5ZtDhYv0WUqwIAg&lg={language}', {
					attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
					subdomains: '1234',
					mapID: 'newest',
					app_id: 'PnLGQmyD8SUwZws9DsaV',
					app_code: '1bD_gbm5ZtDhYv0WUqwIAg',
					base: 'aerial',
					maxZoom: 20,
					type: 'maptile',
					language: 'eng',
					format: 'png8',
					size: '256'
				}).addTo( map );
				angular.forEach(locationsData,function(val,key){
					 var markersData = {
				            	"name": val.location,
				                "url": "https://en.wikipedia.org/wiki/" + val.location,
				                "lat":val.latitude,
				                "lng": val.longitude
				                };
				            L.marker( [markersData.lat, markersData.lng] )
					          .bindPopup( '<a href="' + markersData.url + '" target="_blank">' + markersData.name + '</a>' )
					          .addTo( map );
				})
				return map;
			}
		}
		
		
		
