'use strict';
angular.module('ehubMipApp')
		.factory('MipApiService', mipApiService);

		mipApiService.$inject = [
			'$http',
			'$rootScope',
			'$q',
			'EHUB_API',
			'EHUB_FE_API'
		];
		
		function mipApiService(
				$http,
				$rootScope,
				$q,
				EHUB_API,
				EHUB_FE_API){
			
			return {
				getAllSavedSearches: getAllSavedSearches,
				searchOrgWithFacts: searchOrgWithFacts,
				searchEntity: searchEntity,
				findSearchHistoryById: findSearchHistoryById,
				saveSearchHistory: saveSearchHistory,
				saveOrUpdateSearchHistory: saveOrUpdateSearchHistory
			};
			
			/*
		     * @purpose: get all saved searches
		     * @created: 17 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllSavedSearches(params){
				var apiUrl =  EHUB_API + 'mip/search/getSearchHistoryList';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getAllSavedSearchesSuccess)
		                .catch(getAllSavedSearchesError));
		
		        /*getAllSavedSearches error function*/
		        function getAllSavedSearchesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getAllSavedSearches success function*/
		        function getAllSavedSearchesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: search org with facts
		     * @created: 17 oct 2017
		     * @params: params(object), data(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function searchOrgWithFacts(params, data){
				var apiUrl =  EHUB_API + 'marketIntelligence/search/orgWithFacts';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(searchOrgWithFactsSuccess)
		                .catch(searchOrgWithFactsError));
		
		        /*searchOrgWithFacts error function*/
		        function searchOrgWithFactsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*searchOrgWithFacts success function*/
		        function searchOrgWithFactsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: search entity
		     * @created: 17 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function searchEntity(params){
				var apiUrl =  EHUB_API + 'marketIntelligence/entity/search';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(searchEntitySuccess)
		                .catch(searchEntityError));
		
		        /*searchEntity error function*/
		        function searchEntityError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*searchEntity success function*/
		        function searchEntitySuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: find search history by id
		     * @created: 17 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function findSearchHistoryById(params){
				var apiUrl =  EHUB_API + 'mip/search/findSearchHistoryById';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(findSearchHistoryByIdSuccess)
		                .catch(findSearchHistoryByIdError));
		
		        /*findSearchHistoryById error function*/
		        function findSearchHistoryByIdError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*findSearchHistoryById success function*/
		        function findSearchHistoryByIdSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: save search history
		     * @created: 18 oct 2017
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function saveSearchHistory(data, params){
				var apiUrl =  EHUB_API + "mip/search/saveSearchHistory";
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(saveSearchHistorySuccess)
		                .catch(saveSearchHistoryError));
		
		        /*saveSearchHistory error function*/
		        function saveSearchHistoryError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*saveSearchHistory success function*/
		        function saveSearchHistorySuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: save or update search history
		     * @created: 18 oct 2017
		     * @params: data(object), params(object), url(string)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function saveOrUpdateSearchHistory(data, params, url){
				var apiUrl =  EHUB_API + url;
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(saveOrUpdateSearchHistorySuccess)
		                .catch(saveOrUpdateSearchHistoryError));
		
		        /*saveOrUpdateSearchHistory error function*/
		        function saveOrUpdateSearchHistoryError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*saveOrUpdateSearchHistory success function*/
		        function saveOrUpdateSearchHistorySuccess(response) {
		            return(response);
		        }
			}
	};