<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>

<!--  Main Wrapper Starts  -->
<span class="custom-spinner showMipLoader" ng-show="showMipLoader">
 	<i class="fa fa-spinner fa-spin fa-2x"></I>
 </span>
<div class="main-wrapper mip-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="custom-mip-ui">
			<ul class="collapse-menu list-unstyled">
				<li class="filters active">
					<h5 class="usrfilters toggle text-uppercase hidden">
					  	<a class="editFiltersUI" href="javascript:void(0);">Edit</a>
				    </h5>
					<div class="treeview-menu main-content-wrapper"
						style="display: block;">
						<div class="search-results-row clearfix">
							<div class="search-results-right-col">
								<div class="top-filters-wrapper">
									<ul class="top-filter-navigation list-inline">
										<li>
											<div class="top-filters text-uppercase">
												<h4 class="top-filter-heading">
													Trends
												</h4>
												<p class="text-uppercase">Growth trend</p>
												<div class="trends-line-chart" id="trendsChart">
													
												</div>
											</div>
										</li>
										<li>
											<div class="top-filters transactions text-uppercase">
												<h4 class="top-filter-heading">
													Transactions
												</h4>
												<p class="text-uppercase">By Investments</p>
												<strong class="">$ {{ transactionsByInvestment }}BN</strong>
											</div>
										</li>
										<li>
											<div class="top-filters investments text-uppercase ">
												<h4 class="top-filter-heading">
													Top Investment
												</h4>
												<p class="text-uppercase">{{ topInvestement[0] }}</p>
												<strong class="">$ {{ topInvestement[1] }}BN</strong>
											</div>
										</li>
										<li class="applymscroll">
											<div class="top-filters text-uppercase ">
												<h4 class="top-filter-heading">
													Top Sectors
												</h4>
												<div class="top-filter-list">
													<ul class="filtered-list list-unstyled">
														<li style="float:none" ng-repeat= "sector in topSectors"><span>{{ sector }}</span></li>
														
													</ul>
												</div>
											</div>
										</li>
										<li class="applymscroll">
											<div class="top-filters text-uppercase ">
												<h4 class="top-filter-heading">
													Top Players
												</h4>
												<div class="top-filter-list">
													<ul class="filtered-list blue-list  list-unstyled">
														<li style="float:none" ng-repeat= "player in topPlayers"><span>{{ player }}</span></li>
														
													</ul>
												</div>
											</div>
										</li>
										
										<li class="applymscroll">
											<div class="top-filters text-uppercase ">
												<h4 class="top-filter-heading">
													Top Technologies
												</h4>
												<div class="top-filter-list">
													<ul class="filtered-list list-unstyled">
														<li style="float:none" ng-repeat= "tech in topTechs"><span>{{ tech }}</span></li>
														
													</ul>
												</div>
											</div>
										</li>
										<li class="applymscroll">
											<div class="top-filters text-uppercase ">
												<h4 class="top-filter-heading">
													Top Persons
												</h4>
												<div class="top-filter-list">
													<ul class="filtered-list list-unstyled">
														<li style="float:none" ng-repeat= "person in topPersons"><span>{{ person }}</span></li>
														
													</ul>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="focused-sectors-content">
									<ul class="focused-sectors-navigation list-inline text-uppercase">
										<li>
											<div class="focused-sector-list">
												<h4>focused Sector</h4>
												<div class="result-graph-wrapper-main">
													<div class="focused-bars-wrapper">
														<div class="riskscoreprogress">
															<div class="risk-progressbar-holder" ng-repeat="sector in facetsBarData.FocusedSectors.val">
																<div class="progress-wrapper" title="{{ sector.key }} : {{ sector.pctInt  }}%">
																	<div class="inline">{{ sector.key }}</div>
																	<div class="progress" ng-click="applyFilters(sector,facetsBarData.FocusedSectors.id)">
																		<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ sector.pct }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ sector.pctInt  }}%">
																			
																		</div>
																	</div>
																</div>
															</div>				
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="focused-sector-list">
												<h4>Regions</h4>
												<div class="result-graph-wrapper-main">
													<div class="" id="by-location">
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="focused-sector-list">
												<h4>By Events</h4>
												<div class="events-sector-wrapper event-list-wrapper">
													<select class="" ng-change="loadmipdata()" ng-model="eventsMipSelect">
														<option>Investment</option>
														<option>Location</option>
														<option>Products</option>
													</select>
												</div>
												<h4 class="timelineDatepicker">Time Line</h4>
												<div class="events-sector-wrapper event-list-wrapper">
													<div class='col-sm-6 pad-lt-0'>
														<div class="date-picker">
										                	<input ng-model="MipdatePicker1" ng-change="loadmipdata()" id="datePicker1" placeholder="From" type='text' class="custom-date-input form-control" />
										                	<span class="fa fa-chevron-down"></span>
									                	</div>	              
												    </div>
												    <div class='col-sm-6 pad-rt-0'>
												    	<div class="date-picker">
										                	<input ng-model="MipdatePicker2" ng-change="loadmipdata()" id="datePicker2" placeholder="To" type='text' class="custom-date-input form-control" />
										                	<span class="fa fa-chevron-down"></span>
										                </div>												              
												    </div>
												</div>
<!-- 												<div class="toggle-switch-timeline"> -->
<!-- 													<i ng-click="toggleSwitch()" ng-class="{'fa fa-toggle-on': isSwitchStatus, 'fa fa-toggle-off': !isSwitchStatus}"></i> -->
<!-- 													<span>Show timeline</span> -->
<!-- 												</div> -->
											</div>
										</li>
										<li>
											<div class="focused-sector-list">
												<h4>Entities</h4>
												<div class="result-graph-wrapper-main">
													<div class="" id="by-ownershipStatus">
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="focused-sector-list">
												<h4>Types</h4>
												<div class="result-graph-wrapper-main">
													<div class="" id="by-businessStatus">
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="toggle-sectors-content">
									<ul class="toggle-sectors-navigation list-inline text-uppercase">
										<li>
											<div class="toggle-sector-list">
												<h4>Type</h4>
												<div class="result-graph-wrapper-main">
													<div class="" id="by-numberOfEmployees">
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="toggle-sector-list">
												<h4>Investors</h4>
												<div class="result-graph-wrapper-main">
													<div class="investors-bars-wrapper">
														<div class="riskscoreprogress">
														<div class="risk-progressbar-holder" ng-repeat="sector in facetsBarData.investments.val">
																<div class="progress-wrapper" title="{{ sector.key }} : {{ sector.pctInt  }}%">
																	<div class="inline">{{ sector.key }}</div>
																	<div class="progress" ng-click="applyFilters(sector,facetsBarData.investments.id)">
																		<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ sector.pct }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ sector.pctInt  }}%">
																			
																		</div>
																	</div>
																</div>
															</div>				
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="toggle-sector-list toggle-switch-case">
												<h4>Investment Trend</h4>
												<div class="" id="investmentTrendChart">
													
												</div>
<!-- 												<a href="javascript:void(0);" class="toggle-line-arrow"> -->
<!-- 													<i class="fa fa-caret-down"></i> -->
<!-- 												</a> -->
											</div>
										</li>
										<li>
											<div class="toggle-sector-list">
												<h4>Location Trend</h4>
												<div class="datamapsMip" id="locationTrendMap">
													
												</div>
											</div>
										</li>
										<li>
											<div class="toggle-sector-list">
												<h4>Related News</h4>
												<div class="related-news applymscroll" id="relatedNews">
													<ul class="list-unstyled">
														<li ng-repeat="article in articlesMip">
															<div class="related-news-list">
																<p>{{ article.title }}</p>
																<a target="_blank" href="{{ article.url }}">{{ article.published }}</a>
															</div>
														</li>
														
													</ul>
												</div>
											</div>
										</li>
									</ul>
								</div>
<!-- 								<div class="search-results-left-col"> -->
<!-- 									<div class="search-wrapper search-criteria-wrapper container-fluid"> -->
<!-- 										<div class="row"> -->
<!-- 											<div class="col-sm-3 search-filter-wrapper choose-sectors-wrapper" ng-repeat="(key,value) in facetsPieData" > -->
<!-- 												<h4 class="text-uppercase big-filters">{{ key }}</h4> -->
<!-- 												<div ng-init=InitializeandPlotPie(value.val,value.id,$index) id="{{ value.id }}" class="selected-sectors-chart"><div class="open"></div></div> -->
<!-- 											</div>	 -->
<!-- 										</div>		 -->
<!-- 									</div>	 -->
<!-- 								</div> -->
<!-- 								<div class="search-results-graph container-fluid"> -->
<!-- 									<div class="row" id="bargraphsSrcoller"> -->
<!-- 										<div class="bar-scroller-wrapper" ng-repeat="(key,value) in facetsBarData"> -->
<!-- 											<h4 class="text-uppercase small-filters">{{ value.title}}</h4> -->
<!-- 											<div class="result-graph-wrapper-main"> -->
<!-- 												<div ng-init=InitializeandPlotBar(value.val,value.id,$index) class="result-graph-wrapper" id="{{ value.id }}"></div> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
								<div class="search-results-datatable">
									<div class="total-results clearfix">
										<ul class="list-inline mipfiltersul">
											<li>
												<span class="displaying-key text-uppercase">
													Displaying :
												</span>
											</li>
										</ul>
									</div>
									<div class="search-datatable-wrapper">
										<div class="showing-results-wrapper clearfix" >
											<div class="showing-results" ng-show="mipselect_view_type_value == 'list'">
					                            <span class="text-uppercase">
					                            	Showing :
					                                <a href="javascript:void(0);">{{ mipResultRecords }}</a>
					                                Results so far
					                            </span>
					                            <p class="sorting-results">
					                            	<span class="filter-key text-uppercase">Filter By :</span>
						                            <select class="filter-value text-uppercase">
						                                <option>Relevance</option>
						                                <option>Relevance1</option>
						                                <option>Relevance2</option>
						                                <option>Relevance3</option>
						                            </select>
					                            </p>
				                            </div>
				                            <div class="searching-wrapper pull-right">
<!-- 				                            	<div class=""> -->
<!-- 					                            	<input class="" type="text"> -->
<!-- 					                            	<i class="fa fa-search"></i> -->
<!-- 				                            	</div> -->
<!-- 			                            		<button type="button" class="text-uppercase"> -->
<!-- 					                                save search -->
<!-- 					                            </button> -->
<!-- 					                            <button type="button" id='viewPivot' class="text-uppercase"> -->
<!-- 					                                View Pivot -->
<!-- 					                            </button> -->
					                            <select id="mipselect_view_type" ng-change=loadSelectedView() class="text-uppercase" ng-model="mipselect_view_type_value">
												    <option value="list">LIST (DEFAULT)</option>
												    <option value="networChart">Trend Chart</option>
												    <option value="barChart">Bar Chart</option>		
												    <option value="mapChart">Map Chart</option>									  
												</select>
				                            </div>
										</div>
										<div class="main-filtered-content-wrapper">
											<div class="network-graph-wrapper" ng-show="mipselect_view_type_value == 'networChart'">
												<div class="axis-wrapper">
<!-- 													<div class="axis-selector"> -->
<!-- 														<span>AXIS Y</span> -->
<!-- 														<select class="text-uppercase"> -->
<!-- 															<option>location</option> -->
<!-- 															<option>Country</option> -->
<!-- 															<option>Company</option> -->
<!-- 															<option>Sector</option> -->
<!-- 															<option>Investors</option> -->
<!-- 															<option>Entities</option> -->
<!-- 														</select> -->
<!-- 													</div> -->
													<div class="axis-list-wrapper">
														<ul class="list-unstyled text-center">
															<li class="" ng-repeat="country in Mipcountries" ng-class="{'active': country == Mipcountries[0]}" title="{{ country.name }}" ng-click="loadSelectedView(country.name,$event)">
																<a href="javascript:void(0);" class="">
																	<i class="fa fa-circle"></i>
																</a>
															</li>
														</ul>
													</div>
												</div>
												<div class="country-statistics-wrapper country-statistics-wrapper-mip-map-chart">
													<h5 class="country-name text-uppercase">{{ selectedCountry }}</h5>
													<p class="text-uppercase country-fundings"><span>$45BN</span> Fundings on <span>JAN 2015</span></p>
												</div>
												<div class="networkGraphMipMain">
												<div id="networkGraphMip" ></div>
												</div>
												<div id ="timeline_new_bs_mip"></div>
											</div>
											<div class="filtered-company-list" ng-show="mipselect_view_type_value == 'list'">
												<div class="row">
													<div class="col-sm-3" ng-repeat="(key,value) in resultData1">
														<div class="results-tab-wrapper">
															<div class="results-header">
																<h3 class="text-uppercase">{{ value.name }}</h3>
																<p class="text-uppercase result-address">{{ value['domiciled-in']?value['domiciled-in']:"-" }}</p>
																<a href="javascript:void(0);" ng-click="navigateToEntityCompany(value.name)"><i class="fa fa-check-circle "></i></a>
															</div>
															<div class="results-body">
																<ul class="list-unstyled">
																	<li>
																		<span class="result-key text-uppercase">Industry: </span>
																		<span class="result-value text-uppercase">{{ value['primary-industry']?value['primary-industry']:"-" }}</span>
																	</li>
																	<li>
																		<span class="result-key text-uppercase">Capital: </span>
																		<span class="result-value text-uppercase">{{ value['primary-business-sector']?value['primary-business-sector']:"-"  }}</span>
																	</li>
																</ul>
		<!-- 														<strong> -->
		<!-- 															32BN -->
		<!-- 														</strong> -->
															</div>
															<div class="results-footer">
																<a href="{{ value.url?value.url:'-' }}" target="_blank" class="text-uppercase" title="">{{value.url?value.url:"-" }}</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										<div class="bar-graph-wrapper" ng-show="mipselect_view_type_value == 'barChart'">
											<div id="barGraphMip"></div>
											</div>
										</div>
										<div class="map-graph-wrapper" ng-show="mipselect_view_type_value == 'mapChart'">
										<div class="axis-wrapper">
													
													<div class="axis-list-wrapper">
														<ul class="list-unstyled text-center">
															<li  ng-repeat="country in Mipcountries" ng-class="{'active': country == Mipcountries[0]}" title="{{ country.name }}" ng-click="loadDataAndPlotMapGraphForMIP(country,$event)">
																<a href="javascript:void(0);" class="">
																	<i class="fa fa-circle"></i>
																</a>
															</li>
																</ul>
													</div>
													</div>
											<div class="country-statistics-wrapper country-statistics-wrapper-mip-map-chart">
													<h5 class="country-name text-uppercase">{{ selectedCountryMapChart }}</h5>
													<p class="text-uppercase country-fundings"><span>$45BN</span> Fundings on <span>JAN 2015</span></p>
											</div>
											<div id="mapChartMip" class="datamapsMip" style="width:90%;height:450px;margin:auto"></div>
											<div id ="timeline_new_bs_mip_map"></div>
											</div>
										</div>
<!-- 										<div class="table-responsive expertUsersInfodiv"></div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>

		</div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->