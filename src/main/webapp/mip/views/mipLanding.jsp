<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  Main Wrapper Starts  -->
<span class="custom-spinner showMipLoader" ng-show="showMipLandingLoader">
 	<i class="fa fa-spinner fa-spin fa-2x"></I>
 </span>
<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="search-text-wrapper">
             <form class="search-company-form">
                 <div class="search-icon-submit">
               		 	<select class="text-uppercase" ng-model="mipLandingFilter" ng-change="getTypeAHead()">
							<option ng-repeat=" value in mipData" value="{{ value.id }}">{{ value.title }}</option>
						</select>
						<div class="mipTypeHead">
                     <input type="text" id="mipLandingTypeAHead" class="text-center" placeholder="LOOK FOR A COMPANY">
<!--                      <button type="button" ng-click="openSelectedSearch()"> -->
<!--                          <i class="fa fa-search"></i> -->
<!--                      </button> -->
</div>
                 </div>
             </form>  
         </div>
         <div class="search-sector-wrapper">
<!--              <h4 class="text-center text-uppercase">Or pick sectors</h4> -->
             <form class="text-center search-sector-form">
             	<uib-tabset class="searched-sectors-wrapper" justified="true">
				   <uib-tab index="0">
					     <uib-tab-heading class="text-capitalize text-center">
					       SAVED ANALYSES
					     </uib-tab-heading>
					     <div class="container-fluid">
					     	<ul class="row list-inline">
					     		<li class="col-sm-3" ng-repeat="saved in savedList" ng-click="openSelectedSearch(saved)">
									<a class="searched-one-click" href="javascript:void(0);">{{saved.name}}</a>
					     		</li>
					     	</ul>
					     </div>
				   </uib-tab>
             		<uib-tab index="1">
					     <uib-tab-heading class="text-capitalize text-center">
					       RECENT SEARCHES
					     </uib-tab-heading>
				     	 <div class="container-fluid">
					     	<ul class="row list-inline">
					     		<li class="col-sm-3" ng-repeat="recent in recentList" ng-click="openSelectedSearch(recent)">
	<!-- 				     			<div class="checkbox-wrapper"> -->
	<!-- 				     				<div class="custom-checkbox"> -->
	<!-- 				     					<label class="checkbox-inline"> -->
	<!-- 				     						<input class="" id="sector_{{recent}}" type="checkbox"  name="sector_{{recent}}" value=""/> -->
	<!-- 				     						<small>{{recent}}</small> -->
	<!-- 			     						</label> -->
	<!-- 		     						</div> -->
	<!-- 	     						</div> -->
		     						<a class="searched-one-click" href="javascript:void(0);">{{recent.name}}</a>
					     		</li>
					     	</ul>
					      </div>
				   </uib-tab>
				 </uib-tabset>
                 <button  class="custom-sector-submit" ng-click="openSelectedSearch()">
                     <i class="fa fa-chevron-right"></i>
                 </button>
             </form>
         </div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->

<!-- <div ng-controller="chatCollaborationImport"> -->
<!-- 	<div ng-include="chatCollaborationPartial.url"></div> -->
<!-- 	<div ng-include="chatCollaborationChatBoxPartial.url"></div> -->
<!-- </div> -->
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$(".db-util-process-tab-discover").mThumbnailScroller({
				axis : "x"
			});
			$("#bargraphsSrcoller").mThumbnailScroller({
				axis : "x"
			});
		}, 500);
	});
</script>