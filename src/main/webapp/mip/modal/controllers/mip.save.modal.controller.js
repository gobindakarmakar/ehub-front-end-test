'use strict';
angular.module('ehubMipApp')
	   .controller('MipSaveController', mipSaveController);

		mipSaveController.$inject = [
			'$scope',
			'$rootScope',
			'$state',
			'$uibModalInstance',
			'mipSaveModalData',
			'MipApiService',
			'HostPathService',
			'Flash'
		];
		
		function mipSaveController(
				$scope,
				$rootScope,
				$state,
				$uibModalInstance,
				mipSaveModalData,
				MipApiService,
				HostPathService,
				Flash){
			
			var userId = $rootScope.ehubObject;
			$scope.mipSaveData = mipSaveModalData;
			/*
		     * @purpose: save search data
		     * @created: 18 oct 2017
		     * @params: no
		     * @return: null
		     * @author: swathi
		    */ 
			 $scope.saveSearchData = function(){	
				 var data = {
					 "name": $scope.saveName,
					 "data": encodeURIComponent(JSON.stringify($scope.mipSaveData)),
					 "sourcePage" : "mip"
				 };
				 data = JSON.stringify(data);
				 var params = {
					 token: userId.token	 
				 };
				 $scope.showMipSaveLoader = true;
				  MipApiService.saveSearchHistory(data, params).then(function(resposne){
					  if(resposne.data.searchId){
						  $state.go('mip', {id: resposne.data.searchId});
		                  $("#SearchTxtSpan").text($scope.saveName);
	 				 	  $("#curentSearchName").find("span").addClass("fa fa-times");
	                	  HostPathService.FlashSuccessMessage('', 'Saved succesfully');
					  }else {
						  HostPathService.FlashErrorMessage('', 'Unable to Save, Please try again later');
					  }
					  $scope.showMipSaveLoader = false;
					  $uibModalInstance.dismiss('close');
				  }, function(error){
					  $scope.showMipSaveLoader = false;
					  HostPathService.FlashErrorMessage('', 'Something went wrong, Please try again later');
					  $uibModalInstance.dismiss('close');
				  });
			 }
		 	/*
		     * @purpose: close mip save modal
		     * @created: 18 oct 2017
		     * @params: no
		     * @return: null
		     * @author: swathi
		    */ 
			$scope.closeModal = function(){
		 		$uibModalInstance.dismiss('close');
		 	};
		}
				