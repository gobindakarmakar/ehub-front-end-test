<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="bst_element" ng-app="ehubMipApp">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="../assets/images/logo-new.png" />
		<title>MIP</title>
		
		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="./assets/css/mip-vendor.min.css" rel="stylesheet"/>
		<!-------------------------------  Vendor Styles Ends  --------------------------------->
		
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="./assets/css/mip-styles.min.css" rel="stylesheet"/>
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	<body class="bst_element_body" ng-cloak>
		<div ng-show="topPanelPreview">
			<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
		</div>   
		<div ui-view></div>
		
		<!--  Chat Panel Wrapper Starts  -->
			<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
		<!--  Chat Panel Wrapper Ends  -->
		
		<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>

<!--  ==============================================================================
**********************  Local Path Starts *********************
==============================================================================*/
-->		
			<!--------------------------------  Common Vendor Js Starts  ------------------------------->
			<script src='../vendor/jquery/js/jquery.min.js'></script>
	        <script src='../vendor/jquery/js/jquery-ui.js'></script>
	        <script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
	        <script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
	        <script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
	        <script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
	        <script src='../vendor/jquery/js/bootstrap.min.js'></script>
	        <script src='../vendor/jquery/js/moment.min.js'></script>
			<script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
			<script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
		 	<script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
	        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
	        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
	        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
	        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
	        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
	        
	        <script src='../vendor//angular/js/angular.min.js'></script>
	        <script src='../vendor//angular/js/angular-ui-router.min.js'></script>
	        <script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
	        <script src='../vendor//angular/js/html2canvas.js'></script>
			<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
			<script src="../vendor/angular/js/lodash.min.js"></script>
	        <script src='../vendor//angular/js/angular-flash.min.js'></script>
	        <script src="../vendor/jquery/js/leaflet-image.js"></script>
	        
	    	<script src='../charts/d3.v3.min.js'></script>
	        <script src='../charts/d3.v4.min.js'></script>
	        <script src='../charts/d3v3.js'></script>
	        <script src='../charts/d3js.js'></script>
	       
	        <script src='../scripts/VLA/js/cola.v3.min.js'></script>
	        <script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
	        <script src='../scripts/VLA/js/cytoscape-cola.js'></script>
			<!--------------------------------  Common Vendor Js Ends    ------------------------------->
			
			<!--------------------------------  Mip Vendor Js Starts  ------------------------------->
			 <script src='../scripts/VLA/js/typeahead.bundle.js'></script>
	    
	    <script src="../scripts/VLA/js/cytoscape-panzoom.js"></script>
	    <script src="../scripts/VLA/js/cytoscape-qtip.js"></script>
	    <script src="../scripts/VLA/js/weaver.min.js"></script>
	    <script src="../scripts/VLA/js/cytoscape-spread.js"></script>
	     <script src="../scripts/VLA/js/jquery.qtip.js"></script>
	     <script src="../charts/leaflet/leaflet.js"></script>
		     <script src='../charts/topojson.min.js'></script>
	    	 <script src='../charts/datamaps.all.js'></script>
	         <script src='../charts/WorldMap/js/WorldChart.js'></script>
	         <script src='../charts/networkChart.js'></script>
	         <script src='../charts/reusablePie.js'></script>
	         <script src='../charts/horizontalBarChart.js'></script>
	         <script src='../charts/verticalBarChart.js'></script>
	         <script src='../charts/brushMip.js'></script>
	         <script src='../charts/lineChart.js'></script>
		
			<!--------------------------------  Mip Vendor Js Ends    ------------------------------->
			
			<script src="../scripts/app.js"></script>
			<script src="./mip.app.js"></script>
			<script src="./mip.config.js"></script>
			<script src="./mip.run.js"></script>
		    
		    <!--------------------------------  Common Scripts Js Starts  ------------------------------>
			<script src='../scripts/common/constants/app.constant.js'></script>
	    	<script src='../scripts/common/constants/common.constant.js'></script>
	    	<script src='../scripts/discover/constants/discover.constant.js'></script>
	    	<script src='../scripts/act/constants/act.constant.js'></script>
	    	
	    	<script src='../scripts/common/services/shared.service.js'></script>
	    	<script src='../scripts/common/services/top.panel.api.service.js'></script>
	    	<script src='../scripts/common/services/riskScore.service.js'></script>
	    	<script src='../scripts/common/services/upload.file.service.js'></script>
	    	<script src='../scripts/discover/services/discover.api.service.js'></script>
	    	<script src='../scripts/act/services/act.api.service.js'></script>
	    	<script src='../scripts/act/services/act.graph.service.js'></script>
	    	<script src="../scripts/common/services/common.service.js"></script>
	    	
	    	
	    	
	    	<script src='../scripts/common/js/submenu.controller.js'></script>
	    	<script src='../scripts/common/js/top.panel.controller.js'></script>
	    	<script src='../scripts/common/js/advanced.search.controller.js'></script>
	    	<script src='../scripts/common/js/user.events.controller.js'></script>
    		<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
	    	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
	    	<script src="../scripts/common/js/notification.controller.js"></script>
	    	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
	    	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>   	
	    	
	    	<script src='../scripts/common/js/entity.visualiser.js'></script>
	    		
	 		<script src="../scripts/common/js/chatbot.controller.js"></script>
			<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
			<script src="../scripts/common/modal/js/create.event.controller.js"></script>
			<script src="../scripts/common/modal/js/participants.event.controller.js"></script> 
			<!--------------------------------  Common Scripts Js Ends   ------------------------------->		
			<script src='../mip/constants/mip.constant.js'></script>
	    
	    	<script src='../mip/services/mip.api.service.js'></script>
	    	<script src='../mip/services/mip.service.js'></script>
	    	
	    	<script src='../mip/controllers/mip.landing.controller.js'></script>
	    	<script src='../mip/controllers/mip.controller.js'></script>
	    	
	    	<script src='../mip/modal/controllers/mip.save.modal.controller.js'></script>
			
<!--  ==============================================================================
**********************  Local Path Ends *********************
==============================================================================*/
-->		



<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/
-->		
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
<!-- 		<script src="../vendor/common-vendor.min.js"></script> -->
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		<!--------------------------------  Mip Vendor Js Starts  ------------------------------->
<!-- 		<script src="./assets/minifiedJs/mip-vendor.min.js"></script> -->
		<!--------------------------------  Mip Vendor Js Ends    ------------------------------->
		
<!-- 		<script src="../scripts/app.js"></script> -->
<!-- 		<script src="./mip.app.js"></script> -->
<!-- 		<script src="./mip.config.js"></script> -->
<!-- 		<script src="./mip.run.js"></script> -->
	    
	    <!--------------------------------  Common Scripts Js Starts  ------------------------------>
<!-- 		<script src="../scripts/common-scripts.min.js"></script> -->
		<!--------------------------------  Common Scripts Js Ends   ------------------------------->		
		
<!-- 		<script src="./assets/minifiedJs/mip-scripts.min.js"></script> -->

<script src="../vendor/common.vendor.new.min.js"></script>
<script src="./assets/minifiedJs/mip.vendor.new.min.js"></script>

<script src="./assets/minifiedJs/mip.intialize.new.min.js"></script>
<script src="./assets/minifiedJs/mip.charts.new.min.js"></script>


<script src="../scripts/common.scripts.new.min.js"></script>
<script src="./assets/minifiedJs/mip.scripts.new.min.js"></script>

<script src="./assets/minifiedJs/mip.moduleScripts.new.min.js"></script>

<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->		
	<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('.chat-bot-wrapper').mCustomScrollbar({
           axis: "y"
       });
   });
</script>
	</body>
</html>
