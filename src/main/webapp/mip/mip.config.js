'use strict';

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', function($stateProvider, $provide, $urlRouterProvider, EHUB_FE_API, $locationProvider){
	   window.appName = 'mip';
	   $stateProvider
		.state('mipLanding', {
			url: '/mipLanding',
			templateUrl: function(){
				return  EHUB_FE_API + appName + '/views/mipLanding.jsp';
			},
			controller : 'MipLandingController'
		})
		.state('mip', {
			url: '/mip?id&searchSting',
			templateUrl: function(){
				return  EHUB_FE_API + appName + '/views/mip.jsp';
			},
			controller : 'MipController'
		})
		.state('404', {
			url : '/404',
			templateUrl : function(){
				return EHUB_FE_API + 'error';
			}
		});
	   $urlRouterProvider.when('', '/mipLanding');
	    $urlRouterProvider.otherwise(function(){
			return '/404';
		});

}]);
