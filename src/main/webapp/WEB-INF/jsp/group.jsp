<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div ng-cloak class="transaction-monitoring">
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
	<!--  Super Sub Header Wrapper Ends  -->

	<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">
		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">
			<div class="activiti-task-wrapper reports-wrapper user-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-3 col-sm-4">

							<!--  Aside Wrapper Starts  -->
							<aside class="aside-wrapper text-uppercase">
								<a href="javascript:void(0);" class="btn btn-block btn-create">
									<i class="fa fa-plus"></i>Create Group
								</a>
								<div class="form-group has-success has-feedback">
									<input type="text" class="form-control" placeholder="Search" />
									<span class="glyphicon glyphicon-search form-control-feedback"
										aria-hidden="true"></span>
								</div>

								<div class="row">
									<div class="col-sm-7">
										<h3>Groups</h3>
									</div>
									<div class="col-sm-5">
										<div class="dropdown clearfix">
											<a href="javascript:void(0);" uib-dropdown-toggle class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											</a>
											<ul uib-dropdown-menu class="dropdown-menu" aria-labelledby="dLabel">
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
											</ul>
										</div>
									</div>
								</div>

								<div class="panel custom-panel groups-panel" ng-class="{'active':groupdata.active }" ng-repeat="groupdata in group.groupData2">
									<div class="panel-heading">
										{{groupdata.name}} <strong>{{groupdata.value}}</strong>
									</div>
								</div>
							</aside>
							<!--  Aside Wrapper Ends  -->

						</div>
						<div class="col-md-9 col-sm-8">

							<!--  Create New Task Wrapper Starts  -->
							<div class="create-new-task-wrapper">
								<div class="create-new-task-inner-wrapper">
									<!--  Notification Panel Starts  -->
									<div
										class="panel custom-panel notification-panel user-notification-panel">
										<div class="panel-heading">
											<div class="notification-header">
												<h3>Admin</h3>
												<ul class="list-inline">
													<li><i class="fa fa-user-secret"></i>09 Persons</li>
												</ul>
											</div>
											<a class="btn btn-light-blue" href="javascript:void(0);">Edit</a>
											<a class="btn btn-light-red" href="javascript:void(0);"><i
												class="fa fa-times"></i>Remove</a>
										</div>
									</div>
									<!--  Notification Panel Ends  -->

									<!--  Associated Groups Panel Starts  -->
									<div
										class="panel custom-panel associated-people-panel associated-groups-panel">
										<div class="panel-body investigation-wrapper">
											<ul class="list-inline admin-info-list">
												<li>Id : Admin</li>
												<li>Name : Admin</li>
												<li>Type : Security-Code</li>
											</ul>
											<h3>
												Members <a href="javascript:void(0);" class="add-link"><i
													class="fa fa-plus-circle"></i>Add Group</a>
											</h3>
											<div class="table-responsive text-uppercase">
												<table class="table table-condensed table-striped">
													<thead>
														<tr>
															<th width="20%">Id</th>
															<th width="25%">First Name</th>
															<th width="25%">Email</th>
															<th width="30%">Action</th>
															<th width="10%">Action</th>
														</tr>
													</thead>
													<tbody>
														<tr ng-repeat="groupdata in group.groupData">
															<td>{{groupdata.Id}}</td>
															<td>{{groupdata.name}}</td>
															<td>{{groupdata.email}}</td>
															<td>{{groupdata.Action2}}</td>
															<td><i class="{{groupdata.Action}}"></i></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!--  Associated Groups Panel Ends  -->

								</div>
								<!--  Related Content Panel Ends  -->

							</div>
							<!--  Create New Task Wrapper Ends  -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  Content Wrapper Ends  -->
	</div>
</div>
