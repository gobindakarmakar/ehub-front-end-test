<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
		<!--  Super SubMenu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super SubMenu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->


				<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Cluster Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper isra-card-wrapper cluster-dashboard-wrapper bg-dark-grey1">
            <!-- Top Grid Wrapper-->
            <div class="top-grid-wrapper container-fluid">
                <ul class="list-inline custom-tabs row top-filters-list">

                    <li class="col-sm-7">
                        <div class="col-sm-4">
                            <strong class="value">18 Clusters </strong>
                            <h4 class="top-grid-heading">generated</h4>
                        </div>

                        <div class="col-sm-4">
                            <strong class="value">127,889,901,227 </strong>
                            <h4 class="top-grid-heading">total customers</h4>
                        </div>
                        <div class="col-sm-4">
                            <strong class="value">$34BN</strong>
                            <h4 class="top-grid-heading">Total Spendings</h4>
                        </div>
                    </li>

                    <li class="col-sm-5 pad-t0">
                        <div class="col-sm-6">
                            <div class="usage-feature-box">
                                <h3 class="usage-feature-box-heading">
                                    Average Expenditure
                                </h3>
                                <div class="color-input-wrapper">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider" class="custom-range-slider" ng-change="avgExpenditureChange();" ng-model="myValue"></div>
                                        </div>
                                        <div class="col-sm-3">Low</div>
                                        <div class="col-sm-3">Average</div>
                                        <div class="col-sm-3">Medium</div>
                                        <div class="col-sm-3">High</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 ">

                            <div class="usage-feature-box right-box">
                                <h3 class="usage-feature-box-heading">
                                    Cluster Range
                                </h3>
                                <div class="color-input-wrapper grey">
                                     <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider2" class="custom-range-slider"></div>
                                        </div>
                                        <div class="col-sm-3 pull-left"  ng-change="clusterRangeChange();" ng-model="myValue">Min</div>
                                        <div class="col-sm-3 pull-right">Max</div>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!--Top Grid Wrapper Ends-->
            <div class="cluster-content-wrapper">
                <div id="israClusterBubbleChart"></div>
                <div class="list-notations">
                    <ul ng-repeat="legend in bubbleChartLegend">
                        <li class="notation-list">
                            <span class="icon-left" style="color:{{legend.color}};">
                                <i class="fa fa-circle"></i>
                            </span><span  style="color:{{legend.color}};">{{legend.name}}</span></li>
                     </ul>
                </div>

            </div>

            <!--Top Grid Wrapper Ends-->

        </div>


        <script>
            function collision($div1, $div2) {
                var x1 = $div1.offset().left;
                var w1 = 40;
                var r1 = x1 + w1;
                var x2 = $div2.offset().left;
                var w2 = 40;
                var r2 = x2 + w2;

                if (r1 < x2 || x1 > r2) return false;
                return true;

            }

           
              
          </script>
        