<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
	<!--  Super Sub Header Wrapper Ends  -->

	<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">
		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">
			<div
				class="activiti-task-wrapper reports-wrapper user-wrapper crystallball-wrapper">
				<div class="container-fluid">
					<!--  Create New Task Wrapper Starts  -->
					<div class="create-new-task-wrapper">
						<div class="create-new-task-inner-wrapper process-instances-wrapper">
							<!--  Associated Groups Panel Starts  -->
							<div
								class="panel custom-panel associated-people-panel associated-groups-panel">
								<div class="panel-body investigation-wrapper">
									<h3>Process Instances</h3>
									<div class="table-responsive text-uppercase">
<!-- 										<a href="javascript:void(0);" class="custom-reply">Reply</a> -->
										<table class="table table-condensed table-striped">
											<thead>
												<tr>
													<th>Id</th>
													<th>Name</th>
													<th>Started By</th>
													<th>Ended</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="crystalballdata in crystalBall.crystalBallData">
													<td>{{crystalballdata.Id}}</td>
													<td class="text-uppercase">{{crystalballdata.name}}</td>
													<td class="text-uppercase">{{crystalballdata.StartedBy}}</td>
													<td></td>
												</tr>
												
											</tbody>
										</table>
									</div>
									<div class="process-crystallball">
										<ul class="list-inline">
											<li><a class="text-uppercase" href="#processDefinition"
												data-toggle="pill"> Process Definition </a></li>
											<li class="active"><a class="text-uppercase"
												href="#executeNextEvent" data-toggle="pill"> Execute
													Next Event </a></li>
										</ul>
									</div>
								</div>
							</div>
							<!--  Associated Groups Panel Ends  -->

						</div>
						<!--  Related Content Panel Ends  -->

					</div>
					<!--  Create New Task Wrapper Ends  -->
				</div>
			</div>
		</div>
		<!--  Content Wrapper Ends  -->
	</div>
	<!--  Main Wrapper Ends  -->
</div>
