<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
<!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="activiti-task-wrapper reports-wrapper vacation-request">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">

                                <!--  Aside Wrapper Starts  -->
                                <aside class="aside-wrapper text-uppercase">

                                    <div class="form-group has-success has-feedback">
                                        <input type="text" class="form-control" placeholder="Search Tasks"/>
                                        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                    </div>

                                    <div class="sidebar-header">
                                        <h3>Recent Instances</h3>
                                        <div class="bydate-sorting">  
                                            <div class="dropdown clearfix">
                                                <a href="javascript:void(0);" class="pull-right" id="dLabel" data-toggle="dropdown">
                                                    By Date<span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel custom-panel panel-info">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <p>Vaccation Request</p>
                                            <h3>1475</h3>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <p>Simple Approval process</p>
                                            <h3>1098</h3>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <p>Leave Approval Process</p>
                                            <h3>134</h3>
                                        </a>
                                    </div>
                                </aside>
                                <!--  Aside Wrapper Ends  -->

                            </div>
                            <div class="col-md-9 col-sm-8">

                                <!--  Create New Task Wrapper Starts  -->
                                <div class="create-new-task-wrapper">
                                    <div class="create-new-task-inner-wrapper">
                                        <!--  Notification Panel Starts  -->
                                        <div class="panel custom-panel notification-panel">
                                            <div class="panel-heading">
                                                <div class="notification-header">
                                                    <a href="javascript:void(0);" class="fa fa-cogs server-link"></a>
                                                    <h3>Vacation Request</h3>
                                                    <ul class="list-inline">
                                                        <li><i class="fa fa-calendar-o"></i>Created on : 12/09/2016</li>
                                                    </ul>
                                                </div>
                                                <a class="btn btn-light-blue" href="javascript:void(0);">
                                                    <i class="fa fa-trash-o"></i>DELETE
                                                </a>
                                            </div>
                                        </div>
                                        <!--  Notification Panel Ends  -->


                                        <!--  Sub Task Panel Starts  -->
                                        <div class="panel custom-panel sub-task-panel">
                                            <div class="vacation-request-content-body">
                                                <h4 class="text-uppercase">Task</h4>
                                                <div class="table-responsive custom-table-design">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-uppercase">
                                                                    Name
                                                                </th>
                                                                <th class="text-uppercase">
                                                                    Priority
                                                                </th>
                                                                <th class="text-uppercase text-center">
                                                                    Assignee
                                                                </th>
                                                                <th class="text-uppercase">
                                                                    Due Date
                                                                </th>
                                                                <th class="text-uppercase">
                                                                    Created
                                                                </th>
                                                                <th class="text-uppercase">
                                                                    Completed
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-uppercase">
                                                                    <div class="request-name">
                                                                        <i class="fa fa-check-circle text-green"></i>
                                                                        <span>Handle vacation Request</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>60(High)</span>
                                                                </td>
                                                                <td class="text-uppercase  text-center">
                                                                    <div class="assignee-name">
                                                                        <small><i class="fa fa-user"></i></small>
                                                                        <span>George Peter</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>NA</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>12/09/2016</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>6 Days Ago</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-uppercase">
                                                                    <div class="request-name">
                                                                        <i class="fa fa-check-circle text-green"></i>
                                                                        <span>Adjust Leave Notification</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>60(High)</span>
                                                                </td>
                                                                <td class="text-uppercase  text-center">
                                                                    <div class="assignee-name">
                                                                        <small><i class="fa fa-user"></i></small>
                                                                        <span>George Peter</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>NA</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>12/09/2016</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>6 Days Ago</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-uppercase">
                                                                    <div class="request-name">
                                                                        <i class="fa fa-exclamation-circle text-pink"></i>
                                                                        <span>Simple Request Pending</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>60(High)</span>
                                                                </td>
                                                                <td class="text-uppercase  text-center">
                                                                    <div class="assignee-name">
                                                                        <small><i class="fa fa-user"></i></small>
                                                                        <span>George Peter</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>NA</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>12/09/2016</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>6 Days Ago</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-uppercase">
                                                                    <div class="request-name">
                                                                        <span>Handle Vacation Request</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>60(High)</span>
                                                                </td>
                                                                <td class="text-uppercase  text-center">
                                                                    <div class="assignee-name">
                                                                        <small><i class="fa fa-user"></i></small>
                                                                        <span>George Peter</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>NA</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>12/09/2016</span>
                                                                </td>
                                                                <td class="text-uppercase">
                                                                    <span>6 Days Ago</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <h4 class="text-uppercase">VARIABLES</h4>
                                                <div class="variables-request-session">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="candidate-request-details">
                                                                <h5 class="text-uppercase">
                                                                    kermit
                                                                    <a href="javascript:void(0);" class="text-lowercase" ng-click="openModal()">more</a>
                                                                </h5>
                                                                <p class="text-uppercase">
                                                                    WED MAR 01 04:16:00 GMT 2017
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="candidate-request-details">
                                                                <h5 class="text-uppercase">
                                                                    George Peter
                                                                    <a href="javascript:void(0);" class="text-lowercase" ng-click="openModal()">more</a>
                                                                </h5>
                                                                <p class="text-uppercase">
                                                                    WED MAR 01 04:16:00 GMT 2017
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="candidate-request-details">
                                                                <h5 class="text-uppercase">
                                                                    Anna James
                                                                    <a href="javascript:void(0);" class="text-lowercase" ng-click="openModal()">more</a>
                                                                </h5>
                                                                <p class="text-uppercase">
                                                                    WED MAR 01 04:16:00 GMT 2017
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Sub Task Panel Ends  -->

                                    </div>
                                    <!--  Related Content Panel Ends  -->

                                </div>
                                <!--  Create New Task Wrapper Ends  -->

                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->



