<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ehubApp" class="bst_element">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="./assets/images/logo-new.png" />
		<title class="text-capitalize" ng-bind="stateName"></title>
		
		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="assets/css/common-vendor.min.css" rel="stylesheet"/>
		<!------------------------alsh-------  Vendor Styles Ends  --------------------------------->
		<link href="assets/css/common-styles.min.css" rel="stylesheet"/>
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="charts/leaflet/leaflet.search.css" rel="stylesheet"/>
		<!-- Css file for flag icons -->

		<link href="vendor/jquery/css/flag-icon.min.css" rel="stylesheet">
		<style>
				#createCaseWrapper .mCSB_container{
					display: flex;
					flex-direction: column;
				}
			</style>
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	<body ng-cloak class="custom-scroll-wrapper custom-modal-body bst_element_body" ng-class="{'bg-process-background': $state.current.name == 'process'}"> 
		<flash-message class="main-flash-wrapper"></flash-message>
		<div ng-show="topPanelPreview">
			<%@include file="topPanel.jsp" %>
		</div>   
		<div ui-view></div>
 	
		<!--  Chat Panel Wrapper Starts  -->
			<%@include file="../../WEB-INF/jsp/common/chatBot.jsp"%>
		<!--  Chat Panel Wrapper Ends  -->
		
		<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>
		
<!--  ==============================================================================
**********************  Local Path Starts *********************
==============================================================================*/
-->		
		
		<!-------------------------------  Jquery Plugins Starts  ------------------------------->
		<script src="vendor/jquery/js/jquery.min.js"></script>
		<script src="vendor/jquery/js/jquery-ui.js"></script>
		<script src="vendor/jquery/js/jquery.mThumbnailScroller.min.js"></script>
		<script src="vendor/jquery/js/jquery.mCustomScrollbar.js"></script>
		<script src="vendor/jquery/js/jquery.dataTables.min.js"></script>
		<script src="vendor/jquery/js/dataTables.responsive.min.js"></script>
		<script src="vendor/jquery/js/raphael.min.js"></script>
		<script src="vendor/jquery/js/raphael.icons.min.js"></script>
		<script src="vendor/jquery/js/wheelnav.min.js"></script>
		<script src="vendor/jquery/js/bootstrap.min.js"></script>
		<script src="vendor/jquery/js/jquery.resize.js"></script>
		<script src="vendor/jquery/js/moment.min.js"></script>
		<script src="vendor/jquery/js/underscore.min.js"></script>
		<script src="vendor/jquery/js/displacy-entity.min.js"></script>
		<script src="vendor/jquery/js/leaflet-image.js"></script>
		<script src="vendor/jquery/js/jquery.scrollbar.js"></script>
		<script src="vendor/jquery/js/chosen.jquery.min.js"></script>
		<!-------------------------------  Jquery Plugins Ends  ------------------------------->
		
		<!-------------------------------  d3 Plugins Starts  ------------------------------->
		<script src="charts/timeformat.js"></script>
	    <script src="charts/timeformatsupport.js"></script>
		<script src="charts/d3.v3.min.js"></script>
		<script src="charts/d3.v4.min.js"></script>
		<script src="charts/d3v3.js"></script>
		<script src="charts/d3js.js"></script>
		<script src="charts/groupedColumChart.js"></script>
		<script src="charts/timescaleBubble.js"></script>
		<script src="charts/timescaleBubble2.js"></script>
		<script src="charts/bubbleChart.js"></script>
		<script src="charts/pieChart.js"></script>
		<script src="charts/leaflet/leaflet.js"></script>
		<script src="charts/leaflet/TileLayer.Grayscale.js"></script>
        <script src='charts/leaflet/leaflet.search.js'></script>
		<script src="charts/hotTopics.js"></script>
		<script src="charts/topojson.min.js"></script>
		<script src="charts/cloud.js"></script>
		<script src="charts/ClusterBubble.js"></script>
		<script src="charts/caseTimeline.js"></script>
		<script src="charts/WorldMap/js/WorldChart.js"></script>
		<script src="charts/brush.js"></script>
		<script src="charts/collapse.js"></script>
		<script src="charts/sank.min.js"></script>
		<script src="charts/reusablePie.js"></script>
		<script src="charts/networkChart.js"></script>
		<script src="charts/networkChartEntity.js"></script>
		<script src="charts/lineChart.js"></script>
		<script src="charts/horizontalBarChart.js"></script>
		<script src="charts/verticalBarChart.js"></script>
		<script src="charts/cytoscape-cola.js"></script>
		<script src="charts/datamaps.all.js"></script>
		<script src="charts/brushMip.js"></script>
		<script src="charts/timelineSimpleColumn.js"></script>
		<script src="charts/bubbleEntity.js"></script>
		<script src="charts/stackedtime.js"></script>
		<script src="charts/temporal.js"></script>
		<script src="charts/sankRiskScore.js"></script>
		<script src="charts/D3radarChart.js"></script>		
		<script src="charts/ordinalLineChart.js"></script>
		<!-------------------------------  d3 Plugins Ends  ------------------------------->
		
		<!-------------------------------  Angular Plugins Starts  ------------------------------->
		<script src="vendor/angular/js/angular.min.js"></script>
		<script src="vendor/angular/js/angular-ui-router.min.js"></script>
		<script src="vendor/angular/js/ui-bootstrap-tpls-2.5.0.min.js"></script>
		<script src="vendor/angular/js/angular-gridster.js"></script>
		<script src="vendor/angular/js/angular-datatables.min.js"></script>
		<script src="vendor/angular/js/html2canvas.js"></script>
		<script src="vendor/angular/js/ng-file-upload.min.js"></script>
		
		<script src="vendor/angular/js/angular-flash.min.js"></script>
		<script src="vendor/angular/js/FileSaver.min.js"></script>
		<script src="vendor/angular/js/socket.io.min.js"></script>
		<script src="vendor/angular/js/angular-timeline.js"></script>
		<script src="vendor/angular/js/ngStorage.js"></script>
		<script src="vendor/angular/js/angular-moment.min.js"></script>
		<script src="vendor/angular/js/angular-cookies.js"></script>
		<script src="vendor/angular/js/rzSlider.js"></script>
		<script src="vendor/angular/js/angular-chosen.min.js"></script>
		<!-------------------------------  Angular Plugins Ends  ------------------------------->
		
		<!----------------------------  Initial Configuration JS Starts  ------------------------------->
		
		<script src="scripts/app.js"></script>
		<script src="scripts/app.config.js"></script>
		<script src="scripts/app.run.js"></script>
		<!----------------------------  Initial Configuration JS Ends  ------------------------------->
		
		<!--------------------------------  Constants Js Starts  ------------------------------->
		<script src="scripts/common/constants/app.constant.js"></script>
		<script src="scripts/common/constants/common.constant.js"></script>
		<script src="scripts/discover/constants/discover.constant.js"></script>
		<script src="scripts/act/constants/act.constant.js"></script>
		<script src="scripts/common/constants/enrich.search.constant.js"></script>
		<script src="scripts/transactionMonitoring/constants/transmonitoring.constant.js"></script>
		<script src="scripts/adverseNews/constants/adverse.news.constant.js"></script>
		<script src="scripts/enrich/constants/entity.search.constant.js"></script>
		<!--------------------------------  Directives Js Ends  ------------------------------->

		<!--------------------------------  Services Js Starts  ------------------------------->
		<script src="scripts/common/services/shared.service.js"></script>
		<script src="scripts/common/services/common.service.js"></script>
		<script src="scripts/common/services/riskScore.service.js"></script>
		<script src="scripts/common/services/top.panel.api.service.js"></script>
		<script src="scripts/common/services/upload.file.service.js"></script>
		<script src="scripts/discover/services/discover.api.service.js"></script>
		<script src="scripts/discover/services/discover.graph.service.js"></script>
		<script src="scripts/enrich/services/enrich.api.service.js"></script>
		<script src="scripts/enrich/services/enrich.graph.service.js"></script>
		<script src="scripts/act/services/act.api.service.js"></script>
		<script src="scripts/act/services/act.graph.service.js"></script>
		<script src="scripts/manage/services/generalSettings.api.service.js"></script>
		<script src="scripts/transactionMonitoring/services/transmonitoring.api.service.js"></script>
		<script src="scripts/transactionMonitoring/services/transmonitoring.graph.service.js"></script>
		<script src="scripts/screeningTesting/services/screening.testing.api.service.js"></script>
		<script src="scripts/adverseNews/services/adverse.news.api.service.js"></script>
		<script src="scripts/adverseNews/services/adverse.news.service.js"></script>
		<script src="scripts/discover/services/investigation.landing.service.js"></script>
		<script src="scripts/enrich/services/entity.api.services.js"></script>
		<script src="transactionIntelligence/services/transaction.analysis.api.service.js"></script>
		<!--------------------------------  Services Js Ends  ------------------------------->
		
		<!--------------------------------  Directives Js Starts  ------------------------------->
		
		<script src="scripts/discover/directives/work.diary.directive.js"></script>
		<!--------------------------------  Directives Js Ends  ------------------------------->
		
		<!--------------------------------  Controllers Js Starts  ------------------------------->
			
		<script src="scripts/domain/js/domain.controller.js"></script>	
		<script src="scripts/process/js/process.controller.js"></script>
		<script src="scripts/common/js/submenu.controller.js"></script>
		<script src="scripts/common/js/entity.visualiser.js"></script>
		<script src="scripts/common/js/chatbot.controller.js"></script>
		<script src="scripts/common/modal/js/chatbot.modal.controller.js"></script>
		<script src="scripts/common/modal/js/confirmationForDocparserPage.modal.js"></script>
		<script src="scripts/common/modal/js/confirmDelSticky.controller.js"></script>
		<!--------------------------------  Controllers Js Ends  ------------------------------->
		
		<!--------------------------------  Top Panel Js Starts  ------------------------------->	
		<script src="scripts/common/js/top.panel.controller.js"></script>
		<script src="scripts/common/js/advanced.search.controller.js"></script>
		<script src="scripts/common/js/user.events.controller.js"></script>
		<script src="scripts/common/js/my.clipboard.controller.js"></script>
		<script src="scripts/common/js/my.entity.clipboard.controller.js"></script>
		<script src="scripts/common/js/notification.controller.js"></script>
		<!--------------------------------  Top Panel Js Ends ------------------------------->
		
		<!--------------------------------   Common Js Starts  ------------------------------->
		<script src="scripts/common/modal/js/entityRIskModal.controller.js"></script>
		<script src="scripts/common/modal/js/add.media.modal.controller.js"></script>
		<script src="scripts/common/modal/js/riskScore.modal.controller.js"></script>
		
		<script src="scripts/common/modal/js/widget.capture.modal.controller.js"></script>
		<script src="scripts/common/modal/js/maximize.modal.controller.js"></script>
		<script src="scripts/common/modal/js/dataPopUp.modal.controller.js"></script>
		<script src="scripts/common/modal/js/onboarding.modal.controller.js"></script>
		<script src="scripts/common/modal/js/uploadquestionaire.controller.js"></script>
		<script src="scripts/common/modal/js/upload.documents.modal.controller.js"></script>
		<script src="scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js"></script>
		<script src="scripts/common/modal/js/idv.questionaire.controller.js"></script>
		<script src="scripts/common/modal/js/create.event.controller.js"></script>
		<script src="scripts/common/modal/js/participants.event.controller.js"></script>
		<script src="scripts/common/directives/common.directives.js"></script>
		<script src="scripts/common/modal/js/screeningTesting.modal.controller.js"></script>
		<script src="scripts/common/filters/common.filters.js"></script>
		<script src="scripts/common/directives/context.menu.directive.js"></script>
		<!--------------------------------   Common Js Ends  ------------------------------->	
		
		<!--------------------------------  Discover Dashboard Js Starts  ------------------------------->
		<script src="scripts/discover/js/workspace.controller.js"></script>
		<script src="scripts/discover/js/transaction.monitoring.controller.js"></script>
		<script src="scripts/discover/js/investigation.landing.controller.js"></script>
		<script src="scripts/discover/js/discover.dashboard.controller.js"></script>
		<script src="scripts/discover/js/gridster.controller.js"></script>
		<script src="scripts/discover/js/my.case.diary.controller.js"></script>
		<script src="scripts/discover/js/top.locations.controller.js"></script>
		<script src="scripts/discover/js/your.case.metrics.controller.js"></script>
		<script src="scripts/discover/js/case.in.focus.controller.js"></script>
		<script src="scripts/discover/js/hot.topics.controller.js"></script>
		<script src="scripts/discover/js/my.entities.controller.js"></script>
		<script src="scripts/discover/js/entities.in.focus.controller.js"></script>
		<script src="scripts/discover/js/your.work.diary.controller.js"></script>
		<script src="scripts/discover/modal/js/create.case.modal.controller.js"></script>
		<script src="scripts/common/modal/js/filter.case.modal.controller.js"></script>

		<!--------------------------------  Discover Dashboard Js Ends  -------------------------->

		<!--------------------------------  Act Dashboard Js Starts  -------------------------->
	    <script src="scripts/act/js/act.dashboard.controller.js"></script>
		<script src="scripts/act/js/case.page.landing.controller.js"></script>
		<script src="scripts/act/js/entity.page.controller.js"></script>
		<script src="scripts/act/js/acthottopics.controller.js"></script>
		<script src="scripts/act/js/acttoplocations.controller.js"></script>
		<script src="scripts/act/js/associatedentities.controller.js"></script>
		<script src="scripts/act/js/casetimeline.controller.js"></script>
		<script src="scripts/act/js/entitiesinfocus.controller.js"></script>
		<script src="scripts/act/js/gridster.controller.js"></script>
		<script src="scripts/act/js/related.cases.controller.js"></script>
		<script src="scripts/act/js/peer.group.controller.js"></script>	
		<script src="scripts/act/js/related.company.controller.js"></script>				
		<script src="scripts/act/js/riskratio.controller.js"></script>
		<script src="scripts/act/modal/js/reassign.modal.controller.js"></script>
		<script src="scripts/act/js/linkanalysis.controller.js"></script>
		<script src="scripts/act/modal/js/riskoverview.modal.controller.js"></script>
		<script src="scripts/act/js/audit.trail.controller.js"></script>	
			
		<!--------------------------------  Act Dashboard Js Ends  -------------------------->
		
		<!--------------------------------  Enrich Dashboard Js Starts  -------------------------->
		<script src="scripts/enrich/js/enrich.dashboard.controller.js"></script>
		<script src="scripts/enrich/js/dfd.controller.js"></script>
		<script src="scripts/enrich/js/social.live.feed.controller.js"></script>
		<script src="scripts/enrich/directives/enrich.directives.js"></script>
		<!--------------------------------  Enrich Dashboard Js Ends  -------------------------->
				
		<!--------------------------------  Manage Dashboard Js Starts  -------------------------->
		<script src="scripts/manage/constants/manage.constants.js"></script>
		<script src="scripts/manage/js/app.manager.controller.js"></script>
		<script src="scripts/manage/js/data.management.controller.js"></script>
		<script src="scripts/manage/js/general.settings.controller.js"></script>
		<script src="scripts/manage/js/manage.dashboard.controller.js"></script>
		<script src="scripts/manage/js/system.monitoring.controller.js"></script>
		<script src="scripts/manage/js/user.management.controller.js"></script>
		<script src="scripts/manage/modal/js/addUser.Modal.Controller.js"></script>
		<script src="scripts/manage/js/users.controller.js"></script>
		<script src="scripts/manage/js/groups.controller.js"></script>
		<script src="scripts/manage/js/manageDb.controller.js"></script>
		<script src="scripts/manage/js/deployments.controller.js"></script>
		<script src="scripts/manage/js/jobs.controller.js"></script>
		<script src="scripts/manage/js/administrations.controller.js"></script>
		<script src="scripts/manage/js/crystalBall.controller.js"></script>
		<script src="scripts/manage/js/activeProcess.controller.js"></script>
		<script src="scripts/manage/js/suspendedProcess.controller.js"></script>
		<script src="scripts/manage/js/dataSource.controller.js"></script>
		<script src="scripts/manage/js/tasks.controller.js"></script>
		<script src="scripts/manage/js/process.instance.controller.js"></script>
		<script src="scripts/manage/js/reports.controller.js"></script>
		<script src="scripts/manage/js/deployment.process.controller.js"></script>
		<script src="scripts/manage/js/model.workspace.controller.js"></script>
		<script src="scripts/manage/modal/js/addSource.Modal.controller.js"></script>
		<script src="scripts/manage/modal/js/deploymentProcess.modal.controller.js"></script>
		<script src="scripts/manage/modal/js/modelWorkspace.modal.controller.js"></script>
		<script src="scripts/manage/modal/js/candidate.info.modal.controller.js"></script> 
		<script src="scripts/manage/modal/js/uploadCustomerlogoModal.controller.js"></script>
		
		<!--------------------------------  Data Curation JS Starts ------------------------------------------------------------------->
		<script src="scripts/manage/js/addNewCuration.js"></script>
		<script src="scripts/manage/js/staging.mip.controller.js"></script>
		<script src="scripts/manage/services/dataCuration.api.service.js"></script>
		<script src="scripts/manage/modal/js/relation.modal.controller.js"></script>
		<script src="scripts/manage/modal/js/previewModal.modal.controller.js"></script>
		<!----------------------------- --->
		<!--------------------------------  Manage Dashboard Js Ends  -------------------------->
		
		<!--------------------------------  Transaction Monitoring Js Starts  -------------------------->
		<script src="scripts/transactionMonitoring/controllers/transaction.monitoring.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/geographic.distribution.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/grouped.peer.analysis.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/risky.transactions.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/transaction.linkage.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/transaction.per.period.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/transactions.journey.controller.js"></script>
		<script src="scripts/transactionMonitoring/controllers/volume.monitor.controller.js"></script>
		<!--------------------------------  Transaction Monitoring Js Ends  -------------------------->
		
		<!--------------------------------  Adverse News Js Starts  -------------------------->
		<script src="scripts/adverseNews/controllers/adverseNews.Controller.js"></script> 
		<!--------------------------------  Adverse News Js Ends  -------------------------->
		
		<!--------------------------------  screenign testing Js Starts  -------------------------->
		<script src="scripts/screeningTesting/controllers/screeningTesting.Controller.js"></script> 
		<!--------------------------------   screenign testing Js Ends  -------------------------->
		
		<!--------------------------------  IsraCard Js Starts  -------------------------->
		<script src="scripts/discover/js/arc.selection.controller.js"></script> 
		
		<!--------------------------------  IsraCard Js Ends  -------------------------->

		<!--------------------------------  Start VLA JS ------------------------------------------------------------------->
		<script src="scripts/VLA/js/cola.v3.min.js"></script>
	    <script src="scripts/VLA/js/cytoscape_2.7.12.js"></script>
	    <script src="scripts/VLA/js/cytoscape-cola.js"></script>
	    <script src="scripts/VLA/js/cytoscape-cose-bilkent.js"></script>
	    <script src="scripts/VLA/js/cytoscape-cxtmenu.js"></script>
	    <script src="scripts/VLA/js/cytoscape-markov-cluster.js"></script>
	    <script src="scripts/VLA/js/cytoscape-navigator.js"></script>
	    <script src="scripts/VLA/js/cytoscape-ngraph.forcelayout.js"></script>
	    <script src="scripts/VLA/js/cytoscape-panzoom.js"></script>
	    <script src="scripts/VLA/js/cytoscape-qtip.js"></script>
	    <script src="scripts/VLA/js/cytoscape-undo-redo.js"></script>
	    <script src="scripts/VLA/js/weaver.min.js"></script>
	    <script src="scripts/VLA/js/cytoscape-spread.js"></script>
	    <script src="scripts/VLA/js/random-color.js"></script>
	    <script src="scripts/VLA/js/spectrum.js"></script>
	    <script src="scripts/VLA/js/jquery.qtip.js"></script>
	    <script src="scripts/VLA/js/typeahead.bundle.js"></script>
	    <script src="scripts/VLA/js/jquery.selectric.min.js"></script>
		<script src="scripts/VLA/js/custom_config.js"></script>
		<script src="scripts/VLA/js/app.js"></script>
<!-- 		<script src="scripts/VLA/js/app_locale_ua.js"></script> -->
<!-- 		<script src="scripts/VLA/js/app_locale_ar.js"></script> -->
		<script src="scripts/VLA/tagcloud/js/tagcloud.js"></script>
		<script src="scripts/VLA/js/bluebird.min.js"></script>
		<script src="scripts/VLA/js/base64toblob.js"></script>
		<script src="scripts/VLA/js/loadash.js"></script>		
		<script src="scripts/createNotification.controller.js"></script>	
		<!--------------------------------  END VLA JS ------------------------------------------------------------------->
		

		
		
		<script src="vendor/jquery/js/jquery.nicescroll.min.js"></script>
        <script src="vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
        <script src="vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
        <script src='vendor/jquery/js/jquery-daterangepicker.js'></script>
        <script src='vendor/jquery/js/bootstrap-daterangepicker.js'></script>

        <script src="vendor/jquery/js/moment-timezone-with-data.min.js"></script>
        <script src="vendor/jquery/js/dom-to-image.js"></script>
        <script src="vendor/jquery/js/moment-timezone-utils.js"></script>
		<script src="vendor/jquery/js/angularjs-dropdown-multiselect.js"></script>		
        
<!--         <script src="vendor/jquery/js/moment-timezone.min.js"></script> -->
<!--         <script src="vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script> -->
        
        
<!--  ==============================================================================
**********************  Local Path Ends *********************
==============================================================================*/
-->	


<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/
-->			
		
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
<!-- 		<script type="text/javascript" src="vendor/common-vendor.min.js"></script> -->
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		<!--------------------------------  Vendor Js Starts  -------------------------->
<!--  		<script type="text/javascript" src="vendor/vendor.min.js"></script> -->
		<!--------------------------------  Vendor Js Ends  ---------------------------->
		
		<!--------------------------------  Charts Js Starts  -------------------------->
<!--  		<script type="text/javascript" src="charts/charts.min.js"></script> -->
		<!--------------------------------  Charts Js Ends  ---------------------------->
		
		<!--------------------------------  Common Js Starts  -------------------------->
<!--  		<script type="text/javascript" src="scripts/scripts.min.js"></script>  -->
		<!--------------------------------  Common Js Ends  ---------------------------->
				
		<!--------------------------------  Custom Js Starts  -------------------------->
<!--  		<script type="text/javascript" src="scripts/common-scripts.min.js"></script>  -->
		<!--------------------------------  Custom Js Ends  ---------------------------->
		
<!--new minified Js start -->		
<!-- <script src="./assets/minifiedJs/root.moduleScripts.new.min.js"></script> -->
<!--new minified Js end -->
<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->	
		



<!-- Settings Color Popover -->
<script type="text/ng-template" id="SettingsIcons.html">
		<div class="width-100 height-100 p-rel d-block pad-b30 settings-content-wrapper">
		<h3 class="f-14 roboto-regular text-cream mar-b0">Choose Icon</h3>
			<form class="form jc-sb pad-x0" ng-submit="applySourceEvidenceEntityClipBoard(form)" name="form">
				<div class="input-group mar-b5 pad-l5 width-100 custom-input-group custom-select-dropdown inner-element">
												<!-- <input type="text" maxlength="100" class="custom-input border-blue-thin-b width-100">
										<span class=" pull-right r-10 t-5 input-group-addon1"  style="cursor:pointer; position: absolute;  z-index: 100;">
									<i class="fa fa-search"></i>
								</span> -->
										</div>
				
			</form>
			<ul id="myDIV" class="custom-list pad-l0 mxh-140 overflow-a custom-scroll-wrapper mar-b15 d-flex fw-w settings-icons-list">
				<li class="mar-5 list-icon" ng-repeat="icon in iconList">
					<div  ng-click="addIcon(icon.name)" class="icon-wrapper d-flex fd-c ai-c icons-btn-wrapper" ng-class="{'active' : addColorIconObj.obj.icon === icon.name}">
						<span class="icon f-18"><i class="fa fa-{{icon.name}}"></i></span>
						<span class="icon-name ws-pl word-bw f-10 lh-1 mar-t5">{{icon.name}}</span>
					</div>
				</li>
				
			</ul>
			<h3 class="f-16 roboto-regular text-cream mar-b10">Choose Flag</h3>
			<div class= "bst_input_group">
			<input type='text' ng-model='searchtxt.jurisdictionName' class="width-100 custom-input" placeholder="Search">	</div>
			<ul class="custom-list pad-l0 settings-icons-list mxh-100 overflow-a custom-scroll-wrapper d-block">
					<li class="mar-5 pad-5" ng-repeat="country in jurisdictionList | filter : searchtxt " >		
						<div class="flag-name-wrapper fd-c ai-c d-flex" ng-click="addCountryFlag(country.key,$index)" ng-class="{'active' : nameOfFlag === country.key}">
							<!-- <span  ng-click="addCountryFlag(country.country)" class="square-25 radius-5 check-btn" ng-class="{'activeCheck' : addColorIconObj.obj.colorCode === color.colorCode}" ng-style="{'background-color':setColor(color.colorCode)}"></span> -->
							<!-- <span class="icon-name ws-pl word-bw f-14 mar-t5">{{color.color}}</span> -->
							<span class="flag-wrapper"><img  src="{{country.flag}}" class="square-18"></span>
							<span class="flag-name ws-pl word-bw f-10 lh-1 mar-t5">{{country.key}}</span>
						</div>
					</li>	
									
				</ul>
			<h3 class="f-16 roboto-regular text-cream mar-b10">Choose Color</h3>
			
			<ul class="custom-list pad-l0 mxh-100 overflow-a custom-scroll-wrapper d-flex icon-color-wrapper fw-w settings-icons-list">			
				<li class="mar-5" ng-repeat="color in colorList">		
					<div class="color-name-wrapper d-flex fd-c ai-c ">
						<span  ng-click="addColor(color.colorCode)" class="square-25 radius-5 check-btn" ng-class="{'activeCheck' : addColorIconObj.obj.colorCode === color.colorCode}" ng-style="{'background-color':setColor(color.colorCode)}"></span>
						<!-- <span class="icon-name ws-pl word-bw f-14 mar-t5">{{color.color}}</span> -->
					</div>
				</li>	
								
			</ul>
			
			<div class="d-flex jc-fe">
				<button class="grad-button sm-btns f-14 mar-r10 lh-1" ng-click="changeColorNIconToTable()">Add</button>
				<button class="bordered-button sm-btns  f-14 lh-1" ng-click="closePopover()">Close</button>
			</div>
			
	</div>
	</script>
	<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('.chat-bot-wrapper , .settings-icons-list').mCustomScrollbar({
           axis: "y"
       });
       

       //$('#chatBotTextArea').scrollbar();
       
   });


console.clear();
</script>
	</body>
</html>