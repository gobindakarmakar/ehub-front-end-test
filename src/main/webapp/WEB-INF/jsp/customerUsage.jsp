<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

		<!--  Super Sub-Menu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super Sub-Menu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->


			<!--  Usage Dashboard Wrapper Starts  -->
			<div class="dashboard-wrapper usage-dashboard-wrapper bg-dark-grey1">
					<!-- Top Grid Wrapper-->
					<p class="top-heading">
						Showing: 04 Customer Analysis
					</p>
					<div class="row ">
						<div class="col-sm-3 usage-feature-box">
							<h3 class="usage-feature-box-heading">
								01.General Card Holder
							</h3>
							<p class="usage-feature-box-text">
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos rem eligendi similique dolorum nihil incidunt sint optio minus,
								quaerat ea. Aliquam laudantium officia deleniti eos obcaecati perspiciatis totam cumque praesentium.
							</p>

						</div>
						<div class="col-sm-3 usage-feature-box">
							<h3 class="usage-feature-box-heading">
								02.Dual Card Holders
							</h3>
							<p class="usage-feature-box-text">
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos rem eligendi similique dolorum nihil incidunt sint optio minus,
								quaerat ea. Aliquam laudantium officia deleniti eos obcaecati perspiciatis totam cumque praesentium.
							</p>
						</div>
						<div class="col-sm-3 usage-feature-box">
							<h3 class="usage-feature-box-heading">
								03. Predicting Dual Card Holders
							</h3>
							<p class="usage-feature-box-text">
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos rem eligendi similique dolorum nihil incidunt sint optio minus,
								quaerat ea. Aliquam laudantium officia deleniti eos obcaecati perspiciatis totam cumque praesentium.
							</p>
						</div>
					</div>
					<div class="row ">
						<div class="col-sm-3 usage-feature-box">
							<h3 class="usage-feature-box-heading">
								04.Early Adaptors
							</h3>
							<p class="usage-feature-box-text">
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos rem eligendi similique dolorum nihil incidunt sint optio minus,
								quaerat ea. Aliquam laudantium officia deleniti eos obcaecati perspiciatis totam cumque praesentium.
							</p>
						</div>

					</div>


					<!--Top Grid Wrapper Ends-->
				</div>
				<!--  Usage Dashboard Wrapper Ends  -->
		

					



					<script>
						$(document).ready(function () {
							setTimeout(function () {
								$(".top-filters-wrapper").mThumbnailScroller({
									axis: "x"
								});
							}, 500);
						});
					</script>