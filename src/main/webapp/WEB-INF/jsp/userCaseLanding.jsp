<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  User Case Dashboard Wrapper Starts  -->

        <div class="dashboard-wrapper user-case-wrapper act-dashboard-wrapper bg-dark-grey1">

            <!--  Top Grid Wrapper Starts  -->
            <div class="top-grid">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="top-heading">
                            <i class="fas fa-chart-line"></i> Case TimeLine

                            <span class="colored-dots">
                                <i class="fa fa-circle"></i>
                                <i class="fa fa-circle"></i>
                                <i class="fa fa-circle"></i>
                            </span>
                        </h3>
                    </div>

                    <div class="col-sm-4">
                        <h3 class="top-heading">
                            <i class="fas fa-chart-line"></i> Your Case Logs
                        </h3>
                    </div>
                </div>
            </div>
            <!--  Top Grid Wrapper Ends  -->
            <!--  Bottom Panel Wrapper Starts  -->
            <div class="bottom-panel-wrapper">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>

                        <!-- user Panel Ends -->

                    </div>
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>

                        <!-- user Panel Ends -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                    <div class="col-sm-4">
                        <div class="panel custom-panel-wrapper  user-panel-wrapper">
                            <div class="panel-heading">
                                <h3>Spending genre</h3>
                            </div>
                            <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Mobiles Phones</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">PC And Tablets</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cloths</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Footwears</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Cosemetics</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Pc Games</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Ice-Creams</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </div>
                        <!-- user Panel Ends -->
                    </div>
                </div>

            </div>
            <!--  Bottom Panel Wrapper Ends  -->

        </div>
        <!--  User Case Dashboard Wrapper Starts  -->