<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div ng-cloak class="transaction-monitoring">
	<%@include file="common/submenu.jsp"%>
	<create visible="showModalCreate"> </create>
	
	<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">

		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">

			<!--  Workspace Wrapper Starts  -->
			<div class="dashboard-wrapper workspace-wrapper">
				<div class="custom-tabs-wrapper">

					<!--  Nav Tabs Starts  -->
					<ul class="nav nav-tabs">
						<li><a ui-sref="discover">Dashboard</a></li>
						<li class="active"><a ui-sref="workspace" data-toggle="tab">Workspace</a></li>
					</ul>
					<!--  Nav Tabs Ends  -->

					<!--  Tab Panes Starts  -->
					<div class="tab-content">
						<div class="tab-pane" id="dashboard"></div>
						<div class="tab-pane active" id="workspace">
							<div class="row">
								<div class="col-sm-4">

									<!--  Recent Activities Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>Recent Activities</h3>
										<ul class="list-unstyled recent-activity-list">
											<li>Jane Palmer Case accepted Yesterday 3:00 PM</li>
											<li>Case of HSBC AML Bublin forwarded to analyst Jacob Wilson Yesterday at 2:00 PM.</li>
											<li>Link Analyses on Jane Austin AML Started Yesterday at 5:00 PM.</li>
											<li>Bank of Kenya Onboarding started Yesterday at 10:00 AM.</li>
											<li>Joseph Lubdick Case rejected Yesterday at 12:00 PM forwarded by Amnda Jayson</li>
											<li>Jane Dick Case Accepted Yesterday 3:00 PM</li>
											<li>Case of HSBC AML Bublin forwarded to analyst Jacob Wilson Yesterday at 4:00 PM.</li>
										</ul>
									</div>
									<!--  Recent Activities Wrapper Ends  -->

									<!--  Activity Graph Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Activity Graph<span>Last Week</span>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Visual Link Analyses</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/graph-slider.jpg"
														alt="green-slider" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/graph-slider.jpg" alt="green-slider" /></td>
													<td><span class="count">60%</span></td>
												</tr>
												<tr>
													<td>Investigations</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/graph-slider.jpg"
														alt="green-slider" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/graph-slider.jpg" alt="green-slider" /></td>
													<td><span class="count">30%</span></td>
												</tr>
												<tr>
													<td>Disseminations</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/graph-slider.jpg"
														alt="green-slider" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/graph-slider.jpg" alt="green-slider" /></td>
													<td><span class="count">08%</span></td>
												</tr>
												<tr>
													<td>Search</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/graph-slider.jpg"
														alt="green-slider" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/graph-slider.jpg" alt="green-slider" /></td>
													<td><span class="count">01%</span></td>
												</tr>
												<tr>
													<td>Idle Time</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/graph-slider.jpg"
														alt="green-slider" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/graph-slider.jpg" alt="green-slider" /></td>
													<td><span class="count">01%</span></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  Activity Graph Wrapper Ends  -->

								</div>
								<div class="col-sm-4">

									<!--  Case In Progress Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Case In Progress<strong class="count">03</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Syria Arms Trade</td>
													<td>Yesterday</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td>-->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Dublin Money Laundering</td>
													<td>12/09/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/light-green-progressbar.jpg"
														alt="light-green-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/light-green-progressbar.jpg" alt="light-green-progressbar" /></td>
												</tr>
												<tr>
													<td>Dubai Forex Trail</td>
													<td>11/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/sky-blue-progressbar.jpg"
														alt="sky-blue-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/sky-blue-progressbar.jpg" alt="sky-blue-progressbar" /></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  Case In Progress Wrapper Ends  -->

									<!--  On going Investigation Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Ongoing Investigations<strong class="count">04</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Jane Palmer</td>
													<td>Yesterday</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Joseph Lubdick Aml</td>
													<td>12/09/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Anna Palmer</td>
													<td>11/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/light-green-progressbar.jpg"
														alt="light-green-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/light-green-progressbar.jpg" alt="light-green-progressbar" /></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  On going Investigation Wrapper Ends  -->

									<!--  Visual Link Analyses Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Visual Link Analyses<strong class="count">03</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Fintech HSBC Scam</td>
													<td>Yesterday</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Joseph Lubdick Aml</td>
													<td>12/09/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/sky-blue-progressbar.jpg"
														alt="sky-blue-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/sky-blue-progressbar.jpg" alt="sky-blue-progressbar" /></td>
												</tr>
												<tr>
													<td>Jake Shields House Loan</td>
													<td>11/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/sky-blue-progressbar.jpg"
														alt="sky-blue-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/sky-blue-progressbar.jpg" alt="sky-blue-progressbar" /></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  Visual Link Analyses Wrapper Ends  -->

								</div>
								<div class="col-sm-4">

									<!--  Active Disseminations Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Active Disseminations<strong class="count">02</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>HSBC House Loan-KYC</td>
													<td>12/09/2016</td>
													<td><i class="fa fa-file-o file-icon"></i></td>
												</tr>
												<tr>
													<td>Bank of America</td>
													<td>11/08/2016</td>
													<td><i class="fa fa-file-o file-icon"></i></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  Active Disseminations Wrapper Ends  -->

									<!--  On Boardings Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											On Boardings<strong class="count">03</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Bank of Kenya -KYC</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/tooltip-slider.jpg"
														alt="tooltip-slider" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/tooltip-slider.jpg" alt="tooltip-slider" /></td>
												</tr>
												<tr>
													<td>Dublin Money Laundering</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/tooltip-slider.jpg"
														alt="tooltip-slider" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/tooltip-slider.jpg" alt="tooltip-slider" /></td>
												</tr>
												<tr>
													<td>Dubai Forex Trail</td>
													<td class="text-right"><i class="fa fa-file-o file-icon"></i></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  On Boardings Wrapper Ends  -->

									<!--  New Cases Accepted Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											New Cases Accepted<strong class="count">04</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Jane Palmer</td>
													<td>Yesterday</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Joseph Lubdick Aml</td>
													<td>12/09/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/light-green-progressbar.jpg"
														alt="light-green-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/light-green-progressbar.jpg" alt="light-green-progressbar" /></td>
												</tr>
												<tr>
													<td>Anna Palmer</td>
													<td>11/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/light-green-progressbar.jpg"
														alt="light-green-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/light-green-progressbar.jpg" alt="light-green-progressbar" /></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  New Cases Accepted Wrapper Ends  -->

									<!--  Case Pipeline Wrapper Starts  -->
									<div class="table-responsive table-custom-condensed">
										<h3>
											Case Pipeline<strong class="count">05</strong>
										</h3>
										<table class="table table-condensed">
											<tbody>
												<tr>
													<td>Fintech HSBC Scam</td>
													<td>Yesterday</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Joseph Lubdick Aml</td>
													<td>12/09/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/sky-blue-progressbar.jpg"
														alt="sky-blue-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/sky-blue-progressbar.jpg" alt="sky-blue-progressbar" /></td>
												</tr>
												<tr>
													<td>Jake Shields House Loan</td>
													<td>11/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/red-progressbar.jpg"
														alt="red-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/red-progressbar.jpg" alt="red-progressbar" /></td>
												</tr>
												<tr>
													<td>Mike Shields House Loan</td>
													<td>12/08/2016</td>
													<!-- <td><img class="img-responsive"
														src="..{{rootPath}}static/images/dashboard/light-green-progressbar.jpg"
														alt="light-green-progressbar" /></td> -->
													<td><img class="img-responsive" src="assets/images/dashboard/light-green-progressbar.jpg" alt="light-green-progressbar" /></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  Case Pipeline Wrapper Ends  -->

								</div>
							</div>
						</div>
					</div>
					<!--  Tab Panes Ends  -->

				</div>
			</div>
			<!--  Workspace Wrapper Ends  -->

		</div>
		<!--  Content Wrapper Ends  -->

	</div>
	<!-- 	<div ng-controller="chatCollaborationImport"> -->
	<!-- 		<div ng-include="chatCollaborationPartial.url"></div> -->
	<!-- 		<div ng-include="chatCollaborationChatBoxPartial.url"></div> -->
	<!-- 	</div> -->
	<%-- <%@include file="footer.jsp"%> --%>
	<script>
		$(document).ready(function() {			
			$(".db-util-process-tab-discover").mThumbnailScroller({
				axis : "x",
				speed: 10
			});			
		});
		 var userDetails = '<%=new ObjectMapper().writeValueAsString((Object) session.getAttribute("userInfo"))%>'
		';
		userDetails = JSON.parse(userDetails);
		if (userDetails.token != undefined && userDetails.token != null) {
		} else {
			window.location.href = '';
		}
	</script>
</div>
