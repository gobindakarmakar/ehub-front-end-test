<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<Style>
	.h-190
	{
		height: 190px;
	}
	.market-pulse-table table thead tr th:first-child {
	    width: 20%!important;
	    max-width: 20%!important;
	}
	     .market-pulse-table table tbody tr td:nth-child(4), .market-pulse-table table thead tr th:nth-child(4){
               width: 16%!important;
           max-width: 16%!important;
      }
</Style>
<span class="custom-spinner showMarketPulseLoader" ng-show="showMarketPulseLoader"> <i
	class="fa fa-spinner fa-spin fa-2x"></I>
</span>
<!--  Main Wrapper Starts  -->
<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="market-pulse-wrapper">
			
			<div class="pulse-analysis-body">
				<div class="pulse-analysis-content clear-fix">
					<div class="pulse-analysis-left-col text-uppercase h-190">
						<div class="analysis-total-wrapper">
							<h4>Start An Analysis</h4>
							<div class="start-analysis-wrapper">
								<div class="custom-upload-wrapper">
									<div class="custom-file-upload">
		                                <input type="file" name="fileuploadOne" id="fileuploadOne" ng-model="uploadFIlenameOne" >
		                                <label style="padding-bottom:8px" for="fileuploadOne" class="file-inline" ngf-select="upload($file,$event)">
		                                    <span class="custom-input">
		                                		<strong class="text-uppercase uploaded-name"  >{{uploadnameone }} </strong>
	                                			<small style="font-size:smaller" class="text-uppercase" >json-file</small>
		                                	</span>
		                                </label>
		                            </div>
								</div>
								
								
								<a class="btn text-uppercase text-center"
									href="javascript:void(0);" id="startAnalysis" ng-click="loaddata()" ng-disabled="!uploadFIlenameOne"> <span>Analyze</span>
								</a>
							</div>
						</div>
					</div>
					<div class="pulse-analysis-right-col h-190">
						<div class="market-pulse-table">
							<div id="marketPulseTableDiv">
								<table id="marketPulseTable" width="100%" datatable="ng" dt-options="dtOptions" class="table text-uppercase">
									<thead>
										<th style="width:16% !important;cursor:default">Type</th>
										<th style="width:16% !important;cursor:default">Name</th>
										<th style="width:16% !important;cursor:default">Founded Date / Date of Birth</th>
										<th style="width:16% !important;cursor:default">Max Score (%)</th>
										<th style="width:16% !important;cursor:default">Rule Name</th>
										<th style="width:16% !important;cursor:default"></th>
									</thead>
									<tbody>
										<tr class="custom-market-modal"  ng-repeat="screeningData in screeningDataFinal" >										
											<td style="width:16% !important;cursor:default">{{ screeningData['type']}}</td>
											<td style="width:16% !important;cursor:default">{{ screeningData['name']}}</td>
											<td style="width:16% !important;cursor:default">{{ screeningData['dob']}}</td>
											<td style="width:16% !important;cursor:default">{{ screeningData['maxScore']}}</td>
											<td style="width:16% !important;cursor:default">{{ screeningData['ruleName']}}</td>
											<td style="width:16% !important;cursor:default" ng-click="openMoreDetails(screeningData)"><a href="javascript:void(0);">More Details</a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->
<script>
	/*$(document).ready(function() {
		setTimeout(function() {
			$("#bargraphsSrcoller").mThumbnailScroller({
				axis : "x"
			});
		}, 500);
		$('.pulse-analysis-left-col').mCustomScrollbar({
			axis : "y",
			theme : "minimal-dark"
		});
	});*/
</script>




