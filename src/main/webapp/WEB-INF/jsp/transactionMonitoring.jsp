<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--  SubMenu Starts  -->
<%@include file="common/submenu.jsp"%>  
<!--  SubMenu Ends  -->
<div class="transaction-monitoring container-fluid">

	<div class="top-content">
		<div class="headerpart">
			<h4>  
				Investigation > PetroSaudi International ltd
			</h4>
		</div>
		<div class="header-content">
			<div class="header-content-entry person-monitoring">
				<div class="header-main-content">
					<img class="media-object" src="assets/images/activity/petrosaudi_logo.png" alt="Profile">
					<h5 class="case-name" title="PetroSaudi International LTD">PetroSaudi International LTD</h5>
					<p class="case-type">TYPE : {{details.clientType}}</p>
					<span class="case-start">Start Date : 28/03/2005</span>
					<span class="case-updated">Updated : 30/01/2017</span>
				</div>
			</div>
			<div class="header-content-entry person-description">
				<p class="description">
					PetroSaudi International
					is a privately owned oil exploration and production company, with
					offices in Saudi Arabia, England, and Switzerland. Founded in 2005,
					the company is paving a pathway into the future of the energy
					industry through compelling multinational projects on the ground
					and in the marketplace. PetroSaudi is involved in oil and gas
					projects in energy-producing regions around the world.
				</p>
			</div>
			<div class="header-content-entry person-entities">
				<div class="row">
					<div class="col-sm-6">
						<div class="score-title">Entities</div>
						<div class="score">123</div>
					</div>
					<div class="col-sm-6">
						<div class="score-title">Related Cases</div>
						<div class="score">45</div>
					</div>
				</div>
			</div>
			<div class="header-content-entry person-bar-graph progress-bar-wrapper">
				<div class="row">
					<div class="col-sm-1 progress-label">DR</div>
					<div class="col-sm-10 bar-holder">
						<div class="progress">
							<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{scaledNumArr[1]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[1]}}%"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-1 progress-label">IR</div>
					<div class="col-sm-10 bar-holder">
						<div class="progress">
							<div class="progress-bar progressir" role="progressbar" aria-valuenow="{{scaledNumArr[0]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[0]}}%"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-1 progress-label">TR</div>
					<div class="col-sm-10 bar-holder">
						<div class="progress">
							<div class="progress-bar progresstr" role="progressbar" aria-valuenow="{{scaledNumArr[2]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[2]}}%"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-content-entry person-circle-graph">
				<div class="c100 p{{cumulativeRisk | number: 0}} orange radial">
					<span class="inner-number">{{cumulativeRisk | number: 0}}
						<span class="inner-text">
							Cumulative
							<span>Risk</span>
						</span>
					</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="riskyTransactionalTimeline">
		<div class="timeline-wrapper"></div>
	</div>
	<div class="riskyTransactionalTable text-uppercase" ng-controller="RiskyTransactionsController">
		<span ng-show="riskyTransactionsLoader" class="custom-spinner case-dairy-spinner"> 
			<i class="fa fa-spinner fa-spin fa-2x"></i>
		</span>
		<div class="risky-transaction-wrapper box">
			<div class="box-header">
				<h4>Client(s) in Risky Transactions</h4>
			</div>
			<div class="box-content custom-box-content" style="overflow-x: hidden; height: 300px; overflow-y: scroll;">
				<table id="riskyTransaction" class="table table-condensed table-striped" width="100%">
					<thead>
						<tr>
							<th>Time</th>
							<th>business line</th>
							<th>client name</th>
							<th>client type</th>
							<th>country</th>
							<th>country risk</th>
							<th>industry type</th>
							<th>region</th>
							<th>risk category</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="riskyTransData in riskyTransDatas">
							<td>{{riskyTransData.timestamp | date}}</td>
							<td>{{riskyTransData.businessLine}}</td>
							<td>{{riskyTransData.clientName}}</td>
							<td>{{riskyTransData.clientType}}</td>
							<td>{{riskyTransData.country}}</td>
							<td>{{riskyTransData.countryRisk}}</td>
							<td>{{riskyTransData.industryType}}</td>
							<td>{{riskyTransData.region}}</td>
							<td>{{riskyTransData.riskCategory}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="trans-widgets-wrapper row">
		<div id="volumeMonitor" ng-controller="VolumeMonitorController" class="col-lg-6 box-6-left widget top-widgets">
			<div class="chart-wrapper" id="associatedEntitiesChart"></div>
		</div>
		<div id="geographicDistribution" ng-controller="GeographicDistributionController" class="col-lg-6 box-6-right widget widget-trans-map top-widgets">
			<span ng-show="geographicPreloader" class="custom-spinner case-dairy-spinner"> 
				<i class="fa fa-spinner fa-spin fa-2x"></i>
			</span>
			<div class='widget-header'>
				<span>GEOGRAPHIC DISTRUBUTION</span> 
				<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-ellipsis-v ellipsis"></i>
				</a>
				<ul class="dropdown-menu filter-menu">
					<li>
						<a href="javascript:void(0);" id=""><i class="fa fa-filter"></i></a>
					</li>
					<li>
						<a href="javascript:void(0);" ng-click="TransCaptureDiv('geographicDistribution', toggleGraphTopLocation)"> 
							<i title="Capture Screen" class="capture-current-portlet icon-camera"></i> 
							</a>
					</li>
					<li ng-hide="toggleGraphTopLocation">
						<a href="javascript:void(0);" ng-click="toggleGraphTopLocation = true">
							<i class="fa fa-bar-chart"></i>
						</a>
					</li>
					<li ng-show="toggleGraphTopLocation">
						<a href="javascript:void(0);" ng-click="toggleGraphTopLocation = false">
							<i class="fa fa-list-alt"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="widget-content widget-top-location">
				<div id="mapLeaflet" class="chartContentDiv" ng-show="toggleGraphTopLocation">
					<div id="map"></div>
				</div>
				<div ng-hide="toggleGraphTopLocation" class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>COUNTRY</th>
								<th>COUNT</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="(location, count) in locationsData" ng-class-odd="'odd'" ng-class-even="'even'">
								<td>{{location}}</td>
								<td>{{count}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<!--  Top Locations Widget Ends  -->
		</div>
		<div id="transactionLinkage" ng-controller="TransactionLinkageController" class="col-lg-6 box-6-left widget">
			<div class="chart-wrapper" id="transactionLinkage"></div>
		</div>
		<div id="groupedPeerAnalysis" ng-controller="GroupedPeerAnalysisController" class="col-lg-6 box-6-right widget">
			<div class="chart-wrapper" id="groupbar"></div>
		</div>
		<div id="transactionJourney" ng-controller="TransactionsJourneyController" class="col-lg-6 box-6-left widget top-widgets">
			<div class="chart-wrapper" id="transactions_journey"></div>
		</div>
		<div id="transactionPerPeriod" ng-controller="TransactionPerPeriodController" class="col-lg-6 box-6-right widget top-widgets">
			<div class="box">
				<div class="box-header">
					<h4>Transaction Per Period</h4>
				</div>
				<div class="box-content">
					<table id="dailyTransactionMetrics" class="table table-condensed table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Time stamp per day</th>
								<th>Count</th>
								<th>Sum of amount</th>
								<th>Average amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>January 19th 2017, 13:14:00.256</td>
								<td>1,992</td>
								<td>10,000</td>
								<td>Individuals - High Net Worth</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		/*Custom Scroll Bar*/
		$('.custom-box-content').mCustomScrollbar({
			axis : "y",
			theme : "minimal"
		});
	});
</script>
