<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="admin-details-wrapper">
			<div class="total-users-wrapper">
				<ul class="list-inline clearfix">
					<li>
						<div class="total-users text-center">
							<p class="text-uppercase">Total Users</p>
							<span>647</span>
						</div>
					</li>
					<li>
						<div class="total-organisation text-center">
							<p class="text-uppercase">Organisation</p>
							<span>19</span>
						</div>
					</li>
					<li>
						<div class="total-organisation-ratio text-center">
							<p class="text-uppercase graph">Organisation wise ratio</p>
							<img src="assets/images/donut.png" alt="donut" />
						</div>
					</li>
					<li>
						<div class="total-rolewise-ratio text-center">
							<p class="text-uppercase graph">Role wise ratio</p>
							<img src="assets/images/donut.png" alt="donut" />
						</div>
					</li>
					<li>
						<div class="total-user-trend">
							<p class="text-uppercase graph">User Trend</p>
							<img src="assets/images/donut.png" alt="donut" />
						</div>
					</li>
				</ul>
			</div>
			<div class="usermanagement-data">
				<div class="management-header">
					<ul class="content-wrapper-pills clearfix">
						<li class="{{userdata.active}}"  ng-repeat="userdata in usermanagement.usermanagementdata"><a class="management-tabs" ng-click="usermanagement.navigatepage(userdata.name)"> <i class="{{userdata.class}}"></i> <span class="text-uppercase">{{userdata.name}}</span>
						</a></li>
					</ul>
				</div>
				<div class="management-body">
					<div class="data-table-wrapper">
						<div id="managementData">
							<div class="data-table-accessories">
								<div class="organisation">
									<div class="organisation-data">
										<span class="key-text text-uppercase">Organisation :</span> <span class="value-text text-uppercase"> All</span>
									</div>
									<div class="viewby-data">
										<span class="key-text text-uppercase">View By :</span> <span class="value-text text-uppercase"> Recently Added</span>
									</div>
								</div>

								<div class="new-user-content">
									<a class="" href="javascript:void(0);" data-toggle="modal" data-target="#addUser" ng-click="usermanagement.addUserModel()"> <i class="fa fa-plus"></i> <span>ADD USER</span>
									</a> <a class="" href="javascript:void(0);"> <i class="fa fa-sign-out"></i> <span>EXPORT</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



