<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>
<!--  Super Sub Header Wrapper Ends  -->
<div class="activiti-supersubheader-wrapper user-supersubheader-content">
	<div class="container-fluid">
		<div class="activiti-supersubheader-content">
			<ul class="list-inline">
				<li class="active"><a class="text-uppercase"
					href="javascript:void(0);"> <span
						class="text-uppercase">General Reports</span>
				</a></li>
				<li><a class="text-uppercase"
					href="javascript:void(0);"> <span
						class="text-uppercase">Saved Reports</span>
				</a></li>
	
			</ul>
		</div>
	</div>
</div>
<!--  Main Wrapper Starts  -->
<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="activiti-task-wrapper reports-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3 col-sm-4">

						<!--  Aside Wrapper Starts  -->
						<aside class="aside-wrapper text-uppercase">

							<div class="form-group has-success has-feedback">
								<input type="text" class="form-control"
									placeholder="Search Reports" /> <span
									class="glyphicon glyphicon-search form-control-feedback"
									aria-hidden="true"></span>
							</div>

							<div class="row">
								<div class="col-sm-7">
									<h3>Recent Reports</h3>
								</div>
								<div class="col-sm-5">
									<div class="dropdown clearfix">
										<a href="javascript:void(0);" class="pull-right" id="dLabel"
											data-toggle="dropdown"> By Date<span class="caret"></span>
										</a>
										<ul class="dropdown-menu" aria-labelledby="dLabel">
											<li><a href="javascript:void(0);">By Date</a></li>
											<li><a href="javascript:void(0);">By Date</a></li>
											<li><a href="javascript:void(0);">By Date</a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="panel custom-panel panel-info">
								<div class="panel-body">
									<h3>Employee Productivity</h3>
									<p>Created : 1 Week Ago</p>
								</div>
							</div>
							<div class="panel custom-panel">
								<div class="panel-body">
									<h3>Help Desk Process</h3>
									<p>Last Edited : 2 Week Ago</p>
								</div>
							</div>
							<div class="panel custom-panel">
								<div class="panel-body">
									<h3>Process Instances</h3>
									<p>Created : 3 Week Ago</p>
								</div>
							</div>
							<div class="panel custom-panel">
								<div class="panel-body">
									<h3>Task Duration</h3>
									<p>Created : 1 Week Ago</p>
								</div>
							</div>
							<div class="panel custom-panel">
								<div class="panel-body">
									<h3>Sales Process</h3>
									<p>Last Edited : 1 Week Ago</p>
								</div>
							</div>
							<div class="panel custom-panel">
								<div class="panel-body">
									<h3>Customer Complaints</h3>
									<p>Last Edited : 1 Week Ago</p>
								</div>
							</div>
						</aside>
						<!--  Aside Wrapper Ends  -->

					</div>
					<div class="col-md-9 col-sm-8">

						<!--  Create New Task Wrapper Starts  -->
						<div class="create-new-task-wrapper">
							<div class="create-new-task-inner-wrapper">
								<!--  Notification Panel Starts  -->
								<div class="panel custom-panel notification-panel">
									<div class="panel-heading">
										<div class="notification-header">
											<a href="javascript:void(0);"
												class="fa fa-file-text-o server-link"></a>
											<h3>Employee Productivity</h3>
											<ul class="list-inline">
												<li><i class="fa fa-check-circle"></i>Version : 1</li>
												<li><i class="fa fa-calendar-o"></i>Created on :
													12/09/2016</li>
											</ul>
										</div>
										<a class="btn btn-light-blue" href="javascript:void(0);">Save</a>
									</div>
								</div>
								<!--  Notification Panel Ends  -->

								<!--  Associated People Panel Starts  -->
								<div class="panel custom-panel associated-people-panel">
									<div class="panel-body">
										<form class="form-inline">
											<div class="form-group">
												<label class="control-label">Employee</label>
											</div>
											<div class="form-group">
												<button type="button" class="btn btn-light-blue btn-circle">Select
													User</button>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-4">Month</label>
												<div class="col-sm-8">
													<div class="input-group date" data-provide="datepicker">
														<input type="text" class="form-control">
														<div class="input-group-addon">
															<span class="fa fa-calendar"></span>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<button type="button"
													class="btn btn-light-blue btn-generate">Generate
													Report</button>
											</div>
										</form>
									</div>
								</div>
								<!--  Associated People Panel Ends  -->

								<!--  Sub Task Panel Starts  -->
								<div class="panel custom-panel sub-task-panel"></div>
								<!--  Sub Task Panel Ends  -->

							</div>
							<!--  Related Content Panel Ends  -->

						</div>
						<!--  Create New Task Wrapper Ends  -->

					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->