<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
		<!--  Super SubMenu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super SubMenu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->

				<!--  Adopters Dashboard Wrapper Starts  -->
				<div class="dashboard-wrapper adopters-dashboard-wrapper bg-dark-grey1">

					<!--  Main Filters Wrapper Starts  -->
					<div class="main-filters-wrapper isra-filters-wrappers adopters-filters-wrappers">

						<!--  Top Filters Wrapper Starts  -->
						<div class="top-filters-wrapper">
							<ul class="top-filter-navigation list-inline">
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Total Costumers</h4>
										<strong class="value">958746855
										</strong>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Total Spendings</h4>
										<strong class="value">958746855
										</strong>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Age</h4>

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list two-col">
											<li class="progressbar-list-item">
												<div class="left-col">10-20</div>
												<div class="progress">

													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:95%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">20-40</div>
												<div class="progress">

													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:70%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">40-60</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:50%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">60-above</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:50%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->

									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Gender</h4>
										<div id=""></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Average Spending</h4>
										<div id=""></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Transaction Type</h4>

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list two-col">
											<li class="progressbar-list-item">
												<div class="left-col">Debit Card</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:95%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">Credit Card</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:70%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->

									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Transaction Mode</h4>
										<div id=""></div>
									</div>
								</li>
							</ul>
						</div>
						<!--  Top Filters Wrapper Ends  -->

					</div>
					<!--  Main Filters Wrapper Ends  -->

					<!-- Main Adopters Content Starts Here -->
					<div class="row main-adopters-wrapper mar-t10">
						<div class="col-sm-9 pad-r0 custom-col left-col">
							<div class="transaction-adopters-wrapper custom-data-table-wrapper">
								<table id="" class="table table-striped border-b0" role="grid">
									<thead>
										<tr role="row">
											<th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
												Costumer
											</th>
											<th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
											 aria-label="Name: activate to sort column descending">
												Early Adopters
											</th>
											<th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
											 aria-label="Name: activate to sort column descending">
												Merchant Types
											</th>
										</tr>
									</thead>
									<tbody>
										<tr id="" role="row" class="odd">
											<td>
												JaneAustin
											</td>
											<td>High</td>
											<td>Mobile Phones</td>
										</tr>
										<tr id="" role="row" class="even">
											<td>
												Amanda Merelin</td>
											<td>Medium</td>
											<td>Fashion</td>
										</tr>
										<tr id="" role="row" class="odd">
											<td>
												Merlin Methew
											</td>
											<td>Medium</td>
											<td>Cosmetics</td>
										</tr>
										<tr id="" role="row" class="even">
											<td>Amanda Merelin</td>
											<td>Medium</td>
											<td>Fashion</td>
										</tr>
										<tr id="" role="row" class="odd">
											<td>
												JaneAustin
											</td>
											<td>High</td>
											<td>Mobile Phones</td>
										</tr>
										<tr id="" role="row" class="even">
											<td>Amanda Merelin</td>
											<td>Medium</td>
											<td>Fashion</td>
										</tr>
										<tr id="" role="row" class="odd">
											<td>
												Merlin Methew
											</td>
											<td>Medium</td>
											<td>Cosmetics</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<!-- Right Col Starts -->
						<div class="col-sm-3 custom-col right-col">
							<div class="row custom-row">
								<div class="col-sm-12 custom-col">

									<!-- Adopters Panel Starts -->
									<div class="panel custom-panel-wrapper adopters-panel-wrapper">
										<div class="panel-heading">
											<h3>Sectors</h3>
										</div>
										<div class="panel-body mCustomScrollbar" style="position: relative; overflow: visible;">
											<div id="">
											</div>
											<div class="text-right">
												<a href="javascript:void(0);" class="btn btn-blue-link">View All</a>
											</div>
										</div>
									</div>
									<!-- Adopters Panel Ends -->

									<!-- Adopters Panel Starts -->
									<div class="panel custom-panel-wrapper adopters-panel-wrapper">
										<div class="panel-heading">
											<h3>Products</h3>
										</div>
										<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
											<!--  Progressbar List Starts -->
											<ul class="list-unstyled progressbar-list">
												<li class="progressbar-list-item">
													<div class="left-col">Mobiles Phones</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:98%">
															<span class="sr-only ">1 Complete</span>
														</div>
													</div>
													<div class="right-col">98%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">PC And Tablets</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:83%">
															<span class="sr-only">1651 Complete</span>
														</div>
													</div>
													<div class="right-col">83%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Cloths</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:73%">
															<span class="sr-only">710 Complete</span>
														</div>
													</div>
													<div class="right-col">73%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Footwears</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:65%">
															<span class="sr-only">5 Complete</span>
														</div>
													</div>
													<div class="right-col">65%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Cosemetics</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:50%">
															<span class="sr-only">2921 Complete</span>
														</div>
													</div>
													<div class="right-col">50%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Pc Games</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:43%">
															<span class="sr-only">168 Complete</span>
														</div>
													</div>
													<div class="right-col">43%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Ice-Creams</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:30%">
															<span class="sr-only">32 Complete</span>
														</div>
													</div>
													<div class="right-col">30%</div>
												</li>
											</ul>
											<!--  Progressbar List Ends -->
											<div class="text-right">
												<a href="javascript:void(0);" class="btn btn-blue-link">View All</a>
											</div>
										</div>
									</div>
									<!-- Adopters Panel Ends -->

									<!-- Adopters Panel Starts -->
									<div class="panel custom-panel-wrapper adopters-panel-wrapper">
										<div class="panel-heading">
											<h3>Location Trends</h3>
										</div>
										<div class="panel-body">
											<div id=""></div>
										</div>
									</div>
									<!-- Adopters Panel Ends -->

									<!-- Adopters Panel Starts -->
									<div class="panel custom-panel-wrapper adopters-panel-wrapper">
										<div class="panel-heading">
											<h3>Top Merchants</h3>
										</div>
										<div class="panel-body panel-scroll " style="position: relative; overflow: visible;">
											<!--  Progressbar List Starts -->
											<ul class="list-unstyled progressbar-list">
												<li class="progressbar-list-item">
													<div class="left-col">Wall Mart</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:98%">
															<span class="sr-only">1 Complete</span>
														</div>
													</div>
													<div class="right-col">98%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">KFC</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:83%">
															<span class="sr-only">1651 Complete</span>
														</div>
													</div>
													<div class="right-col">83%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Subway</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:70%">
															<span class="sr-only">710 Complete</span>
														</div>
													</div>
													<div class="right-col">70%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Alibaba</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:63%">
															<span class="sr-only">5 Complete</span>
														</div>
													</div>
													<div class="right-col">63%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Ebay</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:50%">
															<span class="sr-only">2921 Complete</span>
														</div>
													</div>
													<div class="right-col">50%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Amazon</div>
													<div class="progress ">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:43%">
															<span class="sr-only">168 Complete</span>
														</div>
													</div>
													<div class="right-col">43%</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Disney</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:30%">
															<span class="sr-only">32 Complete</span>
														</div>
													</div>
													<div class="right-col">30%</div>
												</li>
											</ul>
											<div class="text-right">
												<a href="javascript:void(0);" class="btn btn-blue-link text-right">View All</a>
											</div>
										</div>
									</div>
									<!-- Adopters Panel Ends -->

									<!-- Adopters Panel Starts -->
									<div class="panel custom-panel-wrapper adopters-panel-wrapper">
										<div class="panel-heading">
											<h3>Spending Trends</h3>
										</div>
										<div class="panel-body panel-scroll border-b0" style="position: relative; overflow: visible;">
										</div>
									</div>
									<!-- Adopters Panel Ends -->

								</div>
								<!--  Twelfth Column Ends  -->
							</div>
						</div>
						<!-- Right Col Starts -->

					</div>
					<!-- Main Adopters Content ends Here -->

				</div>
				<!--  Adopters Dashboard Wrapper Ends  -->

				<script>
					$(document).ready(function () {
						setTimeout(function () {
							$(".top-filters-wrapper").mThumbnailScroller({
								axis: "x"
							});
							$(".alert-notification-wrapper .chart-panel-wrapper .panel-body").mCustomScrollbar({
								axis: "y",
								theme: "minimal-dark"
							});
							$(".chart-panel-wrapper .panel-scroll").mCustomScrollbar({
								axis: "y",
								theme: "minimal-dark"
							});
							$(
								'#datePicker,#transactionDatePicker,#riskDatePicker,#corporateDatePicker,#counterDatePicker'
							).datepicker();
						}, 500);
					});
				</script>