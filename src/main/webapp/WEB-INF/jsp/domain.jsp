<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

		<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>

			<!--  Domain Wrapper Starts  -->
			<div class="landing-wrapper domain-wrapper">

				<!--  Welcome Message Wrapper Starts  -->
				<div class="welcome-msg-wrapper">
					<p class="choose-msg">
						<span>Hey {{ehubObject.fullName || ehubObject.email}},</span> Lets get started! Choose your element</p>
					<p class="nothanks">
						<a href="javascript:void(0);" ng-click="toWorkspace()">
							<span>No thanks! Skip to Dashboard</span>
						</a>
					</p>
				</div>
				<!--  Welcome Message Wrapper Starts  -->

				<!--  Tile Wrapper Starts  -->
				<div class="tile-wrapper">

					<!--  Tile Group Wrapper Starts  -->
					<div class="tile-group-wrapper">
						<!--
							 <div class="tile intelligence-tile" ng-click="toDashboard()">
				<img class="img-responsive img-tile" src="assets/images/intelligence.png" />
				<div class="tile-contents">
					<h4><a href="javascript:void(0);">Element of Government</a></h4>
					<p>All sources Intelligence Platform.</p>
				</div>
			</div> -->

						<!-- 
								<div class="tile asset-tile" ng-click="toDashboard()">
				<img class="img-responsive img-tile" src="assets/images/asset.png" />
				<div class="tile-contents">
					<h4><a href="javascript:void(0);">Element of Financial Terrain</a></h4>
					<p>Cognitive system to track the trail of money, people and funds.</p>
				</div>
			</div>	 -->
						<div class="tile insurance-risk-tile" ng-click="toDashboard('Risk')">
							<img class="img-responsive img-tile" src="assets/images/risk.png" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Element of Insurance</a>
								</h4>
								<p>Cognitive System for complex underwriting and dynamic risk.</p>
							</div>
						</div>
						<div class="tile credit-cards-tile" ng-click="toDashboard('CREDIT CARDS')">
							<img class="img-responsive img-tile" src="assets/images/creditcards.jpg" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Element of Financial Terrain</a>
								</h4>
								<p>AI enabled applications to the Credit Cards market identifying risk while revealing opportunities.</p>
							</div>
						</div>
					</div>
					<!--  Tile Group Wrapper Ends  -->

					<!--  Tile Group Wrapper Starts  -->
					<div class="tile-group-wrapper middle-tile-group-wrapper">
						<!-- 			<div class="tile fraud-tile" ng-click="toDashboard()"> -->
						<!-- 				<img class="img-responsive img-tile" src="assets/images/ok_05.gif" /> -->
						<!-- 				<div class="tile-contents"> -->
						<!-- 					<h4><a href="javascript:void(0);">Element of Risk</a></h4> -->
						<!-- 					<p>Cognitive system to identify and investigate Fraud and other unlawful activities.</p> -->
						<!-- 				</div> -->
						<!-- 			</div> -->
						<div class="tile aml-tile" ng-click="toDashboard('AML')">
							<img class="img-responsive img-tile" src="assets/images/aml.png"/>
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Element of Compliance</a>
								</h4>
								<p>Cognitive system for all your compliance requirements.</p>
							</div>
						</div>
						<div class="tile financial-tile" ng-click="toDashboard('FINANCIAL CRIME')">
							<img class="img-responsive img-tile" src="assets/images/financial.png" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Element of Investigation</a>
								</h4>
								<p>Cognitive system for deep and complex investigations.</p>
							</div>
						</div>
						<div class="tile social-listening-tile" ng-click="toDashboard('SOCIAL LISTENING')">
							<img class="img-responsive img-tile" src="assets/images/social-listening.jpg" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Social Listening</a>
								</h4>
								<p>Contextual searching and monitoring of social media, news portals and other open source outlets</p>
							</div>
						</div>

						<!-- 			<div class="tile cognitive-accounting-tile" ng-click="toDashboard()"> -->
						<!-- 				<img class="img-responsive img-tile" src="assets/images/cognitive_accounting.png" /> -->
						<!-- 				<div class="tile-contents"> -->
						<!-- 					<h4><a href="javascript:void(0);">Cognitive Accounting</a></h4> -->
						<!-- 					<p></p> -->
						<!-- 				</div> -->
						<!-- 			</div> -->
						<!-- 			<div class="tile peoplesearch-tile" ng-click="toDashboard()"> -->
						<!-- 				<img class="img-responsive img-tile" src="assets/images/peoplesearch.png" /> -->
						<!-- 				<div class="tile-contents"> -->
						<!-- 					<h4><a href="javascript:void(0);">Element of Human Capital</a></h4> -->
						<!-- 					<p>Cognitive search engine to crawl, search and identify the most suitable executives from mass of unstructured data.</p> -->
						<!-- 				</div> -->
						<!-- 			</div> -->
					</div>
					<!--  Tile Group Wrapper Ends  -->

					<!--  Tile Group Wrapper Starts  -->
					<div class="tile-group-wrapper">
						<!-- 			<div class="tile customer-tile" ng-click="toDashboard()"> -->
						<!-- 				<img class="img-responsive img-tile" src="assets/images/journey.png" /> -->
						<!-- 				<div class="tile-contents"> -->
						<!-- 					<h4><a href="javascript:void(0);">Element of Experience</a></h4> -->
						<!-- 					<p>Cognitive system to learn, predict and customized the most personalized experience.</p> -->
						<!-- 				</div> -->
						<!-- 			</div> -->
						<div class="tile market-tile" ng-click="toDashboard('MARKET INTELLIGENCE')">
							<img class="img-responsive img-tile" src="assets/images/market_intel.png" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">Element of markets</a>
								</h4>
								<p>Cognitive system to proactively search, crawl and identify the global hidden market of non public companies.</p>
							</div>
						</div>
						<!-- 			<div class="tile enhance-deligence-tile" ng-click="toDashboard()"> -->
						<!-- 				<img class="img-responsive img-tile" src="assets/images/enhance_deligence.png" /> -->
						<!-- 				<div class="tile-contents"> -->
						<!-- 					<h4><a href="javascript:void(0);">Element of Dilidence</a></h4> -->
						<!-- 					<p>Cognitive system to find the What, Where, Who, When and Why.</p> -->
						<!-- 				</div> -->
						<!-- 			</div> -->
						<div class="tile social-listening-tile" ng-click="toDashboard('INSURANCE')">
							<img class="img-responsive img-tile" src="assets/images/insurance.png" />
							<div class="tile-contents">
								<h4>
									<a href="javascript:void(0);">insurance</a>
								</h4>
								<p>Onboard clients, upload claims and monitor fraud</p>
							</div>
						</div>
					</div>
					<!--  Tile Group Wrapper Ends  -->

				</div>
				<!--  Tile Wrapper Ends  -->

			</div>

			<!--  Domain Wrapper Ends  -->
			<script>
				function getTokenBySession() {
					var userDetails = '<%=new ObjectMapper().writeValueAsString((Object)session.getAttribute("userInfo"))%>';
					userDetails = JSON.parse(userDetails);
					if (userDetails.token != undefined && userDetails.token != null) {
						return userDetails;
					} else {
						return null;
// 						window.location.href = '';
					}
				}
			</script>