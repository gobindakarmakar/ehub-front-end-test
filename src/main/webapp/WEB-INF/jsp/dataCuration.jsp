<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


		<!--  Data Curation Wrapper Starts  -->
		<div class="data-curation-wrapper">

			<!--  Utilities Panel Starts  -->
			<%@include file="common/submenu.jsp"%>
				<!--  Utilities Panel Ends  -->

				<!--  Loader Starts  -->
				<div ng-show="staging.isLoading" class="custom-spinner full-page-spinner">
					<i class="fa fa-spinner fa-spin fa-3x"></i>
				</div>
				<!--  Loader Ends  -->

				<!--  Enrich Main Wrapper Starts  -->
				<div id="enrich-dashboard" class="entity-staging-wrapper data-curation-wrapper">
					<div class="container-fluid">
						<!-- <div class="totalEntitiesWrapper">
										<p class="text-uppercase">Total Entities</p>
										<span class="stagingCount">3192</span>
									</div>							 -->
						<div class="addEntitybtn">
							<button type="button" class="text-center" ng-click="onClickAddDataCuration();">
								<i class="fa fa-plus"></i>
								<span class="text-uppercase">Add New</span>
							</button>
						</div>
						<div class="addEntitybtn" style="right:150px;">
							<button type="button" class="text-center" ng-click="openPreviewModal();">
								<i class="fa fa-eye"></i>
								<span class="text-uppercase">Preview</span>
							</button>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="custom-widget">
									<div class="widget-header">
										<div class="row">
											<div class="col-sm-3">
												Organisation
											</div>
											<div class="col-sm-9 text-right">
												<div class="custom-table-filter-wrapper">
													<input class="form-control custom-input" ng-model="searchEntity.entityQuery" type="text">
													<select class="form-control custom-select-box" ng-model="searchEntity.entitySize">
														<option>10</option>
														<option>20</option>
														<option>30</option>
														<option>50</option>
														<option>100</option>
														<option>500</option>
													</select>
													<select class="form-control custom-select-box hidden">
														<option>Name</option>
														<option>Url</option>
													</select>
													<button class="btn btn-blue" ng-click="searchEntity.searchQuery()" type="button">Search</button>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-content">
										<div class="row">
											<div class="col-sm-12 custom-data-table-wrapper">
												<table width="100%" datatable="ng" dt-options="staging.dtOptions" dt-column-defs="staging.dtColumnDefs" class="table table-striped">
													<thead>
														<tr>
															<th width="20%">NAME</th>
															<th width="35%">HEADQUATER</th>
															<th width="15%">DOMICILE</th>
															<th width="10%">UPDATED</th>
															<th width="10%">STATUS</th>
															<th width="10%" class="text-center">VIEW/EDIT</th>
														</tr>
													</thead>
													<tbody class="case-diary-list">
														<tr ng-repeat="stages in staging.list">
															<td>{{stages.name}}</td>
															<td>{{stages.headquater}}</td>
															<td>{{stages.domicile}}</td>
															<td>{{stages.lastUpdatedOn}}</td>
															<td>{{stages.status}}</td>
															<td class="text-center">
																<a class="action-entity view-entity" href ng-click="openPreviewModal(stages)">
																		<i class="fa fa-eye"></i>
																</a>
																<a class="action-entity view-entity" href ng-click="staging.onClickEditStaging(stages)">
																	<i class="fa fa-pencil"></i>
																</a>
																<!--  <a class="action-entity delete-entity" href ng-click="staging.onClickRemoveStaging($index)">
																<i class="fa fa-times"></i>
															</a> -->
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12" ng-hide="true">
								<div class="custom-widget">
									<div class="widget-header">Personal</div>
									<div class="widget-content">
										<div class="row">
											<div class="col-sm-12 custom-data-table-wrapper">
												<table width="100%" datatable="ng" dt-options="persons.dtOptions" dt-column-defs="persons.dtColumnDefs" class="table table-striped">
													<thead>
														<tr>
															<th>NAME</th>
															<th>FAMILY NAME</th>
															<th>GENDER</th>
															<th>STATUS</th>
															<th class="text-center">VIEW/EDIT</th>
														</tr>
													</thead>
													<tbody class="case-diary-list">
														<tr ng-repeat="person in persons.list">
															<td>{{ person.name }}</td>
															<td>{{ person.familyname }}</td>
															<td>{{ person.gender }}</td>
															<td>{{ person.status }}</td>
															<td class="text-center">
																<a class="action-entity view-entity" href ng-click="staging.onClickEditStaging(person)">
																	<i class="fa fa-pencil"></i>
																</a>
																<!-- <a class="action-entity delete-entity" href ng-click="persons.onClickRemovePerson($index)"><i class="fa fa-times"></i></a> -->
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--  Enrich Main Wrapper Ends  -->

		</div>
		<!--  Data Curation Wrapper Ends  -->

		<script>
			$(document).ready(function () {
				/*Custom Scroll Bar*/
				$('#saved-searches .panel-body').mCustomScrollbar({
					axis: "y"
				});
				$('#recent-searches .panel-body').mCustomScrollbar({
					axis: "y"
				});
				$('#widgetFilter').mCustomScrollbar({
					axis: "y",
					autoHideScrollbar: true
				});
				$('.company-person-collapse-result').mCustomScrollbar({
					axis: "y"
				});
				$('#nodeSearchContentParent').mCustomScrollbar({
					axis: "y",
					autoHideScrollbar: true
				});
				$('.searched-tag-wrapper').mThumbnailScroller({
					axis: "x"
				});
			});
		</script>
		<script>
			function getTokenBySession() {
				var userDetails = '<%=new ObjectMapper().writeValueAsString((Object)session.getAttribute("userInfo"))%>';
				userDetails = JSON.parse(userDetails);
				if (userDetails.token != undefined && userDetails.token != null) {
					return userDetails;
				} else {
					return null;
					window.location.href = '';
				}
			}
		</script>