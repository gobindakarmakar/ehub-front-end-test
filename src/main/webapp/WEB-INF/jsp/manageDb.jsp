<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<!-- 	<div ng-include="callSubMenuPath()" ng-controller="subMenuController"></div> -->
<!--  Super Sub Header Wrapper Starts  -->
<%@include file="supersubmenu.jsp"%>
<!--  Sub Header Wrapper Ends  -->

        <!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="activiti-task-wrapper reports-wrapper user-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">

                                <!--  Aside Wrapper Starts  -->
                                <aside class="aside-wrapper text-uppercase">
                                    <div class="form-group has-success has-feedback">
                                        <input type="text" class="form-control" placeholder="Search" />
                                        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-7"><h3>Jobs</h3></div>
                                        <div class="col-sm-5">  
                                            <div class="dropdown clearfix">
                                              <a href="javascript:void(0);" uib-dropdown-toggle class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											</a>
											<ul uib-dropdown-menu class="dropdown-menu" aria-labelledby="dLabel">
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
											</ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel custom-panel active groups-panel" ng-repeat="manageDbdata2 in manageDb.manageDbData2">
                                        <div class="panel-heading db-heading">
                                            <i class="fa fa-database"></i><i class="db-icon-left" ng-class="{'{{manageDbdata2.class}}' :manageDbdata2.class}"></i>{{manageDbdata2.name}}<strong>{{manageDbdata2.value}}</strong>
                                        </div>
                                    </div>

                                </aside>
                                <!--  Aside Wrapper Ends  -->

                            </div>
                            <div class="col-md-9 col-sm-8">

                                <!--  Create New Task Wrapper Starts  -->
                                <div class="create-new-task-wrapper">
                                    <div class="create-new-task-inner-wrapper">
                                        <!--  Notification Panel Starts  -->
                                        <div class="panel custom-panel notification-panel">
                                            <div class="panel-heading">
                                                <div class="notification-header">
                                                    <a href="javascript:void(0);" class="fa fa-database server-link db-link"></a>
                                                    <h3>ACT_EVT_LOG</h3>
                                                    <ul class="list-inline">
                                                        <li>14 Rows</li>
                                                    </ul>
                                                </div>                                               
                                            </div>
                                        </div>
                                        <!--  Notification Panel Ends  -->

                                        <!--  Associated Groups Panel Starts  -->
                                        <div class="panel custom-panel associated-people-panel associated-groups-panel">
                                            <div class="panel-body investigation-wrapper">        
                                                         <div class="table-responsive">
                                                    <table class="table table-condensed table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="25%">Id</th>
                                                                <th width="25%">Rev_</th>
                                                                <th width="25%">Name_</th>
                                                                <th width="25%">Type_</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                          
                                                            <tr ng-repeat="manageDbdata in manageDb.manageDbData">
                                                                <td>{{manageDbdata.Id}}</td>
                                                                <td>{{manageDbdata.Rev_}}</td>
                                                                <td>{{manageDbdata.Name_}}</td>
                                                                <td>{{manageDbdata.Type_}}</td>
                                                            </tr>                                                           
                                                                                                                    </tr>                                                           
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Associated Groups Panel Ends  -->

                                    </div>
                                    <!--  Related Content Panel Ends  -->

                                </div>
                                <!--  Create New Task Wrapper Ends  -->

                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->	    
</div>
	