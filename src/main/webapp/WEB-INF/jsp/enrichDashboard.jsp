<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  Sub Menu Starts  -->
<%@include file="common/submenu.jsp"%>
<!--  Sub Menu Ends  -->

<!--  Main Contents Wrapper Starts  -->
<div id="enrich-dashboard" class="main-contents-wrapper">

    <upload visible="showModalUpload"></upload>
    <uploadquestionnaire visible="showModalUploadQuestionnaire"></uploadquestionnaire>
    <uploadkyc visible="showModalUploadKYC"></uploadkyc>

    <!--  Loader Starts  -->
         <div ng-show="disableSearchButton" class="custom-spinner full-page-spinner">
             <i class="fa fa-spinner fa-spin fa-3x"></i>
         </div>
    <!--  Loader Ends  -->

    <!--  Enrich Dashboard Wrapper Starts  -->
    <div class="dashboard-wrapper enrich-dashboard-wrapper">

        <!--  Enrich Advanced Search Wrapper Starts  -->
        <div id="enrich-advanced-search" class="enrich-advanced-search-wrapper">

            <!--  Enrich Search Wrapper Starts  -->
            <div class="enrich-search-wrapper animated bounceInUp" gridster='gridsterConfiguration' ng-show="!isSearchStarted">
                <ul>

                    <!--  Set Search Criteria Widget Starts  -->
                    <li class="save-search-item" gridster-item size-x="1" size-y="1">
                        <div class="widget-content">
                            <h3 class="title">Set search Criteria</h3>
                            <div class="panel panel-default custom-panel" id="saved-searches">
                                <div class="panel-heading">
                                    <div class="container-fluid">
                                        <h4>SAVED SEARCHES</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                	 <div class="col-md-12" id="fixed" when-scrolled="loadMore()">
                                        <p ng-repeat="data in searchHistory | orderBy: '-updatedOn' " ng-click="displayResult(data)">{{data.searchData}}</p>
                                    </div>
                                    <!-- <div class="col-md-12"  ng-repeat="data in searchHistory | orderBy: '-updatedOn' " ng-click="displayResult(data)">
                                        <p>{{data.searchData}}</p>
                                    </div> -->
                                    <div ng-show="searchHistory.length === 0">
                                    No saved searches found!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--  Set Search Criteria Widget Starts  -->

                    <!--  Entity Widget Starts  -->
                    <li class="entities-grid-item" gridster-item size-x="5" size-y="2">
                        <div class="widget-content">

                            <!--  Panel Entity Starts  -->
                            <div class="panel panel-default" id="panelentities">
                                <div class="panel-body">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                        <!--  Custom Searchbar Wrapper Starts  -->
                                        <div class="panel panel-default custom-searchbar-wrapper">

                                            <!--  Custom Searchbar Wrapper Starts  -->
                                            <div class="btn-group headpart-search">
                                                <button type="button" class="btn btn-primary px-3" ng-click="delaySearchutility()" ng-disabled="!hasSourceDetails && !hasOpenSourceDetails && !hasPeopleDetails && !hasCompanyDetails && !hasLocationAdded && !hasRulesAdded && !hasCaseAdded && !hasAccountAdded && !hasIdentifiersAdded || disableSearchButton">
                                                    <span><i class="fa fa-file-search"></i></span> <span>SEARCH</span>
                                                </button>
                                            </div>
                                            <!--  Custom Searchbar Wrapper Starts  -->

                                            <!--  Custom Searchbar Wrapper Starts  -->
                                            <div class="custom-search-tags clearfix" role="tab" id="headingcompany">
                                                <div class="custom-switch switch-label-wrapper">
                                                    <div class="switch-label">Entity Types</div>
                                                    <div class="custom-dropdown-wrapper">
                                                        <div class="btn-group" uib-dropdown>
                                                            <button id="single-button" type="button" class="btn btn-primary custom-search-btn clearfix" uib-dropdown-toggle>
                                                                {{entityType}} <span class="fa fa-angle-down pull-right"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" uib-dropdown-menu>
                                                                <li role="menuitem"><a href ng-click="onChangeEntityType('Company')"><span class="icon"><i class="fa lh-27 fa-briefcase"></i></span>Company</a></li>
                                                                <li role="menuitem"><a href ng-click="onChangeEntityType('Person')"><span class="icon"><i class="fa lh-27 fa-user-o"></i></span>Person</a></li><!-- ng-click="onChangeEntityType('Person')" -->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0);" class="fa fa-plus add-link"></a>
                                                </div>
                                            </div>
                                            <!--  Custom Searchbar Wrapper Starts  -->

                                            <!--  Custom Search Tags Starts  -->
                                            <div class="custom-search-tags clearfix" role="tab">
                                                <div class="custom-switch switch-label-wrapper">
                                                    <div class="switch-label">Domain</div>
                                                    <div class="custom-dropdown-wrapper">
                                                        <div class="btn-group" uib-dropdown>
                                                            <button id="single-button" type="button" class="btn btn-primary custom-search-btn clearfix" uib-dropdown-toggle>
                                                                {{domainSelected}} <span class="fa fa-angle-down pull-right"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" uib-dropdown-menu>
                                                                <li role="menuitem" ng-click="onChangeDomainType(domain.name)" ng-repeat="domain in domainCategoryList"><a href><span class="icon">{{domain.key}}</span>{{domain.name}}</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0);" class="fa fa-plus add-link"></a>
                                                </div>
                                            </div>
                                            <!--  Custom Search Tags Ends  -->
											<!--  Custom Search Tags Starts  -->
                                            <!-- <div class="custom-search-tags clearfix" role="tab">
                                                <div class="custom-switch switch-label-wrapper">
                                                    <div class="switch-label">Jurisdictions</div>
                                                    <div class="custom-dropdown-wrapper">
                                                        <div class="btn-group" uib-dropdown>
                                                            <button id="single-button" type="button" class="btn btn-primary custom-search-btn clearfix" uib-dropdown-toggle>
                                                                {{jurisdictionSelected}} <span class="fa fa-angle-down pull-right"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" uib-dropdown-menu ng-model="selectedJurisdiction">
                                                                <li role="menuitem" ng-click="onChangeJurisdictionType(jurisdiction.name, jurisdiction.key)" ng-repeat="jurisdiction in jurisdictionList"><a href><span class="icon">{{jurisdiction.key}}</span>{{jurisdiction.name}}</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0);" class="fa fa-plus add-link"></a>
                                                </div>
                                            </div> -->
                                            <!--  Custom Search Tags Ends  -->
                                            <!--  Custom Add Tags Starts  -->
                                            <!-- below ng-click() for <a> tag and tooltip-enable has been commmented to remove functionality @ram singh 10-jan-2018 -->
                                            <div class="custom-add-tags clearfix">
                                                <div class="custom-switch-tags">
                                                    <div class="switch-label custom-switch">Add more</div>
                                                    <div class="custom-tags-wrapper">
                                                        <div class="panel-heading" role="tab" id="headinglocation" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="!hasPeopleDetails && !hasCompanyDetails">
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add" ng-click="sourceParamUpdate()"> <span class="fa-stack fa-md"><i class="fa fa-dot-circle-o fa-1x fa-stack-1x"></i></span>&nbsp;SOURCE TYPES <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapselocation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headinglocation">
                                                            <div class="panel-body">Source Types here</div>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingOpenSourse" ng-if="isSourceTypeAsOSINIT">
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add" ng-click="openSourceParamUpdate()">
                                                                    <span class="fa-stack fa-md"> <i class="fa fa-dot-circle-o fa-1x fa-stack-1x"></i></span>
                                                                    &nbsp;OPEN SOURCE <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOpenSource" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOpenSource">
                                                            <div class="panel-body">Open Source here</div>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingrules" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="false"><!-- tooltip-enable="!hasPeopleDetails && !hasCompanyDetails" -->
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add disabled" ><!-- ng-click="locationParamUpdate()" --> <span class="fa-stack fa-md"> <i class="fa fa-map-marker fa-1x fa-stack-1x"></i></span>&nbsp;LOCATION <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapserules" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingrules">
                                                            <div class="panel-body">Location here</div>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingrules" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="false"><!-- tooltip-enable="!hasPeopleDetails && !hasCompanyDetails" -->
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add disabled" ><!-- ng-click="rulesParamUpdate()" --> <span class="fa-stack fa-md"> <i class="fa fa-list-ul fa-1x fa-stack-1x"></i></span>&nbsp;RULES <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapserules" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingrules">
                                                            <div class="panel-body">Rules here</div>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingcase" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="false"><!-- tooltip-enable="!hasPeopleDetails && !hasCompanyDetails" -->
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add disabled"><!--  ng-click="caseParamUpdate()" --> <span class="fa-stack fa-md"><i class="fa fa-suitcase fa-1x fa-stack-1x"></i></span>&nbsp;CASE <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsecase" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingcase">
                                                            <div class="panel-body">Case here</div>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingaccount" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="false"><!-- tooltip-enable="!hasPeopleDetails && !hasCompanyDetails" -->
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add disabled" > <!-- ng-click="accountParamUpdate()" --><span class="fa-stack fa-md"><i class="fa fa-database fa-1x fa-stack-1x"></i></span>&nbsp;ACCOUNT <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-heading" role="tab" id="headingpi" uib-tooltip="Please select Person or Company first!" tooltip-placement="top" tooltip-trigger="'outsideClick'" tooltip-enable="false"><!-- tooltip-enable="!hasPeopleDetails && !hasCompanyDetails" -->
                                                            <h4 class="panel-title">
                                                                <a class="custom-enrich-add disabled"> <!--  ng-click="identifiersParamUpdate()" --><span class="fa-stack fa-md"><i class="fa fa-mobile fa-2x fa-stack-2x"></i></span>&nbsp;PERSONAL IDENTIFIERS <i class="fa fa-plus"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Custom Add Tags Ends  -->

                                        </div>
                                        <!--  Custom Searchbar Wrapper Ends  -->

                                    </div>
                                </div>
                            </div>
                            <!--  Panel Entity Ends  -->

                            <!--  Node Search Content Starts  -->
                            <div id="nodeSearchContentParent">
                                <div id="nodeSearchContent">
                                    <div class="node-param-content clearfix" ng-class="{'no-border': nodeSearchDatas.length === 1}" ng-repeat="nodeData in nodeSearchDatas">
                                        <div class="node-head" ng-click="updateNodeData(nodeData.name)">
                                            <span>{{nodeData.name}}</span>
                                        </div>
                                        <div class="search-string-values search-string-domain">
                                            <div class="search-string-item middle" ng-repeat="content in nodeData.content track by $index">
                                                <span>{{content}}</span>
                                            </div>
                                            <div class="remove-param" ng-click="removeUpdateNodeData($index)">
                                                <span><i class="fa fa-times" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  Node Search Content Ends  -->

                        </div>
                    </li>
                    <!--  Entity Widget Ends  -->

                    <!--  Recent Search Widget Starts  -->
                    <li class="recent-search-wrapper" gridster-item size-x="1" size-y="1">
                        <div class="widget-content">
                            <div class="panel panel-default custom-panel" id="recent-searches">
                                <div class="panel-heading">
                                    <div class="container-fluid">
                                        <h4>RECENT SEARCHES</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12" ng-repeat="data in recentsearch_keyword"   ng-click="displayResult(data)" >
                                        <p>{{data.searchData}}</p>
                                    </div>
                                    <div ng-show="recentsearch_keyword.length === 0">
                                    No recent searches found!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--  Recent Search Widget Ends  -->

                </ul>
            </div>
            <!--  Enrich Search Wrapper Ends  -->

            <!--  Custom Search Wrapper Starts  -->
            <div class="custom-search-wrapper animated bounceInUp" gridster='gridsterConfiguration' ng-show="isSearchStarted">
                <ul>

                    <!--  Custom Grid Item Starts  -->
                    <li class="custom-grid-item" gridster-item size-x="6" size-y="2">
                        <div class="widget-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel clearfix criteria-search-wrapper" id="criteria-searches">
                                            <h4>Criteria Search</h4>
                                            <div class="clearfix searched-tag-wrapper">
                                                <div ng-repeat="nodeData in nodeSearchDatas" class="pull-left search-string-values">
                                                    <div class="search-string-item" ng-repeat="content in nodeData.content">{{content}}</div>
                                                </div>
                                            </div>
                                            <div class="searched-operation-btns">
                                                <button class="btn btn-default search-tag-edit" ng-click="editSearchCriteria()">Edit</button>
                                                <button class="btn btn-primary search-tag-save" ng-click="savefavioutesearch()">Save</button>
                                                <button class="btn btn-default search-tag-cancel" ng-click="cancelSearchCriteria()">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--  Filter Wrapper Starts  -->
                                <div class="row filter-wrapper">

                                    <!--  Panel Filter Starts  -->
                                    <div class="col-sm-4 panel-filter">
                                        <div class="panel custom-panel" id="filter-by-searches">
                                            <div class="panel-heading"><h4>Filter By</h4></div>
                                            <div class="panel-body company-person-collapse-result">
                                                <div class="collapse-wrapper keyword-wrapper">
                                                    <div class="collapse-header" ng-click="isCollapsed = !isCollapsed">Keyword</div>
                                                    <!-- <input type="text" ng-model="keywordData" ng-keyup="filterSearchResult(keywordData, 'keyword')"> -->
                                                    <input type="text" autocomplete="off" ng-model="keywordData">
                                                </div>
                                                <div class="collapse-wrapper" ng-show="personFilterList.length > 0">
                                                    <div class="collapse-header" ng-click="isCollapsed = !isCollapsed">
                                                        <span>Person</span>
                                                        <i class="fa fa-angle-down icon" ng-if="isCollapsed"></i>
                                                        <i class="fa fa-angle-up icon" ng-if="!isCollapsed"></i>
                                                    </div>
                                                    <div class="collapse-body person-collapse-result" uib-collapse="isCollapsed">
                                                        <div class="checkbox-inline" ng-repeat="person in personFilterList">
                                                            <input type="checkbox" id="person-list-{{$index}}" class="custom-checkbox" ng-model="personData" ng-change="FilterTheEntity(person)">
                                                            <label for="person-list-{{$index}}">{{person}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="collapse-wrapper" ng-show="companyFilterList.length > 0">
                                                    <div class="collapse-header" ng-click="isCollapsed2 = !isCollapsed2">
                                                        <span>Company</span>
                                                        <i class="fa fa-angle-down icon" ng-if="isCollapsed2"></i>
                                                        <i class="fa fa-angle-up icon" ng-if="!isCollapsed2"></i>
                                                    </div>
                                                    <div class="collapse-body company-collapse-result" uib-collapse="isCollapsed2">
                                                        <div class="checkbox-inline" ng-repeat="company in companyFilterList track by $index">
                                                            <input type="checkbox" id="company-list-{{$index}}" class="custom-checkbox" ng-model="companyData" ng-change="FilterTheEntity(company)">
                                                            <label for="company-list-{{$index}}" uib-popover="{{company| uppercase}}" popover-class="top-pop-wrapper mxw-200 ws-normal top-pop" popover-trigger="'mouseenter'" popover-append-to-body="true" popover-placement="top">{{company}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  Panel Filter Ends  -->

                                    <!--  Results Wrapper Starts  -->
                                    <div class="col-sm-8 results-wrapper">
                                        <div ng-if="!isSearchFilteredStarted" class="col-md-12 d-flex search-header animated bounceInUp ai-c">
                                            <h3 class="d-ib"> {{entitiesSearchResolveResult.length}}  RESULTS <!-- {{entitiesSearchResolveResult.length}} --></h3><span ng-if="enrichObject.loadingText" class="mar-l25  pad-l10 text-dark-blue">Loading..</span>
                                        </div>
                                        <div ng-if="isSearchFilteredStarted" class="col-md-12 d-flex search-header ai-c animated bounceInUp">
                                            <h3 class="d-ib">{{fileterdDisplayedData2.length}} RESULTS <!-- {{entitiesFilteredResult.length}} --></h3><span ng-if="enrichObject.loadingText" class="mar-l25 pad-l10 text-dark-blue">Loading..</span>
                                        </div>

                                        <!--  Widget Filter Starts  -->
                                        <div id="widgetFilter">

                                            <!--  Search Results Wrapper Starts  -->
                                            <div class="col-sm-12 search-results-wrapper animated bounceInUp" ng-repeat="data in entitiesSearchResolveResult | orderBy:entitySiginificance | filter: keywordData as filteredDataLength   " ng-if="!isSearchFilteredStarted">
                                                <div class="row" ng-if="!isSearchResultNoData">
                                                    <div class="col-md-2 col-sm-3  circle-image-wrapper">
                                                       <!-- <img class="img-responsive brand-logo" src="assets/images/enrich/blackswan_tech_logo.png" ng-if="$first"> -->
                                                        <div class="brand-title" ng-if="data.image === '' && !$first">
                                                            <span>{{data.displayName}}</span>
                                                        </div> 
                                                        <img class="img-responsive brand-logo" ng-src="{{data.image}}" alt="fetcher_logo" ng-if="data.image !== ''"></img>
                                                    </div>
                                                    <div class="col-md-8 col-sm-7 center-content-wrapper" ng-click="onClickFetcherGoToEntity($index, 'resolve',data)">
                                                        <h4>{{data.name || data["vcard:organization-name"]}} <a href="javascript:void(0);" class="mar-t5 mar-l5 mar-r25 f-15"   popover-trigger="'mouseenter'" popover-placement="bottom" ng-class=" data.entitySiginificance ?  'text-dark-blue' : 'hidden'  " ng-model="mySignificant" >
                                                            <i class="fa fa-star lh-18 va-m" aria-hidden="true"></i>
                                                        </a>  </h4>
                                                        <h5>{{ data["bst:description"]}}</h5>
                                                        <h5 ng-if="data['mdaas:RegisteredAddress'].country || data['mdaas:RegisteredAddress'].fullAddress">COUNTRY: {{data["mdaas:RegisteredAddress"].country}} | FULL ADDRESS: {{data["mdaas:RegisteredAddress"].fullAddress}}</h5>
                                                        <h5 ng-if="data.hasURL">WEBSITE: {{data.hasURL}}</h5>
                                                        
                                                        <!-- <p>FULL ADDRESS: {{data["mdaas:RegisteredAddress"].fullAddress}}</p> -->
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 btn-content-wrapper btn-right-wrapper" ng-if="$index !== 1">
                                                        <div class="btn-wrapper">
                                                            <button class="btn btn-circle" ng-click="showBstNamedEntity(data, $index)" title="BST Visualizer" ng-if="!$first && data['bst:description']">
                                                                <i class=" fa fa-eye fa-2x "></i>
                                                            </button>
                                                            <a class="btn btn-circle btn-link-circle" href="javascript:void(0);" ng-click="goToNetworkAnalysis(data['vcard:organization-name'])" target="_blank" title="Network analysis">
                                                                <i class="fa fa-sitemap fa-2x"></i>
                                                            </a>
                                                            <a class="btn btn-circle btn-link-circle btn-link-center" href="{{data['bst:registryURI']}}" target="_blank" title="Source" ng-if="!$first">
                                                                <i class="fa fa-link fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-if= "false">
                                                    <div class="col-md-2 col-sm-3  circle-image-wrapper">
                                                        <img class="img-responsive brand-logo" ng-src="{{data.image}}" alt="fetcher_logo" />
                                                    </div>
                                                    <div class="col-md-8 col-sm-7 center-content-wrapper" ng-click="navigateToDFDPage()">
                                                        <h4>{{data.name || data["vcard:organization-name"]}}</h4>
                                                        <h5 ng-if="data['mdaas:RegisteredAddress'].country || data['mdaas:RegisteredAddress'].fullAddress">COUNTRY: {{data["mdaas:RegisteredAddress"].country}} | FULL ADDRESS: {{data["mdaas:RegisteredAddress"].fullAddress}}</h5>
                                                        <h5 ng-if="data.hasURL">WEBSITE: {{data.hasURL}}</h5>
                                                        <!-- <p>FULL ADDRESS: {{data["mdaas:RegisteredAddress"].fullAddress}}</p> -->
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 btn-content-wrapper btn-right-wrapper">
                                                        <a class="btn btn-circle btn-link-circle" href ng-click="navigateToDFDPage()" target="_blank" title="Source">
                                                            <img class="img-responsive" src="assets/images/network_force_collapse.png" alt="fetcher_logo" />
                                                        </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            <!--  Search Results Wrapper Ends  -->

                                            <!--  Search Results Wrapper Starts  -->
                                            <div class="col-sm-12 search-results-wrapper animated bounceInUp" ng-repeat="data in entitiesFilteredResult | filter: keywordData as fileterdDisplayedData2" ng-if="isSearchFilteredStarted">
                                                <div class="row" ng-if="data.type !== 'dfd'">
                                                    <div class="col-md-2 col-sm-3  circle-image-wrapper">
                                                        <div class="brand-title" ng-if="data.image === ''">
                                                            <span>{{data.displayName}}</span>
                                                        </div>
                                                        <img class="img-responsive brand-logo" ng-src="{{data.image}}" alt="fetcher_logo" ng-if="data.image !== ''" />
                                                    </div>
                                                    <div class="col-md-8 col-sm-7 center-content-wrapper" ng-click="onClickFetcherGoToEntity($index, 'filter-result',data)">
                                                        <h4>{{data.name || data["vcard:organization-name"]}}<a href="javascript:void(0);" class="mar-t5 mar-l5 mar-r25 f-15"   popover-trigger="'mouseenter'" popover-placement="bottom" ng-class=" data.entitySiginificance ?  'text-dark-blue' : 'hidden'  " ng-model="mySignificant" >
                                                            <i class="fa fa-star lh-18 va-m" aria-hidden="true"></i>
                                                        </a>  </h4>
                                                        <h5>{{ data["bst:description"]}}</h5>
                                                        <h5>COUNTRY: {{data["mdaas:RegisteredAddress"].country}} | FULL ADDRESS: {{data["mdaas:RegisteredAddress"].fullAddress}}</h5>
                                                        <h5 ng-if="data.hasURL">WEBSITE: {{data.hasURL}}</h5>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 btn-content-wrapper btn-right-wrapper">
                                                        <div class="btn-wrapper">
                                                            <button class="btn btn-circle" ng-click="showBstNamedEntity(data, $index)" ng-if="data['bst:description']  && data.image !== 'assets/images/enrich/blackswan_tech_logo.png' " title="BST Visualizer">
                                                                <i class=" fa fa-eye fa-2x"></i>
                                                            </button>
                                                            <a class="btn btn-circle btn-link-circle" href="javascript:void(0);"   ng-click="goToNetworkAnalysis(data['vcard:organization-name'])" target="_blank" title="Network analysis">
                                                                <i class="fa fa-sitemap fa-2x"></i>
                                                            </a>
                                                            <a class="btn btn-circle btn-link-circle btn-link-center" href="{{data['bst:registryURI'] || data.source}}" ng-if="data['bst:registryURI'] && data.image !== 'assets/images/enrich/blackswan_tech_logo.png'" target="_blank" title="Source">
                                                                <i class="fa fa-link fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-if="data.type === 'dfd'">
                                                    <div class="col-md-2 col-sm-3  circle-image-wrapper">
                                                        <img class="img-responsive brand-logo" ng-src="{{data.image}}" alt="fetcher_logo" />
                                                    </div>
                                                    <div class="col-md-8 col-sm-7 center-content-wrapper" ng-click="navigateToDFDPage()">
                                                        <h4>{{data.name}}</h4>
                                                        <h5>{{data.title}}</h5>
                                                        <p>{{data.details}}</p>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 btn-content-wrapper btn-right-wrapper">
                                                        <a class="btn btn-circle" href ng-click="navigateToDFDPage()" target="_blank" title="Source">
                                                            <img class="img-responsive" src="assets/images/network_force_collapse.png" alt="fetcher_logo" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Search Results Wrapper Ends  -->

                                        </div>
                                        <!--  Widget Filter Ends  -->

                                    </div>
                                    <!--  Results Wrapper Ends  -->

                                </div>
                                <!--  Filter Wrapper Ends  -->

                            </div>
                        </div>
                    </li>
                    <!--  Custom Grid Item Ends  -->

                </ul>
            </div>
            <!--  Custom Search Wrapper Ends  -->

        </div>
        <!--  Enrich Advanced Search Wrapper Ends  -->
    </div>
    <!--  Enrich Dashboard Wrapper Ends  -->

    <updatesource visible="showSourceUpdate"></updatesource>
    <updatepeople visible="showPeopleUpdate"></updatepeople>
    <confirmunchecking visible="showConfirmChecking"></confirmunchecking>
    <updatecompany visible="showCompanyUpdate"></updatecompany>
    <updatelocation visible="showLocationUpdate"></updatelocation>
    <updaterules visible="showRulesUpdate"></updaterules>
    <updatecase visible="showCaseUpdate"></updatecase>
    <updateaccount visible="showAccountUpdate"></updateaccount>
    <updateidentifiers visible="showIdentifiersUpdate"></updateidentifiers>

	<!--  Custom Menu Starts  -->
		<!-- <wheel-nav-context-menu contextmenuname="enrichDashboard.contentMenu" contextmenuname0="enrichDashboard.contentMenu0" contextmenuname1="enrichDashboard.contentMenu1" contextmenuname2="enrichDashboard.contentMenu2" contextmenuname3="enrichDashboard.contentMenu3" contextmenuname4="enrichDashboard.contentMenu4" contextmenuname5="enrichDashboard.contentMenu5"></wheel-nav-context-menu> -->
    <!--  Custom Menu Ends  -->

</div>
<!--  Main Contents Wrapper Ends  -->

<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('#saved-searches .panel-body').mCustomScrollbar({
        callbacks:{
            onScrollStart:function() {
           angular.element('#saved-searches').scope().loadMore();
        }
    },
           axis: "y"
       });
       $('#recent-searches .panel-body').mCustomScrollbar({
           axis: "y"
       });
       $('#widgetFilter').mCustomScrollbar({
           axis: "y",
           autoHideScrollbar: true
       });
       $('.collapse-body').mCustomScrollbar({
           axis: "y"
       });
       $('#nodeSearchContentParent').mCustomScrollbar({
           axis: "y",
           autoHideScrollbar: true
       });
       $('.searched-tag-wrapper').mThumbnailScroller({
           axis: "x"
       });

       $('#enrichInput_container').find('ul').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: "minimal"
       });
       $('#enrichInput_container').find('ul').addClass('h-200')
   });
</script>
<script type="text/ng-template" id="customTemplate.html">
	<a>
		<img ng-src="{{match.model.flag}}" width="16">
		<span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
	</a>
</script>
<style>

</style>