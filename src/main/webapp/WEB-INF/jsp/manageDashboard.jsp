<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	<%@include file="common/submenu.jsp"%>

	<!--  Main Contents Wrapper Starts  -->
	<div id="manage-dashboard" class="main-contents-wrapper">

		<!--  Manage Dashboard Wrapper Starts  -->
		<div class="dashboard-wrapper manage-dashboard-wrapper">


			<!--  Welcome Hub Wrapper Starts  -->
			<div class="content-wrapper welcome-hub-wrapper">
				<div class="container">
					<div class="welcome-hub-wrapper">
						<h2 class="text-uppercase text-center">Welcome To The Hub</h2>
						<form class="welcome-search">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="SEARCH">
								<span class="fa fa-search"></span>
							</div>
						</form>
						<div class="hub-list-wrapper text-center">
							<div class="row">
								<div class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ui-sref="userManagement" ng-disabled="true">
											<div class="text-center">
												<i class="fa fa-users"></i>
											</div>
											<h5 class="text-center text-uppercase">User Management</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="users" ng-disabled="true">Users</button> 
											<button ui-sref="groups" ng-disabled="true">Groups</button> 
											<button ui-sref="roles" ng-disabled="true">Roles</button> 
											<button ui-sref="passwords" ng-disabled="true">Passwords</button> 
											<button ui-sref="monitor" ng-disabled="true">Monitor</button> 
											<button ui-sref="accessControls" ng-disabled="true">Access Controls</button>
										</p>
									</div>
								</div>
								<div id="process-title-menu-workflow" class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ng-click="onWorkFlow();">
											<div class="text-center">
												<i class="fa fa-flag"></i>
											</div>
											<h5 class="text-center text-uppercase">Big Data Workflow</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="businessRules" ng-disabled="true">Business Rules</button>, 
											<button ui-sref="bigDataRules" ng-disabled="true">Big Data Rules</button>, 
											<button ng-click="onWorkFlow();">Workflows</button>, 
											<button ui-sref="ontology" ng-disabled="true">Ontology</button>, 
											<button ui-sref="logics" ng-disabled="true">Logics</button>
										</p>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ui-sref="appManager" ng-disabled="true">
											<div class="text-center">
												<i class="fa fa-th"></i>
											</div>
											<h5 class="text-center text-uppercase">App Manager</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="myApps" ng-disabled="true">My apps</button>,
											 <button ui-sref="store" ng-disabled="true">Store</button>,
											  <button ui-sref="appConfiguration" ng-disabled="true">App configuration</button>, 
											  <button ui-sref="licenseManager" ng-disabled="true">License Manager</button>, 
											  <button ui-sref="socialManager" ng-disabled="true">Social Manager</button>
										</p>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ui-sref="dataManagement" ng-disabled="true">
											<div class="text-center">
												<i class="fa fa-database"></i>
											</div>
											<h5 class="text-center text-uppercase">Data Management</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="myDataSources" ng-disabled="true">My data sources</button>, 
											<button ui-sref="addNewSources" ng-disabled="true">Add new sources</button>, 
											<button ui-sref="manageSources" ng-disabled="true">Manage sources</button>, 
											<button ui-sref="exportImport" ng-disabled="true">Export/Import</button>
										</p>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ui-sref="systemMonitoring" ng-disabled="true">
											<div class="text-center">
												<i class="fa fa-line-chart"></i>
											</div>
											<h5 class="text-center text-uppercase">System Monitoring</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="events" ng-disabled="true">Events</button>, 
											<button ui-sref="hosts" ng-disabled="true">Hosts</button>, 
											<button ui-sref="response" ng-disabled="true">Response</button>, 
											<button ui-sref="process" ng-disabled="true">Process</button>, 
											<button ui-sref="fields" ng-disabled="true">Fields</button>
										</p>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="hub-list usermanagement">
										<button ui-sref="generalSettings" ng-disabled="true">
											<div class="text-center">
												<i class="fa fa-cog"></i>
											</div>
											<h5 class="text-center text-uppercase">General Settings</h5>
										</button>
										<p class="text-uppercase text-left">
											<button ui-sref="portalSettings" ng-disabled="true">Portal Settings</button>, 
											<button ui-sref="serveSettings" ng-disabled="true">Serve Settings</button>, 
											<button ui-sref="systemProperties" ng-disabled="true">System Properties</button>, 
											<button ui-sref="customFields" ng-disabled="true">Custom Fields</button>, 
											<button ui-sref="portalInstances" ng-disabled="true">Portal Instances</button>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  Welcome Hub Wrapper Ends  -->
		</div>
	</div>
</div>
