<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<!--  Header Wrapper Starts  -->
<header class="header-wrapper" ng-controller="TopPanelController">
    <div class="container-fluid">
        <nav class="navbar navbar-fixed-top" ng-class="{'docparser-header':path == 'docParserPage' || path == 'landing','bst_element_header' : path == 'dualCardHolderDetails' ||path == 'systemSettings' || path == 'sourceManagement'|| path == 'entity' || path == 'entityCompanyNew'|| currentState=='process'}">

            <!--  Navbar Header Starts  -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" ng-init="isCollapsed = true" ng-click="isCollapsed = !isCollapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand element-navbar-logo" ng-click="navigateOtherPage('domain');">
                    <img ng-src="{{logo}}">
                    <span>Element</span>
                </a>
            </div>
            <!--  Navbar Header Ends  -->

            <!--  Navbar Collapse Starts  -->
            <div id="navbar" class="collapse custom-select-wrapper navbar-collapse" uib-collapse="isCollapsed">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-search mar-r15" uib-dropdown>
                        <div class="search-bar custom-select-dropdown" ng-controller="AdvancedSearchController">
                              <!--    <a ng-click="SearchNewRouting();" class="btn">
                                        <i class="fa fa-search nav-link-icon"></i>Search
                                    </a> -->
                            <!-- <a class="dropdown-toggle search-left" href="javascript:void(0);" uib-dropdown-toggle>
                                <i class="fa fa-bars nav-icon"></i>
                                <i class="fa fa-caret-down nav-icon"></i>
                            </a> -->
                            <!-- <input type="text" ng-keypress="SearchCompany($event)" ng-focus="!dsbl" ng-model="search_organization_name" uib-typeahead="company as company['vcard:organization-name'] for company in ceriLandingObject.getSuggestedName($viewValue)"  typeahead-template-url="customTemplate.html" typeahead-loading="loadingCompaniesNames" typeahead-no-results="noOrgResults" class="custom-input text-uppercase" typeahead-on-select="ceriSearchRouting($item,$event)" typeahead-focus-first="false">
                            {{company}} -->
                            <span class=" pull-right" ng-click="search_organization_name = ''"  style="cursor:pointer; position: absolute; top: 22px; right: 30px; z-index: 1;">
                                <i class="fa fa-times" ng-if="search_organization_name"></i>
                                <!--                          <button  ng-click="clearSearch()"  style="cursor:pointer">clear</button> -->
                            </span>
                            <div class="text-center custom-dropdown-menu" ng-show="loadingCompaniesNames"><i class="fa fa-spinner fa-pulse"></i></div>
                            <div ng-show="noOrgResults" class="custom-dropdown-menu">
                                <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                            </div>
                            <a href="javascript:void(0);" class="search-right">
                                <!-- <i class="fa fa-chevron-down nav-icon"></i> -->
                            </a>
                            <ul class="typeahead">
                                <li ng-repeat="a in names track by $index" ng-cloak ng-click="doSearch(a);">
                                    <a href="javascript:void(0);">{{a}}</a>
                                </li>
                            </ul>
                            <ul class="dropdown-menu navbar-dropdown-menu" uib-dropdown-menu role="menu">
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-bars"></i>ALL</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-group"></i>PEOPLE</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-building-o"></i>OFFICE, ORGANIZATION, COMPANY</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-map-marker"></i>LOCATION</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-suitcase"></i> CASES</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-list"></i> RULES</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-database"></i>ACCOUNT</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-user"></i>PERSONAL IDENTIFIER</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a  ng-click="SearchNewRouting();" class="btn">
                            <i class="fa fa-search nav-link-icon"></i><span class="topheader-menu">Search</span>
                        </a>
                    </li>
<!--                     <li id="event-calendar-dropdown" ng-click="toggleTabsDropdown('userEvents')"> -->
<!--                         <a class="btn" id="event-calendar-menu"> -->
<!--                             <i class="fa fa-calendar nav-link-icon"></i>Today -->
<!--                         </a> -->
<!--                     </li> -->
                     <li id="event-calendar-dropdown" ng-disabled="true">
                        <a class="btn" id="event-calendar-menu">
                            <i class="fa fa-calendar nav-link-icon"></i><span class="topheader-menu">Today</span>
                        </a>
                    </li>
                    <li ng-click="toggleTabsDropdown('clipboard')">
                        <a class="btn my-clipboard-nav">
                            <i class="fa fa-bookmark nav-link-icon"></i><span class="topheader-menu">My clipboard</span>
                        </a>
                    </li>
                    <li ng-click="toggleTabsDropdown('notification')" >
                        <a class="btn notification-menu">
                            <i class="fa fa-bell nav-link-icon" ></i><span class="topheader-menu">Notifications  </span>
                           <!--  <span class="count">{{checkCount}}</span>  -->
                        </a>
                    </li>
                    <li class="dropdown dropdown-profile" uib-dropdown dropdown-append-to-body ng-click="toggleTabsDropdown('logout')">
                        <a class="dropdown-toggle clearfix" href="javascript:void(0);" uib-dropdown-toggle>
                            <div class="pull-left user-name">
                                <span class="log-username">{{ehubObject.fullName|| ehubObject.email}}</span>
                                <span class="last-login">Last login: {{ehubObject.lastLoginDate| date: 'MM/dd/yyyy h:mm:ss a'}}</span>
                            </div>
                            <div class="pull-right user-icon">
                                <img class="img-responsive" ng-src="{{userImg}}" alt="userImg" />
                            </div>
                        </a>
                        <%-- <ul class="dropdown-menu top-panel-profile-dropdown-menu " uib-dropdown-menu role="menu" ng-show="showLogout"> --%>
                        <ul class="dropdown-menu top-panel-profile-dropdown-menu " uib-dropdown-menu role="menu">

                            <li>
                                <a href="javascript:void(0);" ng-disabled="true">
                                    <i class="fa fa-user"></i>PROFILE
                                </a>
                            </li>
                            <li>
                                <!-- <a href="javascript:void(0);" ng-disabled="true" >
                                    <i class="fa fa-cog"></i>SETTINGS
                                </a> -->
                           		<a href="javascript:void(0);" ng-disabled="true">
                                    <i class="fa fa-cog"></i>SETTINGS
                                </a> 
                            </li>
                            <li>
                                <a href="javascript:void(0);" ng-disabled="true">
                                    <i class="fa fa-gittip"></i>MY PREFERENCES
                                </a>
                            </li>
                            <li>
                                <a ng-click="onManage();">
                                    <i class="fa fa-server"></i>MANAGE
                                </a>
                            </li>
                            <li>
                                <a ng-click="logout()" href="javascript:void(0);">
                                    <i class="fa fa-sign-out"></i>LOG OUT
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--  Navbar Collapse Ends  -->

        </nav>
    </div>

    <!--  Notification Dropdown Starts  -->
    <div class="dropdown-menu-wrapper d-none notifications-dropdown-menu-wrapper" ng-controller="NotificationsController" ng-show="showNotification">
        <div class="notifications-container">
            <div class="clearfix context-menu">
                <div class="pull-left context-menu-item">
                    <span class="notifications-counter">You have total {{checkCount}} notifications today</span>
                </div>
                <div class=" pull-right context-menu-item">
                    <a href="javascript:void(0);">
                        <i class="fa fa-share-alt"></i>
                    </a>
                    <a href="javascript:void(0);">
                        <i class="fa fa-camera" ng-click='notificationFunctions.CaptureDiv("notificationList")'></i>
                    </a>
                    <a href="javascript:void(0);" ng-click = 'notificationFunctions.openFilterModal()'>
                        <i class="fa fa-filter"></i>
                    </a>
                </div>
            </div>
<!--            <div class="custom-collapse-wrapper">
                <ul class="notifications-list">
                    <li ng-repeat="notiInfo in notificationVariable.notificationList">
                        <div>
                            <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">{{notiInfo.subject}}</button>
                            <hr>
                            <div uib-collapse="isCollapsed">
                                <div class="well well-lg">{{notiInfo.body}}</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>-->
            <!--  Custom Accordion Starts  -->
            <div class="panel-group custom-accordion" id="notificationList" role="tablist" aria-multiselectable="true">
                 <div class="panel panel-default"  ng-repeat="notiInfo in notificationVariable.notificationList" >
                    <div class="panel-heading" role="tab" id="headingOne" ng-class="{ 'border-0': !isCollapsed }">
                        <h4 class="panel-title">
                            <a class="collapsed" ng-click="isCollapsed = !isCollapsed">
                              <b> {{notiInfo.subject}}</b>
                                <span class = 'pull-right fa fa-angle-down f-18'></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse"  uib-collapse="isCollapsed">
                        <div class="panel-body f-12">
                        	{{notiInfo.body}}
                        </div>
                    </div>
                </div>
               
            </div>
            <!--  Custom Accordion Ends  -->
        </div> 

    </div>
</div> 
</div>
<!--  Notification Dropdown Ends  -->


<!--  Clipboard Dropdown Starts  -->
<div class="dropdown-menu-wrapper clipboard-dropdown-menu-wrapper"  ng-controller="MyClipBoardController" ng-show="showMyClipboard">
    <div class="row search-clip-holder" >
        <div class="col-sm-5 search-clip-item">
            <p>Total {{documents.length}} Files</p>
            <!--  Progressbar List Starts -->
            <!-- <div class="list-unstyled progressbar-list">
                                            <div class="progressbar-list-item pad-t0">
                                                    <div class="progress progress-light-gray">
                                                            <div class="progress-bar progress-curious-blue" role="progressbar" aria-valuenow="{{progress1}}" aria-valuemin="0" aria-valuemax="100"
                                                             style="width:90px">
                                                                    <span class="sr-only "></span>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div> -->
        </div>
        <div class="col-sm-7 text-right search-clip-item pad-x0">
            <h3>Add Media</h3>
            <!-- <button class="btn btn-primary btn-blue">
                                            <span class="fa fa-camera"></span> Capture Screen</button> -->
            <button class="btn btn-primary btn-blue" ng-click="createSticky()">
                <span class="fa fa-plus-square"></span>Note</button>
            <button class="btn btn-primary btn-blue" ng-click="clipboardUploadFile()">
                <span class="fa fa-upload"></span>Upload</button>
                <button type="button" class="pad-0 bg-transparent border-0">
                    <span class="fa mar-0 fa-times text-cream" ng-click="closeEntityClipboard()"></span></button>
        </div>
    </div>
    <div class="row context-menu">
        <div class="col-sm-5 context-menu-item">
            <ul>
                <li id="removefilterByCase" class="active" ng-click="displayFiles('all')">
                    <a>All FILES</a>
                </li>
                <li id="filterByCase" ng-click="displayFiles(actCaseDetail)">
                    <a>{{actCaseDetail.name}}</a>
                </li>
            </ul>
        </div>

        <div class="col-sm-7 text-right context-menu-item">
            <div class="input-group custom-input-group pull-left">
                <span class="input-group-addon">
                    <i class="fa fa-search"></i>
                </span>
                <input type="text" class="form-control" ng-keyup="fullTextSearch($event, $index)">
            </div>
            <div class="select-group">
                <span class="filter-key text-uppercase">Display :</span>
                <select id="displayByIconOrTable" class="filter-value text-uppercase" ng-model="checkedValueDisplay" ng-change="changeDropDown(checkedValue)">
                    <option value="icon" selected="selected">Icons</option>
                    <option value="table">Table</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row clipboard-items">
        <div class="hidden table-details-wrapper" id="tableDetail">
            <div class="transaction-predictive-wrapper custom-data-table-wrapper">
                <table id="" class="table table-scroll five-col-equal table-striped border-b0" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Name
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Type
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Created by
                            </th>
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Created on
                            </th>
                            <!-- <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                                     aria-label="Name: activate to sort column descending">
                                                                            Case
                                                                    </th> -->
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                File size
                            </th>
                        </tr>
                    </thead>
                    <tbody id="dataList">
                        <tr id="" role="row" class="odd" ng-repeat="doc in documents">
                            <td>{{doc.title}}</td>
                            <td ng-if="doc.type == ''">Sticky Notes</td>
                            <td ng-if="doc.type != ''">{{doc.type}}</td>
                            <td>{{doc.uploadedUserName}}</td>
                            <td>{{doc.uploadedOn| date: 'dd/MM/yyyy'}}</td>
                            <!-- <td>HSBC AML Dubai</td> -->
                            <td>{{doc.size / 1000}} Mb</td>
                        </tr>
                </table>
            </div>
        </div>
        <div class="tiles-style">
            <span ng-if="loadDocument" class="custom-spinner case-dairy-spinner">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span>
            <div class="select-group">
                <span class="filter-key text-uppercase">List-By :</span>
                <select id="displayByOrder" class="filter-value text-uppercase" ng-model="checkedValueListBy" ng-change="changeListByDropDown(checkedValue)">
                    <option value="desc">Descending</option>
                    <option value="asc" selected>Ascending</option>
                </select>
            </div>
            <div class="folder-wrapper">
                <div class="folder-list-wrapper">
                    <h5>Shared Sticky Notes</h5>
                    <ul class="list-inline clipboard-items-list top-pop" id="folder">
                        <li class="clipboard-list-item" ng-if="doc.type == ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-append-to-body='true' popover-class="top-popover-wrapper"  ng-repeat="doc in mysharedSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span  class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                        <li class="clipboard-list-item" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mysharedSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mysharedSticky.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <div class="text-right pagin">
                        <ul class="clipboard-pagination"   ng-if="(mysharedSticky.length < mysharedStickyLength && mysharedStickyLength>9)"  uib-pagination total-items="mysharedStickyLength" items-per-page="9" ng-model="pageNum3" ng-change="pageChangedmysharedSticky(pageNum3)" max-size="2" items-per-page="5" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div>
                </div>
                <div class="folder-list-wrapper">
                    <h5>Shared Uploaded Document</h5>
                    <ul class="list-inline clipboard-items-list top-pop " id="folder">
                        <li class="clipboard-list-item" ng-if="doc.type == ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-append-to-body='true' popover-class="top-popover-wrapper" ng-repeat="doc in mysharedUploaded" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()"  ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                        <li class="clipboard-list-item" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-append-to-body='true' popover-class="top-popover-wrapper" ng-repeat="doc in mysharedUploaded" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mysharedUploaded.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <div class="text-right pagin">
                        <ul class="clipboard-pagination"   ng-if="(mysharedUploaded.length < mysharedUploadedLength && mysharedUploadedLength>9)"    uib-pagination total-items="mysharedUploadedLength" items-per-page="9" ng-model="pageNum4" ng-change="pageChangedmysharedUploaded(pageNum4)" max-size="2" items-per-page="5" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div>
                </div>
                <div class="folder-list-wrapper">
                    <h5>My Sticky Notes</h5>
                    <ul class="list-inline clipboard-items-list top-pop" id="folder">
                        <li class="clipboard-list-item" ng-if="doc.type == ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-append-to-body='true' popover-class="top-popover-wrapper" popover-placement='top' ng-repeat="doc in mydocumentSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()"  ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                        <li class="clipboard-list-item" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-append-to-body='true' popover-class="top-popover-wrapper" popover-placement='top' ng-repeat="doc in mydocumentSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mydocumentSticky.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <div class="text-right pagin">
                        <ul class="clipboard-pagination" ng-if="(mydocumentSticky.length < mydocumentStickyLength && mydocumentStickyLength>9)" uib-pagination total-items="mydocumentStickyLength" items-per-page="9" ng-model="pageNum1" ng-change="pageChangedmydocumentSticky(pageNum1)" max-size="2" items-per-page="9" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div>
                </div>
                <div class="folder-list-wrapper">
                    <h5>My Uploaded Document</h5>
                    <ul class="list-inline clipboard-items-list top-pop" id="folder">
                        <li class="clipboard-list-item" ng-if="doc.type == ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentUploaded" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()"  ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                        <li class="clipboard-list-item" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentUploaded" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mydocumentUploaded.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <div class="text-right pagin">
                        <ul class="clipboard-pagination"  ng-if="(mydocumentUploaded.length < mydocumentUploadedLength && mydocumentUploadedLength>9)" uib-pagination total-items="mydocumentUploadedLength"   items-per-page="9" ng-model="pageNum2" ng-change="pageChangedmydocumentUploaded(pageNum2)" max-size="2" items-per-page="5" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div>
                </div>
            </div>
            <!--  Right Click Starts  -->
            <!-- <div class="right-click-wrapper pull-right" id="rightClickMenu" style="position:absolute;display:none">
                                    <ul class="right-click-menu">
                                            <li class="right-click-options" ng-click="viewStickyNotes($event)">
                                                    <a>View</a>
                                            </li>
                                            <li class="right-click-options" ng-click="shareCase($event)">
                                                    <a>Link to case</a>
                                            </li>
                                            <li class="right-click-options" ng-click="shareUser($event)">
                                                    <a>Share</a>
                                            </li>
                                            <li class="right-click-options" ng-click="downloadDocument(doc,'download')">
                                                    <a>Download</a>
                                            </li>
                                            <li class="right-click-options" ng-click="onClickStickyNotesDelete()">
                                                    <a>Remove</a>
                                            </li>
                                    </ul>
                            </div> -->
            <!--  Right Click Ends  -->

            <!--  Sticky Notes Starts  -->
            <div class="panel custom-panel-wrapper sticky-panel-wrapper  panel-info " id="stickyNotes" style="display:none;position:absolute;">
                   <div class="panel-heading pad-b0">
                    <div class="custom-input-group">
                        <input type="text" ng-if="permissionDocument == 'Read Only'" id="documentTitleEntity" ng-value={{doc.title}} ng-keyup="stickyTitleAutoSave($event, $index)" disabled>
                        <input type="text" ng-if="permissionDocument != 'Read Only'" id="documentTitleEntity" ng-keyup="stickyTitleAutoSave($event, $index)">
                        <ul class="top-icon-list input-group-addon pull-right">
                            <li>
                                <a>
                                    <i class="fa fa-minus" style="visibility:hidden"></i>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <i class="fa fa-times" ng-click="closeSticky()"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <span ng-if="loadDocumentContent" class="custom-spinner case-dairy-spinner">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                    </span>
                    <textarea ng-if="permissionDocument == 'Read Only'" id="documentText" rows="9" class="textarea-scrollbar scrollbar-outer custom-scroll-wrapper" ng-keyup="stickyAutoSave($event, $index)" readonly>
                    </textarea>
                    <textarea ng-if="permissionDocument != 'Read Only'" id="documentText" rows="9" class="textarea-scrollbar scrollbar-outer custom-scroll-wrapper" ng-keyup="stickyAutoSave($event, $index)">
                    </textarea>
                </div>
                <div class="panel-footer">
                    <span>{{docUserList.length|| 0}} shares | {{caseList.length|| 0}} cases linked</span>
                    <!-- <ul class="bottom-icon-list pull-right">
                                                    <li>
                                                            <a>
                                                                    <i class="fa fa-comments-o"></i>
                                                            </a>
                                                    </li>
                                                    <li>
                                                            <a>
                                                                    <i class="fa fa-link"></i>
                                                            </a>
                                                    </li>
                                                    <li>
                                                            <a>
                                                                    <i class="fa fa-user-plus"></i>
                                                            </a>
                                                    </li>
                                                    <li>
                                                            <a>
                                                                    <i class="fa fa-trash"></i>
                                                            </a>
                                                    </li>
                                            </ul> -->
                </div>
            </div>
            <!--  Sticky Notes Ends  -->

            <!-- New UI for the Context Menu Starts-->
            <div class="panel custom-panel-wrapper context-menu-2  panel-info " id="context_menu" style="display:none;position:absolute;">
                <div class="panel-heading pad-b0">
                    <h4 ng-if="permissionDocument != 'Read Only'">{{doc.title}}
                        <span style="color:#325D80;">
                            <i class="fa fa-pencil-square-o"></i>
                        </span>
                    </h4>
                    <h4 ng-if="permissionDocument == 'Read Only'">{{doc.title}}
                        <span style="color:#325D80;">
                            <i class="fa fa-lock"></i>
                        </span>
                    </h4>
                    <ul class="top-icon-list input-group-addon pull-right">
                        <li>
                            <a>
                                <i class="fa fa-minus" style="visibility:hidden"></i>
                            </a>
                        </li>
                        <li>
                            <a>
                                <i class="fa fa-times" ng-click="closeBox('contextMenu')"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body pad-t0 row">
                    <div class="col-sm-8">
                        <p>Type :
                            <span ng-if="doc.type == ''">Sticky Notes</span>
                            <span ng-if="doc.type != ''">{{doc.type}}</span>
                        </p>
                        <p>Created By :
                            <span>{{doc.uploadedUserName}}</span>
                        </p>
                        <p>Created On :
                            <span>{{doc.uploadedOn| date: 'dd/MM/yyyy'}}</span>
                        </p>
                    </div>
                    <div class="col-sm-4 custom-dropdown-wrapper">
                        <div class="btn-group " uib-dropdown keyboard-nav>
                            <button id="simple-btn-keyboard-nav " type="button" class="btn btn-primary custom-action-btn" uib-dropdown-toggle>
                                Action
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="simple-btn-keyboard-nav" id="">
                                <li role="menuitem" ng-click="viewStickyNotes($event)" id="openstickyNotes">
                                    <a>Open</a>
                                </li>
                                <li role="menuitem" ng-click="downloadDocument(doc, 'download')">
                                    <a>Download</a>
                                </li>
                                <li role="menuitem" ng-click="onClickStickyNotesDelete()" id="deleteDocument">
                                    <a>Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 tab-wrapper" id="toShareLinkTab">
                        <uib-tabset active="active">
                            <uib-tab index="0" heading="Comments {{commentDes.length}}">
                                <div class="comment-wrapper pad-b0">
                                    <div class="top-comment-wrapper">
                                        <p class="active" ng-repeat="desc in commentDes">
                                            {{desc.description}}
                                            <span> on {{desc.createdOn|date: 'MM/dd/yyyy'}}
                                                <!-- <span>{{ehubObject.fullName}} on {{desc.createdOn |date: 'MM/dd/yyyy'}} -->
                                                <!-- <a class=" pull-right">
                                                remove
                                        </a> -->
                                            </span>
                                        </p>
                                    </div>
                                    <div class="comment-footer-wrapper">
                                        <input type="text" ng-if="permissionDocument == 'Read Only'" ng-model="comment" placeholder="Please Type Here" id="comentInput" ng-keypress="changeCommentBox($event, comment)" disabled>
                                        <input type="text" ng-if="permissionDocument != 'Read Only'" ng-model="comment" placeholder="Please Type Here" id="comentInput" ng-keypress="changeCommentBox($event, comment)">
                                    </div>
                                </div>
                            </uib-tab>
                            <uib-tab index="1" heading="SHARE">
                                <div class="share-tab-wrapper" id="" style="display:block;position:relative;">
                                    <span ng-if="shareLoader" class="custom-spinner case-dairy-spinner">
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                    </span>
                                    <div class="tab-body">
                                        <div class="top-wrapper">
                                            <div class="right pull-left">
                                                <span class="">
                                                    <i class="fa fa-user"></i> Share</span>
                                            </div>
                                            <div class="custom-input-group user-list-wrapper custom-select-dropdown pull-left">
                                                <span class="input-group-addon" id="">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                                <input id="sharewithuser" class="form-control custom-input" type="text" ng-model="casevalue" placeholder="" ng-change="selectedUserForshare(userValue)" uib-typeahead=" user as user.firstName for user in userList | filter:$viewValue " typeahead-loading="loadingPersonsNames"
                                                       typeahead-no-results="noREResults" typeahead-on-select="onSelectUser($item, $model, $label)"> {{user}}
                                                <div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
                                                    <i class="fa fa-spinner fa-pulse"></i>
                                                </div>
                                                <div class="custom-dropdown-menu" ng-show="noREResults">
                                                    <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                                                </div>
                                            </div>
                                            <div class="custom-dropdown-wrapper share-user pull-right">
                                                <div class="btn-group " uib-dropdown keyboard-nav>
                                                    <button type="button" class=" btn btn-primary custom-action-btn" id="shareButton" uib-dropdown-toggle>
                                                        SHARE
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="simple-btn-keyboard-nav">
                                                        <li role="menuitem">
                                                            <a id="onclickSharewithUser" ng-click="shareWithUser(0)">Read</a>
                                                        </li>
                                                        <li role="menuitem">
                                                            <a ng-click="shareWithUser(1)">Write</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bottom-wrapper">
                                            <div class=" top-table-data-wrapper">
                                                <strong class="col-sm-6 pad-0">Shared to {{docUserList.length}} users</strong>
                                                <div class="col-sm-6 text-right pad-r0">
                                                    <!-- <span class="  ">Revoke All</span>
                                                                                                            <label class="switch custom-switch-wrapper" for="checkbox">
                                                                                                                    <input type="checkbox" class="custom-switch-checkbox" id="checkbox" />
                                                                                                                    <div class="slider round"></div>
                                                                                                            </label> -->
                                                </div>
                                            </div>
                                            <table class="table border-b0 data-table table-scroll two-col-equal " role="grid">
                                                <tbody id="">
                                                    <tr id="" role="row" class="odd" ng-repeat='userDetail in docUserList'>
                                                        <td>
                                                            <p> {{userDetail.userName}}</p>
                                                            <p>Id: {{userDetail.userId}}
                                                            <p>on {{userDetail.sharedOn| date: 'MM/dd/yyyy'}}</p>
                                                            <!-- <p>by {{ehubObject.fullName}}</p> -->
                                                        </td>
                                                        <td>
                                                            <a href="" ng-click="removeUser(userDetail)">
                                                                <i class="fa fa-chain-broken "></i>Revoke
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </uib-tab>
                            <uib-tab index="2" heading="Links">
                                <div class="links-tab-wrapper " id="shareCaseWrapper" style="position: relative;">
                                    <span ng-if="shareLoader" class="custom-spinner case-dairy-spinner">
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                    </span>
                                    <div class="tab-body">
                                        <div class="top-wrapper">
                                            <div class="right pull-left">
                                                <span class="">
                                                    <i class="fa fa-briefcase"></i>Find Cases
                                                </span>
                                            </div>
                                            <div class="custom-input-group custom-select-dropdown pull-left">
                                                <span class="input-group-addon" id="beforeInput">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                                <input id="linkWithUser" class="form-control custom-input" type="text" ng-model="caseValue" placeholder="" uib-typeahead=" case as case.name for case in caseDetail| filter:$viewValue " typeahead-loading="loadingPersonsNames" typeahead-no-results="noREResults"
                                                       typeahead-on-select="onSelectCase($item, $model, $label)"> {{case}}
                                                <div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
                                                    <i class="fa fa-spinner fa-pulse"></i>
                                                </div>
                                                <div class="custom-dropdown-menu" ng-show="noREResults">
                                                    <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                                                </div>
                                            </div>
                                            <a href="" id="onclicklinkwithcase" class=" pull-right" ng-click="linkToCase()">Link</a>
                                        </div>
                                        <div class="bottom-wrapper">
                                            <div class=" top-table-data-wrapper">
                                                <strong class="col-sm-6 pad-0">Linked to {{caseList.length}} Cases</strong>
                                                <div class="col-sm-6 text-right pad-r0">
                                                    <!-- <span class="  ">Unlink All</span>
                                                                                                            <label class="switch custom-switch-wrapper" for="checkbox1">
                                                                                                                    <input type="checkbox" class="custom-switch-checkbox" id="checkbox1" />
                                                                                                                    <div class="slider round"></div>
                                                                                                            </label> -->
                                                </div>
                                            </div>
                                            <table class="table border-b0 data-table table-scroll two-col-equal " role="grid">
                                                <tbody>
                                                    <tr id="" role="row" class="odd" ng-repeat='caselist in caseList'>
                                                        <td>
                                                            <p>{{caselist.name}}</p>
                                                            <p>CaseId: {{caselist.caseId}}
                                                            <p> on {{caselist.createdOn| date: 'MM/dd/yyyy'}}</p>
                                                            <!-- <p>by {{ehubObject.fullName}}</p> -->
                                                        </td>
                                                        <td>
                                                            <a href="" ng-click="removeCase(caselist)">
                                                                Unlink
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </uib-tab>
                        </uib-tabset>
                    </div>
                </div>
            </div>
            <!-- New UI for the Context Menu Ends-->

            <!--  Share Box Starts -->
            <div class="panel custom-panel-wrapper share-panel-wrapper" id="shareBoxWrapper" style="display:none;position:absolute;">
                <div class="panel-heading pad-b0">
                    <h4>Sample Data Base.CSV
                        <a class="pull-right">
                            <i ng-click="closeBox('shareDoc')" class="fa fa-times"></i>
                        </a>
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="top-wrapper">
                        <div class="custom-input-group custom-select-dropdown pull-left">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="fa fa-user"></i>
                            </span>
                            <input class="form-control custom-input" type="text" ng-model="userValue" placeholder="Search Users" ng-change="selectedUserForshare(userValue)" uib-typeahead=" user as user.firstName for user in userList | filter:$viewValue " typeahead-loading="loadingPersonsNames"
                                   typeahead-no-results="noREResults"> {{user}}
                            <div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
                                <i class="fa fa-spinner fa-pulse"></i>
                            </div>
                            <div class="custom-dropdown-menu" ng-show="noREResults">
                                <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                            </div>
                        </div>
                        <a href="" class="share-user pull-right" ng-click="shareWithUser()">Share</a>
                    </div>
                    <div class="bottom-wrapper">
                        <div class=" top-table-data-wrapper">
                            <strong class="col-sm-6 pad-0">Shared to 4 users</strong>
                            <div class="col-sm-6 text-right pad-r0">
                                <span class="  ">REvoke All</span>
                                <label class="switch custom-switch-wrapper" for="checkbox">
                                    <input type="checkbox" class="custom-switch-checkbox" id="checkbox" />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </div>
                        <table class="table border-b0 data-table table-scroll three-col-equal " role="grid">
                            <tbody onscroll="scrolled(this)">
                                <tr id="" role="row" class="odd">
                                    <td>
                                        Maria Philipp
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Revoke
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        Anitha MCAFE
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Revoke
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        JULY Christof
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Revoke
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        MARIA VICTOR
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Revoke
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--  Share Box Ends  -->

            <!--  Link Box Starts -->
            <div class="panel custom-panel-wrapper link-panel-wrapper " id="shareCaseWrapper" style="display:none;position:absolute;">
                <div class="panel-heading pad-b0">
                    <h4>Sample Data sheet
                        <a class="pull-right">
                            <i ng-click="closeBox('shareCase')" class="fa fa-times"></i>
                        </a>
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="top-wrapper">
                        <div class="custom-input-group custom-select-dropdown pull-left">
                            <span class="input-group-addon" id="">
                                <i class="fa fa-briefcase"></i>
                            </span>
                            <input class="form-control custom-input" type="text" ng-model="value" placeholder="Search Case" ng-change="selectedUserForshare(userValue)" uib-typeahead=" case as case.name for case in caseList | filter:$viewValue " typeahead-loading="loadingPersonsNames"
                                   typeahead-no-results="noREResults" class="form-control"> {{case}}

                            <div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
                                <i class="fa fa-spinner fa-pulse"></i>
                            </div>
                            <div class="custom-dropdown-menu" ng-show="noREResults">
                                <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                            </div>
                        </div>
                        <a href="" class="share-user pull-right">Link</a>
                    </div>
                    <div class="bottom-wrapper">
                        <div class=" top-table-data-wrapper">
                            <strong class="col-sm-6 pad-0">Linked to 4 Cases</strong>
                            <div class="col-sm-6 text-right pad-r0">
                                <span class="  ">Unlink All</span>
                                <label class="switch custom-switch-wrapper" for="checkbox1">
                                    <input type="checkbox" class="custom-switch-checkbox" id="checkbox1" />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </div>
                        <table class="table border-b0 data-table table-scroll three-col-equal " role="grid">
                            <tbody id="" onscroll="scrolled(this)">
                                <tr id="" role="row" class="odd">
                                    <td>
                                        Maria Philipp
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Unlink
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        Anitha MCAFE
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Unlink
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        JULY Christof
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Unlink
                                        </a>
                                    </td>
                                </tr>
                                <tr id="" role="row" class="odd">
                                    <td>
                                        MARIA VICTOR
                                    </td>
                                    <td>
                                        by jason palmer
                                    </td>
                                    <td>
                                        <a href="">
                                            Unlink
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!--  Link Box Ends  -->
        </div>
    </div>
  </div>
    <!--  Clipboard Dropdown Ends  -->








<!-- Entity Clipboard Dropdown Starts  -->
<div  id="entityWrapper" class="dropdown-menu-wrapper clipboard-entity-menu-wrapper clipboard-dropdown-menu-wrapper" ng-class ="clipBoardObject.file_wraper ? 'clipboard_entity_file_wraper':''"  ng-style="{'left': clipBoardObject.modal_postion.left,'top':clipBoardObject.modal_postion.top}" ng-controller="MyEntityClipBoardController" ng-if="(showMyEntityClipboard && (currentStateMyEntity == 'entityCompanyNew'))">
    <div class="row search-clip-holder" >
        <div class="col-sm-5 search-clip-item" ng-hide="isFromEntitySection || isUploadFromEntitySection">
            <p>Total {{totalDocumentLength}} Files</p>
           
        </div>
        <div class="col-sm-7 text-right search-clip-item pad-x0" ng-class ="{'width-100' : isFromEntitySection || isUploadFromEntitySection}">
            <h3>Add Media</h3>
            <!-- <button class="btn btn-primary btn-blue">
                                            <span class="fa fa-camera"></span> Capture Screen</button> -->
            <button type="button" class="btn btn-primary grad-button sm-btns btn-blue" ng-click="createSticky()" ng-hide="isUploadFromEntitySection">
                <span class="fa fa-plus-square"></span>Note</button>
            <button type="button" class="btn btn-primary  grad-button sm-btns btn-blue" ng-click="clipboardUploadFile()" ng-hide="isFromEntitySection">
                <span class="fa fa-upload"></span>Upload</button>
                <button type="button" class="pad-0 bg-transparent border-0">
                    <span class="fa mar-0 fa-times text-cream" ng-click="closeEntityClipboard()"></span></button>
        </div>
    </div>
    <div class="row context-menu" >
        <div class="col-sm-5 context-menu-item" ng-hide="isFromEntitySection || isUploadFromEntitySection">
            <ul>
                <li id="removefilterByCase" class="active" ng-click="displayFiles('all')">
                    <a>All FILES</a>
                </li>
                <li id="filterByCase" ng-click="displayFiles(actCaseDetail)">
                    <a>{{actCaseDetail.name}}</a>
                </li>
            </ul>
        </div>

        <div class="col-sm-7 text-right context-menu-item" ng-hide="isFromEntitySection || isUploadFromEntitySection">
            <div class="input-group custom-input-group pull-left">
                <span class="input-group-addon">
                    <i class="fa fa-search"></i>
                </span>
                <input type="text" class="form-control" ng-keyup="fullTextSearch($event, $index)">
            </div>
            <div class="select-group">
                <span class="filter-key text-uppercase">Display :</span>
                <select id="displayByIconOrTable" class="filter-value text-uppercase" ng-model="checkedValueDisplay" ng-change="changeDropDown(checkedValue)">
                    <option value="icon" selected="selected">Icons</option>
                    <option value="table">Table</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row clipboard-items">
        <div class="hidden table-details-wrapper" id="tableDetail">
            <div class="transaction-predictive-wrapper custom-data-table-wrapper">
                <table id="" class="table table-scroll five-col-equal table-striped border-b0" role="grid" ng-hide="isFromEntitySection || isUploadFromEntitySection">
                    <thead>
                        <tr role="row">
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Name
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Type
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Created by
                            </th>
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Created on
                            </th>
                            <!-- <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                                     aria-label="Name: activate to sort column descending">
                                                                            Case
                                                                    </th> -->
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                File size
                            </th>
                        </tr>
                    </thead>
                    <tbody id="dataList">
                        <tr id="" role="row" class="odd" ng-repeat="doc in documents">
                            <td>{{doc.title}}</td>
                            <td ng-if="doc.type == ''">Sticky Notes</td>
                            <td ng-if="doc.type != ''">{{doc.type}}</td>
                            <td>{{doc.uploadedUserName}}</td>
                            <td>{{doc.uploadedOn| date: 'dd/MM/yyyy'}}</td>
                            <!-- <td>HSBC AML Dubai</td> -->
                            <td>{{doc.size / 1000}} Mb</td>
                        </tr>
                </table>
            </div>
        </div>
        <div class="tiles-style">
            <span ng-if="loadDocument" class="custom-spinner case-dairy-spinner">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span>
            <div class="select-group">
                <span class="filter-key text-uppercase">List-By :</span>
                <select id="displayByOrder" class="filter-value text-uppercase" ng-model="checkedValueListBy" ng-change="changeListByDropDown(checkedValue)">
                    <option value="desc">Descending</option>
                    <option value="asc" selected>Ascending</option>
                </select>
            </div>
            <div class="folder-wrapper mxh-140" ng-class="{'height-a':isFromEntitySection ||isUploadFromEntitySection}" >
                <div class="folder-list-wrapper" ng-hide="isUploadFromEntitySection">
                    <h5>My Sticky Notes</h5>
                    <ul class="list-inline clipboard-items-list top-pop" id="folder">
                        <li class="clipboard-list-item p-rel" ng-if="doc.type == ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
							ng-click="deleteDoc($event, doc.docId,doc)"></i>
                            <img ng-if="pathChange()"  ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title | removeSntExt}}</span>
                        </li>
                        <li class="clipboard-list-item p-rel" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentSticky" ng-click="getMenuForDoc($event, doc)" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
							ng-click="deleteDoc($event, doc.docId,doc)"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title | removeSntExt}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mydocumentSticky.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <!-- <div class="text-right pagin">
                        <ul class="clipboard-pagination" ng-if="(mydocumentSticky.length < mydocumentStickyLength && mydocumentStickyLength>9)" uib-pagination total-items="mydocumentStickyLength" items-per-page="9" ng-model="pageNum1" ng-change="pageChangedmydocumentSticky(pageNum1)" max-size="2" items-per-page="9" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div> -->
                </div>
                <div class="folder-list-wrapper" ng-hide="isFromEntitySection">
                    <h5>My Uploaded Document</h5>
                    <ul class="list-inline clipboard-items-list top-pop" id="folder">
                        <li class="clipboard-list-item p-rel" ng-if="doc.type == 'snt'" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentUploaded track by $index" ng-click="downloadDocument(doc, 'download')" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
							ng-click="deleteDoc($event, doc.docId,doc)"></i>
                            <img ng-if="pathChange()"  ng-src="assets/img/clipBoardIcon/sticky.png" />
                            <img ng-if="!pathChange()"  ng-src="../assets/img/clipBoardIcon/sticky.png" />
                            <span class="clipboard-item-name" style="display:none;">{{doc}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                        <li class="clipboard-list-item p-rel" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' popover-class="top-popover-wrapper" popover-append-to-body='true' ng-repeat="doc in mydocumentUploaded track by $index" ng-click="downloadDocument(doc, 'download')" ng-cloak>
                            <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                            <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
							ng-click="deleteDoc($event, doc.docId,doc)"></i>
                            <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type.toLowerCase()}}.png" />
                            <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type.toLowerCase()}}.png" />
                            <span class="clipboard-item-name" style="" ng-if="!doc.title">{{doc.docName}}</span>
                            <span class="clipboard-item-name">{{doc.title}}</span>
                        </li>
                    </ul>
                    <div class="alertsDashErrorDiv" ng-if="mydocumentUploaded.length == 0">
                        <span>No Document Found</span>
                    </div>
                    <!-- <div class="text-right pagin">
                        <ul class="clipboard-pagination"  ng-if="(mydocumentUploaded.length < mydocumentUploadedLength && mydocumentUploadedLength>9)" uib-pagination total-items="mydocumentUploadedLength"   items-per-page="9" ng-model="pageNum2" ng-change="pageChangedmydocumentUploaded(pageNum2)" max-size="2" items-per-page="5" class="pagination-sm"
                            boundary-link-numbers="true">
                        </ul>
                    </div> -->
                </div>
                <div class="folder-list-wrapper" ng-hide="isFromEntitySection || isUploadFromEntitySection">
                        <h5>My Evidence section</h5>
                        <ul class="list-inline clipboard-items-list top-pop" id="folder">
                            <li class="clipboard-list-item p-rel" ng-repeat="doc in evidenceReportAddToPageFromclip" ng-click="downloadDocument(doc, 'download')" >
                                     <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
                                     ng-click="deleteDoc($event, doc.docId,doc)"></i>
                                     <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type.toLowerCase()}}.png" />
                                     <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type.toLowerCase()}}.png" />
                                     <!-- <img ng-src="../assets/img/clipBoardIcon/png.png"  class="square-18"  ng-if="doc.type =='png'"/> -->
                                <!-- <span class="clipboard-item-name" style="display:none;">{{doc}}</span> -->
                                <span class="clipboard-item-name" style="" ng-if="!doc.title">{{doc.docName}}</span>
                                <span class="clipboard-item-name">{{doc.title}}</span>
                            </li>
                            <!-- <li class="clipboard-list-item p-rel" ng-if="doc.type != ''" uib-popover={{doc.title}} popover-trigger="'mouseenter'" popover-placement='top' ng-repeat="doc in mydocumentUploaded" ng-click="downloadDocument(doc, 'download')" ng-cloak>
                                <i class="fa fa-file-pdf-o clipboard-icon hidden"></i>
                                <i class="fa fa-times p-abs clipboard-icon text-coral-red f-12 r-5"
                                ng-click="deleteDoc($event, doc.docId)"></i>
                                <img ng-if="pathChange()" ng-src="assets/img/clipBoardIcon/{{doc.type}}.png" />
                                <img ng-if="!pathChange()" ng-src="../assets/img/clipBoardIcon/{{doc.type}}.png" />
                                <span class="clipboard-item-name" style="" ng-if="!doc.title">{{doc.docName}}</span>
                                <span class="clipboard-item-name">{{doc.title}}</span>
                            </li> -->
                        </ul>
                        <div class="alertsDashErrorDiv" ng-if="evidenceReportAddToPageFromclip.length == 0">
                            <span>No Document Found</span>
                        </div>
                        <!-- <div class="text-right pagin">
                            <ul class="clipboard-pagination"  ng-if="(evidenceReportAddToPageFromclip.length < evidenceReportAddToPageFromclipLength && evidenceReportAddToPageFromclipLength>9)"  uib-pagination total-items="evidenceReportAddToPageFromclipLength"   items-per-page="9" ng-model="pageNum2" ng-change="pageChangedmydocumentUploaded(pageNum2)" max-size="2" items-per-page="5" class="pagination-sm"
                                boundary-link-numbers="true">
                            </ul>
                        </div> -->
                    </div>
            </div>
           

            <!--  Sticky Notes Starts  -->
            <div class="panel custom-panel-wrapper sticky-panel-wrapper  panel-info " id="stickyNotesEntity" style="display:none;position:absolute;">
                <div class="panel-heading pad-b0">
                    <div class="custom-input-group">
                        <input type="text" ng-if="permissionDocument == 'Read Only'" id="documentTitleEntitySec" ng-value={{doc.title}} ng-keyup="stickyTitleAutoSave($event, $index)" disabled>
                        <input type="text" ng-if="permissionDocument != 'Read Only'" id="documentTitleEntitySec" ng-keyup="stickyTitleAutoSave($event, $index)">
                        <ul class="top-icon-list input-group-addon pull-right">
                            <li ng-click="showSourceLinkModal()">

                                <a 
                                popover-placement="bottom-right"
                                uib-popover-template="'stickyLinks.html'" popover-append-to-body='true'                               
                                popover-class="sticky-link-popover width-50 bottom-pop-wrapper  mxh-none">
                                    <i class="fa pad-r0 pad-l20 fa-link" ></i>
                                </a> 

                            </li>
                            <li>
                                <a>
                                    <i class="fa fa-times" ng-click="closeSticky()"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body textarea-body">
                    <span ng-if="loadDocumentContent" class="custom-spinner case-dairy-spinner">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                    </span>
                    <div contenteditable="true" wrap="hard" ng-if="permissionDocument == 'Read Only'" id="documentTextEntity" rows="9" class="textarea-scrollbar scrollbar-outer custom-scroll-wrapper " ng-keyup="stickyAutoSave($event,$index)" readonly>
                    </div>
                    <div contenteditable="true" wrap="hard" ng-if="permissionDocument != 'Read Only'" id="documentTextEntity" rows="9" class="textarea-scrollbar scrollbar-outer custom-scroll-wrapper " maxlength ="4000"ng-keyup="stickyAutoSave($event,$index)">
                    </div>
                </div>
                <div class="panel-footer">
                    <span>{{docUserList.length|| 0}} shares | {{caseList.length|| 0}} cases linked</span>
                </div>
            </div>
            <!--  Sticky Notes Ends  -->

         </div>
    </div>
  </div>
    <!--  Entity Clipboard Dropdown Ends  -->














 <!--  Calendar Dropdown Starts  -->
    <div class="dropdown-menu-wrapper calendar-dropdown-menu-wrapper" ng-controller="UserEventsController" id="setEvents" ng-show="showUserEvents">
		<button ng-click="openCreateEvent(eventText)"><i class="fa fa-plus" aria-hidden="true">{{eventText}}</i></button>
		<div class="select-group" ng-show="eventText == 'Update an Event'">
            <span class="filter-key text-uppercase">Participants :</span>
            <select id="" class="filter-value text-uppercase" ng-model="participantsSelected" ng-change="participantsDropDown(participantsSelected)">
                <option value="" disabled selected>Select an action</option>
                <option value="Add">Add</option>
                <option value="Remove">Remove</option>
            </select>
        </div>
       <div class="select-group">
            <span class="filter-key text-uppercase">Events :</span>
            <select id="" class="filter-value text-uppercase" ng-model="calendarSelected" ng-change="calendarDropDown(calendarSelected)">
                <option value="Created" selected="selected">Created</option>
                <option value="Participated">Participated</option>
            </select>
        </div>
        <div class="events-list" id="eventsListId" ng-show="calendarSelected == 'Created'">
            <div class="event-list-nav event-item-holder">
                <div class="event-list-item" ng-repeat="eventItem in calendarList" ng-if="calendarList.length > 0">
                	<div class="avatar-holder">
						<div class="custom-checkbox width-a">
                             <label class="checkbox-inline">
                                 <input type="checkbox" ng-click="toggleEventSelection($index, calendarList, eventItem)" ng-model="eventItem.checked">
                                 <span class="">
                                     <i class="fa fa-check text-grey-slate"></i>
                                 </span>
                             </label>
                         </div>
                         <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
	                     <h1 class="headeline">{{eventItem.subject}}</h1>
	                     <div class="details">
	                         <strong>Description:</strong>
	                         <span>{{eventItem.body}}</span>
	                         <br>
	                         <strong>Start:</strong>
	                         <span>{{eventItem.scheduledFrom | date:'MM/dd/yyyy @ h:mma'}}</span>
	                         <br>
	                         <strong>End:</strong>
	                         <span>{{eventItem.scheduledTo | date:'MM/dd/yyyy @ h:mma'}}</span>
	                     </div>
                     </div>
                </div>
                <div ng-if="calendarList.length == 0">
                	<span>No events found</span>
                </div>
            </div>
        </div>
        <div class="events-list" id="participatedListId" ng-show="calendarSelected == 'Participated'">
        	<div class="event-list-nav event-item-holder">
                <div class="event-list-item" ng-repeat="eventItem in ParticipatedEventList" ng-if="ParticipatedEventList.length > 0">
                	<div class="avatar-holder">
						<div class="custom-checkbox width-a">
                             <label class="checkbox-inline">
                                 <input type="checkbox" ng-click="toggleEventSelection($index, ParticipatedEventList, eventItem)" ng-model="eventItem.checked">
                                 <span class="">
                                     <i class="fa fa-check text-grey-slate"></i>
                                 </span>
                             </label>
                         </div>
                         <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
	                     <h1 class="headeline">{{eventItem.subject}}</h1>
	                     <div class="details">
	                         <strong>Description:</strong>
	                         <span>{{eventItem.body}}</span>
	                         <br>
	                         <strong>Start:</strong>
	                         <span>{{eventItem.scheduledFrom | date:'MM/dd/yyyy @ h:mma'}}</span>
	                         <br>
	                         <strong>End:</strong>
	                         <span>{{eventItem.scheduledTo | date:'MM/dd/yyyy @ h:mma'}}</span>
	                     </div>
                     </div>
                </div>
                <div ng-if="ParticipatedEventList.length == 0">
                	<span>No events found</span>
                </div>
            </div>
        </div>
<!--         <calendar-event events="allEvents"></calendar-event> -->
    </div>
    <!--  Calendar Dropdown Ends  -->
    <!-- 	<flash-message class="main-flash-wrapper"  show-close="true"></flash-message> -->
</header>
<!--  Header Wrapper Ends  -->

<script>
            function getAgainTokenBySession() {
            var userDetails = '<%=new ObjectMapper().writeValueAsString((Object)session.getAttribute("userInfo"))%>';
                    userDetails = JSON.parse(userDetails);
                    if (userDetails != undefined && userDetails != null) {
            return userDetails;
            } else {
            return null;
                    window.location.href = '';
            }
            }
</script>
<script type="text/ng-template" id="confirmDeleteDoc.html">
    <div class="modal-header d-none">
   <button type="button" class="close text-cream" ng-click="closeDataPopUp()" data-dismiss="modal" aria-hidden="true"><span>&#x2716;</span></button>
   <h4 class="modal-title" id="mainTitle">Delete</h4>
</div>
<div class="modal-body pad-x15">
	<div class="row">
		<div class="col-sm-12">
			<p id="mainData" class="text-left text-cream">
	              Are you sure you wish to delete?
			</p>
		</div>
		<div class="btn-group width-100 text-right">
				<button class="grad-button" type="button" ng-click="deleteDocument()">Yes</button>
				<button class="bordered-button" type="button" ng-click="cancelDel()">No</button>
		</div>	
	</div>
</div>
</script>
<script type="text/ng-template" id="customTemplate.html">
    <a>
    <span ng-bind-html="match.model['vcard:organization-name'] | uibTypeaheadHighlight:query"></span>
    <span ng-show="match.model['isDomiciledIn']" ng-bind-html=" '-'+ match.model['isDomiciledIn'] | uibTypeaheadHighlight:query"></span>
    </a>
    <!-- <a>
    <span ng-bind-html="match.model.title | uibTypeaheadHighlight:query"></span>
    - <span ng-show="match.model.title" ng-bind-html="match.model.address.locality | uibTypeaheadHighlight:query"></span>
    </a> -->
</script>
<script>
    function textbox()
    {
            var ctl = document.getElementById('documentTextEntity');
            var startPos = ctl.selectionStart;
            var endPos = ctl.selectionEnd;
            console.log(startPos + ", " + endPos);
            alert(startPos + ", " + endPos);
    }
    </script>