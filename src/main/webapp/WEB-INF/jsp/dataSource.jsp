<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<!--   <div ng-include="callSubMenuPath()" ng-controller="subMenuController"></div> -->
   <!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="datasource-details-wrapper">
                    <div class="total-users-wrapper">
                        <ul class="list-inline">
                            <li>
                                <div class="total-users text-center">
                                    <p class="text-uppercase">Sources</p>
                                    <span>28</span>
                                </div>
                            </li>
                            <li>
                                <div class="total-organisation text-center">
                                    <p class="text-uppercase">Source Type</p>
                                    <span>04</span>
                                </div>
                            </li>
                            <li>
                                <div class="total-organisation-ratio text-center">
                                    <p class="text-uppercase">Data Base Type</p>
                                    <span>17</span>
                                </div>
                            </li>
                            <li>
                                <div class="total-rolewise-ratio text-center">
                                    <p class="text-uppercase graph">Source type ratio</p>
                                    <img ng-src="..{{rootPath}}assets/images/donut.png" alt="donut"/>
                                </div>
                            </li>
                        </ul> 
                    </div>
                    <div class="usermanagement-data">
                        <div class="management-header">
                            <ul class="content-wrapper-pills clearfix">
                                <li class="active">
                                    <a class="management-tabs" data-toggle="pill" href="#dataSources" >
                                        <i class="fa fa-pie-chart"></i>
                                        <span class="text-uppercase">Data Sources</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="management-tabs" data-toggle="pill" href="#databaseTypes">
                                        <i class="fa fa-database"></i>
                                        <span class="text-uppercase">Database types</span>
                                    </a>
                                </li>
                                <li>
                            </ul>
                        </div>
                        <div class="management-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="dataSources">
                                    <div class="data-table-wrapper">
                                        <div class="management-data" id="managementData">
                                            <div class="data-table-accessories">
                                                <div class="organisation">
                                                    <div class="viewby-data">
                                                        <span class="key-text text-uppercase">View By :</span>
                                                        <span class="value-text text-uppercase"> Recently Added</span>
                                                    </div>
                                                </div>

                                                <div class="new-user-content">
                                                    <a class="" ng-click="dataSource.addSourceModel()">
                                                        <i class="fa fa-plus"></i>
                                                        <span>ADD SOURCE</span>
                                                    </a>
                                                    <a class="" href="javascript:void(0);">
                                                        <i class="fa fa-sign-out"></i>
                                                        <span>EXPORT</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="databaseTypes">
                                    <div class="data-table-wrapper">
                                        <div class="management-data">
                                            <div class="data-table-accessories">
                                                <div class="organisation">
                                                    <div class="viewby-data">
                                                        <span class="key-text text-uppercase">View By :</span>
                                                        <span class="value-text text-uppercase"> Recently Added</span>
                                                    </div>
                                                </div>

                                                <div class="new-user-content">
                                                    <a class="" href="javascript:void(0);">
                                                        <i class="fa fa-plus"></i>
                                                        <span>ADD DATABASE</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="database-types-wrapper">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Data Warehouse</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Nosql</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Graph</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Rdbms</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Big Data</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Columnar</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Files</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Media</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">Open Source</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">In Rest</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="data-base-types">
                                                            <h3 class="text-uppercase">In Motion</h3>
                                                            <p>
                                                                A database management system (DBMS) is a computer software application 
                                                                that interacts with the user, other applications.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->

        <!-- Details Modal Wrapper Starts  -->
        <div id="createDB" class="modal fade database-modal-wrapper" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <form id="addNewUser">
                    <div class="modal-content">
                        <div class="modal-header clearfix">
                            <div class="add-user-submit">
                                <button type="button" class="cancel" data-dismiss="modal" aria-label="Close">
                                    <span class="text-uppercase">cancel</span>
                                </button>
                                <button type="submit" class="custom-submit" >
                                    <span class="text-uppercase">save</span>
                                </button>
                            </div>
                            <div class="modal-title">
                                <!--<span><i class="fa fa-plus"></i></span>-->
                                <p class="text-uppercase">New data source</p>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="adddb-stages">
                                <ul class="list-inline clearfix">
                                    <li class="active">
                                        <a class="text-uppercase list-arrow select-source" href="javascript:void(0);">
                                            Select Source
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-uppercase list-arrow staging" href="javascript:void(0);">
                                            Staging
                                        </a>
                                    </li>
                                    <li>
                                </ul>
                            </div>
                            <div class="clearfix form-stages">
                                <div class="col-sm-7">
                                    <div class="form-group clearfix">
                                        <label class="control-label col-sm-4 text-uppercase" for="name">Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="control-label col-sm-4 text-uppercase" for="sourceType">Source Type</label>
                                        <div class="col-sm-8">
                                            <select  class="form-control text-uppercase" id="sourceType">
                                                <option class="text-uppercase">select</option>
                                                <option class="text-uppercase">Data Warehouse</option>
                                                <option class="text-uppercase">GRAPH</option>
                                                <option class="text-uppercase">BIG DATA</option>
                                                <option class="text-uppercase">FILES</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Details Modal Wrapper Ends  -->

	