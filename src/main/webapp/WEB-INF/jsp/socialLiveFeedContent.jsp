<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!--  Spinner Starts  -->
<div class="custom-spinner full-page" ng-if="disableSearchButton">
	<i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<!--  Spinner Ends  -->
	
<!--  Entity Company Starts  -->
<div id="entity-company" class="entity-company-wrapper">
	
	<!--  Submenu Starts  -->
	<%@include file="common/submenu.jsp"%>
	<!--  Submenu Ends  -->
        
	<!--  Entity Company Content Starts  -->
	<div id="entity-company-content" class="entity-page live-feed-content-wrapper">
		<div class="entity-company-content-inner-wrapper">	 
		
			<!--  Entity Company Content Left Starts  -->
			<div class="entity-company-content-left">
			
			<!--  Custom Tab Wrapper Starts  --> 
			<div class="custom-tab-wrapper">
			
				 <!--  Nav Tabs Starts  -->
				<ul class="nav nav-tabs custom-tabs">
					<li ng-class="{'active': entitySearchResult.selectedTab === 'graph'}" ng-click="onClickliveFeedTabs('graph')"><a>Graph</a></li>
					<li ng-class="{'active': entitySearchResult.selectedTab === 'location'}" ng-click="onClickliveFeedTabs('location')"><a>Location</a></li>
					<li ng-class="{'active': entitySearchResult.selectedTab === 'tweets'}" ng-click="onClickliveFeedTabs('tweets')"><a>Tweets</a></li>
				</ul>
				<!--  Nav Tabs Ends  -->
				
				<!--  Tab Panes Starts  -->
				<div class="tab-content">
					<div class="tab-pane" ng-class="{'active': entitySearchResult.selectedTab === 'graph'}" id="graph">
						<div id="twitter-network-chart"></div>
					</div>
					<div class="tab-pane" ng-class="{'active': entitySearchResult.selectedTab === 'location'}" id="location">
						<div id="live-feed-location-chart"></div>
					</div>
					<div class="tab-pane" ng-class="{'active': entitySearchResult.selectedTab === 'tweets'}" id="tweets">
						<!--  Risk Store Panel Starts  -->
						<div class="risk-score-panel live-feed-wrapper">						
							<ul class="media-list">
							  <li class="media" ng-repeat="twitterData in liveFeed.nodeList | reverse">
							    <div class="media-left">
								      <a href="javascript:void(0);">
								      	<img class="image-object" ng-src="{{twitterData.profile_image || 'assets/images/trans_twiiter_pic.png'}}" />
								      </a>
							    </div>
							    <div class="media-body">
								    <h3 class="media-heading">
								      <a href="javascript:void(0);">
								    	{{twitterData.name}}
								      </a>
							    	</h3>
							   		 <p class="description">
								        {{twitterData.text}}
							         </p>							
									<p class="progress-text">
										<i class="fa fa-wechat icon"></i>
										<span>{{twitterData.replyCounts}}</span>
										<img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" />
										<span>{{twitterData.retweetCounts}}</span>
										<i class="fa fa-heart icon"></i>
										<span>{{twitterData.likeCounts}}</span>
									 </p>				
								</div>
							  </li>
							</ul>
						</div>
						<!--  Risk Store Panel Starts  -->
					</div>
				</div>
				<!--  Tab Panes Ends  -->
				
			</div>
			<!--  Custom Tab Wrapper Ends  --> 
				
			</div>
			<!--  Entity Company Content Left Edns  -->
			
			<!--  Entity Company Content Right Starts  -->
			<div class="entity-company-content-right">			
				
				<!--  Risk Store Panel Starts  -->
				<div class="risk-score-panel live-feed-wrapper">
				
					<!--  First Panel Starts  -->
					<div class="first-panel custom-panel tweets-count-panel">						
						<div class="row">
							<div class="col-sm-5 left-grid">
								<div class="row">
									<div class="col-sm-6">
										<span>Tweets</span>
										<p>{{entitySearchResult.tweetCounts}}</p>
									</div>
									<div class="col-sm-6">
										<span>Retweet</span>
										<p>{{entitySearchResult.mentionCounts}}</p>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<span>Mentions</span>
										<p>{{entitySearchResult.mentionCounts}}</p>
									</div>
								</div>
							</div>
							<div class="col-sm-7 right-grid">
								<h5>Overall Positive Sentiment <a class="fa fa-hand-o-right icon"></a></h5>
								<div class="row">
									<div class="col-sm-4">
										<span>Positive</span>
										<p class="text-green">{{entitySearchResult.positivePercentage}}%</p>
									</div>
									<div class="col-sm-4">
										<span>Negative</span>
										<p class="text-red">{{entitySearchResult.negativePercentage}}%</p>
									</div>
									<div class="col-sm-4">
										<span>Neutral</span>
										<p class="text-light-blue">{{entitySearchResult.neutralPercentage}}%</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  First Panel Ends  -->
										
					<!--  Third Panel Starts  -->
					<div class="third-panel custom-panel">
						<h3>Top Tags</h3>
						<div id="tagcloud"></div>				
					</div>
					<!--  Third Panel Ends  -->
					
					<!--  Fourth Panel Starts  -->
					<div class="fourth-panel custom-panel">
						<h3>Latest Tweets</h3>
						<ul class="media-list">
						  <li class="media">
						    <div class="media-left">
						      <a href="javascript:void(0);">
						      	<img class="img-responsive" ng-src="{{liveFeed.nodeList[liveFeed.nodeList.length - 1].profile_image || 'assets/images/dummy-logo.png'}}" alt="logo">
						      </a>
						    </div>
						    <div class="media-body">
						        <h3 class="media-heading">{{liveFeed.nodeList[liveFeed.nodeList.length - 1].name}}</h3>			
						        <p class="description">
						    	    {{liveFeed.nodeList[liveFeed.nodeList.length - 1].text}}
						         </p>						
								<p class="progress-text">
									<i class="fa fa-wechat icon"></i>
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 1].replyCounts}}</span>
									<img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" />
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 1].retweetCounts}}</span>
									<i class="fa fa-heart icon"></i>
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 1].likeCounts}}</span>
								 </p>				
						    </div>
						  </li>
						  <li class="media" ng-if="liveFeed.nodeList.length > 1">
						    <div class="media-left">
						      <a href="javascript:void(0);">
						      	<img class="img-responsive" ng-src="{{liveFeed.nodeList[liveFeed.nodeList.length - 2].profile_image || 'assets/images/dummy-logo.png'}}" alt="logo">
						      </a>
						    </div>
						    <div class="media-body">
						        <h3 class="media-heading">{{liveFeed.nodeList[liveFeed.nodeList.length - 2].name}}</h3>			
						        <p class="description">
						    	    {{liveFeed.nodeList[liveFeed.nodeList.length - 2].text}}
						         </p>					
								<p class="progress-text">
									<i class="fa fa-wechat icon"></i>
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 2].replyCounts}}</span>
									<img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" />
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 2].retweetCounts}}</span>
									<i class="fa fa-heart icon"></i>
									<span>{{liveFeed.nodeList[liveFeed.nodeList.length - 2].likeCounts}}</span>
								 </p>								
						    </div>
						  </li>
						</ul>
					</div>
					<!--  Fourth Panel Ends  -->
					
					<!--  Fifth Panel Starts  -->
					<div class="fifth-panel custom-panel">
						<h3>Locations</h3>	
						<div id="locationchart"></div>		
					</div>
					<!--  Fifth Panel Ends  -->
					
					<!--  Second Panel Starts  -->
					<div class="second-panel custom-panel">
						<h3>Activity Threshold</h3>
							<div id="areachart"></div>			
					
					</div>
					<!--  Second Panel Ends  -->
					
				</div>
				<!--  Risk Store Panel Ends  -->
				
			</div>
			<!--  Entity Company Content Right Ends  -->
		</div>
	</div>
	<!--  Entity Company Content Ends  -->
		
</div>
<!--  Entity Company Ends  -->

<script src="http://d3js.org/d3.v4.min.js"></script>
<script>
$(document).ready(function() {
	/*Custom Scroll Bar*/
	$('.live-feed-wrapper').mCustomScrollbar({
		axis : "y"
	});
});
</script>
