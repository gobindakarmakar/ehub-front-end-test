<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <style>
        @media screen and (min-width: 1400px) and (max-width:1920px) {
            ol.breadcrumb.breadcrumb-wrapper.mar-t15 li.breadcrumb-item a{
                font-size: 14px !important;
            }
        }
    </style>
        <!--  Utilities Panel Wrapper Starts  -->
        <div class="clearfix utilities-panel-wrapper db-util-background-{{submenu.dashboardName}}" ng-class="{'bst_element_submenu' : currentState == 'generalSettings' || currentState == 'docparser','bst_element_submenu' : currentState == 'docParserPage','bst_element_submenu' : (currentState == 'dualCardHolderDetails' || currentState === 'generalSettings' || currentLocationPath == 'sourceManagement' || currentState == 'entityCompanyNew') }" ng-controller="SubMenuController">

            <!--  Custom Spinner Starts  -->
            <div class="custom-spinner case-dairy-spinner" ng-show="submenu.fetchingcaseDocuments">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>

            <!--  Custom Spinner Ends  -->

            <!--  Utility Group Starts  -->
            <div class="btn-group utility-group z-999" uib-dropdown ng-hide="ehubObject.fullName == 'DataEntry User'">
                <a id="single-button" class="dropdown-toggle" uib-dropdown-toggle>
                    <i class="fa fa-bars icon-left visible-xs"></i>
                    <strong class="hidden-xs">{{submenu.dashboardName}}</strong>
                    <i class="fa fa-angle-down icon-right"></i>
                </a>
                <ul class="dropdown-menu bg-{{submenu.hoverdashboardName| lowercase}}" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                   <li ng-if="(currentState == 'docparser' || currentState == 'docParserPage' || currentState == 'dualCardHolderDetails' || currentState == 'sourceManagement' || currentState == 'entityCompanyNew')">
                    <a class="dropdown-toggle bg-transparent border-0 ai-c d-flex">
                        <img class="square-40" ng-src="{{logo}}">
                        <span ng-show="!submenu.hoverdashboardName||(submenu.dashboardName == submenu.hoverdashboardName.toLowerCase())" class="hidden-xs f-21  mar-l10">{{submenu.dashboardName}}</span>
                        <span ng-if="submenu.dashboardName != submenu.hoverdashboardName.toLowerCase()" class="hidden-xs f-21 mar-l10">{{submenu.hoverdashboardName}}</span>
                        <i class="fa fa-times icon-right"></i>
                    </a>
                   </li>
                    <li >
                        <uib-tabset active="submenu.activeDropdown">
                            <uib-tab index="$index" class="bg-{{dashboardName.menu| lowercase}}" ng-repeat="dashboardName in submenu.dashboarDropDownMenuItems"
                                ng-mouseover="submenu.onhoversubMenuDropdown($index, dashboardName.menu, $event, dashboardName.disabled)"
                                ng-click="submenu.onclicksubMenuDropdown(dashboardName.menu)" disable="dashboardName.disabled=='yes'">
                                <uib-tab-heading>
                                    <i class="{{dashboardName.mainIconClass}}"></i>
                                </uib-tab-heading>
                                 <h3 ng-if="submenu.dashboardName != submenu.hoverdashboardName.toLowerCase() && (currentState !== 'docparser' && currentState !== 'docParserPage' && currentState !== 'dualCardHolderDetails' && currentState !== 'sourceManagement' && currentState !== 'entityCompanyNew')" ng-class="{'submenu-title': submenu.hoverdashboardName.toLowerCase() ? true : false}">{{submenu.hoverdashboardName}}</h3>
                                <ul class="list-inline list-submenu">
                                    <li ng-click="submenu.getSubMenu(content, content.disabled, dashboardName)" ng-repeat="content in dashboardName.content">
                                        <a ng-disabled="submenu.checkSubmenuDisable(content.disabled);" ng-class="submenu.addClass($index);">
                                            <i ng-if="!content.imgIcon" class="{{content.iconClass}} icon"></i>
                                            <img class='img-responsive center-block ' ng-if="content.imgIcon && checkLocationForIcon(content)" ng-src="{{content.imgIcon}}"
                                                style="width:40px">
                                            <img class='img-responsive center-block' ng-if="content.imgIcon && !checkLocationForIcon(content)" ng-src="../{{content.imgIcon}}"
                                                style="width:40px"> <span ng-if="content.name !== 'Personalization'">{{content.name}}</span>
                                                <span ng-if="content.name === 'Personalization'"><small>{{content.name}}</small></span>
                                        </a>
                                    </li>
                                </ul>
                            </uib-tab>
                        </uib-tabset>
                    </li>
                </ul>
            </div>
            <!--  Utility Group Ends  -->


            <!--  Discover Panel Wrapper Starts  -->
            <div ng-if="currentState == 'discover' || currentState == 'transactionMonitoring'" class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix">

                <!--  Discover Bar Starts  -->
                <div class="discover-bar">
                    <ul class="list-inline discover-bar-list" ng-class="{'utility-mip-wrapper':isMip}">
                        <!-- <li ng-repeat="item in submenu.countAllCaseStatus" class="hello flex-grid counter flex-grid-expand">
                  <span class="counter-label">{{item.label}}</span> 
                  <span class="counter-value">{{item.value | zpad:2}}</span>
               </li> -->
                    </ul>
                </div>
                <!--  Discover Bar Ends  -->

                <!--  Work Diary Dropdown Wrapper Starts  -->
                <div class="dropdown work-diary-dropdown-wrapper" id="work-diary-dropdown-button-panel" ng-controller="YourWorkDiaryController"
                    ng-if="currentState == 'discover' || currentState == 'discoverCase' || currentState == 'workspace' || currentState == 'investigationLanding' || currentState == 'transactionMonitoring'"
                    ng-class="{'open': workDairyToggle}" ng-click="dashboardUtilitiesPanelDiscoverOnClickWorkDiaryDropDown();">
                    <div id="work-diary-dropdown-button" class="height-100 pad-t20">
                        <span class="icon-left">
                            <i class="fa fa-rss"></i>
                        </span>
                        <div class="work-diary-dropdown-button-text hidden-xs">
                            <div>YOUR WORK DIARY</div>
                            <div class="small" ng-if="dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount>0">New cases has been assigned to you</div>
                        </div>
                        <span class="pull-right">
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </div>
                    <div id="work-diary-dropdown-menu-panel" class="dropdown-menu work-diary-dropdown-menu-panel" style="min-width={{submenu.workDiaryWidth}}">
                        <ul class="list-unstyled">
                            <li>
                                <button type="button" id="work-diary-create-case" class="btn btn-default" ng-click="displayCreate()">
                                    <span id="work-diary-create-case-icon" class="pull-left">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    <span id="work-diary-create-case-text">CREATE CASE</span>
                                </button>
                            </li>
                            <li>
                                <div id="work-diary-count-case">
                                    <div ng-if="dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount>0">
                                        <span id="work-diary-count-case-num">{{dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount| zpad:2}}</span>
                                        <span id="work-diary-count-case-text" >Cases Pending for your action. Please initiate your action.</span>
                                    </div>
                                    <div ng-if="dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount==0">
                                    	<span id="work-diary-count-case-text" >No Cases Pending for your action. Please create a new.</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div id="work-diary-sub-menu">
                                    <div id="work-diary-sub-menu-filter" ng-click="OpenfilterCaseDataModal()" class="pull-right">
                                        <span>
                                            <i class="fa fa-filter"></i>
                                        </span>
                                    </div>
                                    <!-- 							<div id="work-diary-sub-menu-camera"  ng-click="submenuCaptureDiv('createCaseWrapper')" class="pull-right"> -->
                                    <!-- 								<span><i class="fa fa-camera"></i></span> -->
                                    <!-- 							</div> -->
                                </div>
                            </li>
                            <li>
                                <span ng-show="yourWorkDairyPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div id="createCaseWrapper" style="overflow-y: auto; overflow-x: hidden; height: 100%; max-height: 470px; margin: 0;">
                                    <dashboard-utilities-discover-work-diary-case-list status-data-for-each-case="statusDataForEachCase[$index]" index="$index" name="item.name" purpose="item.description" type="item.type"
                                        assignedby="item.assignedBy" trisk="item.transactionalRisk" drisk="item.directRisk" irisk="item.indirectRisk"
                                        seedid="item.caseId" currentstatus="item.currentStatus" ng-repeat="item in dashboardUtilitiesPanelDiscoverWorkDiaryCaseList"
                                        ng-mouseover="" on-click-act-dropdown="discoverOnClickWorkDiaryActDropDown(event, index)"
                                        is-hide-act-dropdown="discoverIsHideWorkDiaryActDropDown(index)" on-click-act-dropdown-item="discoverOnClickWorkDiaryActDropDownItem(event, ev, index, seedid, currentstatus, selectedStatus)"
                                        on-click-work-diary-card="discoverClickWorkDiaryCard(index)" is-hide-detail-section="discoverIsHideDetailSection(index)">
                                    </dashboard-utilities-discover-work-diary-case-list>
                                </div>
                                <div class="text-center" ng-hide="dashboardUtilitiesPanelDiscoverWorkDiaryCaseList.length == 0 || allCasesCount <= 10">
                                    <ul uib-pagination total-items="allCasesCount" ng-model="pageNumber" ng-change="pageChanged(pageNumber)" max-size="2" class="pagination-sm"
                                        boundary-link-numbers="true"></ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--  Work Diary Dropdown Wrapper Ends  -->

            </div>
            <!--  Discover Panel Wrapper Ends  -->

            <!--  Transaction Analysis Panel Wrapper Starts  -->
            <div ng-if="currentState == 'transactionIntelligence'" class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix">
                <div class="col-lg-9 col-md-6 col-sm-6">
                    <h4 class="heading-wrapper text-uppercase" id="currenttransName"></h4>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
                    <button class="btn custom-heading-btn text-uppercase" ng-click="taSanpShot()">SnapShot</button>
                    <button class="btn custom-heading-btn text-uppercase " ng-click="taCompare()" ng-class="taisDisableCompare ? 'disabled' : ''">Compare</button>
                </div>
            </div>
            <!--  Transaction Analysis Panel Wrapper Ends  -->
               <!--  Source Management Panel Wrapper Starts  -->
               <div ng-if="currentState == 'sourceManagement'" class="utilities-panel-contents-wrapper utility-manage-wrapper clearfix">
                    <div class="col-lg-9 col-md-6 col-sm-6">
                      
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
                            <button type="button" id="addSource" ng-click="openModalForAddSource()" class="btn grad-button ">Add
                                    source</button>
                    </div>
                </div>
                <!--  Source Management Panel Wrapper Ends  -->
            
            <!--  lead Generation Panel Wrapper Starts  -->
            <div ng-if="currentState == 'cluster' || currentState == 'EAD' || currentState == 'UCtwoCluster' || currentState == 'generalCardHolderDetails' || currentState == 'dualCardHolderDetails' || currentState == 'predictingDualCardHolderDetails' || currentState == 'customer'"
                class="utilities-panel-contents-wrapper utility-predict-wrapper clearfix">
                <div class="col-lg-9 col-md-6 col-sm-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-wrapper mar-t15">
                            <li class="breadcrumb-item" ng-class="{'active': currentState == 'leadneneration'}">
                                <a ui-sref="leadneneration">{{view}}</a>
                            </li>
                            <li   class="breadcrumb-item" ng-class="{'active': currentState == 'cluster'}" ng-if="currentState == 'generalCardHolderDetails' || currentState == 'cluster' || (currentState == 'customer' && fromEod=='true')">
                                <a ui-sref="cluster">Cluster</a>
                            </li>
                            <li class="breadcrumb-item" ng-class="{'active': currentState == 'UCtwoCluster'}" ng-if="currentState == 'UCtwoCluster' || currentState == 'dualCardHolderDetails'">
                                <a ui-sref="UCtwoCluster">Cluster</a>
                            </li>
                            <li class="breadcrumb-item" ng-class="{'active': currentState == 'EAD'}" ng-if="currentState == 'EAD' || (currentState == 'customer' && fromEod=='false')">
                                <a ui-sref="EAD">Early Adopter Details</a>
                            </li>
                             <li class="breadcrumb-item"  ng-click ="redirectToDetailPageWithId('UC1')" ng-class="{'active': currentState == 'generalCardHolderDetails'}" ng-if="currentState == 'generalCardHolderDetails' || (currentState == 'customer' && fromEod=='true')">
                                <a ui-sref="generalCardHolderDetails">Details Page</a>
                            </li>
                            <li class="breadcrumb-item" ng-class="{'active':showDetailPageActive }"  ng-if="(showDetailPage && currentState == 'dualCardHolderDetails')" ng-click="backToChord()">
                                <a ui-sref="dualCardHolderDetails">Details Page</a>
                            </li>
                              <li class="breadcrumb-item" ng-class="{'active':showComparisonPageBreadCrumbsActive }"  ng-if="(showComparisonPageBreadCrumbs && currentState == 'dualCardHolderDetails')">
                                <a ui-sref="dualCardHolderDetails">Comparison Page</a>
                            </li>
                            <li class="breadcrumb-item" ng-class="{'active': currentState == 'customer' }" ng-if="currentState == 'customer'">
                                <a ui-sref="customer">Customer</a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
                    <button class="btn custom-heading-btn text-uppercase generate-button-wrapper disabled" style="" ng-disabled="true" ng-click="JSONToCSVConvertor(data1.myInputArray, data1.exportFilename, data1.displayLabel)">Generate Report</button>
                </div>
            </div>
            
            <!--  lead Generation Panel Wrapper Ends  -->

            <!--  Transaction Intelligence Panel Wrapper Starts  -->
            <div class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix"  ng-if="currentState == 'addDataCuration'">
                <div class="col-lg-9 col-md-6 col-sm-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-wrapper mar-t15">
                            <li class="breadcrumb-item">
                                <a ui-sref="dataCuration">Data Curation</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Add Data Curation</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
                    <label class="custom-radio white">
                        <input id="simpleForm" type="radio" name="mode" ng-model="simpleForm" ng-click="ChangeForm(simpleForm)" value="simple">
                        <span>
                            <i class="fa fa-circle"></i>Simple Form</span>
                    </label>
                    <label class="custom-radio white">
                        <input id="fullForm" type="radio" name="mode" ng-model="fullForm" ng-click="ChangeForm(fullForm)" value="full">
                        <span>
                            <i class="fa fa-circle"></i>Full Form</span>
                    </label>

                </div>
            </div>
            <!--  Transaction Intelligence Panel Wrapper Ends  -->
            <!--  docparser Panel Wrapper Starts  -->
              <div class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix" ng-if="currentState == 'docParserPage'|| currentState == 'docparser'">
                   <div class="col-lg-7 col-md-6 col-sm-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-wrapper mar-t15">
                            <li class="breadcrumb-item"  ng-class="{'active': currentState == 'docparser'}">
                                <a id= "docparserLanding" >Data Parsing</a>
                            </li>
                             <li class="breadcrumb-item" ng-if="currentState == 'docParserPage'">
                                <a id= "docparserTemplatename" ui-sref="docParserPage" ng-if="!storyClick" ng-class="{'active': currentState == 'docParserPage'}" ></a>
                                 <span ng-show="!storyClick" class=" right-addon text-cream fa fa-pencil" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'" ng-click="storyClick = true;"> </span>
                                <input ng-show="storyClick" ng-model="templateName" type="text" name="" id="templateName" class="custom-input pad-l5 pad-r25 width-50 d-ib   border-0 "ng-keypress="($event.keyCode == 13)?storyClick = false:storyClick = true;myFunct($event,templateName)">
                                <span ng-show="storyClick" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Save" popover-trigger="'mouseenter'" class=" right-addon1  f-16 c-pointer  text-dark-blue fa fa-check" ng-click="storyClick = false;changedDocument(templateName,'change')"></span>
                                <span ng-show="storyClick" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Cancel" popover-trigger="'mouseenter'" class=" icon-right-close right-80 c-pointer  f-16   text-dark-blue fa fa-times" ng-click="storyClick = false;changedDocument(document,'dontChange')"></span>

                                <!-- <a id="star" class="fa fa-star f-16 mar-l15 "></a> -->
                                
                               
                            
                            </li>
                           
                        </ol>
                    </nav>
                   
                </div>
           <div class="col-lg-5 col-md-6 col-sm-6 tasubmenu-btn-wrapper pad-r15 "  ng-class="{'docparser-buttons-wrapper':currentState == 'docParserPage'}">
                      <div class="d-ib" id="docParserPreviewDiv"><button class="btn grad-button text-capitalize border-0 px-2 docParserbtns" ng-click ="handleDocParserButtonclicks('docParserPreview')" id="docParserPreview" ng-if="currentState == 'docParserPage'" >Preview</button></div>
                      <div class="d-ib" id="docParserRunDiv">  <button class="btn grad-button text-capitalize  px-2 docParserbtns" ng-click ="handleDocParserButtonclicks('docParserRun')" id="docParserRun" ng-if="currentState == 'docParserPage'" >Run</button></div>
                        <div class="d-ib" id="docParserCancleDiv">  <button class="btn bordered-button text-capitalize  px-2 docParserbtns" ng-click ="handleDocParserButtonclicks('docParserCancle')" id="docParserCancle" ng-if="currentState == 'docParserPage'">Cancel</button></div>
                            <div class="d-ib" id="docParserSaveDiv">	<button class="btn grad-button border-0 text-capitalize  px-2 docParserbtns" ng-click ="handleDocParserButtonclicks('docParserSave')" id="docParserSave" ng-if="currentState == 'docParserPage'">Save</button></div>
                    <button type="button" class="btn grad-button text-capitalize  border-0 px-2"  ng-click="displayUploadModal()">

                                    <span class="hidden-lg" title="UPLOAD">
                                        <i class="icon icon-cloud-upload"></i>
                                    </span>
                                    <span class="hidden-md hidden-sm hidden-xs text-capitalize"> <i class="icon icon-cloud-upload mar-r5"></i>Upload new file</span>
                    </button>
                    

                </div>
            </div>
             <!--  docparser Panel Wrapper Ends -->
            <!--  MIP Landing Panel Wrapper Starts  -->
            <div ng-if="currentState == 'mipLanding' || currentState == 'transactionIntelligenceLanding' || currentState == 'dashboard'"
                class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix"></div>
            <!--  MIP Landing Panel Wrapper Ends  -->

            <!--  MIP Search Panel Wrapper Starts  -->
            <div class="utilities-panel-contents-wrapper db-util-background2-discover utility-discover-wrapper clearfix mip-search-heading-wrapper"
                ng-class="{'utility-mip-wrapper':isMip}" ng-if="currentState == 'mip'">
                <div class="row">
                    <div class="col-sm-9">
                        <div class=""></div>
                        <h4 class="heading-wrapper text-uppercase" id="curentSearchName">
                            <div id="SearchTxtSpan"></div>
                            <span ng-click="closeMipSearch()"></span>
                        </h4>
                    </div>
                    <div class="col-sm-3 text-right">
                        <button class="btn btn-primary custom-heading-btn" ng-click="showMipSaveModal()">Save</button>
                        <!--<button class="btn btn-primary custom-heading-btn" ng-click="showMipExportModal()">export</button> -->
                    </div>
                </div>
            </div>
            <!--  MIP Search Panel Wrapper Starts  -->
            
            <!--  Enrich Panel Wrapper Starts  -->
 			<div class="utilities-panel-contents-wrapper utility-enrich-wrapper pull-right" ng-if="currentState == 'enrich' || currentState == 'entityPage'  || currentState == 'entityCompany' || currentState == 'entityCompanyNew' || currentState == 'social-live-feed' || currentState == 'entityCompanyStatic'">
                <div class="row d-flex ai-c">
                    <div class="col-xs-6">
                        <span class="search-text">ADVANCED SEARCH</span>
                    </div>
                    <div class="col-xs-6 btn-onboarding-wrapper d-flex ai-c jc-fe">
                    <div id="generateReportMainDiv" class="" style="display:none; margin-right:1%;">
                            <div class="btn-group" ng-if="currentState == 'entityCompanyNew'">
                                    <span class="text-cream mar-l5" uib-popover-template="'sourceCountForEntities.html'" ng-click="addScrollToEvidencePopup()" popover-is-open="myPopover.isOpen" 
								 popover-placement="bottom-left" popover-append-to-body='true'  popover-class="bottom-pop-wrapper source-count-for-entities-wrapper"><i class="fa mar-x10 f-28 c-pointer fa-link"></i></span>
                            </div>
                            <div class="btn-group" ng-if="currentState == 'entityCompanyNew'" id="entityClipboard">
                                    <a ng-click="submenu.entityClipboard(true)" href class=" px-3 ">
                                        <span class="fa f-28 mar-r10 text-cream fa-paperclip"></span>
                                    </a>
                                </div>
                                <div class="btn-group d-if ai-c mar-b0 bst-checkbox-wrapper" style="" >
                                        <div class="custom-checkbox d-ib d-none">
                                                <div class="mar-r5 checkbox-inline"  popover-trigger="'mouseenter'" 
                                                popover-placement="top-left" uib-popover-template="'approveAll.html'"
                                                popover-class="top-popover-wrapper approval-popover old-approval"
                                                popover-append-to-body='true'> <input type="checkbox" ng-model="checkAll" ng-click="markAllWidgets(checkAll)" class="mar-l0 d-block z-99 c-pointer op-0 ">
                                                    <span class="mar-r0" style="border-color: #fbfbfb !important;"> <i class="fa l-1- fa-check" style="color: #fbfbfb !important;"></i></span> </div></div>
			                            <a id="generateReport" ng-click="generateReport()" href class="bordered-button grey-bordered sm-btns d-flex ai-c px-3 btn-onboarding">
			                                <span style="white-space: nowrap;">GENERATE REPORT</span>
			                            </a>
                            	</div></div>
                     <div class="btn-group" style="margin-right:1%;" ng-if="currentState == 'entityCompanyNew' && allowChecklist">
		                            <a ng-click="displayIDVQuestionnaire()" href class="bordered-button grey-bordered sm-btns d-flex ai-c px-3 btn-onboarding">
		                                <span>CHECKLIST</span>
		                            </a>
		                        </div>
                        <div class="btn-group">
                            <a ng-click="submenu.onboarding(true)" href class="bordered-button grey-bordered sm-btns d-flex ai-c px-3 btn-onboarding">
                                <span>ONBOARDING</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/ng-template" id="approveAll.html">
                <div class="approval-popover-wrapper height-100 border-b0">
                        <div class=top-content-wrapper>
                                <h3 class="roboto-regular mar-b0 f-14 roboto-regular mar-b0 f-14 text-dark-grey2"><span>Click to check/uncheck all as reviewed</span> </h3>
                        </div>
                </div>
        </script>
            <!--  Enrich Panel Wrapper Ends  -->


            <!--  Upload documents Wrapper Starts  -->
            <div class="utilities-panel-contents-wrapper utility-act-wrapper db-util-background-act" ng-if="currentState == 'uploadDocuments'">
                <div class="row">
                    <div class="col-lg-9 mar-t10 col-md-6 col-sm-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-wrapper mar-t5">
                                <li class="breadcrumb-item active" aria-current="page">Questionnaire list</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="utilities-panel-contents-wrapper utility-act-wrapper db-util-background-act" ng-if="currentState == 'analyzeDetails'">
                <div class="row">
                    <div class="col-lg-7 mar-t10 col-md-6 col-sm-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-wrapper mar-t5">
                                <li class="breadcrumb-item">
                                    <a ui-sref="uploadDocuments">Questionaire list</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Application for cyber insurance</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-xs-5 upload-buttons-wrapper ">
                        <ul class="upload-buttons list-inline pull-right">
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-click="submenu.onboarding(true)"><span>SAVE</span></button>
                                
                            </li>
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-click="submenu.onboarding(true)">
                                    <span>CLOSE</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-click="onDocLinkAnalysis($event)" target="_blank">
                                    <span>Link Analysis</span>
                                </button>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <!--  Upload documents Wrapper Ends  -->
            <!--  Act Panel Wrapper Starts  -->
            <div class="utilities-panel-contents-wrapper  utility-act-wrapper db-util-background-act" ng-if="currentState == 'act' || currentState == 'actCase' || currentState == 'linkAnalysis'
                           || currentState == 'casePageLanding' || currentState == 'auditTrail'">
                <div class="row-fluid">
                    <div class="col-xs-6 case-list-wrapper" ng-if="currentState == 'actCase' || currentState == 'casePageLanding'">
                        <ul class="list-inline clearfix case-list">
                            <li class="case-list-item">
                                <div>
                                    <span style="font-weight: 500">CASE NAME: </span>
                                    <span style="font-weight: 500" title="{{submenu.caseSeedDetails.name}}">{{submenu.caseSeedDetails.name}}</span>
                                </div>
                                <div>
                                    <span title="{{(submenu.cumulativeRisk) * 100}}%">Risk Transaction: {{submenu.cumulativeRisk| number: 0}}%</span>
                                </div>
                            </li>
                            <li class="case-list-item">
                                <div class="case-type">
                                    <span>TYPE: </span>
                                    <span title="{{submenu.caseSeedDetails.type}}">{{submenu.caseSeedDetails.type}}</span>
                                </div>
                                <div class="case-id">
                                    <span>CASE ID: </span>
                                    <span title="{{submenu.caseSeedDetails.caseId}}">{{submenu.caseSeedDetails.caseId}}</span>
                                </div>
                            </li>
                            <li class="case-list-item">
                                <div>
                                    <span>DESCRIPTION: </span>
                                    <span title="{{submenu.caseSeedDetails.description}}">{{submenu.caseSeedDetails.description| limitTo:32}}{{submenu.caseSeedDetails.description.length
                                        > 32 ? '...' : ''}}</span>
                                </div>
                                <div>
                                    <span>REMARKS: </span>
                                    <span title="{{submenu.caseSeedDetails.remarks}}">{{submenu.caseSeedDetails.remarks| limitTo:40}}{{submenu.caseSeedDetails.remarks.length
                                        > 40 ? '...' : ''}}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-6 upload-buttons-wrapper pull-right" ng-if="currentState == 'act' || currentState == 'actCase' || currentState == 'casePageLanding'">
                        <ul class="upload-buttons list-inline clearfix">
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-disabled="true" ng-click="onDissemination($event)">
                                    <span class="hidden-lg" title="DISSEMINATION">
                                        <i class="fa fa-file-powerpoint-o"></i>
                                    </span>
                                    <span class="hidden-md hidden-sm hidden-xs">DISSEMINATION</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-disabled="true" ng-click="submenu.uploadDocuments();">
                                    <span class="hidden-lg" title="UPLOAD">
                                        <i class="fa fa-upload"></i>
                                    </span>
                                    <span class="hidden-md hidden-sm hidden-xs">UPLOAD</span>
                                </button>
                            </li>

                            <!-- SHOULD ONLY DISPLAY FOR KYC -->
                            <!-- <span class="onboarding" ng-if="dashboardUtilitiesActType == 'KYC'"> -->

                            <li class="onboarding">
                                <button type="button" class="btn btn-sm px-2" ng-class="{'btn-danger':incomplete}" ng-click="submenu.onboarding(true)">
                                    <span class="hidden-lg glyphicon glyphicon-alert" title="ONBOARDING">
                                        <i ng-class="{'glyphicon glyphicon-alert':incomplete}"></i>
                                    </span>
                                    <span class="hidden-xs hidden-sm hidden-md">ONBOARDING</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-sm px-2" ng-disabled="!submenu.reassignment" ng-click="submenu.reAssignCase();">
                                    <span class="hidden-lg" title="RE-ASSIGN">
                                        <i class="fa fa-retweet"></i>
                                    </span>
                                    <span class="hidden-xs hidden-sm hidden-md">RE-ASSIGN</span>
                                </button>
                            </li>
                            <li>
                                <a class="btn btn-sm px-2" ng-click="onLinkAnalysis($event)" target="_blank">
                                    <span class="hidden-lg" title="Link Analysis">
                                        <i class="fa fa-link"></i>
                                    </span>
                                    <span class="hidden-xs hidden-sm hidden-md">Link Analysis</span>
                                </a>
                            </li>
                            <li ng-if="currentState == 'actCase'">
                                <button type="button" class="btn btn-sm px-2" ng-click="onEntity($event)" target="_blank">
                                    <span>Entity</span>
                                </button>
                            </li>
                            <li ng-if="currentState == 'actCase'">
                                <button type="button" class="btn btn-sm px-2" disabled>
                                    <span class="hidden-lg" title="RESOLVE">
                                        <i class="fa fa-check-square"></i>
                                    </span>
                                    <span class="hidden-xs hidden-sm hidden-md">RESOLVE</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--  Discover Panel Wrapper Ends  -->

            <!--  Manage Panel Wrapper Starts  -->
            <div class="utilities-panel-contents-wrapper utility-manage-wrapper db-util-background-manage" ng-if="currentState == 'userManagement' || currentState == 'appManager' || currentState == 'dataSource' || currentState == 'systemMonitoring' 
                                || currentState == 'activeProcess' || currentState == 'appConfiguration' || currentState == 'customFields' || currentState == 'exportImport' || currentState == 'jobs'
                                || currentState == 'addNewSources' || currentState == 'bigDataRules' || currentState == 'deploymentProcess' || currentState == 'fields' || currentState == 'licenseManager'
                                || currentState == 'socialManager' || currentState == 'businessRules' || currentState == 'deployments' || currentState == 'groups' || currentState == 'logics'
                                || currentState == 'administrations' || currentState == 'crystalBall' || currentState == 'events' || currentState == 'hosts' || currentState == 'accessControls'
                                || currentState == 'manageDb' || currentState == 'passwords' || currentState == 'manageSources' || currentState == 'myApps' || currentState == 'myDataSources'
                                || currentState == 'monitor' || currentState == 'roles' || currentState == 'modelWorkspace' || currentState == 'ontology' || currentState == 'systemProperties'
                                || currentState == 'portalSettings' || currentState == 'processInstance' || currentState == 'reports' || currentState == 'response' || currentState == 'portalInstances'
                                || currentState == 'serveSettings' || currentState == 'suspendedProcess' || currentState == 'tasks' || currentState == 'users' || currentState == 'store'">
                <div class="activiti-subheader-wrapper">
                    <div class="container-fluid clearfix">
                        <div class="task-subheader">
                            <h4>Management Hub</h4>
                            <h6>Business Process Workflow</h6>
                        </div>
                        <div class="task-subheader-content">
                            <ul class="list-inline">
                                <li ng-class="{'active': activeTasks}">
                                    <a class="text-uppercase" ui-sref="tasks">
                                        <i class="fa fa-server"></i>
                                        <span class="text-uppercase">TASKS</span>
                                    </a>
                                </li>
                                <li ng-class="{'active': activeProcesses}">
                                    <a class="text-uppercase" ui-sref="processInstance">
                                        <i class="fa fa-cogs"></i>
                                        <span class="text-uppercase">Processes</span>
                                    </a>
                                </li>
                                <li ng-class="{'active': activeReports}">
                                    <a class="text-uppercase" ui-sref="reports">
                                        <i class="fa fa-file-text"></i>
                                        <span class="text-uppercase">Reports</span>
                                    </a>
                                </li>
                                <li ng-class="{'active': activeManage}">
                                    <a class="text-uppercase" ui-sref="manageDb">
                                        <i class="fa fa-sliders"></i>
                                        <span class="text-uppercase">Manage</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="utilities-panel-contents-wrapper utility-manage-wrapper db-util-background-manage" ng-if="currentState == 'identityUsers' || currentState == 'identityGroups' || currentState == 'identityPersonal'">

                <!-- 			<div class="utilities-panel-contents-wrapper db-util-process-tab-manage db-util-background-manage"> -->
                <div class="activiti-subheader-wrapper">
                    <div class="container-fluid clearfix">
                        <div class="row">
                            <div class="col-sm-2 task-subheader text-center">
                                <a class="btn btn-link" ng-click="goToBpm()">ELEMENT BPM</a>
                            </div>
                            <div class="col-sm-10 task-subheader-content">
                                <ul class="nav navbar-nav" ng-cloak>
                                    <li ng-class="{'active': currentState == 'identityUsers'}" ng-show="admin == true">
                                        <a ui-sref="identityUsers">Users</a>
                                    </li>
                                    <li ng-class="{'active': currentState == 'identityGroups'}" ng-show="admin == true">
                                        <a ui-sref="identityGroups">Groups</a>
                                    </li>
                                    <li ng-class="{'active': currentState == 'identityPersonal'}">
                                        <a ui-sref="identityPersonal">Personal</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  Manage Panel Wrapper Ends  -->

            <!-- 		<div class="utilities-panel-contents-wrapper db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'editWorkflow'"> -->
            <!-- 			<div class="subheader-wrapper"> -->
            <!-- 				<div class="container-fluid"> -->
            <!-- 					<div class="row"> -->
            <!-- 						<div class="col-xs-6"> -->
            <!-- 							<h4>Management Hub</h4> -->
            <!-- 							<a href="javascript:void(0);">Workflow</a>  -->
            <!-- 							<span class="fa fa-angle-right"></span>  -->
            <!-- 							<a href="javascript:void(0);">Integration and Data Science Platform</a> -->
            <!-- 						</div> -->
            <!-- 						<div class="col-xs-6" id="save"> -->
            <!-- 							<div class="NameUpdate"> -->
            <!-- 								<label>Workflow Name :</label>  -->
            <!-- 								<input class="workflowNameChange" name="workflow.title" ng-model="workflow.title" type="text" id="ChangeName"> -->
            <!-- 							</div> -->
            <!-- 							<button class="text-uppercase btn btn-info" type="button" id="save-workflow" ng-click="onClickSaveWorflow()">Save</button> -->
            <!-- 						</div> -->
            <!-- 					</div> -->
            <!-- 				</div> -->
            <!-- 			</div> -->
            <!-- 		</div>	 -->

            <div class="utilities-panel-contents-wrapper db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'editWorkflow'">
                <!--  Sub Header Wrapper Starts  -->
                <div class="subheader-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-6">
                                <h4>Management Hub</h4>
                                <a href="javascript:void(0);">Workflow</a>
                                <span class="fa fa-angle-right"></span>
                                <a href="javascript:void(0);">Integration and Data Science Platform</a>
                            </div>
                            <div class="col-xs-6" id="save">
                                <div class="NameUpdate">
                                    <label>Workflow Name :</label>
                                    <input class="workflowNameChange" name="workflow.title" ng-model="workflow.title" type="text" id="ChangeName">
                                </div>
                                <button class="text-uppercase btn btn-info" type="button" id="save-workflow" ng-click="onClickSaveWorflow()">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  Sub Header Wrapper Ends  -->
            </div>
        </div>
        <!--  Utilities Panel Wrapper Ends  -->
  
 <!--  Evidence Source Count popover Wrapper Starts  -->
              <script type="text/ng-template" id="sourceCountForEntitiesCount.html">
                <div class="d-flex ai-c"><p class="mar-b0 text-cream"><span>4</span> Entities Selected</p><button class="bordered-button mar-autol mar-r10 sm-btns">Deselect</button> <button class="grad-button d-flex ai-c sm-btns"><i class="fa fa-file mar-r5"></i>Add To Page</button></div>
              </script>
        <script>
            $('.upload-buttons-wrapper').mThumbnailScroller({
                axis: "x",
                speed: 10
            });
            $('.utility-act-wrapper .case-list-wrapper').mThumbnailScroller({
                axis: "x",
                speed: 10
            });
          
        </script>