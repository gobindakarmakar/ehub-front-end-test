<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!--  Utilities Panel Wrapper Starts  -->
<div class="clearfix utilities-panel-wrapper db-util-background-{{submenu.dashboardName}}" ng-controller="SubMenuController">
   
   <!--  Custom Spinner Starts  -->	
   <div class="custom-spinner case-dairy-spinner" ng-show="submenu.fetchingcaseDocuments">
   		<i class="fa fa-spinner fa-spin fa-3x"></i>
   </div> 
  
   <!--  Custom Spinner Ends  -->	
   
   <!--  Utility Group Starts  -->	
   <div class="btn-group utility-group" uib-dropdown >
      <a id="single-button" class="dropdown-toggle" uib-dropdown-toggle>
	      <i class="fa fa-bars icon-left visible-xs"></i>
	      <strong class="hidden-xs">{{submenu.dashboardName}}</strong>
	      <i class="fa fa-angle-down icon-right"></i>
      </a>
      <ul class="dropdown-menu bg-{{submenu.hoverdashboardName | lowercase}}" uib-dropdown-menu role="menu" aria-labelledby="single-button">
         <li>
            <uib-tabset active="submenu.activeDropdown">
               <uib-tab index="$index" class="bg-{{dashboardName.menu | lowercase}}" ng-repeat="dashboardName in submenu.dashboarDropDownMenuItems" ng-mouseover="submenu.onhoversubMenuDropdown($index, dashboardName.menu, $event)" ng-click="submenu.onclicksubMenuDropdown(dashboardName.menu)">
                  <uib-tab-heading>
			        <i class="{{dashboardName.mainIconClass}}"></i> 
			      </uib-tab-heading>
                  <h3 ng-if="submenu.dashboardName != submenu.hoverdashboardName.toLowerCase()" ng-class="{'submenu-title': submenu.hoverdashboardName.toLowerCase() ? true : false}">{{submenu.hoverdashboardName}}</h3>
                  <ul class="list-inline list-submenu">
                     <li ng-click="submenu.getSubMenu(content, content.disabled)" ng-repeat="content in dashboardName.content">	                    
                     	<a ng-disabled="submenu.checkSubmenuDisable(content.disabled);" ng-class="submenu.addClass($index);"><i class="{{content.iconClass}} icon"></i>{{content.name}}</a>
                     </li>
                  </ul>
               </uib-tab>
            </uib-tabset>
         </li>
      </ul>
   </div>
   <!--  Utility Group Ends  -->	
   
   
   <!--  Discover Panel Wrapper Starts  -->
   <div ng-if="currentState == 'discover' || currentState == 'transactionMonitoring'" class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix">
	   
	   <!--  Discover Bar Starts  -->
	   <div class="discover-bar">
	   		<ul class="list-inline discover-bar-list" ng-class="{'utility-mip-wrapper':isMip}" >
		       <!-- <li ng-repeat="item in submenu.countAllCaseStatus" class="hello flex-grid counter flex-grid-expand">
		         <span class="counter-label">{{item.label}}</span> 
		         <span class="counter-value">{{item.value | zpad:2}}</span>
		      </li> -->
	   		</ul>
   		</div>   	
   		<!--  Discover Bar Ends  -->
   		
   		<!--  Work Diary Dropdown Wrapper Starts  -->	
		<div class="dropdown work-diary-dropdown-wrapper" id="work-diary-dropdown-button-panel" ng-controller="YourWorkDiaryController" ng-if="currentState == 'discover' || currentState == 'discoverCase' || currentState == 'workspace' || currentState == 'investigationLanding' || currentState == 'transactionMonitoring'"  ng-class="{'open': workDairyToggle}"	ng-click="dashboardUtilitiesPanelDiscoverOnClickWorkDiaryDropDown();">
			<div id="work-diary-dropdown-button">
				<span class="icon-left"><i class="fa fa-rss"></i></span>
				<div class="work-diary-dropdown-button-text hidden-xs">
					<div>YOUR WORK DIARY</div>
					<div class="small">New cases has been assigned to you</div>
				</div>
				<span class="pull-right"><i class="fa fa-angle-down"></i></span>
			</div>
			<div id="work-diary-dropdown-menu-panel" class="dropdown-menu work-diary-dropdown-menu-panel" style="min-width={{submenu.workDiaryWidth}}">
				<ul class="list-unstyled">
					<li>
						<button type="button" id="work-diary-create-case" class="btn btn-default" ng-click="displayCreate()">
							<span id="work-diary-create-case-icon" class="pull-left">
								<i class="fa fa-plus"></i>
							</span>
							<span id="work-diary-create-case-text">CREATE CASE</span>
						</button>
					</li>
					<li>
						<div id="work-diary-count-case">
							<div>
								<span id="work-diary-count-case-num">{{dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount | zpad:2}}</span> 
								<span id="work-diary-count-case-text">Cases Pending for your action. Please initiate your action.</span>
							</div>
						</div>
					</li>
					<li>
						<div id="work-diary-sub-menu">
							<div id="work-diary-sub-menu-filter" ng-click="OpenfilterCaseDataModal()" class="pull-right">
								<span><i class="fa fa-filter"></i></span>
							</div>
							<div id="work-diary-sub-menu-camera"  ng-click="submenuCaptureDiv('createCaseWrapper')" class="pull-right">
								<span><i class="fa fa-camera"></i></span>
							</div>
						</div>
					</li>
					<li>
						<span ng-show="yourWorkDairyPreloader" class="custom-spinner case-dairy-spinner"> 
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</span>
						<div id="createCaseWrapper" style="overflow-y: auto; overflow-x: hidden; height: 100%; max-height: 470px; margin: 0;">
							<dashboard-utilities-discover-work-diary-case-list index="$index"
								name="item.name" purpose="item.description" type="item.type"
								assignedby="item.assignedBy" trisk="item.transactionalRisk"
								drisk="item.directRisk" irisk="item.indirectRisk" seedid="item.caseId"
								currentstatus="item.currentStatus"
								ng-repeat="item in dashboardUtilitiesPanelDiscoverWorkDiaryCaseList"
								ng-mouseover=""
								on-click-act-dropdown="discoverOnClickWorkDiaryActDropDown(event, index)"
								is-hide-act-dropdown="discoverIsHideWorkDiaryActDropDown(index)"
								on-click-act-dropdown-item="discoverOnClickWorkDiaryActDropDownItem(event, ev, index, seedid, currentstatus)"
								on-click-work-diary-card = "discoverClickWorkDiaryCard(index)"
								is-hide-detail-section = "discoverIsHideDetailSection(index)">
							</dashboard-utilities-discover-work-diary-case-list>
						</div>
						<div class="text-center" ng-hide="dashboardUtilitiesPanelDiscoverWorkDiaryCaseList.length == 0 || allCasesCount <= 10">
							<ul uib-pagination total-items="allCasesCount" ng-model="pageNumber" ng-change="pageChanged(pageNumber)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
						</div>
					</li>
	     		 </ul>
	    	</div>
	   </div>
	   <!--  Work Diary Dropdown Wrapper Ends  -->
	   
	</div>
    <!--  Discover Panel Wrapper Ends  -->
	 
	<!--  Transaction Analysis Panel Wrapper Starts  -->
	<div ng-if="currentState == 'transactionIntelligence'" class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix">
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h4 class="heading-wrapper text-uppercase" id="currenttransName"></h4>		
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
			<button class="btn custom-heading-btn text-uppercase" ng-click="taSanpShot()">SnapShot</button>
			<button class="btn custom-heading-btn text-uppercase " ng-click="taCompare()" ng-class="taisDisableCompare?'disabled':'' ">Compare</button>
		</div>
	</div>
	<!--  Transaction Analysis Panel Wrapper Ends  -->
	
	<!--  Transaction Intelligence Panel Wrapper Starts  -->
	<div class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix">
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h4 class="heading-wrapper text-uppercase" id="currenttransName"></h4>		
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6 tasubmenu-btn-wrapper">
			<div class="custom-radio">
				<label class="radio-inline"><input type="radio" name="role"><span>Single</span><i class="fa fa-circle"></i></label>
				<label class="radio-inline"><input type="radio" name="role"><span>All</span><i class="fa fa-circle"></i></label>
			</div>
		</div>
	</div>
	<!--  Transaction Intelligence Panel Wrapper Ends  -->
	
	<!--  MIP Landing Panel Wrapper Starts  -->
 	<div ng-if="currentState == 'mipLanding' || currentState == 'transactionIntelligenceLanding' || currentState == 'dashboard'" class="utilities-panel-contents-wrapper utility-discover-wrapper clearfix"></div>
	<!--  MIP Landing Panel Wrapper Ends  -->
	
	<!--  MIP Search Panel Wrapper Starts  -->
	<div class="utilities-panel-contents-wrapper db-util-background2-discover utility-discover-wrapper clearfix mip-search-heading-wrapper" ng-class="{'utility-mip-wrapper':isMip}" ng-if="currentState == 'mip'">
		<div class="row">
			<div class="col-sm-9">
				<div class=""></div>
				<h4 class="heading-wrapper text-uppercase" id="curentSearchName"> 
					<div id="SearchTxtSpan"></div>
					<span ng-click="closeMipSearch()"></span>
				</h4>
			</div>
			<div class="col-sm-3 text-right">
				<button class="btn btn-primary custom-heading-btn" ng-click="showMipSaveModal()">Save</button>
				<!--<button class="btn btn-primary custom-heading-btn" ng-click="showMipExportModal()">export</button> -->
			</div>
		</div>
	</div>
	<!--  MIP Search Panel Wrapper Starts  -->
	
	<!--  Enrich Panel Wrapper Starts  -->
   <div class="utilities-panel-contents-wrapper utility-enrich-wrapper pull-right" ng-if="currentState == 'enrich' || currentState == 'entityPage' || currentState == 'entityCompany' || currentState == 'entityCompanyNew' || currentState == 'social-live-feed' || currentState == 'entityCompanyStatic'">
      <div class="row">
         <div class="col-xs-6">
            <span class="search-text">ADVANCED SEARCH</span>
         </div>
         <div class="col-xs-6 btn-onboarding-wrapper">
            <div class="btn-group">
               <a ng-click="submenu.onboarding(true)" href class="btn btn-primary px-3 btn-onboarding"><span>ONBOARDING</span></a>
            </div>
         </div>
      </div>
   </div>
   <!--  Enrich Panel Wrapper Ends  -->
   
   <!--  Act Panel Wrapper Starts  -->
   <div class="utilities-panel-contents-wrapper  utility-act-wrapper db-util-background-act" ng-if="currentState == 'act' || currentState == 'actCase' || currentState == 'linkAnalysis'
   			|| currentState == 'casePageLanding' || currentState == 'auditTrail'">
		<div class="row-fluid">
			<div class="col-xs-6 case-list-wrapper" ng-if="currentState == 'actCase' || currentState == 'casePageLanding'">
				<ul class="list-inline clearfix case-list">
					<li class="case-list-item">
						<div>
							<span style="font-weight: 500">CASE NAME: </span>
							<span style="font-weight: 500" title="{{submenu.caseSeedDetails.name}}">{{submenu.caseSeedDetails.name}}</span>
						</div>
						<div>
							<span title="{{(submenu.cumulativeRisk)*100}}%">Risk Transaction: {{submenu.cumulativeRisk | number: 0}}%</span>
						</div>
					</li>
					<li class="case-list-item">
						<div class="case-type">
							<span>TYPE: </span> 
							<span title="{{submenu.caseSeedDetails.type}}">{{submenu.caseSeedDetails.type}}</span>
						</div>
						<div class="case-id">
							<span>CASE ID: </span> 
							<span title="{{submenu.caseSeedDetails.caseId}}">{{submenu.caseSeedDetails.caseId}}</span>
						</div>
					</li>
					<li class="case-list-item">
						<div>
							<span>DESCRIPTION: </span> 
							<span title="{{submenu.caseSeedDetails.description}}">{{submenu.caseSeedDetails.description | limitTo:32}}{{submenu.caseSeedDetails.description.length > 32 ? '...' : ''}}</span>
						</div>
						<div>
							<span>REMARKS: </span> 
							<span title="{{submenu.caseSeedDetails.remarks}}">{{submenu.caseSeedDetails.remarks | limitTo:40}}{{submenu.caseSeedDetails.remarks.length > 40 ? '...' : ''}}</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-xs-6 upload-buttons-wrapper pull-right" ng-if="currentState == 'act' || currentState == 'actCase'|| currentState == 'casePageLanding'">
				<ul class="upload-buttons list-inline clearfix">
					<li>
						<button disabled type="button" class="btn btn-sm px-2">
							<span class="hidden-lg" title="DISSEMINATION">
								<i class="fa fa-file-powerpoint-o"></i>
							</span> 
							<span class="hidden-md hidden-sm hidden-xs">DISSEMINATION</span>
						</button>
					</li>
					<li>
						<button type="button" class="btn btn-sm px-2" ng-disabled="!submenu.isUploadQuestionaire" ng-click="submenu.uploadDocuments();">
							<span class="hidden-lg" title="UPLOAD">
								<i class="fa fa-upload"></i>
							</span>
							<span class="hidden-md hidden-sm hidden-xs">UPLOAD</span>
						</button>
					</li>
					
					<!-- SHOULD ONLY DISPLAY FOR KYC -->
					<!-- <span class="onboarding" ng-if="dashboardUtilitiesActType == 'KYC'"> -->
					
					<li class="onboarding">
						<button type="button" class="btn btn-sm px-2" ng-class="{'btn-danger':incomplete}" ng-click="submenu.onboarding(true)">
							<span class="hidden-lg glyphicon glyphicon-alert" title="ONBOARDING">
								<i ng-class="{'glyphicon glyphicon-alert':incomplete}"></i>
							</span> 
							<span class="hidden-xs hidden-sm hidden-md">ONBOARDING</span>
						</button>
					</li>
					<li>
						<button type="button" class="btn btn-sm px-2" ng-disabled="!submenu.reassignment" ng-click="submenu.reAssignCase();">
							<span class="hidden-lg" title="RE-ASSIGN">
								<i class="fa fa-retweet"></i>
							</span> 
							<span class="hidden-xs hidden-sm hidden-md">RE-ASSIGN</span>
						</button>
					</li>
					<li>
				      <a class="btn btn-sm px-2" ng-click="onLinkAnalysis($event)" target="_blank"> 
					       <span class="hidden-lg" title="Link Analysis">
				        		<i class="fa fa-link"></i>
					       </span>
					       <span class="hidden-xs hidden-sm hidden-md">Link Analysis</span>
				      </a>
				     </li>
					<li ng-if="currentState == 'actCase'">
						<button type="button" class="btn btn-sm px-2" ng-click="displayResolveModal()">
							<span class="hidden-lg" title="RESOLVE">
								<i class="fa fa-check-square"></i>
							</span> 
							<span class="hidden-xs hidden-sm hidden-md">RESOLVE</span>
						</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--  Discover Panel Wrapper Ends  -->
	
	<!--  Manage Panel Wrapper Starts  -->	
	<div class="utilities-panel-contents-wrapper utility-manage-wrapper db-util-background-manage" ng-if="currentState == 'userManagement' || currentState == 'appManager' || currentState == 'dataSource' || currentState == 'systemMonitoring' || currentState == 'generalSettings'
				|| currentState == 'activeProcess' || currentState == 'appConfiguration' || currentState == 'customFields' || currentState == 'exportImport' || currentState == 'jobs'
				|| currentState == 'addNewSources' || currentState == 'bigDataRules' || currentState == 'deploymentProcess' || currentState == 'fields' || currentState == 'licenseManager'
				|| currentState == 'socialManager' || currentState == 'businessRules' || currentState == 'deployments' || currentState == 'groups' || currentState == 'logics'
				|| currentState == 'administrations' || currentState == 'crystalBall' || currentState == 'events' || currentState == 'hosts' || currentState == 'accessControls'
				|| currentState == 'manageDb' || currentState == 'passwords' || currentState == 'manageSources' || currentState == 'myApps' || currentState == 'myDataSources'
				|| currentState == 'monitor' || currentState == 'roles' || currentState == 'modelWorkspace' || currentState == 'ontology'|| currentState == 'systemProperties'
				|| currentState == 'portalSettings' || currentState == 'processInstance' || currentState == 'reports' || currentState == 'response' || currentState == 'portalInstances'
				|| currentState == 'serveSettings' || currentState == 'suspendedProcess' || currentState == 'tasks' || currentState == 'users' || currentState == 'store'">
			<div class="activiti-subheader-wrapper">
				<div class="container-fluid clearfix">
					<div class="task-subheader">
						<h4>Management Hub</h4>
						<h6>Business Process Workflow</h6>
					</div>
					<div class="task-subheader-content">
						<ul class="list-inline">
							<li ng-class="{'active': activeTasks}">
								<a class="text-uppercase" ui-sref="tasks"> 
									<i class="fa fa-server"></i>
									<span class="text-uppercase">TASKS</span>
								</a>
							</li>
							<li ng-class="{'active': activeProcesses}">
								<a class="text-uppercase" ui-sref="processInstance"> 
									<i class="fa fa-cogs"></i> 
									<span class="text-uppercase">Processes</span>
								</a>
							</li>
							<li ng-class="{'active': activeReports}">
								<a class="text-uppercase" ui-sref="reports"> 
									<i class="fa fa-file-text"></i> 
									<span class="text-uppercase">Reports</span>
								</a>
							</li>
							<li ng-class="{'active': activeManage}">
								<a class="text-uppercase" ui-sref="manageDb"> 
									<i class="fa fa-sliders"></i> 
									<span class="text-uppercase">Manage</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--  Manage Panel Wrapper Ends  -->
		
<!-- 		<div class="utilities-panel-contents-wrapper db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'editWorkflow'"> -->
<!-- 			<div class="subheader-wrapper"> -->
<!-- 				<div class="container-fluid"> -->
<!-- 					<div class="row"> -->
<!-- 						<div class="col-xs-6"> -->
<!-- 							<h4>Management Hub</h4> -->
<!-- 							<a href="javascript:void(0);">Workflow</a>  -->
<!-- 							<span class="fa fa-angle-right"></span>  -->
<!-- 							<a href="javascript:void(0);">Integration and Data Science Platform</a> -->
<!-- 						</div> -->
<!-- 						<div class="col-xs-6" id="save"> -->
<!-- 							<div class="NameUpdate"> -->
<!-- 								<label>Workflow Name :</label>  -->
<!-- 								<input class="workflowNameChange" name="workflow.title" ng-model="workflow.title" type="text" id="ChangeName"> -->
<!-- 							</div> -->
<!-- 							<button class="text-uppercase btn btn-info" type="button" id="save-workflow" ng-click="onClickSaveWorflow()">Save</button> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div>	 -->
	
	<div class="utilities-panel-contents-wrapper db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'editWorkflow'">
		<!--  Sub Header Wrapper Starts  -->
			<div class="subheader-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-6">
							<h4>Management Hub</h4>
							<a href="javascript:void(0);">Workflow</a> 
							<span class="fa fa-angle-right"></span> 
							<a href="javascript:void(0);">Integration and Data Science Platform</a>
						</div>
						<div class="col-xs-6" id="save">
							<div class="NameUpdate">
								<label>Workflow Name :</label> 
								<input class="workflowNameChange" name="workflow.title" ng-model="workflow.title" type="text" id="ChangeName">
							</div>
							<button class="text-uppercase btn btn-info" type="button" id="save-workflow" ng-click="onClickSaveWorflow()">Save</button>
						</div>
					</div>
				</div>
			</div>
		<!--  Sub Header Wrapper Ends  -->
		</div>
	
		
</div>
<!--  Utilities Panel Wrapper Ends  -->

<script>
$('.upload-buttons-wrapper').mThumbnailScroller({
	axis : "x",
	speed : 10
});
$('.utility-act-wrapper .case-list-wrapper').mThumbnailScroller({
	axis : "x",
	speed : 10
});
</script>
