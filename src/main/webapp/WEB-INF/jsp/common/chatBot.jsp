<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


		<!--  Chat Panel Wrapper Starts  -->
		<div ng-class="{'chat-panel-wrapper':ChatBotScope.chatBot}" ng-controller="ChatBotController">

			<!--  Chat Panel Content Starts  -->
			<div class="chat-panel-content" ng-show="ChatBotScope.chatBot" >
				<!--  Chat Panel Header Starts  -->
				<div class="chat-panel-header" >
					<p class="error-message" ng-if="ChatBotScope.errorHandle != ''">{{ChatBotScope.errorHandle}}
						<a href="javascript:void(0);" ng-click="ChatBotScope.resetChatbot();">Please Try Again</a>
					</p>
					<div class="input-group input-group-header">
						<!-- <span class="input-group-addon left-icon">
							<i class="fa fa-search rotate-90"></i>
						</span>
						<input type="text" class="form-control" /> -->
						<span class="input-group-addon right-icon right-icon-minimize" ng-click="ChatBotScope.minimizeChatBot();">
							<i class="fa fa-minus"></i>
						</span>
						<span class="input-group-addon right-icon" ng-click="ChatBotScope.expandChatbot();">
							<i class="fa fa-arrows rotate-45"></i>
						</span>
					</div>
					<h3 class="title" ng-if="ChatBotScope.backToHomeScreen">
						<a href="javascript:void(0);" class="fa fa-chevron-left arrow-left" ng-click="ChatBotScope.backToHome();"></a>
						Hello {{ehubObject.fullName}}
<!-- 						<span>Yesterday 5:00 PM</span> -->


						<!-- <a href="javascript:void(0);" class="fa fa-minus minimize"></a> -->
						<!-- <a href="javascript:void(0);" class="fa fa-arrows rotate-45 close"></a> -->
					</h3>
				</div>
				<!--  Chat Panel Header Ends  -->

				<!--  Chat Panel Body Starts  -->
				<div class="chat-panel-body" ng-show="ChatBotScope.backToHomeScreen">
					<div class="chat-bot-wrapper">
						<div ng-repeat="chatBot in ChatBotScope.chatbotData">
							<p class="chat-bot-input">

								<!-- 						{{chatBot | json}} -->
								<span>{{chatBot.chatBotInput}}</span>
								<a href="javascript:void(0);" class="btn btn-circle" ng-if="chatBot.chatBotInput != undefined">
									<i class="fa fa-user"></i>
								</a>
							</p>
							<p class="chat-bot-message">
								<a href="javascript:void(0);" class="btn btn-circle" ng-if="chatBot.message != undefined && chatBot.message != ''">
									<img class="img-responsive"  ng-src="{{chatBotGreylogo}}" alt="logo-img" />
								</a>
								<span>{{chatBot.message}}</span>
							</p>
							<ul class="list-inline btn-list" ng-if="chatBot['input-choices'].length > 0">
								<li ng-repeat="inputChoices in chatBot['input-choices']">
									<a href="javascript:void(0)" ng-style="{'pointer-events': chatBot.disableChat ? 'none': 'all'}" class="btn btn-{{chatBot.chatbotColor === $index ? 'blue': 'grey'}}"
									     ng-click="ChatBotScope.submitChatData($event, inputChoices, chatBot)">{{inputChoices}}</a>
								  
<!-- 								    <a href="javascript:void(0)" ng-style="{'pointer-events': chatBot.disableChat ? 'none': 'all'}" class="btn btn-{{chatBot.chatbotColor === $index ? 'blue': 'grey'}}" -->
<!-- 									    ng-click="ChatBotScope.submitChatData($event, inputChoices, chatBot)">{{inputChoices}}</a> -->
								</li>
							</ul>
						</div>
					</div>
					<p class="message" ng-if="ChatBotScope.checkvalue(ChatBotScope.chatbotData);">Please Select from above Options</p>
					<p class="loader" ng-if="ChatBotScope.showLoader">
						<i class="fa fa-spinner fa-spin fa-2x"></i>
					</p>
				</div>
				<!--  Chat Panel Body Ends  -->

				<!--  Chat Panel Footer Starts  -->
				<div class="chat-panel-footer" ng-show="ChatBotScope.backToHomeScreen">
					<form name="chatForm" class="chat-form">
						<div class="input-group input-group-footer" ng-if="!showUpload">
							<span class="input-group-addon left-icon">
								<i class="fa fa-paperclip rotate-90"></i>
							</span>
							<input type="text" class="form-control" placeholder="Type Here" ng-disabled="ChatBotScope.checkvalue(ChatBotScope.chatbotData) || ChatBotScope.disabledChat ? true : false"
							    id="chatBotTextArea" ng-model="ChatBotScope.chatBotInput" name="chtbot" ng-pattern='ChatBotScope.patternMatch' ng-keypress="ChatBotScope.submitChatData($event, ChatBotScope.chatBotInput)"
							/>
							<span class="input-group-addon btn right-icon" ng-disabled="ChatBotScope.checkvalue(ChatBotScope.chatbotData) || ChatBotScope.disabledChat ? true : false" ng-click="ChatBotScope.submitChatData($event, ChatBotScope.chatBotInput)">
								<i class="fa fa-paper-plane"></i>
							</span>
						</div>
						<div class="input-group input-group-footer" ng-if="showUpload">
							<span class="input-group-addon left-icon">
								<i class="fa fa-paperclip rotate-90"></i>
							</span>
							<input type="file" class="form-control" ngf-select ng-model="picFiles" name="file" required ngf-model-invalid="errorFile"
           					  multiple="multiple"/>
							<span class="input-group-addon btn right-icon" ng-click="goToDiscover(picFiles)">
								<i class="fa fa-paper-plane"></i>
							</span>
						</div>
						<span ng-show="chatForm.chtbot.$error.pattern" class="error-message">{{ChatBotScope.patternError}}</span>
					</form>
				</div>
				<!--  Chat Panel Footer Ends  -->
								<div class="recent-conversions-wrapper" ng-show="!ChatBotScope.backToHomeScreen">
									<ul class="list-unstyled custom-list">
										<li class="disabled">
											<a href="javascript:void(0);" ng-click="$event.stopPropagation();">Profile Completions
												<span class="badge">3</span>
											</a>
										</li>
										<li class="disabled">
											<a href="javascript:void(0);">System Walkthrough</a>
										</li>
										<li class="disabled">
											<a href="javascript:void(0);">Customer Support
												<span class="badge">2</span>
											</a>
										</li>
									</ul>
									<h3>Recent Conversions <span>{{ChatBotScope.chatbotData.length > 0 ? 1 : 0}}</span></h3>
									<ul class="list-unstyled custom-list" ng-if="ChatBotScope.chatbotData.length > 0">
										<li>
											<a href="javascript:void(0);" ng-click="ChatBotScope.addNewChat();">{{ehubObject.fullName}}
 												<span class="help-block">Yesterday</span>
											</a>
<!-- 											<a href="javascript:void(0);" ng-click="ChatBotScope.addNewChat();">Alastair Conway -->
<!-- 												<span class="help-block">Yesterday</span> -->
<!-- 											</a> -->
										</li>
<!-- 										<li> -->
<!-- 											<a href="javascript:void(0);">John Hopkings Case Entities -->
<!-- 												<span class="help-block">Yesterday</span> -->
<!-- 											</a> -->
<!-- 										</li> -->
<!-- 										<li> -->
<!-- 											<a href="javascript:void(0);">HSBC Market Intelligence -->
<!-- 												<span class="help-block">12/09/2018</span> -->
<!-- 											</a> -->
<!-- 										</li> -->
<!-- 										<li> -->
<!-- 											<a href="javascript:void(0);">James Cameroon case -->
<!-- 												<span class="help-block">12/09/2018</span> -->
<!-- 											</a> -->
<!-- 										</li> -->
<!-- 										<li> -->
<!-- 											<a href="javascript:void(0);">Anitha Macnaught case -->
<!-- 												<span class="help-block">12/09/2018</span> -->
<!-- 											</a> -->
<!-- 										</li> -->
									</ul>
									<a href="javascript:void(0);" class="btn btn-add" ng-click="ChatBotScope.resetChatbot();">
										<i class="fa fa-plus icon"></i>
									</a>
								</div>

								<div class="profile-completion-wrapper" ng-show="ChatBotScope.showProfile">
									<div class="chat-bot-wrapper">
										<div >
											<p class="chat-bot-input">
<!-- 												<a href="javascript:void(0);" class="btn btn-circle"> -->
<!-- 													<img class="img-responsive"  ng-src="{{chatBotlogo}}" alt="logo-img" /> -->
<!-- 												</a>{{chatBot.chatBotInput}}</p> -->
											<ul class="list-inline info-list">
												<li>
													<a href="javascript:void(0);">
														<i class="fa fa-check success-circle-icon"></i>
														<span>Basic Info</span>
													</a>
												</li>
												<li>
													<a href="javascript:void(0);">
														<i class="fa fa-check success-circle-icon"></i>
														<span>Professional</span>
													</a>
												</li>
												<li>
													<a href="javascript:void(0);">
														<i class="fa fa-check success-circle-icon"></i>
														<span>Financial</span>
													</a>
												</li>
												<li class="active">
													<a href="javascript:void(0);">
														<i class="fa fa-check default-circle-icon"></i>
														<span>Documents</span>
													</a>
												</li>
											</ul>
											<ul class="list-unstyled info-list">
												<li>
													<a href="javascript:void(0);">
														<i class="fa fa-check success-circle-icon"></i>
														<span>Passport</span>
														<i class="fa fa-times remove-icon"></i>
														<span>PPCopy.pdf</span>
														<span>Success</span>
													</a>
												</li>
												<li>
													<a href="javascript:void(0);">
														<i class="fa fa-check default-circle-icon"></i>
														<span>Professional</span>
														<span>Please Upload</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>

			</div>
			<a href="javascript:void(0);" class="chat-link" ng-click="ChatBotScope.toggleChatBot();" ng-if="!ChatBotScope.chatBot" style="z-index: 10000000">
				<sup class="chat-notification" ng-if="ChatBotScope.chatbotData.length != 0">{{ChatBotScope.chatbotData.length}}</sup>
				<img class="img-responsive"  ng-src="{{chatBotlogo}}" alt="logo-img" />
			</a>
		</div>
		<!--  Chat Panel Wrapper Ends  -->