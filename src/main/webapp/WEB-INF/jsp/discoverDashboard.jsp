<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<!--  SubMenu Starts  -->
	<%@include file="common/submenu.jsp"%>
		<!--  SubMenu Ends  -->

		<!--  Main Contents Wrapper Starts  -->
		<div id="main-ground" class="main-contents-wrapper">
			<div id="updateDiv" style="display:none"></div>
			<!--  Discover Dashboard Wrapper Starts  -->
			<div class="dashboard-wrapper discover-dashboard-wrapper" ng-controller='GridsterController'>
				<div gridster='gridsterConfiguration'>
					<ul>
						<!--  Case Diary Widget Starts  -->
						<li gridster-item row="0" col="0" size-x="4" size-y="1" ng-controller="MyCaseDiaryController" id="myCaseDiaryGridster">
							<span ng-show="myCaseDiaryPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-briefcase icon-widget"></i>
								<span>MY CASE DIARY</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" id="" ng-click="OpenfilterCaseDataModal()">
											<i class="fa fa-filter"></i>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)" ng-click="mainCaptureDiv('myCaseDiaryGridster')" id="caseDiaryCapture">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraph">
										<a ng-click="toggleGraph = true">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraph">
										<a ng-click="toggleGraph = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
									<li ng-show="toggleGraph">
										<a ng-click="maximizeDiv('myCaseDiaryGridster')">
											<i class="fa fa-arrows rotate-45"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="widget-content">
								<div class="table-responsive case-diary-table" ng-hide="toggleGraph">
									<table datatable="ng" dt-options="dtOptions" dt-column-defs="dtColumnDefs" class="table table-striped table-scroll">
										<thead>
											<tr>
												<th st-sort="caseId">Seed</th>
												<th st-sort="name">Name</th>
												<th>Priority</th>
											
												<th>Risk Profile</th>
												<th st-sort="modifiedOn">Last Updated</th>
												<th class="pad-r10">Cumulative Risk</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody class="case-diary-list" id="myCaseDiaryItems">
											<tr ng-repeat="case in cases">
												<td class="caseseed" valign="top" ng-click="caseSelect(case.caseId, '', case)">
													<span>{{case.caseId}}</span>
												</td>
												<td class="casename" valign="top" ng-click="caseSelect(case.caseId, case.name)" ng-if="case.type == 'Corporate-KYC' ">
													<a href="javascript:void(0);" class="avatar">
														<img ng-src="{{case.imageBank}}" class="img-responsive img-avatar" alt="corporate" />
													</a>
													<span>{{case.name}}</span>
												</td>
												<td class="casename" valign="top" ng-click="caseSelect(case.caseId, case.name)" ng-if="case.type == 'Individual-KYC'">
													<a href="javascript:void(0);" class="avatar avatar-user">
														<img src="assets/images/menu_icons/user.png" class="img-responsive img-avatar" alt="individual" />
													</a>
													<span>{{case.name}}</span>
												</td>
												<td class="casename" valign="top" ng-click="caseSelect(case.caseId, '', case)" ng-if="case.type == 'Lead Generation'">
													<a href="javascript:void(0);" class="avatar avatar-user">
														<img src="assets/images/icon/leadGeneration.svg" class="img-responsive img-avatar" alt="individual" />
													</a>
													<span>{{case.name}}</span>
												</td>
												 <td class="casepriority" ng-click="caseSelect(case.caseId, '', case)">
													<div ng-show="case.priority == 'LOW'">
														<i class="fa fa-circle low"></i>
													</div>
													<div ng-show="case.priority == 'MEDIUM'">
														<i class="fa fa-circle text-lighter-green"></i>
													</div>
													<div ng-show="case.priority == 'HIGH'">
														<i class="fa fa-circle high"></i>
													</div>
												</td> 
												<!-- <td class="casehealth" ng-click="caseSelect(case.caseId)">{{case.health}}</td> -->
												<td class="caseriskprofile" ng-click="caseSelect(case.caseId, '', case)">
													<div class="risk-progressbar-holder">
														<div class="inline">DR</div>
														<div class="progress">
															<div title="{{case.DR * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{case.DR * 100}}%" aria-valuemin="0"
															    aria-valuemax="100" style="width: {{case.DR * 100}}%">
																<span class="sr-only">{{case.DR * 100}}% Complete</span>
															</div>
														</div>
													</div>
													<div class="risk-progressbar-holder">
														<div class="inline">TR</div>
														<div class="progress">
															<div title="{{case.TR * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{case.TR * 100}}%" aria-valuemin="0"
															    aria-valuemax="100" style="width: {{(case.TR * 100)}}%">
																<span class="sr-only">{{case.TR * 100}}% Complete</span>
															</div>
														</div>
													</div>
													<div class="risk-progressbar-holder">
														<div class="inline">IR</div>
														<div class="progress">
															<div title="{{case.IR * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{case.IR * 100}}%" aria-valuemin="0"
															    aria-valuemax="100" style="width: {{(case.IR * 100)}}%">
																<span class="sr-only">{{case.IR * 100}}% Complete</span>
															</div>
														</div>
													</div>
												</td>
												<td class="caselastupdated" ng-click="caseSelect(case.caseId, '', case)">
													<span>{{case.modifiedOn| date:'MM/dd/yyyy'}}</span>
												</td>
												<td class="casecumulativerisk" ng-click="loadDecisionScoring(case.caseId,case.name,case.caseData)">
													<div class="c100 p{{getRiskScoreValue(case.caseData) | number:0}} pink radial">
														<span>{{getRiskScoreValue(case.caseData)  | number:0}}</span>
														<div class="slice">
															<div class="bar"></div>
															<div class="fill"></div>
														</div>
													</div>
												</td>
												<td class="casestatus">
													<input type="hidden" ng-model="case.currentStatus" /> 
													<!-- <select class="form-control custom-select" ng-model="case.selectedStatus" ng-options="status.code as status.status disable when disableStatus(status.code, case.currentStatus) for status in statusList"
													    ng-change="updateStatus(case.caseId, case.currentStatus, case.selectedStatus)"></select> -->
														<div class="custom-selectbox-wrapper"><select class="form-control custom-select" ng-model="case.selectedStatus" ng-options="status.code as status.status disable when disableStatus(status.code, caseDiaryDataForMyCaseDiarySelectOptions[$index]) for status in statusList"
														ng-change="updateStatus(case.caseId, case.currentStatus, case.selectedStatus)"></select></div>
														
														<!-- <select class="form-control custom-select" ng-model="case.selectedStatus" ng-options="status.key as status.name for status in caseDiaryDataForMyCaseDiarySelectOptions[$index]"
														ng-change="updateStatus(case.caseId, case.currentStatus, case.selectedStatus)">
														<option value="" disabled selected>{{statusName[$index]}}</option>
														</select> -->
														
												</td>
											</tr>
										</tbody>
									</table>
									<div class="text-right" ng-hide="cases.length == 0 || casesLength <= recordsPerPage">
										<ul class="case-diary-pagination" uib-pagination total-items="casesLength" items-per-page="recordsPerPage" ng-model="pageNum"
										    ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
									</div>
								</div>
								<div ng-show="toggleGraph">
									<div class="columnchartdiary" id="chartOptions"></div>
								</div>
							</div>
						</li>
						<!--  Case Diary Widget Ends  -->

						<!--  Top Locations Widget Starts  -->
						<li gridster-item row="0" col="4" size-x="2" size-y="3" ng-controller="TopLocationsController" id="topLocationsdiv">
							<span ng-show="topLocationsPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-map-marker icon-widget"></i>
								<span>TOP LOCATIONS</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" ng-click="mainCaptureDiv('topLocationsdiv', toggleGraphTopLocation)">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraphTopLocation">
										<a href="javascript:void(0);" ng-click="toggleGraphTopLocation = true">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraphTopLocation">
										<a href="javascript:void(0);" ng-click="toggleGraphTopLocation = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="widget-content widget-top-location">
								<div id="mapLeaflet" class="chartContentDiv" ng-show="toggleGraphTopLocation">
									<div class="container-fluid map-info-wrapper" id="mapInfo">
										<div class="row">
											<div class="col-sm-6">
												<span>Total Locations</span>
												<span class="count">{{totalLocation}}</span>
											</div>
											<div class="col-sm-6">
												<span>Total Cases</span>
												<span class="count">{{CountCases}}</span>
											</div>
										</div>
									</div>
									<div id="map"></div>
								</div>
								<div ng-hide="toggleGraphTopLocation" class="table-responsive">
									<table class="table table-striped table-scroll two-col">
										<thead>
											<tr>
												<th>COUNTRY</th>
												<th>COUNT</th>
											</tr>
										</thead>
										<tbody id="topLocationWidget">
											<tr ng-repeat="(location, count) in locationsData" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>{{location}}</td>
												<td>{{count}}</td>
											</tr>
											<!-- 									<tr ng-show="locationsData.length == 0"> -->
											<!-- 										<td colspan="2"> -->
											<!-- 											<p class="no-data">No data found</p> -->
											<!-- 										</td> -->
											<!-- 									</tr> -->
										</tbody>
									</table>
									<div ng-show="locationsData.length == 0">
										<p class="no-data">NO DATA FOUND</p>
									</div>
								</div>
							</div>
						</li>
						<!--  Top Locations Widget Ends  -->

						<!--  Your Case Metrics Widget Starts  -->
						<li gridster-item ng-controller="YourCaseMetricsController" id="yourCaseMetricsdiv">
							<span ng-show="yourCaseMetricsPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-tachometer icon-widget"></i>
								<span>YOUR CASE METRICS</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" ng-click="OpenfilterCaseDataModal()">
											<i class="fa fa-filter"></i>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);" ng-click="mainCaptureDiv('yourCaseMetricsdiv')">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraphCaseMetrics">
										<a href="javascript:void(0);" ng-click="OntoggleGraph()">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraphCaseMetrics">
										<a href="javascript:void(0);" ng-click="toggleGraphCaseMetrics = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="widget-content">

								<div class="yourCaseMetrics" ng-show="toggleGraphCaseMetrics">
									<div class="lower panel-body">
										<div class="col-md-6">
											<h4 class="text-blue">Total Ongoing Cases: {{ongoing.length}}</h4>
										</div>
										<div class="col-md-6">
											<h4 class="text-orange">Delayed: {{delayed.length}}</h4>
										</div>
										<div id="yourCaseMetricsWidget">
											<div class="col-md-6">
												<ol>
													<li ng-repeat="case in ongoing">{{$index + 1}}. {{case.name}}</li>
												</ol>
											</div>
											<div class="col-md-6">
												<ol>
													<li ng-repeat="case in delayed">{{$index + 1}}. {{case.name}}</li>
												</ol>
											</div>
										</div>
									</div>
									<div class="upper panel-body">
										<div class="col-md-12" id="caseMetricsGraph"></div>
									</div>
								</div>
								<div ng-hide="toggleGraphCaseMetrics" class="table-responsive">
									<table class="table table-striped table-scroll two-col">
										<thead>
											<tr>
												<th>CASE STATUSES</th>
												<th>COUNT</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="item in yourCaseMetricsData" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>{{item.key}}</td>
												<td>{{item.value}}</td>
											</tr>
											<!--<tr ng-show="showErrorMsg || yourCaseMetricsData.length== 0">
										<td colspan="2">
											<p class="no-data">No data found</p>
										</td>
									</tr>-->
										</tbody>
									</table>
									<div ng-show="showErrorMsg || yourCaseMetricsData.length== 0">
										<p class="no-data">NO DATA FOUND</p>
									</div>
								</div>
							</div>
						</li>
						<!--  Your Case Metrics Widget Ends  -->

						<!--  Case Focus Widget Starts  -->
						<li gridster-item ng-controller="CaseInFocusController">
							<span ng-show="caseFocusPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-briefcase icon-widget"></i>
								<span>CASE IN FOCUS</span>
							</div>
							<div class="widget-content" id="caseFocusWidget">
								<div id="caseFocus-container">
									<table id="caseFocus" class="table table-case-focus" datatable="ng" dt-column-defs="dtColumnDefs" dt-options="dtOptions1">
										<thead class="caseFocus-header">
											<tr>
												<th>NAME</th>
												<th>RISK</th>
												<th>UPDATED</th>
												<th>STATUS</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="case in data.result" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>
													<div class="custom-media-object" ng-if="case.type == 'Corporate-KYC'">
														<a href="javascript:void(0);" class="avatar">
															<img class="img-responsive img-avatar" ng-src="{{case.imageBank}}" alt="flat-avatar" />
														</a>
														<p ng-trim="true">{{case.name}}</p>
													</div>
													<div class="custom-media-object" ng-if="case.type == 'Individual-KYC'">
														<a href="javascript:void(0);" class="avatar">
															<img src="assets/images/menu_icons/user.png" class="img-responsive img-avatar" alt="individual" />
														</a>
														<p ng-trim="true">{{case.name}}</p>
													</div>
													<div class="custom-media-object" ng-if="case.type == 'Lead Generation'">
														<a href="javascript:void(0);" class="avatar">
															<img src="assets/images/icon/leadGeneration_2.svg" class="img-responsive img-avatar" alt="individual" />
														</a>
														<p ng-trim="true">{{case.name}}</p>
													</div>
												</td>
												<td class="casecumulativerisk">
													<div class="c100 p{{case.cumulativeRisk | number:0}} pink radial">
														<span>{{case.cumulativeRisk | number:0}}</span>
														<div class="slice">
															<div class="bar"></div>
															<div class="fill"></div>
														</div>
													</div>
												</td>
												<td>{{case.modifiedOn | date:'MM/dd/yyyy'}}</td>
												<td class="casestatus">
													<input type="hidden" ng-model="case.currentStatus" />
													<!-- <select class="form-control custom-select" ng-model="case.selectedStatus" ng-options="status.code as status.status disable when disableStatus(status.code, case.currentStatus) for status in statusList"
														ng-change="updateStatus(case.caseId, case.currentStatus, case.selectedStatus)"></select> -->
														<div class="custom-selectbox-wrapper"><select class="form-control custom-select" ng-model="case.selectedStatus" ng-options="status.code as status.status disable when disableStatus(status.code, caseDiaryDataForCaseInFocusSelectOptions[$index]) for status in focusStatusList"
														ng-change="updateStatus(case.caseId, case.currentStatus, case.selectedStatus)"></select></div>
												</td>
											</tr>
											<!-- 									<tr ng-if="data.result.length == 0"> -->
											<!-- 										No data available -->
											<!-- 									</tr> -->
										</tbody>
									</table>
								</div>
							</div>
							<div class="text-right" ng-hide="data.length == 0 || caseFocusLength <= 4">
								<ul class="case-diary-pagination" uib-pagination total-items="caseFocusLength" items-per-page="4" ng-model="pageNum" ng-change="pageChanged(pageNum)"
								    max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
							</div>
						</li>
						<!--  Case Focus Widget Ends  -->

						<!--  Hot Topics Widget Starts  -->
						<li gridster-item ng-controller="HotTopicsController" id="hottopicsdiv">
							<span ng-show="hotTopicsPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-text-width icon-widget"></i>
								<span>HOT TOPICS</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" id="" ng-click="openFilterModalForChart()">
											<i class="fa fa-filter"></i>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);" ng-click="mainCaptureDiv('hottopicsdiv')" id="caseDiaryCapture">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraphHotTopics">
										<a href="javascript:void(0);" ng-click="resizeHotTopics()">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraphHotTopics">
										<a href="javascript:void(0);" ng-click="toggleGraphHotTopics = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
									<li ng-show="toggleGraphHotTopics">
										<a href="javascript:void(0);" ng-click="maximizeDiv('hottopicsdiv')">
											<i class="fa fa-arrows rotate-45"></i>
										</a>
									</li>

								</ul>
							</div>
							<div class="widget-content">
								<div ng-show="toggleGraphHotTopics" id="tagcloud"></div>
								<div ng-hide="toggleGraphHotTopics" class="table-responsive">
									<table class="table table-striped table-scroll two-col">
										<thead>
											<tr>
												<th>Name</th>
												<th>Amount</th>
											</tr>
										</thead>
										<tbody id="hotTopicsWidget">
											<tr ng-repeat="hottopicsData in hotTopicsDatas" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>{{ hottopicsData.name}}</td>
												<td>{{ hottopicsData.count}}</td>
											</tr>
											<!-- <tr ng-show="hotTopicsDatas.length == 0">
										<td colspan="2">
											<p class="no-data">No data found</p>
										</td>
									</tr> -->
										</tbody>
									</table>
									<div ng-show="hotTopicsDatas.length == 0">
										<p class="no-data">NO DATA FOUND</p>
									</div>
								</div>
							</div>
						</li>
						<!--  Hot Topics Widget Ends  -->

						<!--  My Entities Widget Starts  -->
						<li gridster-item size-x="2" size-y="1" ng-controller="MyEntitiesController" id="myEntitiesdiv">
							<span ng-show="myEntitiesPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-code-fork icon-widget"></i>
								<span>MY ENTITIES</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" ng-click="openFilterModalForChart()">
											<i class="fa fa-filter"></i>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);" ng-click="mainCaptureDiv('myEntitiesdiv')" id="caseDiaryCapture">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraphmyEntities">
										<a href="javascript:void(0);" ng-click="openChartGraph()">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraphmyEntities">
										<a href="javascript:void(0);" ng-click="toggleGraphmyEntities = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
									<li ng-show="toggleGraphmyEntities">
										<a href="javascript:void(0);" ng-click="maximizeDiv('myEntitiesdiv')">
											<i class="fa fa-arrows rotate-45"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="widget-content">
								<div class="chartContentDiv" id="myEntitiesChart" ng-show="toggleGraphmyEntities"></div>
								<div ng-hide="toggleGraphmyEntities" class="table-responsive">
									<table class="table table-striped table-scroll three-col">
										<thead>
											<tr>
												<th>DATE</th>
												<th>AMOUNT</th>
												<th>OCCURANCE</th>
											</tr>
										</thead>
										<tbody id="myEntitiesWidget">
											<tr ng-repeat="myentitiesData in myentitiesDatas" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>{{ myentitiesData.date | date:'MM/dd/yyyy'}}</td>
												<td>{{ myentitiesData.rate}}</td>
												<td>{{ myentitiesData.size}}</td>
											</tr>
											<!--  <tr ng-show="myentitiesDatas.length == 0">
										<td colspan="3">
											<p class="no-data">No data found</p>
										</td>
									</tr>-->
										</tbody>
									</table>
									<div ng-show="myentitiesDatas.length == 0">
										<p class="no-data">NO DATA FOUND</p>
									</div>
								</div>
							</div>
						</li>
						<!--  My Entities Widget Ends  -->

						<!--  Entities In Focus Widget Starts  -->
						<li gridster-item size-x="2" size-y="1" ng-controller="EntitiesInFocusController" id="discoverEntitiesInFocusdiv">
							<span ng-show="discoverEntitiesInFocusPreloader" class="custom-spinner case-dairy-spinner">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
							</span>
							<div class='widget-header'>
								<i class="fa fa-crosshairs icon-widget"></i>
								<span>ENTITIES IN FOCUS</span>
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-ellipsis-v ellipsis"></i>
								</a>
								<ul class="dropdown-menu filter-menu">
									<li>
										<a href="javascript:void(0);" id="" ng-click="openFilterModalForEntitiesInFocusChart()">
											<i class="fa fa-filter"></i>
										</a>
									</li>
									<li>
										<a href='javascript:void(0);' ng-click="mainCaptureDiv('discoverEntitiesInFocusdiv')" id="caseDiaryCapture">
											<i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
										</a>
									</li>
									<li ng-hide="toggleGraphdiscoverEntitiesInFocus">
										<a href="javascript:void(0);" ng-click="resizeChart()">
											<i class="fa fa-bar-chart"></i>
										</a>
									</li>
									<li ng-show="toggleGraphdiscoverEntitiesInFocus">
										<a href="javascript:void(0);" ng-click="toggleGraphdiscoverEntitiesInFocus = false">
											<i class="fa fa-list-alt"></i>
										</a>
									</li>
									<li ng-show="toggleGraphdiscoverEntitiesInFocus">
										<a href="javascript:void(0);" ng-click="maximizeDiv('discoverEntitiesInFocusdiv')">
											<i class="fa fa-arrows rotate-45"></i>
										</a>
									</li>
								</ul>
							</div>

							<div class="widget-content">
								<div ng-show="toggleGraphdiscoverEntitiesInFocus" id="discoverEntitiesInFocusBubbleChart"></div>
								<div ng-hide="toggleGraphdiscoverEntitiesInFocus" class="table-responsive">
									<table class="table table-striped table-scroll three-col">
										<thead>
											<tr>
												<th>NAME</th>
												<th>OCCURANCE</th>
												<th>GROUP</th>
											</tr>
										</thead>
										<tbody id="entitiesInFocusWidget">
											<tr ng-repeat="discoverEntitiesInFocusData in discoverEntitiesInFocusDatas" ng-class-odd="'odd'" ng-class-even="'even'">
												<td>{{ discoverEntitiesInFocusData.name}}</td>
												<td>{{ discoverEntitiesInFocusData.size}}</td>
												<td>{{ discoverEntitiesInFocusData.group}}</td>
											</tr>
											<!-- <tr ng-show="discoverEntitiesInFocusDatas.length == 0">
										<td colspan="3">
											<p class="no-data">No data found</p>
										</td>
									</tr> -->
										</tbody>
									</table>
									<div ng-show="discoverEntitiesInFocusDatas.length == 0">
										<p class="no-data">NO DATA FOUND</p>
									</div>
								</div>
							</div>
						</li>
						<!--  Entities In Focus Widget Ends  -->

					</ul>
				</div>
			</div>
			<!--  Discover Dashboard Wrapper Ends  -->
			<wheel-nav-context-menu contextmenuname="discoverDashboard.contentMenu" contextmenuname0="discoverDashboard.contentMenu0"
			    contextmenuname1="discoverDashboard.contentMenu1" contextmenuname2="discoverDashboard.contentMenu2" contextmenuname3="discoverDashboard.contentMenu3"
			    contextmenuname4="discoverDashboard.contentMenu4" contextmenuname5="discoverDashboard.contentMenu5"></wheel-nav-context-menu>
		</div>
		<!--  Main Contents Wrapper Ends  -->