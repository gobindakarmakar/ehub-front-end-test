<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>

<!--  Super Sub Header Wrapper Ends  -->

        <!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="activiti-task-wrapper reports-wrapper user-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">

                                <!--  Aside Wrapper Starts  -->
                                <aside class="aside-wrapper text-uppercase">
                                    <a href="javascript:void(0);" class="btn btn-block btn-create">
                                        <i class="fa fa-plus"></i>Create Group
                                    </a>
                                    <div class="form-group has-success has-feedback">
                                        <input type="text" class="form-control" placeholder="Search" />
                                        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-7"><h3>Groups</h3></div>
                                        <div class="col-sm-5">  
                                            <div class="dropdown clearfix">
                                               <a href="javascript:void(0);" uib-dropdown-toggle class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											   </a>
												<ul  uib-dropdown-menu class="dropdown-menu" aria-labelledby="dLabel">
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
												</ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel custom-panel groups-panel" ng-class="{'active':administrationsdata2.active }" ng-repeat="administrationsdata2 in administrations.administrationsData2">
                                        <div class="panel-heading">
                                          {{administrationsdata2.name}} <strong>{{administrationsdata2.value}}</strong>
                                        </div>
                                    </div>
                                    
                                </aside>
                                <!--  Aside Wrapper Ends  -->

                            </div>
                            <div class="col-md-9 col-sm-8">

                                <!--  Create New Task Wrapper Starts  -->
                                <div class="create-new-task-wrapper">
                                    <div class="create-new-task-inner-wrapper">
                                        <!--  Notification Panel Starts  -->
                                        <div class="panel custom-panel notification-panel user-notification-panel">
                                            <div class="panel-heading process-heading">
                                                <div class="notification-header">
                                                    <h3>Process definitions of running instances</h3>
                                                </div>                                               
                                            </div>
                                        </div>
                                        <!--  Notification Panel Ends  -->

                                        <!--  Associated Groups Panel Starts  -->
                                        <div class="panel custom-panel associated-people-panel associated-groups-panel">
                                            <div class="panel-body investigation-wrapper">        
                                                
                                                <!--  Process Definition Table Starts -->
                                                <h3>Process Definitions</h3>
                                                <div class="table-responsive text-uppercase">
                                                    <table class="table table-condensed table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">Id</th>
                                                                <th width="60%">Name</th>
                                                                <th width="10%">NR of Instances</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                          
                                                            <tr ng-repeat="administrationstable1data in administrations.administrationsTable1Data">
                                                                <td>{{administrationstable1data.Id}}</td>
                                                                <td>{{administrationstable1data.name}}</td>
                                                                <td>{{administrationstable1data.Instances}}</td>
                                                            </tr>                                                                                                      
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--  Process Definition Table Starts -->
                                                
                                                <!--  Process Instances Table Starts -->
                                                <h3>Process Instances</h3>
                                                <div class="table-responsive text-uppercase">
                                                    <table class="table table-condensed table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="10%">Id</th>
                                                                <th width="25%">Business Key</th>
                                                                <th width="25%">Started By</th>
                                                                <th width="20%">Started Activity Id</th>
                                                                <th width="20%">Started</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>                                          
                                                            <tr ng-repeat="administrationstable2data in administrations.administrationsTable2Data">
                                                                <td>{{administrationstable2data.Id}}</td>
                                                                <td>{{administrationstable2data.BusinessKey}}</td>
                                                                <td>{{administrationstable2data.BusinessKey}}</td>
                                                                <td>{{administrationstable2data.StartedActivityId}}</td>
                                                                <td>{{administrationstable2data.Started}}</td>
                                                            </tr>                                                       
                                                                                                             
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--  Process Instances Table Ends -->
                                                
                                            </div>
                                        </div>
                                        <!--  Associated Groups Panel Ends  -->

                                    </div>
                                    <!--  Related Content Panel Ends  -->

                                </div>
                                <!--  Create New Task Wrapper Ends  -->

                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->
  </div>
	