<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--  SubMenu Starts  -->
<%@include file="common/submenu.jsp"%>
<!--  SubMenu Ends  -->
<!--  Main Wrapper Starts  -->
<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="custom-tabs-wrapper">
				<span class="custom-spinner custom-spinner-main full-custom-spinner" ng-show="InvestConsole"> <i
					class="fa fa-spinner fa-spin fa-2x"></i>
				</span>
<!-- 			 Nav Tabs Starts  -->
<!-- 			<ul class="nav nav-tabs"> -->
<!-- 				<li class="active investigationsLaning"><a data-toggle="tab" ng-click='investigationTabClick("InvestigationLanding")'>Investigation Landing</a></li> -->
<!-- 				<li class="investigationDetails"><a data-toggle="tab" ng-click='investigationTabClick("Investigation")'>Investigation</a></li> -->
<!-- 			</ul> -->
			<!--  Investigation Wrapper Starts  -->
			<div class="landing-pages-wrapper" ng-show="showLanding">

				<div class="search-case-wrapper">
					<h5 class="text-uppercase">
						<span class="search-text">All Investigations</span>
					</h5>
					<div class="case-search-input">
						<i class="fa fa-search"></i> <input class="form-control text-uppercase text-center" type="text" placeholder="Search Here"> <a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>

				<div class="case-analysis-wrapper">
					<div class="filter-analysis text-center">
						<ul class="filter-list-wrapper list-inline">
							<li><a class="text-uppercase" href="#all"> All </a></li>
							<li class="active"><a class="text-uppercase" href="#recentlyAccepted"> recently started </a></li>
						</ul>
					</div>
					<div class="case-analysis-content">
						<div class="row">
							<div class="col-sm-3">
								<div class="total-cases-wrapper">
									<ul class="list-unstyled">
										<li class="text-uppercase blue-ta">
											<h2>
												67 <span> Total Investigations </span>
											</h2>
										</li>
										<li class="text-uppercase pink-ta">
											<h2>
												19 <span> Risky Cases </span>
											</h2>
										</li>
										<li class="text-uppercase"><img class="img-responsive" src="assets/images/landing/investigation-progress.png" alt="alberta" /></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-9">
								<div class="total-members-wrapper">
									<div class="row">
										<div class="col-sm-4 col-xs-6">
											<a href  ng-click='investigationTabClick("Investigation")' style="display: block;" class="case-member"> <span class="investigation-icon border-grey"><i class="fa fa-wrench"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Investigation</h4>
													<span class="text-uppercase"> As on 06/12/2012 </span>
												</div>
											</a>
										</div>
										<div class="col-sm-4 col-xs-6">
											<a ui-sref="entityCompany" style="display: block;" class="case-member"> <span class="investigation-icon border-grey"><i class="fa fa-wrench"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">PetroSaudi</h4>
													<span class="text-uppercase"> As on 06/12/2012 </span>
												</div>
											</a>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-wrench"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Amy Smith - HL</h4>
													<span class="text-uppercase"> As on 12/02/2013 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-spinner"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">threat Analysis</h4>
													<span class="text-uppercase"> As on 16/19/2011 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-spinner"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Chase Loans</h4>
													<span class="text-uppercase"> As on 05/01/2013 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-spinner"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Marine Shootout</h4>
													<span class="text-uppercase"> As on 12/19/2014 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-line-chart"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Loan Analysis</h4>
													<span class="text-uppercase"> As on 06/06/2011 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-line-chart"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Marine Shootout</h4>
													<span class="text-uppercase"> As on 11/01/2010 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-line-chart"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Loan Analysis</h4>
													<span class="text-uppercase"> As on 08/05/2013 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-chain"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Fraud Analysis</h4>
													<span class="text-uppercase"> As on 12/19/2012 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-chain"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Loan Analysis</h4>
													<span class="text-uppercase"> As on 09/20/2009 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-chain"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Fraud Analysis</h4>
													<span class="text-uppercase"> As on 01/19/2010 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-pause"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Tony Parker - HL</h4>
													<span class="text-uppercase"> As on 03/03/2013 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-pause"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Fraud Analysis</h4>
													<span class="text-uppercase"> As on 04/21/2010 </span>
												</div>
											</div>
										</div>
										<div class="col-sm-4 col-xs-6">
											<div class="case-member">
												<span class="investigation-icon border-grey"><i class="fa fa-pause"></i></span>
												<div class="case-member-details">
													<h4 class="text-uppercase">Chase Analysis</h4>
													<span class="text-uppercase"> As on 01/29/2014 </span>
												</div>
											</div>
										</div>
									</div>
									<a class="forward-rendering" href="javascript:void(0);"> <i class="fa fa-angle-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!--  Investigation Wrapper Ends  -->
			<div class="custom-modal-drop" ng-hide="showLanding">
				<!--  Main Wrapper Starts  -->
				<div class="main-wrapper">
					<!--  Content Wrapper Starts  -->
					<div class="content-wrapper">
						<!--  Investigation Wrapper Starts  -->
						<div class="modeller-process-wrapper investigation-wrapper">
							<div class="panel investigation-panel">
								<div class="panel-heading clearfix">
									<ol class="breadcrumb text-uppercase">
										<li><a ng-click="loadLanding()" href="javascript:void(0);">Investigations</a></li>
										<li><a href="javascript:void(0);">{{ entityName }}</a></li>
									</ol>
<!-- 									<a href="javascript:void(0);" class="fa fa-ellipsis-v"></a> -->
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-3 col-sm-3 pad-rt-0">
											<div class="media-company text-uppercase">
												<div class="media-left-company">
													<a href="javascript:void(0);">
														<img class="media-object" ng-src="{{ imgPath }}" alt="peter">
													</a>
												</div>
												<div class="media-body-company">
													<h4 class="media-heading-company" title="{{entityName }}">
														{{entityName }}<span>Type : {{ entityType }} </span>
													</h4>
													<p>
														<strong>Start Date :</strong> {{ founded_year }}
														<strong>Updated :</strong> N/A
													</p>
												</div>  
											</div>
										</div>
										<div class="col-md-5 col-sm-5 basic-content-investigation" ng-show="entityDetails.properties.details || entityDetails.properties.summary ">
											<p> {{ entityDetails.properties.details || entityDetails.properties.summary}} </p>
										</div>
										<div class="col-md-4 col-sm-4">
											<div class="custom-head" ng-if="scaledNumArr">
<!-- 												<div class="custom-head-list"> -->
<!-- 													<span>Entities</span> -->
<!-- 													<h2>{{ (entityName.length*6) }}</h2> -->
<!-- 												</div> -->
<!-- 												<div class="custom-head-list"> -->
<!-- 													<span>Related Cases</span> -->
<!-- 													<h2>{{ (entityName.length*2) }}</h2> -->
<!-- 												</div> -->
												<div class="custom-head-list progress-bar-chart-list">
													<div class="bar-holder" title="DR: {{scaledNumArr[1]}}%">
														<div class="inline">DR</div>
														<div class="progress">
															<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{scaledNumArr[1]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[1]}}%">
																<span class="sr-only">{{scaledNumArr[1]}}% Complete</span>
															</div>
														</div>
													</div>
													<div class="bar-holder" title="IR: {{scaledNumArr[0]}}%">
														<div class="inline">IR</div>
														<div class="progress">
															<div class="progress-bar progressir" role="progressbar" aria-valuenow="{{scaledNumArr[0]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[0]}}%"></div>
														</div>
													</div>
													<div class="bar-holder" title="TR: {{scaledNumArr[2]}}%">
														<div class="inline">TR</div>
														<div class="progress">
															<div class="progress-bar progresstr" role="progressbar" aria-valuenow="{{scaledNumArr[2]}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{scaledNumArr[2]}}%"></div>
														</div>
													</div>
												</div>
												<div class="custom-head-list">
													<div id="transPersonCircle" class="header-content-entry person-circle-graph">
														<div class="c100 p{{cumulativeRisk | number: 0}} orange radial">
															<span class="inner-number">
																{{cumulativeRisk | number: 0}} 
																<span class="inner-text">
																	Cumulative 
																	<span>Risk</span>
																</span>
															</span>
															<div class="slice">
																<div class="bar"></div>
																<div class="fill"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!--  Select Tool Wrapper Starts  -->
							<div class="select-tool-wrapper">
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-3 col-sm-6 col-xs-6 common-col">
											<div class="dropdown text-uppercase custom-tool-dropdown">
												<button class="btn dropdown-toggle" data-toggle="dropdown">
													<i class="fa fa-legal icon-left"></i> Select Your Tool <small>Total 5 Tools</small> <i class="fa fa-chevron-down icon-right"></i>
												</button>
												<!-- INVESTIGATION Drop Down Wrapper Starts  -->
												<ul class="tool-list-wrapper dropdown-menu">
													<li>
														<div class="clearfix">
															<span class="left-icon text-center"><i class="fa fa-road"></i></span>
															<h5 draggable="true" ondragstart="drag(event)" class="text-uppercase">Journey and Decision</h5>
															<ul class="list-inline list-action-wrapper pull-right">
																<li><button disabled class="btn more-info text-center" data-toggle="modal" data-target="#investigationInfoWrapper"> more info </a></li>
																<li><button class="btn apply text-uppercase text-center jouneydeci"> Apply </button></li>
															</ul>
														</div>
													</li>
													<li>
														<div class="clearfix">
															<span class="left-icon text-center"><i class="fa fa-list-ul"></i></span>
															<h5 draggable="true" ondragstart="drag(event)" class="text-uppercase">Taxonomy</h5>
															<ul class="list-inline list-action-wrapper pull-right">
																<li><button disabled class="btn more-info text-center" data-toggle="modal" data-target="#investigationInfoWrapper"> more info </button></li>
																<li><button class="btn apply text-uppercase text-center"> Apply </button></li>
															</ul>
														</div>
													</li>
													<li>
														<div class="clearfix">
															<span class="left-icon text-center"><i class="fa fa-pause"></i></span>
															<h5 draggable="true" ondragstart="drag(event)" class="text-uppercase">Comparison Analysis</h5>
															<ul class="list-inline list-action-wrapper pull-right">
																<li><button disabled class="btn more-info text-center" data-toggle="modal" data-target="#investigationInfoWrapper"> more info </button></li>
																<li><button class="btn apply text-uppercase text-center"> Apply </button></li>
															</ul>
														</div>
													</li>
													<li>
														<div class="clearfix">
															<span class="left-icon text-center"><i class="fa fa-users"></i></span>
															<h5 draggable="true" ondragstart="drag(event)" class="text-uppercase dropdown-item-text">Cluster Analysis</h5>
															<ul class="list-inline list-action-wrapper pull-right">
																<li><button disabled class="btn more-info text-center"> more info </button></li>
																<li><button class="btn apply text-uppercase text-center"> Apply </button></li>
															</ul>
														</div>
													</li>
													<li>
														<div class="clearfix">
															<span class="left-icon text-center"><i class="fa fa-flask"></i></span>
															<h5 draggable="true" ondragstart="drag(event)" class="text-uppercase">Dynamic and Temporal Analysis</h5>
															<ul class="list-inline list-action-wrapper pull-right">
																<li><button disabled class="btn more-info text-center" data-toggle="modal" data-target="#investigationInfoWrapper"> more info </button></li>
																<li><button disabled class="btn apply text-uppercase text-center"> Apply </button></li>
															</ul>
														</div>
													</li>
												</ul>
												<!-- INVESTIGATION Drop Down Wrapper Ends  -->
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6 common-col">
											<h3 title="LARGE AMOUNT INTERNATIONAL TRANSFER TO OFFSHORE JURISDICTION">
												<i class="fa fa-user-secret custom-icon"></i>{{ scenario }} <span>Start date : {{ scenarioDate }}</span>
											</h3>
										</div>
										<div class="col-md-3 col-sm-6 col-xs-12 common-col">
											<div class="dropdown text-uppercase custom-tool-dropdown">
												<button class="btn dropdown-toggle" data-toggle="dropdown">
													Transactions <small>Total {{ transactionDetails.length }} Transactions</small> <i class="fa fa-chevron-down icon-right"></i>
												</button>
												<!-- INVESTIGATION Drop Down Wrapper Starts  -->
												<ul class="tool-list-wrapper transactions-investagation-list dropdown-menu">
													<li class="panel-group" ng-repeat="transact in transactionDetails">
														<div class="panel panel-default">
														    <div class="panel-heading">
														    	<div class="tranx-panel-heading-content">
														    		<ul class="list-inline">
														    			<li class="tranx-heading-list">
														    				<div class="panel-tranx-id">
														    					<p>ID : <span> {{ transact.id || "N/A"}}</span></p>
														    				</div>
														    			</li>
														    			<li class="tranx-heading-list">
														    				<div class="panel-tranx-date">
														    					<p>Date : <span> {{ (transact.date | date:"dd/MM/yyyy") || "N/A" }}</span></p>
														    				</div>
														    			</li>
														    			<li class="tranx-heading-list">
														    				<div class="panel-tranx-type">
														    					<p>Type : <span> {{ transact.txype || "N/A" }}</span></p>
														    				</div>
														    			</li>
														    			<li class="tranx-heading-list">
														    				<div class="panel-tranx-counterparty">
														    					<p>Counter Party : <span> {{ transact.counterParty || "N/A" }} </span></p>
														    				</div>
														    			</li>
														    		</ul>
														    	</div>
														    </div>
														    <div class="panel-body">
														    	<div class="tranx-panel-body-content">
														    		<div class="row">
														    			<div class="col-sm-6">
														    				<div class="detailed-tranx-counterparty">
														    					<ul class="list-unstyled">
														    						<li>
														    							<p class="detailed-tranx-header">Beneficiary </p>
														    						</li>
														    						<li>
														    							<p> Name : <span>{{ transact.namebene || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Amount : <span>{{ transact.activity_amount_bene || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Location : <span>{{ transact.countrybene || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Currency : <span>{{ transact.currencybene || "N/A" }}</span></p>
														    						</li>
														    					</ul>
														    				</div>
														    			</div>
														    			<div class="col-sm-6 pad-lt-0">
														    				<div class="detailed-tranx-ownparty">
														    					<ul class="list-unstyled">
														    						<li>
														    							<p class="detailed-tranx-header">Organization </span></p>
														    						</li>
														    						<li>
														    							<p>Name : <span>{{ transact.nameorg || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Amount : <span>{{ transact.activity_amount_org || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Location : <span>{{ transact.countryorg || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<p>Currency  : <span>{{ transact.currencyorg || "N/A" }}</span></p>
														    						</li>
														    						<li>
														    							<div class="detailed-tranx-amount {{ transact.DirectoinCls}}">
																	    					<p>{{ transact.inputOut }} 
																	    						<strong>{{ (transact.amt  | currency:"$":0) || "N/A" }}</strong>
																	    					</p>
																	    				</div>
														    						</li>
														    					</ul>
														    				</div>
														    			</div>
														    			<!-- <div class="col-sm-4 pad-lt-0">
														    				<div class="detailed-tranx-purpose">
														    					<ul class="list-unstyled">
														    						<li>
														    							<p>Type : <span>{{transact.txtype2  }}</span></p>
														    						</li>
														    						<li>
														    							<p>Data Source : <span>{{ (transact['data-source']?transact['data-source']:"-") }}</span></p>
														    						</li>
														    						<li>
														    							<p>Purpose : <span class="purpose-first">{{ transact.purpose1  }}</span> , <span class="purpose-second">{{ transact.purpose1  }}</span></p>
														    						</li>
														    						<li>
														    							<div class="detailed-tranx-amount {{ transact.DirectoinCls}}">
																	    					<p>{{ transact.inputOut }} 
																	    						<strong>{{ transact.amt  | currency:"$":0 }}</strong>
																	    					</p>
																	    				</div>
														    						</li>
														    					</ul>
														    				</div>
														    			</div>-->
														    		</div>
														    	</div>
														    </div>
														 </div>
													</li>
												</ul>
												<!-- INVESTIGATION Drop Down Wrapper Ends  -->
											</div>
										</div>
										<div class="col-md-2 col-sm-6 col-xs-12 common-col">
											<ul class="list-inline share-icon-list">
												<li ng-click="captureImage('chartContentDiv')"><button class="btn" title="SAVE" href="javascript:void(0);"><i class="fa fa-save"></i></button></li>
												<li><button disabled class="btn" title="SHARE" href="javascript:void(0);"><i class="fa fa-share-alt"></i></button></li>
												<li><button disabled class="btn" title="RESOLVE" href="javascript:void(0);"><i class="fa fa-check"></i></button></li>
											</ul>
										</div>
									</div>
								</div>
							</div>  
							<!--  Select Tool Wrapper Ends  -->

							<div class="chart-panel-wrapper">
								<div class="row">
									<div class="col-sm-12">
										<ul class="list-inline tags-list tags-list-wrapper">
											<!--                                    <li class="active"><a href="javascript:void(0);">Taxonomy<i class="fa fa-times"></i></a></li>
                                    <li><a href="javascript:void(0);">Inference Analysis<i class="fa fa-times"></i></a></li>-->
										</ul>
									</div>
									<div class="col-sm-2"></div>
								</div>
								<div class="row">
									<div id="chartContentDiv" ondrop="drop(event)" ondragover="allowDrop(event)" style="min-height: 300px"></div>
								</div>
							</div>
						</div>
						<!--  Investigation Wrapper Ends  -->
					</div>
					<!--  Content Wrapper Ends  -->
				</div>
				<!--  Main Wrapper Ends  -->

				<!-- Investigation Modal Wrapper Starts  -->
				<div id="investigationInfoWrapper" class="modal fade investigation-modal-wrapper" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-md" role="document">
						<form id="changeValueMessage">
							<div class="modal-content">
								<div class="modal-header clearfix">
									<div class="add-user-submit">
										<button type="submit" class="custom-submit" ng-click="modalClose()">
											<span class="text-uppercase">Apply</span>
										</button>
									</div>
									<div class="modal-title">
										<p class="text-uppercase">Cluster Analysis</p>
									</div>
								</div>
								<div class="modal-body">
									<div class="form-stages clearfix">
										<div class="row">
											<div class="col-sm-4">
												<div class="analysis-wrapper">
													<p class="analysis-description">Social network analysis [SNA] is the mapping and measuring of relationships and flows between people, groups, organizations, computers, URLs, and other connected information/knowledge entities. The nodes in the network are the people and groups while the links show relationships or flows between the nodes</p>
													<div class="analysis-applications">
														<h5 class="text-uppercase">When to be used</h5>
														<p>Social Analysis can be used when the customer seeks to know about the info of the flow of the product with respect to the chosen analysis. Many of the analysis can be used same the content shown with the flow of the analysis. Many nodes describes what ever the data flow at the end of the node.</p>
													</div>
													<div class="analysis-similarities">
														<h5 class="text-uppercase">Similar Analysis Type</h5>
														<ol>
															<li class="text-uppercase">Comparison Analysis</li>
															<li class="text-uppercase">Inference Analysis</li>
															<li class="text-uppercase">Dynamic and Temporary Analysis</li>
														</ol>
													</div>
												</div>
											</div>
											<div class="col-sm-8">
												<div class="chart-wrapper">
													<div class="chart-demo-wrapper">
														<h5 class="text-uppercase">Demo</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->

<%-- <%@include file="footer.jsp"%> --%>
<!-- jQuery Js -->
<script src="http://d3js.org/d3.v4.min.js"></script>
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$(".db-util-process-tab-discover").mThumbnailScroller({
				axis : "x"
			});
			$(".select-tool-charts").mCustomScrollbar({
				axis : "y",
				theme : "minimal-dark"
			});
			$(".transactions-investagation-list").mCustomScrollbar({
				axis : "y",
				theme : "minimal-dark"
			});
		}, 500);
	});
</script>



