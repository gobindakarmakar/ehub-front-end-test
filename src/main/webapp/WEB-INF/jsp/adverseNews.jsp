<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>

<span class="custom-spinner showMarketPulseLoader" ng-show="showMarketPulseLoader"> <i
	class="fa fa-spinner fa-spin fa-2x"></I>
</span>
<!--  Main Wrapper Starts  -->
<div class="main-wrapper">
	<!--  Content Wrapper Starts  -->
	<div class="content-wrapper">
		<div class="market-pulse-wrapper">
			<div class="pulse-analysis-header">
				<div class="row">
					<div class="col-sm-3">
						<div class="analysis-header-space pulse-set-period">
							<h4 class="text-uppercase">Set period</h4>
							<div class="set-period-select">
								<select class="text-uppercase" id="setPeriod" ng-model="setPeriod" ng-change="ChangePeriod()">
									<option>Year</option>
									<option>Month</option>
									<option>Week</option>
									<option>Day</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="analysis-header-space pulse-risk-ratio">
							<h4 class="text-uppercase">Risk Ratio</h4>
							<div class="text-uppercase custom-chart-div">
								<div class="row">
									<div class="col-sm-5">
										<div class="risk-ratio-list">
											<ul class="list-unstyled text-uppercase">
												<li><span class="high-red">High</span></li>
												<li><span class="medium-blue">Medium</span></li>
												<li><span class="low-green">Low</span></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-7">
										<div id="riskRatio"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="analysis-header-space pulse-entity-ratio">
							<h4 class="text-uppercase">Entity Ratio</h4>
							<div class="text-uppercase custom-chart-div">
								<div class="row">
									<div class="col-sm-5">
										<div class="entity-ratio-list">
											<ul class="list-unstyled text-uppercase">
												<li><span class="">people</span></li>
												<li><span class="">Organization</span></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-7">
										<div id="entityRatio"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="analysis-header-space pulse-risk-trend">
							<h4 class="text-uppercase">Risk Trend</h4>
							<div class="text-uppercase" id="riskTrend"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pulse-analysis-body">
				<div class="pulse-analysis-content clear-fix">
					<div class="pulse-analysis-left-col text-uppercase">
						<div class="analysis-total-wrapper">
							<h4>Start An Analysis</h4>
							<div class="start-analysis-wrapper">
								<div class="custom-upload-wrapper">
									<div class="custom-file-upload">
		                                <input type="file" name="fileuploadOne" id="fileuploadOne" ng-model="uploadFIlenameOne" >
		                                <label for="fileuploadOne" class="file-inline" ngf-select="upload($file,'one')">
		                                    <span class="custom-input">
		                                		<strong class="text-uppercase uploaded-name"  >{{uploadnameone }} </strong>
	                                			<small class="text-uppercase" >tx-file</small>
		                                	</span>
		                                </label>
		                            </div>
								</div>
								<div class="custom-upload-wrapper mb20">
									<div class="custom-file-upload">
		                                <input type="file" name="fileuploadTwo" ng-model="fileuploadTwo" >
		                                <label for="fileuploadTwo" class="file-inline" ngf-select="upload($file,'two')">
		                                	<span class="custom-input">
		                                		<strong class="text-uppercase uploaded-name"  >{{ uploadnameTwo }}</strong>
	                                			<small class="text-uppercase" >ent-file</small>
		                                	</span>
		                                </label>
		                            </div>
								</div>
								<a class="btn text-uppercase text-center"
									href="javascript:void(0);"  ng-click="uploadFile()"> <span>Upload</span>
								</a>
								<a class="btn text-uppercase text-center"
									href="javascript:void(0);" id="startAnalysis" ng-click="loadmarketPulsedata()" ng-disabled="startAnalysisDisabled"> <span>Start</span>
								</a>
							</div>
						</div>
						<div class="analysis-total-wrapper">
							<h4>Risk Total</h4>
							<div class="risk-total-wrapper">
								<div class="riskscoreprogress">
									<div class="risk-progressbar-holder risk-high">
										<div class="progress-wrapper">
											<div class="inline">HiGH</div>
											<div class="progress" style="width: {{ (riskTotalPct[0]?riskTotalPct[0]:0) | number : 0 }}%">
												<div class="progress-bar progressdr" role="progressbar"
													aria-valuenow="{{ riskTotal[0] | number : 0 }}"
													aria-valuemin="0"
													aria-valuemax="{{ $scope.totalRiskAmount }}"
													style="width: 100%"></div>
											</div>
											<div class="ng-binding progress-footer">{{ riskTotal[0]
												| number : 0 }}</div>
										</div>
									</div>
									<div class="risk-progressbar-holder risk-medium">
										<div class="progress-wrapper">
											<div class="inline">MEDIUM</div>
											<div class="progress" style="width: {{ (riskTotalPct[1]?riskTotalPct[1]:0) | number : 0 }}%">
												<div class="progress-bar progressdr" role="progressbar"
													aria-valuenow="{{ riskTotal[1] | number : 0 }}"
													aria-valuemin="0"
													aria-valuemax="{{ $scope.totalRiskAmount }}"
													style="width: 100%">
													<!-- 													<span class="sr-only ng-binding">{{ riskTotal[1] | number : 0 }}%</span> -->
												</div>
											</div>
											<div class="ng-binding progress-footer">{{ riskTotal[1]
												| number : 0 }}</div>
										</div>
									</div>
									<div class="risk-progressbar-holder risk-low">
										<div class="progress-wrapper">
											<div class="inline">LOW</div>
											<div class="progress" style="width: {{ (riskTotalPct[2]?riskTotalPct[2]:0) | number : 0 }}%">
												<div class="progress-bar progressdr" role="progressbar"
													aria-valuenow="{{ riskTotal[2] | number : 0 }}"
													aria-valuemin="0"
													aria-valuemax="{{ $scope.totalRiskAmount }}"
													style="width: 100%"></div>
											</div>
											<div class="ng-binding progress-footer">{{ riskTotal[2]
												| number : 0 }}</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="analysis-total-wrapper block-two">
							<h4>Transaction Amount Trend</h4>
							<div class="amount-trend-wrapper">
								<div id="trans_amt_trend"></div>
							</div>
						</div>
						<!-- 						<div class="analysis-total-wrapper block-two"> -->
						<!-- 							<h4>Top Locations</h4> -->
						<!-- 							<div class="top-locations-wrapper"> -->

						<!-- 							</div> -->
						<!-- 						</div> -->
					</div>
					<div class="pulse-analysis-right-col">
						<div class="market-pulse-table">
							<div id="marketPulseTableDiv">
								<table id="marketPulseTable" width="100%" datatable="ng" dt-options="dtOptions" class="table text-uppercase">
									<thead>
										<th>Name</th>
										<th>Amount</th>
										<th>Date</th>
										<th>Entity Type</th>
										<th>Risk</th>
									</thead>
									<tbody>
										<tr class="custom-market-modal"  ng-repeat="marketpulse in adversTxData" ng-click="marketModalClick(marketpulse)">
										<!-- ng-click="marketModalClick()"--> 
											<td>{{ marketpulse['entity-name']}}</td>
											<td>{{ marketpulse['amount'] | currency}}</td>
											<td>{{ marketpulse['time']}}</td>
											<td>{{ marketpulse['entity-type']}}</td>
											<td><span ng-class="{{marketpulse.tableColor}}">{{ marketpulse['risk']}}</span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  Content Wrapper Ends  -->
</div>
<!--  Main Wrapper Ends  -->
<script>
	$(document).ready(function() {
		setTimeout(function() {
			$("#bargraphsSrcoller").mThumbnailScroller({
				axis : "x"
			});
		}, 500);
		$('.pulse-analysis-left-col').mCustomScrollbar({
			axis : "y",
			theme : "minimal-dark"
		});
	});
</script>




