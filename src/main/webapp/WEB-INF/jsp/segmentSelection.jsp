<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

		<!--  Super Sub-Menu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super Sub-Menu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->


				<!--  Segment Dashboard Wrapper Starts  -->

				<div class="dashboard-wrapper isra-card-wrapper segment-dashboard-wrapper bg-dark-grey1">

					<!--  Main Filters Wrapper Starts  -->
					<div class="main-filters-wrapper">

						<!-- Top Grid Wrapper-->
						<div class="top-grid-wrapper container-fluid">
							<ul class="list-inline custom-tabs row top-filters-list">
								<li class="col-sm-4">
									<div class="col-sm-9">
										<p class="top-heading">
											<i class="fa fa-angle-left icon-right"></i> Family having two kids</p>
										<div id="familyChart col-sm-3"></div>
									</div>
								</li>
								<li class="col-sm-4">
									<div class="col-sm-6">
										<strong class="value">672,889 </strong>
										<h4 class="top-grid-heading">Total Costumers</h4>
									</div>

									<div class="col-sm-6">
										<strong class="value">$348,990,12 </strong>
										<h4 class="top-grid-heading">Total Spendings</h4>
									</div>
								</li>
								<li class="col-sm-4">
									<div class="top-filters segment-filters-wrappers">

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list">
											<li class="progressbar-list-item">
												<div class="left-col">Average Expenditure</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:95%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
												<div class="right-col">$34,990</div>
											</li>

											<li class="progressbar-list-item">
												<div class="left-col">GP Average</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:80%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
												<div class="right-col">$14,990</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->

									</div>
								</li>
							</ul>
						</div>
						<!--Top Grid Wrapper Ends-->

						<!--  Top Filters Wrapper Starts  -->
						<div class="top-filters-wrapper segment-filters-wrappers">
							<ul class="top-filter-navigation list-inline ">
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Age</h4>

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list">
											<li class="progressbar-list-item">
												<div class="left-col">10-20</div>
												<div class="progress">

													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:100%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">20-40</div>
												<div class="progress">

													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:80%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">40-60</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:60%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">60-above</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:40%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->

									</div>
								</li>

								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Gender</h4>
										<div class="col-sm-6 filters-subhead-wrapper">
											<div class="filters-subhead">male </div>
											<div class="filters-subhead">female</div>
											<div class="filters-subhead">Others</div>
										</div>
										<div id="" class="col-sm-6"></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Risk Level</h4>

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list">
											<li class="progressbar-list-item">
												<div class="left-col">a-b</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:95%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">b-s</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:80%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Average Spending</h4>
										<div id=""></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Average Spending</h4>
										<div id=""></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Transaction Type</h4>

										<!--  Progressbar List Starts -->
										<ul class="list-unstyled progressbar-list">
											<li class="progressbar-list-item">
												<div class="left-col">10-20</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:90%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">20-40</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:75%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">40-60</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:50%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
											<li class="progressbar-list-item">
												<div class="left-col">60-above</div>
												<div class="progress">
													<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
													 style="width:30%">
														<span class="sr-only ">100 Complete</span>
													</div>
												</div>
											</li>
										</ul>
										<!--  Progressbar List Ends -->
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Transaction Mode</h4>
										<div id=""></div>
									</div>
								</li>
								<li>
									<div class="top-filters">
										<h4 class="top-filter-heading">Transaction Mode</h4>
										<div id=""></div>
									</div>
								</li>
							</ul>
						</div>
						<!--  Top Filters Wrapper Ends  -->

					</div>
					<!--  Main Filters Wrapper Ends  -->

					<!-- Main Segment Content Starts Here -->
					<div class="main-segment-content">
						<div class="row main-segment-wrapper">
							<div class="col-sm-9 pad-r0 custom-col left-col">
								<div id="customerUsageChart"></div>
							</div>

							<!-- Right Col Starts -->
							<div class="col-sm-3 custom-col right-col">
								<div class="row custom-row usage-panel-wrapper">
									<div class="col-sm-12 custom-col segment-right-panel">

										<!-- Segment Panel Starts -->
										<div class="panel custom-panel-wrapper  segment-panel-wrapper">
											<div class="panel-heading">
												<div class="row">
													<div class="col-md-8">
														<h4>Isra card</h4>
													</div>
													<div class="col-md-4">
														<img src="assets/images/isra-card/isracard.png" alt="IsraCard Image" height="36" width="53">
													</div>
												</div>

											</div>

											<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
												<div class="panel-data-wrapper">
													<h3>Customers</h3>
													<strong>123,982,90</strong>
													<h3>Spendings</h3>
													<strong>$278,908</strong>
												</div>
											</div>
										</div>
										<!-- Segment Panel Ends -->

										<!-- Segment Panel Starts -->
										<div class="panel custom-panel-wrapper  segment-panel-wrapper">
											<div class="panel-heading">
												<h3>Cash Holding Pattern</h3>
											</div>
											<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
												<!--  Progressbar List Starts -->
												<ul class="list-unstyled progressbar-list">
													<li class="progressbar-list-item">
														<div class="left-col">Mobiles Phones</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:90%">
																<span class="sr-only ">1 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">90%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">PC And Tablets</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:80%">
																<span class="sr-only">1651 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">80%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">Cloths</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:70%">
																<span class="sr-only">710 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">70%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">Footwears</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:60%">
																<span class="sr-only">5 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">60%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">Cosemetics</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:50%">
																<span class="sr-only">2921 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">50%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">Pc Games</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:40%">
																<span class="sr-only">168 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">40%</div>
													</li>
													<li class="progressbar-list-item">
														<div class="left-col">Ice-Creams</div>
														<div class="progress">
															<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
															 style="width:30%">
																<span class="sr-only">32 Complete</span>
															</div>
														</div>
														<div class="right-col-pink">30%</div>
													</li>
												</ul>
												<!--  Progressbar List Ends -->
											</div>

											<!-- Segment Panel Ends -->

											<!-- Segment Panel Starts -->
											<div class="panel custom-panel-wrapper segment-panel-wrapper">
												<div class="panel-body">
													<div class="col-sm-6 left-col">
														<h3>Primary Card</h3>
														<div class="custom-content">MASTERCard </div>
													</div>


													<div class="col-sm-6 right-col">
														<h3>Secondary Card</h3>
														<div class="custom-content">Cit</div>
													</div>
												</div>
											</div>
											<!-- Segment Panel Ends -->

											<!-- Segment Panel Starts -->
											<div class="panel custom-panel-wrapper segment-panel-wrapper">
												<div class="panel-heading">
													<h3>Usage Pattern</h3>
												</div>
												<div class="panel-body border-b0">
													<div id=""></div>
												</div>
											</div>
											<!-- Segment Panel Ends -->
										</div>
										<!--  Twelfth Column Ends  -->
									</div>
								</div>
								<!-- Right Col Ends -->


							</div>
						</div>
						<!-- Main Segment Content ends Here -->

					</div>
				</div>
				
				<!--  Segment Dashboard Wrapper Ends  -->

			
				<script>
					$(document).ready(function () {
						setTimeout(function () {
							$(".top-filters-wrapper").mThumbnailScroller({
								axis: "x"
							});
						}, 500);
					});
				</script>