<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>
<!--  Super Sub Header Wrapper Ends  -->
	<div
		class="activiti-supersubheader-wrapper user-supersubheader-content">
		<div class="container-fluid">
			<div class="activiti-supersubheader-content">
				<ul class="list-inline">
                    <li class="active">
                        <a class="text-uppercase" href="javascript:void(0);">
                            <span class="text-uppercase">inbox</span>
                            <small>12</small>
                        </a>
                    </li>
                    <li>
                        <a class="text-uppercase" href="javascript:void(0);">
                            <span class="text-uppercase">My tasks</span>
                            <small>09</small>
                        </a>
                    </li>
                    <li>
                        <a class="text-uppercase" href="javascript:void(0);">
                            <span class="text-uppercase">queued</span>
                            <small>09</small>
                        </a>
                    </li>
                    <li>
                        <a class="text-uppercase" href="javascript:void(0);">
                            <span class="text-uppercase">involved</span>
                            <small>12</small>
                        </a>
                    </li>
                    <li>
                        <a class="text-uppercase" href="javascript:void(0);">
                            <span class="text-uppercase">archived</span>
                            <small>12</small>
                        </a>
                    </li>
                </ul>
<!-- 				<ul class="list-inline"> -->
<!-- 					<li class="active"><a href="javascript:void(0)" class="text-uppercase" -->
<!-- 						> <span -->
<!-- 							class="text-uppercase">General Tasks</span> -->
<!-- 					</a></li> -->
<!-- 					<li><a  href="javascript:void(0)" class="text-uppercase" -->
<!-- 						> <span -->
<!-- 							class="text-uppercase">Saved Tasks</span> -->
<!-- 					</a></li> -->
		
<!-- 				</ul> -->
			</div>
		</div>
	</div>

	<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">
		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">
			<div class="activiti-task-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3 side-col">

                            <!--  Aside Wrapper Starts  -->
                            <aside class="aside-wrapper text-uppercase">

                                <div class="form-group has-success has-feedback">
                                    <input type="text" class="form-control" placeholder="Search Task" />
                                    <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                </div>

                                <div class="row">
                                    <div class="col-sm-7"><h3>Recent Tasks</h3></div>
                                    <div class="col-sm-5">  
                                        <div class="dropdown clearfix">
                                            <a href="javascript:void(0);" class="pull-right" id="dLabel" data-toggle="dropdown">
                                                By Date<span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel custom-panel panel-info">
                                    <div class="panel-heading">Leave Notifications</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">Database Configure</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">Usability Evaluation</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">Backend Process</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">UX Bootcamp</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">Product Manager Meetup</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel custom-panel">
                                    <div class="panel-heading">Data Visualisation</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">On 12/09/2016</div>
                                            <div class="col-md-6 light-red">Priroty : High</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">Due : 30days</div>
                                            <div class="col-md-6 light-green">People : 08</div>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <!--  Aside Wrapper Ends  -->

                        </div>
                        <div class="col-sm-6 center-col">

                            <!--  Create New Task Wrapper Starts  -->
                            <div class="create-new-task-wrapper">
                                <h2>Create a new task<a class="glyphicon glyphicon-plus-sign" data-toggle="modal" data-target="#addActivity" href="javascript:void(0);"></a></h2>

                                <div class="create-new-task-inner-wrapper">
                                    <!--  Notification Panel Starts  -->
                                    <div class="panel custom-panel notification-panel">
                                        <div class="panel-heading">
                                            <div class="notification-header">
                                                <a href="javascript:void(0);" class="fa fa-server server-link"></a>
                                                <h3>Leave Notification</h3>
                                                <ul class="list-inline">
                                                    <li><i class="fa fa-history"></i>Created on 12/09/2016</li>
                                                    <li><i class="fa fa-calendar"></i>Due Date : 30/12/2016</li>
                                                    <li><i class="fa fa-cubes"></i>Priority : High</li>
                                                </ul>
                                            </div>
                                            <a class="btn btn-light-blue" href="javascript:void(0);">Mark as Done<i class="fa fa-check"></i></a>
                                        </div>
                                        <div class="panel-body">
                                            OrangeHRM Leave Notificatin add-on, notifies the leave to be
                                            taken by colleagues. This is useful to know who will be on leave on
                                            which date. For example the meetings and other priorities can be
                                            arranged on a convenient date.
                                            OrangeHRM Leave Notificatin add-on, notifies the leave to be
                                            taken by colleagues. This is useful to know who will be on leave on
                                            which date. For example the meetings and other priorities can be
                                            arranged on a convenient date.
                                            OrangeHRM Leave Notificatin add-on, notifies the leave to be
                                            taken by colleagues. This is useful to know who will be on leave on
                                            which date. For example the meetings and other priorities can be
                                            arranged on a convenient date.
                                        </div>
                                    </div>
                                    <!--  Notification Panel Ends  -->

                                    <!--  Associated People Panel Starts  -->
                                    <div class="panel custom-panel associated-people-panel">
                                        <div class="panel-heading">Associated People<a href="javascript:void(0);" class="fa fa-plus add-link"></a></div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                <img class="media-object" src="assets/images/activity/victor-leyland.jpg" alt="victor-leyland">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Victor Leyland</h4>
                                                            <p>
                                                                Owner
                                                                <a href="javascript:void(0);" class="btn btn-light-blue">Transfer</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                <img class="media-object" src="assets/images/activity/james.jpeg" alt="james">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">James Sheelan</h4>
                                                            <p>
                                                                Assignee
                                                                <a href="javascript:void(0);" class="btn btn-light-blue">Re-Assign</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                <img class="media-object" src="assets/images/activity/amanda-elliot.jpg" alt="amanda-elliot">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Amanda Elliot</h4>
                                                            <p>
                                                                Contributor
                                                                <a href="javascript:void(0);" class="btn btn-light-blue">Re-Assign</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12 visible-md visible-sm visible-xs">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                <img class="media-object" src="assets/images/activity/natally.png" alt="natally">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Natally Armstrong</h4>
                                                            <p>
                                                                Implementor
                                                                <a href="javascript:void(0);" class="btn btn-light-blue">Re-Assign</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row hidden-md hidden-sm hidden-xs">                                            
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                <img class="media-object" src="assets/images/activity/natally.png" alt="natally">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Natally Armstrong</h4>
                                                            <p>
                                                                Implementor
                                                                <a href="javascript:void(0);" class="btn btn-light-blue">Re-Assign</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  Associated People Panel Ends  -->

                                    <!--  Sub Task Panel Starts  -->
                                    <div class="panel custom-panel sub-task-panel">
                                        <div class="panel-heading">
                                            Sub Task<a href="javascript:void(0);" class="fa fa-plus add-link"></a>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-server"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Developer Meetup<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-server"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Database Configure<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">                                            
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-server"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Usability Evaluation<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  Sub Task Panel Ends  -->

                                    <!--  Related Content Panel Starts  -->
                                    <div class="panel custom-panel related-content-panel">
                                        <div class="panel-heading">
                                            Related Content<a href="javascript:void(0);" class="fa fa-plus add-link"></a>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-globe"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Leave Management<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-file-word-o"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Enterprise Mobility<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">                                            
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);"><i class="fa fa-file-pdf-o"></i></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading">Oracle Leave Systems<a href="javascript:void(0);" class="fa fa-times close-link"></a></h4>
                                                            <p>Created By : James Sheelan</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  Related Content Panel Ends  -->

                            </div>
                            <!--  Create New Task Wrapper Ends  -->

                        </div>
                        <div class="col-sm-3 side-col">

                            <!--  Aside Wrapper Starts  -->
                            <aside class="aside-wrapper text-uppercase aside-wrapper-right">
                                <div class="form-group has-success has-feedback">
                                    <input type="text" class="form-control" placeholder="SAY SOMETHING">
                                    <span class="glyphicon glyphicon-plus form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <h3>Recent Events</h3>  
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="dropdown clearfix">
                                            <a class="pull-right" href="javascript:void(0);" id="dLabel" data-toggle="dropdown">
                                                By Date<span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                                <li><a href="javascript:void(0);">By Date</a></li>
                                            </ul>
                                        </div>                                            
                                    </div>
                                </div>

                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/peter.jpg" alt="peter">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Peter Sheelan</h4>
                                        <p>
                                            Attached leave management system as sub task
                                            <span>12 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>

                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/ontario.jpg" alt="ontario">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Ontario Sheelan</h4>
                                        <p>
                                            Yes, will be doing in an hour
                                            <span>2 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>

                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/alberta.jpg" alt="alberta">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Alberta Elliot</h4>
                                        <p>
                                            Attached oracle leave system as sub task
                                            <span>4 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/quebec.jpg" alt="quebec">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Quebec Leyland Said</h4>
                                        <p>
                                            Please upload lms hosted prototype 
                                            <span>9 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/saskatchewan.jpg" alt="saskatchewan">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Saskatchewan Sheelan Said</h4>
                                        <p>
                                            Yes, will be doing in an hour
                                            <span>5 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="assets/images/activity/nova.jpg" alt="nova">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Nova Scotia</h4>
                                        <p>
                                            ITE been done. Will be sharing the link
                                            <span>6 Hours Ago</span>
                                        </p>
                                    </div>
                                </div>
                            </aside>
                            <!--  Aside Wrapper Ends  -->

                        </div>
                    </div>
                </div>
            </div>
<!-- 			<div class="activiti-task-wrapper reports-wrapper user-wrapper crystallball-wrapper"> -->
<!-- 				<div class="container-fluid"> -->

<!-- 					<h3>Under Construction</h3> -->

<!-- 				</div> -->
<!-- 			</div> -->
		</div>
		<!--  Content Wrapper Ends  -->
	</div>
	<!--  Main Wrapper Ends  -->
</div>

 <!-- Add Activity Modal Wrapper Starts  -->
        <div id="addActivity" class="modal fade adduser-modal-wrapper" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <form id="addNewActivity">
                    <div class="modal-content">
                        <div class="modal-header clearfix">
                            <div class="add-user-submit">
                                <button type="button" class="cancel" data-dismiss="modal" aria-label="Close">
                                    <span class="text-uppercase">cancel</span>
                                </button>
                                <button type="submit" class="custom-submit" >
                                    <span class="text-uppercase">save</span>
                                </button>
                            </div>
                            <div class="modal-title">
                                <span><i class="fa fa-plus"></i></span>
                                <p class="text-uppercase">New Task</p>
                            </div>
                        </div>
                        <div class="modal-body">                       
                            <div class="clearfix form-stages">
                                <div class="col-sm-10">
                                    <div class="form-group clearfix">
                                        <label class="control-label col-md-3 col-sm-4 text-uppercase" for="name">Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="control-label col-md-3 col-sm-4 text-uppercase" for="description">Description</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" id="description" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="control-label col-md-3 col-sm-4 text-uppercase" for="dueDate">Due Date</label>
                                        <div class="col-sm-6">
                                            <div class="input-group date" data-provide="datepicker">
                                                   <div class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </div>
                                                <input type="text" class="form-control">
                                                <div class="input-group-addon">
                                                    <span class="fa fa-angle-down"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <label class="control-label col-md-3 col-sm-4 text-uppercase" for="profession">Priority</label>
                                        <div class="col-sm-6">
                                            <select class="form-control text-uppercase" id="sourceType">
                                                <option class="text-uppercase">High</option>
                                                <option class="text-uppercase">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Add Activity Modal Wrapper Ends  -->