<div ng-controller="SubMenuController">
<div class="activiti-supersubheader-wrapper user-supersubheader-content" ng-if="currentState == 'activeProcess' || currentState == 'administrations' || currentState == 'crystalBall' || 
																				currentState == 'deployments' || currentState == 'jobs' || currentState == 'groups' || currentState == 'users' ||
																				currentState == 'manageDb' || currentState == 'suspendedProcess'">
	<div class="container-fluid">
		<div class="activiti-supersubheader-content">
			<ul class="list-inline">
				<li class="active"><a class="text-uppercase" ui-sref="manageDb"> <span
						class="text-uppercase">Database</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="deployments"> <span
						class="text-uppercase">Deployments</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="activeProcess"> <span
						class="text-uppercase">Active Process</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="suspendedProcess"> <span
						class="text-uppercase">Suspended Process</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="jobs"> <span
						class="text-uppercase">Jobs</span>
				</a></li>
				<li><a class="text-uppercase"
					ui-sref="users"> <span class="text-uppercase">Users</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="groups"> <span
						class="text-uppercase">Groups</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="administrations"> <span
						class="text-uppercase">Administrations</span>
				</a></li>
				<li><a class="text-uppercase" ui-sref="crystalBall"> <span
						class="text-uppercase">Crystalball</span>
				</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="activiti-supersubheader-wrapper" ng-if="currentState == 'processInstance' || currentState == 'deploymentProcess' || currentState == 'modelWorkspace'">
	<div class="container-fluid">
		<div class="activiti-supersubheader-content">
			<ul class="list-inline">
				<li ng-class="{active : currentState == 'processInstance'}"><a class="text-uppercase" ui-sref="processInstance">
						<span class="text-uppercase">My Instances</span> <small>09</small>
				</a></li>
				<li ng-class="{active : currentState == 'deploymentProcess'}"><a class="text-uppercase" ui-sref="deploymentProcess">
						<span class="text-uppercase">Deployed Process Definitions</span>
						<small>12</small>
				</a></li>
				<li ng-class="{active : currentState == 'modelWorkspace'}"><a class="text-uppercase" ui-sref="modelWorkspace"> 
							<span class="text-uppercase">Model Workspace</span> <small>12</small>
				</a></li>
			</ul>
		</div>
	</div>
</div>

</div>