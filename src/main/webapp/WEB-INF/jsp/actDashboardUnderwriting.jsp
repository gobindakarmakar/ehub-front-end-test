<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="common/submenu.jsp"%>
        <!--  SubMenu Ends  -->
        <style>
            .peerGroupTextColor {

                color: #657f8b !important;

            }

            .act-pagination {
                top: 90.5% !important;
                right: 70px !important;
            }
            .customDisabled {
                cursor: not-allowed !important;
                opacity: .5;
            }

            .pad-70 {
                padding-top: 70px !important;
            }
        </style>
        <!--  Main Contents Wrapper Starts  -->
        <div id="act-dashboard" class="main-contents-wrapper">
            <span ng-show="caseDetailsPreloader" class="act-custom-spinner">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span>
            <onboarding visible="showModalOnboarding"></onboarding>
            <upload visible="showModalUpload"></upload>
            <reassign visible="showModalReassign"></reassign>
            <uploadquestionnaire visible="showModalUploadQuestionnaire"></uploadquestionnaire>
            <uploadkyc visible="showModalUploadKYC"></uploadkyc>
            <modal-dialog1 show="dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownDialogShown" width="500px" height="70px">
                <!--         <div class="alert alert-success custom-success-modal"> -->
                <!--             <strong>{{dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownDialogMessage}}</strong> -->
                <!--         </div> -->
            </modal-dialog1>

            <!--  Act Dashboard Wrapper Ends  -->
            <div id="act-dashboard" class="dashboard-wrapper act-dashboard-wrapper">
                <div ng-controller='ActGridsterController'>
                    <div gridster='gridsterConfiguration'>
                        <div id="relatedCaseDivAp" class="widget-content" style="display:none">
                            <li ng-controller="RelatedCasesController" id="actRelatedCasesDivCopy">
                                <span ng-show="relatedCasesPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-briefcase icon-widget"></i>
                                    <span>RELATED CASES</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                    </ul>
                                </div>
                                <div class="widget-content">
                                    <div id="relatedCases-containerCopy" ng-controller="RelatedCasesController">


                                        <table id="relatedCasesCopy" class="relatedCases table table-striped dataTable no-footer table-scroll five-col">
                                            <thead class="relatedCases-header">
                                                <tr>
                                                    <th>CASE NAME</th>
                                                    <th>PROXIMITY</th>
                                                    <th>DR</th>
                                                    <th>IR</th>
                                                    <th>TR</th>
                                                </tr>
                                            </thead>
                                            <tbody class="case-diary-list" id="relatedCasesWidgetCopy">
                                                <tr ng-repeat="caseItem in relatedCasesDataCopy" ng-class-odd="'odd'" ng-class-even="'even'" ng-if="relatedCasesDataCopy.length > 0">
                                                    <td>
                                                        <div class="media-object custom-media-object">
                                                            <div class="media-object-section">
                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Corporate-KYC'">
                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imageBank}}" alt="avatar" />
                                                                </a>
                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Individual-KYC'">
                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imagePerson}}" alt="avatar" />
                                                                </a>
                                                                <p ng-trim="true">{{caseItem.caseName}}</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseseed">
                                                        <span>{{caseItem.proximity}}</span>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.directRisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{caseItem.directRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.directRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.directRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.indirectRisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{caseItem.indirectRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.indirectRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.indirectRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.transactionalRisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{caseItem.transactionalRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.transactionalRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.transactionalRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr ng-show="relatedCasesDataCopy.length == 0">
                                                    <td colspan="5">
                                                        <p class="no-data">No data found</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                        </div>
                        <ul>

                            <!--  Case Timeline Widget Starts  -->
                            <li gridster-item row="0" col="0" size-x="4" size-y="2" ng-controller="CaseTimelineController" id="actCaseTimelineDiv">
                                <span ng-show="caseTimelinePreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>

                                <div class='widget-header' ng-hide="toggleCase" ng-hide="showCaseTimeLine">
                                    <i class="fa fa-line-chart icon-widget"></i>
                                    <span>CASE TIMELINE</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="filtercaseTimeline()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-show="toggleCase">
                                            <a href="javascript:void(0);" ng-click="toggleCase = false">
                                                <i class="fa fa-line-chart"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0);' ng-click="actCaptureDiv('actCaseTimelineDiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0);' ng-click="maximizeDiv('actCaseTimelineDiv')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content" ng-hide="toggleCase" id="caseTimelineWidget">
                                    <div id="caseTimelineChart"></div>
                                </div>
                                <div class='widget-header' ng-show="toggleCase">
                                    <i class="fa fa-tachometer"></i>
                                    <span>YOUR CASE LOG</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);'>
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="toggleCase">
                                            <a href="javascript:void(0);" ng-click="getCaseLog()">
                                                <i class="fa fa-tachometer"></i>
                                            </a>
                                        </li>
                                        <li ng-show="toggleCase">
                                            <a href="javascript:void(0);" ng-click="toggleCase = false">
                                                <i class="fa fa-line-chart"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:actCaptureDiv("actCaseTimelineDiv")'>
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>

                            </li>
                            </ul>
                            </div>
                            <div class="widget-content" ng-show="toggleCase">
                                <div class="row">
                                    <div class="col-sm-12 case-log-container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-status">STATUS: COMPLETE</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-total-logs">TOTAL LOGS: {{logs.length}}</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-comments">COMMENTS: 12</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-started">STARTED: 12/03/2016</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-health">HEALTH: 1</span>
                                                        <span class="case-log-priority">PRIORITY: 2</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-last-updated">LAST UPDATED: 16/04/2016</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-by-name">BY AMILY ALBERT</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#latestActivity">LATEST ACTIVITY(07)</a>
                                            </li>
                                            <li>
                                                <a href="#comments">COMMENTS(09)</a>
                                            </li>
                                            <li>
                                                <a href="#uploads">UPLOADS(08)</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="latestActivity" class="tab-pane fade in active">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem" ng-repeat="log in logs">{{log.message}}</li>
                                                </ul>
                                            </div>
                                            <div id="comments" class="tab-pane fade">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                </ul>
                                            </div>
                                            <div id="uploads" class="tab-pane fade">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </li>
                            <!--  Case Timeline Widget Ends  -->

                            <!--  Alerts Widget Starts  -->
                            <li gridster-item row="0" col="4" size-x="2" size-y="1" id="actPeerAlertsDiv">
                                <span ng-show="alertsPreloader1" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-briefcase icon-widget"></i>
                                    <span>PEER GROUP</span>
                                    <!--                                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> -->
                                    <!--                                         <i class="fa fa-ellipsis-v ellipsis"></i> -->
                                    <!--                                     </a> -->
                                    <!--                                     <ul class="dropdown-menu filter-menu"> -->
                                    <!--                                         <li> -->
                                    <!--                                             <a href='javascript:void(0);' ng-click="filterAlertsData()"> -->
                                    <!--                                                 <i class="fa fa-filter"></i> -->
                                    <!--                                             </a> -->
                                    <!--                                         </li> -->
                                    <!--                                         <li> -->
                                    <!--                                             <a href='javascript:void(0);' ng-click="actCaptureDiv('actAlertsDiv')"> -->
                                    <!--                                                 <i title="Capture Screen" class="capture-current-portlet icon-camera"></i> -->
                                    <!--                                             </a> -->
                                    <!--                                         </li> -->
                                    <!--                                     </ul> -->
                                </div>
                                <div class="widget-content" id="alertsWidget1">

                                    <div id="notifications-wrapper1" class="text-center">

                                        <div class="row custom-row">
                                            <div class="col-sm-5  pad-r0 pad-l10">
                                                <div class="panel pad-t25 border-0 width-100 d-ib  mar-b15 bg-transparent custom-panel-wrapper">
                                                    <div class="panel-body right-panel-body pad-0 text-left">
                                                        <div class="view-type-wrapper  pad-l15 pad-y10 mnh-100px">

                                                            <ul class="custom-list pad-l0 item-1">
                                                                <li class="pad-r15  mar-y5">
                                                                    <span class="ws-normal peerGroupHeadColor">Find similarity based on</span>
                                                                </li>
                                                                <li class="pad-r15  mar-y5">
                                                                    <span class="answer-content peerGroupTextColor">Industry</span>
                                                                    <label class="switch pull-right custom-switch-wrapper search-switch threat-switch-wrapper">
                                                                        <input type="checkbox" ng-model="checkOptions.peerIndustry" ng-change="getPeerOptions()" class="custom-switch-checkbox ng-pristine ng-untouched ng-valid ng-not-empty"
                                                                            ng-disabled="checkRadarParams['industry'] == false">
                                                                        <span class="slider round" ng-class="{'customDisabled': checkRadarParams['industry'] == false}">

                                                                        </span>
                                                                    </label>
                                                                </li>
                                                                <li class="pad-r15 mar-y5">
                                                                    <span class="answer-content peerGroupTextColor">Technology</span>
                                                                    <label class="switch pull-right custom-switch-wrapper search-switch threat-switch-wrapper">
                                                                        <input type="checkbox" ng-model="checkOptions.peerTechnology" ng-change="getPeerOptions()" class="custom-switch-checkbox ng-pristine ng-untouched ng-valid ng-not-empty"
                                                                            ng-disabled="checkRadarParams['technology'] == false">
                                                                        <span class="slider round" ng-class="{'customDisabled': checkRadarParams['technology'] == false}">

                                                                        </span>
                                                                    </label>
                                                                </li>
                                                                <li class="pad-r15 mar-y5">
                                                                    <span class="answer-content peerGroupTextColor">Location</span>
                                                                    <label class="switch pull-right custom-switch-wrapper search-switch threat-switch-wrapper">
                                                                        <input type="checkbox" ng-model="checkOptions.peerLocation" ng-change="getPeerOptions()" class="custom-switch-checkbox ng-pristine ng-untouched ng-valid ng-not-empty"
                                                                            ng-disabled="checkRadarParams['country-code'] == false">
                                                                        <span class="slider round" ng-class="{'customDisabled': checkRadarParams['country-code'] == false}">

                                                                        </span>
                                                                    </label>
                                                                </li>
                                                                <li class="pad-r15 mar-y5">
                                                                    <span class="answer-content peerGroupTextColor">Legal type</span>
                                                                    <label class="switch pull-right custom-switch-wrapper search-switch threat-switch-wrapper">
                                                                         <input type="checkbox" ng-model="checkOptions.peerLegalType" ng-change="getPeerOptions()" class="custom-switch-checkbox ng-pristine ng-untouched ng-valid ng-not-empty" ng-disabled="checkRadarParams['legal-type'] == false">
                                                                          <span class="slider round" ng-class="{'customDisabled': checkRadarParams['legal-type'] == false}">

                                                                          </span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7  border-dark-grey-thin-r mnh-250 pad-0">
                                                <div ng-show="actualPeergroupData.length == 0 && !alertsPreloader1">
                                                    <p class="text-center no-data-wrapper d-ib">NO DATA FOUND</p>
                                                </div>
                                                <div class="panel-heading width-100 text-center bg-transparent pad-5">
                                                    <h3 class="top-details-heading f-12">
                                                        <span class="pad-l10" style="color:#BA4441">
                                                            <b>GLOBAL</b>
                                                        </span>
                                                        <span class="pad-l10" style="color:#BB9800">
                                                            <b>SIMILAR COMPANIES</b>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <div class="panel-body pad-0">
                                                    <div id="global_company_radar_chart"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- <div class="panel mar-0 border-0  bg-transparent custom-panel-wrapper">
                                      
                                    <div class="panel-heading bg-transparent panel-heading-right pad-b0">
                                        <h3 class="top-details-heading multiple select-group pad-x0 pad-y10 custom-select-dropdown">Peer Group
                                        </h3>
                                    </div>
                                    <div class="panel-body pad-5 right-panel-body ">
                                        <div class="view-type-wrapper  pad-l15 pad-y10 mnh-100px">

                                  
                                        </div>
                                    </div>
                                </div> -->
                            </li>
                            <!--  Alerts Widget Ends  -->

                            <!--  Associated Entities Widget Starts  -->
                            <li gridster-item id="relatedCompaniesDiv">
                                <span ng-show="relatedCompaniesPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-briefcase icon-widget"></i>
                                    <span>RELATED COMPANIES</span>
                                </div>

                                <div class="custom-data-table-wrapper overflow-h mxh-250 p-rel mnh-250">
                                    <table class="table table-scroll custom-data-table four-col-act ">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Risk Score</th>
                                                <th class="pad-r0">Similarity By</th>
                                                <th>Avg Score</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="comp in relatedCompaniesData">
                                                <td ng-click="openEntityPage(comp)" class="c-pointer">
                                                    <span class="pull-left company-icon-wrapper text-center f-18">
                                                        <i class="fa fa-building-o lh-27"></i>
                                                    </span>
                                                    <span class="company-name-wrapper">{{ comp.name }}</span>
                                                </td>
                                                <td class="casecumulativerisk c-pointer">
                                                    <div class="c100 p{{comp.riskScore | number:0}} pink radial f-40" ng-click="openriskModal(comp)">
                                                        <span>{{comp.riskScore | number:0}}</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="info-badge-class" ng-if="checkOptions.peerIndustry">Industry</span>
                                                    <span class="warning-badge-class" ng-if="checkOptions.peerLocation">Location</span>
                                                    <span class="primary-badge-class" ng-if="checkOptions.peerTechnology">Technology</span>
                                                    <span class="bg-darkgreen2 primary-badge-class" ng-if="checkOptions.peerLegalType">Legal type</span>
                                                </td>
                                                <td class="text-center">{{avgScore}}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div ng-show="relatedCompaniesData.length == 0 && !relatedCompaniesPreloader">
                                        <p class="text-center no-data-wrapper pad-70">NO DATA FOUND</p>
                                    </div>

                                </div>
                                <div class="text-right" ng-hide="relatedCompaniesData.length == 0 || similarPeerLength <= recordsPerPage">
                                    <ul class="case-diary-pagination act-pagination" uib-pagination total-items="similarPeerLength" items-per-page="recordsPerPage"
                                        ng-model="pageNum" ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm"
                                        boundary-link-numbers="true"></ul>
                                </div>
                            </li>
                            <!--  Associated Entities Widget Ends  -->


                            <!--  Related Cases Widget Starts  -->
                            <!--                             <li style="display:none" gridster-item row="3" col="2" size-x="2" size-y="1" ng-controller="RelatedCasesController" id="actRelatedCasesDiv"> -->
                            <!--                                 <div id="casesDiv"> -->
                            <!--                                     <span ng-show="relatedCasesPreloader" class="custom-spinner case-dairy-spinner"> -->
                            <!--                                         <i class="fa fa-spinner fa-spin fa-2x"></i> -->
                            <!--                                     </span> -->
                            <!--                                     <div class='widget-header'> -->
                            <!--                                         <i class="fa fa-briefcase icon-widget"></i> -->
                            <!--                                         <span>RELATED CASES</span> -->
                            <!--                                         <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> -->
                            <!--                                             <i class="fa fa-ellipsis-v ellipsis"></i> -->
                            <!--                                         </a> -->
                            <!--                                         <ul class="dropdown-menu filter-menu"> -->
                            <!--                                             <li> -->
                            <!--                                                 <a href='javascript:void(0);' ng-click="openFilterModalForChart()"> -->
                            <!--                                                     <i class="fa fa-filter"></i> -->
                            <!--                                                 </a> -->
                            <!--                                             </li> -->
                            <!--                                             <li> -->
                            <!--                                                 <a href='javascript:void(0);' ng-click="actCaptureDiv('actRelatedCasesDiv')"> -->
                            <!--                                                     <i title="Capture Screen" class="capture-current-portlet icon-camera"></i> -->
                            <!--                                                 </a> -->
                            <!--                                             </li> -->
                            <!--                                         </ul> -->
                            <!--                                     </div> -->
                            <!--                                     <div class="widget-content"> -->
                            <!--                                         <div id="relatedCases-container"> -->
                            <!--                                             <table id="relatedCases" class="relatedCases table table-striped dataTable no-footer table-scroll five-col"> -->
                            <!--                                                 <thead class="relatedCases-header"> -->
                            <!--                                                     <tr> -->
                            <!--                                                         <th>CASE NAME</th> -->
                            <!--                                                         <th>PROXIMITY</th> -->
                            <!--                                                         <th>DR</th> -->
                            <!--                                                         <th>IR</th> -->
                            <!--                                                         <th>TR</th> -->
                            <!--                                                     </tr> -->
                            <!--                                                 </thead> -->
                            <!--                                                 <tbody class="case-diary-list" id="relatedCasesWidget"> -->
                            <!--                                                     <tr ng-repeat="caseItem in relatedCasesData" ng-class-odd="'odd'" ng-class-even="'even'" ng-if="relatedCasesData.length > 0"> -->
                            <!--                                                         <td> -->
                            <!--                                                             <div class="media-object custom-media-object"> -->
                            <!--                                                                 <div class="media-object-section"> -->
                            <!--                                                                     <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Corporate-KYC'"> -->
                            <!-- 	                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imageBank}}" alt="avatar" /> -->
                            <!-- 	                                                                </a> -->
                            <!-- 	                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Individual-KYC'"> -->
                            <!-- 	                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imagePerson}}" alt="avatar" /> -->
                            <!-- 	                                                                </a> -->
                            <!--                                                                     <p ng-trim="true">{{caseItem.caseName}}</p> -->
                            <!--                                                                 </div> -->
                            <!--                                                             </div> -->
                            <!--                                                         </td> -->
                            <!--                                                         <td class="caseseed"> -->
                            <!--                                                             <span>{{caseItem.proximity}}</span> -->
                            <!--                                                         </td> -->
                            <!--                                                         <td class="caseriskprofile"> -->
                            <!--                                                             <div class="progress"> -->
                            <!--                                                                 <div title="{{caseItem.directRisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{caseItem.directRisk * 100}}%" -->
                            <!--                                                                     aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.directRisk * 100}}%"> -->
                            <!--                                                                     <span class="sr-only">{{caseItem.directRisk * 100}}% Complete</span> -->
                            <!--                                                                 </div> -->
                            <!--                                                             </div> -->
                            <!--                                                         </td> -->
                            <!--                                                         <td class="caseriskprofile"> -->
                            <!--                                                             <div class="progress"> -->
                            <!--                                                                 <div title="{{caseItem.indirectRisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{caseItem.indirectRisk * 100}}%" -->
                            <!--                                                                     aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.indirectRisk * 100}}%"> -->
                            <!--                                                                     <span class="sr-only">{{caseItem.indirectRisk * 100}}% Complete</span> -->
                            <!--                                                                 </div> -->
                            <!--                                                             </div> -->
                            <!--                                                         </td> -->
                            <!--                                                         <td class="caseriskprofile"> -->
                            <!--                                                             <div class="progress"> -->
                            <!--                                                                 <div title="{{caseItem.transactionalRisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{caseItem.transactionalRisk * 100}}%" -->
                            <!--                                                                     aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.transactionalRisk * 100}}%"> -->
                            <!--                                                                     <span class="sr-only">{{caseItem.transactionalRisk * 100}}% Complete</span> -->
                            <!--                                                                 </div> -->
                            <!--                                                             </div> -->
                            <!--                                                         </td> -->
                            <!--                                                     </tr> -->
                            <!--                                                     <tr ng-show="relatedCasesData.length == 0 && !relatedCasesPreloader"> -->
                            <!--                                                         <td colspan="5"> -->
                            <!--                                                             <p class="no-data">No data found</p> -->
                            <!--                                                         </td> -->
                            <!--                                                     </tr> -->
                            <!--                                                 </tbody> -->
                            <!--                                             </table> -->
                            <!--                                         </div> -->
                            <!--                                     </div> -->
                            <!--                                 </div> -->
                            <!--                             </li> -->
                            <!--  Related Cases Widget Ends  -->

                        </ul>
                    </div>
                </div>
            </div>
            <!--  Act Dashboard Wrapper Ends  -->
            <wheel-nav-context-menu contextmenuname="actDashboard.contentMenu" contextmenuname0="actDashboard.contentMenu0" contextmenuname1="actDashboard.contentMenu1"
                contextmenuname2="actDashboard.contentMenu2" contextmenuname3="actDashboard.contentMenu3" contextmenuname4="actDashboard.contentMenu4"
                contextmenuname5="actDashboard.contentMenu5"></wheel-nav-context-menu>
        </div>
        <!--  Main Contents Wrapper Ends  -->

        <!--  Details Modal Starts  -->
        <div id="detailsModal" class="modal fade">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Details</h4>
                    </div>
                    <div class="modal-body">
                        <p id="detailsText"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--  Details Modal Ends  -->

        <!--  Capture Modal Starts  -->
        <div class="modal fade custom-capture-modal" id="actWidgetCapture">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Capture Widget</h4>
                    </div>
                    <div class="modal-body screencapture-modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="actWidgetCapturePreview" style="text-align: center"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="captured-name pull-left" id="actCaptureFilename" type="text" placeholder="File Name">
                        <a class="pull-right" href="javascript:void(0);" id="actDownloadWidgetCapture"> Download </a>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #map {
                position: static !important
            }
        </style>
        <!--  Capture Modal Ends  -->