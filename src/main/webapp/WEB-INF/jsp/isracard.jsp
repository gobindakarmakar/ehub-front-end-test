<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

		<!--  Super Sub-Menu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super Sub-Menu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->


				<!--  Segment Dashboard Wrapper Starts  -->

				<div class="dashboard-wrapper isra-card-wrapper user-dashboard-wrapper bg-dark-grey1">

					<!-- Main Segment Content Starts Here -->

					<div class="row main-segment-wrapper">


						<!-- Right Col Starts -->
						<div class="col-sm-3 custom-col left-col">
							<div class="row custom-row usage-panel-wrapper">
								<div class="col-sm-12 custom-col segment-right-panel">

									<!-- Segment Panel Starts -->
									<div class="panel custom-panel-wrapper segment-panel-wrapper">
										<div class="panel-heading">
											<p class="panel-text">Age : 37years</p>
											<p class="panel-text">Profession : Buisness</p>
											<p class="panel-text">Location : Sunnyvale | USA</p>
										</div>

										<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
											<div class="panel-data-wrapper">
												<strong>$1.2BN</strong>
												<p class="panel-text">Total Spendings</p>
												<strong>$348,990,12</strong>
												<p class="panel-text">Average Spendings</p>
												<div class="panel-bottom-wrapper">
													<p class="panel-text">Holding Cards</p>
													<p class="panel-text">HASBC Paltinum - Debitcard</p>
													<p class="panel-text">ISRA GOLD - CreditCard</p>
												</div>
											</div>
										</div>
									</div>
									<!-- Segment Panel Ends -->

									<!-- Segment Panel Starts -->
									<div class="panel custom-panel-wrapper  segment-panel-wrapper">
										<div class="panel-heading">
											<h3>Spending genre</h3>
										</div>
										<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
											<!--  Progressbar List Starts -->
											<ul class="list-unstyled progressbar-list">
												<li class="progressbar-list-item">
													<div class="left-col">Mobiles Phones</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:90%">
															<span class="sr-only ">1 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">PC And Tablets</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:80%">
															<span class="sr-only">1651 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Cloths</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:70%">
															<span class="sr-only">710 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Footwears</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:60%">
															<span class="sr-only">5 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Cosemetics</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:50%">
															<span class="sr-only">2921 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Pc Games</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:40%">
															<span class="sr-only">168 Complete</span>
														</div>
													</div>
												</li>
												<li class="progressbar-list-item">
													<div class="left-col">Ice-Creams</div>
													<div class="progress">
														<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
														 style="width:30%">
															<span class="sr-only">32 Complete</span>
														</div>
													</div>
												</li>
											</ul>
											<!--  Progressbar List Ends -->
										</div>

										<!-- Segment Panel Ends -->

										<!-- Segment Panel Starts -->
										<div class="panel custom-panel-wrapper segment-panel-wrapper">
											<div class="panel-body">
												<div class="col-sm-6 left-col">
													<h3>Primary Card</h3>
													<div class="custom-content">MASTERCard </div>
												</div>


												<div class="col-sm-6 right-col">
													<h3>Secondary Card</h3>
													<div class="custom-content">Cit</div>
												</div>
											</div>
										</div>
										<!-- Segment Panel Ends -->

										<!-- Segment Panel Starts -->
										<div class="panel custom-panel-wrapper segment-panel-wrapper">
											<div class="panel-heading">
												<h3>Usage Pattern</h3>
											</div>
											<div class="panel-body border-b0">
												<div id=""></div>
											</div>
										</div>
										<!-- Segment Panel Ends -->
									</div>
									<!--  Twelfth Column Ends  -->
								</div>
							</div>
							<!-- Right Col Ends -->


						</div>
						<div class="col-sm-9 pad-r0 custom-col right-col">
							<div id="customerUsageChart"></div>
						</div>
					</div>
					<!-- Main Segment Content ends Here -->


				</div>

				<!--  Segment Dashboard Wrapper Ends  -->


				<script>
					$(document).ready(function () {
						setTimeout(function () {
							$(".top-filters-wrapper").mThumbnailScroller({
								axis: "x"
							});
						}, 500);
					});
				</script>