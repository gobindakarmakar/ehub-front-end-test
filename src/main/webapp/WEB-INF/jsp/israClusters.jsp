<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
		<!--  Super SubMenu Wrapper Starts  -->
		<%@include file="common/submenu.jsp"%>
			<!--  Super SubMenu Wrapper Ends  -->

			<!--  Super Sub Header Wrapper Starts  -->
			<%@include file="supersubmenu.jsp"%>
				<!--  Super Sub Header Wrapper Ends  -->




				<!--  Cluster Dashboard Wrapper Starts  -->
				<div class="dashboard-wrapper isra-card-wrapper cluster-dashboard-wrapper bg-dark-grey1">
					<!-- Top Grid Wrapper-->
					<div class="top-grid-wrapper container-fluid">
						<ul class="list-inline custom-tabs row top-filters-list">

							<li class="col-sm-7">
								<div class="col-sm-4">
									<strong class="value">18 Clusters </strong>
									<h4 class="top-grid-heading">generated</h4>
								</div>

								<div class="col-sm-4">
									<strong class="value">127,889,901,227 </strong>
									<h4 class="top-grid-heading">total customers</h4>
								</div>
								<div class="col-sm-4">
									<strong class="value">$34BN</strong>
									<h4 class="top-grid-heading">Total Spendings</h4>
								</div>
							</li>
							<li class="col-sm-5">
								<div class="col-sm-6">
									<div class="usage-feature-box">
										<h3 class="usage-feature-box-heading">
											Average Expenditure
										</h3>
										<div id=""></div>
									</div>


								</div>
								<div class="col-sm-6 ">

									<div class="usage-feature-box">
										<h3 class="usage-feature-box-heading">
											Cluster Range
										</h3>
										<div id=""></div>
									</div>

								</div>
							</li>
						</ul>
					</div>
					<!--Top Grid Wrapper Ends-->
					<div id=""></div>
				</div>