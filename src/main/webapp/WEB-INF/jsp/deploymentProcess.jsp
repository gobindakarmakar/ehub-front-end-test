<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
 <!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="activiti-task-wrapper reports-wrapper vacation-request model-workspace deployed-process-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">

                                <!--  Aside Wrapper Starts  -->
                                <aside class="aside-wrapper text-uppercase">

                                    <div class="form-group has-success has-feedback">
                                        <input type="text" class="form-control" placeholder="Search Process"/>
                                        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                    </div>

                                    <div class="sidebar-header">
                                        <h3>Recent Process</h3>
                                        <div class="bydate-sorting">  
                                            <div class="dropdown clearfix">
                                                <a href="javascript:void(0);" class="pull-right" id="dLabel" data-toggle="dropdown">
                                                    By Date<span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                    <li><a href="javascript:void(0);">By Date</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel custom-panel panel-info">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Employee Productivity</h3>
                                            <p>Deployed : 1 Week Ago</p>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Fix System Failure</h3>
                                            <p>Deployed : 1 Week Ago</p>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Help Desk Process</h3>
                                            <p>Deployed : 1 Week Ago</p>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Process Instance Overview</h3>
                                            <p>Deployed : 1 Week Ago</p>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Review Sales Lead</h3>
                                            <p>Deployed : 1 Week Ago</p>
                                        </a>
                                    </div>
                                    <div class="panel custom-panel">
                                        <a class="panel-body" href="javascript:void(0);">
                                            <h3>Vaccation Request</h3>
                                            <p>Deployed  : 1 Week Ago</p>
                                        </a>
                                    </div>
                                </aside>
                                <!--  Aside Wrapper Ends  -->

                            </div>
                            <div class="col-md-9 col-sm-8">
                                <!--  Create New Task Wrapper Starts  -->
                                <div class="create-new-task-wrapper">
                                    <div class="create-new-task-inner-wrapper">
                                        <!--  Notification Panel Starts  -->
                                        <div class="panel custom-panel notification-panel">
                                            <div class="panel-heading">
                                                <div class="notification-header">
                                                    <a href="javascript:void(0);" class="fa fa-cogs server-link"></a>
                                                    <h3>employee productivity</h3>
                                                    <ul class="list-inline">
                                                        <li><i class="fa fa-check-circle"></i>version : 1</li>
                                                        <li><i class="fa fa-calendar-o"></i>Created on : 12/09/2016</li>
                                                    </ul>
                                                </div>
                                                <a class="btn btn-light-blue" href="javascript:void(0);" ng-click="openModal()">
                                                    Start Process
                                                </a>
                                            </div>
                                        </div>
                                        <!--  Notification Panel Ends  -->


                                        <!--  Sub Task Panel Starts  -->
                                        <div class="panel custom-panel sub-task-panel">
                                            <div class="vacation-request-content-body">
                                                <h4 class="text-uppercase">Process Diagram</h4>
                                            </div>
                                        </div>
                                        <!--  Sub Task Panel Ends  -->

                                    </div>
                                    <!--  Related Content Panel Ends  -->

                                </div>
                                <!--  Create New Task Wrapper Ends  -->

                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->