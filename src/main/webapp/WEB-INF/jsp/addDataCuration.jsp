<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!--  SubMenu Starts  -->
	<%@include file="common/submenu.jsp"%>
		<!--  SubMenu Ends  -->
		<!--  Loader Starts  -->
		<div ng-show="isLoading" class="custom-spinner full-page-spinner">
			<i class="fa fa-spinner fa-spin fa-3x"></i>
		</div>
		<!--  Loader Ends  -->

		<!--  Enrich Main Wrapper Starts  -->
		<div id="enrich-dashboard" class="entity-staging-wrapper editStaging-entity-modal">
			<div class="container-fluid">

				<!--  Entity Company Starts  -->
				<div class="editStaging-wrapper">
					<form class="data-curation-form" name="stagingForm" ng-submit="dataCuration.saveCurationDetails(allFormsModal)"  novalidate>
						<uib-tabset class="details-tab" active="dataCuration.tabActive">

							<!--  General Form Tab Starts  -->
							<uib-tab index="0" title="Company Information" ng-click="enableSave();">
								<uib-tab-heading>
									<span>Company Information</span>
								</uib-tab-heading>
								<div class="staging-content-tab tab-one">

									<!--  First Row Starts  -->
									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header">General Details</div>
												<div class="widget-content">
													<!--  First Row Starts  -->
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group companyOne custom-select-dropdown">
																<label class="control-label">Company Name <span style="color:red">*</span></label>
																<a class="resetRefresh" data-title="Discard current details and Create new" ng-click="dataCuration.ResetOrgFields()"><i class="fa fa-refresh"></i></a>
																<input type="text" ng-focus="!dsbl" ng-model="allFormsModal.organizationModal['vcard:organization-name']" placeholder="" uib-typeahead="company as company['vcard:organization-name'] for company in dataCuration.getCompanySuggestions($viewValue)" typeahead-template-url="customTemplate.html" typeahead-loading="loadingCompaniesNames" typeahead-no-results="noOrgResults" class="form-control">
																    {{company}}
																    <div class="text-center custom-dropdown-menu" ng-show="loadingCompaniesNames"><i class="fa fa-spinner fa-pulse"></i></div>
																    <div ng-show="noOrgResults" class="custom-dropdown-menu">
																      <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
																    </div>
<!-- 																<label class="control-label">Company Name</label> -->
<!-- 																<input type="text" ng-model="allFormsModal.organizationModal['vcard:organization-name']" placeholder="" uib-typeahead="company as company['vcard:organization-name'] for company in dataCuration.getCompanySuggestions($viewValue)" -->
<!-- 																    typeahead-loading="loadingCompaniesNames" typeahead-no-results="noResults" class="form-control"> {{company}} -->
<!-- 																<i ng-show="loadingCompaniesNames" class="glyphicon glyphicon-refresh"></i> -->
<!-- 																<div ng-show="noResults"> -->
<!-- 																	<i class="glyphicon glyphicon-remove"></i> No Results Found -->
<!-- 																</div> -->
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group IDOne">
																<label class="control-label">ID <span style="color:red">*</span></label>
																<input type="text" ng-focus="dsbl" class="form-control" ng-model="allFormsModal.organizationModal['bst:registrationId']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group VATOne">
																<label class="control-label">VAT</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:VAT']"></input>
															</div>
														</div>
													</div>
													<!--  First Row Ends  -->

													<!--  Second Row Starts  -->
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group fbLinkOne companyUrlTitle">
																<label class="control-label">Company URL <span style="color:red">*</span> <a class="company-url-title" data-title="can add multiple url using comma operator(,)."><i class="fa fa-info-circle"></i></a></label>
																<input type="text" class="form-control" name="Urlname" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-z]{2,5}[\.]{0,1}/" http-prefix ng-model="allFormsModal.organizationModal['hasURL']"></input>
																<span style="color:Red" ng-show="stagingForm.Urlname.$dirty&&stagingForm.Urlname.$error.pattern">Enter Valid Url</span>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group foundedOne">
																<label class="control-label">Founded</label>
																<p class="input-group">
																	<input type="text" class="form-control" id="OrgFoundedDate" uib-datepicker-popup ng-model="allFormsModal.organizationModal.hasLatestOrganizationFoundedDate"
																	    is-open="dataCuration.popupfoundedDate.opened" datepicker-options="dataCuration.dateOptionsfounded"
																	    ng-required="false" close-text="Close" placeholder="yyyy-mm-dd" ng-blur="dataCuration.CheckDate('OrgFoundedDate')" />
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default" ng-click="dataCuration.openfoundedDate()">
																			<i class="glyphicon glyphicon-calendar"></i>
																		</button>
																	</span>
																</p>
															</div>
															<div class="error-message" >{{ dataCuration.OrgFoundedDate }}</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group emailOne">
																<label class="control-label">Email</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:email']" name="email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"></input>
																<span style="color:Red" ng-show="stagingForm.email.$dirty&&stagingForm.email.$error.pattern">Please Enter Valid Email</span>
															</div>
														</div>
													</div>
													<!--  Second Row Starts  -->
												</div>
											</div>
										</div>
									</div>
									<!--  First Row Ends  -->

									<!--  Third Row Starts  -->
									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header saveDetailsWrapper">
													<div class="row">
														<div class="col-sm-4">Address Details</div>
														<div class="col-sm-8 text-right">
															<button
																ng-hide="!allFormsModal.organizationModal.personalDetails.length"
																type="button"
																class="btn btn-xs btn-remove btn-update pull-right text-uppercase mar-r5"
																ng-click="ContentRemove()" value="Remove">Remove</button>
															<button type="button"
																class="btn btn-xs btn-primary btn-update pull-right text-uppercase addnew"
																value="Add New" ng-click="ContentAdd()">Add New</button>
														</div>
													</div>
												</div>
												<div class="widget-content">
													<table class="table table-condensed">
														<thead>
															<tr>
																<th>
																	<div class="custom-checkbox light-grey">
																		<label class="checkbox-inline"> <input
																			type="checkbox" id="CheckAllId"
																			ng-model="dataCuration.selectedAll" ng-click="checkAll()" />
																			<span><i class="fa fa-check"></i></span>
																		</label>
																	</div>
																</th>
																<th class="text-left">Address Type</th>
																<th class="text-left">Address</th>
																<th class="text-left">City</th>
																<th class="text-left">Zip</th>
																<th class="text-left">Country</th>
															</tr>
														</thead>
														<tbody>
															<tr
																ng-repeat="personalDetail in allFormsModal.organizationModal.personalDetails">
																<td><div class="custom-checkbox light-grey">
																		<label class="checkbox-inline"> <input
																			type="checkbox" ng-model="personalDetail.selected"
																			ng-click="checkUncheckAll()" /> <span><i
																				class="fa fa-check"></i></span>
																		</label>
																	</div></td>
																<td><select ng-model="personalDetail.type">
																		<option>Registered address</option>
																		<option>HQ address</option>
																</select>
																<td><input type="text" class="form-control"
																	ng-model="personalDetail.streetAddress" /></td>
																<td><input type="text" class="form-control"
																	ng-model="personalDetail.city" /></td>
																<td><input type="text" class="form-control"
																	ng-model="personalDetail.zip" /></td>
																<td>
																	<div class="select-box-wrapper custom-select-dropdown">
																		<input type="text" ng-model="personalDetail.country"
																			uib-typeahead="state for state in dataCuration.countriesData | filter:$viewValue | limitTo:8"
																			class="form-control">
																	</div>
															</tr>
															<tr
																ng-if="allFormsModal.organizationModal.personalDetails.length == 0">
																<td colspan="6" style="text-align: center;">No Address
																	Details Available</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!--  Third Row Ends  -->

									<!--  Fourth Row Starts  -->
									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header">Social Links</div>
												<div class="widget-content">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group twitterLinkOne">
																<label class="control-label">Twitter Link</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:twitter']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group fbLinkOne">
																<label class="control-label">FB Link</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:facebook']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group linkedIn">
																<label class="control-label">LinkedIn</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:linkedin']"></input>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group google+">
																<label class="control-label">Google+</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:gplus']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group xingProfileOne">
																<label class="control-label">YouTube</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:youtube']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group xingProfileOne">
																<label class="control-label">Instagram</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['bst:instagram']"></input>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--  Fourth Row Ends  -->

									<!--  Fifth Row Starts  -->
									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header">Sectors</div>
												<div class="widget-content">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group foundedOne">
																<label class="control-label">Industry</label>
																<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.organizationModal['hasPrimaryIndustryGroup']" uib-typeahead="state for state in dataCuration.industriesData | filter:$viewValue | limitTo:8" class="form-control">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group locationsOne">
																<label class="control-label">Holding Classifications</label>
																<input type="text" class="form-control" ng-model="allFormsModal.organizationModal['hasHoldingClassification']"></input>
															</div>
														</div>
														<div class="col-sm-4">
															<label class="control-label">Economic Sector</label>
															<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.organizationModal['hasPrimaryEconomicSector']" uib-typeahead="state for state in dataCuration.economicSectorData | filter:$viewValue | limitTo:8" class="form-control">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-4">
															<label class="control-label">Business Sector</label>
															<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.organizationModal['hasPrimaryBusinessSector']" uib-typeahead="state for state in dataCuration.businessSectorData | filter:$viewValue | limitTo:8" class="form-control">
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group xingProfileOne">
																<label class="control-label">Incorporated In</label>
																<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.organizationModal['isIncorporatedIn']" uib-typeahead="state for state in dataCuration.countriesData | filter:$viewValue | limitTo:8" class="form-control">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group google+">
																<label class="control-label">Domicile</label>
																<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.organizationModal['isDomiciledIn']" uib-typeahead="state for state in dataCuration.countriesData | filter:$viewValue | limitTo:8" class="form-control">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--  Fifth Row Ends  -->

									<!-- Personal information  Table Shown -->
									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header saveDetailsWrapper">
													<div class="row">
														<div class="col-sm-2">Related Entities</div>
														<div class="col-sm-4 custom-select-dropdown">
															<input class="form-control custom-input" type="text" ng-model="allFormsModal['relatedEntities'].entitySearch" placeholder=""
															    uib-typeahead="person as person['vcard:hasName'] for person in dataCuration.getPersonSuggestions($viewValue)"
															    typeahead-loading="loadingPersonsNames" typeahead-no-results="noREResults" class="form-control"> {{person}}
															<div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
																<i class="fa fa-spinner fa-pulse"></i>
															</div>
															<div class="custom-dropdown-menu" ng-show="noREResults">
																<i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
															</div>
														</div>
														<div class="col-sm-6 text-right">
															<button type="button" ng-click="dataCuration.addNewPerson()" ng-disabled="editTab" class="btn btn-xs btn-primary btn-update text-uppercase">Add Related Person</button>
															<button type="button" class="btn btn-xs btn-primary btn-update text-uppercase" ng-disabled="true">Add Related Company</button>
														</div>
													</div>
												</div>
												<div class="widget-content">
													<div class="row">
														<div class="col-sm-12 custom-data-table-wrapper">
															<table id="RelatedPersonInfo" width="100%" datatable="ng" dt-options="persons.dtOptions" dt-column-defs="persons.dtColumnDefs" class="table table-striped">
																<thead>
																	<tr>
																		<th>Person Name</th>
																		<th>Relation</th>
																		<th>Type</th>
																		<th>From Date</th>
																		<th>To Date</th>
																		<th class="text-center">VIEW/EDIT</th>
																	</tr>
																</thead>
																<tbody class="case-diary-list">
																	<tr ng-repeat="person in persons.list">
																		<td>{{ person.name }}</td>
																		<td>{{ person.position }}</td>
																		<td>{{ person.title }}</td>
																		<td id='from_date_'+{{$index}} ng-blur = dataCuration.CheckDate('')>{{ person.from | date }}</td>
																		<td ng-blur = dataCuration.CheckDate('')>{{ person.to | date }}</td>
																		<td class="text-center">
																			<a class="action-entity view-entity" href ng-click="persons.onClickEditStaging(person)">
																				<i class="fa fa-pencil"></i>
																			</a>
																			<a class="action-entity delete-entity" href ng-click="persons.onClickRemovePerson($index)"><i class="fa fa-times"></i></a>
																		</td>
																	</tr>
																	<tr ng-if="ShowClicked">
																	<td>
																	<input type="text" ng-model ="clickedPerson.personName" value = "{{clickedPerson.personName}}" disabled>
																	</td>
																	<td >
																	<div class="select-box-wrapper">
																	<select class="form-control" id="relatedPersonId" data-show-subtext="true" data-live-search="true" ng-model="clickedPerson.relation" ng-change="changeRoleType(relatedPersonId)">
																		<option ng-repeat="a in allrelationsList">{{a}}</option>
																	</select>
																	</div>
																	</td>
																	<td><input ng-model="clickedPerson.title"></td>
																	<td><input ng-model="clickedPerson.from" id="fromRelatedPerson" value="{{ clickedPerson.from | date : 'yyyy-MM-dd'}}" placeholder="yyyy-mm-dd" ng-blur="dataCuration.CheckDate('fromRelatedPerson')">
																	<div class="error-message" >{{ dataCuration.fromRelatedPerson }}</div>
																	</td>
																	<td><input ng-model="clickedPerson.to" id="toRelatedPerson" value="{{clickedPerson.to | date : 'yyyy-MM-dd'}}" placeholder="yyyy-mm-dd" ng-blur="dataCuration.CheckDate('toRelatedPerson')">
																	<div class="error-message" >{{ dataCuration.toRelatedPerson }}</div>
																	</td>
																	<td class="text-center">
																			<a class="action-entity view-entity" href ng-click="addNewRelation()">
																				<i class="fa fa-plus"></i>
																			</a>
<!-- 																			<a class="action-entity delete-entity" href ng-click="persons.onClickRemovePerson($index)"><i class="fa fa-times"></i></a> -->
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- Personal information  Table Ends -->
								</div>
							</uib-tab>
							<!--  General Form Tab Starts  -->

							<!--  Personal Information Tab Starts  -->
							<uib-tab index="1" title="Personal Information" ng-click="disableSave();">
								<uib-tab-heading> Personal Information </uib-tab-heading>
								<div class="staging-content-tab tab-four">

									<div class="row">
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header saveDetailsWrapper">
													<div class="row">
														<div class="col-sm-6">
															Personal Information
														</div>
														<div class="col-sm-6 text-right">
															<button type="button" ng-click="dataCuration.resetFields()" class="btn btn-xs btn-primary btn-update pull-right text-uppercase">Reset Fields</button>
														</div>
													</div>
												</div>
												<div class="widget-content">
													<div class="row actions-wrapper">
														<div class="col-sm-2">
															<div class="form-group nameTwo">
																<label class="control-label">Name Prefix</label>
																<select ng-model="allFormsModal.personModal['vcard:honorific-prefix']" ng-change="dataCuration.changeGender(allFormsModal.personModal['vcard:honorific-prefix'])">
																	<option>Mr.</option>
																	<option>Mrs.</option>
																	<option>Miss</option>
																	<option>Dr.</option>
																</select>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group nameTwo custom-select-dropdown	">
																<label class="control-label">Name</label>
<!-- 																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:hasName']" typeahead-on-select="onSelect($item)" -->
<!-- 																/> -->
															<input class="form-control custom-input" type="text" ng-model="allFormsModal.personModal['vcard:hasName']" placeholder=""
															    uib-typeahead="person as person['vcard:hasName'] for person in dataCuration.getPersonSuggestions($viewValue)"
															    typeahead-loading="loadingPersonsNames" typeahead-no-results="noPIResults" class="form-control" typeahead-template-url="customPersonTemplate.html"> {{person}}
															<i ng-show="loadingPersonsNames" class="glyphicon glyphicon-refresh text-light-blue"></i>
															<div ng-show="noPIResults">
																<i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
															</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group jobTitleTwo">
																<label class="control-label">Family Name</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:family-name']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Additional Name </label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:additional-name']" />
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Preferred name</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['tr-vcard:preferred-name']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Given Name</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:given-name']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Honourable Name Suffix</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:honorific-suffix']" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Nationality</label>
																<div class="select-box-wrapper custom-select-dropdown">
																<input type="text" ng-model="allFormsModal.personModal['bst:nationality']" uib-typeahead="state for state in dataCuration.nationalityData | filter:$viewValue | limitTo:8" class="form-control">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo alsoKnownAsTitle">
																<label class="control-label">Also known As  <a class="also-known-as-title"  data-title="can add multiple names using comma operator(,)."><i class="fa fa-info-circle"></i></a></label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:aka']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group Photo">
																<label class="control-label">Photo</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:hasPhoto']" />
															</div>
														</div>
													</div>

													<div class="row">
													<div class="col-sm-12">
															<div class="custom-widget">
																<div class="widget-header saveDetailsWrapper">
																<div class="row">
																	<div class="col-sm-4">Address Details</div>
																	<div class="col-sm-8 text-right">
																		<button
																			ng-hide="!allFormsModal.personModal['vcard:hasAddress'].length"
																			type="button"
																			class="btn btn-xs btn-remove btn-update pull-right text-uppercase mar-r5"
																			ng-click="ContentRemove()" value="Remove">Remove</button>
																		<button type="button"
																			class="btn btn-xs btn-primary btn-update pull-right text-uppercase addnew"
																			value="Add New" ng-click="ContentAdd()">Add New</button>
																	</div>
																</div>
															</div>
															<div class="widget-content">
																<table class="table table-condensed table-striped table-hover">
																	<thead>
																		<tr>
																			<th>
																				<div class="custom-checkbox light-grey">
																					<label class="checkbox-inline"> <input
																						type="checkbox" id="CheckAllIdDetails"
																						ng-model="dataCuration.selectedAllDetails" ng-click="checkAll()" /> <span><i
																							class="fa fa-check"></i></span>
																					</label>
																				</div>
																			</th>
																			<th class="text-left">Address</th>
																			<th class="text-left">City</th>
																			<th class="text-left">Zip</th>
																			<th class="text-left">Country</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr
																			ng-repeat="personalDetail in allFormsModal.personModal['vcard:hasAddress']">
																			<td><div class="custom-checkbox light-grey">
																					<label class="checkbox-inline"> <input
																						type="checkbox" ng-model="personalDetail.selected"
																						ng-click="checkUncheckAll()" /> <span><i
																							class="fa fa-check"></i></span>
																					</label>
																				</div></td>
																			<td><input type="text" class="form-control"
																				ng-model="personalDetail.streetAddress" /></td>
																			<td><input type="text" class="form-control"
																				ng-model="personalDetail.city" /></td>
																			<td><input type="text" class="form-control"
																				ng-model="personalDetail.zip" /></td>
																			<td>
																				<div class="select-box-wrapper custom-select-dropdown">
																				<input type="text" ng-model="personalDetail.country" uib-typeahead="state for state in dataCuration.countriesData | filter:$viewValue" class="form-control">
																				</div>
																		</tr>
																		<tr
																			ng-if="allFormsModal.personModal['vcard:hasAddress'].length == 0">
																			<td colspan="6" style="text-align:  center;">No Address Details Available</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
			                            			  </div>
				                          			</div>					
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Gender</label>
																<div>
																	<label class="custom-radio">
																		<input type="radio" name="gender" id="maleVal" class="form-control" ng-model="allFormsModal.personModal['vcard:hasGender']"
																		    value="male" ng-change="dataCuration.changePrefix(allFormsModal.personModal['vcard:hasGender'])" />
																		<span>
																			<i class="fa fa-circle"></i>Male</span>
																	</label>
																	<label class="custom-radio">
																		<input type="radio" name="gender" id="femaleVal" class="form-control" ng-model="allFormsModal.personModal['vcard:hasGender']"
																		    value="female" ng-change="dataCuration.changePrefix(allFormsModal.personModal['vcard:hasGender'])" />
																		<span>
																			<i class="fa fa-circle"></i>Female</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Birth Date</label>
																<p class="input-group">
																	<input type="text" id="personBirthDate" class="form-control" uib-datepicker-popup ng-model="allFormsModal.personModal['vcard:bday']" is-open="dataCuration.popupbirthDate.opened"
																	    placeholder="yyyy-mm-dd" ng-blur="dataCuration.CheckDate('personBirthDate')" datepicker-options="dataCuration.dateOptionsbirth" ng-required="true" close-text="Close"
																	/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default" ng-click="dataCuration.openbirthDate()">
																			<i class="glyphicon glyphicon-calendar"></i>
																		</button>
																	</span>
																</p>
															</div>
															<div class="error-message" >{{ dataCuration.personBirthDate }}</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Death Date</label>
																<p class="input-group">
																	<input type="text" class="form-control" id="personDeathDate" uib-datepicker-popup ng-model="allFormsModal.personModal['bst:deathDate']" is-open="dataCuration.popupdeathDate.opened"
																	    placeholder="yyyy-mm-dd" ng-blur="dataCuration.CheckDate('personDeathDate')" datepicker-options="dataCuration.dateOptionsdeath" ng-required="true" close-text="Close"
																	/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default" ng-click="dataCuration.opendeathDate()">
																			<i class="glyphicon glyphicon-calendar"></i>
																		</button>
																	</span>
																</p>
															</div>
															<div class="error-message" >{{ dataCuration.personDeathDate }}</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Telephone</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:hasTelephone']" numbersonly/>
															</div>
														</div>
														<div class="col-sm-8">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Email</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:email']" name="Personemail"
																    ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" />
																<span style="color:Red" ng-show="stagingForm.Personemail.$dirty&&stagingForm.Personemail.$error.pattern">Please Enter Valid Email</span>
															</div>
														</div>
<!-- 														<div class="col-sm-4"> -->
<!-- 															<div class="form-group linkedinUrlTwo"> -->
<!-- 																<label class="control-label">Source Id</label> -->
<!-- 																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['@source-id']" /> -->
<!-- 															</div> -->
<!-- 														</div> -->
													</div>

													<div class="row">
														<div class="col-sm-8">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Description</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:description']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group linkedinUrlTwo">
																<label class="control-label">URL</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['vcard:hasURL']" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="custom-widget">
												<div class="widget-header">Social Links</div>
												<div class="widget-content">
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Facebook Link</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:facebook']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Twitter</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:twitter']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group linkedinUrlTwo">
																<label class="control-label">Linkedin</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:linkedin']" />
															</div>
														</div>
													</div>


													<div class="row">
														<div class="col-sm-4">
															<div class="form-group workLocationTwo">
																<label class="control-label">Instagram</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:instagram']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group jobCategoryTwo">
																<label class="control-label">Google Plus</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:gplus']" />
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group linkedinUrlTwo">
																<label class="control-label">YouTube</label>
																<input type="text" placeholder="" class="form-control" ng-model="allFormsModal.personModal['bst:youtube']" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</uib-tab>
							<!--  Personal Information Tab Ends  -->

							<!--  Board of Management Tab Starts  -->
							<uib-tab disable="!CheckDisable" index="5" style="pointer-events:none;" title="Board of Management">
								<uib-tab-heading> Board of Management </uib-tab-heading>
								<div class="staging-content-tab tab-six">
									<h5 class="info-headings">
										GENERAL INFORMATION
										<button type="button" class="btn add-more-btn" ng-click="staging.boardMembersBM()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group nameSix">
												<label class="control-label">Name</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['name']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group titleSix ">
												<label class="control-label">Title</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['title']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Areas of accountability</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['area_acountability']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Date of Appointment</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['apponitment']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Salary</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['salary']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Country of Origin</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['country']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Work Phone</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['phone_work']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">Work Fax</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['work_fax']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group accountabilitySix">
												<label class="control-label">year of Birth</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['birth_year']" />
											</div>
										</div>
									</div>
									<div class="managementBoard relative" ng-repeat="subsidiary in staging['BoardMgmt_BM']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group nameSix">
													<label class="control-label">Name</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['name']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group titleSix ">
													<label class="control-label">Title</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['title']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Areas of accountability</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['area_acountability']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Date of Appointment</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['apponitment']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Salary</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['salary']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Country of Origin</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['country']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Work Phone</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['phone_work']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">Work Fax</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['work_fax']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group accountabilitySix">
													<label class="control-label">year of Birth</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_BM']['birth_year']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveboardMembersBM($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<h5 class="info-headings">
										KEY EXECUTIVES
										<button type="button" class="btn add-more-btn" ng-click="staging.boardMembersKE()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group nameSix">
												<label class="control-label">Name</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_KE']['name']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group titleSix">
												<label class="control-label">Title</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_KE']['title']" />
											</div>
										</div>
									</div>
									<div class="managementBoard_KE relative" ng-repeat="subsidiary in staging['BoardMgmt_KE']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group nameSix">
													<label class="control-label">Name</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_KE']['name']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group titleSix">
													<label class="control-label">Title</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_KE']['title']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveboardMembersKE($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<h5 class="info-headings">
										AUDIT COMMITTEE
										<button type="button" class="btn add-more-btn" ng-click="staging.boardMembersAC()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group nameSix">
												<label class="control-label">Name</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_AC']['name']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group titleSix">
												<label class="control-label">Title</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_AC']['title']" />
											</div>
										</div>
									</div>
									<div class="managementBoard_AC relative" ng-repeat="subsidiary in staging['BoardMgmt_AC']">
										<div class="form-group addMoreContnet text-right"></div>
										<div class="form-group removeContent text-right " ng-if="$index > 0">
											<button type="button" class="btn remove-btn" ng-click="staging.onClickRemoveboardMembersAC($index)">
												<i class="fa fa-trash"></i>Remove
											</button>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group nameSix">
													<label class="control-label">Name</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_AC']['name']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group titleSix">
													<label class="control-label">Title</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_AC']['title']" />
												</div>
											</div>
										</div>
									</div>
									<h5 class="info-headings">
										OTHER BOARD MEMBERS ON BOARD MEMBERS
										<button type="button" class="btn add-more-btn" ng-click="staging.boardMembersOBM()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group nameSix">
												<label class="control-label">Name</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['name']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group primaryCompanySix">
												<label class="control-label">Primary Company</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['primary_company']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group primaryCompanySix">
												<label class="control-label">Type of Board Members</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['type_board_members']" />
											</div>
										</div>
									</div>
									<div class="managementBoard_OBM relative" ng-repeat="subsidiary in staging['BoardMgmt_OBM']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group nameSix">
													<label class="control-label">Name</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['name']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group primaryCompanySix">
													<label class="control-label">Primary Company</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['primary_company']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group primaryCompanySix">
													<label class="control-label">Type of Board Members</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['BoardMgmt_OBM']['type_board_members']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveboardMembersOBM($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Board of Management Tab Ends  -->

							<!--  Subsidaries Form Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="3" title="Subsidaries Of {{ staging['general_information']['company_name'] || 'Company name' }}">
								<uib-tab-heading> Subsidaries Of {{ staging['general_information']['company_name'] || 'Company name' }} </uib-tab-heading>
								<div class="staging-content-tab tab-four">
									<h3 class="info-headings">
										Subsidaries Of Company
										<button type="button" class="btn add-more-btn" ng-click="staging.addnewGroup()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h3>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group reinsuranceGroupFour">
												<label class="control-label">Company Name</label>
												<input type="text" placeholder="Comapny Name" class="form-control" ng-model="subsidiaries['company_name']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group groupFour">
												<label class="control-label">Group Name</label>
												<input type="text" uib-typeahead="group for group in getStates($viewValue) | filter:$viewValue | limitTo:8" placeholder=""
												    class="form-control" ng-model="group_name" typeahead-on-select="onSet($item)" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group ceoFour">
												<label class="control-label">CEO</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['ceo']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group revenueFour">
												<label class="control-label">Revenue</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['revenue']" />
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group companySizeFour">
												<label class="control-label">Company Size</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['company_size']" />
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group mainBusinessFour">
												<label class="control-label">Main Business</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['main_business']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group hqLocationFour">
												<label class="control-label">HQ Location</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['hq_location']" />
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group linkedinProfileFour">
												<label class="control-label">linkedin profile</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['linkedin_profile']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group facebookProfileFour">
												<label class="control-label">Facebook Profile</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['facebook_profile']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group twitterUrlFour">
												<label class="control-label">Twitter Url</label>
												<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['twitter_url']" />
											</div>
										</div>
									</div>
									<div class="investmentRepeat1 relative" ng-repeat="subsidiary in staging['subsidiaries']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group reinsuranceGroupFour">
													<label class="control-label">Company Name</label>
													<input type="text" placeholder="Comapny Name" class="form-control" ng-model="subsidiaries['company_name']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group groupFour">
													<label class="control-label">Group Name</label>
													<input type="text" uib-typeahead="group for group in getStates($viewValue) | filter:$viewValue | limitTo:8" placeholder=""
													    class="form-control" ng-model="group_name" typeahead-on-select="onSet($item)" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group ceoFour">
													<label class="control-label">CEO</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['ceo']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group revenueFour">
													<label class="control-label">Revenue</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['revenue']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group companySizeFour">
													<label class="control-label">Company Size</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['company_size']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group mainBusinessFour">
													<label class="control-label">Main Business</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['main_business']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group hqLocationFour">
													<label class="control-label">HQ Location</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['hq_location']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group linkedinProfileFour">
													<label class="control-label">linkedin profile</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['linkedin_profile']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group facebookProfileFour">
													<label class="control-label">Facebook Profile</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['facebook_profile']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group twitterUrlFour">
													<label class="control-label">Twitter Url</label>
													<input type="text" placeholder="" class="form-control" ng-model="subsidiaries['twitter_url']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeCurrentGroup($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Subsidaries Form Tab Starts  -->

							<!--  Top Mutual Fund Holders Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="9" title="Top Mutual Fund Holders">
								<uib-tab-heading> Stocks Graph </uib-tab-heading>
								<div class="staging-content-tab tab-ten">
									<h5 class="info-headings">
										Top Mutual Fund Holders
										<button type="button" class="btn add-more-btn" ng-click="staging.stocksGroup()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="form-group holderTen">
										<label class="control-label">Holder</label>
										<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['holder']" />
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group sharesTen">
												<label class="control-label">Shares</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['shares']">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group reportedDateTen">
												<label class="control-label">Date Reported</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['data_reported']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group outTen">
												<label class="control-label">% Out</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['percent_out']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group valueTen">
												<label class="control-label">$ Value</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['total_value']" />
											</div>
										</div>
									</div>
									<div class="stocksGraph relative" ng-repeat="subsidiary in staging['stocksGrpah']">
										<div class="form-group holderTen">
											<label class="control-label">Holder</label>
											<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['holder']" />
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group sharesTen">
													<label class="control-label">Shares</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['shares']">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group reportedDateTen">
													<label class="control-label">Date Reported</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['data_reported']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group outTen">
													<label class="control-label">% Out</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['percent_out']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group valueTen">
													<label class="control-label">$ Value</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['stocksGrpah']['total_value']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeStocksGroup($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<div class="form-group graph">
										<h2>Graph</h2>
									</div>
								</div>
							</uib-tab>
							<!--  Top Mutual Fund Holders Tab Starts  -->

							<!--  Last 5 Yrs Annual Report Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="2" title="Last 5 Yrs Annual Report">
								<uib-tab-heading> Last 5 Yrs Annual Report </uib-tab-heading>
								<div class="staging-content-tab tab-three">
									<div class="form-group taxesOnIncomeThree">
										<label class="control-label">Taxes on income ( Munich Re Group )</label>
										<input type="text" placeholder="" class="form-control" ng-model="staging['leadership']['taxes_on_income']" />
									</div>
									<div class="form-group currencyTypeThree">
										<label class="control-label">Currency type/ Amount type</label>
										<input type="text" placeholder="" class="form-control" ng-model="staging['leadership']['currency_type']" />
									</div>
									<h5>Respective Financial Years:</h5>
									<div class="form-group 2016Three">
										<label class="control-label">2016</label>
										<input type="number" placeholder="" class="form-control" ng-model="staging['leadership']['2016two']" />
									</div>
									<div class="form-group 2015Three">
										<label class="control-label">2015</label>
										<input type="number" placeholder="" class="form-control" ng-model="staging['leadership']['2015two']" />
									</div>
									<div class="form-group 2014Three">
										<label class="control-label">2014</label>
										<input type="number" placeholder="" class="form-control" ng-model="staging['leadership']['2014two']" />
									</div>
									<div class="form-group 2013Three">
										<label class="control-label">2013</label>
										<input type="number" placeholder="" class="form-control" ng-model="staging['leadership']['2013two']" />
									</div>
									<div class="form-group 2012Three">
										<label class="control-label">2012</label>
										<input type="number" placeholder="" class="form-control" ng-model="staging['leadership']['2012two']" />
									</div>
									<div class="form-group addProfile text-right">
										<button type="button add-more-btn" class="btn">
											<i class="fa fa-plus"></i> Add More
										</button>
									</div>
									<div class="form-group removeProfile text-right " ng-if="$index > 0">
										<button type="button" class="btn remove-btn">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Last 5 Yrs Annual Report Tab Starts  -->

							<!--  Current Job Openings Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="4" title="Current Job Openings">
								<uib-tab-heading> Current Job Openings </uib-tab-heading>
								<div class="staging-content-tab tab-five">
									<h3 class="info-headings">
										Current Job Openings At {{ staging['general_information']['company_name'] || 'Company name' }}
										<button type="button" class="btn add-more-btn" ng-click="staging.jobOpenings()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h3>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group titleFive">
												<label class="control-label">Title</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['title']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group companySubsidaryFive">
												<label class="control-label">Company / Subsidary</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['company_subsidary']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group locationFive">
												<label class="control-label">Location</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['location_five']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-8">
											<div class="form-group investorSinceFive">
												<label class="control-label">description</label>
												<textarea type="text" placeholder="" class="form-control" ng-model="staging['investors']['description_five']"></textarea>
											</div>
										</div>
									</div>
									<div class="jobOpenings relative" ng-repeat="job in staging['job_openings']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group titleFive">
													<label class="control-label">Title</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['title']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group companySubsidaryFive">
													<label class="control-label">Company / Subsidary</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['company_subsidary']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group locationFive">
													<label class="control-label">Location</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['investors']['location_five']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-8">
												<div class="form-group investorSinceFive">
													<label class="control-label">description</label>
													<textarea type="text" placeholder="" class="form-control" ng-model="staging['investors']['description_five']"></textarea>
												</div>
											</div>
										</div>

										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveJobOpenings($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Current Job Openings Tab Starts  -->

							<!--  Management Profiles Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="6" title="Management Profiles">
								<uib-tab-heading> Management Profiles </uib-tab-heading>
								<div class="staging-content-tab tab-seven">
									<h5 class="info-headings">
										Board of Management
										<button type="button" class="btn add-more-btn" ng-click="staging.addManagementProfile()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group nameSeven">
												<label class="control-label">Name</label>
												<input type="text" placeholder="enter name" class="form-control" ng-model="staging['mgmt_profile']['name']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group url1Seven">
												<label class="control-label">Birth Year</label>
												<input type="text" placeholder="Birth Year" class="form-control" ng-model="staging['mgmt_profile']['birth_year']">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group url2Seven">
												<label class="control-label">Compensation</label>
												<input type="text" placeholder="Compensation Amount" class="form-control" ng-model="staging['mgmt_profile']['compensation']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group url3Seven">
												<label class="control-label">Country of Origin</label>
												<input type="text" placeholder="Country name with native Place" class="form-control" ng-model="staging['mgmt_profile']['country']">
												</textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group url4Seven">
												<label class="control-label">Contact Phone</label>
												<input type="text" placeholder="Phone number" class="form-control" ng-model="staging['mgmt_profile']['contact']">
												</textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group url5Seven">
												<label class="control-label">Fax</label>
												<input type="text" placeholder="fax" class="form-control" ng-model="staging['mgmt_profile']['fax']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="BMProfile relative" ng-repeat="profile in staging['mgmt_profile']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group nameSeven">
													<label class="control-label">Name</label>
													<input type="text" placeholder="enter name" class="form-control" ng-model="staging['mgmt_profile']['name']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group url1Seven">
													<label class="control-label">Birth Year</label>
													<input type="text" placeholder="Birth Year" class="form-control" ng-model="staging['mgmt_profile']['birth_year']">
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group url2Seven">
													<label class="control-label">Compensation</label>
													<input type="text" placeholder="Compensation" class="form-control" ng-model="staging['mgmt_profile']['compensation']">
													</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group url3Seven">
													<label class="control-label">Country of Origin</label>
													<input type="text" placeholder="Country name with native Place" class="form-control" ng-model="staging['mgmt_profile']['country']">
													</textarea>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group url4Seven">
													<label class="control-label">Contact Phone</label>
													<input type="text" placeholder="Phone Number" class="form-control" ng-model="staging['mgmt_profile']['contact']">
													</textarea>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group url5Seven">
													<label class="control-label">Fax</label>
													<input type="text" placeholder="fax" class="form-control" ng-model="staging['mgmt_profile']['fax']">
													</textarea>
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removemanagementProfile($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<h5 class="info-headings">
										Key Executives
										<button type="button" class="btn add-more-btn" ng-click="staging.addexecutiveProfile()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group contactPersonOne">
												<label class="control-label">Name</label>
												<input class="form-control" ng-model="staging['mgmt_key_exec']['name']"></input>
											</div>
										</div>
										<div class="col-sm-8">

											<div class="form-group contactDetailsOne">
												<label class="control-label">Profile Links</label>
												<textarea class="form-control" ng-model="staging['mgmt_key_exec']['links']"></textarea>
											</div>
										</div>
									</div>
									<div class="personals relative" ng-repeat="persons in staging['mgmt_key_exec']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group contactPersonOne">
													<label class="control-label">Name</label>
													<input class="form-control" ng-model="staging['mgmt_key_exec']['name']"></input>
												</div>
											</div>
											<div class="col-sm-8">

												<div class="form-group contactDetailsOne">
													<label class="control-label">Profile Links</label>
													<textarea class="form-control" ng-model="staging['mgmt_key_exec']['links']"></textarea>
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeexecutiveProfile($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Management Profiles Tab Starts  -->

							<!--  Articles Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="7" title="Articles">
								<uib-tab-heading> Articles </uib-tab-heading>
								<div class="staging-content-tab tab-eight">
									<h5 class="info-headings">
										ARTICLES DETAILS
										<button type="button" class="btn add-more-btn" ng-click="staging.addNewSource()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group sourceEight">
												<label class="control-label">Source</label>
												<input type="text" placeholder="enter source" class="form-control" ng-model="staging['source_links']['from']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group dumpURLEight">
												<label class="control-label">URL</label>
												<input type="text" placeholder="enter url" class="form-control" ng-model="staging['source_links']['dump_uRL']">
												</textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group informationEight">
												<label class="control-label">Information</label>
												<input type="text" placeholder="Related information" class="form-control" ng-model="staging['source_links']['information']">
											</div>
										</div>
									</div>
									<div class="personals relative one">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group sourceEight">
													<label class="control-label">Source</label>
													<input type="text" placeholder="enter source" class="form-control" ng-model="staging['source_links']['from']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group dumpURLEight">
													<label class="control-label">URL</label>
													<input type="text" placeholder="enter url" class="form-control" ng-model="staging['source_links']['dump_uRL']">
													</textarea>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group informationEight">
													<label class="control-label">Information</label>
													<input type="text" placeholder="Related information" class="form-control" ng-model="staging['source_links']['information']">
												</div>
											</div>
										</div>
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['source_links']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group sourceEight">
													<label class="control-label">Source</label>
													<input type="text" placeholder="enter source" class="form-control" ng-model="staging['source_links']['from']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group dumpURLEight">
													<label class="control-label"> URL</label>
													<input type="text" placeholder="enter url" class="form-control" ng-model="staging['source_links']['dump_uRL']">
													</textarea>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group informationEight">
													<label class="control-label">Information</label>
													<input type="text" placeholder="Related information" class="form-control" ng-model="staging['source_links']['information']">
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeCurrentSource($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<hr>
									<h5 class="info-headings">
										Gossips About
										<button type="button" class="btn add-more-btn" ng-click="staging.addNewgossipTopic()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group continentOne">
												<label class="control-label">Title</label>
												<input class="form-control" ng-model="staging['source_gossips']['title']"></input>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group locationsOne">
												<label class="control-label">Link</label>
												<input class="form-control" ng-model="staging['source_gossips']['link']">
											</div>
										</div>
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['source_gossips']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group continentOne">
													<label class="control-label">Title</label>
													<input class="form-control" ng-model="staging['source_gossips']['title']"></input>
												</div>

											</div>
											<div class="col-sm-6">
												<div class="form-group locationsOne">
													<label class="control-label">Link</label>
													<input class="form-control" ng-model="staging['source_gossips']['link']">
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeNewgossipTopic($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>

								</div>
							</uib-tab>
							<!--  Articles Tab Starts  -->

							<!--  Partnerships & Invesments Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="8" title="Partnerships & Invesments">
								<uib-tab-heading> Partnerships / Invesments </uib-tab-heading>
								<div class="staging-content-tab tab-nine">
									<h5 class="info-headings">
										Aquisitions
										<button type="button" class="btn add-more-btn" ng-click="staging.addnewAquisition()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group entityNameNine">
												<label class="control-label">Acqired Entity Name</label>
												<input type="text" placeholder="enter entity name" class="form-control" ng-model="staging['aquisition']['entity']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group acquisitionNine">
												<label class="control-label">Date of Acquisition</label>
												<input type="date" placeholder="enter date of acquisition" class="form-control" ng-model="staging['aquisition']['date']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group entityNameNine">
												<label class="control-label">Acqired By</label>
												<input type="text" placeholder="enter Company name" class="form-control" ng-model="staging['aquisition']['company']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group acquisitionNine">
												<label class="control-label">Type</label>
												<input type="text" placeholder="enter amount of shares" class="form-control" ng-model="staging['aquisition']['shares_of']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="form-group articleLinkNine">
										<label class="control-label">Article Link</label>
										<input type="text" placeholder="enter article link" class="form-control" ng-model="staging['aquisition']['links']" />
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['aquisition']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group entityNameNine">
													<label class="control-label">Acqired Entity Name</label>
													<input type="text" placeholder="enter entity name" class="form-control" ng-model="staging['aquisition']['entity']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group acquisitionNine">
													<label class="control-label">Date of Acquisition</label>
													<input type="date" placeholder="enter date of acquisition" class="form-control" ng-model="staging['aquisition']['date']">
													</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group entityNameNine">
													<label class="control-label">Acqired By</label>
													<input type="text" placeholder="enter Company name" class="form-control" ng-model="staging['aquisition']['company']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group acquisitionNine">
													<label class="control-label">Type</label>
													<input type="text" placeholder="enter amount of shares" class="form-control" ng-model="staging['aquisition']['shares_of']">
													</textarea>
												</div>
											</div>
										</div>
										<div class="form-group articleLinkNine">
											<label class="control-label">Article Link</label>
											<input type="text" placeholder="enter article link" class="form-control" ng-model="staging['aquisition']['links']" />
										</div>

										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removenewAquisition($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<hr>
									<h5 class="info-headings">
										Investments/Fundings
										<button type="button" class="btn add-more-btn" ng-click="staging.addNewInvestmentDetail()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group titlleNine">
												<label class="control-label">Company Name</label>
												<input type="text" placeholder="Enter title" class="form-control" ng-model="staging['invest_details']['company_name']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group urlNine">
												<label class="control-label">Amount</label>
												<input type="text" placeholder="Enter amount" class="form-control" ng-model="staging['invest_details']['amount']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group titlleNine">
												<label class="control-label">Funding Date</label>
												<input type="date" placeholder="Enter title" class="form-control" ng-model="staging['invest_details']['fund_date']" />
											</div>
										</div>
										<div class="col-sm-9">
											<div class="form-group urlNine">
												<label class="control-label">Related Article Link</label>
												<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['invest_details']['article']" />
											</div>
										</div>
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['invest_details']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group titlleNine">
													<label class="control-label">Company Name</label>
													<input type="text" placeholder="Enter title" class="form-control" ng-model="staging['invest_details']['company_name']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group urlNine">
													<label class="control-label">Amount</label>
													<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['news']['amount']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<div class="form-group titlleNine">
													<label class="control-label">Funding Date</label>
													<input type="text" placeholder="Enter title" class="form-control" ng-model="staging['news']['fund_date']" />
												</div>
											</div>
											<div class="col-sm-9">
												<div class="form-group urlNine">
													<label class="control-label">Related Article Link</label>
													<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['news']['article']" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removeInvestmentDetail($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
									<h5>Cooperations, memberships and committees</h5>
									<div class="form-group nameNine">
										<textarea type="text" placeholder="List of Entities" class="form-control" ng-model="staging['']['']"></textarea>
									</div>

									<h5>Foundations</h5>
									<div class="form-group nameNine">
										<textarea type="text" placeholder="List of Foundations" class="form-control" ng-model="staging['']['']"></textarea>
									</div>

									<h5>Initiatives and Activities</h5>
									<div class="form-group nameNine">
										<textarea type="text" placeholder="List of Initiatives and Activities" class="form-control" ng-model="staging['']['']"></textarea>
									</div>
								</div>
							</uib-tab>
							<!--  Partnerships & Invesments Tab Starts  -->

							<!--  Competitors Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="10" title="Competitors">
								<uib-tab-heading> Competitors </uib-tab-heading>
								<div class="staging-content-tab tab-eleven">
									<h5 class="info-headings">
										Competitors of {{ staging['general_information']['company_name'] || 'Company name' }}
										<button type="button" class="btn add-more-btn" ng-click="staging.onClickCompetitors()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group sourceEleven">
												<label class="control-label">Name Of Company</label>
												<input type="text" placeholder="Enter company name" class="form-control" ng-model="staging['Competitors']['source_eleven']"
												/>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group sizeEleven">
												<label class="control-label">Size of Company</label>
												<input type="text" placeholder="Enter company size" class="form-control" ng-model="staging['Competitors']['size_eleven']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group revenueEleven">
												<label class="control-label">Revenue</label>
												<input type="text" placeholder="enter revenue" class="form-control" ng-model="staging['Competitors']['revenue_eleven']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group ceoEleven">
												<label class="control-label">Ceo</label>
												<input type="text" placeholder="Enter ceo" class="form-control" ng-model="staging['Competitors']['ceo_eleven']" />
											</div>
										</div>
									</div>
									<div class="form-group linkedinEleven">
										<label class="control-label">Linkedin Profile</label>
										<input type="text" placeholder="Enter Linkedin profile" class="form-control" ng-model="staging['Competitors']['linkedin_eleven']"
										/>
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['Competitors']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group sourceEleven">
													<label class="control-label">Name Of Company</label>
													<input type="text" placeholder="Enter company name" class="form-control" ng-model="staging['Competitors']['source_eleven']"
													/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group sizeEleven">
													<label class="control-label">Size of Company</label>
													<input type="text" placeholder="Enter company size" class="form-control" ng-model="staging['Competitors']['size_eleven']">
													</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group revenueEleven">
													<label class="control-label">Revenue</label>
													<input type="text" placeholder="enter revenue" class="form-control" ng-model="staging['Competitors']['revenue_eleven']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group ceoEleven">
													<label class="control-label">Ceo</label>
													<input type="text" placeholder="Enter ceo" class="form-control" ng-model="staging['Competitors']['ceo_eleven']" />
												</div>
											</div>
										</div>
										<div class="form-group linkedinEleven">
											<label class="control-label">Linkedin Profile</label>
											<input type="text" placeholder="Enter Linkedin profile" class="form-control" ng-model="staging['Competitors']['linkedin_eleven']"
											/>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveCompetitors($index)">
											<i class="fa fa-trash"></i> Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Competitors Tab Starts  -->

							<!--  Technology Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="11" title="Technology">
								<uib-tab-heading> Technology </uib-tab-heading>
								<div class="staging-content-tab tab-twelve">
									<h5 class="info-headings">
										Technologies of {{ staging['general_information']['company_name'] || 'Company name' }}
										<button type="button" class="btn add-more-btn" ng-click="staging.addnewtech()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group sourceTwelve">
												<label class="control-label">Source</label>
												<input type="text" placeholder="Enter source" class="form-control" ng-model="staging['technology']['source']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group urlTwelve">
												<label class="control-label">URL</label>
												<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['technology']['url']">
												</textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group aboutTwelve">
												<label class="control-label">About</label>
												<textarea type="text" placeholder="enter information" class="form-control" ng-model="staging['technology']['about']"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group sourceTwelve">
												<label class="control-label">Source</label>
												<input type="text" placeholder="Enter source" class="form-control" ng-model="staging['technology']['source']" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group urlTwelve">
												<label class="control-label">URL</label>
												<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['technology']['url']">
												</textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group aboutTwelve">
												<label class="control-label">About</label>
												<textarea type="text" placeholder="enter information" class="form-control" ng-model="staging['technology']['about']"></textarea>
											</div>
										</div>
									</div>
									<div class="Locations relative" ng-repeat="locations in staging['technology']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group sourceTwelve">
													<label class="control-label">Source</label>
													<input type="text" placeholder="Enter source" class="form-control" ng-model="staging['technology']['source']" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group urlTwelve">
													<label class="control-label">URL</label>
													<input type="text" placeholder="Enter url" class="form-control" ng-model="staging['technology']['url']">
													</textarea>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group aboutTwelve">
													<label class="control-label">About</label>
													<textarea type="text" placeholder="enter information" class="form-control" ng-model="staging['technology']['about']"></textarea>
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.removecurrenttech($index)">
											<i class="fa fa-trash"></i> Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Technology Tab Starts  -->

							<!--  Documents Tab Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="12" title="Documents">
								<uib-tab-heading> Documents </uib-tab-heading>
								<div class="staging-content-tab tab-thirteen">
									<h5 class="info-headings">
										Documents Related to Munich Re
										<button type="button" class="btn add-more-btn" ng-click="staging.munchiReDocuments()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group titleThirteen">
												<label class="control-label">Title of Document</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['title_eleven']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group urlThirteen">
												<label class="control-label">File URL</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['url_eleven']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group titleThirteen">
												<label class="control-label">Title of Document</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['title_eleven']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group urlThirteen">
												<label class="control-label">File URL</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['url_eleven']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group titleThirteen">
												<label class="control-label">Title of Document</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['title_eleven']" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group urlThirteen">
												<label class="control-label">File URL</label>
												<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['url_eleven']">
												</textarea>
											</div>
										</div>
									</div>
									<div class="Documents relative" ng-repeat="doc in staging['Documents_eleven']">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group titleThirteen">
													<label class="control-label">Title of Document</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['title_eleven']" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group urlThirteen">
													<label class="control-label">File URL</label>
													<input type="text" placeholder="" class="form-control" ng-model="staging['Documents_eleven']['url_eleven']">
													</textarea>
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickMunchiReDocuments($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Documents Tab Starts  -->

							<!--  Top People Who work for the Company Starts  -->
							<uib-tab disable="!CheckDisable" style="pointer-events:none;" index="16" title="Top People Who work for the Company!!!">
								<uib-tab-heading> Assets of Company </uib-tab-heading>
								<div class="staging-content-tab tab-two">
									<h5 class="info-headings">
										Executives Who Work For {{ staging['general_information']['company_name'] || 'Company name' }} :
										<button type="button" class="btn add-more-btn" ng-click="staging.onClickAddInvestment()">
											<i class="fa fa-plus"></i> Add More
										</button>
									</h5>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group nameTwo">
												<label class="control-label">Name</label>
												<input type="text" uib-typeahead="file as file.name for file in names | filter:$viewValue | limitTo:8" placeholder="" class="form-control"
												    ng-model="investment['name']" typeahead-on-select="onSelect($item)" />
												<i class="fa fa-plus addDetails" ng-model="addNewDetails" ng-click="staging.addNewPersonDetail()"></i>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group jobTitleTwo">
												<label class="control-label">Job Title</label>
												<input type="text" placeholder="" class="form-control" ng-model="job_title" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group subsidaryTwo">
												<label class="control-label">Subsidary Name</label>
												<input type="text" placeholder="" class="form-control" ng-model="investment['subsidary_name']" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group workLocationTwo">
												<label class="control-label">Work Location </label>
												<input type="text" placeholder="" class="form-control" ng-model="workLocat" typeahead-on-select="onSetlocations($item)" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group jobCategoryTwo">
												<label class="control-label">Job Category</label>
												<input type="text" placeholder="" class="form-control" ng-model="job_category" />
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group linkedinUrlTwo">
												<label class="control-label">Linkedin Url</label>
												<input type="text" placeholder="" class="form-control" ng-model="linkedin_url" />
											</div>
										</div>
									</div>
									<div class="personals relative" ng-repeat="investments in staging['investment']">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group nameTwo">
													<label class="control-label">Name</label>
													<input type="text" uib-typeahead="file as file.name for file in names | filter:$viewValue | limitTo:8" placeholder="" class="form-control"
													    ng-model="investment['name']" typeahead-on-select="onSelect($item)" />
													<i class="fa fa-plus addDetails" ng-model="addNewDetails" ng-click="staging.addNewPersonDetail()"></i>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group jobTitleTwo">
													<label class="control-label">Job Title</label>
													<input type="text" placeholder="" class="form-control" ng-model="job_title" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group subsidaryTwo">
													<label class="control-label">Subsidary Name</label>
													<input type="text" placeholder="" class="form-control" ng-model="investment['subsidary_name']" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group workLocationTwo">
													<label class="control-label">Work Location </label>
													<input type="text" placeholder="" class="form-control" ng-model="workLocat" typeahead-on-select="onSetlocations($item)" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group jobCategoryTwo">
													<label class="control-label">Job Category</label>
													<input type="text" placeholder="" class="form-control" ng-model="job_category" />
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group linkedinUrlTwo">
													<label class="control-label">Linkedin Url</label>
													<input type="text" placeholder="" class="form-control" ng-model="linkedin_url" />
												</div>
											</div>
										</div>
										<button type="button" class="btn remove-btn" ng-if="$index > 0" ng-click="staging.onClickRemoveInvestment($index)">
											<i class="fa fa-trash"></i>Remove
										</button>
									</div>
								</div>
							</uib-tab>
							<!--  Top People Who work for the Company Starts  -->

						</uib-tabset>
						<div class="form-group saveDetailsWrapper text-left">
							<button type="submit" class="btn" ng-disabled="dsbl && (!allFormsModal.personModal['vcard:hasName'] && !allFormsModal.personModal['vcard:given-name'] && !allFormsModal.personModal['vcard:family-name'])">Save</button>

							<button type="button" ng-click="dataCuration.saveCurationDetails(allFormsModal,'CreateNew')" ng-hide="dataCuration.tabActive ==1 || !editTab"
							    class="btn1" ng-disabled="dsbl && (!allFormsModal.personModal['vcard:hasName'] && !allFormsModal.personModal['vcard:given-name'] && !allFormsModal.personModal['vcard:family-name'])">Save and Create New</button>
						</div>
					</form>
				</div>
				<!--  Entity Company Ends  -->

			</div>
		</div>
		<!--  Enrich Main Wrapper Ends  -->

		<script>
			$(document).ready(function () {
				/*Custom Scroll Bar*/
				$('#saved-searches .panel-body').mCustomScrollbar({
					axis: "y"
				});
				$('#recent-searches .panel-body').mCustomScrollbar({
					axis: "y"
				});
				$('#widgetFilter').mCustomScrollbar({
					axis: "y",
					autoHideScrollbar: true
				});
				$('.company-person-collapse-result').mCustomScrollbar({
					axis: "y"
				});
				$('#nodeSearchContentParent').mCustomScrollbar({
					axis: "y",
					autoHideScrollbar: true
				});
				$('.staging-content-tab').mCustomScrollbar({
					axis: "y",
					autoHideScrollbar: true
				});
// 				$(".editStaging-wrapper .nav-tabs").mCustomScrollbar({
// 					axis: "y",
// 					autoHideScrollbar: true
// 				});
				$('.searched-tag-wrapper').mThumbnailScroller({
					axis: "x"
				});
				$(
					'.update-entities-modal.related-person-modal .modal-body .editStaging-wrapper  .tab-content .staging-content-tab'
				).mCustomScrollbar({
					axis: "y"
				});
			});
		</script>
<!--        <img ng-src="http://upload.wikimedia.org/wikipedia/commons/thumb/{{match.model.flag}}" width="16">
			<span ng-bind-html="match.model['hasLatestOrganizationFoundedDate'] | uibTypeaheadHighlight:query"></span> 		 -->
<script type="text/ng-template" id="customTemplate.html">
  <a>
	  <span ng-bind-html="match.model['vcard:organization-name'] | uibTypeaheadHighlight:query"></span>
      - <span ng-show="match.model['isDomiciledIn']" ng-bind-html="match.model['isDomiciledIn'] | uibTypeaheadHighlight:query"></span>
  </a>
</script>

<script type="text/ng-template" id="customPersonTemplate.html">
  <a>
	  <span ng-bind-html="match.model['vcard:hasName'] | uibTypeaheadHighlight:query"></span>
        <span ng-show="match.model['vcard:bday']" ng-bind-html="match.model['vcard:bday'] | uibTypeaheadHighlight:query | date:'MM/dd/yyyy'"></span>
  </a>
</script>
		