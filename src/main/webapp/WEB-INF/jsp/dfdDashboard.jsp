<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="common/submenu.jsp"%>  
<!--  SubMenu Ends  -->

	<div class="container dfd-wrapper">
		<div ng-if="disableSearchButton" class="custom-spinner full-page-spinner">
			<i class="fa fa-spinner fa-spin fa-3x"></i>
		</div>
		<div ng-if="!disableSearchButton" class="search-button-wrpper">
			<!-- <go-previous-page></go-previous-page> -->
			<h3>{{searchText}}</h3>
			
			<div class="row">
			 	<div class="col-sm-6">
			 		<p><span>TOTAL NODES </span><strong>{{totalNodesCount}}</strong></p>
			 	</div>
			 	<div class="col-sm-6">
			 		<p><span>HIGH RELEVANCE </span><strong>{{totalHighRelevanceCount}}</strong></p>
			 	</div>
			</div>	
			
			<div id="counts-on-hover" class="row" style="display: none;">
				<div class="col-sm-6">
					<p><span>TOTAL CONNECTIONS </span><strong>{{totalHoverNodesCount}}</strong></p>
				</div>
				<div class="col-sm-6">
					<p><span>HIGH RELEVANCE </span><strong>{{totalHoverHighRelevanceCount}}</strong></p>
				</div>
			</div>
		</div>
		<div class="row chart-row inactive" style="display: block;">
			<div class="chart-col" ng-show="!noResultFound">		
				<div id="forceCollapse"></div>
			</div>
			<div class="chart-col">
				<div id="netwrokForceCollapse"></div>
			</div>
		</div>
		<div ng-if="noResultFound" class="error-message-wrpper mnh-dashboard ">
			<h3 class="no-data-wrapper text-center">No data found </h3>
		</div>
	</div>
</div>

<script>
$(".case-list-wrapper").mThumbnailScroller({
	axis : "x"
});
$(".upload-buttons-wrapper").mThumbnailScroller({
	axis : "x"
});
</script>