<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.util.ResourceBundle" %>
<% ResourceBundle resource = ResourceBundle.getBundle("ehub-ui");
String basePath=resource.getString("element.api.base.url");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="bst_element">
<head>
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> -->
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Element by Blackswan</title>
    <link rel="shortcut icon" href="assets/images/logo-new.png" />
    <link href="assets/css/common-styles.min.css" rel="stylesheet" />
    <script src='./vendor/jquery/js/jquery.min.js'></script>
</head>
<body class="bst_login_wrapper p-rel">
    <!-- Login Page Video CLip -->
    <div class="shadow-wrapper hidden-small">
    </div>
    <video autoplay muted id="login_video" class="hidden-small">
        <source src="assets/clip/BlackSwan_Trim.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <!--Version Numbers Info-->
    <p class="text-center mar-0 p-abs v-position c-arrow f-14 roboto-regular z-999 text-dark-cream" id="version">version 1.7.0</p>

    <!-- Authentication Wrapper Starts -->
    <div class="container width-100 authentication-wrapper">
        <div class="row">
            <!-- Login Wrapper Starts -->
            <div class="col-sm-12 mxw-none login-wrapper">
                <h2 class="hidden">
                    Experience the next generation cognitive intelligent platform for your business analysis
                    <img class="img-responsive" src="assets/images/half-spinner.png" alt="half-spinner">
                </h2>
                <!-- Box Wrapper Starts -->
                <div class="row box-group-wrapper">
                    <div class="login-info-wrapper z-999 pad-l0 d-ib p-rel col-sm-6"><img class="img-responsive d-ib center-block"
                            src="assets/images/logo-ele.png" alt="logo">

                        <p class="login-caption-wrapper c-arrow text-cream">Intelligence
                            of Everything</p>
                    </div>
                    <!-- Logo Box Starts -->
                    <div class="col-xs-6 hidden box logo-box">
                        <img class="img-responsive center-block" src="assets/images/logo.png" alt="logo" />
                        <img class="img-responsive center-block" src="assets/images/element.png" alt="element" />
                        
                    </div>
                    <!-- Logo Box Ends -->
                    <!-- Login Box Starts -->
                    <div class="col-xs-4 box  z-999 pad-0 pull-right dark-black-panels login-box " id="loginSection">
                        <div class="shadow"></div>
                        <div class="shadow-overlay-wrapper">
                            <c:url value='/login' var="loginUrl" />
                            <form class="form" name="loginForm" id="lgnFrm" action="${loginUrl}" method='POST'>
                                <h3 class="text-center form-name-wrapper c-arrow  text-cream roboto-bold text-capitalize">Sign
                                    In</h3>
                                <div class="form-group mar-b25 mar-x0">
                                    <div class="width-100 p-rel bst_input_group">
                                            <div class="input-shadow-wrapper"></div>
                                        <input type="text" placeholder="Username or Email address" class="custom-input height-100" id="username"
                                            name="username" />
                                        <span class="label">Username or Email address</span>
                                    </div>
                                </div>
                                <div class="form-group mar-x0">
                                    <div class="width-100 p-rel bst_input_group">
                                            <div class="input-shadow-wrapper"></div>
                                        <input type="password" placeholder="Password" id="password" class="custom-input height-100"
                                            name="password" />
                                        <span for="password" class="label">Password</span>

                                        <span id="showpsd" onclick="changeIcon('showpsd','password')" class="fa fa-eye c-pointer icon-right text-dark-cream pull-right t-25">


                                        </span>
                                    </div>
                                </div>
                                <%if (request.getParameter("login_error") != null) {%>
                                    <div class="danger-alerts lh-14 d-flex errors " id="loginErrorBox">                                       
                                        <span>Sorry, We are unable to find the user with username and password provided.</span>
                                        <span class="c-pointer text-dark-cream mar-l25 as-st f-18" onclick='closeLoginErrorBox()'>&times;</span>
                                    </div>
                                    <% } %> 
                                <!-- <p class="danger-alerts hidden">Lorem ipsum dolor sit amet consectetur.</p> -->
                                <div class="clearfix buttons-wrapper">
                                    <!-- <div class="col-xs-5 pad-0">
                                    <div class="custom-checkbox bst_custom_checkbox width-100">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" checked class=""> <span><i class="fa fa-check"></i></span>Remember
                                            Me
                                        </label>
                                    </div>

                                </div> -->
                                    <div class="col-xs-5 pad-0 text-left">
                                        <span class="bg-transparent border-0 pad-0 c-pointer forgot-link f-14 text-dark-blue"
                                            id="forgotPwd" ui-sref="forgotPassword">Forgot password?</span>
                                    </div>
                                    <div class="col-xs-7 pad-0 text-right"> <button type="submit" id="signIn" class="grad-button"  >Sign
                                            In</button>
                                    </div>
                                </div>
                            </form>
                            <!--password recovery form start-->
                            <form class="form" name="resetPwdForm" action="" id="frgtPwdFrm" hidden>
                                <h3 class="text-center text-cream roboto-bold c-arrow  text-capitalize form-name-wrapper">
                                    Password Recovery</h3>
                                <span class="custom-spinner case-dairy-spinner l-0 t-0 hidden" id="loginLoader">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </span>
                                <div class="form-group mar-x0">
                                    <div class="width-100 p-rel bst_input_group">
                                        <div class="input-shadow-wrapper"></div>
                                        <span class="input-group-prepend fa-user fa"></span>
                                        <input type="text" tabindex="1" onfocus="hideerromsg()" placeholder="Username or Email address" class="custom-input height-100" id="existingUserName"
                                            name="existingUserName" />
                                        <span for="username" class="label">Username or Email address</span>
                                    </div>
                                </div>
                                <div class="strike-text mar-b10">
                                    <span class="f-18 roboto-light">or</span>
                                </div>
                                <div class="form-group mar-x0">
                                    <div class="width-100 p-rel bst_input_group">
                                            <div class="input-shadow-wrapper"></div>
                                        <span class="input-group-prepend fa-envelope fa"></span>
                                        <input type="text" id="email" tabindex="2" onfocus="hideerromsg()" placeholder="Email Address" class="custom-input height-100"
                                            name="email" />
                                        <span for="password" class="label">Email Address</span>
                                    </div>
                                </div>

                                <div class="danger-alerts d-flex lh-14 errors" id="forgotErrorBox" hidden>
                                    
                                </div>
                                <div class="success-alerts d-flex lh-14" id="successBox" hidden>

                                </div>
                                <!-- <p class="danger-alerts hidden">Lorem ipsum dolor sit amet consectetur.</p> -->
                                <div class="clearfix pad-0 buttons-wrapper">
                                    <div class=" pad-0 col-xs-7">
                                        <a  tabindex="4" class="bordered-button  d-flex ai-c remembrPwd">I remembered my password!</a>
                                    </div>
                                    <div  class="col-xs-5 pad-0 text-right">
                                        <button tabindex="3"  id="rstPwd" type="submit" disabled class="btn grad-button">Reset Password</button>
                                    </div>
                                </div>
                            </form>
                            <!--password recovery form end-->

                            <!--Thank you form start-->
                            <form class="form pad-20" name="resetPwdForm" action="" hidden id="thankYouForm">
                                <h3 class="text-center text-cream c-arrow  roboto-bold text-capitalize form-name-wrapper">
                                    Thank You</h3>
                                <span class="custom-spinner case-dairy-spinner l-0 t-0 hidden" id="loginLoader">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </span>

                                <p class="roboto-regular thanks-message-wrapper">We've sent password reset instructions
                                    to your email
                                    address. If no email is received within ten minutes, check that the submitted
                                    address is correct.</p>
                                <div class="clearfix pad-y25">
                                    <div class="text-center pad-0">
                                        <button class="bordered-button remembrPwd">Back to Login!</button>
                                    </div>
                                </div>
                            </form>
                            <!--Thank you form start-->
                        </div>
                    </div>
                    <!-- Login Box Ends -->

                    <!-- change password starts -->
                    <div class="col-xs-4 box hidden login-box z-999 pad-0 pull-right dark-black-panels" id="updatePwdSection">
                        <div class="shadow"></div>
                        <div class="shadow-overlay-wrapper">
                            <h3 class="text-center text-cream roboto-bold text-capitalize form-name-wrapper c-arrow">Reset
                                Password</h3>
                            <form class="form" name="loginForm" id="chngPwdFrm">
                                <div class="form-group mar-x0 ">
                                    <div class="width-100 p-rel bst_input_group">
                                            <div class="input-shadow-wrapper"></div>
                                        <span class="input-group-append">
                                            <i id="showpsd_newpw" onclick="changeIcon('showpsd_newpw','newPwd')" class="fa fa-eye"></i>
                                        </span>
                                        <input type="password" placeholder="New Password" id="newPwd" class="custom-input"
                                            name="new_password">
                                        <span for="new_password" class="label">New Password</span>
                                    </div>
                                </div>
                                <div class="form-group mar-x0 mar-y25">
                                    <div class="width-100 p-rel bst_input_group">
                                            <div class="input-shadow-wrapper"></div>
                                        <span class="input-group-append">
                                            <i id="showpsd_cnfpw" onclick="changeIcon('showpsd_cnfpw','cnfPwd')" class="fa fa-eye"></i>
                                        </span>
                                        <input type="password" placeholder="Confirm Password" id="cnfPwd" class="custom-input"
                                            name="new_password">
                                        <span for="confirm_password" class="label">Confirm Password</span>
                                    </div>
                                </div>
                                <div class="success-alerts lh-14 d-flex" id="updateBox" hidden>

                                </div>

                                <div class="danger-alerts errors d-flex lh-14" id="updateErrorBox" hidden>

                                </div>
                                <div class="row mar-x0">
                                    <div class="pull-right pad-0">
                                        <button type="submit" id="updatePwd" class="btn grad-button btn-submit"
                                            disabled="">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- change password Ends -->
                </div>
            </div>
            <!-- Login Wrapper Ends -->
        </div>
        <!-- Login Wrapper Ends -->

    </div>

    <!-- Authentication Wrapper Ends -->
    <script>
        var BoxId = document.getElementById("loginErrorBox");
     

        function changeIcon(iconId, fieldId) {
            if ($("#" + iconId).hasClass('fa-eye')) {
                $('#' + fieldId).attr('type', 'text');
                $("#" + iconId).removeClass('fa-eye');
                $("#" + iconId).addClass('fa-eye-slash');

            } else if ($("#" + iconId).hasClass('fa-eye-slash')) {
                $('#' + fieldId).attr('type', 'password');
                $("#" + iconId).removeClass('fa-eye-slash');
                $("#" + iconId).addClass('fa-eye');
            }
            var data = $('#' + fieldId).val();
            $('#' + fieldId).val('');
            $('#' + fieldId).val(data);

        };

        function closeErrorBox() {
            BoxId.style.display = 'none';
        };

        function closeFrgtErrorBox() {
            $("#forgotErrorBox").css("cssText", "display: none !important;");
        };

        function closeSucessBox() {
            $("#successBox").css("cssText", "display: none !important;");
        };

        function closeUpdateErrorBox() {
            $("#updateErrorBox").css("cssText", "display: none !important;");
        };

        function closeUpdateBox() {
            $("#updateBox").css("cssText", "display: none !important;");
        };
        function closeLoginErrorBox() {
            $("#loginErrorBox").css("cssText", "display: none !important;");
        };
        
        function hideerromsg(){
            $("#forgotErrorBox").css("cssText", "display: none !important;");
        }
        $(function () {

            //for ISRA

            /*var api = "http://172.20.159.11/ehubrest/api/";

            $.get("http://172.20.159.11/ehubrest/api/productVersions/latest", function (data, status) {
            $("#version").html("version " + data.version);
            }); */


            //for local

            var api = window.location.protocol + "//" + window.location.host + "/" + "<%=basePath%>";

            $.get("<%=basePath%>" + "productVersions/latest", function (data, status) {
                $("#version").html("version " + data.version.toUpperCase());
            });


            //for server 7070

            /*
             var api = "http://188.166.40.105:7070/ehubrest/api/";

            $.get("http://188.166.40.105:7070/ehubrest/api/productVersions/latest", function(data, status){
            $("#version").html("version " + data.version);
            }); */


            //for server 8081

            /* 
            var api = "http://188.166.40.105:8081/ehubrest/api/";

            $.get("http://188.166.40.105:8081/ehubrest/api/productVersions/latest", function(data, status){
            $("#version").html("version " + data.version);
            }); */


            //for server 8080

            /* 
            var api = "http://82.196.12.232:8080/ehubrest/api/";
            $.get("http://82.196.12.232:8080/ehubrest/api/productVersions/latest", function(data, status){
            $("#version").html("version " + data.version);
            }); */



            $('#forgotPwd').click(function (event) {
                $('#lgnFrm').css("cssText", "display: none !important;");
                $('#frgtPwdFrm').show();
                $('#existingUserName').val('');
                $('#email').val('');
                event.preventDefault();
            });

            if(location.href.indexOf('login_error') >= 0){
                $('#forgotPwd').click(function (event) {
                        $('#loginErrorBox').css("cssText", "display: none !important;");
                    });
            }

            $(".remembrPwd").click(function (event) {
                $('#lgnFrm').show();
                $('#frgtPwdFrm').css("cssText", "display: none !important;");
                $('#username').val('');
                $('#password').val('');
                $('#successBox').css("cssText", "display: none !important;");
                $('#forgotErrorBox').css("cssText", "display: none !important;");
                event.preventDefault();
                $("#existingUserName").removeAttr('disabled');
                $("#email").removeAttr('disabled');
                $('#rstPwd').attr('disabled', 'disabled');
                $('#thankYouForm').css("cssText", "display: none !important;");
                $('#existingUserName').attr('required', 'required');
                $('#email').attr('required', 'required');
            });


            $("body").on("keyup", "#existingUserName", function () {
                $('#email').attr('required', 'required');
                if ($("#existingUserName").val() != "") {
                    $("#email").val('');
                    $('#email').attr('disabled', 'disabled');
                    $('#email').attr('title', '');
                    $('#rstPwd').removeAttr('disabled');
                } else {
                    $('#email').attr('disabled', false);
                    $('#email').removeAttr('title');
                    $('#rstPwd').attr('disabled', 'disabled');
                }
            });

            $("body").on("keyup", "#email", function () {
                $('#existingUserName').attr('required', 'required');
                if ($("#email").val() != "" && validateEmail($('#email').val())) {
                    $('#rstPwd').removeAttr('disabled');
                    $("#existingUserName").val('');
                    $('#existingUserName').attr('disabled', 'disabled');
                    $('#existingUserName').attr('title', '');
                } else {
                        $('#existingUserName').attr('disabled', false);
                        $('#existingUserName').removeAttr('title');
                        $('#rstPwd').attr('disabled', 'disabled');
                }
            });

            // function to send reset password link email

            $("body").on("click", "#rstPwd", function (event) {
                var data, query;
                if ($("#existingUserName").val() != "") {
                    query = 'username';
                    data = $("#existingUserName").val();
                } else if ($("#email").val() != "") {
                    query = 'emailAddress';
                    data = $("#email").val();
                };
                $('#loginLoader').removeClass('hidden');
                $.post(api + "security/forgotPassword?" + query + '=' + data, function (data, status) {
                    $('#loginLoader').addClass('hidden');
                    $('#successBox').show();
                    $('#successBox').html("<span>"+data.responseMessage+"</span><span class='c-pointer f-18 mar-l25 text-dark-cream as-st' onclick='closeSucessBox()'>&times;</span>");
                    $('#forgotErrorBox').css("cssText", "display: none !important;");
                    setTimeout(function () {
                        $('#successBox').css("cssText", "display: none !important;");
                        $('#lgnFrm').css("cssText", "display: none !important;");
                        $('#thankYouForm').show();
                        $('#frgtPwdFrm').css("cssText", "display: none !important;");
                    }, 3000);
                }).fail(function (err, res) {
                    $('#loginLoader').addClass('hidden');
                    $('#forgotErrorBox').show();
                    $('#forgotErrorBox').html("<span>"+err.responseJSON.responseMessage+"</span><span class='c-pointer f-18 mar-l25 as-st text-dark-cream' onclick='closeFrgtErrorBox()'>&times;</span>");
                    // setTimeout(function () {
                    //     $('#forgotErrorBox').css("cssText", "display: none !important;");
                    // }, 3000);

                });

                event.preventDefault();
            });


            if ($('#newPwd').val() == '' && $('#cnfPwd').val() == '') {
                $('#updatePwd').prop('disabled', 'disabled');
            } else {
                $('#updatePwd').prop('disabled', false);
            };

            $('body').on('keyup', '#newPwd,#cnfPwd', function () {
                if ($('#newPwd').val() == $('#cnfPwd').val() && $('#newPwd').val() != '' &&
                    validatePasswords($('#newPwd').val())) {
                    $('#updatePwd').prop('disabled', false);
                    $('#updateErrorBox').css("cssText", "display: none !important;");
                } else if ($('#newPwd').val() != '' && !validatePasswords($('#newPwd').val())) {
                    $('#updatePwd').prop('disabled', 'disabled');
                    $('#updateErrorBox').show();
                    $('#updateErrorBox').html('Password should be alphanumeric');                   
                }else if ($('#newPwd').val() != '' && validatePasswords($('#newPwd').val())) {                  
                        $('#updateErrorBox').css("cssText", "display: none !important;")   
                        $('#updatePwd').prop('disabled', 'disabled');
                        if ($('#newPwd').val() != '' && $('#cnfPwd').val() != '') {
                            $('#updateErrorBox').show();
                            $('#updateErrorBox').html('Passwords do not match');                      
                        }
                }  else if($('#newPwd').val() == '' && $('#cnfPwd').val() !=''){
                    $('#updatePwd').prop('disabled', 'disabled');
                        $('#updateErrorBox').show();
                        $('#updateErrorBox').html('Passwords cannot be empty'); 
                }
            });


            function validateEmail(mail) {
                var filter =
                    /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(mail)) {
                    return true;
                } else {
                    return false;
                }
            };

            function validatePasswords(pwd) {
                var filter = /^(?=.*\d).{3,20}$/;
                if (filter.test(pwd)) {
                    return true;
                } else {
                    return false;
                }
            }

            if (location.href.indexOf('loginId') >= 0) {

                $("#loginSection").css("cssText", "display: none !important;");
                $("#updatePwdSection").removeClass('hidden')

                var Id = location.href.split('=')[1];

                $('body').on('click', '#updatePwd', function (event) {
                    $.post(api + 'security/resetPassword?userId=' + Id + '&newPassword=' + $('#newPwd')
                        .val() + '&reTypePassword=' + $('#cnfPwd').val(),
                        function (data, status) {
                            $("#updateBox").show();
                            $("#updateBox").html(data.message);
                            if (data.validated) {
                                //add success class to message
                                $("#username").val(data.screenName);
                                $("#password").val($('#newPwd').val());
                                $("#signIn").click();
                            } else {
                                //add danger class to message
                            }

                            setTimeout(function () {
                                $("#updateBox").css("cssText", "display: none !important;");
                            }, 3000);
                        }).fail(function (err, res) {
                        //show error message
                        $('#updateErrorBox').show();
                        $("#updateErrorBox").html("<span>"+err.responseJSON.message+ "</span><span class='c-pointer f-18 as-st mar-l25 text-dark-cream' onclick='closeUpdateErrorBox()'>&times;</span>");
                    });
                    event.preventDefault();
                });
            };

            if (location.href.indexOf('error=could') >= 0 && !(location.href.indexOf('login_error') >= 0) ) {
                $('#lgnFrm').css("cssText", "display: none !important;");
                $('#frgtPwdFrm').show();
                $('#forgotErrorBox').show();
                $("#forgotErrorBox").html("<span> Your temporary password has expired, please create a new one </span><span class='c-pointer f-18 as-st mar-l25 text-dark-cream' onclick='closeFrgtErrorBox()'>&times;</span>");
                // setTimeout(function () {
                //     $("#forgotErrorBox").css("cssText", "display: none !important;");
                // }, 3000);
            };
            //reloading login page after session expired
            if(location.href.indexOf('/#') >= 0){
                window.location.reload();
            }

        });

    </script>
    <script>
     $(".bst_input_group input").change(function() {
	if ($(this).val().trim() != "") {
		$(this).addClass('filled');
	} else {
		$(this).removeClass('filled');
	}
})</script>
</body>

</html>