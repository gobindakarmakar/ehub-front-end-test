<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
<!--  Sub Header Wrapper Ends  -->

        <!--  Main Wrapper Starts  -->
        <div class="main-wrapper">
            <!--  Content Wrapper Starts  -->
            <div class="content-wrapper">
                <div class="activiti-task-wrapper reports-wrapper vacation-request deployment-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">

                                <!--  Aside Wrapper Starts  -->
                                <aside class="aside-wrapper text-uppercase">

                                    <div class="form-group has-success has-feedback">
                                        <input type="text" class="form-control" placeholder="Search"/>
                                        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                    </div>

                                    <div class="sidebar-header">
                                        <h3>Deployments</h3>
                                        <div class="bydate-sorting">  
                                            <div class="dropdown clearfix">
                                               <a href="javascript:void(0);" uib-dropdown-toggle class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											  </a>
												<ul  uib-dropdown-menu class="dropdown-menu" aria-labelledby="dLabel">
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
												</ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel custom-panel panel-info" ng-repeat="deploymentdata in deployment.deploymentData">
                                        <a class="panel-body" href="javascript:void(0);">

                                            <i class="fa fa-cube"></i>
                                            {{deploymentdata.name}}

                                        </a>
                                    </div>
                                </aside>
                                <!--  Aside Wrapper Ends  -->

                            </div>
                            <div class="col-md-9 col-sm-8">

                                <!--  Create New Task Wrapper Starts  -->
                                <div class="create-new-task-wrapper">
                                    <div class="create-new-task-inner-wrapper">
                                        <!--  Notification Panel Starts  -->
                                        <div class="panel custom-panel notification-panel">
                                            <div class="panel-heading">
                                                <div class="notification-header">
                                                    <a href="javascript:void(0);" class="fa fa-cube server-link"></a>
                                                    <h3>Event Calendar</h3>
                                                    <ul class="list-inline">
                                                        <li><i class="fa fa-calendar-o"></i>Deployed One week Ago</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Notification Panel Ends  -->


                                        <!--  Sub Task Panel Starts  -->
                                        <div class="panel custom-panel sub-task-panel">
                                            <div class="vacation-request-content-body">
                                                <div class="row">
                                                    <div class="col-sm-6 br-1 pr-0">
                                                        <h4 class="text-uppercase">Process Definitions</h4>
                                                        <ul class="list-unstyled">
                                                            <li ng-repeat="deploymentdata2 in deployment.deploymentData2">
                                                                <a class="text-uppercase process-defs" href="javascript:void(0);">
                                                                    <i ng-class="{'fa fa-clock-o':deploymentdata2.process}"></i>
                                                                    <span>{{deploymentdata2.process}}</span>
                                                                </a> 
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h4 class="text-uppercase">Resources</h4>
                                                        <ul class="list-unstyled resource-list">
                                                            <li ng-repeat="deploymentdata2 in deployment.deploymentData2">
                                                                <a class="text-lowercase resource" href="javascript:void(0);">
                                                                  {{deploymentdata2.resource}}
                                                                </a> 
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Sub Task Panel Ends  -->

                                    </div>
                                    <!--  Related Content Panel Ends  -->

                                </div>
                                <!--  Create New Task Wrapper Ends  -->

                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <!--  Content Wrapper Ends  -->
        </div>
        <!--  Main Wrapper Ends  -->
    </div>
	