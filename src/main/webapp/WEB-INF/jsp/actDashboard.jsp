<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Main Contents Wrapper Starts  -->
        <div id="act-dashboard" class="main-contents-wrapper">
            <span ng-show="caseDetailsPreloader" class="act-custom-spinner">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span>
            <onboarding visible="showModalOnboarding"></onboarding>
            <upload visible="showModalUpload"></upload>
            <reassign visible="showModalReassign"></reassign>
            <uploadquestionnaire visible="showModalUploadQuestionnaire"></uploadquestionnaire>
            <uploadkyc visible="showModalUploadKYC"></uploadkyc>
            <modal-dialog1 show="dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownDialogShown" width="500px" height="70px">
                <!--         <div class="alert alert-success custom-success-modal"> -->
                <!--             <strong>{{dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownDialogMessage}}</strong> -->
                <!--         </div> -->
            </modal-dialog1>

            <!--  Act Dashboard Wrapper Ends  -->
            <div id="act-dashboard" class="dashboard-wrapper act-dashboard-wrapper">
                <div ng-controller='ActGridsterController'>
                    <div gridster='gridsterConfiguration'>
                        <div id="relatedCaseDivAp" class="widget-content" style="display:none">
                            <div ng-controller="RelatedCasesController" id="actRelatedCasesDivCopy">
                                <span ng-show="relatedCasesPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-briefcase icon-widget"></i>
                                    <span>RELATED CASES</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                    </ul>
                                </div>
                                <div class="widget-content">
                                    <div id="relatedCases-containerCopy">
                                        <table id="relatedCasesCopy" class="relatedCases table table-striped dataTable no-footer table-scroll five-col">
                                            <thead class="relatedCases-header">
                                                <tr>
                                                    <th>CASE NAME</th>
                                                    <th>PROXIMITY</th>
                                                    <th>DR</th>
                                                    <th>IR</th>
                                                    <th>TR</th>
                                                </tr>
                                            </thead>
                                            <tbody class="case-diary-list" id="relatedCasesWidgetCopy">
                                                <tr ng-repeat="caseItem in relatedCasesDataCopy" ng-class-odd="'odd'" ng-class-even="'even'" ng-if="relatedCasesDataCopy.length > 0">
                                                    <td>
                                                        <div class="media-object custom-media-object">
                                                            <div class="media-object-section">
                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Corporate-KYC'">
                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imageBank}}" alt="avatar" />
                                                                </a>
                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Individual-KYC'">
                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imagePerson}}" alt="avatar" />
                                                                </a>
                                                                <p ng-trim="true">{{caseItem.caseName}}</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseseed">
                                                        <span>{{caseItem.proximity}}</span>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.directRisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{caseItem.directRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.directRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.directRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.indirectRisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{caseItem.indirectRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.indirectRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.indirectRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="caseriskprofile">
                                                        <div class="progress">
                                                            <div title="{{caseItem.transactionalRisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{caseItem.transactionalRisk * 100}}%"
                                                                aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.transactionalRisk * 100}}%">
                                                                <span class="sr-only">{{caseItem.transactionalRisk * 100}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr ng-show="relatedCasesDataCopy.length == 0">
                                                    <td colspan="5">
                                                        <p class="no-data">No data found</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul>

                            <!--  Case Timeline Widget Starts  -->
                            <li gridster-item row="0" col="0" size-x="4" size-y="2" ng-controller="CaseTimelineController" id="actCaseTimelineDiv">
                                <span ng-show="caseTimelinePreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>

                                <div class='widget-header' ng-hide="toggleCase" ng-hide="showCaseTimeLine">
                                    <i class="fa fa-line-chart icon-widget"></i>
                                    <span>CASE TIMELINE</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="filtercaseTimeline()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-show="toggleCase">
                                            <a href="javascript:void(0);" ng-click="toggleCase = false">
                                                <i class="fa fa-line-chart"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0);' ng-click="actCaptureDiv('actCaseTimelineDiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0);' ng-click="maximizeDiv('actCaseTimelineDiv')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content" ng-hide="toggleCase" id="caseTimelineWidget">
                                    <div id="caseTimelineChart"></div>
                                </div>
                                <div class='widget-header' ng-show="toggleCase">
                                    <i class="fa fa-tachometer"></i>
                                    <span>YOUR CASE LOG</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);'>
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="toggleCase">
                                            <a href="javascript:void(0);" ng-click="getCaseLog()">
                                                <i class="fa fa-tachometer"></i>
                                            </a>
                                        </li>
                                        <li ng-show="toggleCase">
                                            <a href="javascript:void(0);" ng-click="toggleCase = false">
                                                <i class="fa fa-line-chart"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:actCaptureDiv("actCaseTimelineDiv")'>
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>

                            </li>
                            </ul>
                            </div>
                            <div class="widget-content" ng-show="toggleCase">
                                <div class="row">
                                    <div class="col-sm-12 case-log-container">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-status">STATUS: COMPLETE</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-total-logs">TOTAL LOGS: {{logs.length}}</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-comments">COMMENTS: 12</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-started">STARTED: 12/03/2016</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-health">HEALTH: 1</span>
                                                        <span class="case-log-priority">PRIORITY: 2</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-last-updated">LAST UPDATED: 16/04/2016</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="case-log-by-name">BY AMILY ALBERT</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#latestActivity">LATEST ACTIVITY(07)</a>
                                            </li>
                                            <li>
                                                <a href="#comments">COMMENTS(09)</a>
                                            </li>
                                            <li>
                                                <a href="#uploads">UPLOADS(08)</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="latestActivity" class="tab-pane fade in active">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem" ng-repeat="log in logs">{{log.message}}</li>
                                                </ul>
                                            </div>
                                            <div id="comments" class="tab-pane fade">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                </ul>
                                            </div>
                                            <div id="uploads" class="tab-pane fade">
                                                <ul class="latestActivityList">
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                    <li class="latestActivityListItem"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </li>
                            <!--  Case Timeline Widget Ends  -->

                            <!--  Alerts Widget Starts  -->
                            <li gridster-item row="0" col="4" size-x="2" size-y="1" id="actAlertsDiv">
                                <span ng-show="alertsPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-briefcase icon-widget"></i>
                                    <span>ALERTS</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="filterAlertsData()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0);' ng-click="actCaptureDiv('actAlertsDiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content" id="alertsWidget">
                                    <div id="notifications-wrapper">
                                        <ul class="list-unstyled alerts-list" ng-repeat="alert in alertsData" ng-hide="alertsData.length == 0">
                                            <li class="dataItem">
                                                <div class="clearfix">
                                                    <i class="fa fa-star-o pull-left" aria-hidden="true" ng-if="alert.type == 'Notification'"></i>
                                                    <i class="fa fa-warning pull-left" aria-hidden="true" ng-if="alert.type == 'Alert'"></i>
                                                    <span class="pull-left notification-headline-right">{{alert.label}}</span>
                                                </div>
                                                <p class="notification-content">{{alert.description}}</p>
                                                <p class="notification-content" ng-if="alert.type == 'Notification'">{{alert.datetime | date: 'MM/dd/yyyy'}}</p>
                                                <span class="pull-left notification-headline-right">
                                                    <a href="javascript:void(0);" ng-click="onTransactionIntelligence()" ng-if="alert.type == 'Alert'" target="_blank">Risky Transaction on {{alert.datetime}} Read More</a>
                                                </span>
                                            </li>
                                        </ul>
                                        <ul id="noAlerts" ng-if="alertsData.length == 0">
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <!--  Alerts Widget Ends  -->

                            <!--  Associated Entities Widget Starts  -->
                            <li gridster-item ng-controller="AssociatedEntitiesController" id="actAssociatedEntitiesDiv">
                                <span ng-show="associatedEntitiesPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-code-fork icon-widget"></i>
                                    <span>ASSOCIATED ENTITIES</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href="javascript:void(0);" ng-click="openFilterModalForChart()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="actAssociatedEntitiestoggleGraph">
                                            <a href="javascript:void(0);" ng-click="OnToggleGraph()">
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
                                        </li>
                                        <li ng-show="actAssociatedEntitiestoggleGraph">
                                            <a href="javascript:void(0);" ng-click="actAssociatedEntitiestoggleGraph = false">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" ng-click="actCaptureDiv('actAssociatedEntitiesDiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li ng-show="actAssociatedEntitiestoggleGraph">
                                            <a href="javascript:void(0);" ng-click="maximizeDiv('associatedEntities')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content" ng-show="actAssociatedEntitiestoggleGraph">
                                    <div class="chartContentDiv" id="associatedEntitiesChart"></div>
                                </div>
                                <div class='widget-content' ng-hide="actAssociatedEntitiestoggleGraph">
                                    <div class="entity-table">
                                        <table class="table table-striped table-scroll three-col">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th>Similarity</th>
                                                </tr>
                                            </thead>
                                            <tbody id="associatedEntitiesWidget">
                                                <tr ng-repeat="data in associatedEntitiesDatas" ng-class-odd="'odd'" ng-class-even="'even'">
                                                    <td>{{data.entityName}}</td>
                                                    <td>{{data.date | date: 'MM/dd/yyyy'}}</td>
                                                    <td>{{data.rate}}%</td>
                                                </tr>
                                                <!--  <tr ng-show="associatedEntitiesDatas.length == 0">
                                        	<td colspan="3">
                                        		<p class="no-data">No data found</p>
                                        	</td>
                                        </tr>-->
                                            </tbody>
                                        </table>
                                        <div ng-show="associatedEntitiesDatas.length == 0">
                                            <p class="no-data">NO DATA FOUND</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--  Associated Entities Widget Ends  -->

                            <!--  Risk Ratio Widget Starts  -->
                            <li gridster-item row="2" col="0" size-x="2" size-y="1" ng-controller="RiskRatioController" id="riskRationdiv">
                                <span ng-show="riskRatioPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-heartbeat icon-widget"></i>
                                    <span>RISK RATIO</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="openFilterModalForChart()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="riskRatiotoggleGraph">
                                            <a href="javascript:void(0);" ng-click="ToggleGraphChart()">
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
                                        </li>
                                        <li ng-show="riskRatiotoggleGraph">
                                            <a href="javascript:void(0);" ng-click="UpdateToggle()">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" ng-click="actCaptureDiv('riskRationdiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li ng-show="riskRatiotoggleGraph">
                                            <a href="javascript:void(0);" ng-click="maximizeDiv('riskRationdiv')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content">
                                    <div ng-show="riskRatiotoggleGraph" id="riskRatioBubbleChart" class="risk-ratio-bubble-container"></div>
                                    <div ng-hide="riskRatiotoggleGraph" class="table-responsive">
                                        <table class="table table-striped table-scroll three-col">
                                            <thead>
                                                <tr>
                                                    <th>NAME</th>
                                                    <th>RISK</th>
                                                    <th>RISK LEVEL</th>
                                                </tr>
                                            </thead>
                                            <tbody id="riskRatioWidget">
                                                <tr ng-repeat="riskRatioData in riskRatioDatas" ng-class-odd="'odd'" ng-class-even="'even'">
                                                    <td>{{riskRatioData.name}}</td>
                                                    <td>{{(riskRatioData.size).toFixed(3)}}</td>
                                                    <td>{{riskRatioData.group}}</td>
                                                </tr>
                                                <!-- <tr ng-show="riskRatioDatas.length == 0">
                                        	<td colspan="3">
                                        		<p class="no-data">No data found</p>
                                        	</td>
                                        </tr> -->
                                            </tbody>
                                        </table>
                                        <div ng-show="riskRatioDatas.length == 0">
                                            <p class="no-data">NO DATA FOUND</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--  Risk Ratio Widget Ends  -->

                            <!--  Hot Topics Widget Starts  -->
                            <li gridster-item row="2" col="2" size-x="2" size-y="1" ng-controller="ActhottopicsController" id="acthottopicsdiv">
                                <span ng-show="actHottopicsPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-text-width icon-widget"></i>
                                    <span>HOT TOPICS</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="openFilterModalForChart()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="acthottopicstoggleGraph">
                                            <a href="javascript:void(0);" ng-click="resizeGraph()">
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
                                        </li>
                                        <li ng-show="acthottopicstoggleGraph">
                                            <a href="javascript:void(0);" ng-click="acthottopicstoggleGraph = false">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='javascript:void(0)' ng-click="actCaptureDiv('acthottopicsdiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li ng-show="acthottopicstoggleGraph">
                                            <a href="javascript:void(0)" ng-click="maximizeDiv('acthottopicsdiv')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget-content">
                                    <div ng-show="acthottopicstoggleGraph" id="hottopics"></div>
                                    <div ng-hide="acthottopicstoggleGraph" class="table-responsive" id="hottopicsTable">
                                        <table class="table table-striped table-scroll two-col">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="hotTopicsWidget">
                                                <tr ng-repeat="acthottopicsData in acthottopicsDatas" ng-show="acthottopicsDatas.length > 0" ng-class-odd="'odd'" ng-class-even="'even'">
                                                    <td>{{ acthottopicsData.name}}</td>
                                                    <td>{{ acthottopicsData.count}}</td>
                                                </tr>
                                                <!-- <tr ng-show="acthottopicsDatas.length == 0">
                                        	<td colspan="2">
                                        		<p class="no-data">No Data Found</p>
                                        	</td>
                                        </tr> -->
                                            </tbody>
                                        </table>
                                        <!--                                 <div ng-show="acthottopicsDatas.length == 0"> -->
                                        <!-- 									<p class="no-data">NO DATA FOUND</p> -->
                                        <!-- 								</div> -->
                                    </div>
                                </div>
                            </li>
                            <!--  Hot Topics Widget Ends  -->

                            <!--  Top Locations Widget Starts  -->
                            <li gridster-item row="2" col="4" size-x="2" size-y="1" ng-controller="ActTopLocationsController" id="actTopLocationsdiv">
                                <span ng-show="topLocationsPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-map-marker icon-widget"></i>
                                    <span>TOP LOCATIONS</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li ng-hide="actTopLocationstoggleGraph">
                                            <a href="javascript:void(0);" ng-click="actTopLocationstoggleGraph = true">
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
                                        </li>
                                        <li ng-show="actTopLocationstoggleGraph">
                                            <a href="javascript:void(0);" ng-click="actTopLocationstoggleGraph = false">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" ng-click="actCaptureDiv('actTopLocationsdiv', actTopLocationstoggleGraph)">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="widget-content">
                                    <div ng-show="actTopLocationstoggleGraph" id="mapLeaflet" class="chartContentDiv">
                                        <div class="container-fluid map-info-wrapper" id="mapInfo">
                                            <div class="row">
                                                <div class="col-sm-6" ng-if="case_id != undefined && case_id != null && case_id != ''">
                                                    <span>Total Locations: </span>
                                                    <span class="count">{{totalLocation}}</span>
                                                </div>
                                                <div class="col-sm-6" ng-if="case_id == undefined || case_id == null || case_id == ''">
                                                    <span>Total Locations: </span>
                                                    <span class="count">NA</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="map" style="height: 280px;"></div>
                                    </div>
                                    <div ng-hide="actTopLocationstoggleGraph" class="table-responsive">
                                        <table class="table table-striped table-scroll two-col">
                                            <thead>
                                                <tr>
                                                    <th>COUNTRY</th>
                                                    <th>COUNT</th>
                                                </tr>
                                            </thead>
                                            <tbody id="actTopLocationsTable">
                                                <tr ng-repeat="(location,count) in actworldDatas" ng-class-odd="'odd'" ng-class-even="'even'">
                                                    <td>{{location}}</td>
                                                    <td>{{count}}</td>
                                                </tr>
                                                <tr ng-show="(actworldDatas | json) == '{}'">
                                                    <td colspan="2">
                                                        <p class="no-data">No Data Found</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <!--  Top Locations Widget Ends  -->

                            <!--  Entities In Focus Widget Starts  -->
                            <li gridster-item row="3" col="0" size-x="2" size-y="1" ng-controller="ActEntitiesInFocusController" id="actentitiesInFocusdiv">
                                <span ng-show="actEntitiesInFocusPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class='widget-header'>
                                    <i class="fa fa-crosshairs icon-widget"></i>
                                    <span>ENTITIES IN FOCUS</span>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v ellipsis"></i>
                                    </a>
                                    <ul class="dropdown-menu filter-menu">
                                        <li>
                                            <a href='javascript:void(0);' ng-click="openFilterModalForEntitiesInFocusChart()">
                                                <i class="fa fa-filter"></i>
                                            </a>
                                        </li>
                                        <li ng-show="toggleCase">
                                            <a href="javascript:void(0);" ng-click="ResizeToggleGraph()">
                                                <i class="fa fa-bar-chart"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="toggleCase">
                                            <a href="javascript:void(0);" ng-click="toggleCase = true">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" ng-click="actCaptureDiv('actentitiesInFocusdiv')">
                                                <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                            </a>
                                        </li>
                                        <li ng-hide="toggleCase">
                                            <a href="javascript:void(0);" ng-click="maximizeDiv('actentitiesInFocusdiv')">
                                                <i class="fa fa-arrows rotate-45"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="widget-content" ng-hide="toggleCase">
                                    <div id="actEntitiesInFocusBubbleChart"></div>
                                </div>
                                <div class="widget-content" ng-show="toggleCase">
                                    <div id="entities-container">
                                        <table class="table table-striped table-scroll three-col">
                                            <thead>
                                                <tr>
                                                    <th>NAME</th>
                                                    <th>OCCURANCE</th>
                                                    <th>GROUP</th>
                                                </tr>
                                            </thead>
                                            <tbody id="entitiesInFocusWidget">
                                                <tr ng-repeat="actEntitiesInFocusData in actEntitiesInFocusDatas" ng-class-odd="'odd'" ng-class-even="'even'">
                                                    <td>{{actEntitiesInFocusData.name}}</td>
                                                    <td>{{actEntitiesInFocusData.size}}</td>
                                                    <td>{{actEntitiesInFocusData.group}}</td>
                                                </tr>
                                                <!-- <tr ng-show="actEntitiesInFocusDatas.length == 0">
	                                        <td colspan="3">
	                                        	<p class="no-data">No Data Found</p>
	                                        </td>
                                        </tr> -->
                                            </tbody>
                                        </table>
                                        <div ng-show="actEntitiesInFocusDatas.length == 0">
                                            <p class="no-data">NO DATA FOUND</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--  Entities In Focus Widget Ends  -->
                            <!--  Related Cases Widget Starts  -->
                            <li gridster-item row="3" col="2" size-x="2" size-y="1" ng-controller="RelatedCasesController" id="actRelatedCasesDiv">
                                <div id="casesDiv">
                                    <span ng-show="relatedCasesPreloader" class="custom-spinner case-dairy-spinner">
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                    </span>
                                    <div class='widget-header'>
                                        <i class="fa fa-briefcase icon-widget"></i>
                                        <span>RELATED CASES</span>
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-v ellipsis"></i>
                                        </a>
                                        <ul class="dropdown-menu filter-menu">
                                            <li>
                                                <a href='javascript:void(0);' ng-click="openFilterModalForChart()">
                                                    <i class="fa fa-filter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href='javascript:void(0);' ng-click="actCaptureDiv('actRelatedCasesDiv')">
                                                    <i title="Capture Screen" class="capture-current-portlet icon-camera"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="widget-content">
                                        <div id="relatedCases-container">
                                            <table id="relatedCases" class="relatedCases table table-striped dataTable no-footer table-scroll five-col">
                                                <thead class="relatedCases-header">
                                                    <tr>
                                                        <th>CASE NAME</th>
                                                        <th>PROXIMITY</th>
                                                        <th>DR</th>
                                                        <th>IR</th>
                                                        <th>TR</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="case-diary-list" id="relatedCasesWidget">
                                                    <tr ng-repeat="caseItem in relatedCasesData" ng-class-odd="'odd'" ng-class-even="'even'" ng-if="relatedCasesData.length > 0">
                                                        <td>
                                                            <div class="media-object custom-media-object">
                                                                <div class="media-object-section">
                                                                    <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Corporate-KYC'">
	                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imageBank}}" alt="avatar" />
	                                                                </a>
	                                                                <a href="javascript:void(0);" class="avatar" ng-if="caseItem.type == 'Individual-KYC'">
	                                                                    <img class="img-responsive img-avatar" ng-src="{{caseItem.imagePerson}}" alt="avatar" />
	                                                                </a>
                                                                    <p ng-trim="true">{{caseItem.caseName}}</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="caseseed">
                                                            <span>{{caseItem.proximity}}</span>
                                                        </td>
                                                        <td class="caseriskprofile">
                                                            <div class="progress">
                                                                <div title="{{caseItem.directRisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{caseItem.directRisk * 100}}%"
                                                                    aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.directRisk * 100}}%">
                                                                    <span class="sr-only">{{caseItem.directRisk * 100}}% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="caseriskprofile">
                                                            <div class="progress">
                                                                <div title="{{caseItem.indirectRisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{caseItem.indirectRisk * 100}}%"
                                                                    aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.indirectRisk * 100}}%">
                                                                    <span class="sr-only">{{caseItem.indirectRisk * 100}}% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="caseriskprofile">
                                                            <div class="progress">
                                                                <div title="{{caseItem.transactionalRisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{caseItem.transactionalRisk * 100}}%"
                                                                    aria-valuemin="0" aria-valuemax="100" style="width: {{caseItem.transactionalRisk * 100}}%">
                                                                    <span class="sr-only">{{caseItem.transactionalRisk * 100}}% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr ng-show="relatedCasesData.length == 0 && !relatedCasesPreloader">
                                                        <td colspan="5">
                                                            <p class="no-data">No data found</p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--  Related Cases Widget Ends  -->

                        </ul>
                    </div>
                </div>
            </div>
            <!--  Act Dashboard Wrapper Ends  -->
            <wheel-nav-context-menu contextmenuname="actDashboard.contentMenu" contextmenuname0="actDashboard.contentMenu0" contextmenuname1="actDashboard.contentMenu1"
                contextmenuname2="actDashboard.contentMenu2" contextmenuname3="actDashboard.contentMenu3" contextmenuname4="actDashboard.contentMenu4"
                contextmenuname5="actDashboard.contentMenu5"></wheel-nav-context-menu>
        </div>
        <!--  Main Contents Wrapper Ends  -->

        <!--  Details Modal Starts  -->
        <div id="detailsModal" class="modal fade">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Details</h4>
                    </div>
                    <div class="modal-body">
                        <p id="detailsText"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--  Details Modal Ends  -->

        <!--  Capture Modal Starts  -->
        <div class="modal fade custom-capture-modal" id="actWidgetCapture">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Capture Widget</h4>
                    </div>
                    <div class="modal-body screencapture-modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="actWidgetCapturePreview" style="text-align: center"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="captured-name pull-left" id="actCaptureFilename" type="text" placeholder="File Name">
                        <a class="pull-right" href="javascript:void(0);" id="actDownloadWidgetCapture"> Download </a>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #map {
                position: static !important
            }
        </style>
        <!--  Capture Modal Ends  -->