<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@include file="common/submenu.jsp"%>
		<!--
<span class="custom-spinner showMarketPulseLoader" ng-show="true" >
	<i class="fa fa-spinner fa-spin fa-2x"></i>
</span> -->

		<style>
			.timeline:before {
				top: 0;
				bottom: 0;
				position: absolute;
				content: " ";
				width: 3px;
				background-color: #cccccc;
				left: 50%;
				margin-left: -1.5px;
			}
		</style>
		<!--  Main Wrapper Starts  -->
		<div class="main-wrapper">
			<!--  Content Wrapper Starts  -->
			<div class="content-wrapper">
				<div class="audit-trail-wrapper">
					<div class="audit-trail-content clear-fix">
						<div class="audit-trail-left-col">
							<div class="audit-left-content audit-viewby">
								<!-- 						<h5 class="text-uppercase">View By</h5> -->
								<form>
									<!-- 							<div class="form-group"> -->
									<!-- 							    <select class="form-control text-uppercase" id="searchByType"> -->
									<!-- 								    <option>User</option> -->
									<!-- 								    <option>Company</option> -->
									<!-- 								    <option>Date</option> -->
									<!-- 								    <option>Location</option> -->
									<!-- 								</select> -->
									<!-- 						  	</div> -->
									<!-- 						  	<div class="form-group search-text-icon"> -->
									<!-- 							    <input type="text" placeholder="ANDREW RICHARDS" class="form-control" id="textSearch"> -->
									<!-- 							    <i class="fa fa-search"></i> -->
									<!-- 						  	</div> -->
									<div class="form-group">
										<label>By Activity</label>
										<select class="form-control text-uppercase" ng-model="byActivityType" ng-change="filterByActivity()" id="byActivityType"  >
											<!-- <option>All</option>
											<option>Case</option>
											<option>Entity</option>
											<option>Other</option> -->
											<option ng-repeat="activity in activityTypeLists" ng-value="activity | capitalize">
												{{activity }}
											</option>
										</select>
									</div>
									<!-- 						  	<div class="form-group"> -->
									<!-- 						    	<label>Sort By</label> -->
									<!-- 							    <select class="form-control text-uppercase" id="sortByType"> -->
									<!-- 								    <option>Recent</option> -->
									<!-- 								    <option>Today</option> -->
									<!-- 								    <option>Yesterday</option> -->
									<!-- 								    <option>Last Month</option> -->
									<!-- 								</select> -->
									<!-- 						  	</div> -->
									<!-- 						  	<button type="button" class="btn btn-default text-uppercase">Update</button> -->
								</form>
							</div>
							<div class="audit-left-content audit-searchintimeline">
								<h5 class="text-uppercase">search in timeline</h5>
								<form>
									<div class="form-group search-text-icon">
										<input type="text" placeholder="SEARCH" ng-change="searchTxt.length === 0 ? searchLogs(1,true) : null" ng-model="searchTxt" ng-keyup="$event.keyCode == 13 ? searchLogs(1,true) : null" class="form-control" id="textSearch">
										<button ng-click="searchLogs(1,true)" type="button">
											<i class="fa fa-search"></i> Search</button>
									</div>
								</form>
							</div>
							<div class="audit-left-content p-rel audit-activitygraph">
								<h5 class="text-uppercase">Activity Graph</h5>
								<div class="activitygraph-chart">
									<div id="activityGraphChart"></div>
								</div>
								<div ng-show="LineGraphisloading" >
									<span class="text-dark-red no-data">No Data Found</span>
								</div>
							</div>
							<div class="audit-left-content p-rel audit-activityratio">
								<h5 class="text-uppercase">ActivityData Ratio</h5>
								<div class="activityratio-chart" ng-hide="PieGraphisloading">
									<div id="activityRatioChart"></div>
								</div>
								<div  class="activityratio-chart" ng-show ="PieGraphisloading">
									<span class="text-dark-red no-data">No Data Found</span>
								</div>
							</div>
						</div>
						<div class="audit-trail-right-col">
							<div class="audit-trail-header">
								<ul class="list-inline audit-trail-ul">
									<li class="audit-trail-li">
										<div class="audit-trail-details audit-trail-person-details">
											<div class="audit-person-image">
												<img class="img-responsive" src="assets/images/user-img.png" alt="user-img">
											</div>
											<div class="audit-person-info">
												<h4>{{userDetails.firstName}} {{userDetails.lastName}}</h4>
												<span class="text-uppercase">DOB: {{userDetails.dob | date}}</span>
												<span class="text-uppercase">{{userDetails.country}}</span>
											</div>
										</div>
									</li>
									<!-- 							<li class="audit-trail-li"> -->
									<!-- 								<div class="audit-trail-details audit-trail-associatedwith"> -->
									<!-- 									<span class="text-uppercase text-center">Associated With</span> -->
									<!-- 									<strong class="text-uppercase text-center">OCBC</strong> -->
									<!-- 								</div> -->
									<!-- 							</li> -->
									<li class="audit-trail-li">
										<div class="audit-trail-details audit-trail-associated-persons">
											<div class="riskscoreprogress">
												<div class="risk-progressbar-holder" ng-repeat="data in ratioData">
													<div class="progress-wrapper">
														<div class="inline text-overflow">{{ data.key }}</div>
														<div class="progress">
															<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ data.value }}" aria-valuemin="0" aria-valuemax="100"
															    style="width: {{ data.value }}%">
																<span class="sr-only ng-binding">{{ data.value }}%</span>
															</div>
														</div>
													</div>
												</div>

											</div>
										</div>
									</li>
									<!-- 							<li class="audit-trail-li"> -->
									<!-- 								<div class="audit-trail-details audit-trail-associatedcases"> -->
									<!-- 									<span class="text-uppercase text-center">Associated Cases</span> -->
									<!-- 									<strong class="text-uppercase text-center">19</strong> -->
									<!-- 								</div> -->
									<!-- 							</li> -->
									<li class="audit-trail-li">
										<div class="audit-trail-details audit-trail-contact-details">
											<div class="person-contacts">
												<p class="email">
													<span class="key">Email:</span>
													<span class="value">
														<a class="text-dark-grey2 c-arrow" href="javascript:void(0);">{{userDetails.emailAddress}}</a>
													</span>
												</p>
												<p class="phone">
													<span class="key">Last Login:</span>
													<span class="value">
														<a  class="text-dark-grey2 c-arrow" href="javascript:void(0);">{{userDetails.lastLoginDate | date}}</a>
													</span>
												</p>
											</div>
											<!-- 									<button type="button" class="text-uppercase btn-viewprofile" ng-click="viewProfile()">View Profile</button> -->
										</div>
									</li>
								</ul>
							</div>
							<div class="audit-trail-timeline-section">
								<div class="auditTrailTimeline">
									<div class="audit-select-filter">
										<label class="text-uppercase text-center">View BY :</label>
										<select class="text-uppercase" ng-model="datefilterSelect">
											<option>Days</option>
											<option>Month</option>
											<option>Year</option>

											<!-- 									<option>Hour</option> -->
										</select>
									</div>
									<ul class="list-inline filter-variation-timeline">
										<!-- <li class="filter-variations-list" ng-repeat="item in timeline_date" ng-class="{'active': item == itemActive}" ng-click="filterbytimeline('',item)"> -->
										<!-- This must be there for every variation -->
										<!-- <a href="javascript:void(0);" class="filter-variations-content " ng-class="filterSelectedClass[item]">
												<span href="{{ '#'+ item }}">{{ item }} </span>
											</a>
										</li> -->
										<li class="filter-variations-list" ng-repeat="item in timesline.day" ng-class="{'active': item == itemActive}" ng-click="filterbytimeline('',item)"
										    ng-if="datefilterSelect==='Days'">
											<!-- This must be there for every variation -->
											<a href="javascript:void(0);" class="filter-variations-content ">
												<span href="{{ '#'+ item }}">{{ item }} </span>
											</a>
										</li>
										<li class="filter-variations-list" ng-repeat="item in timesline.month" ng-class="{'active': item == itemActive}" ng-click="filterbytimeline('',item)"
										    ng-if="datefilterSelect==='Month'">
											<!-- This must be there for every variation -->
											<a href="javascript:void(0);" class="filter-variations-content ">
												<span href="{{ '#'+ item }}">{{ item }} </span>
											</a>
										</li>
										<li class="filter-variations-list" ng-repeat="item in timesline.year" ng-class="{'active': item == itemActive}" ng-click="filterbytimeline('',item)"
										    ng-if="datefilterSelect==='Year'">
											<!-- This must be there for every variation -->
											<a href="javascript:void(0);" class="filter-variations-content ">
												<span href="{{ '#'+ item }}">{{ item }} </span>
											</a>
										</li>
										<!-- 								<li class="filter-variations-list">  This must be there for every variation -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content selected-activity"> -->
										<!-- 										<span>Recent</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  This must be there for every variation -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content selected-activity"> -->
										<!-- 										<span>Today</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> handle a class('month-activity'/'year-activity') here to show year and month variation -->
										<!-- 										<span>Nov</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>oct</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Sep</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Aug</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Jul</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Jun</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>May</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Apr</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Mar</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Feb</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Jan</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content year-activity"> -->
										<!-- 										<span>2016</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
										<!-- 								<li class="filter-variations-list">  ng-repeat here -->
										<!-- 									<a href="javascript:void(0);" class="filter-variations-content month-activity"> -->
										<!-- 										<span>Dec</span> -->
										<!-- 									</a> -->
										<!-- 								</li> -->
									</ul>
								</div>
							</div>
							<div class="audit-trail-activity-section">
								<h4 class="text-uppercase text-center">
									Recent Activity
									<span class="custom-spinner header-spinner" ng-show="headerspinner">
										<i class="fa fa-spinner fa-spin fa-2x"></i>
									</span>
								</h4>
								<div id="auditTrailActivity">
									<timeline>
										<timeline-event ng-repeat="event in events" side="">
											<timeline-badge class="{{event.badgeClass}}">
												<i class="glyphicon {{event.badgeIconClass}}" id="{{event.day}}"></i>
											</timeline-badge>
											<timeline-panel class="{{event.badgeClass}}">
												<timeline-heading>
													<h4>{{event.title}}</h4>
													<p class="time-when-wrapper" ng-if="event.timestamp" class="ng-scope" id="{{event.month}}">
														<small class="ng-binding">
															<i class="glyphicon glyphicon-time"></i>
															<time am-time-ago="event.timestamp"></time>
														</small>
													</p>
												</timeline-heading>
												<div class="timeline-body-wrapper " ng-class="event.imgsrc?'custom-image-display':''" id="{{event.year}}">
													<img class="img-responsive" ng-if="event.imgsrc" ng-src="{{event.imgsrc}}">
													<p>{{event.description}}</p>
												</div>
											</timeline-panel>
										</timeline-event>
									</timeline>
									<span class="custom-spinner footer-spinner" ng-show="footerspinner">
										<i class="fa fa-spinner fa-spin fa-2x"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  Content Wrapper Ends  -->
		</div>
		<!--  Main Wrapper Ends  -->

		<script>
			$(document).ready(function () {
				$(".audit-trail-header").mThumbnailScroller({
					axis: "x"
				});
				$('.audit-trail-left-col').mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});

				$('.riskscoreprogress').mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});
				$('#auditTrailActivity').mCustomScrollbar({
					callbacks: {
						onTotalScroll: function () {
							angular.element('#auditTrailActivity').scope().loglist_scroll();
						}
					},
					axis: "y",
					theme: "minimal-dark"
				});
			});
		</script>