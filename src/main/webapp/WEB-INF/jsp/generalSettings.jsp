<style>
		.ng-invalid  { border: 1px solid red!important; }
</style>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<!--  SubMenu Starts  -->
	<%@include file="common/submenu.jsp"%>
		<!--  SubMenu Ends  -->
		<!--  Settings Wrapper Starts  -->
		<span ng-show="settingsloader" class="custom-spinner full-page-spinner ">
			<i class="fa fa-spinner fa-spin fa-2x"></i>
		</span>
		<div class="dashboard-wrapper settings-dashboard general-settings-wrapper bg-process-background">
			<div class=" d-flex settings-wrapper">
				<div class="side-defined p-rel ai-c">
					<!-- Segment Panel Starts -->
					<div class="overflow-a custom-scroll-wrapper height-100">
						
						<!-- <div class="panel-body side-menu-wrapper pad-15px panel-scroll pad-t0" style="position: relative; overflow: visible;">
							<ul id="navPills" class="custom-list item-1 pad-l0 text-uppercase nav-pills nav-stacked">
								<li ng-repeat = "(key,val) in settingsData" ng-click="switchActive(key)" ng-class="{'active': key === indexPosition}">

									<a class="ws-normal dropdown-toggle d-flex ai-c" href="#main_{{$index}}" prevent-default="" scroll-to="main_{{$index}}" style="text-align:left ! important" >
										<span class="fa fa-cog t-0 left-icon f-18"></span>
										{{ key.substr(0, key.indexOf(" ")) }}
										<span class="fa fa-angle-down t-0 right-icon f-18"></span>

									</a>

									<ul class="custom-list item-1" ng-class="key === indexPosition ? 'list-show' : 'list-hide'">
										<li ng-repeat="val1 in val">
											<a href="#sub{{key.split(' ').join('_')}}{{val1.key.split(' ').join('_')}}" prevent-default="" class="f-12" scroll-to="sub{{key.split(' ').join('_')}}{{val1.key.split(' ').join('_')}}">{{val1.key}}</a>
										</li>
										

									</ul>

								</li>
								
							</ul>

						</div> -->
						<uib-accordion class="bst_accordian_wrapper" id="right-navigation" close-others="oneAtATime">
							<div uib-accordion-group class="panel-default" is-open="status.open" ng-repeat = "(key,val) in settingsData" ng-click="switchActive(key)" ng-class="{'active': key === indexPosition}">
							<uib-accordion-heading>
								<a class="ws-normal dropdown-toggle d-flex ai-c" href="#main_{{$index}}" prevent-default="" scroll-to="main_{{$index}}" style="text-align:left ! important" >
									<span class="fa mar-r8 {{(key.split(' ').join('_')).toLowerCase()}} t-0 f-20 left-icon f-18"></span>
									<span class="f-18 roboto-light">{{ key.substr(0, key.indexOf(" ")) }}</span>
									<span class="fa f-20 mar-autol fa-caret-down t-0 right-icon f-18"></span>
								</a>
							</uib-accordion-heading>
							<ul class="custom-list pad-l30 item-1" ng-class="key === indexPosition ? 'list-show' : 'list-hide'">
								<li ng-repeat="val1 in val" class="pad-y10 pad-l5 width-100">
									<a href="#sub{{key.split(' ').join('_')}}{{val1.key.split(' ').join('_')}}" prevent-default="" class="f-14 roboto-light" scroll-to="sub{{key.split(' ').join('_')}}{{val1.key.split(' ').join('_')}}">{{val1.key}}</a>
								</li>
							</ul>
							</div>
						</uib-accordion>
						<!--  Custom Accordion Ends  -->
					</div>
				</div>
				<div class="side-remain">
					<div class="panel mar-0 height-100 pad-15px">
					<!-- <div class="panel-heading">
							<div class="input-group width-100 bst_input_group custom-input-group custom-select-dropdown inner-element">


								<input type="text" maxlength="100" placeholder="SEARCH SETTINGS" class="custom-input border-blue-thin-b width-100"
								 ng-change="searchedSettings()" ng-model="searchSettings">

								<span class=" pull-right input-group-addon1" ng-click="ceriSearchRouting(search_organization_name)" style="cursor:pointer; position: absolute;  z-index: 100;">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div> -->
						<div class="panel-body-wrapper custom-scroll-wrapper panel-scroll" id="scroller" scroll-spy="" data-target="#navPills" data-scroll-offset="80">

							<div class="panel-body mnh-100px  {{(key.split(' ').join('-')).toLowerCase()}}-wrapper"  ng-repeat="(key,val) in settingsData"  id="main_{{$index}}" >

								<h3 class="main-heading mar-b0 setting-name-wrapper">
								<span class="fa mar-r8 {{(key.split(' ').join('_')).toLowerCase()}}"></span>
									{{ key }}
								</h3>
								<div class=" width-100 general-wrapper  pad-x25 pad-y15" >
									<div class="{{val1.key.split(' ').join('_').toLowerCase()}}"  ng-repeat="val1 in val" id="sub{{key.split(' ').join('_')}}{{val1.key.split(' ').join('_')}}">
										<h4 class="sub-heading mar-t15 f-24 text-dark-cream roboto-light width-100 mar-r5 "> {{val1.key }}</h4>
										<div class="custom-select-wrapper d-flex fw-w  clearfix pad-0  setting-select-wrapper {{val1.key.split(' ').join('_').toLowerCase()}}">
											<div class=" select-group width-48 mar-r10 mar-b10 custom-select-dropdown"  ng-repeat="setting in val1.values" 
											ng-class="setting.classType?setting.classType:'bst_input_group '"  >
												
												<select ng-if="setting.options && (setting.name !== 'Questionnaries')" ng-change="updateVal(setting,selectedVal['name' +key.split(' ').join('_')+ '_' + (val1.key.split(' ').join('_')) +'sub' +$index])" ng-model="selectedVal['name' +key.split(' ').join('_') + '_' + (val1.key.split(' ').join('_')) +'sub' + $index]" class="custom-input">
													<option ng-repeat="opt in setting.options" value="{{$index}}">{{ opt.attributeName }}</option>
	
												</select>

												<select ng-if="setting.options && (setting.name == 'Questionnaries') && (selectedVal['nameGeneral_SettingssubAllowchecklist'])" ng-change="updateVal(setting,selectedVal['name' +key.split(' ').join('_')+ '_' + (val1.key.split(' ').join('_'))+'sub' +($index)],'','','Questionnaries')" ng-model="selectedVal['name' +key.split(' ').join('_') + '_' + (val1.key.split(' ').join('_')) +'sub'+  ($index)]" class="custom-input">
													
													<option ng-repeat="opt in setting.options" value="{{$index}}">{{ opt.attributeName }}</option>
	
												</select>
												<a class="fa fa-plus c-pointer f-24 text-cream p-abs b-0 pull-right" ng-class="setting.secondaryClassType?setting.secondaryClassType:' '" ng-click="addRow();" ng-if="setting.name === 'Predefined answers for :'"></a>
												<div class="section custom-data-table-wrapper pad-25 p-rel" ng-if="setting.name === 'Predefined answers for :'">
													<table class="table table-scroll scroll-table table-striped-z full-width three-col-settings">
														<thead>
															<tr>
																<th>Answer Title</th>
																<th>Answer Body</th>
																<th></th>
															</tr>
														</thead>
														<tbody class="mxh-250 scroll-tbody">
															<tr ng-repeat="list in answerToshowInTable">
																<td class="word-bw ws-pl" ng-if="list.disableInput">{{list.code}}</td>
																<td class="word-bw ws-pl" ng-if="!list.disableInput">
																	<div class="input-group width-90 custom-input-group custom-select-dropdown inner-element mar-b0"
																		ng-class="{'c-ban':list.disableInput}">
																		<input type="text" maxlength="50" class="custom-input border-blue-thin-b width-100"
																			ng-model="list.code" ng-class="{'pe-none':list.disableInput}"
																			ng-diabled="list.disableInput">
																		<!-- <input ng-if="selectedValueAsperList =='Jurisdictions'" type="text" autocomplete="off" ng-model="list.displayName" placeholder="Jurisdiction Name" uib-typeahead="state as state.jurisdictionOriginalName for state in jurisdictionList | filter:{name:$viewValue}" typeahead-template-url="customTemplateSettings.html" class="custom-input pad-y0 pad-x15 mar-t0 bg-transparent form-control typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="onChangeJurisdictionType($label,$item)" ng-diabled="list.disableInput" ng-class="{'pe-none':list.disableInput}"> -->
																	</div>
																</td>
																<td class="word-bw ws-pl" ng-if="list.disableInput">{{list.displayName}}</td>
																<td class="word-bw ws-pl" style="overflow: visible" ng-if="!list.disableInput">
																	<div class="input-group width-90 custom-input-group custom-select-dropdown inner-element mar-b0"
																		ng-class="{'c-ban':list.disableInput}">
																		<input type="text" maxlength="200" class="custom-input border-blue-thin-b width-100"
																			ng-model="list.displayName" ng-class="{'pe-none':list.disableInput}"
																			ng-diabled="list.disableInput">
																		<!-- <input ng-if="selectedValueAsperList =='Jurisdictions'" type="text" autocomplete="off" ng-model="list.displayName" placeholder="Jurisdiction Name" uib-typeahead="state as state.jurisdictionOriginalName for state in jurisdictionList | filter:{name:$viewValue}" typeahead-template-url="customTemplateSettings.html" class="custom-input pad-y0 pad-x15 mar-t0 bg-transparent form-control typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="onChangeJurisdictionType($label,$item)" ng-diabled="list.disableInput" ng-class="{'pe-none':list.disableInput}"> -->
																	</div>
																</td>
																<td>
																	<!-- <span ng-if="!list.flagName" class="fa c-pointer fa-{{list.icon?list.icon:'ban'}}"
																		ng-style="{'color': setColor(list.colorCode)}" uib-popover-template="'SettingsIcons.html'"
																		popover-placement="bottom-right" popover-trigger="'outsideClick'"
																		popover-class="bottom-pop setting-icons-popover" popover-append-to-body="true"
																		popover-is-open="isOpen" ng-click="handlingApi($index,list)"></span>
																	<span ng-if="list.flagName"><img src="{{list.flagPath}}" class="square-12"
																			uib-popover-template="'SettingsIcons.html'" popover-placement="bottom-right"
																			popover-trigger="'outsideClick'" popover-class="bottom-pop setting-icons-popover"
																			popover-append-to-body="true" popover-is-open="isOpen"
																			ng-click="handlingApi($index,list)"></span> -->
																	<span class="fa mar-l10  mar-l15 fa-trash c-pointer text-coral-red" ng-click="deleteRow(list);"
																		ng-class="{'pe-none':!list.allowDelete}" ng-disabled="!list.allowDelete"></span>
																	<span ng-if="list.disableInput"><i class="fa fa-pencil f-12 mar-l15 text-dark-cream c-pointer"
																			ng-click="editUpdateListInTable($index,list,'AnswerTable');"></i></span>
																	<span ng-if="!list.disableInput"><i class="fa fa-check f-12 mar-l15 text-dark-cream c-pointer"
																			ng-click="saveEditUpdateListInTable($index,list,'AnswerTable');"></i></span>
																	<span ng-if="!list.disableInput"><i class="fa fa-times  f-12 mar-l15 text-dark-cream c-pointer"
																			ng-click="cancelEditUpdateListInTable($index,'AnswerTable');"></i></span>
																</td>
															</tr>
														</tbody>
													</table>
													   <span class="fa fa-plus  text-cream c-pointer f-24 p-abs b-0 r-10" ng-click="addRowInTable('AnswerTable');"></span>
												    </div>
													<label  class="switch mar-l25 custom-switch-wrapper filters-switch-wrapper bst-switch-wrapper" ng-if="val1.key !== 'Branding' && (!setting.type || setting.type== 'Toggle On/Off')">
														<input ng-click="updateVal(setting,'',hyperCheck_+setting.name.split(' ').join('_'))" id="hyperCheck_{{setting.name.split(' ').join('_')}}" type="checkbox" ng-model="selectedVal['name' +key.split(' ').join('_')+'sub'+ setting.name.split(' ').join('')]" ng-checked="setting.selectedValue == 'On'?true:false"
														 class="custom-switch-checkbox" />
														<span class="slider round">
														</span>
													</label>
													<!-- ng-if="val1.key !== '(setting.type || setting.type== 'slider')" --><!-- slider -->
													<div class="side-details-wrapper custom-range-wrapper entity-range-bar " ng-show="setting.type == 'Slider'">
														<div class="color-input-wrapper" id="">
															<div class="row">
																<div class="col-sm-3 text-dark-cream pad-b5 roboto-light pull-left">0
													
																</div>
																<div class="col-sm-3 text-dark-cream text-right pad-b5 roboto-light pull-right">
																	{{setting.selectedValueMax}}
													
																</div>
																<div class="col-sm-12">
																	<div id="{{setting.splitName}}" class="custom-range-slider blue round-thumb-slider customer-slider"></div>
																</div>
													
															</div>
														</div>
													</div>
													<div class="col-sm-12 pad-x0 d-flex" ng-if="val1.key === 'Branding'">
														<div class="" id="customer_logo">
															<img alt="No Logo" ng-src="{{'data:image/png;base64,'+(setting.customerLogoImage || setting.defaultValue)}}" style=""/>
														</div>
														<div class="pad-10">
															<p class="f-16 mar-b5">{{setting.name}} </p>
															<p class="roboto-light">Recommended to use PNG ,JPEG or SVG file formats with 70px X 320px resolution</p>
															<button class="grad-button sm-btns" ng-click="openUploadCustomerLogoModal(setting)"> Upload</button>
															<button class="bordered-button sm-btns" ng-click="removeCustomerLogo(setting)"> Remove</button>
														</div>
													</div>
													
														 <input  ng-if="setting.type== 'Text'&& setting.name =='Port'" type="text" ng-blur="updateVal(setting)" class="custom-input "
                                                                                                    ng-model="setting.selectedValue"    ng-pattern='/^([0-5]?[0-9]?[0-9]?[0-9]?[0-9]|6[0-4][0-9][0-9][0-9]|65[0-4][0-9][0-9]|655[0-2][0-9]|6553[0-5])$/'                                                                                            
                                                                                                />
                                                         <input  ng-if="setting.type== 'Text'&& setting.name =='Outbound SMTP Server'" type="text" ng-blur="updateVal(setting)"  class="custom-input "
                                                                                                    ng-model="setting.selectedValue"   maxlength = "{{setting.maximumLength}}"                                                                                                 
                                                                                                /> 
                                                          <input  ng-if="setting.type== 'Text'&& setting.name !='Port'&& setting.name !='Outbound SMTP Server'" type="text" ng-blur="updateVal(setting)" class="custom-input "
                                                                                                    ng-model="setting.selectedValue"   maxlength = "{{setting.maximumLength}}"                                                                                              
                                                                                                />                                                                             
                                                         <input  ng-if="setting.type== 'Password'" type="password" ng-blur="updateVal(setting)" class="custom-input "
                                                                                                    ng-model="setting.selectedValue" maxlength = "{{setting.maximumLength}}"                                                                                                    
                                                                                                />                                     
													  <input  ng-if="setting.type== 'Email'" type="email" ng-blur="updateVal(setting,'','','email')" class="custom-input "
                                                                                                    ng-model="setting.selectedValue"   maxlength = "{{setting.maximumLength}}"                                                                                                  
                                                                                               />   
																							   <span ng-if="(setting.name !== 'Questionnaries' && val1.key !== 'Branding')" class="label"><i ng-if="(setting.name !== 'Questionnaries' && val1.key !== 'Branding')" class="mar-r5 fa fa-{{setting.iconClass}}"></i>{{setting.name}} </span>
																							   <span ng-if="(setting.name == 'Questionnaries') && (selectedVal['nameGeneral_SettingssubAllowchecklist'])" class="label">{{setting.name}} </span>
																									 
													<div ng-show="setting.validationFail" style="margin-left:20%"> <span style="color:Red" class="mar-l25">Please Enter Valid Email</span></div>
											</div>
										</div>
									</div>
								</div>
								


															<!-- listmangement -->
															<div class="section custom-data-table-wrapper pad-25 p-rel" ng-if="key === 'List Management Settings'">
																<table class="table table-scroll scroll-table table-striped-z full-width three-col-settings">
															   <thead> 
																   <tr>
																	 <th>Code</th>
																	 <th>Display Name</th>
																	 <th>Icon</th>
																   </tr>
															  </thead>
																 <tbody class="mxh-250 scroll-tbody">
																 <tr ng-repeat="list in dataToShowAsperList">
																	 <td class="word-bw ws-pl" ng-if="list.disableInput">{{list.code}}</td>
																	 <td class="word-bw ws-pl" ng-if="!list.disableInput">
																			 <div class="input-group width-90 custom-input-group custom-select-dropdown inner-element mar-b0" ng-class="{'c-ban':list.disableInput}">
																					 <input  type="text" maxlength="50" class="custom-input border-blue-thin-b width-100" ng-model="list.code" ng-class="{'pe-none':list.disableInput}" ng-diabled="list.disableInput">
																					 <!-- <input ng-if="selectedValueAsperList =='Jurisdictions'" type="text" autocomplete="off" ng-model="list.displayName" placeholder="Jurisdiction Name" uib-typeahead="state as state.jurisdictionOriginalName for state in jurisdictionList | filter:{name:$viewValue}" typeahead-template-url="customTemplateSettings.html" class="custom-input pad-y0 pad-x15 mar-t0 bg-transparent form-control typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="onChangeJurisdictionType($label,$item)" ng-diabled="list.disableInput" ng-class="{'pe-none':list.disableInput}"> -->
																			 </div>
																	 </td>
																	 <td class="word-bw ws-pl" ng-if="list.disableInput">{{list.displayName}}</td>
																	 <td  class="word-bw ws-pl" style="overflow: visible" ng-if="!list.disableInput">
																		 <div class="input-group width-90 custom-input-group custom-select-dropdown inner-element mar-b0" ng-class="{'c-ban':list.disableInput}">
																			 <input  type="text" maxlength="200" class="custom-input border-blue-thin-b width-100" ng-model="list.displayName" ng-class="{'pe-none':list.disableInput}" ng-diabled="list.disableInput">
																			 <!-- <input ng-if="selectedValueAsperList =='Jurisdictions'" type="text" autocomplete="off" ng-model="list.displayName" placeholder="Jurisdiction Name" uib-typeahead="state as state.jurisdictionOriginalName for state in jurisdictionList | filter:{name:$viewValue}" typeahead-template-url="customTemplateSettings.html" class="custom-input pad-y0 pad-x15 mar-t0 bg-transparent form-control typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="onChangeJurisdictionType($label,$item)" ng-diabled="list.disableInput" ng-class="{'pe-none':list.disableInput}"> -->
																		 </div>
																	 </td>
																	 <td><span  ng-if="!list.flagName" class = "fa c-pointer fa-{{list.icon?list.icon:'ban'}}" ng-style ="{'color': setColor(list.colorCode)}" uib-popover-template="'SettingsIcons.html'" popover-placement="bottom-right"  popover-trigger="'outsideClick'" popover-class="bottom-pop-wrapper setting-icons-popover" popover-append-to-body="true" popover-is-open="isOpen" ng-click="handlingPopUpFlagColorIcon($index,list)"></span>
																		<span ng-if="list.flagName"><img  src="{{list.flagPath}}" class="square-12" uib-popover-template="'SettingsIcons.html'" popover-placement="bottom-right"  popover-trigger="'outsideClick'" popover-class="bottom-pop setting-icons-popover" popover-append-to-body="true" popover-is-open="isOpen" ng-click="handlingPopUpFlagColorIcon($index,list)"></span>
																		 <span class = "fa mar-l10  mar-l15 fa-trash c-pointer text-coral-red" ng-click="deleteRow(list);" ng-class="{'pe-none':!list.allowDelete}" ng-disabled="!list.allowDelete"></span>
																		 <span ng-if="list.disableInput"><i class="fa fa-pencil f-12 mar-l15 text-dark-cream c-pointer" ng-click="editUpdateListInTable($index,list,'ListTable');"></i></span>
																		 <span ng-if="!list.disableInput"><i class="fa fa-check f-12 mar-l15 text-dark-cream c-pointer" ng-click="saveEditUpdateListInTable($index,list,'ListTable');"></i></span>
																		 <span ng-if="!list.disableInput"><i class="fa fa-times  f-12 mar-l15 text-dark-cream c-pointer" ng-click="cancelEditUpdateListInTable($index,'ListTable');"></i></span>
																	 </td>
																 </tr>
																 </tbody>
															 </table>
															 <span class="fa fa-plus text-cream c-pointer f-24 p-abs b-0 r-10" ng-click="addRowInTable('ListTable');"></span>
														 </div>

							</div>
					
							<div ng-if="isSettingsError " class="no-data-wrapper text-center">Data Not Found</div>
								
									
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<script type="text/ng-template" id="uploadCustomerLogo.html">
			<div>
				<div class="modal-header pad-b10 d-flex ai-c">
					<h4 class="modal-title mar-autor  text-overflow">				 
							<span id="articleHeader">Company Logo <span>					   
					</h4>
					<button type="button" class="f-24 bg-transparent border-0 text-cream" ng-click="closeUploadCustomerLogoModal()" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body height-100 pad-t15">
					<div class="clearfix height-100 custom-upload-wrapper " id="imageDiv" style="display: flex;justify-content: center;border: 1px dashed #545454;border-radius: 10px;padding: 15px 0;">
						<div>
							<div class="dropzone" file-dropzone="[image/png, image/jpeg, image/svg]" file="image" file-name="imageFileName" readfile="uploadme.src" data-max-file-size="4" closemodal = "closeUploadCustomerLogoModal()">
								<i class="fa fa-cloud-upload text-cream f-40" style="display: flex;justify-content: center;"></i>
								<h1 class="text-cream">Drag your Company Logo here</h1>
								<p style="margin:auto;text-align:center;">Recommended to use PNG ,JPEG or SVG file formats with 70px X 320px resolution</p><br/>
								<p style="margin:auto;text-align:center;margin-bottom: 15px;"> ---- or ----</p>
							</div>
							<div class="custom-upload-button-wrapper">
								<button class="grad-button">Choose file</button>
								<input type="file"  class="grad-button custom-upload-button" customerfileread="uploadme.src" closemodal = "closeUploadCustomerLogoModal()"/>  
							</div>							
						</div>
					</div>
				</div>
			</div>
		</script>
		<script>
			$(document).ready(function () {
			$(".panel .panel-body-wrappe, .scroll-tbody").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});});
		</script>
		<!-- DELETE MODAL -->
		<script type="text/ng-template" id="deleteFromTable.html">
			<div class="modal-header d-none">
				<button type="button" class="close text-cream" ng-click="closeDataPopUp()" data-dismiss="modal" aria-hidden="true"><span>&#x2716;</span></button>
				<h4 class="modal-title" id="mainTitle">Delete Template</h4>
			 </div>
			 <div class="modal-body pad-x15">
				 <div class="row">
					 <div class="col-sm-12">
						 <p id="mainData" class="text-left text-cream">
							Are you sure you wish to delete this item?
						 </p>
					 </div>
					 <div class="btn-group width-100 text-right">
							 <button class="grad-button sm-btns" type="button" ng-click="deleteItemFromList()">Yes</button>
							 <button class="bordered-button sm-btns" type="button" ng-click="closeDataPopUp()">No</button>
					 </div>	
				 </div>
			 </div>
		</script>
		<!-- ADD MODAL -->
		<script type="text/ng-template" id="AddItemToTable.html">
			
			 <div class="modal-body pad-x15">
				 <div class="row">
					 <div class="col-sm-12 pad-x0">
							<div class="width-100 height-100 p-rel d-block pad-b30 settings-content-wrapper mxh-300 overflow-a custom-scroll-wrapper">
									<form class="form jc-sb pad-x0" ng-submit="applySourceEvidenceEntityClipBoard(form)" name="form">
										<div class="bst_input_group mar-b10 p-rel ">	
											<input  type="text" maxlength="50" placeholder="d-code" class="custom-input width-100" ng-model="codeTodisplay">
											<span class="label">Display Code</span>
										</div>
											<div class="bst_input_group p-rel mar-b10 ">
												<input  type="text" maxlength="200" placeholder="d-name" class="custom-input width-100" ng-model="addDisplayName">
												<span class="label">Display Name</span>
										</div>
												<!-- <div class="input-group custom-input-group d-ib mar-b0 jurisdiction-drop custom-select-dropdown width-100 pad-left" style="position: relative;">
													<input ng-if="selectedValueAsperList =='Jurisdictions'" type="text" autocomplete="off" ng-model="settingsCustomSelect" placeholder="Jurisdiction Name" uib-typeahead="state as state.jurisdictionOriginalName for state in jurisdictionList | filter:{name:$viewValue}" typeahead-template-url="customTemplateSettings.html" class="custom-input pad-y0 pad-x15 mar-t0  mar-b10 bg-transparent form-control typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="onAddChangeJurisdictionType($item)" >
												</div> -->
									</form>
									<h3 class="f-14 roboto-regular text-cream mar-b0">Choose Icon</h3>
										<form class="form jc-sb pad-x0" ng-submit="applySourceEvidenceEntityClipBoard(form)" name="form">
											<div class="input-group mar-b5 pad-l5 width-100 custom-input-group custom-select-dropdown inner-element">
																			<!-- <input type="text" maxlength="100" class="custom-input border-blue-thin-b width-100">
																	<span class=" pull-right r-10 t-5 input-group-addon1"  style="cursor:pointer; position: absolute;  z-index: 100;">
																<i class="fa fa-search"></i>
															</span> -->
																	</div>
											
										</form>
										<ul id="myDIV" class="custom-list pad-l0 mxh-140 overflow-a custom-scroll-wrapper mar-b15 d-flex fw-w settings-icons-list">
											<li class="mar-5 list-icon" ng-repeat="icon in iconList">
												<div  ng-click="addIcon(icon.name)" class="icon-wrapper d-flex fd-c ai-c icons-btn-wrapper" ng-class="{'active' : nameOfIcon === icon.name}">
													<span class="icon f-18"><i class="fa fa-{{icon.name}}"></i></span>
													<span class="icon-name ws-pl word-bw f-10 lh-1 mar-t5">{{icon.name}}</span>
												</div>
											</li>
											
										</ul>
										<h3 class="f-16 roboto-regular text-cream mar-b10">Choose Flag</h3>
										<div class="bst_input_group p-rel mar-b10">
										<input type='text' ng-model='searchtxt.jurisdictionName' placeholder="Choose Flag" class=" width-100 custom-input" placeholder="Search">	
									</div>
									<ul class="custom-list pad-l0 settings-icons-list mxh-100 overflow-a custom-scroll-wrapper d-block">
												<li class="mar-5 pad-5" ng-repeat="country in jurisdictionList | filter : searchtxt " >		
													<div class="flag-name-wrapper fd-c ai-c d-flex" ng-click="addCountryFlag(country.key,$index)" ng-class="{'active' : nameOfFlag === country.key}">
														<!-- <span  ng-click="addCountryFlag(country.country)" class="square-25 radius-5 check-btn" ng-class="{'activeCheck' : addColorIconObj.obj.colorCode === color.colorCode}" ng-style="{'background-color':setColor(color.colorCode)}"></span> -->
														<!-- <span class="icon-name ws-pl word-bw f-14 mar-t5">{{color.color}}</span> -->
														<span class="flag-wrapper"><img  src="{{country.flag}}" class="square-18"></span>
														<span class="flag-name ws-pl word-bw f-10 lh-1 mar-t5">{{country.key}}</span>
													</div>
												</li>	
																
											</ul>
										<h3 class="f-16 roboto-regular text-cream mar-b10">Choose Color</h3>
										
										<ul class="custom-list pad-l0 mxh-100 overflow-a custom-scroll-wrapper d-flex icon-color-wrapper fw-w settings-icons-list">			
											<li class="mar-5" ng-repeat="color in colorList">		
												<div class="color-name-wrapper d-flex fd-c ai-c ">
													<span  ng-click="addColor(color.colorCode)" class="square-25 radius-5 check-btn" ng-class="{'activeCheck' : nameOfColor === color.colorCode}" ng-style="{'background-color':setColor(color.colorCode)}"></span>
													<!-- <span class="icon-name ws-pl word-bw f-14 mar-t5">{{color.color}}</span> -->
												</div>
											</li>	
															
										</ul>
									</div>
					 </div>
					 <div class="btn-group width-100 d-flex jc-fe">
							 <button class=" grad-button sm-btns mar-r10" type="button" ng-click="addItemToTable(addDisplayName,codeTodisplay)" ng-disabled="!codeTodisplay" >ADD</button>
							 <button class="bordered-button sm-btns mar-r10" type="button" ng-click="closeAddDataPopUp()">CANCEL</button>
					 </div>	
				 </div>
			 </div>
		</script>
		<script type="text/ng-template" id="customTemplateSettings.html">
			<a>
				<img ng-src="{{match.model.flag}}" width="16">
				<span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
			</a>
		</script>
		<!-- <script type="text/ng-template" id="AddItemToTable.html">
			
			<div class="modal-body pad-x15">
				<div class="row">
					<div class="col-sm-12">
						   <form class="form jc-sb pad-x0" ng-submit="applySourceEvidenceEntityClipBoard(form)" name="form">
								   <div class="bst_input_group p-rel bst_input_group_r  height-a">
									   <input class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10" ng-model="dataPopObjectEntityClipBoard.selectedSourceTitle"/>
									   <span class="label f-12 text-dark-cream">Display Name</span>
								   </div>
						   </form>
					</div>
					<div class="btn-group width-100 text-right">
							<button type="submit" value="Submit" class="grad-button">Apply</button>
							<button class="bordered-button" type="button" ng-click="closeDataPopUp()">Cancel</button>
					</div>	
				</div>
			</div>
	   </script> -->