<!--  Link Analysis -->
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<span ng-show="caseDetailsPreloader" class="act-custom-spinner"> <i class="fa fa-spinner fa-spin fa-2x"></i>
</span>

<%@include file="common/submenu.jsp"%>

<!--  VLA Main Wrapper Starts  -->
<div class="vla-body vla-main-wrapper">

	<!--  Custom VLA Starts  -->
	<div class="custom-vla sidebar-open clearfix customFixes">

		<!--  Custom Spinner Starts  -->
		<span class="custom-spinner full-custom-spinner">
		 <i class="fa fa-spinner fa-spin fa-2x"></i>
		</span>
		<!--  Custom Spinner Ends  -->
		
		<!--  Header Wrapper Starts  -->
		<div class="pull-left header-wrapper actLinkAnalysis">
		
			<!--  Side Bar Wrapper Starts  -->
			<div id="mainsidewrapper" class="bs-theme mainsidewrapper sidebar-wrapper-main" style="max-height: 424px;">
				<aside class="bs-theme navbar-default navbar-static-side1">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="bs-theme panel panel-default" id="basicInformation">
							<div class="panel-heading" role="tab" id="headingOne" ng-click="model.onBasicInfo($event);">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion1" aria-expanded="true" aria-controls="collapseOne" ng-class="{'collapsed': model.showBasicInfo ? true : false}">
									 	<span>BASIC INFORMATION</span>
									 	<span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingOne" ng-show="model.showBasicInfo">
								<div class="panel-body">
									<div class="basic-header">
										<span>{{entityName}}</span>
									</div>
									<ul class="clearfix basicInfo">
									</ul>
									<div class="basic-content">
										<p></p>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default" id="displayFeatures_div_node" ng-click="model.onNode();">
							<div class="panel-heading" role="tab" id="headingSix">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion2" aria-expanded="false" aria-controls="collapseSix" ng-class="{'collapsed': model.showOnNode ? true : false}">
										 NODE <span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseSix" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingSix" ng-show="model.showOnNode">
								<div class="panel-body">
									<div class="node-styles-panel appearence-styles"></div>
								</div>
							</div>
						</div>

						<div class="panel panel-default" id="displayFeatures_div_edge" ng-click="model.onedge();">
							<div class="panel-heading" role="tab" id="headingSeven">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion3" aria-expanded="false" aria-controls="collapseSeven" ng-class="{'collapsed': model.showOnEdge ? true : false}">
										 <span>EDGE</span>
										 <span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseSeven" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingSeven" ng-show="model.showOnEdge">
								<div class="panel-body">
									<div class="edge-styles-panel  appearence-styles"></div>
								</div>
							</div>
						</div>

						<!--                        <div class="bs-theme panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                FILTER
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class=" bs-theme panel-body">
                                                            <h5>BY ENTITIES</h5>
                                                            <ul class="list-inline custom-img-icons clearfix">
                                                                <li>
                                                                    <a class="sidebar-2 all" href="#"><span>All</span></a>
                                                                </li>
                                                                <li>
                                                                    <a class="sidebar-2 bc" href="#"><img class="" src="KeylineICONS/briefcase.png"  alt="img"></a>
                                                                </li>
                                                                <li>
                                                                    <a class="sidebar-2 user" href="#"><img class="user" src="KeylineICONS/user.png" alt="img"></a>
                                                                </li>
                                                                <li>
                                                                    <a class="sidebar-2 mm" href="#"><img class="mm" src="KeylineICONS/map-marker.png" alt="img"></a>
                                                                </li>
                                                                <li>
                                                                    <a class="sidebar-2 ie" href="#"><img class="ie" src="KeylineICONS/internet-explorer.png" alt="img"></a>
                                                                </li>
                                                            </ul>
                                                            <h5>BY HEIRARCHY</h5>
                                                            <ul class="list-inline sidebar-2">
                                                                <li><a href="#">1ST</a></li>
                                                                <li><a href="#">2ND</a></li>
                                                                <li><a href="#">3RD</a></li>
                                                                <li><a href="#">BY</a></li>
                                                            </ul>
                        
                                                            <div id="slider">
                        
                                                            </div>
                                                            <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-tooltip="hide" data-slider-step="1" data-slider-value="14"/>
                                                            <h5>BY PROXIMITY</h5>
                                                            <ul class="list-inline sidebar-2">
                                                                <li><a href="#">Low</a></li>
                                                                <li><a href="#">Average</a></li>
                                                                <li><a href="#">High</a></li>
                                                            </ul>
                                                            <div id="slider1"></div>
                                                            <input id="ex2" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-tooltip="hide" data-slider-step="1" data-slider-value="14"/>
                                                        </div>
                                                    </div>
                                                </div>-->

						<div class="panel panel-default" id="displayadditionalDetails" ng-click="model.onadditionalDetails();">
							<div class="panel-heading" role="tab" id="headingEight">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion8" aria-expanded="false" aria-controls="collapseEight" ng-class="{'collapsed': model.showAdditionalDetails ? true : false}">
										 <span>ADDITIONAL DETAILS</span>
										 <span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseEight" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingEight" ng-show="model.showAdditionalDetails">
								<div class="panel-body">
									<div id="entityDetails"></div>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default" id="displayTagCloud">
							<div class="panel-heading" role="tab" id="headingFive" ng-click="model.onTagCloud();">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion5" aria-expanded="true" aria-controls="collapseFive" ng-class="{'collapsed': model.showTagCloud ? true : false}">
									  <span>TAGCLOUD</span>
									  <span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingFive" ng-show="model.showTagCloud">
								<div class="panel-body">
									<div id="tagCloud_div"></div>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree" ng-click="model.onlocation();">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" aria-controls="collapseThree" ng-class="{'collapsed': model.showlocation ? true : false}">
									 <span>LOCATION</span>
									 <span class="toggle-sidebar-panel"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse custom-slide-collapse in" role="tabpanel" aria-labelledby="headingThree" ng-show="model.showlocation">
								<div class="panel-body">
									<div id="worldmap_div" class="datamapsTA" style="position: relative"></div>
								</div>
							</div>
						</div>
						<!--                        <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingFour">
                                                        <h4 class="panel-title">
                                                            <a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                FIND
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                                        <div class="panel-body">
                                                            <h5>FIND AN ENTITY,RELATION,ETC</h5>
                                                            <div class="find">
                                                                <form class="form-horizontal" role="form">
                                                                    <input type="text" id="searchTop" class="form-control navbar-wrapper" placeholder="Search"><br>
                                                                    <h5>FIND RELATION</h5>
                                                                    <input type="text" id="searchRelation" class="form-control" placeholder="Pick One Entity"><br>
                                                                    <input type="text" id="searchRelation" class="form-control" placeholder="Pick One Entity"><br>
                                                                    <input type="text" id="searchRelation" class="form-control" placeholder="Select the close ones" > <br>
                                                                    <input type="submit" class="text-right btn-primary" value="FIND">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->

					</div>
				</aside>
			</div>
			<!--  Side Bar Wrapper Ends  -->

		</div>
		<!--  Header Wrapper Ends  -->
		
		<!--  Update Entities Modal Starts  -->
		<div class="modal fade update-entities-modal" id="basicInfoModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span>&#x2716;</span>
						</button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<p></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!--  Update Entities Modal Ends  -->
		
		<!--  VLA Starts  -->
		<button type="button" class="btn btn-primary toggle-sidebar" style="display: none;">
			<span class="fa fa-angle-right"></span>
		</button>
		<button class="btn btn-primary toggle-header" style="display: none;">
			<span class="fa fa-angle-up"></span>
		</button>
		<div id="vlaerror" class="alert text-center" ng-if="isVlaLoad"></div>
		<div id="vla" class="pull-right"></div>
		<!--  VLA Ends  -->
		
	</div>
	<!--  Custom VLA Ends  -->
	
</div>
<!--  VLA Main Wrapper Ends  -->

<script>
	var actGetCanvas;

	function actCaptureDiv(divElement) {
		html2canvas($('#' + divElement), {
			onrendered : function(canvas) {
				actGetCanvas = canvas;
				$('#actWidgetCapturePreview').html(canvas);
				$('#actWidgetCapture').modal();
			}
		});
	}

	$('#actDownloadWidgetCapture').click(
			function() {
				var imageData = actGetCanvas.toDataURL("image/png");
				var newData = imageData.replace(/^data:image\/png/,
						"data:application/octet-stream");
				$('#actDownloadWidgetCapture').attr("download",
						$('#actCaptureFilename').val()).attr("href", newData);
			});

	$(document)
			.ready(
					function() {

						window.create_new_graph = false; 
						$(".nav-tabs a").click(function() {
							$(this).tab('show');
						});

						$(".case-list-wrapper").mThumbnailScroller({
							axis : "x"
						});
						$(".upload-buttons-wrapper").mThumbnailScroller({
							axis : "x"
						});

						$('.widget-content').mCustomScrollbar({
							axis : "y",
							theme : "minimal"
						});

						$("#mainsidewrapper").mCustomScrollbar({
							axis : "y",
							theme : "minimal-dark",
							alwaysShowScrollbar : 1
						});
						$(".nav-tabs a").click(function() {
							$(this).tab('show');
						});
						
						
						
						
						
						$("body").on(
								"click",
								".dropdown",
								function(e) {
									$(this).toggleClass('open');
									$(this).find('.dropdown-menu').css(
											'display', '');
									$(this).find('.first_dropdown')
											.removeClass('first_dropdown');
									$(this).find('.second_dropdown')
											.removeClass('second_dropdown');
									e.stopPropagation();
									e.preventDefault();
								});

						$("body").on(
								"click",
								".dropdown-submenu a.test",
								function(e) {
									($(this).siblings('ul').not($(this).next(
											'ul'))).css("display", "none");
									$(this).next('ul').toggle();
									$(this).next('ul').find("select").val(
											"nodeBackground");
									e.stopPropagation();
									e.preventDefault();
								})
						$('body').on('click', 'a.menu_first_dropdown',
								function(e) {
									$(this).toggleClass('first_dropdown');
									$('#custom-rearrange').val(window.layout);
									//            $(this).parent().find('.dropdown-menu').css('display','none');   
									e.stopPropagation();
									e.preventDefault();
								});
						$('body').on('click', 'a.menu_second_dropdown',
								function(e) {
									$(this).toggleClass('second_dropdown');
									//            $(this).parent().find('.dropdown-menu').css('display','none');   
									e.stopPropagation();
									e.preventDefault();
								});

						$('body')
								.on(
										'click',
										'a.first_sub_menu',
										function(e) {
											var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
											for(var i=0; i< window.CurentVLAIDs.length; i++){
												if('#' + Id[0].id === window.CurentVLAIDs[i]){
													$(window.CurentVLAIDs[i])
														.attr('style',
														'background-color: #283c45 !important;');
												}
											}
											$(this)
													.toggleClass(
															'second_first_sub_dropdown');
											if (!$(this)
													.hasClass(
															'second_first_sub_dropdown_check'))
												$(this)
														.addClass(
																'second_first_sub_dropdown_check');

											$(this)
													.parent()
													.find(
															'.second_second_sub_dropdown_check')
													.removeClass(
															'second_second_sub_dropdown_check');
											if($('.second_first_sub_dropdown_check')[0])
												window.selectedTheme = $('.second_first_sub_dropdown_check')[0].text;
											setTimeout(
													function() {
														var nodes = _cy_
																.elements();

														nodes.addClass('risks');
														nodes
																.removeClass('whiteTheme');
														nodes
																.removeClass('nodewithoutBackground');
														e.stopPropagation();
														e.preventDefault();
													}, 0);
										});
					
						$('body')
								.on(
										'click',
										'a.second_sub_menu',
										function(e) {
											var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
											for(var i=0; i< window.CurentVLAIDs.length; i++){
												if('#' + Id[0].id === window.CurentVLAIDs[i]){
													$(window.CurentVLAIDs[i])
															.attr('style',
																	'background-color: #F8F8FF !important;');
												}
											}
											$(this)
													.toggleClass(
															'second_second_sub_dropdown');
											if (!$(this)
													.hasClass(
															'second_second_sub_dropdown_check')) {
												$(this)
														.addClass(
																'second_second_sub_dropdown_check');
											}
						
											// console.log($('#configurations').parent());
											$(this)
													.parent()
													.find(
															'.second_first_sub_dropdown_check')
													.removeClass(
															'second_first_sub_dropdown_check');
											if($('.second_second_sub_dropdown_check')[0])
												window.selectedTheme = $('.second_second_sub_dropdown_check')[0].text;
											setTimeout(
													function() {
														var nodes = _cy_
																.elements();
														nodes
																.addClass('whiteTheme');
														nodes
																.removeClass('risks');
														nodes
																.removeClass('nodewithoutBackground');
														e.stopPropagation();
														e.preventDefault();
													}, 0);
										});
						

						$('body').on('click', '.custom-configurations .vla-checkbox', function(n){
							window.create_new_graph =!window.create_new_graph;
							window.initializeVLA(window.create_new_graph);
						});
					});
</script>
<script>
	function openModal() {
		$('#basicInfoModal').modal('show');
		$('#basicInfoModal .modal-title').html(window.entityName);
		$('#basicInfoModal .modal-body p').html(window.count);
	};
</script>
