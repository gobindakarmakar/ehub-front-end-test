<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>


<!--  Process Wrapper Starts  -->
<div class="process-wrapper">

	<!--  Welcome Message Wrapper Starts  -->
	<div class="welcome-msg-wrapper ai-c">
		<p class="choose-msg mar-b0">
			Hi, {{ehubObject.fullName || ehubObject.email}}! Now choose your process
		</p>
		<p class="nothanks mar-autol">
			<a href="javascript:void(0);" class="roboto-regular" ng-click="toDiscover();"><i class="fa fa-tachometer pad-r3"></i> Thanks! Skip to Dashboard</a>
		</p>
	</div>
	<!--  Welcome Message Wrapper Ends  -->

	<!--  Tile Menu Wrapper Starts  -->
	<div class="tile-wrapper">

		<div class="tile-group-wrapper p-rel" ng-repeat="dashboardItem in process.dashboardItems">
				<div class="tile-name">
						<div class="tile-content ai-c d-flex">
							<a href=javascript:void(0); class="{{dashboardItem.mainIconClass}} mar-r10 icon"></a>
							<h4>{{dashboardItem.menu}}</h4>
						</div>
					</div>
			<div class="tile height-100 {{dashboardItem.menu.toLowerCase()}}-tile" ng-class="{'disabled': process.checkMainmenuDisable(dashboardItem.disabled)}" >
				
				<div class="tile-contents height-100 ">
					<ul class=" tile-contents-list mar-b0 height-100 ">
						<li class="tile-contents-list-item" ng-repeat="content in dashboardItem.content" ng-click="process.getSubMenu(content, content.disabled, dashboardItem); $event.stopPropagation()">
							<i class="fa fa-home home-icon c-pointer" popover-trigger="'mouseenter'" 
							popover-placement="top-right" ng-class="{'op-1':(defaultModuleName === content.name.toLowerCase())}"
							popover-class="top-popover-wrapper border-0 pad-0" uib-popover="Set as main screen"
							popover-append-to-body='true' ng-click="updateVal(content,$event)"></i>
							<button ng-disabled="process.checkSubmenuDisable(content.disabled);" class="btn btn-default" ng-class="process.addClass($index);">
							<i class="{{content.iconClass}} icon" ng-if="content.name != 'Fraud' && content.name != 'Personalization'" ></i>
							<img src="assets/images/icon/fraud.svg" class="img-responsive center-block" style="width:32px;height:32px" ng-if="content.name == 'Fraud'" >
							<img src="assets/images/icon/leadGeneration.svg" class="img-responsive center-block" style="width:32px;height:32px" ng-if="(content.name == 'Personalization' || content.name =='Lead Generation')" >
							<p class="text-center text-cream">{{content.name}}</p></button> 
						</li>
					</ul>			
				</div>
			</div>
		</div>

	</div>
	<!--  Tile Menu Wrapper Ends  -->
	<span ng-show="processPageLoader" class="custom-spinner case-dairy-spinner"> 
		<i class="fa fa-spinner fa-spin fa-4x"></i>
	</span>

</div>
<!--  Process Wrapper Ends  -->
		