<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  Notification Wrapper Starts  -->
<div class="notification-wrapper centered custom-card">

    <form class="form">
        <h3 class="form-title">
            Generate Events
        </h3>
       
        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Subject</label>
            <div class="col-sm-9">
                <input class="form-control" placeholder="Subject" type='text'  ng-model = 'notiSubject'/>
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
                <textarea class="form-control" rows="5"  id="" ng-model = 'notiBody'></textarea>
            </div>
        </div>
         <div class="form-group clearfix">
            <label class="col-sm-3 control-label"><i class="fa fa-user"></i> Send To:</label>
            <div class="col-sm-9 custom-input-group user-list-wrapper custom-select-dropdown">
                <span class="input-group-addon">
                    <i class="fa fa-search"></i>
                </span>
                <input class="form-control mar-b5" type="text"  ng-model="notiUserValue" placeholder=""  uib-typeahead=" user as user.firstName for user in notificationVariable.userListNotification | filter:$viewValue " typeahead-loading="loadingPersonsNames"
                       typeahead-no-results="noREResults" typeahead-on-select="notificationFunctions.shareNotificationToUser($item, $model, $label)"> {{user}}
                <div class="text-center custom-dropdown-menu" ng-show="loadingPersonsNames">
                    <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <div class="custom-dropdown-menu" ng-show="noREResults">
                    <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                </div>
                <ul class="list-inline custom-list search-result-list">
                    <li ng-repeat="(k,selectedUser) in notificationVariable.selectedUser">
                        <a>
                            {{selectedUser.firstName}}<i class="glyphicon glyphicon-remove text-light-red icon-right" ng-click='notificationFunctions.removeUser(k,selectedUser)'></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div> 
        <div class="form-group clearfix">
	          <div class="row">
      <div class="col-md-6">
        <p class="input-group">
          <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="todayVariable.dtFrom" is-open="todayVariable.fromDate" datepicker-options="todayVariable.dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" ng-change="todayFunctions.onChangeDateFrom(todayVariable.dtFrom)" />
          <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="todayFunctions.openscheduleFrom()"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </p>
      </div>
	    <div class="col-md-6">
        <p class="input-group">
          <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="todayVariable.dtTo" is-open="todayVariable.toDate" datepicker-options="todayVariable.dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" ng-change="todayFunctions.onChangeDateTo(todayVariable.dtTo)"  />
          <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="todayFunctions.openscheduleTo()"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </p>
      </div>
	         
	    
		</div>
         <div class="form-group clearfix">
            <div class="col-sm-12">              
                <button class='btn btn-blue' ng-click ='notificationFunctions.sendNotification(notiBody,notiSubject)'>Send</button>
            </div>            
        </div>           
    </form>

</div>
<!--  Notification Wrapper Ends  -->