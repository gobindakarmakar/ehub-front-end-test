<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-cloak>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
	<!--  Super Sub Header Wrapper Ends  -->

	<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">
		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">
			<div class="activiti-task-wrapper reports-wrapper user-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-3 col-sm-4">

							<!--  Aside Wrapper Starts  -->
							<aside class="aside-wrapper text-uppercase">
								<div class="form-group has-success has-feedback">
									<input type="text" class="form-control" placeholder="Search" />
									<span class="glyphicon glyphicon-search form-control-feedback"
										aria-hidden="true"></span>
								</div>

								<div class="row">
									<div class="col-sm-7">
										<h3>Jobs</h3>
									</div>
									<div class="col-sm-5">
										<div class="dropdown clearfix">
											 <a href="javascript:void(0);" uib-dropdown-toggle class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											  </a>
												<ul  uib-dropdown-menu class="dropdown-menu" aria-labelledby="dLabel">
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
													<li><a href="javascript:void(0);">By Date</a></li>
												</ul>
										</div>
									</div>
								</div>

								<div class="panel custom-panel active groups-panel" ng-repeat="jobsdata in jobs.jobsData">
									<div class="panel-heading">
										<i class="fa fa-clock-o"></i>{{jobsdata.name}}<span>{{jobsdata.value}}</span>
									</div>
								</div>

							</aside>
							<!--  Aside Wrapper Ends  -->

						</div>
						<div class="col-md-9 col-sm-8">

							<!--  Create New Task Wrapper Starts  -->
							<div class="create-new-task-wrapper">
								<div class="create-new-task-inner-wrapper">
									<!--  Notification Panel Starts  -->
									<div class="panel custom-panel notification-panel">
										<div class="panel-heading">
											<div class="notification-header">
												<a href="javascript:void(0);"
													class="fa fa-clock-o server-link calendar-link"></a>
												<h3>Calendar</h3>
												<ul class="list-inline">
													<li><i class="fa fa-calendar-o"></i>Due Week Ago</li>
												</ul>
											</div>
										</div>
									</div>
									<!--  Notification Panel Ends  -->

									<!--  Job Execution Panel Starts  -->
									<div class="panel custom-panel job-execution-panel">
										<div class="panel-body">
											<h3>Job Execution</h3>
											<p class="notification-text">
												<i class="fa fa-exclamation-circle"></i> No more retires
												left
											</p>
											<p class="error text-danger">Error occurred while
												executing job: problem evaluating script: javax.script.
												ScriptExecution: java.lang.RuntimeException: Activity Engine
												Rocks!</p>
											<div class="error-wrapper">
												<p class="lead-text">
													rg.activiti.engine.ActivitiException: problem evaluating
													script: javax.script.ScriptException:
													java.lang.RuntimeException: Activiti Engine Rocks!</p>
												<p class="sub-text">at
													org.activiti.engine.impl.scripting.ScriptingEngines.evaluate(ScriptingEngines.java:89)
													at
													org.activiti.engine.impl.scripting.ScriptingEngines.evaluate(ScriptingEngines.java:73)
													at
													org.activiti.engine.impl.bpmn.behavior.ScriptTaskActivityBehavior.execute(ScriptTaskActivityBehavior.java:78)
													at
													org.activiti.engine.impl.pvm.runtime.AtomicOperationActivityExecute.execute(AtomicOperationActivityExecute.java:60)
													at
													org.activiti.engine.impl.interceptor.CommandContext.performOperation(CommandContext.java:97)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperationSync(ExecutionEntity.java:644)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperation(ExecutionEntity.java:637)
													at</p>
												<p class="lead-text">
													org.activiti.engine.impl.pvm.runtime.AtomicOperationTransitionNotifyListenerStart.eventNotificationsCompleted(AtomicOperationTransitionNotifyListenerStart.java:52)
												</p>
												<p class="sub-text">at
													org.activiti.engine.impl.pvm.runtime.AbstractEventAtomicOperation.execute(AbstractEventAtomicOperation.java:56)
													at
													org.activiti.engine.impl.interceptor.CommandContext.performOperation(CommandContext.java:97)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperationSync(ExecutionEntity.java:644)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperation(ExecutionEntity.java:637)
													at
													org.activiti.engine.impl.pvm.runtime.AbstractEventAtomicOperation.execute(AbstractEventAtomicOperation.java:49)
													at
													org.activiti.engine.impl.interceptor.CommandContext.performOperation(CommandContext.java:97)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperationSync(ExecutionEntity.java:644)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperation(ExecutionEntity.java:637)
													at
													org.activiti.engine.impl.pvm.runtime.AtomicOperationTransitionCreateScope.execute(AtomicOperationTransitionCreateScope.java:49)
													at
													org.activiti.engine.impl.interceptor.CommandContext.performOperation(CommandContext.java:97)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperationSync(ExecutionEntity.java:644)
													at
													org.activiti.engine.impl.persistence.entity.ExecutionEntity.performOperation(ExecutionEntity.java:637)
												</p>
											</div>
										</div>
									</div>
									<!-- Job Execution Panel Ends  -->

								</div>
								<!--  Related Content Panel Ends  -->

							</div>
							<!--  Create New Task Wrapper Ends  -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  Content Wrapper Ends  -->
	</div>
	<!--  Main Wrapper Ends  -->
</div>
