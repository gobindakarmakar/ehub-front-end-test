<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="common/submenu.jsp"%>
<%@include file="supersubmenu.jsp"%>
<!--  Main Wrapper Starts  -->
	<div class="main-wrapper">
		<!--  Content Wrapper Starts  -->
		<div class="content-wrapper">
			<div
				class="activiti-task-wrapper reports-wrapper vacation-request model-workspace">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-3 col-sm-4">

							<!--  Aside Wrapper Starts  -->
							<aside class="aside-wrapper text-uppercase">

								<div class="form-group has-success has-feedback">
									<input type="text" class="form-control"
										placeholder="Search Models" /> <span
										class="glyphicon glyphicon-search form-control-feedback"
										aria-hidden="true"></span>
								</div>

								<div class="sidebar-header">
									<h3>Recent Models</h3>
									<div class="bydate-sorting">
										<div class="dropdown clearfix">
											<a href="javascript:void(0);" class="pull-right" id="dLabel"
												data-toggle="dropdown"> By Date<span class="caret"></span>
											</a>
											<ul class="dropdown-menu" aria-labelledby="dLabel">
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
												<li><a href="javascript:void(0);">By Date</a></li>
											</ul>
										</div>
									</div>
								</div>

								<div class="panel custom-panel panel-info">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Leave Management Model</h3>
										<p>Created : 1 Week Ago</p>
									</a>
								</div>
								<div class="panel custom-panel">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Provident Fund Model</h3>
										<p>Last Edited : 1 Week Ago</p>
									</a>
								</div>
								<div class="panel custom-panel">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Call Center Process Model</h3>
										<p>Created : 1 Week Ago</p>
									</a>
								</div>
								<div class="panel custom-panel">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Customer Complaint Model</h3>
										<p>Created : 1 Week Ago</p>
									</a>
								</div>
								<div class="panel custom-panel">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Sales Process Model</h3>
										<p>Last Edited : 1 Week Ago</p>
									</a>
								</div>
								<div class="panel custom-panel">
									<a class="panel-body" href="javascript:void(0);">
										<h3>Sales Lead Process</h3>
										<p>Last Edited : 1 Week Ago</p>
									</a>
								</div>
							</aside>
							<!--  Aside Wrapper Ends  -->

						</div>
						<div class="col-md-9 col-sm-8">

							<!--  Create New Task Wrapper Starts  -->
							<div class="create-new-task-wrapper">
								<div class="new-import-model clearfix">
									<ul class="list-inline text-uppercase pull-right">
										<li><a class="btn btn-custom" href="javascript:void(0);" ng-click="openModal()"> 
											<i class="fa fa-plus"></i> New Model
										</a></li>
										<li><a class="btn btn-custom" href="javascript:void(0);">
												<i class="fa fa-arrow-down"></i> Import Model
										</a></li>
									</ul>
								</div>
								<div class="create-new-task-inner-wrapper">
									<!--  Notification Panel Starts  -->
									<div class="panel custom-panel notification-panel">
										<div class="panel-heading">
											<div class="notification-header">
												<a href="javascript:void(0);" class="fa fa-cogs server-link"></a>
												<h3>Leave Management Model</h3>
												<ul class="list-inline">
													<li><i class="fa fa-check-circle"></i>version : 1</li>
													<li><i class="fa fa-calendar-o"></i>Created on :
														12/09/2016</li>
												</ul>
											</div>
											<ul class="list-inline custom-options">
												<li><a class="btn btn-custom"
													href="javascript:void(0);"> Edit </a></li>
												<li class="dropdown"><a class="btn btn-dropdown"
													href="javascript:void(0);" data-toggle="dropdown"> Act<i
														class="fa fa-angle-down"></i>
												</a>
													<ul class="dropdown-menu">
														<li><a href="javascript:void(0);">Act 1</a></li>
														<li><a href="javascript:void(0);">Act 2</a></li>
														<li><a href="javascript:void(0);">Act 3</a></li>
													</ul></li>
											</ul>
										</div>
									</div>
									<!--  Notification Panel Ends  -->


									<!--  Sub Task Panel Starts  -->
									<div class="panel custom-panel sub-task-panel">
										<div class="vacation-request-content-body">
											<h4 class="text-uppercase">Process Diagram</h4>
										</div>
									</div>
									<!--  Sub Task Panel Ends  -->

								</div>
								<!--  Related Content Panel Ends  -->

							</div>
							<!--  Create New Task Wrapper Ends  -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  Content Wrapper Ends  -->
	</div>
	<!--  Main Wrapper Ends  -->


	