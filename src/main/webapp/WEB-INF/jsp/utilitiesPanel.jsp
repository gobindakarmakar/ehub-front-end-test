<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<div class="utilities-panel-wrapper" ng-controller="subMenuController">
	<div id="process-menu-dropdown-button-panel" ng-class="{'db-util-background-discover':isDiscover, 'db-util-background-act':isAct, 'db-util-background-enrich':isEnrich, 'db-util-background-predict':isPredict, 'db-util-background-manage':isManage}" class="dropdown db-util-process-dropdown">
		<div id="process-menu-dropdown-button" ng-click="utildashboarddropmenu($event)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars visible-xs"></i> <strong ng-cloak class="hidden-xs">{{dashboardUtilitiesPanelSelectedMenu}}</strong> <span class="pull-right"><i class="fa fa-angle-down"></i></span>
		</div>
		<div id="process-menu-dropdown-menu-panel" class="dropdown-menu" ng-class="{'db-util-background-discover':isDiscover, 'db-util-background-act':isAct, 'db-util-background-enrich':isEnrich, 'db-util-background-predict':isPredict, 'db-util-background-manage':isManage}" ng-click="$event.stopPropagation();">
			<ul class="nav nav-tabs nav-justified">
				<li id="dashboard_tab_FIRST" class="active"><a ng-click="dashboardUtitlitiesDropdownMenuTabClick($event);" ng-mouseover="dashboardUtilitiesDropDownmenuHover($event);"></a></li>
				<li id="dashboard_tab_{{item}}" ng-repeat="item in dashboardUtilitiesDropDownMenuItems"><a ng-click="dashboardUtitlitiesDropdownMenuTabClick($event);" ng-mouseover="dashboardUtilitiesDropDownmenuHover($event);" ng-if="item != 'ENRICH'" ng-cloak> {{item}} </a> <a ng-click="dashboardUtitlitiesDropdownMenuTabClick($event);" ng-mouseover="dashboardUtilitiesDropDownmenuHover($event);" ng-if="item == 'ENRICH'" onclick="sessionStorage.isAdvancedSearch = true" ng-cloak> {{item}} </a></li>
			</ul>
			<div id="act-menu-content" class="col-lg-12 utility-menu-content">
				<div class="col-sm-12">
					<ul class="list-inline">
						<li><a ng-click="onEntities($event)">Entities</a></li>
						<li><a ui-sref="casePageLanding">Cases</a></li>
						<li><a href="http://178.62.195.202/VLA/?{{vlaQuery || entitySearchResult.name || queryText || 'petro saudi'}}" target="_blank">Link Analysis</a></li>
					</ul>
				</div>
			</div>
			<div id="discover-menu-content" class="col-lg-12 utility-menu-content">
				<div class="col-sm-12">
					<ul class="list-inline">
						<li><a ui-sref="discover">Dashboard</a></li>
						<li><a ui-sref="workspace">Workspace</a></li>
						<li><a ui-sref="investigationLanding">Investigation</a></li>
						<li><a ui-sref="transactionMonitoring">Transaction Monitoring</a></li>
						<li><a ui-sref="mip">Market Intelligence</a></li>
					</ul>
				</div>
			</div>
			<div id="enrich-menu-content" class="col-lg-12 utility-menu-content">
				<div class="col-sm-12">
					<ul class="list-inline">
						<li><a ui-sref="enrich">Advance Search</a></li>
						<li><a ui-sref="enrich">On Boarding</a></li>
					</ul>
				</div>
			</div>
			<div id="predict-menu-content" class="col-lg-12 utility-menu-content">
				<div class="col-sm-12">
					<ul class="list-inline">
						<li><a href="javascript:void(0);"></a></li>
					</ul>
				</div>
			</div>
			<div id="manage-menu-content" class="col-lg-12 utility-menu-content">
				<div class="col-sm-12">
					<ul class="list-inline">
						<li><a ui-sref="userManagement">User Management</a></li>
						<li><a ui-sref="workflow">Big Data Workflow</a></li>
						<li><a ui-sref="appManager">App Manager</a></li>
						<li><a ui-sref="dataSource">Data Management</a></li>
						<li><a ui-sref="systemMonitoring">System Monitoring</a></li>
						<li><a ui-sref="generalSettings">General Settings</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-------------- 	PREDICT MENU    ---------------->
	<div class="flex-grid db-util-process-tab-predict db-util-background-predict" ng-if="currentState == 'predict'"></div>

	<!-------------- 	ENRICH MENU    ---------------->
	<div class="flex-grid db-util-process-tab-enrich db-util-background-enrich" ng-if="currentState == 'enrich' || currentState == 'enrichCase' || currentState == 'enrichSearchItem'">
		<div class="col-xs-6">
			<span class="search-text">ADVANCED SEARCH</span>
		</div>
		<div class="col-xs-6 btn-onboarding-wrapper">
			<div class="btn-group">
				<a ng-click="onboarding()" href class="btn btn-primary px-3 btn-onboarding"><span>ONBOARDING</span></a>
			</div>
		</div>
	</div>
	<div class="flex-grid db-util-process-tab-enrich db-util-background-enrich" ng-if="currentState == 'data-find-data'">
		<div class="col-xs-6">
			<span class="search-text" style="font-size: 1.5em; line-height: 1.8em;">ADVANCED SEARCH</span>
		</div>
		<!-- <div class="col-xs-6" style="text-align: right;">
				<div class="btn-group">
					<a ng-click="onboarding()" href class="btn btn-primary px-3 btn-onboarding"> <span>ONBOARDING</span>
					</a>
				</div>
			</div> -->
	</div>
	<div class="flex-grid db-util-process-tab-enrich db-util-background-enrich custom-rightbar" ng-if="currentState == 'social-media-content'">
		<div class="col-xs-9">
			<div class="breadcrumbs text-uppercase">
				Entity <span ng-show="entitySearchResult.name"></span> {{entitySearchResult.name}}
			</div>
		</div>
		<div class="col-xs-3 btn-onboarding-wrapper">
			<div class="btn-group">
				<a href="http://178.62.195.202/VLA/?{{entitySearchResult.name || vlaQuery}}" target="_blank" title="Network analysis" class="btn btn-primary px-3 btn-onboarding"><span>VLA</span></a>
			</div>
		</div>
	</div>
	<div class="flex-grid db-util-process-tab-enrich db-util-background-enrich custom-rightbar" ng-if="currentState == 'entityCompany' || currentState == 'entityPage' || currentState == 'social-live-feed'">
		<div class="col-xs-9">
			<div class="breadcrumbs text-uppercase">
				<a>Entity</a> <a><span ng-show="entitySearchResult.name"></span> {{entitySearchResult.name}}</a>
			</div>
		</div>
		<div class="col-xs-3 btn-onboarding-wrapper">
			<div class="btn-group">
				<a href="http://178.62.195.202/VLA/?{{entitySearchResult.name}}" target="_blank" title="Network analysis" class="btn btn-primary px-3 btn-onboarding"><span>Link Analysis</span></a>
			</div>
		</div>
	</div>
	<div class="flex-grid db-util-process-tab-enrich db-util-background-enrich custom-rightbar" ng-if="currentState == 'entityPageYukos'" style="padding: 20px; width: 82%; float: left;">
		<div class="flex-grid">
			<div class="breadcrumbs text-uppercase">Entity > Yukos International</div>
		</div>
	</div>
	<!-------------- 	DISCOVER MENU    ---------------->
	<div class="flex-grid db-util-process-tab-discover db-util-background2-discover" ng-class="{'db-util-process-tab-mip':isMip}" ng-if="currentState == 'discover' || currentState == 'discoverCase' || currentState == 'workspace' || currentState == 'investigationLanding' || currentState == 'transactionMonitoring' || currentState == 'mip'">
		<div ng-repeat="item in dashboardUtilitiesPanelDiscoverCounter" class="hello flex-grid counter flex-grid-expand">
			<span class="counter-label" ng-cloak>{{item.label}}</span> <span class="counter-value" ng-cloak>{{item.value | zpad:2}}</span>
		</div>
	</div>

	<div id="work-diary-dropdown-button-panel" ng-if="currentState == 'discover' || currentState == 'discoverCase' || currentState == 'workspace' || currentState == 'investigationLanding' || currentState == 'transactionMonitoring'" class="dropdown db-util-work-diary-dropdown" ng-class="{'open': workDairyToggle}" ng-click="dashboardUtilitiesPanelDiscoverOnClickWorkDiaryDropDown();">
		<div id="work-diary-dropdown-button">
			<span class="icon-left"><i class="fa fa-rss"></i></span>
			<div class="work-diary-dropdown-button-text hidden-xs">
				<div>YOUR WORK DIARY</div>
				<div class="small">New cases has been assigned to you</div>
			</div>
			<span class="pull-right"><i class="fa fa-angle-down"></i></span>
		</div>
		<div id="work-diary-dropdown-menu-panel" class="dropdown-menu">
			<ul style="list-style: none;">
				<li>
					<button type="button" id="work-diary-create-case" class="btn btn-default" ng-click="displayCreate()">
						<span id="work-diary-create-case-icon" class="pull-left"><i class="fa fa-plus"></i></span> <span id="work-diary-create-case-text">CREATE CASE</span>
					</button>
				</li>
				<li>
					<div id="work-diary-count-case">
						<div>
							<span id="work-diary-count-case-num" ng-cloak>{{dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount | zpad:2}}</span> <span id="work-diary-count-case-text">Cases Pending for your action. Please initiate your action.</span>
						</div>
					</div>
				</li>
				<li>
					<div id="work-diary-sub-menu">
						<div id="work-diary-sub-menu-filter" class="pull-right">
							<span><i class="fa fa-filter"></i></span>
						</div>
						<div id="work-diary-sub-menu-camera" class="pull-right">
							<span><i class="fa fa-camera"></i></span>
						</div>
					</div>
				</li>

				<li><span ng-show="yourWorkDairyPreloader" class="custom-spinner case-dairy-spinner"> <i class="fa fa-spinner fa-spin fa-2x"></i>
				</span>

					<div id="createCaseWrapper" style="overflow-y: auto; overflow-x: hidden; height: 100%; max-height: 470px; margin: 0;">
						<dashboard-utilities-discover-work-diary-case-list index=$index name=item.name purpose=item.description type=item.type assignedby=item.assignedBy trisk=item.transactionRisk drisk=item.directRisk irisk=item.indirectRisk seedid=item.seedId currentstatus=item.currentStatus ng-repeat="item in dashboardUtilitiesPanelDiscoverWorkDiaryCaseList" ng-mouseover="" on-click-act-dropdown="discoverOnClickWorkDiaryActDropDown(index)" is-hide-act-dropdown="discoverIsHideWorkDiaryActDropDown(index)" on-click-act-dropdown-item="discoverOnClickWorkDiaryActDropDownItem(ev, index, seedid, currentstatus)"> </dashboard-utilities-discover-work-diary-case-list>
					</div>
					<div class="text-center" ng-hide="dashboardUtilitiesPanelDiscoverWorkDiaryCaseList.length == 0 || allCasesCount <= 10">
						<ul uib-pagination total-items="allCasesCount" ng-model="pageNumber" ng-change="pageChanged(pageNumber)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
					</div></li>
			</ul>
		</div>
	</div>
	<!-------------- 	ACT MENU    ---------------->
	<div class="db-util-process-tab-act db-util-background-act" ng-if="currentState == 'act' || currentState == 'actCase' || currentState == 'casePageLanding'">
		<div class="row-fluid">
			<div class="col-xs-6 case-list-wrapper">
				<ul class="list-inline clearfix">
					<li class="item2"><span style="font-weight: 500">CASE NAME: </span><span style="font-weight: 500" title="{{dashboardUtilitiesActCaseName}}" ng-cloak>{{dashboardUtilitiesActCaseName}}</span>
						<div style="position: absolute; top: 30px;">
							<span title="{{dashboardRiskyTransaction}}%">Risk Transaction: {{dashboardRiskyTransaction}}%</span>
						</div></li>
					<li class="item2">
						<div class=case-type">
							<span>TYPE: </span> <span title="{{dashboardUtilitiesActType}}" ng-cloak>{{dashboardUtilitiesActType}}</span>
						</div>
						<div class=case-id">
							<span>CASE ID: </span> <span title="{{dashboardUtilitiesActCaseId}}" ng-cloak>{{dashboardUtilitiesActCaseId}}</span>
						</div>
					</li>
					<li class="item3">
						<div>
							<span>DESCRIPTION: </span> <span title="{{dashboardUtilitiesActDescription}}" ng-cloak>{{dashboardUtilitiesActDescription | limitTo:32}}{{dashboardUtilitiesActDescription.length > 32 ? '...' : ''}}</span>
						</div>
						<div>
							<span>REMARKS: </span> <span title="{{dashboardUtilitiesActRemarks}}" ng-cloak>{{dashboardUtilitiesActRemarks | limitTo:40}}{{dashboardUtilitiesActRemarks.length > 40 ? '...' : ''}}</span>
						</div>
					</li>
				</ul>
			</div>

			<div class="col-xs-6 upload-buttons-wrapper pull-right">
				<ul class="upload-buttons list-inline clearfix">
					<li>
						<button disabled ng-click="disseminate()" type="button" class="btn btn-success btn-sm px-2">
							<span class="hidden-lg" title="DISSEMINATION"><i class="fa fa-file-powerpoint-o"></i></span> <span class="hidden-md hidden-sm hidden-xs">DISSEMINATION</span>
						</button>
					</li>
					<li>
						<button type="button" class="btn btn-success btn-sm px-2" ng-disabled="showCaseTimeLineCaseId" ng-click="upload()">
							<span class="hidden-lg" title="UPLOAD"><i class="fa fa-upload"></i></span> <span class="hidden-md hidden-sm hidden-xs">UPLOAD</span>
						</button>
					</li>
					<!-- SHOULD ONLY DISPLAY FOR KYC -->
					<!-- 				 <span class="onboarding" ng-if="dashboardUtilitiesActType == 'KYC'"> -->
					<li class="onboarding">
						<button type="button" class="btn btn-danger btn-sm px-2" data-toggle="dropdown" ng-class="{'btn-danger':incomplete}" ng-click="onboarding()">
							<span class="hidden-lg glyphicon glyphicon-alert" title="ONBOARDING"><i ng-class="{'glyphicon glyphicon-alert':incomplete}"></i></span> <span class="hidden-xs hidden-sm hidden-md">ONBOARDING</span>
						</button>
					</li>

					<!--<ul class="onboarding-list" ng-if=workflowdisplay >
							 <li ng-repeat="(x, y) in workflow" ng-switch on="y">
								<p class="">{{x}} - {{y}}</p>
							 </li>
						</ul>-->

					<li>
						<button type="button" class="btn btn-success btn-sm px-2" ng-click="displayReassignModal()">
							<span class="hidden-lg" title="RE-ASSIGN"><i class="fa fa-retweet"></i></span> <span class="hidden-xs hidden-sm hidden-md">RE-ASSIGN</span>
						</button>
					</li>
					<li><a style="color: #fff" class="btn btn-success btn-sm px-2" href="http://178.62.195.202/VLA/?{{vlaQuery || 'petro saudi'}}" target="_blank"> <span class="hidden-lg" title="Link Analysis"><i class="fa fa-link"></i></span> <span class="hidden-xs hidden-sm hidden-md">Link Analysis</span>
					</a></li>
					<li ng-if="dashboardUtilitiesActStatus == 4">
						<button type="button" class="btn btn-success btn-sm px-2" ng-click="displayResolveModal()">
							<span class="hidden-lg" title="RESOLVE"><i class="fa fa-check-square"></i></span> <span class="hidden-xs hidden-sm hidden-md">RESOLVE</span>
						</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-------------- 	MANAGE MENU    ---------------->
	<div class="flex-grid db-util-process-tab-manage db-util-background-manage"
		ng-if="currentState == 'userManagement' || currentState == 'appManager' || currentState == 'dataSource' || currentState == 'systemMonitoring' || currentState == 'generalSettings'
																							|| currentState == 'activeProcess' || currentState == 'appConfiguration' || currentState == 'customFields' || currentState == 'exportImport' || currentState == 'jobs'
																							|| currentState == 'addNewSources' || currentState == 'bigDataRules' || currentState == 'deploymentProcess' || currentState == 'fields' || currentState == 'licenseManager'
																							|| currentState == 'socialManager' || currentState == 'businessRules' || currentState == 'deployments' || currentState == 'groups' || currentState == 'logics'
																							|| currentState == 'administrations' || currentState == 'crystalBall' || currentState == 'events' || currentState == 'hosts' || currentState == 'accessControls'
																							|| currentState == 'manageDb' || currentState == 'passwords' || currentState == 'manageSources' || currentState == 'myApps' || currentState == 'myDataSources'
																							|| currentState == 'monitor' || currentState == 'roles' || currentState == 'modelWorkspace' || currentState == 'ontology'|| currentState == 'systemProperties'
																							|| currentState == 'portalSettings' || currentState == 'processInstance' || currentState == 'reports' || currentState == 'response' || currentState == 'portalInstances'
																							|| currentState == 'serveSettings' || currentState == 'suspendedProcess' || currentState == 'tasks' || currentState == 'users' || currentState == 'store'"
	>
		<div class="activiti-subheader-wrapper">
			<div class="container-fluid clearfix">
				<div class="task-subheader">
					<h4>Management Hub</h4>
					<h6>Business Process Workflow</h6>
				</div>
				<div class="task-subheader-content">
					<ul class="list-inline">
						<li ng-class="{'active': activeTasks}">
							<a class="text-uppercase" ui-sref="tasks"> 
								<i class="fa fa-server"></i> <span class="text-uppercase">TASKS</span>
							</a>
						</li>
						<li ng-class="{'active': activeProcesses}">
							<a class="text-uppercase" ui-sref="processInstance"> 
								<i class="fa fa-cogs"></i> <span class="text-uppercase">Processes</span>
							</a>
						</li>
						<li ng-class="{'active': activeReports}">
							<a class="text-uppercase" ui-sref="reports"> 
								<i class="fa fa-file-text"></i> <span class="text-uppercase">Reports</span>
							</a>
						</li>
						<li ng-class="{'active': activeManage}">
							<a class="text-uppercase" ui-sref="manageDb"> 
								<i class="fa fa-sliders"></i> <span class="text-uppercase">Manage</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="flex-grid db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'workflowDashboard'">
		<!--  Sub Header Wrapper Starts  -->
		<div class="subheader-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-6">
						<h4>Management Hub</h4>
						<a href="javascript:void(0);">Workflow</a> <span class="fa fa-angle-right"></span> <a href="javascript:void(0);">Integration and Data Science Platform</a>

					</div>
					<div class="col-xs-6">
						<div class="NameUpdate">
							<label>Workflow Name :</label> <input class="workflowNameChange" type="text" id="ChangeName">
						</div>
						<button class="text-uppercase btn btn-info" type="button" id="save-workflow">Save</button>

					</div>
				</div>
			</div>
		</div>
		<!--  Sub Header Wrapper Ends  -->
	</div>
	<div class="flex-grid db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'manage'"></div>
	<div class="flex-grid db-util-process-tab-manage db-util-background-manage" ng-if="currentState == 'workflow'">
		<!--  Sub Header Wrapper Starts  -->
		<div class="subheader-wrapper">
			<div class="container-fluid">
				<h4>Management Hub</h4>
				<a href="javascript:void(0);">Workflow</a> <span class="fa fa-angle-right"></span> <a href="javascript:void(0);">Integration and Data Science Platform</a>
			</div>
		</div>
		<!--  Sub Header Wrapper Ends  -->
	</div>
</div>