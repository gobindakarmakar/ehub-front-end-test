"use strict";

angular.module('uploadDocumentsApp')
    .factory('UpoadDocumentService', upoadDocumentService);


upoadDocumentService.$inject = [
    '$http',
    'EHUB_API',
    '$q',
    '$rootScope',
    '$stateParams',
    'Upload'
];

function upoadDocumentService(
    $http,
    EHUB_API,
    $q,
    $rootScope,
    $stateParams,
    Upload) {
    return {
        getAdverseNews: getAdverseNews,
        getIdentifier: getIdentifier,
        saveIntoDatabase: saveIntoDatabase,
        generateJsonResponse: generateJsonResponse,
        getDocumentDetails: getDocumentDetails,
        getNews: getNews,
        getInfoCompanyHouse: getInfoCompanyHouse,
        updateAnswer: updateAnswer,
        createCaseFromQuestioner: createCaseFromQuestioner,
        downloadQuestionnaire: downloadQuestionnaire,
        caseListForUnderwriting:caseListForUnderwriting
    };
    /*
     * @purpose: getAdverseNews
     * @created: 4th May 2018
     * @params: company name
     * @return: success, error functions
     * @author: Prasanthi
     */

    function getAdverseNews(data) {
        var defer = $q.defer();
        //var apiUrl = EHUB_API + 'adverseNews/news/search?' + "&token=" + $rootScope.ehubObject.token;
        var apiUrl = EHUB_API + "adverseNews/events/search?token=" + $rootScope.ehubObject.token;
        var request = $http({
            method: "POST",
            data: data,
            /*data:{
                "class": "Cyber Security",
                 "offset":1,
                 "size": 30
                },*/
            headers: {
                "Content-Type": "application/json"
            },
            url: apiUrl,
        });
        request
            .then(getAdverseNewsSuccess)
            .catch(getAdverseNewsError);

        /*getAdverseNews error function*/
        function getAdverseNewsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getAdverseNews success function*/
        function getAdverseNewsSuccess(response) {
            defer.resolve(response);
        }

        return defer.promise;
    }
    /*
     * @purpose: getIdentifier
     * @created: 4th May 2018
     * @params: company name
     * @return: success, error functions
     * @author: Prasanthi
     */

    function getIdentifier(name) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'entity/org/suggest-name' + "?query=" + name + "&token=" + $rootScope.ehubObject.token;
        var request = $http({
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
            url: apiUrl,
        });
        request
            .then(getIdentifierSuccess)
            .catch(getIdentifierError);

        /*getIdentifier error function*/
        function getIdentifierError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getIdentifier success function*/
        function getIdentifierSuccess(response) {
            defer.resolve(response);
        }

        return defer.promise;
    }
    /*
     * @purpose: saveIntoDatabase
     * @created: 30th April 2018
     * @params: docId
     * @return: success, error functions
     * @author: varsha
     */

    function saveIntoDatabase(docId) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'documentParser/auditDatabase' + "?token=" + $rootScope.ehubObject.token + "&docId=" + docId;
        var request = $http({
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
            url: apiUrl,
        });
        request
            .then(saveIntoDatabaseSuccess)
            .catch(saveIntoDatabaseError);

        /*saveIntoDatabase error function*/
        function saveIntoDatabaseError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*saveIntoDatabase success function*/
        function saveIntoDatabaseSuccess(response) {
            defer.resolve(response);
        }

        return defer.promise;
    }

    /*
     * @purpose: generateJsonResponse
     * @created: 30th April 2018
     * @params: docId
     * @return: success, error functions
     * @author: varsha
     */

    function generateJsonResponse(docId) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'documentParser/getDocumentParseResponse' + "?token=" + $rootScope.ehubObject.token + "&docId=" + docId;
        //        var apiUrl = "scripts/data.json"
        var request = $http({
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
            url: apiUrl,
        });
        request
            .then(generateJsonResponseSuccess)
            .catch(generateJsonResponseError);

        /*generateJsonResponse error function*/
        function generateJsonResponseError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*saveIntoDatabase success function*/
        function generateJsonResponseSuccess(response) {
            defer.resolve(response);
        }

        return defer.promise;
    }

    /*
     * @purpose: get document details
     * @created: 1st May 2018
     * @params: docId
     * @return: success, error functions
     * @author: zameer
     */

    function getDocumentDetails(Id) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'documentStorage/getDocumentDetails' + "?token=" + $rootScope.ehubObject.token + "&docId=" + Id;
        var request = $http({
            method: 'GET',
            url: apiUrl
        });
        request
            .then(getDocumentDetailsSuccess)
            .catch(getDocumentDetailsError);

        /*getDocumentDetails error function*/
        function getDocumentDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getDocumentDetails success function*/
        function getDocumentDetailsSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }

    /*
     * @purpose: get news details
     * @created: 4th May 2018
     * @params: docId
     * @return: success, error functions
     * @author: zameer
     */

    function getNews(identifier) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'tuna/profile/' + identifier + "?token=" + $rootScope.ehubObject.token + "&orgOrPerson=org";
        var request = $http({
            method: 'GET',
            url: apiUrl
        });
        request
            .then(getNewsSuccess)
            .catch(getNewsError);

        /*getNews error function*/
        function getNewsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getNews success function*/
        function getNewsSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }

    /*
     * @purpose: get company details from company house
     * @created: 9th May 2018
     * @params: companyId
     * @return: success, error functions
     * @author: Prasanthi
     */

    function getInfoCompanyHouse(companyId) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'tuna/getComapniesHouse/' + companyId + "?token=" + $rootScope.ehubObject.token;
        var request = $http({
            method: 'GET',
            url: apiUrl
        });
        request
            .then(getInfoCompanyHouseSuccess)
            .catch(getInfoCompanyHouseError);

        /*getInfoCompanyHouse error function*/
        function getInfoCompanyHouseError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getInfoCompanyHouse success function*/
        function getInfoCompanyHouseSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }

    /*
     * @purpose: update answer
     * @created: 11th May 2018
     * @params: id, answer
     * @return: success, error functions
     * @author: Prasanthi
     */

    function updateAnswer(data) {
        var defer = $q.defer();

        var apiUrl = EHUB_API + 'documentParser/updateAnswer?token=' + $rootScope.ehubObject.token;
        var request = $http({
            method: 'POST',
            url: apiUrl,
            data: data
        });
        request
            .then(updateAnswerSuccess)
            .catch(updateAnswerError);

        /*updateAnswer error function*/
        function updateAnswerError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*updateAnswer success function*/
        function updateAnswerSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }
    /*
     * @purpose: create Case From Questioner
     * @created: 25th Jun 2018
     * @params: params, file
     * @return: success, error functions
     * @author: Swathi
     */

    function createCaseFromQuestioner(params, file) {
        var defer = $q.defer();
        var apiUrl = EHUB_API + 'case/createCaseFromQuestioner?token=' + $rootScope.ehubObject.token;
        var request = Upload.upload({
            method: 'POST',
            url: apiUrl,
            params: params,
            timeout: defer.promise,
            data: file
        });
        request
            .then(createCaseFromQuestionerSuccess)
            .catch(createCaseFromQuestionerError);
        setTimeout(function(){
        	if(!success){
        		 defer.resolve('Cancelled');
        	}
        },600000);
        var success;
        /*create Case From Questioner error function*/
        function createCaseFromQuestionerError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
        	if(response.status == -1){
        		defer.reject(response);
        	}else{
        		defer.reject(response.data.message);
        	}
            
        }

        /*create Case From Questioner success function*/
        function createCaseFromQuestionerSuccess(response) {
        	success = true ;
            defer.resolve(response);
        }
        return defer.promise;
    }
    
    /*
     * @purpose: download Questionnaire
     * @created: 25th Jun 2018
     * @params: params
     * @return: success, error functions
     * @author: Swathi
     */

    function downloadQuestionnaire(params) {
        var defer = $q.defer();
        var apiUrl = EHUB_API + 'case/downloadQuestioner?token=' + $rootScope.ehubObject.token;
        var request = $http({
            method: 'GET',
            url: apiUrl,
            params: params,
            dataType : "binary",
            processData : false,
            responseType : 'arraybuffer'
        });
        request
            .then(downloadQuestionnaireSuccess)
            .catch(downloadQuestionnaireError);

        /*download Questionnaire error function*/
        function downloadQuestionnaireError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*download Questionnaire success function*/
        function downloadQuestionnaireSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }
    /*
     * @purpose:caseListForUnderwriting
     * @created: 25th Jun 2018
     * @params: params
     * @return: success, error functions
     * @author: Swathi
     */

    function caseListForUnderwriting(params) {
        var defer = $q.defer();
        var apiUrl = EHUB_API + 'investigation/caseListForUnderwriting?token=' + $rootScope.ehubObject.token;
        var request = $http({
            method: 'GET',
            url: apiUrl,
            params: params
        });
        request
            .then(caseListForUnderwritingSuccess)
            .catch(caseListForUnderwritingError);

        /*caseListForUnderwriting error function*/
        function caseListForUnderwritingError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*caseListForUnderwriting success function*/
        function caseListForUnderwritingSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }
}