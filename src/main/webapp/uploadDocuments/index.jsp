<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="uploadDocumentsApp">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="../assets/images/logo-new.png" />
		<title ng-bind="underwritingName"></title>

		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="./assets/css/upload-documents-vendor.min.css" rel="stylesheet" type="text/css" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.2/css/simple-line-icons.css" rel="stylesheet" type="text/css">
		<!-------------------------------  Vendor Styles Ends  --------------------------------->
		
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="./assets/css/upload-documents-styles.min.css" rel="stylesheet" type="text/css" />
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	
	<body ng-cloak>
		 <flash-message class="main-flash-wrapper"></flash-message>
		 <div ng-show="topPanelPreview">
			<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
		 </div>   
		<div ui-view></div>
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
		<script src='../vendor/jquery/js/jquery.min.js'></script>
        <script src='../vendor/jquery/js/jquery-ui.js'></script>
        <script src="../vendor/jquery/js/jquery.mousewheel.min.js"></script>
        <script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
        <script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
        <script src='../vendor/jquery/js/bootstrap.min.js'></script>
        <script src='../vendor/jquery/js/jquery.ui.widget.js'></script>
        <script src='../vendor/jquery/js/jquery.iframe-transport.js'></script>
        <script src='../vendor/jquery/js/jquery.fileupload.js'></script>
        <script src='../vendor/jquery/js/moment.min.js'></script>
		<script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
		<script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
	 	<script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
        <script src="../vendor/angular/js/FileSaver.min.js"></script>
        <script src='../charts/d3.v3.min.js'></script>
		<script src='../charts/d3.v4.min.js'></script>
        <script src='../charts/d3v3.js'></script>
        <script src='../charts/d3js.js'></script>

        <script src='../vendor//angular/js/angular.min.js'></script>
        <script src='../vendor//angular/js/angular-ui-router.min.js'></script>
        <script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
        <script src='../vendor//angular/js/html2canvas.js'></script>
		<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
		<script src="../vendor/angular/js/lodash.min.js"></script>
        <script src='../vendor//angular/js/angular-flash.min.js'></script>
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		
		<!----------------------------  Initial Configuration JS Starts  ------------------------------->
		<script src='../scripts/app.js'></script>
		<script src="uploadDocuments.app.js"></script>
		<script src="uploadDocuments.config.js"></script>
		<script src="uploadDocuments.run.js"></script> 
		<!----------------------------  Initial Configuration JS Ends  ------------------------------->
		
		<!--------------------------------  Common Scripts Js Starts  ------------------------------>
		<script src='../scripts/common/constants/app.constant.js'></script>
    	<script src='../scripts/common/constants/common.constant.js'></script>
    	<script src='../scripts/discover/constants/discover.constant.js'></script>
    	<script src='../scripts/act/constants/act.constant.js'></script>
    	
		<script src="../scripts/common/services/upload.file.service.js"></script>
		<script src="../scripts/common/services/common.service.js"></script>
		<script src="../scripts/common/services/shared.service.js"></script>
		<script src="../scripts/common/services/top.panel.api.service.js"></script>
		<script src='../scripts/common/services/riskScore.service.js'></script>
		<script src='../scripts/discover/services/discover.api.service.js'></script>
    	<script src='../scripts/act/services/act.api.service.js'></script>
    	<script src='../scripts/act/services/act.graph.service.js'></script>
    	
    	
    	<script src='../scripts/common/js/submenu.controller.js'></script>
    	<script src='../scripts/common/js/top.panel.controller.js'></script>
    	<script src='../scripts/common/js/advanced.search.controller.js'></script>
    	<script src='../scripts/common/js/user.events.controller.js'></script>
    	<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
    	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
    	<script src="../scripts/common/js/notification.controller.js"></script>
    	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
    	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
    	<script src="../scripts/common/modal/js/create.event.controller.js"></script>
    	<script src="../scripts/common/modal/js/participants.event.controller.js"></script>
    	<script src="../scripts/common/modal/js/confirmationForDocparserPage.modal.js"></script>
		<!--------------------------------  Common Scripts Js Ends  ------------------------------>
		
		<!--------------------------------  Upload documents Scripts Js Starts  ------------------------------>
		<script src="scripts/uploadDocuments.controller.js"></script>
		<script src="scripts/analyzeDocuments.controller.js"></script>
		<script src="modal/controllers/news.controller.js"></script>
		<script src="services/uploadDocuments.api.service.js"></script>
		<script src="../scripts/enrich/services/enrich.api.service.js"></script>
		<!--------------------------------  Upload documents Scripts Js Ends  ------------------------------>
		
		
		
		<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/

		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
		<!-- <script src="../vendor/common-vendor.min.js"></script> -->
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->

		<!----------------------------  Initial Configuration JS Starts  ------------------------------->
<!-- 			<script src='../scripts/app.js'></script>
			<script src="uploadDocuments.app.js"></script>
			<script src="uploadDocuments.config.js"></script>
			<script src="uploadDocuments.run.js"></script>
			<script src='../scripts/common/constants/app.constant.js'></script> -->
		<!----------------------------  Initial Configuration JS Ends  ------------------------------->

		<!--------------------------------  Common Scripts Js Starts  ------------------------------>
		<!-- <script src="../scripts/common-scripts.min.js"></script> -->
		<!--------------------------------  Common Scripts Js Ends   ------------------------------->
		
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
		<!-- <script src="./assets/minifiedJs/upload-documents-scripts.min.js"></script> -->
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->


        <!--new minified Js start -->
		<!-- <script src="../vendor/common.vendor.new.min.js"></script>
		<script src="./assets/minifiedJs/uploadDocuments.vendor.new.min.js"></script>

		<script src="./assets/minifiedJs/uploadDocuments.intialize.new.min.js"></script>

		<script src="../scripts/common.scripts.new.min.js"></script>
		<script src="./assets/minifiedJs/uploadDocuments.scripts.new.min.js"></script>

		<script src="./assets/minifiedJs/uploadDocuments.moduleScripts.new.min.js"></script> -->
		<!--new minified Js end -->

<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->
		

	</body>
</html>
