'use strict';
/*jshint unused:false*/
angular.module('uploadDocumentsApp')
	.run(['$rootScope', 'UploadFileService', 'HostPathService', '$location', '$state', '$http', 'EHUB_FE_API', '$timeout', '$stateProvider', '$urlRouterProvider', function ($rootScope, UploadFileService, HostPathService, $location, $state, $http, EHUB_FE_API, $timeout, $stateProvider, $urlRouterProvider) {
		var userDetails = getAgainTokenBySession();
		$rootScope.ehubObject = userDetails;
		window.appName = 'uploadDocuments';
		HostPathService.setHostPath($location.absUrl());
		var rootPath = HostPathService.getHostPath();
		$rootScope.hyperLinksNewTab = true;
		$rootScope.$on('$stateChangeStart', function (event, toState,
			fromState) {
			$(".Bubble_Chart_tooltip").css("display", "none");
			if (userDetails.fullName === "DataEntry User") {
				if (toState.name == 'dataCuration' || toState.name == 'addDataCuration') {
				} else {
					$location.path('/404');
				}
			}
		});

		/*
		 * @purpose: Get system settings
		 * @created: 10 Aug 2018
		 * @params: params
		 * @return: no
		 * @author: Prasanthi
		*/
		var url;
		function getSystemSettings() {

			UploadFileService.getGeneralSettings().then(function (response) {
				if (response.data && response.data['General Settings']) {
					angular.forEach(response.data['General Settings'], function (val, k) {
						if (!val.options && val.name == "Open link in a new tab") {
							$rootScope.hyperLinksNewTab = val.selectedValue == "On" ? true : false;
						}
					});
				}
			}, function (error) {

			});
		}
		getSystemSettings();
		/*
		 * @purpose: Get Submenu Data
		 * @created: 23rd April 2018
		 * @params: params
		 * @return: no
		 * @author: Zameer
		*/
		var url;
		function getSubmenu() {
			if (window.location.hash.indexOf("#!/") < 0 && window.location.hash.indexOf("#") >= 0)
				{url = 'scripts/common/data/submenu.json';}
			else
				{url = '../scripts/common/data/submenu.json';}
			UploadFileService.getSubmenuData(url).then(function (response) {
				if ($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)) {
					angular.forEach(response.data.dashboarDropDownMenuItems, function (val, key) {
						if (val.menu == "Manage") {
							angular.forEach(val.content, function (v, k) {
								if (v.name == "System Settings" && $rootScope.ehubObject.adminUser) {
									v.disabled = "no";
								}
								if (v.name == "Document Parsing") {
									v.disabled = "no";
								}
								if(v.name == "Source Management"  && $rootScope.ehubObject.adminUser){
									v.disabled ="no";
								}
								if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
									v.disabled ="yes";
								}
							});
						}
					});
				}
				HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
				HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
			}, function (error) {
				HostPathService.FlashErrorMessage('ERROR', error.responseMessage);
			});
		}
		getSubmenu();

		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			if (toState.name == '404') {
				$rootScope.topPanelPreview = false;
			} else {
				if ($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== "")
					{$rootScope.topPanelPreview = true;}
				if (toState.name == 'uploadDocuments') {
					$rootScope.underwritingName = 'Underwriting';
				}
			}
		});

		$stateProvider
			.state('uploadDocuments', {
				url: '/landing',
				templateUrl: function () {
					return EHUB_FE_API + appName + '/views/uploadDocument.jsp';
				},
				controller: 'UploadDocumentsController'
			}).state('analyzeDetails', {
				url: '/analyzeDetails/{fileId}',
				templateUrl: function () {
					return EHUB_FE_API + appName + '/views/analyzeDetails.jsp';
				},
				controller: 'AnalyzeDocumentsController'
			})
			.state('404', {
				url: '/404',
				templateUrl: function () {
					return EHUB_FE_API + 'error';
				}
			});
		$urlRouterProvider.when('', '/landing');
		$urlRouterProvider.otherwise(function () {
			return '/404';
		});
	}]);