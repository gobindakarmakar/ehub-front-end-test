"use strict";

angular.module('uploadDocumentsApp')
	.controller('UploadDocumentsController', uploadDocumentsController);

uploadDocumentsController.$inject = [
	'$scope',
	'$rootScope',
	'TopPanelApiService',
	'UpoadDocumentService',
	'HostPathService',
	'$timeout',
	'$state',
	'EnrichApiService',
	'EHUB_FE_API',
	'$uibModal'
];

function uploadDocumentsController(
	$scope,
	$rootScope,
	TopPanelApiService,
	UpoadDocumentService,
	HostPathService,
	$timeout,
	$state,
	EnrichApiService,
	EHUB_FE_API,
	$uibModal) {
	$scope.statusesInfo = {
		"1": "Fresh",
		"2": "Submitted",
		"3": "Acknowledge",
		"4": "Accepted",
		"5": "Pause",
		"6": "Rejected",
		"7": "Focus",
		"8": "Resolved",
		"9": "Canceled"
	};
	$scope.token = $rootScope.ehubObject.token;
	$scope.userID = $rootScope.ehubObject.userId;
	$scope.uploadDocumentUploadedFile = null;
	$scope.uploadDocumentFileTitle = '';
	$scope.uploadDocumentRemarks = '';
	$scope.uploadquestionaire = {
		picFile: '',
		token: '',
		createcaseParams: {
			'token': ''
		},
		questionaireStatus: false,
		uploadspinner: false,
		filesize: []
	};
	$scope.uploadquestionaire.token = $rootScope.ehubObject.token;
	$scope.uploadquestionaire.createcaseParams.token = $rootScope.ehubObject.token;

	/*
	 * @purpose: upload Document
	 * @created: 24th April 2018
	 * @params: file(object)
	 * @returns: no
	 * @author: zameer
	 */

	/* for drag n drop */
	$scope.$watch('filess', function () {
		if ($scope.filess) {
			$scope.uploadDocumentOnSubmit($scope.filess);
		}
	});
	/* for drag n drop end */

	$scope.files = [];

	var cancelUpload = null;
	// Abort the current request (if its running).
	$scope.cancelRequest = function (idIndex, $event, file) {
		$("#progressBar" + idIndex).width(75);
		$("#percent" + idIndex).html('Cancelled');
		$event.stopPropagation();
		$scope.files.shift(file);
		HostPathService.FlashSuccessMessage('UPLOAD CANCELLED', '');
		return (cancelUpload && cancelUpload.abort());
	};

	$scope.uploadDocumentOnSubmit = function (files) {
		fileCount = 0;
		if (files && files.length) {
			files.forEach(function (file) {
				$scope.uploadDocument(file);
			});
		} else {
			$scope.uploadDocument(files);
		}
	};
	/*
	 * @purpose: upload Document
	 * @created: 25th Jun 2018
	 * @params: file(object)
	 * @author: swathi
	 */
	var fileCount = 0;
	$scope.uploadDocument = function (files) {
		fileCount++;
		var currentFile = {};
		$scope.amountCompleted = 0;
		currentFile.uploadedOn = new Date().getTime();
		currentFile.size = (files.size / 1000000).toFixed(3);
		currentFile.docType = (files.name).split('.')[1];
		currentFile.fileStatus = 'Analyzing';
		currentFile.fileName = files.name;
		currentFile.name = '-';
		$scope.docsLoader = true;
		var ext = files.name.split('.').pop().toLowerCase();
		if ($.inArray(ext, ['doc', 'docx', 'pdf', 'csv']) == -1) {//checking type of file
			HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: csv, word, pdf");
			$scope.docsLoader = false;
			return;
		}
		else if (fileCount > 50) {//checking no.of files
			HostPathService.FlashErrorMessage('Number of files should not be more than 50.', '');
			$scope.docsLoader = false;
			return;
		}
		else if (files.size > 10240000) {//checking file size
			HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', files.name);
			$scope.docsLoader = false;
			return;
		}
		else {
			$scope.files.unshift(currentFile);
			var params = {
				"token": $scope.token
			};
			var data = {
				uploadFile: files
			};
			(cancelUpload = UpoadDocumentService.createCaseFromQuestioner(params, data)).then(function (response) {
				if (response == 'Cancelled') {
					$scope.docsLoader = false;
					HostPathService.FlashErrorMessage('Time limit exceed for uploading file.', '');
				} else {
					$scope.getcaseListForUnderwriting();
					HostPathService.FlashSuccessMessage('SUCCESSFULLY UPLOADED DOCUMENT', '');
				}
			}, function (error) {
				$scope.files.map(function (d) {
					if (d.fileStatus == 'Analyzing') {
						d.fileStatus = 'Failed';
					}
				});
				$scope.docsLoader = false;

				if (error.responseMessage === "Document is not matched with any template") {
					var ext = files.name.split('.').pop().toLowerCase();
					if ($.inArray(ext, ['pdf']) == -1) { //checking uploaded file type
						HostPathService.FlashErrorMessage("ERROR", "Document not matched with any template");
						$rootScope.docsLoader = false;
						return;
					}
					else
					{
						$scope.openModal(files);
					}
				} else {
					HostPathService.FlashErrorMessage('ERROR', error.responseMessage);
				}
			}, function () {
				$scope.docsLoader = false;
			});
		}
	};

	/*
	* @purpose: download Document
	* @created: 25th Jun 2018
	* @params: caseId(string)
	* @author: swathi
	*/
	$scope.downloadQuestionnaire = function (caseId, name, type) {/*jshint unused:false*/
		$scope.docsLoader = true;
		var fileTitle = name.split('.')[0];
		var params = {
			'caseId': caseId
		};
		UpoadDocumentService.downloadQuestionnaire(params).then(function (response) {
			$scope.docsLoader = false;
			var blob = new Blob([response.data], {
				type: "application/octet-stream",
			});
			saveAs(blob, name);// jshint ignore:line
			HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title: ' + fileTitle);
		}, function (e) {
			$scope.docsLoader = false;
			HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
		});
	};

	$scope.redirectToact = function (id) {
		window.location.href = EHUB_FE_API + '#/act/' + id;
	};

	$scope.pageNum = 1;
	$scope.recordsPerPage = 6;
	// var loadCaseParams = {
	// 	orderIn: "DESC",
	// 	recordsPerPage: $scope.recordsPerPage,
	// 	pageNumber: $scope.pageNum
	// };

	/*
	* @purpose: on page change
	* @created: 27th Jun 2018
	* @params: page(number)
	* @author: swathi
   */
	$scope.pageChanged = function (page) {
		$scope.pageNum = page;
		var newParams = {
			pageNumber: page,
			recordsPerPage: $scope.recordsPerPage
		};
		if ($scope.searchDocuments && $scope.searchDocuments.length >= 5) {
			newParams = {
				pageNumber: page,
				recordsPerPage: $scope.recordsPerPage,
				keyWord: $scope.searchDocuments
			};
		}
		$scope.casesPerPage(newParams);
	};

	$scope.getcaseListForUnderwriting = function () {
		$scope.docsLoader = true;
		UpoadDocumentService.caseListForUnderwriting().then(function (response1) {
			$scope.totalDocsCount = response1.data.paginationInformation.totalResults;
			if ($scope.totalDocsCount <= 6) {
				$scope.docsLoader = false;
				$scope.files = response1.data.result;
				$scope.files.map(function (v) {
					v.fileStatus = 'Completed';
				});
			} else {
				var newParams = {
					pageNumber: $scope.pageNum,
					recordsPerPage: $scope.recordsPerPage
				};
				$scope.casesPerPage(newParams);
			}
		}, function () {
			$scope.docsLoader = false;
		});
	};
	/*
	* @purpose: get docs per page
	* @created: 27th Jun 2018
	* @params: newParams(object)
	* @author: swathi
   */
	$scope.casesPerPage = function (newParams) {
		UpoadDocumentService.caseListForUnderwriting(newParams).then(function (response) {
			$scope.docsLoader = false;
			$scope.files = response.data.result;
			$scope.files.map(function (v) {
				v.fileStatus = 'Completed';
			});
		}, function () {
			$scope.docsLoader = false;
		});
	};
	$scope.getcaseListForUnderwriting();

	/*
	 * @purpose: search for Documents
	 * @created: 27th Jun 2018
	 * @author: swathi
	 */
	$scope.searchDocs = function () {
		if ($scope.searchDocuments.length >= 5) {
			$scope.docsLoader = true;
			var params = {
				"keyWord": $scope.searchDocuments
			};
			UpoadDocumentService.caseListForUnderwriting(params).then(function (response1) {
				$scope.totalDocsCount = response1.data.paginationInformation.totalResults;
				if ($scope.totalDocsCount <= 6) {
					$scope.docsLoader = false;
					$scope.files = response1.data.result;
					$scope.files.map(function (v) {
						v.fileStatus = 'Completed';
					});
					$scope.pageNum = 1;
				} else {
					$scope.pageNum = 1;
					var newParams = {
						pageNumber: $scope.pageNum,
						recordsPerPage: $scope.recordsPerPage,
						keyWord: $scope.searchDocuments
					};
					$scope.casesPerPage(newParams);
				}
			}, function () {
				$scope.docsLoader = false;
			});
		}
		if ($scope.searchDocuments.length === 0) {
			$scope.getcaseListForUnderwriting();
			$scope.pageNum = 1;
		}
	};
	/*
	 * @purpose: to redirect to corresponding page
	 * @created: 27th June 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: prasanthi
	 */
	$scope.openActorDiscover = function (caseId, docId, status) {
		if (status == 1 || status == 2 || status == 3) {
			var url = window.location.href.split("uploadDocuments/#!")[0] + "#/discover";
		} else {
			var url = window.location.href.split("uploadDocuments/#!")[0] + "#/act/" + caseId;
		}
		if ($rootScope.hyperLinksNewTab) {
			window.open(url, '_blank');
		} else {
			window.open(url, '_self');
		}
	};

	$(document).ready(function () {
		$('tbody#docsList').css("height", "calc( 100vh - 320px)");
		$('tbody#docsList').find('tr').css("width", '100%');
	});
	/*
	* @purpose: to open modal popup To confirm to navigate to docparser page
	* @created: 7th sep 2018
	* @params: params
	* @returns: data
	* @author: prasanthi
	*/
	$scope.openModal = function (data) {
		var dataModal = $uibModal.open({
			templateUrl: function () {
				if (window.location.hash.indexOf("#!/") < 0)
					{return 'scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
				else
					{return '../scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
			},
			controller: 'ConfirmationForDocparser',
			size: 'xs',
			windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',//adding css classes to modal html elements
			resolve: {
				data: function () {
					return data;//passing data to modal controller
				}
			}
		});
		dataModal.result.then(function () { }, function () { });
	};
}