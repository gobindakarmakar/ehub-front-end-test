"use strict";

angular.module('uploadDocumentsApp')
    .controller('AnalyzeDocumentsController', analyzeDocumentsController);
analyzeDocumentsController.$inject = [
    '$scope',
    '$rootScope',
    'TopPanelApiService',
    'HostPathService',
    '$timeout',
    '$state',
    'UpoadDocumentService',
    '$stateParams',
    '$anchorScroll',
    '$location',
    '$uibModal',
    '$sce',
    'EHUB_FE_API'
];

function analyzeDocumentsController(
    $scope,
    $rootScope,
    TopPanelApiService,
    HostPathService,
    $timeout,
    $state,
    UpoadDocumentService,
    $stateParams,
    $anchorScroll,
    $location,
    $uibModal,
    $sce,
    EHUB_FE_API) {
    var nameOfTheCompany, current_identifier, current_companyId;

    $scope.docDetailsPreloader = false;
    $scope.currentState = $state.current.name;
    $scope.headers = [];
    $scope.digitalAssets = [];
    $scope.digi_obj = {};
    $scope.digiHeaders = [];
    $scope.documentId = $stateParams.fileId;
    /*
     * @purpose: get file Details like size, type etc
     * @created: 1st May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */

    ($scope.fetchFileDetails = function () {
        UpoadDocumentService.getDocumentDetails($scope.documentId).then(function (response) {
            $scope.docDetails = response.data;
            $scope.docDetails.uploadedOn = new Date($scope.docDetails.uploadedOn).toLocaleString().split(',')[0];
            $scope.docDetails.size = ($scope.docDetails.size / 1000000).toFixed(3);
            $scope.docDetails.modifiedOn = new Date($scope.docDetails.modifiedOn).toLocaleString().split(',')[0];
        }, function () {

        });
    })();// jshint ignore:line

    /*
     * @purpose: get all details of a document
     * @created: 1st May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */

    ($scope.fetchDocumentDetails = function () {
        $scope.docDetailsPreloader = true;
        UpoadDocumentService.generateJsonResponse($scope.documentId).then(function (response) {
            $scope.docDetailsPreloader = false;
            $scope.headers = [];
            response.data.shift(); //remove heading from the list 
            $scope.indexesToRemove = [];
            angular.forEach(response.data, function (value) {
                var questionNum = 0;
                //var isParaOrQuestion = false;
                //var currentHeaders = [];
                value.documentContentDto.map(function (ele) {
                    if ((ele.contentType == "HEADING" || ele.contentType == "SUB HEADING") && ele.contentData.text) {
                        ele.contentData.id = $scope.headers.length;
                        $scope.headers.push(ele.contentData.text);
                    }
                    /*if(ele.cont$scope.headersentType == 'SUB PARAGRAPH' || ele.contentType == 'PARAGRAPH'){
                     isParaOrQuestion = true;
                    }*/
                    if (ele.contentType == 'SUB PARAGRAPH' && ele.isQuestion && ele.contentData.answerType == 'TEXT' && ele.contentData.question == "Name") {
                        $scope.searchName = ele.contentData.answer;
                        fetchNews(ele.contentData.answer);
                    }
                    if (ele.contentData && ele.contentData.answerType == 'CHECKBOX') {
                        questionNum = questionNum + 1;
                        ele.contentData.questionNum = questionNum;
                        if (ele.contentData.possibleAnswer) {
                            ele.contentData.possibleAnswer = (ele.contentData.possibleAnswer).toUpperCase().split(',');
                        }
                        if (ele.contentData.answer) {
                            ele.contentData.answer = (ele.contentData.answer).toUpperCase();
                        }
                    }
                    if (ele.contentData && ele.contentData.answerType == 'CHECKBOX' && ele.contentData.answer == 'YES') {
                        ele.contentData.val = "YES";
                    }
                    if (ele.contentData && ele.contentData.answerType == 'CHECKBOX' && ele.contentData.answer == 'NO') {
                        ele.contentData.val = "NO";
                    }
                });
                /*if(!isParaOrQuestion){
                 
                 $scope.indexesToRemove.push(key)
                }else{
                 currentHeaders.map(function(d){
                  $scope.headers.push(d);
                 })
                 
                }*/
            });

            $scope.docsData = response.data;
            //console.log($scope.docsData, '$scope.docsData');

        }, function () {
            $scope.docDetailsPreloader = false;
        });

    })();// jshint ignore:line
    window.loadDocLinkAnalysis = function () {
        var vla_url;
        vla_url = EHUB_FE_API + '#/linkAnalysis?p=doc&q=' + nameOfTheCompany + '&identity=' + current_identifier;
        window.open(vla_url, '_blank');
    };
    /*
     * @purpose: get adverse news without identifier
     * @created: 7th May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */

    $scope.cyberNews = [];

    function fetchAdverseNews(Name, data) {
        $scope.AdverseNewsPreloader = true;
        UpoadDocumentService.getAdverseNews(data).then(function (response) {
            $scope.cyberNews.push(...response.data.hits);
            $scope.totalCyber = response.data.total;
            //console.log($scope.cyberNews,'$scope.cyberNews');
            $scope.cyberNews.map(function (ele) {
                ele.text = ele.text.substring(0, 100) + "...",
                    ele.published = ele.published.split('T')[0],// jshint ignore:line
                    ele.technologies = ele.technologies.length > 0 ? ele.technologies : " - ";// jshint ignore:line
            });
            $scope.AdverseNewsPreloader = false;

        }, function () {
            $scope.AdverseNewsPreloader = false;
        });
    }

    /*
     * @purpose: load more cyber news on scroll
     * @created: 8th May May 2018
     * @params: params
     * @returns: success error functions
     * @author: Zameer
     */
    $scope.count = 1;
    $scope.getCyberNews = function () {
        $scope.count++;
        var data = {
            "class": "Cyber Security",
            "offset": $scope.count,
            "country-code": "GB",
            "size": 30
        };
        fetchAdverseNews($scope.searchName, data);
    };
    /*
     * @purpose: get the identifier tofetch news
     * @created: 1st May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */
    function fetchNews(Name) {
        nameOfTheCompany = Name;
        var data = {
            "class": "Cyber Security",
            "offset": 1,
            "country-code": "GB",
            "size": 30
        };
        fetchAdverseNews(Name, data);

        $scope.docDetailsPreloader = true;
        UpoadDocumentService.getIdentifier(Name).then(function (response) {
            if (response.data.hits.length > 0) {

                getNewsDetails(response.data.hits[0]['@identifier']);
                fetchProfileInfoFromCompanyHouse(response.data.hits[0]['bst:registrationId']);
            } else {
                $scope.docDetailsPreloader = false;
            }
        }, function () {
            $scope.docDetailsPreloader = false;
        });
    }

    /*
     * @purpose: get News tofetch news
     * @created: 1st May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */
    function getNewsDetails(identifier) {
        current_identifier = identifier;
        UpoadDocumentService.getNews(identifier).then(function (response) {
            $scope.digi_obj = {};
            angular.forEach(response.data['digital-assets'], function (value) {
                angular.forEach(value.exposure, function (val) {
                    if ($.inArray(val['class'], $scope.digiHeaders) == -1) {
                        $scope.digiHeaders.push(val['class']);
                    }
                    if (!$scope.digi_obj[val['class']]) {
                        $scope.digi_obj[val['class']] = [];
                    }
                    $scope.digi_obj[val['class']] = $scope.digi_obj[val['class']].concat(val.source_url);

                });
            });

            $scope.digitalAssets = response.data['digital-assets'];
            $scope.totalDigitalAssets = $scope.digitalAssets.length;

            $scope.docDetailsPreloader = false;
        }, function () {
            $scope.docDetailsPreloader = false;
        });
    }
    /*
     * @purpose: scroll to div
     * @created: 1st May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */
    $scope.scrollTODiv = function (index) {
        $(".mainInfoPanel").mCustomScrollbar("scrollTo", "#docContent" + index);
    };
    /*
     * @purpose: make checkbox behave like radio buttons
     * @created: 3rd May 2018 
     * @author: Prasanthi
     */

    $scope.CheckBoxToRadio = function (selectorOfCheckBox) {
        $(selectorOfCheckBox).each(function () {
            $(this).change(function () {
                var isCheckedThis = $(this).prop('checked');
                $(selectorOfCheckBox).prop('checked', false);

                if (isCheckedThis === true) {
                    $(this).prop('checked', true);
                }
            });
        });
    };

    $scope.changeDetails = function () {
        console.log($scope.docsData);
    };
    $scope.sendDetails = function (p, s) {
        $scope.primaryCode = p ? p : " ";
        $scope.secondary = s ? s : " ";
    };

    var div = d3.select("body").append("div").attr("class", "Screening_new_tooltip").style("position", "absolute").style("width", "auto").style("z-index", "99").style("width", "auto").style("min-width", "300px").style("background", "rgb(27, 39, 53)").style("padding", "10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");// jshint ignore:line

    $("body").on("mouseover", ".showTooltip", function () {
        $(".Screening_new_tooltip").css("display", "block");
    }).on("mousemove", ".showTooltip", function (event) {
        $(".Screening_new_tooltip").html('<div class="col-sm-12 pad-x0"><div class="secondary-section"><a class="text-gray-ta" style="">Related ISO Codes: ' + $scope.secondary + '</a></div></div>');
        
        var p = $("#tlTip");
        var position = p.offset();
        var windowWidth = window.innerWidth;// jshint ignore:line
        var tooltipWidth = $(".Screening_new_tooltip").width() + 50;
        var cursor = event.pageX;
        if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
            var element = document.getElementsByClassName("Screening_new_tooltip");
            for (var i = 0; i < element.length; i++) {
                element[i].classList.remove("tooltip-left");
                element[i].classList.add("tooltip-right");
            }
            $(".Screening_new_tooltip").css("left", (event.pageX - 30 - $(".Screening_new_tooltip").width()) + "px");
        } else {
            var element = document.getElementsByClassName("Screening_new_tooltip");
            for (var i = 0; i < element.length; i++) {
                element[i].classList.remove("tooltip-right");
                element[i].classList.add("tooltip-left");
            }
            $(".Screening_new_tooltip").css("left", (event.pageX) + 10 + "px");
        }
        return $(".Screening_new_tooltip").css("top", event.pageY + "px");
    }).on("mouseout", ".showTooltip", function () {
        $(".Screening_new_tooltip").css("display", "none");
    });

    $("body").on("mouseover", ".showIconTooltip", function () {
        $(".Screening_new_tooltip").css("display", "block");
    }).on("mousemove", ".showIconTooltip", function (event) {

        $(".Screening_new_tooltip").css("left", event.pageX + 10 + "px");
        $(".Screening_new_tooltip").css("top", event.pageY - 32 + "px");
    }).on("mouseout", ".showIconTooltip", function () {
        $(".Screening_new_tooltip").css("display", "none");
    });
    /*
     * @purpose: scroll for pop over
     * @created: 8th May 2018 
     * @author: Anil
     */
    $scope.initializeScroll = function () {
        $timeout(function () {
            $("#popOver").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark",
                callbacks: {
                    onTotalScroll: function () {
                        $scope.getCyberNews();
                    }
                }
            });

            $("#DApopOver").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
        }, 0);
    };
    /*
     * @purpose: get company profile info from company house
     *  @created: 9th May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */
    function fetchProfileInfoFromCompanyHouse(companyId) {
        current_companyId = companyId;
        $scope.docDetailsPreloader = true;
        UpoadDocumentService.getInfoCompanyHouse(companyId).then(function (response) {
            $scope.docDetailsPreloader = false;
            var companyName = response.data.company_name;
            var companyFullAddressArr = [response.data.registered_office_address.address_line_1, response.data.registered_office_address.locality, response.data.registered_office_address.postal_code];
            var companyFullAddress = companyFullAddressArr.join(", ");
            var companyAddressLocality = response.data.registered_office_address.locality;
            angular.forEach($scope.docsData, function (value) {
                angular.forEach(value.documentContentDto, function (val) {
                    if (val.isQuestion) {
                        if (val.contentData.question == 'Name' && companyName) {
                            val.contentData.osintVal = companyName;
                            if (val.contentData.answer && val.contentData.answer.toLowerCase() == companyName.toLowerCase()) {
                                val.contentData.hasVerified = true;
                            }
                        }
                        if (val.contentData.question == 'Address' && companyFullAddress) {
                            val.contentData.osintVal = companyFullAddress;
                            if (val.contentData.answer && companyAddressLocality && companyFullAddress.toLowerCase().indexOf(val.contentData.answer.toLowerCase()) >= 0) {
                                val.contentData.hasVerified = true;
                            }
                        }

                    }
                });
            });
        }, function () {
            $scope.docDetailsPreloader = false;
        });
    }
    $scope.ShowIconTooltipData = function (osnit, currentAns) {

        if (currentAns) {
            $(".Screening_new_tooltip").html('<div class="col-sm-12 pad-x0"><div class="primary-section"><p style=""><b>OSNIT Data:</b> ' + osnit + '</p><p style=""><b>Form Data:</b> ' + currentAns + '</p></div></div>');
        } else
            {$(".Screening_new_tooltip").html('<div class="col-sm-12 pad-x0"><div class="primary-section"><p style=""><b>Source:</b> https://beta.companieshouse.gov.uk/company/' + current_companyId + '</p></div></div>');}
    };

    /*
     * @purpose: update answer values
     *  @created: 11th May 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */

    $scope.updateValue = function (newAnswer, doc) {
        console.log(newAnswer, doc);
        $scope.updateDoc = {};
        if (doc.contentData) {
            $scope.updateDoc.id = doc.contentData.answerId;
            $scope.updateDoc.answer = newAnswer.toLowerCase();
            $scope.updateDoc.documentQuestions = {
                id: doc.contentData.questionId
            };
        } else if (doc.contentDataDto) {
            $scope.updateDoc.id = doc.contentDataDto.answerId;
            $scope.updateDoc.answer = newAnswer.toLowerCase();
            $scope.updateDoc.documentQuestions = {
                id: doc.contentDataDto.questionId
            };
        }

        UpoadDocumentService.updateAnswer($scope.updateDoc).then(function () {

        });
    };
}