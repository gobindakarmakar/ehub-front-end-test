<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->
        <!--  Analyze Document Wrapper Starts  -->
        <span ng-show="docDetailsPreloader" class="custom-spinner full-page-spinner ">
            <i class="fa fa-spinner fa-spin fa-2x"></i>
        </span>
        <div class="dashboard-wrapper upload-document-dashboard analyze-details-wrapper bg-dark-grey1">
            <div class="top-details-wrapper pad-15px">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="client-details">
                            <li>
                                <p>Uploaded By:
                                    <span>{{docDetails.uploadedUserName}}</span>
                                </p>
                            </li>
                            <li>
                                <p>Size:
                                    <span>{{docDetails.size}}mb</span>
                                </p>
                            </li>
                            <li>
                                <p>Created On:
                                    <span>{{docDetails.uploadedOn}}</span>
                                </p>
                            </li>
                            <li>
                                <p>Status:
                                    <span>{{docDetails.documentParsingStatus}}</span>
                                </p>
                            </li>
                            <li>
                                <p>Type:
                                    <span>{{docDetails.type}}</span>
                                </p>
                            </li>
                            <li>
                                <p>Last Edited On:
                                    <span>{{docDetails.modifiedOn}}</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-cancel pull-right">reset to default</button>
                        <ul class="list-inline switch-list width-25 pull-left">
                            <li class="text-grey width-100 mar-t0">
                                <a href="javascript:void(0);">
                                    <label class="switch custom-switch-wrapper">
                                        <input type="checkbox" ng-model="showIso" class="custom-switch-checkbox" />
                                        <span class="slider round">
                                            <span class="iso-codes pull-left left" ng-show="showIso">NO</span>
                                            <span class="iso-codes pull-right right " ng-hide="showIso">YES</span>
                                        </span>
                                    </label>

                                </a>
                                <p class="inline mar-t5">SHOW ISO</p>

                            </li>
                        </ul>
                        <div class="bottom-pop">

                            <button class="width-40 btn btn-cancel " ng-if= "totalDigitalAssets > 0" ng-click="initializeScroll()" popover-trigger="'outsideClick'" popover-placement="bottom" uib-popover-template="'digitalAssestsTemplate.html'"
                                popover-title="{{dynamicPopover.title}}" id="assetsBtn">Online Presence
                                <span class="pull-right bg-blue ">{{totalDigitalAssets}}</span>
                            </button>
                                <button class="width-40 btn btn-cancel " ng-if= "totalDigitalAssets ==0" ng-click="initializeScroll()" popover-trigger="'outsideClick'" popover-placement="bottom" uib-popover-template=""
                                popover-title="{{dynamicPopover.title}}" id="assetsBtn">Online Presence
                                <span class="pull-right bg-blue ">{{totalDigitalAssets}}</span>
                            </button>
                            <button class="width-50 btn btn-cancel " ng-if= "totalCyber > 0" ng-click="initializeScroll()" popover-trigger="'outsideClick'" popover-placement="bottom" uib-popover-template="'cyberNewsTemplate.html'"
                                popover-title="{{dynamicPopover.title}}" id="cyberBtn">Cyber Security News
                                <span class="pull-right bg-blue ">{{totalCyber}}</span>
                            </button>
                        
                            <button class="width-50 btn btn-cancel " ng-if= "totalCyber ==0" ng-click="initializeScroll()" popover-trigger="'outsideClick'" popover-placement="bottom" uib-popover-template=""
                                popover-title="{{dynamicPopover.title}}" id="cyberBtn">Cyber Security News
                                <span class="pull-right bg-blue ">{{totalCyber}}</span>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="bottom-analyze-wrapper" id="tlTip">
                <div class="row custom-row">
                    <div class="col-sm-4 height-100 custom-col left-col">
                        <!-- Segment Panel Starts -->
                        <div class="analyze-panel-wrapper height-100">
                            <div class="panel height-100 custom-panel-wrapper ">
                                <div class="panel-heading  ">
                                    <h3>Table Of Content</h3>
                                </div>
                                <div class="panel-body height-85 panel-scroll">
                                    <a ng-repeat="header in headers" ng-click="scrollTODiv($index)">
                                        <p class="content-heading text-dark-blue text-uppercase ">{{ header }}</p>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                    </div>
                    <div class="col-sm-8 custom-col right-col mainInfoPanel">
                        <div class="analyze-panel-wrapper" ng-repeat="(mainkey,docSection) in docsData">
                            <div class="panel custom-panel-wrapper ">
                                <div class="panel-body">
                                    <div ng-repeat="(key,doc) in docSection.documentContentDto">
                                        <div class="panel-heading  " ng-if="doc.contentType == 'HEADING' || doc.contentType == 'SUB HEADING'">
                                            <h3 id="docContent{{ doc.contentData.id }}" class="heading-wrapper">{{doc.contentData.text}}</h3>
                                        </div>
                                        <div class="paragraph-wrapper text-uppercase text-grey-ta" ng-if="doc.contentType != 'HEADING'">

                                            <p ng-if="doc.contentType == 'PARAGRAPH'" class="paragraph-content text-uppercase">{{doc.contentData.text}}</p>

                                            <!--  question content starts-->
                                            <div ng-if=" doc.isQuestion && doc.contentData.answerType == 'CHECKBOX'" class="question-wrapper checkbox-wrapper-main{{mainkey}}-sub{{key}}">
                                                <p class="question-content d-ib">
                                                    <span class="iso-codes showTooltip" ng-mouseover="sendDetails(doc.contentData.primaryIsoCode, doc.contentData.secondaryIsoCodes)"
                                                        ng-if="showIso">{{ doc.contentData.primaryIsoCode || 'A8.3.1'}}</span>
                                                    <span class="question-numbers">
                                                        {{doc.contentData.questionNum}})</span> {{doc.contentData.question}}
                                                    <span class="iso-codes bg-dark-green-risk" ng-if="doc.contentData.riskScore<40">{{ doc.contentData.riskScore }}</span>
                                                     <span class="iso-codes iso-codes-medium bg-dark-orange-risk" ng-if="doc.contentData.riskScore>=40 && doc.contentData.riskScore<80">{{ doc.contentData.riskScore }}</span>
                                                      <span class="iso-codes bg-dark-red-risk"  ng-if="doc.contentData.riskScore>=80">{{ doc.contentData.riskScore }}</span>
                                                </p>
                                                <ul class="list-inline width-a custom-list action-icon-list ">
                                                    <!--                                                         <li> -->
                                                    <!--                                                             <i class="fa fa-check-circle  text-dark-green2"></i> -->

                                                    <!--                                                         </li> -->

                                                    <!--                                                         <li> -->
                                                    <!--                                                             <i class="fa fa-info-circle  text-light-orange"></i> -->

                                                    <!--                                                         </li> -->

                                                    <!--                                                         <li> -->
                                                    <!--                                                             <i class="icon-globe icons text-dark-green2"></i> -->

                                                    <!--                                                         </li> -->
                                                    <li>
                                                        <i class="icon-globe icons disabled"></i>

                                                    </li>
                                                </ul>

                                                <div ng-repeat="answer in doc.contentData.possibleAnswer" class="answer-wrapper">
                                                    <span class="answer-content">{{answer.toUpperCase()}}</span>
                                                    <div class="custom-checkbox inline">
                                                        <label class="checkbox-inline">
                                                            <input ng-click="CheckBoxToRadio('.checkbox-wrapper-main'+mainkey+'-sub'+key+' .chs'); updateValue(answer, doc)" type="checkbox" ng-value="answer"
                                                                class="chs" ng-checked="doc.contentData.answer === answer">
                                                            <span>
                                                                <i class="fa fa-check"></i>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <!--  icons panel start -->

                                                <div class="icons-wrapper pull-right">

                                                </div>


                                                <!--  icons panel end -->
                                            </div>
                                            <div ng-if=" doc.isQuestion && doc.contentData.answerType == 'TEXT'" class="clearfix">
                                                <span class="width-20 text-overflow">
<!--                                                     <span ng-mouseover="sendDetails(doc.contentData.primaryIsoCode, doc.contentData.secondaryIsoCodes)" tooltip-placement="right" -->
<!--                                                         class="iso-codes showTooltip" ng-if="showIso">{{ doc.contentData.primaryIsoCode || 'A19.1.1' }}</span> -->
                                                        {{doc.contentData.question}}
                                                        </span>
                                                <span class=" width-40">
                                                    <div class="input-group width-100">
                                                        <input type="text" ng-blur="updateValue(doc.contentData.answer, doc);" class="form-control mar-l15 custom-input pad-x5 text-gray-ta" ng-model="doc.contentData.answer" />

                                                    </div>
                                                </span>
                                                <span class="width30">
                                                    <ul class="list-inline custom-list action-icon-list ">
                                                       <li ng-mouseover = "ShowIconTooltipData(doc.contentData.osintVal, doc.contentData.answer)" class= "showIconTooltip" ng-if="doc.contentData.osintVal && doc.contentData.hasVerified">
                                                           <i class="fa fa-check-circle  text-dark-green2"></i>

                                                       </li>

                                                       <li ng-mouseover = "ShowIconTooltipData(doc.contentData.osintVal, doc.contentData.answer)" class= "showIconTooltip" ng-if="doc.contentData.osintVal && !doc.contentData.hasVerified" >
                                                           <i class="fa fa-info-circle  text-light-orange"></i>

                                                       </li>

                                                       <li ng-mouseover = "ShowIconTooltipData(doc.contentData.osintVal)" class= "showIconTooltip" ng-if="doc.contentData.osintVal">
                                                           <i class="icon-globe icons text-dark-green2"></i>

                                                       </li>
                                                        <li ng-if="!doc.contentData.osintVal">
                                                            <i class="icon-globe icons disabled"></i>

                                                        </li>
                                                        <!-- <li ng-if="doc.contentData.question == 'Website URLs:'">
                                                            <i class="fa fa-newspaper-o text-dark-green2 showTooltip" uib-tooltip-template="'myTooltipTemplate.html'" tooltip-placement="right" tooltip-trigger="'click'"></i>
                                                            
                                                       </li> -->
                                                    </ul>
                                                </span>
                                            </div>
                                            <!--  question content ends-->
                                            <p ng-if="doc.contentType == 'SUB PARAGRAPH' && !doc.isQuestion">{{doc.contentData.text}}</p>

                                            <textarea rows="3" cols="100" ng-if="doc.contentType == 'TEXTAREA' " class="border-grey-thin width-70 pad-x5 custom-text-area"
                                                ng-model="doc.contentData.answer"></textarea>



                                            <div ng-if="doc.contentType == 'TABLE'" class="custom-data-table-wrapper">
                                                <table class="table border-b0" role="grid">
                                                    <!-- <thead>
                                                        <tr role="row" ng-repeat="rows in doc.contentDataTable.dataTableRowsDto" ng-if="$index ==0 && doc.contentDataTable.tableHeader">
                                                            <th class="text-left sorting" rowspan="1"  ng-repeat="col in rows.dataTableColumsDto" colspan=" {{ col.contentDataDto.columnSpan || 1 }}"> {{ col.contentDataDto.question || "" }} </th>
                                                        </tr>
                                                    </thead> -->
                                                    <tbody>
                                                        <tr role="row" ng-repeat="rows in doc.contentDataTable.dataTableRowsDto" ng-class="{'bg-th' :(doc.contentDataTable.tableHeader && doc.contentDataTable.tableHeaderPostions.indexOf($index)!= -1)}">
                                                            <td ng-repeat="col in rows.dataTableColumsDto" colspan=" {{ col.contentDataDto.columnSpan || 1 }}">
                                                            	<span class="iso-codes showTooltip" ng-mouseover="sendDetails(col.contentDataDto.primaryIsoCode, col.contentDataDto.secondaryIsoCodes)"
                                                        			ng-class="{'risk-score' : ((!col.contentDataDto.columnSpan  || col.contentDataDto.columnSpan==1) && col.contentDataDto.question.length >10 && rows.dataTableColumsDto.length > 5)}"ng-if="showIso && col.contentDataDto.question && col.contentDataDto.answerType == 'TEXT'">{{ col.contentDataDto.primaryIsoCode || 'A8.3.1'}}</span>
                                                                <span ng-if="!col.contentDataDto.answerId " class="no-wrap">{{ col.contentDataDto.question || col.contentDataDto.answer}}</span>
                                                                <input type="text" ng-blur="updateValue(col.contentDataDto.answer, col);" class="custom-input text-left" ng-if="col.contentDataDto.answerId  && col.contentDataDto.answerType == 'TEXT'" ng-model="col.contentDataDto.answer" value="{{ col.contentDataDto.answer }}">
                                                                </span>
                                                                <div class="answer-wrapper" ng-if="col.contentDataDto.answerId  && col.contentDataDto.answerType == 'CHECKBOX'">

                                                                    <div class="custom-checkbox inline">
                                                                        <label class="checkbox-inline">
                                                                            <input type="checkbox" ng-value="col.contentDataDto.answer" ng-checked="col.contentDataDto.answer == 'empty'?false:true">
                                                                            <span>
                                                                                <i class="fa fa-check"></i>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- Segment Panel Ends -->
                </div>
            </div>
        </div>
        </div>
        <script type="text/ng-template" id="cyberNewsTemplate.html">
            <div class="panel bg-transparent  custom-panel-wrapper" id="popOver" ng-if="cyberNews.length> 0">
               
                <div class="panel-body pad-x15 pad-b15" ng-repeat="news in cyberNews">
                    <a href="{{news.url}}" target="_blank">
                        <p class="content-heading text-uppercase text-dark-blue">{{ news.title }}</p>

                    </a>
                    <p>Technology: {{news.technologies[0].type}}</p>
                    <p>{{news.text}}</p>
                    <p>{{news.published}}</p>
                    <p>{{news.location.name}}</p>
                </div>
			<span ng-show="AdverseNewsPreloader" class="custom-spinner">
            		<i class="fa fa-spinner fa-spin fa-2x"></i>
        		</span>
            </div>
        </script>

        <script type="text/ng-template" id="digitalAssestsTemplate.html">
            <div class="panel tab-wrapper custom-uib-tab-wrapper bg-transparent  custom-panel-wrapper" ng-if="digiHeaders.length>0">
               <uib-tabset active="active">
    				<uib-tab ng-repeat="tab in digiHeaders" heading="{{tab}}">
      					<div class="panel-body pad-10px" id="DApopOver">
                    		<p ng-repeat="source in digi_obj[tab] track by $index"><a href="{{source}}" class=" text-dark-blue" target="_blank">{{source}}</a></p>
                		</div>
    				</uib-tab>
  				</uib-tabset>
            </div>
        </script>



        <script>
            //             $(".analyze-panel-wrapper").mCustomScrollbar({
            //                 axis: "y",
            //                 theme: "minimal-dark"
            //             });
            $(".mainInfoPanel").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });

            $(".panel-scroll").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });

             $(document).ready(function () {
                    setTimeout(function () {
                        $(".bottom-pop .uib-tab.nav-item").mThumbnailScroller({
                            axis: "x"
                        });
                    }, 500);
                });
        </script>