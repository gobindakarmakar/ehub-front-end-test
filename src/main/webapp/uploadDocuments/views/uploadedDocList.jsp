<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
    <!--  SubMenu Ends  -->
    <!--  Upload Document Wrapper Starts  -->
    <div class="dashboard-wrapper upload-document-dashboard upload-landing-wrapper bg-dark-grey1">
        <div class="upload-content-wrapper">
            <span ng-show="addMediaPreloader" class="custom-spinner case-dairy-spinner">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
            </span>
            <h4>Recently Uploaded
            </h4>
        </div>
        <!-- <div class="custom-data-table-wrapper" ng-repeat ="data in fullData.data">
        <p class="paragraph-text" ng-if="data.contentTypeParagraph.contentType == 'PARAGRAPH'">{{data.contentTypeParagraph.content}}</p>
        <p class="paragraph-text sub-text" ng-if="data.contentTypeParagraph.contentType == 'SUB_PARAGRAPH'">{{data.contentTypeParagraph.content}}</p>
        <table class="table" ng-if = "data.contentTypeTable.contentType == 'TABLE'" style="margin-bottom:40px;">
            <tr ng-repeat="tabledata in data.contentTypeTable.content">
                <td ng-repeat = "(k,v) in tabledata.rowWiseAnsweredData">
                    {{getKey(v)}}
                </td>
            </tr>
        </table> 
    </div>  -->
        <div class="upload-document-wrapper">
            <div class="custom-upload-wrapper">
                <div class="row mar-x0">
                    <div class="col-sm-12">
                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper mar-0 upload-panel-wrapper">
                            <div class="panel-body pad-15px" ngf-drop ng-model="filess" ngf-allow-dir="true">                               
                                <div class="upload-table-wrapper custom-data-table-wrapper" ng-model="filess" ngf-allow-dir="true">
                                    <table id="" class="table table-striped border-b0" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                                    Name And Type
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Size
                                                </th>
                                                <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                                    Progress
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Status
                                                </th>

                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="" role="row" class="odd" ng-repeat="file in files track by $index">
                                                <td>
                                                    <p>
                                                        <span>
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        </span>{{file.name}}</p>
                                                </td>
                                                <td>{{file.size}}</td>
                                                <td>
                                                    <ul class="list-unstyled progressbar-list">
                                                        <li class="progressbar-list-item pad-t0">


                                                            <div class="progress progress-light-gray">
                                                                <div class="progress-bar progress-mettise-blue" role="progressbar" aria-valuenow="{{prgrs.loaded}}" aria-valuemin="0" aria-valuemax="{{prgrs.total}}"
                                                                    style="width:{{amountCompleted}}%">
                                                                    <span class="sr-only ">{{prgrs.loaded}}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                </td>
                                                <td>
                                                    {{amountCompleted}}%
                                                </td>
                                                <td>
                                                    <a>
                                                        <i class="fa fa-ban"></i>
                                                    </a>
                                                    <a>

                                                        <i class="fa fa-times" ng-click="abort()"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="bottom-table-wrapper">
                                        <div class="col-sm-8">
                                            <p class="table-text">Total 70 files uploaded</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-primary pull-right btn-update" ngf-select="uploadDocumentOnSubmit($file)">
                                               Analyze
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Upload Document Wrapper Ends  -->