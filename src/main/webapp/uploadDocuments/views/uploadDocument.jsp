<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->
        <!--  Upload Document Wrapper Starts  -->
        <style>
            .p-none {
                pointer-events: none;
            }
        </style>
        <div class="dashboard-wrapper upload-document-dashboard upload-landing-wrapper bg-dark-grey1">
            <div class="custom-spinner full-page-spinner" ng-if="uploadquestionaire.uploadspinner">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <div class="upload-content-wrapper mar-b0 pad-b0">
                <h4 class="mar-y10 text-gray-ta">Select Files to Upload
                </h4>
                <form class="form ng-pristine pad-b0 ng-valid">
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-primary mar-t10 btn-update" ngf-select="uploadDocumentOnSubmit($files)" ngf-multiple="true">
                                <i class="fa fa-upload mar-r5"></i>
                                <span>Upload</span>
                            </button>
                            <!-- <div class="width-40 pull-right">
                            <p class="text-uppercase">Upto 50 files only </p>
                            <p class="mar-b0 text-uppercase">maximum file size:10mb </p>
                        </div> -->
                        </div>

                        <div class="col-sm-6">
                            <label class="control-label pad-t10 width-55 text-right pull-left">Search File :</label>
                            <div class="input-group width-40 pull-right custom-input-group">
                                <span class="input-group-addon pull-right border-0 bg-transparent">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control pull-left ng-pristine inline ng-valid ng-empty ng-touched" id="upload-doc-file-title"
                                    ng-model="searchDocuments" ng-change="searchDocs()">
                            </div>
                            <span class="message-text mar-t5 pull-right text-dark-red" ng-if="searchDocuments.length != 0 && searchDocuments.length < 5">Search keyword length should be atleast 5 characters.</span>
                        </div>
                    </div>
                </form>
            </div>
            <!-- <div class="custom-data-table-wrapper" ng-repeat ="data in fullData.data">
        <p class="paragraph-text" ng-if="data.contentTypeParagraph.contentType == 'PARAGRAPH'">{{data.contentTypeParagraph.content}}</p>
        <p class="paragraph-text sub-text" ng-if="data.contentTypeParagraph.contentType == 'SUB_PARAGRAPH'">{{data.contentTypeParagraph.content}}</p>
        <table class="table" ng-if = "data.contentTypeTable.contentType == 'TABLE'" style="margin-bottom:40px;">
            <tr ng-repeat="tabledata in data.contentTypeTable.content">
                <td ng-repeat = "(k,v) in tabledata.rowWiseAnsweredData">
                    {{getKey(v)}}
                </td>
            </tr>
        </table> 
    </div>  -->
            <div class="upload-document-wrapper">
                <div class="custom-upload-wrapper">
                    <div class="row mar-x0">
                        <div class="col-sm-12">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper mar-0 upload-panel-wrapper">
                                <div class="panel-body pad-15px" ngf-drop ngf-multiple="true" ng-model="filess" ngf-allow-dir="true">
                                    <div class="upload-table-wrapper custom-data-table-wrapper h-450" ng-model="filess" ngf-allow-dir="true">
                                        <table id="" class="table table-scroll twelve-col-unequal table-striped border-b0" role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        Case ID
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        File Name
                                                    </th>
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        Company Name
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Industry
                                                    </th>

                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Broker
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Status
                                                    </th>
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        case Status
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        File Format
                                                    </th>
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        Size (B)
                                                    </th>

                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Assigned to
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Download
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        Uploaded Date
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="docsList" class="mxh-350" ng-if="files.length > 0">

                                                <tr role="row" class="odd" ng-repeat="file in files">

                                                    <td ng-class="{'c-arrow p-none':userID !=file.assigneeId}" ng-click="openActorDiscover(file.caseId, file.docId, file.currentStatus)">
                                                        <p>
                                                            {{file.caseId}}</p>
                                                    </td>
                                                    <td ng-class="{'c-arrow p-none':userID !=file.assigneeId}" ng-click="openActorDiscover(file.caseId, file.docId, file.currentStatus)">
                                                        <p> {{file.fileName}} </p>
                                                    </td>

                                                    <td ng-class="{'c-arrow p-none':userID !=file.assigneeId}" ng-click="openActorDiscover(file.caseId, file.docId, file.currentStatus)">
                                                        <p> {{file.name}} </p>
                                                    </td>

                                                    <td>
                                                        <p> {{file.industry|| '-'}} </p>
                                                    </td>
                                                    <td>
                                                        <p> {{file.broker || '-'}}</p>
                                                    </td>
                                                    <td>
                                                        <span ng-class="file.fileStatus == 'Analyzing' || file.fileStatus == 'Failed' ? 'foo orange': 'foo green'"></span>
                                                        <p style="display: inline;">{{file.fileStatus || '-'}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{statusesInfo[file.currentStatus] }}</p>
                                                    </td>
                                                    <td>
                                                        <p> {{file.docType || '-'}} </p>
                                                    </td>
                                                    <td>
                                                        <p> {{file.size}} </p>
                                                    </td>
                                                    <td>
                                                        <p>{{file.assignedTo}} </p>

                                                    </td>

                                                    <td>
                                                        <button ng-disabled="userID != file.assigneeId" ng-click="downloadQuestionnaire(file.caseId,file.fileName,file.docType)">
                                                            <i class="fa fa-download"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <p>{{file.createdOn | date : 'medium' || '-'}} </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="message-text text-center pad-t10 no-data" ng-if="files.length == 0">
                                            <span>Data Not Found</span>
                                        </div>

                                        <div class="custom-spinner case-dairy-spinner" ng-show="docsLoader">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                    </div>
                                    <div class="text-right" ng-hide="totalDocsCount <= recordsPerPage">
                                        <ul class="upload-pagination-wrapper" uib-pagination total-items="totalDocsCount" items-per-page="recordsPerPage" ng-model="pageNum"
                                            ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  Upload Document Wrapper Ends  -->