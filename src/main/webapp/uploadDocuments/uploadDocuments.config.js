"use strict";

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', '$qProvider', function ($stateProvider, $provide, $urlRouterProvider, EHUB_FE_API, $locationProvider, $qProvider) {
	 $provide.factory('$stateProvider', function () {
		 return $stateProvider;
	 });
 	$provide.factory('$urlRouterProvider', function () {
        return $urlRouterProvider;
    });
	$qProvider.errorOnUnhandledRejections(false);
 
}]);