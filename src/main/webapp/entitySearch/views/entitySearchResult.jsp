<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


    <!--  Landing Dashboard Wrapper Starts  -->
    <div class="dashboard-wrapper ceri-wrapper details-dashboard-wrapper bg-dark-grey1">

        <!--  Search Results Wrapper Starts  -->
        <div class="div-250 mar-t15 clearfix">
           <!--  <div class="custom-spinner full-page-spinner" ng-show="EntityNewSearchObject.fullpageSpinner">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div> -->
            <div class="side-defined pad-x0 col-sm-3">
                <div class="side-panel-wrapper pad-y10">
                    <!-- Segment Panel Starts -->
                    <div class="panel border-0 custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Search Results</h3>
                        </div>
                        <div class="panel-body pad-0" style="position: relative; overflow: visible;">
                            <div class="search-input-wrapper pad-l15 clearfix ">
                                <div class="input-group custom-input-group">
                                    <input type="text" name="" placeholder="SEARCH RESULTS" ng-model="EntityNewSearchObject.SearchFilterEntity" class="w-220 border-blue-thin-b custom-input"
                                        id="attrList" ng-keydown="$event.keyCode === 13 && SearchFilter(EntityNewSearchObject.SearchFilterEntity)">
                                    <span class="input-group-addon right-addon fa fa-times"></span>
                                </div>
                                <a class="btn pull-right text-uppercase mar-r15 pad-5 mar-b5  btn-blue" ng-click="SearchFilter(EntityNewSearchObject.SearchFilterEntity)">update</a>
                            </div>

                            <div class="view-type-wrapper pad-l15 pad-y10 border-dark-grey-thin-b mnh-100px">
                                <div class="heading-wrapper pad-t15">
                                    <h4 class="mar-b10">View Type</h4>
                                </div>
                                <ul class="custom-list pad-l0 item-1">
                                    <li class="pad-r15  mar-y5">
                                        <span class="answer-content ">Case</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showbycase" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                    <li class="pad-r15 mar-y5">
                                        <span class="answer-content">Alerts</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showbyAlerts" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                    <li class="pad-r15 mar-y5">
                                        <span class="answer-content">Entities</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showByentity" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                    <li class="pad-r15 mar-y5">
                                        <span class="answer-content">Network Analysis</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showbyVla" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                    <li class="pad-r15 mar-y5">
                                        <span class="answer-content">News</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showbyNews" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                     <li class="pad-r15 mar-y5">
                                        <span class="answer-content">Lead Generation</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.ShowLeadGeneration" class="custom-switch-checkbox" />
                                            <span class="slider round">

                                            </span>
                                        </label>
                                    </li>
                                    <!--   <li class="pad-r15 mar-y5">
                                        <span class="answer-content">Others</span>
                                        <label class="switch pull-right custom-switch-wrapper search-switch">
                                            <input type="checkbox" ng-model="EntityNewSearchObject.showbyOthers" class="custom-switch-checkbox" />
                                            <span class="slider round">
                                            </span>
                                        </label>
                                    </li> -->
                                </ul>
                            </div>

                        </div>
                    </div>
                    <!-- Segment Panel Ends -->
                </div>
            </div>
            <div class="side-remain col-sm-9">
                <!-- Segment Panel Starts -->
                <h3 class="mar-b0 f-14 text-uppercase text-bright-grey">Displaying {{TempHoold.tempCases.length + totalEnties + EntityNewSearchObject.transcationAlerts.length +TempHoold.tempVLA.length+TempHoold.tempNews.length}}
                    Results </h3>
                <div class="panel p-rel custom-panel-wrapper  segment-panel-wrapper" ng-show="EntityNewSearchObject.showbycase">
                    <div class="panel-heading pad-b0">
                        <h3>Case</h3>
                        <div class="right-heading-content text-right">
                            <span class="mar-r15">Displaying {{EntityNewSearchObject.CaseNum}} out of {{TempHoold.tempCases.length}} results
                            </span>
                            <a class="btn btn-blue" ng-if="TempHoold.tempCases.length > 8" ng-click="EntityNewSearchObject.typeSeeAll(EntityNewSearchObject.casesSeen,'case')"
                                ng-disabled="EntityNewSearchObject.casesSeen ">see all</a>
                        </div>
                    </div>
                    
                    <div class="panel-body mnh-250 pad-t0" style="position: relative; overflow: visible;" ng-show="TempHoold.tempCases.length > 0">
                        <!--  Top Filters Wrapper Starts  -->
                        <div class="top-filters-wrapper segment-filters-wrappers">
                            <ul class="top-filter-navigation list-inline">
                                <li class="top-filter-lists" ng-repeat="caseDetails in  EntityNewSearchObject.case" ng-click="EntityNewSearchObject.redirectToCase(caseDetails,'case')">
                                    <div class="top-filters">
                                        <div class="company-details-wrapper">
                                            <div class="top-content">
                                                <div class="widget-header mar-b0 custom-input-group pad-0 bg-transparent border-0">
                                                    <h3 class="text-overflow mar-b0 text-uppercase mar-b0 f-16 width-90 border-0 c-pointer">{{caseDetails.name}}</h3>
                                                    <!-- <a href="javascript:void(0);" class="dropdown-toggle pad-y0 pad-r0 z-999999" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-v  text-dark-grey ellipsis"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pad-r15 filter-menu">
                                                        <li>
                                                            <a href="javascript:void(0);" class="border-0">
                                                                Rename
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="border-0">
                                                                Clone
                                                            </a>
                                                        </li>

                                                    </ul> -->
                                                </div>

                                                <p>{{caseDetails.caseId}}</p>
                                            </div>
                                            <div class="company-type">
                                                <div class="d-ib icon-wrapper">
                                                    <i class="fa fa-briefcase"></i>
                                                </div>
                                                <!-- <img ng-src="{{caseDetails.thumbnail}}" class="img-responsive" />  -->
                                            </div>
                                            <div class="bottom-content" ng-if="caseDetails.assignedTo">
                                                <div class="image-wrapper" ng-show="caseDetails.editor_img">
                                                    <img ng-src="{{caseDetails.editor_img}}" class="img-responsive" />
                                                </div>
                                                <div class="image-wrapper text-center pad-5" ng-hide="caseDetails.editor_img">
                                                    <span class="fa fa-user f-25">
                                                </div>
                                                <div class="img-content-wrapper">
                                                    <p>Assigned To</p>
                                                    <strong>{{caseDetails.assignedTo}}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="text-right search-pagination" ng-show="EntityNewSearchObject.casesSeen">
                            <ul class="pull-right mar-y0" uib-pagination total-items="TempHoold.tempCases.length" items-per-page="8" ng-model="entitySeen"
                                ng-change="pageChangeCase(entitySeen)" max-size="EntityNewSearchObject.max_size_entites" class="pagination-sm"
                                boundary-link-numbers="true"></ul>
                        </div>
                        <!--  Top Filters Wrapper Ends  -->
                    </div>
                    <div class="custom-spinner pad-t0 height-100  case-dairy-spinner" ng-show="EntityNewSearchObject.fullpageSpinner" >
                        <i class="fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                    <div class=" pad-t0 mnh-250" style="position: relative; overflow: visible;" ng-show="TempHoold.tempCases.length === 0 &&  !EntityNewSearchObject.fullpageSpinner">
                        <span class="text-center no-data-wrapper">No results found</span>
                    </div>
                </div>
                <!-- Segment Panel Ends -->
                <!-- Segment Panel Starts -->

                <div class="panel p-rel custom-panel-wrapper  segment-panel-wrapper" ng-show="EntityNewSearchObject.showByentity">
                    <div class="p-rel panel-body mnh-250 pad-b0 active-text-blue custom-uib-tab-wrapper tab-wrapper">
                        <!--   <ul>
                            <li ng-click="SingleEntity.tabchanged('all')" >All</li>
                            <li ng-click="SingleEntity.tabchanged('company')">Company</li>
                            <li ng-click="SingleEntity.tabchanged('person')">Person</li>
                        </ul> -->
                        <div class="ng-isolate-scope d-ib">
                            <ul class="nav nav-tabs">
                                <li ng-class="{'active': SingleEntity.entityTab === 'all'}">
                                    <a href ng-click="SingleEntity.tabchanged('all')">
                                        <span class="main-nav-texts">All</span>
                                    </a>
                                </li>
                                <li ng-class="{'active': SingleEntity.entityTab === 'company'}">
                                    <a href ng-click="SingleEntity.tabchanged('company')">
                                        <span class="main-nav-texts">Company</span>
                                    </a>
                                </li>
                                <li ng-class="{'active': SingleEntity.entityTab === 'person'}">
                                    <a href ng-click="SingleEntity.tabchanged('person')">
                                        <span class="main-nav-texts">Person</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- Tab for ALL starts  -->
                        <!--     <uib-tabset active="active"> -->
                        <div class="right-heading-content pull-right text-right">
                            <div class="select-group d-ib pad-x0  custom-select-dropdown autodesk-newslist-border-class">
                                <span>Viewed :</span>
                                <!--      <select id="displayByIconOrTable" class="filter-value mar-y0 radius-5 bg-transparent border-blue-thin text-uppercase select-width-class "
                                        ng-model="searchType" ng-change="typeofSearch(searchType)" >
                                        <option ng-repeat=" val in TabsValue" value="{{val}}" >{{val}}</option>
                                    </select> -->
                                <select class="filter-value mar-y0 radius-5 bg-transparent border-blue-thin text-uppercase select-width-class " name="level"
                                    ng-model="filterBY.levels" ng-options="obj.value as obj.label for obj in filterBY.levelsArr"
                                    ng-change="SingleEntity.PaginationEntites(1,filterBY.levels,true)"></select>
                            </div>
                            <span class="mar-x5 f-11 lh-22 text-uppercase">Displaying {{SingleEntity.showingItem }} out of {{ SingleEntity.Entitylength}} results
                            </span>
                            <a class="btn text-uppercase pad-3 pull-right  btn-blue" ng-if="TempHoold.tempEntites.length > 8 " ng-click="SingleEntity.showPagination()">see all</a>
                        </div>
                        <!--  <uib-tab index="0" heading="ALL" id='ALLTab' ng-click="TabSelected('All')"> -->
                        <div class="top-filters-wrapper segment-filters-wrappers">
                            <ul class="top-filter-navigation list-inline ">
                                <li class="top-filter-lists" ng-repeat="entitesDetails in  SingleEntity.type track by $index" ng-if="entitesDetails.name">
                                    <div class="top-filters p-rel entity-panel-wrapper">
                                        <div class="company-details-wrapper">
                                            <div class="top-content">
                                                <div class="widget-header mar-b0 custom-input-group pad-0 bg-transparent border-0">
                                                    <h3 class="text-overflow mar-b0 text-uppercase mar-b0 f-16 width-90 border-0 c-pointer text-center" ng-click="EntityNewSearchObject.redirectTOPage(entitesDetails)">{{entitesDetails.name}}</h3>
                                                </div>
                                                <p class="type-info">Type: {{entitesDetails.type}}</p>
                                            </div>
                                            <div class="company-type" ng-show="entitesDetails.logo">
                                                <img ng-src="{{entitesDetails.logo}}" class="img-responsive"> </div>
                                            <div class="company-type" ng-hide="entitesDetails.logo">
                                                <div ng-if="entitesDetails.type === 'Individual'" class="pad-15px d-ib">
                                                    <i class="fa fa-user" style="font-size: 55px"></i>
                                                </div>
                                                <div ng-if="entitesDetails.type === 'Corporate'" class="pad-15px d-ib">
                                                    <i class="fa fa-building" style="font-size: 55px"></i>
                                                </div>
                                            </div>
                                            <div class="bottom-content">
                                                <p class="f-10 two-line-wrapper text-center mar-t5">{{entitesDetails.adress.fullAddress}}</p>
                                                <a href ng-click="EntityNewSearchObject.redirectTOPage(entitesDetails)" class="info-link">more Info</a>
                                            </div>
                                            <div class="custom-badge-wrapper">
                                                <p class="custom-badge mar-b0" ng-hide="entitesDetails.external">Internal</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <div class=" mnh-250 pad-t0" style="position: relative; overflow: visible;" ng-show ="SingleEntity.type.length === 0 && !EntityNewSearchObject.entitySectionSpinner">
                                    <span class="text-center no-data-wrapper">No results found</span>
                                </div>
                            </ul>
                        </div>
                        <!--   </uib-tab> -->
                        <!-- tab for ALL Ends  -->
                        <div class="custom-spinner  height-100 pad-t0 case-dairy-spinner" ng-show="EntityNewSearchObject.entitySectionSpinner" >
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>                    
                        <!--   </uib-tabset> -->
                        <div class="text-right search-pagination" ng-show="SingleEntity.seeallbutton">
                            <ul class="pull-right mar-y0" uib-pagination total-items="SingleEntity.EntitesNumber" items-per-page="8" ng-model="entitySeen"
                                ng-change="SingleEntity.PaginationEntites(entitySeen,filterBY.levels)" max-size="SingleEntity.numberofPages"
                                class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                    </div>
                </div>

                <!-- Segment Panel Starts -->
                <div class="panel p-rel custom-panel-wrapper  segment-panel-wrapper" ng-show="EntityNewSearchObject.showbyAlerts">
                    <div class="panel-heading pad-b0">
                        <h3>Alerts</h3>
                        <div class="right-heading-content text-right ">
                            <span class="mar-r15">Displaying {{EntityNewSearchObject.AlertPageNum}} out of {{TempHoold.tempAlert.length}}
                                results
                            </span>
                                <a class="btn btn-blue" ng-if="TempHoold.tempAlert.length > 8" ng-click="EntityNewSearchObject.typeSeeAll(EntityNewSearchObject.alertSeen,'transcation')">see all</a>
                        </div>
                    </div>
                    <div class="panel-body mnh-250 pad-t0" style="position: relative; overflow: visible;" ng-show="EntityNewSearchObject.transcationAlerts.length > 0">
                        <div class="row">
                            <div class="col-sm-4" ng-repeat="transcationAlerts in EntityNewSearchObject.transcationAlerts track by $index">
                                <div class="transaction-details-wrapper">
                                    <p class="mar-b5"> {{transcationAlerts.alertDescription}}</p>
                                    <p class="text-uppercase mar-b5">{{transcationAlerts.focalEntityDisplayName}}</p>
                                    <p> {{transcationAlerts.alertBusinessDate | date:'medium'}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="text-right search-pagination" ng-show="EntityNewSearchObject.alertSeen">
                            <ul class="pull-right mar-y0" uib-pagination total-items="TempHoold.tempAlert.length" items-per-page="8" ng-model="alertNum"
                                ng-change="pageChangedAlert(alertNum)" max-size="numberofalerts" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                    </div>
                    <div class=" height-100 custom-spinner pad-t0 case-dairy-spinner" ng-show="EntityNewSearchObject.fullpageSpinner" >
                        <i class="fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                    <div class=" mnh-250 pad-t0" style="position: relative; overflow: visible;" ng-show ="EntityNewSearchObject.transcationAlerts.length === 0 && !EntityNewSearchObject.fullpageSpinner">
                        <span class="text-center no-data-wrapper">No results found</span>
                    </div>
                  
                </div>
                <!-- Segment Panel Ends -->
                <!-- Segment Panel Starts -->
                <div class="panel custom-panel-wrapper p-rel  segment-panel-wrapper" ng-show="EntityNewSearchObject.showbyVla">
                    <div class="panel-heading pad-b0">
                        <h3>Network Analysis </h3>
                        <div class="right-heading-content text-right">
                            <span class="mar-r15">Displaying {{EntityNewSearchObject.pageNum}} out of {{TempHoold.tempVLA.length}} results
                            </span>
                            <a class="btn btn-blue" ng-click="EntityNewSearchObject.typeSeeAll(EntityNewSearchObject.vlaSeen,'vla')" ng-show="TempHoold.tempVLA.length > 8"
                                ng-disabled="EntityNewSearchObject.vlaSeen">see all</a>
                        </div>
                    </div>
                    <div class="panel-body mnh-250 pad-t0" style="position: relative; overflow: visible;">
                        <!--  Top Filters Wrapper Starts  -->
                        <div class="top-filters-wrapper segment-filters-wrappers" ng-show="EntityNewSearchObject.byVladata.length > 0">
                            <ul class="top-filter-navigation list-inline c-pointer">
                                <li class="top-filter-lists" ng-repeat="caseDetails in  EntityNewSearchObject.byVladata" ng-click="EntityNewSearchObject.redirectToCase(caseDetails,'vla')">
                                    <div class="top-filters">
                                        <div class="company-details-wrapper">
                                            <div class="top-content">
                                                <div class="widget-header mar-b0 custom-input-group pad-0 bg-transparent border-0">
                                                    <h3 class="text-overflow mar-b0 text-uppercase mar-b0 f-16 width-90 border-0 c-pointer">{{caseDetails.name}}</h3>
                                                    <!--  <a href="javascript:void(0);" class="dropdown-toggle pad-y0 pad-r0 z-999999" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-v text-dark-grey ellipsis"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pad-r15 filter-menu">
                                                        <li>
                                                            <a href="javascript:void(0);" class="border-0">
                                                                Rename
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="border-0">
                                                                Clone
                                                            </a>
                                                        </li>

                                                    </ul> -->
                                                </div>

                                                <p>{{caseDetails.labelV}}</p>
                                            </div>
                                            <div class="company-type">
                                                <div class="d-ib icon-wrapper">
                                                    <i class="fa fa-code-fork"></i>
                                                </div>
                                                <!-- <img ng-src="{{caseDetails.thumbnail}}" class="img-responsive" /> -->
                                            </div>
                                            <!--   <div class="bottom-content">
                                                <div class="image-wrapper" ng-show="caseDetails.editor_img">
                                                    <img ng-src="{{caseDetails.editor_img}}" class="img-responsive" />
                                                </div>
                                                <div class="image-wrapper image-wrapper text-center pad-5" ng-hide="caseDetails.editor_img">
                                                    <span>
                                                        <i class="fa fa-user f-25"></i>
                                                    </span>
                                                </div>
                                                <div class="img-content-wrapper">
                                                    <p>Last edited by </p>
                                                    <strong>{{caseDetails.editedby_name}}</strong>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="height-100 custom-spinner pad-t0 case-dairy-spinner" ng-show="EntityNewSearchObject.fullpageSpinner" >
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <div class="top-filters-wrapper segment-filters-wrappers" ng-show="EntityNewSearchObject.byVladata.length === 0 && !EntityNewSearchObject.fullpageSpinner">
                            <span class="text-center no-data-wrapper">No results found</span>
                        </div>
                        <div class="text-right search-pagination" ng-show="EntityNewSearchObject.vlaSeen">
                            <ul class="pull-right mar-y0" uib-pagination total-items="TempHoold.tempVLA.length" items-per-page="8" ng-model="pageNum"
                                ng-change="pageChangedVLA(pageNum)" max-size="VlaPagesnum" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                        <!--  Top Filters Wrapper Ends  -->
                    </div>
                </div>
                <div class="panel p-rel custom-panel-wrapper  segment-panel-wrapper" ng-show="EntityNewSearchObject.showbyNews">
                    <div class="panel-heading pad-b0">
                        <h3>News</h3>
                        <div class="right-heading-content text-right">
                            <span class="mar-r15">Displaying {{EntityNewSearchObject.newsnum}} out of {{TempHoold.tempNews.length}} results

                            </span>
                            <a class="btn btn-blue" ng-click="EntityNewSearchObject.typeSeeAll(EntityNewSearchObject.newsseen,'news')" ng-show="TempHoold.tempNews.length > 8"
                                ng-disabled="EntityNewSearchObject.newsSeen">see all</a>
                        </div>
                    </div>
                    <div class="panel-body mnh-250 pad-t0" style="position: relative; overflow: visible;">
                        <!--  Top Filters Wrapper Starts  -->
                        <div class="top-filters-wrapper segment-filters-wrappers" ng-show=" EntityNewSearchObject.newsSection.length > 0">
                            <ul class="top-filter-navigation list-inline ">
                                <li class="top-filter-lists" ng-repeat="newsDetails in  EntityNewSearchObject.newsSection  track by $index" ng-if="newsDetails.title">
                                    <div class="top-filters p-rel">
                                        <div class="company-details-wrapper">
                                            <div class="top-content">
                                                <div class="widget-header mar-b0 custom-input-group pad-0 bg-transparent border-0">
                                                    <h3 class="text-overflow mar-b0 text-uppercase mar-b0 f-16 width-90 border-0 c-pointer text-center">{{newsDetails.title}}</h3>
                                                    <!--    <a href="javascript:void(0);" class="dropdown-toggle pad-y0 pad-r0 z-999999" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-v text-dark-grey ellipsis"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pad-r15 filter-menu">
                                                        <li>
                                                            <a href="javascript:void(0);" class="border-0">
                                                                Rename
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="border-0">
                                                                Clone
                                                            </a>
                                                        </li>

                                                    </ul> -->
                                                </div>
                                                <p class="type-info text-overflow">Date: {{(newsDetails.time || newsDetails.updated_at) |date :'medium'}}</p>
                                            </div>
                                            <div class="company-type">
                                                <div class="d-ib icon-wrapper">
                                                    <i class="fa fa-newspaper-o " style="font-size: 55px"></i>
                                                </div>
                                            </div>
                                            <div class="bottom-content ">

                                                <!--  <div class="image-wrapper" ng-show="caseDetails.editor_img">
                                                        <img ng-src="{{caseDetails.editor_img}}" class="img-responsive" />
                                                    </div> -->
                                                <!--  <div class="image-wrapper image-wrapper text-center pad-5" ng-hide="caseDetails.editor_img">
                                                        <span>
                                                            <i class="fa fa-user f-25"></i>
                                                        </span>
                                                    </div> -->
                                                <div class="pad-t20 text-center">
                                                    <!-- <p>Last edited by </p> -->
                                                    <a href="{{newsDetails.link || newsDetails.url}}" target="_blank" ng-click="" class="info-link">MORE INFO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                        </div>
                        <div class="custom-spinner height-100 pad-t0 case-dairy-spinner" ng-show="EntityNewSearchObject.fullpageSpinner" >
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <div class="top-filters-wrapper height-100 segment-filters-wrappers" ng-show=" EntityNewSearchObject.newsSection.length === 0 && !EntityNewSearchObject.fullpageSpinner">
                            <span class="text-center no-data-wrapper">No results found</span>
                        </div>
                        <div class="text-right search-pagination" ng-show="EntityNewSearchObject.newsSeen">
                            <ul class="pull-right mar-y0" uib-pagination total-items="TempHoold.tempNews.length" items-per-page="8" ng-model="newsNum"
                                ng-change="pageChangedVLA(newsNum)" max-size="numberofNews" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                        <!--  Top Filters Wrapper Ends  -->
                    </div>

                    <!-- Segment Panel Ends -->

                </div>
               <!--  LeadGeneration Wrapper -->
               
                <div class="panel p-rel custom-panel-wrapper  segment-panel-wrapper" ng-show="EntityNewSearchObject.ShowLeadGeneration">
                    <div class="panel-heading pad-b0">
                        <h3>Lead Generation</h3>
                        <div class="right-heading-content text-right">
                            <span class="mar-r15">Displaying {{EntityNewSearchObject.customer_story.length}} out of {{EntityNewSearchObject.customer_story.length}} results

                            </span>
                        </div>
                    </div>
                    <div class="panel-body mnh-250 pad-t0" style="position: relative; overflow: visible;">
                        <!--  Top Filters Wrapper Starts  -->
                        <div class="top-filters-wrapper segment-filters-wrappers">
                            <ul class="top-filter-navigation list-inline ">
                                <li class="top-filter-lists" ng-repeat ="customerData in EntityNewSearchObject.customer_story" ng-click="EntityNewSearchObject.redirectToCustomerPage(customerData)">
                                    <div class="top-filters p-rel">
                                        <div class="company-details-wrapper">
                                            <div class="top-content">
                                                <div class="widget-header mar-b0 custom-input-group pad-0 bg-transparent border-0">
                                                    <h3 class="text-overflow mar-b0 text-uppercase mar-b0 f-16 width-90 border-0 c-pointer text-center">{{customerData.story_name.split('_').join(' ')}}</h3>
                                                <p class="type-info text-overflow">{{customerData.cluster_name.split('_').join(' ')}}</p>
                                            </div>
                                            <div class="company-type">
                                                <div class="d-ib icon-wrapper">
                                                    <i class="fa fa-circle-o  " style="font-size: 55px"></i>
                                                    <i class="fa fa-circle-o  " style="font-size: 20px"></i>
                                                    <i class="fa fa-circle-o  " style="font-size: 30px;position: absolute; top: 60px;"></i>
                                                </div>
                                            </div>
                                            <div class="bottom-content ">
                                                <div class="pad-t20 text-center">
                                                    <!-- <p>Last edited by </p> -->
                                                    <a href="{{newsDetails.link || newsDetails.url}}" target="_blank" ng-click="" class="info-link">MORE INFO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="custom-spinner height-100 pad-t0 case-dairy-spinner" ng-show="EntityNewSearchObject.fullpageSpinner" >
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <div class="top-filters-wrapper segment-filters-wrappers" ng-show="(EntityNewSearchObject.customer_story.length === 0 ||EntityNewSearchObject.customer_story == undefined) && !EntityNewSearchObject.fullpageSpinner">
                            <span class="text-center no-data-wrapper">No results found</span>
                        </div>
                        <div class="text-right search-pagination" ng-show="EntityNewSearchObject.newsSeen">
                            <ul class="pull-right mar-y0" uib-pagination total-items="TempHoold.tempNews.length" items-per-page="8" ng-model="newsNum"
                                ng-change="pageChangedVLA(newsNum)" max-size="numberofNews" class="pagination-sm" boundary-link-numbers="true"></ul>
                        </div>
                        <!--  Top Filters Wrapper Ends  -->
                    </div>

                    <!-- Segment Panel Ends -->

                </div>
         	    <!--  LeadGeneration Wrapper Ends -->
            </div>
            <!--  Search Results Wrapper Ends  -->

        </div>
    </div>
    <!--  Landing Dashboard Wrapper Ends  -->
    <script>
        $(".panel .panel-scroll").mCustomScrollbar({
            axis: "y",
            theme: "minimal-dark"
        });
    </script>
    <script>
        $(".panel .panel-h-scroll").mCustomScrollbar({
            axis: "xy",
            theme: "minimal-dark"
        });
    </script>

    <style>
        #chart-container {
            position: relative;
            display: inline-block;
            top: 10px;
            left: 10px;
            height: 420px;
            width: calc(100% - 24px);
            /*   width:300px; */
            border: 0;
            border-radius: 5px;
            text-align: center;
        }
    </style>
    <script>
        /**
                   * Function to get tool tip for progres bars
                   * 
                   */
        $("body").on("mouseover", ".company-details-wrapper", function () {
            if (location.hash.indexOf('entitySearchResult') >= 0) {
                div.style("display", "inline-block");
                div.style("visibility", "visible");
                div.style("min-width", "auto");

            }

        }).on("mousemove", ".company-details-wrapper", function (event) {
            if (location.hash.indexOf('entitySearchResult') >= 0) {
                div.style("left", event.pageX + 10 + "px");
                div.style("top", event.pageY + "px");
                div.html($(this).find("h3").text());
            }
        }).on("mouseout", function () {
            div.style("display", "none");
        })
    </script>