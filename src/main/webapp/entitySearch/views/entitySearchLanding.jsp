<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
        <!--  SubMenu Ends  -->
        <!--  Landing Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper ceri-wrapper landing-dashboard-wrapper bg-dark-grey1">
            <!--  Flex Container Starts  -->
            <div class="flex-container clearfix">
                <div class="top-image-wrapper"></div>
                <div class="input-group custom-input-group custom-select-dropdown inner-element">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                        <input type="text"  ng-model="search_organization_name" maxlength="100" placeholder="Search element for company or person"
                        class="custom-input width-100"  ng-keydown="$event.keyCode === 13 && ceriSearchRouting(search_organization_name)">
                    <span class=" pull-right input-group-addon1" ng-click="ceriSearchRouting(search_organization_name)" style="cursor:pointer; position: absolute;  z-index: 100;">
                        <i class="fa fa-angle-right"></i>
                        <!--                          <button  ng-click="clearSearch()"  style="cursor:pointer">clear</button> -->
                    </span>
                    <div class="row text-center" >
                            <a class="btn text-dark-blue text-uppercase" href ng-click="NavigateoAdvanceSearch()">Advanced Search</a> 
                        </div>
                    <div class="text-center custom-dropdown-menu" ng-show= "">
                        <i class="fa fa-spinner fa-pulse"></i>
                    </div>
                    <div ng-show="noOrgResults" class="custom-dropdown-menu">
                        <i class="glyphicon glyphicon-remove text-light-red"></i> No Results Found
                    </div>
                </div>

            </div>
            <!--  Flex Container Ends  -->
            <!--  Landing page Starts Here  -->
            <div class="search-top-wrapper ">
                <div class="main-filtered-content-wrapper">
                    <div class="filtered-company-list">
                        <div class="row padding-x10">
                            <div class="company-list-wrapper c-pointer" ng-repeat="recentdata in entityNewSearch.recentSearch">
                                <div class="company-details-wrapper"  ng-click="EntityRouting(recentdata.search)">
                                    <h5>{{recentdata.search.name}}</h5>
                                    <p>{{recentdata.search.type | date :'medium'}}</p>

                                    <div class="company-type  pad-y15 text-center">
                                        <span  class="icon-wrapper" style="height:  100px;width: 100px!important;border-radius:  50%;background:  #3a4f59;padding: 23px;" ng-if="recentdata.search.type === 'Individual'"><i class ="fa fa-user"  style="font-size:55px"></i></span> 
                                        <span class="icon-wrapper"style="height:  100px;width: 100px!important;border-radius:  50%;background:  #3a4f59;padding: 23px;" ng-if="recentdata.search.type ==='Corporate'" ><i class ="fa fa-building"  style="font-size:55px"></i></span> 
                                        <span class="icon-wrapper"style="height:  100px;width: 100px!important;border-radius:  50%;background:  #3a4f59;padding: 23px;" ng-if="recentdata.search.type=== 'case'"><i class ="fa fa-briefcase" style="font-size:55px"></i></span> 
                                        <span class="icon-wrapper"style="height:  100px;width: 100px!important;border-radius:  50%;background:  #3a4f59;padding: 23px;" ng-if="recentdata.search.type === 'vla'" ><i class ="fa fa-code-fork"  style="font-size:55px"></i></span> 
                                    </div>
                                </div>
                              </div>
                           </div>
                    </div>
                </div>
            </div>
                                <!--  Landing Page Ends Here  -->
    </div>


                            <style>
                                .company-list-wrapper a {
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis;
                                    width: 100%;
                                    display: block;

                                }
                            </style>
