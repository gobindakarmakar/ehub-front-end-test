'use strict';
angular.module('entitySearchApp')
	   .factory('EntitySearchApiService', entitySearchApiService);


entitySearchApiService.$inject = [
			'$http',
			'EHUB_API',
			'$q',
			'$rootScope',
//			'DataCurationAPIConstant',
			'$stateParams'
		];
	
	function entitySearchApiService(
			$http,
			EHUB_API,
			$q,
			$rootScope,
//			DataCurationAPIConstant,
			$stateParams){
		return {
			getlistofEntities :getlistofEntities,
			getlistofCaseData :getlistofCaseData,
			getlistofTransactionAlertsData : getlistofTransactionAlertsData,
			getConcernedCompanies:getConcernedCompanies,
			SearchRecentCompany:SearchRecentCompany,
			SaveasRecentSearch:SaveasRecentSearch,
			RecentSearchMade:RecentSearchMade,
			getScreeningDetails:getScreeningDetails,
			getFinancialData:getFinancialData,
			getDirectorsIdentifier:getDirectorsIdentifier,
			getResolvedEntityData:getResolvedEntityData,
			customer__stories_clusters:customer__stories_clusters
		}	
		
	/* @purpose: GET the result Entites 
		* 			 
		* @created: 26 july 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
	/*To get list of Entites */
	function getlistofEntities(name) {
		//	var apiUrl = EHUB_API + 'entity/org/search';
		var apiUrl = EHUB_API + "advancesearch/graph/entity/" + name+"?token="+$rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getlistofOrganisationsSuccess)
			.catch(getlistofOrganisationsError));
		/*getlistofOrganisations error function*/
		function getlistofOrganisationsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getlistofOrganisations success function*/
		function getlistofOrganisationsSuccess(response) {
			return (response);
		}

	}
	/*
	* @purpose: get org name suggestion
	* @created: 27 july 2018
	* @params: token(object)
	* @return: success, error functions getlistofTransactionAlertsData
	* @author: ramsingh
	*/
	function getlistofCaseData(name) {
		var apiUrl = EHUB_API + "investigation/searchCaseList?token="+$rootScope.ehubObject.token+"&keyWord="+name;
		var request = $http({
			method: "GET",
			url: apiUrl
		});

		return (request
			.then(getlistofCaseDataSuccess)
			.catch(getlistofCaseDataError));
		/*getlistofCaseData error function*/
		function getlistofCaseDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getlistofCaseData success function*/
		function getlistofCaseDataSuccess(response) {
			return (response);
		}
	}

	/*
	* @purpose: get org TransactionAlertsData
	* @created: 27 july 2018
	* @params: token(object)
	* @return: success, error functions 
	* @author: ramsingh
	*/
	function getlistofTransactionAlertsData(name) {
		var apiUrl = EHUB_API + "tx/searchAlerts?token="+$rootScope.ehubObject.token+"&keyWord="+name;
		var request = $http({
			method: "GET",
			url: apiUrl
		});

		return (request
			.then(getlistofTransactionAlertsDataSuccess)
			.catch(getlistofTransactionAlertsDataError));
		/*getlistofCaseData error function*/
		function getlistofTransactionAlertsDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getlistofCaseData success function*/
		function getlistofTransactionAlertsDataSuccess(response) {
			return (response);
		}
	}
	/* 
	* purpose: get company details Information
	* created: 28 july 2018
	* params: params(object)
	* return: success, error functions
	* author: Ram Singh
	*/
	function SearchRecentCompany(companyName) {
		var apiUrl = EHUB_API + "recentSearch/search?token=" + $rootScope.ehubObject.token + "&searchKeyword=" + companyName;
		var request = $http({
			url: apiUrl,
			method: 'GET'
		});
		return (request
			.then(SearchRecentCompanySuccess)
			.catch(SearchRecentCompanyError));
		/* getclustersInfo error function */
		function SearchRecentCompanyError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function SearchRecentCompanySuccess(response) {
			return (response);
		}
	}
	/* 
	* purpose: save as recent search 
	* created: 1st May 2018
	* params: params(object)
	* return: success, error functions
	* author: Ram 
	*/
	function SaveasRecentSearch(data) {
		var apiUrl = EHUB_API + "recentSearch/saveAsRecentSearch";
		var request = $http({
			url: apiUrl,
			method: "POST",
			params: {
				token: $rootScope.ehubObject.token
			},
			data: data
		});
		return (request
			.then(SaveasRecentSearchSuccess)
			.catch(SaveasRecentSearchError));

		/* getclustersInfo error function */
		function SaveasRecentSearchError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function SaveasRecentSearchSuccess(response) {
			return (response);
		}
	}
			 /* 
			 * purpose: get Searches made Previously
			 * created: 29 july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: Ram
			 */
			function RecentSearchMade(token){
				var apiUrl = EHUB_API +"recentSearch/getRecentSearchs";
				var request = $http({
					url: apiUrl,
					method: 'GET',
					 params: {
			            	token:token
			            }
				});
				return(request
						.then(RecentSearchMadeSuccess)
						.catch(RecentSearchMadenError));
				
				/* getclustersInfo error function */
				function RecentSearchMadenError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfo success function */
				function RecentSearchMadeSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Concerned Company Information
			 * created: 1st May 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: Amritesh
			 */
			function getConcernedCompanies(query){
				var apiUrl = EHUB_API +'tuna/search/companies?token='+$rootScope.ehubObject.token+'&q='+query;
				var request = $http({
					url: apiUrl,
					method: 'GET'
//					 params: {
//			            	token: $rootScope.ehubObject.token
//			            }
				});
				return(request
						.then(getConcernedCompaniesSuccess)
						.catch(getConcernedCompaniesError));
				
				/* getclustersInfo error function */
				function getConcernedCompaniesError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfo success function */
				function getConcernedCompaniesSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Concerned Company Information
			 * created: 1st May 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: Amritesh
			 */
			function getScreeningDetails(query,entitytype){
				
				var apiUrl = EHUB_API +'tuna/profile/'+query+'?token='+$rootScope.ehubObject.token+'&orgOrPerson='+entitytype;
				  
				var request = $http({
					url: apiUrl,
					method: 'GET'
//					 params: {
//			            	token: $rootScope.ehubObject.token
//			            }
				});
				return(request
						.then(getScreeningDetailsSuccess)
						.catch(getScreeningDetailsError));
				
				/* getclustersInfo error function */
				function getScreeningDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfo success function */
				function getScreeningDetailsSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Concerned Company Information
			 * created: 1st May 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: Amritesh
			 */
			function getFinancialData(query, isMocked, country){
				if(isMocked == false)
					var apiUrl = EHUB_API +'tuna/getFinanceStatement?token='+$rootScope.ehubObject.token+'&source=finance.4-traders.com&query='+query+'&apiKey=xdty-0997-drt22345-009011';
				else
					var apiUrl = EHUB_API +'tuna/getFinanceStatement?token='+$rootScope.ehubObject.token+'&source=finance.4-traders.com&query='+query+'&apiKey=xdty-0997-drt22345-009011&exchange=' + country;

				var request = $http({
					url: apiUrl,
					method: 'GET'
//					 params: {
//			            	token: $rootScope.ehubObject.token
//			            }
				});
				return(request
						.then(getFinancialDataSuccess)
						.catch(getFinancialDataError));
				
				/* getclustersInfo error function */
				function getFinancialDataError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfo success function */
				function getFinancialDataSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Concerned Company Information
			 * created: 1st May 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: Ram Singh
			 */
			function getDirectorsIdentifier(name) {
				
				var apiUrl = EHUB_API + 'entity/person/search';
				var key = {};

				var limit =1;
				if (name) {
					//			apiUrl +=  name  ? "?query=" + name : "";
					angular.extend(key, {
						"vcard:hasName": name
					});
				}
				if (limit) {
					//			apiUrl +=  limit  ? "&size=" + limit : "";
					angular.extend(key, {
						"size": limit
					});
				} else {
					angular.extend(key, {
						"size": 1
					});
				}
				//		angular.extend(Parameters,{"token": $rootScope.ehubObject.token});
				//		
				var request = $http({
					method: "POST",
					url: apiUrl,
					data: key,
					params: {
						token: $rootScope.ehubObject.token
					}
				});

				return (request
					.then(getDirectorsIdentifierSuccess)
					.catch(getDirectorsIdentifierError));

				/*getDirectorsIdentifier error function*/
				function getDirectorsIdentifierError(response) {
					if (!angular.isObject(response.data) || !response.data.message) {
						return ($q.reject(response.data));
					}
					/*Otherwise, use expected error message.*/
					return ($q.reject(response.data.message));
				}

				/*getDirectorsIdentifier success function*/
				function getDirectorsIdentifierSuccess(response) {
					return (response);
				}
			}

	/*
		* @purpose: getResolvedEntityData
		* @created: 31 jul 2018
		* @params: data(object)
		* @return: success, error functions
		* @author: Ram singh
		*/
	function getResolvedEntityData(data) {
		//var apiUrl =EHUB_API+'search/getData?token='+$rootScope.ehubObject.token+'&searchFlag=resolve';
		var apiUrl = EHUB_API + "advancesearch/graph/search/entity?token="+$rootScope.ehubObject.token;

		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(getResolvedEntityDataSuccess)
			.catch(getResolvedEntityDataError));

		/*        getResolvedEntityData error function*/
		function getResolvedEntityDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getResolvedEntityData success function*/
		function getResolvedEntityDataSuccess(response) {
			return (response);
		}
	}
	
	//---------------------Lead Generation service-----------------------------------
	
	/*
	 * purpose: get customer__stories_clusters by customerId
	 * created: 3rd September 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: varsha
	 */
	function customer__stories_clusters(customerId,param){
		var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_4/customer__stories_clusters/'+customerId+'?token='+$rootScope.ehubObject.token
		var request = $http({
				url: apiUrl,
				method: 'GET',
				data:param
			});
		return(request
				.then(customer__stories_clustersSuccess)
				.catch(customer__stories_clustersError));
		
		/* getStories error function */
		function customer__stories_clustersError(response){
			if(!angular.isObject(response.data) || !response.data.message){
				return($q.reject(response.data));
			}
			return($q.reject(response.data.message));
		}
		/* getStories success function */
		function customer__stories_clustersSuccess(response){
			return(response);
		}
	}
	
	
			

	};