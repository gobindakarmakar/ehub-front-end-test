'use strict';
angular.module('entitySearchApp')
	   .controller('DataModalController', dataModalController);

		dataModalController.$inject = [
			'$scope',
			'$rootScope',
			'data',
			'$uibModalInstance',
			'$timeout'
		];
	    function dataModalController(
	    		$scope,
	    		$rootScope,
	    		data,
	    		$uibModalInstance,
	    		$timeout){
	    	
	    	/*
	         * @purpose:Set data in modal
	         * @created: 5 MAY 2018
	         * @author:Amritesh
	        */
			$scope.data =data;
	    	$timeout(function(){
	    		  $("#mainData").html(data.txt)	
//	    		  console.log(data)
	    	},0)
	    	if(data.name){
	    		$scope.header=data.name;
	    	}
	    	else if(data.secondary){
	    		$scope.header='Secondary Links';
	    	} else{
	    		$scope.header='Primary Links';
	    	}
	    	$scope.closeDataPopUp=function(){
	    		$uibModalInstance.close();
	    	}
          
		
	    }
	    	
	    	