'use strict';
angular.module('entitySearchApp').controller('EntitySearchResultController',
	entitySearchAppLandingController);
entitySearchAppLandingController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$timeout',
	'$stateParams',
	'EntitySearchApiService',
	'$window',
	'$uibModal',
	'mockedCompany',
	'chartsConst',
	'$location',
	'EHUB_FE_API',
	'SearchResultData'
	//			'chartsConst'
];

function entitySearchAppLandingController(
	$scope,
	$state,
	$rootScope,
	$timeout,
	$stateParams,
	EntitySearchApiService,
	$window,
	$uibModal,
	mockedCompany,
	chartsConst,
	$location,
	EHUB_FE_API,
	SearchResultData
	//				chartsConst
) {
	$scope.filterBY = {};
	$scope.filterBY.levelsArr = [

		{
			value: "all",
			label: "ALL"
		},
		{
			value: "external",
			label: "EXTERNAL"
		},
		{
			value: "internal",
			label: "INTERNAL"
		}

	];
	$scope.filterBY.levels= $scope.filterBY.levelsArr[0].value; 
	$scope.numberofNews= 0;
	$scope.VlaPagesnum = 0;
	$scope.numberofalerts = 0;
	$scope.EntityNewSearchObject = {
		case: [],
		transcationAlerts: [],
		SearchedEntity: [],
		byVladata:[],
		SearchFilterEntity:"",
		redirectTOPage: redirectTOPage,
		typeSeeAll: typeSeeAll,
		redirectToCase: redirectToCase,
		casesSeen: false,
		EntitySeen: false,
		CompanySeen:false,
		entityPersonSeen:false,
		alertSeen: false,
		vlaSeen:false,
		showbycase:true,
		showbyAlerts :true,
		showbyTransactions:true,
		showbyVla:true,
		showbyNews:true,
		showByentity:true,
		showbyOthers:true,
		sortbyDateFilter: "",
		CreationUpdated: false,
		MostUpdated: false,
		fullpageSpinner:true,
		entitySectionSpinner:true,
		pageNum:0,
		Entitypagenum:0,
		CaseNum:0,
		Companypagenum:0,
		Personpagenum:0,
		AlertPageNum:0,	
		entityPerson:[],
		entityCompany:[],
		max_size_entites:0,
		max_comapany_number:0,
		max_person_number:0,
		newsSection:[],
		newsnum:0,
		newsSeen:false
	};
	//$scope.searchType ="all";
	$scope.TempHoold = {
		tempVLA : [],
		tempEntites:[],
		tempCases:[],
		tempNews:[],
		tempPerson:[],
		tempCompany:[],
		tempAlert:[]
	};
	$scope.tabtype  = "All";
	$scope.TabsValue = ["all","external","internal"];
	//$scope.searchType = $scope.TabsValue[0];

	var ExternalCompany =[];
	$scope.totalEnties = 0;
	$scope.Showbutton = {
		Company: false,
		person: false,
		entityAll:false
	}
	$scope.SingleEntity= {
		type:[],
		tabchanged:tabchanged,
		Entitylength:0,
		EntitesNumber:0,
		showingItem :0,
		numberofPages:0,
		entityTab:"all",
		PaginationEntites:PaginationEntites,
		seeallbutton :false,
		showPagination : showPagination
	};
	/* @purpose:  Get  the Searched Company Data
	 * 			 
	 * @created: 29 july 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function GetDataofSearchedEntity(name) {
		EntitySearchApiService.SearchRecentCompany(name).then(function (response) {
			$scope.TempHoold.tempCases = response.data.caseList;
			
			var vertices =JSON.parse(response.data.network);
			$scope.TempHoold.tempVLA = vertices.vertices;
			$scope.TempHoold.tempAlert  =  response.data.alertList;
			$scope.TempHoold.tempEntites = $scope.TempHoold.tempEntites.concat(response.data.entities);
			$scope.TempHoold.tempEntites = $scope.TempHoold.tempEntites.filter(function (val) {
				if (val.name)
					return val;
			});	
			$scope.EntityNewSearchObject.transcationAlerts = $scope.TempHoold.tempAlert.length > 8 ? $scope.TempHoold.tempAlert.slice(0,8): $scope.TempHoold.tempAlert;
			$scope.EntityNewSearchObject.AlertPageNum = $scope.EntityNewSearchObject.transcationAlerts.length;
			$scope.numberofalerts = Math.ceil($scope.TempHoold.tempAlert/8);
			if ($scope.TempHoold.tempVLA.length > 8) {
				$scope.EntityNewSearchObject.byVladata = $scope.TempHoold.tempVLA.slice(0, 8);
				$scope.EntityNewSearchObject.pageNum = 8;
			} else {
				$scope.EntityNewSearchObject.byVladata = $scope.TempHoold.tempVLA.slice(0, $scope.TempHoold.tempVLA.length);
				$scope.EntityNewSearchObject.pageNum= $scope.EntityNewSearchObject.byVladata.length;
			}
			$scope.VlaPagesnum = Math.ceil( $scope.TempHoold.tempVLA.length/8);
			$scope.EntityNewSearchObject.SearchedEntity = $scope.TempHoold.tempEntites.slice(0, 8) ;
			$scope.SingleEntity.type = $scope.TempHoold.tempEntites.length > 8 ? $scope.TempHoold.tempEntites.slice(0, 8) : $scope.TempHoold.tempEntites;
			$scope.SingleEntity.showingItem = $scope.SingleEntity.type.length;
			$scope.SingleEntity.numberofPages=Math.ceil( $scope.TempHoold.tempEntites.length/8);
			$scope.SingleEntity.Entitylength = $scope.TempHoold.tempEntites.length;
			$scope.SingleEntity.EntitesNumber= $scope.TempHoold.tempEntites.length;
			$scope.Showbutton.entityAll =	$scope.TempHoold.tempEntites.length > 8 ? true :false;
			$scope.EntityNewSearchObject.Entitypagenum = $scope.TempHoold.tempEntites.length > 8 ? 8 : $scope.TempHoold.tempEntites.length;
			if ($scope.TempHoold.tempCases.length > 8) {
				$scope.EntityNewSearchObject.case = $scope.TempHoold.tempCases.slice(0, 8);
				$scope.EntityNewSearchObject.CaseNum = 8;
			} else {
				$scope.EntityNewSearchObject.case = $scope.TempHoold.tempCases.slice(0, $scope.TempHoold.tempCases.length);
				$scope.EntityNewSearchObject.CaseNum= $scope.EntityNewSearchObject.case.length;
			}
			for(var i=0;i<$scope.TempHoold.tempEntites.length;i++){
				$scope.TempHoold.tempEntites[i].internal ="";
				$scope.TempHoold.tempEntites[i].internal = true;
			}
			$scope.totalEnties= $scope.TempHoold.tempEntites.length;
			ExtenalData(name);
			filterEntitties();
			newsSection(response.data);
			$scope.EntityNewSearchObject.max_size_entites =Math.ceil($scope.TempHoold.tempEntites.length/8);
			$scope.EntityNewSearchObject.fullpageSpinner = false;
		}, function (orgError) {
			ExtenalData(name);
			$scope.EntityNewSearchObject.fullpageSpinner = false;
			$scope.EntityNewSearchObject.entitySectionSpinner= false;

		});
	}
	
	$scope.SearchFilter = function(name){
		if (name) {	
			$scope.TempHoold = {
				tempVLA: [],
				tempEntites: [],
				tempCases: [],
				tempNews: [],
				tempPerson: [],
				tempCompany: [],
				tempAlert: []
			};
			$scope.EntityNewSearchObject = {
				case: [],
				transcationAlerts: [],
				SearchedEntity: [],
				byVladata: [],
				SearchFilterEntity: "",
				typeSeeAll: typeSeeAll,
				redirectTOPage: redirectTOPage,
				redirectToCase:redirectToCase,
				casesSeen: false,
				EntitySeen: false,
				CompanySeen: false,
				entityPersonSeen: false,
				alertSeen: false,
				vlaSeen: false,
				showbycase: true,
				showbyAlerts: true,
				showbyTransactions: true,
				showbyVla: true,
				showbyNews: true,
				showByentity: true,
				showbyOthers: true,
				sortbyDateFilter: "",
				CreationUpdated: false,
				MostUpdated: false,
				fullpageSpinner: true,
				entitySectionSpinner: true,
				pageNum: 0,
				Entitypagenum: 0,
				CaseNum: 0,
				Companypagenum: 0,
				Personpagenum: 0,
				AlertPageNum: 0,
				entityPerson: [],
				entityCompany: [],
				max_size_entites: 0,
				max_comapany_number: 0,
				max_person_number: 0,
				newsSection: [],
				newsnum: 0,
				newsSeen: false
			};
			$scope.SingleEntity = {
				type: [],
				tabchanged: tabchanged,
				Entitylength: 0,
				EntitesNumber: 0,
				showingItem: 0,
				numberofPages: 0,
				entityTab: "all",
				PaginationEntites: PaginationEntites,
				seeallbutton: false,
				showPagination: showPagination
			};
			ExternalCompany = [];
			$scope.EntityNewSearchObject.fullpageSpinner = true;
			$scope.EntityNewSearchObject.entitySectionSpinner = true;
			$scope.totalEnties = 0;
			GetDataofSearchedEntity(name);
		}
	};
	
		 /*
    * @purpose:  Differentiate the APi call for numbers and name of the search query
    * @created: 3rd september
    * @params: params(object)
    * @return: success, error functions
    * @author: varsha
    */
	var customerId;
	if(isNaN($stateParams.companyName)){
		$scope.EntityNewSearchObject.ShowLeadGeneration = false;
		GetDataofSearchedEntity($stateParams.companyName);
	}else{
		$scope.EntityNewSearchObject = {
			showbycase:false,
			showbyAlerts:false,
			showByentity:false,
			showbyVla:false,
			showbyNews:false,
			ShowLeadGeneration:true,
			redirectToCustomerPage:redirectToCustomerPage
		};	
		customer__stories_clusters($stateParams.companyName)
		customerId = $stateParams.companyName;
	}
	
//-----------------------------------Lead Generation functionality Starts-----------------

	 /*
    * @purpose: Get customer stories details
    * @created: 3rd september
    * @params: params(object)
    * @return: success, error functions
    * @author: varsha
    */
   
   function customer__stories_clusters(customerId){
   	EntitySearchApiService.customer__stories_clusters(customerId,{}).then(function(response) {
  		 $scope.EntityNewSearchObject.customer_story = response.data.data
  	 });
  }
	
   /*
    * @purpose: Redirect to leadGeneration Customer Page
    * @created: 3rd september
    * @params: params(object)
    * @return: success, error functions
    * @author: varsha
    */
   
   function redirectToCustomerPage(customerDetail){
   	sessionStorage.setItem("StoryName",customerDetail.story_name);
       sessionStorage.setItem("clusterId", customerDetail.cluster_id);
       sessionStorage.setItem("storyId", customerDetail.story_id);
       window.open(EHUB_FE_API +'leadGeneration/#!/customerPage/'+customerId, '_blank');
	}
 
 //-----------------------------------Lead Generation functionality ends-----------------
	
	/* @purpose: GET the result Entites 
		* 			 
		* @created: 26 july 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
	function SaveRecentSearches(data) {
		var stringfy = JSON.stringify(data);
		EntitySearchApiService.SaveasRecentSearch(stringfy).then(function (resp) {
		}, function (eror) {

		});
	}
	/* @purpose:  Increase the result
	 * 			 
	 * @created: 20 july 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function typeSeeAll(length, type) {
		if (type === 'case') {
			$scope.EntityNewSearchObject.casesSeen = true;
		} else if (type === 'entites') {
			$scope.EntityNewSearchObject.EntitySeen = true;
		} else if (type === 'transcation') {
			$scope.EntityNewSearchObject.alertSeen = true;
		} else if (type === "vla") {
			$scope.EntityNewSearchObject.vlaSeen = true;
		}else if(type==="news"){
			$scope.EntityNewSearchObject.newsSeen =true;
		}
		else if(type==="Company"){
			var end = $scope.TempHoold.tempCompany.length > 8 ? 8 : $scope.TempHoold.tempCompany.length;
			$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany.slice(0,end);
			$scope.EntityNewSearchObject.CompanySeen =$scope.TempHoold.tempCompany.length > 8 ? true :false;
			$scope.EntityNewSearchObject.EntitySeen = false;
			$scope.EntityNewSearchObject.entityPersonSeen =false;
		}
		else if(type==="Person"){
			var end = $scope.TempHoold.tempPerson.length < 8? $scope.TempHoold.tempPerson.length : 8;
			$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson.slice(0,end);
			$scope.EntityNewSearchObject.entityPersonSeen = $scope.TempHoold.tempPerson.length < 8 ? false: true;
			$scope.EntityNewSearchObject.CompanySeen =false;
			$scope.EntityNewSearchObject.EntitySeen = false;
		}
	}
	/* @purpose: redirect Form search to resppective Pages
	 * 			 
	 * @created: 27 july 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function redirectToCase(data, type) { 
		data.type ="";
		data.type = type;
		SaveRecentSearches(data);
		if (type === "case") {
			var url = EHUB_FE_API + "#/act/" + data.caseId;
			window.open(url, "_blank");
		} else if (type === "vla") {
			var vlaurl = EHUB_FE_API +"#/linkAnalysis?caseId="+data.caseId +"&entity="+data.name;
			window.open(vlaurl, "_blank");
		}
	}
	/* @purpose: redirect Form search to Entity COmpany or Person Pages
	 * @created: 27 july 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function redirectTOPage(data) {
		SaveRecentSearches(data);
		var url = EHUB_FE_API;
		if (data.type === "Corporate") {
			if(data.identifiier){
				url = url + "entity/#!/company/"+data.identifiier;
			}else{
				url = url + "entity/#!/company/" + data.name;
			}
			window.open(url, "_blank");
		} else if (data.type === "Individual") {
			url = url + "entity/#!/person/" + data.name;
			window.open(url, "_blank");
		}
	
	}
	/* @purpose: VLA Pagination
		* @created: 27 july 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
	$scope.pageChangedVLA = function(number){
		var start = (number-1)*8;
		var end =start + 8;
		$scope.EntityNewSearchObject.byVladata = $scope.TempHoold.tempVLA.slice(start,end); 
		$scope.EntityNewSearchObject.pageNum  =  number*8 < $scope.TempHoold.tempVLA.length ? number*8 : $scope.TempHoold.tempVLA.length;
	};
	$scope.pageChangeEntities = function(number){
		var start = (number-1)*8;
		var end =start + 8;
		$scope.EntityNewSearchObject.SearchedEntity = $scope.TempHoold.tempEntites.slice(start,end); 
		$scope.EntityNewSearchObject.Entitypagenum  =  number*8 < $scope.TempHoold.tempEntites.length ? number*8 : $scope.TempHoold.tempEntites.length;
	};
	$scope.pageChangeCase = function(number){
		var start = (number-1)*8;
		var end =start + 8;
		$scope.EntityNewSearchObject.case = $scope.TempHoold.tempCases.slice(start,end); 
		$scope.EntityNewSearchObject.CaseNum  =  number*8 < $scope.TempHoold.tempCases.length ? number*8 : $scope.TempHoold.tempCases.length;
	};
	$scope.pageChangeCompany = function (number) {
		var start = (number - 1) * 8;
		var end = start + 8;
		$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany.slice(start, end);
		$scope.EntityNewSearchObject.Companypagenum = number * 8 < $scope.TempHoold.tempCompany.length ? number * 8 : $scope.TempHoold.tempCompany.length;
		filterPaginationCompany($scope.searchType);
	};
	
	$scope.pageChangePerson = function(number){
		var start = (number-1)*8;
		var end =start + 8;
		$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson.slice(start,end); 
		$scope.EntityNewSearchObject.Personpagenum  =  number*8 < $scope.TempHoold.tempPerson.length ? number*8 : $scope.TempHoold.tempPerson.length;
		filterPaginationPerson();
	};
	$scope.pageChangedAlert= function(number){
		var start = (number-1)*8;
		var end =start + 8;
		$scope.EntityNewSearchObject.transcationAlerts = $scope.TempHoold.tempAlert.slice(start,end); 
		$scope.EntityNewSearchObject.AlertPageNum  =  number*8 < $scope.TempHoold.tempAlert.length ? number*8 : $scope.TempHoold.tempAlert.length;
	};
	function ExtenalData(name) {
		var CompanyData = {
			keyword: name,
			searchType: 'Company'
		};
		var PersonData = {
			keyword: name,
			searchType: 'Person'
		};
		getResolvedData(CompanyData, "Corporate");
		getResolvedData(PersonData, "Individual");
	}
	function getResolvedData (name,type){
		$scope.EntityNewSearchObject.entitySectionSpinner= true;
		EntitySearchApiService.getResolvedEntityData(name).then(function(response){
			for (var i = 0; i < 5; i++) {
				if (response.data.data.results[i] &&  response.data.data.results[i]["merged-result"] && response.data.data.results[i]["merged-result"].basic) {
					ExternalCompany.push({
						name: response.data.data.results[i]["merged-result"].basic["0"].value["vcard:organization-name"],
						type: type,
						adress: response.data.data.results[i]["merged-result"].basic["0"].value["mdaas:RegisteredAddress"],
						identifiier:response.data.data.results[i]["merged-result"]["@identifier"],
						external:true
					});
				}
			}
			$scope.TempHoold.tempEntites = $scope.TempHoold.tempEntites.concat(ExternalCompany);
			$scope.SingleEntity.type = $scope.TempHoold.tempEntites.length > 8 ? $scope.TempHoold.tempEntites.slice(0, 8) : $scope.TempHoold.tempEntites;
			$scope.SingleEntity.showingItem = $scope.SingleEntity.type.length;
			$scope.totalEnties= $scope.TempHoold.tempEntites.length;
			$scope.SingleEntity.Entitylength = $scope.TempHoold.tempEntites.length;
			$scope.TempHoold.tempPerson = _.filter($scope.TempHoold.tempEntites, ["type", "Individual"]);
			$scope.TempHoold.tempCompany = _.filter($scope.TempHoold.tempEntites, ["type", "Corporate"]);
			$scope.SingleEntity.EntitesNumber= $scope.TempHoold.tempEntites.length;
			$scope.EntityNewSearchObject.entitySectionSpinner= false;
		
		},function(error){
			$scope.EntityNewSearchObject.entitySectionSpinner= false;

		});
	
	}
	/* @purpose:filter the Type of Search Internal or External
		* 			 
		* @created: 26 july 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
	$scope.typeofSearch = function (type) {
		$scope.searchType  = type;			
		if ($scope.tabtype === "All") {
			if (type === "external") {
				$scope.EntityNewSearchObject.SearchedEntity = _.filter($scope.TempHoold.tempEntites, ["external", true]);
			} else if (type === "internal") {
				$scope.EntityNewSearchObject.SearchedEntity = _.filter($scope.TempHoold.tempEntites, ["internal", true]);
			} else if (type === "all") {
				$scope.EntityNewSearchObject.SearchedEntity = $scope.TempHoold.tempEntites;
			}
			$scope.EntityNewSearchObject.Entitypagenum = $scope.EntityNewSearchObject.SearchedEntity.length;
			$scope.totalEnties = $scope.TempHoold.tempEntites.length;
			if($scope.EntityNewSearchObject.Entitypagenum  > 8){
				$scope.EntityNewSearchObject.EntitySeen = true;
			}
		}else  if ($scope.tabtype === "Company") {
			if (type === "external") {
				$scope.EntityNewSearchObject.entityCompany = _.filter($scope.TempHoold.tempCompany,["external", true]);
			} else if (type === "internal") {
				$scope.EntityNewSearchObject.entityCompany = _.filter($scope.TempHoold.tempCompany,["internal", true]);

			} else if (type === "all") {
				$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany;
			}
			$scope.EntityNewSearchObject.Companypagenum = $scope.EntityNewSearchObject.entityCompany.length;
			$scope.totalEnties = $scope.TempHoold.tempCompany.length;
			if($scope.EntityNewSearchObject.Companypagenum > 8){
			//	$scope.EntityNewSearchObject.CompanySeen = true;
			}else{
				//$scope.EntityNewSearchObject.CompanySeen = false;
			}
		}else  if ($scope.tabtype === "Person") {
			if (type === "external") {
				$scope.EntityNewSearchObject.entityPerson = _.filter($scope.TempHoold.tempPerson,["external", true]);
			} else if (type === "internal") {
				$scope.EntityNewSearchObject.entityPerson = _.filter($scope.TempHoold.tempPerson,["internal", true]);

			} else if (type === "all") {
				$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson;
			}
			$scope.EntityNewSearchObject.Personpagenum =$scope.TempHoold.tempPerson.length;
			$scope.totalEnties = $scope.TempHoold.tempCompany.length;
			if($scope.EntityNewSearchObject.Personpagenum > 8){
				$scope.EntityNewSearchObject.entityPersonSeen = true;
			}
		}
	};
	/* @purpose:Get the Data  For News Section
	 * 			 
	 * @created: 26 july 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function newsSection(data) {		
		var News = data.entityNews ? (data.entityNews ? JSON.parse(data.entityNews) : []) : [];
		var orgNews = data.orgNews ? (data.orgNews ? JSON.parse(data.orgNews) : []) : [];
		var personNews = data.personNews ? (data.personNews ? JSON.parse(data.personNews) : []) : [];
		$scope.TempHoold.tempNews = orgNews ? orgNews.news : [];
		var EntityNews = [];
		if (News.results) {
			for (var i = 0; i < News.results.length; i++) {
				for (var j = 0; j < News.results[i].entities.length; j++) {
					EntityNews.push(News.results[i].entities[j].properties)
				}
			}

		}
		$scope.TempHoold.tempNews = $scope.TempHoold.tempNews.concat(EntityNews);
		if ($scope.TempHoold.tempNews.length >8) {
			$scope.EntityNewSearchObject.newsSection = $scope.TempHoold.tempNews.slice(0,8);
			$scope.EntityNewSearchObject.newsnum =8;

		} else {
			$scope.EntityNewSearchObject.newsSection = $scope.TempHoold.tempNews.slice(0, $scope.TempHoold.tempNews.length);
			$scope.EntityNewSearchObject.newsnum = $scope.TempHoold.tempNews.length;
		}
		$scope.numberofNews = Math.ceil($scope.TempHoold.tempNews.length/10);
	}
	$scope.TabSelected = function (tabselect) {
		$scope.tabtype  = tabselect;
		$scope.filterBY.levels = $scope.filterBY.levelsArr[0].value;
		if (tabselect === "Company") {
	//$scope.TabsValue = ["all","external","internal"];
	$scope.searchType = "all";
			filterEntitties();
			$scope.totalEnties = $scope.TempHoold.tempCompany.length;
			$scope.Showbutton.Company = true;
			$scope.Showbutton.person = false;
			$scope.EntityNewSearchObject.EntitySeen  =false;
			$scope.EntityNewSearchObject.entityPersonSeen = false;
			$scope.Showbutton.entityAll= false;

		} else if(tabselect === "Person") {
			filterEntitties();
			$scope.totalEnties  =  $scope.TempHoold.tempPerson.length;
			$scope.Showbutton.Company = false;
			$scope.Showbutton.entityAll= false;
			$scope.Showbutton.person = true;
			$scope.EntityNewSearchObject.EntitySeen =false; 
			$scope.EntityNewSearchObject.CompanySeen =false;

		}else{
			$scope.totalEnties  = $scope.TempHoold.tempEntites.length;
			$scope.Showbutton.Company = false;
			$scope.Showbutton.person = false;
			$scope.Showbutton.entityAll = true;
			$scope.EntityNewSearchObject.entityPersonSeen = false;
			$scope.EntityNewSearchObject.CompanySeen =false;
		}
	}
	function filterEntitties() {
		$scope.TempHoold.tempPerson = _.filter($scope.TempHoold.tempEntites, ["type", "Individual"]);
		$scope.TempHoold.tempCompany = _.filter($scope.TempHoold.tempEntites, ["type", "Corporate"]);
		$scope.EntityNewSearchObject.max_comapany_number = Math.ceil($scope.TempHoold.tempCompany.length /8);
		$scope.EntityNewSearchObject.max_person_number = Math.ceil($scope.TempHoold.tempPerson.length / 8);
		if ($scope.TempHoold.tempPerson.length > 8) {
			$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson.slice(0, 8);
			$scope.EntityNewSearchObject.Personpagenum = $scope.EntityNewSearchObject.entityPerson.length;
		} else {
			$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson.slice(0, $scope.TempHoold.tempPerson.length);
			$scope.EntityNewSearchObject.Personpagenum = $scope.EntityNewSearchObject.entityPerson.length;
			
		}
		if ($scope.TempHoold.tempCompany.length > 8) {
			$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany.slice(0, 8);
			$scope.EntityNewSearchObject.Companypagenum = $scope.EntityNewSearchObject.entityCompany.length;
			//$scope.EntityNewSearchObject.CompanySeen  = true;
		} else {
			$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany.slice(0, $scope.TempHoold.tempCompany.length);
			$scope.EntityNewSearchObject.Companypagenum = $scope.EntityNewSearchObject.entityCompany.length;
			$scope.EntityNewSearchObject.CompanySeen  = false;
			
		}
		if($scope.tabtype  === "All"){
			if($scope.TempHoold.tempEntites.length > 8 )
			$scope.EntityNewSearchObject.EntitySeen  = true;
			$scope.EntityNewSearchObject.CompanySeen  = false;
		}

	}
	function filterPaginationCompany(type){
		if (type === "external") {
			$scope.EntityNewSearchObject.entityCompany = _.filter($scope.TempHoold.tempCompany,["external", true]);
		} else if (type === "internal") {
			$scope.EntityNewSearchObject.entityCompany = _.filter($scope.TempHoold.tempCompany,["internal", true]);

		} else if (type === "all") {
			$scope.EntityNewSearchObject.entityCompany = $scope.TempHoold.tempCompany;
		}
	}
	function filterPaginationPerson(type){
		if (type === "external") {
			$scope.EntityNewSearchObject.entityPerson = _.filter($scope.TempHoold.tempPerson,["external", true]);
		} else if (type === "internal") {
			$scope.EntityNewSearchObject.entityPerson = _.filter($scope.TempHoold.tempPerson,["internal", true]);

		} else if (type === "all") {
			$scope.EntityNewSearchObject.entityPerson = $scope.TempHoold.tempPerson;
		}
		$scope.EntityNewSearchObject.Personpagenum =$scope.TempHoold.tempPerson.length;
		$scope.totalEnties = $scope.TempHoold.tempCompany.length;
		if($scope.EntityNewSearchObject.Personpagenum > 8){
			$scope.EntityNewSearchObject.entityPersonSeen = true;
		}else{
			$scope.EntityNewSearchObject.entityPersonSeen = false;
		}
	}
	function tabchanged(type) {
		$scope.SingleEntity.entityTab = type;
		$scope.SingleEntity.type = [];
		$scope.filterBY.levels = $scope.filterBY.levelsArr[0].value;
		if (type === "all") {
			$scope.SingleEntity.type = $scope.TempHoold.tempEntites.length  > 8 ? $scope.TempHoold.tempEntites.slice(0,8) : $scope.TempHoold.tempEntites;
			$scope.SingleEntity.Entitylength = $scope.TempHoold.tempEntites.length;
			$scope.SingleEntity.EntitesNumber= $scope.TempHoold.tempEntites.length;
			$scope.SingleEntity.numberofPages=Math.ceil( $scope.TempHoold.tempEntites.length/8);
		} else if (type === "person") {
			$scope.SingleEntity.type = $scope.TempHoold.tempPerson.length  > 8 ? $scope.TempHoold.tempPerson.slice(0,8) :$scope.TempHoold.tempPerson;
			$scope.SingleEntity.Entitylength = $scope.TempHoold.tempPerson.length;
			$scope.SingleEntity.numberofPages=Math.ceil( $scope.TempHoold.tempPerson.length/8);
			$scope.SingleEntity.EntitesNumber=  $scope.TempHoold.tempPerson.length;
		} else if (type === "company") {
			$scope.SingleEntity.type = $scope.TempHoold.tempCompany.length >8 ? $scope.TempHoold.tempCompany.slice(0,8) : $scope.TempHoold.tempCompany;
			$scope.SingleEntity.Entitylength = $scope.TempHoold.tempCompany.length;	
			$scope.SingleEntity.numberofPages=Math.ceil( $scope.TempHoold.tempCompany.length/8);
			$scope.SingleEntity.EntitesNumber= $scope.TempHoold.tempCompany.length;	

		}
		$scope.SingleEntity.showingItem = $scope.SingleEntity.type.length;
		$scope.entitySeen = 1;
	}
	function PaginationEntites(number, type,filter) {
		if(filter)
		$scope.entitySeen = 1;
		var tempfilter = [];
		var start = (number - 1) * 8;
		var end = start + 8;
		if ($scope.SingleEntity.entityTab === "all") {
			if (type === "external") {
				tempfilter = _.filter($scope.TempHoold.tempEntites, ["external", true]);
			} else if (type === "internal") {
				tempfilter = _.filter($scope.TempHoold.tempEntites, ["internal", true]);
			} else if (type === "all") {
				tempfilter = $scope.TempHoold.tempEntites;
			}
	 
		} else if ($scope.SingleEntity.entityTab === "person") {
			if (type === "external") {
				tempfilter = _.filter($scope.TempHoold.tempPerson,["external", true]);
			} else if (type === "internal") {
				tempfilter = _.filter($scope.TempHoold.tempPerson,["internal", true]);
	
			} else if (type === "all") {
				tempfilter = $scope.TempHoold.tempPerson;
			}

		} else if ($scope.SingleEntity.entityTab === "company") {
			if (type === "external") {
				tempfilter = _.filter($scope.TempHoold.tempCompany,["external", true]);
			} else if (type === "internal") {
				tempfilter = _.filter($scope.TempHoold.tempCompany,["internal", true]);
			} else if (type === "all") {
				tempfilter = $scope.TempHoold.tempCompany;
			}
		}
		$scope.SingleEntity.EntitesNumber  = tempfilter.length;
		$scope.SingleEntity.type = tempfilter.length > 8 ? tempfilter.slice(start, end) : tempfilter;
		$scope.SingleEntity.numberofPages = Math.ceil(tempfilter.length / 8);
		$scope.SingleEntity.showingItem = number * 8 > tempfilter.length ? tempfilter.length : number * 8;
	}
	function showPagination() {
		$scope.SingleEntity.seeallbutton = true;

	}
};