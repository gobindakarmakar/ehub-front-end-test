'use strict';
angular.module('entitySearchApp').controller('EntitySearchAppLandingController',
		entitySearchAppLandingController);

entitySearchAppLandingController.$inject = [ 
	'$scope',
	'$state',
	'$rootScope',
	'EntitySearchApiService',
	'EHUB_FE_API'
//	'DataCurationApiService'
//	'chartsConst'
	];

function entitySearchAppLandingController(
		$scope,
		$state,
		$rootScope,
		EntitySearchApiService,
		EHUB_FE_API
//		DataCurationApiService
//		chartsConst
		) {
    $scope.ceriLandingObject={
		getSuggestedName : getSuggestedName,
		organization_name : ''
	};
	$scope.entityNewSearch ={
		recentSearch:[],
	};
    $scope.search_organization_name ='';
	/* var color = ["#5d96c8", "#B753CD", "#69ca6b", "#325c80", "#c1bd4f",'#db3f8d' ,"#669900", "#334433","#fdcb6e","#b2bec3","#fab1a0","#e17055"]
	var entityStatus =  [ //Pie data
		{
			'key': "ACTIVE",
			'value': 231
		}, {

			'key': "DISSOLVE",
			'value': 127
		}
		, {

			'key': "LIQUIDATION",
			'value': 23
		}
	]
	var timeIncorporation =  [ //Pie data
		{
			'key': "THIS MONTH",
			'value': 231
		}, {

			'key': "THIS QUARTER",
			'value': 127
		}
		, {

			'key': "THIS YEAR",
			'value': 23
		}, {

			'key': "YEAR BACK",
			'value': 23
		}
	]
	var legalEntity =  [ //Pie data
		{
			'key': "PRIVATE ",
			'value': 216
		}, {

			'key': "PUBLIC",
			'value': 278
		}
		, {

			'key': "GOVERNMENT",
			'value': 192
		}
	]
	var employeeStrength =  [ //Pie data
		{
			'key': "employeeStrength > 5000",
			'value': 2
		}, {
			
			'key': "employeeStrength 2000-5000 ",
			'value': 1
		}
		, {
			
			'key': "employeeStrength 500-1000",
			'value': 4
		}, {
			
			'key': "employeeStrength < 500",
			'value': 6
		}
	]

	function plotPie(data, id, colors, height,txtSize,txtColor,islegends,legendwidth,legendmargintop,noSplice) {
	 	var pieData = jQuery.extend(true, [], data);
	    pieData = pieData.sort(function(a, b) {
	        return b.value - a.value
	    })
//	    if(!noSplice){
//	    	pieData.splice(3)
//	    }
	    var maxval = d3.max(pieData, function(d) {
	        return d.value;
	    });
	    var sum = d3.sum(pieData, function(d) {
	        return d.value;
	    });

	    var piechartoption = { //Piechart option
	        container: id,
	        colors: colors,
	        data: pieData,
	        height: height,
	        islegends: islegends?islegends:false,
	        //     islegendleft: true,
	        legendwidth: legendwidth?legendwidth:30,
	        istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
	        txtColor: txtColor?txtColor:'#8F4582',
	        txtSize: txtSize?txtSize:'10px',
	        legendmargintop:legendmargintop?legendmargintop: 30
	    };
	    new reusablePie(piechartoption);
	}

	plotPie(entityStatus, '#entityStatusPie', color,150,"10px","#4d6772") 
	plotPie(legalEntity, '#legalEntityTypesPie', color,150,"10px","#4d6772") 
	plotPie(timeIncorporation, '#timeIncorporationPie', color,150,"10px","#4d6772") 
	plotPie(employeeStrength, '#employeeStrengthPie', color,150,"10px","#4d6772")
	 */
		/*
     * @purpose: get Company names on page load
     * @created:1st May 2018
     * @params: token(object)
     * @author: Amritesh
    */
	$scope.list = [];
	 $scope.searchCompanyList= function(val,event){
		 EntitySearchApiService.getlistofOrganisations(val,10).then(function (resp) {
			if (resp && resp.data && resp.data.hits && resp.data.hits.length > 0) {
				$scope.list = [];
				angular.forEach(resp.data.hits, function (d, i) {
					var updatedTime = d["@updated"] ? moment(d["@updated"]).format("YYYY-MM-DD HH:mm:ss") : "-";
					var createdTime = d["@created"] ? moment(d["@created"]).format("YYYY-MM-DD HH:mm:ss") : "-";
					var key = {
						name: d["vcard:organization-name"] ? d["vcard:organization-name"] : "-",
						number: "",		
						headquater: d["mdaas:HeadquartersAddress"] ? d["mdaas:HeadquartersAddress"]["fullAddress"] ? d["mdaas:HeadquartersAddress"]["fullAddress"] : "-" : "-",
						location: d["isIncorporatedIn"] ? d["isIncorporatedIn"] : "-",
						domicile: d.isDomiciledIn ? d.isDomiciledIn : "-",
						lastUpdatedOn: updatedTime,
						createdOn: createdTime,
						identifier: d["@identifier"],
						status: d["@status"],
						type: d["hasPrimaryIndustryGroup"]? d["hasPrimaryIndustryGroup"] : "-",
							
					}
					$scope.list.push(key);
					//if(key.location !== '' && key.location)
					//$scope.list = $scope.list.length === 0 ? $scope.list.push(key) : $scope.list;
					//$scope.list.push(key);

				});
			} else {
			}
			//	DTDefaultOptions.setLoadingTemplate('<i class="fa fa-spinner fa-spin fa-3x custom-spinner full-page-spinner"></i>');
		}, function (respError) {
		});
	}
	
		/*
     * @purpose: get suggested names  
     * @created:1st May 2018
     * @params: token(object)
     * @author: Ram singh
    */
	function getSuggestedName(val){
		return EntitySearchApiService.SearchRecentCompany(val).then(function(suggestedresponse){
			console.log('suggestedresponse: ', suggestedresponse);
		//	return orgResponse.data;
//			return orgResponse.data.items;
		},function(orgError){

		});
	}
	$scope.SearchCompany = function(event){
	 if(event.which === 13){
			$scope.onClickSearchCompanyList( $scope.search_organization_name)
		}
	}
	/*
     * @purpose: Routing the Page to result page
     * @created:1st May 2018
     * @params: token(object)
     * @return: success, error functions
     * @author: Ram Singh
    */ 
	$scope.ceriSearchRouting = function (name, event) {
	//	SaveRecentSearches(name);
	if(name){
		if (event && event.type == "click") {} else if (event && event.which === 13) {
			//$scope.onClickSearchCompanyList(item.company_number)
		}
		$state.go("entitySearchResult", {
			companyName: name
		});
	}
	};
	/*
     * @purpose: routing to the Specfic Page 
     * @created:1st May 2018
     * @params: token(object)
     * @return: success, error functions
     * @author: Ram Singh
    */ 
	$scope.EntityRouting = function (data) {
		console.log('data: ', data);
		if (data.type === "case") {
			var url = EHUB_FE_API + "#/act/" + data.caseId;
			window.open(url, "_blank");
		} else if (data.type === "vla") {
			var vlaurl = EHUB_FE_API + "#/linkAnalysis?caseId=" + data.caseId + "&entity=" + data.name;
			window.open(vlaurl, "_blank");
		} else if (data.type === "Corporate") {
			var url = EHUB_FE_API;
			if(data.identifiier){
				url = url + "entity/#!/company/" + data.name+"?eid="+data.identifiier;
			}else{
				url = url + "entity/#!/company/" + data.name;
			}
			window.open(url, "_blank");
		} else if (data.type === "Individual") {
			var url = EHUB_FE_API + "entity/#!/person/" + data.name;
			window.open(url, "_blank");
		}
	};
	/*
     * @purpose: Save the Recent Search
     * @created:1st May 2018
     * @params: token(object)
     * @return: success, error functions
     * @author: Ram Singh
    */ 
	function SaveRecentSearches(name) {
		var data ={
			searchQuery:name
		}
		EntitySearchApiService.SaveasRecentSearch(data).then(function (resp) {
			console.log('resp: ', resp);
		}, function (eror) {

		});
	}
	/*
     * @purpose: getRecent Searches made
     * @created:29 july 2018
     * @params: token(object)
     * @return: success, error functions
     * @author: Ram 
    */
	function loadRecentSearches(){
		EntitySearchApiService.RecentSearchMade($rootScope.ehubObject.token).then(function (resp) {
			 
			console.log('resp: ', resp);
			for (var i = 0 ;i< resp.data.length; i++) {
				resp.data[i].search = JSON.parse(resp.data[i].search);		
				$scope.entityNewSearch.recentSearch.push({
					search :resp.data[i].search,
					date:resp.data[i].createdDate
				});
			}
		}, function (eror) {

		});
	}
	loadRecentSearches();
	//$scope.searchCompanyList(data);
	
	/*
     * @purpose: get Concernced Company on SEarch
     * @created:1st May 2018
     * @params: token(object)
     * @return: success, error functions
     * @author: Amritesh
    */
	
$scope.onClickSearchCompanyList= function(val,isloadResult){
	EntitySearchApiService.getConcernedCompanies(val).then(function (resp) {
			if (resp && resp.data && resp.data.items && resp.data.items.length > 0) {
				if(isloadResult){
					$state.go("ceriSearchResult",{companyName:resp.data.items[0].company_number});
				}else{					
					$scope.list = [];
					angular.forEach(resp.data.items, function (d, i) {
						
						var key = {
							name: d["title"] ? d["title"] : "-",
				            number:d["company_number"] ? d["company_number"] : "-",
							headquater: d["address"] ? d["address"]["country"] ? d["address"]["country"] : "-" : "-",
							location: d["address"] ? d["address"]["locality"] ? d["address"]["locality"] : "-" : "-",
							type: d["company_type"]? d["company_type"] : "-",
						}
						$scope.list.push(key);						
					});
				}
			} else {
			}
			//	DTDefaultOptions.setLoadingTemplate('<i class="fa fa-spinner fa-spin fa-3x custom-spinner full-page-spinner"></i>');
		}, function (respError) {
		});
	}
/*
 * @purpose: Trans navigating to different page 
 * @created:1st May 2018
 * @params: token(object)
 * @author: Amritesh
*/
$scope.onClickSwitchToNextPage= function(values){
	if(!values.number){
		$scope.onClickSearchCompanyList(values.name,"loadResult")
	} else{
		$state.go("ceriSearchResult",{companyName:values.number});
	}
}

$scope.clearSearch = function (value) {
	$scope.search_organization_name ='';
	console.log('value: ', value);
   
};
$scope.NavigateoAdvanceSearch = function(){
	var url =EHUB_FE_API+"/#/enrich";
	window.open(url, '_blank');
}
};
