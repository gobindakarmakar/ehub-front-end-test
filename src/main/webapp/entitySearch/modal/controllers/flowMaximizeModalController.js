'use strict';
angular.module('entitySearchApp')
	   .controller('FlowMaximizeModalController', flowMaximizeModalController);

		flowMaximizeModalController.$inject = [
			'$scope',
			'$rootScope',
			'Id',
			'$uibModalInstance',
			'ChartOptions',
			'$timeout'
		];
	    function flowMaximizeModalController(
	    		$scope,
	    		$rootScope,
	    		Id,
	    		$uibModalInstance,
	    		ChartOptions,
	    		$timeout
	    		){
	    	console.log("flowMaximizeModalController", Id, ChartOptions);
	    	$scope.modalOpen = true;
	    	
	    	$scope.closeWidgetCaptureModal = function(){
	    		$uibModalInstance.dismiss('close');
	    		$scope.modalOpen = false;
	    	};
	    	/*
			 * @purpose: Append chart for full screen 
			 * @created: 09 may 2018		
			 * @author: swathi
			 */
	    	if(Id == 'flowChartViewDiv'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
				     setTimeout(function(){
				    	 $('#maximizeChart').orgchart({
				    		'data': ChartOptions,
				    		'nodeContent': 'title'
				    	 });
				     },0)
				     angular.element('.orgchart').css('width', '10px');
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
		    }
	    }