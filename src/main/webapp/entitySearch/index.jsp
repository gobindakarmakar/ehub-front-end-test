<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html ng-app="entitySearchApp">

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <link rel="shortcut icon" href="../assets/images/logo-new.png" />
            <title>Search</title>

            <!-------------------------------  Vendor Styles Starts  ------------------------------->
			<link href="./assets/css/due-vendor.min.css" rel="stylesheet" type="text/css" />
            <!-- -----------------------------  Vendor Styles Ends  -------------------------------->

            <!---------------------------------  Custom Styles Starts  ----------------------------->
            <link href="./assets/css/due-styles.min.css" rel="stylesheet" type="text/css" />            
            <!-------------------------------  Custom Styles Ends  --------------------------------->
            <link href="../vendor/jquery/css/jquery.orgchart.css" rel="stylesheet"  type="text/css" />
            <style>input[type=number]::-webkit-inner-spin-button, 
                input[type=number]::-webkit-outer-spin-button { 
                  -webkit-appearance: none; 
                  -moz-appearance: none;
                  appearance: none;
                  margin: 0;      
                }</style>
        </head>

        <body ng-cloak class="custom-scroll-wrapper">
            <div ng-show="topPanelPreview">
                <%@include file="../WEB-INF/jsp/topPanel.jsp" %>
            </div>
            <div ui-view></div>
            <script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>
            <!--  Chat Panel Wrapper Starts  -->
            <%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
                <!--  Chat Panel Wrapper Ends  -->
                <!-- 
				==============================================================================
				*************************  Local Path Starts ********************************
				==============================================================================
			-->
                <!--------------------------------  Common Vendor Js Starts  ------------------------------->
                <script src='../vendor/jquery/js/jquery.min.js'></script>
                <script src='../vendor/jquery/js/jquery-ui.js'></script>
                <script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
                <script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
                <script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
                <script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
                <script src='../vendor/jquery/js/bootstrap.min.js'></script>
                <script src='../vendor/jquery/js/displacy-entity.min.js'></script>
                
                <script src='../charts/d3.v3.min.js'></script>
                <script src='../charts/d3.v4.min.js'></script>
                <script src='../charts/d3v3.js'></script>
                <script src='../charts/d3js.js'></script>

                <script src='../vendor//angular/js/angular.min.js'></script>
                <script src='../vendor//angular/js/angular-ui-router.min.js'></script>
                <script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
                <script src='../vendor//angular/js/html2canvas.js'></script>
                <script src='../vendor//angular/js/ng-file-upload.min.js'></script>
                <script src='../vendor//angular/js/angular-flash.min.js'></script>
                <script src='../vendor/angular/js/angular_multiselect.js'></script>
                <script src="../vendor/jquery/js/jquery.orgchart.js"></script>
                

                <script src='../scripts/VLA/js/cola.v3.min.js'></script>
                <script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
                <script src='../scripts/VLA/js/cytoscape-cola.js'></script>
                <script src='../scripts/VLA/js/jquery.qtip.js'></script>
                <script src='../scripts/VLA/js/cytoscape-qtip.js'></script>
                <!--------------------------------  Common Vendor Js Ends    ------------------------------->

                <!-------------------------------- dueDiligence Vendor Js Starts  ------------------------------->
                <script src='../scripts/VLA/js/cytoscape-cose-bilkent.js'></script>
                <script src='../scripts/VLA/js/cytoscape-cxtmenu.js'></script>
                <script src='../scripts/VLA/js/cytoscape-markov-cluster.js'></script>
                <script src='../scripts/VLA/js/cytoscape-navigator.js'></script>
                <script src='../scripts/VLA/js/cytoscape-ngraph.forcelayout.js'></script>
                <script src='../scripts/VLA/js/cytoscape-panzoom.js'></script>
                <script src='../scripts/VLA/js/cytoscape-undo-redo.js'></script>
                <script src='../scripts/VLA/js/weaver.min.js'></script>
                <script src='../scripts/VLA/js/cytoscape-spread.js'></script>
                <script src='../scripts/VLA/js/random-color.js'></script>
                <script src='../scripts/VLA/js/spectrum.js'></script>
                <script src='../scripts/VLA/js/typeahead.bundle.js'></script>
                <script src='../scripts/VLA/js/jquery.selectric.min.js'></script>
                <script src='../scripts/VLA/js/custom_config.js'></script>
                <script src='../scripts/VLA/js/app.js'></script>
                <!--     	 <script src='../scripts/VLA/js/app_locale_ua.js'></script> -->
                <!--     	 <script src='../scripts/VLA/js/app_locale_ar.js'></script> -->
                <script src='../scripts/VLA/tagcloud/js/tagcloud.js'></script>
                <script src='../scripts/VLA/js/bluebird.min.js'></script>
                <script src='../scripts/VLA/js/base64toblob.js'></script>
                <script src='../scripts/VLA/js/loadash.js'></script>
                <script src='../vendor/jquery/js/moment.min.js'></script>
                <script src='../vendor/angular/js/angular-moment.min.js'></script>
                <script src='../vendor/angular/js/angular-datatables.min.js'></script>
                <script src='../vendor/angular/js/FileSaver.min.js'></script>
                <script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
                <script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
                <script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
		        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
                <!--------------------------------  dueDiligence Vendor Js Ends    ------------------------------->
			    <!--------------------------------  dueDiligence Charts Js Starts  ------------------------------>
				<script src='../charts/topojson.min.js'></script>
				<script src='../charts/datamaps.all.js'></script>
				<script src='../charts/WorldMap/js/WorldChart.js'></script>
				<script src='../charts/lineChart.js'></script>
				<script src='../charts/reusablePie.js'></script>
				<script src='../charts/stackedtime.js'></script>
				<script src='../charts/cloud.js'></script>
				<script src='../charts/hotTopics.js'></script>
				<script src='../charts/groupedColumChart.js'></script>
				<script src="../charts/cloud.js"></script>
				<script src="../charts/bubbleEntity.js"></script>
				<script src="../charts/stackedtime.js"></script>
				<script src="../charts/topojson.min.js"></script>
				<script src="../charts/WorldMap/js/WorldChart.js"></script>
				<script src="../charts/timeformat.js"></script>
				<script src="../charts/timeformatsupport.js"></script>
				<script src="../charts/SimpleBarChart.js"></script>
				<script src="../charts/verticalNegativeBarChart.js"></script>
				<script src="../charts/ordinalLineChart.js"></script>
				<script src="../charts/areaLineChart.js"></script>
				<script src="../charts/networkChartEntity.js"></script>
				<script src="../charts/verticalBarChart.js"></script>
				<script src="../charts/NegativeGroupedColumnChart.js"></script>
				<!--------------------------------dueDiligence  Chart scripts Js Ends   ------------------------------->
                
                
                <!-------------------------------- dueDiligenceScripts Js Starts   ------------------------------->
                <script src="../scripts/app.js"></script>
                <script src="entitySearch.app.js"></script>
                <script src="entitySearch.config.js"></script>
                <script src="entitySearch.run.js"></script>
                 <script src='./constants/entitySearch.constant.js'></script>
                <script src="./controllers/entitySearchLandingController.js"></script>
                <script src= "./controllers/entitySearchResultController.js"></script>
                <script src= "./services/entitySearch.api.service.js"></script>
                <script src="./controllers/data.PopUp.controller.js"></script>
                <script src="./modal/controllers/flowMaximizeModalController.js"></script>
                <!-------------------------------- dueDiligence Charts Js Ends ------------------------------>
               
               
<!--                 <script src='../vendor//angular/js/angular-flash.min.js'></script> -->
               
               
                
                <!--------------------------------  Common Scripts Js Starts  ------------------------------>
               
                <script src='../scripts/discover/constants/discover.constant.js'></script>
                <script src='../scripts/act/constants/act.constant.js'></script>
                <script src='../scripts/common/constants/app.constant.js'></script>
                <script src='../scripts/common/constants/common.constant.js'></script>
                <script src="../scripts/enrich/services/enrich.graph.service.js"></script>
			    <script src="../entity/services/entity.graph.services.js"></script>
                <script src="../scripts/manage/services/dataCuration.api.service.js"></script>
                <script src='../scripts/common/services/shared.service.js'></script>
                <script src='../scripts/common/services/top.panel.api.service.js'></script>
                <script src='../scripts/common/services/upload.file.service.js'></script>
                <script src="../scripts/common/services/common.service.js"></script>
                <script src='../scripts/common/services/top.panel.api.service.js'></script>
                <script src='../scripts/common/services/upload.file.service.js'></script>
                <script src='../scripts/common/services/shared.service.js'></script>
                <script src='../scripts/discover/services/discover.api.service.js'></script>
                <script src='../scripts/act/services/act.api.service.js'></script>
                <script src='../scripts/act/services/act.graph.service.js'></script>
                <script src='../scripts/common/js/top.panel.controller.js'></script>
                <script src='../scripts/common/services/riskScore.service.js'></script>
    			<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
                <script src='../scripts/common/js/advanced.search.controller.js'></script>
                <script src='../scripts/common/js/user.events.controller.js'></script>
                <script src='../scripts/common/js/my.clipboard.controller.js'></script>
                <script src="../scripts/common/js/notification.controller.js"></script>
                <script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
                <script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
                 <script src="../scripts/common/js/chatbot.controller.js"></script>
                <script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
                <script src='../scripts/common/js/submenu.controller.js'></script>
                <script src="../scripts/common/modal/js/create.event.controller.js"></script> 
                <script src="../scripts/common/modal/js/participants.event.controller.js"></script>
                <!--------------------------------  Common Scripts Js Ends   ------------------------------->

             
                
              

                <!---

                ============================================================================== ********************** Local Path Ends ********************* ==============================================================================*/
-->

                <!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/
-->
                <!-- 		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
                <!-- 		
<!-- 	    ------------------------------     Common Scripts Js Starts  ---------------------------- -->
                <!-- 		
<!-- 		<!--------------------------------  Common Scripts Js Ends   ------------------------------->

                <!-- 		<!-------------------------------- dueDiligenceScripts Js Starts  ------------------------------>
                <!-- 		
<!-- 		<!--------------------------------dueDiligence Scripts Js Ends   ------------------------------->

                <!-- 		<!-------------------------------- dueDiligence Charts Js Starts  ------------------------------>
                <!-- 		
<!-- 		<!-------------------------------- dueDiligence charts Js Ends   ------------------------------->
<!--new minified Js start -->
<!-- <script src="../vendor/common.vendor.new.min.js"></script>
<script src="./assets/minifiedJs/entitySearch.vendor.new.min.js"></script>
<script src="../scripts/app.js"></script>
<script src="entitySearch.app.js"></script>
<script src="entitySearch.config.js"></script>
<script src="entitySearch.run.js"></script>
<script src='./constants/entitySearch.constant.js'></script>
<script src="./controllers/entitySearchLandingController.js"></script>
<script src= "./controllers/entitySearchResultController.js"></script>
<script src= "./services/entitySearch.api.service.js"></script>
<script src="./controllers/data.PopUp.controller.js"></script>
<script src="./modal/controllers/flowMaximizeModalController.js"></script> 

<script src="./assets/minifiedJs/entitySearch.charts.new.min.js"></script>


<script src="../scripts/common.scripts.new.min.js"></script>
<script src="./assets/minifiedJs/entitySearch.scripts.new.min.js"></script> -->
<!--new minified Js end -->
                <!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->

        </body>

        </html>