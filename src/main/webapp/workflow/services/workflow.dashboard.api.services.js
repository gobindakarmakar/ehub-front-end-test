'use strict';
 
elementApp
	   .factory('WorkflowApiService', workflowApiService);

       workflowApiService.$inject = [
			'$http',
			'EHUB_API',
			'$q',
			'$rootScope'
		];
		
		function workflowApiService(
				$http,
				EHUB_API,
				$q,
				$rootScope){
	
			return {
				getstageRequest: getstageRequest,
				getSampleData:getSampleData,
				postSinkCassandaratable:postSinkCassandaratable,
				createCassandaraKeyspace:createCassandaraKeyspace,
				createNewNotebook : createNewNotebook ,
				getNotebookDetails: getNotebookDetails,
				reloadNotebookStage : reloadNotebookStage,
				deleteNotebookStage : deleteNotebookStage,
				updateNotebookStage : updateNotebookStage,
				saveWorkflowCase: saveWorkflowCase,
				updateWorkflowCase: updateWorkflowCase,
				getWorkflowCase: getWorkflowCase
			};
			/*
		     * @purpose: get cases summary by limit
		     * @created: 19 jan 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getstageRequest(token, data,type){
                var apiUrl =EHUB_API + "workflow/es/workflow?token="+token;
		        var request = $http({
		            method: type,
		            url: apiUrl,
                    data:data                    
		        });
		        return(request
		                .then(getstageRequestSuccess)
		                .catch(getstageRequestError));
		
		        /*getstageRequest error function*/
		        function getstageRequestError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function getstageRequestSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get Sample Data
		     * @created: 14 jun 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Ram 
		    */
		   function getSampleData(workflowID, stageid,token){
			var apiUrl =EHUB_API + 'workflow/es/workflowstats/' + workflowID + '/' + stageid + '/LATEST?token='+token;
			var request = $http({
				method: 'GET',
				url: apiUrl
			});
			return(request
					.then(getSampleDataSuccess)
					.catch(getSampleDataError));
	
			/*getSampleData error function*/
			function getSampleDataError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
		   /*getSampleData success function*/
			function getSampleDataSuccess(response) {
				return(response);
			}
		}
		/*
		     * @purpose: Save SinkCassandara -cassandra create table api
		     * @created: 22 jun 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Ram 
		    */
		   function postSinkCassandaratable(data,token){
			var apiUrl =EHUB_API + 'workflow/es/cassandra/create_family?token='+token;
			var request = $http({
				method: 'POST',
				url: apiUrl,
				data:data
			});
			return(request
					.then(postSinkCassandaratableSuccess)
					.catch(postSinkCassandaratableError));
	
			/*postSinkCassandaratable error function*/
			function postSinkCassandaratableError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
		   /*postSinkCassandaratable success function*/
			function postSinkCassandaratableSuccess(response) {
				return(response);
			}
		}
		/*
		     * @purpose: Save SinkCassandara -cassandra create Keyspace api
		     * @created: 27 jun 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Ram 
		    */
		   function createCassandaraKeyspace(data,token){
			var apiUrl =EHUB_API + 'workflow/es/cassandra/create_keyspace?token='+token;
			var request = $http({
				method: 'POST',
				url: apiUrl,
				data:data
			});
			return(request
					.then(createCassandaraKeyspaceSuccess)
					.catch(createCassandaraKeyspaceError));
	
			/*createCassandaraKeyspace error function*/
			function createCassandaraKeyspaceError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
		   /*createCassandaraKeyspace success function*/
			function createCassandaraKeyspaceSuccess(response) {
				return(response);
			}
		}
		   
			/*
		     * @purpose: create new notebook
		     * @created: 11 July 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Virendra
		    */
			function createNewNotebook(token, data,type){
               var apiUrl =EHUB_API + "workflow/es/notebook?token="+token;
//				var apiUrl ="http://159.89.108.32:9091/es/api/notebook";  
				var request = $http({
		            method: "POST",
		            url: apiUrl,
                   data:data                    
		        });
		        return(request
		                .then(notebookCreateSuccess)
		                .catch(notebookCreateError));
		
		        /*getstageRequest error function*/
		        function notebookCreateError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function notebookCreateSuccess(response) {
		            return(response);
		        }
			} 
			
			/*
		     * @purpose: get notebook details
		     * @created: 11 July 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Virendra
		    */
			function getNotebookDetails(token, data){
               var apiUrl =EHUB_API + "workflow/es/notebook/"+ data.workflowId + "/" + data.notebookName +"?token="+token;
//				var apiUrl ="http://159.89.108.32:9091/es/api/notebook/"+ data.workflowId + "/" + data.notebookName ;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
//                   data:data                    
		        });
		        return(request
		                .then(notebookSuccess)
		                .catch(notebookError));
		
		        /*getstageRequest error function*/
		        function notebookError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function notebookSuccess(response) {
		            return(response);
		        }
			} 
			
			/*
		     * @purpose: relaod Notebook Stage
		     * @created: 11 July 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Virendra
		    */
			function reloadNotebookStage(token, data){
               var apiUrl =EHUB_API + "workflow/es/workflow/"+ data.workflowId + "/" + data.stageId + "/start?token="+token;
//				var apiUrl ="http://159.89.108.32:9091/es/api/workflow/"+ data.workflowId + "/" + data.stageId + "/start";
				var request = $http({
		            method: "GET",
		            url: apiUrl,
//                   data:data                    
		        });
		        return(request
		                .then(notebookRunSuccess)
		                .catch(notebookRunError));
		
		        /*getstageRequest error function*/
		        function notebookRunError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function notebookRunSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: delete Notebook
		     * @created: 12 July 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: Virendra
		    */
			function deleteNotebookStage(token, data){
               var apiUrl =EHUB_API + "workflow/es/notebook/" + data.workflowId + "/" + data.notebookName + "?token="+token;
//				var apiUrl ="http://159.89.108.32:9091/es/api/notebook/" + data.workflowId + "/" + data.notebookName;
				var request = $http({
		            method: "DELETE",
		            url: apiUrl,
//                   data:data                    
		        });
		        return(request
		                .then(notebookRunSuccess)
		                .catch(notebookRunError));
		
		        /*getstageRequest error function*/
		        function notebookRunError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function notebookRunSuccess(response) {
		            return(response);
		        }
			}
			
			function updateNotebookStage(token, data){
				var new_data = {
				"notebookName" : data.notebookName,
				"workflowId": data.workflowId,
			    "workflowName": data.workflowName,
			    "stageId": data.stageId,
			    "stageName": "jupyternotebook_operations",
			    "kernelName" : data.kernelName
				};
//	               var apiUrl =EHUB_API + "workflow/es/notebook/" + data.workflowId + "/" + data.notebookName + "?token="+token;
					var apiUrl ="http://159.89.108.32:9091/es/api/notebook/" + data.workflowId + "/" + data.oldName;
					var request = $http({
			            method: "PUT",
			            url: apiUrl,
	                   data:new_data                    
			        });
			        return(request
			                .then(notebookupdateSuccess)
			                .catch(notebookupdateError));
			
			        /*getstageRequest error function*/
			        function notebookupdateError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			       /*getstageRequest success function*/
			        function notebookupdateSuccess(response) {
			            return(response);
			        }
			}
			
			 /*
		     * @purpose: save Workflow Case
		     * @created: 27th sep 2018
		     * @params: data(object), id(string)
		     * @return: success, error functions
		     * @author: swathi
		     */
		    function saveWorkflowCase(data, id) {
		        var defer = $q.defer();
		        var apiUrl = EHUB_API + 'workflow/es/workflow-cases/' + id + '?token=' + $rootScope.ehubObject.token;
		        var request = $http({
		            method: 'POST',
		            url: apiUrl,
		            data: data
		        });
		        request
		            .then(saveWorkflowCaseSuccess)
		            .catch(saveWorkflowCaseError);

		        /*saveWorkflowCase error function*/
		        function saveWorkflowCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                defer.reject(response.data);
		            }
		            /*Otherwise, use expected error message.*/
		            defer.reject(response.data.message);
		        }

		        /*saveWorkflowCase success function*/
		        function saveWorkflowCaseSuccess(response) {
		            defer.resolve(response);
		        }
		        return defer.promise;
		    };
		    
		    /*
		     * @purpose: update Workflow Case
		     * @created: 3rd oct 2018
		     * @params: data(object), id(string)
		     * @return: success, error functions
		     * @author: swathi
		     */
		    function updateWorkflowCase(data, id) {
		        var defer = $q.defer();
		        var apiUrl = EHUB_API + 'workflow/es/workflow-cases/' + id + '?token=' + $rootScope.ehubObject.token;
		        var request = $http({
		            method: 'PUT',
		            url: apiUrl,
		            data: data
		        });
		        request
		            .then(updateWorkflowCaseSuccess)
		            .catch(updateWorkflowCaseError);

		        /*updateWorkflowCase error function*/
		        function updateWorkflowCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                defer.reject(response.data);
		            }
		            /*Otherwise, use expected error message.*/
		            defer.reject(response.data.message);
		        }

		        /*updateWorkflowCase success function*/
		        function updateWorkflowCaseSuccess(response) {
		            defer.resolve(response);
		        }
		        return defer.promise;
		    };
		    
		    /*
		     * @purpose: get Workflow Case
		     * @created: 3rd oct 2018
		     * @params: id(string)
		     * @return: success, error functions
		     * @author: swathi
		     */
		    function getWorkflowCase(id) {
		        var defer = $q.defer();
		        var apiUrl = EHUB_API + 'workflow/es/workflow-cases/' + id + '?token=' + $rootScope.ehubObject.token;
		        var request = $http({
		            method: 'GET',
		            url: apiUrl
		        });
		        request
		            .then(getWorkflowCaseSuccess)
		            .catch(getWorkflowCaseError);

		        /*getWorkflowCase error function*/
		        function getWorkflowCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                defer.reject(response.data);
		            }
		            /*Otherwise, use expected error message.*/
		            defer.reject(response.data.message);
		        }

		        /*getWorkflowCase success function*/
		        function getWorkflowCaseSuccess(response) {
		            defer.resolve(response);
		        }
		        return defer.promise;
		    };
		};