'use strict';

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', function($stateProvider, $provide, $urlRouterProvider, EHUB_FE_API, $locationProvider){
	 $provide.factory('$stateProvider', function () {
		 return $stateProvider;
	 });
 	$provide.factory('$urlRouterProvider', function () {
        return $urlRouterProvider;
    });
}]);

