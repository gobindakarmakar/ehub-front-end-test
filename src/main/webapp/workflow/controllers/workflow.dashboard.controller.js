'use strict';
angular.module('ehubWorkflowApp')
	   .controller('WorkflowDashboardController', workflowDashboardController);

		workflowDashboardController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$http',
			'$location',
			'$timeout',
			'HostPathService',
			'$stateParams',
			'EHUB_API',
			'WorkflowApiService'
		];
		
		function workflowDashboardController(
				$scope, 
				$rootScope,
				$state,
				$http,
				$location,
				$timeout,
				HostPathService,
				$stateParams,
				EHUB_API,
				WorkflowApiService) {
			$scope.Dashboardobj ={
				createNew :true,
				cloneNew:false,
				landingData :[],
				workflow_name :'demo_workflow'
			};
			$scope.rootPath = HostPathService.getHostPath();
			$scope.callSubMenuPath = function() {
				return $rootScope.rootPath + 'static/js/commonSubMenu/submenu.html';
			};
			var clone_name ='';
			var workflowcloneId ='';
			$(".spinner_async_index").css("display", "block");
			var url = EHUB_API + "workflow/es/workflow?token="+$rootScope.ehubObject.token;
			// var url = $rootScope.rootPath + "/workflow/";
			$.ajax({
				type : "GET",
				url : url,
//				headers:{
//					"token":$rootScope.ehubObject.token
//				},
				cache : true,
				contentType : "application/x-www-form-urlencoded;",
				dataType : "json",
				success : function(landingData) {
					//
					$rootScope.populateLanding(landingData);
					
					$(".spinner_async_index").css("display", "none");
				},
				error : function(o) {
					// alert("error");
					$(".spinner_async_index").css("display", "none");
					var msg = "";
					if (o.responseText) {
						
						msg = o.responseText.split('"message":"')[1].split('"')[0];
						// 
					} else {
						msg = "Something went wrong! Please try again."
					}
					$("#saveInfoText").html(msg);
					$('#saveNewWorkflowModal').modal('show');
					setTimeout(function() {
						$('#saveNewWorkflowModal').modal('hide');
					}, 2000);
				}
			});

			$rootScope.populateLanding = function (landingData) {
				var tcontent = "<tbody>";

				$.each(landingData,function(i, v) {
									var timeduation, status = "In progress";
									if (v.workflow_data.end_time) {
										status = "Completed";
									}
									if (v.workflow_data.created_at
											&& v.workflow_data.updated_at) {
										timeduation = getTimeFormated(new Date(v.workflow_data.updated_at)- new Date(v.workflow_data.created_at));
									}
									if (v.workflow_data.created_at) {
										var created = v.workflow_data.created_at.split(":");
										var created_at_min = [ created.slice(0, 2),created.slice(2) ].map(function(item) {
											return item.join(":");
										});
									} else {
										return "-";
									}
          
									var updated = v.workflow_data.updated_at.split(":");
									var updated_at_min = [ updated.slice(0, 2),
											updated.slice(2) ].map(function(item) {
										return item.join(":");
									});
									tcontent = tcontent
											+ "<tr id='workflowtr_"
											+ (v.workflow_id)
											+ "'><td>"
											+ ((v.workflow_id) ? (v.workflow_id) : '-')
											+ "</td><td>"
											+ ((v.workflow_data.name) ? (v.workflow_data.name)
													: '-')
											+ "</td><td>"
											+ ((created_at_min[0]) ? (created_at_min[0])
													: '-')
											+ "</td><td>"
											+ ((updated_at_min[0]) ? (updated_at_min[0])
													: '-')
											+
											// "</td><td>" + timeduation +
											// "</td><td>" + timeduation +
											// "</td><td>" + status +
											// "</td><td>" + "-" +
											"</td><td class='text-center'><a class='text-info monitorWorkflow' href='javascript:void(0)'><i class='fa fa-desktop'></i></a></td><td class='text-center'><a class='text-info editWorkflow' href='javascript:void(0)'><i class='fa fa-pencil-square-o'></i></a></td><td class='text-center'><a class='text-danger deleteWorkflow' href='javascript:void(0)'><i class='fa fa-trash-o'></i></a></td><td class='text-center'><a class='text-info cloneWorkflow' href='javascript:void(0)'><i class='fa  fa-clone'></i></a></td>";
								});

				tcontent = tcontent + "</tbody>";

				$('#landingTableData').append(tcontent);

				$('#landingTableData').DataTable({
					"bLengthChange" : false,
					"showNEntries" : false,
					"bInfo" : false,
					"order" : [ [ 1, "asc" ] ],
					"columnDefs" : [ {
						targets : [ 0, 4, 5, 6 , 7],
						orderable : false
					},
					// { "orderSequence": [ "desc" ], "targets": [1] }
					]
				});
			}

			$("body")
					.on(
							"click",
							".editWorkflow",
							function() {
								var work_flow_id = $(this).parent().parent().attr("id")
										.split("workflowtr_")[1];
//
//								var windowHref = window.location.href
//										.split('/workflow');
//								// alert(windowHref);
//								window.location.href = (windowHref[0]
//										+ "editWorkflow?id=" + work_flow_id);
//								$("body").unbind("click");
//								window.location.href = (windowHref[0]
//										+ "editWorkflow?id=" + work_flow_id);
//								
								$(".spinner_async_index").css("display", "block");
								$state.go('editWorkflow',{id: work_flow_id});
							});
			$("body").on(
					"click",
					".monitorWorkflow",
					function() {
						var work_flow_id = $(this).parent().parent().attr("id").split(
								"workflowtr_")[1];

//						var windowHref = window.location.href.split('/workflow');
//
//						window.location.href = (windowHref[0] + "editWorkflow?id="
//								+ work_flow_id + "&monitor=true");
//						
						$(".spinner_async_index").css("display", "block");
						$state.go('editWorkflow', {'id': work_flow_id, 'monitor': true})
					});
			
			$("body").on("click", ".deleteWorkflow", function () {
				
			    var work_flow_id = $(this).parent().parent().attr("id").split("workflowtr_")[1];
			    var _this = $(this).closest('tr').attr("id");
			    $("#ConfirmButton").addClass("id_" + work_flow_id);
			    $("#ConfirmButton").addClass("cuurrow_" + _this);
			    $("#confirmBox").show();

//			    if (window.confirm("Do you really want to Delete this workflow?")) {
//			        var work_flow_id = $(this).parent().parent().attr("id").split("workflowtr_")[1];
//			        var _this = $(this);
//			        var url = server + "/workflow/" + work_flow_id;
			//
//			        $.ajax({
//			            type: "DELETE",
//			            url: url,
//			            cache: true,
//			            contentType: "application/x-www-form-urlencoded;",
//			            dataType: "json",
//			            success: function () {
//			                _this.closest('tr').hide();
//			            },
//			            error: function (o) {
//			                alert("error");
//			            }
//			        });
//			    }
			});


			$("body").on("click", "#ConfirmButton", function (i, d) {
				$("#confirmBox").hide();
				$(".spinner_async_index").css("display", "block");
			    var rowId = $(this).attr("class").split("cuurrow_")[1];
			    var work_flow_id = $(this).attr("class").split("id_")[1].split(" ")[0];
			    var url = EHUB_API + "workflow/es/workflow/" + work_flow_id + "?token=" + $rootScope.ehubObject.token;
				$("#ConfirmButton").removeClass();
			    $("#ConfirmButton").addClass("text-uppercase confirmBtn");
			    $.ajax({
			        type: "DELETE",
			        url: url,
			        cache: true,
			        contentType: "application/x-www-form-urlencoded;",
			        dataType: "json",
			        success: function (o) {
			            $("#" + rowId).hide();
			            $("#saveInfoText_index").html(o.message);
			            $(".spinner_async_index").css("display", "none");
			            $('#saveNewWorkflowModal_index').modal('show');
			            setTimeout(function () {
			                $('#saveNewWorkflowModal_index').modal('hide');
			            }, 1000);
			        },
			        error: function (o) {
			            alert("error");
			            $(".spinner_async_index").css("display", "none");
			        }
			    });
			});
			$("body")
					.on(
							"click",
							".cloneWorkflow",
							function() {
								workflowcloneId = $(this).parent().parent().attr("id")
										.split("workflowtr_")[1];
								 clone_name =$(this).parent().parent().find('td:eq(1)')[0].innerText;								
								cloneRow(clone_name,workflowcloneId);
							});				
			$scope.closeDialogBox = function () {
			    $("#confirmBox").hide();
			    $("#ConfirmButton").removeClass();
			    $("#ConfirmButton").addClass("text-uppercase confirmBtn");
			};
			
			/*$("body").on(
					"click",
					".deleteWorkflow",
					function() {
						var work_flow_id = $(this).parent().parent().attr("id").split(
								"workflowtr_")[1];
						var _this = $(this);
						var url = WorkflowDemoserverEndPoint + "/workflow/"
								+ work_flow_id;

						$.ajax({
							type : "DELETE",
							url : url,
							cache : true,
							contentType : "application/x-www-form-urlencoded;",
							dataType : "json",
							success : function() {
								_this.closest('tr').hide();
							},
							error : function(o) {
								alert("error");
							}
						});
					});*/
			
			
			
			
			// $("#deleteAllWorkflows").on("click", function () {
			// $('#deleteAllWorkflowModal').modal('show');
			// });
			//
			// $("body").on("click", "#confirmWorkflowDelete", function () {
			//
			// var url = $rootScope.rootPath + "/workflow/deleteAll";
			// // var url = "here";
			// $.ajax({
			// type: "GET",
			// url: url,
			// cache: true,
			// contentType: "application/x-www-form-urlencoded;",
			// dataType: "json",
			// success: function () {
			// $('#deleteAllWorkflowModal').modal('hide');
			// $('#landingTableData').find("tbody").empty();
			//
			// },
			// error: function (o) {
			// $('#deleteAllWorkflowModal').modal('hide');
			// alert("error");
			// }
			// });
			// })
			// ------------------------------------------------------------------------------
			/**
			 * Click on Create new
			 */
			$("#createNewWorkflow").on("click", function() {
				$scope.Dashboardobj.workflow_name = 'demo_workflow';
				$("#workflowName").val('demo_workflow')
				$scope.$apply(function(){
					$scope.Dashboardobj.cloneNew = false;
					$scope.Dashboardobj.createNew = true;
				});
				$('#createNewWorkflowModal').modal('show');
			});
			// ------------------------------------------------------------------------------
			/**
			 * Click on Create
			 */
			$("body").on(
					"click",
					"#workflowcreatenew",
					function() {						
						var inputval = $("#workflowName").val();
//						var windowHref = window.location.href.split('/workflow');
//
//						window.location.href = (windowHref[0]
//								+ "editWorkflow?name=" + inputval);
						$(".spinner_async_index").css("display", "block");
						$state.go('editWorkflow', {name: inputval});
						$('#createNewWorkflowModal').modal('toggle');
					});
			// ------------------------------------------------------------------------------
			/**
			 * Function to get timeformated
			 */
			function getTimeFormated(o) {
				var time;
				if (o < 1000) {
					time = o + " ms";
				} else if (o < 60000) {
					time = ((o / 1000).toFixed(2)) + " sec";
				} else if (o < 3600000) {
					time = ((o / 60000).toFixed(2)) + " mins";
				} else if (o < (3600000 * 24)) {
					time = ((o / 3600000).toFixed(2)) + " hrs";
				} else {
					time = ((o / (3600000 * 24)).toFixed(2)) + " days";
				}
				return time;
			}
			function cloneRow(clone_name, workflowid) {
				$scope.$apply(function () {
					$scope.Dashboardobj.cloneNew = true;
					$scope.Dashboardobj.createNew = false;
				});
				$("#workflowName").val(clone_name);
				$('#createNewWorkflowModal').modal('show');
			};
			$scope.CloneData = function () {
				$(".spinner_async_index").css("display", "block");
				$('#createNewWorkflowModal').modal('toggle');
				var url = EHUB_API + "workflow/es/workflow/" + workflowcloneId + "?token=" + $rootScope.ehubObject.token;
				$.getJSON(url, function (Data) {
					stageChangeRequest(Data);
					$scope.Dashboardobj.createNew = true;
					$scope.Dashboardobj.cloneNew = false;
					//$(".spinner_async_index").css("display", "none");
				});
			};
			function stageChangeRequest(Data) {
				delete Data.workflow_id;
				Data.name = $scope.Dashboardobj.workflow_name;
				Data.updated_at = new Date();
				Data.created_at = new Date();
				WorkflowApiService.getstageRequest($rootScope.ehubObject.token, Data, "POST").then(function (response) {
					location.reload();
					$(".spinner_async_index").css("display", "none");
				});
			};
};