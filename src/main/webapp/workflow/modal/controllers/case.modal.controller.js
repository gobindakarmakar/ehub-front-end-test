'use strict';
angular.module('ehubWorkflowApp')
	   .controller('CaseModalController', caseModalController);

		caseModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance',
			'WorkflowApiService',
			'workflowId',
			'Flash',
			'HostPathService'
		];
		
		function caseModalController(
			$scope,
			$rootScope,
			$uibModalInstance,
			WorkflowApiService,
			workflowId,
			Flash,
			HostPathService) { 
			
			$scope.data = {};
			$scope.caseLoader = false;
			$scope.isCaseExisted = false;
			var worflowCaseData;
			
			/*
			 * @purpose: get Workflow Case
			 * @created: 3rd oct 2018
			 * @author: swathi
			*/
			function getWorkflowCase(){
				$scope.caseLoader = true;
				WorkflowApiService.getWorkflowCase(workflowId).then(function(response){
					$scope.caseLoader = false;
					if(response && response.data && (response.data.data || response.data.status)){
						$scope.isCaseExisted = true;
						worflowCaseData = response.data.data;
						$scope.data.title = worflowCaseData.name;
			 			$scope.data.link = worflowCaseData.type;
			 			$scope.data.subType = worflowCaseData["sub-type"];
					}else{
						$scope.isCaseExisted = false;
					}
				}, function(e){
					$scope.caseLoader = false;
					HostPathService.FlashErrorMessage(e.responseMessage, "");
				});
			}
			getWorkflowCase();
			/*
		 	*@purpose: close modal
		 	*@created: 27th sep 2018
		 	*@author: swathi
		 	*/
		 	$scope.closeModal = function(){
		 		$uibModalInstance.close();
		 	};
		 	
		 	/*
		 	*@purpose: save workflow case
		 	*@created: 27th sep 2018
		 	*@author: swathi
		 	*/
		 	$scope.save = function(){
		 		$scope.caseLoader = true;
		 		$scope.data = {
		 			"name":	$scope.data.title,
		 			"type": $scope.data.link,
		 			"sub-type": $scope.data.subType,
		 			"token": $rootScope.ehubObject.token
		 		};
		 		if($scope.isCaseExisted){
		 			WorkflowApiService.updateWorkflowCase($scope.data, workflowId).then(function(response){
		 				HostPathService.FlashSuccessMessage(response.data.message, "");
			 			$scope.caseLoader = false;
			 			$uibModalInstance.close();
			 		}, function(e){
			 			$scope.caseLoader = false;
			 			HostPathService.FlashErrorMessage(e.responseMessage, "");
			 		});
		 		}else{
			 		WorkflowApiService.saveWorkflowCase($scope.data, workflowId).then(function(response){
			 			if(response && response.data && response.data.data){
			 				HostPathService.FlashSuccessMessage(response.data.message, "");
			 			}else{
			 				HostPathService.FlashErrorMessage(response.data.message, "");
			 			}
			 			$scope.caseLoader = false;
			 			$uibModalInstance.close();
			 		}, function(e){
			 			$scope.caseLoader = false;
			 			HostPathService.FlashErrorMessage(e.responseMessage, "");
			 		});
		 		}
		 	};
		}
		