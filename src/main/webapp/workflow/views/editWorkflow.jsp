<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
        
            <script>
                $(document).ready(function () {
                    /*Custom Scroll Bar*/
                    $('.widget-content').mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });

                    $('#horizontal-scroll-wrapper').mCustomScrollbar({
                        axis: "x",
                        theme: "minimal-dark"
                    });

                    $('#vertical-scroll-wrapper').mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                });
            </script>
            <style>
         
            
            .minimizeScreen{
            }
          
            </style>
            <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
                <!-- <div ng-include="callSubMenuPath()" ng-controller="subMenuController"></div> -->
                <div id="workflow-dashboard" class="" style="padding: 0px;">

                    <div class="spinner-wrapper" style="display:none">
                        <div class="spinner">
                            <div class="double-bounce1"></div>
                            <div class="double-bounce2"></div>
                        </div>
                    </div>
                    <div class="spinner_async">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                    <!-- Your content -->
                    <div class="workflow-dash main-wrapper" class="col-md-12">
                        <!--  Content Wrapper Starts  -->
                        <div class="workflow-dash content-wrapper">

                            <!-- Zoom-in-out Wrapper Starts  -->
                            <div class="zoom-in-out-wrapper">
                                <div class="zoom-content">
                                    <a id="zoom-in" href="javascript:void(0);">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div id="slider-zoom" style="height: 5px; width: 152px; margin: 6px auto; background-color: #272D32; border: none;"></div>
                                    <!--<span class="zoom-path"><a draggable="true" ondrag="onMouseWheel(event)" id="slider-zoom" href="javascript:void(0);"><i class="fa fa-minus"></i></a></span>-->
                                    <a id="zoom-out" href="javascript:void(0);">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- Zoom-in-out Wrapper Ends  -->

                            <!-- Leftside Icons Wrapper Starts  -->
                            <div class="workflow-dash toolbar-wrapper">
                                <div class="workflow-dash toolbar-icons">
                                    <ul class="workflow-dash list-unstyled toolbar-icons-list">
                                        <li>
                                            <div class="datasource">
                                                <a class="workflow-dash " href="javascript:void(0);">
                                                    <i class=" workflow-dash fa fa-road"></i>
                                                </a>
                                                <div class="source-list-wrapper sources" id="customScroll" style="height: 410px;">
                                                    <h5 class="workflow-dash text-uppercase">Sources</h5>
                                                    <ul id="sourceStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource1">
                                                <a class="workflow-dash " href="javascript:void(0);">
                                                    <i class="workflow-dash fa fa-crop"></i>
                                                </a>
                                                <div class="source-list-wrapper operations" id="customScroll1" style="height: 410px;">
                                                    <h5 class="workflow-dash text-uppercase">Operations</h5>
                                                    <ul id="operationsStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource2">
                                                <a class="workflow-dash " href="javascript:void(0);">
                                                    <i class="workflow-dash fa fa-th"></i>
                                                </a>
                                                <div class="source-list-wrapper analytic" id="customScroll2" style="height: 410px;">
                                                    <h5 class="workflow-dash text-uppercase">ANALYTIC</h5>
                                                    <ul id="analyticStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource3">
                                                <a class="workflow-dash " href="javascript:void(0);">
                                                    <i class="workflow-dash fa fa-futbol-o"></i>
                                                </a>
                                                <div class="source-list-wrapper gis" id="customScroll9" style="height: 410px;">
                                                    <h5 class="workflow-dash text-uppercase">GIS Sensitivity</h5>
                                                    <ul id="gisStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource4">
                                                <a class="workflow-dash " href="javascript:void(0);">
                                                    <i class="workflow-dash fa fa-cog"></i>
                                                </a>
                                                <div class="source-list-wrapper iot">
                                                    <h5 class="workflow-dash text-uppercase">IOT</h5>
                                                    <ul id="IOTstencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource5">
                                                <a class="workflow-dash" href="javascript:void(0);">
                                                    <i>
                                                        <img src="./assets/images/sinks/sink.png" alt="sinks-img" />
                                                    </i>
                                                </a>
                                                <div class="source-list-wrapper sinks" id="customScroll3" style="height: 410px;">
                                                    <h5 class="workflow-dash text-uppercase">SINKS</h5>
                                                    <ul id="sinkStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource6">
                                                <a class="workflow-dash" href="javascript:void(0);">
                                                    <i>
                                                        <img src="./assets/images/domain/domian.png" alt="domain-img" style="width: 25px;" />
                                                    </i>
                                                </a>
                                                <div class="source-list-wrapper domain">
                                                    <h5 class="workflow-dash text-uppercase">DOMAIN</h5>
                                                    <ul id="domainStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource7">
                                                <a class="workflow-dash" href="javascript:void(0);">
                                                    <i>
                                                        <img src="./assets/images/industries/industries.png" alt="industries-img" />

                                                    </i>
                                                </a>
                                                <div class="workflow-dash source-list-wrapper industries">
                                                    <h5 class="workflow-dash text-uppercase">INDUSTRIES</h5>
                                                    <ul id="industriesStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="datasource8">
                                                <a class="workflow-dash" href="javascript:void(0);">
                                                    <i class="fa fa-bar-chart">
                                                        <!--                                            <img
                                                             src="images/industries/industries.png"
                                                            alt="industries-img" />-->

                                                    </i>
                                                </a>
                                                <div class="workflow-dash source-list-wrapper graphs">
                                                    <h5 class="workflow-dash text-uppercase">GRAPHS</h5>
                                                    <ul id="GraphStencil">
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Leftside Icons Wrapper Ends  -->

                            <!-- RightSide Icons Wrapper Starts  -->
                            <div class="rightsidetoolbar-wrapper">
                                <div class="toolbar-icons">
                                    <ul class="list-unstyled toolbar-icons-list">
                                        <li title= "Exit Workflow">
                                            <div class="">
                                                <a href="javascript:void(0);" class="choose-schema" id="workflow_list_icon">
                                                    <i class="fa fa-sign-out"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Run Workflow">
                                            <div class="">
                                                <a class="scheduler-wrapper" href="javascript:void(0);">
                                                     <i class="fa fa-play"></i> 
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Monitoring Workflow">
                                            <div class="show-hide-monitor">
                                                <a class="monitoring-wrapper" href="javascript:void(0);">
                                                    <i class="fa fa-desktop"></i>
                                                </a>
                                                <a class="monitoring-wrapper-extra" style="display: none;" href="javascript:void(0);">
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Profile Dashboard">
                                            <div class="">
                                                <a class="profiles-wrapper" href="javascript:void(0);">
                                                    <i class="fa fa-database "></i>
                                                </a>
                                                <a class="modalcanvas-wrapper" style="display: none" href="javascript:void(0);">
                                                    <i class="fa fa-pencil-square"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Tag Dashboard">
                                            <div class="">
                                                <a class="tags-wrapper" href="javascript:void(0);" ng-click="showTagsdashboard()">
                                                    <i class="fa fa-tag"></i>
                                                </a>
                                                <a class="modalcanvas-wrapper_tag" style='display: none' href='javascript:void(0);'>
                                                    <i class="fa fa-pencil-square"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Staging Workflow">
                                            <div class="">
                                                <a class="models-wrapper" href="javascript:void(0);">
                                                    <i class="fa fa-briefcase"></i>
                                                </a>
                                                <a class="modalcanvas-wrapper_models" style="display: none" href="javascript:void(0);">
                                                    <i class="fa fa-pencil-square"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title= "Risk Based Filter">
                                            <div class="">
                                                <a class="graph-wrapper" href="javascript:void(0);">
                                                    <i class="fa fa-bar-chart"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li title="Case">
                                            <div class="">
                                                <a href="javascript:void(0);" ng-click="openCaseModal()">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- RightSide Icons Wrapper Ends  -->

                        </div>
                        <!--  Content Wrapper Ends  -->
                        <!-- 		<div id="modelCanvas" class="col-md-12" -->
                        <!-- 			style="overflow: auto; display: block; position: relative; background-color: #354851; left: 0; top: 0;"> -->
                        <!-- 		</div> -->
                        <div id="modelCanvas" style="height: calc(100vh - 117px); width: 100%; display: block; position: absolute; background-color: #283c45; left: 0; top: 0; overflow: hidden;"></div>
                        <div id="profilesdiv" class="profiles-wrapper-content">
                            <div class="profiles-content">
                                <div class="add-profile-btn">
                                    <button class="text-uppercase btn btn-info" type="button" id="addNewProfile">
                                        <i class="fa fa-plus"></i> &ensp;Add New Profile
                                    </button>
                                </div>
                                <!-- 					<div id="profilesdivTableContent" class="profile-wrapper-table table-responsive"></div> -->
                                <div id="profilesdivTableContent" class="table-responsive profile-wrapper-table" ng-controller="ProfilesLoadCtrl as PLC">
                                    <table datatable="ng" dt-options="PLC.dtOptions" dt-column-defs="PLC.dtColumnDefs" class="row-border hover table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NAME</th>
                                                <th>TYPE</th>
                                                <th>HOST</th>
                                                <th>PORT</th>
                                                <th>REST PORT</th>
                                                <th>TRANSPORT PORT</th>
                                                <th>CLUSTER NAME</th>
                                                <th>USER NAME</th>
                                                <th>ZOOKEEPER IP</th>
                                                <th>ZOOKEEPER PORT</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="person in PLC.persons">
                                                <td>{{ person.profile_id}}</td>
                                                <td>{{ person.profile_data.name}}</td>
                                                <td>{{ person.profile_data.type}}</td>
                                                <td>{{ person.profile_data.credentials.host}}</td>
                                                <td>{{ person.profile_data.credentials.port}}</td>
                                                <td>{{ person.profile_data.credentials.rest_port}}</td>
                                                <td>{{ person.profile_data.credentials.transport_port}}</td>
                                                <td>{{ person.profile_data.credentials.cluster_name}}</td>
                                                <td>{{ person.profile_data.credentials.username}}</td>
                                                <td>{{ person.profile_data.credentials.zookeeper_ip}}</td>
                                                <td>{{ person.profile_data.credentials.zookeeper_port}}</td>
                                                <td>
                                                    <a class='editProfile' ng-click='profileEdit(person.profile_id)' href='javascript:void(0)'>
                                                        <i class='fa fa-pencil-square-o'></i>
                                                    </a>&ensp;
                                                    <a class='deleteProfile' ng-click='profileDelete(person.profile_id)' href='javascript:void(0)'>
                                                        <i class='fa fa-trash-o' style='color: red'></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tagsdiv" class="tags-wrapper-content" style="position: absolute; top: 0; left: 50%; transform: translateX(-50%);">
                            <div class="tags-content">
                                <div class="add-tag-btn">
                                    <button class="text-uppercase btn btn-info" type="button" id="addnewTag">
                                        <i class="fa fa-plus"></i> &ensp;Add New Tag
                                    </button>
                                </div>
                                <div ng-controller="TagsLoadCtrl as TLC" class="table-responsive profile-wrapper-table">
                                    <table datatable="ng" dt-options="TLC.dtOptions" dt-column-defs="TLC.dtColumnDefs" class="row-border hover  table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="loadTags in TLC.loadTags">
                                                <td>{{ loadTags.tag_id}}</td>
                                                <td>{{ loadTags.tag_data.name}}</td>
                                                <td>
                                                    <a class='' ng-click='edittagsvalue(loadTags.tag_id)' href='javascript:void(0)'>
                                                        <i class='fa fa-pencil-square-o'></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class='' ng-click='deletetagsvalue(loadTags.tag_id)' href='javascript:void(0)'>
                                                        <i class='fa fa-trash-o' style='color: red'></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="modelsdiv" class="models-wrapper-content" style="position: absolute; top: 35px; left: 50%; transform: translateX(-50%);">
                            <div class="multiselect-models custom-multiselect-wrapper">
                                <div class="custom-optiongroup clearfix">
                                    <button class="btn btn-primary custom-dropdown-toggle" type="button">
                                        <span class=not-selected>0</span>Selected
                                        <b class=caret> </b>
                                    </button>
                                    <div class=custom-dropdown-menu>
                                        <ul class="custom-multiselect-container list-unstyled" id="modelsUL"></ul>
                                    </div>
                                </div>
                                <div class="Current_Workflow" style="margin-right: 5px;">
                                    <a href="javascript:void(0)" id="currWorkflow">
                                        <button ng-click="CheckCurrWorkflow()">Current Workflow</button>
                                    </a>
                                    <a href="javascript:void(0)" id="AllWorkflow" style="display: none">
                                        <button ng-click="CheckAllWorkflow()">All Workflow</button>
                                    </a>
                                </div>
                            </div>
                            <uib-tabset active="active">
                                <uib-tab index="0" heading="Staging" active="true">
                                    <div id="staging" ng-controller="StagingTableCtrl as STC" class="table-responsive model-wrapper">
                                        <table datatable="ng" dt-options="STC.dtOptions" dt-column-defs="STC.dtColumnDefs" class="row-border hover">
                                            <thead>
                                                <tr>
                                                    <th>Workflow ID</th>
                                                    <th>Model ID</th>
                                                    <th>Model Name</th>
                                                    <th>Algorithm</th>
                                                    <th>Algorithm SubType</th>
                                                    <th>Timestamp</th>
                                                    <th>Preview</th>
                                                    <th>Publish</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="loadData in STC.loadData">
                                                    <td>{{ loadData.workflow_id}}</td>
                                                    <td>{{ loadData.model_id}}</td>
                                                    <td>{{ loadData.model_name}}</td>
                                                    <td>{{ loadData.algorithm}}</td>
                                                    <td>{{ loadData.algorithm_sub_type}}</td>
                                                    <td>{{ loadData.timestamp}}</td>
                                                    <td>
                                                        <a class='' ng-click="previewID(loadData.model_id, 'staging')" href='javascript:void(0)'>
                                                            <i class='fa fa-external-link'></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a class='' ng-click="publishID(loadData.model_id, 'staging')" href='javascript:void(0)'>
                                                            <i class='fa fa-pencil-square-o'></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a class='' ng-click="deleteEntry(loadData.model_id, 'staging')" href='javascript:void(0)'>
                                                            <i class='fa fa-trash-o' style='color: red'></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </uib-tab>
                                <uib-tab index="1" heading="Published">
                                    <div id="published" ng-controller="PublishedContents as PCS" class="table-responsive model-wrapper">
                                        <table datatable="ng" dt-options="PCS.dtOptions" dt-column-defs="PCS.dtColumnDefs" class="row-border hover">
                                            <thead>
                                                <tr>
                                                    <th>Workflow ID</th>
                                                    <th>Model ID</th>
                                                    <th>Model Name</th>
                                                    <th>Algorithm</th>
                                                    <th>Algorithm SubType</th>
                                                    <th>Timestamp</th>
                                                    <th>Preview</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="loadPub in PCS.loadPub">
                                                    <td>{{ loadPub.workflow_id}}</td>
                                                    <td>{{ loadPub.model_id}}</td>
                                                    <td>{{ loadPub.model_name}}</td>
                                                    <td>{{ loadPub.algorithm}}</td>
                                                    <td>{{ loadPub.algorithm_sub_type}}</td>
                                                    <td>{{ loadPub.timestamp}}</td>
                                                    <td>
                                                        <a class='' ng-click="previewID(loadPub.model_id, 'published')" href='javascript:void(0)'>
                                                            <i class='fa fa-external-link'></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a class='' ng-click="deleteEntry(loadPub.model_id, 'published')" href='javascript:void(0)'>
                                                            <i class='fa fa-trash-o' style='color: red'></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </uib-tab>
                            </uib-tabset>
                        </div>
                    </div>
                </div>

                <script>
                    $('#modelCanvas').width(
                        $('#workflow-dashboard.col-md-12').width());
                    $('#modelCanvas').height(
                        $('#workflow-dashboard.col-md-12').height());
                </script>
                <!--  Properties Modal Starts  -->
                <div id="hdfsProperties" class="workflow-dash modal custom-workflow-properties-wrapper custom-modal">
                    <div class="workflow-dash properties-wrapper">
                        <div class="workflow-dash properties-content modal-content">
                            <div class="workflow-dash properties-header">
                                <button type="button" ng-click="CloseWorkflowDiv()" class="workflow-dash close-workflow setCancleSave">
                                    <span>&#x2716;</span>
                                </button>
                                <button type="button" class="refresh-class pull-right" id="refreshIcon" style="color: #b3d0e6;display:none">
                                    <i class="fa fa-refresh nocolor" id="reload_notebook" ng-click="editWorkflowobj.reloadParentStage()"></i>
                                </button>
                                <div class="workflow-dash properties-title">
                                    <h5 class="workflow-dash text-uppercase" id="modalHeader">HDFS SOURCE</h5>
                                </div>
                            </div>
                            <div class="workflow-dash properties-body modal-body" id="appendingModal"></div>
                        </div>
                    </div>
                </div>
                <!--  Properties Modal Ends  -->


                <!-- scheduler properties Wrapper Starts  -->
                <div id="schedulerProperties" class="workflow-dash custom-scheduling-properties-wrapper custom-modal">
                    <div class="properties-wrapper">
                        <div class="properties-content modal-content" >
                            <div class="properties-header">
                                <button type="button" class="schedule-close">
                                    <span>&#x2716;</span>
                                </button>
                                <div class="properties-title">
                                    <h5 class="text-uppercase">Scheduling Workflow</h5>
                                </div>
                            </div>
                            <div class="properties-body modal-body">
                                <form id="schedulingForm" class="form">
                                    <div class="form-group">
                                        <span class="input-group-addon" style="width: 15%; position: absolute; right: 16px; top: 30px; background-color: #121D21; border: none;">GB</span>
                                        <label class="control-label text-uppercase">Memory</label>
                                        <input type="text" class="form-control text-uppercase OnlyNum" value="4" id="memory">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-uppercase">Core</label>
                                        <input type="text" class="form-control  text-uppercase OnlyNum" name="WFcores" ng-model="WFcores" value="4" id="cores">
                                    </div>
                                    <div class="form-group">
                                        <label id="Scheduling_message" class="control-label text-uppercase"></label>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-6 pad-lt-0">
                                            <button class="text-uppercase" id="scheduleRun" type="button">Run</button>
                                            <button class="text-uppercase" style="display: none" id="scheduleStop" type="button">Stop</button>
                                        </div>
                                        <div class="col-sm-6 pad-rt-0">
                                            <button class="text-uppercase showing-scheduling-content" type="button">Schedule</button>
                                        </div>
                                    </div>
                                    <div class="scheduling-content">
                                        <div class="form-group">
                                            <label class="control-label text-uppercase">Start Date</label>
                                            <div class="input-group date" data-provide="datepicker">
                                                <div class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </div>
                                                <!-- 								<input type="text" class="form-control" id="start_date"> -->
                                                <input placeholder="08/29/2017 00:00" type="text" class="form-control alphanumSpecials" value="" id="rest_example_3_start"
                                                    name="rest_example_3_start" />
                                                <div class="input-group-addon">
                                                    <span class="fa fa-angle-down"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label text-uppercase">End Date</label>
                                            <div class="input-group date" data-provide="datepicker">
                                                <div class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </div>
                                                <!-- 								<input type="text" class="form-control" id="end_date"> -->
                                                <input placeholder="12/21/2017 00:00" type="text" class="form-control alphanumSpecials" value="" id="rest_example_3_end"
                                                    name="rest_example_3_end" />
                                                <div class="input-group-addon">
                                                    <span class="fa fa-angle-down"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label text-uppercase">Recurrence Interval</label>
                                            <input type="text" class="form-control  text-uppercase" placeholder="15 Min, 1 Hr or 3 Hr" id="recurrence_interval">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label  text-uppercase" for="recurrence_unit">Recurrence Unit</label>
                                            <select class="form-control text-uppercase" id="recurrence_unit">
                                                <option>Minutes</option>
                                                <option>Hours</option>
                                                <option>Day</option>
                                                <option>Week</option>
                                                <option>Month</option>
                                                <option>Year</option>
                                            </select>
                                        </div>
                                        <div class="form-group clearfix">
                                            <button class="text-uppercase schedule-close" type="button">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- scheduler properties Wrapper Ends  -->

                <!-- monitoring properties Wrapper Starts  -->
                <div id="monitoringProperties" class="custom-monitoring-properties-wrapper custom-modal">
                    <div class="properties-wrapper">

                        <div class="properties-content modal-content" ng-class = "{'minimizeScreen':!heightMinimize,'maximizeScreen':heightMinimize}">
                            <div class="properties-header">
                                <button type="button" class="monitoring-close">
                                    <span>&#x2716;</span>
                                </button>
                                <div class="properties-title">
                                    <h5 class="text-uppercase">Monitoring Workflow</h5>
                                </div>
                            </div>

                            <div class="properties-body modal-body table-responsive" id="monetring-previous-table-modal" ng-show="editWorkflowobj.showmontioniringTab">
                                <!--  Custom Tabs Wrapper Starts  -->
                                <div class="custom-tabs-wrapper pad-0">
                                    <div class="custom-uib-tab-wrapper pad-x15 tab-wrapper clearfix">
                                        <uib-tabset active="active">
                                            <uib-tab index="0" heading="Running " id='' ng-click="onClickInfoTabs('Running')">

                                                <div class="running-tab-wrapper custom-data-table-wrapper">
                                                    <div>
                                                        <span ng-show="tabPreLoader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                        <table class="table six-col-wf table-expandable table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>START TIME</th>
                                                                    <th>END TIME</th>
                                                                    <th>EXECUTION TIME</th>
                                                                    <th>STATUS</th>
                                                                    <th>EXPLORE</th>
                                                                    <th>DELETE</th>
                                                                    <!-- 				 			<th></th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody id="runningTable" ng-class="newStagesData1.length != 0 ? 'mxh-100' :'mxh-100 p-rel mnh-100px'" class="p-rel">
                                                                <tr ng-repeat="item in newStagesData1" ng-if="newStagesData1.length > 0">
                                                                    <td>{{item.workflow_states.StartTime}}</td>
                                                                    <td>{{item.workflow_states.EndTime}}</td>
                                                                    <td>{{item.workflow_states.executionTime}}</td>
                                                                    <td>{{item.workflow_states.workflowStatus}}</td>
                                                                    <td ng-click="showWorkflowStages(item.workflow_states.stagesStateMap, 'Running',item)">
                                                                        <a class="text-danger exploreMore" href="javascript:void(0)">
                                                                            <i class="fa fa-external-link-square"></i>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="text-danger deleteWorkflowStat" href="javascript:void(0)" ng-click="deleteWorkflowStat(item,mainInfoTabType)">
                                                                            <i class="fa fa-trash-o"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr ng-if="newStagesData1.length == 0">
                                                                    <td class="no-data-wrapper text-center">No Running Workflow</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!--     <div ng-if='workflowStagesRunning.length > 0'>
                                                        <table class="table three-col table-expandable table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>Stage</th>
                                                                    <th>Status</th>
                                                                    <th>Time</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="runningStagesTable" class="mxh-100">
                                                                <tr ng-repeat="item in workflowStagesRunning" ng-click="showStageData(item, 'Running')">
                                                                    <td>{{item.stage_name}}</td>
                                                                    <td>{{item.status}}</td>
                                                                    <td>{{item.executionTime}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div> -->
                                                    <!--  <div ng-if='stageInfoRunningLen > 0'>
                                                        <table class="table six-col-wf table-expandable table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>STAGE NAME</th>
                                                                    <th>START TIME</th>
                                                                    <th>END TIME</th>
                                                                    <th>Execution Time</th>
                                                                    <th>STATUS</th>
                                                                    <th>LOG</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="mxh-100">
                                                                <tr>
                                                                    <td>{{workflowStageInfoRunning.stage_name}}</td>
                                                                    <td>{{workflowStageInfoRunning.startTime}}</td>
                                                                    <td>{{workflowStageInfoRunning.EndTime}}</td>
                                                                    <td>{{workflowStageInfoRunning.executionTime}}</td>
                                                                    <td>{{workflowStageInfoRunning.status}}</td>
                                                                    <td>{{workflowStageInfoRunning.log}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div> -->
                                                </div>
                                            </uib-tab>
                                            <uib-tab index="1" heading="Previous" id='' ng-click="onClickInfoTabs('Previous')">
                                                <div class="sucess-tab-wrapper custom-data-table-wrapper">
                                                    <div>
                                                        <span ng-show="tabPreLoader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                        <table class="table table-expandable six-col-wf table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>START TIME</th>
                                                                    <th>END TIME</th>
                                                                    <th>EXECUTION TIME</th>
                                                                    <th>STATUS</th>
                                                                    <th>EXPLORE</th>
                                                                    <th>DELETE</th>
                                                                    <!-- 				 			<th></th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody id="previousTable" ng-class="newStagesData1.length != 0 ? 'mxh-100' :'mxh-100 p-rel mnh-100px'" class="p-rel">
                                                                <tr ng-repeat="item in newStagesData1" ng-if="newStagesData1.length > 0">
                                                                    <td>{{item.workflow_states.StartTime}}</td>
                                                                    <td>{{item.workflow_states.EndTime}}</td>
                                                                    <td>{{item.workflow_states.executionTime}}</td>
                                                                    <td>{{item.workflow_states.workflowStatus}}</td>
                                                                    <td ng-click="showWorkflowStages(item.workflow_states.stagesStateMap, 'Previous',item)">
                                                                        <a class="text-danger exploreMore" href="javascript:void(0)">
                                                                            <i class="fa fa-external-link-square"></i>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="text-danger deleteWorkflowStat" href="javascript:void(0)" ng-click="deleteWorkflowStat(item,mainInfoTabType)">
                                                                            <i class="fa fa-trash-o"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr ng-if="newStagesData1.length == 0">
                                                                    <td class="no-data-wrapper text-center ">No Previous Workflow</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!--  <div ng-if='workflowStagesPrevious.length > 0'>
                                                        <table class="table three-col table-expandable table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>Stage</th>
                                                                    <th>Status</th>
                                                                    <th>Time</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="previousStagesTable" class="mxh-100">
                                                                <tr ng-repeat="item in workflowStagesPrevious" ng-click="showStageData(item, 'Previous')">
                                                                    <td>{{item.stage_name}}</td>
                                                                    <td>{{item.status}}</td>
                                                                    <td>{{item.executionTime}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div> -->
                                                    <!--    <div ng-if='stageInfoPreviousLen > 0'>
                                                        <table class="table six-col-wf table-expandable table-scroll">
                                                            <thead>
                                                                <tr>
                                                                    <th>STAGE NAME</th>
                                                                    <th>START TIME</th>
                                                                    <th>END TIME</th>
                                                                    <th>Execution Time</th>
                                                                    <th>STATUS</th>
                                                                    <th>LOG</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="mxh-100">
                                                                <tr>
                                                                    <td>{{workflowStageInfoPrevious.stage_name}}</td>
                                                                    <td>{{workflowStageInfoPrevious.startTime}}</td>
                                                                    <td>{{workflowStageInfoPrevious.EndTime}}</td>
                                                                    <td>{{workflowStageInfoPrevious.executionTime}}</td>
                                                                    <td>{{workflowStageInfoPrevious.status}}</td>
                                                                    <td>{{workflowStageInfoPrevious.log}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div> -->
                                                </div>
                                            </uib-tab>
                                        </uib-tabset>

                                    </div>
                                </div>
                            </div>
                            <!-- new monitoring Data Starts -->
                            <div ng-if="!editWorkflowobj.showmontioniringTab" >
                                <div class="top-content-wrapper border-grey-thin-b  pad-y15">
                                    <button class="btn btn-blue fixed-t z-9999" ng-click="editWorkflowobj.backtoMonitoring(mainInfoTabType)">Back</button>
                                    <!--  {{editWorkflowobj.montior_stageData}} -->
                                    <ul class="custom-list item-4 pad-l15">
                                        <li class="cursor-default-class">
                                            <h3 class="text-grey-slate">Start Time</h3>
                                            <p>{{editWorkflowobj.montior_stageData.workflow_states.startTime}}</p>
                                        </li>
                                        <li class="cursor-default-class">
                                            <h3 class="text-grey-slate">End Time</h3>
                                            <p>{{editWorkflowobj.montior_stageData.workflow_states.endTime}}</p>
                                        </li>
                                        <li class="cursor-default-class">
                                            <h3 class="text-grey-slate">Execution Time</h3>
                                            <p>{{editWorkflowobj.montior_stageData.workflow_states.executionTime}}</p>
                                        </li>
                                        <li class="cursor-default-class">
                                            <h3 class="text-grey-slate">Status</h3>
                                            <p>{{editWorkflowobj.montior_stageData.workflow_states.workflowStatus}}</p>
                                        </li>
                                    </ul>
                                </div>
                                <div ng-if="!editWorkflowobj.showmontioniringTab">

                                    <div class="middle-content-wrapper border-grey-thin-b pad-y15" ng-class="{'d-block':editWorkflowobj.sampledata , 'd-none ':!editWorkflowobj.sampledata}">
                                        <ul class="custom-list item-4 pad-l15">
                                            <li class="cursor-default-class">
                                                <h3 class="text-grey-slate">Stage Name</h3>
                                            </li>
                                            <li class="cursor-default-class">
                                                <h3 class="text-grey-slate">Execution Time</h3>
                                            </li>
                                            <li class="cursor-default-class">
                                                <h3 class="text-grey-slate">Status</h3>
                                            </li>
                                            <li class="cursor-default-class">
                                                <h3 class="text-grey-slate">Details</h3>
                                            </li>
                                        </ul>
                                        <ul class="custom-list item-4 pad-l15" ng-repeat="stages in editWorkflowobj.montior_stageData.wfcurrentStageinfo" ng-class="{'active' :(editWorkflowobj.selected_index === $index)}">
                                            <li class="cursor-default-class">
                                                <p class="">{{stages.stage_name}}</p>
                                            </li>
                                            <li class="cursor-default-class">
                                                <p class="">{{stages.executionTime}}</p>
                                            </li>
                                            <li class="cursor-default-class">
                                                <p class="">{{stages.status}}</p>
                                            </li>
                                            <li>
                                                <a class="text-danger margin-left-class" href="javascript:void(0)" ng-click="showStageData(stages, mainInfoTabType ,$index); editWorkflowobj.displayData('false')">
                                                    <i class="fa fa-info-circle"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div ng-if="editWorkflowobj.stageDetails">
                                    <div class="custom-uib-tab-wrapper pad-x15 pad-t10 tab-wrapper clearfix">
                                        <span ng-show="editWorkflowobj.workflowStatgeInfoSpinner" class="custom-spinner case-dairy-spinner">
                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                        </span>
                                         <button type="button" class="workflow-dash maximize" ng-click="editWorkflowobj.displayDataInFullScreen(heightMinimize);" style="right: 70px;">
		                                    <i class="fa lh-30"  ng-class = "{'fa-window-maximize':!heightMinimize,'fa-window-minimize':heightMinimize}" aria-hidden="true"></i>
		                                </button>
                                        <button class="btn btn-blue pad-y5 pad-x10 text-uppercase pull-right z-9999" ng-click="editWorkflowobj.displayData('true');">Back</button>
                                        <uib-tabset active="active">
                                            <uib-tab index="0" heading="Stats " id=''>
                                                    <div class="running-tab-wrapper  custom-scroll-wrapper table-responsive" ng-class ="{'mxh-130':!heightMinimize , 'mxh-450':heightMinimize};">

                                                    <div class="" ng-repeat="(key,value) in editWorkflowobj.monitoringSampleData.stage_stats.stats">

                                                        <h3 class="mar-t15 width-40 d-ib text-grey-slate-dark">{{key}}</h3>

                                                        <p class="text-dark-grey d-ib"> {{value}}</p>
                                                    </div>
                                                </div>
                                            </uib-tab>
                                            <uib-tab index="1" heading="SampleData" id=''>
                                                <div class="tab-wrapper verticle-tabs-wrapper">
                                                    <div class="running-tab-wrapper  custom-scroll-wrapper table-responsive" ng-class ="{'mxh-130':!heightMinimize , 'mxh-450':heightMinimize};">
                                                        <table class="table  table-expandable" id="">
                                                            <thead>
                                                                <tr>
                                                                    <th ng-repeat=" heading in  editWorkflowobj.monitoringSampleData.stage_stats.data.header" class="text-grey-slate f-14">{{heading}}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="">
                                                                <tr ng-repeat=" heading in  editWorkflowobj.monitoringSampleData.stage_stats.data.structuredSampleData track by $index">
                                                                    <td ng-repeat=" headingrow in heading.row track by $index" class="text-dark-grey"> {{headingrow}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </uib-tab>
                                            <uib-tab index="2" heading="Log" id=''>

                                                    <div class="running-tab-wrapper  custom-scroll-wrapper table-responsive" ng-class ="{'mxh-130':!heightMinimize , 'mxh-450':heightMinimize};">
                                                            <p>{{editWorkflowobj.workflowstageCurrentInfo.log}}</p>
                                                </div>
                                            </uib-tab>
                                        </uib-tabset>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- monitoring properties Wrapper Ends  -->



                <!-- Server Data Modal Starts -->
                <div id="serverDataFiles" class="modal custom-severdata-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <form id="severDataForm">
                                <div class="modal-header clearfix">
                                    <div class="data-folder-input">
                                        <a class="folder-back" href="javascript:void(0);">
                                            <i class="fa fa-angle-left"></i>
                                        </a>
                                        <input type="text" class="folder-selection form-control" id="folderSelection" placeholder="/input">
                                        <a class="folder-go text-uppercase" href="javascript:void(0);">
                                            <span>GO</span>
                                        </a>
                                    </div>
                                    <div class="data-file-directory">
                                        <select class="form-control" id="fileDirectories">
                                            <option>Files and Directory</option>
                                            <option>Files and Directory</option>
                                            <option>Files and Directory</option>
                                            <option>Files and Directory</option>
                                        </select>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                    <div class="add-user-submit">
                                        <button id="saveserverDataFiles" type="button" class="custom-submit">
                                            <span class="text-uppercase">Done</span>
                                        </button>
                                        <button type="button" class="cancel" id="cancelserverDataFiles" data-dismiss="modal" aria-label="Close" ng-click="cancleHDFSList()">
                                            <span class="text-uppercase">cancel</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3 pr-8">
                                            <div class="exploring-directory yx-scroll" id="directories-list-modal">
                                                <div class="directory-header">
                                                    <h5 class="text-uppercase">Explore</h5>
                                                </div>
                                                <div class="directory-body" id="tree1"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pl-8 pr-8">
                                            <div class="exploring-directory-table vertical-scroll" id="directory-table-modal">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-uppercase ">Name</th>
                                                            <th class="text-uppercase ">Kind</th>
                                                            <th class="text-uppercase ">Size</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 pl-8">
                                            <div class="exploring-directory  vertical-scroll">
                                                <div class="directory-header">
                                                    <h5 class="text-uppercase">Selected Files/Folders</h5>
                                                </div>
                                                <div class="directory-body" id="select-file-folder-modal"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Server Data Modal Ends -->

                <!-- Schema Data Modal Starts -->
                <div id="schemaDataFiles" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="add-user-submit">
                                    <button type="button" class="custom-submit">
                                        <span id="save_hdfcsource_schema" class="text-uppercase">Done</span>
                                    </button>
                                    <button type="button" class="close text-uppercase" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">cancel</span>
                                    </button>
                                </div>
                                <div class="schema-navtabs-wrapper">

                                    <!-- Nav tabs -->
                                    <!-- - Edited by virendra for no redirection on click -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="javascript:void(0)" aria-controls="sample" role="tab" data-toggle="tab">sample</a>
                                        </li>
                                        <li role="presentation" ng-class="{'disabled':true}">
                                            <a href="javascript:void(0)" aria-controls="statistics" role="tab" data-toggle="tab">statistics</a>
                                        </li>
                                        <li role="presentation" ng-class="{'disabled':true}">
                                            <a href="javascript:void(0)" aria-controls="map" role="tab" data-toggle="tab">map</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="sample">
                                            <div class="sample-schema-wrapper">
                                                <div class="row">
                                                    <div class="col-sm-4 pr-8">
                                                        <div class="exploring-directory">
                                                            <div class="directory-header" style="position: relative;">
                                                                <h5 class="text-uppercase">
                                                                    <span>
                                                                        <i class="fa fa-filter"></i>
                                                                    </span> Attributes
                                                                </h5>
                                                                <div class="setDatatype">
                                                                    <select id="set" class="class_select">
                                                                        <option>Select Type</option>
                                                                    </select>
                                                                    <a href="javascript:void(0)" id="Setdefault">
                                                                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="directory-body">
                                                                <div class="select-btn-wrapper">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <a class="select-all-btn text-uppercase" id="selectAll" href="javascript:void(0);"> Select All </a>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <a class="unselect-all-btn text-uppercase" id="unSelectAll" href="javascript:void(0);"> un-select All </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="source-api-data-left vertical-scroll">
                                                                    <ul class="list-unstyled sidebar-menu" id="directory-body">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8 pl-8">
                                                        <div class="exploring-directory-table">
                                                            <div class="directory-header">
                                                                <h5 class="text-uppercase clearfix">
                                                                    <span>
                                                                        <i class="fa fa-database"></i>
                                                                    </span> Data
                                                                    <label id="modeOfMysql" class="sampleClass text-uppercase clearfix" style="position: sticky;margin-top: -2%;left: 75%; display: none;">Selected Mode : Query</label>
                                                                </h5>
                                                            </div>
                                                            <div class="custom-height-scroll" style="padding: 0 10px;">
                                                                <div class="yx-scroll" style="max-height: 389px; min-height: 389px">
                                                                    <table class="table  custom-table">
                                                                        <thead id="tableMainHead">
                                                                            <tr id="tableHeader" class="directory-header-MainHead">

                                                                            </tr>
                                                                            <tr id="tableSubheader" class="directory-header-SubHead">

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="tableBody" class="directory-header-tableBody">

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="statistics">Content yet to come</div>
                                        <div role="tabpanel" class="tab-pane" id="map">Content yet to come</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Schema Data Modal Ends -->

                <!-- HDFS Add Profile Modal Starts -->
                <div id="HDFSaddProfile" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Add HDFS Profile</h5>
                                    </div>
                                    <form class="form">
                                        <div class="form-group custom-select-add">
                                            <label for="SourceHDFSProfileName"> Profile Name</label>
                                            <input type="text" class="form-control" id="SourceHDFSProfileName" placeholder="Profile Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="host"> HOST</label>
                                            <input type="text" class="form-control" id="SourceHDFSHostAddProfile" placeholder="LocalHost">
                                        </div>
                                        <div class="form-group">
                                            <label for="host"> PORT</label>
                                            <input type="text" class="form-control" id="SourceHDFSPortAddProfile" placeholder="PORT">
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="SourceHDFSProfileSave">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- HDFS Add Profile Modal Ends -->


                <!-- Orient DB SInk Add Profile Modal Starts -->
                <div id="OrientDBAddProfileModal" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document" style="width: 400px">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">ADD Orient DB PROFILE</h5>
                                    </div>
                                    <form class="form add-profile-wrapper">
                                        <div class="form-group custom-select-add">
                                            <label for="OrientDBSinkProfile">PROFILE NAME</label>
                                            <input class="form-control" id="OrientDBSinkProfileName">
                                        </div>
                                        <div class="form-group">
                                            <label for="OrientDBAddProfileHost"> HOST</label>
                                            <input type="text" class="form-control" id="OrientDBAddProfileHost" placeholder="LocalHost">
                                        </div>
                                        <div class="form-group">
                                            <label for="OrientDBAddProfilePort"> PORT</label>
                                            <input type="text" class="form-control" id="OrientDBAddProfilePort" placeholder="PORT">
                                        </div>
                                        <div class="form-group">
                                            <label for="OrientDBAddProfileUsername"> USERNAME</label>
                                            <input type="text" class="form-control" id="OrientDBAddProfileUsername" placeholder="USERNAME">
                                        </div>
                                        <div class="form-group">
                                            <label for="OrientDBAddProfilePassword"> PASSWORD</label>
                                            <input type="password" class="form-control" id="OrientDBAddProfilePassword" placeholder="PASSWORD">
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="orientDBProfileSave">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Orient DB Sink Add Profile Modal Ends -->


                <!-- Source API Choose EntityData Modal Starts -->
                <div id="sourceAPIchooseEntityModal" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span>&#x2716;</span>
                                </button>
                                <div class="properties-title">
                                    <h5 class="text-uppercase">Source API Entity</h5>
                                </div>
                                <div class="schema-navtabs-wrapper">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="sample">
                                            <div class="sample-schema-wrapper">
                                                <div class="row">
                                                    <div class="col-sm-6 pr-8">
                                                        <div class="exploring-directory">
                                                            <div class="directory-header">
                                                                <h5 class="text-uppercase">
                                                                    <span>
                                                                        <i class="fa fa-filter"></i>
                                                                    </span> Attributes
                                                                </h5>
                                                            </div>
                                                            <div class="directory-body">
                                                                <div class="select-btn-wrapper">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <a class="select-all-btn text-uppercase" id="SelectAllEntities" href="javascript:void(0);"> Select All </a>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <a class="unselect-all-btn text-uppercase" id="unSelectAllEntities" href="javascript:void(0);"> un-select All </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="source-api-data-left vertical-scroll">
                                                                    <ul class="list-unstyled sidebar-menu" id="chooseDirectory-body">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 pl-8">
                                                        <div class="exploring-directory-table">
                                                            <div class="directory-header">
                                                                <h5 class="text-uppercase clearfix">
                                                                    <span>
                                                                        <i class="fa fa-database"></i>
                                                                    </span> Data
                                                                </h5>
                                                            </div>
                                                            <div class="source-api-data-right vertical-scroll">
                                                                <ul class="list-unstyled " id="chooseDirectory-list">
                                                                    <!--                                                        <li><div class="text-uppercase toggle clearfix">Entity1</div></li>
                                                                <li><div class="text-uppercase toggle clearfix">Entity2</div></li>-->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Source API Choose Entity Data Modal Ends -->

                <!-- Save Workflow Modal Starts  -->
                <div id="saveNewWorkflowModal" class="custom-operations-modal modal fade custom-modal" role="dialog">
                    <div class="modal-dialog">
                        <!--  Modal content  -->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&#x2716;</span>
                                </button>
                                <h5 id="saveInfoText" class="modal-title text-uppercase"></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Save Workflow Modal Ends  -->

                <!-- add new profile Modal Starts -->
                <div id="addProfileModal" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document" style="width: 400px">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">ADD PROFILE</h5>
                                    </div>
                                    <form class="form add-profile-wrapper" id="AddProfileForm">
                                        <div class="form-group custom-select-add">
                                            <label for="profilename">PROFILE NAME</label>
                                            <input class="form-control" id="profilename" value="">
                                        </div>
                                        <div class="form-group custom-select-add" id="profileTypeDiv">
                                            <label for="profiletype">PROFILE TYPE</label>
                                            <select class="form-control text-uppercase" id="profiletype">
                                                <option>HDFS</option>
                                                <option>ELASTIC</option>
                                                <option>Orient DB</option>
                                                <option>Kafka</option>
                                                <option>AEROSPIKE</option>
                                                <option>MYSQL</option>
                                                <option>Cassandra</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="profilehostsDiv">
                                            <label for="profilehost"> HOST</label>
                                            <input type="text" class="form-control" id="profilehost" placeholder="LocalHost" value="">
                                        </div>
                                        <div class="form-group" id="profileportdiv">
                                            <label for="profileport"> PORT</label>
                                            <input type="text" class="form-control" id="profileport" placeholder="PORT" value="">
                                        </div>
                                        <div class="form-group" id="profileusernamediv" style="display: none">
                                            <label for="profileUsername"> USERNAME</label>
                                            <input type="text" class="form-control" id="profileUsername" placeholder="USERNAME" value="">
                                        </div>
                                        <div class="form-group" id="profilepassworddiv" style="display: none">
                                            <label for="profilePassword"> PASSWORD</label>
                                            <input type="password" class="form-control" id="profilePassword" placeholder="PASSWORD" value="">
                                        </div>
                                        <div class="form-group" id="profileclusternnamediv" style="display: none">
                                            <label for="profileClusterName"> CLUSTER NAME</label>
                                            <input type="text" class="form-control" id="profileClusterName" placeholder="CLUSTER NAME" value="">
                                        </div>
                                        <div class="form-group" style="display:none" id="profilerestportdiv">
                                            <label for="profilerestport"> REST PORT</label>
                                            <input type="text" class="form-control" id="profilerestport" placeholder="PORT" value="">
                                        </div>
                                        <div class="form-group" style="display:none" id="profiletransportportdiv">
                                            <label for="profiletransportport"> TRANSPORT PORT</label>
                                            <input type="text" class="form-control" id="profiletransportport" placeholder="PORT" value="">
                                        </div>
                                        <div class="form-group" style="display:none" id="profileZookeeperIpdiv">
                                            <label for="profiletransportport"> ZOOKEEPER IP</label>
                                            <input type="text" class="form-control" id="profileZookeeperIp" placeholder="IP" value="">
                                        </div>
                                        <div class="form-group" style="display:none" id="profileZookeeperportdiv">
                                            <label for="profiletransportport"> ZOOKEEPER PORT</label>
                                            <input type="text" class="form-control" id="profileZookeeperport" placeholder="PORT" value="">
                                        </div>
                                        <div class="form-group" id="profileiddiv" style="display: none">
                                            <input type="text" class="form-control" id="profileidhide">
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="profileSavebtn">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- add new profile Modal Ends -->

                <!-- ESModel Add Profile Modal Starts -->
                <div id="ESaddIndex" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Add ES Index</h5>
                                    </div>
                                    <form class="form">
                                        <div class="form-group custom-select-add">
                                            <label for="SourceESIndexName">Index Name</label>
                                            <input type="text" class="form-control" id="SourceESIndexName" placeholder="Index Name" value="test">
                                        </div>

                                        <!-- <div class="form-group custom-select-add">
                                            <label for="SourceESShardsCount">Shards Count</label>
                                            <input type="text" class="form-control" id="SourceESShardsCount" placeholder="Shards Count">
                                        </div>

                                        <div class="form-group custom-select-add">
                                            <label for="SourceESReplicaCount">Replica Count</label>
                                            <input type="text" class="form-control" id="SourceReplicaCount" placeholder="Replica Count">
                                        </div> -->
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="SourceESindexSave">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ESModel Add Profile Modal Ends -->



                <!-- ESModel Add Profile Modal Starts -->
                <div id="ESaddType" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Add ES Type</h5>
                                    </div>
                                    <form class="form">
                                        <div class="form-group custom-select-add">
                                            <label for="SourceESIndexName">Add Type</label>
                                            <input type="text" class="form-control" id="SourceESTypeName" placeholder="Type Name" value="test">
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="SourceESTypeSave">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ESModel Add Type  Ends -->

                <!-- Add new Tags Profile  starts here-->
                <div id="addtagModal" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document" style="width: 400px">
                        <div class="modal-content ">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase" id="tagTitle">ADD TAG</h5>
                                    </div>
                                    <form class="form add-tag-wrapper TagScroll" style="height: 410px; padding: 15px;">
                                        <div class="form-group custom-select-add">
                                            <label for="tagname">TAG NAME</label>
                                            <input class="form-control" id="tagname" value="">
                                        </div>
                                        <div class="form-group custom-select-add">
                                            <label for="tagdesc">TAG DESCRIPTION</label>
                                            <input class="form-control" id="tagdesc" value="">
                                        </div>
                                        <div class="form-group custom-select-add" id="tagTypeDiv">
                                            <label for="profiletype">TAG TYPE</label>
                                            <select class="form-control text-uppercase" id="tagtype">
                                                <option>Select Type</option>
                                                <option>Range</option>
                                                <option>Nominal</option>
                                                <option>Fixed</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="tagiddiv" style="display: none">
                                            <input type="text" class="form-control" id="tagidhide">
                                        </div>
                                        <div class="form-group custom-select-add">
                                            <label for="profiletype">VALUES &ensp;</label>
                                            <a id="addNewTagvalue" href="javascript:void(0)" style="display: none">
                                                <i class="fa fa-plus-circle" style="color: #4676A0"></i>
                                            </a>
                                        </div>
                                        <div class="form-group col-sm-12" id="fieldValue" style="padding: 0px;">
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="tagsAliasDiv">
                                                <label for="profilehost"> ALIAS</label>
                                                <input type="text" class="form-control" id="tagsalias" placeholder="Alias Name" value="">
                                            </div>
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="tagsSVDiv">
                                                <label for="tagstart"> START VALUE</label>
                                                <input type="text" class="form-control" id="tagstart" placeholder="Start Value" value="">
                                            </div>
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="tagsEVDiv">
                                                <label for="tagend"> END VALUE</label>
                                                <input type="text" class="form-control" id="tagend" placeholder="End Value" value="">
                                            </div>
                                        </div>
                                        <div class="form-group" id="addNewTagField" style="display: none"></div>
                                        <div class="form-group col-sm-12" id="elseValue" style="padding: 0px;">
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="elseAliasDiv">
                                                <label for="alias"> ALIAS</label>
                                                <input type="text" class="form-control" id="elseAlias" placeholder="Alias Name" value="">
                                            </div>
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="elseSVDiv">
                                                <label for="tagstart"> START VALUE</label>
                                                <input type="text" class="form-control" id="elseStart" placeholder="Start Value" value="ELSE" disabled>
                                            </div>
                                            <div class="form-group col-sm-4" style="display: none; padding: 3px;" id="elseEVDiv">
                                                <label for="tagend"> END VALUE</label>
                                                <input type="text" class="form-control" id="elseEnd" placeholder="End Value" value="ELSE" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                    <button class="text-uppercase" type="button" id="tagSavebtn">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Add new Tags PRofile ends here-->


                <!-- ODB Sink Add node Modal Starts -->
                <div id="ODBaddNode" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" data-dismiss="modal" ng-click="closeDiv()" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Add New Node</h5>
                                    </div>
                                    <form>
                                        <div class="form-group" id="NodeDiv"></div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="NodeSaveData">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ODB Sink Add Node  Ends -->

                <!-- ODB Sink Add Edge Modal Starts -->
                <div id="ODBaddEdge" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close" ng-click="closeDiv()" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Add New Edge</h5>
                                    </div>
                                    <form>
                                        <div class="form-group" id="EdgeDiv"></div>
                                        <div class="form-group clearfix">
                                            <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                                <button class="text-uppercase" type="button" id="EdgeSaveData">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ODB Sink Add Edge Ends -->

                <!-- Display  Modal Starts -->
                <div id="Displaymodel" class="modal displaymodel-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body" style="padding: 15px;" id="DispBody">
                                <div class="displaymodel-wrapper">
                                    <div class="properties-title">
                                        <button type="button" class="close closedDisp" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h4 class="text-uppercase" style="color: #869fa8; font-weight: 500">Model Details</h4>
                                    </div>
                                    <div class="model-preview-wrapper" id="dispModelDiv">
                                        <ul class="nav nav-tabs" id="ModelDiv" role="tablist">
                                            <li role="presentation" class="active">
                                                <a class="general_tab" aria-controls="information" data-taget="#general" role="tab" data-toggle="tab" aria-expanded="false">Information</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="features_tab" data-taget="#features" aria-controls="features" role="tab" onclick="return;" data-toggle="tab" aria-expanded="false">Features</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="schema_tab" data-taget="#schema" aria-controls="schema" role="tab" onclick="" data-toggle="tab" aria-expanded="false">Schema</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="params_tab" data-taget="#params" aria-controls="params" role="tab" onclick="javascript:void(0)" data-toggle="tab"
                                                    aria-expanded="false">Parameters</a>
                                            </li>
                                            <li role="presentation">
                                                <a class="metrics_tab" data-taget="#metrics" aria-controls="metrics" role="tab" onclick="javascript:void(0)" data-toggle="tab"
                                                    aria-expanded="false">Training Metrics</a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="general">
                                                <div class="model-scroll">
                                                    <div id="generalDiv"></div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="features">
                                                <div class="model-scroll">
                                                    <div id="featuresDiv"></div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="schema">
                                                <div class="model-scroll">
                                                    <div id="schemaDiv"></div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="params">
                                                <div class="model-scroll">
                                                    <div id="paramsDiv"></div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="metrics">
                                                <div class="model-scroll">
                                                    <div id="TMDiv"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="close-btn-wrapper clearfix">
                                            <button class="text-uppercase closedDisp pull-right" type="button" id="CloseDispM">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- display Model Ends -->

                <!-- Preview Logs of Failed stages Modal Starts -->
                <div id="FailedLogs" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" id="CloseBtn" class="close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Logs Details</h5>
                                    </div>
                                    <div class="form-group" id="LogsDiv"></div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-6 pull-right pad-lt-0 pad-rt-0">
                                            <button class="text-uppercase" type="button" id="CloseLog">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Preview Logs of Failed stages Modal Ends -->

                <!-- Graph Wrapper Starts  -->
                <div id="graphDashboard" class="custom-workflow-properties-wrapper custom-modal">
                    <div class="properties-wrapper">
                        <div class="properties-content modal-content">
                            <div class="properties-header">
                                <button type="button" class="pull-right graph-close">
                                    <span>&#x2716;</span>
                                </button>
                                <!--                        <div class="form-group status-by-filter">
                                    <label class="">Filter by Status: </label>
                                    <select name="workFlowStatStatus" class="form-control" id="workFlowStatStatus">
                                        <option value="All">All</option>
                                        <option value="Running">Running</option>
                                    </select>
                                </div>-->

                                <div class="properties-title">
                                    <h5 class="text-uppercase">RISK BASED FILTER</h5>
                                </div>
                            </div>
                            <div class="properties-body modal-body" id="graph-table-modal"></div>
                        </div>
                    </div>
                </div>
                <!--</div>-->
                <!-- monitoring properties Wrapper Ends  -->


                <!-- Confirmation Box Starts  -->
                <div id="confirmBox" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="confirm-close" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Workflow</h5>
                                    </div>
                                    <div class="form-group" id="publishBox">Are you sure you want to Publish the Modal?</div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-6">
                                            <button class="text-uppercase confirmBtn" id="ConfirmButton" type="button">Yes</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="text-uppercase confirmBtn confirm-closeBtn" data-dismiss="modal" type="button">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--</div>-->
                <!-- Confirmation Box Ends  -->


                <!-- Link Custom Name Box Starts  -->
                <div id="customIP" class="modal schema-modal-wrapper custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="cancleBtn" data-dismiss="modal" aria-label="Close" style="top:0px;">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase">Link Label</h5>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="customName" class="form-control" placeholder="Label Name" />
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-6">
                                            <button class="saveBtn" id="saveLabel" type="button">SAVE</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="text-uppercase saveBtn cancleBtn" data-dismiss="modal" type="button">Cancle</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--</div>-->
                <!-- Link Custom Name Box Ends  -->
                <!-- Sink Cassandara KeySpace Modal Box -->
                <div id="SCddKeyspace" class="modal custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close button-grey-workflow" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase text-grey-slate-dark">Add Sink Cassandara Keyspace</h5>
                                    </div>
                                    <form class="form">
                                        <div class="form-group custom-select-add">
                                            <label for="SourceESIndexName">KeySpace Name</label>
                                            <input type="text" class="form-control text-grey-slate-dark mar-l5" ng-model="editWorkflowobj.sinkCassandaraobj.keyspaceName"
                                                placeholder="KeySpace Name">
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="pull-right">
                                                <button class="text-uppercase btn btn-blue" type="button" ng-click="editWorkflowobj.sinkCassandaraobj.SaveCassandaraKeyspace()">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sink Cassandara Keyspace Modal Box Ends -->
                <!-- Sink Cassandara Family Schema Modal Starts -->
                <div id="SCfamilyschema" class="modal custom-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="exploring-directory">
                                    <div class="properties-title">
                                        <button type="button" class="close button-grey-workflow" data-dismiss="modal" aria-label="Close">
                                            <span>&#x2716;</span>
                                        </button>
                                        <h5 class="text-uppercase text-grey-slate-dark">Define Schema</h5>
                                    </div>
                                    <div id="newFamilySinkcassandra">
                                        <p class="d-ib width-30">Enter Family Name</p>
                                        <input type="text" class="form-control text-grey-slate-dark d-ib width-60 custom-input pad-l5 border-blue-thin-b  mar-b10" id="newCassandraFamliy"
                                            placeholder="Enter Family name" />
                                    </div>
                                    <!--  <div class="pull-right width-60" ng-click="addSinkCassandraSchema()">
                                          <span ng-if="cassandraSchema.length >0 "> Set as Primary</span>  
                                                     <a class="addKafkaSource pull-right" href="javascript:void(0);">
                                                         <i class="fa fa-plus-circle fa-2x"></i>
                                                     </a>
                                                 </div> -->
                                    <div class="custom-data-table-wrapper ">
                                        <table class="table table-scroll four-col-cassandra">
                                            <thead>
                                                <th>
                                                    Column Name
                                                </th>
                                                <th>
                                                    Data Type
                                                </th>
                                                <th>
                                                    Set As Primary
                                                </th>
                                                <th>
                                                    Remove
                                                    <a class="pull-right" href="javascript:void(0);" ng-click="addSinkCassandraSchema()">
                                                        <i class="fa fa-plus-circle f-18 text-dark-blue"></i>
                                                    </a>
                                                </th>
                                            </thead>
                                            <tbody class="cassandra-scroll mxh-300">
                                                <tr ng-repeat="cassSchema in cassandraSchema">
                                                    <td>
                                                        <input type="text" class="form-control pad-l5 border-blue-thin-b custom-input sourceKafkaInput nospaceclass" ng-model="cassSchema.schemaname"
                                                            id="input{{$index}}" placeholder="name" value="">
                                                    </td>
                                                    <td>
                                                        <select ng-model="cassSchema.dataType" class="custom-select-dropdown-wrapper">
                                                            <option ng-repeat="item in editWorkflowobj.sinkCassandaraobj.strings_array">{{item}}</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="custom-radiobutton-wrapper">
                                                            <label class="radio-inline">
                                                                <input class="sinkCassandrachkbox1" type="radio" checked="" name="checkbox-radio-mysql-sinkCassandraDB" value="{{$index}}"
                                                                    ng-model="editWorkflowobj.sinkCassandaraobj.setasPrimary">
                                                                <span>
                                                                    <i class="fa fa-circle"></i>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <!-- <a class="deleteSourceKafka pad-l15 width-100" href="javascript:void(0);">
                                                            <i class="fa fa-trash f-18 text-dark-blue"></i>
                                                        </a> -->
                                                        <a class="pad-l15 width-100" href="javascript:void(0);" ng-click="deleteSelectedSchema($index)">
                                                            <i class="fa fa-trash f-18 text-dark-blue"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                <div class="row">
                                    <button type="button" class="btn btn-blue btn-block pull-right classBtn" ng-click="SaveoperationalFormulae()">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sink Cassandara Keyspace Modal Box Ends -->


                <script>
                    var userDetails =
                        '<%=new ObjectMapper().writeValueAsString((Object) session.getAttribute("userInfo"))%>';
                    userDetails = JSON.parse(userDetails);
                    if (userDetails.token != undefined && userDetails.token != null) {} else {
                        window.location.href = '';
                    }
                </script>
                <script>
                    //            $(".exploring-directory").mCustomScrollbar({
                    //                axis: "y",
                    //                theme: "minimal-dark"
                    //            });
                    /* $(".horizontal-scroll").mCustomScrollbar({
                     axis: "x",
                     theme: "minimal-dark"
                     }); */
                    //        $("#monitoringProperties").mCustomScrollbar({
                    //            axis: "y",
                    //            theme: "minimal-dark"
                    //        });


                    $(".cassandra-scroll").mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                    //cassandra - scroll
                </script>