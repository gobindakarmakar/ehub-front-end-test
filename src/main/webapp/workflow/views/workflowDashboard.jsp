<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>  
<!--  SubMenu Ends  -->

<!--  Workflow Dashboard Wrapper Starts  -->
<div class="spinner_async_index">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
</div>

<div id="workflow-dashboard" class="workflow-dashboard-wrapper">

    <!--  Main Wrapper Starts  -->
    <div class="main-wrapper">
        <!--  Content Wrapper Starts  -->
        <div class="content-wrapper">
            <div class="business-workflow-landing-wrapper">
                <div class="workflow-buttons clearfix">
                    <a class="btn btn-primary text-uppercase pull-right" id="createNewWorkflow" href="javascript:void(0);">Create New</a>
                </div>
                <div class="workflow-landing-table">
                    <div class=" table-responsive">
                        <table id="landingTableData" class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-left">Id</th>
                                    <th class="text-left">Name</th>
                                    <th class="text-left">Created</th>
                                    <th class="text-left">Updated</th>
                                    <th class="text-center">Monitoring</th>
                                    <th class="text-center">Edit</th>
                                    <th class="text-center">Delete</th>
                                    <th class="text-center">Clone</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Main Wrapper Ends  -->

    <!--  Create New Workflow Modal Starts  -->
    <div id="createNewWorkflowModal" class="modal fade custom-modal custom-operations-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&#x2716;</span></button>
                    <h4 class="modal-title text-uppercase">Create New Workflow</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group container-fluid">
                        <div class="row">
                            <label class="control-label col-sm-4 " for="workflowName">Workflow Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="workflowName" value="demo_workflow" placeholder="Enter Workflow Name" ng-model="Dashboardobj.workflow_name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default text-uppercase  btn-update" id="workflowcreatenew" ng-if="Dashboardobj.createNew">Create</button>
                    <button type="button" class="btn btn-default text-uppercase  btn-update"  ng-if="Dashboardobj.cloneNew" ng-click="CloneData()">Clone</button>
                    <button type="button" class="btn btn-default text-uppercase btn-cancel" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--  Create New Workflow Modal Ends  -->

<!--      Delete Workflow Modal Starts  -->
<!--     <div id="deleteAllWorkflowModal" class="modal fade custom-modal custom-operations-modal" role="dialog"> -->
<!--         <div class="modal-dialog">            -->
<!--             <div class="modal-content">      -->
<!--                 <div class="modal-body"> -->
<!--                     <h3 class="text-danger text-center text-uppercase">Are you sure you want to Delete All these things from the database?</h3> -->
<!--                 </div> -->
<!--                 <div class="modal-footer"> -->
<!--                     <button type="button" class="btn btn-default text-uppercase btn-update" id="confirmWorkflowDelete">Delete</button> -->
<!--                     <button type="button" class="btn btn-default text-uppercase btn-cancel" data-dismiss="modal">Cancel</button> -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->
<!--      Delete Workflow Modal Ends  -->

    <!--  Save Workflow Modal Starts  -->
    <div id="saveNewWorkflowModal_index" class="modal fade custom-modal custom-operations-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&#x2716;</span></button>
                    <h5 id="saveInfoText_index" class="modal-title text-uppercase"></h5>
                </div>
            </div>
        </div>
    </div>
    <!--  Save Workflow Modal Ends  -->

                <!-- Confirmation Box Starts  -->
        <div id="confirmBox" class="modal schema-modal-wrapper"
             tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="exploring-directory">
                            <div class="properties-title">
                                <button type="button" class="confirm-close" data-dismiss="modal" ng-click="closeDialogBox()" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h5 class="text-uppercase">Workflow</h5>
                            </div>
                            <div class="form-group">Are you sure you want to Delete the Workflow?</div>
                            <div class="form-group clearfix">
                                <div class="col-sm-6">
                                    <button class="text-uppercase confirmBtn" id="ConfirmButton" type="button">Yes</button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="text-uppercase confirmBtn confirm-closeBtn" ng-click="closeDialogBox()" data-dismiss="modal" type="button">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirmation Box Ends  -->


</div>
<!--  Workflow Dashboard Wrapper Starts  -->
<script>
<%--     var userDetails = '<%=new ObjectMapper().writeValueAsString((Object) session.getAttribute("userInfo"))%>'; --%>
//     userDetails = JSON.parse(userDetails);
//     if (userDetails.token != undefined && userDetails.token != null) {
//     } else {
//         window.location.href = '';
//     }
</script>