'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('CorporateAlertModalController', corporateAlertModalController);

corporateAlertModalController.$inject = [
    '$scope',
    '$rootScope',
    '$uibModalInstance'

];

function corporateAlertModalController(
    $scope,
    $rootScope,
    $uibModalInstance
) {
	
	$scope.closeModal = function(){
		$uibModalInstance.dismiss('close');
	};
	console.log("corporateAlertModalController");
}