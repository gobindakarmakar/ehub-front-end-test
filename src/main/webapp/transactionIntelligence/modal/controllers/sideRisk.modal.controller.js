'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('SideRiskModalController', sideRiskModalController);

sideRiskModalController.$inject = [
    '$scope',
    '$rootScope',
    'EnrichSearchGraph',
    'EntityGraphService',
    '$uibModal',
    '$uibModalInstance',
    'TransactionAnalysisApiService',
    'data',
    'type',
    'date',
    'color',
    'filtersObj'

];

function sideRiskModalController(
    $scope,
    $rootScope,
    EnrichSearchGraph,
    EntityGraphService,
    $uibModal,
    $uibModalInstance,
    TransactionAnalysisApiService,
    data,
    type,
    date,
    color,
    filtersObj
) {

    var element = document.getElementsByTagName('body');
    /*addclass on body */
    element[0].classList.add("modal-visible");
    setTimeout(function() {
        //var e = document.getElementsByClassName('custom-modal right');
       
       /*  if (e.length != 0 && document.body.scrollTop > 60) {
            e[0].classList.add("modal-top")
        } else if (e.length > 0 && document.body.scrollTop < 60) {
            e[0].classList.remove("modal-top");
        } */
    }, 0);

    $scope.closeUploadDocument = function() {
        $uibModalInstance.dismiss('close');
        /*removeclass on body */
        element[0].classList.remove("modal-visible");
    };
    var bubbleSocialOptions = { //BubbleEntity option
        "height": 250,
        //            "data": bubbledata,
        "bubbleColor": ['#8F4582']
    };
    $scope.txtColor = color;
    $scope.sideRiskBarChartOnePreloader = false;
    $scope.sideModalPreloader = true;
    $scope.loadDummy = true;
    $scope.selectedToTopModel = [];
    $scope.selectedToTopData = [];

    $scope.selectedToTopSettings = {
        selectedToTop: true,
        checkBoxes: true,
        styleActive: true,
        displayProp: 'id',
        //scrollableHeight: '100px',
        //scrollable: false,
        enableSearch: true,
        //searchField: 'age',//By default, search is done on all items, by specifying the searchField in the settings object one can specify on which field of the objects the filtering should be done.
        //showEnableSearchButton: true ,//showEnableSearchButton to true will add the enable/disable search button under the Select all / Deselect all buttons
        keyboardControls: true,
        //selectionLimit: 2
    };

    //code for number formatting
    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };
    var filtersData = filtersObj?jQuery.extend(true, {}, filtersObj):{};
    $scope.TopData = [];
    if (type == '#riskBloomBergChartOne' || type == '#riskBarChartOne') {
    	filtersData.customerRisk = data.data.className;
    }else if (type == '#riskBloomBergChartTwo' || type == '#riskBarChartTwo') {
    	filtersData.productRiskType = data.data.className;
    }else if (type == '#riskBloomBergChartThree' || type == '#riskBarChartThree') {
    	filtersData.geoRiskType = data.data.className;
    }else if (type == '#riskBloomBergChartFour' || type == '#riskBarChartFour') {
    	filtersData.corruptionRiskType = data.data.className;
    }else if (type == '#riskBloomBergChartFive' || type == '#riskBarChartFive') {
    	filtersData.politicalRiskType = data.data.className;
    }else if (type == '#corpoStructureBloombergChartOne' || type == '#corpoStructureBarChartOne') {
    	filtersData.corporateStructureType = data.data.className;
    }else if (type == '#corpoStructureBloombergChartTwo' || type == '#corpoStructureBarChartTwo') {
    	filtersData.shareholderType = data.data.className;
    }else if (type == '#corpoStructureBarChartTree') {
    	filtersData.corporateGeographyType = data.data.className;
    }else if (type == '#counterPartiesBloombergChartOne' || type == '#counterPartiesBarChartOne') {
    	filtersData.activityCategory = data.data.className;
    } else if (type == '#counterPartiesBarChartTwo') {
    	filtersData.counterpartyCountry = data.data.className;
    }else if (type == '#counterPartiesBloombergChartTwo' || type == '#counterPartiesBarChartThree') {
    	filtersData.bankName = data.data.className;
    }if (type == '#counterPartiesBloombergChartThree') {
    	filtersData.bankLocations = data.data.className;
    }
    TransactionAnalysisApiService.getScenariosByType(date, data.data.className,filtersData).then(function(response) {
        $scope.type = response.data[1][0].type;
        $scope.Amount = '$' + ' ' + Number(response.data[1][0].alertAmount).formatAmt();
        $scope.CountTotal = response.data[1][0].alertCount;
        $scope.alertRatio = response.data[1][0].alertRatio;
        $scope.scenarios = response.data[1][0].scenario;
        angular.forEach(response.data[1][0].scenario, function(d, i) {
            $scope.selectedToTopData.push({
                'id': i,
                'label': d
            });
        });
        $scope.sideModalPreloader = false;
        plotAssociatedCharts(response.data[2]);
      
    });
    

    function handleError() {
        setTimeout(function() {
            angular.element("#sideRiskBarChartOne").html('<p class="no-data">No Data Found</p>');
        }, 0);
    }
    $scope.getScenariosGraph = function() {
        $scope.sideRiskBarChartOnePreloader = true;
        window.selectedScenario = $scope.selectedToTopModel;  
        window.selectedFilters = filtersData;
//        var selectedScenarios = [$scope.selectedToTopModel];
        $scope.loadDummy = false;
        filtersData.scenario = $scope.selectedToTopModel;
        TransactionAnalysisApiService.getAlertsByScenario(date, data.data.className, filtersData).then(function(response) {
            $scope.sideRiskBarChartOnePreloader = false;
            if (response.data.length > 0) {
                plotScenariosGraph(response.data, "#sideRiskBarChartOne","","byamount");
            } else {
                handleError();
            }
        });
       
    };

    function plotAssociatedCharts(data) {
        var bubbledata = { //BubbleEntity Data
            name: '',
            children: []
        };
        angular.forEach(data, function(d) {
            bubbledata.children.push({
                "name": d.type?d.type:d.corporateStructure,
                "size": d.alertAmount
            });
        });
        plotBubbleChart('#associatedAlertsByAmount', [color], bubbledata);
        data.map(function(d) {
            d.letter = d.type;
            d.frequency = d.alertCount;
        });
        plotScenariosGraph(data, "#associatedAlertsByNumber", true);

    }

    function plotScenariosGraph(data, id, isdataFormated,type) {
        if (!isdataFormated) {
            data.map(function(d) {
                d.letter = d.customerName;
                return (d.frequency = d.totalAmount);
            });
        }
        var options = { //Bar chart option
            container: id,
            width: $(id).width(),
            height: 200,
            data: data,
            marginTop: 1,
            marginBottom: 1,
            marginRight: 5,
            marginLeft: 5,
            tickColor: "rgb(51, 69, 81)",
            colors: color,
            type:type
        };
        $scope.showDummyChart = false;
        $scope.addChart = true;
        simpleVerticalBarChart(options);
    }

    function plotBubbleChart(id, color, data) {
        var bubbleSocialOptions1 = jQuery.extend(true, {}, bubbleSocialOptions);
        bubbleSocialOptions1.container = id;
        bubbleSocialOptions1.bubbleColor = color;
        bubbleSocialOptions1.data = data;
        BubbleHierarchyChart(bubbleSocialOptions1);
    }

    function plotBarChart(id, color, data) {// jshint ignore:line
        var politiciansOptions1 = jQuery.extend(true, {}, politiciansOptions);// jshint ignore:line
        politiciansOptions1.container = id;
        politiciansOptions1.colors = color;
        politiciansOptions1.data = data;

        simpleVerticalBarChart(politiciansOptions1);
    }

}