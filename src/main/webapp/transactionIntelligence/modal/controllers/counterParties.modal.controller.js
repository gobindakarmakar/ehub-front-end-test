'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('CounterPartiesAlertModalController', counterPartiesAlertModalController);

counterPartiesAlertModalController.$inject = [
    '$scope',
    '$rootScope',
    '$uibModalInstance'
];

function counterPartiesAlertModalController(
    $scope,
    $rootScope,
    $uibModalInstance
) {
	$scope.closeModal = function(){
		$uibModalInstance.dismiss('close');
	};
	console.log("counterPartiesAlertModalController");
}