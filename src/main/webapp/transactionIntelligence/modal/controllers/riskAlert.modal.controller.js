'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('RiskAlertModalController', riskAlertModalController);

	riskAlertModalController.$inject = [
	    '$scope',
	    '$rootScope',
	    '$uibModalInstance',
	    'TransactionAnalysisApiService',
	    'fromDt',
	    'toDt',
	    'id',
	    'colors',
	    'customerAmount',
	    'filtersObj',
	    'EntityGraphService'
	];
	
	function riskAlertModalController(
	    $scope,
	    $rootScope,
	    $uibModalInstance,
	    TransactionAnalysisApiService,
	    fromDt,
	    toDt,
	    id,
	    colors,
	    customerAmount,
	    filtersObj,
	    EntityGraphService
	) {
		$scope.currentColor =colors;
		$scope.itemsPerPage=10;
		$scope.customerAmount =customerAmount;
		$scope.selectedScenario =window.selectedScenario;
		var selectedFilters = window.selectedFilters;
		var params = {
			token: $rootScope.ehubObject.token,
			id: id
		};
		var isDetected = true;
		
		
		/*----------------------------------------- LINE CHART -------------------------------*/
		var lineData = [];
		function loadLineChart(lineData){
//			 if (!isdataFormated) {
//		            data.map(function(d) {
//		                d.letter = d.customerName;
//		                return d.frequency = d.totalAmount;
//		            });
//		        }
		        var options = { //Bar chart option
		            container: '#custCounterPartyChart',
		            width: $('#custCounterPartyChart').width(),
		            height: 200,
		            data: lineData,
		            marginTop: 1,
		            marginBottom: 1,
		            marginRight: 5,
		            marginLeft: 5,
		            tickColor: "rgb(51, 69, 81)",
		            colors: colors,
		            type:"byamount"
		        };
		       
		       
		        
//		    var options = {
//	            container: '#sideRiskBarChartOne',
//	            height: 190,
//	            axisX : true,
//	            axisY : true,
//	            gridy : true,
//	            labelRotate:0,
//	            height: 250,
//	            width: $('sideRiskBarChartOne').width(),
//	            data: lineData,
//	            labelRotate:'-65',
//	            color: colors
//	        }
	        setTimeout(function(){
	        	 simpleVerticalBarChart(options);
	        }, 0);
		}
		
		/*--------------------------------------- WORLD MAP CHART -------------------------------*/
	    function World(options) {
	        //var worldmapId = options.container;
	        //var worldmapuri1 = options.uri1;
	        d3.json(options.uri1, function(error, data) {
	            var WorldData = data;
	            options.data = [WorldData, []];
	            options.type="byamount";
	            EntityGraphService.plotWorldLocationChart(options);
	        });
	    }

	   

		var markers = [];
		var worldChartOptions = {
	        container: "#associatedEntitychart",
	        uri1: "../vendor/data/worldCountries.json",
	        uri2: "../vendor/data/worldMapData.json",
	        height: 250,
	        markers: markers
	    };
		function loadMap(markers){
            var maxValue = d3.max( markers, function(d) {
                return d.amount;
            });
            var worldOptions1 = jQuery.extend(true, {}, worldChartOptions);
            worldOptions1.markers = markers;
            worldOptions1.domain = {
                x: 1,
                y: maxValue + 2000
            };
            worldOptions1.ChangeRadius = 'true';
            worldOptions1.container = "#riskAlertModalGeography";
            World(worldOptions1);
		}
		/*----------------------------------BUBBLE CHART-------------------------------*/
		var bubbledata = { 
	        name: '',
	        children: []
	    };
		var bubbleSocialOptions = {
	        "height": 250,
	        "data": bubbledata,
	        "bubbleColor": [colors]
	    };
		function plotBubbleChart(id, color, data) {
			var bubbleSocialOptions1 = jQuery.extend(true, {}, bubbleSocialOptions);
	        bubbleSocialOptions1.container = id;
	        bubbleSocialOptions1.bubbleColor = color;
	        bubbleSocialOptions1.data = data;
	        BubbleHierarchyChart(bubbleSocialOptions1);
	    }
		var countriesBubbleData = {
	          name: '',
	          children: []
		};
		function loadBubbleChart(countriesBubbleData){
			setTimeout(function(){
				plotBubbleChart('#riskAlertModalCountries', [colors], countriesBubbleData);
			}, 0);
		}
		/*
	     * @purpose:  get customer info
	     * @created: 17 feb 2018
	     * @params: fromDt, toDt, params
	     * @return: no
	     * @author: swathi
	    */
		function getCustomerInfo(){
			TransactionAnalysisApiService.getCustomerInfo(params).then(function(response){
//				response.data.dob = response.data.customerType == 'CORP'?new Date(dateOfIncorporation):new Date(dateOfBirth);
				$scope.customerDetails = response.data;
			}, function(){
				
			});
		}
		getCustomerInfo();
		/*
	     * @purpose:  get risk Counter Parties
	     * @created: 17 feb 2018
	     * @params: fromDt, toDt, params
	     * @return: no
	     * @author: swathi
	    */
		function loadRiskCounterParties(){

	        TransactionAnalysisApiService.getRiskCounterParties(fromDt, toDt, params,selectedFilters).then(function(response) {
	        	$scope.counterpartiesData = response.data;
	        	var countriesList =[],counterPartiesList=[];
	        	angular.forEach(response.data.Country, function(d) {
					 if (d.country ) {
						 countriesList.push(d.country);
						countriesBubbleData.children.push({
		                    "name": d.country,
		                    "size": d.amount
		                });
						markers.push({	
							'lat': d.latitude, 
	                        'long': d.longitude,
		                    'mark': "assets/images/redpin.png",
		                    'name': d.country,
		                    'title': d.country,
		                    'amount': d.amount,
		                    'fillColor':colors
		                });
					 }
				});
				angular.forEach(response.data.Customer, function(d) {
					counterPartiesList.push(d.name);
					 lineData.push({
						 'letter': d.name,
				         'frequency': d.amount
					 });
				});

				$scope.counterPartiesListString = counterPartiesList.join(", ");
				$scope.countriesListString = countriesList.join(", ");

				loadLineChart(lineData);
				
				loadBubbleChart(countriesBubbleData);
				loadMap(markers);
	        }, function() {
	
	        });
		}
		loadRiskCounterParties();
        /*
	     * @purpose:  close risk alerts modal
	     * @created: 17 feb 2018
	     * @params: no
	     * @return: no
	     * @author: swathi
	    */  
		$scope.closeModal = function(){
			$uibModalInstance.dismiss('close');
		};
		
		/*
	     * @purpose:  get Counter Parties Transactions
	     * @created: 17 feb 2018
	     * @params: fromDt, toDt, params, isDetected
	     * @return: no
	     * @author: swathi
	    */
		function loadDetectedTxns(pageNum){
			var parms = jQuery.extend(true, {}, params);
			parms.pageNumber=pageNum?pageNum:1;
			parms.recordsPerPage=$scope.itemsPerPage;
			TransactionAnalysisApiService.getCounterPartiesTransactions(fromDt, toDt, parms, isDetected,selectedFilters).then(function(response){
				$scope.detectedTransactionsLength = response.data.paginationInformation.totalResults;
				$scope.detectedTransactions = response.data.result;
			}, function(){
				
			});
		}
		function loadAllTxns(pageNum){			
			//load all txn's
			var parms = jQuery.extend(true, {}, params);
			parms.pageNumber=pageNum?pageNum:1;
			parms.recordsPerPage=$scope.itemsPerPage;
			TransactionAnalysisApiService.getCounterPartiesTransactions(fromDt, toDt, parms, false,selectedFilters).then(function(response){
				$scope.allTransactions = response.data.result;
				$scope.allTransactionsLength = response.data.paginationInformation.totalResults;
			}, function(){
				
			});
		}
		loadDetectedTxns();
		loadAllTxns();
		/*
	     * @purpose: page changed for  txns table
	     * @created: 17 feb 2018
	     * @params: tab
	     * @return: no
	     * @author: swathi
	    */
		$scope.pageChanged = function(pagenum,isall){
			if(isall){
				loadAllTxns(pagenum);
			}else{
				loadDetectedTxns(pagenum);
			}
		};
		/*
	     * @purpose: tab active on click
	     * @created: 17 feb 2018
	     * @params: tab
	     * @return: no
	     * @author: swathi
	    */
		$scope.detectedTransactionTab = true;
		$scope.tabClicked = function(tab){
			if(tab == 'detectedTransaction'){
				$scope.detectedTransactionTab = true;
			}else{
				$scope.detectedTransactionTab = false;
			}
		};
	}