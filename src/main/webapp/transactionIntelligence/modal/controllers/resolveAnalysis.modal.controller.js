'use strict';
angular.module('ehubTransactionIntelligenceApp')
	   .controller('ResolveTranxModalController', resolveTranxModalController);

resolveTranxModalController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'$uibModalInstance',
	'rsolveAnlysisData',
	'EHUB_API',
	'TransactionAnalysisApiService',
	'$timeout'
];

function resolveTranxModalController(
		$scope,
		$rootScope,
		$state,
		$uibModalInstance,rsolveAnlysisData,EHUB_API,TransactionAnalysisApiService,$timeout){
	$scope.sarReported = false;
	$scope.strReported = false;
	if(rsolveAnlysisData[2]){
		$scope.strReported = true;
		$timeout(function(){ 			
 				$uibModalInstance.dismiss('close');
			},1000);
	}
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
 	/*
 	*@purpose: resolve analysis 
 	*@created: 17 jan 2018
 	*@params: none
 	*@returns: none
 	*@author: prasanthi
 	*/
 	$scope.resolveTransaction = function(sar){
 		if(sar || rsolveAnlysisData[1]){
 			$scope.sarReported = true;
 			
 			var timeout=0;
 			if(sar){
 				timeout =1000;
 			}
 			$timeout(function(){
	 			$state.go("dashboard");
	 			$uibModalInstance.dismiss('close');
 			},timeout);
 			$scope.showSpinner1 = false;
 			return;
 		}
 		$scope.showSpinner1 = true; 
 		var params ={"alertId":rsolveAnlysisData[0],"comment":$scope.resolveComment};
 		var token ={"token":$rootScope.ehubObject.token};
 		 TransactionAnalysisApiService.resolveAlert(params, token).then(function(){
 			$timeout(function(){
        		$scope.showSpinner1 = false;
        		$uibModalInstance.close("success");
        		$state.go("dashboard");
        	},0);
 		 }, function(){
 			$timeout(function(){
        		$scope.showSpinner1 = false;
        		$uibModalInstance.dismiss('error');
        		
        	},0);
        	
        	
            return false;
		 });

 	};
}
