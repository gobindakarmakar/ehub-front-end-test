'use strict';
angular.module('ehubTransactionIntelligenceApp')
	   .controller('fullAnalysisModalController', fullAnalysisModalController);

fullAnalysisModalController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'$uibModalInstance',
	'fullAnalysisData',
	'fullTranxData',
	'scenario',
	'EHUB_API',
	'$timeout'
];

function fullAnalysisModalController(
		$scope,
		$rootScope,
		$state,
		$uibModalInstance,
		fullAnalysisData,
		fullTranxData,
		scenario,
		EHUB_API,
		$timeout){
	$scope.transactionDetails = fullTranxData;
	window.vlaId = "#vlaTIFull";
	var actualBackgrounColors =[];
	$scope.setScenario = scenario;
	$scope.changeScenario =function(){		
		window.changeScenarioMain($scope.setScenario);
		window.vlaId = "#vlaTIFull";
		$( "#vlaTIFull").empty();
		plotChart();
	};
	function plotChart(){
		setTimeout(function(){
			
			global.dimensions.width = $("#vlaTIFull").width();
			global.dimensions.height= $("#vlaTIFull").height();
			global.dimensions.top=0;
			global.dimensions.left=0;
	//	$scope.iframeURL = "http://localhost:8080/ehubui/#/transactionIntelligence?q=petroSaudi&type=entity&entityID=petroSaudi&searchName=petroSaudi";
			var vla_options = {"target_html_element": "vlaTIFull", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true}};
		    if (typeof custom_config != 'undefined')
		        {vla_options = $.extend({}, custom_config, vla_options);}// jshint ignore:line
//		    var vla_inst = new VLA.Main(vla_options);
		    if(window.global.vlaData['#vla']){
		    	window.global.graphURL = "../vendor/data/tiCompareDataStructure.json";
		    	vla_options.data = window.global.vlaData['#vla'];
		    }
		    //var vla_inst =PlotVLA(vla_options);
		    $timeout(function(){
		    	angular.forEach(window.highlightedTXns,function(d){
					$(d+'full').click();
				});
		    },1000);
		},1000);
	}
	plotChart();
	$scope.highlightTx = function(tx,index){					
		var val = $("#txCheck"+index+"full").prop("checked");				
		window[vlaId+"cy"]  .nodes().forEach(function( n ){// jshint ignore:line
          if( n.data().oid == "tx-"+(tx.id)){ 
        	  if(!actualBackgrounColors[tx.id]){
        		  actualBackgrounColors[tx.id] =n.style("background-color");
        	  }                	 
        	  if(val){
        		  if($.inArray("#txCheck"+index, window.highlightedTXns) ==-1)
        		  		{window.highlightedTXns.push("#txCheck"+index);}
        		  n.style("background-color","#4192b7");
        	  }else{
        		  var index1 =  window.highlightedTXns.indexOf("#txCheck"+index);
        		  if (index1 > -1) {
        			  window.highlightedTXns.splice(index1, 1);
        			}
        		  n.style("background-color", actualBackgrounColors[tx.id]? actualBackgrounColors[tx.id]:"#768A93");
        	  }
          }
		});
	};
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
		window.vlaId ="#vla";
//		$timeout(function(){			
////			$(".txcheckCls").prop("checked",false);
////			$(window.vlaId).empty();
			global.dimensions.width = $("#vla").width();
			global.dimensions.height=450;
			global.dimensions.top=0;
			global.dimensions.left=0;
////		$scope.iframeURL = "http://localhost:8080/ehubui/#/transactionIntelligence?q=petroSaudi&type=entity&entityID=petroSaudi&searchName=petroSaudi";
//			var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true}};
//		    if (typeof custom_config != 'undefined')
//		        vla_options = $.extend({}, custom_config, vla_options);
////		    var vla_inst = new VLA.Main(vla_options);
//		    var  vla_inst =  PlotVLA(vla_options);
//		    $timeout(function(){
//		    	angular.forEach(window.highlightedTXns,function(d,i){
//					$(d).click();
//				});
//		    },1000)
//		},1000)
			
		
		
 		$uibModalInstance.dismiss('close');
 	};
 	
}
