'use strict';
angular.module('ehubTransactionIntelligenceApp')
	   .controller('createAlertModalController', createAlertModalController);

createAlertModalController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'$uibModalInstance',
	'HostPathService',
	'TransactionAnalysisApiService',
	'$uibModal'
];

function createAlertModalController(
		$scope,
		$rootScope,
		$state,
		$uibModalInstance,
		HostPathService,
		TransactionAnalysisApiService,
		$uibModal){
	
	//Show hide upload list
	$scope.selectAlert = true;	
	$scope.header ="Generate/Upload An Alert";
	$scope.showUploadAlert = function(){
		$scope.resetFiles();
		$scope.selectAlert = false;
		$scope.isGenerate = false;
	};
	$scope.showGenerateAlert = function(){
		$scope.resetFiles();
		$scope.selectAlert = false;
		$scope.isGenerate = true;
	};
	
	$scope.resetFiles=function(){
		$scope.uploadCustomerDetailsName ="Customer Details";
		$scope.uploadAccountName ="Account Details";
		$scope.uploadCustomerRelationShiptxName ="Customer RelationShip Details";
		$scope.uploadShareHolderstxName ="Shareholders";
		$scope.alertName ="Alert Details";
		$scope.uploadTxDataName ="Transactions Details";
		$scope.uploadCustomerDetailsFile='';
		$scope.uploadAccountFile ='';
		$scope.uploadCustomerRelationShiptxFile ='';
		$scope.alertFile ='';
		$scope.uploadTxDataFile ='';
	};	
	$scope.resetFiles();
	
	//var token = $rootScope.ehubObject.token;
	// var params = {
	// 	token: token
	// };
	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
 	 /**
		 * Function to upload file
		 */
   	 $scope.upload = function(file,item){
   		 //var queryParam ='';
   		//var files;
   		 if(item == 'customers'){
   			$scope.uploadCustomerDetailsName= file.name;
//   			queryParam = "upload/uploadCustomerDetailsFile";
   			$scope.uploadCustomerDetailsFile=file;
   		 }else if(item == 'accounts'){
   			$scope.uploadAccountName = file.name;
//   			queryParam = "upload/uploadAccountFile";
   			$scope.uploadAccountFile =file;   			
   		 }else if(item == 'transactions'){
//   			queryParam = "upload/uploadTxData";
//   			files ={"multipartFile1": file};   			
   			$scope.uploadTxDataFile =file;
   			$scope.uploadTxDataName = file.name;
   		 }else if(item == 'customerRelationships'){
//   			queryParam = "upload/uploadCustomerRelationShipDetailsFile";
   			$scope.uploadCustomerRelationShiptxName =file.name;
   			$scope.uploadCustomerRelationShiptxFile =file;   			
   		 }else if(item == 'alert'){
//   			queryParam = "upload/uploadAlertFile";
			 $scope.alertName = file.name;
			 $scope.alertFile =file;
   		 }else if(item == 'shareHolders'){
//   			queryParam = "upload/uploadAlertFile";
			 $scope.uploadShareHolderstxName = file.name;
			 $scope.uploadShareHoldersFile =file;
   		 }
//   		 if(!files)
//   			 files ={"multipartFile": file};
//   		$scope.showSpinner1 = true;
//   		 TransactionAnalysisApiService.uploadFile(params, files,queryParam).then(function(response){
//			$scope.showSpinner1 = false;		   			
//		 	HostPathService.FlashSuccessMessage('SUCCESSFUL UPLOAD DOCUMENT', response.responseMessage);
//		}, function(error){
//			$scope.showSpinner1 = false;
//		 	HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Uploaded Failed, Please try again later..');
//		});
   	 };
 	/**
	 * Function to process data
	 */
   	 $scope.processData = function(){
   		 var msg =[];
   		 if($scope.isGenerate && !$scope.uploadCustomerDetailsFile && !$scope.uploadAccountFile && !$scope.uploadCustomerRelationShiptxFile  && !$scope.uploadTxDataFile && !$scope.uploadShareHoldersFile){
   			HostPathService.FlashErrorMessage('Please upload atleast one file', '');
   			return false;
   		 }else if(!$scope.isGenerate && !$scope.uploadCustomerDetailsFile && !$scope.uploadAccountFile && !$scope.uploadCustomerRelationShiptxFile && !$scope.alertFile && !$scope.uploadTxDataFile && !$scope.uploadShareHoldersFile){
   			HostPathService.FlashErrorMessage('Please upload atleast one file', '');
   			return false;
   		 }
   		if(!$scope.uploadCustomerDetailsFile){
   			msg.push("Customer");
  		 }
   		 if(!$scope.uploadAccountFile){
   			msg.push("Account");
   		 }
   		if(!$scope.uploadCustomerRelationShiptxFile){
   			msg.push("Customer Relationship");
  		 }
   		if(!$scope.uploadShareHoldersFile){
   			msg.push("Shareholders");
   		}
   		if(!$scope.alertFile && !$scope.isGenerate){
   			msg.push("Alerts");
 		 }
   		if(!$scope.uploadTxDataFile){
   			msg.push("Transactions");
		 }
   		var finalMsg ="Are you sure you have "+(msg.join(','))+" details in DB?";
   		
   			if(msg.length>0){
   				confirmMissingFiles(finalMsg,$scope.isGenerate);
   			}else{
   				Uploadfiles("upload");
   			}
   		
	  };
   	/**
 	 * Function to show confirm alert
 	 */
   	var ConfirmProcessModalInstance;
   	 function confirmMissingFiles(finalMsg,isGenerate){  // jshint ignore:line
   		 $scope.finalMsg = finalMsg;
	   	ConfirmProcessModalInstance = $uibModal.open({
		     templateUrl: './modal/views/ConfirmProcess.modal.html',
	         scope: $scope,
	         size: 'lg',
	         backdrop: 'static',
	         windowClass: 'custom-modal'	         
		    });
	
	   		ConfirmProcessModalInstance.result.then(function () {
		    }, function () {
	 	});
   	 }
   	 $scope.confirmProcessResp =function(yesNo){
   		ConfirmProcessModalInstance.dismiss();
   		 if(yesNo){
   			Uploadfiles($scope.isGenerate);
   		 }else{
   			 return false;
   		 }
   	 };
   	/**
  	 * Function to call upload files 
  	 */
   	 function Uploadfiles(){ 
   		 var filesObj={};
   		 
   		 if($scope.uploadCustomerDetailsFile){   			
//   			var queryParam = "upload/uploadCustomerDetailsFile";   			
   			filesObj.multipartFileCustomer = $scope.uploadCustomerDetailsFile;
//   			callUploadService(params, files,queryParam);
   		 }
   		 if($scope.uploadAccountFile){   			
//   			var queryParam = "upload/uploadAccountFile";   				
   			filesObj.multipartFileAccount =$scope.uploadAccountFile;
//   			callUploadService(params, files,queryParam);
   		 }
   		 if($scope.uploadTxDataFile){
//   			var queryParam = "upload/uploadTxData";   			
   			filesObj.multipartFileTransaction = $scope.uploadTxDataFile;
//   			callUploadService(params, files,queryParam);
   		}
   		 if($scope.uploadCustomerRelationShiptxFile){
//   			var queryParam = "upload/uploadCustomerRelationShipDetailsFile";
   			filesObj.multipartFileCustomerRelationShipDetailsFile = $scope.uploadCustomerRelationShiptxFile;
//   			callUploadService(params, files,queryParam);
   		 }
   		 if($scope.alertFile){
//   			var queryParam = "upload/uploadAlertFile";
   			filesObj.multipartFileAlert =$scope.alertFile;
//   			callUploadService(params, files,queryParam);
   		 }
   		 if($scope.uploadShareHoldersFile){

			filesObj.multipartFileShareholders =$scope.uploadShareHoldersFile;

		 }
   		callUploadService(filesObj);
   	 }
   	 /**
   	  * Function to call upload file service 
   	  */
   	 function callUploadService(files){
   		 TransactionAnalysisApiService.uploadFile(files).then(function(response){
   			$scope.showSpinner1 = false;		   	
   			 if(response.data.flag){
   				HostPathService.FlashSuccessMessage(response.data.message,'');
   			    clearInterval(window.alertInterval);
   				window.alertList();
   			 }else{
   			 }
 					
 		 	
 		}, function(response){
 			$scope.showSpinner1 = false;
 		 	HostPathService.FlashErrorMessage(response.data.message,'');
 		});
   		$uibModalInstance.close('upload');
   	 }
}