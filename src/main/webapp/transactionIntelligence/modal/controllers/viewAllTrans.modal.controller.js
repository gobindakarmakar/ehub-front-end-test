'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('ViewAllTransModalController', viewAllTransModalController);

	viewAllTransModalController.$inject = [
	    '$scope',
	    '$rootScope',
	    'EnrichSearchGraph',
	    'EntityGraphService',
	    '$uibModalInstance',
	    'TransactionAnalysisApiService',
	    'productType',
	    'fromDt',
	    'toDt',
	    'txsdetails',
	    'checkedTypeDetails',
	    'DTOptionsBuilder',
	    'viewByDetails',
	    'header',
	    'filtersObj'
	];
	
	function viewAllTransModalController(
	    $scope,
	    $rootScope,
	    EnrichSearchGraph,
	    EntityGraphService,
	    $uibModalInstance,
	    TransactionAnalysisApiService,
	    productType,
	    fromDt,
	    toDt,
	    txsdetails,
	    checkedTypeDetails,
	    DTOptionsBuilder,
	    viewByDetails,
	    header,
	    filtersObj
	) {
	
	$scope.viewAllTxsPreloader = true;
	$scope.txsdetails = txsdetails;
	$scope.productType = productType;
	$scope.modalHeader = header;
	$scope.dtOptions1 = DTOptionsBuilder.newOptions()
										  .withOption('bLengthChange', false)
										  .withOption("bPaginate", false)
										  .withOption("bFilter", false)
										  .withOption("bInfo", false)
										  .withOption('responsive', true);
	$scope.pageNum = 1;
	var params = {
		token: $rootScope.ehubObject.token,
		recordsPerPage: 15,
		pageNumber: $scope.pageNum,
		productType: productType
	};
	/*
	 * @purpose: page change
	 * @created: 13 feb 2018
	 * @params: pageNum
	 * @returns: no
	 * @author: swathi
	 */
	$scope.pageChanged = function(pageNum){
		params = {
				token: $rootScope.ehubObject.token,
				recordsPerPage: 15,
				pageNumber: pageNum
			};
		if(productType == 'allTxs'){			
			getAllTxs(params);
		}else if(productType == 'CustomerRisk' || productType == 'ProductRisk' || productType == 'CorruptionRisk' || productType == 'PoliticalRisk' || productType == 'GeoGraphicRisk' || productType == 'CorporateStructure' || productType == 'Geography' || productType == 'Shareholders' || productType == 'ActivityType' || productType == 'Banks' || productType == 'GeoGraphic' || productType == 'BankLocations'){
			params.type= productType;
			getViewAllForCorpCounterRisk(params);
		}else{
			params.type= productType;
			getTxsByProductType(params);
		}
	};
	/*
	 * @purpose: get Txs By Product Type
	 * @created: 13 feb 2018
	 * @params: params
	 * @returns: no
	 * @author: swathi
	 */
	function getTxsByProductType(params){
		$scope.viewAllTxsPreloader = true;
		var viewBy = viewByDetails.viewBy;
		TransactionAnalysisApiService.getViewAllForTxsByType(fromDt, toDt, viewBy, params,filtersObj).then(function(response){
			$scope.viewAllTxsPreloader = false;
			$scope.txsDataByproductType = response.data.fetchTxByTypeListViewAll.transactionsDataList;
			angular.forEach($scope.txsDataByproductType, function(v){
				timestampToDatetime(v.txDate);
				v.txDate = formattedDate; 
				v.txTime = formattedTime;
			});
			$scope.txsLength = response.data.pagination.totalResults;
		}, function(){
			$scope.viewAllTxsPreloader = false;
		});
	}
	/*
	 * @purpose: get All Txs
	 * @created: 13 feb 2018
	 * @params: params
	 * @returns: no
	 * @author: swathi
	 */
	function getAllTxs(params){
		$scope.viewAllTxsPreloader = true;
		var date = [fromDt, toDt, checkedTypeDetails];
		params = {
			token: $rootScope.ehubObject.token,
			recordsPerPage: 15,
			pageNumber: $scope.pageNum
		};
		TransactionAnalysisApiService.getFetchTxsCountAndRatio(date, params,filtersObj).then(function(response){
			$scope.viewAllTxsPreloader = false;
			$scope.txsDataByproductType =  response.data.transactionsListAlertCountRatioDto.transactionsDataList;
			angular.forEach($scope.txsDataByproductType, function(v){
				timestampToDatetime(v.txDate);
				v.txDate = formattedDate; 
				v.txTime = formattedTime;
			});
			$scope.txsLength = response.data.pagination.totalResults;
		}, function(){
			$scope.viewAllTxsPreloader = false;
		});
	}
	
	/*
	 * @purpose: get All risks by type
	 * @created: 14 feb 2018
	 * @params: params
	 * @returns: no
	 * @author: swathi
	 */
	function getViewAllForCorpCounterRisk(){
		$scope.viewAllTxsPreloader = true;
		params = {
			token: $rootScope.ehubObject.token,
			recordsPerPage: 15,
			pageNumber: $scope.pageNum,
			type: productType
		};
		TransactionAnalysisApiService.getViewAllForCorpCounterRisk(fromDt, toDt, params,filtersObj).then(function(response){
			$scope.viewAllTxsPreloader = false;
			$scope.txsDataByproductType =  response.data.result;			
			angular.forEach($scope.txsDataByproductType, function(v){
				timestampToDatetime(v.businessDate);
				v.txDate = formattedDate; 
				v.txTime = formattedTime;
				v.txAmount = v.transactionAmount;
				
			});
			$scope.txsLength = response.data.paginationInformation.totalResults;
		}, function(){
			$scope.viewAllTxsPreloader = false;
		});
	}
	
	if(productType == 'allTxs'){
		getAllTxs(params);
	}else if(productType == 'CustomerRisk' || productType == 'ProductRisk' || productType == 'CorruptionRisk' || productType == 'PoliticalRisk' || productType == 'GeoGraphicRisk' || productType == 'CorporateStructure' || productType == 'Geography' || productType == 'Shareholders' || productType == 'ActivityType' || productType == 'Banks' || productType == 'GeoGraphic' || productType == 'BankLocations'){
		getViewAllForCorpCounterRisk(params);
	}else{
		getTxsByProductType(params);
	}
	
	var formattedTime, formattedDate;
	/*
	 * @purpose: convert timestamp To Datetime
	 * @created: 12 feb 2018
	 * @params: timestamp
	 * @returns: no
	 * @author: swathi
	 */
	function timestampToDatetime(timestamp){
		var date = new Date(timestamp);
		var monthName = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		formattedTime = date.toLocaleTimeString() + ' IST';
		formattedDate = date.getDate() + ' ' + monthName[date.getMonth()] + ' ' + date.getFullYear();
	}
	/*
	 * @purpose: close ViewAllTransModal
	 * @created: 12 feb 2018
	 * @params: null
	 * @returns: no
	 * @author: swathi
	 */
	$scope.closeViewAllTransModal = function(){
		$uibModalInstance.dismiss('close');
	};
}