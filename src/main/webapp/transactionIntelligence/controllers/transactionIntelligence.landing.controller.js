'use strict';
angular.module('ehubTransactionIntelligenceApp')
	   .controller('TransactionIntelligenceLandingController', transactionIntelligenceLandingController);

	transactionIntelligenceLandingController.$inject = [
			'$scope', 
			'$http',
			'$window',
			'$location', 
			'$rootScope',
			'$timeout',
			'$state',
			'$uibModal',
			'EHUB_API',
			'TransactionAnalysisApiService'
		];
		
	function transactionIntelligenceLandingController(
			$scope, 
			$http, 
			$window, 
			$location, 
			$rootScope, 
			$timeout, 
			$state,
			$uibModal,
			EHUB_API, 
			TransactionAnalysisApiService) {
		var pollAlertInterval;
		//pagination variables starts
		$scope.alertsLength=0;
		$scope.pageNum =1;
		//pagination variables ends
		// Create Alert Modal 
		$scope.createAlertModal = function(){
			 var opencreateAlertModal = $uibModal.open({
		            templateUrl: './modal/views/createAlertTi.modal.html',
		            controller: 'createAlertModalController',
		            backdrop: 'static',
		            size: 'sm',
		            windowClass: 'custom-modal mip-save-modal mip-save-wrapper creatAlertModalWrapper',
//		            resolve:{
//		            	
//		            }
		        });

		 		opencreateAlertModal.result.then(function (response) {		 			
		 			if(response =="upload"){
			        	$scope.getAllAlerts();
			        	pollAlerts();
		        	}
		        }, function () {
		        	
		        });
		 };
		
		/**
		 * Function to poll data after uploading the file
		 * 
		 */
		 function pollAlerts(){
			 if(pollAlertInterval){
				 clearInterval(pollAlertInterval);
			 }
			 pollAlertInterval = setInterval(function(){
				 $scope.getAllAlerts("","noloader");
			 },60000);
		 }
		//dummy data starts here
		var locationData=[{
			"count":3,
			"point":{				
				country	:" Switzerland",
				latitude:46.204391,
				longitude:6.143158
			}
		}];
		var lineDataDummy =[{
		                "time": '2010-01-05T00:00:00.000Z',
		                "value": 700000000,
		                "x": new Date('2010-01-05T00:00:00.000Z')
		            },{
		                "time": '2010-12-12T00:00:00.000Z',
		                "value": 5000000,
		                "x": new Date('2010-12-12T00:00:00.000Z')
		            },{
		                "time": '2010-12-12T00:00:00.000Z',
		                "value": 5000000,
		                "x": new Date('2010-12-12T00:00:00.000Z')
		            }];
		var pieData =[{
			"val":3,
			"key":"Online"
		}];
		//dummy data ends here
		var div = d3.select("body").append("div").attr("class", "toolTip_horizontal text-uppercase").style("position", "absolute").style("z-index", 1000).style("background", "#283c45").style("padding", "5px 10px").style("border-radius", "10px").style("color", "rgb(36, 115, 181)").style("font-size", "10px").style("display", "none");// jshint ignore:line
	//	Map code starts here
	$scope.dtOptions = {
		bPaginate:false,
	    bFilter: false,
	    bInfo: false,
//	    "pageLength": 25,
	    bSort: false
	};
	var bombMap = new Datamap({
	    element: document.getElementById('taLocations'),
	    scope: 'world',
	    fills: {
	        'red': '#884BA2',
	        'green': '#54A063',
	        'blue': '#2CBCFA',
	        'seablue': '#37FEFF',
	        defaultFill: '#334851'
	    },
	    geographyConfig: {
	        highlightFillColor: '#1b2735',
	        popupOnHover: false,
	        highlightBorderOpacity: 0
	    },
	    bubblesConfig: {
	        borderWidth: 0,
	        borderOpacity: 0,
	        borderColor: 'transparent',
	
	    },
	    done: function (datamap) {
	        datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
	        function redraw() {
	            datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
	        }
	    }
	
	});
	//map code ends here
	//get  all alerts
	$scope.alerts = [];
	
	$scope.getAllAlerts = function (search,noLoader) {
		
//	    $('.transactionAnalysisLandingLoader').css('display', 'block');
		if(!noLoader)
			{$scope.transactionAnalysisLandingLoader = true;}
	    var queryParams = 'recordsPerPage=25&pageNumber='+$scope.pageNum;
	    if (search) {
	        if ($scope.searchPerson) {
	            queryParams = queryParams + "&name=" + $scope.searchPerson;
	        }
	        if ($('#searchFrom').val()) {
	            var dtfrmArr = $('#searchFrom').val().split("/");
	            queryParams = queryParams + "&date-from=" + dtfrmArr[2] + "-" + dtfrmArr[0] + "-" + dtfrmArr[1];
	
	        }
	        if ($('#searchTO').val()) {
	            var dtToArr = $('#searchTO').val().split("/");
	            queryParams = queryParams + "&date-to=" + dtToArr[2] + "-" + dtToArr[0] + "-" + dtToArr[1];
	
	        }
	    }
	    queryParams =queryParams+"&token="+$rootScope.ehubObject.token;
	  
	    TransactionAnalysisApiService.getAlertsList(queryParams).then(function(result){
	    	if(!result.data.listAlerts){
	    		$scope.alerts = [];
	    		$scope.alertsLength =0;
	    		$scope.transactionAnalysisLandingLoader = false;
	    		return false;
	    	}
	    	$scope.alertsLength = result.data.information.totalResults;
	    	
	    	$.map(result.data.listAlerts,function(d){
	    		d['search-name'] = d['search-name']?d['search-name']:d['focal-entity'];
	    		return (d.id =d.id?d.id:d.alertId);
	    	});
	    	
	    	var r = result.data.listAlerts;
	    	$timeout(function () {
	    		$scope.transactionAnalysisLandingLoader=false;
//                $('.transactionAnalysisLandingLoader').css('display', 'none');
                if (r.length == 0) {
                    $scope.alerts = [];
                    $("#transactionTypes").empty();
                    $("#taAmountRatio").empty();
                    $("#taLocations").empty();
                    return false;
                }
                r.sort(function (a, b) {
                    if (a['search-name'] < b['search-name'])
                        {return -1;}
                    if (a['search-name'] > b['search-name'])
                        {return 1;}
                    return 0;
                });
                r.map(function(d){
                	return (d.scenariotype= (d.scenario && d.scenario.toLowerCase() =="large reportable trans"?"Peak Activity Scenario":"Average Activity Scenario"));
                });
                $scope.alerts = r;
                $scope.loadEntityDetails(r[0],"","dummy");

            }, 0);
	    }, function () {
	    	$scope.transactionAnalysisLandingLoader=false;
//            $('.transactionAnalysisLandingLoader').css('display', 'none');
        });
	};
	$scope.getAllAlerts();
	/*
     * @purpose: on page change
     * @created: 15 sep 2017
     * @params: page(number)
     * @return: no
     * @author: swathi
    */ 
    $scope.pageChanged = function (page) {
        $scope.pageNum = page;
    	$scope.getAllAlerts();
    };
	/**
	 * Function to load alert details
	 */
	$scope.loadEntityDetails = function (alert, event,dummy) {
	    if (event) {
	        $(event.target).parent().parent().find("tr").removeClass("transactionTableActive");
	        $(event.target).parent().addClass('transactionTableActive');
	    }
	   
	    if(!dummy){
//	    	 $('.transactionAnalysisLandingLoader').css('display', 'block');
	    	 $scope.transactionAnalysisLandingLoader=true;
		    var data = {
	    		id: alert.id,
	    		token: $rootScope.ehubObject.token
		    };
		    TransactionAnalysisApiService.getAlertSummaryById(data).then(function(result){
		    	var r = result.data;
		    	$timeout(function () {
//	                $('.transactionAnalysisLandingLoader').css('display', 'none');
	                $scope.transactionAnalysisLandingLoader=false;
	                plotAlertDetailCharts(r);
	            }, 0);
		    }, function () {
		    	$('#taAmountRatio').html("<div style='color:#6B7E8C;margin-top:70px' class='text-uppercase'><center>Something went wrong.please try again later..</center></div>");
	            $('#transactionTypes').html("<div style='color:#6B7E8C;margin-top:70px' class='text-uppercase'><center>Something went wrong.please try again later..</center></div>");
//	            $('.transactionAnalysisLandingLoader').css('display', 'none');
	            $scope.transactionAnalysisLandingLoader=false;
	        });
	    }else{
	    	plotAlertDetailCharts('',dummy);
	    }
	};
	/**
	 * Function to plot alert detail charts
	 */
	function plotAlertDetailCharts(alertDetails,dummy) {
		if(!dummy){
		    //call function to plot pie
		    var piedata = [];
		    var sumCount = 0;
		    if (!alertDetails['tx-by-type'] || alertDetails['tx-by-type'].length == 0) {
		        $('#transactionTypes').html("<div style='color:#6B7E8C;margin-top:70px' class='text-uppercase'><center>No Data available</center></div>");
		
		    } else {
		        angular.forEach(alertDetails['tx-by-type'], function (d) {
		            piedata.push({
		                key: d.point,
		                val: d.count
		            });
		            sumCount = sumCount + d.count;
		        });
		        var colors = ["#2873B5", "#E42645", "#339971", "#DA51DF"];
		        $scope.InitializeandPlotPie(piedata, "transactionTypes", colors, "true", sumCount, "true", "", "30", "160");
		    }
		    if (!alertDetails['tx-by-time'] || alertDetails['tx-by-time'] == 0) {
		        $('#taAmountRatio').html("<div style='color:#6B7E8C;margin-top:70px' class='text-uppercase'><center>No Data available</center></div>");
		
		    } else {
		        //call function to plot line
		        var lineData = [{
		                key: "",
		                values: []
		            }];
		        angular.forEach(alertDetails['tx-by-time'], function (d) {
//		            var tm = d.point.slice(0, 4) + "-" + d.point.slice(4, 6) + "-" + d.point.slice(6);
		            lineData[0].values.push({
		                "time": new Date(d.point),
		                "value": d.sum,
		                x: new Date(d.point)
		            });
		        });
		        var options = {
		            container: "#taAmountRatio",
		            height: '160',
		            axisX: true,
		            axisY: true,
		
		            data: lineData,
		            actualData: lineData[0].values
		        };
		        InitializeandPlotLine(options);
		    }
		    if (!alertDetails['tx-by-location'] || alertDetails['tx-by-location'].length == 0) {
		        d3.select("#taLocations").selectAll(".datamaps-bubble").remove();
		    } else {
		        d3.select("#taLocations").selectAll(".datamaps-bubble").remove();
		        plotCirclesOnMap(bombMap, "taLocations", alertDetails['tx-by-location']);
		    }
		}else{
			  d3.select("#taLocations").selectAll(".datamaps-bubble").remove();
		        plotCirclesOnMap(bombMap, "taLocations", locationData);
		        var line_date = [{
	                key: "",
	                values: lineDataDummy
	            }];
		        var options = {
			            container: "#taAmountRatio",
			            height: '160',
			            axisX: true,
			            axisY: true,			
			            data: line_date,
			            actualData: line_date[0].values
			        };
		      
		        InitializeandPlotLine(options);
		        var colors = ["#2873B5", "#E42645", "#339971", "#DA51DF"];
		        $scope.InitializeandPlotPie(pieData, "transactionTypes", colors, "true", 3, "true", "", "30", "160");
		}
	}
	
	var tool_tip = $('body').append('<div class="Bubble_Chart_tooltip" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');// jshint ignore:line
	  

	/**
	 * Funtoin to plot circles on map
	 */
	function plotCirclesOnMap(bombMap, id, data) {
	    //plotting circles on map
	    d3.select("#" + id).selectAll(".datamaps-bubble").remove();
	    var currentData = [];
	    if (data) {
	        currentData = data;
	    } else {
	        return;
	    }
	    var bombScale = d3.scaleLinear().domain(d3.extent(currentData, function (d) {
	        return d.count;
	    })).range([5, 10]);
	
	    if (currentData.length > 0) {
	
	        currentData.map(function (d) {
	            if (data && !d.count) {
	                d.count = (Math.floor(Math.random() * 150)) + 1;
	            }
	            if (d.count < 20) {
	                d.fillKey = "green";
	            } else if (d.count < 80) {
	                d.fillKey = "seablue";
	            } else if (d.count < 130) {
	                d.fillKey = "blue";
	            } else {
	                d.fillKey = "red";
	            }
	            d.location = d.point.country;
	            d.latitude = d.point.latitude;
	            d.longitude = d.point.longitude;
	            d.weight = d.count;
	            return (d.radius = bombScale(d.count));
	        });
	        //circles on world map
	        var bombs = currentData;
	        //draw bubbles for bombs
	        bombMap.bubbles(bombs, {
//	            popupTemplate: function (geo, d) {
//	                if (d.type) {
//	                    return ["<div class='tooltip_datamap_all_data text-uppercase'><div style='color:white;margin-bottom:15px'><h5>" + (d.location) + "</h5></div><div style='margin-bottom:15px'>Funding Received <h4 style='color:#3372A8;margin-top:0'>" + d.count + "BN</h4></div><div style='margin-bottom:15px'>Type <h6 style='color:#3372A8;margin-top:0'>" + d.type + "</h6></div></div>"].join('');
//	                } else {
//	                    return ['<div class="hoverinfo">Investments: ' + d.count,
//	                        '</div>'].join('');
//	                }
//	
//	            }
	        });
	        d3.select("#"+id).selectAll(".datamaps-hoverover").remove();
	        d3.select("#"+id).selectAll(".datamaps-bubble")
	        .on("mouseover",function(d){
	        	
	        	 $(this).css("opacity", 1);
	        	$(".Bubble_Chart_tooltip").html('<div class=" text-uppercase"><div style="color:white;padding:5px"><span>' + (d.location) + ' :'+d.count+'</span></div>');
                
                return $(".Bubble_Chart_tooltip").css("display", "block");
           
	        })
	        .on("mousemove",function(){
	        	var p=$("#" + id);
            	var position=p.offset();
            	//var windowWidth=window.innerWidth;
            	var tooltipWidth=$(".Bubble_Chart_tooltip").width()+50;
            	var cursor=d3.event.x;
             	if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)){
            		var element = document.getElementsByClassName("Bubble_Chart_tooltip");
            		element[0].classList.remove("tooltip-left");
        			element[0].classList.add("tooltip-right");
               		$(".Bubble_Chart_tooltip").css("left",(d3.event.pageX - 15-$(".Bubble_Chart_tooltip").width()) + "px");
             	}else{
            		var element =document.getElementsByClassName("Bubble_Chart_tooltip");
               		element[0].classList.remove("tooltip-right");
           		    element[0].classList.add("tooltip-left");
           		    $(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
             	}
                return $(".Bubble_Chart_tooltip").css("top",d3.event.pageY-20 + "px");
	
	        })
	        .on("mouseout",function(){
	        	
	        	 $(this).css("opacity", 0.7);
                    //hide tool-tip
                    return $(".Bubble_Chart_tooltip").css("display", "none");
	        	
	        });
	        d3.select("#"+id).selectAll(".datamaps-bubble").attr("r", function (d) {
	            return d.radius;
	        }).style("fill-opacity", 0.7);
	    }
	}
	/**
	 * Function to load dashboard 
	 */
	$scope.loadDashboard = function (alert,dummy) {
		if(dummy){
			 $state.go('transactionIntelligence',{'q':"petroSaudi International ltd", 'type': 'entity', 'entityID': "petroSaudi International ltd", 'searchName': "petroSaudi International ltd","Etype":"Company"});
		}
		if(alert){
			if (alert['focal-entity-id']) {
		        $state.go('transactionIntelligence', {'q': alert.id,  'entityID': alert['focal-entity-id'], 'searchName': alert['search-name'],"Etype":(alert.accountType=="single"?"Person":"Company")});
		    }
		}
	};
	/**
	 * Function to plot pie
	 */
	$scope.InitializeandPlotPie = function (data, id, colors, ispadding, istxt, islegends, islegendleft, legendwidth, height) {
	
	    $("#" + id).empty();
	    data.map(function (d) {
	        return (d.value = d.doc_count ? d.doc_count : d.val);
	    });
	    var newData = [];
	    var keys = [];
	    angular.forEach(data, function (d) {
	        if (d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(), keys) == -1) {
	            newData.push(d);
	            keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
	        }
	    });
	    var maxval = d3.max(newData, function (d) {
	        return d.val;
	    });
	    var options = {
	        container: "#" + id,
	        data: newData,
	        height: height ? height : 140,
	        maxval: maxval,
	        colors: colors,
//	        ispadding: ispadding,
	        istxt: istxt,
	        islegends: islegends,
	        islegendleft: islegendleft,
	        legendwidth: legendwidth
	    };
	    setTimeout(function () {
	        new reusablePie(options);
	    });
	};
		
}