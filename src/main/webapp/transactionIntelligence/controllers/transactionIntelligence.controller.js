'use strict';
angular.module('ehubTransactionIntelligenceApp')
	.controller('TransactionIntelligenceController', transactionIntelligenceController);

transactionIntelligenceController.$inject = [
	'$scope',
	'$http',
	'$window',
	'$location',
	'$rootScope',
	'$state',
	'$stateParams',
	'HostPathService',
	'Flash',
	'$timeout',
	'$uibModal',
	'ActApiService',
	'SearchAPIEndpoint',
	'$sce',
	'EHUB_API',
	'TransactionAnalysisApiService',
	'EntityApiService'
];

function transactionIntelligenceController(
	$scope,
	$http,
	$window,
	$location,
	$rootScope,
	$state,
	$stateParams,
	HostPathService,
	Flash,
	$timeout,
	$uibModal,
	ActApiService,
	SearchAPIEndpoint,
	$sce,
	EHUB_API,
	TransactionAnalysisApiService,
	EntityApiService
) {
	$scope.otherAlertsForCust = [];
	$scope.dataTxByTypeData = [];
	$scope.activityScenario = [];
	var count;
	//add dummy data here
	var inputDetaislPetro = {
		"total": {
			"point": "ALL",
			"count": 50,
			"sum": 1000000000
		},
		"counterparty": [{
			"point": "1MDB",
			"count": 11,
			"sum": 1000000000
		}],
		"country": [{
			"point": "Malaysia",
			"count": 3,
			"sum": 1000000000,
			country: {
				country: "Malaysia",
				latitude: 4.210484,
				longitude: 101.975766
			}
		}]
	};
	var outPutDetaislPetro = {
		"total": {
			"point": "ALL",
			"count": 50,
			"sum": 700000000
		},
		"counterparty": [{
			"point": "Good Star",
			"count": 50,
			"sum": 700000000
		}, {
			"point": "Totality LTD",
			"count": 2,
			"sum": 5000000
		}, {
			"point": "Jasmine LOO Ai Swan",
			"count": 2,
			"sum": 5000000
		}
		],
		"country": [{
			"point": "Seychelles",
			"count": 3,
			"sum": 700000000,
			country: {
				country: "Seychelles",
				latitude: -4.679574,
				longitude: 55.491977
			}
		}]
	};
	var transactionPetro = [{
		"custom-text2": "FE-CC-84-CE",
		"date-bene": "2010-01-05T00:00:00.000Z",
		"currency-bene": "USD",
		"date": "2010-01-05T00:00:00.000Z",
		"party-identifier": "C0-12-4A-F6",
		"normalized-name-bene": "petrosaudi",
		"name-org": "1MDB",
		"normalized-name-org": "1MDB",
		"currency-org": "USD",
		"country-org": " Malaysia",
		"tx-id-bene": "22536449981",
		"tx-id-org": "22536449981",
		"data-source": "DEP",
		"channel": "International Transfer",
		"type": "7754",
		"debit-credit": "C",
		"name-bene": "petrosaudi",
		"currency": "USD",
		"product-type": "EFT-OTHER",
		"id": "22653121283",
		"amount-activity-org": "1000000000",
		"amount-activity-bene": "1000000000",
		"country-bene": " Seychelles",
		"date-org": "2010-01-05T00:00:00.000Z",
		"custom-text4": "C0-99-6E-F3",
		"amount-base": 1000000000,
		"amount-activity": 1000000000
	}, {
		"custom-text2": "FE-CC-84-CE",
		"date-bene": "2010-02-05T00:00:00.000Z",
		"currency-bene": "USD",
		"date": "2010-02-05T00:00:00.000Z",
		"party-identifier": "C0-12-4A-F6",
		"normalized-name-bene": "Good Star",
		"name-org": "Petro Saudi",
		"normalized-name-org": "Petro Saudi",
		"currency-org": "USD",
		"country-org": " Seychelles",
		"tx-id-bene": "22536449981",
		"tx-id-org": "22536449981",
		"data-source": "DEP",
		"channel": "Wired Transfer",
		"type": "7754",
		"debit-credit": "D",
		"name-bene": "Good Star",
		"currency": "USD",
		"product-type": "EFT-OTHER",
		"id": "22653121284",
		"amount-activity-org": "700000000",
		"amount-activity-bene": "700000000",
		"country-bene": " Seychelles",
		"date-org": "2010-02-05T00:00:00.000Z",
		"custom-text4": "C0-99-6E-F3",
		"amount-base": 700000000,
		"amount-activity": 700000000
	}, {
		"custom-text2": "FE-CC-84-CE",
		"date-bene": "2010-12-12T00:00:00.000Z",
		"currency-bene": "USD",
		"date": "2010-12-12T00:00:00.000Z",
		"party-identifier": "C0-12-4A-F6",
		"normalized-name-bene": "Totality LTD",
		"name-org": "Good Star",
		"normalized-name-org": "Good Star",
		"currency-org": "USD",
		"country-org": " Seychelles",
		"tx-id-bene": "22536449981",
		"tx-id-org": "22536449981",
		"data-source": "DEP",
		"channel": "Wired Transfer",
		"type": "7754",
		"debit-credit": "D",
		"name-bene": "Totality LTD",
		"currency": "USD",
		"product-type": "EFT-OTHER",
		"id": "22653121285",
		"amount-activity-org": "5000000",
		"amount-activity-bene": "5000000",
		"country-bene": " Seychelles",
		"date-org": "2010-12-12T00:00:00.000Z",
		"custom-text4": "C0-99-6E-F3",
		"amount-base": 5000000,
		"amount-activity": 5000000
	}, {
		"custom-text2": "FE-CC-84-CE",
		"date-bene": "2010-12-13T00:00:00.000Z",
		"currency-bene": "USD",
		"date": "2010-12-13T00:00:00.000Z",
		"party-identifier": "C0-12-4A-F6",
		"normalized-name-bene": "Jasmine LOO Ai Swan",
		"name-org": "Good Star",
		"normalized-name-org": "Good Star",
		"currency-org": "USD",
		"country-org": " Seychelles",
		"tx-id-bene": "22536449981",
		"tx-id-org": "22536449981",
		"data-source": "DEP",
		"channel": "Wired Transfer",
		"type": "7754",
		"debit-credit": "D",
		"name-bene": "Jasmine LOO Ai Swan",
		"currency": "USD",
		"product-type": "EFT-OTHER",
		"id": "22653121286",
		"amount-activity-org": "5000000",
		"amount-activity-bene": "5000000",
		"country-bene": " Seychelles",
		"date-org": "2010-12-13T00:00:00.000Z",
		"custom-text4": "C0-99-6E-F3",
		"amount-base": 5000000,
		"amount-activity": 5000000
	}];
	var newsPetro = [
		{
			"text": "How The DOJ Exposed The Deceit And Lies By 1MDB's Staff Plotters",
			"url": 'http://www.sarawakreport.org/2017/09/how-doj-exposed-the-deceit-and-lies-by-1mdbs-staff-plotters-analysis/'
		}
	];
	//ends dummy data here
	//code for number formatting
	Number.prototype.formatAmt = function (decPlaces, thouSeparator, decSeparator) {
		var n = this,
			decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
			decSeparator = decSeparator == undefined ? "." : decSeparator,
			thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
			sign = n < 0 ? "-" : "",
			i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
			j = (j = i.length) > 3 ? j % 3 : 0;
		return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
	};

	function getFormattedAmount(amt) {
		var formattedAmt = amt;
		return (formattedAmt ? Number(formattedAmt).formatAmt() : "");
	}
	//code for number formatting ends here
	//get params by name function starts here
	function getParameterByName(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) { return null; }
		if (!results[2]) { return ''; }
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	$scope.isPetroSaudi = false;
	$scope.taminCptyamoutFilter;// jshint ignore:line
	$scope.tamaxCptyamoutFilter;// jshint ignore:line
	$scope.taminamoutFilter;// jshint ignore:line
	$scope.taminTxFilter;// jshint ignore:line
	$scope.setScenario = "All";
	$scope.applyAnalysisSelect = "Select";
	$scope.isCompareShow = false;
	// Initaizinig model variables
	$scope.model = {
		//			    showBasicInfo: true,
		showOnNode: true,
		showOnEdge: true,
		showTagCloud: true,
		showlocation: true,
		showAdditionalDetails: true,
		showtxBytype: true,
		showdtvstx: true,
		//			    onBasicInfo: function (event) {
		//			        this.showBasicInfo = !this.showBasicInfo;
		//			    },
		onNode: function () {
			this.showOnNode = !this.showOnNode;
		},
		onedge: function () {
			this.showOnEdge = !this.showOnEdge;
		},
		onadditionalDetails: function () {
			this.showAdditionalDetails = !this.showAdditionalDetails;
		},
		onTagCloud: function () {
			this.showTagCloud = !this.showTagCloud;
		},
		onlocation: function () {
			this.showlocation = !this.showlocation;
		},
		ontxBytype: function () {
			this.showtxBytype = !this.showtxBytype;
		},
		onshowdtvstx: function () {
			this.showdtvstx = !this.showdtvstx;
		}
	};
	window.imgPath = "../scripts/VLA/";
	window.global = {
		dimensions: {},
		graphURL: null,
		page: "tI",
		vlaData: {},
		entityID: null,
		expandedNodes: {},
		locations: [],
		compare: '',
		isstatic: false,
		openArticleModal: function (entityID) {
			var text = entityID.data();
			var openRiskOverviewModal = $uibModal.open({
				templateUrl: 'scripts/act/modal/views/riskoverview.modal.html',
				resolve: {
					text: function () {
						return text;
					}
				},
				controller: 'RiskOverViewController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'marketModal-pulse-modal-wrapper',
			});

			openRiskOverviewModal.result.then(function () {

			}, function () {
			});
		}

	};
	$scope.iframeURL = '';
	$scope.iframeURL1 = '';


	var actualBackgrounColors = [];
	window.highlightedTXns = [];
	var vla_inst = {}, summaryURL;
	var locationfilterFromCompare;
	$scope.locations = [];
	$scope.scenarios = [];
	var config_map = {
		"tI": {
			"graph": EHUB_API + "tx/graph/",
			"summary": EHUB_API + "marketIntelligence/entity/search"
		}
	};
	//get params by name function ends here
	/**
	 * Function to get filter query sting
	 */
	function getfilteredQueryString() {
		var filteredQueryString = '';
		if ($scope.taminamoutFilter) {
			filteredQueryString = filteredQueryString + "&minAmount=" + $scope.taminamoutFilter;
		}
		if ($scope.taminTxFilter) {
			filteredQueryString = filteredQueryString + "&minTxs=" + $scope.taminTxFilter;
		}
		if ($scope.taminCptyamoutFilter) {
			filteredQueryString = filteredQueryString + "&counterPartyMin=" + $scope.taminCptyamoutFilter;
		}
		if ($scope.tamaxCptyamoutFilter) {
			filteredQueryString = filteredQueryString + "&counterPartyMax=" + $scope.tamaxCptyamoutFilter;
		}
		if ($scope.setScenario != 'All') {
			filteredQueryString = filteredQueryString + "&transactionType=" + $scope.setScenario;
		}

		// start code for get locations selected
		var locationsArr = [];
		$(".TAlocationClassActive").each(function () {
			locationsArr.push($(this).find("a").text());
		});
		if (locationsArr.length > 0) {
			filteredQueryString = filteredQueryString + "&locations=" + locationsArr.join(",");
		}
		// end code for get locations selected


		return filteredQueryString;
	}

	var widthOfsideMenus = 630;
	var snapShotData = {};
	$scope.locationClass = [];
	$scope.taisDisableCompare = true;

	if (getParameterByName("filters")) {
		$(".utilities-panel").css("display", "none");
		$timeout(function () {
			$(".db-util-background-discover").css("display", "none");
			$('body').addClass("pad-top-10-ta-compare");

		}, 1000);
		$(".resolve-analysis-button").css("display", "none");
		$(".ocbctop-filter-wrapper").css("display", "none");
		$(".main-vla-section").addClass("vlaCompareMain");
		$(".resolve-analysis-wrapper").addClass("resolve-analysis-wrapperCompare");
		widthOfsideMenus = 200;
	}

	var div = d3.select("body").append("div").attr("class", "toolTip_horizontal").style("position", "absolute").style("z-index", 1000).style("background", "rgb(27, 39, 53)").style("padding", "5px 10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");
	$scope.inputDetails = [];
	$scope.outputDetails = [];
	$scope.transactionDetails = [];
	//var WorldData = [], populationData = [];
	function nFormatter(num, digits) {
		var si = [
			{ value: 1E18, symbol: "E" },
			{ value: 1E15, symbol: "P" },
			{ value: 1E12, symbol: "T" },
			{ value: 1E9, symbol: "G" },
			{ value: 1E6, symbol: "M" },
			{ value: 1E3, symbol: "k" }
		], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
		for (i = 0; i < si.length; i++) {
			if (num >= si[i].value) {
				return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
			}
		}
		return num.toFixed(digits).replace(rx, "$1");
	}


	var bombMap = new Datamap({
		element: document.getElementById('worldmap_div'),
		scope: 'world',
		fills: {
			'red': '#884BA2',
			'green': '#54A063',
			'blue': '#2CBCFA',
			'seablue': '#37FEFF',
			defaultFill: '#334851'
		},
		geographyConfig: {
			highlightFillColor: '#1b2735',
			popupOnHover: false,
			highlightBorderOpacity: 0
		},
		bubblesConfig: {
			borderWidth: 0,
			borderOpacity: 0,
			borderColor: 'transparent',

		},
		done: function (datamap) {
			datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
			function redraw() {
				datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
			}
		}

	});
	/*------------------------------------highlight transaction on click of transaction check-box starts  here ---------------------------------*/

	$scope.highlightTx = function (tx, index) {
		var val = $("#txCheck" + index).prop("checked");
		window[vlaId + "cy"].nodes().forEach(function (n) {// jshint ignore:line
			if (n.data().oid == "tx-" + (tx.id)) {
				if (!actualBackgrounColors[tx.id]) {
					actualBackgrounColors[tx.id] = n.style("background-color");
				}
				if (val) {
					if ($.inArray("#txCheck" + index, window.highlightedTXns) == -1) { window.highlightedTXns.push("#txCheck" + index); }
					n.style("background-color", "#4192b7");
				} else {
					var index1 = window.highlightedTXns.indexOf("#txCheck" + index);
					if (index1 > -1) {
						window.highlightedTXns.splice(index1, 1);
					}
					n.style("background-color", actualBackgrounColors[tx.id] ? actualBackgrounColors[tx.id] : "#768A93");
				}
			}
		});
	};
	/*------------------------------------highlight transaction on click of transaction check-box ends  here ---------------------------------*/

	d3.select("#worldmap_div").selectAll(".datamaps-bubble").remove();
	/**
	 * Funciton to plot loactions
	 */
	/*jshint unused:false*/
	var locationsMap = {};
	window.plotCountriesInTransactionAnalysis = function (vlaId) {
		var data = window.global.vlaData[vlaId];
		//			    var mapData = [];
		var locations = [];
		var maplocations = [];

		if (data && data.vertices && data.vertices.length > 0) {		       
			$timeout(function () {
				angular.forEach(data.vertices, function (d) {
					if (d.customer_number == window.global.entityID || d.name == window.global.entityID) {
						window.loadAdverseNewsForSelected(d, "isloading");
					}
				});
			}, 0);
		}
	};
	var tool_tip = $('body').append('<div class="Bubble_Chart_tooltip" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

	/**
	 * Funtoin to plot circles on map
	 */
	function plotCirclesOnMap(bombMap, id, data) {
		// plotting circles on map
		d3.select("#" + id).selectAll(".datamaps-bubble").remove();
		var currentData = [];
		if (data) {
			currentData = data;
		} else {
			return;
		}
		var bombScale = d3.scaleLinear().domain(d3.extent(currentData, function (d) {
			return d.amount;
		})).range([5, 10]);

		if (currentData.length > 0) {

			currentData.map(function (d) {
				d.weight = d.count;
				return (d.radius = bombScale(d.count));
			});


			// circles on world map
			var bombs = currentData;
			// draw bubbles for bombs
			bombMap.bubbles(bombs, {
				//			            popupTemplate: function (geo, d) {
				//			            	return ['<div class="hoverinfo text-uppercase"><div style="color:white;margin-bottom:15px"><h5>' + (d.location) + '</h5></div><div>Amount: '+(parseFloat(d.amount).toFixed(2))+'</div>Transactions: ' + d.count,
				//		                        '</div>'].join('');
				//
				//			            }
			});
			d3.select("#" + id).selectAll(".datamaps-hoverover").remove();
			d3.select("#" + id).selectAll(".datamaps-bubble")
				.on("mouseover", function (d) {
					console.log(d, 'ddd');

					$(this).css("opacity", 1);
					$(".Bubble_Chart_tooltip").html('<div class=" text-uppercase"><div style="color:white;"><h5 style="margin-bottom:5px">' + (d.location) + '</h5></div><div>Amount: ' + (getFormattedAmount(d.amount)) + '</div>Transactions: ' + d.count,
						'</div>');

					return $(".Bubble_Chart_tooltip").css("display", "block");

				})
				.on("mousemove", function () {
					var p = $("#" + id);
					var position = p.offset();
					//var windowWidth=window.innerWidth;
					var tooltipWidth = $(".Bubble_Chart_tooltip").width() + 50;
					var cursor = d3.event.x;
					if ((position.left < d3.event.pageX) && (cursor > tooltipWidth)) {
						var element = document.getElementsByClassName("Bubble_Chart_tooltip");
						element[0].classList.remove("tooltip-left");
						element[0].classList.add("tooltip-right");
						$(".Bubble_Chart_tooltip").css("left", (d3.event.pageX - 15 - $(".Bubble_Chart_tooltip").width()) + "px");
					} else {
						var element = document.getElementsByClassName("Bubble_Chart_tooltip");
						element[0].classList.remove("tooltip-right");
						element[0].classList.add("tooltip-left");
						$(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
					}
					return $(".Bubble_Chart_tooltip").css("top", d3.event.pageY - 20 + "px");

				})
				.on("mouseout", function () {

					$(this).css("opacity", 0.7);
					//hide tool-tip
					return $(".Bubble_Chart_tooltip").css("display", "none");

				});
			d3.select("#" + id).selectAll(".datamaps-bubble").attr("r", function (d) {
				return d.radius;
			}).style("fill-opacity", 0.7);
		}
	}
	/**
	 * Function to toggle location Class
	 */
	$scope.toggleLocationClass = function (id) {
		$scope.locationClass[id] = $scope.locationClass[id] == 'TAlocationClassActive' ? '' : 'TAlocationClassActive';
		$timeout(function () {
			$scope.init('', 'filters');
		}, 0);
	};

	/**
	 * Function to load input output Tx details
	 */
	$scope.getTxDetails = function (queryString) {

		if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
			if (!$scope.TxtypeOptions) {
				$scope.TxtypeOptions = [

					{
						value: "International Transfer",
						displayVal: "International Transfer"
					}, {
						value: "Wired Transfer",
						displayVal: "Wired Transfer"
					}
				];
			}
			//					$scope.setScenario =$scope.setScenario;
			$scope.transactionDetails = filterTxTableForPetroSaudi();
			$scope.transactionDetails.map(function (d) {
				d.txype = d.channel ? d.channel : 'N/A';
				d.txtype2 = d['product-type'];
				d.purpose1 = d['custom-text2'];
				d.purpose2 = d['custom-text4'];
				d.amt = d['amount-base'];
				if (d['debit-credit'] == "C") {
					d.DirectoinCls = "input-tranx";
					d.inputOut = "input";
					if (d.currency != d['currency-bene']) {
						d.amt = parseInt(d['amount-activity-bene']);
					}
					d.counterParty = d['normalized-name-bene'] ? d['normalized-name-bene'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');
				} else {
					d.DirectoinCls = "output-tranx";
					d.inputOut = "outward";

					if (d.currency != d['currency-org']) {
						d.amt = parseInt(d['amount-activity-org']);
					}
					d.counterParty = d['normalized-name-org'] ? d['normalized-name-org'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');

				}
				d.namebene = d['name-bene'] ? d['name-bene'] : 'N/A';
				d.currencybene = d['currency-bene'] ? d['currency-bene'] : 'N/A';
				d.countrybene = d['country-bene'] ? d['country-bene'] : 'N/A';
				d.activity_amount_bene = d['amount-activity-bene'] ? parseFloat(d['amount-activity-bene']) : 'N/A';
				d.nameorg = d['name-org'] ? d['name-org'] : 'N/A';
				d.countryorg = d['country-org'] ? d['country-org'] : 'N/A';
				d.activity_amount_org = d['amount-activity-org'] ? parseFloat(d['amount-activity-org']) : 'N/A';
				d.currencyorg = d['currency-org'] ? d['currency-org'] : 'N/A';
			});

			$(".txcheckCls").prop("checked", false);
			loadOtherScenarioCharts();
			$timeout(function () {
				angular.forEach(window.highlightedTXns, function (d) {
					$(d).click();
				});
			}, 1000);
			return;
		}
		var filteredQueryString = '';

		if ($scope.taminamoutFilter) {
			filteredQueryString = filteredQueryString + "&minAmount=" + $scope.taminamoutFilter;
		}
		if ($scope.taminTxFilter) {
			filteredQueryString = filteredQueryString + "&minTxs=" + $scope.taminTxFilter;
		}
		if ($scope.taminCptyamoutFilter) {
			filteredQueryString = filteredQueryString + "&counterPartyMin=" + $scope.taminCptyamoutFilter;
		}
		if ($scope.tamaxCptyamoutFilter) {
			filteredQueryString = filteredQueryString + "&counterPartyMax=" + $scope.tamaxCptyamoutFilter;
		}
		if ($scope.setScenario != 'All') {
			filteredQueryString = filteredQueryString + "&transactionType=" + $scope.setScenario;
		}
		/* start code for get locations selected */
		var locationsArr = [];
		$(".TAlocationClassActive").each(function () {
			locationsArr.push($(this).find("a").text());
		});
		if (locationsArr.length > 0) {
			filteredQueryString = filteredQueryString + "&locations=" + locationsArr.join(",");
		}
		var params = {
			queryString: queryString,
			filteredQueryString: filteredQueryString
		};
		params.filteredQueryString = params.filteredQueryString + "&alertTransactionId=" + getParameterByName('tId');

		var token = {
			token: $rootScope.ehubObject.token
		};
		TransactionAnalysisApiService.getTransactionsListForSelectedEntites(params, token).then(function (response) {
			if (!response.data.listTxDtos || response.data.listTxDtos.length == 0) {
				$scope.transactionDetails = [];
				return;
			}
			var r = response.data.listTxDtos;
			$timeout(function () {
				//move the main transaction to top
				var index = response.data.listTxDtos.map(function (e) { return e['is-alerted-transaction']; }).indexOf(true);

				if (index > -1) {
					var current_tx = response.data.listTxDtos[index];
					response.data.listTxDtos.splice(index, 1);
					response.data.listTxDtos.unshift(current_tx);
				}
				//move the main transaction to top
				$scope.transactionDetails = response.data.listTxDtos;

				$scope.transactionDetails.map(function (d) {
					d.txype = d.channel ? d.channel : 'N/A';
					d.txtype2 = d['product-type'];
					d.purpose1 = d['custom-text2'];
					d.purpose2 = d['custom-text4'];
					d.amt = d['amount-base'];
					if (d['debit-credit'] == "D") {
						d.DirectoinCls = "output-tranx";
						d.inputOut = "outward";
						if (d.currency != d['currency-bene']) {
							d.amt = parseInt(d['amount-activity-bene']);
						}
						d.counterParty = d['normalized-name-bene'] ? d['normalized-name-bene'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');
					} else {
						d.DirectoinCls = "input-tranx";
						d.inputOut = "input";

						if (d.currency != d['currency-org']) {
							d.amt = parseInt(d['amount-activity-org']);
						}
						d.counterParty = d['normalized-name-org'] ? d['normalized-name-org'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');

					}
					d.namebene = d['name-bene'] ? d['name-bene'] : 'N/A';
					d.currencybene = d['currency-bene'] ? d['currency-bene'] : 'N/A';
					d.countrybene = d['country-bene'] ? d['country-bene'] : 'N/A';
					d.activity_amount_bene = d['amount-activity-bene'] ? parseFloat(d['amount-activity-bene']) : 'N/A';
					d.nameorg = d['name-org'] ? d['name-org'] : 'N/A';
					d.countryorg = d['country-org'] ? d['country-org'] : 'N/A';
					d.activity_amount_org = d['amount-activity-org'] ? parseFloat(d['amount-activity-org']) : 'N/A';
					d.currencyorg = d['currency-org'] ? d['currency-org'] : 'N/A';
				});
				// get values for transaction type filter

				if (!$scope.TxtypeOptions) {
					var nested_tx_data = d3.nest().key(function (d) { return d.txype; }).entries($scope.transactionDetails);
					$scope.TxtypeOptions = [];
					angular.forEach(nested_tx_data, function (d) {
						if (d.key && d.key != "-") {
							$scope.TxtypeOptions.push({
								value: d.key,
								displayVal: d.key.split("-").join(" ")
							});
						}
					});
				}
				$(".txcheckCls").prop("checked", false);
				loadOtherScenarioCharts();
				$timeout(function () {
					angular.forEach(window.highlightedTXns, function (d) {
						$(d).click();
					});
				}, 1000);
			}, 0);

		}, function () {

		});
	};
	/**
	 * Function to load Tx details
	 */
	$scope.getInputOutputTxDetails = function (queryString, id) {
		if (id == 'vla' || id == '#vla') {
			if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
				var r = inputDetaislPetro;
				$scope.inputDetails = inputDetaislPetro;
				var maxcpty = d3.max($scope.inputDetails.counterparty, function (d) {
					return d.sum;
				});
				var maxcontryVal = d3.max($scope.inputDetails.country, function (d) {
					return d.sum;
				});
				$scope.inputDetails.counterparty.map(function (d) {
					d.sumformatted = nFormatter(d.sum, 0);
					d.sumRatio = (d.sum / maxcpty) * 100;
				});
				$scope.inputDetails.country.map(function (d) {
					d.sumformatted = nFormatter(d.sum, 0);
					d.sumRatio = (d.sum / maxcontryVal) * 100;
				});
				inputDetails = r;

				outPutDetails = outPutDetaislPetro;
				$scope.outputDetails = outPutDetaislPetro;
				var maxcpty = d3.max($scope.outputDetails.counterparty, function (d) {
					return d.sum;
				});
				var maxcontryVal = d3.max($scope.outputDetails.country, function (d) {
					return d.sum;
				});
				$scope.outputDetails.counterparty.map(function (d) {
					d.sumformatted = nFormatter(d.sum, 0);
					d.sumRatio = (d.sum / maxcpty) * 100;
				});
				$scope.outputDetails.country.map(function (d) {
					d.sumformatted = nFormatter(d.sum, 0);
					d.sumRatio = (d.sum / maxcontryVal) * 100;
				});
				plotInputOutPie(inputDetails, outPutDetails, id);
			} else {
				/* load input stats */
				var flagForReqComplete = 0;
				var inputDetails, outPutDetails;
				var filteredQueryString = '';

				if ($scope.taminamoutFilter) {
					filteredQueryString = filteredQueryString + "&minAmount=" + $scope.taminamoutFilter;
				}
				if ($scope.taminTxFilter) {
					filteredQueryString = filteredQueryString + "&minTxs=" + $scope.taminTxFilter;
				}
				if ($scope.taminCptyamoutFilter) {
					filteredQueryString = filteredQueryString + "&counterPartyMin=" + $scope.taminCptyamoutFilter;
				}
				if ($scope.tamaxCptyamoutFilter) {
					filteredQueryString = filteredQueryString + "&counterPartyMax=" + $scope.tamaxCptyamoutFilter;
				}
				if ($scope.setScenario != 'All') {
					filteredQueryString = filteredQueryString + "&transactionType=" + $scope.setScenario;
				}
				/* start code for get locations selected */
				var locationsArr = [];
				$(".TAlocationClassActive").each(function () {
					locationsArr.push($(this).find("a").text());
				});
				if (locationsArr.length > 0) {
					filteredQueryString = filteredQueryString + "&locations=" + locationsArr.join(",");
				}
				var params = {
					queryString: queryString,
					filteredQueryString: filteredQueryString
				};
				var token = {
					token: $rootScope.ehubObject.token
				};
				TransactionAnalysisApiService.getTransactionInputsAggregates(params, token).then(function (response) {
					var r = response.data;
					$scope.inputDetails = response.data;
					if (!$scope.inputDetails.country) {
						$scope.inputDetails.country = [];
					}
					if (!$scope.inputDetails.counterparty) {
						$scope.inputDetails.counterparty = [];
					}
					var maxcpty = d3.max($scope.inputDetails.counterparty, function (d) {
						return d.sum;
					});

					var maxcontryVal = d3.max($scope.inputDetails.country, function (d) {
						return d.sum;
					});
					$scope.inputDetails.counterparty.map(function (d) {
						d.sumformatted = nFormatter(d.sum, 0);
						d.sumRatio = (d.sum / maxcpty) * 100;
					});
					$scope.inputDetails.country.map(function (d) {
						d.sumformatted = nFormatter(d.sum, 0);
						d.sumRatio = (d.sum / maxcontryVal) * 100;
					});
					inputDetails = r;
					flagForReqComplete++;
					if (flagForReqComplete == 2) {
						plotInputOutPie(inputDetails, outPutDetails, id);
					}
				}, function () {

				});
				/* load output stats */
				TransactionAnalysisApiService.getTransactionOutputsAggregates(params, token).then(function (response) {
					outPutDetails = response.data;
					$scope.outputDetails = response.data;
					if (!$scope.outputDetails.country) {
						$scope.outputDetails.country = [];
					}
					if (!$scope.outputDetails.counterparty) {
						$scope.outputDetails.counterparty = [];
					}
					var maxcpty = d3.max($scope.outputDetails.counterparty, function (d) {
						return d.sum;
					});
					var maxcontryVal = d3.max($scope.outputDetails.country, function (d) {
						return d.sum;
					});
					$scope.outputDetails.counterparty.map(function (d) {
						d.sumformatted = nFormatter(d.sum, 0);
						d.sumRatio = (d.sum / maxcpty) * 100;
					});
					$scope.outputDetails.country.map(function (d) {
						d.sumformatted = nFormatter(d.sum, 0);
						d.sumRatio = (d.sum / maxcontryVal) * 100;
					});
					flagForReqComplete++;
					if (flagForReqComplete == 2) {
						plotInputOutPie(inputDetails, outPutDetails, id);
					}
				}, function () {

				});
			}
		} else {
			if (id == '#vlaCompare1' || id == "vlaCompare1") {
				plotInputOutPie($scope.snapShotData.inputDetails, $scope.snapShotData.outputDetails, id);
			} else if (id == '#vlaCompare2' || id == "vlaCompare2") {
				plotInputOutPie($scope.dataBythetimeofCompare.inputDetails, $scope.dataBythetimeofCompare.outputDetails, id);
			}
		}
	};
	/**
	 * Function to plot inputout donut
	 */
	function plotInputOutPie(inputDetails, outPutDetails, id) {

		if (!id || id == "vla" || id == "transactionIO") {
			handleDataToplotcountriesOnMap(inputDetails.country, outPutDetails.country);
			id = "transactionIO";
		}

		if (id == 'vlaCompare1') {
			$("#transactionIOCompare1").empty();
		} else if (id == 'vlaCompare2') {
			$("#transactionIOCompare2").empty();
		} else {
			$("#transactionIO").empty();
		}
		var piedata = [];
		if (!outPutDetails.total) {
			outPutDetails.total = {};
			outPutDetails.total.sum = 0;
		} else if (!outPutDetails.total.sum) {
			outPutDetails.total.sum = 0;
		}
		if (!inputDetails.total) {
			inputDetails.total = {};
			inputDetails.total.sum = 0;
		} else if (!inputDetails.total.sum) {
			inputDetails.total.sum = 0;
		}
		var maxVal = d3.max([inputDetails.total.sum, outPutDetails.total.sum]);
		piedata.push({
			key: "Input",
			val: inputDetails.total.sum
		});
		piedata.push({
			key: "Output",
			val: outPutDetails.total.sum
		});

		var colors = ["#3A9035", "#A85CC1"];
		if (maxVal) {
			var txt = nFormatter(maxVal, 1);
			$scope.InitializeandPlotPie(piedata, id, colors, "", txt, "true", "", "30", "160");
		}
	}
	/**
	 * Function to
	 */
	function handleDataToplotcountriesOnMap(inputCountryDetails, outputCountryDetails) {

		var mapData = [];
		$.each(inputCountryDetails, function (i, d) {
			if ($.inArray(d.country.country, $scope.locations) == -1) {
				$scope.locations.push(d.country.country);
			}
			mapData.push({
				"latitude": d.country.latitude,
				"longitude": d.country.longitude,
				"location": d.country.country,
				"count": d.count,
				"amount": d.sum,
				"fillKey": "green"
			});
		});
		$.each(outputCountryDetails, function (i, d) {
			if ($.inArray(d.country.country, $scope.locations) == -1) {
				$scope.locations.push(d.country.country);
			}
			mapData.push({
				"latitude": d.country.latitude,
				"longitude": d.country.longitude,
				"location": d.country.country,
				"count": d.count,
				"amount": d.sum,
				"fillKey": "red"
			});
		});
		plotCirclesOnMap(bombMap, "worldmap_div", mapData);
	}
	/**
	 * Function to load other sceanrio chart
	 */
	function loadOtherScenarioCharts() {
		//load transactions by type of transaction bar 
		var dataTxByTypeData = [];
		var dataTxByTypeData = d3.nest().key(function (d) { return d.txype; })
			.rollup(function (d) {
				return d3.sum(d, function (d1) { return d1.amt; });
			}).entries($scope.transactionDetails);
		var txValMax = d3.max(dataTxByTypeData, function (d) {
			return parseFloat(d.value);
		});
		dataTxByTypeData.map(function (d) {
			d.valluePct = (d.value / txValMax) * 100;
			d.valPctInt = parseInt((d.value / txValMax) * 100);
			d.time = d.key;
			d.type = "transactions";
			d.txt = d.key + ': $' + getFormattedAmount(d.value);
			return (d.y = d.value);
		});
		$scope.dataTxByTypeData = dataTxByTypeData;
		var dataTxByDateData = d3.nest().key(function (d) { return d.date; })
			.rollup(function (d) {
				return d3.sum(d, function (d1) { return d1.amt; });
			}).entries($scope.transactionDetails);

		var sum = d3.sum(dataTxByDateData, function (d) { return d.value; });
		var avg = (sum / (dataTxByDateData.length));
		dataTxByDateData.map(function (d) {
			d.key = parseInt(d.key);
			d.date = new Date(d.key);
			d.x = new Date(d.key);
			d.average = avg;
			d.deviation = parseFloat(((d.value - avg) / (d.value)) * 100).toFixed(2);
			return (d.time = new Date(d.key));
		});

		$scope.dataTxByDateData = dataTxByDateData;
		plotLineChart(dataTxByDateData, "#showdtvstx_div", $("#showdtvstx_div").width(), '180', avg);

	}
	/**
	 * Function to load sceanrio chart
	 */
	function loadScenarioChart(maxVal, peakAvg) {

		//				 //code for transaction scenario chart
		//				var ratioPercent=0;
		//				if(peakAvg =="average"){
		//					ratioPercent =120; 
		//				}else{
		//					ratioPercent =125; 
		//				}
		//				var colorObj ={
		//						"Estimated turnover according to KYC questionnaire":"rgba(201, 212, 37, 0.53)",	
		//						"Acceptable amount of turnover based on the AML risk score":"#69ca6b",
		//						"Factual amount of turnover":"#E32646",
		//				}
		//				 var txScenarioData =[{
		//						"label":"Estimated Amount",
		//						"y":maxVal*0.5,
		//						"cls":"estimated",
		//						type:"sceanrio",
		//						"txt":"Estimated turnover according to KYC questionnaire Amount:" +getFormattedAmount(maxVal*0.5)
		//					},{
		//						"label":"Acceptable Amount",
		//						"y":((maxVal*0.5)*ratioPercent)/100,
		//						"cls":"acceptable",
		//						type:"sceanrio",
		//						"txt":"Acceptable amount of turnover based on the AML risk score Amount:" +getFormattedAmount(((maxVal*0.5)*ratioPercent)/100)
		//					},{
		//						"label":"Factual Amount",
		//						"y":maxVal,
		//						"cls":"factual",
		//						type:"sceanrio",
		//						"txt":"Factual amount of turnover and Amount:" +getFormattedAmount(maxVal)
		//					}];
		//				var txMaxVal  = d3.max(txScenarioData,function(d){return d.y;});
		//				txScenarioData.map(function(d){
		//					d.valpct = (d.y/txMaxVal)*100;
		//					return d.valPctInt = parseInt( (d.y/txMaxVal)*100);
		//				});
		//				$scope.activityScenario = txScenarioData;
		//				console.log($scope.activityScenario);
		//				$scope.txScenarioData ={
		//						"estimated":getFormattedAmount(maxVal*0.5),
		//						"acceptable":getFormattedAmount(((maxVal*0.5)*ratioPercent)/100),
		//						"actual":getFormattedAmount(maxVal)
		//				};
		//				console.log(maxVal);
		//			    var data=[];
		//			    data[0] = txScenarioData;
		//			    var options={
		//		         		container:"#transactionScenario",
		//		         		width:$("#transactionScenario").width(),
		//		         		height:"180",
		//		         		data:data,
		//		         		marginTop:5,
		//		         		marginBottom:5,
		//		         		marginRight:5,
		//		         		marginLeft:5,
		//		         		tickColor:"rgb(51, 69, 81)",
		//		         		axisX:false,
		//		         		colors:["rgba(201, 212, 37, 0.53)"],
		//		         		rectrad:5,
		//		         		colorObj:colorObj
		//				}

		//			 stackedbarTimelinechart(options);
		//			    $timeout(function(){
		//			    	 d3.select(options.container).selectAll("rect").attr("rx","5").attr("ry",5)
		//			    },100);


	}

	$timeout(function () {
		$scope.token = $rootScope.ehubObject.token;
		var queryString = getParameterByName('q');
		var pageString = "tI";
		var type = getParameterByName('Etype');
		var entityId = getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1 ? "petroSaudi" : getParameterByName('entityID');
		window.global.queryString = queryString;
		window.global.entityID = entityId;
		global.dimensions.width = $(window).width() - widthOfsideMenus;
		global.dimensions.height = 450;
		global.dimensions.left = "0";
		global.dimensions.top = "0";
		var data = {
			"fetchers": ["1013", "1006", "1008", "1021"],
			"keyword": "",
			"searchType": "Company",
			"lightWeight": true,
			"saveGraph": true
		};
		// -----------------------------------------------------------
		/**
		 * Funciton to get entity name for adverse news
		 */
		function getEntityName(entityDetails) {
			$.ajax({
				method: 'GET',
				url: url,// jshint ignore:line

				contentType: "application/json; charset=utf-8",
				success: function (response) {
					return response.entityName;
				},
				error: function () {
					$('.custom-spinner-main').hide();
					return false;
				}
			});
		}
		// -----------------------------------------------------------
		/**
		 * Function to hide node with context menu
		 * 
		 */
		window.showHideTransactionAnalysisNode = function (entityDetails, type) {

			var url = EHUB_API + "transactionAnalysis/graph/edit/" + type + "?id=" + encodeURIComponent(entityDetails.id) + "&token=" + $rootScope.ehubObject.token;
			$.ajax({
				method: 'POST',
				url: url,
				contentType: "application/json; charset=utf-8",
				success: function (response) {

					if (type == "unhide" && response > 0) {
						$scope.init("", "filters");
					}
				},
				error: function () {
					$('.custom-spinner-main').hide();
					$('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>Something Went wrong. please try again later..</div>");

					return false;
				}
			});
			//				    var param = "id="+ encodeURIComponent(entityDetails.id)+"&token="+$rootScope.ehubObject.token;
			//			        TransactionAnalysisApiService.showRemoveVertex(param, type).then(function(response){
			//			        	if (type == "unhide" && response.data.vertices.length == 0) {
			//		                    $scope.init("", "filters");
			//		                }
			//				    }, function(error){
			//				    	$('.custom-spinner').hide();
			//		                $('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>Something Went wrong. please try again later..</div>");
			//		                return false;
			//				    });
			return false;
		};
		// -----------------------------------------------------------
		/**
		 * Function to load adverse news with context menu
		 * 
		 */
		/*jshint unused:false*/
		window.loadAdverseNewsForSelected = function (entityDetails, isloading) {

			if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
				$('#displayadditionalDetails').css('display', 'block');

				var summary = {
					"article-references": newsPetro.length,
					articles: newsPetro

				};
				var html = '';
				html = html + '<div class="pulse-entities text-uppercase"><ul class="list-inline text-center"><li><h6 style="display:inline" class="key">Article References: </h6><span >' + summary['article-references'] + '</span></li></ul></div>';
				// add direct article connections
				html = html + '<div class="pulse-relations directRelationEntities"><h5> Articles</h5><ul class="list-unstyled">';
				$.each(summary.articles, function (i, val) {
					html = html + '<li><a target="_blank" title="' + val.text + '" href="' + val.url + '"><img src="../scripts/VLA/KeylineICONS/Article.svg" width="15" height="15">' + val.text + '</a></li>';
				});
				html = html + '</ul></div>';

				$('#entityDetails').html(html);
				return;
			}
			$('#displayadditionalDetails').css('display', 'block');
			$('.custom-spinner-main').show();

			var type = "organization";
			if (!entityDetails.type) {
				entityDetails.type = entityDetails._type;
			}
			if (!entityDetails.type) {
				return false;
			}
			if (entityDetails.type.toUpperCase() == "IND") {
				type = "person";
			}
			//			        var url = EHUB_API + "entity/search/" + (type == "organization" ? "org" : "person") + "?query=" + entityDetails['search-name']+"&token=" + $rootScope.ehubObject.token;
			var type = type == "organization" ? "org" : "person";
			var params = {
				query: entityDetails['search-name'],
				token: $rootScope.ehubObject.token
			};
			var summaryURL = config_map[pageString].summary + "?entity=" + entityDetails['search-name'] + "&token=" + $rootScope.ehubObject.token;
			mainEntityDetails(summaryURL, isloading, entityDetails['search-name']);
			//			        TransactionAnalysisApiService.getEntitySearch(params, type).then(function(response){
			//				    	if (response.data && response.data.orgs && response.data.orgs.length > 0) {
			//		                    var entityName = response.data.orgs[0]['_identifier'];
			//		                    var summaryURL = config_map[pageString].summary + type + "/" + entityName+"?token=" + $rootScope.ehubObject.token;;
			//		 					mainEntityDetails(summaryURL,isloading)
			//		                } else {
			//		                    $('.custom-spinner').hide();
			//		                    $('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>No Information available</div>");
			//		                }
			//				    }, function(error){
			//				    	$('.custom-spinner').hide();
			//		                $('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>Something Went wrong. please try again later..</div>");
			//		                return false;
			//				    });

		};
		/* Check caseId is Number */
		function is_numeric(caseId) {
			return !isNaN(parseFloat(caseId)) && isFinite(caseId);
		}
		$scope.init = function (value, filters, id, appliedfilters, current_data) {
			if (!id) {
				id = "vla";
			}

			if (filters) {
				window.global.vlaData["#" + id] = null;
				window.selectedScenario = $scope.setScenario;
			} else {
				getBasicInfo();
				getScenarioDetails();
				getCustomerAlerts();
			}
			if (current_data) {
				window.global.vlaData["#" + id] = current_data;
			}

			$("#" + id).empty();

			$("#collapseOne").find(".vla-selection-panel").remove();
			if (queryString) {

				$('#displayadditionalDetails').css('display', 'none');
				$('.custom-spinner-main').hide();
			}
			if (!pageString && !queryString) {
				// show error
				$("#vlaerror").html("Please provide query string to load data");
				$('.custom-spinner-main').hide();
				return false;
			} else if (queryString && pageString) {
				// plot for the curent page
				if (!config_map[pageString]) {
					// show error
					$("#vlaerror").html("Please Provide valid Page");
					$('.custom-spinner-main').hide();
					return false;
				} else if (pageString == "tI" && !type) {
					// show error that adverse news needs an type
					$("#vlaerror").html("Please Provide valid type for entity");
					$('.custom-spinner-main').hide();
					return false;
				} else {
					if (pageString == "tI") {
						// $('#displayadditionalDetails').css('display','block');
						var filteredQueryString = getfilteredQueryString();
						if (appliedfilters) {
							filteredQueryString = appliedfilters;
						}

						if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
							window.global.graphURL = "../vendor/data/petorTI.json";
							window.global.isstatic = "true";
							$scope.isPetroSaudi = true;
						} else if (current_data) {
							window.global.graphURL = "../vendor/data/tiCompareDataStructure.json";
						} else { window.global.graphURL = config_map[pageString].graph + queryString + "/alert?" + filteredQueryString + "&token=" + $rootScope.ehubObject.token; }

						var vla_options = { "target_html_element": id, "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": { "type": "json", "caseId": "", "limit": 100, "queryParams": true } };
						if (current_data) {
							vla_options.data = current_data;
						}
						if (typeof custom_config != 'undefined') { vla_options = $.extend({}, custom_config, vla_options); }// jshint ignore:line
						//			                    vla_inst[id] = new VLA.Main(vla_options);
						vla_options.adverseNews = "adversenews";
						vla_inst[id] = PlotVLA(vla_options);
						$scope.getInputOutputTxDetails(entityId, id);
						if (id != "vlaCompare1" && id != "vlaCompare2") { $scope.getTxDetails(entityId, id); }

						$("#currenttransName").text(getParameterByName('searchName') ? getParameterByName('searchName').split("%20").join(" ") : entityId);

					}
				}
			}
			/**
			 * Function to fetch all expanded nodes data 
			 */
			window.loadExpandedNodeDetails = function (id, currentIds) {
				if (!currentIds) { currentIds = window.global.expandedNodes[id]; }

				angular.forEach(currentIds, function (d) {
					window.getGraphIDWithAlert(d, id);
				});
			};
			/**
			 * Function to get expand data
			 */
			window.getTxInputOutput = function (entityids, id) {
				if (id == "#vla") {
					$scope.getInputOutputTxDetails(entityids, id);
					$scope.getTxDetails(entityids);
				}
			};
			/**
			 * Function to get actual id for graph expand data
			 */
			window.getGraphIDWithAlert = function (vertexid, id) {
				if (!id) {
					id = "#vla";
				}

				var filteredQueryString = getfilteredQueryString();

				$("#collapseOne").find(".vla-selection-panel").remove();
				if (filteredQueryString) {
					window.global.graphURL = config_map[pageString].graph + (vertexid) + "/expand/" + getParameterByName('q') + "?" + filteredQueryString + "&token=" + $rootScope.ehubObject.token;
				} else {
					window.global.graphURL = config_map[pageString].graph + (vertexid) + "/expand/" + getParameterByName('q') + "?&token=" + $rootScope.ehubObject.token;

				}
				if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
					window.global.graphURL = "../vendor/data/petorTI.json";
				}
				if (id == "#vla") {
					$("#vla").empty();
					global.dimensions.width = $(window).width() - widthOfsideMenus;
					global.dimensions.height = 450;
					global.dimensions.left = "0";
					global.dimensions.top = "0";
				}
				//			            else if (id=="#vlaCompare1" || id=="#vlaCompare2"){
				//		            	 $(id).empty();
				//		            	 global.dimensions.width = $(window).width() - widthOfsideMenus;
				//			  			    global.dimensions.height = 450;
				//			  			    global.dimensions.left = "0";
				//			  			    global.dimensions.top = "0";
				//		            }
				else {
					$(id).empty();
				}

				var vla_options = { "target_html_element": id.split("#")[1], "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": { "type": "json", "caseId": "", "limit": 100, "queryParams": true } };
				if (typeof custom_config != 'undefined') { vla_options = $.extend({}, custom_config, vla_options); }// jshint ignore:line
				$(id).siblings(".custom-spinner").show();
				vla_inst[id] = PlotVLA(vla_options);
				//			            if(id != "vlaCompare1" && id !="vlaCompare2"){
				//				        	$(".txcheckCls").prop("checked",false);
				//				            $timeout(function(){
				//						    	angular.forEach(window.highlightedTXns,function(d,i){
				//									$(d).click();
				//								});
				//						    },1000)
				//			            }

			};

			if (value != void 0) { data = value; }
			$('.custom-spinner-main').css('display', 'block');
			$('.mainsidewrapper').addClass('sidebar-wrapper-main');
			function getBasicInfo() {
				if (getParameterByName('q').toLowerCase().indexOf("petrosaudi") > -1 && getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1) {
					var txt = "Petro Saudi International is a privately owned oil exploration and production company, with its main offices in the United Kingdom, Saudi Arabia and Switzerland. Founded in 2005, it is paving the way for the future of energy industry standards through compelling multinational projects on the ground and in the marketplace.";
					$('.basic-header span').html("LARGE AMOUNT INTERNATIONAL TRANSFER TO OFFSHORE JURISDICTION");
					$('.basic-content p').html(txt);
					return;
				}
				var token = {
					token: $rootScope.ehubObject.token
				};
				TransactionAnalysisApiService.searchResolve(JSON.stringify(data), token).then(function (response) {
					//					    	var response = response.data;
					var foundData = false;
					if (response !== void 0) {
						var resolvedResponse = [], resolvedName = '', parentIndex = -1, childIndex = -1;

						if (response.data.resolved.length > 0 && response.data.resolved[0].entities[0].name) { resolvedResponse = response.data.resolved; }
						else if (response.data.sortedResults.length > 0) {
							resolvedName = response.data.sortedResults[0].name;
							response.data.sortedResults[0].fetcher === 'wikipedia.com' ? parentIndex = 0 :
								response.data.sortedResults[0].fetcher === 'bloomberg.com' ? parentIndex = 1 :
									response.data.sortedResults[0].fetcher === 'company.linkedin.com' ? parentIndex = 2 : parentIndex = 3;// jshint ignore:line
							childIndex = response.data.sortedResults[0].index;
							resolvedResponse.push(response.data.results[parentIndex]);
						} else { resolvedResponse = response.data.results; }
						resolvedResponse.forEach(function (entity) {
							if (entity.status) {
								entity.entities.forEach(function (entityID) {
									if (entityID.name !== '' && entityID.name !== void 0 && !foundData) {
										foundData = true;
										window.entityName = entityID.name;
										$('.basic-header span').html(entityID.name);
										if (entityID.properties.details != void 0) {
											window.count = entityID.properties.details;
											if (count) {
												var count1 = count.substring(0, 300);
												$('.basic-content p').html(count1 + '<a onClick="openModal();" style="font-size:10px;" title="Read More"> READ MORE</a>');
											}
										} else {
											count = entityID.properties.summary;
											if (count) {
												var count1 = count.substring(0, 300);
												$('.basic-content p').html(count1 + '<a onClick="openModal();" href="javascript:void(0);" title="Read More"> READ MORE</a>');
											}
										}
										var category = entityID.properties.business_type === undefined ? entityID.properties.organization_name : entityID.properties.business_type;
										$('ul.basicInfo').html('<li>ID: ' + entityID.entityID + '</li><li>TYPE: ' + entityID.entityType + '</li><li>CATEGORY: ' + category + '</li><li style="color:#E32646;">RISK: ' + entityID.riskScore + '</li>');
									}
								});
							}
						});
					}
				}, function () {
					$('ul.basicInfo').html('<div class="">The server is busy, please try after sometime.</div>');
					$('.custom-spinner-main').hide();
				});

			}
		};

		if (queryString == '') { return; }

		data.keyword = getParameterByName('searchName') ? getParameterByName('searchName') : entityId;

		$scope.init();


		/*-- Side bar height ----*/
		$(function () {
			$('.mainsidewrapper').css({ 'max-height': (($(window).height()) - 160) + 'px' });
			$(window).resize(function () {
				$('.mainsidewrapper').css({ 'max-height': (($(window).height()) - 160) + 'px' });
			});
		});

		/*--  Custom ScrollBar for Sidebar Elements  --*/
		$(".selectric").on("click", function () {
			$(this).parents(".selectric-wrapper").find(".selectric-scroll").mCustomScrollbar();
		});
		/* entityDetails */
		function mainEntityDetails(summaryURL, isloading, searchName) {
			$('#displayadditionalDetails').css('display', 'block');
			if (summaryURL) {
				EntityApiService.getIdentifier(searchName).then(function (response1) {
					if (response1.data && response1.data.hits.length > 0) {
						EntityApiService.getNewsByEntityTypeID('', response1.data.hits[0]['@identifier']).then(function (response) {
							$('.custom-spinner-main').hide();
							if (response.data) {
								var r = response.data;
								if (!r.news || r.news.length == 0) {
									$('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>No Information available</div>");
									return;
								}
								var summary = {
									"article-references": (r.news).length,
									articles: r.news
								};
								var html = '';
								html = html + '<div class="pulse-entities text-uppercase"><ul class="list-inline text-center"><li><h6 style="display:inline" class="key">Article References: </h6><span >' + summary['article-references'] + '</span></li></ul></div>';
								// add direct article connections
								html = html + '<div class="pulse-relations directRelationEntities"><h5> Articles</h5><ul class="list-unstyled">';
								$.each(summary.articles, function (i, val) {
									html = html + '<li><a target="_blank" title="' + val.text + '" href="' + val.url + '"><img src="../scripts/VLA/KeylineICONS/Article.svg" width="15" height="15">' + val.title + '</a></li>';
								});
								html = html + '</ul></div>';

								$('#entityDetails').html(html);
								if (!isloading) {
									loadArticleVisualizerModal(response.data.news);
								}
							} else {
								$('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>No Information available</div>");
							}
						}, function (error) {
							$('.custom-spinner-main').hide();
							$('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>No Information available</div>");
						});
					}
				}, function (error) {
					$('.custom-spinner-main').hide();
					$('#entityDetails').html("<div style='color:#6B7E8C' class='text-uppercase'>No Information available</div>");
				});
			}
		}
	}, 0);
	/**
	 * Function to load article visualizer modal
	 */
	function loadArticleVisualizerModal(articles) {
		var articleVisualizerModalInstance = $uibModal.open({
			templateUrl: './modal/views/articleVisualizer.modal.html',
			controller: 'ArticleVisualizerController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal full-analysis-wrapper',
			resolve: {
				text: function () {
					return articles[0].text;
				},
				articles: function () {
					return articles;
				},
				header: function () {
					return articles[0].title;
				}
			}
		});

		articleVisualizerModalInstance.result.then(function () {
		}, function () {
		});
	}
	/**
	 * Function to plot pie
	 */
	$scope.InitializeandPlotPie = function (data, id, colors, ispadding, istxt, islegends, islegendleft, legendwidth, height) {
		if (id == 'vlaCompare1') {
			id = "transactionIOCompare1";
		} else if (id == 'vlaCompare2') {
			id = "transactionIOCompare2";
		} else {
			id = "transactionIO";
		}

		$("#" + id).empty();
		data.map(function (d) {
			return (d.value = d.doc_count ? d.doc_count : d.val);
		});
		var newData = [];
		var keys = [];
		angular.forEach(data, function (d) {
			if (d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(), keys) == -1) {
				newData.push(d);
				keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
			}
		});
		var maxval = d3.max(newData, function (d) {
			return d.val;
		});
		var options = {
			container: "#" + id,
			data: newData,
			height: height ? height : 140,
			maxval: maxval,
			colors: colors,
			ispadding: ispadding,
			istxt: istxt,
			islegends: islegends,
			islegendleft: islegendleft,
			legendwidth: legendwidth
		};
		setTimeout(function () {
			new reusablePie(options);
		});
	};
	/*------------------------------------- pie code ends here -----------------------------------*/
	/*------------------------------------ Modal code starts here ---------------------------------*/
	$scope.loadFullVLA = function () {

		var fullAnalysisModalInstance = $uibModal.open({
			templateUrl: './modal/views/fullAnalysis.modal.html',
			controller: 'fullAnalysisModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal full-analysis-wrapper',
			resolve: {
				fullAnalysisData: function () {
					return window.global.graphURL;
				},
				fullTranxData: function () {
					return $scope.transactionDetails;
				},
				scenario: function () {
					return $scope.setScenario;
				}
			}
		});

		fullAnalysisModalInstance.result.then(function () {
		}, function () {
		});
	};
	// ---------------------------------------- Modal code ends here ---------------------------------*/

	/*------------------------------------ Modal code starts here ---------------------------------*/
	$scope.showResolveAnalysis = function (str) {
		var strcls = '';
		if (str) {
			strcls = "transactionSTR";
		}
		var resolveTranxModalInstance = $uibModal.open({
			templateUrl: './modal/views/resolveAnalysis.modal.html',
			controller: 'ResolveTranxModalController',
			size: 'sm',
			backdrop: 'static',
			windowClass: 'custom-modal mip-save-modal mip-save-wrapper ' + strcls,
			resolve: {
				rsolveAnlysisData: function () {
					return [getParameterByName('q'), window.global.isstatic, str];
				}
			}
		});

		resolveTranxModalInstance.result.then(function () {

			HostPathService.FlashSuccessMessage('Resolved Successfully', '');

		}, function (error) {
			if (error == "error") { HostPathService.FlashErrorMessage('Something went wrong. Please try again later..', ''); }
		});
	};
	// ---------------------------------------- Modal code ends here ---------------------------------*/
	/**
	 * Merge two array by property
	 */
	window.mergeByProperty = function (arr1, arr2, prop) {
		_.each(arr2, function (arr2obj) {
			var arr1obj = _.find(arr1, function (arr1obj) {
				return (arr1obj[prop] === arr2obj[prop]);
			});
			arr1obj ? _.extend(arr1obj, arr2obj) : arr1.push(arr2obj);// jshint ignore:line
		});
	};
	/**
	 * Function to snapshot 
	 */
	$scope.taSanpShot = function () {
		var filteredQueryString = getfilteredQueryString();
		snapShotData = {
			filteredQueryString: filteredQueryString,
			expandedNodes: $.extend([], window.global.expandedNodes["#vla"]),
			data: window.global.vlaData['#vla'],
			outputDetails: $scope.outputDetails,
			inputDetails: $scope.inputDetails
		};

		$scope.snapShotData = snapShotData;
		$scope.taisDisableCompare = false;
	};
	/**
	 * Function to compare 
	 */
	$scope.taCompare = function () {
		if (event.srcElement.innerHTML == "Compare") {// jshint ignore:line

			//			        if (snapShotData.expandedNodes) {
			var filteredQueryString = getfilteredQueryString();
			$scope.dataBythetimeofCompare = {
				expandedNodes: $.extend([], window.global.expandedNodes["#vla"]),
				outputDetails: $scope.outputDetails,
				data: window.global.vlaData['#vla'],
				inputDetails: $scope.inputDetails
			};
			$scope.isCompareShow = true;
			$timeout(function () {
				window.vlaId = "#vlaCompare1";
				global.dimensions.width = $("#vlaCompare1").width();
				global.dimensions.height = $("#vlaCompare1").height();
				window.global.expandedNodes["#vlaCompare1"] = snapShotData.expandedNodes;
				$scope.init('', 'filters', "vlaCompare1", snapShotData.filteredQueryString, snapShotData.data);
				//					            $.when($scope.init('', 'filters',"vlaCompare1",snapShotData.filteredQueryString)).then(function(){
				$timeout(function () {
					window.vlaId = "#vlaCompare2";
					window.global.expandedNodes["#vlaCompare2"] = $scope.dataBythetimeofCompare.expandedNodes;
					$scope.init('', 'filters', "vlaCompare2", filteredQueryString, $scope.dataBythetimeofCompare.data);
				}, 2000);

				//					            });
			}, 0);
			//			            $scope.iframeURL = $state.href("transactionAnalysis", {q: window.global.queryString, entityID: window.global.entityID, type: "entity", searchName: getParameterByName('searchName'), filters: "frmComp&" + snapShotData.filteredQueryString, 'expNod': snapShotData.expandedNodes.join(","), compare: 1});
			//			            $scope.iframeURL1 = $state.href("transactionAnalysis", {q: window.global.queryString, entityID: window.global.entityID, type: "entity", searchName: getParameterByName('searchName'), filters: "frmComp&" + filteredQueryString, 'expNod': window.global.expandedNodes.join(","), compare: 2});
			event.srcElement.innerHTML = "Analyze";// jshint ignore:line
			//			        }
		} else {
			window.vlaId = "#vla";
			event.srcElement.innerHTML = "Compare";// jshint ignore:line
			$scope.iframeURL = '';
			$scope.iframeURL1 = '';
			$scope.isCompareShow = false;
		}
	};

	/**
	 * Function to scroll to selected tx in te table
	 */
	window.ScrollTxTable = function () {
		//				goToByScroll("#"+id);
		//				$("#"+id).find("checkbox").click();
	};
	//----------------------------------------------------------------
	/**;
	 * Function to scroll page to particular div
	 */
	function goToByScroll(idOrClass) {
		// Scroll
		$('html,body').animate({
			scrollTop: $(idOrClass).offset().top
		},
			'slow');
	}
	/*-----------------------------dummy code here--------------------------------*/
	window.filterData = function (data, id) {
		var newData;
		if (id && window.global.vlaData[CurentVLAID]) {// jshint ignore:line
			newData = window.global.vlaData[CurentVLAID];// jshint ignore:line
		} else if ($scope.setScenario.toLowerCase() == "all") {
			newData = data;

		} else {
			//do filtering here
			newData = {
				edges: [],
				vertices: []
			};
			var allNodes = [];
			angular.forEach(data.edges, function (d) {
				if (d.transferType.toLowerCase() == $scope.setScenario.toLowerCase()) {
					if ($.inArray(d.from, allNodes) == -1) {
						allNodes.push(d.from);
					}
					if ($.inArray(d.to, allNodes) == -1) {
						allNodes.push(d.to);
					}
					newData.edges.push(d);
				}
			});
			angular.forEach(data.vertices, function (d) {
				if ($.inArray(d.id, allNodes) > -1) {
					newData.vertices.push(d);
				}

			});

		}
		//apply loaction filter if needed
		var locationsArr = [];
		if (locationfilterFromCompare) {
			locationsArr = locationfilterFromCompare.split(",");
		}

		var finalData;
		$(".TAlocationClassActive").each(function () {
			locationsArr.push($(this).find("a").text());
		});
		if (!locationfilterFromCompare && (locationsArr.length == 0 || $scope.locations.length == 0 || locationsArr.length == $scope.locations.length)) {
			finalData = newData;
		} else {
			finalData = {
				edges: [],
				vertices: []
			};
			var all_nodes = [];
			angular.forEach(newData.vertices, function (d) {
				if ($.inArray(d.location.country, locationsArr) > -1) {
					all_nodes.push(d.id);
					finalData.vertices.push(d);
				}

			});
			angular.forEach(newData.edges, function (d) {
				if ($.inArray(d.from, all_nodes) > -1 && $.inArray(d.to, all_nodes) > -1) {
					finalData.edges.push(d);
				}

			});
		}
		return finalData;

	};
	$scope.applyAnalysis = function () {
		var entityId = getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1 ? "petroSaudi" : getParameterByName('entityID');
		var typeOfAlert = getParameterByName('entityID').toLowerCase().indexOf("petrosaudi") > -1 ? "company" : getParameterByName('Etype');
		//				var url = $state.href('investigationLanding', {eId:entityId,caseId:getParameterByName("caseId"),q: $scope.applyAnalysisSelect,entity:  getParameterByName('searchName') ? getParameterByName('searchName') : entityId});
		var url = window.location.href.split("transactionIntelligence/#!")[0] + "#/investigationConsole/?q=" + $scope.applyAnalysisSelect + "&caseId=" + getParameterByName("caseId") + "&id=" + getParameterByName("q") + "&eId=" + entityId + "&Etype=" + typeOfAlert + "&entity=" + (getParameterByName('searchName') ? getParameterByName('searchName') : entityId);
		window.open(url, '_blank');
	};
	window.changeScenarioMain = function (val) {
		$scope.setScenario = val;
	};
	function filterTxTableForPetroSaudi() {
		//				return transactionPetro;
		var dataPetro = jQuery.extend(true, [], transactionPetro);
		var finalData;
		var filteredScenarioData;
		if ($scope.setScenario.toLowerCase() == "all") {
			filteredScenarioData = dataPetro;
		} else {
			var nested_data = d3.nest().key(function (d) {
				return d.channel;
			}).entries(dataPetro);
			angular.forEach(nested_data, function (d) {
				if (d.key.toLowerCase() == $scope.setScenario.toLowerCase()) {
					filteredScenarioData = d.values;
				}
			});
		}

		var locationsArr = [];
		$(".TAlocationClassActive").each(function () {
			locationsArr.push($(this).find("a").text().toLowerCase().trim());
		});
		if (!locationfilterFromCompare && (locationsArr.length == 0 || $scope.locations.length == 0 || locationsArr.length == $scope.locations.length)) {
			finalData = filteredScenarioData;
		} else {
			finalData = [];
			angular.forEach(filteredScenarioData, function (d) {
				if ($.inArray(d['country-org'].toLowerCase().trim(), locationsArr) > -1) {
					finalData.push(d);
				}

			});

		}

		return finalData;


	}
	/*---------------------------------------------- Line chart Starts---------------------------------------------*/


	function plotLineChart(lineChartData, id, lineWidth, height, avg) {
		var lineData = [];

		lineData = [{
			"key": "",
			"values": lineChartData
		}];

		var options = {
			container: id,
			height: height != undefined ? height : '',
			width: lineWidth != undefined ? lineWidth : '',
			axisX: true,
			axisY: true,
			gridy: true,
			tickfomat: "days",
			data: lineData,
			marginRight: 20,
			gridcolor: "#2e424b",
			actualData: lineData[0].values,
			ystartsFromZero: true,
			plotLineValue: avg,
			plotLineColor: "rgb(105, 202, 107)"
		};
		new InitializeandPlotLine(options);
	}


	/*---------------------------------------------- Line chart Ends---------------------------------------------*/

	/*-----------------------------dummy code ends here--------------------------------*/
	/**
	 * Function to load scenario details
	 */
	function getScenarioDetails() {
		var param = "alertId=" + (getParameterByName('q')) + "&token=" + $rootScope.ehubObject.token;
		TransactionAnalysisApiService.getScenarioCharts(param).then(function (response) {

			if (response.data.alertScenarioType == "amount" && response.data.maximumAmount && response.data.amount) {
				plotSceanrioCharts(response.data);
				$scope.sceanrioHeader = response.data.alertDescription;
			} else {
				$scope.sceanrioHeader = response.data.alertDescription;
			}
			$('.custom-spinner').hide();
		}, function () {
			$('.custom-spinner').hide();
			return false;
		});
	}
	/**
	* Function to load customer alerts
	*/
	function getCustomerAlerts() {
		var param = "customerNumber=" + (getParameterByName('entityID')) + "&token=" + $rootScope.ehubObject.token;
		TransactionAnalysisApiService.getCustomerAlerts(param).then(function (response) {
			$scope.otherAlertsForCust = response.data.filter(function (n) { return n.id != (getParameterByName('q')); });
		}, function () {
			return false;
		});
	}
	//-----------------------------------------------------------------------
	/**
	 * Function to plot scenario charts
	 */
	function plotSceanrioCharts(data) {
		//code for transaction scenario chart

		var colorObj = {
			"Acceptable amount of turnover based on the AML risk score": "#69ca6b",
			"Factual amount of turnover": "#E32646",
		};
		var txScenarioData = [

			{
				"label": "Acceptable Amount",
				"y": data.maximumAmount,
				"cls": "acceptable",
				type: "sceanrio",
				"txt": "Acceptable amount of turnover based on the AML risk score:" + getFormattedAmount(data.maximumAmount)
			}, {
				"label": "Factual Amount",
				"y": data.amount,
				"cls": "factual",
				type: "sceanrio",
				"txt": "Factual amount of turnover:" + getFormattedAmount(data.amount)
			}];
		var txMaxVal = d3.max(txScenarioData, function (d) { return d.y; });
		txScenarioData.map(function (d) {
			d.valpct = (d.y / txMaxVal) * 100;
			return (d.valPctInt = parseInt((d.y / txMaxVal) * 100));
		});
		$scope.activityScenario = txScenarioData;
	}
	/**
	 * Function to load dashboard 
	 */
	$scope.loadDashboard = function (alert) {
		var url = $state.href('transactionIntelligence', { 'q': alert.id, 'entityID': (getParameterByName('entityID')), 'searchName': (getParameterByName('searchName')), "Etype": (getParameterByName('Etype')), "tId": alert.alertTransactionId });
		window.open(url, '_blank');
	};
}