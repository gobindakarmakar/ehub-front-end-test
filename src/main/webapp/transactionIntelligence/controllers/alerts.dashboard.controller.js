'use strict';

angular.module('ehubTransactionIntelligenceApp')
    .controller('AlertsDashboardController', alertsDashboardController);

alertsDashboardController.$inject = [
    '$scope',
    '$rootScope',
    'EnrichSearchGraph',
    'EntityGraphService',
    '$uibModal',
    '$state',
    'TransactionAnalysisApiService',
    'TransactionGraphService',
    '$uibModalStack',
    '$q',
    '$window'

];

function alertsDashboardController(
    $scope,
    $rootScope,
    EnrichSearchGraph,
    EntityGraphService,
    $uibModal,
    $state,
    TransactionAnalysisApiService,
    TransactionGraphService,
    $uibModalStack,
    $q,
    $window
) {
    $scope.filtersNamesToShow = {
        "isGeographyRiskType": "Risk Type",
        "isCustomerRiskType": "Risk Type",
        "isProductRisk": "Risk Type",
        "isOutput": "Transaction",
        "isInput": "Transaction",
        "isAlertsByPeriodgreaterthan30Days": "Alert Period",
        "isAlertsByPeriod10to20Days": "Alert Period",
        "isAlertsByPeriod20to30Days": "Alert Period",
        "isAlertByStatusGreaterThan30Days": "Alert Status",
        "isBelowOneMillionUSD": "Monthly Turnover",
        "isBetweenOneToFiveMillionUSD": "Monthly Turnover",
        "isBetweenFiveToTenMillionUSD": "Monthly Turnover",
        "isAboveTenMillionUSD": "Monthly Turnover",
        "transactionType": "Transction Type",
        "corporateStructureType": "Corporate Structure Type",
        "customerName": "Customer Name",
        "scenario": "Scenario",
        "country": "Country",
        "shareholderType": "Shareholder Type",
        "corporateGeographyType": "Corporate Geography Type",
        "bankName": "Bank Name",
        "bankLocation": "Bank Location",
        "productRiskType": "Product Risk Type",
        "customerRisk": "Customer Risk Type",
        "geoRiskType": "Geography Risk Type",
        "corruptionRiskType": "Corruption Risk Country",
        "politicalRiskType": "Political Risk Country",
        "activityCategory": "Activity Category",
        "alertStatus": "Alert Status",
        "counterpartyCountry": "Counterparty Country",
        "counterPartyNames": "Counterparty"

    };
    var filtersObj = {}, finalFiltersObj = {};
    $scope.filterArr = [];

    // Create Risk Alert Modal 
    window.riskAlertModal = function (data, colors) {
        var openRiskAlertModal = $uibModal.open({
            templateUrl: 'riskAlert.modal.html',
            controller: 'RiskAlertModalController',
            backdrop: 'static',
            size: 'sm',
            windowClass: 'custom-modal transaction-modal riskAlertModal',
            resolve: {
                fromDt: function () {
                    return $scope.fromDt;
                },
                toDt: function () {
                    return $scope.toDt;
                },
                id: function () {
                    return data.id ? data.id : data.customerId;
                },
                customerAmount: function () {
                    return data.totalAmount ? data.totalAmount : data.transactionAmount;

                },
                colors: function () {
                    return colors;
                },
                filtersObj: function () {
                    return finalFiltersObj;
                }
            }
        });

        openRiskAlertModal.result.then(function () {

        }, function () {

        });
    };
    window.transSelectedFilter = {};
    $scope.alertDashboard = {
        contentMenu: 'alertMainMenu',
        contentMenu0: 'alertSubMenu0',
        contentMenu1: 'alertSubMenu1',
        contentMenu2: 'alertSubMenu2',
        contentMenu3: 'alertSubMenu3',
        contentMenu4: 'alertSubMenu4',
        contentMenu5: 'alertSubMenu5'
    };

    /*loader*/
    var urlCalls = [];
    var riskSidePanelProgressBarChart = {
        CustomerRisk: [],
        ProductRisk: [],
        GeoGraphicRisk: []
    };
    var TopCounterPartyActivities = [],
        TopCounterPartyBanks = [],
        TopCounterPartyCountries = [];
    var Top3CounterPartyActivities = [],
        Top3CounterPartyBanks = [],
        Top3CounterPartyCountries = [];
    $scope.alertDasbboardloader = false;
    $scope.riskPieChartPreloader = true;
    $scope.lineChartPreloader = true;
    $scope.counterpartyWorldMapPreloader = true;
    $scope.transactipTypesPieChartPreloader = true;
    $scope.barchartnotificationPreloader = true;
    $scope.associatedEntitychartPreloader = true;
    $scope.transfersByGeographyChartPreloader = true;
    $scope.aml_top_scenarios_pieData = [];
    $scope.amlAlertByStatusData = [];
    $scope.amlAlertByPeriod = [];
    $scope.alertsByStatusGreaterThan30 = [];
    $scope.loader = {
        corporateStructureloader: false,
        transfersByMonthChartPreloader: true,
        corpoStructureWorldMapOnePreloader: true,
        counterPartiesWorldMapOnePreloader: true,
        counterPartiesWorldMapTwoPreloader: true,
        counterparty_CorruptionRiskPreloader: true,
        counterparty_PoliticalRiskPreloader: true

    };

    $scope.dashboardTabloader = {
        topScenariosChartloader: true,
        dashboardTopBranchesloader: true,
        dashboardTopGeographyloader: true,
        dashboradTopCustomersloader: true,
        dashboardAlertsByCustomerloader: true,
        dashboardTransfersByBranchloader: true,
        dashboatdPeriodPieChartLoader: true,
        Statuschartloader: true,
        Status30Chartloader: true,
        dashboardAlertsByScenarioloader: true,
    };

    $scope.transactionTabloader = {
        inputOutputRatioloader: true,
        transalertsChartloader: true,
        transLineChartloader: true,
        barchart1loader: true,
        transaction_pieChartloader: true,
        alertRatioloader: true,
        top3Transactionloader: true,
        topTransactionTypesloader: true,
        monthlyTurnoverPieChartloader: true,
        transactionComparisonChartloader: true,
    };


    $scope.riskTabloader = {
        risk_pieChartloader: true,
        risk_productpieChartloader: true,
        risk_geopieChartloader: true,
        top3Transactionloader: true,
        riskBloomBergChartOneloader: true,
        riskBarChartOneloader: true,
        riskBloomBergChartTwoloader: true,
        riskBarChartTwoloader: true,
        riskBloomBergChartThreeloader: true,
        riskBarChartThreeloader: true,
        riskBloomBergChartFourloader: true,
        riskBarChartFourloader: true,
        riskBloomBergChartFiveloader: true,
        riskBarChartFiveloader: true,
        counterparty_CorruptionRiskloader: true,
        counterparty_PoliticalRiskloader: true,
        customerTransactionChartloader: true,
        productTransactionChartloader: true,
        geoTransactionChartloader: true,
        riskPieChartOneloader: true,
    };

    $scope.corporateStructureTabloader = {
        corpoStructurePieChartOneloader: true,
        corpoStructurePieChartTwoloader: true,
        corpoStructurePieChartThreeloader: true,
        alertTrendloader: true,
        corpoStructureBloombergChartOneloader: true,
        corpoStructureBarChartOneloader: true,
        corpoStructureBloombergChartTwoloader: true,
        corpoStructureBarChartTwoloader: true,
        corpoStructureBarChartTreeloader: true,
        corpoStructureWorldMapOneloader: true,
        corpoStructurePieChartFourloader: true,
        corpoStructurePieChartFiveloader: true,
        corpoStructurePieChartSixloader: true,
    };

    $scope.counterPartiesStructureTabloader = {
        counterPartiestopActivityloader: true,
        counterPartiestopGeographyloader: true,
        counterPartiestopBanksloader: true,
        counterPartiesLineChartOneloader: true,
        counterPartiesBanksLocationsRatioloader: true,
        counterPartiesBanksRatioloader: true,
        counterPartiesGeographyRatioloader: true,
        counterPartyAlertActvityRatioloader: true,
        counterPartiesBloombergChartOneloader: true,
        counterPartiesBloombergChartTwoloader: true,
        counterPartiesBloombergChartThreeloader: true,
        counterPartiesWorldMapOneloader: true,
        counterPartiesWorldMapTwoloader: true,
        counterPartiesBarChartOneloader: true,
        counterPartiesBarChartTwoloader: true,
        counterPartiesBarChartThreeloader: true,
    };
    var div = d3.select("body").append("div").attr("class", "alert_dashboard_progress_tooltip").style("position", "absolute").style("z-index", 1000).style("background", "rgb(27, 39, 53)").style("padding", "5px 10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");

    /**
     * Function to get tool tip for progress bars
     * 
     */
    $("body").on("mouseover", ".progressbar-list-item", function () {
        if (location.hash == "#!/alertDashboard") {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".progressbar-list-item", function (event) {
        if (location.hash == "#!/alertDashboard") {
            var amount_units = '';
            if ($(this).attr("class").indexOf('by-amount-bar') > 0) {
                amount_units = "$";
            }
            div.html(($(this).find(".left-col").text()).toUpperCase() + ": " + amount_units + $(this).find(".right-col").text());

            var p = $(".alert-dashboard-wrapper");
            var position = p.offset();
            var windowWidth = window.innerWidth; /* jshint ignore: line */
            var tooltipWidth = $(".alert_dashboard_progress_tooltip").width() + 50;
            var cursor = event.pageX;
            if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
                var element = document.getElementsByClassName("alert_dashboard_progress_tooltip");
                for (var i = 0; i < element.length; i++) {
                    element[i].classList.remove("tooltip-left");
                    element[i].classList.add("tooltip-right");
                }
                div.style("left", (event.pageX - 30 - $(".alert_dashboard_progress_tooltip").width()) + "px");
            } else {
                var element = document.getElementsByClassName("alert_dashboard_progress_tooltip");
                for (var i = 0; i < element.length; i++) {
                    element[i].classList.remove("tooltip-right");
                    element[i].classList.add("tooltip-left");
                }
                div.style("left", (event.pageX) - 20 + "px");
            }
            return div.style("top", event.pageY + "px");

            div.style("left", event.pageX + "px");/* jshint ignore: line */
            div.style("top", event.pageY + "px");

        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to load dashboard analyze
     */
    $scope.loadDashboard = function (alert) {
        if (alert) {
            if (alert.focalEntityId) {
                var url = $state.href('transactionIntelligence', {
                    'q': alert.id,
                    'entityID': alert.focalEntityId,
                    'searchName': (alert.searchName ? alert.searchName : alert.customerName),
                    "Etype": (alert.customerType == "IND" ? "Person" : "Company"),
                    "tId": alert.alertTransactionId
                });
                window.open(url, '_blank');

            }
        }
    };

    /*
     * @purpose: get date from week
     * @created: 13 feb 2018
     * @return: date
     * @author: prasanthi
     */
    function getDateOfISOWeek(w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            {ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);}
        else
            {ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());}

        return ISOweekStart.getFullYear() + "-" + ISOweekStart.getMonth() + "-" + ISOweekStart.getDate();
    }

    /*
     * @purpose: get formated number with thousand suffix
     * @created: 13 feb 2018
     * @return: date
     * @author: prasanthi
     */
    function nFormatter(num, digits) {
        var si = [{
            value: 1,
            symbol: ""
        },
        {
            value: 1E3,
            symbol: "k"
        },
        {
            value: 1E6,
            symbol: "M"
        },
        {
            value: 1E9,
            symbol: "G"
        },
        {
            value: 1E12,
            symbol: "T"
        },
        {
            value: 1E15,
            symbol: "P"
        },
        {
            value: 1E18,
            symbol: "E"
        }
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }
        return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

    //code for number formatting
    Number.prototype.formatAmt = function (decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };


    /* @purpose: addClass while scrolling
     * @created: 14 feb 2018
     * @return: no
     * @author: varsha
     */
    var scrollposition = 0;
    angular.element($window)
        .bind(
            "scroll",
            function () {
                scrollposition = this.scrollY;

                if (this.scrollY > 60) {
                    $(".custom-modal.right").addClass("modal-top");
                } else {
                    $(".custom-modal.right").removeClass("modal-top");
                }
            });


    $scope.conterPartyLocNotifyOpts = {};
    $scope.NotifyRiskPieDate = [];
    $scope.current_tab = "notification";
    $scope.checkedValue = "DESC";
    $scope.transactionTypesDetails = {
        txDetails: [],
        txAmountNumberDetails: []
    };
    $scope.inputOutputProgress = {
        txAmountNumberDetails: []
    };
    // Create Alert Modal 
    $scope.createAlertModal = function () {
        var opencreateAlertModal = $uibModal.open({
            templateUrl: 'createAlertTi.modal.html',
            controller: 'createAlertModalController',
            backdrop: 'static',
            size: 'sm',
            windowClass: 'custom-modal mip-save-modal mip-save-wrapper creatAlertModalWrapper',

        });

        opencreateAlertModal.result.then(function (response) {
            if (response == "upload") {
            }
        }, function () {

        });
    };
    //------------------------------------------------------------------------
    /**
     * Function to get granularity based on date range
     */
    function getGranularity(dateArr) {
        if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= 86400000) {
            //day
            return "daily";
        } else if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= (7 * 86400000)) {
            //week
            return "daily";
        } else if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= (30 * 86400000)) {
            //month
            return "daily";
        } else if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= (180 * 86400000)) {
            //6 months
            return "weekly";
        } else if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= (365 * 86400000)) {
            //year
            return "weekly";
        } else if (new Date(dateArr[1]).getTime() - new Date(dateArr[0]).getTime() <= (5 * 365 * 86400000)) {
            //5 year
            return "monthly";
        } else {
            return "yearly";
            //more than 5 year
        }

    }

    /**
     * Function to handle data g grouped bar
     */

    function handleData(data, x, y, type) {/* jshint ignore: line */
        var str = JSON.stringify(data);
        str = str.replace(/model/g, 'x');
        var object = JSON.parse(str);

        return object;
    }


    //Common Options

    var tagCloudOptions = { /* jshint ignore: line */ //TagCloud option
        header: "MY ENTITIES",
        container: "#associatedEntitychart",
        height: 180,
        width: ($("#associatedEntitychart").width()),
        //    data: tagCloudData,
        margin: {
            bottom: 10,
            top: 10,
            right: 10,
            left: 10
        },
        domain: {
            x: 1,
            y: 1000
        }
        // xticks:5,
        // yticks:5,
        // xtext:""
        // ytext:""
    };

    //EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOptions);

    var colorsObj = {
        "negative": "#5D96C8",
        "neutral": "#69CA6B",
        "positive": "#B753CD"
    };
    var colors = ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d", "#669900", "#334433"];
    var options = { /* jshint ignore: line */ //Piechart option
        container: '#1',
        colors: colors,
        height: 160,
        colorsObj: colorsObj,
        islegends: true,
        //     islegendleft: true,
        legendwidth: 30,
        istxt: '100%',
        txtColor: '4D6772',
        legendmargintop: 30
    };

    var bubbleSocialOptions = { //BubbleEntity option
        "height": 210,
        "bubbleColor": ['#8F4582']
    };

    var politiciansOptions = { //Bar chart option
        container: "#corpoStructureBarChartOne",
        width: $("#corpoStructureBarChartOne").width(),
        height: 200,
        marginTop: 1,
        marginBottom: 1,
        marginRight: 5,
        marginLeft: 5,
        tickColor: "rgb(51, 69, 81)",
        color_hash: {
            0: ["Invite", "#8F4582"]
        }
    };

    var worldChartOptions = { //worldchart option
        container: "#associatedEntitychart",
        uri1: "../vendor/data/worldCountries.json", // Url of data
        uri2: "../vendor/data/worldMapData.json", // Url of data
        height: 185,
        //markers: markers
    };
    var worldDataActual;

    function World(options) {
        if (!options || !options.container) {
            return false;
        }
        var worldmapId = options.container;/* jshint ignore: line */
        var worldmapuri1 = options.uri1;/* jshint ignore: line */
        if (!worldDataActual) {
            d3.json(options.uri1, function (error, data) {
                var WorldData = data;
                worldDataActual = data;
                options.data = [WorldData, []];
                WorldMapLoader();
                if ($(options.container).width() > 0) {
                    TransactionGraphService.plotWorldLocationChart(options);
                }
            });
        } else {
            options.data = [worldDataActual, []];
            WorldMapLoader();
            if ($(options.container).width() > 0) {
                TransactionGraphService.plotWorldLocationChart(options);
            }
        }

    }

    function WorldMapLoader() {
        $scope.transfersByGeographyChartPreloader = false;
        $scope.counterpartyWorldMapPreloader = false;
    }


    /*
     * @purpose: to format the date to required format
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function formatDate(date) {
        return new Date(date).toString().split("GMT")[0];
        var time = new Date(date);/* jshint ignore: line */
        var Hr_min_time = time.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
        });
        var weekdays = ["SUN", "MON", "TUES", "WED", "THU", "FRI", "SAT"];
        var weekday = weekdays[time.getDay()];
        var curr_date = time.getDate();
        var curr_year = time.getFullYear();
        var finalTime = Hr_min_time + "," + weekday + " " + curr_date + "," + curr_year;
        return finalTime;
    }

    function removeDuplicates(arr) {
        var unique_array = [];
        for (var i = 0; i < arr.length; i++) {
            if (unique_array.indexOf(arr[i]) == -1) {
                unique_array.push(arr[i]);
            }
        }
        return unique_array;
    }


    //-------------------------------------Notification Tab Starts---------------------------------------------------------------//		    

    /*
     * @purpose: Get Associated Entities
     * @created: 12 feb 2018
     * @return: no
     * @author: varsha
     */
    var notiTagCloudData = [];
    function getAssociatedEntities(date) {
        urlCalls.push(TransactionAnalysisApiService.getAssociatedEntities(date, finalFiltersObj).then(function (response) {
            response.data.map(function (d) {
                d.text = d.customerName;
                d.size = d.amount;
            });
            $scope.associatedEntitychartPreloader = false;
            notiTagCloudData = jQuery.extend(true, [], response.data);
            if ($scope.current_tab == "notification") {
                loadNotiTagCloud(notiTagCloudData);
            }
        }));

    }

    /*
     * @purpose: Notification TagCloud
     * @created: 12 feb 2018
     * @return: no
     * @author: varsha
     */
    function loadNotiTagCloud(data) {
        var maxDta = d3.max(data, function (d) {
            return d.amount;
        });
        var tagOption = {
            container: "#associatedEntitychart",
            height: 180,
            width: ($("#associatedEntitychart").width()),
            data: data,
            domain: {
                x: 1,
                y: parseInt(maxDta) + 2000
            },
            textClick: 'false'
        };
        if (location.hash == "#!/alertDashboard" && (!data || data.length == 0)) {
            $(("#associatedEntitychart")).empty();
            d3.select("#associatedEntitychart").append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");
            return false;
        }
        if ($(tagOption.container).width() > 0) {
            setTimeout(function () {
                TransactionGraphService.textcloudChart(tagOption);
            }, 0);

        }

    }

    /*
     * @purpose: Notification data table
     * @created: 12 feb 2018
     * @return: no
     * @author: varsha
     */

    function alertList(date) {
        urlCalls.push(TransactionAnalysisApiService.getAlertlist(date, finalFiltersObj).then(function (response) {
            $scope.listAlerts = [];
            $scope.alertsLength = response.data.paginationInformation.totalResults;
            response.data.result.map(function (d) {
                d.name = d.customerName,
                    d.date = formatDate(d.alertBusinessDate)/* jshint ignore: line */
                d.amount = '$' + ' ' + Number(d.totalAmount).formatAmt();
            });
            $scope.listAlerts = response.data.result;
        }, function () {
            $scope.listAlerts = [];
        }));

    }

    /*
     * @purpose: on upload files call api in every 30 seconds
     * @created: 17 feb 2018
     * @return: no
     * @author: varsha
     */
    var reloadPage = false;
    window.alertList = function () {
        var newDate = [];
        var alertlistSort = jQuery.extend(true, [], $scope.listAlerts);
        alertlistSort.sort(function (a, b) {
            return b.alertCreatedDate - a.alertCreatedDate;
        });
        var new_date1 = alertlistSort[0].alertCreatedDate;
        newDate[0] = new_date1;
        newDate[2] = '';
        if ($scope.pageNum == undefined) {
            newDate[3] = 1;
        } else {
            newDate[3] = $scope.pageNum;
        }
        newDate[4] = '';
        newDate[5] = 20;
        var timeValue = 0;
        window.alertInterval = setInterval(function () {
            timeValue = timeValue + 1;
            var new_date2 = moment()._d.getTime();
            newDate[1] = new_date2;
            urlCalls.push(TransactionAnalysisApiService.getAlertlistWithTimeStamp(newDate, finalFiltersObj).then(function (response) {
                if (response.data.result.length > 0) {
                    response.data.result.sort(function (a, b) {
                        return b.alertCreatedDate - a.alertCreatedDate;
                    });
                    new_date1 = response.data.result[0].alertCreatedDate;
                    newDate[0] = new_date1;
                    reloadPage = true;
                    confirmMissingFiles();
                }
            }, function () { }));
            if (((30 * timeValue) / 60) == 10) {
                clearInterval(window.alertInterval);
            }
        }, 30000);
    };


    /*
     * @purpose: on upload files confirm alert to refresh the whole page
     * @created: 21 march 2018
     * @return: no
     * @author: varsha
     */
    var ConfirmProcessModalInstance;
    function confirmMissingFiles() {
        $uibModalStack.dismissAll();
        $scope.finalMsg = "New Alert has been created, Do you want to see ?";
        ConfirmProcessModalInstance = $uibModal.open({
            templateUrl: './modal/views/ConfirmProcess.modal.html',
            scope: $scope,
            size: 'lg',
            backdrop: 'static',
            windowClass: 'custom-modal'
        });

        ConfirmProcessModalInstance.result.then(function () { }, function () { });
    }
    $scope.confirmProcessResp = function (yesNo) {
        ConfirmProcessModalInstance.dismiss();
        if (yesNo) {
            if (reloadPage == true) {
                location.reload();
            }
        } else {
            //  			 clearInterval(window.alertInterval);
            return false;
        }
    };


    /*
     * @purpose: dropbox change value
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    $scope.changeDropDown = function (checkedValue) {
        $scope.Date[2] = checkedValue;
        alertList($scope.Date);
    };

    /*
     * @purpose: page change value
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    $scope.pageChanged = function (page) {
        $scope.Date[3] = page;
        alertList($scope.Date);
    };

    /*
     * @purpose: textbox change value
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */
    $scope.changeSearchBox = function (event, alertSearchName) {
        if (event.which == 13) {
            $scope.Date[4] = alertSearchName;
            alertList($scope.Date);
        }
    };

    /*
     * @purpose:getMonthlyTurnOver
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function getMonthlyTurnOver(date) {
        urlCalls.push(TransactionAnalysisApiService.getMonthlyTurnOver(date, finalFiltersObj).then(function (response) {
            $scope.MonthlyTurnOverData = [];
            angular.forEach(response.data, function (d, i) {
                $scope.MonthlyTurnOverData.push({
                    key: i,
                    value: d
                });
            });

            if ($scope.current_tab == "transaction") {
                $scope.loadChart_transaction();
            }

        }, function () {

        }));

    }

    /*
     * @purpose:getcounterPartyLocationsNotify
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function getcounterPartyLocationsNotify(date, id, type) {
        urlCalls.push(TransactionAnalysisApiService.getcounterPartyLocationsNotify(date, type, finalFiltersObj).then(function (response) {
            loadWorldChart(response, id);
        }, function () {

        }));

    }

    /*
     * @purpose: Function to load worldchart in multiple tab's
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function loadWorldChart(response, id) {
        var markers = [];
        angular.forEach(response.data.inputCounterpartyList, function (d) {
            if (d.country && d.country.country) {
                markers.push({
                    'lat': d.country.latitude, //worldMap Data
                    'long': d.country.longitude,
                    'mark': "assets/images/redpin.png",
                    'name': d.country.country,
                    'title': d.country.country,
                    'amount': d.amount,
                    'fillColor': '#3361B6'

                });
            }
        });
        angular.forEach(response.data.outputCounterpartyList, function (d) {
            if (d.country && d.country.country) {
                markers.push({
                    'lat': d.country.latitude, //worldMap Data
                    'long': d.country.longitude,
                    'mark': "assets/images/redpin.png",
                    'name': d.country.country,
                    'title': d.country.country,
                    'amount': d.amount,
                    'fillColor': '#E94488'

                });
            }
        });

        var maxValue_input = d3.max(response.data.inputCounterpartyList, function (d) {
            return d.amount;
        });
        var maxValue_output = d3.max(response.data.outputCounterpartyList, function (d) {
            return d.amount;
        });
        var maxValue = d3.max([maxValue_input, maxValue_output], function (d) {
            return d;
        });
        var worldOptions1 = jQuery.extend(true, {}, worldChartOptions);
        worldOptions1.markers = markers;
        worldOptions1.domain = {
            x: 1,
            y: maxValue + 2000
        };
        worldOptions1.ChangeRadius = 'true';
        worldOptions1.container = "#" + id;
        if ((worldOptions1.container == "#counterparty_PoliticalRisk" || worldOptions1.container == "#counterparty_CorruptionRisk")) {
            worldOptions1.height = 220;
        }
        $scope.conterPartyLocNotifyOpts[id] = jQuery.extend(true, {}, worldOptions1);

        if (worldOptions1.container == "#counterpartyWorldMap" && $scope.current_tab == "notification") {
            setTimeout(function () {
                World(worldOptions1);
            }, 0);
        } else if ((worldOptions1.container == "#counterparty_PoliticalRisk" || worldOptions1.container == "#counterparty_CorruptionRisk") && $scope.current_tab == "risk") {
            setTimeout(function () {
                World(worldOptions1);
            }, 0);
        }
        $scope.counterpartyWorldMapPreloader = false;
        $scope.riskTabloader.counterparty_CorruptionRiskloader = false;
        $scope.riskTabloader.counterparty_PoliticalRiskloader = false;

    }
    /*
     * @purpose: get TransactionAndAlertAggregates
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function getTransactionAndAlertAggregates(date, checkedTypeDetails) {
        $scope.getTransactionAndAlertAggregates = [];
        var granularity = getGranularity(date);
        urlCalls.push(TransactionAnalysisApiService.getTransactionAndAlertAggregates(date, granularity, checkedTypeDetails, finalFiltersObj).then(function (response) {
            if (granularity == "weekly") {
                response.data.alertAggregator.map(function (d) {
                    return d.businessDate = getDateOfISOWeek(d.businessDate.split('-')[1], d.businessDate.split('-')[0]);/* jshint ignore: line */
                });
                response.data.transactionAggregator.map(function (d) {
                    return d.businessDate = getDateOfISOWeek(d.businessDate.split('-')[1], d.businessDate.split('-')[0]);/* jshint ignore: line */
                });
            }
            window.isTabChangeFromContextTrans = false;
            $scope.getTransactionAndAlertAggregates = response;
            $scope.counterPartiesStructureTabloader.counterPartiesLineChartOneloader = false;
            $scope.corporateStructureTabloader.alertTrendloader = false;
            if ($('#linechartnotification').width() > 0) {
                loadLinechart('#linechartnotification', 150, response);
            }
            if ($('#transLineChart').width() > 0) {
                loadLinechart('#transLineChart', 200, response); //height=200
            }
            if ($('#barchart1').width() > 0) {
                loadBarChart(response, 215, '#barchart1');
            }

            if ($('#corpoStructureLineChartOne').width() > 0) {
                loadLinechart('#corpoStructureLineChartOne', 60, response, true);
            }
            if ($('#counterPartiesLineChartOne').width() > 0) {
                loadLinechart('#counterPartiesLineChartOne', 60, response, true);
            }

        }, function () {

        }));

    }

    /*
     * @purpose: load line Charts
     * @created: 8 feb 2018
     * @return: no
     * @author: varsha
     */

    function loadLinechart(id, height, response, noXyAxis) {
        if (!response || !response.data || (response.data.alertAggregator.length == 0 && response.data.transactionAggregator.length == 0)) {
            $(id).empty();
            d3.select(id).append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");
            $scope.transactionTabloader.transLineChartloader = false;
            //    	  /  $scope.lineChartPreloader=false;
            return false;
        }
        var lineData = [{
            key: "alertAggregator",
            values: []
        },
        {
            key: "transactionAggregator",
            values: []
        }
        ];
        if (noXyAxis) {
            noXyAxis = true;
        } else {
            noXyAxis = false;
        }
        var alertAggregator = [];/* jshint ignore: line */
        var allDates = [];/* jshint ignore: line */
        angular.forEach(response.data.alertAggregator, function (value) {
            if (value.businessDate == "0007-07") {
                value.businessDate = "2017-07";
            }
            if (value.businessDate && (value.totalTransactionAmount || value.alertedAmount)) {
                lineData[0].values.push({
                    time: value.businessDate,
                    value: value.totalTransactionAmount || value.alertedAmount
                });
            }
        });
        var transactionAggregator = [];/* jshint ignore: line */
        angular.forEach(response.data.transactionAggregator, function (value) {
            if (value.businessDate && value.totalTransactionAmount) {
                lineData[1].values.push({
                    time: value.businessDate,
                    value: value.totalTransactionAmount
                });
            }
        });

        var lineoptions = {
            container: id,
            axisX: true,
            height: height,
            axisY: true,
            gridy: true,
            tickformat: "year",
            data: lineData,
            marginRight: 20,
            gridcolor: "#2e424b",
            actualData: lineData[0].values,
            ystartsFromZero: true,
            labelRotate: '-65',
            colorsObj: ['#D53F8B', '#2773B3'],
            noXyAxis: noXyAxis
        };
        if (noXyAxis) {
            lineoptions.labelRotate = 1;
            lineoptions.marginTop = 1;
            lineoptions.marginBottom = 1;
            lineoptions.marginLeft = 1;
            lineoptions.marginRight = 1;
        }

        if (id == '#linechartnotification') {
            lineoptions.type = 'Alert/Transaction Trend';
        } else if (id == '#transfersByMonthChart') {
            lineoptions.type = 'Transfer By Month';
        } else if (id == '#transLineChart') {
            lineoptions.type = 'all';
        } else if (id == '#corpoStructureLineChartOne') {
            lineoptions.type = 'Alert Trend';
        } else if (id == '#counterPartiesLineChartOne') {
            lineoptions.type = 'Alert Trend';
        } else {
            lineoptions.type = id.split("amountChart_")[1];
        }

        $scope.transactionTabloader.transLineChartloader = false;
        //       $scope.lineChartPreloader=false;
        if ($(lineoptions.container).width() > 0) {
            new InitializeandPlotLine(lineoptions);
        }
    }

    /*
     * @purpose: load bar Charts
     * @created: 6 feb 2018
     * @return: no
     * @author: varsha
     */

    function loadBarChart(response, height, id) {
        if (!response || !response.data) {
            $(id).empty();
            d3.select(id).append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");
            return false;
        }
        var alertAggregator = [];
        angular.forEach(response.data.alertAggregator, function (value) {
            alertAggregator.push({
                model: value.businessDate,
                alertAggregator: value.alertCount,
                transactionAggregator: 0
            });
        });
        var transactionAggregator = [];
        angular.forEach(response.data.transactionAggregator, function (value) {
            transactionAggregator.push({
                model: value.businessDate,
                transactionAggregator: value.transactionsCount,
                alertAggregator: 0
            });
        });

        var dateArray = [];
        var usedDate = [];
        angular.forEach(alertAggregator, function (value) {
            transactionAggregator.map(function (d) {
                if (d.model == value.model) {
                    usedDate.push(value.model);
                    if (value.alertAggregator == null) {
                        d.alertAggregator = 0;
                    } else {
                        d.alertAggregator = value.alertAggregator;
                    }

                } else {
                    dateArray.push(value.model);
                }
            });
        });

        var final = removeDuplicates(dateArray);
        var fdate = removeDuplicates(usedDate);
        var finalDate = final.filter(function (obj) {
            return fdate.indexOf(obj) == -1;
        });
        for (var i = 0; i < finalDate.length; i++) {
            angular.forEach(alertAggregator, function (value) {/* jshint ignore: line */
                if (finalDate[i] == value.model) {
                    if (value.alertAggregator == null) {
                        value.alertAggregator = '';
                    }
                    transactionAggregator.push({
                        model: value.model,
                        transactionAggregator: value.transactionAggregator,
                        alertAggregator: value.alertAggregator
                    });
                }
            });

        }
        var transbarData = handleData(transactionAggregator, "model", "", "bar");

        var baropt = {
            container: id,
            data: transbarData,
            height: height,
            islegends: false,
            isHeaer: false,
            showlineaxisY: false,
            showlineaxisX: false,
            xtext: ' ',
            ytext: ' ',
            showgroupedstacked: false,
            colors: ['#3472A8', '#DB3F8D'],
            showxYTxt: true,
            xLabelRotation: "rotate(-65)"
        };
        if (id == '#barchart1') {
            baropt.type = 'all';
        } else {
            baropt.type = id.split("countChart_")[1];
        }
        $scope.transactionTabloader.barchart1loader = false;
        setTimeout(function () {
            new groupedColumChart(baropt); //load groupedbar chart
        });
    }

    /*
     * @purpose: load top 3 transaction progress bar in notification tab
     * @created: 7 feb 2018
     * @return: no
     * @author: varsha
     */

    function setNotiTopThreeTransaction() {
        var topDate = jQuery.extend(true, [], $scope.Date);
        topDate[2] = 'DESC';
        topDate[5] = 3;

        urlCalls.push(TransactionAnalysisApiService.getAlertlist(topDate, finalFiltersObj).then(function () {
            $scope.riskTabloader.top3Transactionloader = false;
        }, function () {
            $scope.notification_top_transaction_type_progressData = [];
        }));
    }

    /*
     * @purpose: getFetchTxsCountAndRatio
     * @created: 7 feb 2018
     * @params: date, token
     * @return: no
     * @author: varsha
     */
    var token = {
        token: $rootScope.ehubObject.token
    };
    var getFetchTxsCountAndRatioData = [];

    function getFetchTxsCountAndRatio(date) {
        urlCalls.push(TransactionAnalysisApiService.getFetchTxsCountAndRatio(date, token, finalFiltersObj).then(function (response) {
            getFetchTxsCountAndRatioData = response;
            topSidePanel(getFetchTxsCountAndRatioData);
        }));
    }
    /*
     * @purpose: getFetchTxsCountAndRatio by input and output
     * @created: 15 feb 2018
     * @params: date
     * @return: no
     * @author: swathi
     */
    var getFetchTxsCountAndRatioByInputData = [];

    function getFetchTxsCountAndRatioByInput(date) {
        getFetchTxsCountAndRatioByInputData = [];
        var isInput = {
            input: true
        };
        urlCalls.push(TransactionAnalysisApiService.getFetchTxsCountAndRatioByInput(date, token, isInput, finalFiltersObj).then(function (response) {
            getFetchTxsCountAndRatioByInputData.push({
                key: 'input',
                value: response.data.pagination.totalResults
            });
            isInput = {
                input: false
            };
            urlCalls.push(TransactionAnalysisApiService.getFetchTxsCountAndRatioByInput(date, token, isInput, finalFiltersObj).then(function (response2) {
                getFetchTxsCountAndRatioByInputData.push({
                    key: 'output',
                    value: response2.data.pagination.totalResults
                });
                inputOutputRatioPieChart(getFetchTxsCountAndRatioByInputData);
            }, function () {

            }));
        }, function () {

        }));
    }
    /*
     * @purpose: getAlertNotification
     * @created: 7 feb 2018
     * @return: no
     * @author: varsha
     */
    var getAlertNotificationData = [];

    function getAlertNotification(date) {
        urlCalls.push(TransactionAnalysisApiService.getAlertNotification(date, finalFiltersObj).then(function (response) {
            getAlertNotificationData = jQuery.extend(true, {}, response);
            loadProgressBarchart_PiechartNoti(response);
            loadProgressBarchart_PiechartTrans(response);
            loadNotiProgressBar(response);
            setTxTopThreeTransaction(response);
        }, function () {
            $scope.notification_transaction_type_progressData = [];
            $scope.notification_top_transaction_type_progressData = [];
        }));
    }

    function loadNotiProgressBar(response) {
        $scope.notification_top_transaction_type_progressData = [];
        var transmaxvalue = d3.max(response.data.txsAggRespDto.topFiveTransactions, function (d) {
            return d.amount;
        });

        $scope.notification_top_transaction_type_progressData = jQuery.extend(true, [], response.data.txsAggRespDto.topFiveTransactions);
        $scope.notification_top_transaction_type_progressData.map(function (d) {
            d.key = d.customerName,
                d.value = (d.amount / transmaxvalue) * 100,/* jshint ignore: line */
                d.amount = nFormatter(parseInt(d.amount))/* jshint ignore: line */

        });
        $scope.notification_top_transaction_type_progressData.sort(function (a, b) {
            return b.amount - a.amount;
        });
        $scope.notification_top_transaction_type_progressData.splice(3);

    }

    /*
     * @purpose: Load side Top Values
     * @created: 7 feb 2018
     * @return: no
     * @author: varsha
     */
    function topSidePanel(response) {
        $scope.transactionTabloader.transalertsChartloader = false;
        $scope.notificationtxsAmountToShow = '$' + ' ' + Number(parseInt(response.data.transactionsListAlertCountRatioDto.txAlertCountAmountAgg[0].totalTransactionAmount)).formatAmt(0);
        $scope.notificationtxsAmount = '$' + (new Intl.NumberFormat().format(response.data.transactionsListAlertCountRatioDto.txAlertCountAmountAgg[0].totalTransactionAmount)) || 'N/A';
        $scope.notificationStatusCountTotal = response.data.transactionsListAlertCountRatioDto.txAlertCountAmountAgg[0].alertCount || 'N/A';
        $scope.txsCount = response.data.transactionsListAlertCountRatioDto.txAlertCountAmountAgg[0].transactionsCount || 'N/A';

        /*Progress pie (notification top panel)*/
        $scope.pieValue = Math.round(response.data.transactionsListAlertCountRatioDto.txAlertCountAmountAgg[0].alertRatio) || 'N/A';

    }
    /*
     * @purpose: Load Top three transaction in transaction tab
     * @created: 7 feb 2018
     * @return: no
     * @author: varsha
     */

    function setTxTopThreeTransaction(response) {
        $scope.transactionTabloader.top3Transactionloader = false;
        $scope.trans_top_three_transaction_type_progressData = [];
        var maxvalue = d3.max(response.data.txsAggRespDto.topFiveTransactions, function (d) {
            return d.amount;
        });

        load_data(maxvalue);

        function load_data(maxvalue) {
            response.data.txsAggRespDto.topFiveTransactions.map(function (d) {
                d.key = d.customerName,
                    d.value = (d.amount / maxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.amount));/* jshint ignore: line */

            });
            $scope.trans_top_three_transaction_type_progressData = response.data.txsAggRespDto.topFiveTransactions;
            $scope.trans_top_three_transaction_type_progressData.sort(function (a, b) {
                return b.amount - a.amount;
            });
            $scope.trans_top_three_transaction_type_progressData.splice(3);
        }
    }

    /*
     * @purpose: Notification tab  Pie Chart and Barchart
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */
    var notiTransPieChartDta = [];

    function loadProgressBarchart_PiechartNoti(response) {
        /*  $scope.txAlertsCount = response.data.notificationDto.txsDataCountBwDates;*/

        /* load notification  top transaction type Progress Bar */
        $scope.notification_transaction_type_progressData = [];
        var maxnotivalue = d3.max(response.data.txsAggRespDto.alertProductTypeGroupByAndCountList, function (d) {
            return d.amountTotal;
        });

        loadnotData(maxnotivalue);

        function loadnotData(maxvalue) {
            $scope.transactipTypesPieChartPreloader = false;
            notiTransPieChartDta = jQuery.extend(true, [], response.data.txsAggRespDto.alertProductTypeGroupByAndCountList);
            notiTransPieChartDta.map(function (d) {
                d.key = d.transactionProductType;
                d.value = d.amountTotal;
            });
            if ($scope.current_tab == "notification") {
                notiTransPieChart(notiTransPieChartDta);
            }
            $scope.notification_transaction_type_progressData = jQuery.extend(true, [], response.data.txsAggRespDto.alertProductTypeGroupByAndCountList);
            $scope.notification_transaction_type_progressData.map(function (d) {
                d.key = d.transactionProductType,
                    d.value = (d.amountTotal / maxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.amountTotal))/* jshint ignore: line */

            });
            $scope.notification_transaction_type_progressData.sort(function (a, b) {
                return b.amountTotal - a.amountTotal;
            });
            $scope.notification_transaction_type_progressData.splice(3);
        }

        /*  load corporate structure Progress Bar notification tab*/

        var max_value = d3.max(response.data.txsAggRespDto.corporateStructureDto, function (d) {
            return d.transactionAmount;
        });

        load_Data(max_value);

        function load_Data(maxvalue) {
            response.data.txsAggRespDto.corporateStructureDto.sort(function (a, b) {
                return b.transactionAmount - a.transactionAmount;
            });
            response.data.txsAggRespDto.corporateStructureDto.map(function (d) {
                d.key = d.corporateStructure ? d.corporateStructure : d.type,
                    d.value = (d.transactionAmount / maxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.transactionAmount))/* jshint ignore: line */

            });

            $scope.notification_corporateStructureProgressBar = response.data.txsAggRespDto.corporateStructureDto;
            $scope.notification_corporateStructureProgressBar.splice(10);
        }

        /*load counterparties structure Progress Bar notification tab*/

        var counterpartiesmaxvalue = d3.max(response.data.txsAggRespDto.counterpartiesNotifDtos, function (d) {
            return d.amount;
        });

        counterload_Data(counterpartiesmaxvalue);

        function counterload_Data(maxvalue) {
            response.data.txsAggRespDto.counterpartiesNotifDtos.sort(function (a, b) {
                return b.amount - a.amount;
            });
            response.data.txsAggRespDto.counterpartiesNotifDtos.map(function (d) {
                d.key = d.customerName,
                    d.value = ((d.amount / maxvalue) * 100),/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.amount))/* jshint ignore: line */

            });
            $scope.notification_counterpartiesProgressBar = response.data.txsAggRespDto.counterpartiesNotifDtos;
            $scope.notification_counterpartiesProgressBar.splice(10);

        }

        /*load top transaction type piechart*/
        response.data.txsAggRespDto.alertStatusDto.map(function (d) {
            d.key = d.alertStatus,
                d.value = d.count/* jshint ignore: line */
        });

        var maxval = d3.max(response.data.txsAggRespDto.alertStatusDto, function (d) {
            return d.value;
        });
        var sum = d3.sum(response.data.txsAggRespDto.alertStatusDto, function (d) {
            return d.value;
        });
        var colors = ["#5d96c8", "#69ca6b", "#B753CD", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
        var alertsChartpieoption = { //Piechart option
            container: '#alertStatusChart',
            colors: colors,
            data: response.data.txsAggRespDto.alertStatusDto,
            height: 75,
            islegends: true,
            txtSize: '10px',
            //     islegendleft: true,
            legendwidth: 40,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtColor: '4D6772',
            legendmargintop: 30
        };

        new reusablePie(alertsChartpieoption); //load pie chart

    }

    /*
     * @purpose: Transaction tab  Pie Chart and Barchart
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */

    function loadProgressBarchart_PiechartTrans(response) {
        $scope.transactionTabloader.topTransactionTypesloader = false;
        //load transaction top transaction type Progress Bar 
        $scope.transaction_transaction_type_progressData = [];
        var maxvalue = d3.max(response.data.txsAggRespDto.transactionsProductTypeGroupByAndCountList, function (d) {
            return d.amountTotal;
        });

        loadData(maxvalue);

        function loadData(maxvalue) {

            //  notiTransPieChart(response.data.txsAggRespDto.alertProductTypeGroupByAndCountList)
            $scope.transaction_transaction_type_progressData = jQuery.extend(true, [], response.data.txsAggRespDto.transactionsProductTypeGroupByAndCountList);
            $scope.transaction_transaction_type_progressData.map(function (d) {
                d.key = d.transactionProductType,
                    d.value = (d.amountTotal / maxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.amountTotal))/* jshint ignore: line */

            });
            $scope.transaction_transaction_type_progressDataPie = jQuery.extend(true, [], response.data.txsAggRespDto.transactionsProductTypeGroupByAndCountList);
            $scope.transaction_transaction_type_progressDataPie.map(function (d) {
                d.key = d.transactionProductType;
                d.value = d.amountTotal;

            });
            //console.log( $scope.transaction_transaction_type_progressDataPie ," $scope.transaction_transaction_type_progressDataPie  $scope.transaction_transaction_type_progressDataPie ")
            //transationTransPieChart( $scope.transaction_transaction_type_progressDataPie)
            $scope.transaction_transaction_type_progressData.sort(function (a, b) {
                return b.amountTotal - a.amountTotal;
            });
            $scope.transaction_transaction_type_progressData.splice(3);
        }

    }

    /*
     * @purpose: Notification tab Transaction Pie Chart
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */

    function notiTransPieChart(data) {

        var maxval = d3.max(data, function (d) {
            return d.value;
        });
        var sum = d3.sum(data, function (d) {
            return d.value;
        });
        d3.map(data, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var colors = ["#5d96c8", "#69ca6b", "#B753CD", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
        var piechartoption2 = { //Piechart option
            container: '#TransactipTypesPieChart',
            colors: colors,
            data: data,
            height: 160,
            islegends: true,
            //     islegendleft: true,
            legendwidth: 30,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtColor: '#4D6772',
            showValue: 'noValue',
            legendmargintop: 30,
            format: true
        };
        $scope.transactipTypesPieChartPreloader = false;
        new reusablePie(piechartoption2); //load pie chart
    }

    /*
     * @purpose: load Charts in notification tab
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */

    setTimeout(function () {
        //load datePicker
        var start = moment().subtract(2, 'year');
        var end = moment();
        initializeDatePicker('notification', start, end);
    });

    function plotNotifyRiskPie() {
        var maxval = d3.max($scope.NotifyRiskPieDate, function (d) {
            return d.value;
        });
        var sum = d3.sum($scope.NotifyRiskPieDate, function (d) {
            return d.value;
        });
        d3.map($scope.NotifyRiskPieDate, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var colors = ["#5d96c8", "#69ca6b", "#B753CD", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
        var piechartoption2 = { //Piechart option
            container: '#RiskPieChart',
            colors: colors,
            data: $scope.NotifyRiskPieDate,
            height: 160,
            islegends: true,
            //     islegendleft: true,
            legendwidth: 30,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtColor: '4D6772',
            legendmargintop: 30,
            showValue: 'noValue'
        };
        $scope.riskPieChartPreloader = false;
        new reusablePie(piechartoption2); //load pie chart
    }


    /*
     * @purpose: set data for alert comparison chart
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */


    var alertComparison = [];

    function alertComparisonNotification(date, data) {
        if (!data) {
            alertComparison = [];
            $scope.barchartnotificationLegendData = [];
            urlCalls.push(TransactionAnalysisApiService.alertComparisonNotification(date, finalFiltersObj).then(function (response) {
                alertComparison = [];
                angular.forEach(response.data, function (d) {
                    var value, pastVal;
                    if (d.present == null) {
                        value = 0;
                    } else {
                        value = d.present.count;
                    }
                    if (d.past == null) {
                        pastVal = 0;
                    } else {
                        pastVal = d.past.count;
                    }
                    alertComparison.push({
                        name: d.past ? d.past.productType : d.present.productType,
                        value: value - pastVal
                    });
                });
                if ($scope.current_tab == "notification") {
                    loadNotificationComparisonChart();
                }
            }));
        } else {
            if ($scope.current_tab == "notification") {
                loadNotificationComparisonChart();
            }
        }
    }
    /*
     * @purpose: loads alert comparison chart
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */
    function loadNotificationComparisonChart() {

        var color = ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d", "#669900", "#334433"];
        var options = {
            container: '#barchartnotification',
            height: 175,
            marginRight: 5,
            marginLeft: 5,
            marginBottom: 1,
            marginTop: 1,
            width: $('#barchartnotification').parent().width(),
            data: alertComparison,
            color: color
        };
        $scope.barchartnotificationPreloader = false;
        if ($(options.container).width() > 0) {
            verticalBarChart(options);
        }
    }

    $scope.loadChart_notification = function (isTxTab) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (isTxTab) {
            $scope.current_tab = "notification";
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }

        if ($scope.current_tab == "notification") {
            setTimeout(function () {
                topSidePanel(getFetchTxsCountAndRatioData);
                plotNotifyRiskPie();
                loadProgressBarchart_PiechartNoti(getAlertNotificationData);
                loadProgressBarchart_PiechartTrans(getAlertNotificationData);
                setTxTopThreeTransaction(getAlertNotificationData);
                topSidePanel(getFetchTxsCountAndRatioData);
                notiTransPieChart(notiTransPieChartDta);
                alertComparisonNotification('', alertComparison);
                loadNotiTagCloud(notiTagCloudData);
                loadLinechart('#linechartnotification', 150, $scope.getTransactionAndAlertAggregates);
                World($scope.conterPartyLocNotifyOpts.counterpartyWorldMap);

            });
        }
    };

    //-------------------------------------Dashboard Tab Stars---------------------------------------------

    /*
     * @purpose:Get response for alertStatues in dahboard tab
     * @created: 21 feb 2018
     * @return: no
     * @author: varsha
     */

    var dashboardAlertByStatusData = [];

    function dashboardAlertByStatus(date) {
        urlCalls.push(TransactionAnalysisApiService.getTransactionAndAlertAggregates(date, "monthly", "", finalFiltersObj).then(function (response) {
            dashboardAlertByStatusData = response;
            $scope.loader.transfersByMonthChartPreloader = false;
            $scope.lineChartPreloader = false;
            if ($scope.current_tab == "aml") {
                loadLinechart('#transfersByMonthChart', 150, dashboardAlertByStatusData);
            }
        }, function () {

        }));

    }

    function removeDuplicatesByKey(arr, productType) {
        var ar = arr.filter(function (obj) {
            obj.type === productType;/* jshint ignore: line */
        });
        var splicedArray = ar.splice(ar.length - 1);
        var notCurrency = arr.filter(function (obj) {
            obj.type === productType;/* jshint ignore: line */
        });
        var res = splicedArray.concat(notCurrency);
        typeList = res;
        return res;
    }



    //-------------------------------------Transaction Tab Starts---------------------------------------------------------------//		    
    /*
     * @purpose:Get getTotalAmountAndAlertCountByProductType
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */
    var typeList = [];
    $scope.inputOutputProgress.txDetails = [];

    function getTransactionAndAlertByProductType(date, productCheckedOption, productType) {
        var granularity = getGranularity(date);
        urlCalls.push(TransactionAnalysisApiService.getTotalAmountAndAlertCountByProductType(date, productCheckedOption, productType, granularity, finalFiltersObj).then(function (response) {
            if (productCheckedOption) {
                if (productCheckedOption != 'ALL') {
                    angular.forEach($scope.transactionTypesDetails.txDetails, function (val) {
                        if (val.transactionProductType == response.data[0].transactionProductType) {
                            val = response.data[0].transactionProductType;
                        }
                    });
                    angular.forEach($scope.inputOutputProgress.txDetails, function (val) {
                        if (val.transactionProductType == response.data[0].transactionProductType) {
                            val = response.data[0].transactionProductType;
                        }
                    });
                    angular.forEach($scope.trans_alert_ratio_data, function (val) {
                        if (val.transactionProductType == response.data[0].transactionProductType) {
                            val = response.data[0].transactionProductType;
                        }
                    });
                } else {
                    $scope.transactionTypesDetails.txDetails = jQuery.extend(true, [], response.data);
                    $scope.inputOutputProgress.txDetails = jQuery.extend(true, [], response.data);
                    $scope.trans_alert_ratio_data = jQuery.extend(true, [], response.data);
                }
                typeList.push({
                    'type': productType,
                    'option': productCheckedOption
                });
                var finalArray = removeDuplicatesByKey(typeList, productType);/* jshint ignore: line */
                angular.forEach($scope.transactionTypesDetails.txDetails, function (val) {
                    angular.forEach(typeList, function (v) {
                        if (val.transactionProductType == v.type) {
                            val.checkedOption = v.option;
                        } else {

                        }
                    });
                });
            } else {
                $scope.transactionTypesDetails.txDetails = jQuery.extend(true, [], response.data);
                $scope.inputOutputProgress.txDetails = jQuery.extend(true, [], response.data);
                $scope.trans_alert_ratio_data = jQuery.extend(true, [], response.data);
            }

            //            angular.forEach($scope.inputOutputProgress.txDetails, function(v, k){
            //            	v.inputRatio = (v.inputCount/(v.inputCount + v.outputCount))* 100;
            //            });
            $scope.inputOutputProgress.txDetails.map(function (v) {
                v.calinputvalue = (v.inputCount / (v.inputCount + v.outputCount)) * 100;
            });
            $scope.transactionTabloader.alertRatioloader = false;

            $scope.trans_alert_ratio_data.map(function (d) {
                d.key = d.transactionProductType,
                    d.value = d.alertRatio,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.alertRatio))/* jshint ignore: line */

            });
            $scope.trans_alert_ratio_data.sort(function (a, b) {
                return b.alertRatio - a.alertRatio;
            });
            $scope.trans_alert_ratio_data.splice(3);
            //call API to get amount and number details
            urlCalls.push(TransactionAnalysisApiService.getTransactionAndAlertByProductType(date, productCheckedOption, productType, granularity, finalFiltersObj).then(function (response) {

                if (productCheckedOption) {
                    var resultedData = $scope.transactionTypesDetails.txAmountNumberDetails;
                    resultedData[productType] = response.data[productType];
                } else {
                    var resultedData = response.data;
                }
                if (granularity == "weekly") {
                    var keys = Object.keys(resultedData);
                    angular.forEach(keys, function (d) {
                        resultedData[d].alertAggregator.map(function (d) {
                            return d.businessDate = getDateOfISOWeek(d.businessDate.split('-')[1], d.businessDate.split('-')[0]);/* jshint ignore: line */
                        });
                        resultedData[d].transactionAggregator.map(function (d) {
                            return d.businessDate = getDateOfISOWeek(d.businessDate.split('-')[1], d.businessDate.split('-')[0]);/* jshint ignore: line */
                        });
                    });
                }

                $scope.transactionTypesDetails.txAmountNumberDetails = resultedData;
                $scope.inputOutputProgress.txAmountNumberDetails = resultedData;

                if ($scope.current_tab == "transaction") {
                    $scope.loadChart_transaction();
                }
            }));
        }, function () {
            $scope.inputOutputProgress.txDetails = [];
        }));
    }

    /*
     * @purpose: open view all transaction
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */

    $scope.viewAllTrans = function (productType, totalTransactions, totalAmount, alerted, event, header) {
        if (productType == 'ActivityType' || productType == 'Banks' || productType == 'GeoGraphic' || productType == 'BankLocations' || productType == 'CorporateStructure' || productType == 'CustomerRisk' || productType == 'ProductRisk' || productType == 'CorruptionRisk' || productType == 'PoliticalRisk' || productType == 'GeoGraphicRisk' || productType == "Geography" || productType == "Shareholders") {
            $scope.chk = '';
        } else {
            var p = event.currentTarget.parentElement.parentElement;
            var q = p.getElementsByClassName('active');
            $scope.chk = q[0].innerText;
        }
        var txsdetails = {
            "totalTransactions": totalTransactions,
            "totalAmount": totalAmount,
            "alerted": alerted
        };
        checkedTypeDetails = {
            'allOption': $scope.allOption,
            'inputOption': $scope.inputOption,
            'outputOption': $scope.outputOption
        };
        var viewByDetails = {
            'viewBy': $scope.chk
        };
        var viewAllTransModal = $uibModal.open({
            templateUrl: 'viewAllTrans.modal.html',
            controller: 'ViewAllTransModalController',
            backdrop: 'static',
            size: 'sm',
            windowClass: 'custom-modal mip-save-modal mip-save-wrapper creatAlertModalWrapper allTransactionsModal',
            resolve: {
                productType: function () {
                    return productType;
                },
                fromDt: function () {
                    return $scope.fromDt;
                },
                toDt: function () {
                    return $scope.toDt;
                },
                txsdetails: function () {
                    return txsdetails;
                },
                checkedTypeDetails: function () {
                    return checkedTypeDetails;
                },
                viewByDetails: function () {
                    return viewByDetails;
                },
                header: function () {
                    return header;
                },
                filtersObj: function () {
                    return finalFiltersObj;
                }
            }
        });

        viewAllTransModal.result.then(function () {

        }, function () {

        });
    };

    /* To check different view by options  */

    $scope.checkActive = function (event, productType) {
        var parent = event.currentTarget.parentElement;
        var grandparent = parent.parentElement;
        var li = grandparent.querySelectorAll('li');
        var i;
        for (i = 0; i < li.length; i++) {
            li[i].className = '';
        }
        parent.className = 'active';

        var p = event.currentTarget.parentElement.parentElement;
        var q = p.getElementsByClassName('active');
        $scope.chk = q[0].innerText;
        $scope.productType = productType;
        getTransactionAndAlertByProductType($scope.selectedDate, $scope.chk, productType);
    };

    /* end on viewby options */

    $scope.allOption = true;
    $scope.inputOption = false;
    $scope.outputOption = false;
    var checkedTypeDetails;

    $scope.viewByAllTxs = function (option) {
        if (option == 'All') {
            $scope.allOption = true;
            $scope.inputOption = false;
            $scope.outputOption = false;
        } else if (option == 'Input') {
            $scope.allOption = false;
            $scope.inputOption = true;
            $scope.outputOption = false;
        } else {
            $scope.allOption = false;
            $scope.inputOption = false;
            $scope.outputOption = true;
        }
        checkedTypeDetails = {
            'allOption': $scope.allOption,
            'inputOption': $scope.inputOption,
            'outputOption': $scope.outputOption
        };
        getTransactionAndAlertAggregates($scope.selectedDate, checkedTypeDetails);
        var dateWithOptions = [$scope.selectedDate[0], $scope.selectedDate[1], checkedTypeDetails];
        getFetchTxsCountAndRatio(dateWithOptions);
    };


    /*
     * @purpose: Transaction tab  transaction type pie chart
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */
    function transationTransPieChart() {
        var data = $scope.transaction_transaction_type_progressDataPie;
        var maxval = d3.max(data, function (d) {
            return d.value;
        });
        var sum = d3.sum(data, function (d) {
            return d.value;
        });
        d3.map(data, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var colors = ["#5d96c8", "#69ca6b", "#B753CD", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
        var piechartoption2 = { //Piechart option
            container: '#Transaction_pieChart',
            colors: colors,
            data: data,
            height: 180,
            islegends: true,
            //     islegendleft: true,
            legendwidth: 30,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtColor: '4D6772',
            showValue: 'noValue',
            format: true,
            legendmargintop: 30
        };

        new reusablePie(piechartoption2); //load pie chart
        $scope.transactionTabloader.transaction_pieChartloader = false;
    }

    /*
     * @purpose: Transaction tab  top panel input/output  pie chart
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */
    function inputOutputRatioPieChart(data) {
        var maxval = d3.max(data, function (d) {
            return d.value;
        });
        var sum = d3.sum(data, function (d) {
            return d.value;
        });
        d3.map(data, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var colors = ["#5d96c8", "#3472A8"];
        var piechartoption2 = {
            container: '#inputOutputRatio',
            colors: colors,
            data: data,
            height: 70,
            islegends: true,
            legendwidth: 45,
            txtColor: '4D6772',
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtSize: '10px',
            showValue: 'noValue',
            legendmargintop: 20
        };
        $scope.transactionTabloader.inputOutputRatioloader = false;
        new reusablePie(piechartoption2);
    }

    /*
     * @purpose: Transaction tab  alert comparison negative bar chart
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */

    var alertComparisonTrans = [];
    function alertComparisonTransactions(date) {
        alertComparisonTrans = [];
        $scope.transactionComparisonChartLegendData = [];
        urlCalls.push(TransactionAnalysisApiService.alertComparisonTransactions(date, finalFiltersObj).then(function (response) {
            alertComparisonTrans = [];

            angular.forEach(response.data, function (d) {
                var value, pastVal;
                if (d.present == null) {
                    value = 0;
                } else {
                    value = d.present.count;
                }
                if (d.past == null) {
                    pastVal = 0;
                } else {
                    pastVal = d.past.count;
                }
                alertComparisonTrans.push({
                    name: d.past ? d.past.productType : d.present.productType,
                    value: value - pastVal
                });
            });
            if ($scope.current_tab == "transaction") {
                LoadComparisionChartTransaction();
            }
        }));

    }
    /*
     * @purpose: Transaction tab  alert comparison negative bar chart
     * @created: 9 feb 2018
     * @return: no
     * @author: varsha
     */
    function LoadComparisionChartTransaction() {
        if (alertComparisonTrans.length > 0) {
            var color = ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
            var options = {
                container: '#transactionComparisonChart',
                height: 160,
                marginRight: 5,
                marginLeft: 5,
                marginBottom: 1,
                marginTop: 1,
                width: $('#transactionComparisonChart').parent().width(),
                data: alertComparisonTrans,
                color: color
            };
            $scope.transactionTabloader.transactionComparisonChartloader = false;
            if ($(options.container).width() > 0) {
                verticalBarChart(options);
            }
        } else {
            $scope.transactionTabloader.transactionComparisonChartloader = false;
            $scope.transactionComparisonChartLegendData = [];
            $("#transactionComparisonChart").empty();
            d3.select('#transactionComparisonChart').append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");
        }
    }
    /*
     * @purpose:load charts in aml tab
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */
    $scope.loadChart_aml = function (isAml) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (isAml) {
            $scope.current_tab = "aml";
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }

        if ($scope.current_tab == "aml") {
            topSidePanel(getFetchTxsCountAndRatioData);
            loadLinechart('#transfersByMonthChart', 150, dashboardAlertByStatusData);
            var colors = ["#3A64AA", "#608CD3", "#71A0E7"];
            if ($scope.aml_top_scenarios_pieData.length > 0) {
                plotPie($scope.aml_top_scenarios_pieData, '#topScenariosChart', colors, 75, colors[2]);
            } else {
                $('#topScenariosChart').empty();
                d3.select('#topScenariosChart').append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");
            }
            if ($scope.amlAlertByStatusData.length > 0) {
                var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                plotPie($scope.amlAlertByStatusData, '#alertsByStatuschart', colors, 160, colors[1]);
            } else {
                $scope.alertsByStatuschartLegendData = [];
                $('#alertsByStatuschart').empty();
                d3.select('#alertsByStatuschart').append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");

            }
            if ($scope.amlAlertByPeriod.length > 0) {
                var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                plotPie($scope.amlAlertByPeriod, '#alertsByPeriodPieChart', colors, 160, colors[1]);
            } else {
                $scope.alertsByPeriodPieChartLegendData = [];
                $('#alertsByPeriodPieChart').empty();
                d3.select('#alertsByPeriodPieChart').append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");

            }
            if ($scope.alertsByStatusGreaterThan30.length > 0) {
                var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                plotPie($scope.alertsByStatusGreaterThan30, '#alertsByStatus30Chart', colors, 160, colors[1]);
            } else {
                $scope.alertsByStatus30ChartLegendData = [];
                $('#alertsByStatus30Chart').empty();
                d3.select('#alertsByStatus30Chart').append("div").attr("class", "alertsDashErrorDiv").html("<span>Data Not Found</span>");

            }
            if ($scope.amlLocationOtions) {
                World($scope.amlLocationOtions);
            }
        }

    };
    /*
     * @purpose:load charts in transaction tab
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */
    $scope.loadChart_transaction = function (isTxTab) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (isTxTab) {
            $scope.current_tab = "transaction";
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }

        if ($scope.current_tab == "transaction") {
            topSidePanel(getFetchTxsCountAndRatioData);
            if ($scope.transaction_transaction_type_progressDataPie != undefined) {
                transationTransPieChart();
            }

            setTimeout(function () {
                if ($(".chart-legends-transact")) {
                    $(".chart-legends-transact").mThumbnailScroller({
                        axis: "x"
                    });
                }
            }, 2000);
            //load pie chart
            var colors = ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d", "#669900", "#334433"];
            var monthlyTurnoverPieChartcolorArray = [];
            angular.forEach($scope.MonthlyTurnOverData, function (d, i) {
                if (d.value != 0) {
                    monthlyTurnoverPieChartcolorArray[d.key] = colors[i];
                }
            });
            $scope.monthlyTurnoverPieChartcolorObj = jQuery.extend(true, {}, monthlyTurnoverPieChartcolorArray);
            var maxval = d3.max($scope.MonthlyTurnOverData, function (d) {
                return d.value;
            });
            var sum = d3.sum($scope.MonthlyTurnOverData, function (d) {
                return d.value;
            });
            d3.map($scope.MonthlyTurnOverData, function (d) {
                d.piePercentage = parseInt((d.value / sum) * 100);
            });
            var piechartoption3 = { //Piechart option
                container: '#monthlyTurnoverPieChart',
                colors: colors,
                data: $scope.MonthlyTurnOverData,
                height: 150,
                islegends: false,
                //     islegendleft: true,
                legendwidth: 30,
                istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
                txtColor: '4D6772',
                showValue: 'noValue',
                legendmargintop: 30
            };
            $scope.transactionTabloader.monthlyTurnoverPieChartloader = false;
            new reusablePie(piechartoption3);
            LoadComparisionChartTransaction();
            angular.forEach($scope.transactionTypesDetails.txDetails, function (val) {
                var id = val.transactionProductType.split(' ').join('--');
                var lineId = "#amountChart_" + id;
                var currentData = {
                    data: $scope.transactionTypesDetails.txAmountNumberDetails[val.transactionProductType]
                };
                var barId = "#countChart_" + id;
                loadLinechart(lineId, 230, currentData);
                loadBarChart(currentData, 245, barId);
            });

        }

    };


    /*
     * @purpose:load charts in transaction tab
     * @created: 2 feb 2018
     * @return: no
     * @author: varsha
     */
    $scope.loadChart_transactionNew = function (isTxTab) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (isTxTab) {
            $scope.current_tab = "transaction";
        }
        if (window.isTabChangeFromContextTrans == true) {
            window.isTabChangeFromContextTrans == false;/* jshint ignore: line */
            return false;
        }

        if (getFetchTxsCountAndRatioByInputData != undefined) {
            inputOutputRatioPieChart(getFetchTxsCountAndRatioByInputData);
        }
        loadLinechart('#transLineChart', 200, $scope.getTransactionAndAlertAggregates); //height=200
        loadBarChart($scope.getTransactionAndAlertAggregates, 215, '#barchart1');
    };

    //-------------------------------------Risk Tab Starts---------------------------------------------------------------//		    
    /*
     * @purpose: Side modal popUp on click of bubble
     * @created: 3 feb 2018
     * @return: no
     * @author: varsha
     */

    window.openSideModal = function (data, type, color) {
        $uibModalStack.dismissAll();
        var newwindowclass = '';
        if (scrollposition > 60) {
            newwindowclass = 'custom-modal right modal-top';
        } else {
            newwindowclass = 'custom-modal right';
        }
        var openSideModal = $uibModal.open({
            templateUrl: 'sideRisk.modal.html',
            controller: 'SideRiskModalController',
            backdrop: false,
            size: 'sm',
            windowClass: newwindowclass,
            resolve: {
                data: function () {
                    return data;
                },
                type: function () {
                    return type;
                },
                date: function () {
                    return $scope.Date;
                },
                color: function () {
                    return color;
                },
                filtersObj: function () {
                    return finalFiltersObj;
                }


            }

        });
        openSideModal.result.then(function () {

        }, function () {

        });
    };

    /*
     * @purpose:Get riskRatioChart
     * @created: 13 feb 2018
     * @return: no
     * @author: varsha
     */
    var customerRisk = [],/* jshint ignore: line */
        productRisk = [],/* jshint ignore: line */
        corruptionRisk = [];/* jshint ignore: line */

    function riskRatioChart(date) {
        $scope.NotifyRiskPieDate = [];
        urlCalls.push(TransactionAnalysisApiService.riskRatioChart(date, finalFiltersObj).then(function (response) {
            response.data.map(function (d) {
                d.key = d.type,
                    d.value = d.alertCount/* jshint ignore: line */
            });
            $scope.NotifyRiskPieDate = response.data;
            //   $scope.loadChart_risk();
            plotNotifyRiskPie();
            if ($("#riskPieChartOne").width() > 0) {
                plotRiskPie(response.data);
            }


        }));

    }
    /*
     * @purpose:Get Customer Risk Counts
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    function getCustomerRiskCounts(date) {
        urlCalls.push(TransactionAnalysisApiService.getCustomerRiskCounts(date, finalFiltersObj).then(function (response) {
            $scope.CustomerRisk = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount
            };

        }, function () {
            $scope.CustomerRisk = {};
        }));

    }
    /*
     * @purpose:Get Customer Risk Aggregates
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    var customerRiskAggregatesbubbledata = {};
    var customerRiskAggregatespoliticiandata = [];
    var customerRiskPieData = [];

    function getCustomerRiskAggregates(date) {
        customerRiskAggregatespoliticiandata = [];
        customerRiskAggregatesbubbledata = {};
        customerRiskPieData = [];
        urlCalls.push(TransactionAnalysisApiService.getCustomerRiskAggregates(date, finalFiltersObj).then(function (response) {
            customerRiskAggregatesbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                customerRiskAggregatesbubbledata.children.push({
                    "name": value.type,
                    "size": value.alertAmount
                });
                customerRiskAggregatespoliticiandata.push({
                    "letter": value.type,
                    "frequency": value.alertCount
                });
                customerRiskPieData.push({
                    "key": value.type,
                    "value": parseFloat(value.alertCount)
                });
            });
            customerRiskPieData.sort(function (a, b) {
                return b.value - a.value;
            });
            riskSidePanelProgressBarChart.CustomerRisk = response.data;
            $scope.riskTabloader.customerTransactionChartloader = false;
            plotRiskSidePanelProgressBarChart(riskSidePanelProgressBarChart);
            var colors = ["#8F4582", "#A25394", "#CC6BBB"];
            setTimeout(function () {
                plotPie(customerRiskPieData, '#riskTypeChart', colors, 75); /*  data,color,height*/
            }, 0);
            $scope.riskTabloader.risk_pieChartloader = false;
            $scope.riskTabloader.riskBloomBergChartOneloader = false;
            $scope.riskTabloader.riskBarChartOneloader = false;
            setTimeout(function () {
                if ($('#riskBloomBergChartOne').width() > 0) {
                    plotBubbleChart('#riskBloomBergChartOne', ['#8F4582'], customerRiskAggregatesbubbledata);
                }
                if ($('#riskBarChartOne').width() > 0) {
                    plotBarChart("#riskBarChartOne", "#8F4582", customerRiskAggregatespoliticiandata);
                }
            }, 2000);

        }, function () {
            customerRiskAggregatespoliticiandata = [];
            customerRiskAggregatesbubbledata = {};
            customerRiskPieData = [];
        }));
    }

    /*
     * @purpose:Get GeoGraphic Count Ratio
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    function getGeoGraphicCountRatio(date) {
        urlCalls.push(TransactionAnalysisApiService.getGeoGraphicCountRatio(date, finalFiltersObj).then(function (response) {
            $scope.GeoGraphicRisk = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount

            };

        }, function () {
            $scope.GeoGraphicRisk = {};
        }));

    }

    /*
     * @purpose: Get GeoGraphic Aggregates
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    var geographicRiskAggregatesbubbledata = {};
    var geographicRiskAggregatespoliticiandata = [];
    var geoRiskPieData = [];

    function getGeoGraphicAggregates(date) {
        geographicRiskAggregatesbubbledata = {};
        geographicRiskAggregatespoliticiandata = [];
        geoRiskPieData = [];
        urlCalls.push(TransactionAnalysisApiService.getGeoGraphicAggregates(date, finalFiltersObj).then(function (response) {
            geographicRiskAggregatesbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                if (value.alertAmount) {

                    geographicRiskAggregatesbubbledata.children.push({

                        "name": value.type,
                        "size": value.alertAmount

                    });
                    geoRiskPieData.push({
                        "key": value.type,
                        "value": parseFloat(value.alertCount)
                    });
                }
                if (value.alertCount) {
                    geographicRiskAggregatespoliticiandata.push({
                        "letter": value.type,
                        "frequency": value.alertCount
                    });
                }

            });
            riskSidePanelProgressBarChart.GeoGraphicRisk = response.data;
            $scope.riskTabloader.geoTransactionChartloader = false;
            plotRiskSidePanelProgressBarChart(riskSidePanelProgressBarChart);
            geoRiskPieData.sort(function (a, b) {
                return b.value - a.value;
            });

            //pie code starts here
            var colors = ["#8F4582", "#A25394", "#CC6BBB"];
            plotPie(geoRiskPieData, '#riskMfFtChart', colors, 75); /*  data,color,height*/
            $scope.riskTabloader.risk_geopieChartloader = false;
            //pie ends here
            $scope.riskTabloader.riskBloomBergChartThreeloader = false;
            $scope.riskTabloader.riskBarChartThreeloader = false;
            setTimeout(function () {
                if ($('#riskBloomBergChartThree').width() > 0) {
                    plotBubbleChart('#riskBloomBergChartThree', ['#8F4582'], geographicRiskAggregatesbubbledata);
                }
                if ($('#riskBarChartThree').width() > 0) {
                    plotBarChart("#riskBarChartThree", "#8F4582", geographicRiskAggregatespoliticiandata);
                }
            }, 2000);
        }, function () {
            geographicRiskAggregatesbubbledata = {};
            geographicRiskAggregatespoliticiandata = [];
            geoRiskPieData = [];
        }));
    }

    /*
     * @purpose:Get CorruptionRisk
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    function getCorruptionRisk(date) {
        urlCalls.push(TransactionAnalysisApiService.getCorruptionRisk(date, finalFiltersObj).then(function (response) {
            $scope.CorruptionRisk = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount

            };
        }, function () {
            $scope.CorruptionRisk = {};
        }));
    }

    /*
     * @purpose: Get CorruptionRiskByType
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    var corruptionRiskbubbledata = {};
    var corruptionRiskpoliticiandata = [];

    function getCorruptionRiskAggregates(date) {
        corruptionRiskbubbledata = {};
        corruptionRiskpoliticiandata = [];
        urlCalls.push(TransactionAnalysisApiService.getCorruptionRiskAggregates(date, finalFiltersObj).then(function (response) {
            corruptionRiskbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                if (value.alertAmount) {
                    corruptionRiskbubbledata.children.push({
                        "name": value.type,
                        "size": value.alertAmount
                    });
                }
                if (value.alertCount) {
                    corruptionRiskpoliticiandata.push({
                        "letter": value.type,
                        "frequency": value.alertCount
                    });
                }

            });
            $scope.riskTabloader.riskBloomBergChartFourloader = false;
            $scope.riskTabloader.riskBarChartFourloader = false;
            setTimeout(function () {
                if ($('#riskBloomBergChartFour').width() > 0) {
                    plotBubbleChart('#riskBloomBergChartFour', ['#8F4582'], corruptionRiskbubbledata);
                }
                if ($('#riskBarChartFour').width() > 0) {
                    plotBarChart("#riskBarChartFour", "#8F4582", corruptionRiskpoliticiandata);
                }
            }, 2000);
        }, function () {
            corruptionRiskbubbledata = {};
            corruptionRiskpoliticiandata = [];
        }));

    }

    /*
     * @purpose:Get CorruptionRisk
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    function getPoliticalRisk(date) {
        urlCalls.push(TransactionAnalysisApiService.getPoliticalRisk(date, finalFiltersObj).then(function (response) {
            $scope.PoliticalRisk = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount
            };

        }, function () {
            $scope.PoliticalRisk = {};
        }));
    }

    /*
     * @purpose: Get CorruptionRiskByType
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    var politicalRiskbubbledata = {};
    var politicalRiskpoliticiandata = [];

    function getPoliticalRiskAggregates(date) {
        politicalRiskbubbledata = {};
        politicalRiskpoliticiandata = [];
        urlCalls.push(TransactionAnalysisApiService.getPoliticalRiskAggregates(date, finalFiltersObj).then(function (response) {
            politicalRiskbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                if (value.alertAmount) {
                    politicalRiskbubbledata.children.push({
                        "name": value.type,
                        "size": value.alertAmount
                    });
                }
                if (value.alertCount) {
                    politicalRiskpoliticiandata.push({
                        "letter": value.type,
                        "frequency": value.alertCount
                    });
                }

            });
            $scope.riskTabloader.riskBloomBergChartFiveloader = false;
            $scope.riskTabloader.riskBarChartFiveloader = false;
            setTimeout(function () {
                if ($('#riskBloomBergChartFive').width() > 0) {
                    $scope.riskTabloader.riskBloomBergChartFiveloader = false;
                    plotBubbleChart('#riskBloomBergChartFive', ['#8F4582'], politicalRiskbubbledata);
                }
                if ($('#riskBarChartFive').width() > 0) {
                    plotBarChart("#riskBarChartFive", "#8F4582", politicalRiskpoliticiandata);
                }

            }, 2000);
        }, function () {
            politicalRiskbubbledata = {};
            politicalRiskpoliticiandata = [];
        }));
    }
    /*
     * @purpose:Get CorruptionRisk
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    function getProductRisk(date) {
        urlCalls.push(TransactionAnalysisApiService.getProductRisk(date, finalFiltersObj).then(function (response) {

            $scope.ProductRisk = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount
            };

        }, function () {
            $scope.ProductRisk = {};
        }));
    }
    /*
     * @purpose: Get CorruptionRiskByType
     * @created: 10 feb 2018
     * @return: no
     * @author: varsha
     */
    var ProductRiskbubbledata = {};
    var ProductRiskbardata = [];
    var productRiskPieData = [];

    function getProductRiskAggregates(date) {
        ProductRiskbubbledata = {};
        ProductRiskbardata = [];
        productRiskPieData = [];
        urlCalls.push(TransactionAnalysisApiService.getProductRiskAggregates(date, finalFiltersObj).then(function (response) {
            ProductRiskbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                ProductRiskbubbledata.children.push({
                    "name": value.type,
                    "size": value.alertAmount
                });
                ProductRiskbardata.push({
                    "letter": value.type,
                    "frequency": value.alertCount
                });
                productRiskPieData.push({
                    "key": value.type,
                    "value": parseFloat(value.alertCount)
                });

            });
            riskSidePanelProgressBarChart.ProductRisk = response.data;
            $scope.riskTabloader.productTransactionChartloader = false;
            plotRiskSidePanelProgressBarChart(riskSidePanelProgressBarChart);
            //plot top pie starts here
            productRiskPieData.sort(function (a, b) {
                return b.value - a.value;
            });
            var colors = ["#8F4582", "#A25394", "#CC6BBB"];
            $scope.riskTabloader.risk_productpieChartloader = false;
            plotPie(productRiskPieData, '#productRiskChart', colors, 75); /*  data,color,height*/
            //plot top pie ends here

            $scope.riskTabloader.riskBloomBergChartTwoloader = false;
            $scope.riskTabloader.riskBarChartTwoloader = false;
            setTimeout(function () {
                if ($('#riskBloomBergChartTwo').width() > 0) {
                    plotBubbleChart('#riskBloomBergChartTwo', ['#B13161'], ProductRiskbubbledata);
                }
                if ($('#riskBarChartTwo').width() > 0) {
                    plotBarChart("#riskBarChartTwo", "#B13161", ProductRiskbardata);
                }
            }, 2000);

        }, function () {
            ProductRiskbubbledata = {};
            ProductRiskbardata = [];
            productRiskPieData = [];
        }));
    }

    function plotBubbleChart(id, color, data) {
        var bubbleSocialOptions1 = jQuery.extend(true, {}, bubbleSocialOptions);
        bubbleSocialOptions1.container = id;
        bubbleSocialOptions1.bubbleColor = color;
        bubbleSocialOptions1.data = data;
        BubbleHierarchyChart(bubbleSocialOptions1);
    }
    function plotBarChart(id, color, data, type) {
        var politiciansOptions1 = jQuery.extend(true, {}, politiciansOptions);
        politiciansOptions1.container = id;
        politiciansOptions1.colors = color;
        politiciansOptions1.data = data;
        politiciansOptions1.type = type;
        simpleVerticalBarChart(politiciansOptions1);
    }

    function plotPie(data, id, colors, height, txtColor, islegends, legendwidth, legendmargintop, noSplice) {
        var pieData = jQuery.extend(true, [], data);
        pieData = pieData.sort(function (a, b) {
            return b.value - a.value;
        });
        if (!noSplice) {
            pieData.splice(3);
        }
        var maxval = d3.max(pieData, function (d) {
            return d.value;
        });
        var sum = d3.sum(pieData, function (d) {
            return d.value;
        });
        d3.map(pieData, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var txtSize;
        if (height > 75) {
            txtSize = '20px';
        } else {
            txtSize = '10px';
        }
        var piechartoption = { //Piechart option
            container: id,
            colors: colors,
            data: pieData,
            height: height,
            islegends: islegends ? islegends : false,
            //     islegendleft: true,
            legendwidth: legendwidth ? legendwidth : 30,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            txtColor: txtColor ? txtColor : '#8F4582',
            txtSize: txtSize,
            legendmargintop: legendmargintop ? legendmargintop : 30,
            showValue: 'noValue'
        };

        new reusablePie(piechartoption);

    }

    function plotProgressBar(data) {
        var maxvalue = d3.max(data, function (d) {
            return d.alertAmount;
        });

        loadData(maxvalue);
        //        $scope.riskTabloader.transactionChartloader = false;
        function loadData(maxvalue) {
            data.map(function (d) {
                d.key = d.type,
                    d.amountValue = (d.alertAmount / maxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.alertAmount))/* jshint ignore: line */

            });

        }
        return data;
    }


    function plotRiskSidePanelProgressBarChart(data) {
        $scope.CustomerRiskBarData = [];
        $scope.CustomerRiskBarData = plotProgressBar(data.CustomerRisk);
        $scope.ProductRiskBarData = [];
        $scope.ProductRiskBarData = plotProgressBar(data.ProductRisk);
        $scope.GeoGraphicRiskBarData = [];
        $scope.GeoGraphicRiskBarData = plotProgressBar(data.GeoGraphicRisk);
    }

    /*
     * @purpose: load chart for risk tab
     * @created: 3 feb 2018
     * @return: no
     * @author: varsha
     */
    $scope.loadChart_risk = function (txtype) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (txtype) {
            $scope.current_tab = txtype;
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }
        if ($scope.current_tab == "risk") {
            topSidePanel(getFetchTxsCountAndRatioData);
            plotBubbleChart('#riskBloomBergChartOne', ['#8F4582'], customerRiskAggregatesbubbledata);
            plotBubbleChart('#riskBloomBergChartTwo', ['#B13161'], ProductRiskbubbledata);
            $scope.riskTabloader.riskBloomBergChartTwoloader = false;
            plotBubbleChart('#riskBloomBergChartThree', ['#61458F'], geographicRiskAggregatesbubbledata);
            plotBubbleChart('#riskBloomBergChartFour', ['#61458F'], corruptionRiskbubbledata);
            plotBubbleChart('#riskBloomBergChartFive', ['#61458F'], politicalRiskbubbledata);

            World($scope.conterPartyLocNotifyOpts.counterparty_PoliticalRisk);
            World($scope.conterPartyLocNotifyOpts.counterparty_CorruptionRisk);

            plotBarChart("#riskBarChartOne", "#8F4582", customerRiskAggregatespoliticiandata);
            plotBarChart("#riskBarChartTwo", "#B13161", ProductRiskbardata);
            $scope.riskTabloader.riskBarChartTwoloader = false;
            plotBarChart("#riskBarChartThree", "#61458F", geographicRiskAggregatespoliticiandata);
            plotBarChart("#riskBarChartFour", "#61458F", corruptionRiskpoliticiandata);
            plotBarChart("#riskBarChartFive", "#61458F", politicalRiskpoliticiandata);
            //plot top panel pie charts
            var colors_risk_top_pie = ["#8F4582", "#A25394", "#CC6BBB"];
            $scope.riskTabloader.risk_productpieChartloader = false;
            plotPie(productRiskPieData, '#productRiskChart', colors_risk_top_pie, 75); /*  data,color,height*/
            plotPie(customerRiskPieData, '#riskTypeChart', colors_risk_top_pie, 75); /*  data,color,height*/
            plotPie(geoRiskPieData, '#riskMfFtChart', colors_risk_top_pie, 75); /*  data,color,height*/
            $scope.riskTabloader.productTransactionChartloader = false;
            plotRiskSidePanelProgressBarChart(riskSidePanelProgressBarChart);
            plotRiskPie($scope.NotifyRiskPieDate);
        }

    };


    function plotRiskPie(data) {
        var maxval = d3.max(data, function (d) {
            return d.value;
        });
        var sum = d3.sum(data, function (d) {
            return d.value;
        });
        d3.map(data, function (d) {
            d.piePercentage = parseInt((d.value / sum) * 100);
        });
        var colors = ["#CD5A85", "#B13161", "#EE75A2", "#5d96c8", "#69ca6b", "#B753CD", "#c1bd4f", "#db3f8d", "#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"];
        var colorArray = [];
        var colorsO = {/* jshint ignore: line */
            "twitter": "#a75dc2",
            "linkedin": "#5D97C9",
            "facebook": "#399034"
        };

        angular.forEach(data, function (d, i) {
            if (d.value) {
                colorArray[d.key] = colors[i];
            }
        });
        $scope.colorObj = jQuery.extend(true, {}, colorArray);
        var piechartoption2 = { //Piechart option
            container: '#riskPieChartOne',
            //  colors: colors,
            colorsObj: $scope.colorObj,
            data: data,
            height: 160,
            islegends: false,
            //     islegendleft: true,
            //            legendwidth: 30,
            istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
            showValue: 'noValue',
            txtColor: '4D6772',

        };

        setTimeout(function () {
            new reusablePie(piechartoption2); //load pie chart
        }, 0);
        $scope.riskTabloader.riskPieChartOneloader = false;
    }

    //-------------------------------------Corporate Tab Starts---------------------------------------------------------------//		    
    /*
     * @purpose: Get CorporateStructure TopFiveAlerts
     * @created: 20 feb 2018
     * @return: no
     * @author: varsha
     */
    var topCorporateStructures = [];
    var corpoStructurePieChartOnecolors = [];

    function getTopFiveAlerts(date) {
        topCorporateStructures = [];
        topgeography = [];
        topShareholders = [];
        urlCalls.push(TransactionAnalysisApiService.getTopFiveAlerts(date, finalFiltersObj).then(function (response) {
            plotCorporatePieCharts(response);

        }));
    }

    /*
     * @purpose: plot corporate pie chart
     * @created: 20 feb 2018
     * @return: no
     * @author: varsha
     */
    var topCorporateStructures = [];
    var corpoStructurePieChartOnecolors = [];
    var topgeography = [];
    var topgeographycolors = [];
    var topShareholders = [];
    var topShareholderscolors = [];

    function plotCorporatePieCharts(response) {

        response.data.topCorporateStructures.map(function (d) {
            d.key = d.type ? d.type : d.corporateStructure,
                d.value = d.alertCount/* jshint ignore: line */
        });
        response.data.topgeography.map(function (d) {
            d.key = d.type ? d.type : (d.corporateStructure ? d.corporateStructure : d.residentCountry),
                d.value = d.alertCount/* jshint ignore: line */
        });

        response.data.topShareholders.map(function (d) {
            d.key = d.type ? d.type : d.corporateStructure,
                d.value = d.alertCount/* jshint ignore: line */
        });
        topCorporateStructures = response.data.topCorporateStructures;
        topgeography = response.data.topgeography;
        topShareholders = response.data.topShareholders;
        corpoStructurePieChartOnecolors = ["#007A98", "#0091B2", "#00ACCE", "#26C2E4"];
        topgeographycolors = ["#47882C", "#549A35", "#69B648", "#77CE51"];
        topShareholderscolors = ["#009C8E", "#00B7A8", "#00D0BF", "#00E1D0"];
        setTimeout(function () {
            plotPie(response.data.topCorporateStructures, '#corpoStructurePieChartOne', corpoStructurePieChartOnecolors, 75, '#32AFCB'); /*  data,color,height*/
            plotPie(response.data.topgeography, '#corpoStructurePieChartTwo', topgeographycolors, 75, '#759F4B'); /*  data,color,height*/
            plotPie(response.data.topShareholders, '#corpoStructurePieChartThree', topShareholderscolors, 75, '#36B1A7'); /*  data,color,height*/
            plotPie(response.data.topCorporateStructures, '#corpoStructurePieChartFour', corpoStructurePieChartOnecolors, 150, '#32AFCB', '', '', '', 'noSplice'); /*  data,color,height*/
            plotPie(response.data.topgeography, '#corpoStructurePieChartSix', topgeographycolors, 150, '#759F4B', '', '', '', 'noSplice'); /*  data,color,height*/
            plotPie(response.data.topShareholders, '#corpoStructurePieChartFive', topShareholderscolors, 150, '#36B1A7', '', '', '', 'noSplice'); /*  data,color,height*/

        }, 1000);
        corpoStructurePieChartResolveloader();
    }

    function corpoStructurePieChartResolveloader() {
        $scope.corporateStructureTabloader.corpoStructurePieChartOneloader = false;
        $scope.corporateStructureTabloader.corpoStructurePieChartTwoloader = false;
        $scope.corporateStructureTabloader.corpoStructurePieChartThreeloader = false;
        $scope.corporateStructureTabloader.corpoStructurePieChartFourloader = false;
        $scope.corporateStructureTabloader.corpoStructurePieChartFiveloader = false;
        $scope.corporateStructureTabloader.corpoStructurePieChartSixloader = false;
    }

    /*
     * @purpose: Get CorporateStructure
     * @created: 16 feb 2018
     * @return: no
     * @author: varsha
     */
    function getCorporateStructure(date) {
        urlCalls.push(TransactionAnalysisApiService.getCorporateStructure(date, finalFiltersObj).then(function (response) {
            $scope.structure = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount
            };
        }));
    }
    var structurebubbledata = {};
    var structurebardata = [];

    function getCorporateStructureAggregates(date) {
        structurebubbledata = {};
        structurebardata = [];
        urlCalls.push(TransactionAnalysisApiService.getCorporateStructureAggregates(date, finalFiltersObj).then(function (response) {
            structurebubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {

                if (value.alertAmount) {
                    structurebubbledata.children.push({
                        "name": value.corporateStructure,
                        "size": value.alertAmount
                    });
                }
                if (value.alertCount) {
                    structurebardata.push({
                        "letter": value.corporateStructure,
                        "frequency": value.alertCount
                    });
                }


            });
            setTimeout(function () {
                if ($('#corpoStructureBloombergChartOne').width() > 0) {
                    plotBubbleChart('#corpoStructureBloombergChartOne', ['#0D7D96'], structurebubbledata);
                }
                if ($('#corpoStructureBarChartOne').width() > 0) {
                    plotBarChart("#corpoStructureBarChartOne", "#0D7D96", structurebardata);
                }

            }, 2000);
            $scope.corporateStructureTabloader.corpoStructureBloombergChartOneloader = false;
            $scope.corporateStructureTabloader.corpoStructureBarChartOneloader = false;
        }));


    }

    /*
     * @purpose: Get getShareholderCorporateStructure
     * @created: 16 feb 2018
     * @return: no
     * @author: varsha
     */
    function getShareholderCorporateStructure(date) {
        urlCalls.push(TransactionAnalysisApiService.getShareholderCorporateStructure(date, finalFiltersObj).then(function (response) {
            $scope.shareholders = {
                totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
                alertGenerated: response.data.alertCount,
                alertGeneratedPerValue: Math.round(response.data.alertRatio),
                totalAmountCount: response.data.transactionCount
            };
        }));
    }

    var ShareholderAggregatesbubbledata = {};
    var ShareholderAggregatesbardata = [];

    function getShareholderAggregates(date) {
        ShareholderAggregatesbubbledata = {};
        ShareholderAggregatesbardata = [];
        urlCalls.push(TransactionAnalysisApiService.getShareholderAggregates(date, finalFiltersObj).then(function (response) {
            ShareholderAggregatesbubbledata = { //BubbleEntity Data
                name: '',
                children: []
            };
            angular.forEach(response.data, function (value) {
                if (value.alertAmount) {
                    ShareholderAggregatesbubbledata.children.push({
                        "name": value.corporateStructure,
                        "size": value.alertAmount
                    });
                }
                if (value.alertCount) {
                    ShareholderAggregatesbardata.push({
                        "letter": value.corporateStructure,
                        "frequency": value.alertCount
                    });
                }

            });

            setTimeout(function () {
                if ($('#corpoStructureBloombergChartTwo').width() > 0) {
                    plotBubbleChart('#corpoStructureBloombergChartTwo', ['#26968D'], ShareholderAggregatesbubbledata);
                }
                if ($('#corpoStructureBarChartTwo').width() > 0) {
                    plotBarChart("#corpoStructureBarChartTwo", "#26968D", ShareholderAggregatesbardata);
                }

            }, 2000);
            $scope.corporateStructureTabloader.corpoStructureBloombergChartTwoloader = false;
            $scope.corporateStructureTabloader.corpoStructureBarChartTwoloader = false;
        }));

    }

    /*
     * @purpose: Get AlertEntitiesGeography
     * @created: 19 feb 2018
     * @return: no
     * @author: varsha
     */
    function getAlertEntitiesGeography(date) {
        urlCalls.push(TransactionAnalysisApiService.getAlertEntitiesGeography(date, finalFiltersObj).then(function (response) {
            $scope.corporateGeography = setSidePanelCounterParty(response);
        }));
    }

    /*
     * @purpose: Get GeographyAggregates
     * @created: 19 feb 2018
     * @return: no
     * @author: varsha
     */

    var corporateGeographyAggregatesbardata = [];
    var corporateWorldmapOptions = {};

    function getGeographyAggregates(date) {
        corporateGeographyAggregatesbardata = [];
        corporateWorldmapOptions = {};
        urlCalls.push(TransactionAnalysisApiService.getGeographyAggregates(date, finalFiltersObj).then(function (response) {
            angular.forEach(response.data, function (value) {
                if (value.alertCount) {
                    corporateGeographyAggregatesbardata.push({
                        "letter": value.residentCountry,
                        "frequency": value.alertAmount
                    });
                }
            });
            var corporateGeographyworlddata = [];
            angular.forEach(response.data, function (value) {
                if (value.residentCountry) {
                    corporateGeographyworlddata.push({
                        'lat': value.latitude, //worldMap Data
                        'long': value.longitude,
                        'mark': "assets/images/redpin.png",
                        'name': value.residentCountry,
                        'title': value.residentCountry,
                        'amount': value.alertCount,
                        'fillColor': '#5F7845'
                    });
                }
            });
            var maxValue = d3.max(response.data, function (d) {
                return d.alertAmount;
            });
            corporateWorldmapOptions = jQuery.extend(true, {}, worldChartOptions);
            corporateWorldmapOptions.markers = corporateGeographyworlddata;
            corporateWorldmapOptions.domain = {
                x: 1,
                y: maxValue + 2000
            };
            corporateWorldmapOptions.text = 'bycount';
            corporateWorldmapOptions.ChangeRadius = 'true';
            corporateWorldmapOptions.height = 220;
            corporateWorldmapOptions.container = "#corpoStructureWorldMapOne";
            setTimeout(function () {
                if ($('#corpoStructureWorldMapOne').width() > 0) {
                    World(corporateWorldmapOptions);
                }
                if ($('#corpoStructureBarChartTree').width() > 0) {
                    plotBarChart("#corpoStructureBarChartTree", "#5F7845", corporateGeographyAggregatesbardata, "byamount");
                }

            }, 2000);
            $scope.corporateStructureTabloader.corpoStructureBarChartTreeloader = false;
            $scope.corporateStructureTabloader.corpoStructureWorldMapOneloader = false;

        }));

    }

    loadLinechart('#corpoStructureLineChartOne', 60, $scope.getTransactionAndAlertAggregates, true);



    $scope.loadChart_corporate = function (tab) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (tab) {
            $scope.current_tab = "corporate";
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }

        if ($scope.current_tab == "corporate") {
            topSidePanel(getFetchTxsCountAndRatioData);
            plotBarChart("#corpoStructureBarChartOne", "#0D7D96", structurebardata);
            plotBarChart("#corpoStructureBarChartTree", "#5F7845", corporateGeographyAggregatesbardata, "byamount");
            plotBubbleChart('#corpoStructureBloombergChartOne', ['#0D7D96'], structurebubbledata);
            loadLinechart('#corpoStructureLineChartOne', 60, $scope.getTransactionAndAlertAggregates, true);
            plotBubbleChart('#corpoStructureBloombergChartTwo', ['#26968D'], ShareholderAggregatesbubbledata);
            plotBarChart("#corpoStructureBarChartTwo", "#26968D", ShareholderAggregatesbardata);
            plotPie(topCorporateStructures, '#corpoStructurePieChartOne', corpoStructurePieChartOnecolors, 75, '#32AFCB'); /*  data,color,height*/
            plotPie(topgeography, '#corpoStructurePieChartTwo', topgeographycolors, 75, '#759F4B'); /*  data,color,height*/
            plotPie(topShareholders, '#corpoStructurePieChartThree', topShareholderscolors, 75, '#36B1A7'); /*  data,color,height*/
            plotPie(topCorporateStructures, '#corpoStructurePieChartFour', corpoStructurePieChartOnecolors, 150, '#32AFCB', '', '', 'noSplice'); /*  data,color,height*/
            plotPie(topgeography, '#corpoStructurePieChartSix', topgeographycolors, 150, '#759F4B', '', '', '', 'noSplice'); /*  data,color,height*/
            plotPie(topShareholders, '#corpoStructurePieChartFive', topShareholderscolors, 150, '#36B1A7', '', '', '', 'noSplice'); /*  data,color,height*/
            World(corporateWorldmapOptions);

        }

    };

    //-------------------------------------Counter Parties Tab Starts---------------------------------------------------------------//		    

    /*
     * @purpose:load chart for counterParties
     * @created: 3 feb 2018
     * @return: no
     * @author: varsha
     */

    $scope.loadChart_counterparties = function (tab) {
        $uibModalStack.dismissAll();
        $('#alertMainMenu').css('display', 'none');
        if (tab) {
            $scope.current_tab = "counterparties";
        }
        if (window.isTabChangeFromContext == true) {
            window.isTabChangeFromContext = false;
            return false;
        }
        // Create Corporate Alert Modal 

        //  $scope.counterPartiesAlertModal();

        if ($scope.current_tab == "counterparties") {
            topSidePanel(getFetchTxsCountAndRatioData);
            plotBubbleChart('#counterPartiesBloombergChartOne', ['#12936D'], counterPartyAggregateBubbleChart);
            plotBarChart("#counterPartiesBarChartOne", "#12936D", counterPartyAggregateBarChart);
            plotBarChart("#counterPartiesBarChartTwo", "#11847D", counterPartyGeographicBarChart, "byamount");
            plotBubbleChart('#counterPartiesBloombergChartTwo', ['#3D6F7C'], counterPartyBankBubbleChart);
            plotBarChart("#counterPartiesBarChartThree", "#3D6F7C", counterPartyBankBarChart);
            plotBubbleChart('#counterPartiesBloombergChartThree', ['#3D6F7C'], counterPartyBankLocationBubbleChart);
            loadLinechart('#counterPartiesLineChartOne', 60, $scope.getTransactionAndAlertAggregates, true);
            World(counterPartyGeographicWorldmapOptions);
            World(counterPartyBankLocationWorldmapOptions);
            var colorsActivity = ["#009E6E", "#00B785", "#00CA80", "#00E198"];
            var colorsCountries = ["#00897F", "#009C91", "#00B3A7", "#00C6B9"];
            var colorsBanks = ["#396E7D", "#447E8E", "#5698AB", "#75B5C7"];
            plotPie(Top3CounterPartyActivities, '#counterPartiestopActivity', colorsActivity, 75, colorsActivity[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/
            plotPie(TopCounterPartyActivities, '#counterPartyAlertActvityRatio', colorsActivity, 160, colorsActivity[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
            plotPie(Top3CounterPartyCountries, '#counterPartiestopGeography', colorsCountries, 75, colorsCountries[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/
            plotPie(TopCounterPartyCountries, '#counterPartiesGeographyRatio', colorsCountries, 160, colorsCountries[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
            plotPie(TopCounterPartyCountries, '#counterPartiesBanksLocationsRatio', colorsBanks, 160, colorsBanks[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
            plotPie(Top3CounterPartyBanks, '#counterPartiestopBanks', colorsBanks, 75, colorsBanks[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/
            plotPie(TopCounterPartyBanks, '#counterPartiesBanksRatio', colorsBanks, 160, colorsBanks[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/

        }
    };
    /*
     * @purpose: function to call all API's related to counterparty section
     * @created: 5 feb 2018
     * @author: prasanthi
     */
    function getDataForCounterPartyTab(date) {

        activityCounterParty(date);
        activitycounterPartyAggregates(date);
        counterPartygetGeoGraphic(date, false);
        counterPartygetGeoGraphicAggregates(date, false);
        counterPartygetBanks(date);
        counterPartygetBankAggregates(date);
        //bank locations
        counterPartygetGeoGraphic(date, true);
        counterPartygetGeoGraphicAggregates(date, true);


    }
    /*
     * @purpose: set side panel for all types in counter party
     * @created: 17 feb 2018
     * @author: varsha
     */
    function setSidePanelCounterParty(response) {
        return {
            totalAmount: '$' + ' ' + Number(Math.round(response.data.alertAmount)).formatAmt(),
            alertGenerated: response.data.alertCount,
            alertGeneratedPerValue: Math.round(response.data.alertRatio),
            totalAmountCount: response.data.transactionCount
        };
    }


    /*
     * @purpose: calling Api for activityCounterParty 
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function activityCounterParty(date) {
        urlCalls.push(TransactionAnalysisApiService.activityCounterParty(date, finalFiltersObj).then(function (response) {
            $scope.counterPartyActivityTypesDetail = setSidePanelCounterParty(response);
        }));
    }
    /*
     * @purpose: calling Api for activitycounterPartyAggregates 
     * @created: 17 feb 2018
     * @author: prasanthi
     */

    function activitycounterPartyAggregates(date) {
        urlCalls.push(TransactionAnalysisApiService.activitycounterPartyAggregates(date, finalFiltersObj).then(function (response) {
            counterPartyActivityType(response);
        }));
    }

    /*
     * @purpose: calling chart for activity type
     * @created: 17 feb 2018
     * @author: varsha
     */
    var counterPartyAggregateBubbleChart = [];
    var counterPartyAggregateBarChart = [];

    function counterPartyActivityType(response) {
        counterPartyAggregateBubbleChart = [];
        counterPartyAggregateBarChart = [];
        TopCounterPartyActivities = [];
        counterPartyAggregateBubbleChart = { //BubbleEntity Data
            name: '',
            children: []
        };
        angular.forEach(response.data, function (value) {
            if (value.alertAmount) {
                counterPartyAggregateBubbleChart.children.push({
                    "name": value.type,
                    "size": value.alertAmount
                });
            }
            if (value.alertCount) {
                counterPartyAggregateBarChart.push({
                    "letter": value.type,
                    "frequency": value.alertCount
                });
            }
        });

        setTimeout(function () {
            if ($('#counterPartiesBloombergChartOne').width() > 0) {
                plotBubbleChart('#counterPartiesBloombergChartOne', ['#12936D'], counterPartyAggregateBubbleChart);
            }
            if ($('#counterPartiesBarChartOne').width() > 0) {
                plotBarChart("#counterPartiesBarChartOne", "#12936D", counterPartyAggregateBarChart);
            }

        }, 2000);

        $scope.counterPartiesStructureTabloader.counterPartiesBloombergChartOneloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesBarChartOneloader = false;

        //load pie charts starts here
        var colorsActivity = ["#009E6E", "#00B785", "#00CA80", "#00E198"];
        TopCounterPartyActivities = jQuery.extend(true, [], response.data);
        Top3CounterPartyActivities = jQuery.extend(true, [], response.data);
        Top3CounterPartyActivities.splice(3);
        TopCounterPartyActivities.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        Top3CounterPartyActivities.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        $scope.counterPartiesStructureTabloader.counterPartyAlertActvityRatioloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiestopActivityloader = false;
        plotPie(Top3CounterPartyActivities, '#counterPartiestopActivity', colorsActivity, 75, colorsActivity[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/

        plotPie(TopCounterPartyActivities, '#counterPartyAlertActvityRatio', colorsActivity, 160, colorsActivity[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
        //load pie charts ends here
    }

    /*
     * @purpose: calling Api for counterPartygetGeoGraphic 
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function counterPartygetGeoGraphic(date, flag) {
        urlCalls.push(TransactionAnalysisApiService.counterPartygetGeoGraphic(date, flag, finalFiltersObj).then(function (response) {
            if (!flag) {
                $scope.counterPartyGeoGraphicDetail = setSidePanelCounterParty(response);
            } else {
                handleForBankLocatinos(response);
            }
        }));
    }
    /*
     * @purpose: calling Api for counterPartygetGeoGraphicAggregates 
     * @created: 17 feb 2018
     * @author: prasanthi
     */

    function counterPartygetGeoGraphicAggregates(date, flag) {
        urlCalls.push(TransactionAnalysisApiService.counterPartygetGeoGraphicAggregates(date, flag, finalFiltersObj).then(function (response) {
            if (!flag) {
                counterPartyGeography(response);
            } else {
                handleAggregatesForBankLocatinos(response);
                handleWorldMap(response);

            }
        }));

    }

    /*
     * @purpose: calling chart for geography
     * @created: 17 feb 2018
     * @author: varsha
     */
    var counterPartyGeographicWorldmapOptions = {};
    var counterPartyGeographicBarChart = [];

    function counterPartyGeography(response) {
        counterPartyGeographicWorldmapOptions = {};
        counterPartyGeographicBarChart = [];
        angular.forEach(response.data, function (value) {
            if (value.alertCount) {
                counterPartyGeographicBarChart.push({
                    "letter": value.type,
                    "frequency": value.alertAmount
                });
            }
        });

        var counterPartyGeographicWorldMapMarker = [];
        angular.forEach(response.data, function (value) {
            counterPartyGeographicWorldMapMarker.push({
                'lat': value.latitude, //worldMap Data
                'long': value.longitude,
                'mark': "assets/images/redpin.png",
                'name': value.type,
                'title': value.type,
                'amount': value.alertCount,
                'fillColor': '#11847D'
            });
        });
        var maxValue = d3.max(response.data, function (d) {
            return d.alertAmount;
        });
        counterPartyGeographicWorldmapOptions = jQuery.extend(true, {}, worldChartOptions);
        counterPartyGeographicWorldmapOptions.markers = counterPartyGeographicWorldMapMarker;
        counterPartyGeographicWorldmapOptions.domain = {
            x: 1,
            y: maxValue + 2000
        };
        counterPartyGeographicWorldmapOptions.ChangeRadius = 'true';
        counterPartyGeographicWorldmapOptions.height = 220;
        counterPartyGeographicWorldmapOptions.text = 'bycount';
        counterPartyGeographicWorldmapOptions.container = "#counterPartiesWorldMapOne";
        setTimeout(function () {
            if ($('#counterPartiesWorldMapOne').width() > 0) {
                World(counterPartyGeographicWorldmapOptions);
            }
            if ($('#counterPartiesBarChartTwo').width() > 0) {
                plotBarChart("#counterPartiesBarChartTwo", "#11847D", counterPartyGeographicBarChart, "byamount");
            }

        }, 2000);

        $scope.counterPartiesStructureTabloader.counterPartiesBarChartTwoloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesWorldMapOneloader = false;
        //load pie charts starts here
        var colorsCountries = ["#00897F", "#009C91", "#00B3A7", "#00C6B9"];
        var colorsBanks = ["#396E7D", "#447E8E", "#5698AB", "#75B5C7"];
        TopCounterPartyCountries = jQuery.extend(true, [], response.data);
        Top3CounterPartyCountries = jQuery.extend(true, [], response.data);
        Top3CounterPartyCountries.splice(3);
        TopCounterPartyCountries.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        Top3CounterPartyCountries.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        $scope.counterPartiesStructureTabloader.counterPartiestopGeographyloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesGeographyRatioloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesBanksLocationsRatioloader = false;
        plotPie(Top3CounterPartyCountries, '#counterPartiestopGeography', colorsCountries, 75, colorsCountries[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/
        plotPie(TopCounterPartyCountries, '#counterPartiesGeographyRatio', colorsCountries, 160, colorsCountries[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
        plotPie(TopCounterPartyCountries, '#counterPartiesBanksLocationsRatio', colorsBanks, 160, colorsBanks[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/
        //load pie charts ends here
    }
    /*
     * @purpose: calling Api for counterPartygetBanks 
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function counterPartygetBanks(date, flag) {
        urlCalls.push(TransactionAnalysisApiService.counterPartygetBanks(date, flag, finalFiltersObj).then(function (response) {

            handleForBanks(response);

        }));
    }
    /*
     * @purpose: calling Api for counterPartygetBankAggregates 
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function counterPartygetBankAggregates(date, flag) {
        if (!flag) {
            counterPartyBankBubbleChart = [];
            counterPartyBankBarChart = [];
        } else {
            counterPartyBankLocationBubbleChart = [];
        }
        urlCalls.push(TransactionAnalysisApiService.counterPartygetBankAggregates(date, flag, finalFiltersObj).then(function (response) {

            handleAggregatesForBanks(response);

        }));
    }
    /*
     * @purpose: function to handle banks data
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function handleForBankLocatinos(response) {
        $scope.bankLocatinonDetail = setSidePanelCounterParty(response);

    }
    /*
     * @purpose: funciton to handle bank locations data
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    function handleForBanks(response) {
        $scope.bankDetail = setSidePanelCounterParty(response);

    }
    /*
     * @purpose: function to handle aggregates banks data
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    var counterPartyBankBubbleChart = [];
    var counterPartyBankBarChart = [];

    function handleAggregatesForBanks(response) {
        counterPartyBankBubbleChart = [];
        counterPartyBankBarChart = [];
        counterPartyBankBubbleChart = { //BubbleEntity Data
            name: '',
            children: []
        };
        angular.forEach(response.data, function (value) {
            if (value.alertAmount) {
                counterPartyBankBubbleChart.children.push({
                    "name": value.type,
                    "size": value.alertAmount
                });
            }
            if (value.alertCount) {
                counterPartyBankBarChart.push({
                    "letter": value.type,
                    "frequency": value.alertCount
                });
            }
        });

        setTimeout(function () {
            if ($('#counterPartiesBloombergChartTwo').width() > 0) {
                plotBubbleChart('#counterPartiesBloombergChartTwo', ['#3D6F7C'], counterPartyBankBubbleChart);
            }
            if ($('#counterPartiesBarChartThree').width() > 0) {
                plotBarChart("#counterPartiesBarChartThree", "#3D6F7C", counterPartyBankBarChart);
            }

        }, 2000);
        $scope.counterPartiesStructureTabloader.counterPartiesBloombergChartTwoloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesBarChartThreeloader = false;

        //load pie charts starts here

        var colorsBanks = ["#396E7D", "#447E8E", "#5698AB", "#75B5C7"];
        TopCounterPartyBanks = jQuery.extend(true, [], response.data);
        Top3CounterPartyBanks = jQuery.extend(true, [], response.data);
        Top3CounterPartyBanks.splice(3);
        TopCounterPartyBanks.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        Top3CounterPartyBanks.map(function (d) {
            d.key = d.type,
                d.value = d.alertCount/* jshint ignore: line */
        });
        $scope.counterPartiesStructureTabloader.counterPartiestopBanksloader = false;
        $scope.counterPartiesStructureTabloader.counterPartiesBanksRatioloader = false;
        plotPie(Top3CounterPartyBanks, '#counterPartiestopBanks', colorsBanks, 75, colorsBanks[1], true, 50, 10); /*  data,color,height,text-color,islegend,lengendwidth,lengendmargintop*/

        plotPie(TopCounterPartyBanks, '#counterPartiesBanksRatio', colorsBanks, 160, colorsBanks[1], '', '', '', 'noSplice'); /*  data,color,height,text-color*/

        //load pie charts ends here
    }
    /*
     * @purpose: funciton to handle Aggregates bank locations data
     * @created: 17 feb 2018
     * @author: prasanthi
     */
    var counterPartyBankLocationBubbleChart = [];

    function handleAggregatesForBankLocatinos(response) {
        counterPartyBankLocationBubbleChart = { //BubbleEntity Data
            name: '',
            children: []
        };
        angular.forEach(response.data, function (value) {
            counterPartyBankLocationBubbleChart.children.push({
                "name": value.type,
                "size": value.alertAmount
            });
        });

        setTimeout(function () {
            if ($('#counterPartiesBloombergChartThree').width() > 0) {
                plotBubbleChart('#counterPartiesBloombergChartThree', ['#3D6F7C'], counterPartyBankLocationBubbleChart);
            }

        }, 2000);
        $scope.counterPartiesStructureTabloader.counterPartiesBloombergChartThreeloader = false;
    }
    /*
     * @purpose: funciton to handle Aggregates bank world chart data
     * @created: 21 feb 2018
     * @author: varsha
     */
    var counterPartyBankLocationWorldmapOptions = {};

    function handleWorldMap(response) {
        var counterPartyMapMarker = [];
        angular.forEach(response.data, function (value) {
            counterPartyMapMarker.push({
                'lat': value.latitude, //worldMap Data
                'long': value.longitude,
                'mark': "assets/images/redpin.png",
                'name': value.type,
                'title': value.type,
                'amount': value.alertCount,
                'fillColor': '#3D6F7C'
            });
        });
        var maxValue = d3.max(response.data, function (d) {
            return d.alertAmount;
        });
        counterPartyBankLocationWorldmapOptions = jQuery.extend(true, {}, worldChartOptions);
        counterPartyBankLocationWorldmapOptions.markers = counterPartyMapMarker;
        counterPartyBankLocationWorldmapOptions.domain = {
            x: 1,
            y: maxValue + 2000
        };
        counterPartyBankLocationWorldmapOptions.ChangeRadius = 'true';
        counterPartyBankLocationWorldmapOptions.height = 220;
        counterPartyBankLocationWorldmapOptions.text = 'bycount';
        counterPartyBankLocationWorldmapOptions.container = "#counterPartiesWorldMapTwo";
        setTimeout(function () {
            if ($('#counterPartiesWorldMapTwo').width() > 0) {
                World(counterPartyBankLocationWorldmapOptions);
            }

        }, 2000);
        $scope.counterPartiesStructureTabloader.counterPartiesWorldMapTwoloader = false;

    }

    loadLinechart('#counterPartiesLineChartOne', 60, $scope.getTransactionAndAlertAggregates, true);

    /*
     * @purpose:load datePicker
     * @created: 5 feb 2018
     * @author: varsha
     */

    function initializeDatePicker(tab, start, end) {
        var id = "#" + tab + '_reportrange';

        function cb(start, end) {
            $scope.Date = [];
            $scope.Date[0] = new Date(start._d).getFullYear() + "-" + ((new Date(start._d).getMonth()) + 1) + "-" + new Date(start._d).getDate();
            $scope.Date[1] = new Date(end._d).getFullYear() + "-" + ((new Date(end._d).getMonth()) + 1) + "-" + new Date(end._d).getDate();
            $scope.Date[2] = '';
            if ($scope.pageNum == undefined) {
                $scope.Date[3] = 1;
            } else {
                $scope.Date[3] = $scope.pageNum;
            }
            $scope.Date[4] = '';
            $scope.Date[5] = 20;

            $(id + ' ' + 'span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            //call Api with selected Date
            alertCallApi(tab, $scope.Date);

        }

        $(id).daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    }
    /*
     * @purpose: vertical Negative Chart Legend Data
     * @created: 23rd feb 2018
     * @author: varsha
     */
    window.VerticalNegativeBarChartlegendData = function (data, id) {
        if (id == '#transactionComparisonChart') {
            $scope.transactionComparisonChartLegendData = data;
            setTimeout(function () {
                if ($(".chart-legends-transact")) {
                    $(".chart-legends-transact").mThumbnailScroller({
                        axis: "x"
                    });
                }
            }, 2000);
        } else if (id == '#barchartnotification') {
            $scope.barchartnotificationLegendData = data;
            setTimeout(function () {
                if ($(".chart-legends-notify")) {
                    $(".chart-legends-notify").mThumbnailScroller({
                        axis: "x"
                    });
                }
            }, 2000);
        }

    };
    /*
     * @purpose: Reusable pie Legend Data
     * @created: 23rd feb 2018
     * @author: varsha
     */

    window.reusablePieLegendData = function (data, id) {
        if (id == '#alertsByPeriodPieChart') {
            $scope.alertsByPeriodPieChartLegendData = data;
        } else if (id == '#alertsByStatuschart') {
            $scope.alertsByStatuschartLegendData = data;
        } else if (id == '#alertsByStatus30Chart') {
            $scope.alertsByStatus30ChartLegendData = data;
        }
    };

    /*
     * @purpose: calling Api
     * @created: 5 feb 2018
     * @author: varsha
     */
    function alertCallApi(tab, date) {
        resetAllLoadersAndDivs();
        urlCalls = [];
        $scope.alertDasbboardloader = true;
        $scope.riskPieChartPreloader = true;
        $scope.lineChartPreloader = true;
        $scope.counterpartyWorldMapPreloader = true;
        $scope.transactipTypesPieChartPreloader = true;
        // $scope.barchartnotificationPreloader=true;
        $scope.associatedEntitychartPreloader = true;
        //call counterparty API's

        //call notification API's
        getAssociatedEntities(date);
        alertList(date);
        getTransactionAndAlertAggregates(date);
        getMonthlyTurnOver(date);
        getcounterPartyLocationsNotify(date, 'counterpartyWorldMap');
        getcounterPartyLocationsNotify(date, 'counterparty_PoliticalRisk', 'PoliticalRisk');
        getcounterPartyLocationsNotify(date, 'counterparty_CorruptionRisk', 'CorruptionRisk');
        setNotiTopThreeTransaction();
        alertComparisonNotification(date);
        dashboardAlertByStatus(date); //dashboard api
        //call aml
        getAllAMLDetails(date);
        //call tx & notification API
        getFetchTxsCountAndRatioByInput(date);
        getFetchTxsCountAndRatio(date);
        getAlertNotification(date);

        //call tx API
        getTransactionAndAlertByProductType(date);
        riskRatioChart(date);
        getCustomerRiskCounts(date);
        getCustomerRiskAggregates(date);
        getGeoGraphicCountRatio(date);
        getGeoGraphicAggregates(date);
        getCorruptionRisk(date);
        getCorruptionRiskAggregates(date);
        getPoliticalRisk(date);
        getPoliticalRiskAggregates(date);
        alertComparisonTransactions(date);
        getProductRisk(date);
        getProductRiskAggregates(date);

        $scope.fromDt = date[0];
        $scope.toDt = date[1];
        $scope.selectedDate = date;

        //call corporate API
        getTopFiveAlerts(date);
        getCorporateStructure(date);
        getCorporateStructureAggregates(date);
        getShareholderCorporateStructure(date);
        getShareholderAggregates(date);
        getGeographyAggregates(date);
        getAlertEntitiesGeography(date);
        getDataForCounterPartyTab(date);
    }
    /*
     * @purpose: calling Api for aml 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function getAllAMLDetails(date) {
        //    	amlAlertNotifications();
        //    	amlAlertAggregation();
        amlTopScenarios(date);
        amlAlertByStatus(date);
        amlAlertByStatus(date, true);
        amlAlertByPeriod(date);
        amlTopGeographyAndTransfers(date);
        amlTopCustomers(date);
        //    	amlTransferByMonth(date);
        setTimeout(function () {
            $q.all(urlCalls)
                .then(
                    function () {
                        $scope.alertDasbboardloader = false;
                    },
                    function () {
                        $scope.alertDasbboardloader = false;
                    },
                    function () {
                        $scope.alertDasbboardloader = false;
                    });
        });
    }


    /*
     * @purpose: calling Api for amlTopScenarios 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlTopScenarios(date) {
        $scope.aml_top_scenarios_pieData = [];
        urlCalls.push(TransactionAnalysisApiService.amlTopScenarios(date, finalFiltersObj).then(function (response) {
            $scope.aml_top_scenarios_pieData = [];
            $scope.aml_scenarios_progressData = [];
            var transmaxvalue = d3.max(response.data.amlAlertsAggregation, function (d) {
                return d.amount;
            });
            $scope.aml_top_scenarios_pieData = jQuery.extend(true, [], response.data.amlAlertsAggregation);
            $scope.aml_scenarios_progressData = jQuery.extend(true, [], response.data.amlAlertsAggregation);
            $scope.aml_scenarios_progressData.map(function (d) {
                d.key = d.scenario;
                d.value = (d.amount / transmaxvalue) * 100;
                d.amount = nFormatter(parseInt(d.amount));
            });
            $scope.aml_top_scenarios_pieData.map(function (d) {
                d.key = d.scenario;
                d.value = d.count;
            });
            $scope.aml_scenarios_progressData.sort(function (a, b) {
                return b.amount - a.amount;
            });
            $scope.aml_top_scenarios_pieData.sort(function (a, b) {
                return b.count - a.count;
            });
            $scope.aml_top_scenarios_pieData.splice(3);
            $scope.dashboardTabloader.topScenariosChartloader = false;
            if ($scope.current_tab == "aml") {
                var colors = ["#3A64AA", "#608CD3", "#71A0E7"];
                plotPie($scope.aml_top_scenarios_pieData, '#topScenariosChart', colors, 75, colors[2]);
            }
            $scope.dashboardTabloader.dashboardAlertsByScenarioloader = false;
        }));
    }
    /*
     * @purpose: calling Api for amlAlertByStatus 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlAlertByStatus(date, periodBy) {
        if (periodBy) {
            $scope.alertsByStatusGreaterThan30 = [];
            $scope.alertsByStatus30ChartLegendData = [];
        } else {
            $scope.amlAlertByStatusData = [];
        }
        urlCalls.push(TransactionAnalysisApiService.amlAlertByStatus(date, periodBy, finalFiltersObj).then(function (response) {
            if (periodBy) {
                response.data.amlAlertByStatus.map(function (d) {
                    d.key = d.alertStatus;
                    d.value = d.count;
                });
                $scope.alertsByStatusGreaterThan30 = response.data.amlAlertByStatus;
                if ($scope.current_tab == "aml") {
                    var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                    plotPie($scope.alertsByStatusGreaterThan30, '#alertsByStatus30Chart', colors, 160, colors[1]);
                }
                $scope.dashboardTabloader.Status30Chartloader = false;
            } else {
                response.data.amlAlertByStatus.map(function (d) {
                    d.key = d.alertStatus;
                    d.value = d.count;
                });
                $scope.amlAlertByStatusData = response.data.amlAlertByStatus;
                if ($scope.current_tab == "aml") {
                    var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                    plotPie($scope.amlAlertByStatusData, '#alertsByStatuschart', colors, 160, colors[1]);
                }
                $scope.dashboardTabloader.Statuschartloader = false;
            }

        }, function () {
            $scope.aml_scenarios_progressData = [];
        }));

    }
    /*
     * @purpose: calling Api for amlAlertByPeriod 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlAlertByPeriod(date) {
        $scope.amlAlertByPeriod = [];
        urlCalls.push(TransactionAnalysisApiService.amlAlertByPeriod(date, finalFiltersObj).then(function (response) {
            var pieData = [{
                key: "10-20 Days",
                value: response.data.amlAlertByStatus.tenToTwentyCount ? response.data.amlAlertByStatus.tenToTwentyCount : 0
            }, {
                key: "20-30 Days",
                value: response.data.amlAlertByStatus.twentyToThirtyCount ? response.data.amlAlertByStatus.twentyToThirtyCount : 0
            }, {
                key: ">30 Days",
                value: response.data.amlAlertByStatus.greaterThirtyDaysCount ? response.data.amlAlertByStatus.greaterThirtyDaysCount : 0
            }];
            $scope.amlAlertByPeriod = pieData;
            $scope.dashboardTabloader.dashboatdPeriodPieChartLoader = false;
            if ($scope.current_tab == "aml") {
                var colors = ["#C500CC", "#EB0089", "#38DD66", "#587381"];
                plotPie($scope.amlAlertByPeriod, '#alertsByPeriodPieChart', colors, 160, colors[2]);

            }

        }));
    }
    /*
     * @purpose: calling Api for amlTopGeographyAndTransfers 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlTopGeographyAndTransfers(date) {
        $scope.aml_top_branches_progressData = [];
        urlCalls.push(TransactionAnalysisApiService.amlTopGeographyAndTransfers(date, finalFiltersObj).then(function (response) {
            $scope.aml_branches_progressData = [];
            $scope.aml_top_branches_progressData = jQuery.extend(true, [], response.data.inputCounterpartyList);
            $scope.aml_branches_progressData = jQuery.extend(true, [], response.data.inputCounterpartyList);
            var transmax_topvalue = d3.max(response.data.inputCounterpartyList, function (d) {
                return d.count;
            });
            var transmax_topvalue_amount = d3.max(response.data.inputCounterpartyList, function (d) {
                return d.amount;
            });

            $scope.aml_branches_progressData.sort(function (a, b) {
                return b.amount - a.amount;
            });
            $scope.aml_top_branches_progressData.sort(function (a, b) {
                return b.count - a.count;
            });
            $scope.aml_top_branches_progressData.splice(3);
            $scope.dashboardTabloader.dashboardTopBranchesloader = false;
            $scope.dashboardTabloader.dashboardTransfersByBranchloader = false;
            $scope.aml_top_branches_progressData.map(function (d) {
                d.key = d.country.country,
                    d.value = (d.count / transmax_topvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.count))/* jshint ignore: line */
            });
            $scope.aml_branches_progressData.map(function (d) {
                d.key = d.country.country,
                    d.value = (d.amount / transmax_topvalue_amount) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.amount))/* jshint ignore: line */
            });

            //load location chart
            var markers = [];
            angular.forEach(response.data.inputCounterpartyList, function (d) {
                if (d.country && d.country.country) {
                    markers.push({
                        'lat': d.country.latitude, //worldMap Data
                        'long': d.country.longitude,
                        'mark': "assets/images/redpin.png",
                        'name': d.country.country,
                        'title': d.country.country,
                        'amount': d.count,
                        'fillColor': d.colour

                    });
                }
            });
            var maxValue = d3.max(response.data.inputCounterpartyList, function (d) {
                return d.count;
            });
            var worldOptions1 = jQuery.extend(true, {}, worldChartOptions);
            worldOptions1.markers = markers;
            worldOptions1.domain = {
                x: 1,
                y: maxValue + 2000
            };
            worldOptions1.ChangeRadius = 'true';
            worldOptions1.container = "#transfersByGeographyChart";
            worldOptions1.text = 'bycount';
            $scope.amlLocationOtions = jQuery.extend(true, {}, worldOptions1);
            if ($scope.current_tab == "aml") {
                World(worldOptions1);
            }

        }, function () {
            $scope.aml_branches_progressData = [];
        }));
    }
    /*
     * @purpose: calling Api for amlTopCustomers 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlTopCustomers(date) {
        urlCalls.push(TransactionAnalysisApiService.amlTopCustomers(date, finalFiltersObj).then(function (response) {
            $scope.aml_customers_progressData = [];
            var transmaxvalue = d3.max(response.data, function (d) {
                return d.count;
            });
            response.data.map(function (d) {
                d.key = d.customerName,
                    d.value = (d.count / transmaxvalue) * 100,/* jshint ignore: line */
                    d.amount = nFormatter(parseInt(d.count))/* jshint ignore: line */
            });
            $scope.aml_customers_progressData = jQuery.extend(true, [], response.data);
            $scope.aml_top_customers_progressData = jQuery.extend(true, [], response.data);
            $scope.aml_top_customers_progressData.splice(3);
            $scope.dashboardTabloader.dashboradTopCustomersloader = false;

        }, function () {
            $scope.aml_customers_progressData = [];
        }));
    }
    /*
     * @purpose: calling Api for amlTransferByMonth 
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function amlTransferByMonth(date) {/* jshint ignore: line */
        TransactionAnalysisApiService.amlTransferByMonth(date, finalFiltersObj).then(function () {

        });
    }
    /*
     * @purpose: function to reset all loaders and empt divs
     * @created: 16 feb 2018
     * @author: prasanthi
     */
    function resetAllLoadersAndDivs() {
        //enable loaders
        $scope.transactionTabloader = {
            inputOutputRatioloader: true,
            transalertsChartloader: true,
            transLineChartloader: true,
            barchart1loader: true,
            transaction_pieChartloader: true,
            alertRatioloader: true,
            top3Transactionloader: true,
            topTransactionTypesloader: true,
            monthlyTurnoverPieChartloader: true,
            transactionComparisonChartloader: true,
        };

        $scope.riskTabloader = {
            risk_pieChartloader: true,
            risk_productpieChartloader: true,
            risk_geopieChartloader: true,
            top3Transactionloader: true,
            riskBloomBergChartOneloader: true,
            riskBarChartOneloader: true,
            riskBloomBergChartTwoloader: true,
            riskBarChartTwoloader: true,
            riskBloomBergChartThreeloader: true,
            riskBarChartThreeloader: true,
            riskBloomBergChartFourloader: true,
            riskBarChartFourloader: true,
            riskBloomBergChartFiveloader: true,
            riskBarChartFiveloader: true,
            counterparty_CorruptionRiskloader: true,
            counterparty_PoliticalRiskloader: true,
            //   			 transactionChartloader:true,
            customerTransactionChartloader: true,
            productTransactionChartloader: true,
            geoTransactionChartloader: true,
            riskPieChartOneloader: true,
        };

        $scope.corporateStructureTabloader = {
            corpoStructurePieChartOneloader: true,
            corpoStructurePieChartTwoloader: true,
            corpoStructurePieChartThreeloader: true,
            alertTrendloader: true,
            corpoStructureBloombergChartOneloader: true,
            corpoStructureBarChartOneloader: true,
            corpoStructureBloombergChartTwoloader: true,
            corpoStructureBarChartTwoloader: true,
            corpoStructureBarChartTreeloader: true,
            corpoStructureWorldMapOneloader: true,
            corpoStructurePieChartFourloader: true,
            corpoStructurePieChartFiveloader: true,
            corpoStructurePieChartSixloader: true,
        };

        $scope.counterPartiesStructureTabloader = {
            counterPartiestopActivityloader: true,
            counterPartiestopGeographyloader: true,
            counterPartiestopBanksloader: true,
            counterPartiesLineChartOneloader: true,
            counterPartiesBanksLocationsRatioloader: true,
            counterPartiesBanksRatioloader: true,
            counterPartiesGeographyRatioloader: true,
            counterPartyAlertActvityRatioloader: true,
            counterPartiesBloombergChartOneloader: true,
            counterPartiesBloombergChartTwoloader: true,
            counterPartiesBloombergChartThreeloader: true,
            counterPartiesWorldMapOneloader: true,
            counterPartiesWorldMapTwoloader: true,
            counterPartiesBarChartOneloader: true,
            counterPartiesBarChartTwoloader: true,
            counterPartiesBarChartThreeloader: true,
        };
        //empty all divs of Transaction tab
        $("#inputOutputRatio").empty();
        //    	$("#Transaction_pieChart").empty();
        $("#barchart1").empty();
        $("#transLineChart").empty();
        $("#monthlyTurnoverPieChart").empty();

        //empty all divs of Risk tab
        $("#riskTypeChart").empty();
        $("#productRiskChart").empty();
        $("#riskMfFtChart").empty();
        $("#riskPieChartOne").empty();
        $("#riskBloomBergChartOne").empty();
        $("#riskBarChartOne").empty();
        $("#riskBloomBergChartTwo").empty();
        $("#riskBarChartTwo").empty();
        $("#riskBloomBergChartThree").empty();
        $("#riskBarChartThree").empty();
        $("#riskBloomBergChartFour").empty();
        $("#riskBarChartFour").empty();
        $("#riskBloomBergChartFive").empty();
        $("#riskBarChartFive").empty();

        //empty all divs of Corp structure tab
        $("#corpoStructurePieChartOne").empty();
        $("#corpoStructurePieChartTwo").empty();
        $("#corpoStructurePieChartThree").empty();
        //    	$("#corpoStructureLineChartOne").empty();
        $("#corpoStructurePieChartFour").empty();
        $("#corpoStructurePieChartFive").empty();
        $("#corpoStructurePieChartSix").empty();
        $("#corpoStructureBloombergChartOne").empty();
        $("#corpoStructureBarChartOne").empty();
        $("#corpoStructureBloombergChartTwo").empty();
        $("#corpoStructureBarChartTwo").empty();
        $("#corpoStructureBarChartTree").empty();
        $("#corpoStructureWorldMapOne").empty();

        //empty all divs of Counter parties tab
        $("#counterPartiestopActivity").empty();
        $("#counterPartiestopGeography").empty();
        $("#counterPartiestopBanks").empty();
        $("#counterPartiesLineChartOne").empty();
        $("#counterPartyAlertActvityRatio").empty();
        $("#counterPartiesGeographyRatio").empty();
        $("#counterPartiesBanksRatio").empty();
        $("#counterPartiesBanksLocationsRatio").empty();
        $("#counterPartiesBloombergChartOne").empty();
        $("#counterPartiesBarChartOne").empty();
        $("#counterPartiesBarChartTwo").empty();
        $("#counterPartiesWorldMapOne").empty();
        $("#counterPartiesBloombergChartTwo").empty();
        $("#counterPartiesBarChartThree").empty();
        $("#counterPartiesBloombergChartThree").empty();
        $("#counterPartiesWorldMapTwo").empty();
    }
    //----------------------------------------------------------------------

    /*
     * @purpose: function for filters
     * @created: 5 Mar 2018
     * @author: prasanthi
     */

    var pieIdList = ['#RiskPieChart', '#TransactipTypesPieChart', '#topScenariosChart', '#alertStatusChart', '#alertsByPeriodPieChart', '#inputOutputRatio', '#Transaction_pieChart', '#monthlyTurnoverPieChart', '#riskTypeChart', '#productRiskChart', '#riskMfFtChart', '#riskPieChartOne', '#corpoStructurePieChartOne', '#corpoStructurePieChartTwo', '#corpoStructurePieChartThree', '#corpoStructurePieChartFour', '#corpoStructurePieChartFive', '#corpoStructurePieChartSix', '#counterPartiestopActivity', '#counterPartiestopGeography', '#counterPartiestopBanks', '#counterPartyAlertActvityRatio', '#counterPartiesGeographyRatio', '#counterPartiesBanksRatio', '#counterPartiesBanksLocationsRatio', '#alertsByStatus30Chart', '#alertsByStatuschart'];
    var pieTypeList = ['Risk Type', 'transactionType', 'scenario', 'alertStatus', 'AlertPeriod', 'InputOutputRatio', 'transactionType', 'monthlyTurnover', 'customerRisk', 'productRiskType', 'geoRiskType', 'Risk Ratio', 'corporateStructureType', 'corporateGeographyType', 'shareholderType', 'corporateStructureType', 'shareholderType', 'corporateGeographyType', 'activityCategory', 'corporateGeographyType', 'bankName', 'activityCategory', 'corporateGeographyType', 'bankName', 'bankLocations', 'Alert By Status By 30', 'alertStatus'];

    var lineIdList = ['#transLineChart', '#barchart1'];
    var lineTypeList = ['transactionType', 'transactionType'];

    var bubbleIdList = ['#riskBloomBergChartOne', '#riskBloomBergChartTwo', '#riskBloomBergChartThree', '#riskBloomBergChartFour', '#riskBloomBergChartFive', '#corpoStructureBloombergChartOne', '#corpoStructureBloombergChartTwo', '#counterPartiesBloombergChartOne', '#counterPartiesBloombergChartTwo', '#counterPartiesBloombergChartThree'];
    var bubbleTypeList = ['customerRisk', 'productRiskType', 'geoRiskType', 'corruptionRiskType', 'politicalRiskType', 'corporateStructureType', 'shareholderType', 'activityCategory', 'bankName', 'bankLocations'];

    var simpleBarChartList = ['#riskBarChartOne', '#riskBarChartTwo', '#riskBarChartThree', '#riskBarChartFour', '#riskBarChartFive', '#corpoStructureBarChartOne', '#corpoStructureBarChartTwo', '#corpoStructureBarChartTree', '#counterPartiesBarChartOne', '#counterPartiesBarChartTwo', '#counterPartiesBarChartThree'];
    var simpleBarChartTypeList = ['customerRisk', 'productRiskType', 'geoRiskType', 'corruptionRiskType', 'politicalRiskType', 'corporateStructureType', 'shareholderType', 'corporateGeographyType', 'activityCategory', 'counterpartyCountry', 'bankName'];

    var negativeBarIdList = ['#barchartnotification', '#transactionComparisonChart'];
    var negativeBarTypeList = ['transactionType', 'transactionType'];

    var worldIdList = ['#counterpartyWorldMap', '#transfersByGeographyChart', '#counterparty_CorruptionRisk', '#counterparty_PoliticalRisk', '#corpoStructureWorldMapOne', '#counterPartiesWorldMapOne', '#counterPartiesWorldMapTwo'];
    var worldTypeList = ['counterpartyCountry', 'country', 'corruptionRiskType', 'politicalRiskType', 'corporateGeographyType', 'counterpartyCountry', 'counterpartyCountry'];

    window.aplyAlertDasboardFilters = function (data, id, chartType) {

        $(".transactionTypeCharts").each(function () {
            if (lineIdList.indexOf("#" + $(this).attr("id") == -1)) {
                lineIdList.push("#" + $(this).attr("id"));
                lineTypeList.push('transactionType');
            }
        });

        if (chartType == 'pie') {
            var type;
            if (pieIdList.indexOf(id) >= 0) {
                var index = pieIdList.findIndex(function (pieIdList) {
                    return pieIdList === id;
                });
                type = pieTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) { // just use arr
                if (n.isboolean) {
                    return n.actualType == type && n.value.toLowerCase() == data.data.key.toLowerCase();
                } else {
                    return n.type == type && n.value == data.data.key;
                }
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if ($.inArray(data.data.key, filtersObj[type]) == -1) {

                if (id == '#RiskPieChart' || id == '#riskPieChartOne') {
                    if (data.data.key == 'GeoGraphicRisk') {
                        filtersObj.isGeographyRiskType = 1;
                    } else if (data.data.key == 'CustomerRisk') {
                        filtersObj.isCustomerRiskType = 1;
                    } else if (data.data.key == 'ProductRisk') {
                        filtersObj.isProductRisk = 1;
                    }
                } else if (id == '#inputOutputRatio') {
                    if (data.data.key == 'output') {
                        filtersObj.isOutput = 1;
                    } else if (data.data.key == 'input') {
                        filtersObj.isInput = 1;
                    }
                } else if (id == '#alertsByPeriodPieChart') {
                    if (data.data.key == '>30 Days') {
                        filtersObj.isAlertsByPeriodgreaterthan30Days = 1;
                    } else if (data.data.key == '10-20 Days') {
                        filtersObj.isAlertsByPeriod10to20Days = 1;
                    } else if (data.data.key == '20-30 Days') {
                        filtersObj.isAlertsByPeriod20to30Days = 1;
                    }
                } else if (id == '#alertsByStatus30Chart') {
                    if (data.data.key == 'OPEN') {
                        filtersObj.isAlertByStatusGreaterThan30Days = 1;
                    }
                } else if (id == '#monthlyTurnoverPieChart') {
                    if (data.data.key == "below-one-million-USD") {
                        filtersObj.isBelowOneMillionUSD = 1;
                    } else if (data.data.key == "between-one-to-five-million-USD") {
                        filtersObj.isBetweenOneToFiveMillionUSD = 1;
                    } else if (data.data.key == "between-five-to-ten-million-USD") {
                        filtersObj.isBetweenFiveToTenMillionUSD = 1;
                    } else if (data.data.key == "above-ten-million-USD") {
                        filtersObj.isAboveTenMillionUSD = 1;
                    }
                } else {
                    if (!filtersObj[type]) {
                        filtersObj[type] = [];
                    }
                    filtersObj[type].push(data.data.key);
                }

            }
            $scope.$apply(function () {
                if (id == '#RiskPieChart' || id == '#riskPieChartOne') {
                    if (data.data.key == 'GeoGraphicRisk') {
                        $scope.filterArr.push({
                            type: 'isGeographyRiskType',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == 'CustomerRisk') {
                        $scope.filterArr.push({
                            type: 'isCustomerRiskType',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == 'ProductRisk') {
                        $scope.filterArr.push({
                            type: 'isProductRisk',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    }
                } else if (id == '#inputOutputRatio') {
                    if (data.data.key == 'output') {
                        $scope.filterArr.push({
                            type: 'isOutput',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == 'input') {
                        $scope.filterArr.push({
                            type: 'isInput',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    }
                } else if (id == '#alertsByPeriodPieChart') {
                    if (data.data.key == '>30 Days') {
                        $scope.filterArr.push({
                            type: 'isAlertsByPeriodgreaterthan30Days',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == '10-20 Days') {
                        $scope.filterArr.push({
                            type: 'isAlertsByPeriod10to20Days',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == '20-30 Days') {
                        $scope.filterArr.push({
                            type: 'isAlertsByPeriod20to30Days',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    }
                } else if (id == '#alertsByStatus30Chart') {
                    if (data.data.key == 'OPEN') {
                        $scope.filterArr.push({
                            type: 'isAlertByStatusGreaterThan30Days',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    }
                } else if (id == '#monthlyTurnoverPieChart') {
                    if (data.data.key == "below-one-million-USD") {
                        $scope.filterArr.push({
                            type: 'isBelowOneMillionUSD',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == "between-one-to-five-million-USD") {
                        $scope.filterArr.push({
                            type: 'isBetweenOneToFiveMillionUSD',
                            isboolean: true,
                            value: 'isBetweenOneToFiveMillionUSD',
                            val: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == "between-five-to-ten-million-USD") {
                        $scope.filterArr.push({
                            type: 'isBetweenFiveToTenMillionUSD',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    } else if (data.data.key == "above-ten-million-USD") {
                        $scope.filterArr.push({
                            type: 'isAboveTenMillionUSD',
                            isboolean: true,
                            value: data.data.key,
                            actualType: type
                        });
                    }
                } else {
                    $scope.filterArr.push({
                        type: type,
                        value: data.data.key
                    });
                }


                if ($scope.filterArr.length > 0) {
                    //						setTimeout(function(){
                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });
                    //						},0)
                }
            });

        } else if (chartType == 'line' || chartType == 'bar') {
            var type;
            if (lineIdList.indexOf(id) >= 0) {
                var index = lineIdList.findIndex(function (lineIdList) {
                    return lineIdList === id;
                });
                type = lineTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) { // just use arr

                return n.type == type && n.value.toLowerCase() == data.toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        } else if (chartType == 'bubble') {
            var type;
            if (bubbleIdList.indexOf(id) >= 0) {
                var index = bubbleIdList.findIndex(function (bubbleIdList) {
                    return bubbleIdList === id;
                });
                type = bubbleTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) {
                return n.type == type && n.value.toLowerCase() == data.data.className.toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data.data.className);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data.data.className
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        } else if (chartType == 'world') {
            var type;
            if (worldIdList.indexOf(id) >= 0) {
                var index = worldIdList.findIndex(function (worldIdList) {
                    return worldIdList === id;
                });
                type = worldTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) {
                return n.type == type && n.value.toLowerCase() == data.name.toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data.name);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data.name
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        } else if (chartType == 'simpleBar') {
            var type;
            if (simpleBarChartList.indexOf(id) >= 0) {
                var index = simpleBarChartList.findIndex(function (simpleBarChartList) {
                    return simpleBarChartList === id;
                });
                type = simpleBarChartTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) {
                return n.type == type && n.value.toLowerCase() == data.letter.toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data.letter);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data.letter
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        } else if (chartType == 'negativeBar') {
            var type;
            if (negativeBarIdList.indexOf(id) >= 0) {
                var index = negativeBarIdList.findIndex(function (negativeBarIdList) {
                    return negativeBarIdList === id;
                });
                type = negativeBarTypeList[index];
            }
            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) {
                return n.type == type && n.value.toLowerCase() == data.data.name.toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data.data.name);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data.data.name
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        } else if (chartType == 'progressBar') {
            var type;
            if (id == 'progressBarChart') {
                if (data[1] == "progressbar3") {
                    type = "customerName";
                } else if (data[1] == "progressbar4" || data[1] == "progress_bar5") {
                    type = "country";
                } else if (data[1] == "progressbar1") {
                    type = "customerName";
                } else if (data[1] == "counterparties2") {
                    type = "customerName";
                } else if (data[1] == "progress_bar2") {
                    type = "country";
                } else if (data[1] == "counterparties5_new") {
                    type = "scenario";
                } else if (data[1] == "progressbar2" || data[1] == "progressbar5") {
                    type = "transactionType";
                } else if (data[1] == "progressbar8") {
                    type = "customerRisk";
                } else if (data[1] == "transactionChart_1_new") {
                    type = "productRiskType";
                } else if (data[1] == "transactionChart2_new") {
                    type = "geoRiskType";
                } else if (data[1] == "coperateStructure_new") {
                    type = "corporateStructureType";
                } else if (data[1] == "counterparties1_new") {
                    type = "counterPartyNames";
                } else if (data[1] == "progress_bar1" || data[1] == "progressbar6") {
                    type = "customerName";
                } else if (data[1] == "progressbar7" || data[1] == "progress_bar6") {
                    type = "transactionType";
                } else if (data[1] == "progressbar11") {
                    type = "customerName";
                } else {
                    type = "not included";
                }
            }

            //code to avoid duplicate filters 
            var new_arr = $.grep($scope.filterArr, function (n) {
                return n.type == type && n.value.toLowerCase() == data[0].toLowerCase();
            });
            if (new_arr.length > 0) {
                return false;
            }
            //code to avoid duplicate filters ends 
            if (!filtersObj[type]) {
                filtersObj[type] = [];
            }
            if ($.inArray(data, filtersObj[type]) == -1) {
                filtersObj[type].push(data[0]);
            }
            $scope.$apply(function () {
                $scope.filterArr.push({
                    type: type,
                    value: data[0]
                });
                if ($scope.filterArr.length > 0) {

                    $(".remote-filters-list-wrapper").mThumbnailScroller({
                        axis: "x"
                    });

                }
            });

        }
        if (filtersObj.transactionType) {
            if (filtersObj.transactionType.indexOf('all') > -1) {
                delete filtersObj.transactionType;
            }
        }

        finalFiltersObj = jQuery.extend(true, {}, filtersObj);
        var keys = d3.keys(finalFiltersObj);
        angular.forEach(keys, function (val) {
            if (typeof (finalFiltersObj[val]) != "number") {
                if (finalFiltersObj[val].length > 0) {
                    finalFiltersObj[val] = finalFiltersObj[val].join(",");
                } else {
                    delete finalFiltersObj[val];
                }
            }
        });
        console.log(finalFiltersObj);
        refreshWithFilters();
    };

    //----------------------------------------------------------------------
    /*
     * @purpose: function for remove filters
     * @created: 6 Mar 2018
     * @author: prasanthi
     */
    $scope.removeFilter = function (index, filter) {
        $scope.filterArr.splice(index, 1);
        if (filter.isboolean) {
            delete filtersObj[filter.type];
        } else {
            if (filtersObj[filter.type]) {
                var indexObj = filtersObj[filter.type].indexOf(filter.value);
            }
            if (indexObj > -1) {
                filtersObj[filter.type].splice(indexObj, 1);
            }
        }
        console.log(filtersObj);
        finalFiltersObj = jQuery.extend(true, {}, filtersObj);
        var keys = d3.keys(finalFiltersObj);
        angular.forEach(keys, function (val) {
            if (typeof (finalFiltersObj[val]) != "number") {
                if (finalFiltersObj[val].length > 0) {
                    finalFiltersObj[val] = finalFiltersObj[val].join(",");
                } else {
                    delete finalFiltersObj[val];
                }
            }
        });
        //    	 setTimeout(function(){
        //    		 $(".remote-filters-list-wrapper").mThumbnailScroller({
        //                 axis: "x"
        //             });
        //			},0)

        refreshWithFilters();
    };
    //----------------------------------------------------------------------
    /*
     * @purpose: function for refresh data with  filters
     * @created: 6 Mar 2018
     * @author: prasanthi
     */
    function refreshWithFilters() {
        alertCallApi("", $scope.Date);
    }

    /*
     * @purpose: function for checking  progress bars data not found
     * @created: 27 Mar 2018
     * @author: varsha
     */
    $scope.checkToShowDtaNtFnd = function (data) {
        if (data != undefined) {
            if (data.length === 0 && !window.isTabChangeFromContext) {
                return true;
            }
        }
    };
}