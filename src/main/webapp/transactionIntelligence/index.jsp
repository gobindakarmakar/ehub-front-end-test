<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ehubTransactionIntelligenceApp">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="../assets/images/logo-new.png" />
		<title>TransactionIntelligence</title>
		
		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="./assets/css/transaction-intelligence-vendor.min.css" rel="stylesheet"/>
		<!-------------------------------  Vendor Styles Ends  --------------------------------->
		
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="../assets/css/common-styles.min.css" rel="stylesheet"/>
		<link href="./assets/css/transaction-intelligence-styles.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href='../vendor/jquery/css/rangedatepicker.css' />
		
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	
	<body ng-cloak>
	<flash-message class="main-flash-wrapper"></flash-message>
		 <div ng-show="topPanelPreview">
			<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
	     </div>   
		<div ui-view></div>
		
		<!--  Chat Panel Wrapper Starts  -->
			<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
		<!--  Chat Panel Wrapper Ends  -->
		
		<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>

<!--  ==============================================================================
**********************  Local Path Starts *********************
==============================================================================*/
-->		
			<!--------------------------------  Common Vendor Js Starts  ------------------------------->
			<script src='../vendor/jquery/js/jquery.min.js'></script>
        <script src='../vendor/jquery/js/jquery-ui.js'></script>
        <script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
        <script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
        <script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
        <script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
        <script src="../vendor/jquery/js/raphael.min.js"></script>
		<script src="../vendor/jquery/js/raphael.icons.min.js"></script>
		<script src="../vendor/jquery/js/wheelnav.min.js"></script>
        <script src='../vendor/jquery/js/bootstrap.min.js'></script>
          <script src='../vendor/jquery/js/displacy-entity.min.js'></script>
        
        
        <script src='../vendor//angular/js/angular.min.js'></script>
        <script src='../vendor//angular/js/angular-ui-router.min.js'></script>
        <script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
        <script src='../vendor//angular/js/html2canvas.js'></script>
		<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
		
        <script src='../vendor//angular/js/angular-flash.min.js'></script>
         <script src='../vendor/angular/js/angular_multiselect.js'></script>
        
    	<script src='../charts/d3.v3.min.js'></script>
        <script src='../charts/d3.v4.min.js'></script>
        <script src='../charts/d3v3.js'></script>
        <script src='../charts/d3js.js'></script>
       
        <script src='../scripts/VLA/js/cola.v3.min.js'></script>
        <script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
        <script src='../scripts/VLA/js/cytoscape-cola.js'></script>
        <script src='../scripts/VLA/js/jquery.qtip.js'></script>
        <script src='../scripts/VLA/js/cytoscape-qtip.js'></script>
			<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
			<!--------------------------------  Transaction Intelligence Vendor Js Starts  ------------------------------->
		 <script src='../scripts/VLA/js/cytoscape-cose-bilkent.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-cxtmenu.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-markov-cluster.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-navigator.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-ngraph.forcelayout.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-panzoom.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-undo-redo.js'></script>
    	 <script src='../scripts/VLA/js/weaver.min.js'></script>
    	 <script src='../scripts/VLA/js/cytoscape-spread.js'></script>
    	 <script src='../scripts/VLA/js/random-color.js'></script>
    	 <script src='../scripts/VLA/js/spectrum.js'></script>
    	 <script src='../scripts/VLA/js/typeahead.bundle.js'></script>
    	 <script src='../scripts/VLA/js/jquery.selectric.min.js'></script>
    	 <script src='../scripts/VLA/js/custom_config.js'></script>
    	 <script src='../scripts/VLA/js/app.js'></script>
<!--     	 <script src='../scripts/VLA/js/app_locale_ua.js'></script> -->
<!--     	 <script src='../scripts/VLA/js/app_locale_ar.js'></script> -->
    	 <script src='../scripts/VLA/tagcloud/js/tagcloud.js'></script>
    	 <script src='../scripts/VLA/js/bluebird.min.js'></script>
    	 <script src='../scripts/VLA/js/base64toblob.js'></script>
    	 <script src='../scripts/VLA/js/loadash.js'></script>
    	 <script src='../vendor/jquery/js/moment.min.js'></script>
         <script src='../vendor/angular/js/angular-moment.min.js'></script>
         <script src='../vendor/angular/js/angular-datatables.min.js'></script>
         <script src='../vendor/angular/js/FileSaver.min.js'></script>
         <script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
         <script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
        <script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
			<!--------------------------------  Transaction Intelligence Vendor Js Ends    ------------------------------->
			
			<script src="../scripts/app.js"></script>
			<script src="./transactionIntelligence.app.js"></script>
			<script src="./transactionIntelligence.config.js"></script>
			<script src="./transactionIntelligence.run.js"></script>
	
	    <!--------------------------------  Common Scripts Js Starts  ------------------------------>
		<script src='../scripts/common/constants/app.constant.js'></script>
    	<script src='../scripts/common/constants/common.constant.js'></script>
    	<script src='../scripts/discover/constants/discover.constant.js'></script>
    	<script src='../scripts/act/constants/act.constant.js'></script>
    	
    	<script src='../scripts/common/services/shared.service.js'></script>
    	<script src='../scripts/common/services/top.panel.api.service.js'></script>
    	<script src='../scripts/common/services/riskScore.service.js'></script>
    	<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
    	<script src='../scripts/common/services/upload.file.service.js'></script>
    	<script src='../scripts/discover/services/discover.api.service.js'></script>
    	<script src='../scripts/act/services/act.api.service.js'></script>
    	<script src='../scripts/act/services/act.graph.service.js'></script>
    	<script src="../scripts/common/services/common.service.js"></script>
    	
    	
    	
    	<script src='../scripts/common/js/submenu.controller.js'></script>
    	<script src='../scripts/common/js/top.panel.controller.js'></script>
    	<script src='../scripts/common/js/advanced.search.controller.js'></script>
    	<script src='../scripts/common/js/user.events.controller.js'></script>
    	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
    	<script src="../scripts/common/js/notification.controller.js"></script>
    	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
    	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>  
    	<script src="../scripts/common/modal/js/create.event.controller.js"></script>
    	<script src="../scripts/common/modal/js/participants.event.controller.js"></script>  	
    	
    	<script src='../scripts/common/js/entity.visualiser.js'></script>
    		
 		<script src="../scripts/common/js/chatbot.controller.js"></script>
		<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
		<script src="../scripts/common/directives/context.menu.directive.js"></script>
		<script src="../scripts/enrich/services/entity.api.services.js"></script>
	
	
			<!--------------------------------  Common Scripts Js Ends   ------------------------------->		
			
			<!--------------------------------  Transaction Intelligence Scripts Js Starts  ------------------------------>
			 <script src='../scripts/common/constants/enrich.search.constant.js'></script> 
	    
	    <script src='../transactionIntelligence/controllers/transactionIntelligence.landing.controller.js'></script> 
	    <script src='../transactionIntelligence/controllers/transactionIntelligence.controller.js'></script>
	    <script src='../transactionIntelligence/controllers/alerts.dashboard.controller.js'></script> 
	    
	    <script src='../transactionIntelligence/modal/controllers/fullAnalysis.modal.controller.js'></script>
	    <script src='../transactionIntelligence/modal/controllers/createAlert.modal.controller.js'></script> 
	    <script src='../transactionIntelligence/modal/controllers/resolveAnalysis.modal.controller.js'></script> 
	     <script src='../transactionIntelligence/modal/controllers/articleVisualizer.modal.controller.js'></script> 
	   
	    <script src='../transactionIntelligence/services/transaction.analysis.api.service.js'></script> 
	    <script src='../transactionIntelligence/services/transaction.graph.service.js'></script> 
	    <script src="../transactionIntelligence/modal/controllers/riskAlert.modal.controller.js"></script>
		<script src="../transactionIntelligence/modal/controllers/corporateAlert.modal.controller.js"></script>
		<script src="../transactionIntelligence/modal/controllers/counterParties.modal.controller.js"></script>
	    <script src="../transactionIntelligence/modal/controllers/viewAllTrans.modal.controller.js"></script>
		<script src="../transactionIntelligence/modal/controllers/sideRisk.modal.controller.js"></script>
			
			<!--------------------------------  Transaction Intelligence Scripts Js Ends   ------------------------------->
			
			<!--------------------------------  Transaction Intelligence Charts Js Starts  ------------------------------>
		<script  src='../charts/topojson.min.js'></script> 
    	<script  src='../charts/datamaps.all.js'></script> 
        <script  src='../charts/WorldMap/js/WorldChart.js'></script> 
        <script  src='../charts/lineChart.js'></script> 
        <script  src='../charts/reusablePie.js'></script> 
        <script  src='../charts/stackedtime.js'></script> 
        <script  src='../charts/cloud.js'></script> 
        <script  src='../charts/hotTopics.js'></script> 
        <script  src='../charts/groupedColumChart.js'></script> 
	    <script src="../scripts/enrich/services/enrich.graph.service.js"></script>
	    <script src="../entity/services/entity.graph.services.js"></script>
		<script src="../charts/cloud.js"></script>
	    <script src="../charts/bubbleEntity.js"></script>
	    <script src="../charts/stackedtime.js"></script>
	    <script src="../charts/topojson.min.js"></script>
       	<script src="../charts/WorldMap/js/WorldChart.js"></script>
        <script src="../charts/timeformat.js"></script>
	    <script src="../charts/timeformatsupport.js"></script>
	    <script src="../charts/SimpleBarChart.js"></script>
	    <script src="../charts/verticalNegativeBarChart.js"></script>
	    <script src = "../charts/ordinalLineChart.js"></script>
      
	
      
	
	
		
			<!--------------------------------  Transaction Intelligence Scripts Js Ends   ------------------------------->
			
<!--  ==============================================================================
**********************  Local Path Ends *********************
==============================================================================*/
-->		


<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/
-->		
<!-- 		<!--------------------------------  Common Vendor Js Starts  -------------------------------> 
<!-- 		<script src="../vendor/common-vendor.min.js"></script> -->
<!-- 		<!--------------------------------  Common Vendor Js Ends    -------------------------------> 
	
<!-- 		<!--------------------------------  Transaction Intelligence Vendor Js Starts  ------------------------------->
<!-- 		<script src="./assets/minifiedJs/transactionIntelligence-vendor.min.js"></script> -->
<!-- 		<!--------------------------------  Transaction Intelligence Vendor Js Ends    ------------------------------->
		
<!-- 		<script src="../scripts/app.js"></script> -->
<!-- 		<script src="./transactionIntelligence.app.js"></script> -->
<!-- 		<script src="./transactionIntelligence.config.js"></script> -->
<!-- 		<script src="./transactionIntelligence.run.js"></script> -->

<!-- 	    ------------------------------  Common Scripts Js Starts  ---------------------------- -->
<!-- 		<script src="../scripts/common-scripts.min.js"></script> -->
<!-- 		<!--------------------------------  Common Scripts Js Ends   ------------------------------->		
		
<!-- 		<!--------------------------------  Transaction Intelligence Scripts Js Starts  ------------------------------>
<!-- 		<script src="./assets/minifiedJs/transactionIntelligence-scripts.min.js"></script> -->
<!-- 		<!--------------------------------  Transaction Intelligence Scripts Js Ends   ------------------------------->
		
<!-- 		<!--------------------------------  Transaction Intelligence Charts Js Starts  ------------------------------>
<!-- 		<script src="./assets/minifiedJs/transactionIntelligence-charts.min.js"></script> -->
<!-- 		<!--------------------------------  Transaction Intelligence Scripts Js Ends   -------------------------------> 

<!--new minified Js start -->	
<!-- <script src="../vendor/common.vendor.new.min.js"></script>
<script src="./assets/minifiedJs/transactionIntelligence.vendor.new.min.js"></script>


<script src="./assets/minifiedJs/transactionIntelligence.intialize.new.min.js"></script>
<script src="./assets/minifiedJs/transactionIntelligence.moduleScripts.new.min.js"></script>

<script src="../scripts/common.scripts.new.min.js"></script>
<script src="./assets/minifiedJs/transactionIntelligence.scripts.new.min.js"></script>

<script src="./assets/minifiedJs/transactionIntelligence.charts.new.min.js"></script> -->
<!--new minified Js end -->	
	
	
<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->			
<script type="text/ng-template" id="sideRisk.modal.html">
<!--  Side Risk Modal Starts  -->
<div class="modal-header">
	<button type="button" class="close" style="color:{{txtColor}} !important" ng-click="closeUploadDocument()">
		<span>&#x2716;</span>
	</button>
	<h4 class="modal-title" style="color:{{txtColor}} !important">{{type}}</h4>
</div>
<div class="modal-body">
 <span ng-if="sideModalPreloader" class="custom-spinner case-dairy-spinner">
                                  <i class="fa fa-spinner fa-spin fa-2x"></i>
  </span>	
	<div class="alert-dashboard-wrapper">
		<div class="chart-panel-wrapper four-grid">
			<!--  Risk Right Col Starts  -->
			<div class="row custom-row">

				<!--  First Column Starts  -->
				<div class="col-sm-12 custom-col">  

					<!-- Chart Panel Starts -->
					<div class="panel chart-panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-4"><p class=" text-dark-grey1 mar-b5" >Amount</p></div>
								<div class="col-sm-8"><p class="count text-dark-grey1 mar-b5" style="color:{{txtColor}} !important">{{Amount}}</p></div>
							</div>
							<div class="row">
								<div class="col-sm-4"><p class="text-dark-grey1 mar-0">Alert</p></div>
								<div class="col-sm-8 text-light-red"><p class="text-dark-grey1 count mar-0" style="color:#CD3F87 !important">{{CountTotal}}<span class="text-dark-grey1">({{ alertRatio }}%)</span></p></div>								
							</div>
						</div>
					</div>
					<!-- Chart Panel Ends -->

				</div>
				<!--  First Column Ends  -->

				<!--  Second Column Starts  -->
				<div class="col-sm-12 custom-col">

					<!-- Chart Panel Starts -->
					<div class="panel chart-panel">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-4">
									<h3 class="width-100">Scenarios</h3>
								</div>
								<div class="col-sm-8">
									<div class="custom-multiselect-wrapper w-100">
									<select class="custom-select square" ng-change=getScenariosGraph() ng-model="selectedToTopModel">
										<option ng-repeat="scenario in scenarios">{{ scenario }}</option>
									</select>
<!-- 										<div ng-change=getScenariosGraph() ng-dropdown-multiselect="" options="selectedToTopData" selected-model="selectedToTopModel" extra-settings="selectedToTopSettings"></div> -->
									</div>
								</div>
							</div>
							<!-- Chart Panel Ends -->

						</div>
						<div class="panel-body">
							<div class="side-risk-chart-wrapper" ng-if="loadDummy">
								<div class="centered-text">
									<span>Select A Scenario</span>To View Visualisation
								</div>		
													
							</div>
							<div id="sideRiskBarChartOne"></div>
							 <span ng-if="sideRiskBarChartOnePreloader" class="custom-spinner case-dairy-spinner">
                                  <i class="fa fa-spinner fa-spin fa-2x"></i>
                            </span>	
							
						</div>
					</div>
				</div>
				<!--  Second Column Ends  -->

				<!--  Third Column Starts  -->
				<div class="col-sm-12 custom-col risk-custom-col">

					<!-- Chart Panel Starts -->
					<div class="panel chart-panel">
						<div class="panel-heading">
							<h3>Associated Alerts By Amount</h3>
						</div>
						<div class="panel-body">
							<div id="associatedAlertsByAmount"></div>
						</div>
					</div>
					<!-- Chart Panel Ends -->

					<!-- Chart Panel Starts -->
					<div class="panel chart-panel">
						<div class="panel-heading">
							<h3>Associated Alerts By Numbers</h3>
						</div>
						<div class="panel-body">
							<div id="associatedAlertsByNumber"></div>
						</div>
					</div>
					<!-- Chart Panel Ends -->

				</div>
				<!--  Third Column Ends  -->

			</div>
			<!--  Risk Right Col Ends  -->
		</div>
	</div>
</div>
<!--  Side Risk Modal Ends  -->
<script>
setTimeout(function () {
		$('.custom-modal.right .modal-dialog .modal-content .modal-body').mCustomScrollbar({
			axis: "y",
			theme: "minimal-dark"
		});
	}, 10);
</script>
 
 <script type="text/ng-template" id="riskAlert.modal.html">
<!--  Risk Modal Starts  -->
<div class="modal-header">
	<button type="button" class="close" ng-click="closeModal()">
		<span>&#x2716;</span>
	</button>
	<h4 class="modal-title">Alert Details</h4>
</div>
<div class="modal-body">
	<!--  Alert Dashboard Wrapper Starts  -->
	<div class="alert-dashboard-wrapper">
		<!--  Risk Grid Starts  -->
		<div class="chart-panel-wrapper four-grid">
			<!--  Risk Row Starts  -->
			<div class="row custom-row">

				<!--  First Column Starts  -->
				<div class="col-sm-4 custom-col left-col">

					<!-- Chart Panel Starts -->
					<div class="panel chart-panel panel-alert main-counter-parties-scroll">
						<div class="panel-heading">
							<div class="person-alert-info">
								<div class="person-alert-img">
									<a style="pointer-events:none;" class="btn-grey-circle">
										<i class="fa fa-user"></i>
									</a>
								</div>
								<div class="person-alert-details">
									<h3 class="alert-name text-lighter-grey">{{ customerDetails.customerName }}</h3>
<!-- 									<p class="text-lighter-grey"> -->
<!-- 										Age : <span></span> -->
<!-- 									</p> -->
								</div>
							</div>
						</div>
						<div class="panel-body">
							<ul class="list-unstyled custom-list light-grey">
<!-- 								<li> -->
<!-- 									<a href="javscript:void(0);">Passport Number : -->
<!-- 										<span> </span> -->
<!-- 									</a> -->
<!-- 								</li> -->
								<li ng-if="customerDetails.customerType !='CORP'">
									<a href="javscript:void(0);">Date of Birth :
										<span>{{ customerDetails.dateOfBirth | date:'dd-MM-yyyy'}}</span>
									</a>
								</li>
								<li ng-if="customerDetails.customerType =='CORP'">
									<a href="javscript:void(0);">Founded in :
										<span>{{ customerDetails.dateOfIncorporation | date:'dd-MM-yyyy'}}</span>
									</a>
								</li>
								<li>
									<a href="javscript:void(0);">City : {{customerDetails.city }}</a>
								</li>
								<li>
									<a href="javscript:void(0);">Country : {{customerDetails.country }}</a>
								</li>
								<li>
									<a href="javscript:void(0);">Address :
										<span>{{customerDetails.adderssLine }}</span>
									</a>
								</li>
								<li>
									<a href="javscript:void(0);">Type of Activity :
										<span>{{ customerDetails.activityType }} </span>
									</a>
								</li>
								<li>
									<a href="javscript:void(0);">Average Monthly Turnover :
										<span>{{ customerDetails.estimatedTurnoverAmount }}</span>
									</a>
								</li>
							</ul>
						</div>
						<div class="panel-heading ">
							<h3>Counter Parties</h3>
							
						</div>
						<div class="panel-body">
							<ul class="list-unstyled custom-list light-grey">							
								
								<li>
									<a href="javscript:void(0);">Geography of Business Operations :
										<span>{{ countriesListString }}</span>
									</a>
								</li>
								<li>
									<a style="pointer-events:none;">Main Counter Parties :
										<span class="text-light-blue">{{ counterPartiesListString }}</span>
									</a>
								</li>
							</ul>
						</div>
<!-- 						<div class="panel-heading"> -->
<!-- 							<h3>Documents</h3> -->
<!-- 						</div> -->
<!-- 						<div class="panel-body"> -->
<!-- 							<ul class="list-unstyled custom-list light-grey custom-list-li-a--lh"> -->
<!-- 								<li> -->
<!-- 									<a href="javscript:void(0);">Passport </a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="javscript:void(0);">Certificate of Owenership of Business</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="javscript:void(0);">Financial Statements</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="javscript:void(0);">Registration Card</a> -->
<!-- 								</li> -->
<!-- 							</ul> -->
<!-- 						</div> -->
					</div>
					<!-- Chart Panel Ends -->

				</div>
				<!--  First Column Ends  -->

				<!--  Second Column Starts  -->
				<div class="col-sm-8 custom-col right-col border-l0 border-t0 main-right-parties-scroll">
					<div class="row custom-row">
						<div class="col-sm-12 custom-col ">
							<!-- Chart Panel Starts -->
							<div class="panel chart-panel">
								<div class="panel-heading scenario-amount-border">
									<div class="scenario-amount-wrapper">
										<div class="scenario-amount-details">
											<h3 class="scenario-name risky-name" style="color:{{currentColor}} !important">Scenario : {{ selectedScenario }}</h3>
											<p class="text-lighter-grey scenario-amount">Amount :
												<strong>{{ customerAmount | currency}}</strong>
											</p>
										</div>
<!-- 										<div class="scenario-risk-score"> -->
<!-- 											<p class="text-lighter-grey">Risk Score : -->
<!-- 												<span>19</span> -->
<!-- 											</p> -->
<!-- 										</div> -->
									</div>
								</div>
								<div class="panel-heading">
									<h3>Counter Parties</h3>
								</div>
								<div class="panel-body ">
									<div class="side-risk-chart-wrapper">
										<div id="custCounterPartyChart"></div>
									</div>
								</div>
							</div>
							<!-- Chart Panel Ends -->
						</div>
						<div class="col-sm-6 custom-col">
							<!-- Chart Panel Starts -->
							<div class="panel chart-panel border-b0">
								<div class="panel-heading">
									<h3>Countries</h3>
								</div>
								<div class="panel-body">
									<div id="riskAlertModalCountries"></div>
								</div>
							</div>
							<!-- Chart Panel Ends -->
						</div>
						<div class="col-sm-6 custom-col">
							<!-- Chart Panel Starts -->
							<div class="panel chart-panel border-0">
								<div class="panel-heading">
									<h3>Geography</h3>
								</div>
								<div class="panel-body">
									<div id="riskAlertModalGeography"></div>
								</div>
							</div>
							<!-- Chart Panel Ends -->
						</div>
					</div>
				</div>
				<!--  Second Column Ends  -->

				<!--  Third Column Starts  -->
				<div class="col-sm-12 custom-col risk-custom-col">
					<!--  Custom Tabs Wrapper Starts  -->
					<div class="custom-tabs-wrapper">

						<!--  Nav Tabs Starts  -->
						<ul class="nav nav-tabs">
							<li ng-class="{'active': detectedTransactionTab}" ng-click="tabClicked('detectedTransaction')">
								<a class="a-pad">Detected Transaction</a>
							</li>
							<li ng-class="{'active': !detectedTransactionTab}" ng-click="tabClicked('allTransactions')">
								<a>All Transactions</a>
							</li>
						</ul>
						<!--  Nav Tabs Ends  -->

						<!--  Tab Panes Starts  -->
						<div class="tab-content">

							<!--  Notifications Tab Starts  -->
							<div class="tab-pane" ng-class="{'active': detectedTransactionTab}" id="detectedTransaction">
								<table class="table custom-table-stripped">
									<thead>
										<tr>
											<th>Data</th>
											<th>Type</th>
											<th>Counterparty</th>
                                            <th>Country of Counterparty</th>
											<th>Amount</th>
											<th>Counterparty Bank</th>
											<th>Bank Location</th>
										</tr>
									</thead>
									<tbody>
									<tr ng-repeat="txns in detectedTransactions">
										<td>{{ txns.date | date:'dd-MM-yyyy' }}</td>
										<td>{{ txns.transactionType }}</td>
										<td>{{ txns.counterParty }}</td>
										<td>{{ txns.country }}</td>
										<td>{{ txns.amount }}</td>
										<td>{{ txns.counterPartyBank }}</td>
										<td>{{ txns.country }}</td>
									</tr>
										
									</tbody>
								</table>
								 <div class="text-right">
                                      <ul class="transaction-alert-pagination" uib-pagination total-items="detectedTransactionsLength" items-per-page="{{ itemsPerPage }}" ng-model="pageNum"
                                          ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true">

                                      </ul>
                                  </div>
							</div>
							<!--  Notifications Tab Ends  -->

							<!--  Notifications Tab Starts  -->
							<div class="tab-pane" ng-class="{'active': !detectedTransactionTab}" id="allTransactions">
								<table class="table custom-table-stripped">
									<thead>
										<tr>
											<th>Data</th>
											<th>Type</th>
											<th>Counterparty</th>
                                            <th>Country of Counterparty</th>
											<th>Amount</th>
											<th>Counterparty Bank</th>
											<th>Bank Location</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="txns in allTransactions">
										<td>{{ txns.date | date:'dd-MM-yyyy' }}</td>
										<td>{{ txns.transactionType }}</td>
										<td>{{ txns.counterParty }}</td>
										<td>{{ txns.country }}</td>
										<td>{{ txns.amount }}</td>
										<td>{{ txns.counterPartyBank }}</td>
										<td>{{ txns.country }}</td>
									</tr>
									</tbody>
								</table>
								 <div class="text-right">
                                      <ul class="transaction-alert-pagination" uib-pagination total-items="allTransactionsLength" items-per-page="{{ itemsPerPage }}" ng-model="pageNumAll"
                                          ng-change="pageChanged(pageNumAll,'all')" max-size="2" class="pagination-sm" boundary-link-numbers="true">

                                      </ul>
                                  </div>
							</div>
							<!--  Notifications Tab Ends  -->

						</div>
					</div>
					<!--  Custom Tabs Wrapper Ends  -->
				</div>
				<!--  Third Column Ends  -->

			</div>
			<!--  Risk Row Ends  -->
		</div>
		<!--  Risk Grid Starts  -->
	</div>
	<!--  Alert Dashboard Wrapper Starts  -->
</div>
<!--  Risk Modal Ends  -->
<script>
$(document).ready(function() {
		/*Custom Scroll Bar*/
		$('.transaction-modal .modal-body .alert-dashboard-wrapper').mCustomScrollbar({
			axis : "y",
			theme : "minimal-dark"
		});
	});
</script>

<script type="text/ng-template" id="viewAllTrans.modal.html">
<div class="modal-header">
    <button type="button" class="closePulseModal" ng-click="closeViewAllTransModal()" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" ng-hide="strReported">{{modalHeader}}</h4>
</div>
<div class="modal-body">
<span ng-show="viewAllTxsPreloader" class="custom-spinner case-dairy-spinner"> 
	<i class="fa fa-spinner fa-spin fa-2x"></i>
</span>
    <div class="row text-uppercase">
        <div class="col-sm-4">
            <p class="text-lighter-grey">total transaction :
                <span>{{txsdetails.totalTransactions}}</span>
            </p>
        </div>
        <div class="col-sm-4">
            <p class="text-lighter-grey">total amount :
                <span ng-if="productType == 'ActivityType' || productType == 'Banks' || productType == 'GeoGraphic' || productType == 'BankLocations' || productType == 'Geography' || productType == 'Shareholders' || productType == 'CorporateStructure' ||  productType == 'allTxs' ||  productType == 'CustomerRisk' || productType == 'ProductRisk' || productType == 'CorruptionRisk' || productType == 'PoliticalRisk' || productType == 'GeoGraphicRisk'">
                	{{ txsdetails.totalAmount }}
               	</span>
                <span ng-if="productType != 'ActivityType' && productType != 'Banks' && productType != 'GeoGraphic' && productType != 'BankLocations' && productType != 'Geography' && productType != 'Shareholders' && productType != 'CorporateStructure' &&  productType != 'allTxs' &&  productType != 'CustomerRisk' && productType != 'ProductRisk' && productType != 'CorruptionRisk' && productType != 'PoliticalRisk' && productType != 'GeoGraphicRisk'">
                	$ {{ txsdetails.totalAmount | number : 1 }}
               	</span>
            </p>
        </div>
        <div class="col-sm-4">
            <p class="text-lighter-grey">alerted :
                <span>{{txsdetails.alerted}}</span>
            </p>
        </div>
    </div>
    <div class="custom-data-table-wrapper tab-content">
        <table class="table transaction-alerts-table custom-table-stripped" datatable="ng" dt-options="dtOptions1">
            <thead>
                <th width="20%">Date</th>
                <th width="20%">Time</th>
                <th width="40%">Alerted amount</th>
                <th width="20%">transaction amount</th>
            </thead>
            <tbody>
                <tr ng-repeat="txsData in txsDataByproductType track by $index">
                    <td>{{txsData.txDate}}</td>
                    <td>{{txsData.txTime}}</td>
                    <td>{{txsData.alertAmount | currency}}</td>
                    <td>{{txsData.txAmount | currency}}</td>
                </tr>
            </tbody>
        </table>
        <div class="text-right" ng-hide="txsLength == 0 || txsLength <= 15">
	         <ul class="transaction-alert-pagination" uib-pagination total-items="txsLength" items-per-page="15" ng-model="pageNum"
	             ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true">
	         </ul>
   	    </div>
    </div>
</div>
<!-- <div class="modal-footer" ng-hide="strReported">

</div> -->
</script>



<script type="text/ng-template" id="createAlertTi.modal.html">
<div class="modal-header">
	<button type="button" class="close" aria-hidden="true" ng-click="closeModal()"><span>&#x2716;</span></button>
	<h4 class="modal-title"><span style="cursor:pointer" ng-hide="selectAlert"><i class="fa fa-chevron-left" ng-click="selectAlert =true;header='Generate/Upload An Alert'"></i> &nbsp </span>{{ header }}</h4>
</div>
<div class="modal-body">
<!--  Spinner Starts  -->
<div class="custom-spinner" ng-show="showSpinner1">
	<i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<!--  Spinner Ends  -->
	<!--  Entity Company Starts  -->
	<form class="form">
		<div class="ti-alert-type" ng-show="selectAlert">
			
			<div class="alert-selection-options">
				<ul class="list-inline">
					<li>
						<a href ng-click="showUploadAlert();header='Upload An Alert'" class="alert-select-upload">
							<span class="alert-icon"><i class="fa fa-upload"></i></span>
							<span class="alert-name">Upload</span>
						</a>
					</li>
					<li>
						<a href ng-click="showGenerateAlert();header='Generate An Alert'" class="alert-select-generate">
							<span class="alert-icon"><i class="fa fa-refresh"></i></span>
							<span class="alert-name">Generate</span>
						</a>
					</li>
				</ul>
			</div>
			<!-- <div class="custom-radio">
				<label class="radio-inline">
					<input class="" type="radio" name="tiAlertType" id="tiCustomer" checked value="Customer">
					<span>
						<i class="fa fa-circle"></i>
					</span>Customer
				</label>
				<label class="radio-inline">
					<input class="" type="radio" name="tiAlertType"  id="tiTransaction" value="Transaction">
					<span>
						<i class="fa fa-circle"></i>
					</span>Transaction
				</label>
				<label class="radio-inline">
					<input class="" type="radio" name="tiAlertType" id="tiAccount" value="Account">
					<span>
						<i class="fa fa-circle"></i>
					</span>Account
				</label>
			</div> -->
		</div>
		<div class="row" ng-hide="selectAlert">
			<div class="col-sm-6">
				<div class="ti-alert-upload">
					<!-- <h5>File</h5> -->
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="uploadCustomerDetails" id="uploadCustomerDetails" class="">
		                     <label for="uploadCustomerDetails" class="file-inline" ngf-select="upload($file,'customers')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ uploadCustomerDetailsName }}">{{ uploadCustomerDetailsName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="ti-alert-upload">
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="uploadAccountFile" id="uploadAccountFile" class="">
		                     <label for="uploadAccountFile" class="file-inline" ngf-select="upload($file,'accounts')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ uploadAccountName }}">{{ uploadAccountName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="ti-alert-upload">
					<!-- <h5>File</h5> -->
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="uploadCustomerRelation" id="uploadCustomerRelation" class="">
		                     <label for="uploadCustomerRelation" class="file-inline" ngf-select="upload($file,'customerRelationships')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ uploadCustomerRelationShiptxName }}">{{ uploadCustomerRelationShiptxName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
<div class="col-sm-6">
				<div class="ti-alert-upload">
					<!-- <h5>File</h5> -->
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="uploadShareHolders" id="uploadShareHolders" class="">
		                     <label for="uploadShareHolders" class="file-inline" ngf-select="upload($file,'shareHolders')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ uploadShareHolderstxName }}">{{ uploadShareHolderstxName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
			<div class="col-sm-6" ng-hide="isGenerate">
				<div class="ti-alert-upload">
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="fileuploadAlerts" id="fileuploadAlerts" class="">
		                     <label for="fileuploadAlerts" class="file-inline" ngf-select="upload($file,'alert')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ alertName }}">{{ alertName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="ti-alert-upload">
					<!-- <h5>File</h5> -->
					<div class="custom-upload-wrapper">
						<div class="custom-file-upload">
		                     <input type="file" name="uploadTxData" id="uploadTxData" class="">
		                     <label for="uploadTxData" class="file-inline" ngf-select="upload($file,'transactions')">
		                         <span class="custom-input">
		                     		<strong class="text-uppercase uploaded-name" title="{{ uploadTxDataName }}">{{ uploadTxDataName }} </strong>
		                   			<small class="text-uppercase">Upload</small>
		                     	</span>
		                     </label>
		                 </div>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	<!--  Entity Company Ends  -->
</div>
<div class="modal-footer" ng-hide="selectAlert">
   <!--<button type="button" ng-click="uploadFile()" class="btn btn-update">Create</button> -->
<ul class="list-unstyled" style="float:left;text-align:left">
			<li><a href="../vendor/data/CUSTOMERS.csv"><i class="fa fa-download"></i> Sample Customers File</a></li>
			<li><a href="../vendor/data/ACCOUNTS.csv"><i class="fa fa-download"></i> Sample Accounts File</a></li>
			<li><a href="../vendor/data/CUSTOMER_RELATIONSHIPS.csv"><i class="fa fa-download"></i> Sample Customer Relationships File</a></li>
			<li><a href="../vendor/data/TRANSACTIONS.csv"><i class="fa fa-download"></i> Sample Transactions File</a></li>
			<li><a href="../vendor/data/SHAREHOLDERS.csv"><i class="fa fa-download"></i> Sample Shareholders File</a></li>
			
			<li ng-hide="isGenerate"><a href="../vendor/data/ALERT.csv"><i class="fa fa-download"></i> Sample Alerts File</a></li>
</ul>
			
	<button type="button" class="btn btn-update" ng-click="processData()">Process Data</button>
</div>
</script>
<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('.chat-bot-wrapper').mCustomScrollbar({
           axis: "y"
       });
       $(".main-counter-parties-scroll").mCustomScrollbar({
           axis: "y",
           theme: "minimal-dark"
       });
   });
	
	
</script>
	</body>
</html>
