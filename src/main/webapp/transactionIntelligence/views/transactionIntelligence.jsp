<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<span ng-show="caseDetailsPreloader" class="act-custom-spinner">
	<i class="fa fa-spinner fa-spin fa-2x"></i>
</span>

<style>
.vlaCompareMain{
	height: calc(100vh - 115px);
}
.vlaCompareMain .resolve-analysis-wrapper{
    background-color: #21323a;
    padding: 10px;
}
#vlaCompare1{
    position: absolute;
    width: 100%;
 	height: 100% !important;
    }
#vlaCompare2{
    position: absolute;
    width: 100%;
    height: 100% !important;
    }
.vlaCompare1Division{
    border-right: 2px solid #292727;
    
}
#divvla-panelvlaCompare1{
    left: 0;
    width: calc(100% - 10px);
    position: initial;
    }
#divvla-panelvlaCompare2{
    left: 0;
    width: calc(100% - 10px);
    position: initial;
    }
#vlaCompare1 .vla-map{
  	top: 0;
  	z-index: -1;
  } 
#vlaCompare2 .vla-map{
  	top: 0;
  	z-index: -1;
  } 
</style>
<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>  
<!--  SubMenu Ends  -->

<div class="transaction-ocbc-wrapper" ng-hide="isCompareShow">
	<div class="ocbctop-filter-wrapper clearfix text-uppercase" >
		<div class="ocbcfilter scenario-filter">
			<h5 class="ocbcfilter-title">Transaction Type</h5>
			<div class="ocbc-filter-content">
				<select class="text-uppercase" id="setScenario" ng-model="setScenario" ng-change="init('','filters')">
					<option  vlaue="All">All</option>
					<option  ng-repeat='opt in TxtypeOptions' value="{{opt.value}}">{{opt.displayVal}}</option>
<!-- 					<option  vlaue="International Transfer">International Transfer</option> -->
<!-- 					<option vlaue="Wired Transfer">Wired Transfer</option> -->
<!-- 					<option vlaue="Wired Transfer">CreditCard Transfer</option> -->
<!-- 					<option vlaue="Wired Transfer">Cheque Transfer</option> -->
				</select>
			</div>
		</div>
<!-- 		<div class="ocbcfilter timeline-filter"> -->
<!-- 			<h5 class="ocbcfilter-title">Timeline</h5> -->
<!-- 			<div class="ocbc-filter-content"> -->
<!-- 				<div class="form-group"> -->
<!-- 					<label class="control-label">Min</label> -->
<!-- 					<select class="text-uppercase" id="minTimeline"> -->
<!-- 						<option>2</option> -->
<!-- 						<option>3</option> -->
<!-- 						<option>4</option> -->
<!-- 						<option>5</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
<!-- 				<div class="form-group"> -->
<!-- 					<label class="control-label">Max</label> -->
<!-- 					<select class="text-uppercase" id="maxTimeline"> -->
<!-- 						<option>6</option> -->
<!-- 						<option>7</option> -->
<!-- 						<option>8</option> -->
<!-- 						<option>9</option> -->
<!-- 						<option>10</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
		<div class="ocbcfilter counter-parties-filter">
			<h5 class="ocbcfilter-title">Counter Parties</h5>
			<div class="ocbc-filter-content">
				<div class="form-group">
					<label class="control-label">Min</label>
					<input class="form-control" type="number" ng-model="taminCptyamoutFilter" ng-blur="init('','filters')" placeholder="Enter Amount"/>
		
				</div>
				<div class="form-group">
					<label class="control-label">Max</label>
					<input class="form-control" type="number" ng-model="tamaxCptyamoutFilter" ng-blur="init('','filters')" placeholder="Enter Amount"/>
		
				</div>
			</div>
		</div>
		<div class="ocbcfilter min-amount-filter">
			<h5 class="ocbcfilter-title">Minimum Amount</h5>
			<div class="ocbc-filter-content">
				<input class="form-control" type="number" ng-model="taminamoutFilter" ng-blur="init('','filters')" placeholder="Enter Amount"/>
			</div>
		</div>
		<div class="ocbcfilter minimum-tranx-filter">
			<h5 class="ocbcfilter-title">Minimum Transactions</h5>
			<div class="ocbc-filter-content">
				<input class="form-control" type="number" ng-model="taminTxFilter" ng-blur="init('','filters')" placeholder="Enter Amount"/>
		
			</div>
		</div>
		<div class="ocbcfilter locations-filter">
			<h5 class="ocbcfilter-title">Locations</h5>
			<div class="ocbc-filter-content">
				<div class="filtered-locations-list">
					<div class="row">
						<div class=" col-sm-6" ng-class="locationClass[$index]" ng-repeat="location in locations" ng-click="toggleLocationClass($index)"><!-- Write ng-repeat here -->
							<a class="" href="javascript:void(0);">{{ location }}</a>
						</div>
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Paris</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Dublin</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Cairo</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6">This is mandatory do not remove this -->
<!-- 							<a class="add-new-filter" id="addLocationFilter" href="javascript:void(0);"><i class="fa fa-plus"></i> Add</a> -->
<!-- 						</div> -->
					</div>
				</div>
			</div>
		</div>
<!-- 		<div class="ocbcfilter terminal-locations-filter"> -->
<!-- 			<h5 class="ocbcfilter-title">Post Terminal Locations</h5> -->
<!-- 			<div class="ocbc-filter-content"> -->
<!-- 				<div class="filtered-locations-list"> -->
<!-- 					<div class="row"> -->
<!-- 						<div class=" col-sm-6">Write ng-repeat here -->
<!-- 							<a class="" href="javascript:void(0);">London</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Paris</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Dublin</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Johnesburg</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6"> -->
<!-- 							<a class="" href="javascript:void(0);">Cairo</a> -->
<!-- 						</div> -->
<!-- 						<div class=" col-sm-6">This is mandatory do not remove this -->
<!-- 							<a class="add-new-filter" id="addPostTerminalLocationFilter" href="javascript:void(0);"><i class="fa fa-plus"></i> Add</a> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
	</div>
	<div class="transaction-ocbc-results">
		<div class="vla-body">
			<div class="custom-vla sidebar-open">
				<!--  Navbar Wrapper Starts  -->
				<span class="custom-spinner custom-spinner-main full-custom-spinner"> <i
					class="fa fa-spinner fa-spin fa-2x"></i>
				</span>
				<div class="header-wrapper">
					<!---    Side Bar Wrapper    ---->
		
					<div id="mainsidewrapper" class=" mainsidewrapper sidebar-wrapper-main">
						<aside class=" navbar-static-side1">
							<div class="panel-group" id="accordion" role="tablist"
								aria-multiselectable="true">
								<div class=" panel panel-default"  >
								<!-- 	<div class="panel-heading" role="tab" id="headingOne"
										style="padding: 0;" ng-click="model.onBasicInfo($event);">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion1" 
												aria-expanded="true" aria-controls="collapseOne"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showBasicInfo ? true : false}">
												BASIC INFORMATION <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div> -->
									<!-- <div id="collapseOne" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingOne" ng-show="model.showBasicInfo">
										<div class="panel-body">
											<div class="basic-header">
												<span>{{entityName}}</span>
											</div>
											<ul class="clearfix basicInfo">
											</ul>
											<div class="basic-content">
												<p>
													                                           PetroSaudi International is a privately owned oil exploration and production company, with offices in Saudi Arabia, England, and Switzerland.
		                                                                                        Founded in 2005, the company is paving a pathway into the future of the energy industry through compelling multinational projects on the ground and in the marketplace.... <span>readmore</span>
												</p>
											</div>
		
										</div>
									</div> -->
								</div>
		
								<div class="panel panel-default" id="displayFeatures_div_node" ng-click="model.onNode();">
									<div class="panel-heading" role="tab" id="headingSix"
										style="padding: 0;">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion2" 
												aria-expanded="false" aria-controls="collapseSix"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showOnNode ? true : false}">
												NODE <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
		
										</h4>
									</div>
									<div id="collapseSix" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingSix" ng-show="model.showOnNode">
										<div class="panel-body">
											<div class="node-styles-panel appearence-styles"></div>
										</div>
									</div>
								</div>
		
								<div class="panel panel-default" id="displayFeatures_div_edge" ng-click="model.onedge();">
									<div class="panel-heading" role="tab" id="headingSeven"
										style="padding: 0;">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion3" 
												aria-expanded="false" aria-controls="collapseSeven"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showOnEdge ? true : false}">
												EDGE <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseSeven" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingSeven" ng-show="model.showOnEdge">
										<div class="panel-body">
											<div class="edge-styles-panel  appearence-styles"></div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTen"
										style="padding: 0;" ng-click="model.ontxBytype();">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion10" 
												aria-expanded="false" aria-controls="collapseTen"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showtxBytype ? true : false}">
												Transactions By Type <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseTen" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingTen" ng-show="model.showtxBytype">
										<div class="panel-body">
 											<!-- <div id="txbyType_div"></div> -->
											<div class="transactionByType">
												<div class="riskscoreprogress">
													<div class="risk-progressbar-holder" ng-repeat = "txType in dataTxByTypeData"><!-- ng-repeat here -->
														<div class="progress-wrapper" title="{{txType.txt}} ">
															<div class="inline">{{txType.key}}</div>
															<div class="progress">
																<div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ txType.valluePct }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ txType.valPctInt }}%">
																	
																</div>
															</div>
														</div>
													</div>	

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingeleven"
										style="padding: 0;" ng-click="model.onshowdtvstx();">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion11" 
												aria-expanded="false" aria-controls="collapseeleven"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showdtvstx ? true : false}">
												Transactions Trend <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseeleven" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingeleven" ng-show="model.showdtvstx">
										<div class="panel-body tranx-trend-wrapper">
											<div id="showdtvstx_div"></div>
<!-- 											<h5 class="transaction-trend-avg">Average: {{dataTxByDateData[0].average}}</h5> -->
											<div id="showdtvstx_table">
												<table class="table text-uppercase"> 
													<thead>
														<tr>
															<th>Date</th>
															<th>Amount</th>
															<!--<th>Average</th>-->
															<th title="Mean deviation">Deviation(%)</th>
														</tr>
													</thead>
													<tbody>
														<tr ng-repeat="detail in dataTxByDateData">
															<td>{{ detail.date | date : "dd-MM-y" }}</td>
															<td>{{ detail.value  | currency:"$":0  }}</td>
															<!--<td>{{ detail.average | currency:"$":0 }}</td>-->
															<td>{{ detail.deviation  }}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default" id="displayadditionalDetails" ng-click="model.onadditionalDetails();">
									<div class="panel-heading" role="tab" id="headingEight"
										style="padding: 0;">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion8" 
												aria-expanded="false" aria-controls="collapseEight"
												style="display: block; padding: 10px; text-decoration: none;" ng-class="{'collapsed': model.showAdditionalDetails ? true : false}">
												ADDITIONAL DETAILS <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseEight" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingEight" ng-show="model.showAdditionalDetails">
										<div class="panel-body">
											<div id="entityDetails"></div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFive"
										style="padding: 0;" ng-click="model.onTagCloud();">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion5" 
												aria-expanded="false" aria-controls="collapseFive"
												style="display: block; padding: 11px; text-decoration: none;" ng-class="{'collapsed': model.showTagCloud ? true : false}">
												TAGCLOUD <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseFive" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingFive" ng-show="model.showTagCloud">
										<div class="panel-body">
											<div id="tagCloud_div"></div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree"
										style="padding: 0;" ng-click="model.onlocation();">
										<h4 class="panel-title" style="padding: 0; margin: 0;">
											<a  role="button" data-toggle="collapse"
												data-parent="#accordion4" 
												aria-expanded="false" aria-controls="collapseThree"
												style="display: block; padding: 10px; text-decoration: none;" ng-class="{'collapsed': model.showlocation ? true : false}">
												LOCATION <span class="toggle-sidebar-panel"><i
													class="fa fa-caret-down" aria-hidden="true"></i></span>
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse custom-slide-collapse in"
										role="tabpanel" aria-labelledby="headingThree" ng-show="model.showlocation">
										<div class="panel-body">
											<div id="worldmap_div" class="datamapsTA" style="position:relative"></div>
										</div>
									</div>
								</div>
							</div>
						</aside>
					</div>
		
				</div>
				<div id="vlaerror"></div>
				<div class="main-vla-section">
					<span class="custom-spinner full-custom-spinner"> 
						<i class="fa fa-spinner fa-spin fa-2x"></i>
					</span>
					<div id="vla"></div>
					<button class="expand-vla-section" ng-click="loadFullVLA()">
						<span><i class="fa fa-arrows-alt"></i></span>
					</button>
					<div class="transaction-panels-wrapper">
						<div class="transaction-panel">
							<div class="panel-group">
								<div class="panel panel-default" id="trans{{ transact.id }}" ng-repeat ="transact in transactionDetails"> <!-- ng-repeat here -->
								    <div class="panel-heading">
								    	<div class="tranx-panel-heading-content">
								    		<!-- <div class="transactionCheckbox">
									    		<div class="checkbox-wrapper">
									    			<div class="custom-checkbox">
									    				<label class="checkbox-inline">
									    					<input type="checkbox" id="{{'txCheck'+$index}}"  class="txcheckCls" name="{{'txCheck_'+$index}}" ng-model="txCheck" ng-click="highlightTx(transact,$index)">
									    					<span><i class="fa fa-check"></i></span>
								    					</label>
							    					</div>
						    					</div>
						    				</div> -->
								    		<ul class="list-inline">
												<li class="tranx-heading-list" ng-if="transact['is-alerted-transaction']">
								    				<i class="fa fa-exclamation-triangle" style="color: red;"></i>
								    			</li>
								    			<li class="tranx-heading-list" ng-if="transact['alert'] && !transact['is-alerted-transaction']">
								    				<i class="fa fa-exclamation-triangle" style="color: #fe9b93;"></i>
								    			</li>
								    			<li class="tranx-heading-list">
								    				<div class="panel-tranx-id">
								    					<p>ID : <span> {{ transact.id || "N/A"}}</span></p>
								    				</div>
								    			</li>
								    			<li class="tranx-heading-list">
								    				<div class="panel-tranx-date">
								    					<p>Date : <span> {{ (transact.date | date:"dd/MM/yyyy") || "N/A" }}</span></p>
								    				</div>
								    			</li>
								    			<li class="tranx-heading-list">
								    				<div class="panel-tranx-type">
								    					<p>Product Type : <span> {{ transact.txtype2 || "N/A" }}</span></p>
								    				</div>
								    			</li>
								    			<li class="tranx-heading-list">
								    				<div class="panel-tranx-counterparty">
								    					<p>Counter Party : <span> {{ transact.counterParty || "N/A"}} </span></p>
								    				</div>
								    			</li>
								    		</ul>
								    	</div>
								    </div>
								    <div class="panel-body">
								    	<div class="tranx-panel-body-content">
								    		<div class="row">
								    			<div class="col-sm-3">
								    				<div class="detailed-tranx-purpose">
								    					<ul class="list-unstyled">
								    						<li>
								    							<p>Channel : <span>{{transact.txype  || "N/A"}}</span></p>
								    						</li>
<!-- 								    						<li> -->
<!-- 								    							<p>Data Source : <span>{{ (transact['data-source']?transact['data-source']:"-") }}</span></p> -->
<!-- 								    						</li> -->
								    						<li>
								    							<p>Purpose : <span class="purpose-first">{{ transact.purpose1  || "N/A"}}</span> </p>
								    						</li>
								    					</ul>
								    				</div>
								    			</div>
								    			<div class="col-sm-3">
								    				<div class="detailed-tranx-counterparty">
								    					<ul class="list-unstyled">
								    						<li>
								    							<p class="detailed-tranx-header">Beneficiary </p>
								    						</li>
								    						<li>
								    							<p> Name : <span>{{ transact.namebene || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Amount : <span>{{ transact.activity_amount_bene || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Location : <span>{{ transact.countrybene || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Currency : <span>{{ transact.currencybene || "N/A" }}</span></p>
								    						</li>
								    					</ul>
								    				</div>
								    			</div>
								    			<div class="col-sm-3">
								    				<div class="detailed-tranx-ownparty">
								    					<ul class="list-unstyled">
								    						<li>
								    							<p class="detailed-tranx-header">Originator </span></p>
								    						</li>
								    						<li>
								    							<p>Name : <span>{{ transact.nameorg || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Amount : <span>{{ transact.activity_amount_org || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Location : <span>{{ transact.countryorg || "N/A" }}</span></p>
								    						</li>
								    						<li>
								    							<p>Currency  : <span>{{ transact.currencyorg || "N/A" }}</span></p>
								    						</li>
								    					</ul>
								    				</div>
								    			</div>
								    			<div class="col-sm-3">
								    				<div class="detailed-tranx-amount {{ transact.DirectoinCls}}">
								    					<p>{{ transact.inputOut }}</p>
								    					<strong>{{ (transact.amt  | currency:"$":0) || "N/A" }}</strong>
								    				</div>
								    				<!-- <div class="str-update">
								    					<button type="button" class="btn" ng-click="showResolveAnalysis('str')">STR</button>
								    				</div> -->
								    			</div>
								    		</div>
								    	</div>
								    </div>
								 </div>

							</div>
						</div>
					</div>
				</div>
				<div class="resolve-analysis-wrapper">
					<div class="resolve-analysis-content">
						<div class="resolve-analysis-button">
							<h5 class="text-uppercase">Apply Analysis</h5>
							<div class="apply-analysis-selection">
								<select class="text-uppercase" id="applyAnalysis" ng-model="applyAnalysisSelect">
									<option>Select</option>
									<option>Journey and Decision</option>
									<option>Taxonomy</option>
									<option>Comparison Analysis</option>
									<option>Social Network Analysis</option>
									<option>Dynamic and Temporal Analysis</option>
								</select>
								<a class="text-uppercase applyAnalysis" href="" ng-click="applyAnalysis();">Apply</a>
							</div>
							<a class="text-uppercase" href="" ng-click="showResolveAnalysis();">Resolve Analysis</a>
						</div>

						<div class="resolve-scenario-right-graph" ng-if ="sceanrioHeader">
							<h5 class="text-uppercase">Scenario</h5>
							<p>{{ sceanrioHeader }}</p>
							<div class="average-activity-scenario">
								<div class="riskscoreprogress">
									<div class="risk-progressbar-holder" ng-repeat = "activity in activityScenario">
										<div class="progress-wrapper" title="{{activity.txt}}">
											<div class="inline">{{activity.label}}</div>
											<div class="progress">
												<div class="progress-bar progressdr {{activity.cls}}-legend" role="progressbar" aria-valuenow="{{activity.valPctInt}}" aria-valuemin="0" aria-valuemax="100" style="width: {{activity.valPctInt}}%">
													
												</div>
											</div>
										</div>
									</div>		
								</div>
							</div>
 							<div class="" id="transactionScenario"></div>
						</div>
						<div class="associated-alerts-wrapper">
							<h5 class="text-uppercase">Associated Alerts</h5>
							<div ng-show="otherAlertsForCust.length > 0">
								<div class="associated-alerts-table">
									<table class="table text-uppercase"> 
										<tbody>
											<tr ng-repeat="alert in otherAlertsForCust">
												<td>{{ alert.scenario }}</td>
	<!-- 											<td>{{ alert.alertBusinessDate | date }}</td> -->
												<td>{{ alert.totalAmount | currency:'$':0 }}</td>
												<td class="text-center">
	                                                <button class="btn btn-analyze" ng-click="loadDashboard(alert)">
	                                                    Analyze</button>
	                                            </td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="associated-error-handling" ng-if="otherAlertsForCust.length == 0">
								<span>Data Not Found</span>
							</div>
						</div>
						<div class="resolve-analysis-donut-io clearfix">
							<h5 class="text-uppercase">transactions ratio</h5>
							<div class="" id="transactionIO"></div>
						</div>
						<div class="resolve-input-transactions-wrapper" ng-show="inputDetails.total.sum">
							<h5 class="text-uppercase">Input Transactions</h5>
<!-- 							<div class="input-transaction-table"> -->
<!-- 								<table class="table text-uppercase"> -->
<!-- 									<thead> -->
<!-- 										<tr> -->
<!-- 											<th>Name</th> -->
<!-- 											<th>Type</th> -->
<!-- 											<th>Country</th> -->
<!-- 											<th>No</th> -->
<!-- 											<th>Amount</th> -->
<!-- 										</tr> -->
<!-- 									</thead> -->
<!-- 									<tbody> -->
<!-- 										<tr ng-repeat="input in inputDetails.details"> -->
<!-- 											<td>{{ input.point.name }}</td> -->
<!-- 											<td>{{ input.point.type }}</td> -->
<!-- 											<td>{{ input.point.country }}</td> -->
<!-- 											<td>{{ input.count }}</td> -->
<!-- 											<td>{{ input.sum  | currency:"$":0 }}</td> -->
<!-- 										</tr> -->
										
<!-- 									</tbody> -->
<!-- 								</table> -->
<!-- 							</div> -->
							<div class="totalInputTranxAmount text-right">
								<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ inputDetails.total.sum | currency:"$":0 }}</span></div>
							</div>
							<div class="input-transactions-barcharts" id="inputTranx">
								<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCpty in inputDetails.counterparty ">
											<div class="inline text-uppercase">{{inputCpty.point.displayName || "Unknown" }}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCpty.sumRatio ?inputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCpty.sumformatted
												 }}</div>
										</div>
									</div>
							</div>
							<div class="input-transactions-barcharts" id="inputCounties">
								<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCountry in inputDetails.country ">
											<div class="inline text-uppercase">{{inputCountry.country.country || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCountry.sumRatio ?inputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCountry.sumformatted
												 }}</div>
										</div>
									</div>
							</div>
						</div>
						<div class="resolve-output-transactions-wrapper" ng-show="outputDetails.total.sum">
							<h5 class="text-uppercase">Output Transactions</h5>
<!-- 							<div class="output-transaction-table"> -->
<!-- 								<table class="table text-uppercase"> -->
<!-- 									<thead> -->
<!-- 										<tr> -->
<!-- 											<th>Name</th> -->
<!-- 											<th>Type</th> -->
<!-- 											<th>Country</th> -->
<!-- 											<th>No</th> -->
<!-- 											<th>Amount</th> -->
<!-- 										</tr> -->
<!-- 									</thead> -->
<!-- 									<tbody> -->
<!-- 										<tr ng-repeat="output in outputDetails.details"> -->
<!-- 											<td>{{ output.point.name }}</td> -->
<!-- 											<td>{{ output.point.type }}</td> -->
<!-- 											<td>{{ output.point.country }}</td> -->
<!-- 											<td>{{ output.count }}</td> -->
<!-- 											<td>{{ output.sum  | currency:"$":0 }}</td> -->
<!-- 										</tr> -->
										
<!-- 									</tbody> -->
<!-- 								</table> -->
<!-- 							</div> -->
							<div class="totalOutputTranxAmount text-right">
								<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ outputDetails.total.sum  | currency:"$":0 }}</span></div>
							</div>
							<div class="output-transactions-barcharts" id="outputTranx">
								<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCpty in outputDetails.counterparty ">
											<div class="inline text-uppercase">{{outputCpty.point.displayName || "Unknown" }}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCpty.sumRatio ?outputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCpty.sumformatted
												 }}</div>
										</div>
									</div>
							</div>
							<div class="output-transactions-barcharts" id="outputCounties">
								<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCountry in outputDetails.country ">
											<div class="inline text-uppercase">{{outputCountry.country.country || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCountry.sumRatio ?outputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCountry.sumformatted
												 }}</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="transaction-ocbc-wrapper transaction-ocbc-wrapper-split-view" ng-show="isCompareShow">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 pad-rt-0 pad-lt-0 custom-split-indicator vlaCompare1Division">
				<div class="compareSplit">
					<div class="vlaCompareMain">
						<span class="custom-spinner full-custom-spinner"> 
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</span>
						<div id="vlaCompare1" ></div>
					</div>
					<div class="resolve-analysis-wrapper">
						<div class="resolve-analysis-content">
							<div class="resolve-analysis-donut-io clearfix">
								<h5 class="text-uppercase">transactions ratio</h5>
								<div class="" id="transactionIOCompare1"></div>
							</div>
							<div class="resolve-input-transactions-wrapper" ng-show="snapShotData.inputDetails.total.sum">
								<h5 class="text-uppercase">Input Transactions</h5>
								<div class="totalInputTranxAmount text-right">
									<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ snapShotData.inputDetails.total.sum | currency:"$":0 }}</span></div>
								</div>
								<div class="input-transactions-barcharts" id="inputTranxCompare1">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCpty in snapShotData.inputDetails.counterparty ">
											<div class="inline text-uppercase">{{inputCpty.point.displayName || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCpty.sumRatio ?inputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCpty.sumformatted }}</div>
										</div>
									</div>
								</div>
								<div class="input-transactions-barcharts" id="inputCountiesCompare1">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCountry in snapShotData.inputDetails.country ">
											<div class="inline text-uppercase">{{inputCountry.country.country || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCountry.sumRatio ?inputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCountry.sumformatted}}</div>
										</div>
									</div>
								</div>
							</div>
							<div class="resolve-output-transactions-wrapper" ng-show="snapShotData.outputDetails.total.sum">
								<h5 class="text-uppercase">Output Transactions</h5>
								<div class="totalOutputTranxAmount text-right">
									<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ snapShotData.outputDetails.total.sum  | currency:"$":0 }}</span></div>
								</div>
								<div class="output-transactions-barcharts" id="outputTranxCompare1">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCpty in snapShotData.outputDetails.counterparty ">
											<div class="inline text-uppercase">{{outputCpty.point.displayName || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCpty.sumRatio ?outputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCpty.sumformatted}}</div>
										</div>
									</div>
								</div>
								<div class="output-transactions-barcharts" id="outputCountiesCompare1">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCountry in snapShotData.outputDetails.country ">
											<div class="inline text-uppercase">{{outputCountry.country.country || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCountry.sumRatio ?outputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCountry.sumformatted}}</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 pad-lt-0 pad-rt-0">
				<div class="compareSplit">
					<div class="vlaCompareMain">
						<span class="custom-spinner full-custom-spinner"> 
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</span>
						<div id="vlaCompare2"></div>
					</div >
					<div class="resolve-analysis-wrapper">
						<div class="resolve-analysis-content">
							<div class="resolve-analysis-donut-io clearfix">
								<h5 class="text-uppercase">transactions ratio</h5>
								<div class="" id="transactionIOCompare2"></div>
							</div>
							<div class="resolve-input-transactions-wrapper" ng-show="dataBythetimeofCompare.inputDetails.total.sum">
								<h5 class="text-uppercase">Input Transactions</h5>
								<div class="totalInputTranxAmount text-right">
									<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ dataBythetimeofCompare.inputDetails.total.sum | currency:"$":0 }}</span></div>
								</div>
								<div class="input-transactions-barcharts" id="inputTranxCompare2">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCpty in dataBythetimeofCompare.inputDetails.counterparty ">
											<div class="inline text-uppercase">{{inputCpty.point.displayName || "Unknown" }}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCpty.sumRatio ?inputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCpty.sumformatted}}</div>
										</div>
									</div>
								</div>
								<div class="input-transactions-barcharts" id="inputCountiesCompare2">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="inputCountry in dataBythetimeofCompare.inputDetails.country ">
											<div class="inline text-uppercase">{{inputCountry.country.country || "Unknown" }}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (inputCountry.sumRatio ?inputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ inputCountry.sumformatted}}</div>
										</div>
									</div>
								</div>
							</div>
							<div class="resolve-output-transactions-wrapper" ng-show="dataBythetimeofCompare.outputDetails.total.sum">
								<h5 class="text-uppercase">Output Transactions</h5>
								<div class="totalOutputTranxAmount text-right">
									<div class="text-uppercase"><span class="key">Total :</span> <span class="value">{{ dataBythetimeofCompare.outputDetails.total.sum  | currency:"$":0 }}</span></div>
								</div>
								<div class="output-transactions-barcharts" id="outputTranxCompare2">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCpty in dataBythetimeofCompare.outputDetails.counterparty ">
											<div class="inline text-uppercase">{{outputCpty.point.displayName || "Unknown"}}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCpty.sumRatio ?outputCpty.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCpty.sumformatted}}</div>
										</div>
									</div>
								</div>
								<div class="output-transactions-barcharts" id="outputCountiesCompare2">
									<div class="">
										<div class="progress-wrapper taProgressBarWrpper" ng-repeat="outputCountry in dataBythetimeofCompare.outputDetails.country ">
											<div class="inline text-uppercase">{{outputCountry.country.country || "Unknown" }}</div>
											<div class="taProgressBar" >
											<div class="progress" style="width: {{ (outputCountry.sumRatio ?outputCountry.sumRatio :0)  }}%">
												<div class="progress-bar progressdr" role="progressbar" style="width: 100%"></div>
											</div>
											</div>
											<div class="ng-binding progress-footer">{{ outputCountry.sumformatted}}</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- <script src="/elementexploration/static/VLA/WorldMap/js/script.js"></script> -->
<script>
	var actGetCanvas;

	function actCaptureDiv(divElement) {
		html2canvas($('#' + divElement), {
			onrendered : function(canvas) {
				actGetCanvas = canvas;
				$('#actWidgetCapturePreview').html(canvas);
				$('#actWidgetCapture').modal();
			}
		});
	}

	$('#actDownloadWidgetCapture').click(
			function() {
				var imageData = actGetCanvas.toDataURL("image/png");
				var newData = imageData.replace(/^data:image\/png/,
						"data:application/octet-stream");
				$('#actDownloadWidgetCapture').attr("download",
						$('#actCaptureFilename').val()).attr("href", newData);
			});

	$(document)
			.ready(
					function() {
						
						$(".resolve-analysis-content .associated-alerts-table").mCustomScrollbar({
							axis : "y"
						});
						
						$(".nav-tabs a").click(function() {
							$(this).tab('show');
						});

						$(".case-list-wrapper").mThumbnailScroller({
							axis : "x"
						});
						$(".upload-buttons-wrapper").mThumbnailScroller({
							axis : "x"
						});

						$('.widget-content').mCustomScrollbar({
							axis : "y",
							theme : "minimal"
						});
						$('.input-transactions-barcharts, .output-transactions-barcharts').mCustomScrollbar({
							axis : "y"
						});
						$(".vla-body .header-wrapper").mCustomScrollbar({
							axis : "y",
							theme : "minimal-dark"
						});
						$(".resolve-analysis-wrapper").mCustomScrollbar({
							axis : "y",
							theme : "minimal-dark"
						});
						$('.transaction-panels-wrapper').mCustomScrollbar({
							axis : "y",
							theme: "minimal-dark"
						});
						$(".nav-tabs a").click(function() {
							$(this).tab('show');
						});
						$("body").on(
								"click",
								".dropdown",
								function(e) {
									$(this).toggleClass('open');
									$(this).find('.dropdown-menu').css(
											'display', '');
									$(this).find('.first_dropdown')
											.removeClass('first_dropdown');
									$(this).find('.second_dropdown')
											.removeClass('second_dropdown');
									e.stopPropagation();
									e.preventDefault();
								});

						$("body").on("click", ".dropdown-submenu a.test",
								function(e) {
				           			($(this).siblings('ul').not($(this).next('ul'))).css("display", "none");
									$(this).next('ul').toggle();
									$(this).next('ul').find("select").val("nodeBackground");
									e.stopPropagation();
									e.preventDefault();
								})
						$('body').on('click', 'a.menu_first_dropdown',
								function(e) {
									$(this).toggleClass('first_dropdown');
									$('#custom-rearrange').val(window.layout);
									//            $(this).parent().find('.dropdown-menu').css('display','none');   
									e.stopPropagation();
									e.preventDefault();
								});
						$('body').on('click', 'a.menu_second_dropdown',
								function(e) {
									$(this).toggleClass('second_dropdown');
									//            $(this).parent().find('.dropdown-menu').css('display','none');   
									e.stopPropagation();
									e.preventDefault();
								});

						$('body')
								.on(
										'click',
										'a.first_sub_menu',
										function(e) {
											var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
											for(var i=0; i< window.CurentVLAIDs.length; i++){
												if('#' + Id[0].id === window.CurentVLAIDs[i]){
													$(window.CurentVLAIDs[i])
														.attr('style',
														'background-color: #283c45 !important;');
												}
											}
											
											$(this)
													.toggleClass(
															'second_first_sub_dropdown');
											if (!$(this)
													.hasClass(
															'second_first_sub_dropdown_check'))
												$(this)
														.addClass(
																'second_first_sub_dropdown_check');

											$(this)
													.parent()
													.find(
															'.second_second_sub_dropdown_check')
													.removeClass(
															'second_second_sub_dropdown_check');
											setTimeout(
													function() {
														var nodes = _cy_
																.elements();

														nodes.addClass('risks');
														nodes
																.removeClass('whiteTheme');
														nodes
																.removeClass('nodewithoutBackground');
														e.stopPropagation();
														e.preventDefault();
													}, 0);
										});
						$('body')
								.on(
										'click',
										'a.second_sub_menu',
										function(e) {
											var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
											for(var i=0; i< window.CurentVLAIDs.length; i++){
												if('#' + Id[0].id === window.CurentVLAIDs[i]){
													$(window.CurentVLAIDs[i])
															.attr('style',
																	'background-color: #F8F8FF !important;');
												}
											}
											$(this)
													.toggleClass(
															'second_second_sub_dropdown');
											if (!$(this)
													.hasClass(
															'second_second_sub_dropdown_check')) {
												$(this)
														.addClass(
																'second_second_sub_dropdown_check');
											}

											// console.log($('#configurations').parent());
											$(this)
													.parent()
													.find(
															'.second_first_sub_dropdown_check')
													.removeClass(
															'second_first_sub_dropdown_check');
											setTimeout(
													function() {
														var nodes = _cy_
																.elements();
														nodes
																.addClass('whiteTheme');
														nodes
																.removeClass('risks');
														nodes
																.removeClass('nodewithoutBackground');
														e.stopPropagation();
														e.preventDefault();
														//  $('.darkTheme').parent().css('display','none');
														// $('.whiteTheme').parent().css('display','block');
														// $('.second_second_sub_dropdown').siblings('.dropdown-menu').css('display','block');
													}, 0);

										});
					});
</script>
<script>
	function openModal() {
		$('#basicInfoModal').modal('show');
		$('#basicInfoModal .modal-title').html(window.entityName);
		$('#basicInfoModal .modal-body p').html(window.count);
	};
</script>