<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Main Contents Wrapper Starts  -->
        <div class="main-contents-wrapper">

            <!--  Alert Dashboard Wrapper Starts  -->
            <div class="dashboard-wrapper alert-dashboard-wrapper bg-dark-grey1">
				
				<div class="remote-filters-wrapper" ng-if="filterArr.length>0">
					<ul class="list-inline remote-filters-list-wrapper">
						<li class="remote-filters-list" ng-repeat= "filter in filterArr">
							<a href class="remote-filter">
								<span>{{ filtersNamesToShow[filter.type] }} -> {{ filter.value }}</span>
								<i class="fa fa-times" ng-click="removeFilter($index,filter)"></i>
							</a>
						</li>
					</ul>
				</div>
				
                <!--  Custom Tabs Wrapper Starts  -->
                <div class="custom-tabs-wrapper">
                    <div class="datepicker-wrapper">
                        <div id="notification_reportrange" class="range-wrapper">
                            <i class="fa fa-calendar-o icon-left"></i>
                            <span></span>
                            <i class="fa fa-angle-down icon-right"></i>
                        </div>
                    </div>
                    <ul class="list-inline nav-btn-list">
                        <li ng-click="createAlertModal();">
                            <a href="javascript:void(0);" class="text-uppercase">
                                <i class="fa fa-plus icon"></i> Generate / Upload an alert</a>
                        </li>
                    </ul>

                    <!--  Nav Tabs Starts  -->
                    <uib-tabset active="active">
                        <uib-tab index="0" heading="Notification" id="notification_tab" ng-click="loadChart_notification('notification')">
                            <!--  Notifications Tab Starts  -->
                            <div class="alert-notification-wrapper" id="notification">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="list-inline">
                                                        <li class="">
                                                            <h4 class="top-filter-heading">Total Transactions</h4>
                                                            <strong class="value">{{notificationtxsAmount || 0}}
                                                            </strong>
                                                        </li>
                                                        <li class="mar-l15">
                                                            <ul class="list-inline">
                                                                <li class="">
                                                                    <h4 class="top-filter-heading">Alerts Generated</h4>
                                                                    <strong class="value text-center">{{notificationStatusCountTotal || 0}}
                                                                        <span class="divide-value text-center">/ {{pieValue || 0}} %</span>
                                                                    </strong>
                                                                </li>
                                                                <li class="mar-l15">
                                                                    <h4 class="top-filter-heading">Alert Ratio</h4>
                                                                    <div id="alertsChartPie" class="text-center">
                                                                        <div class="c100 p{{pieValue}} pink radial">
                                                                            <span>{{pieValue || 0}}</span>
                                                                            <div class="slice">
                                                                                <div class="bar"></div>
                                                                                <div class="fill"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top 3 Transactions</h4>
                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar1 list-unstyled progressbar-list"   ng-if="notification_top_transaction_type_progressData.length > 0">
                                                        <li class="progressbar-list-item by-amount-bar"  ng-repeat="pData in notification_top_transaction_type_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(notification_top_transaction_type_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Transactions Types</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar2 list-unstyled progressbar-list" ng-if="notification_transaction_type_progressData.length > 0">
                                                        <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in notification_transaction_type_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(notification_transaction_type_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Alert Status</h4>
                                                    <div id="alertStatusChart"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <div class="row">
                                    <div class="col-sm-7 pad-r0">

                                        <div class="transaction-alerts-wrapper">
                                            <div class="transaction-alerts-header">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <a href="javascript:void(0);" class="title">
                                                            <i class="fa fa-bell-o icon"></i> Alert Notifications
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-sm-8 custom-select-wrapper">
                                                                <label class="control-label col-sm-5 text-right pad-x0 text-uppercase">Sort By <span class="mar-l5">:</span></label>
                                                                <div class=" col-sm-7 pad-0">
                                                                    <select class="form-control text-uppercase" ng-model="checkedValue" ng-change="changeDropDown(checkedValue)">
                                                                        <option value="ASC">Ascending</option>
                                                                        <option value="DESC">Descending</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 clearfix">
                                                                <div class="input-group pull-right custom-input-group right-blue">
                                                                    <span class="input-group-addon" id="basic-addon1">
                                                                        <i class="fa fa-search"></i>
                                                                    </span>
                                                                    <input type="text" ng-model="alertSearchName" ng-keypress="changeSearchBox($event,alertSearchName)" class="form-control"
                                                                        placeholder="Search" aria-describedby="basic-addon1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table transaction-alerts-table" ng-if="listAlerts.length > 0">
                                                <!-- <table class="table transaction-alerts-table" datatable="ng" dt-options="dtOptions"> -->
                                                <tbody>
                                                    <tr ng-repeat="listData in listAlerts track by $index">
                                                        <td>
                                                            <p>
                                                                {{listData.name}}
                                                            </p>
                                                            <span>
                                                                {{listData.scenario}}
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span>{{listData.date}}</span>
                                                            <!--                                                             <span>Score : 45</span> -->
                                                        </td>
                                                        <td>
                                                            <strong>{{listData.amount}}</strong>
                                                        </td>
                                                        <td class="text-center">
                                                            <button class="btn btn-analyze" ng-click="loadDashboard(listData)">
                                                                Analyze</button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(listAlerts)">
                                            	<span >Data Not Found</span>
                                            </div>
                                            <div class="text-right" ng-style="{'display': listAlerts.length === 0 ? 'none': 'block'}">
                                                <ul class="transaction-alert-pagination" uib-pagination total-items="alertsLength" items-per-page="20" ng-model="pageNum"
                                                    ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-5 pad-l0">

                                        <!--  Chart Panel Wrapper Starts  -->
                                        <div class="chart-panel-wrapper">

                                            <div class="row custom-row">

                                                <!--  First Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alert / Transaction Trend</h3>
                                                        </div>
                                                        <div class="panel-body">

                                                            <div id="linechartnotification"></div>
                                                        </div>
                                                        <span ng-if="lineChartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Risks</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="RiskPieChart"></div>
                                                        </div>
                                                        <span ng-if="riskPieChartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->
                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transaction Types</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="TransactipTypesPieChart"></div>
                                                        </div>
                                                        <span ng-if="transactipTypesPieChartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->

                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Corporate Structure</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="coperateStructure">
                                                                <!--  Progressbar List Starts -->
                                                                
                                                                <ul class="coperateStructure_new list-unstyled progressbar-list" ng-if="notification_corporateStructureProgressBar.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in notification_corporateStructureProgressBar">
                                                                        <div class="left-col">{{pData.key}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                                aria-valuemax="100" style="width:{{pData.value}}%">
                                                                                <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{pData.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if= "checkToShowDtaNtFnd(notification_corporateStructureProgressBar)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                            <span ng-if="loader.corporateStructureloader" class="custom-spinner case-dairy-spinner">
                                                                <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->

                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alert Comparission</h3>
                                                        </div>
                                                        <div class="panel-body">
<div class="d-ib mar-x5" ><span style="width:10px;height:10px;font-size:8px;border-radius:50%;margin-right:5px;background-color:{{legendData.color}}'"></span><span style="font-size:10px;">{{legendData.name}}</span></div>
                                                           <!--  <ul class="list-inline custom-list circle chart-legends chart-legends-notify">
                                                                <li ng-repeat="legendData in barchartnotificationLegendData">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{legendData.color}} !important"></i>{{legendData.name}} </a>
                                                                </li>
                                                            </ul> -->

                                                            <div id="barchartnotification"></div>
                                                        </div>
                                                        <span ng-if="barchartnotificationPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Counterparties</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="counterparties1">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="counterparties1_new list-unstyled progressbar-list" ng-if="notification_counterpartiesProgressBar.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in notification_counterpartiesProgressBar">
                                                                        <div class="left-col">{{pData.key}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                                aria-valuemax="100" style="width:{{pData.value}}%">
                                                                                <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{pData.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(notification_counterpartiesProgressBar)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Associated Entity</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="associatedEntitychart"></div>
                                                        </div>
                                                        <span ng-if="associatedEntitychartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->

                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Counterparty Locations</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="counterpartyWorldMap"></div>
                                                        </div>
                                                        <span ng-if="counterpartyWorldMapPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->
                                            </div>

                                        </div>
                                        <!--  Chart Panel Wrapper Ends  -->

                                    </div>
                                </div>
                            </div>
                            <!--  Notifications Tab Ends  -->
                        </uib-tab>


                        <uib-tab index="1" heading="Dashboard" id="dashboard_tab" ng-click="current_tab='aml';loadChart_aml('aml')">
                            <!-- ng-click="loadChart_notification('notification')" -->
                            <!--  Notifications Tab Starts  -->
                            <div class="alert-notification-wrapper" id="antiMoneyLaundering">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="list-inline">

                                                        <li class="mar-l15">
                                                            <ul class="list-inline">
                                                                <li class="">
                                                                    <h4 class="top-filter-heading">Alerts Generated</h4>
                                                                    <strong class="value text-center">{{notificationStatusCountTotal || 0}}
                                                                        <span class="divide-value text-center">/ {{pieValue  || 0}} %</span>
                                                                    </strong>
                                                                </li>

                                                            </ul>
                                                        </li>
                                                        <li class="">
                                                            <h4 class="top-filter-heading">No of Transactions</h4>

                                                            <strong class="value text-center">{{txsCount}}
                                                              </strong>

                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Customers</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar3 list-unstyled progressbar-list" ng-if="aml_top_customers_progressData.length > 0">
                                                        <li class="progressbar-list-item" ng-repeat="pData in aml_top_customers_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(aml_top_customers_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                                <span ng-if="dashboardTabloader.dashboradTopCustomersloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Scenarios</h4>
                                                    <div id="topScenariosChart"></div>
                                                </div>
                                                <span ng-if="dashboardTabloader.topScenariosChartloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Branches</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar4 list-unstyled progressbar-list" ng-if="aml_top_branches_progressData.length > 0">
                                                        <li class="progressbar-list-item" ng-repeat="pData in aml_top_branches_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(aml_top_branches_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                                <span ng-if="dashboardTabloader.dashboardTopBranchesloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Geography</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progress_bar5 list-unstyled progressbar-list" ng-if="aml_top_branches_progressData.length > 0">
                                                        <li class="progressbar-list-item" ng-repeat="pData in aml_top_branches_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                            <span ng-if="dashboardTabloader.dashboardTopBranchesloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   														    </span>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(aml_top_branches_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <div class="row">
                                    <div class="col-sm-7 pad-r0">

                                        <div class="transaction-alerts-wrapper">
                                            <div class="transaction-alerts-header">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <a href="javascript:void(0);" class="title">
                                                            <i class="fa fa-bell-o icon"></i> Alert Notifications
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-sm-8 custom-select-wrapper">
                                                                <label class="control-label col-sm-5 text-right pad-x0 text-uppercase">Sort By <span class="mar-l5">:</span></label>
                                                                <div class=" col-sm-7 pad-0">
                                                                    <select class="form-control text-uppercase" ng-model="checkedValue" ng-change="changeDropDown(checkedValue)">
                                                                        <option value="ASC">Ascending</option>
                                                                        <option value="DESC">Descending</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 clearfix">
                                                                <div class="input-group pull-right custom-input-group right-blue">
                                                                    <span class="input-group-addon" id="basic-addon1">
                                                                        <i class="fa fa-search"></i>
                                                                    </span>
                                                                    <input type="text" ng-model="alertSearchName" ng-keypress="changeSearchBox($event,alertSearchName)" class="form-control"
                                                                        placeholder="Search" aria-describedby="basic-addon1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table transaction-alerts-table" ng-if="listAlerts.length > 0">
                                                <!-- <table class="table transaction-alerts-table" datatable="ng" dt-options="dtOptions"> -->
                                                <tbody>
                                                    <tr ng-repeat="listData in listAlerts track by $index">
                                                        <td>
                                                            <p>
                                                                {{listData.name}}
                                                            </p>
                                                            <span>
                                                                {{listData.scenario}}
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span>{{listData.date}}</span>
                                                            <!--                                                             <span>Score : 45</span> -->
                                                        </td>
                                                        <td>
                                                            <strong>{{listData.amount}}</strong>
                                                        </td>
                                                        <td class="text-center">
                                                            <button class="btn btn-analyze" ng-click="loadDashboard(listData)">
                                                                Analyze</button>
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>
                                            <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(listAlerts)">
                                            	<span >Data Not Found</span>
                                            </div>
                                            <div class="text-right">
                                                <ul class="transaction-alert-pagination" uib-pagination total-items="alertsLength" items-per-page="20" ng-model="pageNum"
                                                    ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-5 pad-l0">

                                        <!--  Chart Panel Wrapper Starts  -->
                                        <div class="chart-panel-wrapper">

                                            <div class="row custom-row">

                                                <!--  First Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alerts By Customer</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="counterparties2">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="progress_bar1 list-unstyled progressbar-list" ng-if="aml_customers_progressData.length > 0">
                                                                    <li class="progressbar-list-item" ng-repeat="pData in aml_customers_progressData">
                                                                        <div class="left-col">{{pData.key}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                                aria-valuemax="100" style="width:{{pData.value}}%">
                                                                                <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{pData.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if= "checkToShowDtaNtFnd(aml_customers_progressData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                             <span ng-if="dashboardTabloader.dashboradTopCustomersloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   												             </span>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transfers By Branch</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="counterparties4">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="progress_bar2 list-unstyled progressbar-list" ng-if="aml_branches_progressData.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in aml_branches_progressData">
                                                                        <div class="left-col">{{pData.key}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                                aria-valuemax="100" style="width:{{pData.value}}%">
                                                                                <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{pData.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(aml_branches_progressData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                        </div>
                                                        <span ng-if="dashboardTabloader.dashboardTransfersByBranchloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   												        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alerts By Period</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="alertsByPeriodPieChart"></div>

                                                              <span ng-if="dashboardTabloader.dashboatdPeriodPieChartLoader" class="custom-spinner case-dairy-spinner">
                                                                  <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                             </span>
                                                            

                                                            <ul class="list-inline custom-list circle charts-aml-legends">
                                                                <li ng-repeat="legData in alertsByPeriodPieChartLegendData">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{legData.color}} !important"></i>{{legData.name}} </a>
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->

                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transfers By Geography</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="transfersByGeographyChart"></div>

                                                        </div>
                                                        <span ng-if="transfersByGeographyChartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>


                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->

                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alerts By Scenario</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="counterparties5">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="counterparties5_new list-unstyled progressbar-list" ng-if="aml_scenarios_progressData.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in aml_scenarios_progressData">
                                                                        <div class="left-col">{{pData.key}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                                aria-valuemax="100" style="width:{{pData.value}}%">
                                                                                <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{pData.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(aml_scenarios_progressData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                           <span ng-if="dashboardTabloader.dashboardAlertsByScenarioloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                          </span>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transfers By Month</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="transfersByMonthChart"></div>

                                                        </div>
                                                        <span ng-if="loader.transfersByMonthChartPreloader" class="custom-spinner case-dairy-spinner">
                                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alerts By Status</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="alertsByStatuschart"></div>
                                                              <span ng-if="dashboardTabloader.Statuschartloader" class="custom-spinner case-dairy-spinner">
                                                                 <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                              </span>
                                                                 <ul class="list-inline custom-list circle charts-aml-legends">
                                                                <li ng-repeat="legData in alertsByStatuschartLegendData">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{legData.color}} !important"></i>{{legData.name}} </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->

                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-6 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Alerts By Status > 30</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="alertsByStatus30Chart"></div>

                                                              <span ng-if="dashboardTabloader.Status30Chartloader" class="custom-spinner case-dairy-spinner">
                                                                  <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                              </span>

                                                            <ul class="list-inline custom-list circle charts-aml-legends">
                                                                <li ng-repeat="legData in alertsByStatus30ChartLegendData">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{legData.color}} !important"></i>{{legData.name}} </a>
                                                                </li>
                                                            </ul>


                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->
                                            </div>

                                        </div>
                                        <!--  Chart Panel Wrapper Ends  -->

                                    </div>
                                </div>
                            </div>
                            <!--  Notifications Tab Ends  -->
                        </uib-tab>


                        <uib-tab index="2" heading="Transactions" id="transaction_tab" ng-click="current_tab='transaction';loadChart_transaction('transaction');loadChart_transactionNew('transaction')">
                            <!--  Transactions Tab Starts  -->
                            <div class="active transactions-wrapper" id="transactions">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="list-inline">
                                                        <li class="">
                                                            <h4 class="top-filter-heading">Total Transactions</h4>
                                                            <strong class="value">{{notificationtxsAmount || 0}}</strong>
                                                        </li>
                                                        <li class="mar-l15">
                                                            <ul class="list-inline">
                                                                <li class="">
                                                                    <h4 class="top-filter-heading">Alerts Generated</h4>
                                                                    <strong class="value text-center">{{notificationStatusCountTotal || 0}}
                                                                        <span class="divide-value text-center">/ {{pieValue || 0}} %</span>
                                                                    </strong>
                                                                </li>
                                                                <li class="">
                                                                    <div id="transalertsChart"></div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <span ng-if="transactionTabloader.transalertsChartloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">input/output ratio</h4>
                                                    <div id="inputOutputRatio"></div>
                                                    <span ng-if="transactionTabloader.inputOutputRatioloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												    </span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Alert Ratio</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar5 list-unstyled progressbar-list" ng-if="trans_alert_ratio_data.length > 0">
                                                        <li class="progressbar-list-item" ng-repeat="pData in trans_alert_ratio_data">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress progress-blue">
                                                                <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(trans_alert_ratio_data)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                                <span ng-if="transactionTabloader.alertRatioloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top 3 Transactions</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar6 list-unstyled progressbar-list" ng-if="trans_top_three_transaction_type_progressData.length > 0">
                                                        <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in trans_top_three_transaction_type_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>


                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(trans_top_three_transaction_type_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                                <span ng-if="transactionTabloader.top3Transactionloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Transactions Types</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progress_bar6 list-unstyled progressbar-list" ng-if="transaction_transaction_type_progressData.length > 0">
                                                        <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in transaction_transaction_type_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-blue" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(transaction_transaction_type_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->

                                                </div>
                                                 <span ng-if="transactionTabloader.topTransactionTypesloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <!--  Chart Panel Wrapper Starts  -->
                                <div class="chart-panel-wrapper four-grid">

                                    <div class="row custom-row">

                                        <!--  Left Col Starts  -->
                                        <div class="col-sm-10 custom-col left-col">
                                            <div class="row custom-row">
                                                <!--  First Column Starts  -->
                                                <div class="col-sm-2 custom-col bg-dark-grey left-side-alert">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel border-light-black-b">
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{notificationtxsAmount | currency:'$':0}}">{{notificationtxsAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{notificationStatusCountTotal}} / {{pieValue}}%">
                                                                    <span class="text-light-red1">{{notificationStatusCountTotal || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{pieValue || 0}}%</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-red1" title="{{ txsCount }}">{{ txsCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('allTxs', txsCount, notificationtxsAmountToShow, notificationStatusCountTotal, $event, 'Total Amount')">View All</a>
                                                                </p>
                                                                <h5 class="text-light-grey1">View By</h5>
                                                                <ul class="list-inline custom-list circle">
                                                                    <li ng-class="{'active':allOption}" ng-click="viewByAllTxs('All')">
                                                                        <a href="javascript:void(0);">
                                                                            <i class="fa fa-circle icon"></i>All</a>
                                                                    </li>
                                                                    <li ng-class="{'active':inputOption}" ng-click="viewByAllTxs('Input')">
                                                                        <a href="javascript:void(0);">
                                                                            <i class="fa fa-circle icon"></i>Input</a>
                                                                    </li>
                                                                    <li ng-class="{'active':outputOption}" ng-click="viewByAllTxs('Output')">
                                                                        <a href="javascript:void(0);">
                                                                            <i class="fa fa-circle icon"></i>Output</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                     <div class="panel chart-panel">
                                                        <div class="panel-heading transactionTabLegends">
                                                            <h3>By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>
                                                                        Transactions 
                                                                    </a>
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>
                                                                        Alerts 
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="transLineChart"></div>
                                                        </div>
                                                        <span ng-if="transactionTabloader.transLineChartloader" class="custom-spinner case-dairy-spinner">
                                                           <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                   <div class="panel chart-panel">
                                                        <div class="panel-heading transactionTabLegends">
                                                            <h3>By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>
                                                                        Transactions 
                                                                    </a>
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>
                                                                        Alerts 
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="barchart1"></div>
                                                        </div>
                                                        <span ng-if="transactionTabloader.barchart1loader" class="custom-spinner case-dairy-spinner">
                                                             <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                        </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->
                                            </div>
                                            <div class="row custom-row" ng-repeat="txtype in transactionTypesDetails.txDetails">
                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-2 custom-col left-side-alert">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey2">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left" title="{{ txtype.transactionProductType}}">{{ txtype.transactionProductType || 0}}</h3>
                                                            <span class="count text-dark-grey pull-right" title="{{ txtype.alertRatio }}%">{{ txtype.alertRatio || 0}}%</span>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">By Amount/Alerts</h5>
                                                                <p class="mar-b10">
                                                                    <span class="text-light-blue" title="$ {{ txtype.totalTransactionAmount | number : 1 }}">$ {{ (txtype.totalTransactionAmount | number : 1) || 0 }}</span>
                                                                </p>
                                                                <p class="mar-b10">
                                                                    <span class="text-light-red1" title="{{ txtype.alertCount }}">{{ txtype.alertCount || 0 }}</span>
                                                                    <span class="text-dark-grey" title="({{ txtype.alertRatio | number : 1 }}%)">({{ (txtype.alertRatio | number : 1) || 0 }}%)</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">By Numbers</h5>
                                                                <p>
                                                                    <span class="text-dark-grey" title="{{ txtype.transactionsCount }}">{{ txtype.transactionsCount || 0 }}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans(txtype.transactionProductType, txtype.transactionsCount, txtype.totalTransactionAmount, txtype.alertCount, $event, txtype.transactionProductType)">View All</a>
                                                                </p>
                                                                <h5 class="text-light-grey1">View By</h5>
                                                                <ul class="list-inline custom-list circle">
                                                                    <li class="" ng-class="{'active': (txtype.checkedOption == undefined || txtype.checkedOption == 'ALL')}">
                                                                        <a href="javascript:void(0);" ng-click="checkActive($event, txtype.transactionProductType);">
                                                                            <i class="fa fa-circle icon"></i>All</a>
                                                                    </li>
                                                                    <li class="" ng-class="{'active': txtype.checkedOption == 'INPUT'}">
                                                                        <a href="javascript:void(0);" ng-click="checkActive($event, txtype.transactionProductType);">
                                                                            <i class="fa fa-circle icon"></i>Input</a>
                                                                    </li>
                                                                    <li class="" ng-class="{'active': txtype.checkedOption == 'OUTPUT'}">
                                                                        <a href="javascript:void(0);" ng-click="checkActive($event, txtype.transactionProductType);">
                                                                            <i class="fa fa-circle icon"></i>Output</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-body panel-scroll">
                                                            <div class="transactionTypeCharts" id="amountChart_{{ txtype.transactionProductType.split(' ').join('--') }}"></div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel panel-scroll">
                                                        <div class="panel-body">
                                                            <div class="transactionTypeCharts" id="countChart_{{ txtype.transactionProductType.split(' ').join('--') }}"></div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->
                                            </div>
                                            <!--                                                 <div class="row custom-row"> -->
                                            <!--                                                      Ninth Column Starts  -->
                                            <!--                                                     <div class="col-sm-2 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel bg-dark-grey2"> -->
                                            <!--                                                             <div class="panel-heading clearfix"> -->
                                            <!--                                                                 <h3 class="pull-left">Wire</h3> -->
                                            <!--                                                                 <span class="count text-dark-grey pull-right">12%</span> -->
                                            <!--                                                             </div> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div class="transaction-tile-wrapper"> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Amount/Alerts</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-light-blue">$ 567, 347,98</span> -->
                                            <!--                                                                         <span class="text-light-red1">329</span> -->
                                            <!--                                                                         <span class="text-dark-grey">(03%)</span> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Numbers</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-dark-grey">4557</span> -->
                                            <!--                                                                         <a href="javascript:void(0);" class="btn btn-blue-link">View All</a> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">View By</h5> -->
                                            <!--                                                                     <ul class="list-inline custom-list circle"> -->
                                            <!--                                                                         <li class="active"> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>All</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Input</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Output</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                     </ul> -->
                                            <!--                                                                 </div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Ninth Column Ends  -->

                                            <!--                                                      Tenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="transactionChart"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Tenth Column Ends  -->

                                            <!--                                                      Eleventh Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="barchart3"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Eleventh Column Ends  -->
                                            <!--                                                 </div> -->
                                            <!--                                                 <div class="row custom-row"> -->
                                            <!--                                                      Thirteenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-2 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel bg-dark-grey2"> -->
                                            <!--                                                             <div class="panel-heading clearfix"> -->
                                            <!--                                                                 <h3 class="pull-left">Cash</h3> -->
                                            <!--                                                                 <span class="count text-dark-grey pull-right">09%</span> -->
                                            <!--                                                             </div> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div class="transaction-tile-wrapper"> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Amount/Alerts</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-light-blue">$ 567, 347,98</span> -->
                                            <!--                                                                         <span class="text-light-red1">329</span> -->
                                            <!--                                                                         <span class="text-dark-grey">(03%)</span> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Numbers</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-dark-grey">4557</span> -->
                                            <!--                                                                         <a href="javascript:void(0);" class="btn btn-blue-link">View All</a> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">View By</h5> -->
                                            <!--                                                                     <ul class="list-inline custom-list circle"> -->
                                            <!--                                                                         <li class="active"> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>All</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Input</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Output</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                     </ul> -->
                                            <!--                                                                 </div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Thirteenth Column Ends  -->

                                            <!--                                                      Fourteenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="transactionChart"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Fourteenth Column Ends  -->

                                            <!--                                                      Fifteenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="barchart4"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Fifteenth Column Ends  -->
                                            <!--                                                 </div> -->
                                            <!--                                                 <div class="row custom-row"> -->
                                            <!--                                                      Seventeenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-2 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel bg-dark-grey2"> -->
                                            <!--                                                             <div class="panel-heading clearfix"> -->
                                            <!--                                                                 <h3 class="pull-left">Cheque</h3> -->
                                            <!--                                                                 <span class="count text-dark-grey pull-right">32%</span> -->
                                            <!--                                                             </div> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div class="transaction-tile-wrapper"> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Amount/Alerts</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-light-blue">$ 567, 347,98</span> -->
                                            <!--                                                                         <span class="text-light-red1">329</span> -->
                                            <!--                                                                         <span class="text-dark-grey">(03%)</span> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">By Numbers</h5> -->
                                            <!--                                                                     <p> -->
                                            <!--                                                                         <span class="text-dark-grey">4557</span> -->
                                            <!--                                                                         <a href="javascript:void(0);" class="btn btn-blue-link">View All</a> -->
                                            <!--                                                                     </p> -->
                                            <!--                                                                     <h5 class="text-light-grey1">View By</h5> -->
                                            <!--                                                                     <ul class="list-inline custom-list circle"> -->
                                            <!--                                                                         <li class="active"> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>All</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Input</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                         <li> -->
                                            <!--                                                                             <a href="javascript:void(0);"> -->
                                            <!--                                                                                 <i class="fa fa-circle icon"></i>Output</a> -->
                                            <!--                                                                         </li> -->
                                            <!--                                                                     </ul> -->
                                            <!--                                                                 </div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Seventeenth Column Ends  -->

                                            <!--                                                      Eighteenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="transactionChart"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Eighteenth Column Ends  -->

                                            <!--                                                      Nineteenth Column Starts  -->
                                            <!--                                                     <div class="col-sm-5 custom-col"> -->

                                            <!--                                                         Chart Panel Starts -->
                                            <!--                                                         <div class="panel chart-panel"> -->
                                            <!--                                                             <div class="panel-body"> -->
                                            <!--                                                                 <div id="barchart4"></div> -->
                                            <!--                                                             </div> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         Chart Panel Ends -->

                                            <!--                                                     </div> -->
                                            <!--                                                      Nineteenth Column Ends  -->
                                            <!--                                                 </div> -->
                                        </div>
                                        <!--  Left Col Ends  -->

                                        <!--  Right Col Starts  -->
                                        <div class="col-sm-2 custom-col right-col">
                                            <div class="row custom-row">
                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-12 custom-col">
                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transaction Types</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="Transaction_pieChart"></div>
                                                               <span ng-if="transactionTabloader.transaction_pieChartloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   															   </span>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Sixteenth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Input Output Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <!--  Progressbar List Starts -->
                                                            <ul class="progressbar7 list-unstyled progressbar-list" ng-if="inputOutputProgress.txDetails.length > 0">
                                                                <li class="progressbar-list-item" ng-repeat="item in inputOutputProgress.txDetails">
                                                                    <div class="left-col">{{item.transactionProductType}}</div>
                                                                    <div class="progress progress-light-blue">
                                                                        <div class="progress-bar progress-blue" role="progressbar" aria-valuenow="calinputvalue"
                                                                            aria-valuemin="0" aria-valuemax="100" style="width:{{item.calinputvalue}}%">
                                                                            <span class="sr-only ng-binding">{{item.calinputvalue}} Complete</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="right-col">{{item.inputCount}}/{{(item.outputCount)}}</div>

                                                                </li>

                                                            </ul>
                                                            <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(inputOutputProgress.txDetails)">
                                                            	<span>Data Not Found</span>
                                                            </div>
                                                            <ul class="list-inline custom-list circle">
                                                                <li class="active">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>Input</a>
                                                                </li>
                                                                <li class="">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon"></i>Output</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixteenth Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Monthly Turnover of Entities</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="monthlyTurnoverPieChart"></div>
                                                               <span ng-if="transactionTabloader.monthlyTurnoverPieChartloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   															   </span>
                                                            <ul class="list-inline custom-list circle">
                                                                <li ng-repeat="(k,v) in monthlyTurnoverPieChartcolorObj" ng-if=" k !=='moveArray'">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{v}} !important"></i>{{k}} </a>
                                                                </li>
                                                            </ul>
                                                            
                                                        </div>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Twelfth  Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Transaction Comparission</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">

                                                            <ul class="list-inline custom-list circle chart-legends chart-legends-transact">
                                                                <li ng-repeat="legData in transactionComparisonChartLegendData">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{legData.color}} !important"></i>{{legData.name}} </a>
                                                                </li>
                                                            </ul>
                                                            <div id="transactionComparisonChart"></div>


                                                        </div>
                                                        <span ng-if="transactionTabloader.transactionComparisonChartloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Twelfth Column Ends  -->
                                            </div>
                                        </div>
                                        <!--  Right Col Ends  -->
                                    </div>

                                </div>
                                <!--  Chart Panel Wrapper Ends  -->

                            </div>
                            <!--  Transactions Tab Ends  -->
                        </uib-tab>

                        <uib-tab index="3" heading="Risk" id="risk_tab" ng-click="current_tab='risk';loadChart_risk('risk')">
                            <!--  Risk Tab Starts  -->
                            <div class="risk-wrapper" id="risk">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="">
                                                        <li class="">
                                                            <h4 class="top-filter-heading">Total Transactions</h4>
                                                            <strong class="value">{{notificationtxsAmount || 0}}</strong>
                                                        </li>
                                                        <li class="mar-l15 clearfix">
                                                            <h4 class="top-filter-heading">Alerts Generated</h4>
                                                            <strong class="text-light-red1 pull-left mar-r5">{{notificationStatusCountTotal || 0}}
                                                                <span class="divide-value text-center">/ {{pieValue || 0}} %</span>
                                                            </strong>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Risk Types</h4>
                                                    <div id="riskTypeChart"></div>
                                                    <span ng-if="riskTabloader.risk_pieChartloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   													</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Product Risk</h4>
                                                    <div id="productRiskChart"></div>
                                                    <span ng-if="riskTabloader.risk_productpieChartloader" class="custom-spinner case-dairy-spinner">
       														   <i class="fa fa-spinner fa-spin fa-2x"></i>
  													</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top MF/FT Risk</h4>
                                                    <div id="riskMfFtChart"></div>
                                                    <span ng-if="riskTabloader.risk_geopieChartloader" class="custom-spinner case-dairy-spinner">
       														   <i class="fa fa-spinner fa-spin fa-2x"></i>
  													</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top 3 Transactions</h4>

                                                    <!--  Progressbar List Starts -->
                                                    <ul class="progressbar11 list-unstyled progressbar-list" ng-if="notification_top_transaction_type_progressData.length > 0">
                                                        <li class="progressbar-list-item by-amount-bar" ng-repeat="pData in notification_top_transaction_type_progressData">
                                                            <div class="left-col">{{pData.key}}</div>
                                                            <div class="progress">
                                                                <div  class="progress-bar progress-pink" role="progressbar" aria-valuenow="{{pData.value}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{pData.value}}%">
                                                                    <span class="sr-only ng-binding">{{pData.value}} Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="right-col">{{pData.amount}}</div>
                                                        </li>

                                                    </ul>
                                                    <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(notification_top_transaction_type_progressData)">
                                                    	<span>Data Not Found</span>
                                                    </div>
                                                    <!--  Progressbar List Ends -->
                                                </div>
                                                <span ng-if="riskTabloader.top3Transactionloader" class="custom-spinner case-dairy-spinner">
       														   <i class="fa fa-spinner fa-spin fa-2x"></i>
  													</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <!--  Chart Panel Wrapper Starts  -->
                                <div class="chart-panel-wrapper four-grid">

                                    <div class="row custom-row">

                                        <!--  Risk Left Col Starts  -->
                                        <div class="col-sm-10 custom-col left-col">
                                            <div class="row custom-row">

                                                <!--  First Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Customer Risk</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{CustomerRisk.totalAmount}}">{{CustomerRisk.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{CustomerRisk.alertGenerated}} / {{CustomerRisk.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{CustomerRisk.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{CustomerRisk.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{CustomerRisk.totalAmountCount}}">{{CustomerRisk.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('CustomerRisk', CustomerRisk.totalAmountCount, CustomerRisk.totalAmount, CustomerRisk.alertGenerated, $event, 'Customer Risk')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBloomBergChartOne"></div>
                                                              
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBloomBergChartOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBarChartOne"></div>
                                                               
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBarChartOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Product Risk</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{ProductRisk.totalAmount}}">{{ProductRisk.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{ProductRisk.alertGenerated}} / {{ProductRisk.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{ProductRisk.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{ProductRisk.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{ProductRisk.totalAmountCount}}">{{ProductRisk.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('ProductRisk', ProductRisk.totalAmountCount, ProductRisk.totalAmount, ProductRisk.alertGenerated, $event, 'Product Risk')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBloomBergChartTwo"></div>
                                                              
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBloomBergChartTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBarChartTwo"></div>
                                                                  
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBarChartTwoloader" class="custom-spinner case-dairy-spinner">
        														      <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Ninth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Geography</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{GeoGraphicRisk.totalAmount}}">{{GeoGraphicRisk.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{GeoGraphicRisk.alertGenerated}} / {{GeoGraphicRisk.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{GeoGraphicRisk.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{GeoGraphicRisk.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{GeoGraphicRisk.totalAmountCount}}">{{GeoGraphicRisk.totalAmountCount}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('GeoGraphicRisk', GeoGraphicRisk.totalAmountCount, GeoGraphicRisk.totalAmount, GeoGraphicRisk.alertGenerated, $event, 'Geography')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Ninth Column Ends  -->

                                                <!--  Tenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBloomBergChartThree"></div>
                                                                 
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBloomBergChartThreeloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Tenth Column Ends  -->

                                                <!--  Eleventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBarChartThree"></div>
                                                                
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBarChartThreeloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eleventh Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Thirteenth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey2">
                                                        <div class="panel-heading">
                                                            <h3>Corruption High Risk Countries</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{CorruptionRisk.totalAmount}}">{{CorruptionRisk.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{CorruptionRisk.alertGenerated}} / {{CorruptionRisk.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{CorruptionRisk.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{CorruptionRisk.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{CorruptionRisk.totalAmountCount}}">{{CorruptionRisk.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('CorruptionRisk', CorruptionRisk.totalAmountCount, CorruptionRisk.totalAmount, CorruptionRisk.alertGenerated, $event, 'Corruption High Risk Countries')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Thirteenth Column Ends  -->

                                                <!--  Fourteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!--  Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount - Corruption High Risk Countries</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBloomBergChartFour"></div>
                                                               
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBloomBergChartFourloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourteenth Column Ends  -->

                                                <!--  Fifteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers - Corruption High Risk Countries</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBarChartFour"></div>
                                                                
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBarChartFourloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifteenth Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Seventeenth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey2">
                                                        <div class="panel-heading">
                                                            <h3>Political & Legal High Risk Countries</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{PoliticalRisk.totalAmount}}">{{PoliticalRisk.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{PoliticalRisk.alertGenerated}} / {{PoliticalRisk.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{PoliticalRisk.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{PoliticalRisk.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{PoliticalRisk.totalAmountCount}}">{{PoliticalRisk.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('PoliticalRisk', PoliticalRisk.totalAmountCount, PoliticalRisk.totalAmount, PoliticalRisk.alertGenerated, $event, 'Political & Legal High Risk Countries')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventeenth Column Ends  -->

                                                <!--  Eighteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount - Political & Legal High Risk Countries</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBloomBergChartFive"></div>
                                                                
                                                        </div>
                                                         <span ng-if="riskTabloader.riskBloomBergChartFiveloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighteenth Column Ends  -->

                                                <!--  Nineteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers - Political & Legal High Risk Countries</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskBarChartFive"></div>
                                                               
                                                        </div>
                                                        <span ng-if="riskTabloader.riskBarChartFiveloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   													   </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Nineteenth Column Ends  -->
                                            </div>
                                        </div>
                                        <!--  Risk Left Col Ends  -->

                                        <!--  Risk Right Col Starts  -->
                                        <div class="col-sm-2 custom-col right-col">
                                            <div class="row custom-row">

                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Risk Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="riskPieChartOne"></div>
                                                               
                                                            <ul class="list-inline custom-list circle">
                                                                <li ng-repeat="(k,v) in colorObj" ng-if=" k !=='moveArray'">
                                                                    <a href="javascript:void(0);">
                                                                        <i class="fa fa-circle icon" style="color: {{v}} !important"></i>{{k}} </a>
                                                                </li>
                                                            </ul>
                                                              
                                                        </div>
                                                        <span ng-if="riskTabloader.riskPieChartOneloader" class="custom-spinner case-dairy-spinner">
        														  <i class="fa fa-spinner fa-spin fa-2x"></i>
   													   </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->

                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Customer Risk</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="transactionChart">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="progressbar8 list-unstyled progressbar-list" ng-if="CustomerRiskBarData.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="barDta in CustomerRiskBarData">
                                                                        <div class="left-col">{{barDta.type}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-trendy-pink" role="progressbar" aria-valuenow="{{barDta.amountValue}}%"
                                                                                aria-valuemin="0" aria-valuemax="100" style="width:{{barDta.amountValue}}%">
                                                                                <span class="sr-only ng-binding">{{barDta.amountValue}}% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{barDta.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(CustomerRiskBarData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                                
                                                        </div>
                                                        <span ng-if="riskTabloader.customerTransactionChartloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->

                                                <!--  Twelfth Column Starts  -->
                                                <div class="col-sm-12 custom-col risk-custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Product Risk</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="transactionChart_1">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="transactionChart_1_new list-unstyled progressbar-list" ng-if="ProductRiskBarData.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="barDta in ProductRiskBarData">
                                                                        <div class="left-col">{{barDta.type}}</div>
                                                                        <div class="progress progress-blue">
                                                                            <div  class="progress-bar progress-light-red" role="progressbar" aria-valuenow="{{barDta.amountValue}}%"
                                                                                aria-valuemin="0" aria-valuemax="100" style="width:{{barDta.amountValue}}%">
                                                                                <span class="sr-only ng-binding">{{barDta.amountValue}}% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{barDta.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(ProductRiskBarData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                           
                                                        </div>
                                                         <span ng-if="riskTabloader.productTransactionChartloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->
                                                </div>
                                                <div class="col-sm-12 custom-col risk-custom-col">
                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Geography Risk</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="transactionChart2">
                                                                <!--  Progressbar List Starts -->
                                                                <ul class="transactionChart2_new list-unstyled progressbar-list" ng-if="GeoGraphicRiskBarData.length > 0">
                                                                    <li class="progressbar-list-item by-amount-bar" ng-repeat="barDta in GeoGraphicRiskBarData">
                                                                        <div class="left-col">{{barDta.type}}</div>
                                                                        <div class="progress">
                                                                            <div  class="progress-bar progress-trendy-pink" role="progressbar" aria-valuenow="{{barDta.amountValue}}%"
                                                                                aria-valuemin="0" aria-valuemax="100" style="width:{{barDta.amountValue}}%">
                                                                                <span class="sr-only ng-binding">{{barDta.amountValue}}% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="right-col">{{barDta.amount}}</div>
                                                                    </li>
                                                                </ul>
                                                                <div class="alertsDashErrorDiv" ng-if="checkToShowDtaNtFnd(GeoGraphicRiskBarData)">
                                                                	<span>Data Not Found</span>
                                                                </div>
                                                                <!--  Progressbar List Ends -->
                                                            </div>
                                                           
                                                        </div>
                                                         <span ng-if="riskTabloader.geoTransactionChartloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Twelfth Column Ends  -->

                                                <!--  Sixteenth  Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Corruption High Risk Countries</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterparty_CorruptionRisk"></div>
                                                                
                                                        </div>

                                                         <span ng-if="riskTabloader.counterparty_CorruptionRiskloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixteenth Column Ends  -->

                                                <!--  Twenty Column Starts  -->
                                                <div class="col-sm-12 custom-col border-grey-thin-l border-grey-thin-b">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Political High Risk Countries</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterparty_PoliticalRisk"></div>
                                                            
                                                        </div>

                                                         <span ng-if="riskTabloader.counterparty_PoliticalRiskloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   													    </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Twenty Column Ends  -->

                                            </div>
                                        </div>
                                        <!--  Risk Right Col Ends  -->

                                    </div>

                                </div>
                                <!--  Chart Panel Wrapper Ends  -->

                            </div>
                            <!--  Risk Tab Ends  -->
                        </uib-tab>


                        <uib-tab index="4" heading="Corporate Structure" id="corporate_structure_tab" ng-click="loadChart_corporate('corporate')">

                            <!--  Corporate Structure Tab Starts  -->
                            <div class="corporate-structure-wrapper" id="corporateStructure">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="list-inline">
                                                        <li class="">
                                                            <h4 class="top-filter-heading">Total Transactions</h4>
                                                            <strong class="value">{{notificationtxsAmount || 0}}</strong>
                                                        </li>
                                                        <li class="mar-l15">
                                                            <div class="clearfix">
                                                                <h4 class="top-filter-heading">Alerts Generated</h4>
                                                                <strong class="value text-light-red1 pull-left mar-r5">{{notificationStatusCountTotal || 0}}
                                                                    <span class="divide-value text-center">/ {{pieValue || 0}} %</span>
                                                                </strong>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Structure</h4>
                                                    <div id="corpoStructurePieChartOne"></div>
                                                </div>
                                                <span ng-if="corporateStructureTabloader.corpoStructurePieChartOneloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Alerted Geography</h4>
                                                    <div id="corpoStructurePieChartTwo"></div>
                                                </div>
                                                <span ng-if="corporateStructureTabloader.corpoStructurePieChartTwoloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Shareholders</h4>
                                                    <div id="corpoStructurePieChartThree"></div>
                                                </div>
                                                <span ng-if="corporateStructureTabloader.corpoStructurePieChartThreeloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Alert Trend</h4>
                                                    <div id="corpoStructureLineChartOne"></div>
                                                </div>
                                                <span ng-if="corporateStructureTabloader.alertTrendloader" class="custom-spinner case-dairy-spinner">
        														   <i class="fa fa-spinner fa-spin fa-2x"></i>
  												</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <!--  Chart Panel Wrapper Starts  -->
                                <div class="chart-panel-wrapper four-grid">

                                    <div class="row custom-row">

                                        <!--  Corporate Structure Left Col Starts  -->
                                        <div class="col-sm-10 custom-col left-col">
                                            <div class="row custom-row">
                                                <!--  First Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Structure</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{structure.totalAmount}}">{{structure.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{structure.alertGenerated}} / {{structure.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{structure.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{structure.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{structure.totalAmountCount}}">{{structure.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('CorporateStructure', structure.totalAmountCount, structure.totalAmount ,structure.alertGenerated, $event, 'corporate structure')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureBloombergChartOne"></div>
                                                                 
                                                        </div>
                                                         <span ng-if="corporateStructureTabloader.corpoStructureBloombergChartOneloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Alerts</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureBarChartOne"></div>
                                                                 
                                                        </div>
                                                         <span ng-if="corporateStructureTabloader.corpoStructureBarChartOneloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   													     </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Shareholders</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{shareholders.totalAmount}}">{{shareholders.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{shareholders.alertGenerated}} / {{shareholders.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{shareholders.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{shareholders.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{shareholders.totalAmountCount}}">{{shareholders.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('Shareholders', shareholders.totalAmountCount, shareholders.totalAmount ,shareholders.alertGenerated, $event, 'corporate structure')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureBloombergChartTwo"></div>
                                                                
                                                        </div>
                                                        <span ng-if="corporateStructureTabloader.corpoStructureBloombergChartTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
	   													</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureBarChartTwo"></div>
                                                        </div>
                                                        <span ng-if="corporateStructureTabloader.corpoStructureBarChartTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
	   													</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Ninth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Geography</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{corporateGeography.totalAmount}}">{{corporateGeography.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{corporateGeography.alertGenerated}} / {{corporateGeography.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{corporateGeography.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{corporateGeography.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{corporateGeography.totalAmountCount}}">{{corporateGeography.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('Geography', corporateGeography.totalAmountCount, corporateGeography.totalAmount ,corporateGeography.alertGenerated, $event, 'Geography')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Ninth Column Ends  -->

                                                <!--  Tenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureBarChartTree"></div>
                                                               
                                                        </div>
                                                         <span ng-if="corporateStructureTabloader.corpoStructureBarChartTreeloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Tenth Column Ends  -->

                                                <!--  Eleventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="corpoStructureWorldMapOne"></div>
                                                        </div>

                                                        <span ng-if="corporateStructureTabloader.corpoStructureWorldMapOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eleventh Column Ends  -->
                                            </div>
                                        </div>
                                        <!--  Corporate Structure Left Col Ends  -->

                                        <!--  Corporate Structure Right Col Starts  -->
                                        <div class="col-sm-2 custom-col right-col">
                                            <div class="row custom-row">
                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Structure Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="corpoStructurePieChartFour"></div>
                                                               
                                                        </div>
                                                         <span ng-if="corporateStructureTabloader.corpoStructurePieChartFourloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->

                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Share Holder Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="corpoStructurePieChartFive"></div>
                                                             
                                                        </div>
                                                         <span ng-if="corporateStructureTabloader.corpoStructurePieChartFiveloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->
                                                <!--  Twelfth  Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Geography Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="corpoStructurePieChartSix"></div>
                                                        </div>
                                                        <span ng-if="corporateStructureTabloader.corpoStructurePieChartSixloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Twelfth Column Ends  -->
                                            </div>
                                        </div>
                                        <!--  Corporate Structure Right Col Ends  -->
                                    </div>
                                </div>
                                <!--  Chart Panel Wrapper Ends  -->

                            </div>
                            <!--  Corporate Structure Tab Ends  -->

                        </uib-tab>

                        <uib-tab index="5" heading="Counter Parties"  id="counter_Parties_tab" ng-click="loadChart_counterparties('counterparties')">

                            <!--  Counter Parties Tab Starts  -->
                            <div class="counter-parties-wrapper" id="counterParties">

                                <!--  Top Filters Wrapper Starts  -->
                                <div class="main-filters-wrapper">
                                    <div class="top-filters-wrapper">
                                        <ul class="top-filter-navigation list-inline">
                                            <li>
                                                <div class="top-filters alert-filters">
                                                    <ul class="list-inline">
                                                        <li class="">
                                                            <h4 class="top-filter-heading">Total Amount</h4>
                                                            <strong class="value">{{notificationtxsAmount || 0}}</strong>
                                                        </li>
                                                        <li class="mar-l15 clearfix">
                                                            <h4 class="top-filter-heading">Alerts Generated</h4>
                                                            <strong class="value text-light-red1 pull-left mar-r5">{{notificationStatusCountTotal || 0}}
                                                                <span class="divide-value text-center">/ {{pieValue || 0}} %</span>
                                                            </strong>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="top-filters distorted-activity-filters">
                                                    <h4 class="top-filter-heading">Top Activity</h4>
                                                    <div id="counterPartiestopActivity"></div>
                                                </div>
                                                <span ng-if="counterPartiesStructureTabloader.counterPartiestopActivityloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Top Alerted Geography</h4>

                                                    <!--  Top Alerted Geo Chart Starts -->
                                                    <div id="counterPartiestopGeography"></div>
                                                    <!--  Top Alerted Geo Chart Ends -->

                                                </div>
                                                <span ng-if="counterPartiesStructureTabloader.counterPartiestopGeographyloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters distorted-activity-filters">
                                                    <h4 class="top-filter-heading">Top Alerted Banks</h4>

                                                    <!--  Top Alerted Bank Chart Starts -->
                                                    <div id="counterPartiestopBanks"></div>
                                                    <!--  Top Alerted Bank Chart Ends -->

                                                </div>
                                                <span ng-if="counterPartiesStructureTabloader.counterPartiestopBanksloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   												</span>
                                            </li>
                                            <li>
                                                <div class="top-filters">
                                                    <h4 class="top-filter-heading">Alert Trend</h4>

                                                    <!--  Alert Trend Chart Starts -->
                                                    <div id="counterPartiesLineChartOne"></div>
                                                    <!--  Alert Trend Chart Ends -->

                                                </div>
                                                <span ng-if="counterPartiesStructureTabloader.counterPartiesLineChartOneloader" class="custom-spinner case-dairy-spinner">
        														    <i class="fa fa-spinner fa-spin fa-2x"></i>
   											   </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Top Filters Wrapper Starts  -->

                                <!--  Chart Panel Wrapper Starts  -->
                                <div class="chart-panel-wrapper four-grid">

                                    <div class="row custom-row">

                                        <!--  Counter Parties Left Col Starts  -->
                                        <div class="col-sm-10 custom-col left-col">
                                            <div class="row custom-row">
                                                <!--  First Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Activity Types</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{counterPartyActivityTypesDetail.totalAmount }}">{{counterPartyActivityTypesDetail.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{counterPartyActivityTypesDetail.alertGenerated}} / {{counterPartyActivityTypesDetail.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{counterPartyActivityTypesDetail.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{counterPartyActivityTypesDetail.alertGeneratedPerValue || 0}}
                                                                        %
                                                                    </span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{counterPartyActivityTypesDetail.totalAmountCount}}">{{counterPartyActivityTypesDetail.totalAmountCount}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('ActivityType', counterPartyActivityTypesDetail.totalAmountCount, counterPartyActivityTypesDetail.totalAmount ,counterPartyActivityTypesDetail.alertGenerated, $event, 'Activity Types')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  First Column Ends  -->

                                                <!--  Second Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBloombergChartOne"></div>
                                                             
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesBloombergChartOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Second Column Ends  -->

                                                <!--  Third Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Alerts</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBarChartOne"></div>
                                                            
                                                             
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesBarChartOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Third Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Fifth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Geography</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{counterPartyGeoGraphicDetail.totalAmount}}">{{counterPartyGeoGraphicDetail.totalAmount || 0 }}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{counterPartyGeoGraphicDetail.alertGenerated}} / {{counterPartyGeoGraphicDetail.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{counterPartyGeoGraphicDetail.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{counterPartyGeoGraphicDetail.alertGeneratedPerValue || 0}}
                                                                        %
                                                                    </span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{counterPartyGeoGraphicDetail.totalAmountCount}}">{{counterPartyGeoGraphicDetail.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('GeoGraphic', counterPartyGeoGraphicDetail.totalAmountCount, counterPartyGeoGraphicDetail.totalAmount ,counterPartyGeoGraphicDetail.alertGenerated, $event, 'Geography')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifth Column Ends  -->

                                                <!--  Sixth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBarChartTwo"></div>
                                                             
                                                        </div>
                                                        <span ng-if="counterPartiesStructureTabloader.counterPartiesBarChartTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixth Column Ends  -->

                                                <!--  Seventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesWorldMapOne"></div>
                                                             
                                                        </div>

                                                        <span ng-if="counterPartiesStructureTabloader.counterPartiesWorldMapOneloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   													    </span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Seventh Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Ninth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Banks</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{bankDetail.totalAmount}}">{{bankDetail.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{bankDetail.alertGenerated}} / {{bankDetail.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{bankDetail.alertGenerated || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{bankDetail.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{bankDetail.totalAmountCount}}">{{bankDetail.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('Banks', bankDetail.totalAmountCount, bankDetail.totalAmount ,bankDetail.alertGenerated, $event, 'Banks')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Ninth Column Ends  -->

                                                <!--  Tenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBloombergChartTwo"></div>
                                                            
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesBloombergChartTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Tenth Column Ends  -->

                                                <!--  Eleventh Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBarChartThree"></div>
                                                             
                                                        </div>
                                                        <span ng-if="counterPartiesStructureTabloader.counterPartiesBarChartThreeloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eleventh Column Ends  -->
                                            </div>
                                            <div class="row custom-row">
                                                <!--  Thirteenth Column Starts  -->
                                                <div class="col-sm-2 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel bg-dark-grey border-light-black-b">
                                                        <div class="panel-heading">
                                                            <h3>Bank Locations</h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="transaction-tile-wrapper">
                                                                <h5 class="text-light-grey1">Total Amount</h5>
                                                                <p>
                                                                    <span class="text-light-blue" title="{{bankLocatinonDetail.totalAmount}}">{{bankLocatinonDetail.totalAmount || 0}}</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Alerts Generated</h5>
                                                                <p title="{{bankLocatinonDetail.alertGenerated}} / {{bankLocatinonDetail.alertGeneratedPerValue}} %">
                                                                    <span class="text-light-red1">{{bankLocatinonDetail.alertGenerated  || 0}}</span>
                                                                    <span class="text-dark-grey">/ {{bankLocatinonDetail.alertGeneratedPerValue || 0}} %</span>
                                                                </p>
                                                                <h5 class="text-light-grey1">Total Transactions</h5>
                                                                <p>
                                                                    <span class="text-light-grey1" title="{{bankLocatinonDetail.totalAmountCount}}">{{bankLocatinonDetail.totalAmountCount || 0}}</span>
                                                                    <a href="javascript:void(0);" class="btn btn-blue-link" ng-click="viewAllTrans('BankLocations',bankLocatinonDetail.totalAmountCount,bankLocatinonDetail.totalAmount ,bankLocatinonDetail.alertGenerated, $event, 'Bank Locations')">View All</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Thirteenth Column Ends  -->

                                                <!--  Fourteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Amount</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesBloombergChartThree"></div>
                                                            
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesBloombergChartThreeloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourteenth Column Ends  -->

                                                <!--  Fifteenth Column Starts  -->
                                                <div class="col-sm-5 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="pull-left">By Numbers</h3>
                                                            <ul class="list-inline pull-right custom-list grey">
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <!-- <i class="fa fa-arrows rotate-45"></i> -->
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="panel-body panel-scroll">
                                                            <div id="counterPartiesWorldMapTwo"></div>
                                                             
                                                        </div>

                                                        <span ng-if="counterPartiesStructureTabloader.counterPartiesWorldMapTwoloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>

                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fifteenth Column Ends  -->
                                            </div>
                                        </div>
                                        <!--  Counter Parties Left Col Ends  -->

                                        <!--  Counter Parties Right Col Starts  -->
                                        <div class="col-sm-2 custom-col right-col">
                                            <div class="row custom-row">

                                                <!--  Fourth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Activity Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="counterPartyAlertActvityRatio"></div>
                                                             
                                                        </div>

                                                         <span ng-if="counterPartiesStructureTabloader.counterPartyAlertActvityRatioloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>  

                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Fourth Column Ends  -->

                                                <!--  Eighth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Geography Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="counterPartiesGeographyRatio"></div>
                                                            
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesGeographyRatioloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Eighth Column Ends  -->

                                                <!--  Twelfth  Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Bank Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="counterPartiesBanksRatio"></div>
                                                                
                                                        </div>
                                                        <span ng-if="counterPartiesStructureTabloader.counterPartiesBanksRatioloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														</span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Twelfth Column Ends  -->

                                                <!--  Sixteenth Column Starts  -->
                                                <div class="col-sm-12 custom-col">

                                                    <!-- Chart Panel Starts -->
                                                    <div class="panel chart-panel">
                                                        <div class="panel-heading">
                                                            <h3>Bank Location Alert Ratio</h3>
                                                        </div>
                                                        <div class="panel-body panel-scroll panel-body-equal">
                                                            <div id="counterPartiesBanksLocationsRatio"></div>
                                                               
                                                        </div>
                                                         <span ng-if="counterPartiesStructureTabloader.counterPartiesBanksLocationsRatioloader" class="custom-spinner case-dairy-spinner">
        														 <i class="fa fa-spinner fa-spin fa-2x"></i>
   														 </span>
                                                    </div>
                                                    <!-- Chart Panel Ends -->

                                                </div>
                                                <!--  Sixteenth Column Ends  -->

                                            </div>
                                        </div>
                                        <!--  Counter Parties Right Col Ends  -->

                                    </div>
                                </div>
                                <!--  Chart Panel Wrapper Ends  -->

                            </div>
                            <!--  Counter Parties Tab Ends  -->

                        </uib-tab>

                    </uib-tabset>
                    <!--  Nav Tabs Ends  -->

                </div>
                <!--  Custom Tabs Wrapper Ends  -->

            </div>
            <!--  Alert Dashboard Wrapper Ends  -->

            <wheel-nav-context-menu contextmenuname="alertDashboard.contentMenu" contextmenuname0="alertDashboard.contentMenu0"
                contextmenuname1="alertDashboard.contentMenu1" contextmenuname2="alertDashboard.contentMenu2" contextmenuname3="alertDashboard.contentMenu3"
                contextmenuname4="alertDashboard.contentMenu4" contextmenuname5="alertDashboard.contentMenu5"></wheel-nav-context-menu>

        </div>
        <!--  Main Contents Wrapper Ends  -->

        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".top-filters-wrapper").mThumbnailScroller({
                        axis: "x"
                    });
                    $(".alert-notification-wrapper .chart-panel-wrapper .panel-body, .main-counter-parties-scroll").mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                    $(".chart-panel-wrapper .panel-scroll").mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                    $(
                        '#datePicker,#transactionDatePicker,#riskDatePicker,#corporateDatePicker,#counterDatePicker'
                    ).datepicker();
                }, 500);
            });
        </script>