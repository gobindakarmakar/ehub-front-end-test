<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>   
<!--  SubMenu Ends  -->

<span class="custom-spinner " ng-show="transactionAnalysisLandingLoader">
 	<i class="fa fa-spinner fa-spin fa-2x"></I>
</span>

<!--  Transaction Landing Wrapper Starts  -->
<div class="transaction-landing-wrapper">

	<!--  Search Panel Starts -->
	<div class="transaction-search-wrapper">
		<div class="">
			<form class="form-inline clearfix text-uppercase">
				<div class="col-md-7 col-sm-6">
					<div class="trans-search-person">
					   <label for="searchPerson"><i class="fa fa-search"></i> Search for a transaction of</label>
					   <input placeholder="CLIENT NAME / PP NUMBER" type="text" class="form-control text-center" ng-model="searchPerson"  id="searchPerson">
					</div>
				</div>
				<div class="col-md-5 col-sm-6">
					<div class="trans-search-period clearfix">
					   <label for="searchPerson">On period</label>
					   <div class="calendarView row pull-left">
						   <div class="col-sm-6">
						   		<div class="date-wrapper">
						   			<input placeholder="From" type="text" class="form-control text-center" id="searchFrom" ng-model="searchFrom">
						   			<i class="fa fa-chevron-down"></i>
					   			</div>
						   </div>
						   <div class="col-sm-6">
						   		<div class="date-wrapper">
						   			<input placeholder="TO" type="text" class="form-control text-center" id="searchTO" ng-model="searchTO">
						   			<i class="fa fa-chevron-down"></i>
						   		</div>
						   </div>
					   </div>
						<button type="button" class="btn pull-right" ng-click="getAllAlerts('search')">Search</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--  Search Panel Ends  -->
	
	<!-- Main Content Panel Starts  -->
	<div class="transaction-main-section container-fluid">
		<div class="row">
			<div class="col-sm-9">
		         <div class="transaction-table-analysis custom-tab">
	         		<a href="" class="btn text-uppercase text-center ti-create-alert" ng-click="createAlertModal();">Generate/Upload an Alert</a>
	             	<uib-tabset>
	             		 <uib-tab index="0">
						     <uib-tab-heading class="text-uppercase text-center">
						      	<i class="fa fa-bell"></i> Your Alerts
						     </uib-tab-heading>
						     <div class="transaction-alerts-wrapper">
						     	<table class="table transaction-alerts-table" datatable="ng" dt-options="dtOptions">
						     		<thead>
							     		<tr>
							     			<th>Main Entity</th>
							     			<th>Scenario</th>
							     			<th>Date(age)</th>
							     			<th>Jurisdiction</th>
							     			<th class="text-center">Action</th>
						     			</tr>
						     		</thead>
						     		<tbody>
<!-- 						     			<tr ng-click="loadEntityDetails('',$event,'dummy')" > -->
<!-- 						     				<td>Petro Saudi</td> -->
<!-- 						     				<td>Large Amount international transfer to Offshore jurisdiction</td> -->
<!-- 						     				<td>2010/01/05</td> -->
<!-- 						     				<td>Offshore</td>						     				 -->
<!-- 						     				<td class="text-center" ><button class="btn btn-primary" ng-click="loadDashboard(alert,'dummy')"> Analyze</button></td> -->
<!-- 						     			</tr> -->
						     			<tr ng-repeat="alert in alerts" ng-click="loadEntityDetails(alert,$event)" ng-class="{'transactionTableActive': alert == alerts[0]}">
						     				<td>{{ alert['search-name'] }}</td>
						     				<td>{{ alert.scenario }}</td>
						     				<td>{{ alert['date'] | date:"yyyy/MM/dd "}} ({{ alert.age }})</td>
						     				<td>{{ alert.jurisdiction }}</td>
						     				
						     				<td class="text-center" ><button class="btn btn-primary" ng-click="loadDashboard(alert)"> Analyze</button></td>
						     			</tr>
						     		</tbody>
						     	</table>
						     	<div class="text-right" ng-hide="alerts.length == 0 || alertsLength <= 4">
								<ul class="transaction-alert-pagination" uib-pagination total-items="alertsLength" items-per-page="25" ng-model="pageNum" ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
							</div>
						     </div>
				   		 </uib-tab>
<!-- 					   	 <uib-tab index="1"> -->
<!-- 						    <uib-tab-heading class="text-uppercase text-center"> -->
<!-- 						      Recent Searches -->
<!-- 						    </uib-tab-heading> -->
<!-- 					    	<div class="container-fluid"> -->
<!-- 					    		<ul class="row list-inline"> -->
<!-- 					    			<li class="col-sm-3 text-uppercase" ng-repeat="search in searchList" ng-click="openSelectedSearch(search)"> -->
<!-- 		     							<a class="searched-one-click" href="javascript:void(0);">{{search.name}}</a> -->
<!-- 					    			</li> -->
<!-- 					    		</ul> -->
<!-- 				    		</div> -->
<!-- 						  </uib-tab> -->
					 </uib-tabset>
		         </div>
			</div>
			
			
			<!--  Right Content Panel Starts  -->
			<div class="col-sm-3">
				<div class="transaction-alert-summary">
					<h4 class="text-uppercase">Alert summary</h4>
					<div class="taLanding-graph-wrapper">
						<h5>Transaction Types</h5>
						<div class="result-graph-wrapper-main-taLanding">
							<div class="" id="transactionTypes">
							</div>
						</div>
					</div>
					<div class="taLanding-graph-wrapper">
						<h5>Transaction amounts</h5>
						<div class="result-graph-wrapper-main-taLanding">
							<div class="" id="taAmountRatio">
							</div>
						</div>
					</div>
					<div class="taLanding-graph-wrapper">
						<h5>Locations</h5>
						<div class="result-graph-wrapper-main-taLanding">
							<div class="datamapsMip" id="taLocations">
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  Right Content Panel Ends  -->
			
		</div>
	</div>
	<!--  Main Content Panel Ends  -->

</div>
<script>
$(document).ready(function(){
	$('.result-graph-wrapper-main-taLanding').mCustomScrollbar({
		axis : "y",
		theme : "minimal"
	});
	//initialize date pickers
	$('#searchFrom').datepicker();
	$('#searchTO').datepicker();
	
})
</script>
