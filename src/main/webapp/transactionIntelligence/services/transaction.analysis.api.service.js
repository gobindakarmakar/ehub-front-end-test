'use strict';
elementApp
		.factory('TransactionAnalysisApiService', transactionAnalysisApiService);

		transactionAnalysisApiService.$inject = [
			'$http',
			'$rootScope',
			'$q',
			'Upload',
			'EHUB_API'
		];
		
		function transactionAnalysisApiService(
				$http,
				$rootScope,
				$q,
				Upload,
				EHUB_API		
			){
			
			return {
				getScenariosByType:getScenariosByType,
				getAlertsByScenario:getAlertsByScenario,
				getScenarioCharts:getScenarioCharts,	
				getCustomerAlerts:getCustomerAlerts,
				activityCounterParty:activityCounterParty,
				activitycounterPartyAggregates:activitycounterPartyAggregates,
				counterPartygetGeoGraphic:counterPartygetGeoGraphic,
				counterPartygetGeoGraphicAggregates:counterPartygetGeoGraphicAggregates,
				counterPartygetBanks:counterPartygetBanks,
				counterPartygetBankAggregates:counterPartygetBankAggregates,
				amlAlertNotifications:amlAlertNotifications,
		    	amlAlertAggregation:amlAlertAggregation,
		    	amlTopScenarios:amlTopScenarios,
		    	amlAlertByStatus:amlAlertByStatus,
		    	amlAlertByPeriod:amlAlertByPeriod,
		    	amlTopGeographyAndTransfers:amlTopGeographyAndTransfers,
		    	amlTopCustomers:amlTopCustomers,
		    	amlTransferByMonth:amlTransferByMonth,
				getTransactionAndAlertAggregates:getTransactionAndAlertAggregates,	
				getProductRisk:getProductRisk,
				getProductRiskAggregates:getProductRiskAggregates,
				getMonthlyTurnOver:getMonthlyTurnOver,
				getcounterPartyLocationsNotify:getcounterPartyLocationsNotify,
				getAlertsList: getAlertsList,
				getAlertSummaryById: getAlertSummaryById,
				getAlertDetailsById:getAlertDetailsById,
				showRemoveVertex: showRemoveVertex,
				getTransactionsListForSelectedEntites: getTransactionsListForSelectedEntites,
				getTransactionInputsAggregates: getTransactionInputsAggregates,
				getTransactionOutputsAggregates: getTransactionOutputsAggregates,
				resolveAlert:resolveAlert,
				getEntitySearch: getEntitySearch,				
				searchResolve: searchResolve,
				searchLogo:searchLogo,
				getRisk:getRisk,
				loadMarketIntelligenceENT:loadMarketIntelligenceENT,
				getEntityDetails: getEntityDetails,				
				uploadFile:uploadFile,
				getAlertNotification:getAlertNotification,
				getFetchTxsCountAndRatio:getFetchTxsCountAndRatio,
				getFetchTxsCountAndRatioByInput: getFetchTxsCountAndRatioByInput,
				getAlertlist:getAlertlist,
				getAlertlistWithTimeStamp:getAlertlistWithTimeStamp,
				getTransactionAndAlertByProductType:getTransactionAndAlertByProductType,
				getTotalAmountAndAlertCountByProductType:getTotalAmountAndAlertCountByProductType,
				riskRatioChart:riskRatioChart,
				getCustomerRiskCounts:getCustomerRiskCounts,
				getCustomerRiskAggregates:getCustomerRiskAggregates,
				getGeoGraphicCountRatio:getGeoGraphicCountRatio,
				getGeoGraphicAggregates:getGeoGraphicAggregates,				
				getCorruptionRisk:getCorruptionRisk,
				getCorruptionRiskAggregates:getCorruptionRiskAggregates,
				getPoliticalRisk:getPoliticalRisk,
				getPoliticalRiskAggregates:getPoliticalRiskAggregates,
				getAssociatedEntities:getAssociatedEntities,
				alertComparisonNotification:alertComparisonNotification,
				alertComparisonTransactions:alertComparisonTransactions,
				getViewAllForTxsByType: getViewAllForTxsByType,
				getViewAllForCorpCounterRisk: getViewAllForCorpCounterRisk,
				getTopFiveAlerts:getTopFiveAlerts,
				getCorporateStructure:getCorporateStructure,
				getCorporateStructureAggregates:getCorporateStructureAggregates,				
				getShareholderCorporateStructure:getShareholderCorporateStructure,
		        getShareholderAggregates:getShareholderAggregates,
		        getAlertEntitiesGeography:getAlertEntitiesGeography,
		        getGeographyAggregates:getGeographyAggregates,
		        getAssociatedAlerts:getAssociatedAlerts,
		        getCounterPartiesTransactions: getCounterPartiesTransactions,
		        getRiskCounterParties: getRiskCounterParties,
		        getCustomerInfo:getCustomerInfo
			};		
			/*
		     * @purpose: Get scenarios  by type
		     * @created: 10th feb 2018
		     * @params: date
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getScenariosByType(params,type,filtersData){
				var url =EHUB_API+"risk/getScenariosByType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+"&type="+type;
               		 var request = $http({
               			method: "POST",
						data:filtersData?filtersData:{},                 
					    url: url              
				        });
			        return(request
			                .then(getScenariosByTypeSuccess)
			                .catch(getScenariosByTypeError));

			        /* getScenariosByType error function */
			        function getScenariosByTypeError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /* Otherwise, use expected error message. */
			            return($q.reject(response.data.message));
			        }

			        /* getScenariosByType success function */
			        function getScenariosByTypeSuccess(response) {
			            return(response);
			        }
			}
			/*
		     * @purpose:  getAlertsByScenario
		     * @created: 12th feb 2018
		     * @params: date
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getAlertsByScenario(params,type,filtersData){
				var url =EHUB_API+"risk/getAlertsByScenario/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+"&type="+type;
               		 var request = $http({
							method: 'POST', 
							headers: {
					            	"Content-Type": "application/json" 
					            		},
					    	url: url ,
					    	data:filtersData
				        });
			        return(request
			                .then(getAlertsByScenarioSuccess)
			                .catch(getAlertsByScenarioError));

			        /* getAlertsByScenario error function */
			        function getAlertsByScenarioError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /* Otherwise, use expected error message. */
			            return($q.reject(response.data.message));
			        }

			        /* getAlertsByScenario success function */
			        function getAlertsByScenarioSuccess(response) {
			            return(response);
			        }
			}
			/*
		     * @purpose: getScenarioCharts
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getScenarioCharts(params,filtersData){/*jshint unused:false*/
				var apiUrl =  EHUB_API + "dashboard/getAlertScenarioById?"+params;
		        var request = $http({
		            method: "GET",
		           
		            url: apiUrl
		        });
		        return(request
		                .then(getScenarioChartsSuccess)
		                .catch(getScenarioChartsError));
		
		        /*getScenarioCharts error function*/
		        function getScenarioChartsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getScenarioCharts success function*/
		        function getScenarioChartsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: getCustomerAlerts
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getCustomerAlerts(params){
				var apiUrl =  EHUB_API + "dashboard/fetchAlertsByNumber?"+params;
		        var request = $http({
		            method: "GET",		           
		            url: apiUrl
		        });
		        return(request
		                .then(getCustomerAlertsSuccess)
		                .catch(getCustomerAlertsError));
		
		        /*getCustomerAlerts error function*/
		        function getCustomerAlertsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getCustomerAlerts success function*/
		        function getCustomerAlertsSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: activityCounterParty
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function activityCounterParty(params,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getActivity/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	 method: "POST",
			         data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(activityCounterPartySuccess)
		                .catch(activityCounterPartyError));
		
		        /*activityCounterParty error function*/
		        function activityCounterPartyError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*activityCounterParty success function*/
		        function activityCounterPartySuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: counterPartyAggregates
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function activitycounterPartyAggregates(params,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getActivityAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
			        data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(activitycounterPartyAggregatesSuccess)
		                .catch(activitycounterPartyAggregatesError));
		
		        /*activitycounterPartyAggregates error function*/
		        function activitycounterPartyAggregatesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*activitycounterPartyAggregates success function*/
		        function activitycounterPartyAggregatesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: counterPartygetGeoGraphic
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function counterPartygetGeoGraphic(params,flag,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getGeoGraphic/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token+"&isBankCountry="+(flag?1:0);
		        var request = $http({
		        	method: "POST",
			        data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(counterPartygetGeoGraphicSuccess)
		                .catch(counterPartygetGeoGraphicError));
		
		        /*counterPartygetGeoGraphic error function*/
		        function counterPartygetGeoGraphicError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*counterPartygetGeoGraphic success function*/
		        function counterPartygetGeoGraphicSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: counterPartygetGeoGraphicAggregates
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function counterPartygetGeoGraphicAggregates(params,flag,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getGeoGraphicAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token+"&isBankCountry="+(flag?1:0);
		        var request = $http({
		        	method: "POST",
			        data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(counterPartygetGeoGraphicAggregatesSuccess)
		                .catch(counterPartygetGeoGraphicAggregatesError));
		
		        /*counterPartygetGeoGraphicAggregates error function*/
		        function counterPartygetGeoGraphicAggregatesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*counterPartygetGeoGraphicAggregates success function*/
		        function counterPartygetGeoGraphicAggregatesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: counterPartygetBanks
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function counterPartygetBanks(params,flag,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getBanks/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
			        data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(counterPartygetBanksSuccess)
		                .catch(counterPartygetBanksError));
		
		        /*counterPartygetBanks error function*/
		        function counterPartygetBanksError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*counterPartygetBanks success function*/
		        function counterPartygetBanksSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: counterPartygetBankAggregates
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function counterPartygetBankAggregates(params,flag,filtersData){
				var apiUrl =  EHUB_API + "counterParty/getBankAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
			        data:filtersData?filtersData:{},
		            url: apiUrl
		        });
		        return(request
		                .then(counterPartygetBankAggregatesSuccess)
		                .catch(counterPartygetBankAggregatesError));
		
		        /*counterPartygetBankAggregates error function*/
		        function counterPartygetBankAggregatesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*counterPartygetBankAggregates success function*/
		        function counterPartygetBankAggregatesSuccess(response) {
		            return(response);
		        }
			}
			
			
			
			
			
			/*
		     * @purpose: amlAlertNotifications
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlAlertNotifications(params,granularity,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlAlertNotifications"+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlAlertNotificationsSuccess)
		                .catch(amlAlertNotificationsError));
		
		        /*amlAlertNotifications error function*/
		        function amlAlertNotificationsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlAlertNotifications success function*/
		        function amlAlertNotificationsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlAlertAggregation
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlAlertAggregation(params,granularity,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlAlertAggregation"+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlAlertAggregationsSuccess)
		                .catch(amlAlertAggregationError));
		
		        /*amlAlertAggregation error function*/
		        function amlAlertAggregationError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlAlertAggregation success function*/
		        function amlAlertAggregationsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlTopScenarios
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlTopScenarios(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlTopScenarios/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlTopScenariosSuccess)
		                .catch(amlTopScenariosError));
		
		        /*amlTopScenarios error function*/
		        function amlTopScenariosError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlTopScenarios success function*/
		        function amlTopScenariosSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlAlertByStatus
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlAlertByStatus(params,periodBy,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlAlertByStatus/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
				 if(periodBy){
			    	   apiUrl =apiUrl+"&periodBy=true";
			       }
				var request = $http({
					method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlAlertByStatusSuccess)
		                .catch(amlAlertByStatusError));
		
		        /*amlAlertByStatus error function*/
		        function amlAlertByStatusError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlAlertByStatus success function*/
		        function amlAlertByStatusSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlAlertByPeriod
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlAlertByPeriod(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlAlertByPeriod/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		       
				var request = $http({
					method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlAlertByPeriodSuccess)
		                .catch(amlAlertByPeriodError));
		
		        /*amlAlertByPeriod error function*/
		        function amlAlertByPeriodError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlAlertByPeriod success function*/
		        function amlAlertByPeriodSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlTopGeographyAndTransfers
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlTopGeographyAndTransfers(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlTopGeographyAndTransfers/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlTopGeographyAndTransfersSuccess)
		                .catch(amlTopGeographyAndTransfersError));
		
		        /*amlTopGeographyAndTransfers error function*/
		        function amlTopGeographyAndTransfersError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlTopGeographyAndTransfers success function*/
		        function amlTopGeographyAndTransfersSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlTopCustomers
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlTopCustomers(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlTopCustomers/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlTopCustomersSuccess)
		                .catch(amlTopCustomersError));
		
		        /*amlTopCustomers error function*/
		        function amlTopCustomersError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlTopCustomers success function*/
		        function amlTopCustomersSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: amlTransferByMonth
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function amlTransferByMonth(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "aml/amlTransferByMonth"+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(amlTransferByMonthSuccess)
		                .catch(amlTransferByMonthError));
		
		        /*amlTransferByMonth error function*/
		        function amlTransferByMonthError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*amlTransferByMonth success function*/
		        function amlTransferByMonthSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: getTransactionAndAlertAggregates
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getTransactionAndAlertAggregates(params,granularity, checkedTypeDetails,filtersData){
				if(checkedTypeDetails && checkedTypeDetails.inputOption == true)
					{var apiUrl =  EHUB_API + "dashboard/getTransactionAndAlertAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token+"&granularity="+granularity + '&input=true';}
				else if(checkedTypeDetails && checkedTypeDetails.outputOption == true)
					{var apiUrl =  EHUB_API + "dashboard/getTransactionAndAlertAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token+"&granularity="+granularity + '&output=true';}
				else
					{var apiUrl =  EHUB_API + "dashboard/getTransactionAndAlertAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token+"&granularity="+granularity;}
				
				var request = $http({
					method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getTransactionAndAlertAggregatesSuccess)
		                .catch(getTransactionAndAlertAggregatesError));
		
		        /*getTransactionAndAlertAggregates error function*/
		        function getTransactionAndAlertAggregatesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getTransactionAndAlertAggregates success function*/
		        function getTransactionAndAlertAggregatesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: getProductRisk
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getProductRisk(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "risk/getProductRisk/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getProductRiskSuccess)
		                .catch(getProductRiskError));
		
		        /*getProductRisk error function*/
		        function getProductRiskError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getProductRisk success function*/
		        function getProductRiskSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: getProductRisk
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getProductRiskAggregates(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "risk/getProductRiskAggregates/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getProductRiskAggregatesSuccess)
		                .catch(getProductRiskAggregatesError));
		
		        /*getProductRiskAggregates error function*/
		        function getProductRiskAggregatesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getProductRiskAggregates success function*/
		        function getProductRiskAggregatesSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: getMonthlyTurnOver
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getMonthlyTurnOver(params,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "dashboard/getMonthlyTurnOver/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		        	method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getMonthlyTurnOverSuccess)
		                .catch(getMonthlyTurnOverError));
		
		        /*getMonthlyTurnOver error function*/
		        function getMonthlyTurnOverError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getMonthlyTurnOver success function*/
		        function getMonthlyTurnOverSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: getcounterPartyLocationsNotify
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getcounterPartyLocationsNotify(params,type,filtersData){
//				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
				var apiUrl =  EHUB_API + "/dashboard/counterPartyLocationsPlot/"+params[0]+"/"+params[1]+"?token=" + $rootScope.ehubObject.token;
		       if(type){
		    	   apiUrl =apiUrl+"&type="+type;
		       }
				var request = $http({
					method: "POST",
		        	data:filtersData?filtersData:{},
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getcounterPartyLocationsNotifySuccess)
		                .catch(getcounterPartyLocationsNotifyError));
		
		        /*getcounterPartyLocationsNotify error function*/
		        function getcounterPartyLocationsNotifyError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getcounterPartyLocationsNotify success function*/
		        function getcounterPartyLocationsNotifySuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get alerts list
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAlertsList(params){
				var apiUrl = EHUB_API +'tx/fetchNonResolvedAlerts?'+params;
//				var apiUrl =  EHUB_API + 'transactionAnalysis/alert?'+params;
		        var request = $http({
		            method: "GET",
		            url: apiUrl
//		            params: params
		        });
		        return(request
		                .then(getAlertsListSuccess)
		                .catch(getAlertsListError));
		
		        /*getAlertsList error function*/
		        function getAlertsListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getAlertsList success function*/
		        function getAlertsListSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get alerts list by id
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAlertSummaryById(params){
				var apiUrl = EHUB_API +'tx/alertStatistics/'+params.id;
//				var apiUrl =  EHUB_API + 'transactionAnalysis/alert/'+params.id;
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: {token:params.token}
		        });
		        return(request
		                .then(getAlertSummaryByIdSuccess)
		                .catch(getAlertSummaryByIdError));
		
		        /*getAlertSummaryById error function*/
		        function getAlertSummaryByIdError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getAlertSummaryById success function*/
		        function getAlertSummaryByIdSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get alerts list by id
		     * @created: 31 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAlertDetailsById(params, token){
				var apiUrl = EHUB_API +'tx/fetchAlertSummary/'+params.queryString;
//				var apiUrl =  EHUB_API + 'transactionAnalysis/alert/'+params.id;
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: token
		        });
		        return(request
		                .then(getAlertDetailsByIdSuccess)
		                .catch(getAlertDetailsByIdError));
		
		        /*getAlertDetailsById error function*/
		        function getAlertDetailsByIdError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getAlertDetailsById success function*/
		        function getAlertDetailsByIdSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: remove vertex
		     * @created: 31 oct 2017
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function showRemoveVertex(param, type){
				var apiUrl =  EHUB_API + "transactionAnalysis/graph/edit/" + type+"?"+param;
		        var request = $http({
		            method: "POST",
		            headers: {   
		            	"Accept":"application/json"  ,
		           
		            	"Content-Type": "application/json" 
		            	
		            		},
		           
		            url: apiUrl
		        });
		        return(request
		                .then(removeVertexSuccess)
		                .catch(removeVertexError));
		
		        /*removeVertex error function*/
		        function removeVertexError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*removeVertex success function*/
		        function removeVertexSuccess(response) {
		            return(response);
		        }
			}
			
		        
		        
		        /*
			     * @purpose: get transactions list for selected entites
			     * @created: 31 oct 2017
			     * @params: params(object)
			     * @return: success, error functions
			     * @author: swathi
			    */
				function getTransactionsListForSelectedEntites(params, token){
					var apiUrl = EHUB_API +'tx/fetchTxsFrSelectedEntitys'+ "?" + params.filteredQueryString;
//					var apiUrl =  EHUB_API + 'transactionAnalysis/tx/'+ params.queryString + "?" + params.filteredQueryString;
			        var request = $http({
			            method: "POST",
			            url: apiUrl,
			            params: token,
			            data:params.queryString.split(",")
			        });
			        return(request
			                .then(getTransactionsListForSelectedEntitesSuccess)
			                .catch(getTransactionsListForSelectedEntitesError));
			
			        /*getTransactionsListForSelectedEntites error function*/
			        function getTransactionsListForSelectedEntitesError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			        /*getTransactionsListForSelectedEntites success function*/
			        function getTransactionsListForSelectedEntitesSuccess(response) {
			            return(response);
			        }
				}
			        /*
				     * @purpose: get transaction inputs aggregates
				     * @created: 31 oct 2017
				     * @params: params(object)
				     * @return: success, error functions
				     * @author: swathi
				    */
					function getTransactionInputsAggregates(params, token){
						var apiUrl = EHUB_API +'tx/input/stats?'+ params.filteredQueryString;
//						var apiUrl =  EHUB_API + 'transactionAnalysis/tx/' + params.queryString + "/inputs/stats?" + params.filteredQueryString;
				        var request = $http({
				        	method: "POST",
				            url: apiUrl,
				            data:params.queryString.split(","),
				            params: token
				        });
				        return(request
				                .then(getTransactionInputsAggregatesSuccess)
				                .catch(getTransactionInputsAggregatesError));
				
				        /*getTransactionInputsAggregates error function*/
				        function getTransactionInputsAggregatesError(response) {
				            if (!angular.isObject(response.data) || !response.data.message) {
				                return($q.reject(response.data));
				            }
				            /*Otherwise, use expected error message.*/
				            return($q.reject(response.data.message));
				        }
				
				        /*getTransactionInputsAggregates success function*/
				        function getTransactionInputsAggregatesSuccess(response) {
				            return(response);
				        }
					}
				        /*
					     * @purpose: get transaction outputs aggregates
					     * @created: 31 oct 2017
					     * @params: params(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getTransactionOutputsAggregates(params, token){
							var apiUrl = EHUB_API +'tx/output/stats?'+ params.filteredQueryString;
//							var apiUrl =  EHUB_API + 'transactionAnalysis/tx/' + params.queryString + "/outputs/stats?" + params.filteredQueryString;
					        var request = $http({
					            method: "POST",
					            url: apiUrl,
					            data:params.queryString.split(","),
					            params: token
					        });
					        return(request
					                .then(getTransactionOutputsAggregatesSuccess)
					                .catch(getTransactionOutputsAggregatesError));
					
					        /*getTransactionOutputsAggregates error function*/
					        function getTransactionOutputsAggregatesError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*getTransactionOutputsAggregates success function*/
					        function getTransactionOutputsAggregatesSuccess(response) {
					            return(response);
					        }
						}
						 /*
					     * @purpose: resolve Alert
					     * @created: 31 oct 2017
					     * @params: params(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function resolveAlert(params, token){
							var apiUrl = EHUB_API +'tx/resolveAlert';
							var request = $http({
					            method: "POST",
					            url: apiUrl,
					            data:params,
					            params: token
					        });
					        return(request
					                .then(resolveAlertSuccess)
					                .catch(resolveAlertError));
					
					        /*resolveAlert error function*/
					        function resolveAlertError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*resolveAlert success function*/
					        function resolveAlertSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: get entity search
					     * @created: 31 oct 2017
					     * @params: params(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getEntitySearch(params, type){
							
							var apiUrl =  EHUB_API + "entity/search/" + type+"?query="+params.query+"&token="+params.token;
					        var request = $http({
					            method: "GET",
					            url: apiUrl,
					        });
					        return(request
					                .then(getEntitySearchSuccess)
					                .catch(getEntitySearchError));
					
					        /*getEntitySearch error function*/
					        function getEntitySearchError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*getEntitySearch success function*/
					        function getEntitySearchSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: search Resolve
					     * @created: 01 nov 2017
					     * @params: token(object), data(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function searchResolve(data){
//							var apiUrl =  SearchAPIEndpoint + 'search/resolve';
							var apiUrl = EHUB_API+'search/getData?token='+$rootScope.ehubObject.token+'&searchFlag=resolve';
					        var request = $http({
					            method: "POST",
					            url: apiUrl,
					            data: data
					        });
					        return(request
					                .then(searchResolveSuccess)
					                .catch(searchResolveError));
					
					        /*searchResolve error function*/
					        function searchResolveError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*searchResolve success function*/
					        function searchResolveSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: search Logo
					     * @created: 01 nov 2017
					     * @params: token(object), data(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function searchLogo(data, token,isGraph){
//							var apiUrl =  SearchAPIEndpoint + 'search';
							var type="search";
							if(isGraph){
								type="graph";
							}
							var apiUrl = EHUB_API+'search/getData?token='+$rootScope.ehubObject.token+'&searchFlag='+type;
						      
					        var request = $http({
					            method: "POST",
					            url: apiUrl,
					            data: data
					        });
					        return(request
					                .then(searchResolveSuccess)
					                .catch(searchResolveError));
					
					        /*searchResolve error function*/
					        function searchResolveError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*searchResolve success function*/
					        function searchResolveSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: get Risk
					     * @created: 01 nov 2017
					     * @params: token(object), data(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getRisk(id){
							var apiUrl = EHUB_API+'search/graphData/'+id+'?token='+$rootScope.ehubObject.token;
					        var request = $http({
					            method: "GET",
					            url: apiUrl
					        });
					        return(request
					                .then(getRiskResolveSuccess)
					                .catch(getRiskResolveError));
					
					        /*getRiskResolve error function*/
					        function getRiskResolveError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*getRiskResolve success function*/
					        function getRiskResolveSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: load MarketIntelligenceENT
					     * @created: 11 oct 2017
					     * @params: apiUrl(string)
					     * @return: success, error functions
					     * @author: Ankit
					    */
						function loadMarketIntelligenceENT(txt){
							 var url =	EHUB_API+"entityVisualizer/ent?token=" + $rootScope.ehubObject.token;
							var request = $http({
								method: 'POST',                
						    	url: url,               
						    	data: JSON.stringify({text: txt})
					        });
					        return(request
					                .then(loadMarketIntelligenceENTSuccess)
					                .catch(loadMarketIntelligenceENTError));

					        /* getNotificationAlert error function */
					        function loadMarketIntelligenceENTError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getNotificationAlert success function */
					        function loadMarketIntelligenceENTSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: get entity details
					     * @created: 01 nov 2017
					     * @params: url(string)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getEntityDetails(url){
					        var request = $http({
					            method: "GET",
					            url: url
					        });
					        return(request
					                .then(getEntityDetailsSuccess)
					                .catch(getEntityDetailsError));
					
					        /*getEntityDetails error function*/
					        function getEntityDetailsError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*getEntityDetails success function*/
					        function getEntityDetailsSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: upload file
					     * @created: 21 oct 2017
					     * @params: params(object), data(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function uploadFile(data){
							var apiUrl =EHUB_API+"upload/uploadAllFiles?token=" + $rootScope.ehubObject.token;
					        var request = Upload.upload({
					            method: "POST",
					            url: apiUrl,
//					            params: params,
					            data: data
					        });
					        return(request
					                .then(uploadFileSuccess)
					                .catch(uploadFileError));
					
					        /*uploadFile error function*/
					        function uploadFileError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*uploadFile success function*/
					        function uploadFileSuccess(response) {
					            return(response);
					        }
						}
						
						/*
					     * @purpose: alert notification
					     * @created: 5th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAlertNotification(params,filtersData){
						//	 var url =	EHUB_API+"entityVisualizer/ent?token=" + $rootScope.ehubObject.token;
//							 var url='../vendor/data/alert1.json'
							var url =EHUB_API+"dashboard/fetchTxsBwDates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   	
								var request = $http({
									method: "POST",
									data:filtersData?filtersData:{},             
							    	url: url              
						        });
						        return(request
						                .then(alertNotificationSuccess)
						                .catch(alertNotificationError));

						        /* getNotificationAlert error function */
						        function alertNotificationError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getNotificationAlert success function */
						        function alertNotificationSuccess(response) {
						            return(response);
						        }
						}
						
						/*
					     * @purpose: alertComparisonNotification
					     * @created: 12th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function alertComparisonNotification(params,filtersData){
							var url =EHUB_API+"dashboard/alertComparisonNotification/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   	
								var request = $http({
									method: "POST",
									data:filtersData?filtersData:{},               
							    	url: url              
						        });
						        return(request
						                .then(alertComparisonNotificationSuccess)
						                .catch(alertComparisonNotificationError));

						        /* getNotificationAlert error function */
						        function alertComparisonNotificationError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getNotificationAlert success function */
						        function alertComparisonNotificationSuccess(response) {
						            return(response);
						        }
						}
                        
						/*
					     * @purpose: alertComparisonTransactions
					     * @created: 12th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function alertComparisonTransactions(params,filtersData){
							var url =EHUB_API+"dashboard/alertComparisonTransactions/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   	
								var request = $http({
									method: "POST",
									data:filtersData?filtersData:{},                  
							    	url: url              
						        });
						        return(request
						                .then(alertComparisonTransactionsSuccess)
						                .catch(alertComparisonTransactionsError));

						        /* getNotificationAlert error function */
						        function alertComparisonTransactionsError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getNotificationAlert success function */
						        function alertComparisonTransactionsSuccess(response) {
						            return(response);
						        }
						}
                        
						
                        
						/*
					     * @purpose: alert notification fetchTxsCountAndRatio 
					     * @created: 12th feb 2018
					     * @params: date, params
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getFetchTxsCountAndRatio(date, params,filtersData){
						//	 var url =	EHUB_API+"entityVisualizer/ent?token=" + $rootScope.ehubObject.token;
//							 var url='../vendor/data/alert1.json'
							if(date[2].inputOption == true)
								{var url = EHUB_API+"dashboard/fetchTxsCountAndRatio/"+date[0]+"/"+date[1] + '?input=true';}
							else if(date[2].outputOption == true)
								{var url = EHUB_API+"dashboard/fetchTxsCountAndRatio/"+date[0]+"/"+date[1] + '?output=true';}
							else
								{var url = EHUB_API+"dashboard/fetchTxsCountAndRatio/"+date[0]+"/"+date[1];}
		                   	
								var request = $http({
									method: "POST",
									data:filtersData?filtersData:{},               
							    	url: url, 
							    	params: params
						        });
						        return(request
						                .then(getFetchTxsCountAndRatioSuccess)
						                .catch(getFetchTxsCountAndRatioError));

						        /* getNotificationAlert error function */
						        function getFetchTxsCountAndRatioError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getNotificationAlert success function */
						        function getFetchTxsCountAndRatioSuccess(response) {
						            return(response);
						        }
						}
						
						/*
					     * @purpose: get Fetch Txs Count And Ratio By Input 
					     * @created: 15 feb 2018
					     * @params: date, params, isInput
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getFetchTxsCountAndRatioByInput(date, params, isInput,filtersData){
							if(isInput.input == true){
								var url = EHUB_API+"dashboard/fetchTxsCountAndRatio/"+date[0]+"/"+date[1] + '?input=true';
							}
							else if(isInput.input == false){
								var url = EHUB_API+"dashboard/fetchTxsCountAndRatio/"+date[0]+"/"+date[1] + '?output=true';
							}
							
							var request = $http({
								method: "POST",
								data:filtersData?filtersData:{},              
						    	url: url, 
						    	params: params
					        });
					        return(request
					                .then(getFetchTxsCountAndRatioByInputSuccess)
					                .catch(getFetchTxsCountAndRatioByInputError));

					        /* getFetchTxsCountAndRatioByInput error function */
					        function getFetchTxsCountAndRatioByInputError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getFetchTxsCountAndRatioByInput success function */
					        function getFetchTxsCountAndRatioByInputSuccess(response) {
					            return(response);
					        }
						}
                        
						/*
					     * @purpose: alertlist
					     * @created: 5th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAlertlist(params,filtersData){
							var url =EHUB_API+"dashboard/fetchAlertsBwDates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+"&pageNumber="+params[3]+"&recordsPerPage="+params[5]+"&orderIn="+params[2]+"&name="+params[4];
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                
								    	url: url              
							        });
						        return(request
						                .then(alertlistSuccess)
						                .catch(alertlistError));

						        /* getAlertlist error function */
						        function alertlistError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function alertlistSuccess(response) {
						            return(response);
						        }
						}
                         
						/*
					     * @purpose: alertlist with timestamp
					     * @created: 5th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAlertlistWithTimeStamp(params,filtersData){
							var url =EHUB_API+"dashboard/fetchAlertsBwDates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+"&pageNumber="+params[3]+"&recordsPerPage="+params[5]+"&orderIn="+params[2]+"&name="+params[4]+"&isTimeStamp=true";
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                
								    	url: url              
							        });
						        return(request
						                .then(getAlertlistWithTimeStampSuccess)
						                .catch(getAlertlistWithTimeStampError));

						        /* getAlertlistWithTimeStamp error function */
						        function getAlertlistWithTimeStampError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        function getAlertlistWithTimeStampSuccess(response) {
						            return(response);
						        }
						}

						
						/*
					     * @purpose: getTransactionAndAlertAggregatesByProductType
					     * @created: 5th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getTransactionAndAlertByProductType(params, productCheckedOption, productType, granularity,filtersData){
							if(productType && productCheckedOption && productCheckedOption == 'INPUT')
								{var url = EHUB_API + "dashboard/getTransactionAndAlertAggregatesByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token + '&input=true' + "&granularity="+granularity + "&type="+productType;}
							else if(productType && productCheckedOption && productCheckedOption == 'OUTPUT')
								{var url = EHUB_API + "dashboard/getTransactionAndAlertAggregatesByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token + '&output=true' + "&granularity="+granularity + "&type="+productType;}
							else
								{var url =EHUB_API+"dashboard/getTransactionAndAlertAggregatesByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token + "&granularity="+granularity;}
	                   		 
							var request = $http({
								method: "POST",
								data:filtersData?filtersData:{},               
						    	url: url              
	                   		 });
					        return(request
					                .then(getTransactionAndAlertByProductTypeSuccess)
					                .catch(getTransactionAndAlertByProductTypeError));

					        /* getAlertlist error function */
					        function getTransactionAndAlertByProductTypeError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertlist success function */
					        function getTransactionAndAlertByProductTypeSuccess(response) {
					            return(response);
					        }
						}
						
						/*
					     * @purpose: getTotalAmountAndAlertCountByProductType
					     * @created: 5th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getTotalAmountAndAlertCountByProductType(params, productCheckedOption, productType,granularity,filtersData){
							
							if(productType && productCheckedOption && productCheckedOption == 'INPUT')
								{var url = EHUB_API + "dashboard/getTotalAmountAndAlertCountByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token + '&input=true' + "&type="+productType+'&groupBy=false';}
							else if(productType && productCheckedOption && productCheckedOption == 'OUTPUT')
								{var url = EHUB_API + "dashboard/getTotalAmountAndAlertCountByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token + '&output=true' + "&type="+productType+'&groupBy=false';}
							else
								{var url = EHUB_API + "dashboard/getTotalAmountAndAlertCountByProductType/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+'&groupBy=true';}
							
						 	var request = $http({
						 		method: "POST",
								data:filtersData?filtersData:{},                
						    	url: url              
					        });
					        return(request
					                .then(getTotalAmountAndAlertCountByProductTypeSuccess)
					                .catch(getTotalAmountAndAlertCountByProductTypeError));

					        /* getAlertlist error function */
					        function getTotalAmountAndAlertCountByProductTypeError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertlist success function */
					        function getTotalAmountAndAlertCountByProductTypeSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: riskRatioChart 
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function riskRatioChart(params,filtersData){
							var url =EHUB_API+"risk/riskRatioChart/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},              
								    	url: url              
							        });
						        return(request
						                .then(riskRatioChartSuccess)
						                .catch(riskRatioChartError));

						        /* riskRatioChart error function */
						        function riskRatioChartError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* riskRatioChart success function */
						        function riskRatioChartSuccess(response) {
						            return(response);
						        }
						}
						
						/*
					     * @purpose: Get customer Risk Counts 
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCustomerRiskCounts(params,filtersData){
							var url =EHUB_API+"risk/getCustomerRisk/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                 
								    	url: url              
							        });
						        return(request
						                .then(getCustomerRiskCountsSuccess)
						                .catch(getCustomerRiskCountsError));

						        /* getAlertlist error function */
						        function getCustomerRiskCountsError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getCustomerRiskCountsSuccess(response) {
						            return(response);
						        }
						}
						
						/*
					     * @purpose: Get Customer Risk Aggregates
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCustomerRiskAggregates(params,filtersData){
							var url =EHUB_API+"risk/getCustomerRiskAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
		                   			method: "POST",
									data:filtersData?filtersData:{},                               
								    	url: url              
							        });
						        return(request
						                .then(getCustomerRiskAggregatesSuccess)
						                .catch(getCustomerRiskAggregatesError));

						        /* getAlertlist error function */
						        function getCustomerRiskAggregatesError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getCustomerRiskAggregatesSuccess(response) {
						            return(response);
						        }
						}
						
						
						/*
					     * @purpose: Get GeoGraphic Count Ratio
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getGeoGraphicCountRatio(params,filtersData){
							var url =EHUB_API+"risk/getGeoGraphicRisk/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},             
								    	url: url              
							        });
						        return(request
						                .then(getGeoGraphicCountRatioSuccess)
						                .catch(getGeoGraphicCountRatioError));

						        /* getAlertlist error function */
						        function getGeoGraphicCountRatioError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getGeoGraphicCountRatioSuccess(response) {
						            return(response);
						        }
						}
						
						/*
					     * @purpose:  Get GeoGraphic Aggregates
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getGeoGraphicAggregates(params,filtersData){
							var url =EHUB_API+"risk/getGeoGraphicRiskAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                  
								    	url: url              
							        });
						        return(request
						                .then(getGeoGraphicAggregatesSuccess)
						                .catch(getGeoGraphicAggregatesError));

						        /* getAlertlist error function */
						        function getGeoGraphicAggregatesError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getGeoGraphicAggregatesSuccess(response) {
						            return(response);
						        }
						}
					
						/*
					     * @purpose: Get CorruptionRisk
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCorruptionRisk(params,filtersData){
							var url =EHUB_API+"risk/getCorruptionRisk/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                
								    	url: url              
							        });
						        return(request
						                .then(getCorruptionRiskSuccess)
						                .catch(getCorruptionRiskError));

						        /* getAlertlist error function */
						        function getCorruptionRiskError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getCorruptionRiskSuccess(response) {
						            return(response);
						        }
						}
						/*
					     * @purpose:  Get CorruptionRiskAggregates
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCorruptionRiskAggregates(params,filtersData){
							var url =EHUB_API+"risk/getCorruptionRiskAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                
								    	url: url              
							        });
						        return(request
						                .then(getCorruptionRiskAggregatesSuccess)
						                .catch(getCorruptionRiskAggregatesError));

						        /* getCorruptionRiskAggregates error function */
						        function getCorruptionRiskAggregatesError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getCorruptionRiskAggregates success function */
						        function getCorruptionRiskAggregatesSuccess(response) {
						            return(response);
						        }
						}
						
						
						/*
					     * @purpose: Get PoliticalRisk
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getPoliticalRisk(params,filtersData){
							var url =EHUB_API+"risk/getPoliticalRisk/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},            
								    	url: url              
							        });
						        return(request
						                .then(getPoliticalRiskSuccess)
						                .catch(getPoliticalRiskError));

						        /* getAlertlist error function */
						        function getPoliticalRiskError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getPoliticalRiskSuccess(response) {
						            return(response);
						        }
						}
						/*
					     * @purpose:  getPoliticalRiskAggregates
					     * @created: 10th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getPoliticalRiskAggregates(params,filtersData){
							var url =EHUB_API+"risk/getPoliticalRiskAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},               
								    	url: url              
							        });
						        return(request
						                .then(getPoliticalRiskAggregatesSuccess)
						                .catch(getPoliticalRiskAggregatesError));

						        /* getPoliticalRiskAggregates error function */
						        function getPoliticalRiskAggregatesError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getPoliticalRiskAggregates success function */
						        function getPoliticalRiskAggregatesSuccess(response) {
						            return(response);
						        }
						}
					
						
					

							
					/*
				     * @purpose: getTransactionAndAlertAggregates
				     * @created: 12 feb 2018
				     * @params: fromDt, toDt, params(object)
				     * @return: success, error functions
				     * @author: swathi
				    */
					function getViewAllForTxsByType(fromDt, toDt, viewBy, params,filtersData){
						if(viewBy == 'OUTPUT')
							{var apiUrl =  EHUB_API + "dashboard/fetchTxByTypeListViewAll/"+ fromDt +"/"+ toDt + '?output=true';}
						else if(viewBy == 'INPUT')
							{var apiUrl =  EHUB_API + "dashboard/fetchTxByTypeListViewAll/"+ fromDt +"/"+ toDt + '?input=true';}
						else
							{var apiUrl =  EHUB_API + "dashboard/fetchTxByTypeListViewAll/"+ fromDt +"/"+ toDt;}
						
				        var request = $http({
				        	method: "POST",
							data:filtersData?filtersData:{}, 
				            url: apiUrl,
				            params: params
				        });
				        return(request
				                .then(getViewAllForTxsByTypeSuccess)
				                .catch(getViewAllForTxsByTypeError));
				
				        /*getViewAllForTxsByType error function*/
				        function getViewAllForTxsByTypeError(response) {
				            if (!angular.isObject(response.data) || !response.data.message) {
				                return($q.reject(response.data));
				            }
				            /*Otherwise, use expected error message.*/
				            return($q.reject(response.data.message));
				        }
				
				        /*getViewAllForTxsByType success function*/
				        function getViewAllForTxsByTypeSuccess(response) {
				            return(response);
				        }
					}
						
						/*
					     * @purpose:  Get Associated Entities
					     * @created: 12th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAssociatedEntities(params,filtersData){
							var url =EHUB_API+"dashboard/fetchAssociatedEntity/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
		                   		 var request = $http({
			                   			method: "POST",
										data:filtersData?filtersData:{},                
								    	url: url              
							        });
						        return(request
						                .then(getAssociatedEntitiesSuccess)
						                .catch(getAssociatedEntitiesError));

						        /* getAlertlist error function */
						        function getAssociatedEntitiesError(response) {
						            if (!angular.isObject(response.data) || !response.data.message) {
						                return($q.reject(response.data));
						            }
						            /* Otherwise, use expected error message. */
						            return($q.reject(response.data.message));
						        }

						        /* getAlertlist success function */
						        function getAssociatedEntitiesSuccess(response) {
						            return(response);
						        }
						}
						
						
						
						
						
						
						/*
					     * @purpose: get View All Risk/Counterparty/corporate structure
					     * @created: 14 feb 2018
					     * @params: fromDt, toDt, params(object)
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getViewAllForCorpCounterRisk(fromDt, toDt, params,filtersData){
							var apiUrl =  EHUB_API + "risk/getViewAll/"+ fromDt +"/"+ toDt;
					        var request = $http({
					        	method: "POST",
					        	data:filtersData?filtersData:{},
					            url: apiUrl,
					            params: params
					        });
					        return(request
					                .then(getViewAllForCorpCounterRiskSuccess)
					                .catch(getViewAllForCorpCounterRiskError));
					
					        /*getViewAllForCorpCounterRisk error function*/
					        function getViewAllForCorpCounterRiskError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /*Otherwise, use expected error message.*/
					            return($q.reject(response.data.message));
					        }
					
					        /*getViewAllForCorpCounterRisk success function*/
					        function getViewAllForCorpCounterRiskSuccess(response) {
					            return(response);
					        }
						}
						
						/*
					     * @purpose:  get TopFiveAlerts
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getTopFiveAlerts(params,filtersData){
							var url =EHUB_API+"corporateStructure/getTopFiveAlerts/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
						        	data:filtersData?filtersData:{},                
							    	url: url              
						        });
					        return(request
					                .then(getTopFiveAlertsSuccess)
					                .catch(getTopFiveAlertsError));

					        /* getTopFiveAlerts error function */
					        function getTopFiveAlertsError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getTopFiveAlerts success function */
					        function getTopFiveAlertsSuccess(response) {
					            return(response);
					        }
						}

						
						
						
						/*
					     * @purpose:  get CorporateStructure
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCorporateStructure(params,filtersData){
							var url =EHUB_API+"corporateStructure/getCorporateStructure/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
						        	data:filtersData?filtersData:{},                
							    	url: url              
						        });
					        return(request
					                .then(getCorporateStructureSuccess)
					                .catch(getCorporateStructureError));

					        /* getCorporateStructure error function */
					        function getCorporateStructureError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getCorporateStructure success function */
					        function getCorporateStructureSuccess(response) {
					            return(response);
					        }
						}
						
						
						/*
					     * @purpose:  get CorporateStructureAggregates
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getCorporateStructureAggregates(params,filtersData){
							var url =EHUB_API+"corporateStructure/getCorporateStructureAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
						        	data:filtersData?filtersData:{},                
							    	url: url              
						        });
					        return(request
					                .then(getCorporateStructureAggregatesSuccess)
					                .catch(getCorporateStructureAggregatesError));

					        /* getCorporateStructureAggregates error function */
					        function getCorporateStructureAggregatesError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getCorporateStructureAggregates success function */
					        function getCorporateStructureAggregatesSuccess(response) {
					            return(response);
					        }
						}
						
						

						
						
						
						/*
					     * @purpose:  get ShareholderCorporateStructure
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getShareholderCorporateStructure(params,filtersData){
							var url =EHUB_API+"corporateStructure/getShareholderAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
						        	data:filtersData?filtersData:{},               
							    	url: url              
						        });
					        return(request
					                .then(getShareholderCorporateStructureSuccess)
					                .catch(getShareholderCorporateStructureError));

					        /* getShareholderCorporateStructure error function */
					        function getShareholderCorporateStructureError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getShareholderCorporateStructure success function */
					        function getShareholderCorporateStructureSuccess(response) {
					            return(response);
					        }
						}
						
						
						/*
					     * @purpose:  get ShareholderAggregates
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getShareholderAggregates(params,filtersData){
							var url =EHUB_API+"corporateStructure/getShareholderCorporateStructure/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
						        	data:filtersData?filtersData:{},                    
							    	url: url              
						        });
					        return(request
					                .then(getShareholderAggregatesSuccess)
					                .catch(getShareholderAggregatesError));

					        /* getAlertlist error function */
					        function getShareholderAggregatesError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertlist success function */
					        function getShareholderAggregatesSuccess(response) {
					            return(response);
					        }
						}
						
						
						
											
						
						
						/*
					     * @purpose:  get AlertEntitiesGeography
					     * @created: 19th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAlertEntitiesGeography(params,filtersData){
							var url =EHUB_API+"corporateStructure/getAlertEntitiesGeography/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
		                   			data:filtersData?filtersData:{},                
							    	url: url              
						        });
					        return(request
					                .then(getAlertEntitiesGeographySuccess)
					                .catch(getAlertEntitiesGeographyError));

					        /* getAlertEntitiesGeography error function */
					        function getAlertEntitiesGeographyError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertEntitiesGeography success function */
					        function getAlertEntitiesGeographySuccess(response) {
					            return(response);
					        }
						}
						
						
						/*
					     * @purpose:  get GeographyAggregates
					     * @created: 19th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getGeographyAggregates(params,filtersData){
							var url =EHUB_API+"corporateStructure/getGeographyAggregates/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token;
	                   		 var request = $http({
		                   			method: "POST",
		                   			data:filtersData?filtersData:{},                
							    	url: url              
						        });
					        return(request
					                .then(getGeographyAggregatesSuccess)
					                .catch(getGeographyAggregatesError));

					        /* getAlertEntitiesGeography error function */
					        function getGeographyAggregatesError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertEntitiesGeography success function */
					        function getGeographyAggregatesSuccess(response) {
					            return(response);
					        }
						}

						
                        
						
						

						
						
						/*
					     * @purpose:  get AssociatedAlerts
					     * @created: 16th feb 2018
					     * @params: date
					     * @return: success, error functions
					     * @author: varsha
					    */
						function getAssociatedAlerts(params,type,filtersData){
							var url =EHUB_API+"corporateStructure/getAssociatedAlerts/"+params[0]+"/"+params[1]+"?token="+$rootScope.ehubObject.token+"&type="+type;
	                   		 var request = $http({
		                   			method: "POST",
		                   			data:filtersData?filtersData:{},               
							    	url: url              
						        });
					        return(request
					                .then(getAssociatedAlertsSuccess)
					                .catch(getAssociatedAlertsError));

					        /* getAlertlist error function */
					        function getAssociatedAlertsError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getAlertlist success function */
					        function getAssociatedAlertsSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose:  get Counter Parties Transactions
					     * @created: 17 feb 2018
					     * @params: fromDt, toDt, params, isDetected
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getCounterPartiesTransactions(fromDt, toDt, params, isDetected,filtersData){
							var url = EHUB_API + "risk/getCounterParties/" + fromDt + "/" + toDt + "?isDetected=" + isDetected;
							var request = $http({
								method: "POST",
	                   			data:filtersData?filtersData:{},              
						    	url: url,
						    	params: params
					        });
					        return(request
					                .then(getCounterPartiesTransactionsSuccess)
					                .catch(getCounterPartiesTransactionsError));

					        /* getCounterPartiesTransactions error function */
					        function getCounterPartiesTransactionsError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getCounterPartiesTransactions success function */
					        function getCounterPartiesTransactionsSuccess(response) {
					            return(response);
					        }
						}
						
						
						/*
					     * @purpose:  get risk Counter Parties
					     * @created: 17 feb 2018
					     * @params: fromDt, toDt, params
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getRiskCounterParties(fromDt, toDt, params,filtersData){
							var url = EHUB_API + "risk/counterPartyInfo/" + fromDt + "/" + toDt;
							var request = $http({							
		                   			method: "POST",
		                   			data:filtersData?filtersData:{},                
							    	url: url,
							    	params: params
					        });
					        return(request
					                .then(getRiskCounterPartiesSuccess)
					                .catch(getRiskCounterPartiesError));

					        /* getRiskCounterParties error function */
					        function getRiskCounterPartiesError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getRiskCounterParties success function */
					        function getRiskCounterPartiesSuccess(response) {
					            return(response);
					        }
						}
						/*
					     * @purpose: getCustomerInfo
					     * @created: 17 feb 2018
					     * @params: fromDt, toDt, params
					     * @return: success, error functions
					     * @author: swathi
					    */
						function getCustomerInfo(params){
							var url = EHUB_API + "risk/customerInfo/";
							var request = $http({
								method: 'GET',                
						    	url: url,
						    	params:params
					        });
					        return(request
					                .then(getCustomerInfoSuccess)
					                .catch(getCustomerInfoError));

					        /* getCustomerInfo error function */
					        function getCustomerInfoError(response) {
					            if (!angular.isObject(response.data) || !response.data.message) {
					                return($q.reject(response.data));
					            }
					            /* Otherwise, use expected error message. */
					            return($q.reject(response.data.message));
					        }

					        /* getCustomerInfo success function */
					        function getCustomerInfoSuccess(response) {
					            return(response);
					        }
						}				
	}