'use strict';
elementApp
.service('TransactionGraphService', transactionGraphService);

transactionGraphService.$inject = [
	'$q',
	'$timeout'
];

function transactionGraphService(
		$q,/*jshint unused:false*/
		$timeout){
	
		/*
	  * @purpose: load world map
	  * @created: 15 march 2018
	  * @pramas: name(string)
	  * @author: varsha
	  */
	 /* jshint validthis: true */
	this.plotWorldLocationChart = function(barOptions){
		var default_map_fill_color="#4E74DF";
		    if (barOptions.container) {
		        $(barOptions.container).empty();
		    }
/*		    //--------------------------Initialize Values-----------------------------
*/		    if (barOptions) {
		        this.container = barOptions.container ? barOptions.container : "body";
		        this.barColor = barOptions.barColor ? barOptions.barColor : "blue";
		        this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false;
		        this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
		        this.data = (barOptions.data) ? barOptions.data : [];
		        this.showTicks = barOptions.showTicks ? barOptions.showTicks : true;
		        this.ticks = barOptions.ticks ? barOptions.ticks : 'all';
		        this.showLegends = (barOptions.showLegends !== undefined) ? barOptions.showLegends : false;
		        this.xLabelRotation = barOptions.xLabelRotation ? barOptions.xLabelRotation : 0;
		        this.yLabelRotation = barOptions.yLabelRotation ? barOptions.yLabelRotation : 0;
		        this.margin = barOptions.margin ? {
		            top: barOptions.margin.top ? barOptions.margin.top : 20,
		            right: barOptions.margin.right ? barOptions.margin.right : 20,
		            bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
		            left: barOptions.margin.left ? barOptions.margin.left : 40
		        } : {top: 0, right: 5, bottom: 0, left: 5};
		        this.height = barOptions.height ? barOptions.height : 600;
		        this.width = barOptions.width ? barOptions.width : $(this.container).width() - 10;
		        this.showAxisX = (barOptions.showAxisX !== undefined) ? barOptions.showAxisX : true;
		        this.showAxisY = (barOptions.showAxisY !== undefined) ? barOptions.showAxisY : true;
		        this.showXaxisTicks = barOptions.showXaxisTicks !== undefined ? barOptions.showXaxisTicks : true;
		        this.showYaxisTicks = barOptions.showYaxisTicks !== undefined ? barOptions.showYaxisTicks : true;
		        this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
		        this.randomIdString = Math.floor(Math.random() * 10000000000);
		        barOptions.ChangeRadius=barOptions.ChangeRadius?barOptions.ChangeRadius:'false';
			    		


		    } else {
		        return false;
		    }
		    var randomSubstring = this.randomIdString;

		    var margin = this.margin,
		            width = this.width - margin.left - margin.right,
		            height = this.height - margin.top - margin.bottom,
		            barColor = this.barColor;
		    if(height>width){
		        height = width;
		        this.height = this.width;
		    }
/*		    //define tool tip
*/		    $(".Bubble_Chart_tooltip").remove();
		   // var tool_tip = $('body').append('<div class="world_map_tooltip1" style="padding:10px;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none; z-index: 1000"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		    var tool_tip = $('body').append('<div class="Bubble_Chart_tooltip" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		    
		    var colorScale = d3.scaleThreshold()
		            .domain([10000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000, 500000000, 1500000000])
		            .range(["rgb(247,251,255)", "rgb(222,235,247)", "rgb(198,219,239)", "rgb(158,202,225)", "rgb(107,174,214)", "rgb(66,146,198)", "rgb(33,113,181)", "rgb(8,81,156)", "rgb(8,48,107)", "rgb(3,19,43)"]);


		    //var centered;
            var chart_container=this.container;
		    var worlddata = this.data[0];
		    var populationData = this.data[1];
		    var path = d3.geoPath();
		    var svg = d3.select(this.container)
		            .append("svg")
		            .attr('height', this.height)
		            .attr('width', this.width)
		            .attr('id', 'mainSvg-' + randomSubstring)
		            .append('g')
		            .attr('class', 'map');
		    var mapScale=Math.floor(height/4);

		    var projection = d3.geoEquirectangular().scale(mapScale).translate([width / 2, height / 2]);
		    var path = d3.geoPath().projection(projection);

		    var WorldData = this.data;
		    if (WorldData) {
		        if (WorldData.length)
		           { drawWorld(worlddata, populationData);}
		    } else {
		    	
		    }
		    function drawWorld(data, population) {
		    	
		        var populationById = {};

		        population.forEach(function (d) {
		            populationById[d.id] = +d.population;
		        });

/*		        //TODO: Need to add Pins
		        //TODO: Need to add onClick behavior for pins to show Information
*/		        svg.append("g")
		                .attr("class", "countries")
		                .selectAll("path")
		                .data(data.features)
		                .enter()
		                .append("path")
		                .attr("d", path)
		                .style("fill", function (d) {
		                    if(populationById[d.id]){
		                        return "#435DBE";
		                    }else{
		                        return "#44555D";
		                    }
		                   
		                    return colorScale(populationById[d.id]);
		                })
		                .style('stroke', 'none')
		                .style('stroke-width', 0.5)
		                .style("opacity", 0.8);
		                
		        
            	svg.selectAll(".mark")
				    .data(barOptions.markers);
            	var geocoder = new google.maps.Geocoder();
            	angular.forEach(barOptions.markers, function(markerV, markerK){  
            		if(markerV.lat !== '' && markerV.lat && markerV.long !== '' && markerV.long){
            			 addLocation(markerV);
            		}else{
            		 setTimeout(function(){
            			 addLocation(markerV);
            		 },1000*markerK);
            		}
		    	});
            	function addLocation(markerV){
            		if(markerV.lat !== '' && markerV.lat && markerV.long !== '' && markerV.long){
            			addCircle(markerV,[markerV.long,markerV.lat]);
            		}else{
	            		//var  latitude, longitude;
	            		geocoder.geocode( { 'address': markerV.name}, function(results, status) {
	            			if (status == google.maps.GeocoderStatus.OK) {
	            				markerV.lat  = results[0].geometry.location.lat();
	            				markerV.long = results[0].geometry.location.lng();            				
	            		    
				    	
				    		var projectedValue = projection([markerV.long,markerV.lat]);
				    		
			        		addCircle(markerV,projectedValue);
	            		}
						});
            		}
            		function addCircle(markerV,projectedValue){
            			  var domainX = barOptions.domain ? barOptions.domain.x : 10;
            			  var domainY = barOptions.domain ? barOptions.domain.y : 5000;
            			 var sizeScale = d3.scaleLinear().domain([domainX,domainY]).range([5,15]);
            	            
            	 //code for number formatting
            			    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
            			        var n = this,
            			            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            			            decSeparator = decSeparator == undefined ? "." : decSeparator,
            			            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            			            sign = n < 0 ? "-" : "",
            			            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            			            j = (j = i.length) > 3 ? j % 3 : 0;
            			        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
            			    };
            		var circle=	svg.append("circle")
					    .attr('class','mark')
					     .on("contextmenu",function(){
			    	        	if(location.hash =="#!/alertDashboard"){
			    	                 window.transSelectedFilter.data = markerV;
			    	           		 window.transSelectedFilter.id = chart_container;
			    	           		 window.transSelectedFilter.chartType ="world";
			    	           	 }
			    		     })
					      .on("mouseover", function () {
					    	 
//		                    $(this).css("opacity", 1);
		                    if(markerV.txt){
				    			 $(".Bubble_Chart_tooltip").html('<span>' + markerV.txt + '</span> ' );	
				    		}else if(barOptions.text == 'bycount'){ 
				    			$(".Bubble_Chart_tooltip").html('<span>Name : ' + markerV.name + '</span> '+'<br>'+'<span>Count : ' + markerV.amount+ '</span>');
				    		}else{
				    		   $(".Bubble_Chart_tooltip").html('<span>Name : ' + markerV.name + '</span> '+'<br>'+'<span>Amount : $' + Number(markerV.amount).formatAmt() + '</span>');
				    		}
		                     return $(".Bubble_Chart_tooltip").css("display", "block");
				         })
		                .on("mousemove", function () {
		                	var p=$(chart_container);
		                	var position=p.offset();
		                	//var windowWidth=window.innerWidth;
		                	var tooltipWidth=$(".Bubble_Chart_tooltip").width()+50;
		                	var cursor=d3.event.x;
		                 	if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)){
		                		var element = document.getElementsByClassName("Bubble_Chart_tooltip");
		                		element[0].classList.remove("tooltip-left");
		            			element[0].classList.add("tooltip-right");
			               		$(".Bubble_Chart_tooltip").css("left",(d3.event.pageX - 15-$(".Bubble_Chart_tooltip").width()) + "px");
			             	}else{
		                		var element =document.getElementsByClassName("Bubble_Chart_tooltip");
		                   		element[0].classList.remove("tooltip-right");
		               		    element[0].classList.add("tooltip-left");
		               		    $(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
			           
		                	}
		                    return $(".Bubble_Chart_tooltip").css("top",d3.event.pageY + "px");
		   	
		                	
		                  //   $(".Bubble_Chart_tooltip").css("top", (d3.event.pageY - 10) + "px")
		                    //return  $(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
		                })
		                .on("mouseout", function () {
//		                    $(this).css("opacity", 0.4);
		                    //hide tool-tip
		                    return $(".Bubble_Chart_tooltip").css("display", "none");
		                })
		                
					   .attr("fill",function(){
						   if(markerV.fillColor){
							   return markerV.fillColor;
						   }
						   if(barOptions.fillColor){
							   return barOptions.fillColor;
						   }else{
							   return default_map_fill_color;
						   }
					   })
					   .attr("transform", function() {return "translate(" + projection([markerV.long,markerV.lat]) + ")";});
            		      		if(barOptions.ChangeRadius=='true'){
            	         			circle.attr('r', function(){
			         					  return sizeScale(parseInt(markerV.amount));
										}).style("cursor", "pointer");
			            		}else{
			            			circle.attr('r',4).style("cursor", "pointer");
			            		}
            		 
            		}
            	}
		        svg.append("path")
		                .datum(topojson.mesh(data.features, function (a, b) {// jshint ignore:line
		                    return a.id !== b.id;
		                }))
		                .attr("class", "names")
		                .attr("d", path);
		        
		var zoom = d3.zoom().on("zoom",function() {
		        svg.attr("transform",d3.event.transform);
		        svg.selectAll("path")
		            .attr("d", path.projection(projection)); 
		});
		  svg.call(zoom);
	    }
	};
	
	
	this.textcloudChart = function(barOptions){
	    if (barOptions.container) {
	        $(barOptions.container).empty();
	    }
	    //--------------------------Initialize Values-----------------------------
	    if (barOptions) {
	        this.container = barOptions.container ? barOptions.container : "body";
	        this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false;
	        this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
	        this.data = (barOptions.data) ? barOptions.data : [];
	         this.margin = barOptions.margin ? {
	            top: barOptions.margin.top ? barOptions.margin.top : 20,
	            right: barOptions.margin.right ? barOptions.margin.right : 20,
	            bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
	            left: barOptions.margin.left ? barOptions.margin.left : 40
	        } : {top: 20, right: 20, bottom: 50, left: 50};
	        this.height = barOptions.height ? barOptions.height : 600;
	        this.width = barOptions.width ? barOptions.width : $(this.container).width() - 10;
	        this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
	        this.randomIdString = Math.floor(Math.random() * 10000000000);
	        barOptions.textClick=barOptions.textClick?barOptions.textClick:'true';


	    } else {
	        console.error('Map Chart Initialization Error : Bar Chart Params Not Defined');
	        return false;
	    }
	    var randomSubstring = this.randomIdString;

	    var margin = this.margin,
	            width = this.width - margin.left - margin.right,
	            height = this.height - margin.top - margin.bottom,
	            barColor = this.barColor;
	    if (height > width) {
	        height = 3 * width / 4;
	        this.height = 3 * this.width / 4;
	    }
	    //define tool tip
//	    $(".world_map_tooltip").remove();
//	    var tool_tip = $('body').append('<div class="world_map_tooltip" style="position: absolute; opacity: 1; pointer-events: none; visibility: hidden;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
	    var colorScale = d3.scaleOrdinal(d3.schemeCategory10);
//	    var colorScale = d3.scaleThreshold()
//	            .domain([10000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000, 500000000, 1500000000])
//	            .range(["rgb(247,251,255)", "rgb(222,235,247)", "rgb(198,219,239)", "rgb(158,202,225)", "rgb(107,174,214)", "rgb(66,146,198)", "rgb(33,113,181)", "rgb(8,81,156)", "rgb(8,48,107)", "rgb(3,19,43)"]);


	    var textcloudData = this.data;
	    var domainX = barOptions.domain ? barOptions.domain.x : 10;
		var domainY = barOptions.domain ? barOptions.domain.y : 5000;
		var sizeScale = d3.scaleLinear().domain([domainX,domainY]).range([12,35]);

	    //define svg
	    var svg = d3.select(this.container)
	            .append("svg")
	            .attr('height', this.height)
	            .attr('width', this.width)
	            .attr('id', 'mainSvg-' + randomSubstring)
	            .attr("class", "wordcloud")
	            .append("g")
	            .attr("transform", "translate(" + this.width / 2 + "," + this.height / 2 + ")");
//	    .attr("transform", "translate(320,200)");
	    d3.layout.cloud()
	            .size([this.width, this.height])
	            .words(textcloudData)
	            .rotate(0)
	            .fontSize(function (d) {
	            	if(location.hash =="#!/alertDashboard"){
	            	  return parseInt(sizeScale(d.amount));
	            	}else {
	                  return parseInt(sizeScale(d.size));
	            	}
	            })
	            .on("end", draw)
	            .start();


	    function draw(words) {
	         var clickText= svg.selectAll("g text")
	                .data(words, function (d) {
	                     return d.text;
	                })

	                //Entering words
	                .enter()
	                .append("text")
	                .style("font-family",'"Roboto Regular"')
	                .style("fill", function () {
	                    return "#4192b7";
	                })
	                .style("font-size", function (d) {
	                	if(location.hash =="#!/alertDashboard"){
	                		return parseInt(sizeScale(d.amount));
	                	}else {
	                		return parseInt(sizeScale(d.size));
	                	}
	                })
	               
	                .attr("text-anchor", "middle")
//	            .attr('font-size', 1)
	                .attr("transform", function (d) {
	                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
	                })
	                .text(function (d) {
	                    return d.text;
	                });
	                if(barOptions.textClick=='true'){
		                clickText.on("click", function (d) {
								if (d.text == void 0)
								   { return;}
								if(window.location.href.split("/transactionIntelligence/#")[1]){
									window.open(window.location.href.split("/transactionIntelligence/#")[0] + "/entity/#!/company/" + d.text, '_blank');
								    
								}else
									{window.open(window.location.href.split("/#/")[0] + "/entity/#!/company/" + d.text, '_blank');}
					   }).style("cursor", "pointer");
		            }

	    }
	};
	
}