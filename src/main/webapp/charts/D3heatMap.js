'use strict';
 d3.select("body").append("div").attr("class", "heatmap_tooltip").style("position", "absolute").style("z-index", 1000).style("background", "rgb(27, 39, 53)").style("padding", "5px 10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");

function heatMap(options) {/*jshint ignore:line*/
        $(options.container).empty();
        if (options)
        {
            options.container = options.container ? options.container : "body";
            options.width = options.width ? options.width : $(options.container).width();
            options.height = options.height ? options.height : 300;
            options.marginTop = options.marginTop ? options.marginTop : 30;
            options.marginBottom = options.marginBottom ? options.marginBottom : 30;
            options.marginRight = options.marginRight ? options.marginRight : 20;
            options.marginLeft = options.marginLeft ? options.marginLeft : 50;
            options.itemSize = options.itemSize ? options.itemSize : 22;
            options.show_YAxis = options.show_YAxis == false ? options.show_YAxis : true;
            options.show_YAxisText = options.show_YAxisText == false ? options.show_YAxisText : true;
            options.rotate_YAxisText = options.rotate_YAxisText ? options.rotate_YAxisText : false;            
            options.show_YAxisPath = options.show_YAxisPath == false ? options.show_YAxisPath : true;
            options.show_YAxisTicks = options.show_YAxisTicks == false ? options.show_YAxisTicks : true;
            options.rotate_YAxisTicks = options.rotate_YAxisTicks == false ? options.rotate_YAxisTicks : true;
            options.fontSize_YAxis =options.fontSize_YAxis ?options.fontSize_YAxis:10;
            options.fontFamily_YAxis = options.fontFamily_YAxis?options.fontFamily_YAxis:"sans-serif";
            options.show_XAxis = options.show_XAxis == false ? options.show_XAxis : true;
            options.show_XAxisText = options.show_XAxisText == false ? options.show_XAxisText : true;
            options.rotate_XAxisText = options.rotate_XAxisText == false ? options.rotate_XAxisText : true;
            options.show_XAxisPath = options.show_XAxisPath == false ? options.show_XAxisPath : true;
            options.show_XAxisTicks = options.show_XAxisTicks == false ? options.show_XAxisTicks : true;
            options.rotate_XAxisTicks = options.rotate_XAxisTicks == false ? options.rotate_XAxisTicks : true;
            options.fontSize_XAxis =options.fontSize_XAxis ?options.fontSize_XAxis:10;
            options.fontFamily_XAxis = options.fontFamily_XAxis?options.fontFamily_XAxis:"sans-serif";
            options.gridx = options.gridx ? options.gridx : false;
            options.gridy = options.gridy ? options.gridy : false;
        }
        var itemSize = options.itemSize,
                cellSize = itemSize - 1,/*jshint ignore:line*/
                margin = {top: options.marginTop, right: options.marginRight, bottom: options.marginBottom, left: options.marginLeft};

        var width = options.width - margin.right - margin.left,
            height = options.height - margin.top - margin.bottom;
        var colorsArr = ['#5E4A91', '#5E883E', '#843F90', '#A69636', '#C03A77', '#4866AA', '#BF4541', '#64C068'];
        
        	var data1 = jQuery.extend(true, [],options.data);
        	var expenseMetrics = d3.nest()
		            .key(function (d) {
		                return d.country;
		            })
		            .rollup(function (v) {
		                return {
		                    extent: d3.extent(v, function (d) {
		                        return d.value;
		                    }),
		                    
		                };
		            })
		            .entries(data1);
		    for (var i in data1){
		        for(var j in expenseMetrics){
		            if(data1[i].country == expenseMetrics[j].key){
		               data1[i].extent = expenseMetrics[j].value.extent;
		            }
		        }
		    } 
	            var x_elements = d3_v3.set(data1.map(function (item) {
	                return item.product;
	            })).values(),
	                    y_elements = d3_v3.set(data1.map(function (item) {
	                        return item.country;
	                    })).values();
	            var cellSizeWidth = (width)/x_elements.length;
	            var cellSizeHeight = (height)/y_elements.length;
	            var xScale = d3_v3.scale.ordinal()
	                    .domain(x_elements)
	                    .rangeBands([0, x_elements.length * cellSizeWidth]);
	
	            var xAxis = d3_v3.svg.axis()
	                    .scale(xScale)
	                    .tickFormat(function (d) {
	                        return d;
	                    })
	                    .orient("top");
	
	            var yScale = d3_v3.scale.ordinal()
	                    .domain(y_elements)
	                    .rangeBands([0, y_elements.length * cellSizeHeight]);
	
	            var yAxis = d3_v3.svg.axis()
	                    .scale(yScale)
	                    .tickFormat(function (d) {
	                        return d;
	                    })
	                    .orient("left");
	
	            var colorScale = d3.scaleOrdinal()
	            		.domain([1, 8])
	                    .range(colorsArr);
	// Draw SVG
	            var svg = d3_v3.select(options.container)
	                    .append("svg")
	                    .attr("width", width + margin.left + margin.right)
	                    .attr("height", height + margin.top + margin.bottom)
	                    .append("g")
	                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	// Draw RECT blocks
	             svg.selectAll('rect')
	                    .data(data1)
	                    .enter().append('g').append('rect')
	                    .attr('class', 'cell')
	                    .attr('width', cellSizeWidth-1)
	                    .attr('height', cellSizeHeight-1)
	                    .style("cursor", "pointer")
	                    .attr('y', function (d) {
	                        return yScale(d.country);
	                    })
	                    .attr('x', function (d) {
	                        return xScale(d.product);
	                    })
	                    .attr('fill', function (d) {
	                    	return colorScale(d.country);
	                    }).attr('opacity', function (d) {
	                    	if(d.extent[0] == d.extent[1]){
	                    		 return d.extent[0];
	                    	}else{
		                        var expScale = d3.scale.linear().domain(d.extent).range([0.1,1]);
		                        return expScale(d.value);
	                    	}
	                    }).on("click", function (d) {
	                   		 window.pushFilters (d.country, false, "attackType", 'notLocation', 'selectedAttackType');
	                   		 setTimeout(function(){
	                   			$(".heatmap_tooltip").css("display", "none");
	                   			 window.pushFilters (d.key, false, 'industry', d.product, 'industryMultiple', 'locationMultipleEnd');
	                   		 }, 100);
	                    }).on('mouseover',function(d){
							
							$(".heatmap_tooltip").css("display", "block");
                	
							$(".heatmap_tooltip").html('<div><span class="text-gray-ta" style="text-transform: uppercase">attack type: '+d.country+'  </span> </div><div><span class="text-gray-ta" style="text-transform: uppercase">industry: '+d.product+'  </span> </div><div><span>'+'NUMBER OF INCIDENTS : '+'</span><span> &nbsp'+nFormatter(d.doc_count,1) +'</span></div>');/*jshint ignore:line*/
							$(".heatmap_tooltip").css("width","230px");
						})
						.on("mousemove",function(d){/*jshint ignore:line*/
							var p = $(options.container);
								var position = p.offset();
								var windowWidth = window.innerWidth;/*jshint ignore:line*/
								var tooltipWidth = $(".heatmap_tooltip").width() + 50;
								var cursor = d3_v3.event.x;
							 if ((position.left < d3_v3.event.pageX) && (cursor > tooltipWidth)) {
									var element = document.getElementsByClassName("heatmap_tooltip");
									for (var i = 0; i < element.length; i++) {
										element[i].classList.remove("tooltip-left");
										element[i].classList.add("tooltip-right");
									}
									$(".heatmap_tooltip").css("left", (d3_v3.event.pageX - 30 - $(".heatmap_tooltip").width()) + "px");
								} else {
									var element = document.getElementsByClassName("heatmap_tooltip");
									for (var i = 0; i < element.length; i++) {
										element[i].classList.remove("tooltip-right");
										element[i].classList.add("tooltip-left");
									}
									$(".heatmap_tooltip").css("left", (d3_v3.event.pageX) +20 + "px");
								}
								return $(".heatmap_tooltip").css("top", d3_v3.event.pageY-20+ "px");
						 
							})
							 .on("mouseout",function(){
								 $(".heatmap_tooltip").css("display", "none");
							});
	// Draw y axis
	            var y_g = svg.append("g")
	                    .attr("class", "y axis")
	                    .style("display", function () {
	                        return options.show_YAxis ? 'block' : 'none';
	                    })
	                    .call(yAxis);
	
	
	//  Y Axis path        
	            y_g.selectAll("path")
	                    .attr("class", "y axispath")
	                    .style("stroke", "#ccc")
	                    .style("shape-rendering", "crispEdges")
	                    .style("fill", "none")
	                    .style("display", function () {
	                        return options.show_YAxisPath ? 'block' : 'none';
	                    });
	//  Y Axis Text                   
	            y_g.selectAll('text')
	                    .attr('font-weight', 'normal')
	                    .style("font-size", options.fontSize_YAxis + "px")
	                    .style("font-family", options.fontFamily_YAxis)
	                    .style("display", function () {
	                        return options.show_YAxisText ? 'block' : 'none';
	                    })
	                    .style("fill", "#6c7e88")
	                    .attr("transform", function () {
	                        return options.rotate_YAxisText ? 'rotate(-120)' : 'rotate(0)';
	                    });
	//  Y Axis Ticks                   
	            y_g.selectAll("line")
	                    .attr("class", "y axisline")
	                    .style("stroke", "#ccc")
	                    .style("shape-rendering", "crispEdges")
	                    .style("fill", "none")
	                    .style("display", function () {
	                        return options.show_YAxisTicks ? 'block' : 'none';
	                    })
	                    .attr("transform", function () {
	                        return options.rotate_YAxisTicks ? 'rotate(-60)' : 'rotate(0)';
	                    });
	// Draw x axis
	            var x_g = svg.append("g")
	                    .attr("class", "x axis")
	                    .style("display", function () {
	                        return options.show_XAxis ? 'block' : 'none';
	                    })
	                    .call(xAxis);
	   //  X Axis path        
	            x_g.selectAll("path")
	                    .attr("class", "y axispath")
	                    .style("stroke", "#ccc")
	                    .style("shape-rendering", "crispEdges")
	                    .style("fill", "none")
	                    .style("display", function () {
	                        return options.show_XAxisPath ? 'block' : 'none';
	                    });                  
	                              
	//  X Axis Text  
	            x_g.selectAll('text')
	                    .attr('font-weight', 'normal')
	                    .style("font-size", options.fontSize_XAxis + "px")
	                    .style("font-family", options.fontFamily_XAxis)
	                    .style("text-anchor", "start")
	                    .attr("dx", ".8em")
	                    .attr("dy", ".5em")
	                    .style("fill", "#6c7e88")
	                    .attr("transform", function () {
	                        return options.rotate_XAxisText ? 'rotate(-80)' : 'rotate(0)';
	                    });
	 
	//  X Axis Ticks                   
	            x_g.selectAll("line")
	                    .attr("class", "x axisline")
	                    .style("stroke", "#ccc")
	                    .style("shape-rendering", "crispEdges")
	                    .style("fill", "none")
	                    .style("display", function () {
	                        return options.show_XAxisTicks ? 'block' : 'none';
	                    })
	                    .attr("transform", function () {
	                        return options.rotate_XAxisTicks ? 'rotate(-60)' : 'rotate(0)';
	                    });
}