'use strict';
var stackedtimelinediv = d3.select("body").append("div").attr("class", "toolTip_piehorizontal tooltip_stackedTime").style("position", "absolute").style("z-index", 1000).style("background","rgb(27, 39, 53)").style("padding","5px 10px").style("border-radius","10px").style("font-size","10px").style("display","none");
function stackedbarTimelinechart(options){/*jshint ignore:line*/
$(options.container).empty();
  if(options)
  {
  options.container=options.container?options.container:"body";
     options.width=options.width?options.width:$(options.container).width()-10;
     options.height=options.height?options.height:300;
     options.marginTop=options.marginTop?options.marginTop:10;
     options.marginBottom=options.marginBottom?options.marginBottom:30;
     options.marginRight=options.marginRight?options.marginRight:10;
     options.marginLeft=options.marginLeft?options.marginLeft:50;
     options.xParam=options.xParam?options.xParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
     options.yParam=options.yParam?options.yParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
     options.gridx=options.gridx?options.gridx:false;
     options.gridy=options.gridy?options.gridy:false;
     options.axisX=options.axisX?options.axisX:false;
     options.axisY=options.axisY?options.axisY:false;
     options.showxYaxis = options.showxYaxis?options.showxYaxis:false;
     options.color_hash = options.color_hash?options.color_hash:{
         0 : ["Invite","#3132FE"],
         1 : ["Accept","#E81F0F"],
         2 : ["Decline","#B2CCE7"]

       };
    				   
    		 
  }   
  var padding = {top: options.marginTop, right:options.marginRight , bottom:options.marginBottom , left:options.marginLeft };
  var   w =  options.width - padding.left - padding.right,
     h = options.height - padding.top - padding.bottom;
 var dataset;
  //Set up stack method
  var stack = d3_v3.layout.stack();

   dataset = options.data;

   //Data, stacked
   stack(dataset);

   //var color_hash = 
if(options.colors)
{var colorScale = d3.scaleOrdinal().range(options.colors);}
   //Set up scales
   var x = d3.scale.ordinal()
   .domain(dataset[0].map(function(d) { return d.time; }))
   .rangeRoundBands([0, w],0.2);

   var y = d3.scale.linear()
   .domain([0, d3.max(dataset, function(d) {  return d3.max(d, function(d) { return d.y0 + d.y; });  })])
   
    .range([h,0]);
   var xAxis = d3_v3.svg.axis()
   .scale(x);
	if(x.domain().length>3){
	
	  
		xAxis .tickValues(x.domain().filter(function(d, i) { return !(i % 3); }))/*jshint ignore:line*/
	         .orient("bottom")
	//         .ticks(d3_v3.time.days,1)
	         .ticks(5);
	}else if(x.domain().length>2){
		xAxis  .tickValues(x.domain().filter(function(d, i) { return !(i % 2); }))/*jshint ignore:line*/
	    .orient("bottom")
	    .ticks(5);
	}else{
		xAxis
	    .orient("bottom")
	    .ticks(5);
	}

   var yAxis = d3_v3.svg.axis()/*jshint ignore:line*/
         .scale(y)
         .orient("left")
         .ticks(10);



   //Easy colors accessible via a 10-step ordinal scale
   var colors = d3_v3.scale.category10();/*jshint ignore:line*/

   //Create SVG element
   var svg = d3_v3.select(options.container)
      .append("svg")
      .attr("width", options.width)
      .attr("height", options.height);

   // Add a group for each row of data
   var groups = svg.selectAll("g")
    .data(dataset)
    .enter()
    .append("g")
    .attr("class","rgroups")
//    .attr("transform","translate("+ padding.left + "," + (h-padding.bottom-padding.top ) +")")
    .style("fill", function(d) {  
    	if(options.colors){
    		return colorScale(d.type);
    	}
     return options.color_hash[dataset.indexOf(d)][1];
    });

   // Add a rect for each data value
   var rects = groups.selectAll("rect")
   .data(function(d) { return d; })
   .enter()
   .append("rect")
   .attr("x", function(d) { return x(d.time); })
   .attr("y", function(d) { return y(d.y0 + d.y); })
   .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
   .attr("width", x.rangeBand())
   .style("fill", function(d) { 
    	
    	if(options.colorObj){
    		return options.colorObj[d.time];
    	}
   })
//	 .attr("rx", (15 / 4))
//	 .attr("ry", (15 / 4))
    .style("fill-opacity",1);
   
   rects.on("click", function (d){
   	window.openSideModal(d);
	
   }).on("mouseover", function (d) {
	   stackedtimelinediv.style("visibility", "visible");
	stackedtimelinediv.style("display", "inline-block");
		if(d.txt){
			stackedtimelinediv.html(d.txt);
		}else
			{stackedtimelinediv.html('<div class="text-uppercase"><div>year: '+d.time+'</div><div>Amount:'+d.y+'</div><div>type:'+d.type+'</div></div>');}
	})
	.on("mousemove", function () {	
		if($(".tooltip_stackedTime").width()+d3_v3.event.pageX+10<$(window).width()){
    		$(".tooltip_stackedTime").css("left", (d3_v3.event.pageX + 10) + "px");
    	}else{
    		$(".tooltip_stackedTime").css("left", (d3_v3.event.pageX - 25-$(".tooltip_stackedTime").width()) + "px");
    	}		                     
      return  stackedtimelinediv.style("top", d3_v3.event.pageY - 25 + "px");
	
	})
	.on("mouseout", function () {
		  stackedtimelinediv.style("visibility", "hidden");
		stackedtimelinediv.style("display", "none");
	});
	if(options.axisX){
	    var x_g = svg.append("g")
	     .attr("class","x axis")
	     .attr("transform","translate("+padding.left+"," + (h - padding.bottom) + ")")
	     .call(xAxis);
	    if(options.tickColor){
	    	x_g.selectAll("line").style("stroke",options.tickColor);
	    	x_g.selectAll("text").style("fill",options.tickColor).style("stroke","none").style("font-size","10px");
	    	x_g.select("path").style("display","none");
	    }
	}
    
//
//
//    svg.append("g")
//     .attr("class","y axis")
//     .attr("transform","translate(" + padding.left + "," + padding.top + ")")
//     .call(yAxis);

    // adding legend

//    var legend = svg.append("g")
//        .attr("class","legend")
//        .attr("x", w - padding.right - 65)
//        .attr("y", 25)
//        .attr("height", 100)
//        .attr("width",100);
//
//    legend.selectAll("g").data(dataset)
//       .enter()
//       .append('g')
//       .each(function(d,i){
//        var g = d3_v3.select(this);
//        g.append("rect")
//         .attr("x", w - padding.right - 65)
//         .attr("y", i*25 + 10)
//         .attr("width", 10)
//         .attr("height",10)
//         .style("fill",color_hash[String(i)][1]);
//
//        g.append("text")
//         .attr("x", w - padding.right - 50)
//         .attr("y", i*25 + 20)
//         .attr("height",30)
//         .attr("width",100)
//         .style("fill",color_hash[String(i)][1])
//         .text(color_hash[String(i)][0]);
//       });

//    svg.append("text")
//    .attr("transform","rotate(-90)")
//    .attr("y", 0 - 5)
//    .attr("x", 0-(h/2))
//    .attr("dy","1em")
//    .text("Number of Messages");
//
//   svg.append("text")
//      .attr("class","xtext")
//      .attr("x",w/2 - padding.left)
//      .attr("y",h - 5)
//      .attr("text-anchor","middle")
//      .text("Days");
//
//   svg.append("text")
//         .attr("class","title")
//         .attr("x", (w / 2))             
//         .attr("y", 20)
//         .attr("text-anchor", "middle")  
//         .style("font-size", "16px") 
//         .style("text-decoration", "underline")  
//         .text("Number of messages per day.");

  
}