"use strict";

angular.module('docparserApp')
    .factory('docParserService', docParserService);


docParserService.$inject = [
    '$http',
    'EHUB_API',
    '$q',
    '$rootScope',
    '$stateParams',
    'Upload'
];

function docParserService(
    $http,
    EHUB_API,
    $q,
    $rootScope,
    $stateParams,
    Upload) {
    return {
        getTemplateList: getTemplateList, //getting template list
        getQuestionAndAnswerNewTemplate:getQuestionAndAnswerNewTemplate, //new template creation
        saveTemplate:saveTemplate, //for saving the template
        getDocumentDetails: getDocumentDetails, //To get the document details
        createCaseFromQuestioner: createCaseFromQuestioner, //for case creation
        editTemplate:editTemplate //To edit the existing template
    };
    /*
     * @purpose: getTemplateList
     * @created: 4th Sep 2018
     * @params: no params
     * @return: success, error functions
     * @author: Prasanthi
     */

    function getTemplateList(params) {
        var defer = $q.defer();       

        var apiUrl = EHUB_API + 'documentParser/getTemplates?' + "&token=" + $rootScope.ehubObject.token;

//        var apiUrl = "data/tableData.json";
        var request = $http({
            method: "GET",           
            headers: {
                "Content-Type": "application/json"
            },
            url: apiUrl,
            params: params
        });
        request
            .then(getTemplateListSuccess)
            .catch(getTemplateListError);

        /*getTemplateList error function*/
        function getTemplateListError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getTemplateList success function*/
        function getTemplateListSuccess(response) {
            defer.resolve(response);
        }

        return defer.promise;
    }

    /*
     * @purpose: getQuestionAndAnswerNewTemplate
     * @created: 7 sep 2018
     * @params: data(object)
     * @return: success, error functions
     * @author: Prasanthi
    */
	function getQuestionAndAnswerNewTemplate(data){
        var apiUrl =EHUB_API+'documentParser/getQuestionAndAnswerNewTemplate?token='+$rootScope.ehubObject.token;			

        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data
        });
        return(request
                .then(getQuestionAndAnswerNewTemplateSuccess)
                .catch(getQuestionAndAnswerNewTemplateError));

/*        getQuestionAndAnswerNewTemplate error function*/
        function getQuestionAndAnswerNewTemplateError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return($q.reject(response.data));
            }
             /*Otherwise, use expected error message.*/
            return($q.reject(response.data.message));
        }

        /*getQuestionAndAnswerNewTemplate success function*/
        function getQuestionAndAnswerNewTemplateSuccess(response) {
            return(response);
        }
    }
    


	 /*
     * @purpose: saveTemplate
     * @created: 7 sep 2018
     * @params: data(object)
     * @return: success, error functions
     * @author: Prasanthi
    */
	function saveTemplate(data,params,update){
        var apiUrl;
        if(update){
        	apiUrl =EHUB_API+'documentParser/updateTemplateQuestions';
        }else{
        	apiUrl =EHUB_API+'documentParser/createNewTemplate';			
        }       

        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data,
            params:params
        });
        return(request
                .then(saveTemplateSuccess)
                .catch(saveTemplateError));

/*        saveTemplate error function*/
        function saveTemplateError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return($q.reject(response.data));
            }
             /*Otherwise, use expected error message.*/
            return($q.reject(response.data.message));
        }

        /*saveTemplate success function*/
        function saveTemplateSuccess(response) {
            return(response);
        }
       
	}
	 /*
     * @purpose: get document details
     * @created: 11th sep 2018
     * @params: Id
     * @return: success, error functions
     * @author: swathi
     */
    function getDocumentDetails(Id) {
        var defer = $q.defer();
        var apiUrl = EHUB_API + 'documentParser/getDocumentDetailsDocParser?token=' + $rootScope.ehubObject.token + "&docId=" + Id;
        var request = $http({
            method: 'GET',
            url: apiUrl
        });
        request
            .then(getDocumentDetailsSuccess)
            .catch(getDocumentDetailsError);

        /*getDocumentDetails error function*/
        function getDocumentDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
            defer.reject(response.data.message);
        }

        /*getDocumentDetails success function*/
        function getDocumentDetailsSuccess(response) {
            defer.resolve(response);
        }
        return defer.promise;
    }
    /*
     * @purpose: create Case From Questioner
     * @created: 11th sep 2018
     * @params: params
     * @return: success, error functions
     * @author: Swathi
     */
    function createCaseFromQuestioner(params, data) {
        var defer = $q.defer();
        var apiUrl = EHUB_API + 'case/createCaseFromQuestioner';
        var request = Upload.upload({
            method: 'POST',
            url: apiUrl,
            timeout: defer.promise,
            params: params,
            data: data
        });
        request
            .then(createCaseFromQuestionerSuccess)
            .catch(createCaseFromQuestionerError);
        
        setTimeout(function(){
        	if(!success){
        		 defer.resolve('Cancelled');
        	}
        },180000);
         var success;
        /*create Case From Questioner error function*/
        function createCaseFromQuestionerError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                defer.reject(response.data);
            }
            /*Otherwise, use expected error message.*/
        	if(response.status == -1){
        		defer.reject(response);
        	}else{
        		 /*Otherwise, use expected error message.*/
	            defer.reject(response.data.message);
        	}
           
        }

        /*create Case From Questioner success function*/
        function createCaseFromQuestionerSuccess(response) {
        	success = true ;
            defer.resolve(response);
        }
        return defer.promise;
	}
	 /*
     * @purpose: Edit template
     * @created: 24th sep 2018
     * @params: params
     * @return: success, error functions
     * @author: Prasanthi
     */
    function editTemplate(params) {
    	 var defer = $q.defer();       

         var apiUrl = EHUB_API + 'documentParser/editTemplate';

//         var apiUrl = "data/tableData.json";
         var request = $http({
             method: "GET",           
             headers: {
                 "Content-Type": "application/json"
             },
             url: apiUrl,
             params: params
         });
         request
             .then(editTemplateSuccess)
             .catch(editTemplateError);

         /*editTemplate error function*/
         function editTemplateError(response) {
             if (!angular.isObject(response.data) || !response.data.message) {
                 defer.reject(response.data);
             }
             /*Otherwise, use expected error message.*/
             defer.reject(response.data.message);
         }

         /*editTemplate success function*/
         function editTemplateSuccess(response) {
             defer.resolve(response);
         }

         return defer.promise;
	}
}