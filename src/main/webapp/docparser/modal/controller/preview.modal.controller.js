'use strict';
angular
	.module("docparserApp")
	.controller('PreviewModalController', previewModalController);

previewModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'previewData'
];

function previewModalController(
	$scope,
	$rootScope,
	$uibModalInstance,
	previewData) {
		
	$scope.previewData = previewData;
	/*
	 * @purpose: close modal
	 * @created: 08 sep 2018
	 * @author: swathi
	*/
	$scope.closeUploadFileModal = function () {
		$uibModalInstance.close();
	};
}