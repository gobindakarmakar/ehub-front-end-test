'use strict';
angular
	.module("docparserApp")
	.controller('UploadDocparserFileController', uploadDocparserFileController);

uploadDocparserFileController.$inject = [
	'$scope',
	'Flash',
	'$state',
	'$uibModal',
	'$rootScope',
	'docParserService',
	'HostPathService',
	'$uibModalInstance',
	'EHUB_FE_API',
	'TopPanelApiService',
	'$window'
];
function uploadDocparserFileController(
	$scope,
	Flash,
	$state,
	$uibModal,
	$rootScope,
	docParserService,
	HostPathService,
	$uibModalInstance,
	EHUB_FE_API,
	TopPanelApiService,
	$window
) {

	$scope.files = [];
	var cancelUpload = null;
	var fileCount = 0;

	/* for drag n drop */
	$scope.$watch('filess', function () {
		if ($scope.filess) {
			$scope.uploadDocumentOnSubmit($scope.filess);
		}
	});
	/* for drag n drop end */

	// Abort the current request (if its running).
	$scope.cancelRequest = function (idIndex, $event, file) {
		$("#progressBar" + idIndex).width(75);
		$("#percent" + idIndex).html('Cancelled');
		$event.stopPropagation();
		$scope.files.shift(file);
		HostPathService.FlashSuccessMessage('UPLOAD CANCELLED', '');
		return (cancelUpload && cancelUpload.abort());
	};

	$scope.uploadDocumentOnSubmit = function (files) {
		fileCount = 0;
		if (files && files.length) {
			files.forEach(function (file) {
				$scope.uploadDocument(file);
			});
		} else {
			$scope.uploadDocument(files);
		}
	};
	/*
	 * @purpose: upload Document
	 * @created: 25th Jun 2018
	 * @params: file(object)
	 * @author: swathi
	 */

	$scope.uploadDocument = function (files) {
		$rootScope.docsLoader = true;
		fileCount++;
		var ext = files.name.split('.').pop().toLowerCase();
		//if ($.inArray(ext, ['doc', 'docx', 'pdf', 'csv']) == -1) {
		
	    if ($.inArray(ext, ['pdf']) == -1) { //checking uploaded file type
			HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: pdf");
			$uibModalInstance.close();
			$rootScope.docsLoader = false;
			return;
		}
		else if (fileCount > 50) { //checking no.of files 
			HostPathService.FlashErrorMessage('Number of files should not be more than 50.', '');
			$rootScope.docsLoader = false;
			return;
		}
		else if (files.size > 10240000) { //checking file size
			HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', files.name);
			$rootScope.docsLoader = false;
			return;
		}
		else {
			var params = {
				token: $rootScope.ehubObject.token,
			};
			var data = {
				uploadFile: files
			};
			//will check if the file is exist or not using existTemplate API
			(cancelUpload = TopPanelApiService.existTemplate(params, data)).then(function (response) {
				var existData = {
					'id': response.data.templateName,
					'docId': response.data.docId,
					'tId': response.data.templateId
				};
				if (response.data && response.data.docId && response.data.templateId == null && response.data.templateName == null) {
					//if file is not exist it will redirect to docparser page to create new template
					$state.go('docParserPage', {docId: response.data.docId,id:undefined,tId:undefined},  {notify: false});
					setTimeout(function(){
						$window.location.reload();
					},1000);
				}
				else if (response.data && response.data.docId && response.data.templateId && response.data.templateName) {
					//if file exist it will ask for edit the existing file 
					$scope.openModal(existData);
					$rootScope.docsLoader = false;
				}
				else if(response.data && response.data.docId == null) {
					HostPathService.FlashErrorMessage('ERROR', "Template already exist, please upload another file.");
					$rootScope.docsLoader = false;
				}
				else if(response.data && response.data.docId == null) {
					HostPathService.FlashErrorMessage('ERROR', "Template already exist, please upload another file.");
					$rootScope.docsLoader = false;
				}
				else {
					HostPathService.FlashErrorMessage('ERROR', "Failed to upload file");
					$rootScope.docsLoader = false;
				}
				$uibModalInstance.close();
			}, function (error) {
				$rootScope.docsLoader = false;
				HostPathService.FlashErrorMessage('ERROR', error.responseMessage);
				$uibModalInstance.close();
			}, function () {
				$uibModalInstance.close();
			});
		}
	};
	/*
* @purpose: to open modal popup To confirm to navigate to docparser page
* @created: 7th sep 2018
* @params: params
* @returns: data
* @author: prasanthi
*/
	$scope.openModal = function (existData) {
		var dataModal = $uibModal.open({
		   templateUrl: function(){
			   if((window.location.hash.indexOf("#!/") < 0))
				   {return 'scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
					else
					  {return '../scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
		   },
		   controller: 'ConfirmationForDocparser',
		   size: 'xs',
		   state:$state,//passing this state to the modal
		   windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',//adding css classes to the modal html element
		   resolve: {
			   data: function () {
				   return existData;//sending data to the modal
			   }
		   }
	   });
	   dataModal.result.then(function () {}, function () {});
 };
	/*
		* @purpose: close modal
		* @created: 5 sep 2018
		* @params: none
		* @return: no
		* @author: prasanthi
	*/
	$scope.closeUploadFileModal = function () {
		$uibModalInstance.close();
	};
}