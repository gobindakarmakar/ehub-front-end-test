'use strict';
/*jshint unused:false*/
elementApp.config(['$stateProvider', '$provide', '$qProvider','$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', function($stateProvider, $provide,$qProvider, $urlRouterProvider, EHUB_FE_API, $locationProvider){
	 $provide.factory('$stateProvider', function () {
		 return $stateProvider;
	 });
 	$provide.factory('$urlRouterProvider', function () {
        return $urlRouterProvider;
    });
	
 	 $qProvider.errorOnUnhandledRejections(false);
}]);
