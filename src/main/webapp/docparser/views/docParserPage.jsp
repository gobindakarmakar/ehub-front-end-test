<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>


        <style>
            .p-none {
                pointer-events: none;
            }
        </style>
        <style type="text/css">
            .disableBtn {
                cursor: not-allowed;
                opacity: .5;
                pointer-events: unset
            }

            .pdfViewer .canvasWrapper {
                box-shadow: 0 0 3px #bbb;
            }

            .pdfViewer .page {
                margin-bottom: 10px;
            }

            .annotationLayer {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            }

            #content-wrapper {
                font-size: 1rem;
                max-width: 100%;
                max-height: calc(100% - 15px);
                overflow: auto;
            }

            .pdfViewer .page {
                border-image: none;
            }

            .listPreventEvents {
                pointer-events: none !important;
            }
        </style>
        <div ng-show="runLoader" class="custom-spinner full-page-spinner">
            <i class="fa fa-spinner fa-spin fa-3x"></i>
        </div>
        <div class="custom-spinner full-page-spinner" ng-if="docsLoader">
            <i class="fa fa-spinner fa-spin fa-3x"></i>
        </div>
          <!--- To show popup on Click of cross icon(delete answer functionality) in Details Tab --->
          <div id="" class="overlay sticky-modal z-9999999 bst_modal static-modal-invisible width-100 " ng-class="{ 'static-modal-visible':deleteAnswerModal}">
            <div class="overlay-content small-message-modal fixed-message-box  mar-auto clearfix border-grey fixed-message-box bg-light-grey1">
                <p class="message-wrapper text-center mar-b5">
                    {{deleteAnswerModalMsg}}
                </p>
                <div class="btn-group mar-autox">
                    <div class="btn grad-button sm-btns text-uppercase pad-x10 pad-y5 mar-r5" ng-click="hideDeleteAnswerModal(true);">Yes</div>
                    <div class="btn bordered-button sm-btns pad-x10 pad-y5 border-grey-thin bg-dark-grey radius-0 text-uppercase" ng-click="hideDeleteAnswerModal(false);">Cancel</div>
                </div>
            </div>
        </div>
         <!--- delete fields from structure tab trash icon modal starts --->
        <div id="" class="overlay sticky-modal z-9999999 bst_modal static-modal-invisible width-100" ng-class="{ 'static-modal-visible':deleteStructureModal}">
            <div class="overlay-content small-message-modal fixed-message-box  mar-auto clearfix border-grey fixed-message-box bg-light-grey1">
                <p class="message-wrapper text-center mar-b5">
                    {{deleteStructureModalMsg}}
                </p>
                <div class="btn-group mar-autox">
                    <div class="btn grad-button sm-btns text-uppercase pad-x10 pad-y5 mar-r5"  ng-click="hideDeleteStructureModal(true);">Yes</div>
                    <div class="btn bordered-button sm-btns pad-x10 pad-y5 border-grey-thin bg-dark-grey radius-0 text-uppercase" ng-click="hideDeleteStructureModal(false);">Cancel</div>
                </div>
            </div>
        </div>
        <div id="" class="overlay sticky-modal z-9999999 bst_modal  static-modal-invisible width-100" ng-class="{ 'static-modal-visible':showCancelModal}">
            <div class="overlay-content small-message-modal fixed-message-box  mar-auto clearfix border-grey fixed-message-box bg-light-grey1">
                <p class="message-wrapper text-center mar-b5">
                    {{showCancelModalMsg}}
                </p>
                <div class="btn-group mar-autox">
                    <div class="btn grad-button sm-btns text-uppercase pad-x10 pad-y5 mar-r5" id="yes" ng-click="hidecancelModal(true);">Yes</div>
                    <div class="btn bordered-button sm-btns pad-x10 pad-y5 border-grey-thin bg-dark-grey radius-0 text-uppercase" ng-click="hidecancelModal(false);">Cancel</div>
                </div>
            </div>
        </div>

        <!--- delete question cancel modal starts --->
        <div id="" class="overlay sticky-modal bst_modal static-modal-invisible z-9999999  width-100" ng-class="{'static-modal-visible':showQuestionCancelModal}">
            <div class="overlay-content small-message-modal fixed-message-box  mar-auto clearfix border-grey fixed-message-box bg-light-grey1">
                <p class="message-wrapper text-center mar-b5">
                    {{showQuestionCancelModalMsg}}
                </p>
                <div class="btn-group mar-autox">
                    <div class="btn grad-button sm-btns text-uppercase pad-x10 pad-y5 mar-r5" ng-click="hideQuestionCancelModal(true, deleteElementType);">Yes</div>
                    <div class="btn bordered-button sm-btns pad-x10 pad-y5 border-grey-thin bg-dark-grey radius-0 text-uppercase" ng-click="hideQuestionCancelModal(false);">Cancel</div>
                </div>
            </div>
        </div>
       
        <!--- delete question cancel modal ends --->
        <div class="dashboard-wrapper mar-t0 doc-parser-dashboard doc-parser-details">
            <div id="pdf-dashboard" class="cluster-content-wrapper height-100 p-rel clearfix div-216">
                <div class="side-defined">
                    <div id="chevronLeftDiv" class="panel-group custom-accordion center-row1 ">
                        <div class="panel-default left-heading ng-scope ng-isolate-scope panel" ng-repeat="story in customer_story">
                            <div class="panel-heading pad-l25 border-0" >
                                <h4 class="panel-title" ng-click="selecttool(story.name,story)">
                                    <a class="p-rel">
                                        <span class="d-flex ai-c pad-l5" ng-if="(story.name === 'Heading') || (story.name === 'Paragraph')">
                                            <span class="list-icon-wrapper"> <span class="{{story.icon}} f-18"></span></span>
                                            <span class="list-content">{{ story.name }}</span>
                                        </span>
                                        <span class="d-flex ai-c" ng-if="(story.name === 'Q&A')">
                                            <span class="icon-header list-img-wrapper"><img src="assets/img/q-a.svg" alt=""
                                                    class="img-responsive"></span>
                                            <span class="list-content">{{ story.name }}</span>
                                        </span>
                                        <span class="d-flex ai-c" ng-if="(story.name === 'Form Fields')">
                                            <span class="icon-header list-img-wrapper"><img src="assets/img/general-form.png"
                                                    alt="" class="img-responsive"></span>
                                            <span class="list-content">{{ story.name }}</span>
                                        </span>

                                    </a>
                                </h4>
                            </div>
                        </div>
                        <p class="mar-t25 text-right text-cream d-block f-22 pad-r25 "><span id="chevronLeft" class="c-pointer mar-autor icon icon-arrow-left"></span></p>
                    </div>
                    <div id="chevronRightDiv" class="d-none">
                        <ul class="custom-list item-1 mar-t5 pad-l10" 
                            ng-repeat="story in customer_story">
                            <li class="mar-y15 " ng-if="story.name === 'Heading'" ng-click="selecttool(story.name,story)"><span
                                    class="fa f-22 mar-l5  text-dark-blue fa-header "></span></li>
                            <li class="mar-y15 " ng-if="story.name === 'Paragraph'" ng-click="selecttool(story.name,story)"><span
                                    class="fa f-22 mar-l5 text-dark-blue fa-text-height "></span></li>
                            <li class="mar-t15" ng-if="story.name === 'Q&A'" ng-click="selecttool(story.name,story)"><span
                                    class="icon-header square-40"><img src="assets/img/q-a.svg" alt="" class="img-responsive"></span></li>
                            <li class="mar-t15 " ng-if="story.name === 'Form Fields'" ng-click="selecttool(story.name,story)"><span
                                    class="icon-header square-40 "><img src="assets/img/general-form.png" alt="" class="img-responsive"></span></li>
                        </ul>
                        <span id="chevronRight" class="f-22 mar-l15 mar-t25 c-pointer text-cream icon icon-arrow-right"></span>
                    </div>
                    <!--  Custom Accordion Ends  -->
                </div>
                <div class="side-remain height-100 pull-right">


                    <div class="div-300 height-100 p-rel clearfix">
                      
                        <div class="side-remain bg-transparent">
                            <!-- PDF Annotations starts -->
                            <div class="panel mar-b0 border-0 pad-15px tab-wrapper custom-uib-tab-wrapper bg-transparent  custom-panel-wrapper">

                                <div ng-show="loadDocument" class="custom-spinner z-9 full-page-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>

                                <div id ="pdf-reader"  class="paragraph-wrapper p-rel panel-body pad-0 text-grey-ta">

                                    <div class="toolbar">
                                        <button class="cursor" type="button" title="Cursor" ng-click="cursorNormal('default','none','none','none','none','none');" data-tooltype="cursor">âžš</button>

                                        <div class="spacer d-none"></div>

                                        <button class="rectangle d-none" type="button" title="Rectangle" data-tooltype="area">&nbsp;</button>
                                        <button class="highlight d-none" type="button" title="Highlight" data-tooltype="highlight">&nbsp;</button>
                                        <button class="strikeout d-none" type="button" title="Strikeout" data-tooltype="strikeout">&nbsp;</button>
                                        <button id="pdf-prev" class=" fa fa-angle-double-left" ng-click="loadDocumentSpiner();"></button>
                                        <button id="pdf-next" class=" fa fa-angle-double-right"></button>
                                        <div class="spacer d-none"></div>
                                        <select class="filter-value text-cream pull-right" ng-model="currentPage" id="pageChange" ng-change="onPdfPageChange(checkedValue)">
                                            <option value="{{value}}" ng-repeat="value in arrayPagination track by $index">{{value}}</option>
                                        </select>
                                        <div class="" style="position: absolute;top: 10px;right: 150px;">Displaying {{currentPage}}
                                            of {{arrayPagination.length}} </div>
                                        <button class="text d-none" type="button" title="Text Tool" data-tooltype="text"></button>
                                        <select class="text-size d-none"></select>
                                        <div class="text-color d-none"></div>

                                        <div class="spacer d-none"></div>
                                        <button class="pen d-none" type="button" title="Pen Tool" data-tooltype="draw">âœŽ</button>
                                        <select class="pen-size d-none"></select>
                                        <div class="pen-color d-none"></div>

                                        <div class="spacer d-none"></div>

                                        <button class="comment d-none" type="button" title="Comment" data-tooltype="point">ðŸ—¨</button>

                                        <div class="spacer d-none"></div>
                                        <select class="d-none" id="zoomChange" ng-model="zoomChange" ng-change="onPdfZoomChange(zoomChange)">
                                            <option value=".5">50%</option>
                                            <option value="1">100%</option>
                                            <option selected="selected" value="1.33">133%</option>
                                            <option value="1.5">150%</option>
                                            <option value="2">200%</option>
                                        </select>

                                        <a href="" class="rotate-ccw d-none" title="Rotate Counter Clockwise">âŸ²</a>
                                        <a href="" class="rotate-cw d-none" title="Rotate Clockwise">âŸ³</a>

                                        <div class="spacer d-none"></div>

                                        <a class="d-none" href="javascript://" class="clear" title="Clear">Ã—</a>
                                    </div>
                                    <div id="content-wrapper" class="custom-scroll-wrapper">
                                        <span id="mouse-pointer" style="display:none;" class="z-999 mouse-question-icon mouse-pointerq fa fa-question"></span>
                                        <span id="mouse-pointer" style="display:none;" class="z-999 mouse-img-icon mouse-pointerf "><img
                                                src="assets/img/general-form.png" alt="" class="img-responsive "></span>
                                        <span id="mouse-pointer" style="display:none;" class="z-999 mouse-img-icon mouse-pointera "> <img
                                                src="assets/img/answer-24.svg" class="img-responsive " alt="answer" /></span>
                                                <span id="mouse-pointer" style="display:none;" class="z-999 header-p-icon mouse-question-icon mouse-header-para-icon mouse-pointerh fa fa-header"></span>
                                                <span id="mouse-pointer" style="display:none;" class="z-999 header-p-icon mouse-question-icon mouse-header-para-icon mouse-pointerp fa fa-text-height"></span>

                                        <!--                            <span id="movingArrow"  class="fa fa-hand-paper-o " style="position:fixed;z-index:9999;font-size:30px;display:none;"></span> -->
                                        <div id="viewer" class="pdfViewer"></div>
                                    </div>
                                    <!-- PDF Annotations end -->
                                </div>
                            </div>
                        </div>
                        <div class="side-defined">
                            <div class="panel border-0 tab-wrapper pad-15px custom-uib-tab-wrapper bg-transparent  custom-panel-wrapper">
                                <uib-tabset active="active">
                                    <uib-tab heading="Details">
                                        <div class="panel-body panel-scroll pad-0">
                                            <div class="info-box-wrapper">
                                                <div class="message-box" ng-if="!currentSelection">
                                                    <p>
                                                        <span class="fa fa-info-circle pull-left f-18"></span>
                                                        No element selected.
                                                        <p>Click on any existing element to show more information or add
                                                            a new element by selecting the required tool from the left panel
                                                            and draw the area over the document canvas</p>
                                                    </p>
                                                </div>
                                                <div class="message-box" ng-if="currentSelection == 'Question' && !currentQuestion">
                                                    <p class="pad-l25 p-rel">
                                                        <span class=" p-abs l-0 t-0 fa fa-info-circle pull-left f-18"></span>
                                                       Question tool
                                                       <span class="mar-t10">Select an area to define the question.</span>
                                                    </p>
                                                </div>
                                                <button type="button" class=" hidden bg-transparent btn bordered-button">
                                                    Cancel This Action
                                                </button>
                                            </div>
                                            <div id="detailsCurrentAnswerList" class="question-box-wrapper pad-r10  pad-b25 pad-t15" ng-if="(currentSelection == 'Question' || currentSelection == 'Answer') && currentQuestion">

                                                <div class="question-heading">
                                                    <span class=" c-pointer pull-left  mar-r10 question-icon fa fa-question" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
                                                        popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'"></span>
                                                    <span class="text-dark-red c-pointer f-18 pull-right  mar-x10  fa fa-trash" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
													    popover-animation="true" uib-popover="Delete" popover-trigger="'mouseenter'" ng-click="deleteElement(currentQuestion, 'QA')"></span>

                                                    <h3 class="text-dark-blue text-overflow mxw-220">Question</h3>


                                                </div>
                                                <div class="question-content mar-b10 width-100 pad-t15 p-rel input-group-mat ng-scope" ng-if="currentQuestion">
                                                    <div ng-hide="editorEnabled" class="custom-input pad-l15  border-0">
                                                        <p class="question-name custom-input width-100 mar-t10  pad-0 border-0"  id="docparserQuestionname"> 
                                                            {{ currentQuestionData.elementName}}  
                                                          </p>
                                                          <span class="fa fa-pencil c-pointer p-abs t-20 r-10 text-cream f-14" ng-click="enableEditor()"></span>
                                                    </div>
                                                   
                                                    <div ng-show="editorEnabled" class="custom-input pad-l15  border-0">
                                                    <input type="text" value="Question Name " id= "questionName" class="custom-input width-100 pad-l0 pad-t10 border-0" ng-model="editableTitle" ng-show="editorEnabled">
                                                    <span class="fa fa-check c-pointer p-abs t-20 r-30  text-cream f-14" ng-click="saveQuestionName(editableTitle)"></span>
                                                    <span class="fa fa-times c-pointer p-abs t-20 r-10 text-cream f-14" ng-click="disableEditor()"></span>                                                   
                                                </div>
                                                    <span class="label">Question Name</span>
                                                 
                                                 
                                                </div>

                                                <div class="question-box" ng-if="currentQuestion">

                                                    <p>{{ currentQuestion }}</p>
                                                </div>
                                                <div class="answer-box">


                                                    <div class="answer-heading mar-y10" ng-if="(!currentAnswers || currentAnswers.length==0) && currentQuestion">
                                                        <span class="fa fa-level-up text-cream rotate-90 va-m mar-r5 lh-22 f-18"></span>
                                                        <span class="c-pointer icon-header" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                                            uib-popover="Answer" popover-trigger="'mouseenter'">
                                                            <img src="assets/img/answer-24.svg" class="img-responsive " alt="answer" /></span>

                                                        <h3 class=" d-ib lh-22 mar-b0 text-overflow mxw-220">Answer</h3>
                                                    </div>
                                                    <div class="info-box-wrapper" ng-if="(!currentAnswers || currentAnswers.length==0) && currentQuestion">
                                                        <div class="message-box">
                                                            <p>
                                                                <span class="fa fa-info-circle pull-left f-18"></span>
                                                                Answer Tool</p>
                                                            <p>Select An Area to Define an Answer</p>
                                                            <p>If there are more than one answer - select each one separately</p>

                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="answer-box-group" ng-if="currentAnswers && currentAnswers.length > 0" ng-repeat="ans in currentAnswers">
                                                    <div class="answer-box">
                                                        <div class="answer-heading mar-y10">
                                                            <span class="fa fa-level-up text-cream rotate-90 va-m mar-r5 lh-22 f-18"></span>
                                                            <span class="c-pointer icon-header" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                                                uib-popover="Answer" popover-trigger="'mouseenter'">
                                                                <img src="assets/img/answer-24.svg" class="img-responsive " alt="answer" /></span>

                                                            <h3 class=" d-ib lh-22 mar-b0 text-overflow mxw-220">
                                                                {{ ans.answerType }}</h3> <a ng-click="ShowDeleteAnswerModal($index, ans, 'QA');"
                                                                class="pull-right"><span class="fa fa-times text-cream  rotate-90 va-m mar-r5 lh-22 f-18"></span></a>
                                                        </div>


                                                        <div class="answer-box">

                                                            <div class="select-group mar-y10 text-cream input-group-mat pad-x0 width-100 pad-y10 custom-select-dropdown">
<!--                                                                 <select id="displayByIconOrTable" class="filter-value text-cream custom-input bg-transparent pull-right border-0 width-100 "> -->
<!--                                                                     <option value="CHECKBOX" ng-selected="ans.answerType == 'CHECKBOX'">Checkbox</option> -->
<!--                                                                     <option value="TEXT" ng-selected="ans.answerType == 'TEXT'">Text</option> -->
<!--                                                                     <option value="YES_NO_CHECKBOX" ng-selected="ans.answerType == 'YES_NO_CHECKBOX'">YES/NO -->
<!--                                                                         CHECKBOX -->
<!--                                                                     </option> -->
<!--                                                                 </select> -->
                                                                <select  ng-change="updateAnswerField(ans,selectedAnsVal['ans' +$index])" ng-model="selectedAnsVal['ans' + $index]" class="filter-value text-cream  custom-input bg-transparent pull-right border-0  width-100 ">
																	<option ng-repeat="type in ansTypes" value="{{$index}}" >{{ type=='YES_NO_CHECKBOX'?'YES/NOCHECKBOX':type }}</option>
					
																</select>
                                                                <span class=" label f-10 l-0">Answer-Type</span>
                                                            </div>

                                                            <div class="p-rel mar-y10 answer-content-box">
                                                                <p class="mar-b0 text-cream text-overflow d-ib  mxw-220" ng-if="ans.answerType == 'CHECKBOX'">{{
                                                                    ans.possibleAnswer }}
                                                                </p>
                                                                <p class="mar-b0 text-cream text-overflow d-ib  mxw-220" ng-if="ans.answerType == 'YES_NO_CHECKBOX'">{{
                                                                    ans.possibleAnswer }}
                                                                </p>
                                                                <p class="mar-b0 text-cream text-overflow d-ib  mxw-220" ng-if="ans.answerType == 'TEXT'">{{
                                                                    ans.answer }}</p>
                                                                <input type="text" class="custom-input hidden pad-0 pad-l5 pad-r35 pad-b5  mxw-220  border-blue-thin-b" aria-describedby="basic-addon1">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mar-b10 width-100 pad-t15 p-rel input-group-mat" ng-if="currentAnswers.length > 0 && !isText">
                                                    <a ng-click="addMoreAnswers()" class="question-name text-cream custom-input adding-options-button width-100 pad-l5 border-0">Add
                                                        New answer <span class="pull-right fa fa-plus text-cream"></span></a>

                                                </div>

                                            </div>
                                            <!--  form fields section starts here -->
                                            <div id="detailsFormFieldList" class="question-box-wrapper form-field-wrapper  pad-r10 pad-b25 pad-t15 ng-scope" ng-if="currentSelection == 'Form Fields' ">
                                                <div class="message-box" ng-if="!currentFormFields.length > 0">
                                                    <p class="pad-l25 p-rel">
                                                        <span class=" p-abs l-0 t-0 fa fa-info-circle pull-left f-18"></span>
                                                       Form Field Tool
                                                       <span class="mar-t10">Select a Form Element on the Form.</span>
                                                    </p>
                                                </div>
                                                <div class="question-heading form-heading"  ng-if="currentFormFields.length > 0">
                                                    <span class="icon-header pull-left"><img src="assets/img/general-form.png"
                                                            alt="" class="img-responsive"></span>
                                                    <span class="text-dark-red c-pointer f-18 pull-right  mar-x10  fa fa-trash" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
                                                        popover-animation="true" uib-popover="Delete" popover-trigger="'mouseenter'" ng-click="deleteElement(currentQuestion, 'form')"></span>
                                                    <h3 class="d-ib text-overflow mxw-220">Form Fields</h3>


                                                </div>

                                                <div class="answer-box-group" ng-if="currentFormFields.length > 0" ng-repeat="field in currentFormFields">
                                                    <div class="answer-box">
                                                        <!--                                                     <div class=" text-cream input-group-mat pad-x0 width-100 mar-t10 pad-y10 custom-select-dropdown"> -->
                                                        <!--                                                             <p  class="custom-input text-cream bg-transparent pull-right border-0 lh-30 mar-b0 mar-t5 pad-b0 pad-l5 f-14 width-100  "> -->
                                                        <!--                                                                Lorem, ipsum dolor. -->
                                                        <!--                                                             </p> -->
                                                        <!--                                                             <span class=" label f-10 l-0">Form-Name</span> -->
                                                        <!--                                                         </div> -->
                                                        <div class="answer-heading mar-y10">
                                                            <span class="fa fa-level-up text-cream rotate-90 va-m mar-l5 lh-22 f-18"></span>
                                                            <span class="c-pointer text-center icon-header" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                                                uib-popover="Form Field" popover-trigger="'mouseenter'">
                                                                <i class="  lh-22 f-18 mar-l5 {{formFieldSubTypeIcons[field.formField]}}"></i></span>
                                                            <!-- <span class="icon-header pe-none"><img src="assets/img/general-form.png" alt="" class="img-responsive"></span> -->

                                                            <h3 class=" d-ib lh-22 mar-b0 text-overflow mxw-220">
                                                                {{ field.formField }}</h3> <a ng-click="ShowDeleteAnswerModal($index, field, 'form');"
                                                                class="pull-right"><span class="fa fa-times text-cream pull-right rotate-90 va-m mar-r5 lh-22 f-18"></span></a>
                                                        </div>


                                                        <div class="answer-box">

                                                            <div class="select-group mar-y10 text-cream input-group-mat pad-x0 width-100 pad-y10 custom-select-dropdown">
																<select  ng-change="updateFormField(field,selectedFormVal['form' +$index])" ng-model="selectedFormVal['form' + $index]" class="filter-value text-cream text-capitalize custom-input bg-transparent pull-right border-0  width-100 ">
																	<option ng-repeat="type in formFieldSubTypes" value="{{$index}}" >{{ type }}</option>
					
																</select>
													
																
                                                                <span class=" label f-10 l-0">Sub-type</span>
                                                            </div>

                                                            <div class="p-rel mar-y10 answer-content-box">
                                                                <p class="mar-b0 text-cream text-overflow d-ib  mxw-220" ng-if="field.answerType == 'TEXT'">{{
                                                                    field.answer }}</p>
                                                                <input type="text" class="custom-input hidden pad-0 pad-l5 pad-r35 pad-b5  mxw-220  border-blue-thin-b" aria-describedby="basic-addon1">

                                                            </div>
                                                        </div>
                                                        <button type="button" class=" hidden bg-transparent btn text-uppercase bordered-button">
                                                            Save This Answer
                                                        </button>
                                                    </div>

                                                </div>
                                                <div class="mar-b10 width-100 pad-t15 p-rel input-group-mat" ng-if="currentFormFields.length > 0">
                                                    <a ng-click="addMoreformFields()" class="question-name text-cream custom-input adding-options-button width-100 pad-l5 border-0">Add
                                                        New Form-Field <span class="pull-right fa fa-plus text-cream"></span></a>

                                                </div>

                                            </div>
                                            <!--  form fields section ends here -->
                                            
											<!--  header and paragraph section starts here -->
                                            <div id="detailsHeaderparaList" class="question-box-wrapper header-para-wrapper  form-field-wrapper  pad-r10 pad-b25 pad-t15 ng-scope" ng-if="currentSelection == 'Heading'  || currentSelection == 'Paragraph'">
												<div class="message-box" ng-if="!headingOrParagraph">
                                                    <p class="pad-l25 p-rel">
                                                        <span class=" p-abs l-0 t-0 fa fa-info-circle pull-left f-18"></span>
                                                       {{ currentSelection }} tool
                                                       <span class="mar-t10">Select an area to define the {{ currentSelection }}.</span>
                                                    </p>
                                                </div>
                                                <div class="question-heading form-heading header-para-heading" ng-if="headingOrParagraph">
                                                    <span class="list-icon-wrapper" ng-if="currentSelection =='Heading'"> <span class="fa fa-header left-icon f-18"></span></span>
                                                     <span class="list-icon-wrapper" ng-if="currentSelection =='Paragraph'"> <span class="fa fa-text-height left-icon f-18"></span></span>
                                                    <span class="text-dark-red c-pointer f-18 pull-right  mar-x10  fa fa-trash" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
                                                        popover-animation="true" uib-popover="Delete" popover-trigger="'mouseenter'" ng-click="deleteElement(headingOrParagraph, 'headingOrParagraph')"></span>
                                                    <h3 class="d-ib text-overflow mxw-220">{{ currentSelection }} </h3>
                                                </div>
                                                <div class="answer-box-group" ng-if="headingOrParagraph">
                                                    <div class="answer-box">
                                                        <div class="answer-box">
                                                            <div class="p-rel mar-y10 answer-content-box">
                                                                <p class="mar-b0 text-cream  d-ib  mxw-220" >{{
                                                                    headingOrParagraph.question }}</p>
                                                                <input type="text" class="custom-input hidden pad-0 pad-l5 pad-r35 pad-b5  mxw-220  border-blue-thin-b" aria-describedby="basic-addon1">

                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                </div>
                                                

                                            </div>
                                            <!--  header and paragraph section ends here -->

                                        </div>
                                    </uib-tab>
                                    <uib-tab heading="Structure" >
                                        <div class="panel-body structure-tab-wrapper side-menu-wrapper pad-0 panel-scroll pad-t0" style="position: relative; overflow: visible;">
                                            <div class="hidden doc-progress-details radius-5 pad-15px">
                                                <h3 class="text-dark-grey">Document Progress</h3>
                                                <div class="progress-bar-wrapper pad-r10">
                                                    <div class="list-unstyled progressbar-list">
                                                        <div class="progressbar-list-item one-col">
                                                            <div class="progress width-100 progress-light-gray">
                                                                <div class="progress-bar progress-light-orange" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width:50%">
                                                                    <span class="sr-only">50% Complete</span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-group question-box-wrapper custom-accordion center-row1 pad-r15" id="accordion" role="tablist" aria-multiselectable="true">
                                                <uib-accordion close-others="false">
                                                    <div uib-accordion-group class="right-panel panel-default" is-open="status.isCustomHeaderOpen" ng-repeat="strucData in finalStructuredData" ng-class=" {'nonCollapse':strucData.key !='Text'&& strucData.key !='Checkbox'}" ng-click="stopCollapse(strucData)">
                                                        <uib-accordion-heading>
                                                            <span ng-if="strucData.contentType =='Question' && strucData.key =='Form Fields'" class="icon-header pull-left "><img
                                                                    src="assets/img/general-form.png" alt="" class="img-responsive"></span>
                                                            <span ng-if="strucData.contentType =='Question' && strucData.key !='Form Fields'" class="{{story.icon}} c-pointer pull-left  mar-r10 question-icon fa fa-question"></span>
                                                             <span ng-if="strucData.contentType =='Heading'" class="{{story.icon}} c-pointer pull-left  mar-r10 fa fa-header question-icon mouse-header-para-icon f-18"></span>
                                                             <span ng-if="strucData.contentType =='Paragraph'" class="{{story.icon}} c-pointer pull-left  mar-r10 fa fa-text-height question-icon mouse-header-para-icon f-18"></span>
                                                            <div ng-click="redirectToQuestion(strucData)" class="structure-name width-85 d-ib text-overflow ">{{strucData.questionNo}}</div>
                                                            <i class="text-right glyphicon right-icon" ng-class="{'glyphicon-chevron-up': status.isCustomHeaderOpen, 'glyphicon-chevron-down': !status.isCustomHeaderOpen, 'd-none':strucData.key !='Text'&& strucData.key !='Checkbox'}"></i>
                                                            <span class="text-dark-red c-pointer f-18 pull-right right-icon icon-2 mar-x10  fa fa-trash" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
													    popover-animation="true" uib-popover="Delete" popover-trigger="'mouseenter'" ng-click="showDeleteStructureModal(strucData,k)"></span>
                                                        </uib-accordion-heading>
                                                        <ul class="custom-list item-1 pad-l15">
                                                            <li class="text-cream" ng-repeat="(k,strucSubData) in strucData.subAnswerType track by $index" class="text-cream" ng-click="redirectToAnswer(strucData,k)">
                                                                <span class="fa fa-level-up rotate-90 va-m mar-r5 f-18"></span>
                                                                <span class="{{story.icon}}c-pointer icon-header"> <img ng-if="strucData.questionNo =='Form Fields'"
                                                                        src="assets/img/general-form.png" alt="" class="img-responsive"><img
                                                                        ng-if="strucData.questionNo !='Form Fields'" src="assets/img/answer-24.svg"
                                                                        class="img-responsive " alt="answer" /></span><p class="structure-name width-65 d-ib text-overflow" ng-if="strucData.contentType =='Question' && strucData.key !='Form Fields'">{{strucSubData.key}}</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </uib-accordion>
                                            </div>
                                            <!--  Custom Accordion Ends  -->

                                        </div>


                                    </uib-tab>
                                </uib-tabset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <span id="movingArrow" class="fa fa-arrow-up rotate-90" style="position:absolute;z-index:99;font-size:20px;display:none;"></span>
        <script>
            $(document).ready(function () {
                //startPdfAnnotate();
                $('.div-300 .panel-group').mCustomScrollbar({
                    axis: "y",
                    theme: "minimal-dark"
                });
            });

            $(document).ready(function () {
                sessionStorage.setItem("docStatus", "half");
                $("#chevronLeft").click(function () {
                    $('#movingArrow').css("display", 'none');
                    sessionStorage.setItem("docStatus", "full");
                    $("#chevronLeftDiv").addClass('d-none');
                    $(".cluster-content-wrapper.height-100.clearfix.div-216").addClass("div-60");
                    $("#chevronRightDiv").removeClass('d-none');
                });
                $("#chevronRight").click(function () {
                    $('#movingArrow').css("display", 'none');
                    sessionStorage.setItem("docStatus", "half");
                    $("#chevronRightDiv").addClass('d-none');
                    $(".cluster-content-wrapper.height-100.clearfix.div-216").removeClass("div-60");
                    $("#chevronLeftDiv").removeClass('d-none');
                });
                $('.docparser-buttons-wrapper').mThumbnailScroller({
                    axis: "x",
                    speed: 10
                });
                          
                });  
                $('#pdf-reader').click(function(e) {
                    var offset = $(this).offset();
                    var x = Math.floor(e.pageX - offset.left);
                    var y = Math.floor(e.pageY - offset.top);
                 });
            
            </script>