<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <style>
            .p-none {
                pointer-events: none;
            }
        </style>

        <div class="dashboard-wrapper mar-t0 doc-parser-dashboard doc-parser-landing">
            <div class="custom-spinner full-page-spinner" ng-if="templateLoader">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <div class="custom-spinner full-page-spinner" ng-if="docsLoader">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <div class="upload-document-wrapper">
                <div class="custom-upload-wrapper d-block border-0">
                    <div class="row mar-x0">
                        <div class="col-sm-12">
                            <!-- Segment Panel Starts -->
                            <div ng-if="!templateLoader" class="panel bg-transparent border-0 custom-panel-wrapper mar-0 upload-panel-wrapper">
                                <div class="panel-body pad-15px">
                                    <div class="top-content-wrapper clearfix p-rel">
                                        <div class="input-group bg-transparent border-0 input-group-mat  pull-left custom-input-group">
                                            <span class="input-group-addon left-addon" id="basic-addon1">
                                                <i class="fa fa-search c-pointer"></i>
                                            </span>
                                            <input type="text" title="" placeholder="" required="required" ng-model='templateNameSearch' class="form-control custom-input border-b0" aria-describedby="basic-addon1" ng-change="searchTemplateName(templateNameSearch)">
                                            <span class="label">Search</span>
                                        </div>

                                        <!-- <div class="text-right p-rel" ng-hide="totalDocsCount <= recordsPerPage">
                                            <ul class="upload-pagination-wrapper" uib-pagination total-items="totalDocsCount" items-per-page="recordsPerPage" ng-model="pageNum"
                                                ng-change="pageChanged(pageNum)" max-size="2" class="pagination-sm" boundary-link-numbers="true"></ul>
                                        </div> -->
                                        <div class=" d-ib pagination-new pull-right p-rel" ng-if="loadSavedJson.length > 0">
                                            <span>displaying {{startCountOnPage}}-{{totalCountOnPage}} results of {{totalResults}}</span>
                                            <a class="d-ib fa fa-chevron-left btn btn-link" ng-class="curPage == 1?'cursorPointerNone':'cursorPointer'" ng-disabled="curPage == 1" ng-click="curPage=curPage-1;getTemplates(curPage)"></a>
                                            <a class="d-ib fa fa-chevron-right btn btn-link" ng-class="curPage == numberOfPages()?'cursorPointerNone':'cursorPointer'" ng-disabled="curPage == numberOfPages()" ng-click="curPage = curPage+1;getTemplates(curPage)"></a>
                                        </div>
                                    </div>
                                    <div class="table-data-wrapper mnh-300">
                                        <table id="" class="table scroll-table six-col-docparser border-b0" role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        <span>Template Name</span>
                                                    </th>

                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">
                                                        <span>Parsed Documents</span>
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        <span>Elements</span>
                                                    </th>

                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        <span>Last Modified</span>
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                                        <span> Modified By</span>
                                                    </th>
                                                    <th class="sorting" rowspan="1" colspan="1" aria-label="Id">

                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody class="mxh-350" >

                                                <tr role="row" class="odd" ng-repeat="data in loadSavedJson" ng-mouseover="disablethis($index,data.templateName)">

                                                    <td>
                                                        <p>
                                                            {{data.templateName}}</p>
                                                    </td>

                                                    <td>
                                                        <p>{{data.docsParsed}} </p>
                                                    </td>

                                                    <td>
                                                        <p>{{data.noOfElements}}</p>
                                                    </td>
                                                    <td>
                                                        <p>{{data.modifiedOn | date}}
                                                         
                                                    </td>
                                                    <td>
                                                        <p>{{data.modifiedBy}}</p>
                                                    </td>
                                                    <!-- <td ng-show="$index === selectedRow">
                                                        <span class="fa c-pointer fa-pencil" ng-class="{'c-arrow p-none':userID !=data.modifiedById}" ng-click="navigateToDocParserPage(data);"></span>
                                                        <span class="fa c-pointer fa-remove mar-l25" ng-class="{'c-arrow p-none':userID !=data.modifiedById}"  ng-click="deleteThisTemplate($index,data.id)"></span>
                                                    </td> -->
                                                    <td ng-show="$index === selectedRow">
                                                        <span class="fa c-pointer fa-pencil" ng-click="navigateToDocParserPage(data);"></span>
                                                        <span class="fa c-pointer fa-remove mar-l25" ng-click="deleteThisTemplate($index,data.id)"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="no-data-wrapper text-center" ng-if="!templateLoader && loadSavedJson.length==0">
                                            <span>Data Not Found</span>
                                        </div>                           
                                    </div>

                                </div>
                            </div>
                             <!-- Segment Panel Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>


        </div>
        <style>
        .cursorPointer{
        cursor: pointer;
        }
         .cursorPointerNone{
         pointer-events: none;
        }
        </style>