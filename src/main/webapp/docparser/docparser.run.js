'use strict';

angular.module('docparserApp')
.run(['$rootScope', 'UploadFileService', 'HostPathService','$location', '$state','EHUB_FE_API','$stateProvider', '$urlRouterProvider','$window', function($rootScope, UploadFileService,  HostPathService, $location, $state,EHUB_FE_API,$stateProvider, $urlRouterProvider,$window){
			var userDetails = getAgainTokenBySession();
			$rootScope.ehubObject = userDetails;
			$rootScope.$state = $state;
			 window.appName = 'docparser';
			$rootScope.$on('$stateChangeStart', function(event, toState) {
				$(".Bubble_Chart_tooltip").css("display", "none");
				if(userDetails.fullName === "DataEntry User"){
					if(toState.name == 'dataCuration' || toState.name == 'addDataCuration'){
					}else{
						$location.path('/404');
					}
				}
			});
			
			/*
		     * @purpose: Get Submenu Data
		     * @created: 2 Feb 2018
		     * @params: params
		     * @return: no
		     * @author: Amritesh
		    */
			var url;
			function getSubmenu(){
				if(window.location.hash.indexOf("#!/") < 0 && window.location.hash.indexOf("#") >= 0)
					{url = 'scripts/common/data/submenu.json';}
		   		else if(window.location.hash === ""){
		   			url = '../scripts/common/data/submenu.json';
		   		}else
		   			{url = '../scripts/common/data/submenu.json';}
				
					UploadFileService.getSubmenuData(url).then(function(response){
						if($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)){
							angular.forEach(response.data.dashboarDropDownMenuItems,function(val){
								if(val.menu == "Manage"){
									angular.forEach(val.content,function(v){
										if(v.name == "System Settings" && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Document Parsing"){
											v.disabled ="no";
										}
										if(v.name == "Source Management"  && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
											v.disabled ="yes";
										}
									});
								}
							});
						}
						HostPathService.setdashboardDisableBydomains(response.data.DisableBydomains);						
						HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
						HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
					},function(error){
						HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
					});
			}
			getSubmenu();
			
			$rootScope.$on('$stateChangeSuccess', function(event, toState) {
				if (toState.name == '404' || toState.name == 'domain') {
					$rootScope.topPanelPreview = false;
					if(toState.name == 'domain' && ($rootScope.ehubObject.token === undefined || $rootScope.ehubObject.token === null || $rootScope.ehubObject.token === "")){
						$window.location.href = '';
					}
				} else {
					if($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== "")
						{
							$rootScope.topPanelPreview = true;
							if(toState.name === 'docparser')
							{
								$rootScope.docParserTitle = toState.name.charAt(0).toUpperCase() + toState.name.slice(1);
							}
							else
							{
								$rootScope.docParserTitle = 'Docparser';
							}
						}
					else
						{$window.location.href = '';}
				}
			});
			
			   $stateProvider
				.state('docparser', {
					url: '/landing',
					templateUrl: function(){
						return  EHUB_FE_API + appName + '/views/landing.jsp';
					},
					controller : 'LandingController'
				})
				.state('docParserPage', {
					url: '/docParserPage?onDoc&id&docId&caseId&tId',
					templateUrl: function(){
						return  EHUB_FE_API + appName + '/views/docParserPage.jsp';
					},
					 reloadOnSearch: false,
					controller : 'DocParserPageController'
				})
				.state('404', {
					url : '/404',
					templateUrl : function(){
						return EHUB_FE_API + 'error';
					}
				});
			   $urlRouterProvider.when('', '/landing');
			   $urlRouterProvider.otherwise(function(){
					return '/404';
				});
			  
		}]);