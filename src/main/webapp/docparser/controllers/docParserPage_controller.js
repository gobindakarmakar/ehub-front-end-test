"use strict";

angular.module('docparserApp')
	.controller('DocParserPageController', docParserPageController);
docParserPageController.$inject = [
	'$scope',
	'$state',
	'$http',
	'HostPathService',
	'$window',
	'docParserService',
	'$rootScope',
	'EHUB_API',
	'$uibModal',
	'Flash',
	'$timeout',
	'EHUB_FE_API',
	'TopPanelApiService'
];

function docParserPageController(
	$scope,
	$state,
	$http,
	HostPathService,
	$window,
	docParserService,
	$rootScope,
	EHUB_API,
	$uibModal,
	Flash,
	$timeout,
	EHUB_FE_API,
	TopPanelApiService) {
	$scope.questions = 0;
	$scope.formFields = 0;
	$scope.headers = 0;
	$scope.paragraphs = 0;
	$scope.selectedFormVal = {};
	$scope.selectedAnsVal = {};
	$scope.finalStructuredData = [];
	$scope.questionRename = false;
	var templateArray = [];
	var savedtemplateArray = [];
	var documentId = HostPathService.getParameterByName('docId');//getting document ID
	$scope.showCancelModal = false;
	$scope.currentSelection = "";
	$scope.currentAnswers = [];
	$scope.currentFormFields = [];
	var existing = [];
	window.currentcolor = "orange";//Setting initial color for rectangle box
	var isSavedDoc = false;
	var isEdited = false;
	var templateId = HostPathService.getParameterByName('tId');//getting template ID
	$scope.answersList = [];
	var formsList = [];
	$scope.questionsList = [];
	var detailsTabData = [];
	var headingOrParagraphList = [];
	$scope.ansTypes = ['CHECKBOX', 'TEXT', 'YES_NO_CHECKBOX'];
	$scope.formFieldSubTypeIcons = { //this object is used to bind icons in subtype selectbox for formfield
		'': '',
		'organization': 'fa fa-building',
		'first name': 'fa fa-user',
		'last name': 'fa fa-user',
		'person name': 'fa fa-user',
		'address': 'fa fa-map-marker',
		'city': 'fa fa-globe',
		'Country': 'fa fa-globe',
		'website': 'fa fa-globe',
		'telephone': 'fa fa-phone',
		'fax': 'fa fa-fax',
		'email': 'fa fa-envelope'
	};
	//Used to show items in subtype select box for formfield
	$scope.formFieldSubTypes = ['', 'organization', 'first name', 'last name', 'person name', 'address', 'city', 'Country', 'website', 'telephone', 'fax', 'email'];
	//Showing headings and icons in left side menu
	$scope.customer_story = [{
		'name': 'Heading',
		'subName': [],
		'icon': 'fa fa-header left-icon'

	}, {
		'name': 'Paragraph',
		'subName': [],
		'icon': 'fa fa-text-height left-icon'

	}, {
		'name': 'Q&A',
		'subName': [],
		'icon': 'fa fa-comments-o left-icon'

	}, {
		'name': 'Form Fields',
		'subName': [],
		'icon': 'fa fa-window-restore left-icon'

	}];
	/*
* @purpose: get Cursor and icons
* @created: 7 Sep 2018	
* @author: Prasanthi
*/
	$scope.cursorNormal = function (cursor_d, display_q, display_a, display_f, display_h, display_p) {
		$("#content-wrapper").css("cursor", cursor_d);//cursor : defualt
		$(".mouse-pointerq").css("display", display_q);//cursor changes to question
		$(".mouse-pointera").css("display", display_a);//cursor changes to answer
		$(".mouse-pointerf").css("display", display_f);//cursor changes to formfield
		if ($scope.currentSelection == 'Paragraph' && display_h == 'block') {
			display_p = "block";//cursor changes to paragraph
			display_h = "none";
		}
		$(".mouse-pointerh").css("display", display_h);//cursor changes to heading
		$(".mouse-pointerp").css("display", display_p);
	};





	/*
	 * @purpose: get current tool selection
	 * @created: 7 Sep 2018	
	 * @author: Prasanthi
	*/
	var id = 0;
	$scope.selecttool = function (item, Obj) {
		if (Obj) {
			if (Obj.subName.length == 0) {

				if (item == 'Q&A') {
					id++;
					$scope.currentSelection = 'Question';
					window.currentcolor = "#44BB65";
					$scope.currentAnswers = [];
					$scope.currentQuestionData = undefined;
					$scope.currentQuestion = undefined;
					$(".rectangle").click();//enable click event on rectangle
					$scope.cursorNormal('crosshair', 'block', 'none', 'none', 'none', 'none');//cursor changes to question
					$scope.currentFormFields = [];
				} else if (item == 'Form Fields') {
					$scope.currentSelection = item;
					window.currentcolor = "#fab764";
					$scope.currentAnswers = [];
					$scope.currentFormFields = [];
					$scope.currentQuestionData = undefined;
					$scope.currentQuestion = undefined;
					$(".rectangle").click();//enable click event on rectangle
					$scope.cursorNormal('crosshair', 'none', 'none', 'block', 'none', 'none');//cursor changes to formfield

				} else {
					$scope.currentSelection = item;
					$(".rectangle").click();//enable click event on rectangle
					window.currentcolor = "blue";
					$scope.headingOrParagraph = '';
					$scope.cursorNormal('crosshair', 'none', 'none', 'none', 'block', 'none');//cursor changes to heading
				}
			}
		}

	};

	/*
	 * @purpose: get doc details
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	*/
	$scope.arrayPagination = [];
	var doc;
	function getDocumentDetails(docId, editedDocResponse) {
		handleSubmenuButtons();//enable and disable buttons in submenu
		doc = EHUB_API + 'documentParser/downloadDocumentDocParser?docId=' + docId + "&token=" + $rootScope.ehubObject.token;
		$scope.loadDocument = true;
		window.onFirstTimePageLoad = true;
		existing = [];
		if (editedDocResponse) {
			//To get existing annotations
			getExistingAnnotations(editedDocResponse, doc);
		} else {
			//find 'startPdfAnnotate' function defination in pdf_annotation.js file
			startPdfAnnotate(encodeURI(doc), existing, '', undefined, 1.33);
		}
	}
	/*
	 * @purpose: get existing annotations
	 * @created: 25 Sep 2018	
	 * @author: Prasanthi
	*/
	function getExistingAnnotations(dataTOcheck, doc) {
		existing = [];
		detailsTabData = [];
		$scope.questions = 0;
		$scope.formFields = 0;
		$scope.headers = 0;
		$scope.paragraphs = 0;
		var data = jQuery.extend(true, {}, dataTOcheck);
		angular.forEach(data, function (val) {
			var queColor = "#44BB65";//Default rectangle border color
			if (val.contentData.formField) {
				queColor = "#fab764";//rectangle border color changes to #fab764
				$scope.formFields = $scope.formFields + 1;//Number for selected formfields
			}
			if (val.contentType == "Heading") {
				$scope.headers = $scope.headers + 1;//Number for selected heading
				queColor = "blue";//rectangle border color changes to blue
			}
			if (val.contentType == "Paragraph") {
				$scope.paragraphs = $scope.paragraphs + 1;//Number for selected paragraphs
				queColor = "blue";//rectangle border color changes to blue
			}
			if (queColor === "#44BB65" && val.contentType == 'Question') {
				$scope.questions = $scope.questions + 1;//Number for selected questions
			}
			if (val.contentType != "SUB_PARAGRAPH") {
				var questionRect = { //Will have selected question rectangle details
					"type": "area",
					"x": val.contentData.questionPosition.split(",")[0],
					"y": val.contentData.questionPosition.split(",")[1],
					"width": val.contentData.questionPosition.split(",")[2],
					"height": val.contentData.questionPosition.split(",")[3],
					"positionOnThePage": val.contentData.questionPositionOnThePage,
					"class": "Annotation",
					"uuid": val.contentData.questionUUID,
					"page": val.contentData.pageNumber,
					"color": queColor,
					"documentId": EHUB_API + 'documentParser/downloadDocumentDocParser?docId=' + documentId + "&token=" + $rootScope.ehubObject.token
				};
				existing.push(questionRect);
				if (!val.contentData.formField && val.contentType != "Heading" && val.contentType != "Paragraph") {// no answer rect for form field,headings and paragraphs
					var answerRect = { //Will have selected answer rectangle details
						"type": "area",
						"class": "Annotation",
						"color": "yellow",
						"page": val.contentData.pageNumber,
						"documentId": EHUB_API + 'documentParser/downloadDocumentDocParser?docId=' + documentId + "&token=" + $rootScope.ehubObject.token
					};
					if (val.contentData.answerType != 'TEXT' && val.contentData.possibleAnswersDto) {
						angular.forEach(val.contentData.possibleAnswersDto, function (v1) {
							var currentAnsRect = jQuery.extend(true, {}, answerRect);
							if (v1.answerPosition) {
								currentAnsRect.x = v1.answerPosition.split(",")[0];
								currentAnsRect.y = v1.answerPosition.split(",")[1];
								currentAnsRect.width = v1.answerPosition.split(",")[2];
								currentAnsRect.height = v1.answerPosition.split(",")[3];
							}
							currentAnsRect.uuid = v1.answerUUID;
							currentAnsRect.positionOnThePage = v1.answerPositionOnThePage;
							existing.push(currentAnsRect);
						});
					} else if (val.contentData.answerPosition) {
						answerRect.x = val.contentData.answerPosition.split(",")[0];
						answerRect.y = val.contentData.answerPosition.split(",")[1];
						answerRect.width = val.contentData.answerPosition.split(",")[2];
						answerRect.height = val.contentData.answerPosition.split(",")[3];
						answerRect.uuid = val.contentData.answerUUID;
						answerRect.positionOnThePage = val.contentData.answerPositionOnThePage;
						existing.push(answerRect);
					}
				}
			}
			//holding complete data of details Tab
			detailsTabData.push({
				"contentType": val.contentType,
				"question": val.contentData.question,
				"questionCoordinates": val.contentData.questionPosition,
				"answer": val.contentData.answer,
				"possibleAnswer": val.contentData.possibleAnswer,
				"possibleAnswersDto": val.contentData.possibleAnswersDto ? val.contentData.possibleAnswersDto : null,
				"answerCoordinates": val.contentData.answerPosition,
				"answerType": val.contentData.answerType,
				"pageNumber": val.contentData.pageNumber,
				"docId": documentId,
				"formField": val.contentData.formField,
				"subType": val.contentData.subType,
				"elementName": val.contentData.elementName,
				"questionPositionOnThePage": val.contentData.questionPositionOnThePage,
				"answerPositionOnThePage": val.contentData.answerPositionOnThePage,
				"icon": null,
				"questionUUID": val.contentData.questionUUID,
				"answerUUID": val.contentData.answerUUID
			});
		});
		window.annotationsFoCancle = jQuery.extend(true, [], existing);//for cancle
		if (existing.length > 0) {
			previousAnnotationsList = existing;
			annotationsList = existing;
			detailsTabData.map(function (d) {
				if (d.contentType == "Question" && !d.formField) {
					var data1 = jQuery.extend(true, {}, d);
					if (d.questionCoordinates) {
						data1.x = d.questionCoordinates.split(",")[0];
						data1.y = d.questionCoordinates.split(",")[1];
					}
					$scope.questionsList.push(data1);
					if (d.answerType == "TEXT") {
						var data2 = jQuery.extend(true, {}, d);
						if (d.answerCoordinates) {
							data2.x = d.answerCoordinates.split(",")[0];
							data2.y = d.answerCoordinates.split(",")[1];
						}
						$scope.answersList.push(data2);
					}
					if (d.answerType == "CHECKBOX" && d.possibleAnswersDto && d.possibleAnswersDto.length > 0) {
						angular.forEach(d.possibleAnswersDto, function (v) {
							var data2 = jQuery.extend(true, {}, d);
							data2.answer = v.possibleAnswer;
							data2.possibleAnswer = v.possibleAnswer;
							data2.answerUUID = v.answerUUID;
							if (v.answerPosition) {
								data2.answerCoordinates = v.answerPosition;
								data2.x = v.answerPosition.split(",")[0];
								data2.y = v.answerPosition.split(",")[1];
							}
							$scope.answersList.push(data2);
						});
					}
				}
				if (d.formField) {
					d.contentType = "Form Fields";
					if (d.questionCoordinates) {
						d.x = d.questionCoordinates.split(",")[0];
						d.y = d.questionCoordinates.split(",")[1];
					}
					formsList.push(d);
				}
				if (d.contentType == "Paragraph" || d.contentType == "Heading") {
					if (d.questionCoordinates) {
						d.x = d.questionCoordinates.split(",")[0];
						d.y = d.questionCoordinates.split(",")[1];
					}
					headingOrParagraphList.push(d);
				}
			});
		}
		if (doc) {
			//find 'startPdfAnnotate' function defination in pdf_annotation.js file
			startPdfAnnotate(encodeURI(doc), existing, '', undefined, 1.33);
		}
	}

	/*
	 * @purpose: function to handle submenu buttons
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	*/
	function handleSubmenuButtons() {
		//disable all buttons
		$("#docParserSave").addClass("pe-none");
		$("#docParserSaveDiv").addClass("c-ban");
		$("#docParserCancle").addClass("pe-none");
		$("#docParserCancleDiv").addClass("c-ban");
		$("#docParserRun").addClass("pe-none");
		$("#docParserRunDiv").addClass("c-ban");
		$("#docParserPreview").addClass("pe-none");
		$("#docParserPreviewDiv").addClass("c-ban");
		$('.fa-pencil').addClass("pe-none");
		$('.fa-pencil').parent().addClass("c-ban");
		if (HostPathService.getParameterByName('docId') && isEdited) {
			//enable save
			$("#docParserSave").removeClass("pe-none");
			$("#docParserSaveDiv").removeClass("c-ban");
		}
		if (isEdited) {
			//enable cancel
			$("#docParserCancle").removeClass("pe-none");
			$("#docParserCancleDiv").removeClass("c-ban");
		}
		if (isSavedDoc && HostPathService.getParameterByName('onDoc')) {
			//enable run
			$("#docParserRun").removeClass("pe-none");
			$("#docParserRunDiv").removeClass("c-ban");
		}
		if (isSavedDoc || templateId) {
			//enable preview
			$("#docParserPreview").removeClass("pe-none");
			$("#docParserPreviewDiv").removeClass("c-ban");
		}
		if (templateId) {
			//enable edit mode
			$('.fa-pencil').removeClass("pe-none");
			$('.fa-pencil').parent().removeClass("c-ban");
			$("#docParserSave").html("update");
		}
	}

	/*
	 * @purpose: Toggle function for the show and hide of list
	 * @created: 13 Aug 2018	
	 * @author: Asheesh
	*/
	$scope.listClass = "list-hide";
	$scope.changeClass = function () {
		if ($scope.listClass === "list-hide") {
			$scope.listClass = "list-show";
		} else {
			$scope.listClass = "list-hide";
		}
	};
	/*
	 * @purpose: Compare difference of two array of objects
	 * @created: 19th Sep 2018	
	 * @author: swathi
	*/
	function comparer(otherArray) {
		return function (current) {
			return otherArray.filter(function (other) {
				return other.x == current.x && other.y == current.y;
			}).length == 0;
		};
	}
	/*
	 * @purpose:Getting x and y coordinates for selected text from pdf file
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	 * @modified by: swathi
	*/
	var previousAnnotationsList = [], annotationsList = [];
	$window.getAnnotationValue = function (annotationForController, type) {
		if (!isDrag) { //checking if it is not dragable rectangle
			annotationsList = annotationForController;
			//enable and disable buttons in submenu
			handleSubmenuButtons();
			if (annotationForController && annotationForController.length > 0) {
				isEdited = true;
				//enable and disable buttons in submenu
				handleSubmenuButtons();
				if (type == "delete") {
					var onlyInA = annotationForController.filter(comparer(previousAnnotationsList));
					var onlyInB = previousAnnotationsList.filter(comparer(annotationForController));
					var result = onlyInA.concat(onlyInB);
					var currentAns = $scope.currentAnswers;
					var currentFormAns = $scope.currentFormFields;
					var isCurrentQuestionDeleted = false;

					$scope.finalStructuredData.map(function (v, k) {
						result.map(function (value) {
							if (v.positionOnPage && value.positionOnThePage) {
								if (v.positionOnPage == JSON.stringify(value.positionOnThePage)) {
									$scope.finalStructuredData.splice(k, 1);
								}
							}
						});
					});
					angular.forEach(result, function (v) {
						if (v.color == "#44BB65") {
							isCurrentQuestionDeleted = true;
						}
						if ($scope.currentAnswers && $scope.currentAnswers.length > 0) {
							if ($scope.currentQuestionData && $scope.currentAnswers.length == 1) {
								angular.forEach(annotationForController, function (d) {
									if ($scope.currentQuestionData.x == d.x && $scope.currentQuestionData.y == d.y) {
										//getting all plotted rectangles into an array
										var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
										if (arr.length > 0) {
											angular.forEach(arr, function (v) {
												//Checking uuid with plotted rectangle uuid
												if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
													var parent = v.parentNode;
													parent.removeChild(v);
													currentAns = [];
												}
											});
										}
									} else {
										isCurrentQuestionDeleted = true;
									}
								});
							}
							$scope.currentAnswers.map(function (d, i) {
								if (d.x == v.x && d.y == v.y) {
									currentAns.splice(i, 1);
									if ($scope.answersList.length > 0) {
										$scope.answersList.map(function (a, b) {
											if (a.answerUUID == d.answerUUID) {
												$scope.answersList.splice(b, 1);
											}
										});
									}
									templateArray.map(function (val, key) {
										if (val.contentData && val.contentData.possibleAnswersDto) {
											val.contentData.possibleAnswersDto.map(function (v1, k1) {
												if (v1.answerUUID == d.answerUUID) {
													templateArray[key].contentData.possibleAnswersDto.splice(k1, 1);
												}
											});
											if (val.contentData.possibleAnswersDto.length == 0) {
												templateArray.splice(key, 1);
											}
										} else {
											if (val.contentData.answerUUID == d.answerUUID) {
												templateArray.splice(key, 1);
											}
										}
									});
								}
							});
							$timeout(function () {
								$scope.currentAnswers = currentAns;
							}, 0);
						}
						if ($scope.currentQuestionData) { //holds all questions which are plotted in pdf file
							if (templateArray.length > 0) {
								templateArray.map(function (val, key) {
									if (val.contentData && val.contentData.possibleAnswersDto) {
										val.contentData.possibleAnswersDto.map(function (v1, k1) {
											if (val.contentData.questionUUID == v.uuid) {
												templateArray[key].contentData.possibleAnswersDto.splice(k1, 1);
											}
										});
										if (val.contentData.possibleAnswersDto.length == 0) {
											templateArray.splice(key, 1);
										}
									} else {
										if (val.contentData && val.contentData.questionUUID == v.uuid) {
											templateArray.splice(key, 1);
										}
									}
								});
							}
						}
						if ($scope.currentFormFields.length > 0) { //holds all FormFields which are plotted in pdf file
							$scope.currentFormFields.map(function (d, i) {
								if (d.x == v.x && d.y == v.y) {
									currentFormAns.splice(i, 1);
									setTimeout(function () {
										$scope.currentFormFields.splice(i, 1);
										$scope.$apply();
									}, 0);

									templateArray.map(function (val, key) {
										if (val.contentData && val.contentData.answerPosition == d.answerCoordinates) {
											templateArray.splice(key, 1);
										}
									});
								}
							});
						}
						if ($scope.headingOrParagraph) { //holds all heading Or Paragraph which are plotted in pdf file
							if ($scope.headingOrParagraph.x == v.x && $scope.headingOrParagraph.y == v.y) {
								var headParaData = $scope.headingOrParagraph;
								setTimeout(function () {
									$scope.headingOrParagraph = "";
									//arranging data for structure tab
									arrangeStructureData();
									$scope.$apply();
								}, 0);
								$scope.deleteQuestion();
								templateArray.map(function (val, key) {
									if (val.contentData && val.contentData.questionPosition == headParaData.questionCoordinates) {
										templateArray.splice(key, 1);
										//arranging data for structure tab
										arrangeStructureData();
									}
								});
							}
						}
					});

					previousAnnotationsList = jQuery.extend(true, [], annotationForController);
					if (templateArray.length == 0) {
						document.querySelector('div#pageContainer' + (1) + ' svg.annotationLayer').innerHTML = '';
						$scope.deleteQuestion();
						$scope.currentAnswers = [];
						$scope.currentFormFields = [];
						$scope.headingOrParagraph = "";
						$scope.finalStructuredData = [];
					} else {
						if (isCurrentQuestionDeleted && $scope.currentAnswers && $scope.currentAnswers.length > 0) {
							angular.forEach(annotationForController, function (v) {
								$scope.currentAnswers.map(function (d) {
									if (d.x == v.x && d.y == v.y) {
										//getting all plotted rectangles into an array
										var arr = document.querySelectorAll('div#pageContainer' + (d.pageNumber) + ' svg.annotationLayer rect');
										if (arr.length > 0) {
											angular.forEach(arr, function (v) {
												//checking uuid for answer type rectangles
												if (d.answerUUID == v.getAttribute('data-pdf-annotate-id')) {
													var parent = v.parentNode;
													parent.removeChild(v);
												}
											});
										}
									}
								});
							});
							currentAns = [];
						}

						if (currentAns && currentAns.length == 0 && result && result.length > 0 && result[0].color != "#fab764" && result[0].color != "blue") {
							$scope.deleteQuestion();
							setTimeout(function () {
								$scope.currentAnswers = [];
								$scope.$apply();
							}, 0);
						}
						if (currentFormAns.length == 0 && result && result.length > 0 && result[0].color == "#fab764") {
							$scope.deleteQuestion();
							setTimeout(function () {
								$scope.currentFormFields = [];
								$scope.$apply();
							}, 0);
						}
					}
				} else {
					previousAnnotationsList = jQuery.extend(true, [], annotationForController);
					//getting selected area content from the API 
					getContentFromAPI(annotationForController[annotationForController.length - 1], $scope.currentSelection);
				}
			} else {
				isEdited = false;
				if (type == "delete") {
					$scope.deleteQuestion();
					$scope.currentAnswers = [];
					$scope.currentFormFields = [];
					$scope.headingOrParagraph = "";
					$scope.finalStructuredData = [];
					templateArray = [];
					//arranging data for structure tab
					arrangeStructureData();
				}
			}
		}
	};
	/*
	 * @purpose:document ready function
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	*/
	$(document).ready(function () {
		if (!documentId) {
			documentId = HostPathService.getParameterByName('docId');//documnet ID
		}
		if (!templateId) {
			templateId = HostPathService.getParameterByName('tId');//Template ID

		}
		//show doc name in doc parser
		if (documentId) {
			$timeout(function () {
				$(".cursor").click();//enable click event on rectangle
			});

			$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
			if (!templateId) {
				getDocumentDetails(documentId);
				$timeout(function () {
					$(".cursor").click();//enable click event on rectangle
				});
				$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
			}
			//get document details
			getDocDetails(documentId);

		}
		// get template data if we have it
		if (templateId) {
			getTemplateDetails(templateId);
			$("#docParserSave").html("update");
		}
		//clicking on edit button to edit bread crumb.
		$(".fa-pencil").on("click", function () {
			$('#templateName').val($scope.templateName);
		});
		$scope.myFunct = function (keyEvent, updateName) {
			if (keyEvent.which === 13) { //update template name by clicking enter button
				$timeout(function () {
					$('#docparserTemplatename').html(updateName);
				});
				var edittedTemplateData = {
					"id": templateId,
					"templateName": updateName
				};
				TopPanelApiService.docparserEditRenameTemplate(edittedTemplateData).then(
					function () {
					},
					function () {

					});
			}
		};
		//to update the editted template name on breadcrumb
		$scope.changedDocument = function (updateName, check) {
			if (check == 'change') { //if template name changed
				$timeout(function () {
					$('#docparserTemplatename').html(updateName);//updated template with new name
				});
				var edittedTemplateData = {
					"id": templateId,
					"templateName": updateName
				};
				TopPanelApiService.docparserEditRenameTemplate(edittedTemplateData).then(
					function () {
					},
					function () {

					});
			} else if (check == 'dontChange') { //if template name not changed
				$timeout(function () {
					$('#docparserTemplatename').html($scope.templateName);//original template name
				});
			}

		};
		//enable and disable buttons in submenu
		handleSubmenuButtons();
		//show pop when clicking on bread crumb to go back to landing page
		$("#docparserLanding").on("click", function () {
			if (isEdited && !isSavedDoc) {
				$timeout(function () {
					$scope.navigateToLanding = true;
					$scope.showCancelModal = true;
					$scope.showCancelModalMsg = "Are you sure you want to discard the changes in the current template?";
				}, 0);

			} else {
				$state.go("docparser");//go to landing page
			}
		});
		/*
		 * @purpose:click on sub menu buttons
		 * @created: 13 Sep 2018	
		 * @author: Prasanthi
		*/
		$(".docParserbtns").on("click", function () {
			var id = $(this).attr("id");
			if (id == "docParserPreview") {

			} else if (id == "docParserRun") {
				if (documentId) { runTemplate(documentId); }
			} else if (id == "docParserCancle") {
				$timeout(function () {
					$scope.navigateToLanding = false;
					$scope.showCancelModal = true;
					$scope.showCancelModalMsg = "Are you sure you want to clear annotations?";
				}, 0);
			} else if (id == "docParserSave") {
				if ($("#docParserSave").html() && $("#docParserSave").html().toLowerCase() == "update") {
					//save template with updated data
					saveTemplate("update");
				} else {
					//save template
					saveTemplate();
				}

			}
		});
	});

	$scope.hidecancelModal = function (condition) {
		if (condition) {
			if ($scope.navigateToLanding) {
				$state.go("docparser");//go to landing page
			}
			isEdited = false;
			//disable cancle button
			$("#docParserCancle").addClass("pe-none");
			$("#docParserCancleDiv").addClass("c-ban");

		}
		$scope.showCancelModal = false;
	};
	/*
	 * @purpose: Appending data to selectBox
	 * @created: 8 Sep 2018	
	 * @author: Varsha
	*/
	var pageArray = [];
	var pageHeight;
	$scope.checkedValue = 'select';
	var pageHeight;
	window.setPaginationForPdf = function (page, page_height) {
		pageArray = [];
		for (var i = 1; i <= page; i++) {
			pageArray.push(i);
		}
		$scope.$apply(function () {
			$scope.loadDocument = false;
			$timeout(function () {
				$(".cursor").click();//enable click event on rectangle
			});
			$scope.arrayPagination = pageArray;

			$timeout(function () {
				$(".cursor").click();//enable click event on rectangle
			});
			$scope.currentPage = '1';

		});
		pageHeight = page_height;
	};
	/*
	* @purpose: Displaying current page out of total page.
	* @created: 8 Sep 2018	
	* @author: Varsha
	*/
	document.getElementById('content-wrapper').addEventListener('scroll', function (e) {
		$('#movingArrow').css("display", 'none');
		$scope.$apply(function () {
			$scope.currentPage = Math.round(e.target.scrollTop / (pageHeight + 10)) + 1;
			$scope.currentPage = $scope.currentPage.toString();
			$timeout(function () {
				//update current annotation color based on current selection
				updateCurrentSelectionColor();
			});
		});
	});
	/*
	 * @purpose: update current annotation color based on current selection
	 * @created: 9 oct 2018	
	 * @author: Prasanthi
	*/
	function updateCurrentSelectionColor() {
		if ($scope.currentSelection == 'Question') {
			window.currentcolor = "#44BB65"; //Question rectangle border color is #44BB65
		} else if ($scope.currentSelection == 'Answer') {
			window.currentcolor = "yellow"; //Answer rectangle border color is yellow
		} else if ($scope.currentSelection == 'Form Fields') {
			window.currentcolor = "#fab764"; //Form Fields rectangle border color is #fab764
		} else {
			window.currentcolor = "blue";//Heading and Paragraph rectangle border color is blue
		}
	}
	/*
	 * @purpose:click on sub menu buttons
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	*/
	function getContentFromAPI(currentAnnotate, currentSelection) {
		$scope.currentSelection = currentSelection; //holds selected heading from left side menu
		$scope.runLoader = true;
		//coordinartes of selected rectangle
		var coOrdinates = parseInt(currentAnnotate.x) + "," + parseInt(currentAnnotate.y) + "," + parseInt(currentAnnotate.width) + "," + parseInt(currentAnnotate.height);
		var data = {
			"answer": "",
			"answerCoordinates": "",
			"answerType": "",
			"docId": documentId,
			"pageNumber": currentAnnotate.page,
			"possibleAnswer": "",
			"question": "",
			"questionCoordinates": coOrdinates,
			"contentType": "Question",
			"questionPositionOnThePage": JSON.stringify(currentAnnotate.positionOnThePage)
		};
		if ($scope.currentSelection == "Form Fields") {
			data.contentType = "Form Fields";
		}
		if ($scope.currentSelection == "Answer") {
			data.questionCoordinates = "";
			data.answerCoordinates = coOrdinates;
			data.answerPositionOnThePage = JSON.stringify(currentAnnotate.positionOnThePage);
		}
		docParserService.getQuestionAndAnswerNewTemplate(data).then(
			function (response) {
				if (isDrag) { //if the rectangle is dragged
					isDrag = false;
				}
				$scope.runLoader = false;
				if (!response.data) {
					if ($scope.currentSelection == "Question") {
						$scope.currentQuestion = '';
					}
				} else {
					if ($scope.currentSelection == "Question" && response.data.question) { //checking for question
						var resData1 = response.data;
						$scope.questions = $scope.questions + 1;
						resData1.x = currentAnnotate.x;
						resData1.y = currentAnnotate.y;
						resData1.uuid = currentAnnotate.uuid;
						resData1.questionUUID = currentAnnotate.uuid;
						resData1.elementName = "Some Question" + $scope.questions;
						$scope.currentQuestionData = resData1;
						$scope.currentQuestion = resData1.question;

						$scope.questionsList.push(resData1);
						$scope.currentSelection = 'Answer';
						window.currentcolor = "yellow";
						$scope.cursorNormal('crosshair', 'none', 'block', 'none', 'none', 'none');//cursor changes to answer
						if (isSetDrag) {
							//update the data if it is draggable rectangle 
							updatedDraggableRectValues(templateArray, resData1, window.dragedAnnotation); isSetDrag = false; 
						}
					} else if ($scope.currentSelection == 'Answer') { //checking for answer
						var resData = response.data;
						resData.x = currentAnnotate.x;
						resData.y = currentAnnotate.y;
						resData.uuid = currentAnnotate.uuid;
						resData.answerUUID = currentAnnotate.uuid;
						resData.questionUUID = $scope.currentQuestionData.uuid;

						$scope.currentAnswers.push(resData);
						angular.forEach($scope.currentAnswers, function (val, k) {
							var currentSelectedIndex = $scope.ansTypes.indexOf(val.answerType);
							if (currentSelectedIndex == -1) {
								currentSelectedIndex = 0;
							}
							$scope.selectedAnsVal['ans' + k] = currentSelectedIndex.toString();
						});
						$scope.answersList.push(resData);
						if (!isSetDrag) {
							//saving new data for question and answer
							saveQuestionAnswersForTemplate('', '', resData);
						}
						else { 
							//update the data if it is draggable rectangle
							updatedDraggableRectValues(templateArray, resData, window.dragedAnnotation); isSetDrag = false; 
						}

					} else if ($scope.currentSelection == 'Form Fields') { //checking for form field
						$scope.formFields = $scope.formFields + 1;//count number for formfield
						var resData = response.data;
						resData.elementName = "Some Form Field" + $scope.formFields;
						resData.x = currentAnnotate.x;
						resData.y = currentAnnotate.y;
						resData.uuid = currentAnnotate.uuid;
						resData.questionUUID = currentAnnotate.uuid;
						$scope.currentFormFields.push(resData);
						angular.forEach($scope.currentFormFields, function (val, k) {
							var currentSelectedIndex = $scope.formFieldSubTypes.indexOf(val.formField.toLowerCase());
							if (currentSelectedIndex == -1) {
								currentSelectedIndex = 0;
							}
							$scope.selectedFormVal['form' + k] = currentSelectedIndex.toString();
						});
						formsList.push(resData);

						if (!isSetDrag) {
							//saving new data for form fields
							saveQuestionAnswersForTemplate('Form Fields'); }
						else { 
							//update the data if it is draggable rectangle
							updatedDraggableRectValues(templateArray, resData, window.dragedAnnotation); isSetDrag = false; }
					} else {
						var resData = response.data;
						if ($scope.currentSelection == 'Heading') { //checking for heading
							$scope.headers = $scope.headers + 1;//count number for heading
							if (!isSetDrag) { resData.elementName = "Some Heading " + $scope.headers; }
						} else { //checking for paragraph
							$scope.paragraphs = $scope.paragraphs + 1; //count number for paragraph
							if (!isSetDrag) { resData.elementName = "Some Paragraph " + $scope.paragraphs; }
						}
						resData.x = currentAnnotate.x;
						resData.y = currentAnnotate.y;
						resData.uuid = currentAnnotate.uuid;
						resData.questionUUID = currentAnnotate.uuid;
						resData.contentType = $scope.currentSelection;
						$scope.headingOrParagraph = resData;
						headingOrParagraphList.push(resData);
						if (!isSetDrag) { headingOrParagraphList.push(resData); 
							//saving new data for headingOrpara
							saveQuestionAnswersForTemplate("", 'headingOrpara'); }
						else {
							//update the data if it is draggable rectangle
							updatedDraggableRectValues(templateArray, resData, window.dragedAnnotation); isSetDrag = false;
						}
					}
				}

			},
			function (e) {
				$scope.runLoader = false;
				if ($scope.currentSelection == "Question") {
					$scope.currentQuestion = '';
					$timeout(function () {
						$(".cursor").click();//enable click event on rectangle
					});
				}
				if ($scope.currentSelection == 'Form Fields') {
					HostPathService.FlashErrorMessage(e.responseMessage, '');//show error msg for wrong form field
				}
			});

		if ($scope.currentAnswers.length > 0 || ($scope.currentFormFields && $scope.currentFormFields.length > 0)) {
			var id;
			if ($scope.currentSelection == 'Form Fields') {
				id = "#detailsFormFieldList";
			} else {
				id = "#detailsCurrentAnswerList";
			}
			$(id).mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
			//$('#isoTable').css('height', '500px');
		}
	}
	/*
	 * @purpose:Update data for dragged rectangle
	 * @created: 29 Sep 2018
	 * @author: Karunakar
	*/
	function updatedDraggableRectValues(templateArray, resData, dragedAnnotation) {
		//disable save button
		$("#docParserSave").removeClass("pe-none");
		$("#docParserSaveDiv").removeClass("c-ban");
		angular.forEach(templateArray, function (val) {
			if (val.contentType && val.contentType == "Question") { //checking for question
				if ((val.contentData.questionUUID && val.contentData.questionUUID === resData.uuid && val.contentData.formField === "")) {
					val.contentData.question = resData.question;//updating question content
					val.contentData.questionPositionOnThePage = dragedAnnotation.positionOnThePage;
					if (resData.questionCoordinates != null) {
						//updating question coordinates
						val.contentData.questionPosition = parseInt(resData.questionCoordinates.split(",")[0]) + "," + parseInt(resData.questionCoordinates.split(",")[1]) + "," + parseInt(resData.questionCoordinates.split(",")[2]) + "," + parseInt(resData.questionCoordinates.split(",")[3]);
					}
				}
				else if (val.contentData.answerUUID && val.contentData.answerUUID === resData.uuid && val.contentData.formField === "") {
					val.contentData.answer = resData.possibleAnswer; //updating answer content
					val.contentData.answerPositionOnThePage = dragedAnnotation.positionOnThePage;
					if (resData.answerCoordinates != null) {
						//updating answer coordinates
						val.contentData.answerPosition = parseInt(resData.answerCoordinates.split(",")[0]) + "," + parseInt(resData.answerCoordinates.split(",")[1]) + "," + parseInt(resData.answerCoordinates.split(",")[2]) + "," + parseInt(resData.answerCoordinates.split(",")[3]);
					}
				}
				else if (val.contentData.formField !== "" && val.contentData.questionUUID === resData.uuid) {
					val.contentData.question = resData.question;//updating formfiled content
					val.contentData.questionPositionOnThePage = dragedAnnotation.positionOnThePage;
					if (resData.questionCoordinates != null) {
						//updating form field coordinates
						val.contentData.questionPosition = parseInt(resData.questionCoordinates.split(",")[0]) + "," + parseInt(resData.questionCoordinates.split(",")[1]) + "," + parseInt(resData.questionCoordinates.split(",")[2]) + "," + parseInt(resData.questionCoordinates.split(",")[3]);
					}
				}
				else {
					//checking yes or no type answer content
					angular.forEach(val.contentData.possibleAnswersDto, function (v) {
						if (v.answerUUID === resData.uuid) {
							//updating answer to 'Yes'
							if (v.possibleAnswer === "Yes") { v.possibleAnswer = "Yes"; }
							//updating answer to 'No'
							else if (v.possibleAnswer === "No") { v.possibleAnswer = "No"; }
							else { v.possibleAnswer = resData.possibleAnswer; }
							v.answerPositionOnThePage = dragedAnnotation.positionOnThePage;
							if (v.answerPosition != null) {
								//updating answer coordinates
								v.answerPosition = parseInt(resData.answerCoordinates.split(",")[0]) + "," + parseInt(resData.answerCoordinates.split(",")[1]) + "," + parseInt(resData.answerCoordinates.split(",")[2]) + "," + parseInt(resData.answerCoordinates.split(",")[3]);
							}
						}
					});
				}
			}
			else if (val.contentType && val.contentType === "Heading") { //checking for heading
				if (val.contentData.questionUUID == resData.uuid) {
					val.contentData.question = resData.question;//updating heading content
					val.contentData.questionPositionOnThePage = dragedAnnotation.positionOnThePage;
					if (resData.questionCoordinates != null) {
						//updating heading coordinates
						val.contentData.questionPosition = parseInt(resData.questionCoordinates.split(",")[0]) + "," + parseInt(resData.questionCoordinates.split(",")[1]) + "," + parseInt(resData.questionCoordinates.split(",")[2]) + "," + parseInt(resData.questionCoordinates.split(",")[3]);
					}
				}
			}
			else {
				if (val.contentData.questionUUID === resData.uuid) { //checking for paragraph
					val.contentData.question = resData.question;//updating paragraph content
					val.contentData.questionPositionOnThePage = dragedAnnotation.positionOnThePage;
					if (resData.questionCoordinates != null) {
						//updating paragraph coordinates
						val.contentData.questionPosition = parseInt(resData.questionCoordinates.split(",")[0]) + "," + parseInt(resData.questionCoordinates.split(",")[1]) + "," + parseInt(resData.questionCoordinates.split(",")[2]) + "," + parseInt(resData.questionCoordinates.split(",")[3]);
					}
				}
			}

		});
		//arranging data for structure tab
		arrangeStructureData();
	}
	/*
	* @purpose: selected Annotation details
	* @created: 24th Sep 2018
	* @params: id(string)
	* @author: Swathi
	*/
	window.selectedAnnotation = function (id, selectedColor) {
		var ansArr = [];
		var idType = "question", quesId, currentId;
		if (selectedColor == "yellow" || selectedColor == "#44BB65") { //checking based on selected rectangle border color for question or answer
			$scope.answersList.map(function (v) {
				if (v.answerUUID == id) {
					idType = "answer";
					quesId = v.questionUUID;
					if (v.answerType == "TEXT") { $scope.isText = true; }
					else { $scope.isText = false; }
				}
			});
			$scope.answersList.map(function (v) {
				if (v.questionUUID == id && idType == "question") {
					ansArr.push(v);
					if (v.answerType == "TEXT") { $scope.isText = true; }
					else { $scope.isText = false; }
				}
				if ((v.answerUUID == id && idType == "answer") || (quesId && v.questionUUID == quesId)) {
					ansArr.push(v);
				}
			});
			setTimeout(function () {
				$scope.currentAnswers = ansArr;
				angular.forEach($scope.currentAnswers, function (val, k) {
					var currentSelectedIndex = $scope.ansTypes.indexOf(val.answerType);
					if (currentSelectedIndex == -1) {
						currentSelectedIndex = 0;
					}
					$scope.selectedAnsVal['ans' + k] = currentSelectedIndex.toString();
				});
				$scope.$apply();
			}, 0);
			$scope.questionsList.map(function (v) {
				currentId = quesId ? quesId : id;
				if (v.questionUUID == currentId) {
					setTimeout(function () {
						$scope.currentSelection = "Question";
						$scope.currentQuestionData = v;
						$scope.currentQuestion = v.question;
						$scope.$apply();
					}, 0);
				}
			});
		}
		if (selectedColor == "#fab764") { //checking based on selected rectangle border color for form field
			formsList.map(function (v) {
				if (v.questionUUID == id) {
					ansArr.push(v);
				}
			});
			setTimeout(function () {
				$scope.currentSelection = "Form Fields";
				$scope.currentFormFields = ansArr;
				angular.forEach($scope.currentFormFields, function (val, k) {
					var currentSelectedIndex = $scope.formFieldSubTypes.indexOf(val.formField.toLowerCase());
					if (currentSelectedIndex == -1) {
						currentSelectedIndex = 0;
					}
					$scope.selectedFormVal['form' + k] = currentSelectedIndex.toString();
				});

				$scope.$apply();
			}, 0);
		}
		if (selectedColor == "blue") { //checking based on selected rectangle border color for heading or paragraph
			headingOrParagraphList.map(function (v) {
				if (v.questionUUID == id) {
					ansArr.push(v);
				}
			});
			setTimeout(function () {
				if (ansArr.length > 0) {
					$scope.currentSelection = ansArr[0].contentType;
					$scope.headingOrParagraph = ansArr[0];
					$scope.$apply();
				}
			}, 0);
		}
	};

	/*
  * @purpose: To show popup on Click of cross icon(Delete answer functionality) in Details Tab. 
  * @created: 15th Oct 2018
  * @params: ans(object)
  * @author: Prasanthi
  */

	$scope.ShowDeleteAnswerModal = function (index, ans, tabType) {
		$timeout(function () {
			$scope.deleteAnswerModal = true;
			$scope.deleteAnswerModalMsg = "Are you sure you want to delete this?";
			$scope.deleteAnsData = ans;
			$scope.tabType = tabType;
			$scope.index = index;
		}, 0);
		setTimeout(function () {
			$('#movingArrow').css("display", 'none');
		}, 0);
	};

	/* 
	* @purpose:  on click of yes and cancel from delete Answer popup 
	* @created: 15th Oct, 2018
	* @author: prasanthi
	*/
	$scope.hideDeleteAnswerModal = function (condition) {
		if (condition) {
			$scope.deleteAnswer($scope.index, $scope.deleteAnsData, $scope.tabType);
		}
		$scope.deleteAnswerModal = false;
	};
	/*
  * @purpose: delete Answer
  * @created: 17th Sep 2018
  * @params: ans(object)
  * @author: Swathi
  */
	$scope.deleteAnswer = function (index, ans, tabType) {
		if (tabType == "form") {
			$scope.finalStructuredData.map(function (v, k) {
				if (v.questionNo.indexOf("Form Field") != -1) {
					if (v.positionOnPage == ans.questionPositionOnThePage) {
						$scope.finalStructuredData.splice(k, 1);
					}
				}
			});
			$scope.currentFormFields.splice(index, 1);
			if ($scope.currentFormFields.length == 0) { $scope.deleteQuestion(); }
		}
		if (tabType == "QA") {
			$timeout(function () {
				$scope.currentAnswers.splice(index, 1);
				if ($scope.answersList.length > 0) {
					$scope.answersList.map(function (a, b) {
						if (a.answerUUID == ans.answerUUID) {
							$scope.answersList.splice(b, 1);
						}
					});
				}
			}, 0);
			$scope.finalStructuredData.map(function (v, k) {
				var structurePos;
				if (v.key == 'Text') {
					structurePos = JSON.parse(v.positionOnPageAnswer);
					var ansPos = JSON.parse(ans.answerPositionOnThePage);
					if (structurePos && ansPos && JSON.stringify(structurePos[0]) === JSON.stringify(ansPos[0])) {
						$scope.finalStructuredData.splice(k, 1);
					}
				} else {
					v.subAnswerType.map(function (val, key) {
						if (val.answerPosition == ans.answerPositionOnThePage) {
							v.subAnswerType.splice(key, 1);
							if (v.subAnswerType.length == 0) {
								$scope.finalStructuredData.splice(k, 1);
							}
						}
					});
				}


			});
		}
		if (ans.answerType == "TEXT") {
			templateArray.map(function (d, i) {
				if (d.contentData.answerPosition == ans.answerCoordinates) {
					templateArray.splice(i, 1);
				}
			});
		}
		if (ans.answerType == "CHECKBOX") {
			templateArray.map(function (d, i) {
				if (d.contentData && d.contentData.possibleAnswersDto) {
					d.contentData.possibleAnswersDto.map(function (v, k) {
						if (v.answerPosition == ans.answerCoordinates) {
							templateArray[i].contentData.possibleAnswersDto.splice(k, 1);
						}
					});
					if (d.contentData.possibleAnswersDto.length == 0) {
						templateArray.splice(i, 1);
					}
				}
			});
		}
		if (templateArray.length == 0) {
			document.querySelector('div#pageContainer' + (1) + ' svg.annotationLayer').innerHTML = '';
			$scope.deleteQuestion();
			$scope.currentAnswers = [];
			$scope.currentFormFields = [];
		}
		if (annotationsList.length > 0 && templateArray.length > 0) {
			angular.forEach(annotationsList, function (d) {
				if (ans.x == d.x && ans.y == d.y) {
					//getting all rectangles which are plotted in pdf file
					var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
					if (arr.length > 0) {
						angular.forEach(arr, function (v) {
							if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
								var parent = v.parentNode;
								parent.removeChild(v);
								//									 window.createEditOverlay(v, "fromJs");
							}
						});
					}
				}
				if (tabType == "QA") {
					$timeout(function () {
						if ($scope.currentAnswers.length == 0 && $scope.currentQuestionData && $scope.currentQuestionData.x == d.x && $scope.currentQuestionData.y == d.y) {
							//getting all rectangles which are plotted in pdf file
							var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
							if (arr.length > 0) {
								angular.forEach(arr, function (v) {
									if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
										var parent = v.parentNode;
										parent.removeChild(v);
										//									 window.createEditOverlay(v, "fromJs");
									}
								});
							}
							$scope.deleteQuestion();
						}
					}, 0);
				}
			});
		}
		//			 window.getAnnotationValue(currentAnnotationsList, "delete");
		//			 window.updateAnnotations(currentAnnotationsList, "delete");
	};
	/*
	* @purpose: delete Question
	* @created: 17th Sep 2018
	* @author: Swathi
	*/
	$scope.deleteQuestion = function () {
		setTimeout(function () {
			$scope.currentSelection = "";
			$timeout(function () {
				$(".cursor").click();//enable click event on rectangle
			});
			$scope.$apply();
		}, 0);
		$scope.currentQuestionData = undefined;
		if (templateArray.length == 0) {
			//disable save,cancle and preview buttons
			$("#docParserSave").addClass("pe-none");
			$("#docParserSaveDiv").addClass("c-ban");
			$("#docParserCancle").addClass("pe-none");
			$("#docParserCancleDiv").addClass("c-ban");
			$("#docParserPreview").addClass("pe-none");
			$("#docParserPreviewDiv").addClass("c-ban");
		}
	};

	/*
	  * @purpose: delete Element
	  * @created: 21st Sep 2018
	  * @author: Swathi
	  */
	$scope.deleteElement = function (currentQuestion, type) {
		$scope.deleteElementType = type;
		if (type == "QA") {
			if ($scope.currentAnswers.length > 0) {
				$timeout(function () {
					$scope.showQuestionCancelModal = true;
					$scope.showQuestionCancelModalMsg = "Removing question " + currentQuestion + ", will also remove all related answers, are you sure you wish to proceed?";
				}, 0);
			} else {
				//delete rectangle
				deleteQuestionAnnotation(type);
			}
		} else {
			$timeout(function () {
				$scope.showQuestionCancelModal = true;
				$scope.showQuestionCancelModalMsg = "Are you sure you wish to delete this element?";
			}, 0);
		}
		//disable save button
		$("#docParserSave").removeClass("pe-none");
		$("#docParserSaveDiv").removeClass("c-ban");
	};
	/*
* @purpose: delete Element annotations from doc
* @created: 26th Sep 2018
* @author: Swathi
*/
	function deleteQuestionAnnotation(type) {
		if (annotationsList.length > 0 && templateArray.length > 0) {
			var answers;
			if (type == "QA") {
				answers = $scope.currentAnswers;
			} else if (type == "form") {
				answers = $scope.currentFormFields;
			} else if (type == "headingOrParagraph") {
				var tempArr = $scope.headingOrParagraph;
				answers = [tempArr];
			}
			if (templateArray.length > 0) {
				angular.forEach(answers, function (v, k) {
					templateArray.map(function (v1, k1) {
						if (v1.contentData.questionUUID == v.questionUUID) {
							templateArray.splice(k1, 1);
							//arranging data for structure tab
							arrangeStructureData();
						}
					});
					$scope.finalStructuredData.map(function (v1, k1) {
						if (v.contentType.indexOf("Form Field") != -1 && v1.questionNo.indexOf("Form Field") != -1) {
							if (v1.positionOnPage == v.questionPositionOnThePage) {
								$scope.finalStructuredData.splice(k, 1);
							}

						} else if (v.contentType.indexOf("Question") != -1 && v1.questionNo.indexOf("Question") != -1) {
							if (v1.key == 'Text') {
								var structurePos = JSON.parse(v1.positionOnPageAnswer);
								var ansPos = JSON.parse(v.answerPositionOnThePage);
								if (structurePos && ansPos && JSON.stringify(structurePos[0]) === JSON.stringify(ansPos[0])) {
									$scope.finalStructuredData.splice(k1, 1);
								}
							} else {
								v1.subAnswerType.map(function (val, key) {
									if (val.answerPosition == v.answerPositionOnThePage) {
										v1.subAnswerType.splice(key, 1);
										if (v1.subAnswerType.length == 0) {
											$scope.finalStructuredData.splice(k1, 1);
										}
									}
								});
							}
						} else if (v.contentType.indexOf("Heading") != -1 && v1.questionNo.indexOf("Heading") != -1) {
							if (v1.positionOnPage == v.questionPositionOnThePage) {
								$scope.finalStructuredData.splice(k1, 1);
							}
						} else if (v.contentType.indexOf("Paragraph") != -1 && v1.questionNo.indexOf("Paragraph") != -1) {
							if (v1.positionOnPage == v.questionPositionOnThePage) {
								$scope.finalStructuredData.splice(k1, 1);
							}
						}

					});
				});
			}
			angular.forEach(annotationsList, function (d) {
				if (type == "QA") {
					if ($scope.currentQuestionData && $scope.currentQuestionData.x == d.x && $scope.currentQuestionData.y == d.y) {
						var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
						if (arr.length > 0) {
							angular.forEach(arr, function (v) {
								if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
									$scope.deleteQuestion();
									var parent = v.parentNode;
									parent.removeChild(v);
								}
							});
						}
					}
					if ($scope.currentAnswers.length > 0) {
						$scope.currentAnswers.map(function (v1, k1) {
							if (v1.x == d.x && v1.y == d.y) {
								//getting all rectangles which are selected in pdf file
								var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
								if (arr.length > 0) {
									angular.forEach(arr, function (v) {
										if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
											$scope.currentAnswers.splice(k1, 1);
											var parent = v.parentNode;
											parent.removeChild(v);
										}
									});
								}
							}
						});
					}
				} else if (type == "form") {
					if ($scope.currentFormFields.length > 0) {
						$scope.currentFormFields.map(function (v1) {
							if (v1.x == d.x && v1.y == d.y) {
								var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
								if (arr.length > 0) {
									angular.forEach(arr, function (v) {
										if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
											$scope.currentFormFields = [];
											var parent = v.parentNode;
											parent.removeChild(v);
										}
									});
								}
							}
						});
						$scope.deleteQuestion();
					}
				} else if (type == "headingOrParagraph") {
					if ($scope.headingOrParagraph) {
						if ($scope.headingOrParagraph.x == d.x && $scope.headingOrParagraph.y == d.y) {
							//getting all rectangles which are selected in pdf file
							var arr = document.querySelectorAll('div#pageContainer' + (d.page) + ' svg.annotationLayer rect');
							if (arr.length > 0) {
								angular.forEach(arr, function (v) {
									if (d.uuid == v.getAttribute('data-pdf-annotate-id')) {
										$scope.currentFormFields = [];
										var parent = v.parentNode;
										parent.removeChild(v);
									}
								});
							}
						}
						$scope.deleteQuestion();
					}
				}
			});
		} else {
			document.querySelector('div#pageContainer' + (1) + ' svg.annotationLayer').innerHTML = '';
			$scope.deleteQuestion();
			$scope.currentAnswers = [];
			$scope.currentFormFields = [];
			$scope.headingOrParagraph = "";
		}
	}
	/*
* @purpose: hide Question Cancel Modal
* @created: 21st Sep 2018
* @author: Swathi
*/
	$scope.hideQuestionCancelModal = function (value, type) {
		if (value == true) {
			//delete rectangle
			deleteQuestionAnnotation(type);
		}
		$scope.showQuestionCancelModal = false;
		setTimeout(function () {
			$('#movingArrow').css("display", 'none');
		}, 0);
	};

	/*
	 * @purpose:click on sub menu buttons
	 * @created: 13 Sep 2018	
	 * @author: Prasanthi
	*/
	function saveQuestionAnswersForTemplate(isFormField, headingOrpara, QandAresponse) {
		if (isFormField && $scope.currentFormFields.length > 0 && $scope.currentSelection == 'Form Fields') { //checking for form field
			var cFormFiled = $scope.currentFormFields[$scope.currentFormFields.length - 1];
			if (templateArray.length == 0 || !checkKeyExistence(cFormFiled.uuid)) {

				var data = {
					"contentType": "Question",
					"contentData": {
						"questionUUID": cFormFiled.uuid,
						"question": cFormFiled.question,
						"questionId": null,
						"questionPosition": cFormFiled.questionCoordinates,
						"answerPosition": cFormFiled.answerCoordinates,
						"pageNumber": cFormFiled.pageNumber,
						"answerType": cFormFiled.answerType,
						"answerId": null,
						"formField": cFormFiled.formField,
						"subType": "Form",
						"elementName": cFormFiled.elementName,
						"isQuestion": true,
						"answerUUID": cFormFiled.uuid,
						"questionPositionOnThePage": cFormFiled.questionPositionOnThePage,
						"specifiedQuestionPosition": null,
						"specifiedAnswerPosition": null,
						"specifiedQuestion": null,
						"specifiedAnswer": null,
						"specifiedAnswerType": null,
						"specifiedQuestionId": null,
						"specifiedAnswerId": null,
						"specifiedQuestionPositionOnThePage": null,
						"specifiedAnswerPositionOnThePage": null,
						"specifiedQuestionUUID": null,
						"specifiedAnswerUUID": null
					}
				};
				$timeout(function () {
					$(".cursor").click();//enable click event on rectangle
				}, 0);
				$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
				templateArray.push(data);
				//arranging data for structure tab
				arrangeStructureData();
			}

		}
		//checking for question or answer 
		else if ($scope.currentQuestion && $scope.currentAnswers && $scope.currentAnswers.length > 0 && ($scope.currentSelection == "Question" || $scope.currentSelection == 'Answer')) {
			var isSameQueAns = false;
			var data = {
				"contentType": "Question",
				"contentData": {
					"questionUUID": $scope.currentQuestionData.uuid,
					"question": $scope.currentQuestion,
					"questionId": null,
					"questionPosition": $scope.currentQuestionData.questionCoordinates,
					"pageNumber": $scope.currentQuestionData.pageNumber,
					"formField": "",
					"subType": "Form",
					"elementName": $scope.currentQuestionData.elementName,
					"isQuestion": true,
					"possibleAnswer": null,
					"questionPositionOnThePage": $scope.currentQuestionData.questionPositionOnThePage,
					"specifiedQuestionPosition": null,
					"specifiedAnswerPosition": null,
					"specifiedQuestion": null,
					"specifiedAnswer": null,
					"specifiedAnswerType": null,
					"specifiedQuestionId": null,
					"specifiedAnswerId": null,
					"specifiedQuestionPositionOnThePage": null,
					"specifiedAnswerPositionOnThePage": null,
					"specifiedQuestionUUID": null,
					"specifiedAnswerUUID": null
				}
			};
			if ($scope.currentAnswers[0].answerType == "CHECKBOX" || $scope.currentAnswers[0].answerType == "YES_NO_CHECKBOX") { //checking for checkbox type answers
				data.contentData.answerType = $scope.currentAnswers[0].answerType;
				if (templateArray.length > 0) {
					angular.forEach(templateArray, function (val, key) {
						if ($scope.currentQuestion == templateArray[key].contentData.question) {
							isSameQueAns = true;
						}
					});
				}
				data.contentData.possibleAnswersDto = [];
				if (templateArray.length == 0 || !checkAnswerKeyExistence(QandAresponse.answerUUID, QandAresponse.answerType)) {
					angular.forEach($scope.currentAnswers, function (val) {
						if (val.answerType == "CHECKBOX" || val.answerType == "YES_NO_CHECKBOX") {
							data.contentData.possibleAnswersDto.push({
								"possibleAnswer": val.possibleAnswer,
								"answerPosition": val.answerCoordinates,
								"answerId": null,
								"answerUUID": val.uuid,
								"answerPositionOnThePage": val.answerPositionOnThePage
							});
						}
						if (isSameQueAns) {
							angular.forEach(templateArray, function (val, key) {
								if ($scope.currentQuestion == templateArray[key].contentData.question) {
									templateArray.splice(key, 1);
								}
							});
						}
						templateArray.push(data);
						//arranging data for structure tab
						arrangeStructureData();
					});
				}
			} else {
				if (templateArray.length == 0 || !checkKeyExistence($scope.currentQuestionData.uuid)) {
					data.contentData.possibleAnswersDto = null;
					data.contentData.answerType = "TEXT";
					data.contentData.answerId = null;
					data.contentData.answerPosition = $scope.currentAnswers[0].answerCoordinates;
					data.contentData.answerPositionOnThePage = $scope.currentAnswers[0].answerPositionOnThePage;
					data.contentData.answerUUID = $scope.currentAnswers[0].uuid;
					if (isSameQueAns) {
						templateArray.splice(templateArray.length - 1);
					}
					templateArray.push(data);
					//arranging data for structure tab
					arrangeStructureData();
				}
			}
			if (data.contentData.answerType == "TEXT") {
				$scope.isText = true;
			} else {
				$scope.isText = false;
			}
			$timeout(function () {
				$(".cursor").click();//enable click event on rectangle
			}, 0);
			$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
		}
		if (headingOrpara && ($scope.currentSelection == 'Heading' || $scope.currentSelection == 'Paragraph')) {//checking for heading or paragraph
			if (templateArray.length == 0 || !checkKeyExistence($scope.headingOrParagraph.uuid)) {
				var data = {
					"contentType": $scope.currentSelection,
					"contentData": {
						"questionUUID": $scope.headingOrParagraph.uuid,
						"question": $scope.headingOrParagraph.question,
						"questionId": null,
						"questionPosition": $scope.headingOrParagraph.questionCoordinates,
						"answerPosition": null,
						"pageNumber": $scope.headingOrParagraph.pageNumber,
						"answerType": null,
						"answerId": null,
						"formField": null,
						"subType": "Form",
						"elementName": $scope.headingOrParagraph.elementName,
						"isQuestion": true,
						"answerUUID": null,
						"questionPositionOnThePage": $scope.headingOrParagraph.questionPositionOnThePage,
						"specifiedQuestionPosition": null,
						"specifiedAnswerPosition": null,
						"specifiedQuestion": null,
						"specifiedAnswer": null,
						"specifiedAnswerType": null,
						"specifiedQuestionId": null,
						"specifiedAnswerId": null,
						"specifiedQuestionPositionOnThePage": null,
						"specifiedAnswerPositionOnThePage": null,
						"specifiedQuestionUUID": null,
						"specifiedAnswerUUID": null,
						"possibleAnswersDto": null,
						"possibleAnswer": null
					}
				};
				$timeout(function () {
					$(".cursor").click();//enable click event on rectangle
				}, 0);
				$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
				templateArray.push(data);
				//arranging data for structure tab
				arrangeStructureData();
			}
		}
	}
	/*
	 * @purpose: Check whether Object already exist or not(redunant)
	 * @created: 8th Oct 2018	
	 * @author: Prasanthi
	 */
	function checkKeyExistence(name) {
		for (var i = 0, len = templateArray.length; i < len; i++) {
			if (templateArray[i].contentData.questionUUID === name) { return true; }
		}
		return false;
	}
	/*
	 * @purpose: Check whether Object already exist with AnswerUUID in case of Question and Answer or not(redunant)
	 * @created: 10th Oct 2018	
	 * @author: Prasanthi
	 */

	function checkAnswerKeyExistence(UUID) {
		for (var i = 0, len = templateArray.length; i < len; i++) {
			if (templateArray[i].contentData.possibleAnswersDto && templateArray[i].contentData.possibleAnswersDto.length > 0) {
				for (var j = 0, dataLen = templateArray[i].contentData.possibleAnswersDto.length; j < dataLen; j++) {
					if (templateArray[i].contentData.possibleAnswersDto[j].answerUUID === UUID) {
						return true;
					}
				}
			}
		}
		return false;
	}
	/*
	 * @purpose:click on sub menu buttons(save button to template save)
	 * @created: 7 Sep 2018	
	 * @author: Prasanthi
	*/
	function saveTemplate(update) {
		var data = {
			"documentContentDto": templateArray
		};
		var params;
		if (update) {
			params = {
				token: $rootScope.ehubObject.token,
				tempId: templateId
			};

		} else {
			params = {
				docId: documentId,
				docFlag: 10,
				token: $rootScope.ehubObject.token,
			};
		}
		$scope.runLoader = true;
		docParserService.saveTemplate(data, params, update).then(function (response) {
			if (response.data && response.data.length > 0) {
				if (response.data[0].documentContentDto) {
					getExistingAnnotations(response.data[0].documentContentDto);
				}
				isSavedDoc = true;
				//enable and disable buttons in submenu
				handleSubmenuButtons();
				savedtemplateArray = jQuery.extend(true, [], templateArray);
				var msg;
				if (update) {
					msg = 'Template Updated Sucessfully';
				} else {
					msg = 'Template Saved Sucessfully';
					$state.go('.', { tId: response.data[0].templateId }, { docId: documentId }, { id: $scope.templateName }, { notify: false });
				}
				HostPathService.FlashSuccessMessage(msg, '');
				templateId = response.data[0].templateId;
				$("#docParserSave").html("update");
				isEdited = false;
				handleSubmenuButtons();//enable and disable buttons in submenu
				getTemplateData(response);
				if (templateId) {
					//enable edit mode
					$('.fa-pencil').removeClass("pe-none");
					$('.fa-pencil').parent().removeClass("c-ban");
				}
			} else {
				var msg;
				if (update) {
					msg = 'Failed to Updated Template';

				} else {
					msg = 'Failed to  Save Template';
				}
				HostPathService.FlashErrorMessage(msg, '');
				handleSubmenuButtons();//enable and disable buttons in submenu
			}
			$scope.runLoader = false;
		}, function () {
			$scope.runLoader = false;
			HostPathService.FlashErrorMessage('Failed to save template', '');
			handleSubmenuButtons();//enable and disable buttons in submenu
		});
	}
	window.clearAnnotations = function () {
		templateArray = jQuery.extend(true, [], savedtemplateArray);
		$scope.currentQuestionData = undefined;
		setTimeout(function () {
			$scope.currentSelection = "";
			$timeout(function () {
				$(".cursor").click();
			});
			$scope.$apply();
		}, 0);
		$scope.currentAnswers = [];
		//disable save and cancle buttons
		$("#docParserSave").addClass("pe-none");
		$("#docParserSaveDiv").addClass("c-ban");
		$("#docParserCancle").addClass("pe-none");
		$("#docParserCancleDiv").addClass("c-ban");
		//arranging data for structure tab
		arrangeStructureData();
		if (!templateId) {
			//disable preview button
			$("#docParserPreview").addClass("pe-none");
			$("#docParserPreviewDiv").addClass("c-ban");
		}
	};
	/*
	 * @purpose: get preview data
	 * @created: 08 Sep 2018	
	 * @params: type(string) 
	 * @author: swathi
	 */
	var QuestionnaireData = [], totQuestions = 0;
	function getTemplateData(response1) { //getting template data
		var response = jQuery.extend(true, {}, response1);
		QuestionnaireData = [];
		totQuestions = 0;
		if (!response || !response.data || !response.data[0] || !response.data[0].documentContentDto) {
			return false;
		}
		angular.forEach(response.data[0].documentContentDto, function (value) {
			if (value.contentData.answerType == 'YES_NO_CHECKBOX') {
				value.contentData.answerType = 'CHECKBOX';
			}
			if (value.contentData.answerType == 'CHECKBOX' && !value.contentData.possibleAnswer && value.contentData.possibleAnswersDto.length > 0) {
				var ansArr = [];
				angular.forEach(value.contentData.possibleAnswersDto, function (v2) {
					if (v2.possibleAnswer) {
						ansArr.push(v2.possibleAnswer);
					}
				});
				value.contentData.possibleAnswer = ansArr.join("/");
			}
			if (value.contentData.isOSINT) { //checking for BST type questions
				value.isOSINT = true;
				value.contentData.topic = value.contentData.level1Code;
				value.contentData.osintVal = value.contentData.actualAnswer;
			}
			if (value.isQuestion && value.contentData) {
				totQuestions++;
				value.contentData.questionNumber = totQuestions;
				value.contentData.originalAns = value.contentData.answer;

				if (value.contentData.answerType == 'CHECKBOX') {
					if (value.contentData.possibleAnswer) {
						value.contentData.possibleAnswer = (value.contentData.possibleAnswer).toUpperCase().split('/');
					}
					if (value.contentData.answer) {
						value.contentData.answer = (value.contentData.answer).toUpperCase();
					}
					if (value.contentData && value.contentData.answerType == 'CHECKBOX' && value.contentData.answer == 'YES') {
						value.contentData.val = "YES";
					}
					if (value.contentData && value.contentData.answerType == 'CHECKBOX' && value.contentData.answer == 'NO') {
						value.contentData.val = "NO";
					}
				}
				if (!value.contentData.level1Code) { //checking for level1 data
					value.contentData.topic = "None";
					value.contentData.primaryIsoCode = 'un mapped';
					value.contentData.level1Code = 'un mapped';
					value.contentData.level3Title = 'un mapped';
				}
				value.contentData.topic = value.contentData.topic ? value.contentData.topic : "None";
				QuestionnaireData.push(value);
			}
			if (value.contentType == "Heading" || value.contentType == "Paragraph") {
				QuestionnaireData.push(value);
			}
		});
	}
	/*
	 * @purpose: get doc details
	 * @created: 09 Sep 2018	
	 * @author: swathi
	*/
	var documentDetails;
	function getDocDetails(docId) {
		docParserService.getDocumentDetails(docId).then(function (response) {
			documentDetails = response.data;
			if (HostPathService.getParameterByName('id')) {
				$('#docparserTemplatename').html(HostPathService.getParameterByName('id'));//doc ID
				$scope.templateName = HostPathService.getParameterByName('id');
				$('.fa-pencil').removeClass("pe-none");
				$('.fa-pencil').parent().removeClass("c-ban");
			}
			else if (response.data && response.data.docName) {
				$('#docparserTemplatename').html(response.data.docName);
				$scope.templateName = response.data.docName;
			}
		}, function () {
		});

	}
	/*
	 * @purpose: run Template
	 * @created: 11th Sep 2018	
	 * @params: docId
	 * @author: swathi
	*/
	//on click of run button
	function runTemplate(docId) {
		var filename = documentDetails.docName;
		var index = filename.lastIndexOf(".");
		var strsubstring = filename.substring(index, filename.length) ? filename.substring(index, filename.length).toLowerCase() : filename.substring(index, filename.length);

		if (documentDetails.size > 10240000) {//checking for uploaded file size
			HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', documentDetails.docName);
		}
		//checking the file type
		else if (strsubstring == '.pdf' || strsubstring == '.doc' || strsubstring == '.docx' || strsubstring == '.csv') {
			$scope.runLoader = true;
			var params = {
				"token": $rootScope.ehubObject.token,
				"docId": docId
			};
			if (HostPathService.getParameterByName('caseId')) {
				params.caseId = HostPathService.getParameterByName('caseId');
			}
			var data = {
				"uploadFile": null
			};
			//create case API
			docParserService.createCaseFromQuestioner(params, data).then(function () {
				$scope.runLoader = false;
				var url = EHUB_FE_API + '#/discover';
				window.open(url, '_blank');
			}, function (e) {
				$scope.runLoader = false;
				HostPathService.FlashErrorMessage('ERROR CREATING CASE ', e.responseMessage);
			});
		}
		else {
			HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: csv, word, pdf");
			$scope.runLoader = false;
		}
	}
	/*
	 * @purpose: open preview doc modal
	 * @created: 08 Sep 2018	
	 * @params: type(string) 
	 * @author: swathi
	 */
	$scope.handleDocParserButtonclicks = function (type) {
		if (type == 'docParserPreview') {
			var dataModal = $uibModal.open({
				templateUrl: '../docparser/modal/views/preview.modal.html',
				controller: 'PreviewModalController',
				windowClass: 'custom-modal bst_modal comments-modal',//adding css classes to the modal elements
				resolve: {
					previewData: function () {
						return QuestionnaireData;//sending data to the modal
					}
				}
			});
			dataModal.result.then(function () {
			}, function () {
			});
		}
	};
	/*
	 * @purpose: Structure panel Data
	 * @created: 11th Sep 2018	
	 * @author: varsha
	 */
	function arrangeStructureData() {
		var structureArray = [];
		templateArray.map(function (d) {
			if (d.contentData.answerType == "TEXT" && !d.contentData.formField) {
				structureArray.push({
					contentType: d.contentType,
					questionNo: d.contentData.elementName,
					'key': 'Text',
					question: d.contentData.question,
					questionPosition: d.contentData.questionPosition,
					subAnswerType: [],
					positionOnPage: d.contentData.questionPositionOnThePage,
					positionOnPageAnswer: d.contentData.answerPositionOnThePage,
					pageNo: d.contentData.pageNumber,
					questionUUID: d.contentData.questionUUID,
					answerUUID: d.contentData.answerUUID,
					color: 'yellow'
				});
				var indSave = structureArray.map(function (d) {
					return d.questionNo;
				}).indexOf(d.contentData.elementName);
				structureArray[indSave].subAnswerType.push({ 'key': 'Text' });
			} else if (d.contentData.answerType == "CHECKBOX" || d.contentData.answerType == "YES_NO_CHECKBOX" && !d.contentData.formField) {
				structureArray.push({
					contentType: d.contentType,
					questionNo: d.contentData.elementName,
					'key': 'Checkbox',
					question: d.contentData.question,
					questionPosition: d.contentData.questionPosition,
					subAnswerType: [],
					positionOnPage: d.contentData.questionPositionOnThePage,
					//						positionOnPageAnswer:[],
					pageNo: d.contentData.pageNumber,
					questionUUID: d.contentData.questionUUID,
					color: 'yellow'
				});
				var indSave = structureArray.map(function (d) {
					return d.questionNo;
				}).indexOf(d.contentData.elementName);
				d.contentData.possibleAnswersDto.map(function (d1, k1) {
					structureArray[indSave].subAnswerType.push({
						'key': 'Checkbox',
						'answerPosition': d.contentData.possibleAnswersDto[k1].answerPositionOnThePage,
						"answerUUID": d.contentData.possibleAnswersDto[k1].answerUUID
					});
				});
			} else if (d.contentData.formField) {
				structureArray.push({
					questionNo: d.contentData.elementName,
					contentType: d.contentType,
					'key': 'Form Fields',
					question: d.contentData.question,
					questionPosition: d.contentData.questionPosition,
					subAnswerType: [],
					positionOnPage: d.contentData.questionPositionOnThePage,
					positionOnPageAnswer: d.contentData.answerPositionOnThePage,
					pageNo: d.contentData.pageNumber,
					questionUUID: d.contentData.questionUUID,
					answerUUID: d.contentData.answerUUID,
					color: '#fab764'
				});
			} else if (d.contentType == "Heading") {
				structureArray.push({
					questionNo: d.contentData.elementName,
					contentType: d.contentType,
					'key': 'Heading',
					question: d.contentData.question,
					questionPosition: d.contentData.questionPosition,
					subAnswerType: [],
					positionOnPage: d.contentData.questionPositionOnThePage,
					pageNo: d.contentData.pageNumber,
					questionUUID: d.contentData.questionUUID,
					color: 'blue'
				});
			} else if (d.contentType == "Paragraph") {
				structureArray.push({
					questionNo: d.contentData.elementName,
					contentType: d.contentType,
					'key': 'Paragraph',
					question: d.contentData.question,
					questionPosition: d.contentData.questionPosition,
					subAnswerType: [],
					positionOnPage: d.contentData.questionPositionOnThePage,
					pageNo: d.contentData.pageNumber,
					questionUUID: d.contentData.questionUUID,
					color: 'blue'
				});
			}
		});
		structureArray.sort(function (a, b) {
			return a.questionPosition.split(",")[1] - b.questionPosition.split(",")[1];
		});
		structureArray.sort(function (a, b) {
			return a.pageNo - b.pageNo;
		});
		$scope.finalStructuredData = structureArray;
	}
	/*
	 * @purpose: Redirect user to the question point of the document canvas
	 * @created: 11th Sep 2018	
	 * @author: varsha
	 */
	//showing arrow symbol on rectangle
	$scope.redirectToQuestion = function (strucData) {
		if (strucData.questionNo == "Form Fields") {
			return false;
		}
		var position = JSON.parse(strucData.positionOnPage);
		var positionLeft;
		if (position[0].docStatus == sessionStorage.getItem("docStatus")) {
			positionLeft = (position[0].left);
		} else if (position[0].docStatus == 'half' && sessionStorage.getItem("docStatus") == 'full') {
			positionLeft = (position[0].left) - 100;
		} else if (position[0].docStatus == 'full' && sessionStorage.getItem("docStatus") == 'half') {
			positionLeft = (position[0].left) + 100;
		}
		$('#content-wrapper').animate({
			scrollTop: position[0].scrollPosition
		}, 0);
		$('#movingArrow').css("top", position[0].top + 'px');
		$('#movingArrow').css("left", positionLeft + 'px');
		setTimeout(function () {
			$('#movingArrow').css("display", 'block');
		}, 0);
	};
	/*
	 * @purpose: Redirect user to the question point of the document canvas
	 * @created: 11th Sep 2018	
	 * @author: varsha
	 */
	//showing arrow symbol on rectangle
	$scope.redirectToAnswer = function (strucData, k) {
		if (strucData.positionOnPageAnswer) {
			var position = JSON.parse(strucData.positionOnPageAnswer);
		} else {
			var position = JSON.parse(strucData.subAnswerType[k].answerPosition);
		}
		var positionLeft;
		if (position[0].docStatus == sessionStorage.getItem("docStatus")) {
			positionLeft = (position[0].left);
		} else if (position[0].docStatus == 'half' && sessionStorage.getItem("docStatus") == 'full') {
			positionLeft = (position[0].left) - 100;
		} else if (position[0].docStatus == 'full' && sessionStorage.getItem("docStatus") == 'half') {
			positionLeft = (position[0].left) + 100;
		}
		$('#content-wrapper').animate({
			scrollTop: position[0].scrollPosition
		}, 0);
		$('#movingArrow').css("top", position[0].top + 'px');
		$('#movingArrow').css("left", (positionLeft) + 'px');
		setTimeout(function () {
			$('#movingArrow').css("display", 'block');
		}, 0);
	};
	/*
	 * @purpose: add more form fields
	 * @created: 18th September
	 * @author: Prasanthi
	 */
	$scope.addMoreformFields = function () {
		$scope.currentSelection = 'Form Fields';
		window.currentcolor = "#fab764";
		$(".rectangle").click();
		$scope.cursorNormal('crosshair', 'none', 'none', 'block', 'none', 'none');//cursor changes to form field
	};
	/*
	 * @purpose: add more form fields
	 * @created: 18th September
	 * @author: Prasanthi
	 */
	$scope.addMoreAnswers = function () {
		$scope.currentSelection = 'Answer';
		window.currentcolor = "yellow";
		$(".rectangle").click();
		$scope.cursorNormal('crosshair', 'none', 'block', 'none', 'none', 'none');//cursor changes to answer
	};
	/*
	 * @purpose: get edited template
	 * @created: 24th September
	 * @author: Prasanthi
	 */
	function getTemplateDetails(tId) {
		var params = {
			templateId: tId,
			token: $rootScope.ehubObject.token
		};
		docParserService.editTemplate(params).then(function (response) {
			if (response && response.data && response.data[0] && response.data[0].documentContentDto) {
				templateArray = [];
				angular.forEach(response.data[0].documentContentDto, function (val) {
					if (val.contentType != "SUB_PARAGRAPH") {
						if (val.contentType == "Heading" || val.contentType == "Paragraph") {
							val.contentData.question = val.contentData.text;
						}
						templateArray.push(val);
					}
				});
				savedtemplateArray = jQuery.extend(true, [], templateArray);
				//arranging data for structure tab
				arrangeStructureData();
				//get document details
				getDocumentDetails(documentId, response.data[0].documentContentDto);
				//get template data
				getTemplateData(response);
				$timeout(function () {
					$(".cursor").click();//enable click event on rectangle
				});
				$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
			} else {
				//get document details
				getDocumentDetails(documentId);
				$timeout(function () {
					$(".cursor").click();//enable click event on rectangle
				});
				$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
			}
		}, function () {
		});
	}

	/*
	 * @purpose: get current selection for dragged rectangle
	 * @created: 29th Sep 2018
	 * @author: Karunakar
	 */
	var isDrag = false;
	var isSetDrag = false;
	window.getUpdatedDragedValues = function (annotation) {
		isDrag = true;
		isSetDrag = true;
		angular.forEach(templateArray, function (val) {
			if (val.contentType && val.contentType == "Question") {
				//checking for question
				if ((val.contentData.questionUUID && val.contentData.questionUUID == annotation.uuid && val.contentData.formField == "")) { $scope.currentSelection = "Question"; }
				//checking for answer
				else if (val.contentData.answerUUID && val.contentData.answerUUID == annotation.uuid && val.contentData.formField == "") { $scope.currentSelection = "Answer"; }
				//checking for form field
				else if (val.contentData.answerUUID === annotation.uuid && val.contentData.questionUUID === annotation.uuid) { $scope.currentSelection = "Form Fields"; }
				//checking for checkbox type answers
				else {
					angular.forEach(val.contentData.possibleAnswersDto, function (v) {
						if (v.answerUUID === annotation.uuid) { $scope.currentSelection = "Answer"; }
					});
				}
			}
			//for headings
			else if (val.contentType && val.contentType == "Heading") {
				if (val.contentData.questionUUID == annotation.uuid) { $scope.currentSelection = "Heading"; }
			}
			//for paragraphs
			else {
				if (val.contentData.questionUUID == annotation.uuid) { $scope.currentSelection = "Paragraph"; }
			}
			//getting selected area content from the API 
		}); getContentFromAPI(annotation, $scope.currentSelection);
	};
	/* 
	 * @purpose: on click of trash icon from structure tab.
	 * @created: 12th Oct, 2018
	 * @author: prasanthi
	 */
	$scope.showDeleteStructureModal = function (deleteStructureData) {
		//To add href on anchor tag 
		$('.right-panel').each(function (k, v) {
			/*jshint scripturl:true*/
			$(v).find('a').attr('href', 'javascript:void(0)');
		});
		$timeout(function () {
			$scope.deleteStructureModal = true;
			$scope.deleteStructureModalMsg = "Are you sure you want to delete this?";
			$scope.deleteStructureData = deleteStructureData;
		}, 0);
		//enable save button
		$("#docParserSave").removeClass("pe-none");
		$("#docParserSaveDiv").removeClass("c-ban");
	};
	/* 
	 * @purpose:   on click of yes and cancel from delete popup 
	 * @created: 12th Oct, 2018
	 * @author: prasanthi
	 */
	$scope.hideDeleteStructureModal = function (condition) {
		if (condition) {
			$scope.deleteStructureElement($scope.deleteStructureData);

		}
		$scope.deleteStructureModal = false;
		setTimeout(function () {
			$('#movingArrow').css("display", 'none');
		}, 0);
	};
	/* 
	 * @purpose: For Deleting the Annotaion and data on click of trash icon from structure tab.
	 * @created: 5th Oct, 2018
	 * @author: prasanthi
	 */
	$scope.deleteStructureElement = function (deleteStructureData) {
		angular.forEach(templateArray, function (v, k) {
			if (v.contentData.questionUUID && deleteStructureData.questionUUID) {
				if (v.contentData.questionUUID === deleteStructureData.questionUUID) {
					if (templateArray[k]) {
						templateArray.splice(k, 1);
						//arranging data for structure tab
						arrangeStructureData();
					}
				}
			}
		});
		//getting all rectangles which are selected in pdf file
		var arr = document.querySelectorAll('div#pageContainer' + (deleteStructureData.pageNo) + ' svg.annotationLayer rect');
		if (arr.length > 0) {
			if (deleteStructureData.key == 'Form Fields') {
				angular.forEach(arr, function (v) {
					if (deleteStructureData.questionUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);

					}
				});
			} else if (deleteStructureData.key == 'Text') {
				angular.forEach(arr, function (v) {
					if (deleteStructureData.questionUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);
					}
					if (deleteStructureData.answerUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);
					}
				});
			} else if (deleteStructureData.key == 'Checkbox') {
				angular.forEach(arr, function (v) {
					if (deleteStructureData.questionUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);
					}
					angular.forEach(deleteStructureData.subAnswerType, function (val) {
						if (val.answerUUID == v.getAttribute('data-pdf-annotate-id')) {
							var parent = v.parentNode;
							parent.removeChild(v);
						}
					});
				});
			} else if (deleteStructureData.key == 'Heading') {
				angular.forEach(arr, function (v) {
					if (deleteStructureData.questionUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);
					}
				});
			} else if (deleteStructureData.key == 'Paragraph') {
				angular.forEach(arr, function (v) {
					if (deleteStructureData.questionUUID == v.getAttribute('data-pdf-annotate-id')) {
						var parent = v.parentNode;
						parent.removeChild(v);
					}
				});
			}
		}
		if ($scope.currentAnswers.length > 0) {
			angular.forEach($scope.currentAnswers, function (v, k) {
				if (v.questionUUID && deleteStructureData.questionUUID) {
					if (v.questionUUID === deleteStructureData.questionUUID) {
						if ($scope.currentAnswers[k]) {
							$scope.currentAnswers.splice(k, 1);
							$scope.currentSelection = "";
						}
					}
				}
			});
		}
		if ($scope.currentFormFields.length > 0) {
			angular.forEach($scope.currentFormFields, function (v, k) {
				if (v.questionUUID && deleteStructureData.questionUUID) {
					if (v.questionUUID === deleteStructureData.questionUUID) {
						if ($scope.currentFormFields[k]) {
							$scope.currentFormFields.splice(k, 1);
							$scope.currentSelection = "";
						}
					}
				}
			});
		}
		if ($scope.headingOrParagraph) {
			if (Object.keys($scope.headingOrParagraph).length > 0) {
				if ($scope.headingOrParagraph.questionUUID && deleteStructureData.questionUUID) {
					if ($scope.headingOrParagraph.questionUUID == deleteStructureData.questionUUID) {
						$scope.headingOrParagraph = "";
						$scope.currentSelection = "";
					}
				}
			}
		}
	};
	/*
	 * @purpose: Getting the position of the cursor
	 * @created: 26th September
	 * @author: Asheesh
	 */

	$('body').mousemove(function (event) {
		if ($state.current.name === 'docParserPage') {
			var offset = $("#pdf-reader").offset();
			$(".mouse-pointerq").css({
				'top': (event.pageY - (offset.top - 10)) + 'px',
				'left': (event.pageX - (offset.left - 10)) + 'px'
			});
			$(".mouse-pointera").css({
				'top': (event.pageY - (offset.top - 10)) + 'px',
				'left': (event.pageX - (offset.left - 10)) + 'px'
			});
			$(".mouse-pointerf").css({
				'top': (event.pageY - (offset.top - 10)) + 'px',
				'left': (event.pageX - (offset.left - 10)) + 'px'
			});
			$(".mouse-pointerh").css({
				'top': (event.pageY - (offset.top - 10)) + 'px',
				'left': (event.pageX - (offset.left - 10)) + 'px'
			});
			$(".mouse-pointerp").css({
				'top': (event.pageY - (offset.top - 10)) + 'px',
				'left': (event.pageX - (offset.left - 10)) + 'px'
			});
		}
	});

	/*
	 * @purpose: For getting the normal cursor once the click is outside the scope
	 * @created: 26th September
	 * @author: Asheesh
	 */
	$(document).mouseup(function (e) {
		var container = $("#pdf-dashboard");
		// if the target of the click isn't the container nor a descendant of the container
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			$scope.cursorNormal('default', 'none', 'none', 'none', 'none', 'none');//cursor changes to default
		}
	});
	/*
	 * @purpose: To stop collapse of heading and paragraph
	 * @created: 5 oct, 2018  
	 * @author: prasanthi
	 */
	$scope.stopCollapse = function (structureData) {

		$('.nonCollapse').each(function (k, v) {
			$(v).children('.panel-collapse').addClass('d-none');
		});
		window.selectedAnnotation(structureData.questionUUID, structureData.color);
	};
	/*
	* @purpose: To update answer type by user selection
	* @created: 5 oct, 2018  
	* @author: prasanthi
	*/
	$scope.updateAnswerField = function (ans, selected) {
		ans.answerType = selected >= 0 ? $scope.ansTypes[selected] : 'NONE';
	};
	/*
	* @purpose: To update form fields by user selection
	* @created: 5 oct, 2018  
	* @author: prasanthi
	*/
	$scope.updateFormField = function (field, selected) {
		isEdited = true;
		//enable and disable buttons in submenu
		handleSubmenuButtons();
		field.formField = selected > 0 ? $scope.formFieldSubTypes[selected] : 'NONE';
		formsList.map(function (v) {
			if (v.questionUUID == field.questionUUID) {
				v.formField = selected > 0 ? $scope.formFieldSubTypes[selected] : 'NONE';
			}
		});
		updateFieldInTemplateArray('formField', field.formField, field.questionUUID);
	};
	/*
	* @purpose: To update  fields in template array (for saving in DB)
	* @created: 5th oct, 2018  
	* @author: prasanthi
	*/
	function updateFieldInTemplateArray(field, newVal, uuid, ispossibleAns) {
		angular.forEach(templateArray, function (value) {
			if (ispossibleAns) {
				//handle for answers
				if (value.contentData && value.contentData.possibleAnswersDto && value.contentData.possibleAnswersDto.length > 0) {
					angular.forEach(value.contentData.possibleAnswersDto, function (v1) {
						if (v1.contentData.answerUUID == uuid) {
							v1.contentData[field] = newVal;
						}
					});
				}
			} else {
				if (value.contentData && value.contentData.questionUUID == uuid) {
					value.contentData[field] = newVal;
				}
			}
		});
		//arranging data for structure tab
		arrangeStructureData();
	}
	/*
	* @purpose: Renaming the Question
	* @created: 7 Sep 2018	
	* @author: Asheesh
	*/
	$scope.editorEnabled = false;
	$scope.enableEditor = function () {
		$scope.editorEnabled = true;
		$scope.editableTitle = $scope.currentQuestionData.elementName;
	};
	$scope.disableEditor = function () {
		$scope.editorEnabled = false;
	};
	$scope.saveQuestionName = function (newTitle) {
		$scope.currentQuestionData.elementName = newTitle;
		isEdited = true;
		//enable and disable buttons in submenu
		handleSubmenuButtons();
		updateFieldInTemplateArray('elementName', newTitle, $scope.currentQuestionData.questionUUID);
		$scope.disableEditor();
	};
}