"use strict";

angular.module("docparserApp").controller("LandingController",
	landingController);

landingController.$inject = ["$scope",
	"$state",
	"$http",
	"docParserService",
	"TopPanelApiService",
	"$uibModal",
	"HostPathService",
	"$timeout",
	"EHUB_FE_API",
	"$window"
];

function landingController($scope,
	$state,
	$http,
	docParserService,
	TopPanelApiService,
	$uibModal,
	HostPathService,
	$timeout,
	EHUB_FE_API,
	$window
) {
	//$scope.userID = $rootScope.ehubObject.userId;
	/*
	 * @purpose: navigate to main docparser page 
	 * @created: 3rd Sep 2018 
	 * @params:
	 * value 
	 * @return: @author: Prasanthi
	 */
	$scope.navigateToDocParserPage = function (data) {
		//sending data from Landing to Service to fetch in docparser
		TopPanelApiService.getEdittedTemplateData(data);
		if (data.fillablePdf) {
			//redirecting to docparser page with edited pdf file
			$state.go('docParserPage', { tId: data.id, docId: data.initialDocId, id: data.templateName }, { notify: false });
			setTimeout(function () {
				$window.location.reload();
			}, 1000);
		} else {
			HostPathService.FlashErrorMessage('ERROR', "Cannot Edit This Template");
		}
	};
	/*
	 * @purpose: to enable/disable options
	 * @created: 3rd Sep 2018 
	 * @params: index postion, Template Name.
	 * value 
	 * @return: @author: Prasanthi
	 */
	$scope.disablethis = function (index) {
		$scope.selectedRow = index;
	};

	/*
	 * @purpose: to SEARCH template name from Table
	 * @created: 9th Sep 2018 
	 * @params: searched character
	 * value 
	 * @return: @author: Prasanthi
	 */
	$scope.searchTemplateName = function (search) {
		var searchParams = {
			"searchKey": search
		};
		//Calling API to get searched result
		if($scope.loadSavedJson.length>0){
			apiCallToGetTempletaName(searchParams);
		}
		$scope.curPage = 1;
	};

	/*
	 * @purpose: Funciton to send params in case of pagination
	 * @created: 9th Sep 2018 
	 * @params: value 
	 * @return: no return 
	 * @author: Prasanthi
	 */
	$scope.curPage = 1;
	$scope.pageSize = 10;
	//Getting templates for pagination
	$scope.getTemplates = function (page) {
		$scope.templateLoader = true;
		if (page) {
			$scope.curPage = page;
		} else {
			$scope.curPage = 1;
		}
		var Params = {
			"pageNumber": $scope.curPage,
			"recordsPerPage": $scope.pageSize,
		};
		apiCallToGetTempletaName(Params);
	};
	$scope.getTemplates();

	/*
		* @purpose: Funciton to get list of templates
		* @created: 3rd Sep 2018 
		* @params:
		* value 
		* @return: @author: Prasanthi
		*/
	function apiCallToGetTempletaName(Params) {
		//getTemplateList API will give template list
		docParserService.getTemplateList(Params).then(
			function (response) {
				if (!response.data || response.data.length == 0) {
					$scope.loadSavedJson = [];
				} else {
					$scope.loadSavedJson = response.data.templatesRepsonseDtos;
					$scope.totalResults = response.data.information.totalResults;
					$scope.totalCountOnPage = (response.data.information.index - 1) * $scope.pageSize + response.data.information.count;
					$scope.startCountOnPage = (response.data.information.index - 1) * $scope.pageSize + 1;
					$scope.numberOfPages = function () {
						return Math.ceil($scope.totalResults / $scope.pageSize);
					};
				}
				$scope.templateLoader = false;
			},
			function () {
				$scope.templateLoader = false;
				$scope.loadSavedJson = [];
			});
	}
	/*
	 * @purpose: To Delete Template Name from  Table row. 
	 * @created: 7th Sep 2018 
	 * @params: index position
	 * value:
	 * @return: @author: Prasanthi
	 */
	$scope.deleteThisTemplate = function (index, templateId) {
		var data = { 'position': index, 'dataTemplate': $scope.loadSavedJson, 'templateId': templateId };
		//Will open modal to get the permission for deleting template 
		$scope.openModal(data);
	};

	/*
	* @purpose: to open modal popup To confirm to navigate to docparser page
	* @created: 7th sep 2018
	* @params: params
	* @returns: data
	* @author: prasanthi
	*/
	$scope.openModal = function (data) {
		//Open the modal
		var dataModal = $uibModal.open({
			templateUrl: function () {
				if ((window.location.hash.indexOf("#!/") < 0)) { return 'scripts/common/modal/views/confirmationForDocparserPage.modal.html'; }
				else { return '../scripts/common/modal/views/confirmationForDocparserPage.modal.html'; }
			},
			controller: 'ConfirmationForDocparser',
			size: 'xs',//extra small size
			scope: $scope,//passing this controller scope values to the modal controller
			state: $state,//passing this controller state to the modal controller
			windowClass: 'custom-modal bst_modal update-entities-modal related-person-modal data-popup-wrapper',//adding css classes to html element
			resolve: {
				data: function () {
					return data;//sending data to the modal controller
				}
			}
		});
		dataModal.result.then(function () {
			//success function for modal
		}, function () { //error function for modal 
		});
	};
}
