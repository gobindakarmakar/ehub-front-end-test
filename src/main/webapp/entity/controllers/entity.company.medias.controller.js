'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyMediasNewController', entityCompanyMediasNewController);
entityCompanyMediasNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyMediasNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,	
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
	$scope.$on('mediasOnload', function (e) {

		if (EntityCommonTabService.lazyLoadingentity.mediaTabFirsttime) {
			EntityCommonTabService.lazyLoadingentity.mediaTabFirsttime = false;
			$scope.showPdf = false;
			$scope.showDoc = false;
			$scope.pdfDocuments = [];
			$scope.docDocuments = [];
			getCaseDocuments(EntityCommonTabService.basicsharedObject.companyName);
			getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.mediaFetchers).then(function (responsemessage) {

			});
		}
	});

	function getCaseDocuments(name) {
		var params = {
			caseSearchKeyword: name,
			token: $rootScope.ehubObject.token,
			orderIn: 'desc',
			orderBy: 'createdOn'
		};
		EntityApiService.caseFullTextSearch(params).then(function (response) {
			angular.forEach(response.data.result, function (v, k) {
				params = {
					caseId: v.caseId,
					token: $rootScope.ehubObject.token
				};
				EntityApiService.getCaseDocuments(params).then(function (response) {
					caseDocumentsList.push(response.data.result);
					if (caseDocumentsList.length > 0) {
						angular.forEach(caseDocumentsList, function (val, key) {
							if (val.length > 0) {
								angular.forEach(val, function (caseObj, key) {
									$scope.caseDocuments.push(caseObj);
								});
							}
						});
					}
					angular.forEach($scope.caseDocuments, function (v, k) {
						if (v.fileType == 'pdf') {
							$scope.showPdf = true;
							$scope.pdfDocuments.push(v);
						} else if (v.fileType == 'doc') {
							$scope.showDoc = true;
							$scope.docDocuments.push(v);
						}
					});
				}, function (error) {
				});
			});
		}, function (error) {
		});
	}
	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		var companyNameSplitted = name.split(' ');
		var copyOfCompanyName = '';
		angular.forEach(companyNameSplitted, function (word, key) {
			var index = Company_Unwanted_End_Points.indexOf(word.toLowerCase());
			if (index === -1) {
				copyOfCompanyName += word + ' ';
			}
		});
		copyOfCompanyName = copyOfCompanyName.trim();
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company'
			};
			data.limit = '1';
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				var currentSubFetcher = id;
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						if (subfetcherData.fetcher === 'channels.youtube.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ((subedv.relationshipName) == 'videos') {
										angular.forEach(subedv.entity.edges, function (subedvVal, subedvKey) {
											if ((subedvVal.relationshipName) == 'thumbnails') {
												angular.forEach(subedvVal.entity.edges, function (subedvEntityVal, subedvEntityKey) {
													if ((subedvEntityVal.relationshipName) == 'high') {
														$scope.entitySearchResult.list["videos_list"].push({
															title: subedv.entity.properties.title,
															watch_url: subedv.entity.properties.watch_url,
															thumbnail: subedvEntityVal.entity.name,
															description: subedv.entity.properties.description
														});
													}
												});
											}
										});
									}
								});
							});
						}

					}
				});
				if (key === fetcherIds.length - 1) {
					deffered.resolve($scope.entitySearchResult);
				}
			}, function (error) {
				deffered.reject(error.responsemessage);
			});
		});
		// calling web socket
		return deffered.promise;
	}
}