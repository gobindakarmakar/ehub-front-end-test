'use strict';
elementApp
    .controller('EntityShareHolderEvidenceModalController', entityShareHolderEvidenceModalController);

entityShareHolderEvidenceModalController.$inject = [
    '$scope',
    '$rootScope',
    '$uibModalInstance',
    '$timeout',
    'EntityApiService',
    'EHUB_FE_API',
    '$filter',
    'shareHolderData',
    'EnrichApiService',
    'currentEntityNode',
    'TopPanelApiService'
];
/*
 * @purpose:Share holder evidence modal controller
 * @created: 25th June 2019
 * @author:Amarjith
*/
function entityShareHolderEvidenceModalController(
    $scope,
    $rootScope,
    $uibModalInstance,
    $timeout,
    EntityApiService,
    EHUB_FE_API,
    $filter,
    shareHolderData,
    EnrichApiService,
    currentEntityNode,
    TopPanelApiService
) {
    if(shareHolderData.length === 0){
        return;
    }
    $scope.entityShareHolderListsData = [];
    $scope.defaultActiveIndex = 0;
    $scope.multisourceDiscData = [];
    $scope.openEditList = false;
    var filter_shareholders = shareHolderData.filter(function (obj) {
        return (obj.entity_id && obj.entity_id == "orgChartParentEntity" && (obj.classification.includes('Intermediate Parent') || obj.classification.includes('Ultimate Legal Parent')));
    });
    $scope.entityShareHolderListsData = filter_shareholders;
    if (currentEntityNode) {
        $scope.defaultActiveIndex = $scope.entityShareHolderListsData.findIndex(function (d) { return d.identifier == currentEntityNode.identifier })
    }
    $scope.shareHOlderLoader = false;
    $scope.screenShot_source = [];
    var requestForMultiSource = null;
    var requestForSelectentity = null;
    var requestForFetchLink = null;
    /*
 * @purpose: getMultiSource click of a company from left pane
 * @created: 25th June 2019
 * @params:  index, companyObj
 * @author:Amarjith
*/
    $scope.getMultiSource = function (index, companyObj) {
        $scope.defaultActiveIndex = index;
        callMultiSourceApi(companyObj)
    }

       /*
 * @purpose: fetch entity on click of a company from left pane
 * @created: 25th June 2019
 * @params:   companyObj
 * @author:Amarjith
*/
    function callMultiSourceApi(companyObj) {
        var dataParams = {
            keyword: companyObj && companyObj.name ? companyObj.name : '',
            jurisdiction: companyObj && companyObj.jurisdiction ? companyObj.jurisdiction : '',
            website: ''
        }
        $scope.shareHOlderLoader = true;
        requestForMultiSource =  EnrichApiService.getEntityResolvedV2(dataParams).then(function (response) {
            if (!response.data['is-completed']) {
                $timeout(function () {
                    // if (dataParams && dataParams.jurisdiction) {
                        callMultiSourceApi(companyObj);
                    // } else {

                    // }
                }, 5000);
            } else {
                $scope.shareHOlderLoader = false;
                multiSourceDiscription(response)
            }
        }, function (error) {
            console.log(error);
        });
    }

    callMultiSourceApi($scope.entityShareHolderListsData[$scope.defaultActiveIndex]); // call initial company share holder 1st item
  
/*
 * @purpose:  After Successfully response of multisource , function to display the results in the right page
 * @created: 25th June 2019
 * @params:   response [Array]
 * @author:Amarjith
*/
    function multiSourceDiscription(response) {
        $scope.multisourceDiscData = [];
        if (response && response.data && response.data.results && response.data.results.length != 0) {
            response.data.results.forEach(function (val) {
                var select_entity_link = val.links['select-entity'];
                // var source_category_considering = (val.overview) ? Object.keys(val.overview)[0] : [];
                var source_category_considering = [];
                Object.keys(val.overview).forEach(function(key) {
                    if(val.overview[key].hasOwnProperty('sourceCategory')){
                        if(val.overview[key]['sourceCategory'] === 'Trade Registry'){
                            source_category_considering.unshift({
                                key : key,
                                value: val.overview[key],
                                identifier: val.identifier
                            });
                        }
                    }
                });
                source_category_considering = _.sortBy(source_category_considering, [function(o) { return o.key; }]);
                for (var key in val.overview) {
                    if(source_category_considering[0] && source_category_considering[0].key === key ){
                        val.overview[key].identifier = val.identifier ? val.identifier.toLowerCase() : '';
                        $scope.multisourceDiscData.push({
                            source_name : key,
                            overviewSrc : val.overview[key],
                            select_entity : select_entity_link,
                            identifier : val.identifier ? val.identifier.toLowerCase() : '',
                            isScreenShot: false,
                            loading : false,
                            screenShot_source:'',
                            screenShot_sourceName:'',
                            addtopageloader:false,
                            addedtopage:false
                        });
                    }
                }
            });

            // callSelectentity($scope.multisourceDiscData[$scope.defaultActiveIndex]['overviewSrc'],$scope.multisourceDiscData[$scope.defaultActiveIndex]['select_entity']);
        }
    }
    //function for changing the icons list
    $scope.openEditListShareHolder = function (entityObj,requestId,$event) {
        callSelectentity(entityObj,requestId)
        $event.stopPropagation();
        $event.preventDefault();
    }
    //function for regitery urlwindow
    $scope.openWindowRegistryURI = function (hasUrl,$event) {
        $event.stopPropagation();
        $event.preventDefault();
        // location.href="$scope.multisourceDiscData[0]['bst:registryURI']";
        window.open(hasUrl);
    }

/*
 * @purpose:  on Click of camera icon , call Select Entity API
 * @created: 25th June 2019
 * @params:   entityObj[object],requestId[string]
 * @author:Amarjith
*/
    function callSelectentity(entityObj,requestId){
        if(!entityObj || !entityObj.identifiers  || !requestId){
            return;
        }
        var identifier = entityObj.identifier; //entityObj.identifiers.bvdID || entityObj.identifiers.legal_entity_identifier || entityObj.identifiers.other_company_id_number;
        identifier = identifier ? identifier.toLowerCase() : '';
        var entityrequestId = requestId ? requestId.split("/")[6] : '';
        update_multisourceDiscData(identifier,{loader:true}, 'loading');
        setTimeout(function(){
            requestForSelectentity = EntityApiService.selectEntity(identifier,entityrequestId).then(function (seletresponse) {
                if (!seletresponse.data['is-completed']) {
                    $timeout(function () {
                        callSelectentity(entityObj,requestId);
                    }, 5000);
                } else {
                    $scope.openEditList = true;
                    getfetchOverViewData(identifier);
                }
            }, function (error) {
            });
        }, 0);
    }

    /*
 * @purpose:  After successfully response call fetch overview data
 * @created: 25th June 2019
 * @params:   entityObj[object],requestId[string]
 * @author:Amarjith
*/
    function getfetchOverViewData(identifier){
        var url = {
            identifier: encodeURIComponent(identifier),
            type: 'overview'
        };

        requestForFetchLink = EntityApiService.complianceFieldsUrl(url).then(function (response) {
            if (!response.data['is-completed']) {
                $timeout(function () {
                    getfetchOverViewData(url);
                }, 5000);
            } else {
                fetchScreenShot(response,identifier);
            }
        },function(err){

        });
    }

    function update_multisourceDiscData(identifier,dataObj,type){
        var currObjIndex = $scope.multisourceDiscData.findIndex(function(entityObj){
            return (entityObj.identifier === identifier );//.overviewSrc.identifiers && ((entityObj.overviewSrc.identifiers.bvdID && entityObj.overviewSrc.identifiers.bvdID.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.legal_entity_identifier && entityObj.overviewSrc.identifiers.legal_entity_identifier.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.other_company_id_number &&  entityObj.overviewSrc.identifiers.other_company_id_number.toLowerCase() === identifier)));
        });
        if(currObjIndex !== -1){
            if(type){
                $scope.multisourceDiscData[currObjIndex]['loading'] = dataObj.loader;
                $scope.multisourceDiscData[currObjIndex]['isScreenShot'] = false;
                $scope.multisourceDiscData[currObjIndex]['screenShot_source'] = '';
                $scope.multisourceDiscData[currObjIndex]['screenShot_sourceName'] = '';
                $scope.multisourceDiscData[currObjIndex]['addtopageloader'] = false;
                $scope.multisourceDiscData[currObjIndex]['addedtopage'] = false;
            }else{
                $scope.multisourceDiscData[currObjIndex]['loading'] = dataObj.loader;
                $scope.multisourceDiscData[currObjIndex]['isScreenShot'] = dataObj.isScreenShot;
                $scope.multisourceDiscData[currObjIndex]['screenShot_source'] = dataObj.screenshot_sourceUrl;
                $scope.multisourceDiscData[currObjIndex]['screenShot_sourceName'] = dataObj.screenshot_sourceUrl ? (dataObj.screenshot_sourceUrl.split('_')[1] + "_" + dataObj.screenshot_sourceUrl.split('_')[3] + "_" + dataObj.screenshot_sourceUrl.split('_')[4]) : '';
                $scope.multisourceDiscData[currObjIndex]['addtopageloader'] = false;
                $scope.multisourceDiscData[currObjIndex]['addedtopage'] = false;

            }
        }
    }

    /*
    * @purpose :  After successfully response of getfetchOverViewData , function to have screenshot Api
    * @author :  Amarjith Kumar
    * @Date : 01-July-2019
    * @params :  response (Obj)
    */

    function fetchScreenShot(response,identifier){
        if(response && response.data && response.data.results && response.data.results.length){
            var currObj = response.data.results[0];
            var dataSources = [];
            for (var source in currObj.overview) {
                if(source !== 'comapnyInfo'){
                    dataSources.push({
                        sourceName : source,
                        sourceValue : currObj.overview[source]
                    });
                }
            }

            if(dataSources && dataSources.length){
                var findIndex_from_multiSrc = $scope.multisourceDiscData.findIndex(function(entityObj){
                    return (entityObj.identifier === identifier);//overviewSrc.identifiers && ((entityObj.overviewSrc.identifiers.bvdID && entityObj.overviewSrc.identifiers.bvdID.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.legal_entity_identifier && entityObj.overviewSrc.identifiers.legal_entity_identifier.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.other_company_id_number &&  entityObj.overviewSrc.identifiers.other_company_id_number.toLowerCase() === identifier)));
                });
                var src_screenshotObj = null;
                if(findIndex_from_multiSrc !== -1){
                    src_screenshotObj =  dataSources.find(function(d){ 
                        return d.sourceName.toLowerCase() === $scope.multisourceDiscData[findIndex_from_multiSrc].source_name.toLowerCase()
                     });
                }
              
                var dataObj = {
                    loader : false,
                    screenshot_sourceUrl :( src_screenshotObj &&  src_screenshotObj.sourceValue &&  src_screenshotObj.sourceValue.source_screenshot) ? src_screenshotObj.sourceValue.source_screenshot : '',
                    isScreenShot:true
                }
                update_multisourceDiscData(identifier,dataObj);

            }
            console.log(dataSources);
        }
    }
   $scope.addtoEvidencePage = function(entityObj,$event){
        var obj_val = jQuery.extend(true, {}, entityObj);
        obj_val.sourceName = obj_val.screenShot_sourceName;
        obj_val.splitSourceScreenshot = obj_val.screenShot_sourceName;
        obj_val.SourceValue = {
            source_screenshot : obj_val.screenShot_source
        };
        
        addToPage(obj_val);
       
        $event.stopPropagation();
        $event.preventDefault();
   }

   $scope.downloadEvidence = function(entityObj,$event){
        var obj_val = jQuery.extend(true, {}, entityObj);
        obj_val.sourceName = obj_val.screenShot_sourceName;
        obj_val.SourceValue = {
            source_screenshot : obj_val.screenShot_source
        };
        $scope.$parent.downloadEvidenceSourceScreenshot(obj_val);
        $event.stopPropagation();
        $event.preventDefault();
   }

   $scope.closeShareOverviewModal= function() {
    // $scope.abortRequest();
    $uibModalInstance.close();

   }
setTimeout(function () {
    $('.evidence-accordian-wrapper, .left-side-lists').mCustomScrollbar({
        axis: "y"
    });
}, 1000);

// abort the current request (if its running).
$scope.abortRequest = function() {
    return( requestForMultiSource && requestForMultiSource.abort() || requestForSelectentity && requestForSelectentity.abort() || requestForFetchLink && requestForFetchLink.abort());
};

function addToPage(SourceDataObj, index, fromShareholderModal) {
  
    $scope.$parent.evidenceReportAddTopage.push(SourceDataObj.SourceValue)
    TopPanelApiService.getEvidenceArray = $scope.$parent.evidenceReportAddTopage;
    var data = {
        "token": $rootScope.ehubObject.token,
        "url" : encodeURIComponent(SourceDataObj.SourceValue.source_screenshot),
        "fileTitle":SourceDataObj.splitSourceScreenshot.split('.')[0],
        "remarks": '',
        "docFlag":6,
        "entityId":$rootScope.entityObj["@identifier"],
        "entityName": $rootScope.entityObj.name
    }
    $scope.showLoaderEvidence=true;
    // $scope.showEvidence = true;
    var identifier =  (SourceDataObj.identifier);//overviewSrc.identifiers && ((SourceDataObj.overviewSrc.identifiers.bvdID && SourceDataObj.overviewSrc.identifiers.bvdID.toLowerCase()) || (SourceDataObj.overviewSrc.identifiers.legal_entity_identifier && SourceDataObj.overviewSrc.identifiers.legal_entity_identifier.toLowerCase() ) || (SourceDataObj.overviewSrc.identifiers.other_company_id_number &&  SourceDataObj.overviewSrc.identifiers.other_company_id_number.toLowerCase())))
    var currObjIndex = $scope.multisourceDiscData.findIndex(function(entityObj){
        return (entityObj.identifier === identifier)//overviewSrc.identifiers && ((entityObj.overviewSrc.identifiers.bvdID && entityObj.overviewSrc.identifiers.bvdID.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.legal_entity_identifier && entityObj.overviewSrc.identifiers.legal_entity_identifier.toLowerCase() === identifier) || (entityObj.overviewSrc.identifiers.other_company_id_number &&  entityObj.overviewSrc.identifiers.other_company_id_number.toLowerCase() === identifier)));
    });
    $scope.multisourceDiscData[currObjIndex]['addtopageloader'] = true;
    
    TopPanelApiService.uploadDocumentByScreenShotURLData(data).then(function(response) {
        $scope.showLoaderEvidence=false;
        $scope.showEvidence=true;
        $scope.multisourceDiscData[currObjIndex]['addtopageloader'] = false;
        $scope.multisourceDiscData[currObjIndex]['addedtopage'] = true;
    }, function() {
    });
}
  
}