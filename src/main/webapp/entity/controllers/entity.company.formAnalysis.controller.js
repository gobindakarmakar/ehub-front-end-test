'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyUploadNewController', entityCompanyUploadNewController);
entityCompanyUploadNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyUploadNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
	$scope.$on('formAnalysis', function () {

	});
	var isoQueIndexes = {}, topicQueIndexes = {};
	var QuestionnaireData = [];
	$scope.metQuestionList = [];
	$scope.notMetQuestionList = [];
	$scope.isoListData = [];
	$scope.currentViewData = [];
	$scope.dataForPieChart = [];
	$scope.kpiobj = {
		isopen: false
	};
	var totQuestions = 0, answeredQuestions = 0;
	var score = 0;
	var dataTopics
	$scope.TopicKPIData = [];
	$scope.IsoStructuredData = [];
	$scope.TopicStructuredData = [];
	var currentDocID;
	$scope.checkedValue = false;
	$scope.QuessionaireView = 'topic';
	var questionnaireFileType, questionnaireFileName;
	$scope.cyberNews = [];

	/*
 * @purpose: fuction to sort numbers and strings
 * @created: 1st May 2018
 * @params: params
 * @returns: success error functions
 * @author: Prasanthi
 */
	function natSort(as, bs) {
		var a, b, a1, b1, i = 0, L, rx = /(\d+)|(\D+)/g, rd = /\d/;
		if (isFinite(as.key) && isFinite(bs.key)) return as.key - bs.key;
		a = String(as.key).toLowerCase();
		b = String(bs.key).toLowerCase();
		if (a === b) return 0;
		if (!(rd.test(a) && rd.test(b))) return a > b ? 1 : -1;
		a = a.match(rx);
		b = b.match(rx);
		L = a.length > b.length ? b.length : a.length;
		while (i < L) {
			a1 = a[i];
			b1 = b[i++];
			if (a1 !== b1) {
				if (isFinite(a1) && isFinite(b1)) {
					if (a1.charAt(0) === "0") a1 = "." + a1;
					if (b1.charAt(0) === "0") b1 = "." + b1;
					return a1 - b1;
				}
				else return a1 > b1 ? 1 : -1;
			}
		}
		return a.length - b.length;
	}

	$scope.fetchDocumentDetails = function (docId, actionType) {
		QuestionnaireData = [];
		currentDocID = docId;
		$scope.docPreloader = true;
		EntityApiService.generateJsonResponse(docId, actionType).then(function (response) {
			$scope.docPreloader = false;
			$scope.headers = [];
			if (!response || !response.data || !response.data[0] || !response.data[0].documentContentDto) {
				return false;
			}
			angular.forEach(response.data[0].documentContentDto, function (value) {
				response.data[0].categories.map(function (item) {
					if (item.isoCode == value.contentData.level1Code) {
						value.contentData.categoryId = item.categoryId;
						value.contentData.IsoEvaluation = item.evaluation;
					}
				});
				if (value.contentData.answerType == 'YES_NO_CHECKBOX') {
					value.contentData.answerType = 'CHECKBOX';
				}
				if (value.contentData.answerType == 'CHECKBOX' && !value.contentData.possibleAnswer && value.contentData.possibleAnswersDto.length > 0) {
					var ansArr = [];
					angular.forEach(value.contentData.possibleAnswersDto, function (v2, k2) {
						if (v2.possibleAnswer) {
							ansArr.push(v2.possibleAnswer);
						}
					})
					value.contentData.possibleAnswer = ansArr.join("/");
				};
				if ((value.contentData.answerType == 'CHECKBOX' || value.contentData.answerType == null) && !value.contentData.possibleAnswer) {
					value.isQuestion = false;
				}
				if (value.contentData.isOSINT) {
					value.isOSINT = true;
					value.contentData.topic = value.contentData.level1Code;
					value.contentData.osintVal = value.contentData.actualAnswer;
				}
				if (value.isQuestion && value.contentData && value.contentData.showINUI) {
					totQuestions++;
					value.contentData.originalAns = value.contentData.answer;
					if (value.contentData.answerType == 'CHECKBOX') {
						if (value.contentData.possibleAnswer) {
							value.contentData.possibleAnswer = (value.contentData.possibleAnswer).toUpperCase().split('/');
						}
						if (value.contentData.answer) {
							value.contentData.answer = (value.contentData.answer).toUpperCase();
						}
						if (value.contentData && value.contentData.answerType == 'CHECKBOX' && value.contentData.answer == 'YES') {
							value.contentData.val = "YES";
						}
						if (value.contentData && value.contentData.answerType == 'CHECKBOX' && value.contentData.answer == 'NO') {
							value.contentData.val = "NO";
						}
					}
					if (!value.contentData.level1Code) {
						value.contentData.topic = "None";
						value.contentData.primaryIsoCode = 'un mapped';
						value.contentData.level1Code = 'un mapped';
						value.contentData.level3Title = 'un mapped';
					}
					value.contentData.topic = value.contentData.topic ? value.contentData.topic : "None";
					value.contentData.evaluation = (!value.contentData.evaluation || value.contentData.evaluation.toLowerCase() == "error" || value.contentData.evaluation.toLowerCase() == "undefined") ? "unevaluated" : value.contentData.evaluation.toLowerCase();
					value.contentData.evaluation = (value.contentData.evaluation == "not_met_at_all" || value.contentData.evaluation == "not_met") ? "not met" : value.contentData.evaluation;
					value.contentData.evaluation = value.contentData.evaluation == "no_info" ? "no info" : value.contentData.evaluation;
					value.contentData.evaluation = value.contentData.evaluation == "not_applicable" ? "not applicable" : value.contentData.evaluation;
					QuestionnaireData.push(value);
				}
			});
			$scope.IsoStructuredData = d3.nest().key(function (d) { return d.contentData.topic }).entries(jQuery.extend(true, [], QuestionnaireData));
			$scope.TopicStructuredData = d3.nest().key(function (d) { return d.contentData.level1Code }).entries(jQuery.extend(true, [], QuestionnaireData));
			$scope.currentViewData = $scope.TopicStructuredData;
			fetchNews($stateParams.query)
			$scope.TopicStructuredData.sort(natSort)
			if ($scope.TopicStructuredData[$scope.TopicStructuredData.length - 1].key == 'un mapped') {
				var last_ele = $scope.TopicStructuredData.pop();
				$scope.TopicStructuredData.unshift(last_ele);
			}
			if ($scope.IsoStructuredData.length > 0) {
				$('#isoTable').mCustomScrollbar({
					axis: "y",
					theme: "minimal"
				});
				$('#isoTable').css('height', '500px');
			}
			var evalStatus = false;
			angular.forEach($scope.IsoStructuredData, function (value, key) {
				evalStatus = false;
				totQuestions = 0; answeredQuestions = 0;
				totQuestions = value.values.length;
				value.isoTitle = value.values[0].contentData.level1Title;
				value.categoryId = value.values[0].contentData.categoryId;
				value.evaluation = value.values[0].contentData.IsoEvaluation;
				if (value.evaluation != undefined && value.evaluation != "undefined") {
					value.formatedEvaluation = value.evaluation.split("_").join(" ");
					if (value.formatedEvaluation == "not met at all") {
						value.formatedEvaluation = "not met";
					}
				}
				else {
					value.formatedEvaluation = "unevaluated";
				}
				angular.forEach(value.values, function (v, k) {
					isoQueIndexes[v.contentData.questionId] = {
						"main": key,
						"sub": k
					}
					if (v.contentData.evaluation == "met") {
						answeredQuestions++;
					}
					v.contentData.formatedEvaluation = v.contentData.evaluation;
					if (v.contentData.formatedEvaluation == "not met at all") {
						v.contentData.formatedEvaluation = "not met";
					}
					//					}
				});
				$scope.isoListData.push({
					'isoCode': value.key,
					'isoSection': value.values[0].contentData.level1Title,
					'isoValues': value.values,
					'score': totQuestions > 0 ? Math.round((answeredQuestions / totQuestions) * 100) + '%' : 0,
					'answeredMetRequirement': answeredQuestions + ' of ' + totQuestions
				});
			});
			$scope.TopicKPIData = [];
			angular.forEach($scope.TopicStructuredData, function (value, key) {
				if (value.values && value.values.length > 0 && value.values[0].contentData && value.values[0].contentData.isOSINT) {
					value.values.sort(function (a, b) {
						return b.contentData.answerRanking - a.contentData.answerRanking;
					})
				}
				value.isoTitle = value.values[0].contentData.level1Title;
				value.categoryId = value.values[0].contentData.categoryId;
				value.evaluation = value.values[0].contentData.IsoEvaluation;
				if (value.evaluation != undefined && value.evaluation != "undefined") {
					value.formatedEvaluation = value.evaluation.split("_").join(" ");
					if (value.formatedEvaluation == "not met at all") {
						value.formatedEvaluation = "not met";
					}
				}
				else {
					value.formatedEvaluation = "unevaluated";
				}
				angular.forEach(value.values, function (v, k) {
					topicQueIndexes[v.contentData.questionId] = {
						"main": key,
						"sub": k
					}
				});
				var nested_kpi_data = d3.nest().key(function (d) {
					return d.contentData.evaluation;
				}).entries(jQuery.extend(true, [], value.values));
				$scope.TopicKPIData.push(
					{
						'values': nested_kpi_data,
						'isoTitle': value.isoTitle,
						'categoryId': value.categoryId,
						'key': value.key,
						'Questions': value.values.length,
						'evaluation': value.evaluation,
						'score': 0
					});
			});
			$scope.currentViewData = $scope.TopicStructuredData;
			angular.forEach($scope.currentViewData, function (val, key) {
				evalStatus = false;
				angular.forEach(val.values, function (v, k) {
					if (v.contentData.answer !== 'TABLE') {
						v.contentData.formatedEvaluation = v.contentData.evaluation;
						if (v.contentData.formatedEvaluation == "not met at all") {
							v.contentData.formatedEvaluation = "not met";
						}
						//							}
					}
				});
			});
			dataTopics = jQuery.extend(true, [], $scope.TopicKPIData);
			setTimeout(function () {
				var colorsObj = {
					"unevaluated": "rgb(150, 165, 162)",
					"met": "#6892b4",
					"not met": "#d28085",
					"overachieved": "#6892b4",
					"underachieved": "#d28085",
					"no info": "#667f8b",
					"not applicable": "#667f8b"
				}
				angular.forEach(dataTopics, function (val, key) {
					val.values.map(function (d) {
						d.key = (d.key == 'not_met_at_all' || d.key == 'not_met') ? 'not met' : d.key;
						//d.key = d.key =='no_info'?'no info':d.key;
						//d.key = d.key =='not_applicable'?'not applicable':d.key;
						d.value = d.values.length;
						d.doc_count = d.values.length;
					});
					EntityCommonTabService.InitializeandPlotPie(val.values, 'dynamic_' + key, colorsObj, '', true);
				})
			}, 1000);
		}, function (failure) {
			$scope.docPreloader = false;
		})
	};
	/*
 * @purpose: update Evaluation
 * @created: 29th Jun 2018
 * @params: answerId(integer), evaluation(string)
 * @author: Swathi
 */
	$scope.openFeedback = function (answerId, evaluation, doc, docMain) {
		if (doc.contentData.level1Code === "un mapped") {
			return false;
		}
		if (!answerId && doc.contentData && doc.contentData.answerType != 'TABLE') {
			$scope.updateValue('', doc, doc.contentData.questionId, "", "", evaluation);
			//    		updateInOtherView('', doc, evaluation,'','','headereval'); 
			return false;
		}
		if (doc.contentData && doc.contentData.answerType == 'TABLE') {
			if (doc.contentData.table && doc.contentData.table.rows && doc.contentData.table.rows.length > 0 && doc.contentData.table.rows[0].columns && doc.contentData.table.rows[0].columns.length > 0 && doc.contentData.table.rows[0].columns[0].columData)
				answerId = doc.contentData.table.rows[0].columns[0].columData.id;
			else
				answerId = '';
		}
		var params = {
			'answerId': answerId,
			'Evaluation': evaluation == "not_met_at_all" ? "not_met" : evaluation,
			'token': $rootScope.ehubObject.token
		};
		//		if(!answerId && doc.contentData){
		EntityApiService.updateEvaluation(params).then(function (response) {
			updateInOtherView('', doc, evaluation);
			if (docMain && response.data && response.data.level1Evaluation) {
				if (response.data.level1Evaluation == undefined || response.data.level1Evaluation == "undefined") {
					response.data.level1Evaluation = 'unevaluated';
				}
				updateInOtherView('', docMain, response.data.level1Evaluation, '', '', 'headereval');
			}
		});
		//		}
	};
	/*
		* @purpose: update Iso Category Evaluation
		* @created: 27th July 2018
		* @params: answerId(integer), evaluation(string)
		* @author: Swathi
		*/
	$scope.openIsoCategoryEvaluation = function (categoryId, evaluation, doc) {
		var params = {
			'categoryId': doc.categoryId,
			'Evaluation': evaluation == "not_met_at_all" ? "not_met" : evaluation,
			'token': $rootScope.ehubObject.token
		};
		EntityApiService.updateIsoCategoryEvaluation(params).then(function (response) {
			updateInOtherView('', doc, evaluation, '', '', 'headereval');
		}, function (error) {
		});
	};
	/*
 * @purpose: change Questionnaire view
 * @created: 20th June 2018
 * @params: params
 * @returns: success error functions
 * @author: Prasanthi
 */
	$scope.changeQuessionaireView = function (val) {
		if (val == 'topic') {
			$scope.QuessionaireView = 'topic';
			$scope.currentViewData = $scope.TopicStructuredData;
		} else {
			$scope.currentViewData = $scope.IsoStructuredData;
			$scope.QuessionaireView = 'iso'
		}
	}
	$scope.loadDocLinkAnalysis = function (id) {
		var vla_url;
		var nameOfTheCompany = $scope.entitySearchResult.name ? $scope.entitySearchResult.name : searchText
		if (id === 'autoDeskVLA') {
			vla_url = EHUB_FE_API + '#/linkAnalysis?p=doc&q=' + nameOfTheCompany + '&identity=' + adverseNewsIdentifier;
		} else {
			vla_url = EHUB_FE_API + '#/linkAnalysis?p=doc&q=' + nameOfTheCompany + '&identity=' + current_identifier;
		}
		window.open(vla_url, '_blank');
	};
	/*
* @purpose: download Document
* @created: 26th Jun 2018
* @author: swathi
*/
	$scope.downloadQuestionnaire = function () {
		var params = {
			'caseId': $scope.qId
		};
		EntityApiService.downloadQuestionnaire(params).then(function (response) {
			var blob = new Blob([response.data], {
				type: "application/octet-stream",
			});
			saveAs(blob, questionnaireFileName);
			HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document');
		}, function (e) {
			HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
		});
	};
		/*
	 * @purpose: get file Details like size, type etc
	 * @created: 1st May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	$scope.fetchFileDetails = function (documentId) {
		EntityApiService.getDocumentDetails(documentId).then(function (response) {
			$scope.docDetails = response.data;
			$scope.docDetails.uploadedOn = new Date($scope.docDetails.uploadedOn).toLocaleString().split(',')[0];
			$scope.docDetails.size = ($scope.docDetails.size / 1000000).toFixed(3);
			$scope.docDetails.modifiedOn = new Date($scope.docDetails.modifiedOn).toLocaleString().split(',')[0];
			questionnaireFileType = response.data.type;
			questionnaireFileName = response.data.docName;
		}, function (error) {
		});
	};
	/*
	 * @purpose: get adverse news without identifier
	 * @created: 7th May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	// $scope.cyberNews = [];
	function fetchAdverseNews(Name, data) {
		$scope.AdverseNewsPreloader = true;
		EntityApiService.getAdverseNewsForThreat(data).then(function (response) {
			if (response.data && response.data.hits && response.data.hits.length > 0) {
				$scope.cyberNews = $scope.cyberNews.concat(response.data.hits);
				$scope.totalCyber = response.data.total;
				$scope.cyberNews.map(function (ele) {
					ele.text = ele.text.substring(0, 100) + "...",
						ele.published = ele.published.split('T')[0],
						ele.technologies = ele.technologies.length > 0 ? ele.technologies : " - ";
				});
				$scope.AdverseNewsPreloader = false;
			}
		}, function (failure) {
			$scope.AdverseNewsPreloader = false;
		})
	};
		/*
	 * @purpose: get the identifier tofetch news
	 * @created: 1st May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	function fetchNews(Name) {
		nameOfTheCompany = Name;
		var data = {
			"class": "Cyber Security",
			"offset": 1,
			"country-code": "GB",
			"size": 30
		};
		fetchAdverseNews(Name, data);
		$scope.docDetailsPreloader = true;
		EntityApiService.getIdentifier(Name).then(function (response) {
			if (response.data.hits.length > 0) {
				getNewsDetails(response.data.hits[0]['@identifier']);
				if (response.data.hits[0]['bst:registrationId'])
					current_companyId = response.data.hits[0]['bst:registrationId'];
			} else {
				$scope.docDetailsPreloader = false;
			}
		}, function (failure) {
			$scope.docDetailsPreloader = false;
		})
	}
	/*
	 * @purpose: get News tofetch news
	 * @created: 1st May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	function getNewsDetails(identifier) {
		current_identifier = identifier;
		if (identifier) {
			EntityApiService.getNews(identifier).then(function (response) {
				$scope.digi_obj = {};
				angular.forEach(response.data['digital-assets'], function (value, key) {
					angular.forEach(value.exposure, function (val, i) {
						if ($.inArray(val['class'], $scope.digiHeaders) == -1) {
							$scope.digiHeaders.push(val['class']);
						}
						if (!$scope.digi_obj[val['class']]) {
							$scope.digi_obj[val['class']] = [];
						}
						$scope.digi_obj[val['class']] = $scope.digi_obj[val['class']].concat(val.source_url)
					})
				})
				$scope.digitalAssets = response.data['digital-assets'];
				$scope.totalDigitalAssets = $scope.digitalAssets ? $scope.digitalAssets.length : 0;
				$scope.docDetailsPreloader = false;
			}, function (failure) {
				$scope.docDetailsPreloader = false;
			});
		}
	}
	/* @purpose: scroll to div
	 * @created: 1st May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	$scope.scrollTODiv = function (index, isQue) {
		if (isQue) {
			$(".mainInfoPanel").mCustomScrollbar("scrollTo", "#ques_" + index);
		} else {
			$(".mainInfoPanel").mCustomScrollbar("scrollTo", "#docContent" + index);
		}
	};
	/*
	 * @purpose: make checkbox behave like radio buttons
	 * @created: 3rd May 2018 
	 * @author: Prasanthi
	 */
	$scope.CheckBoxToRadio = function (selectorOfCheckBox) {
		$(selectorOfCheckBox).each(function () {
			$(this).change(function () {
				var isCheckedThis = $(this).prop('checked');
				$(selectorOfCheckBox).prop('checked', false);
				if (isCheckedThis === true) {
					$(this).prop('checked', true);
				}
			});
		});
	};
	$scope.sendDetails = function (p, s) {
		$scope.primaryCode = p ? p : " ";
		$scope.secondary = s ? s : " ";
	};
	var div = d3.select("body").append("div").attr("class", "Screening_new_tooltip_quessionaire").style("position", "absolute").style("width", "auto").style("z-index", "9999999").style("width", "auto").style("min-width", "300px").style("background", "rgba(84, 84, 84,1)").style("padding", "10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");
	$("body").on("mouseover", ".showTooltip_quessionaire", function () {
		$(".Screening_new_tooltip_quessionaire").css("display", "block");
	}).on("mousemove", ".showTooltip_quessionaire", function (event) {
		$(".Screening_new_tooltip_quessionaire").html('<div class="col-sm-12 pad-x0"><div class="secondary-section"><a class="text-gray-ta" style="">Related ISO Codes: ' + $scope.secondary + '</a></div></div>');
		var p = $("#tlTip")
		var position = p.offset();
		var windowWidth = window.innerWidth;
		var tooltipWidth = $(".Screening_new_tooltip_quessionaire").width() + 50
		var cursor = event.pageX;
		if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip_quessionaire");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip_quessionaire").css("left", (event.pageX - 30 - $(".Screening_new_tooltip_quessionaire").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip_quessionaire");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip_quessionaire").css("left", (event.pageX) + 10 + "px");
		}
		return $(".Screening_new_tooltip_quessionaire").css("top", event.pageY + "px")
	}).on("mouseout", ".showTooltip_quessionaire", function () {
		$(".Screening_new_tooltip_quessionaire").css("display", "none");
	});
	$("body").on("mouseover", ".showIconTooltip", function () {
		$(".Screening_new_tooltip_quessionaire").css("display", "block");
	}).on("mousemove", ".showIconTooltip", function (event) {
		$(".Screening_new_tooltip_quessionaire").css("left", event.pageX + 10 + "px");
		$(".Screening_new_tooltip_quessionaire").css("top", event.pageY - 32 + "px");
	}).on("mouseout", ".showIconTooltip", function () {
		$(".Screening_new_tooltip_quessionaire").css("display", "none");
	});
	/*
	 * @purpose: scroll for pop over
	 * @created: 8th May 2018 
	 * @author: Anil
	 */
	$scope.initializeScroll = function (osnit, currentAns, queData, doc) {
		setTimeout(function () {
			$(".bottom-pop.action-icon-list .popover").css('max-width', 'none');
		}, 0);
		if (doc && doc.isOSINT) {
			$scope.fromCompanyHouse = false;
		} else {
			$scope.fromCompanyHouse = true;
		}
		$scope.osnitAns = osnit;
		$scope.currentAns = currentAns;
		$scope.currentQueData = queData;
		$scope.currentCompanyId = current_companyId;
		$timeout(function () {
			$("#popOver").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark",
				callbacks: {
					onTotalScroll: function () {
						$scope.getCyberNews();
					}
				}
			});
			$("#DApopOver").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 0)
	};
	$scope.ShowIconTooltipData = function (osnit, currentAns, doc) {
		setTimeout(function () {
			$(".bottom-pop.action-icon-list .popover").css('max-width', 'none');
		}, 0);
		if (doc.isOSINT && doc.topEvents && doc.topEvents.length > 0) {
			$scope.showTopEvents = true;
			$scope.currentEvents = doc.topEvents
			setTimeout(function () {
				$(" .analyze-panel-wrapper .action-icon-list li+.popover .primary-section").mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});
			}, 0);
		} else {
			if (doc.isOSINT) {
				$scope.fromCompanyHouseverified = false;
			} else {
				$scope.fromCompanyHouseverified = true;
			}
			$scope.showTopEvents = false;
			$scope.greenOsnit = osnit;
			$scope.greenCurrentAns = currentAns;
		}
		$scope.currentCompanyId = current_companyId;
	};
	$scope.showRefreshQuestionnaire = function () {
		$scope.formAnalysisRefreshISO.isOpen = true;
	};
	$scope.closePopover = function (action) {
		$scope.formAnalysisRefreshISO.isOpen = false;
		if (action == 'ok') {
			$scope.QuessionaireView = 'topic';
			$scope.currentViewData = $scope.TopicStructuredData;
			$scope.showIso = false;
			$scope.fetchDocumentDetails($scope.questionnaireDocId, 'Refresh');
		}
	};
	$scope.ShowEditedIconTooltipData = function (data, ranking) {
		var originalAns, date, label;
		if (ranking) {
			$scope.user = data.rankModefiedBy;
			originalAns = '';
			date = new Date(data.rankUpdatedOn);
			label = ""
		} else {
			$scope.user = data.modefiedBy;
			originalAns = data.actualAnswer;
			date = new Date(data.answerUpdatedOn);
			label = "Original Answer :"
		}
		$(".Screening_new_tooltip_quessionaire").html('<div class="col-sm-12 pad-x0"><div class="primary-section"><p style=""><b>Change Date:</b> ' + date + '</p><p><b>Changed by:</b> ' + $scope.user + '</p><p style=""><b>' + label + '</b> ' + originalAns + '</p></div></div>');
	};
		/*
     * @purpose: update ranking values
     *  @created: 10th july 2018
     * @params: params
     * @returns: success error functions
     * @author: Prasanthi
     */
	$scope.updateRanking = function (newRank, doc) {
		var params = {
			answerId: doc.contentData.answerId,
			Ranking: newRank,
			token: $rootScope.ehubObject.token
		}
		EntityApiService.updateRanking(params).then(function (response) {
			updateInOtherView(newRank, doc, "", "", "ranking");
		});
	}
	/*
	 * @purpose: update answer values
	 *  @created: 11th May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	$scope.updateValue = function (newAnswer, doc, id, docMain, checkedValue) {
		$scope.updateDoc = {};
		if (checkedValue == false) {
			newAnswer = '';
		}
		if (doc.contentData) {
			if (doc.contentData.answerType == 'TABLE') {
				if (checkedValue) {
					$scope.updateDoc.id = checkedValue.id;
				} else {
					$scope.updateDoc.id = '';
				}
			} else {
				$scope.updateDoc.id = doc.contentData.answerId;
			}
			$scope.updateDoc.answer = newAnswer.toLowerCase();
			$scope.updateDoc.documentQuestionId = doc.contentData.questionId;
		} else if (doc.contentData) {
			$scope.updateDoc.id = doc.contentData.specifiedAnswerId;
			$scope.updateDoc.answer = newAnswer.toLowerCase();
			$scope.updateDoc.documentQuestionId = doc.contentData.specifiedQuestionId;
		} else if (doc.contentDataDto) {
			$scope.updateDoc.id = doc.contentDataDto.answerId;
			$scope.updateDoc.answer = newAnswer.toLowerCase();
			$scope.updateDoc.documentQuestionId = doc.contentDataDto.questionId;
		}
		if (!$scope.updateDoc.id) {
			$scope.updateDoc.id = 0;
			$scope.updateDoc.docId = currentDocID;
		}
		EntityApiService.updateAnswer($scope.updateDoc).then(function (response) {
			if (newAnswer == "") {
				$scope.openFeedback(response.data.id, 'no_info', doc, docMain);
			}
			updateInOtherView(newAnswer, doc, "", response.data.id, '', '', checkedValue);
		});
	};
	/* @purpose: update answer values in other views
	 *  @created: 11th May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	function updateInOtherView(newAnswer, doc, feedback, ansId, ranking, headerEval, checkedValue) {
		if (headerEval) {
			var indexes_topicView_header = topicQueIndexes[doc.values[0].contentData.questionId].main;
			$scope.TopicStructuredData[indexes_topicView_header].evaluation = feedback;
			feedback = feedback.split("_").join(" ");
			if (feedback == "not met at all") {
				feedback = "not met";
			}
			$scope.TopicStructuredData[indexes_topicView_header].formatedEvaluation = feedback;
			updateKPIS();
			setTimeout(function () {
				if ($stateParams.qId != undefined && $stateParams.qId != '' && $stateParams.qId != null) {
					$scope.getCaseByRiskDetails();
				}
			}, 1000);
			return false;
		}
		var indexes_topicView = topicQueIndexes[doc.contentData.questionId];
		var changedObj_topicview = $scope.TopicStructuredData[indexes_topicView.main].values[indexes_topicView.sub];
		var indexes_iso = isoQueIndexes[doc.contentData.questionId];
		var changedObj_iso = $scope.IsoStructuredData[indexes_iso.main].values[indexes_iso.sub];
		if (ranking) {
			changedObj_topicview.contentData.answerRanking = newAnswer;
			changedObj_iso.contentData.answerRanking = newAnswer;
			changedObj_topicview.contentData.rankUpdatedOn = new Date();
			changedObj_iso.contentData.rankUpdatedOn = new Date();
			changedObj_topicview.contentData.rankModefiedBy = $rootScope.ehubObject.fullName;
			changedObj_iso.contentData.rankModefiedBy = $rootScope.ehubObject.fullName;
		} else if (feedback) {
			feedback = feedback.split("_").join(" ");
			changedObj_topicview.contentData.evaluation = feedback;
			changedObj_iso.contentData.evaluation = feedback;
			changedObj_topicview.contentData.formatedEvaluation = feedback;
			changedObj_iso.contentData.formatedEvaluation = feedback;
			if (feedback == "not met at all") {
				changedObj_topicview.contentData.formatedEvaluation = "not met";
				changedObj_iso.contentData.formatedEvaluation = "not met";
				changedObj_topicview.contentData.evaluation = "not met";
				changedObj_iso.contentData.evaluation = "not met";
			}
			updateKPIS();
		} else {
			if (checkedValue && doc.contentData.answerType == 'TABLE') {
				if (changedObj_topicview.contentData && changedObj_topicview.contentData.table) {
					angular.forEach(changedObj_topicview.contentData.table.rows, function (v, k) {
						angular.forEach(v.columns, function (colVal, colkey) {
							if (colVal.columData.id == checkedValue.id) {
								colVal.columData = checkedValue;
							}
						});
					});
				}
				if (changedObj_iso.contentData && changedObj_iso.contentData.table) {
					angular.forEach(changedObj_iso.contentData.table.rows, function (v, k) {
						angular.forEach(v.columns, function (colVal, colkey) {
							if (colVal.columData.id == checkedValue.id) {
								colVal.columData = checkedValue;
							}
						});
					});
				}
			} else {
				changedObj_topicview.contentData.answer = newAnswer;
				changedObj_topicview.contentData.answerId = ansId;
				changedObj_iso.contentData.answerId = ansId;
				changedObj_iso.contentData.answer = newAnswer;
			}
			changedObj_topicview.contentData.modefiedBy = $rootScope.ehubObject.fullName;
			changedObj_topicview.contentData.answerUpdatedOn = new Date().getTime();
			changedObj_iso.contentData.answerUpdatedOn = new Date().getTime();
			changedObj_iso.contentData.modefiedBy = $rootScope.ehubObject.fullName;
		}
	}
		/*
	 * @purpose: update kpi's when we change evaluation
	 *  @created: 11th May 2018
	 * @params: params
	 * @returns: success error functions
	 * @author: Prasanthi
	 */
	function updateKPIS() {
		$scope.isoListData = [];
		angular.forEach($scope.IsoStructuredData, function (value, key) {
			totQuestions = 0; answeredQuestions = 0;
			totQuestions = value.values.length;
			value.isoTitle = value.values[0].contentData.level1Title;
			angular.forEach(value.values, function (v, k) {
				if (v.contentData.evaluation == "met") {
					answeredQuestions++;
				}
			});
			$scope.isoListData.push({
				'isoCode': value.key,
				'isoSection': value.values[0].contentData.level1Title,
				'isoValues': value.values,
				'score': totQuestions > 0 ? Math.round((answeredQuestions / totQuestions) * 100) + '%' : 0,
				'answeredMetRequirement': answeredQuestions + ' of ' + totQuestions
			});
		});
		$scope.TopicKPIData = [];
		angular.forEach($scope.TopicStructuredData, function (value, key) {
			value.isoTitle = value.values[0].contentData.level1Title;
			var nested_kpi_data = d3.nest().key(function (d) {
				return d.contentData.evaluation;
			}).entries(jQuery.extend(true, [], value.values));
			$scope.TopicKPIData.push(
				{
					'values': nested_kpi_data,
					'isoTitle': value.isoTitle,
					'key': value.key,
					'Questions': value.values.length,
					'evaluation': value.evaluation ? value.evaluation : 'Unevaluated',
					'score': 0
				});
		});
		dataTopics = jQuery.extend(true, [], $scope.TopicKPIData);
		setTimeout(function () {
			var colorsObj = {
				"unevaluated": "rgb(150, 165, 162)",
				"met": "#63b844",
				"not met": "#e13b6d",
				"overachieved": "#d48c10",
				"underachieved": "#3B7BBC",
				"no info": "#667f8b",
				"not applicable": "#667f8b"
			}
			angular.forEach(dataTopics, function (val, key) {
				val.values.map(function (d) {
					// d.key = (d.key =='not_met_at_all' || d.key =='not_met')?'not met':d.key;
					// d.key = d.key =='no_info'?'no info':d.key;
					// d.key = d.key =='not_applicable'?'not applicable':d.key;
					d.value = d.values.length;
					d.doc_count = d.values.length;
				});
				EntityCommonTabService.InitializeandPlotPie(val.values, 'dynamic_' + key, colorsObj, '', true, 100, 100);
			})
		}, 1000);
	}
		/*
		* @purpose: update Iso Category Evaluation
		* @created: 27th July 2018
		* @params: answerId(integer), evaluation(string)
		* @author: Swathi
		*/
		$scope.openIsoCategoryEvaluation = function (categoryId, evaluation, doc) {
			var params = {
				'categoryId': doc.categoryId,
				'Evaluation': evaluation == "not_met_at_all" ? "not_met" : evaluation,
				'token': $rootScope.ehubObject.token
			};
			EntityApiService.updateIsoCategoryEvaluation(params).then(function (response) {
				updateInOtherView('', doc, evaluation, '', '', 'headereval');
			}, function (error) {
			});
		};
		$scope.renderQuestionnairePie = function () {
			var colorsObj = {
				"unevaluated": "rgb(150, 165, 162)",
				"met": "#6892b4",
				"not met": "#d28085",
				"overachieved": "#6892b4",
				"underachieved": "#d28085",
				"no info": "#667f8b",
				"not applicable": "#667f8b"
			}
			angular.forEach(dataTopics, function (val, key) {
				EntityCommonTabService.InitializeandPlotPie(val.values, 'dynamic_' + key, colorsObj, '', true);
			});
		}

}