'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanySocialMediaNewController', entityCompanySocialMediaNewController);
entityCompanySocialMediaNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanySocialMediaNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
	$scope.$on('socialMediaOnload', function (e) {
		$('#tagcloudCompanyTwitterTags').html('<div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>');
		InteractionPieChart()
		$timeout(function () {
			loadtwitterchart();
		}, 1000); 
		if (EntityCommonTabService.lazyLoadingentity.socialMediaTabFirsttime) {
			getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.socialMediaFetchers).then(function (responsemessage) {
				console.log('responsemessage: ', responsemessage);
			})
			EntityCommonTabService.lazyLoadingentity.socialMediaTabFirsttime = false;
		} else {
			$timeout(function () {
				InteractionPieChart();
			}, 10)
		}

	});
	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var tagCloudOrganizationNameList = [];
		var social_activity_locations = [];
		var twitter_tag_words = EntityCommonTabService.entityChartSharedObject.twitter_tag_words;
		var maxTwitterTagDomainRangeValue = 5;
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		var companyNameSplitted = name.split(' ');
		var copyOfCompanyName = '';
		angular.forEach(companyNameSplitted, function (word, key) {
			var index = Company_Unwanted_End_Points.indexOf(word.toLowerCase());
			if (index === -1) {
				copyOfCompanyName += word + ' ';
			}
		});
		copyOfCompanyName = copyOfCompanyName.trim();
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company'
			};
			// if(ids.indexOf('13') !== -1)
			// data.lightWeight = true;
			if (ids.indexOf('2') !== -1 || ids.indexOf('11') !== -1) {
				fullNameOfCompanyFounder !== '' ? data.keyword = fullNameOfCompanyFounder + ',' + name : '';
			}
			if (ids.indexOf('3004') !== -1 || ids.indexOf('3005') !== -1) {
				data.keyword = $stateParams['website'] ? $stateParams['website'] : websiteOfCompanyFounder !== '' ? websiteOfCompanyFounder : 'www.' + name + '.com';
			}
			if (ids.indexOf('3001') !== -1 || ids.indexOf('3002') !== -1 || ids.indexOf('3003') !== -1 || ids.indexOf('3008') !== -1) {
				data.keyword = $stateParams['website'] ? getWebsiteHostName($stateParams['website']) : websiteOfCompanyFounder !== '' ? getWebsiteHostName(websiteOfCompanyFounder) : name + '.com';
			}
			if (id == '1022') {
				data.keyword = $stateParams['website'] ? $stateParams['website'] : websiteOfCompanyFounder !== '' ? websiteOfCompanyFounder : 'www.' + name + '.com';
			}
			if (id == '2001' || id == '1008' || id == '21' || id == '1016' || id == '1017') {
				data.keyword = copyOfCompanyName;
			}
			if (id == '2001' || id == '2' || id == '1004' || id == '1005' || id == '1006' || id == '1008' || id == '4001' ||
				id == '1009' || id == '33' || id == '1010' || id == '1011' || id == '1012' || id == '48' || id == '16' || id == '21' || id == '23' ||
				id == '1017' || id == '1021' || id == '1022' || id == '1016') {
				data.limit = '1';
			}
			/* 
			*/
			var temp = _.find(handlingCompanyConstantName, { 'name': data.keyword, 'fetcher': id });
			data.keyword = temp === undefined ? data.keyword : temp.alt_name;
			data.limit = temp === undefined ? data.limit : temp.limit;
			/* if(data.keyword  === 'Nestle' && id=== '1006'){
				data.keyword  = 'Nestle S A';
			} */
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				var currentSubFetcher = id;
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							if (subentityKey === 0) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
										angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
											// subsubEnK == 'name' && subsubEnV !== '' ?
											// $scope.entitySearchResult.list[subedv.relationshipName].push(subsubEnV) : '';
											if (d3.keys($scope.entitySearchResult.list["resolve_related_similar_companies"]).length > 0) {
												if (subsubEnK == 'name' && subsubEnV !== '' && $scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											} else {
												if (subsubEnK == 'name' && subsubEnV !== '' && !subenvFlag) {
													if (!$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
														$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher] = [];
													}
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													subenvFlag = true;
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											}
										});
									}
									if ((subedv.relationshipName) == 'specialities') {
										subedv.entity.name !== '' && subedv.entity.name ? specialities.push(subedv.entity.name) : '';
									}
									if ((subedv.relationshipName) == 'people_also_viewd') {
										if (subedv.entity.name !== '' && subedv.entity.name) {
											$scope.entitySearchResult.list['people_also_viewd'].push({
												name: subedv.entity.properties.company_name,
												company_size: subedv.entity.properties.company_size,
												industry: subedv.entity.properties.industry,
												link: subedv.entity.properties.link,
												logo: subedv.entity.properties.logo,
												risk_score: subedv.entity.properties.risk_score,
												source: subedv.entity.source
											})
											if ($scope.entitySearchResult.list['people_also_viewd'].length === 0) {
												$scope.entitySearchResult.is_data_not_found.is_people_visited = false;
											}
										}
									}
								});
							}
						});
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							angular.forEach(subentity.edges, function (subedv, subedk) {
								if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
									angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
										if (subsubEnK == 'name' && subsubEnV !== '') {
											$scope.entitySearchResult.list[subedv.relationshipName].push({
												name: subsubEnV,
												source: subentity.source
											});
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subsubEnV)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subsubEnV,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
									});
								}
							});
						});
						if (subfetcherData.fetcher === 'plus.google.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'recent_public_activity') {
											$scope.entityGooglePlusList.push({
												name: subentity.properties.name,
												url: subentity.properties.url,
												title: edgeValue.entity.properties.title,
												coverPhoto: subentity.properties.cover_photo,
												lastUpdatedDate: new Date(edgeValue.entity.properties.last_updated_date),
												publishedDate: new Date(edgeValue.entity.properties.published_date),
												type: 'google'
											});
										}
									});
								}
							});
							if ($scope.entityGooglePlusList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_googlepost = false;
							}
							if ($scope.entityGooglePlusList.length > 0)
								$scope.socialMediaActiveCount = 2;
						}
						if (subfetcherData.fetcher === 'instagram.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								//	if(subentityKey === 0) {
								angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
									if (edgeValue.relationshipName === 'media') {
										$scope.entityInstagramList.push({
											caption: edgeValue.entity.properties.caption,
											thumbnail: edgeValue.entity.properties.thumbnail,
											image: subentity.properties.image,
											name: subentity.properties.name,
											followingCount: subentity.properties.following_count,
											followersCount: subentity.properties.followers_count,
											followersCountNumberic: 0,
											connectionNumberic: 0,
											type: 'instagram'
										});
									}
								});
								//}
							});
							if ($scope.entityInstagramList.length !== 0) {
								$scope.entityInstagramList[0]['followersCountNumberic'] = nFormatter(parseInt($scope.entityInstagramList[0]['followersCount']), 1);
								$scope.entityInstagramList[0]['connectionNumberic'] = nFormatter(parseInt($scope.entityInstagramList.length), 1);
							}
							if ($scope.entityInstagramList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_instagramlist = false;
							}
							if ($scope.entityInstagramList.length > 0) {
								$scope.socialMediaActiveCount = 1;
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'instagram'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "instagram",
										"size": $scope.entityInstagramList[0]['followersCount']
									});
								}
								$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
								$scope.entitySearchResult.list.social_media_count =parseInt($scope.entityInstagramList[0]['followersCount']) + $scope.entitySearchResult.list.social_media_count; 								
								EntityCommonTabService.socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							}
						}
						if (subfetcherData.fetcher === 'logo.google.com') {
							$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									if (!$scope.entitySearchResult['description'] || $scope.entitySearchResult['description'] === '') {
										$scope.entitySearchResult['description'] = subentity.properties['details'];
									}
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'social_feed') {
											$scope.entityLinkedinList.push({
												commentsCount: edgeValue.entity.properties.comments_count !== '' && edgeValue.entity.properties.comments_count ? edgeValue.entity.properties.comments_count : 0,
												likesCount: edgeValue.entity.properties.likes_count !== '' && edgeValue.entity.properties.likes_count ? edgeValue.entity.properties.likes_count : 0,
												postedOn: edgeValue.entity.properties.posted_at,
												text: edgeValue.entity.properties.text,
												image: subentity.properties.image,
												source: subentity.source
											});
										}
									});
								}
							});
							if ($scope.entityLinkedinList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
							}
							if ($scope.entityLinkedinList.length > 0)
								$scope.socialMediaActiveCount = 3;
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							}
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.properties, function (subev, subek) {
									if (subek === 'location' && subev !== '' && subev) {
										social_activity_locations.push({
											name: subev,
											long: '',
											lat: '',
											mark: 'assets/images/redpin.png',
											source: subentity.source,
											title: subentity.properties.title || subentity.source,
											'txt': '<span>location: ' + subentity.properties.location + '</span></br><span>Title: ' + subentity.properties.title + '</span></br><span>screen name: ' + subentity.properties.screen_name + '</span></br><span>details: ' + subentity.properties.details + '</span></br><span>profile created at: ' + subentity.properties.profile_created_at + '</span></br><span>source: ' + subentity.properties.source + '</span>'
										});
									}
									!$scope.entitySearchResult.hasOwnProperty(subek) && UnWantedKeyList.indexOf(subek) === -1 && subev !== '' && subev ? $scope.entitySearchResult[subek] = subev : '';
								});
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'tweets') {
											var objectData = {
												title: subentity.properties.title,
												createdOn: new Date(edgeValue.entity.properties.created_at),
												image: subentity.properties.image,
												text: edgeValue.entity.properties.text,
												retweetCount: edgeValue.entity.properties.retweet_count,
												tags: [],
												type: 'twitter'
											};
											angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
												if (subedv.relationshipName === 'user_mentions') {
													objectData.tags.push({
														name: subedv.entity.properties.name,
														screen_name: subedv.entity.properties.screen_name,
														risk_score: subedv.entity.properties.risk_score
													});
													var index = -1;
													angular.forEach(twitter_tag_words, function (object, key) {
														if (object.text === subedv.entity.properties.name)
															index = key
													});
													if (index === -1) {
														twitter_tag_words.push({
															text: subedv.entity.properties.name,
															size: 5,
															type: 'person'
														});
													} else {
														twitter_tag_words[index].size += 5;
														twitter_tag_words[index].size > maxTwitterTagDomainRangeValue ? maxTwitterTagDomainRangeValue = twitter_tag_words[index].size : '';
													}
												}
											});
											$scope.entityTwitterList.push(objectData);
											$scope.entitySearchResult['twitter_retweets_count'] += parseInt(edgeValue.entity.properties.retweet_count);
											$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1)
										}
										if (edgeValue.relationshipName === 'followers') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													name: edgeValue.entity.name,
													createdOn: new Date(edgeValue.entity.properties.profile_created_at),
													image: edgeValue.entity.properties.profile_image_url,
													screen_name: edgeValue.entity.properties.screen_name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'following') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													name: edgeValue.entity.name,
													createdOn: new Date(edgeValue.entity.properties.profile_created_at),
													image: edgeValue.entity.properties.profile_image_url,
													screen_name: edgeValue.entity.properties.screen_name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'follower_following_count') {
											$scope.entitySearchResult.list['follower_following_count'].push({
												twitter_followers_number: edgeValue.entity.properties.followers,
												twitter_following_number: edgeValue.entity.properties.following
											});
										}
									});
									if ($scope.entityTwitterList.length > 0)
										$scope.socialMediaActiveCount = 0;
								}
							});
							$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1);
							if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
								$scope.entitySearchResult.list['followers_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number), 1);
								$scope.entitySearchResult.list['twitter_following_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_following_number), 1);
							}
							loadTwitterTagsData(twitter_tag_words);
							if ($scope.entitySearchResult.list['follower_following_count'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_twitter_list = false;
							}
							if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'twitter'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "twitter",
										"size": $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number
									});
								}
								$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
								$scope.entitySearchResult.list.social_media_count = parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number )+ $scope.entitySearchResult.list.social_media_count; 
								EntityCommonTabService.socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
							EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.markers = social_activity_locations;
							// calling world map chart function
							EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions);
							
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							};
							if ($scope.entityTwitterList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							}
							if ($scope.entitySearchResult.list['follower_following_count'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							}
						}
						if (subfetcherData.fetcher === 'pages.facebook.com') {
							var matchedName = '';
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if ((matchedName === '' || (matchedName !== '' && matchedName === subentity.name))
									&& subentity.name !== '' && subentity.name) {
									$scope.entitySearchResult.list['fan_counts'] = parseInt(subentity.properties.fan_count);
									$scope.entitySearchResult.list['fan_counts_numbric'] = nFormatter(parseInt(subentity.properties.fan_count), 1);
									matchedName === '' ? matchedName = subentity.name : '';
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'recent_posts') {
											angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
												if (subsubedv.relationshipName === 'data') {
													$scope.entityFacebookList.push({
														text: subsubedv.entity.properties.message || subsubedv.entity.properties.story,
														created_time: new Date(subsubedv.entity.properties.created_time),
														website: subsubedv.entity.properties.website,
														risk_score: subsubedv.entity.properties.risk_score
													});
												}
											});
										}
										if (subedv.relationshipName === 'milstones') {
											angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
												if (subsubedv.relationshipName === 'data' && subsubedv.entity.properties.is_hidden === 'false') {
													$scope.entitySearchResult.list.latestReleases.push({
														title: subsubedv.entity.properties.title,
														start_time: new Date(subsubedv.entity.properties.start_time),
														created_time: new Date(subsubedv.entity.properties.created_time),
														risk_score: subsubedv.entity.properties.risk_score,
														source: subentity.source
													});
												}
											});
										}
									});
								}
							});
							if ($scope.entityFacebookList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_facebook_list = false;
							}
							if ($scope.entityFacebookList.length > 0) {
								$scope.socialMediaActiveCount = 4;
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'facebook'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "facebook",
										"size": $scope.entitySearchResult.list['fan_counts']
									});
								}
								$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
								$scope.entitySearchResult.list.social_media_count = parseInt($scope.entitySearchResult.list['fan_counts']) + $scope.entitySearchResult.list.social_media_count; 
								EntityCommonTabService.socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							}
						}
						if (subfetcherData.fetcher === 'bloomberg.com' || subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if ((subedv.relationshipName) == 'news_articles') {
											$scope.entitySearchResult.list["all_news_articles"].push({
												title: subedv.entity.properties.title,
												details: subedv.entity.properties.body,
												updatedOn: new Date(subedv.entity.properties.date),
												image: subedv.entity.properties.image != undefined ? subedv.entity.properties.image : '',
												link: subedv.entity.properties.link != undefined ? subedv.entity.properties.link : '',
												source: subentity.source
											});
										}
										if (subedv.relationshipName === 'key_executives') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												var index = -1;
												angular.forEach(EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
										if (subedv.relationshipName === 'compensation_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['compensation_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'nominating_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['nominating_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['corporate_governance_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													risk_score: subedv.entity.properties.risk_score,
													profile_link: subedv.entity.properties.profile_link,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === 'audit_committee') {
											if (subedv.entity.properties.name !== '') {
												$scope.entitySearchResult.list['audit_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subedv.entity.properties.profile_link,
													profile_link: subedv.entity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === "similar_companies") {
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subedv.entity.properties.name)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subedv.entity.properties.name,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
										if (subedv.relationshipName === 'compensation_committee' || subedv.relationshipName === 'nominating_committee' || subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entityOtherMemberList.push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subentity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source
												});
												var index = -1;
												angular.forEach(EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
									});
									if ($scope.entityNewsList.length > 0 && subfetcherData.fetcher === 'bloomberg.com') {
										angular.forEach($scope.entitySearchResult.list["all_news_articles"], function (object, key) {
											$scope.entityNewsList.push(object);
										});
										$scope.entityNewsList = $scope.entityNewsList.sort(function (a, b) {
											return moment(a.updatedOn).isBefore(b.updatedOn) ? 1 : -1;
										});
									}
								}
							});
							if ($scope.entitySearchResult.list['audit_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_audit = false;
							}
							if ($scope.entitySearchResult.list['nominating_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_nominating = false;
							}
							if ($scope.entitySearchResult.list['corporate_governance_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_corporate = false;
							}
							if ($scope.entitySearchResult.list['compensation_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_compensation = false;
							}
						}
					} else {
						var fetcherName = subfetcherData.message.split(' ')[1];
						// entityCompanyEmptyObject[fetcherName] = {};
						if (subfetcherData.fetcher === 'plus.google.com') {
							$scope.entitySearchResult.is_data_not_found.is_googlepost = false;
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
						}
						if (subfetcherData.fetcher === 'profile.linkedin.com') {
							$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							$('#tagcloudCompanyTwitterTags').html('<div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper cloud-tag-ditto"><div class="alert-message-wrapper alert-message">Data Not Found</div></div>');
						}
						if (subfetcherData.fetcher === 'instagram.com') {
							$scope.entitySearchResult.is_data_not_found.is_instagramlist = false;
						}
						if (subfetcherData.fetcher === 'pages.facebook.com') {
							$scope.entitySearchResult.is_data_not_found.is_facebook_list = false;
						}
					}
				});
				if (key === fetcherIds.length - 1) {
					deffered.resolve($scope.entitySearchResult);
				}
			}, function (error) {
				deffered.reject(error.responsemessage);
			});
		});
		// calling web socket
		return deffered.promise;
	}
	function loadTwitterTagsData(twitter_tag_words) {
		if (twitter_tag_words.length > 0) {
			EntityCommonTabService.entityChartSharedObject.tagCloudTwitterTagsOptions.data = twitter_tag_words;
			EnrichSearchGraph.plotTagCloudChartForLiveFeed(EntityCommonTabService.entityChartSharedObject.tagCloudTwitterTagsOptions);
		}
		else {
			$('#tagcloudCompanyTwitterTags').html('<div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper cloud-tag-ditto"><div class="alert-message-wrapper alert-message">Data Not Found</div></div>');
		}
	}
	function InteractionPieChart(value1, value2) {
		if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
			var value1 = $scope.entitySearchResult.list['follower_following_count'][0].twitter_following_number;
			var value2 = $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number;
			var instanceData = [
				{ "key": "FOLLOWING", "doc_count": value1 },
				{ "key": "FOLLOWER", "doc_count": value2 }
			]
			EntityCommonTabService.InitializeandPlotPie(instanceData, "interaction-ratio")
			// pie ends here
		}
		else {
			$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
		}
		// time line starts here
		var activityFinalData = [];
		if ($scope.entityTwitterList.length > 0) {
			angular.forEach($scope.entityTwitterList, function (d, i) {
				activityFinalData.push({
					"time": d.createdOn,
					"amount": Math.floor(Math.random() * 20),
					"type": "twitter",
					'txt': '<table class="table data-custom-table"><tr><th>Title</td><td>' + d.title + '</td></tr><tr><th>createdOn</td><td>' + d.createdOn + '</td></tr><tr><th>RetweetCount</td><td>' + d.retweetCount + '</td></tr><tr><th>Type</td><td>' + d.type + '</td></tr></tr><tr><th>Text</td><td>' + d.text + '</td></tr></table>'
				})
			});
		}
		if ($scope.entityLinkedinList.length > 0) {
			angular.forEach($scope.entityLinkedinList, function (d, i) {
				if (getActualdate(d.postedOn)) {
					activityFinalData.push({
						"time": getActualdate(d.postedOn),
						"amount": Math.floor(Math.random() * 20),
						"type": "linkedin",
						"txt": '<table class="table data-custom-table"><tr><th>PostedOn</td><td> ' + getActualdate(d.postedOn) + '</td></tr><tr><th>CommentsCount</td><td>' + d.commentsCount + '</td></tr><tr><th>LikesCount</td><td>' + d.likesCount + '</td></tr><tr><th>Type</td><td>' + "Linkedin" + '</td></tr></tr><tr><th>Text</td><td>' + d.text + '</td></tr></table>'
					})
				}
			});
		}
		if ($scope.entityFacebookList.length > 0) {
			// no count
			angular.forEach($scope.entityFacebookList, function (d, i) {
				activityFinalData.push({
					"time": d.created_time,
					"amount": Math.floor(Math.random() * 20),
					"type": "facebook",
					"txt": '<table class="table data-custom-table"><tr><th>Created time</td><td>' + d.created_time + '</td></tr><tr><th>Risk score</td><td>' + d.risk_score + '</td></tr><tr><th>Type</td><td>Facebook</td></tr><tr><th>Text</td><td>' + d.text + '</td></tr></table>'
				})
			});
		}
		var colors = {
			"twitter": "#a75dc2",
			"linkedin": "#5D97C9",
			"facebook": "#399034"
		};
		var colorsObj = colors;
		var timelineOptions = {
			container: "#activityFeedTimeline",
			height: 50,
			colors: ["#35942E", "#A15EBB", "#46a2de"],
			colorsObj: colorsObj,
			data: activityFinalData
		};
		$timeout(function () {
			loadtimeLineColumnChart(timelineOptions);
			}, 10)
		EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions);
		// time line ends here
		// tag cloud starts here
		loadTwitterTagsData(EntityCommonTabService.entityChartSharedObject.twitter_tag_words);
	}

	// ---------------------------------------Network
	// chart-----------------------------------------------------------//
	function loadtwitterchart() {
		var WebSocketTwitterUrl = EntityCompanyConst.WebSocketTwitterUrl;
		var nodes = [], links = [], tweetList = [], retweetList = [], mentionList = [],
			locationList = [], finalLocationList = [], locationNames = [], randomGeneratedId = Math.floor((Math.random() * 10) + 19999), maxTwitterTagDomainRangeValue = 5;;
		var socket = {};
		$scope.liveFeed = {
			nodeList: []
		};
		function getWebSocketTwitterData(name) {
			var tweetObjectOneOrigin = {
				id: randomGeneratedId,
				name: $scope.entitySearchResult.name,
				screen_name: $scope.entitySearchResult.name,
				text: $scope.entitySearchResult.name,
				profile_image: '',
				createdOn: new Date(),
				type: 'main'
			};
			nodes.push(tweetObjectOneOrigin);
			socket = io.connect(WebSocketTwitterUrl, { 'forceNew': true });
			socket.on('message', function (response) {
				$scope.disableSearchButton = false;
				$('.custom-spinner').css('display', 'none');
				var isString = false;
				if (response.indexOf('{') === -1) {
					isString = true;
				} else {
					response = JSON.parse(response);
					getSourceTargetData(response);
				}
			});
			socket.on('connect', function () {
				socket.send(name);
			});
			socket.on('disconnect', function () {
			});
		}
		/* Initiate twitter live feed socket ui call */
		getWebSocketTwitterData(EntityCommonTabService.basicsharedObject.companyName);
		function getSourceTargetData(tweetData) {
			var linkDataOrigin = {
				source: null,
				target: null
			};
			if (tweetList.indexOf(tweetData.user.screen_name) === -1) {
				var tweetObjectOne = {
					id: tweetData.id,
					name: tweetData.user.name,
					screen_name: tweetData.user.screen_name,
					text: tweetData.text,
					profile_image: tweetData.user.profile_image_url,
					createdOn: new Date(tweetData.user.created_at),
					likeCounts: tweetData.user.favourites_count || 0,
					retweetCounts: tweetData.user.retweet_count || 0,
					replyCounts: tweetData.user.reply_count || 0,
					type: 'tweet'
				};
				EntityCommonTabService.entityChartSharedObject.tagCloudNameList.splice(0, 0, {
					text: tweetData.user.name,
					size: tweetData.user.favourites_count || 5
				});
				if (EntityCommonTabService.entityChartSharedObject.tagCloudNameList.length === 21) {
					socket.close();
					EntityCommonTabService.entityChartSharedObject.tagCloudNameList.pop();
				}
				if (tweetData.user.location !== '' && tweetData.user.location) {
					EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.locationList.push({
						name: tweetData.user.location,
						long: '',
						lat: '',
						mark: 'assets/images/redpin.png'
					});
				}
				nodes.push(tweetObjectOne);
				$timeout(function () {
					$scope.entitySearchResult.tweetCounts++;
					$scope.liveFeed.nodeList.push(tweetObjectOne);
				}, 0);
				tweetList.push(tweetData.user.screen_name);
				linkDataOrigin.source = randomGeneratedId;
				linkDataOrigin.target = tweetData.id;
				links.push(linkDataOrigin);
			}
			if (tweetData.entities['user_mentions'].length > 0) {
				angular.forEach(tweetData.entities['user_mentions'], function (mention, mentionKey) {
					if (mentionList.indexOf(mention.screen_name) === -1) {
						var mentionObject = {
							id: 'mention-' + mention.id,
							name: mention.name,
							screen_name: mention.screen_name,
							type: 'mention'
						};
						$timeout(function () {
							$scope.entitySearchResult.mentionCounts++;
						}, 0);
						nodes.push(mentionObject);
						mentionList.push(mention.screen_name);
					}
					links.push({
						source: tweetData.id,
						target: 'mention-' + mention.id
					});
				});
			}
			var linkData = {
				source: null,
				target: null
			};
			linkData.source = tweetData.id;
			if (_.isObject(tweetData.retweeted_status)) {
				if (retweetList.indexOf(tweetData.retweeted_status.user.screen_name) === -1) {
					var tweetObject = {
						id: tweetData.retweeted_status.id,
						name: tweetData.retweeted_status.user.name,
						screen_name: tweetData.retweeted_status.user.screen_name,
						text: tweetData.retweeted_status.text,
						profile_image: tweetData.retweeted_status.user.profile_image_url,
						createdOn: new Date(tweetData.retweeted_status.user.created_at),
						likeCounts: tweetData.retweeted_status.user.favourites_count || 0,
						retweetCounts: tweetData.retweeted_status.user.retweet_count || 0,
						replyCounts: tweetData.retweeted_status.user.reply_count || 0,
						type: 'retweet'
					};
					EntityCommonTabService.entityChartSharedObject.tagCloudNameList.splice(0, 0, {
						text: tweetData.retweeted_status.user.name,
						size: tweetData.retweeted_status.user.favourites_count || 5
					});
					if (EntityCommonTabService.entityChartSharedObject.tagCloudNameList.length === 21) {
						EntityCommonTabService.entityChartSharedObject.tagCloudNameList.pop();
					}
					if (tweetData.retweeted_status.user.location !== '' && tweetData.retweeted_status.user.location) {
						EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.locationList.push({
							name: tweetData.retweeted_status.user.location,
							long: '',
							lat: '',
							mark: 'assets/images/redpin.png'
						});
					}
					nodes.push(tweetObject);
					$timeout(function () {
						$scope.entitySearchResult.reTweetCounts++;
						$scope.liveFeed.nodeList.push(tweetObject);
					}, 0);
					retweetList.push(tweetData.retweeted_status.user.screen_name);
				}
				linkData.target = tweetData.retweeted_status.id;
			}
			if (linkData.source && linkData.target)
				links.push(linkData);
			var twitterFinalData = { 'links': links, 'nodes': nodes };
			var twitterFinalDataNew = jQuery.extend(true, [], twitterFinalData);
			$scope.entitySearchResult.list['twitter_socket_network_chart'] = false;
			twitterPlotNetworkChart(twitterFinalDataNew);
		}
		// ----------------------------Twitter Network
		// Chart----------------------------------------
		var tool_tip = $('body').append('<div class="CaseTimeLine_Chart_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		var width = $("#twitter-socket-network-chart").width(),
			height = 300;
		var svg = d3.select("#twitter-socket-network-chart").append("svg")
			.attr("width", width)
			.attr("height", height),
			color = d3.scaleOrdinal(d3.schemeCategory10);
		/* build the arrow. */
		svg.append("svg:defs").selectAll("marker")
			.data(["end"])      /*
									 * Different link/path types can be defined
									 * here
									 */
			.enter().append("svg:marker")    /*
												 * This section adds in the
												 * arrows
												 */
			.attr("id", String)
			.attr("viewBox", "0 -5 10 10")
			.attr("refX", 15)
			.attr("refY", -1.5)
			.attr("markerWidth", 6)
			.attr("markerHeight", 6)
			.attr("orient", "auto")
			.append("svg:path")
			.attr("d", "M0,-5L10,0L0,5")
			.style("fill", "#666");
		var a = { id: "Sarah" },
			b = { id: "Alice" },
			c = { id: "Eveie" },
			d = { id: "Peter" },
			nodesnew = [],
			linksnew = [];
		var simulation = d3.forceSimulation(nodesnew)
			.force("charge", d3.forceManyBody().strength(-300))
			.force("link", d3.forceLink(linksnew).distance(60))
			.force("x", d3.forceX())
			.force("y", d3.forceY())
			.alphaTarget(1)
			.on("tick", ticked);
		var g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
			link = g.append("g").attr("stroke", "#000").attr("stroke-width", 1.5).selectAll(".link"),
			node = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");
		function twitterPlotNetworkChart(data) {
			nodesnew = data.nodes;
			linksnew = [];
			data.links.forEach(function (d) {
				linksnew.push({
					"source": nodesnew[nodesnew.findIndex(function (el) { return el.id == d.source; })],
					"target": nodesnew[nodesnew.findIndex(function (el) { return el.id == d.target })]
				});
			});
			restart();
		}
		restart();
		var colornew;
		var radius;
		function restart() {
			/* legend(nodesnew); */
			/* Apply the general update pattern to the nodes. */
			node = node.data(nodesnew, function (d) {
				return d.id;
			});
			node.exit().remove();
			node = node.enter().append("circle").attr("fill", function (d) {
				if (d.type == "main") {
					colornew = "#000080"/* blue */
				}
				else if (d.type == "tweet") {
					colornew = "#FFD700"/* yellow */
				}
				else if (d.type == "mention") {
					colornew = "#800000"
				}
				else if (d.type == "retweet") {
					colornew = "#008000"/* green */
				}
				return colornew;
			}).attr("r", function (d) {
				if (d.type == "main") {
					radius = "10"/* blue */
				}
				else {
					radius = "6"/* green */
				}
				return radius;
			})
				.on("mouseover", function (d) {
					var appendHTMLTooltip = '';
					if (d.type === 'tweet' || d.type === 'retweet')
						appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' + d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' + d.name + '</p><p><i class="fa fa-retweat icon"></i>' + d.text + '</p><p class="progress-text"><i class="fa fa-wechat icon"></i><span>' + d.replyCounts + '</span><img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" /><span>' + d.retweetCounts + '</span><i class="fa fa-heart icon"></i><span>' + d.likeCounts + '</span></p></div>';
					else
						appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' + d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' + d.name + '</p></div>';
					$(".CaseTimeLine_Chart_tooltip").html(appendHTMLTooltip);
					return $(".CaseTimeLine_Chart_tooltip").css("display", "block");
				}).on("mousemove", function (event) {
					var value = $(this).offset();
					var top = value.top
					var left = value.left + 20
					$(".CaseTimeLine_Chart_tooltip").css("top", top + "px")
					return $(".CaseTimeLine_Chart_tooltip").css("left", left + "px");
				}).on("mouseout", function () {
					/* $(this).css("opacity", 0.4); */
					/* hide tool-tip */
					return $(".CaseTimeLine_Chart_tooltip").css("display", "none");
				}).merge(node);
			/* Apply the general update pattern to the links. */
			link = link.data(linksnew, function (d) {
				return d.source.id + "-" + d.target.id;
			});
			link.exit().remove();
			link = link.enter().append("line").merge(link);
			/* Update and restart the simulation. */
			simulation.nodes(nodesnew);
			simulation.force("link").links(linksnew);
			simulation.alpha(1).restart();
		}
		function legend(nodesnew) {
			$(".legend").empty()
			var type = [];
			nodesnew.forEach(function (d) {
				type.push(d.type)
			})
			var outputArray = [];
			for (var i = 0; i < type.length; i++) {
				if ((jQuery.inArray(type[i], outputArray)) == -1) {
					outputArray.push(type[i]);
				}
			}
			var legendcolor;
			outputArray.forEach(function (d) {
				if (d == "main") {
					d = "Search query"
					legendcolor = "#000080"
				} else if (d == "tweet") {
					legendcolor = "#FFD700"
				}
				else if (d == "mention") {
					legendcolor = "#800000"
				}
				else if (d == "retweet") {
					legendcolor = "#008000"
				}
				else {
					legendcolor = "#000"
				}
				$(".legend").append('<div style="display: inline-block;vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;text-transform: capitalize;margin: 0 10px 0 0">' + d + '</span><span style="margin: 0 10px 0 0;vertical-align: middle;border-radius: 50%;display: inline-block;width: 15px;height: 15px;color: #435561;font-size: 12px;line-height: 20px;text-align: center;padding: 1px 4px;border: 2px solid #435561;background-color: ' + legendcolor + '"></span></div>')
			})
			return $(".legend").css("display", "none");
		}
		function ticked() {
			node.attr("cx", function (d) {
				return d.x;
			})
				.attr("cy", function (d) {
					return d.y;
				})
			link.attr("x1", function (d) {
				return d.source.x;
			})
				.attr("y1", function (d) {
					return d.source.y;
				})
				.attr("x2", function (d) {
					return d.target.x;
				})
				.attr("y2", function (d) {
					return d.target.y;
				}).attr("marker-end", "url(#end)");
		}
	}
}