'use strict';
angular.module('ehubEntityApp')
.controller('EntityPersonNewController', entityPageNewController);

	entityPageNewController.$inject =[
		'$scope',
		'$http',
		'$window',
		'$location',
		'$filter',
		'$rootScope',
		'$state',
		'$stateParams',
		'EntityApiService',
		'$q',
		'UnWantedKeyList',
		'EntityGraphService',
		'$uibModal',
		'EnrichSearchGraph',
		'$timeout',
		'EntityCompanyConst',
		'handlingPersonConstantName',
		'mockedCustomerConst',
		'mockedCompany',
		'chartsConst',
		'worldCountryDetailList'
	];
	function entityPageNewController(
			$scope,
			$http,
			$window,
			$location,
			$filter,
			$rootScope,
			$state,
			$stateParams,
			EntityApiService,
			$q,
			UnWantedKeyList,
			EntityGraphService,
			$uibModal,
			EnrichSearchGraph,
			$timeout,
			EntityCompanyConst,
			handlingPersonConstantName,
			mockedCustomerConst,
			mockedCompany,
			chartsConst,
			worldCountryDetailList) {
//dummy code for CDD starts
$scope.screeningData = [];
var tempscreeningData =[];
		if(($stateParams.query).toUpperCase() == "BERNARD MADOFF" ||  (  $stateParams.query).toUpperCase() == "BARNY MADDOF" || ($stateParams.query).toUpperCase() == "BERNARD LAWRENCE MADOFF"){
			 $scope.personOccupatinAndIndustry = mockedCompany.personOccupatinAndIndustry;
			 $scope.affliatedEntities = mockedCompany.affliatedEntities;
			 $scope.identificationInfo = mockedCustomerConst.identificationInfo;
			 $scope.sourceUrl = mockedCustomerConst.sourceUrl;
			tempscreeningData = mockedCustomerConst.relationshipInfo;
			 $scope.identificationInfoLength =  Object.keys($scope.identificationInfo).length;
		}else{
			 $scope.identificationInfo = {};
			 $scope.identificationInfoLength =  Object.keys($scope.identificationInfo).length;
			 $scope.sourceUrl = '';
			 $scope.screeningData = [];
		}
		$scope.identifiedRisk = 0;
	$scope.token = $rootScope.ehubObject.token;
	/*risk score init*/
	$scope.entityTransactionalRisk = 0;
	$scope.entityDirectRisk = 0;
	$scope.entityIndirectRisk = 0;

	$scope.forex = Math.floor((Math.random() * 100) + 1);
	$scope.equities = Math.floor((Math.random() * 100) + 1);
	$scope.banking = Math.floor((Math.random() * 100) + 1);
	$scope.marketing = Math.floor((Math.random() * 100) + 1);
	$scope.finance = Math.floor((Math.random() * 100) + 1);

	$scope.reliable = Math.floor((Math.random() * 100) + 1);
	$scope.intelligent = Math.floor((Math.random() * 100) + 1);
	$scope.highlyskilled = Math.floor((Math.random() * 100) + 1);
	$scope.trustworthy = Math.floor((Math.random() * 100) + 1);

	$scope.overallsentiment = "POSITIVE";
	var vlaDataArr = {};

	function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

	function getCapitalizedName(companyName){
		var splitedNames = companyName.split(' ');
		var resultedName = '';
		if(splitedNames.length > 0){
			angular.forEach(splitedNames, function(value, key){
				resultedName += value.charAt(0).toUpperCase() + value.slice(1) + ' ';
			});
		} else {
			resultedName = companyName.charAt(0).toUpperCase() + companyName.slice(1);
		}
		return resultedName.trim();
	}
	
	/*
	 * @purpose:Show more data in model @created: 8 jan 2018 @params:
	 * data(string) @return: no @author: varsha
	 */
	$window.popUpdata = function(data){
				var dataModal = $uibModal.open({
					templateUrl: '../scripts/common/modal/views/dataPopUp.modal.html',
			    	controller: 'DataModalController',
					size: 'xs',
					// backdrop: 'static',
					windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',
					resolve: {
						data: function(){
							return data;
						}
					}
				});
				dataModal.result.then(function(response){

				}, function(reject){
				});

	};
	
	$scope.loadLinkAnalysis =function(isadverse, VLAId){
		var indexOfbackSlash = window.location.href.split("/#!/")[0].lastIndexOf('/');
		var entitySearchResult = $scope.entitySearchResult.name?$scope.entitySearchResult.name:searchText;
		var url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/linkAnalysis?q="+entitySearchResult;
		if(isadverse != ''){
			url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/linkAnalysis?q="+entitySearchResult+'&p=Adverse News&type=company';
		}
		var newWindow = window.open(url, '_blank');
		
		newWindow.current_data = jQuery.extend(true, {}, vlaDataArr[VLAId]);
	}
	
	function nFormatter(num, digits) {
	  var si = [
	    { value: 1E18, symbol: "E" },
	    { value: 1E15, symbol: "P" },
	    { value: 1E12, symbol: "T" },
	    { value: 1E9,  symbol: "G" },
	    { value: 1E6,  symbol: "M" },
	    { value: 1E3,  symbol: "k" }
	  ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
	  for (i = 0; i < si.length; i++) {
	    if (num >= si[i].value) {
	      return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
	    }
	  }
	  return num.toFixed(digits).replace(rx, "$1");
	}
/*
	function convertToNumberFromNumberic(num) {
	  var si = [
	    { value: 1E18, symbol: "E" },
	    { value: 1E15, symbol: "P" },
	    { value: 1E12, symbol: "T" },
	    { value: 1E9,  symbol: "G" },
	    { value: 1E6,  symbol: "M" },
	    { value: 1E3,  symbol: "K" }
	  ], i, lastKey = '', number = 0;
	  lastKey = num.slice(-1);
	  for (i = 0; i < si.length; i++) {
	    if (lastKey.toUpperCase() === si[i].symbol) {
	      number = (number * si[i].value);
	    }
	  }
	  return number;
	} */
	var searchText = getCapitalizedName($stateParams.query);
	var unscaledIR = (searchText + 'indirect').toUpperCase(),
		unscaledDR = (searchText + 'direct').toUpperCase(),
		unscaledTR = (searchText + 'transaction').toUpperCase();
	var searchTextRiskArray = [unscaledIR, unscaledDR, unscaledTR];
	var  risk = 0;
	$scope.scaledNumArr = [];

	var scale = d3.scaleBand().domain(searchTextRiskArray).range([40,80]);

	for(var j=0, length = searchTextRiskArray.length; j<length; j++){
		risk = (scale(searchTextRiskArray[j])) + searchText.length;
		if(risk > 80){
			risk = risk - searchText.length;
		}
		$scope.scaledNumArr.push(risk);
	};
	/*--------------------------------------------------------------------------------------------------------------*/

	$scope.readmoreModal = {
			financePersonModal:financePersonModal,
			overviewPersonModal: overviewPersonModal,
			riskOffenceModal: riskOffenceModal,
			socialFollwersModal:socialFollwersModal,
			whoamiModal:whoamiModal
	};

	 /*start entity person page code*/
	 $scope.disableSearchButton = true;
	 var entityPersonEmptyObject = {}, entityPersonSearchObject = {}, locationList = [];

	 var bloombergStockLineChartData = {};
	 $scope.personDetailList = [];
	 $scope.relatedCrimeTab = 'interpol';

	/*tag cloud variables initializations for persons*/
    var tagCloudPersonOptions = {
        header: "MY ENTITIES",
        container: "#tagcloudCompanyPerson",
        height: 300,
        width: $('#tagcloudCompanyPerson').width(),
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        },
        domain: {
        	x: 1,
        	y: 100
        }
    }, tagCloudNameList = [], maxPersonDomainRangeValue = 5;

    /*tag cloud variables initializations for organization*/
    var tagCloudOrganizationOptions = {
        header: "MY ENTITIES",
        container: "#tagcloudCompanyOrganization",
        height: 310,
        width: $('#tagcloudCompanyOrganization').width(),
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        },
        domain: {
        	x: 1,
        	y: 100
        }
    }, tagCloudOrganizationNameList = [], maxOrganizationDomainRangeValue = 5;

    /*tag cloud variables initializations for skills*/
    var tagCloudSkillsOptions = {
        header: "MY ENTITIES",
        container: "#skills-tag-cloud-chart",
        height: 310,
        width: $('#skills-tag-cloud-chart').width(),
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        },
        domain: {
        	x: 1,
        	y: 100
        }
    }, tagCloudSkillsNameList = [], maxSkillsDomainRangeValue = 5;

	 $scope.entitySearchResult = {
	 	 twitter_retweets_count: 0,
		 list: {
			 recomendations: [],
			 previous_work_places: [],
			 skills: [],
			 has_education: [],
			 has_certificates: [],
			 has_degree: [],
			 interests: [],
			 coworkers: [],
			 people_details: [],
			 employment_history: [],
			 news_deep_web: [],
			 twitterList: [],
			 googlePlusList: [],
			 instagramList: [],
			 supplierblacklist: {},
			 boardmemberList: [],
			 keyExecutiveList: [],
			 following: [],
			 followers: [],
			 denied_personlist: [],
			 organizations: [],
			 thumbnails: [],
			 crimeList: {
				 interpol: []
			 },
			 hunterData: [],
			 open_payments: [],
			 exclusions: {},
			 scam: [],
			 unlimitedcriminalchecks: [],
			 'crimestoppers_uk': [],
			 badbuyerslist: [],
			 imsasllc: [],
			 imsasllc_offenses: [],
			 memberships: [],
			 board_memberships: [],
			 fbi_data: [],
			 sanctionssearch: [],
			 sci_ccc_nashville_gov : [],
			 nashville_courtdetails:{
			 	'appearance_details': [],
			 	'court_costs': [],
			 	'probation' :[],
			 	'incarceration' :[],
			 	'disposition' :[]
			 },
			 citizenship : [],
			 openpayment_cms : {
			 	speciality : [],
			 	openpayments_trans : []
			 },
			 'follower_following_count' :  [],
			 'sex-offenders': [],
			 'government_positions': {},
			 'personal_profiles_bloomberg': {
				'honorary_digrees' : []
			 },
			 'board_membership_counts_data': [],
			 'government_positions_list': [],
			 'similar_profiles_list': [],
			 'adverse_news': [],
			 'pep': [],
			 'sanctionList': [],
			 'document_list': [],
			 'videos_list': [],
			 'key_staff_attended_schools': [],
			 'key_relationships': [],
			 'associated_persons': {
				 'connection': []
			 },
			 'associated_companies': {
				'memberships': [],
				'transaction': [],
				'politicians': [],
				'political_organizations': [],
				'holdings': [],
				'donation_grant_recipients': [],
				'business_positions': [],
				'family': [],
				'friends': [],
				'professional_associates': [],
				'education': [],
			 	'connected_industries': []
			 },
			 'corporationwiki': {
				 'name': '',
				 'location': '',
				 'current_job': '',
				 'known_addresses': [],
				 'connected_companies': [],
				 'connecitons': [],
				 'previous_job': []
			 },
			 'truecaller': [],
			 'parents': [],
			 'relatives': [],
			 'born_place': '',
			 'died' : '',
			 'board_member_of': [],
			 'person_transaction_list': [],
			 'competitor_compensation': [],
			 'financial_data': {},
			 'stock_options': {},
			 'dividents': [],
			 'instagramfollowersCountNumberic': 0,
			 'instagramListConnectionNumberic': 0,
			 'followers_numberic': 0,
			 'is_overview_location_chart': true,
			 'is_document_location_chart': true,
			 'is_career_location_chart': true,
			 'is_overview_entities_tag_chart': true,
			 'is_role_based_pie_chart': true,
			 'is_skills_tag_cloud_chart': true,
			 'academic-based-pie-chart' :true,
			 'is_overview_social_bubble_chart': true,
			 'is_twitter_follower_trends': true,
			 'is_twitter_interaction_ratio': true,
			 'twitter_socket_network_chart': true,
			 'is_twitter_tag_chart': true,
			 'is_person_popular_chart': true,
			 'is_assets_ration_pie_chart': true,
			 'is_academic_timelinechart': true,
			 'is_career_timelinechart': true,
			 'is_transaction_type_pie_chart': true,
			 'is_transaction_amount_type_pie_chart': true,
			 'is_holding_pie_chart': true,
			 'is_person_academic_location_chart': true,
			 'is_connection_role_based_pie_chart': true,
			 'is_bubble_compittitors_finance_chart': true,
			 'is_person_social_location_chart': true,
			 'is_risk_alters_pie_chart': true,
			 'is_traffic_by_countries': true,
			 'is_search_keywords': true,
			 'is_traffic_by_domain': true,
			 'is_interest_ratio' :true,
			 'is_donation_ratio' : true,
			 'is_org_donation_ratio' :true,
			 'is_earning_ratio_pie_chart': true,
			 'is_assets_trend_line_chart' : true,
			 geni :{
				family:[],
				timeline:[]
			 },
			 forbes: {
			 	details: []
			 }
		 },
	 'stocks':{
			 'bloomberg_prop':{},
			 'change':{}
		 },
		 data_not_found:{
			instagram :true,
			relatedentity:true,
			socialmedia_follower :true,
			gov_people:true,
			other_membership :true,
			compensation: true,
			holdings: true,
			carrerProgression :true,
			role_ratio :true,
			professional_connection :true,
			academic_timeline:true,
			academic_background :true,
			skill_chart :true,
			google_plus:true,
			competitor_compensation_data :true,
			risk_ratio_data:true,
			donation_politician_data :true,
			data_adverse_news:true,
			deepweb :true,
			recent_tweets :true,
			recent_follower :true,
			tra_by_countries :true,
			sear_keywords :true,
			tra_by_domain :true,
			academicinfo :true,
			amount_ratio: true,
			transaction_types: true,
			asserts_ratio: true,
			familydata :true,
			documents_data: true,
			twitter_tags: true,
			twitter_fol_trend: true,
			twitter_inter_ratio: true,
			earning_ratio: true,
			dividents_data :true,
			stocks_prices: true,
			schooling :true,
			holding_ratio : true,
			awards_data :true,
			hasCertificates :true,
			honoray_data :true,
			initial_supplierblacklist :true,
			supplier_data :true,
			video_data :true,
			asset_data :true,
			interest_data :true,
			popular_tags: true,
			Similarprofile: true,
			coworker_data :true,
			blacklist:true
		 }
	 };
	 var data_parent = true;
	 var littlesis_family = true;
	 var litllesis_friends = true;
	 var data_relative = true;
	 var twitter_followercount = 0;
 	 $scope.socialMediaActiveCount = 0;
	 $scope.socialMediaTab = 'twitter';
	 $scope.relatedPersonTab = 'coworkers';
	 var websiteOfCompanyFounder = '';

	 var roleDataForPieChart = [], activitySocialData = [], assetsRatioPieData = [], assetsTrendLineChart = [], activityAcademicData= [],
	 earningRatioPieChart = [];
	 var activityCareerData =[], professionalConnectionForPieChart = [];
	 var acdemicDataForPieChart =[];
	 var competitors_data = [];
	 var monthNames = ["January", "February", "March", "April", "May", "June",
	  "July", "August", "September", "October", "November", "December"
	 ];
	 var bloombergChartOptions = {
	    container:"#bloomberg-line-chart",
	    height: '350',
	    width: ($(window).width() - 325) / 2,
	    uri :"../vendor/data/bloombergdata.json",
	    data: {}
	 };

	 var bloombergChartDetailsOptions = {
	    container:"#bloomberg-line-chart-details",
	    height: '400',
	    width: $('#bloomberg-line-chart-details').width(),
	    uri :"../vendor/data/bloombergdata.json",
	    data: {}
	 };
	 var worldChartOptions={
	    container :"#person-location-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#person-location-chart').width(),
	    markers: {}
	 };

	 var documentWorldChartOptions={
	    container :"#documents-locations-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#documents-locations-chart').width(),
	    markers: {}
	 };

	 var transactionWorldChartOptions={
	    container :"#transactions-locations-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#transactions-locations-chart').width(),
	    markers: {}
	 };

	 var worldCareerChartOptions={
	    container :"#person-career-location-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#person-career-location-chart').width(),
	    markers: {}
	 };

	 var worldAcademicChartOptions={
	    container :"#person-academic-location-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#person-academic-location-chart').width(),
	    markers: {}
	 };
	  var worldSocialChartOptions={
	    container :"#person-social-location-chart",
	    uri1 : "../vendor/data/worldCountries.json",
	    uri2 :"../vendor/data/worldMapData.json",
	    height:400,
	    width:$('#person-social-location-chart').width(),
	    markers: {}
	 };

	 var tagCloudTwitterTagsOptions = {
        header: "MY ENTITIES",
        container: "#tagPersonTwitterTags",
        height: 200,
        width: $("#tagPersonTwitterTags").width(),
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        }
    }, twitter_tag_words = [], maxTwitterTagDomainRangeValue = 5;

    var popularTagsOptions = {
        header: "MY ENTITIES",
        container: "#personPopularTags",
        height: 200,
        width: $("#personPopularTags").width(),
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        }
    };

	 var bubbleSocialOptions = {
        "container": "#socialBubbleChart",
        "height": 310,
        data: {
        	name: '',
        	children: []
        }
     };

	 var bubbleCompittitorsFinanceOptions = {
		'container': '#bubbleCompittitorsFinanceChart',
		'height': 310,
		data: {
			name: '',
			children: []
		}
	 };
	 /*variable initialization for social activity feed*/
	 var colorsSocial = {
   	       "twitter":"#a75dc2",
   	       "google plus":"#5D97C9",
   	       "instagram":"#399034"
   	      };
   	var academicSocial = {
   	       "degree":"#399034"
   	      };
   	var carrerRole = {
   	       "role":"#399034"
   	      };
	 var timelineSocialOptions = {
        container: "#activityFeedTimeline",
        height: 50,
        colors: ["#35942E","#A15EBB","#46a2de"],
        colorsObj: colorsSocial,
        data: {}
	};
	var timelineAcademics ={
        container: "#academic-timeline",
        height: 50,
        colors: ["#35942E","#A15EBB","#46a2de"],
        colorsObj: academicSocial,
        data: {}
	};
	var timelineCareerRole ={
        container: "#Career-timeline",
        height: 50,
        colors: ["#35942E","#A15EBB","#46a2de"],
        colorsObj: carrerRole,
        data: {}
	};
	 var mainFetcherIds = ['3','8','1008'], fetcherIds = ['1','2001','2','3','4','5','6','7','19','2005','8','1009','1012','9','10','11','12','13','14','3006','15','16','17','18','19','21','22','23','1020','1023','25','26','27','29'], foundData = false;
	/*19 has not changed as missed in Akthar api*/
	// 28 is removed by ram for slow performance
	 var copyMainFetcherIds = mainFetcherIds;
	 var finalLocationList = [], locationNames = [], academicLocationList = [], careerLocationList = [], sociallocationlist =[];
	 var initateData = {
		fetchers: mainFetcherIds,
		keyword: getCapitalizedName($stateParams.query),
        searchType: 'Person',
        lightWeight: true
	 };
	 if(($stateParams.query).toUpperCase() == "BERNARD MADOFF" ||  (  $stateParams.query).toUpperCase() == "BARNY MADDOF" || ($stateParams.query).toUpperCase() == "BERNARD LAWRENCE MADOFF"){
		$scope.screeningData.push({
			"mdaas:RegisteredAddress": {
			  "streetAddress": "885 Third Avenue",
			  "city": "New York City",
			  "zip": "10022",
			  "fullAddress": "885 Third Avenue New York City 10022"
			},
			"name": "Bernard Lawrence Madoff",
			"sanction_bst:description": "",
			"listId": "4563578",
			"@source-id": "",
			"high_risk_jurisdiction": "",
			"sanction_url": "",
			"sanction_source": "",
			"pep_url": "",
			"$$hashKey": ""
		  });
		 angular.forEach(tempscreeningData,function(v, k){
			 callSanction(v.name, 'person');
		 });
	 }else{
		 callSanction(initateData.keyword, 'person');
	 }
	 	
		//getAdverseNewsByPerson('person',initateData.keyword);
	 EntityApiService.getResolvedEntityDataOld(initateData).then(function(response) {
		 var resolvedResponse = [], resolvedName = '', parentIndex = -1, childIndex = -1;
		 if(response.data.resolved.length > 0 && response.data.resolved[0].entities[0].name !== '' && response.data.resolved[0].entities[0].name)
			 resolvedResponse = response.data.resolved;
		 else if(response.data.sortedResults.length > 0){
     		resolvedName = response.data.sortedResults[0].name;
     		response.data.sortedResults[0].fetcher === 'profiles.bloomberg.com' ? parentIndex = 2: response.data.sortedResults[0].fetcher === 'person.wikipedia.com' ?  parentIndex = 0 : parentIndex = 1;
     		childIndex = response.data.sortedResults[0].index;
     		resolvedResponse.push(response.data.results[parentIndex]);
     	} else
		 	resolvedResponse = response.data.results;
		angular.forEach(resolvedResponse, function (fetcherData, key) {
			/*var resolvedKey = parentIndex !== -1 ? parentIndex : key;*/
            if (fetcherData.status && !foundData) {
            	/*copyMainFetcherIds.splice(resolvedKey, 1);
            	fetcherIds.splice(0,0,copyMainFetcherIds[0]);*/
            	angular.forEach(fetcherData.entities, function (entity, entityKey) {
                    if (((entityKey === 0 && childIndex === -1) || (entityKey === childIndex && childIndex !== -1)) &&
                    		entity.name !== '' && entity.name !== undefined) {
                        foundData = true;
                        $scope.entitySearchResult.name = entity.name.replace(/[^a-zA-Z ]/g, "");
                       onpageLoadCharts();
                        angular.forEach(entity.properties, function(ev, ek){
                        	if(ev !== '' && ev && (ek === 'headquarter' || ek === 'country_of_origin' || ek === 'buyer_nationality' || ek === 'destination_market_of_order_in_question'|| ek === 'location'|| ek === 'address'|| ek === 'country'|| ek === 'company_address'|| ek === 'seller_address'|| ek === 'buyers_country'|| ek === 'buyers_address'|| ek === 'places_lived')){
                        		if(locationList.length === 0) {
                    				locationList.push({
                        				name: ev,
                        				long: '',
                        				lat: '',
                        				mark: 'assets/images/redpin.png',
                        				source: entity.source,
                        				title: entity.properties.title || entity.source
                        			});
	                        	} else {
	                        		var locationIndex = -1;
	                        		angular.forEach(locationList, function(location, locationKey){
	                        			if(ev === location.name)
	                        				locationIndex = locationKey;
	                        		});
	                        		if(locationIndex === -1){
	                        			locationList.push({
                            				name: ev,
                            				long: '',
                            				lat: '',
                            				mark: 'assets/images/redpin.png',
                            				source: entity.source,
                            				title: entity.properties.title || entity.source
                            			});
	                        		}
                        		}
                        	}
                        	!$scope.entitySearchResult.hasOwnProperty(ek) && UnWantedKeyList.indexOf(ek) === -1  && ev !== '' && ev  ? $scope.entitySearchResult[ek] = ev : '';
                        });
                        if(entity.properties['details'] && entity.properties['details'] !== ''){
                        	$scope.personDetailList.push({
                        		text: entity.properties['details'],
                        		source: entity.source
                        	});
                        }
                        if(entity.properties['summary'] && entity.properties['summary'] !== ''){
                        	$scope.personDetailList.push({
                        		text: entity.properties['summary'],
                        		source: entity.source
                        	});
                        }
                        angular.forEach(entity.edges, function(edv, edk) {
                    		if(edv.relationshipName === 'recomendations'){
                    			$scope.entitySearchResult.list["recomendations"].push({
	            					name: edv.entity.edges[0].entity.properties.name,
	            					jobtitle: edv.entity.edges[0].entity.properties.jobtitle,
	            					image: edv.entity.edges[0].entity.properties.image,
	            					details: edv.entity.properties.description,
	            					summary: edv.entity.properties.summary
	            				});
                        	}
                        	if(edv.relationshipName === 'previous_work_places'){
                        		if(edv.entity.properties.jobtitle !== '' && edv.entity.properties.jobtitle && edv.entity.properties.jobtitle !== '.'){
	                    			$scope.entitySearchResult.list["previous_work_places"].push({
		            					title: edv.entity.properties.jobtitle,
		            					company: edv.entity.properties.company,
		            					employed_location: edv.entity.properties.employed_location,
		            					details: edv.entity.properties.details,
		            					link: edv.entity.properties.company_link,
		            					employed_dates: edv.entity.properties.employed_dates
		            				});
	            					var indexRole = -1;
                					angular.forEach(roleDataForPieChart, function(object, key){
                						if(object.key === edv.entity.properties.jobtitle)
                							indexRole = key
                					});
                					if(indexRole === -1){
                						roleDataForPieChart.push({
		            						key: edv.entity.properties.jobtitle,
		            						doc_count: 1
		            					});
                					} else {
                						roleDataForPieChart[indexRole].doc_count++;
                					}
                            		if((edv.entity.properties.company !== '' && edv.entity.properties.company) ||
                            				(edv.entity.properties.employed_location !== '' && edv.entity.properties.employed_location)) {
                                		var locationindex = -1;
                                		for(var i = 0; i < careerLocationList.length; i++){
                                			if((careerLocationList[i].name === edv.entity.properties.employed_location) ||
                                					(careerLocationList[i].name === edv.entity.properties.company))
                                				locationindex = i;
                                		}
                                		if(locationindex === -1){
                                			careerLocationList.push({
    	                        				name: edv.entity.properties.employed_location !== '' && edv.entity.properties.employed_location ? edv.entity.properties.employed_location : edv.entity.properties.company,
    	                        				long: '',
    	                        				lat: '',
    	                        				mark: 'assets/images/redpin.png',
    	                        				source: edv.source,
    	                        				title: edv.entity.properties.employed_location || edv.entity.properties.company || edv.source
    	                        			});
                                		}
                            		}
                        		}
	                        }
                        	if(edv.relationshipName === 'has_education'){
                        		$scope.entitySearchResult.list["has_education"].push({
                        			school_name: edv.entity.properties.school_name,
                        			type: edv.entity.properties.type_,
                        			url: edv.entity.properties.url,
                        			logo: edv.entity.properties.logo || '',
                        			risk_score: edv.entity.properties.risk_score,
                        			start_time : edv.entity.properties.start_time,
                        			end_time :  edv.entity.properties.end_time,
                        			source: edv.entity.source
	            				});
                        		if(edv.entity.properties.school_name !== '' && edv.entity.properties.school_name) {
                            		var index = -1;
                            		for(var i = 0; i < academicLocationList.length; i++){
                            			if(academicLocationList[i].name === edv.entity.properties.school_name)
                            				index = i;
                            		}
                            		if(index === -1){
                            			academicLocationList.push({
	                        				name: edv.entity.properties.school_name,
	                        				long: '',
	                        				lat: '',
	                        				mark: 'assets/images/redpin.png',
	                        				source: edv.entity.source,
	                        				title: edv.entity.properties.school_name || edv.entity.source
	                        			});
                            		}
								}
                        	}
                        	if(edv.relationshipName === 'interests'){
                        		$scope.entitySearchResult.list["interests"].push({
                        			entity_name: edv.entity.properties.entity_name,
                        			followers_count: edv.entity.properties.followers_count
	            				});
                        	}
                        	if(edv.relationshipName === 'skills'){
                        		edv.entity.name !== '' && edv.entity.name && $scope.entitySearchResult.list["skills"].indexOf(edv.entity.name) === -1 ? $scope.entitySearchResult.list["skills"].push(edv.entity.name): '';
                        		var index = -1;
            					angular.forEach(tagCloudSkillsNameList, function(object, key){
            						if(object.text === edv.entity.name)
            							index = key
            					});
            					if(index === -1){
            						tagCloudSkillsNameList.push({
	            						text: edv.entity.name,
	            						size: 5,
	            						type: 'skills'
	            					});
            					} else {
            						tagCloudSkillsNameList[index].size += 5;
            						tagCloudSkillsNameList[index].size > maxSkillsDomainRangeValue ? maxSkillsDomainRangeValue = tagCloudSkillsNameList[index].size : '';
            					}
                        	}
                        });
                        entityPersonSearchObject[entity.source] = {};
                        entityPersonSearchObject[entity.source].name = entity.name;
                        entityPersonSearchObject[entity.source].edges = entity.edges;
                        entityPersonSearchObject[entity.source].properties = entity.properties;
					}
				});
			//	callSanction($scope.entitySearchResult.name, 'person');
			    getAdverseNewsByPerson('person', $scope.entitySearchResult.name);
				
                /*getting remaining entity data by name*/
				getEntityPersonDataByName($scope.entitySearchResult.name).then(function(response2) {
					$scope.entitySearchResult['skills'] = $scope.entitySearchResult.list["skills"].length > 0 ? $scope.entitySearchResult.list["skills"].join() : '';
					if($scope.entitySearchResult['work_telephone'] || $scope.entitySearchResult.list['employment_history'].length > 0 || $scope.entitySearchResult.list['similar_profiles_list'].length > 0 || $scope.entitySearchResult.list['people_details'].length > 0){
		    			var number = $scope.entitySearchResult['work_telephone'] ? $scope.entitySearchResult['work_telephone'] :
		    				$scope.entitySearchResult.list['employment_history'].length > 0 ? ($scope.entitySearchResult.list['employment_history'][0].phone !== '' && $scope.entitySearchResult.list['employment_history'][0].phone) ? $scope.entitySearchResult.list['employment_history'][0].phone :
		    					$scope.entitySearchResult.list['similar_profiles_list'].length > 0 ? ($scope.entitySearchResult.list['similar_profiles_list'][0].phone !== '' && $scope.entitySearchResult.list['similar_profiles_list'][0].phone) ? $scope.entitySearchResult.list['similar_profiles_list'][0].phone :
		    						$scope.entitySearchResult.list['people_details'].length > 0 ? ($scope.entitySearchResult.list['people_details'][0].telephone !== '' && $scope.entitySearchResult.list['people_details'][0].telephone) ? $scope.entitySearchResult.list['people_details'][0].telephone : '' : '' : '' : '';
    					number !== '' && number ? getTruecallerPersonDetails(number) : '';
		    		}else{
		    			//code for audit list to save in local-storage
		    	         /* var auditList =[];
		    	         if($localStorage.get('auditList')){
		    	         	auditList = JSON.parse($localStorage.get('auditList'));
		    	         } */
		    	         var copyOfName = $scope.entitySearchResult.name;
		    			 var names = copyOfName.split(' ');
		    			 var searchString="Searched for "+$scope.entitySearchResult.name+" on linkedin.com,openpaymentsdata.cms.gov, twitter.com, zabasearch.com, spokeo.com, Interpol wanted list, sanctions_wrapper, sanctionssearch.ofac.treas.gov, deepdotweb.com, bloomberg.com profile search, bloomberg stocks search, unlimitedcriminalchecks.com, fbi.gov, denied_personlist.gov, https://data.cityofchicago.org/Public-Safety/Sex-Offenders/vc9r-bqvy, crimestoppers-uk.org,https://www.ptc-top.com/2016/01/21/ptc-scam-list-the-greatest-in-the-web-update-daily/, supplierblacklist.com, wikipedia.com, imsasllc.com, people.gov.uk, instagram.com, person.corporationwiki.com, google plus, google searchVyoutube.com, personal-profiles.bloomberg.com, littlesis.com  "
		    			 if( $scope.entitySearchResult.current_current_work_place &&
		    					 $scope.entitySearchResult.current_current_work_place !== '') {
		    				 var currentCompany = $scope.entitySearchResult.current_current_work_place.trim().replace(/\↵/g, '');
		    				/*  auditList.push({
				    	         	title: 'Searched for workplace',
				    	         	timestamp: Date.now(),
				    	         	name: $scope.entitySearchResult.name,
				    	         	description: " Searched for "+($scope.entitySearchResult.name+','+currentCompany)+" on hunter.io, littlesis.com, exclusions.oig.hhs.gov "
		    				 }); */
		    				 }else{
		    				 searchString = searchString+"hunter.io,exclusions.oig.hhs.gov";
		    			 }

		    	        /*  auditList.push({
		    	         	title: 'Searched for '+ $scope.entitySearchResult.name,
		    	         	timestamp: Date.now(),
		    	         	name: $scope.entitySearchResult.name,
		    	         	description: searchString
		    	         });
		    	         $localStorage.put('auditList', JSON.stringify(auditList)); */
		    	         //code for audit ends here
		    		}
				    angular.forEach(locationList, function(locationValue, locationKey) {
			        	if(locationNames.indexOf(locationValue.name) === -1) {
			        		locationNames.push(locationValue.name);
			        		
				        	finalLocationList.push({
				        		name: locationValue.name,
                				long: '',
                				lat: '',
                				mark: 'assets/images/redpin.png',
                				source: locationValue.source,
                				title: locationValue.title,
                				txt: '<span>Name: '+locationValue.name +'</span></br><span>Title: '+locationValue.title+'</span>'
				        	});
			        	}
			        });
			        worldChartOptions.markers = finalLocationList;
			        
			   	 	/*calling world map chart function*/
			        World(worldChartOptions);

			        documentWorldChartOptions.markers = '';
			   	 	/*calling world map chart function*/
			        World(documentWorldChartOptions);

			        transactionWorldChartOptions.makers = '';
			        World(transactionWorldChartOptions);

			        /*getting tag cloud chart for related organization*/
					loadRelatedOrganizationData();
					
			     //   getAdverseNewsByPerson('person', $scope.entitySearchResult.name);
			      //  callSanction($scope.entitySearchResult.name, 'person');

			        $scope.entitySearchResult.list['person_transaction_list'] = $scope.entitySearchResult.list['person_transaction_list'].sort(function(a,b){
        				return moment(new Date(a.date)).isBefore(new Date(b.date)) ? 1 : -1;
					});
			        
					if($scope.entitySearchResult.person_more_info && $scope.entitySearchResult.person_more_info.website){
						websiteOfCompanyFounder =$scope.entitySearchResult.person_more_info.website;
						var str=$scope.entitySearchResult.person_more_info.website.search("http");
						$scope.entitySearchResult.person_more_info.website = str === -1 ? 'http://' +$scope.entitySearchResult.person_more_info.website :$scope.entitySearchResult.person_more_info.website;
					}else if($scope.entitySearchResult.website){
						websiteOfCompanyFounder = $scope.entitySearchResult.website;
						var str=$scope.entitySearchResult.website.search("http");
						$scope.entitySearchResult.website = str === -1 ? 'http://' +$scope.entitySearchResult.website :$scope.entitySearchResult.website;
					}else if($scope.entitySearchResult.list['similar_profiles_list'] && $scope.entitySearchResult.list['similar_profiles_list'][0]&& $scope.entitySearchResult.list['similar_profiles_list'][0].website){
						websiteOfCompanyFounder = $scope.entitySearchResult.list['similar_profiles_list'][0].website;
						var str=$scope.entitySearchResult.list['similar_profiles_list'][0].website.search("http");
						$scope.entitySearchResult.list['similar_profiles_list'][0].website = str === -1 ? 'http://' +$scope.entitySearchResult.list['similar_profiles_list'][0].website :$scope.entitySearchResult.list['similar_profiles_list'][0].website;
					}else {
						websiteOfCompanyFounder = '';
					}
					//websiteOfCompanyFounder = ($scope.entitySearchResult.person_more_info.website !=='' ?  $scope.entitySearchResult.person_more_info.website : $scope.entitySearchResult.website !=='' ? $scope.entitySearchResult.website :  $scope.entitySearchResult.list['similar_profiles_list'][0].website !== '' ?  $scope.entitySearchResult.list['similar_profiles_list'][0].website) : ''; 
					websiteOfCompanyFounder !== '' ? getAlexaDetailsByWebsite(websiteOfCompanyFounder) : '';
					if ($scope.entitySearchResult.list["has_education"].length === 0) {
						$scope.entitySearchResult.data_not_found.academicinfo = false;
					}
					$scope.disableSearchButton = false;
					loadtwitterTags();
				}, function(){
					
				});
            } else {
            	if(!fetcherData.status) {
            		var fetcherName = fetcherData['fetcher'];
            		fetcherName !== '' && fetcherName ? entityPersonEmptyObject[fetcherName] = {} : '';
            	}
            }
		});
       /*  console.log('Final result segregated data: ', $scope.entitySearchResult);
        console.log('Final result of each fetcher data: ', entityPersonSearchObject);
        console.log('Empty result of each fetcher data: ', entityPersonEmptyObject);
        console.log('Bloomberg Stock LineChart Data: ', bloombergStockLineChartData); */
	 }, function(error){
		//console.log(error);
	 });

	 /*
	  * @purpose: getting entity data by name
	  * @created: 28 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	  */
	 function getEntityPersonDataByName(name) {
		 var deffered = $q.defer();
		 var copyOfName = name;
		 var names = copyOfName.split(' ');
		 angular.forEach(fetcherIds, function(id, key){
			 var ids = [];
			 ids.push(id);
			 var data = {
	            fetchers: ids,
	            keyword: name,
	            searchType: 'Person'
			 };
			 if((ids.indexOf('5') !== -1 || ids.indexOf('36') !== -1 || ids.indexOf('41') !== -1) && $scope.entitySearchResult.current_current_work_place &&
					 $scope.entitySearchResult.current_current_work_place !== '') {
				 var currentCompany = $scope.entitySearchResult.current_current_work_place.trim().replace(/\↵/g, '');
				 currentCompany !== '' && currentCompany ? data.keyword += ',' + currentCompany : '';
			 }
			 if(ids.indexOf('18') !== -1) {
				 if(names.length === 1)
					 data.keyword = names[0];
				 else
					 data.keyword = names[names.length - 1];
			 }
			 if(ids.indexOf('39') !== -1 || ids.indexOf('54') !== -1 || ids.indexOf('57') !== -1) {
				 if(names.length === 1)
					 data.keyword = names[0];
				 else
					 data.keyword = names[0] + ',' +names[names.length - 1];
			 }
			 if(ids.indexOf('40') !== -1 || ids.indexOf('52') !== -1 || ids.indexOf('20') !== -1 || ids.indexOf('45') !== -1) {
				 data.keyword = names[0];
			 }
			 var temp =	_.find(handlingPersonConstantName, { 'name': data.keyword, 'fetcher': id });
			 data.keyword = temp === undefined ? data.keyword : temp.alt_name;
			 data.limit = temp === undefined ? data.limit : temp.limit;
			 EntityApiService.getEntityDataByTextId(data).then(function(subResponse) {
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
		            if (subfetcherData.status) {
		            	 angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
		            		 	if(subentity.properties['details'] && subentity.properties['details'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['details'],
		                        		source: subentity.source
		                        	});
		                        }
		            		 	if(subentity.properties['early_life'] && subentity.properties['early_life'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['early_life'],
		                        		source: subentity.source
		                        	});
		                        }
		            		 	if(subentity.properties['personal_life'] && subentity.properties['personal_life'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['personal_life'],
		                        		source: subentity.source
		                        	});
		                        }
		            		 	if(subentity.properties['user_experience'] && subentity.properties['user_experience'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['user_experience'],
		                        		source: subentity.source
		                        	});
		                        }
		            		 	if(subentity.properties['user_recommendations'] && subentity.properties['user_recommendations'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['user_recommendations'],
		                        		source: subentity.source
		                        	});
		                        }
		                        if(subentity.properties['summary'] && subentity.properties['summary'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['summary'],
		                        		source: subentity.source
		                        	});
		                        }
		                        if(subentity.properties['history'] && subentity.properties['history'] !== ''){
		                        	$scope.personDetailList.push({
		                        		text: subentity.properties['history'],
		                        		source: subentity.source
		                        	});
		                        }
			            	 	angular.forEach(subentity.properties, function(ev, ek){
		                        	if(ev !== '' && ev && subentity.source !== 'person.corporationwiki.com' && subentity.source !== 'sex-offenders.data.cityofchicago.org' && subentity.source !== 'unlimitedcriminalchecks.com' && subentity.source !== 'badbuyerslist.org' && (ek === 'headquarter' || ek === 'country_of_origin' || ek === 'buyer_nationality' || ek === 'destination_market_of_order_in_question'|| ek === 'location'|| ek === 'address'|| ek === 'country'|| ek === 'company_address'|| ek === 'seller_address'|| ek === 'buyers_country'|| ek === 'buyers_address'|| ek === 'places_lived')){
		                        		if(locationList.length === 0) {
	                        				locationList.push({
	                            				name: ev,
	                            				long: '',
	                            				lat: '',
	                            				mark: 'assets/images/redpin.png',
	                            				source: subentity.source,
	                            				title: subentity.properties.title || subentity.source
	                            			});
			                        	} else {
			                        		var locationIndex = -1;
			                        		angular.forEach(locationList, function(location, locationKey){
			                        			if(ev === location.name)
			                        				locationIndex = locationKey;
			                        		});
			                        		if(locationIndex === -1){
			                        			locationList.push({
		                            				name: ev,
		                            				long: '',
		                            				lat: '',
		                            				mark: 'assets/images/redpin.png',
		                            				source: subentity.source,
		                            				title: subentity.properties.title || subentity.source
		                            			});
			                        		}
		                        		 }
		                        	 }
		                        	 !$scope.entitySearchResult.hasOwnProperty(ek) && UnWantedKeyList.indexOf(ek) === -1  && ev !== '' && ev  ? $scope.entitySearchResult[ek] = ev : '';
		                        });
		                        entityPersonSearchObject[subentity.source] = {};
		                        entityPersonSearchObject[subentity.source].name = subentity.name;
		                        entityPersonSearchObject[subentity.source].edges = subentity.edges;
		                        entityPersonSearchObject[subentity.source].properties = subentity.properties;
		            		});
			            	if(subfetcherData.fetcher === 'deepdotweb.com') {
			            		for (var i = 0; i < subfetcherData.entities.length; i++) {
			            			$scope.entitySearchResult.list.news_deep_web.push({
		            					name: subfetcherData.entities[i].properties.author_name,
		            					postedOn: new Date(subfetcherData.entities[i].properties.post_date),
		            					url: subfetcherData.entities[i].properties.author_url,
		            					title: subfetcherData.entities[i].properties.title,
		            					details: subfetcherData.entities[i].properties.details
		            				});
								}
								if($scope.entitySearchResult.list.news_deep_web.length===0){
									$scope.entitySearchResult.data_not_found.deepweb =false;
								}
			            	}
			            	if(subfetcherData.fetcher === 'person.wikipedia.com') {
			            		for(var w0 = 0; w0 < subfetcherData.entities.length; w0++) {
			            			if(w0 === 0){
										$scope.entitySearchResult.person_more_info = subfetcherData.entities[w0].properties;
										$scope.spousecount =  subfetcherData.entities[w0].properties['spouse(s)'] === undefined ? 0 : 1;
										console.log('$scope.entitySearchResult.person_more_info: ', $scope.entitySearchResult.person_more_info);
			            				websiteOfCompanyFounder = subfetcherData.entities[w0].properties.website !== '' && subfetcherData.entities[w0].properties.website ? subfetcherData.entities[w0].properties.website : '';
			            			}
			            			for(var ed0 = 0; ed0 < subfetcherData.entities[w0].edges.length; ed0++){
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'title'){
			            					var titleSplitted = subfetcherData.entities[w0].edges[ed0].entity.name.split(' of ');
			            					if(titleSplitted.length > 0){
				            					$scope.entitySearchResult.list["previous_work_places"].push({
					            					title: titleSplitted[0],
					            					company: titleSplitted[1] || '',
					            					employed_location: '',
					            					details: '',
					            					link: '',
					            					employed_dates: ''
					            				});
				            					var indexRole = -1;
			                					angular.forEach(roleDataForPieChart, function(object, key){
			                						if(object.key === titleSplitted[0])
			                							indexRole = key
			                					});
			                					if(indexRole === -1){
			                						roleDataForPieChart.push({
					            						key: titleSplitted[0],
					            						doc_count: 1
					            					});
			                					} else {
			                						roleDataForPieChart[indexRole].doc_count++;
			                					}
			                            		if(titleSplitted[1] !== '' && titleSplitted[1]) {
			                                		var locationindex = -1;
			                                		for(var i = 0; i < careerLocationList.length; i++){
			                                			if(careerLocationList[i].name === titleSplitted[1])
			                                				locationindex = i;
			                                		}
			                                		if(locationindex === -1){
			                                			careerLocationList.push({
			    	                        				name: titleSplitted[1],
			    	                        				long: '',
			    	                        				lat: '',
			    	                        				mark: 'assets/images/redpin.png',
			    	                        				source: subfetcherData.entities[w0].source,
			    	                        				title: titleSplitted[1] || subfetcherData.entities[w0].source
			    	                        			});
			                                		}
			                            		}
			            					}
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'born'){
			            					$scope.entitySearchResult.list['born_place'] ? $scope.entitySearchResult.list['born_place'] += subfetcherData.entities[w0].edges[ed0].entity.name + ' ' : $scope.entitySearchResult.list['born_place'] = subfetcherData.entities[w0].edges[ed0].entity.name + ' ';
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'parent(s)' && subfetcherData.entities[w0].edges[ed0].entity.name !== '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list['parents'].push(subfetcherData.entities[w0].edges[ed0].entity.name);
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'relatives' && subfetcherData.entities[w0].edges[ed0].entity.name !== '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list['relatives'].push(subfetcherData.entities[w0].edges[ed0].entity.name);
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'alma_mater' && subfetcherData.entities[w0].edges[ed0].entity.name !== '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list["has_education"].push({
		                            			school_name: subfetcherData.entities[w0].edges[ed0].entity.name,
		                            			type: '',
		                            			url: '',
		                            			logo: '',
		                            			risk_score: subfetcherData.entities[w0].edges[ed0].entity.properties.risk_score,
		                            			start_time : '',
		                            			end_time :  '',
		                            			source: subfetcherData.entities[w0].edges[ed0].entity.source
		    	            				});
		            						var index = -1;
                                    		for(var i = 0; i < academicLocationList.length; i++){
                                    			if(academicLocationList[i].name === subfetcherData.entities[w0].edges[ed0].entity.name)
                                    				index = i;
                                    		}
                                    		if(index === -1){
                                    			academicLocationList.push({
        	                        				name: subfetcherData.entities[w0].edges[ed0].entity.name,
        	                        				long: '',
        	                        				lat: '',
        	                        				mark: 'assets/images/redpin.png',
        	                        				source: subfetcherData.entities[w0].edges[ed0].entity.source,
        	                        				title: subfetcherData.entities[w0].edges[ed0].entity.name || subfetcherData.entities[w0].edges[ed0].entity.source
        	                        			});
                                    		}
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'board_member_of' && subfetcherData.entities[w0].edges[ed0].entity.name !== '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list['board_member_of'].push(subfetcherData.entities[w0].edges[ed0].entity.name);
		                					var indexPosition = -1;
		                					angular.forEach($scope.entitySearchResult.list['board_membership_counts_data'], function(object, key){
		                						if(object.position === 'Board Member')
		                							indexPosition = key
		                					});
		                					if(indexPosition === -1){
		                						$scope.entitySearchResult.list['board_membership_counts_data'].push({
				            						position: 'Board Member',
				            						count: 1
												});
		                					} else {
		                						$scope.entitySearchResult.list['board_membership_counts_data'][indexPosition].count++;
		                					}
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'awards_and_recognition' && subfetcherData.entities[w0].edges[ed0].entity.name != '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list['has_certificates'].push({
			            						certificate : subfetcherData.entities[w0].edges[ed0].entity.name
			            					});
			            				}
			            				if(subfetcherData.entities[w0].edges[ed0].relationshipName === 'citizenship' && subfetcherData.entities[w0].edges[ed0].entity.name != '' && subfetcherData.entities[w0].edges[ed0].entity.name){
			            					$scope.entitySearchResult.list['citizenship'].push(subfetcherData.entities[w0].edges[ed0].entity.name);
			            				}
			            			}
			            			var edgeValue = d3.nest()
			            		    .key(function(d) { return d.relationshipName; }).entries(subfetcherData.entities[w0].edges);
			            		    for (var i = 0; i < edgeValue.length; i++) {
			            		    	if(edgeValue[i].key ==="died:"){
			            		    		for (var j = 0; j < edgeValue[i].values.length; j++) {
			            		    			$scope.entitySearchResult.list['died'] += edgeValue[i].values[j].entity.name + ' ';
			            		    		}
			            		    	}
			            		    }
			            		}
			            		if($scope.mainInfoTabType === 'Academics' && academicLocationList.length > 0){
			        		        worldAcademicChartOptions.markers = academicLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldAcademicChartOptions);
			        			}
			            		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            		if($scope.mainInfoTabType === 'Career' && careerLocationList.length > 0){
			            			worldCareerChartOptions.markers = careerLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldCareerChartOptions);
								}
								if($scope.entitySearchResult.list['board_membership_counts_data'].length >0){
									$(".keyValueList").mThumbnailScroller({
										axis : "x"
									});
								}
								if($scope.entitySearchResult.list['parents'].length === 0){
									data_parent =false;
									familymember();
								}
								if($scope.entitySearchResult.list['relatives'].length===0){
									data_relative =false;
									familymember();
								}
								if($scope.entitySearchResult.list['has_certificates'].length === 0){
									$scope.entitySearchResult.data_not_found.hasCertificates =false;
								}

			            	}
			            	if(subfetcherData.fetcher === 'openpaymentsdata.cms.gov' || subfetcherData.fetcher === 'spokeo.com') {
			            		var matchedName = '';
			            		for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
			            			if(((matchedName === '') || (matchedName !== '' && matchedName === subfetcherData.entities[fl].name))
			            				&& (subfetcherData.entities[fl].name !== '' && subfetcherData.entities[fl].name)) {
			            				matchedName === '' ? matchedName = subfetcherData.entities[fl].name : '';
			            				if(subfetcherData.entities[0].source=== 'openpaymentsdata.cms.gov'){
			            						$scope.entitySearchResult.list["openpayment_cms"]['speciality'].push(subfetcherData.entities[0].properties);
			            					}
			            				for (var el = 0; el < subfetcherData.entities[fl].edges.length; el++) {
			            					if(subfetcherData.entities[fl].edges[el].relationshipName === 'data'){
			            						$scope.entitySearchResult.list["employment_history"].push(subfetcherData.entities[fl].edges[el].entity.properties);
				            					var indexRole = -1;
				            					for (var rd = 0; rd < roleDataForPieChart.length; rd++) {
				            						if(roleDataForPieChart[rd].key  === subfetcherData.entities[fl].edges[el].entity.properties.job_rank){
				            							indexRole = rd;
				            						}
				            					}
			                					if(indexRole === -1){
			                						roleDataForPieChart.push({
					            						key: subfetcherData.entities[fl].edges[el].entity.properties.job_rank,
					            						doc_count: 1
					            					});
			                					} else {
			                						roleDataForPieChart[indexRole].doc_count++;
			                					}
			                            		if((subfetcherData.entities[fl].edges[el].entity.properties.company !== '' && subfetcherData.entities[fl].edges[el].entity.properties.company) ||
			                            				(subfetcherData.entities[fl].edges[el].entity.properties.city !== '' && subfetcherData.entities[fl].edges[el].entity.properties.city)) {
			                                		var locationindex = -1;
			                                		for(var i = 0; i < careerLocationList.length; i++){
			                                			if((careerLocationList[i].name === subfetcherData.entities[fl].edges[el].entity.properties.company) ||
			                                				(careerLocationList[i].name === subfetcherData.entities[fl].edges[el].entity.properties.city))
			                                				locationindex = i;
			                                		}
			                                		if(locationindex === -1){
			                                			careerLocationList.push({
			    	                        				name: subfetcherData.entities[fl].edges[el].entity.properties.company !== '' && subfetcherData.entities[fl].edges[el].entity.properties.company ? subfetcherData.entities[fl].edges[el].entity.properties.company : subfetcherData.entities[fl].edges[el].entity.properties.city,
			    	                        				long: '',
			    	                        				lat: '',
			    	                        				mark: 'assets/images/redpin.png',
			    	                        				source: subfetcherData.entities[fl].source,
			    	                        				title: subfetcherData.entities[fl].edges[el].entity.properties.company || subfetcherData.entities[fl].edges[el].entity.properties.city || subfetcherData.entities[fl].source
			    	                        			});
			                                		}
			                            		}
			            					}
			            					if(subfetcherData.entities[fl].edges[el].relationshipName === 'person_profile'){
			            						var properties = subfetcherData.entities[fl].edges[el].entity.properties;
			            							$scope.entitySearchResult.list["openpayment_cms"]["openpayments_trans"].push({
			            								address: properties.address_line_1 +' '+properties.city +' '+properties.state+' '+properties.country_name+' '+properties.zipcode,
			            								invested_total_transactions : properties.invested_total_transactions,
														primary_specialty : properties.primary_specialty,
														program_year : properties.program_year,
														research_total_payment : properties.research_total_payment,
														research_total_transactions : properties.research_total_transactions,
														risk_score : properties.risk_score,
														total_amount : properties.total_amount,
														total_associated_research_payments : properties.total_associated_research_payments,
														total_associated_research_transactions : properties.total_associated_research_transactions,
														total_disputed_transactions : properties.total_disputed_transactions,
														total_payment :properties.total_payment,
														total_transactions :properties.total_transactions,
														total_undisputed_transactions :properties.total_undisputed_transactions
			            							});
			            					}
			            					for (var cw = 0; cw < subfetcherData.entities[fl].edges[el].entity.edges.length; cw++) {
			            						if(subfetcherData.entities[fl].edges[el].entity.edges[cw].relationshipName === 'coworkers'
			            							&& subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name !== ''
			            						       && subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name &&
			            						       $scope.entitySearchResult.list["coworkers"].indexOf(subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name) === -1){

					            							$scope.entitySearchResult.list["coworkers"].push({
						            							name: subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name,
						            							source: subfetcherData.entities[fl].source,
						            							risk_score: subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.risk_score
						            						});
					                        			/* 	var index = -1;
					                        				for (var tl = 0; i < tagCloudNameList.length; tl++) {
					                        						if(tagCloudNameList[tl].text === subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name)
						                							index = tl
					                        				}
						                					if(index === -1){
						                						tagCloudNameList.push({
								            						text: subfetcherData.entities[fl].edges[el].entity.edges[cw].entity.properties.name,
								            						size: 5,
								            						type: 'person'
								            					});
						                					} else {
						                						tagCloudNameList[index].size += 5;
						                						tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = tagCloudNameList[index].size : '';
						                					} */
			            						}//if cow0rkers ends
			            					}//loop ends
			            				}//el loop ends
			            			}//if conition ends
			            		}//fl
			            		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            		if($scope.mainInfoTabType === 'Career' && careerLocationList.length > 0){
			            			worldCareerChartOptions.markers = careerLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldCareerChartOptions);
								}
								if($scope.entitySearchResult.list["coworkers"].length === 0){
									$scope.entitySearchResult.data_not_found.coworker_data =false;
								}
			            	}
			            	if(subfetcherData.fetcher === 'zabasearch.com') {
			            		for (var i = 0; i < subfetcherData.entities.length; i++) {
			            			if (subfetcherData.entities[i].name !== '' && subfetcherData.entities[i].name) {
			            				subfetcherData.entities[i].properties['source'] = subfetcherData.entities[i].source;
			            				$scope.entitySearchResult.list['people_details'].push(subfetcherData.entities[i].properties);
			            				var index = -1;
			            				for (var key = 0; key < tagCloudNameList.length; key++) {
			            					 if(tagCloudNameList[key].text === subfetcherData.entities[i].properties.full_name){
			            					 	index = key
			            					 }
			            				}
			            				if(index === -1){
	                						tagCloudNameList.push({
			            						text: subfetcherData.entities[i].properties.full_name,
			            						size: 5,
			            						type: 'person'
			            					});
	                					} else {
	                						tagCloudNameList[index].size += 5;
	                						tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = tagCloudNameList[index].size : '';
	                					}
			            			}
			            		}
			            	}
			            	if(subfetcherData.fetcher === 'twitter.com') {
			            		var edgeValue = d3.nest()
									.key(function(d) { return d.relationshipName; }).entries(subfetcherData.entities[0].edges);
									
									for (var key = 0; key < edgeValue.length; key++){
										if(edgeValue[key].key === 'tweets'){
				            		    	for (var i = 0; i < edgeValue[key].values.length; i++) {
				            		    		$scope.entitySearchResult.list.twitterList.push({
					            					  name: subfetcherData.entities[0].properties.title,
						                              createdOn: new Date(edgeValue[key].values[i].entity.properties.created_at),
						                              image:subfetcherData.entities[0].properties.image,
						                              text: edgeValue[key].values[i].entity.properties.text,
						                              retweetCount: edgeValue[key].values[i].entity.properties.retweet_count,
						                              tags: [],
						                              type: 'twitter'
						            				});
					            		    		activitySocialData.push({
							               					"time": new Date(edgeValue[key].values[i].entity.properties.created_at),
							            		   			"amount": Math.floor(Math.random() * 20),
							               					"type":"twitter",
							               					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">name:</b> '+subfetcherData.entities[0].properties.title+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">createdOn:</b> '+new Date(edgeValue[key].values[i].entity.properties.created_at)+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Retweet Count:</b>  '+edgeValue[key].values[i].entity.properties.retweet_count+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Type:</b>Twitter</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">text:</b>  '+edgeValue[key].values[i].entity.properties.text+'</span>'
							               				});
				            		    			for(var ek=0; ek<edgeValue[key].values[i].entity.edges.length; ek++){
						            					if(edgeValue[key].values[i].entity.edges[ek].relationshipName === 'user_mentions') {
						            						$scope.entitySearchResult.list.twitterList[$scope.entitySearchResult.list.twitterList.length - 1].tags.push({
						            							name: edgeValue[key].values[i].entity.edges[ek].entity.properties.name,
						            							screen_name: edgeValue[key].values[i].entity.edges[ek].entity.properties.screen_name,
						            							risk_score: edgeValue[key].values[i].entity.edges[ek].entity.properties.risk_score
						            						});
						            	 					var index = -1;
						            	 					for (var twtag = 0; twtag < twitter_tag_words.length; twtag++) {
						            	 						if(twitter_tag_words[twtag].text === edgeValue[key].values[i].entity.edges[ek].entity.properties.name){
						            	 							index = twtag
						            	 						}
						            	 					}
						                					if(index === -1){
						                						twitter_tag_words.push({
								            						text: edgeValue[key].values[i].entity.edges[ek].entity.properties.name,
								            						size: 5,
								            						type: 'person'
								            					});
						                					} else {
						                						twitter_tag_words[index].size += 5;
						                						twitter_tag_words[index].size > maxTwitterTagDomainRangeValue ? maxTwitterTagDomainRangeValue = twitter_tag_words[index].size : '';
						                					}
						            					}
						            				}
				            		    		$scope.entitySearchResult['twitter_retweets_count'] += parseInt(edgeValue[key].values[i].entity.properties.retweet_count);
				            		    	}
				            		    }
										if(edgeValue[key].key === 'follower_following_count') {
											twitter_followercount = parseInt(edgeValue[key].values[0].entity.properties.followers);
											$scope.entitySearchResult.list['follower_following_count'].push({
											twitter_followers_number : parseInt(edgeValue[key].values[0].entity.properties.followers),
											twitter_following_number : parseInt(edgeValue[key].values[0].entity.properties.following)
											});
										}
										if(edgeValue[key].key === 'followers') {
				            		    	for (var i = 0; i < edgeValue[key].values.length; i++) {
						            				if(edgeValue[key].values[i].entity.name !== ''){
							            				$scope.entitySearchResult.list[edgeValue[key].values[i].relationshipName].push({
						            					  name: edgeValue[key].values[i].entity.name,
							                              createdOn: new Date(edgeValue[key].values[i].entity.properties.profile_created_at),
							                              image: edgeValue[key].values[i].entity.properties.profile_image_url,
							                              screen_name: edgeValue[key].values[i].entity.properties.screen_name,
							                              risk_score: edgeValue[key].values[i].entity.properties.risk_score,
							                              location : edgeValue[key].values[i].entity.properties.location,
							                              source: edgeValue[key].values[i].entity.source
							            				});
						            				}
						            		}
										}
										if(edgeValue[key].key ===  'following') {
				            				for (var i = 0; i < edgeValue[key].values.length; i++) {
				            						if(edgeValue[key].values[i].entity.name !== ''){
							            				$scope.entitySearchResult.list[edgeValue[key].values[i].relationshipName].push({
						            					  name: edgeValue[key].values[i].entity.name,
							                              createdOn: new Date(edgeValue[key].values[i].entity.properties.profile_created_at),
							                              image: edgeValue[key].values[i].entity.properties.profile_image_url,
							                              screen_name: edgeValue[key].values[i].entity.properties.screen_name,
							                              risk_score: edgeValue[key].values[i].entity.properties.risk_score,
							                              location : edgeValue[key].values[i].entity.properties.location,
							                              source:edgeValue[key].values[i].entity.source
							            				});
				            						}
				            				}
										}

									}
					            $scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1);
								$scope.entitySearchResult.list['followers_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['followers'].length), 1);
			             		if($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number > 0){
							        bubbleSocialOptions.data.children.push({
							        	"name": "twitter",
							            "size": $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number
									});
							        socialBubbleChart(bubbleSocialOptions);
								 }
								 else{
									 $scope.entitySearchResult.data_not_found.socialmedia_follower =false;
								 }
								 $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number = nFormatter($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number, 1);
			            		if($scope.mainInfoTabType === 'Social Media' && activitySocialData.length > 0){
//			          		   	  	timelineSocialOptions.data = activitySocialData;
			             			loadtimeLineColumnChart(timelineSocialOptions);
								}
								if($scope.entitySearchResult.list.twitterList.length === 0){
									$scope.entitySearchResult.data_not_found.recent_tweets = false;
								}
			            	}
				           	if(subfetcherData.fetcher === 'plus.google.com') {
				           		for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
			            			var subentity = subfetcherData.entities[fl];
			            			if(($scope.entitySearchResult.name === subentity.name)	&& (subentity.name !== '' && subentity.name)) {
			            				for (var el = 0; el < subentity.edges.length; el++) {
			            					var edgeValue = subentity.edges[el];
			            					if(edgeValue.relationshipName === 'recent_public_activity') {
										        $scope.entitySearchResult.list.googlePlusList.push({
					            					name: subentity.properties.name,
					            					url: subentity.properties.url,
					            					title: edgeValue.entity.properties.title,
					            					coverPhoto: subentity.properties.image,
					            					lastUpdatedDate: new Date(edgeValue.entity.properties.last_updated_date),
					            					publishedDate: new Date(edgeValue.entity.properties.published_date),
					            					type: 'google'
					            				});
					            				activitySocialData.push({
					               					"time": new Date(edgeValue.entity.properties.published_date),
					            		   			"amount": Math.floor(Math.random() * 20),
					               					"type":"google plus",
					               					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">title:</b> '+edgeValue.entity.properties.title+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Created on:</b> '+ new Date(edgeValue.entity.properties.published_date) +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Updated on:</b>  '+ new Date(edgeValue.entity.properties.last_updated_date) +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Type:</b>Google +</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b>  '+ subentity.properties.name +'</span>'
					               				});
					               				for (var ed = 0; ed < edgeValue.entity.edges.length; ed++) {
					               					var postV = edgeValue.entity.edges[ed];
					               					if(postV.relationshipName === 'post'){
					               						for (var poV = 0; poV < postV.entity.edges.length; poV++) {
					               							var attachmentsV = postV.entity.edges[poV];
					               							if(attachmentsV.relationshipName === 'attachments'){
					               								for (var ae = 0; ae < attachmentsV.entity.edges.length; ae++) {
					               									 var thumbnailsV = attachmentsV.entity.edges[ae];
					               									if(thumbnailsV.relationshipName === 'thumbnails'){
					               									 	for (var img = 0; img< thumbnailsV.entity.edges.length; img++) {
					               									 	    var imageV = thumbnailsV.entity.edges[img];
					               									 		if(imageV.relationshipName === 'image' && imageV.entity.name !== '' && imageV.entity.name && imageV.entity.properties.type_.indexOf('image') !== -1){
																        		$scope.entitySearchResult.list['thumbnails'].push({
																        			image: imageV.entity.name,
																        			source: subentity.source,
																        			risk_score: imageV.entity.properties.risk_score,
																        			type: imageV.entity.properties.type_,
																        			height: imageV.entity.properties.height,
																        			width: imageV.entity.properties.width
																        		});
																        	}
					               									 	}
					               									}
					               								}
					               							}//attachment if end
					               						}//poV loop end
					               					}//post if end
					               				}//ed loop end
			            					}
			            					if($scope.entitySearchResult.list.googlePlusList.length === 0){
			            						$scope.entitySearchResult.data_not_found.google_plus = false;
			            					}
			            					//recent_public_activity if ends
			            					if(edgeValue.relationshipName === 'organizations'){
				            					if(edgeValue.entity.properties.name !== '' && edgeValue.entity.properties.name &&
				            							edgeValue.entity.properties.title !== '' && edgeValue.entity.properties.title){
					            					$scope.entitySearchResult.list['organizations'].push({
					            						name: edgeValue.entity.properties.name,
					            						title: edgeValue.entity.properties.title,
					            						endDate: edgeValue.entity.properties.endDate,
					            						startDate: edgeValue.entity.properties.startDate,
					            						risk_score: edgeValue.entity.properties.risk_score,
					            						type: edgeValue.entity.properties.type_,
					            						source: subentity.source
					            					});
					            					var index = -1;
					            					for (var tag = 0; tag < tagCloudOrganizationNameList.length; tag++) {
					            						if(tagCloudOrganizationNameList[tag].text === edgeValue.entity.properties.name)
			                    							index = tag
					            					}
			                    					if(index === -1){
			    		            					tagCloudOrganizationNameList.push({
			    		            						text: edgeValue.entity.properties.name,
			    		            						size: 5,
			    		            						type: 'organization'
			    		            					});
			                    					} else {
			                    						tagCloudOrganizationNameList[index].size += 5;
			                    						tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
			                    					}
				            					}
				            				}
				            				if(edgeValue.relationshipName === 'places_lived'){
				            					locationList.push({
	                                				name: edgeValue.entity.properties.value,
	                                				long: '',
	                                				lat: '',
	                                				mark: 'assets/images/redpin.png',
	                                				source: subentity.source,
	                                				title: subentity.source
	                                			});
				            				}
			            				}
			            			}
			            		}
			            		if($scope.mainInfoTabType === 'Social Media' && activitySocialData.length > 0){
			          		   	  	timelineSocialOptions.data = activitySocialData;
			             			loadtimeLineColumnChart(timelineSocialOptions);
			            		}
			            	}
				           	if(subfetcherData.fetcher === 'instagram.com') {
								   var matchedName = '';
				           		for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
					           			if(((matchedName === '') || (matchedName !== '' && matchedName === subfetcherData.entities[fl].name))
				            				&& (subfetcherData.entities[fl].name !== '' && subfetcherData.entities[fl].name)) {
					           					matchedName === '' ? matchedName = subfetcherData.entities[fl].name : '';
					           					for (var ed = 0; ed < subfetcherData.entities[fl].edges.length; ed++) {
													   if(subfetcherData.entities[fl].edges[ed].relationshipName === 'media'){
							           						$scope.entitySearchResult.list.instagramList.push({
								            					//caption: subfetcherData.entities[fl].edges[ed].entity.properties.caption || '',
								            					thumbnail: subfetcherData.entities[fl].edges[ed].entity.properties.thumbnail,
								            					likes_count: subfetcherData.entities[fl].edges[ed].entity.properties.likes_count,
								            					image: subfetcherData.entities[fl].properties.image,
								            					name: subfetcherData.entities[fl].properties.name,
								            					publish_date: new Date((subfetcherData.entities[fl].edges[ed].entity.properties.publish_ts) * 1000),
								            					followingCount: subfetcherData.entities[fl].properties.following_count,
								            					followersCount: subfetcherData.entities[fl].properties.followers_count,
								            					type: 'instagram'
							            					});
					           						}//if relationship loop end
					           						activitySocialData.push({
				               							"time": new Date(parseInt(subfetcherData.entities[fl].edges[ed].entity.properties.publish_ts) * 1000),
				            		   					"amount": Math.floor(Math.random() * 20),
				               							"type":"instagram",
				               							'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Caption:</b> '+(subfetcherData.entities[fl].edges[ed].entity.properties.caption || 'NA')+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Published on:</b> '+ new Date((subfetcherData.entities[fl].edges[ed].entity.properties.publish_ts) * 1000) +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Likes Count:</b>  '+ subfetcherData.entities[fl].edges[ed].entity.properties.likes_count +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Type:</b>Instagram</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b>  '+ subfetcherData.entities[fl].properties.name +'</span>'
				               						});
					           					}//for loop edges end

					           			}//if matched name loop ends
				           		}//fl for loop ends
			            		if($scope.entitySearchResult.list.twitterList.length === 0)
			            			$scope.socialMediaTab = 'instagram';

									if($scope.entitySearchResult.list.instagramList.length > 0){
										$scope.entitySearchResult.list['instagramfollowersCountNumberic'] = nFormatter(parseInt($scope.entitySearchResult.list.instagramList[0]['followersCount']), 1);
										$scope.entitySearchResult.list['instagramListConnectionNumberic'] = nFormatter(parseInt($scope.entitySearchResult.list.instagramList.length), 1);
			            			$scope.socialMediaActiveCount = 1;
							        bubbleSocialOptions.data.children.push({
										"name": "instagram",
							            "size": $scope.entitySearchResult.list.instagramList[0]['followersCount']
							        });
							        socialBubbleChart(bubbleSocialOptions);
			            		}else{
									$scope.entitySearchResult.data_not_found.socialmedia_follower =false;
								}
			            		if($scope.mainInfoTabType === 'Social Media' && activitySocialData.length > 0){
			          		   	  	timelineSocialOptions.data = activitySocialData;
			             			loadtimeLineColumnChart(timelineSocialOptions);
								}
								if($scope.entitySearchResult.list.instagramList.length === 0){
									$scope.entitySearchResult.data_not_found.instagram = false;
								}
			            	}
				           	if(subfetcherData.fetcher === 'supplierblacklist.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentityKey === 0 && subentity.properties && checkforAvailableMember(subentity.properties.seller)) {
			            				angular.forEach(subentity.properties, function(value, key){
			            					if(value !== '' && value) {
				            					key = key.replace(/_/g, ' ');
					            				$scope.entitySearchResult.list['supplierblacklist'][key] = value;
			            					}
			            				});
										$scope.entitySearchResult.list['supplierblacklist'].Source = subentity.source;
			            			}
			            		});
				   				if($scope.mainInfoTabType == 'Risks' && $scope.entitySearchResult.list['supplierblacklist'].Source){
				   					PlotriskAlertCharts();
									}
								if( _.isEmpty($scope.entitySearchResult.list['supplierblacklist']) ){
									$scope.entitySearchResult.data_not_found.supplier_data =false;
								}
								else{
									$scope.entitySearchResult.data_not_found.initial_supplierblacklist =false;
								}
			            	}
				           	if(subfetcherData.fetcher === 'unlimitedcriminalchecks.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== '' && subentity.name){
			            				var object = {};
				            			object.name = subentity.name;
			            				angular.forEach(subentity.properties, function(value, key){
			            					if(value !== '' && value) {
				            					key = key.replace(/_/g, ' ');
					            				object[key] = value;
			            					}
			            				});
				            			angular.forEach(subentity.edges, function(subedv, sudedk){
				            				if(subedv.relationshipName === 'other_names' && subedv.entity.name !== '' && subedv.entity.name){
				            					subedv.relationshipName = subedv.relationshipName.replace(/_/g, ' ');
				            					if(!object.hasOwnProperty(subedv.relationshipName))
				            						object[subedv.relationshipName] = subedv.entity.name;
				            					else
				            						object[subedv.relationshipName] += ', '+ subedv.entity.name;
				            				}
				            			});
				            			object.Source = subentity.source;
			            				$scope.entitySearchResult.list['unlimitedcriminalchecks'].push(object);
			            			}
		            			});
			            	}
				           	if(subfetcherData.fetcher === 'crimestoppers-uk.org') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== '' && subentity.name){
			            				$scope.entitySearchResult.list['crimestoppers_uk'].push({
			            					name: subentity.properties.suspect_name,
			            					age_range: subentity.properties.age_range,
			            					crime_location: subentity.properties.crime_location,
			            					crime_type: subentity.properties.crime_type,
			            					police_force: subentity.properties.police_force,
			            					sex: subentity.properties.sex,
			            					image: subentity.properties.image,
			            					cs_reference: subentity.properties.cs_reference,
			            					source: subentity.source,
			            					risk_score: subentity.properties.risk_score
			            				});
			            			}
		            			});
			            	}
				           	if(subfetcherData.fetcher === 'badbuyerslist.org') {
			           			for (var i = 0; i < subfetcherData.entities.length; i++) {
				           			$scope.entitySearchResult.list['badbuyerslist'].push({
		            					name: subfetcherData.entities[i].properties.buyers_name,
		            					buyers_country: subfetcherData.entities[i].properties.buyers_country,
		            					email: subfetcherData.entities[i].properties.email,
		            					reported_at: new Date(subfetcherData.entities[i].properties.reported_at),
		            					payment_method: subfetcherData.entities[i].properties.payment_method,
		            					buyers_address: subfetcherData.entities[i].properties.buyers_address,
		            					details: subfetcherData.entities[i].properties.details,
		            					url: subfetcherData.entities[i].properties.url,
		            					purchase_date: subfetcherData.entities[i].properties.purchase_date,
		            					store: subfetcherData.entities[i].properties.store,
		            					phone_number: subfetcherData.entities[i].properties.phone_number,
		            					source: subfetcherData.entities[i].source,
		            					risk_score: subfetcherData.entities[i].properties.risk_score
		            				});
			           			}
				   				if($scope.mainInfoTabType == 'Risks' && $scope.entitySearchResult.list['badbuyerslist'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'imsasllc.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
		            				var object = {
		            					properties: subentity.properties,
		            					offenses: []
		            				};
		            				angular.forEach(subentity.edges, function(subedv, subedk){
		            					if(subedv.relationshipName === 'Offenses'){
		            						object.offenses.push(subedv.entity.properties);
		            						$scope.entitySearchResult.list['imsasllc_offenses'].push(subedv.entity.properties);
		            					}
		            				});
		            				object.offenses = object.offenses.join();
			            			$scope.entitySearchResult.list['imsasllc'].push(object);
		            			});
				   				if($scope.mainInfoTabType == 'Risks' && $scope.entitySearchResult.list['imsasllc'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'exclusions.oig.hhs.gov') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentityKey === 0) {
			            				angular.forEach(subentity.edges, function(subedv, subedk){
			            					if(subedv.relationshipName === 'person'){
			            						$scope.entitySearchResult.list['exclusions']['person'] = subedv.entity.properties;
			            					}
			            					if(subedv.relationshipName === 'entity'){
			            						$scope.entitySearchResult.list['exclusions']['entity'] = subedv.entity.properties;
			            					}
			            					$scope.entitySearchResult.list['exclusions']['source'] = subentity.source;
			            				});

			            			}
			            		});
				   				if($scope.mainInfoTabType == 'Risks' && $scope.entitySearchResult.list['exclusions']['person']){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
			            	if(subfetcherData.fetcher === 'scam.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
		            				$scope.entitySearchResult.list['scam'].push({
		            					root: subentity.properties,
		            					replies: [],
		            					source: subentity.source
		            				});
		            				angular.forEach(subentity.edges, function(subedv, subedk) {
		            					if(subedv.relationshipName === 'replies'){
		            						$scope.entitySearchResult.list['scam'][subentityKey]['replies'].push(subedv.entity.properties);
		            					}
		            				});
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['scam'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
			            	if(subfetcherData.fetcher === 'sex-offenders.data.cityofchicago.org') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== ''){
				            			var object = {};
				            			object.name = subentity.name;
				            			angular.forEach(subentity.properties, function(value, key){
				            				if(value !== '' && value) {
				            					key = key.replace(/_/g, ' ');
					            				object[key] = value;
			            					}
				            			});
				            			object.source = subentity.source;
				            			$scope.entitySearchResult.list['sex-offenders'].push(object);
			            			}
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['sex-offenders'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'denied_personlist.gov') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									if(checkforAvailableMember(subentity.properties.name)){
		            				$scope.entitySearchResult.list['denied_personlist'].push({
		            					name: subentity.properties.name,
		            					action: subentity.properties.action,
		            					effective_date: new Date(subentity.properties.effective_date),
		            					expiration_date: new Date(subentity.properties.expiration_date),
		            					fr_citation: subentity.properties.fr_citation,
		            					address: subentity.properties.street_address + ' ' + subentity.properties.city + ' ' + subentity.properties.state + ' ' + subentity.properties.country,
		            					risk_score: subentity.properties.risk_score,
		            					source: subentity.source
									});
								}
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['denied_personlist'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'interpol.int') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== '' && subentity.name && checkforAvailableMember(subentity.name)) {
			            				var interpolObject = {
			            					charges: []
			            				};
			            				interpolObject.properties = subentity.properties;
			            				angular.forEach(subentity.edges, function(edv, edk){
			            					interpolObject.charges.push(edv.entity.name);
			            				});
			            				$scope.entitySearchResult.list['crimeList']['interpol'].push(interpolObject);
			            			}
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['crimeList']['interpol'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'sanctions_wrapper') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== '' && subentity.name && subentity.source === 'interpol.int') {
			            				var interpolObject = {
			            					charges: []
			            				};
			            				interpolObject.properties = subentity.properties;
			            				angular.forEach(subentity.edges, function(edv, edk){
			            					if(edv.relationshipName === 'charges'){
			            						interpolObject.charges.push({
			            							name: edv.entity.name,
			            							risk_score: edv.entity.properties.risk_score,
			            							source: subentity.source
			            						});
			            					}
			            				});
			            				$scope.entitySearchResult.list['crimeList']['interpol'].push(interpolObject);
			            			}
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['crimeList']['interpol'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'fbi.gov') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			var object = {
			            				properties: {},
			            				images: [],
			            				aliasNames: '',
			            				source: subentity.source
									};
									var title = subentity.properties.title.replace(/[^\w\s]/gi, '');
			            			angular.forEach(subentity.properties, function(value, key){
			            				if(value !== '' && value) {
			            					key = key.replace(/_/g, ' ');
				            				object.properties[key] = value;
		            					}
			            			});
			            			angular.forEach(subentity.edges, function(subedv, subedk){
			            				if(subedv.relationshipName === 'images' && subedv.entity.name !== ''){
			            					object.images.push({
			            						name: subedv.entity.name,
			            						risk_score: subedv.entity.properties.risk_score
			            					});
			            				}
			            				if(subedv.relationshipName === 'aliases' && subedv.entity.name !== ''){
			            					object.aliasNames += subedv.entity.name + ', '
			            				}
			            			});
			            			$scope.entitySearchResult.list["fbi_data"].push(object);
			            		});
				   				if($scope.mainInfoTabType == 'Risk Alerts' && $scope.entitySearchResult.list['fbi_data'].length > 0){
				   					PlotriskAlertCharts();
				 	   		    }
			            	}
				           	if(subfetcherData.fetcher === 'hunter.io') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentityKey === 0){
				            			$scope.entitySearchResult.list["hunterData"].title = subentity.properties.title;
				            			$scope.entitySearchResult.list["hunterData"].email = subentity.properties.details;
				            			angular.forEach(subentity.edges, function(subedv, subedk){
				            				if(subedv.relationshipName === 'data'){
				            					$scope.entitySearchResult.list["hunterData"].domain = subedv.entity.properties.domain;
				            				}
				            				if(subedv.entity.edges.length > 0)
				            					$scope.entitySearchResult.list["hunterData"].sources = [];
				            				angular.forEach(subedv.entity.edges, function(subsubedv, subsubedk){
				            					if(subsubedv.relationshipName === 'sources'){
				            						$scope.entitySearchResult.list["hunterData"].sources.push({
				            							domain: subsubedv.entity.properties.domain,
				            							extracted_on: new Date(subsubedv.entity.properties.extracted_on),
				            							uri: subsubedv.entity.properties.uri
				            						});
				            					}
				            				});
				            			});
				            			$scope.entitySearchResult.list["hunterData"].source = subentity.source;
			            			}
			            		});
			            	}
				           	if(subfetcherData.fetcher === 'profiles.bloomberg.com') {
								   var firstLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(subfetcherData.entities[0].edges);
								   if(subfetcherData.entities[0].properties.total_anual_sallery)
				           		subfetcherData.entities[0].properties.total_anual_sallery = subfetcherData.entities[0].properties.total_anual_sallery.split('As')[0];
			            		$scope.entitySearchResult.list['financial_data'] = subfetcherData.entities[0].properties;
			            		for(var i0 = 0; i0 < firstLevelData.length; i0++){
			            			if(firstLevelData[i0].key === 'memberships'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					if(firstLevelData[i0].values[v0].entity.properties.company_name !== '' && firstLevelData[i0].values[v0].entity.properties.company_name &&
			            							firstLevelData[i0].values[v0].entity.properties.position !== '' && firstLevelData[i0].values[v0].entity.properties.position){
			            						$scope.entitySearchResult.list["memberships"].push({
		                                			name: firstLevelData[i0].values[v0].entity.properties.company_name,
		                                			position: firstLevelData[i0].values[v0].entity.properties.position,
		                                			risk_score: firstLevelData[i0].values[v0].entity.properties.risk_score,
		                                			source: firstLevelData[i0].values[v0].entity.source
		        	            				});
			            						var index = -1;
		                    					angular.forEach(tagCloudOrganizationNameList, function(object, key){
		                    						if(object.text === firstLevelData[i0].values[v0].entity.company_name)
		                    							index = key
		                    					});
		                    					if(index === -1){
		    		            					tagCloudOrganizationNameList.push({
		    		            						text: firstLevelData[i0].values[v0].entity.company_name,
		    		            						size: 5,
		    		            						type: 'organization'
		    		            					});
		                    					} else {
		                    						tagCloudOrganizationNameList[index].size += 5;
		                    						tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
		                    					}
			            					}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'other_board_memberships'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					if(firstLevelData[i0].values[v0].entity.name !== '' && firstLevelData[i0].values[v0].entity.name){
			            						$scope.entitySearchResult.list["board_memberships"].push({
		                                			name: firstLevelData[i0].values[v0].entity.name,
		                                			risk_score: firstLevelData[i0].values[v0].entity.properties.risk_score,
		                                			source: firstLevelData[i0].values[v0].entity.source
		        	            				});
			            						var index = -1;
		                    					angular.forEach(tagCloudOrganizationNameList, function(object, key){
		                    						if(object.text === firstLevelData[i0].values[v0].entity.name)
		                    							index = key
		                    					});
		                    					if(index === -1){
		    		            					tagCloudOrganizationNameList.push({
		    		            						text: firstLevelData[i0].values[v0].entity.name,
		    		            						size: 5,
		    		            						type: 'organization'
		    		            					});
		                    					} else {
		                    						tagCloudOrganizationNameList[index].size += 5;
		                    						tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
		                    					}
			            					}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'anual_compensation_breakdown'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					!$scope.entitySearchResult.list['financial_data']['other_long_term_compensation'] ? $scope.entitySearchResult.list['financial_data']['other_long_term_compensation'] = firstLevelData[i0].values[v0].entity.properties.other_long_term_compensation : '';
			            					!$scope.entitySearchResult.list['financial_data']['total_annual_cash_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_annual_cash_compensation'] = firstLevelData[i0].values[v0].entity.properties.total_annual_cash_compensation : '';
			            					!$scope.entitySearchResult.list['financial_data']['total_calculated_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_calculated_compensation'] = firstLevelData[i0].values[v0].entity.properties.total_calculated_compensation : '';
			            					!$scope.entitySearchResult.list['financial_data']['total_short_term_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_short_term_compensation'] = firstLevelData[i0].values[v0].entity.properties.total_short_term_compensation : '';
			            					if(firstLevelData[i0].values[v0].entity.properties.total_calculated_compensation !== '' &&
			            							firstLevelData[i0].values[v0].entity.properties.total_calculated_compensation){
			            						earningRatioPieChart.push({
				            						key: 'Compensation',
				            						doc_count: parseInt((firstLevelData[i0].values[v0].entity.properties.total_calculated_compensation.replace('$','')).replace(/,/g,''))
				            					});
			            					}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'stock_options'){
										for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
										$scope.entitySearchResult.list['dividents'].push({
											all_other_compensation: firstLevelData[i0].values[v0].entity.properties.all_other_compensation,
											exercisable_options: firstLevelData[i0].values[v0].entity.properties.exercisable_options,
											exercisable_options_value: firstLevelData[i0].values[v0].entity.properties.exercisable_options_value,
											exercised_options: firstLevelData[i0].values[v0].entity.properties.exercised_options,
											exercised_options_value: firstLevelData[i0].values[v0].entity.properties.exercised_options_value,
											total_number_of_options: firstLevelData[i0].values[v0].entity.properties.total_number_of_options,
											total_value_of_options: firstLevelData[i0].values[v0].entity.properties.total_value_of_options,
											restricted_stock_awards: firstLevelData[i0].values[v0].entity.properties.restricted_stock_awards,
											unexercisable_options: firstLevelData[i0].values[v0].entity.properties.unexercisable_options,
											unexercisable_options_value: firstLevelData[i0].values[v0].entity.properties.unexercisable_options_value
										});
									}
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					!$scope.entitySearchResult.list['stock_options']['all_other_compensation'] ? $scope.entitySearchResult.list['stock_options']['all_other_compensation'] = firstLevelData[i0].values[v0].entity.properties.all_other_compensation : '';
			            					!$scope.entitySearchResult.list['stock_options']['exercisable_options'] ? $scope.entitySearchResult.list['stock_options']['exercisable_options'] = firstLevelData[i0].values[v0].entity.properties.exercisable_options : '';
			            					!$scope.entitySearchResult.list['stock_options']['exercisable_options_value'] ? $scope.entitySearchResult.list['stock_options']['exercisable_options_value'] = firstLevelData[i0].values[v0].entity.properties.exercisable_options_value : '';
			            					!$scope.entitySearchResult.list['stock_options']['exercised_options'] ? $scope.entitySearchResult.list['stock_options']['exercised_options'] = firstLevelData[i0].values[v0].entity.properties.exercised_options : '';
			            					!$scope.entitySearchResult.list['stock_options']['exercised_options_value'] ? $scope.entitySearchResult.list['stock_options']['exercised_options_value'] = firstLevelData[i0].values[v0].entity.properties.exercised_options_value : '';
			            					!$scope.entitySearchResult.list['stock_options']['total_number_of_options'] ? $scope.entitySearchResult.list['stock_options']['total_number_of_options'] = firstLevelData[i0].values[v0].entity.properties.total_number_of_options : '';
			            					!$scope.entitySearchResult.list['stock_options']['total_value_of_options'] ? $scope.entitySearchResult.list['stock_options']['total_value_of_options'] = firstLevelData[i0].values[v0].entity.properties.total_value_of_options : '';
			            					!$scope.entitySearchResult.list['stock_options']['restricted_stock_awards'] ? $scope.entitySearchResult.list['stock_options']['restricted_stock_awards'] = firstLevelData[i0].values[v0].entity.properties.restricted_stock_awards : '';
			            					!$scope.entitySearchResult.list['stock_options']['unexercisable_options'] ? $scope.entitySearchResult.list['stock_options']['unexercisable_options'] = firstLevelData[i0].values[v0].entity.properties.unexercisable_options : '';
			            					!$scope.entitySearchResult.list['stock_options']['unexercisable_options_value'] ? $scope.entitySearchResult.list['stock_options']['unexercisable_options_value'] = firstLevelData[i0].values[v0].entity.properties.unexercisable_options_value : '';
			            					if(firstLevelData[i0].values[v0].entity.properties.total_number_of_options !== '' &&
			            							firstLevelData[i0].values[v0].entity.properties.total_number_of_options){
			            						earningRatioPieChart.push({
				            						key: 'Stock',
				            						doc_count: parseInt((firstLevelData[i0].values[v0].entity.properties.total_number_of_options.replace('$','')).replace(/,/g,''))
				            					});
			            					}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'competitor_compensation'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					if(firstLevelData[i0].values[v0].entity.name !== '' && firstLevelData[i0].values[v0].entity.name &&
			            							firstLevelData[i0].values[v0].entity.properties.compensation !== '' && firstLevelData[i0].values[v0].entity.properties.compensation){
			            						$scope.entitySearchResult.list["competitor_compensation"].push({
		                                			name: firstLevelData[i0].values[v0].entity.name,
		                                			company: firstLevelData[i0].values[v0].entity.properties.company,
												//	compensation: convertToNumberFromNumberic(firstLevelData[i0].values[v0].entity.properties.compensation),
													compensation : firstLevelData[i0].values[v0].entity.properties.compensation,
		                                			risk_score: firstLevelData[i0].values[v0].entity.properties.risk_score,
		                                			source: firstLevelData[i0].values[v0].entity.source
		        	            				});
			            						/* bubbleCompittitorsFinanceOptions.data.children.push({
										        	"name": firstLevelData[i0].values[v0].entity.name,
										            "size": parseInt(convertToNumberFromNumberic(firstLevelData[i0].values[v0].entity.properties.compensation))
										        }); */
			            					}
			            				}

	            					}
			            			if(firstLevelData[i0].key === 'education'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
			            					if(firstLevelData[i0].values[v0].entity.properties.institute !== '' && firstLevelData[i0].values[v0].entity.properties.institute &&
			            							firstLevelData[i0].values[v0].entity.properties.course_title !== '' && firstLevelData[i0].values[v0].entity.properties.course_title){
			            						$scope.entitySearchResult.list["has_education"].push({
			                            			school_name: firstLevelData[i0].values[v0].entity.properties.institute,
			                            			type: firstLevelData[i0].values[v0].entity.properties.course_title,
			                            			url: '',
			                            			logo: '',
			                            			risk_score: firstLevelData[i0].values[v0].entity.properties.risk_score,
			                            			start_time : '',
			                            			end_time :  '',
			                            			source: firstLevelData[i0].values[v0].entity.source
			    	            				});
			            						var index = -1;
	                                    		for(var i = 0; i < academicLocationList.length; i++){
	                                    			if(academicLocationList[i].name === firstLevelData[i0].values[v0].entity.properties.institute)
	                                    				index = i;
	                                    		}
	                                    		if(index === -1){
	                                    			academicLocationList.push({
	        	                        				name: firstLevelData[i0].values[v0].entity.properties.institute,
	        	                        				long: '',
	        	                        				lat: '',
	        	                        				mark: 'assets/images/redpin.png',
	        	                        				source: firstLevelData[i0].values[v0].entity.source,
	        	                        				title: firstLevelData[i0].values[v0].entity.properties.institute || firstLevelData[i0].values[v0].entity.source
	        	                        			});
	                                    		}
			            					}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'board_relationships'){
			            				for(var f0 = 0; f0 < firstLevelData[i0].values.length; f0++){
			            					if(firstLevelData[i0].values[f0].entity.properties.name !== '' && firstLevelData[i0].values[f0].entity.properties.name &&
			            							firstLevelData[i0].values[f0].entity.properties.total_anual_sallery !== '' && firstLevelData[i0].values[f0].entity.properties.total_anual_sallery){
				            					competitors_data.push({
				            						name: firstLevelData[i0].values[f0].entity.properties.name,
				            						job_title: firstLevelData[i0].values[f0].entity.properties.job_title,
				            						total_annual_salary: firstLevelData[i0].values[f0].entity.properties.total_anual_sallery,
				            						work_location: firstLevelData[i0].values[f0].entity.properties.work_location,
				            						work_telephone: firstLevelData[i0].values[f0].entity.properties.work_telephone,
				            						works_for: firstLevelData[i0].values[f0].entity.properties.works_for,
				            						profile_url: firstLevelData[i0].values[f0].entity.properties.profile_url,
				            						professional_background: firstLevelData[i0].values[f0].entity.properties.professional_background
				            					});
				            					var numberic_value = (firstLevelData[i0].values[f0].entity.properties.total_anual_sallery.split('As')[0].replace(/,/g,'')).replace('$','');
				            					bubbleCompittitorsFinanceOptions.data.children.push({
										        	"name": firstLevelData[i0].values[f0].entity.properties.name,
										            "size": parseInt(numberic_value)
										        });
				            				}
				            				var secondLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(firstLevelData[i0].values[f0].entity.edges);
		            						for(var i1 = 0; i1 < secondLevelData.length; i1++){
		            							if(secondLevelData[i1].key === 'memberships'){
		    			            				for(var v0 = 0; v0 < secondLevelData[i1].values.length; v0++){
		    			            					if(secondLevelData[i1].values[v0].entity.properties.company_name !== '' && secondLevelData[i1].values[v0].entity.properties.company_name &&
		    			            							secondLevelData[i1].values[v0].entity.properties.position !== '' && secondLevelData[i1].values[v0].entity.properties.position){
		    			            						$scope.entitySearchResult.list["memberships"].push({
		    		                                			name: secondLevelData[i1].values[v0].entity.properties.company_name,
		    		                                			position: secondLevelData[i1].values[v0].entity.properties.position,
		    		                                			risk_score: secondLevelData[i1].values[v0].entity.properties.risk_score,
		    		                                			source: secondLevelData[i1].values[v0].entity.source
		    		        	            				});
		    			            						var index = -1;
		    		                    					angular.forEach(tagCloudOrganizationNameList, function(object, key){
		    		                    						if(object.text === secondLevelData[i1].values[v0].entity.company_name)
		    		                    							index = key
		    		                    					});
		    		                    					if(index === -1){
		    		    		            					tagCloudOrganizationNameList.push({
		    		    		            						text: secondLevelData[i1].values[v0].entity.company_name,
		    		    		            						size: 5,
		    		    		            						type: 'organization'
		    		    		            					});
		    		                    					} else {
		    		                    						tagCloudOrganizationNameList[index].size += 5;
		    		                    						tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
		    		                    					}
		    			            					}
		    			            				}
		    	            					}
		    			            			if(secondLevelData[i1].key === 'other_board_memberships'){
		    			            				for(var v0 = 0; v0 < secondLevelData[i1].values.length; v0++){
		    			            					if(secondLevelData[i1].values[v0].entity.name !== '' && secondLevelData[i1].values[v0].entity.name){
		    			            						$scope.entitySearchResult.list["board_memberships"].push({
		    		                                			name: secondLevelData[i1].values[v0].entity.name,
		    		                                			risk_score: secondLevelData[i1].values[v0].entity.properties.risk_score,
		    		                                			source: secondLevelData[i1].values[v0].entity.source
		    		        	            				});
		    			            						var index = -1;
		    		                    					angular.forEach(tagCloudOrganizationNameList, function(object, key){
		    		                    						if(object.text === secondLevelData[i1].values[v0].entity.name)
		    		                    							index = key
		    		                    					});
		    		                    					if(index === -1){
		    		    		            					tagCloudOrganizationNameList.push({
		    		    		            						text: secondLevelData[i1].values[v0].entity.name,
		    		    		            						size: 5,
		    		    		            						type: 'organization'
		    		    		            					});
		    		                    					} else {
		    		                    						tagCloudOrganizationNameList[index].size += 5;
		    		                    						tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
		    		                    					}
		    			            					}
		    			            				}
		    	            					}
		    			            			if(secondLevelData[i1].key === 'anual_compensation_breakdown'){
		    			            				for(var v0 = 0; v0 < secondLevelData[i1].values.length; v0++){
		    			            					!$scope.entitySearchResult.list['financial_data']['other_long_term_compensation'] ? $scope.entitySearchResult.list['financial_data']['other_long_term_compensation'] = secondLevelData[i1].values[v0].entity.properties.other_long_term_compensation : '';
		    			            					!$scope.entitySearchResult.list['financial_data']['total_annual_cash_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_annual_cash_compensation'] = secondLevelData[i1].values[v0].entity.properties.total_annual_cash_compensation : '';
		    			            					!$scope.entitySearchResult.list['financial_data']['total_calculated_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_calculated_compensation'] = secondLevelData[i1].values[v0].entity.properties.total_calculated_compensation : '';
		    			            					!$scope.entitySearchResult.list['financial_data']['total_short_term_compensation'] ? $scope.entitySearchResult.list['financial_data']['total_short_term_compensation'] = secondLevelData[i1].values[v0].entity.properties.total_short_term_compensation : '';
		    			            				}
		    	            					}
		    			            			if(secondLevelData[i1].key === 'stock_options'){
		    			            				for(var v0 = 0; v0 < secondLevelData[i1].values.length; v0++){
		    			            					if(firstLevelData[i0].values[f0].entity.properties.name !== '' && firstLevelData[i0].values[f0].entity.properties.name &&
		    			            							Object.keys(secondLevelData[i1].values[v0].entity.properties).length > 1){
			    			            					$scope.entitySearchResult.list['dividents'].push({
			    			            						name: firstLevelData[i0].values[f0].entity.properties.name,
			    			            						all_other_compensation: secondLevelData[i1].values[v0].entity.properties.all_other_compensation || '',
			    			            						exercisable_options: secondLevelData[i1].values[v0].entity.properties.exercisable_options || '',
			    			            						exercisable_options_value: secondLevelData[i1].values[v0].entity.properties.exercisable_options_value || '',
			    			            						exercised_options: secondLevelData[i1].values[v0].entity.properties.exercised_options || '',
			    			            						exercised_options_value: secondLevelData[i1].values[v0].entity.properties.exercised_options_value || '',
			    			            						total_number_of_options: secondLevelData[i1].values[v0].entity.properties.total_number_of_options || '',
			    			            						total_value_of_options: secondLevelData[i1].values[v0].entity.properties.total_value_of_options || '',
			    			            						restricted_stock_awards: secondLevelData[i1].values[v0].entity.properties.restricted_stock_awards || '',
			    			            						unexercisable_options: secondLevelData[i1].values[v0].entity.properties.unexercisable_options || '',
			    			            						unexercisable_options_value: secondLevelData[i1].values[v0].entity.properties.unexercisable_options_value || ''
			    			            					});
		    			            					}
		    			            				}
		    	            					}
		    			            			if(secondLevelData[i1].key === 'education'){
		    			            				for(var v0 = 0; v0 < secondLevelData[i1].values.length; v0++){
		    			            					if(secondLevelData[i1].values[v0].entity.properties.institute !== '' && secondLevelData[i1].values[v0].entity.properties.institute &&
		    			            							secondLevelData[i1].values[v0].entity.properties.course_title !== '' && secondLevelData[i1].values[v0].entity.properties.course_title){
		    			            						$scope.entitySearchResult.list["has_education"].push({
		    			                            			school_name: secondLevelData[i1].values[v0].entity.properties.institute,
		    			                            			type: secondLevelData[i1].values[v0].entity.properties.course_title,
		    			                            			url: '',
		    			                            			logo: '',
		    			                            			risk_score: secondLevelData[i1].values[v0].entity.properties.risk_score,
		    			                            			start_time : '',
		    			                            			end_time :  '',
		    			                            			source: secondLevelData[i1].values[v0].entity.source
		    			    	            				});
		    			            						var index = -1;
		    	                                    		for(var i = 0; i < academicLocationList.length; i++){
		    	                                    			if(academicLocationList[i].name === secondLevelData[i1].values[v0].entity.properties.institute)
		    	                                    				index = i;
		    	                                    		}
		    	                                    		if(index === -1){
		    	                                    			academicLocationList.push({
		    	        	                        				name: secondLevelData[i1].values[v0].entity.properties.institute,
		    	        	                        				long: '',
		    	        	                        				lat: '',
		    	        	                        				mark: 'assets/images/redpin.png',
		    	        	                        				source: secondLevelData[i1].values[v0].entity.source,
		    	        	                        				title: secondLevelData[i1].values[v0].entity.properties.institute || secondLevelData[i1].values[v0].entity.source
		    	        	                        			});
		    	                                    		}
		    			            					}
		    			            				}
		    	            					}
		            						}
			            				}
	            					}
			            		}
			            		if($scope.mainInfoTabType === 'Academics' && academicLocationList.length > 0){
			        		        worldAcademicChartOptions.markers = academicLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldAcademicChartOptions);
			        			}
		        		        if($scope.mainInfoTabType === 'Finance'){
		        		        	if(bubbleCompittitorsFinanceOptions.data.children.length > 0)
					            		socialBubbleChart(bubbleCompittitorsFinanceOptions);
			        		        if(earningRatioPieChart.length > 0)
				            			$scope.InitializeandPlotPie(earningRatioPieChart, 'earning-ratio-pie-chart');
				            		else{
				            			$scope.entitySearchResult.data_not_found.earning_ratio =false;
				            		}
		        		        }

								if($scope.entitySearchResult.list["competitor_compensation"].length === 0){
									$scope.entitySearchResult.data_not_found.competitor_compensation_data =false;
								}
								if($scope.entitySearchResult.list['dividents'].length === 0){
									$scope.entitySearchResult.data_not_found.dividents_data =false;
								}

			            	}
				           	if(subfetcherData.fetcher === 'sanctionssearch.ofac.treas.gov') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name !== ''){
			            				var object = {
			            					name: subentity.name,
		            						properties: {},
			            					addresses: [],
			            					identification: [],
			            					aliases: '',
			            					source: subentity.source
			            				};
				            			angular.forEach(subentity.properties, function(value, key){
				            				if(value !== '' && value) {
				            					key = key.replace(/_/g, ' ');
					            				object.properties[key] = value;
			            					}
				            			});
			            				var address_key = 0;
				            			angular.forEach(subentity.edges, function(subedv, subedk){
				            				if(subedv.relationshipName === 'identification'){
				            					object.identification.push({
				            						expire_date: subedv.entity.properties.expire_date,
				            						country: subedv.entity.properties.country,
				            						type: subedv.entity.properties.type_,
				            						issue_date: subedv.entity.properties.issue_date,
				            						id: subedv.entity.properties.id_,
				            						risk_score: subedv.entity.properties.risk_score
				            					});
				            				}
				            				if(subedv.relationshipName === 'addresses_' && subedv.entity.name !== ''){
				            					address_key++;
				            					object.addresses.push({
				            						key: 'Address '+ address_key,
				            						address: subedv.entity.properties.city + ' ' + subedv.entity.properties.address + ' ' + subedv.entity.properties.country,
				            						risk_score: subedv.entity.properties.risk_score,
				            						state_province: subedv.entity.properties.state_province
				            					});
				            				}
				            				if(subedv.relationshipName === 'aliases	' && subedv.entity.name !== ''){
				            					object.aliases += subedv.entity.name + ', '
				            				}
				            			});
			            				$scope.entitySearchResult.list["sanctionssearch"].push(object);
			            			}
			            		});
			            	}
				           	if(subfetcherData.fetcher === 'stocks.bloomberg.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentityKey === 0) {
				            			angular.forEach(subentity.edges, function(subedv, subedk) {
				            					if(subedv.relationshipName === 'one_year_stock_prices' || subedv.relationshipName === 'one_month_stock_prices' ||
		                        					subedv.relationshipName === 'five_year_stock_prices' || subedv.relationshipName === 'current_day_stock_prices'){
	                        					var entityBloombergKey = subedv.relationshipName.replace(/_/g, ' ');

	                        					angular.forEach(subedv.entity.edges, function(edge, edgekey){
	                        						if(edge.relationshipName === 'price'){
	                        							if(!bloombergStockLineChartData.hasOwnProperty(entityBloombergKey)) {
	    		                        					bloombergStockLineChartData[entityBloombergKey] = [];
	    		                        				}
	                        							var date = '', value = '';
	                        							angular.forEach(edge.entity.properties, function(propValue, propKey){
	                        								if(propKey.indexOf('date') !== -1)
	                        									date = propValue;
	                        								if(propKey.indexOf('value') !== -1)
	                        									value = propValue;
	                        							});
	                    								bloombergStockLineChartData[entityBloombergKey].push({
	                    									'year': new Date(date),
	                        								'value': value
	                        							});
	                        						}
	                        					});
	                        					$scope.entitySearchResult['stocks'][subedv.relationshipName] = subedv.entity.properties;
		                        			}
			            					if(subedv.relationshipName === 'change'){
			            						$scope.entitySearchResult['stocks'][subedv.relationshipName] = subedv.entity.properties;
			            					}
		                        			if(subedv.relationshipName === 'company_key_executives'){
		                        				if(subedv.entity.properties.name !== '' && subedv.entity.properties.name &&
		                        						subedv.entity.properties.designation !== '' && subedv.entity.properties.designation){
			                        				$scope.entitySearchResult.list['keyExecutiveList'].push({
			                        					name: subedv.entity.properties.name,
			                        					designation: subedv.entity.properties.designation,
			                        					website: subentity.properties.company_website,
			                        					extented_properties: subentity.properties,
			                        					risk_score: subedv.entity.properties.risk_score,
			                        					source: subentity.source
			                        				});
			                        				var index = -1;
				                					angular.forEach(tagCloudNameList, function(object, key){
				                						if(object.text === subedv.entity.properties.name)
				                							index = key
				                					});
				                					if(index === -1){
				                						tagCloudNameList.push({
						            						text: subedv.entity.properties.name,
						            						size: 5,
						            						type: 'person'
						            					});
				                					} else {
				                						tagCloudNameList[index].size += 5;
				                						tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = tagCloudNameList[index].size : '';
				                					}
				                					var indexRole = -1;
				                					angular.forEach(roleDataForPieChart, function(object, key){
				                						if(object.key === subedv.entity.properties.designation)
				                							indexRole = key
				                					});
				                					if(indexRole === -1){
				                						roleDataForPieChart.push({
						            						key: subedv.entity.properties.designation,
						            						doc_count: 1
						            					});
				                					} else {
				                						roleDataForPieChart[indexRole].doc_count++;
				                					}
		                        				}
		                        			}
		                        			if(subedv.relationshipName === 'company_boardmembers'){
		                        				if(subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== ''){
			                        				$scope.entitySearchResult.list['boardmemberList'].push({
			                        					name: subedv.entity.properties.name,
			                        					designation: subedv.entity.properties.designation,
			                        					website: subentity.properties.company_website,
			                        					extented_properties: subentity.properties,
			                        					risk_score: subedv.entity.properties.risk_score,
			                        					source: subentity.source
			                        				});
			                        				var index = -1;
				                					angular.forEach(tagCloudNameList, function(object, key){
				                						if(object.text === subedv.entity.properties.name)
				                							index = key
				                					});
				                					if(index === -1){
				                						tagCloudNameList.push({
						            						text: subedv.entity.properties.name,
						            						size: 5,
						            						type: 'person'
						            					});
				                					} else {
				                						tagCloudNameList[index].size += 5;
				                						tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = tagCloudNameList[index].size : '';
				                					}
				                					var indexRole = -1;
				                					angular.forEach(roleDataForPieChart, function(object, key){
				                						if(object.key === subedv.entity.properties.designation)
				                							indexRole = key
				                					});
				                					if(indexRole === -1){
				                						roleDataForPieChart.push({
						            						key: subedv.entity.properties.designation,
						            						doc_count: 1
						            					});
				                					} else {
				                						roleDataForPieChart[indexRole].doc_count++;
				                					}
				                					var indexPosition = -1;
				                					angular.forEach($scope.entitySearchResult.list['board_membership_counts_data'], function(object, key){
				                						if(object.position === subedv.entity.properties.designation)
				                							indexPosition = key
				                					});
				                					if(indexPosition === -1){
				                						$scope.entitySearchResult.list['board_membership_counts_data'].push({
						            						position: subedv.entity.properties.designation,
						            						count: 1
						            					});
				                					} else {
				                						$scope.entitySearchResult.list['board_membership_counts_data'][indexPosition].count++;
				                					}
		                        				}
		                        			}
				                        });
				            			angular.forEach(subentity.properties, function(subValue, subKey){
				            				$scope.entitySearchResult['stocks']['bloomberg_prop'][subKey] = subValue;
				            			});
			            			}
			            		});
							    if(!_.isEmpty(bloombergStockLineChartData)){
				            		angular.forEach(bloombergStockLineChartData, function(bloombergStockData, bloombergStockKey){
				            			bloombergStockData = bloombergStockData.sort(function(a,b){
				            				return moment(a.year).isAfter(b.year) ? 1 : -1;
				            			});
			            			});
				            		$scope.bloombergStockLineChartData = bloombergStockLineChartData;
							   	 	$scope.bloombergDataKey = Object.keys(bloombergStockLineChartData)[0];
							   	 	bloombergChartOptions.data = bloombergStockLineChartData['five year stock prices'];
							   	 	bloombergChartDetailsOptions.data = bloombergStockLineChartData['five year stock prices'];
							   	 	$scope.bloombergSelectedTab = 'five year stock prices';
							   	 	bloombergChartOptions.y_axis_title = $scope.entitySearchResult.exchange_currency;
							   	 	bloombergChartDetailsOptions.y_axis_title = $scope.entitySearchResult.exchange_currency;
							   	 	if(bloombergStockLineChartData['five year stock prices'].length > 0){
								   	 	loadlineData(bloombergChartOptions);
								   	 	loadlineData(bloombergChartDetailsOptions);
							   	 	}else{
							   	 		$scppe.entitySearchResult.data_not_found.stocks_prices = false;
							   	 	}
								}
								else{
									$scppe.entitySearchResult.data_not_found.stocks_prices = false;
								}
			            		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            	}
				           	if(subfetcherData.fetcher === 'people.gov.uk') {
				           		for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
				           			if(subfetcherData.entities[fl].name.indexOf(name) !== -1){
				           				var object = {
			            					properties: subentity.properties,
			            					current_roles: [],
			            					profile_url: [],
			            					previous_roles: []
			            				};
			            				for (var el = 0; el < subfetcherData.entities[fl].edges.length; el++) {
			            					if(subfetcherData.entities[fl].edges[el].relationshipName === 'current_roles'){
			            						object.current_roles.push(subfetcherData.entities[fl].edges[el].entity.properties);
			            					}
			            					if(subfetcherData.entities[fl].edges[el].relationshipName === 'profile_url'){
			            						object.profile_url.push(subfetcherData.entities[fl].edges[el].entity.properties);
			            					}
			            					if(subfetcherData.entities[fl].edges[el].relationshipName === 'previous_roles' && subfetcherData.entities[fl].edges[el].entity.properties.title !== '' &&
			            							subfetcherData.entities[fl].edges[el].entity.properties.title){
			            						object.previous_roles.push(subfetcherData.entities[fl].edges[el].entity.properties);
			            						var indexRole = -1;
			            						for (var rd = 0; rd < roleDataForPieChart.length; rd++) {
			            							if(roleDataForPieChart[rd].key === subfetcherData.entities[fl].edges[el].entity.properties.title)
			                							indexRole = rd
			            						}
			            						if(indexRole === -1){
			                						roleDataForPieChart.push({
					            						key: subfetcherData.entities[fl].edges[el].entity.properties.title,
					            						doc_count: 1
					            					});
			                					} else {
			                						roleDataForPieChart[indexRole].doc_count++;
			                					}
			            					}//previousrole if ends
			            				}//el loop ends
			            				$scope.entitySearchResult.list['government_positions'] = object;
				           			}
									   $scope.entitySearchResult.list['government_positions_list'].push(subfetcherData.entities[fl].properties);
									   if( $scope.entitySearchResult.list['government_positions_list'].length === 0){
										$scope.entitySearchResult.data_not_found.gov_people =false;
									   }
				           		}
				           		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            	}
				           	if(subfetcherData.fetcher === 'personal-profiles.bloomberg.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(subentity.name.indexOf(name) !== -1){
			            				var object = {
			            					properties: subentity.properties,
			            					career_history: [],
			            					publications: [],
			            					honorary_digrees: [],
			            					board_memberships: [],
			            					awards: [],
			            					education: []
			            				};
			            				angular.forEach(subentity.edges, function(subedv, subedk){
			            					if(subedv.relationshipName === 'career_history' && subedv.entity.properties.job_title !== '' &&
			            							subedv.entity.properties.job_title){
			            						object.career_history.push(subedv.entity.properties);
			                            		if(subedv.entity.properties.company !== '' && subedv.entity.properties.company) {
			                                		var locationindex = -1;
			                                		for(var i = 0; i < careerLocationList.length; i++){
			                                			if(careerLocationList[i].name === subedv.entity.properties.company)
			                                				locationindex = i;
			                                		}
			                                		if(locationindex === -1){
			                                			careerLocationList.push({
			    	                        				name: subedv.entity.properties.company,
			    	                        				long: '',
			    	                        				lat: '',
			    	                        				mark: 'assets/images/redpin.png',
			    	                        				source: subedv.source,
			    	                        				title: subedv.entity.properties.company || subedv.source
			    	                        			});
			                                		}
			                            		}
			            					}
			            					if(subedv.relationshipName === 'publications'){
			            						object.publications.push(subedv.entity.properties);
			            					}
			            					if(subedv.relationshipName === 'honorary_digrees'){
			            						object.honorary_digrees.push(subedv.entity.properties);
			            					}
			            					if(subedv.relationshipName === 'awards'){
			            						object.awards.push(subedv.entity.properties);
			            					}
			            					if(subedv.relationshipName === 'education'){
			            						object.education.push(subedv.entity.properties);
			            					}
			            					if(subedv.relationshipName === 'board_memberships'){
			            						object.board_memberships.push(subedv.entity.properties);
			            						var index = -1;
			                					angular.forEach($scope.entitySearchResult.list['board_membership_counts_data'], function(valueData, key){
			                						if(valueData.position === subedv.entity.properties.position_holding)
			                							index = key
			                					});
			                					if(index === -1){
			                						$scope.entitySearchResult.list['board_membership_counts_data'].push({
					            						position: subedv.entity.properties.position_holding,
					            						count: 1
													});
			                					} else {
			                						$scope.entitySearchResult.list['board_membership_counts_data'][index].count++;
			                					}
			            					}
			            				});
				            			$scope.entitySearchResult.list['personal_profiles_bloomberg'] = object;
			            			}
			            			$scope.entitySearchResult.list['similar_profiles_list'].push(subentity.properties);
		            			});
			            		if($scope.mainInfoTabType === 'Career' && careerLocationList.length > 0){
			            			worldCareerChartOptions.markers = careerLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldCareerChartOptions);
								}
								if($scope.entitySearchResult.list['personal_profiles_bloomberg']['education'].length===0 || _.isEmpty($scope.entitySearchResult.list['personal_profiles_bloomberg']) ){
									$scope.entitySearchResult.data_not_found.schooling =false;
								}
								if($scope.entitySearchResult.list['personal_profiles_bloomberg'].awards.length===0 || _.isEmpty($scope.entitySearchResult.list['personal_profiles_bloomberg']) ){
									$scope.entitySearchResult.data_not_found.awards_data =false;
								}
								if($scope.entitySearchResult.list['personal_profiles_bloomberg'].honorary_digrees.length===0 || _.isEmpty($scope.entitySearchResult.list['personal_profiles_bloomberg']) ){
									$scope.entitySearchResult.data_not_found.honoray_data =false;
								}
								if($scope.entitySearchResult.list['similar_profiles_list'].length === 0){
									$scope.entitySearchResult.data_not_found.Similarprofile =false;
								}
			            	}
				           	if(subfetcherData.fetcher === 'documents.google.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
		            				$scope.entitySearchResult.list['document_list'].push({
		            					document_title: subentity.properties.document_title,
		            					mime: subentity.properties.mime,
		            					url: subentity.properties.url,
		            					file_format: subentity.properties.file_format,
		            					source: subentity.source,
		            					risk_score: subentity.properties.risk_score
		            				});
								});
								if($scope.entitySearchResult.list['document_list'].length === 0){
									$scope.entitySearchResult.data_not_found.documents_data =false;
									$scope.entitySearchResult.data_not_found.popular_tags =false;
								}
			            		assetsRatioPieData.push({
			        				"key":"Documents",
			        				"doc_count": $scope.entitySearchResult.list['document_list'].length
			        			});
		            			if($scope.mainInfoTabType === 'Documents' && $scope.entitySearchResult.list['document_list'].length > 0){
			            			$scope.InitializeandPlotPie(assetsRatioPieData, 'pieChartAssetsRatio');
			            			World(documentWorldChartOptions);
			            		}
								if($scope.entitySearchResult.list['document_list'].length > 0) {
									$scope.entitySearchResult.list['is_person_popular_chart'] = false;
									var uniq = $scope.entitySearchResult.list['document_list'];
									uniq = _.uniqBy(uniq, function(e) {
										return e.document_title
									});
									for(var i=0; i < uniq.length ;i++){
										popularTagsOptions.data.push({
											text : uniq[i]['document_title'],
											url : uniq[i]['url'],
											size : 5,
											type : "document"
										})
									};
							    	EnrichSearchGraph.plotTagCloudChartForLiveFeed(popularTagsOptions);
								}
			            	}
			            	if(subfetcherData.fetcher === 'channels.youtube.com'){
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
				            		angular.forEach(subentity.edges, function(subedv, subedk) {
				            			if((subedv.relationshipName) == 'videos'){
				            				angular.forEach(subedv.entity.edges, function(subedvVal, subedvKey){
				            					if((subedvVal.relationshipName) == 'thumbnails'){
				            						angular.forEach(subedvVal.entity.edges, function(subedvEntityVal, subedvEntityKey){
				            							if((subedvEntityVal.relationshipName) == 'high' && subedvEntityVal.entity.name !== '' && subedvEntityVal.entity.name){
				            								$scope.entitySearchResult.list["videos_list"].push({
								            					title: subedv.entity.properties.title,
								            					watch_url: subedv.entity.properties.watch_url,
								            					thumbnail: subedvEntityVal.entity.name,
								            					description: subedv.entity.properties.description,
								            					pub_date: new Date(subedv.entity.properties.pub_date)
								            				});
						                					var indexAssets = -1;
						                					angular.forEach(assetsTrendLineChart, function(objectValue, key){
						                						if(objectValue.monthName === monthNames[new Date(subedv.entity.properties.pub_date).getMonth()])
						                							indexAssets = key
						                					});
						                					if(indexAssets === -1){
						                						assetsTrendLineChart.push({
								            						monthName: monthNames[new Date(subedv.entity.properties.pub_date).getMonth()],
								            						date: new Date(subedv.entity.properties.pub_date),
								            						sum: 1
								            					});
						                					} else {
						                						assetsTrendLineChart[indexAssets].sum++;
						                					}
				            							}
				            						});
				            					}
				            				});
			                        	 }
				            		});
			            		});
			            		assetsRatioPieData.push({
			        				"key":"Videos",
			        				"doc_count": $scope.entitySearchResult.list['videos_list'].length
			        			});
			        			if($scope.mainInfoTabType === 'Documents' && $scope.entitySearchResult.list['videos_list'].length > 0){
				        			$scope.InitializeandPlotPie(assetsRatioPieData, 'pieChartAssetsRatio');
				        			World(documentWorldChartOptions);
				        			if(assetsTrendLineChart.length > 0)
				        				plotLineChart(assetsTrendLineChart, "#assets-trend-line-chart", $("#assets-trend-line-chart").width(), '300');
								}
								if($scope.entitySearchResult.list["videos_list"].length === 0){
									$scope.entitySearchResult.data_not_found.video_data =false;
								}
			            	}
			            	if(subfetcherData.fetcher === 'profiles.littlesis.com') {
			            		var firstLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(subfetcherData.entities[0].edges);
			            		for(var i0 = 0; i0 < firstLevelData.length; i0++){
			            			if(firstLevelData[i0].key === 'financial_supports'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
				            				var secondLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(firstLevelData[i0].values[v0].entity.edges);
		            						for(var i1 = 0; i1 < secondLevelData.length; i1++){
			            						if(secondLevelData[i1].key === 'politicians'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							if(secondLevelData[i1].values[e0].entity.properties.amount !== '' && secondLevelData[i1].values[e0].entity.properties.amount &&
					            								secondLevelData[i1].values[e0].entity.properties.recipient_name !== '' && secondLevelData[i1].values[e0].entity.properties.recipient_name){
					            							secondLevelData[i1].values[e0].entity.properties['source'] = secondLevelData[i1].values[e0].entity.source;
					            							secondLevelData[i1].values[e0].entity.properties['tranx_type'] = 'Person';
					            							$scope.entitySearchResult.list['person_transaction_list'].push(secondLevelData[i1].values[e0].entity.properties);
					            							$scope.entitySearchResult.list['associated_companies']['politicians'].push(secondLevelData[i1].values[e0].entity.properties);
				            							}
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'organizations'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							if(secondLevelData[i1].values[e0].entity.properties.amount !== '' && secondLevelData[i1].values[e0].entity.properties.amount &&
					            								secondLevelData[i1].values[e0].entity.properties.org_name !== '' && secondLevelData[i1].values[e0].entity.properties.org_name){
					            							secondLevelData[i1].values[e0].entity.properties['source'] = secondLevelData[i1].values[e0].entity.source;
					            							secondLevelData[i1].values[e0].entity.properties['tranx_type'] = 'Organization';
					            							$scope.entitySearchResult.list['person_transaction_list'].push(secondLevelData[i1].values[e0].entity.properties);
					            							$scope.entitySearchResult.list['associated_companies']['political_organizations'].push(secondLevelData[i1].values[e0].entity.properties);
				            							}
				            						}
				            					}
		            						}
			            				}
	            					}
			            			if(firstLevelData[i0].key === 'key_relationships'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
					            			var secondLevelData = d3.nest().key(function(d){return d.relationshipName}).entries(firstLevelData[i0].values[v0].entity.edges);
		            						for(var i1 = 0; i1 < secondLevelData.length; i1++){
			            						if(secondLevelData[i1].key === 'memberships' || secondLevelData[i1].key === 'other_positions_&_memberships'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							$scope.entitySearchResult.list['associated_companies']['memberships'].push({
						                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
				                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
				                        					url: secondLevelData[i1].values[e0].entity.properties.url,
				                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
				                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
				                        					source: secondLevelData[i1].values[e0].entity.source
							            				});
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'holdings'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							if(secondLevelData[i1].values[e0].entity.properties.entity_name !== '' && secondLevelData[i1].values[e0].entity.properties.entity_name &&
				            									secondLevelData[i1].values[e0].entity.properties.relationship !== '' && secondLevelData[i1].values[e0].entity.properties.relationship){
					            							$scope.entitySearchResult.list['associated_companies']['holdings'].push({
							                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
					                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
					                        					url: secondLevelData[i1].values[e0].entity.properties.url,
					                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
					                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
					                        					source: secondLevelData[i1].values[e0].entity.source
								            				});
				            							}
				            						}
				            						if($scope.entitySearchResult.list['associated_companies']['holdings'].length === 0){
						            					$scope.entitySearchResult.data_not_found.holdings = false;
						            				}
				            					}
			            						if(secondLevelData[i1].key === 'donation/grant_recipients'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							$scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].push({
						                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
				                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
				                        					url: secondLevelData[i1].values[e0].entity.properties.url,
				                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
				                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
				                        					source: secondLevelData[i1].values[e0].entity.source
							            				});
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'business_positions'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							if(secondLevelData[i1].values[e0].entity.properties.entity_name !== '' && secondLevelData[i1].values[e0].entity.properties.entity_name &&
			            										secondLevelData[i1].values[e0].entity.properties.relationship !== '' && secondLevelData[i1].values[e0].entity.properties.relationship){
						            						secondLevelData[i1].values[e0].entity.properties['source'] = secondLevelData[i1].values[e0].entity.source;
					            							$scope.entitySearchResult.list['associated_companies']['business_positions'].push(secondLevelData[i1].values[e0].entity.properties);
					            							var indexRole = -1;
						                					angular.forEach(roleDataForPieChart, function(object, key){
						                						if(object.key === secondLevelData[i1].values[e0].entity.properties.relationship)
						                							indexRole = key
						                					});
						                					if(indexRole === -1){
						                						roleDataForPieChart.push({
								            						key: secondLevelData[i1].values[e0].entity.properties.relationship,
								            						doc_count: 1
								            					});
						                					} else {
						                						roleDataForPieChart[indexRole].doc_count++;
						                					}
						                					if(secondLevelData[i1].values[e0].entity.properties.entity_name !== '' && secondLevelData[i1].values[e0].entity.properties.entity_name) {
						                                		var locationindex = -1;
						                                		for(var i = 0; i < careerLocationList.length; i++){
						                                			if(careerLocationList[i].name === secondLevelData[i1].values[e0].entity.properties.entity_name)
						                                				locationindex = i;
						                                		}
						                                		if(locationindex === -1){
						                                			careerLocationList.push({
						    	                        				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
						    	                        				long: '',
						    	                        				lat: '',
						    	                        				mark: 'assets/images/redpin.png',
						    	                        				source: secondLevelData[i1].values[e0].source,
						    	                        				title: secondLevelData[i1].values[e0].entity.properties.entity_name || secondLevelData[i1].values[e0].source
						    	                        			});
						                                		}
						                            		}
				            							}
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'education'){
				            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							$scope.entitySearchResult.list['associated_companies']['education'].push({
						                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
				                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
				                        					url: secondLevelData[i1].values[e0].entity.properties.url,
				                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
				                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
				                        					source: secondLevelData[i1].values[e0].entity.source
							            				});
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'family'){
			            							for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
			            								if(secondLevelData[i1].values[e0].entity.properties.entity_name !== '' && secondLevelData[i1].values[e0].entity.properties.entity_name &&
			            										secondLevelData[i1].values[e0].entity.properties.relationship !== '' && secondLevelData[i1].values[e0].entity.properties.relationship){
					            							$scope.entitySearchResult.list['associated_companies']['family'].push({
							                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
					                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
					                        					url: secondLevelData[i1].values[e0].entity.properties.url,
					                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
					                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
					                        					source: secondLevelData[i1].values[e0].entity.source
								            				});
			            								}
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'professional_associates'){
			            							for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
				            							$scope.entitySearchResult.list['associated_companies']['professional_associates'].push({
						                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
				                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
				                        					url: secondLevelData[i1].values[e0].entity.properties.url,
				                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
				                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
				                        					source: secondLevelData[i1].values[e0].entity.source
							            				});
				            						}
				            					}
			            						if(secondLevelData[i1].key === 'friends'){
			            							for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
			            								if(secondLevelData[i1].values[e0].entity.properties.entity_name !== '' && secondLevelData[i1].values[e0].entity.properties.entity_name &&
			            										secondLevelData[i1].values[e0].entity.properties.relationship !== '' && secondLevelData[i1].values[e0].entity.properties.relationship){
						            						$scope.entitySearchResult.list['associated_companies']['friends'].push({
							                    				name: secondLevelData[i1].values[e0].entity.properties.entity_name,
					                        					risk_score: secondLevelData[i1].values[e0].entity.properties.risk_score,
					                        					url: secondLevelData[i1].values[e0].entity.properties.url,
					                        					relationship: secondLevelData[i1].values[e0].entity.properties.relationship,
					                        					summary: secondLevelData[i1].values[e0].entity.properties.summary,
					                        					source: secondLevelData[i1].values[e0].entity.source
								            				});
			            								}
				            						}
				            					}
		            						}
			            				}
			            				if($scope.entitySearchResult.list['associated_companies']['holdings'].length === 0){
						            		$scope.entitySearchResult.data_not_found.holdings = false;
						            	}
	            					}
			            			if(firstLevelData[i0].key === 'connected_industries'){
			            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
					            			if(firstLevelData[i0].values[v0].entity.name !== '' && firstLevelData[i0].values[v0].entity.name){
					            				$scope.entitySearchResult.list['associated_companies']['connected_industries'].push({
				                    				name: firstLevelData[i0].values[v0].entity.name,
		                        					risk_score: firstLevelData[i0].values[v0].entity.properties.risk_score,
		                        					source: firstLevelData[i0].values[v0].entity.source
					            				});
					            			}
			            				}
	            					}
			            		}
                          		if($scope.mainInfoTabType == 'Risks'){
		            		       PlotPoliticalChart();
				            	}
			            		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            		if($scope.mainInfoTabType === 'Career' && careerLocationList.length > 0){
			            			worldCareerChartOptions.markers = careerLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldCareerChartOptions);
								}
								if($scope.entitySearchResult.list['associated_companies']['family'].length === 0){
									littlesis_family =false;
									familymember();
								}
								if($scope.entitySearchResult.list['associated_companies']['friends'].length ===0){
									litllesis_friends =false;
									familymember();
								}
			            	}
			            	if(subfetcherData.fetcher === 'profiles.linkedin.com'){
			            		for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
			            			var subentity =	subfetcherData.entities[fl];
			            			for (var ed = 0; ed < subentity.edges.length; ed++) {
			            				var subedges = subentity.edges[ed];
			            				if(subedges.relationshipName === 'has_education'){
			            					if(subedges.entity.properties.school_name !=='' && subedges.entity.properties.school_name ){
				                        		$scope.entitySearchResult.list["has_education"].push({
				                        			school_name: subedges.entity.properties.school_name,
				                        			type: subedges.entity.properties.type_,
				                        			url: subedges.entity.properties.url,
				                        			logo: subedges.entity.properties.logo || '',
				                        			risk_score: subedges.entity.properties.risk_score,
				                        			start_time : subedges.entity.properties.start_time,
				                        			end_time :  subedges.entity.properties.end_time,
				                        			source: subedges.entity.source
						            			});
				                        	}
			                        		if(subedges.entity.properties.school_name !== '' && subedges.entity.properties.school_name) {
			                            		var index = -1;
			                            		for(var i = 0; i < academicLocationList.length; i++){
			                            			if(academicLocationList[i].name === subedges.entity.properties.school_namee)
			                            				index = i;
			                            		}
			                            		if(index === -1){
			                            			academicLocationList.push({
				                        				name: subedges.entity.properties.school_name,
				                        				long: '',
				                        				lat: '',
				                        				mark: 'assets/images/redpin.png',
				                        				source: subedges.entity.source,
				                        				title:  subedges.entity.properties.school_name || subedges.entity.source
				                        			});
			                            		}
			                        		}
                        				}// has education ends
		                        		if(subedges.relationshipName === 'previous_work_places'){
			                        		if(subedges.entity.properties.jobtitle !== '' && subedges.entity.properties.jobtitle && subedges.entity.properties.jobtitle !== '.'){
				                    			$scope.entitySearchResult.list["previous_work_places"].push({
					            					title: subedges.entity.properties.jobtitle,
					            					company: subedges.entity.properties.company,
					            					employed_location: subedges.entity.properties.employed_location,
					            					details: subedges.entity.properties.details,
					            					link: subedges.entity.properties.company_link,
					            					employed_dates: subedges.entity.properties.employed_dates
					            				});
				            					var indexRole = -1;
			                					angular.forEach(roleDataForPieChart, function(object, key){
			                						if(object.key === subedges.entity.properties.jobtitle)
			                							indexRole = key
			                					});
			                					if(indexRole === -1){
			                						roleDataForPieChart.push({
					            						key: subedges.entity.properties.jobtitle,
					            						doc_count: 1
					            					});
			                					} else {
			                						roleDataForPieChart[indexRole].doc_count++;
			                					}
			                        		}
			                       		}//previous work location ends
			                       		if(subedges.relationshipName === 'skills'){
				                        	subedges.entity.name !== '' && subedges.entity.name && $scope.entitySearchResult.list["skills"].indexOf(subedges.entity.name) === -1 ? $scope.entitySearchResult.list["skills"].push(subedges.entity.name): '';
				                        		var index = -1;
				            					angular.forEach(tagCloudSkillsNameList, function(object, key){
				            						if(object.text === subedges.entity.name)
				            							index = key
				            					});
				            					if(index === -1){
				            						tagCloudSkillsNameList.push({
					            						text: subedges.entity.name,
					            						size: 5,
					            						type: 'skills'
					            					});
				            					} else {
				            						tagCloudSkillsNameList[index].size += 5;
				            						tagCloudSkillsNameList[index].size > maxSkillsDomainRangeValue ? maxSkillsDomainRangeValue = tagCloudSkillsNameList[index].size : '';
				            					}
                        				}//skill ends here
                        				if(subedges.relationshipName === 'interests'){
                        					if(subedges.entity.name !== '' && subedges.entity.name){
                        						$scope.entitySearchResult.list["interests"].push({
                        							entity_name: subedges.entity.properties.entity_name,
                        							followers_count: subedges.entity.properties.followers_count
                        						});
                        					}
                        				}//interest Ends
			            			}
			            		}
			            	}
			            	if (subfetcherData.fetcher ==='sci.ccc.nashville.gov') {
			            		//for (var fl = 0; fl < subfetcherData.entities.length; fl++) {
			            			var edgevalue = subfetcherData.entities[0];
			              			for (var ed = 0; ed < edgevalue.edges.length; ed++) {
			            				var edges = edgevalue.edges[ed];
			            				if(edges.relationshipName === 'crminal_history'){
			            					for (var i = 0; i < edges.entity.edges.length; i++) {
			            						var subedge = edges.entity.edges[i];
			            						if(subedge.relationshipName === 'case_details'){
			            							$scope.entitySearchResult.list["sci_ccc_nashville_gov"].push({
			            								case_number: subedge.entity.properties.case_number,
			            								charged_offense: subedge.entity.properties.charged_offense,
			            								offense_date: subedge.entity.properties.offense_date,
			            								name : subedge.entity.properties.name,
			            								amended_offense : subedge.entity.properties.amended_offense,
			            								case_status : subedge.entity.properties.case_status,
			            								citationarrest_date : subedge.entity.properties.citationarrest_date,
			            								concurrent_with : subedge.entity.properties.concurrent_with,
			            								consecutive_to : subedge.entity.properties.consecutive_to,
			            								convicted_offense : subedge.entity.properties.convicted_offense,
			            								convicted_typefm : subedge.entity.properties.convicted_typefm,
			            								date_of_birth : subedge.entity.properties.date_of_birth,
			            								disposition : subedge.entity.properties.disposition,
			            								disposition_date : subedge.entity.properties.disposition_date,
			            								source : subfetcherData.entities[0].source
			            							});
			            						}//case details ends
			            						if(subedge.relationshipName === 'court_costs'){
			            							$scope.entitySearchResult.list['nashville_courtdetails']['court_costs'].push(subedge.entity.properties);
			            						}//court_cost ends
			            						if(subedge.relationshipName === 'appearance_details'){
			            								$scope.entitySearchResult.list['nashville_courtdetails']['appearance_details'].push(subedge.entity.properties);
			            						}// appearance_details ends
			            						if(subedge.relationshipName === 'probation'){
			            							$scope.entitySearchResult.list['nashville_courtdetails']['probation'].push(subedge.entity.properties);
			            						}//probation ends
			            						if(subedge.relationshipName === 'incarceration'){
			            							$scope.entitySearchResult.list['nashville_courtdetails']['incarceration'].push(subedge.entity.properties);
			            						}//incarceration ends
			            						if(subedge.relationshipName === 'disposition'){
			            							$scope.entitySearchResult.list['nashville_courtdetails']['disposition'].push(subedge.entity.properties);
			            						}//disposition ends
			            					}//for loop ends
			            				}//edges if end
			            			}//ed loop ends
			            		//}//fl loop ends
			            	}
			            	if(subfetcherData.fetcher === 'person.corporationwiki.com') {
			            		for(var f0 = 0; f0 < subfetcherData.entities.length; f0++){
			            			if(subfetcherData.entities[f0].name.indexOf(name) !== -1) {
			            				$scope.entitySearchResult.list['corporationwiki'].name === '' ? $scope.entitySearchResult.list['corporationwiki'].name = subfetcherData.entities[f0].name : '';
			            				$scope.entitySearchResult.list['corporationwiki'].location === '' ? $scope.entitySearchResult.list['corporationwiki'].location = subfetcherData.entities[f0].properties.location : '';
			            				$scope.entitySearchResult.list['corporationwiki'].current_job === '' ? $scope.entitySearchResult.list['corporationwiki'].current_job = subfetcherData.entities[f0].properties.cur_job : '';
					            		if(subfetcherData.entities[f0].properties.cur_job !== '' && subfetcherData.entities[f0].properties.cur_job &&
					            				subfetcherData.entities[f0].properties.location !== '' && subfetcherData.entities[f0].properties.location){
					            			$scope.entitySearchResult.list['corporationwiki']['previous_job'].push({
					            				companyName: subfetcherData.entities[f0].properties.cur_job,
					            				location: subfetcherData.entities[f0].properties.location
					            			});
					            			var indexRole = -1;
		                					angular.forEach(roleDataForPieChart, function(object, key){
		                						if(object.key === subfetcherData.entities[f0].properties.cur_job)
		                							indexRole = key
		                					});
		                					if(indexRole === -1){
		                						roleDataForPieChart.push({
				            						key: subfetcherData.entities[f0].properties.cur_job,
				            						doc_count: 1
				            					});
		                					} else {
		                						roleDataForPieChart[indexRole].doc_count++;
		                					}
		                					if(subfetcherData.entities[f0].properties.location !== '' && subfetcherData.entities[f0].properties.location) {
		                                		var locationindex = -1;
		                                		for(var i = 0; i < careerLocationList.length; i++){
		                                			if(careerLocationList[i].name === subfetcherData.entities[f0].properties.location)
		                                				locationindex = i;
		                                		}
		                                		if(locationindex === -1){
		                                			careerLocationList.push({
		    	                        				name: subfetcherData.entities[f0].properties.location,
		    	                        				long: '',
		    	                        				lat: '',
		    	                        				mark: 'assets/images/redpin.png',
		    	                        				source: subfetcherData.entities[f0].source,
		    	                        				title: subfetcherData.entities[f0].properties.location || subfetcherData.entities[f0].source
		    	                        			});
		                                		}
		                            		}
					            		}
			            				var firstLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(subfetcherData.entities[f0].edges);
					            		for(var i0 = 0; i0 < firstLevelData.length; i0++){
					            			if(firstLevelData[i0].key === 'known_addresses'){
					            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
					            					if(firstLevelData[i0].values[v0].entity.properties.city !== '' && firstLevelData[i0].values[v0].entity.properties.city &&
					            							firstLevelData[i0].values[v0].entity.properties.region !== '' && firstLevelData[i0].values[v0].entity.properties.region){
					            						$scope.entitySearchResult.list['corporationwiki']['known_addresses'].push({
					            							address: firstLevelData[i0].values[v0].entity.properties.street_address + ', ' + firstLevelData[i0].values[v0].entity.properties.city
					            										+ ', ' + firstLevelData[i0].values[v0].entity.properties.region + '-' + firstLevelData[i0].values[v0].entity.properties.post_code,
						            						address_url: firstLevelData[i0].values[v0].entity.properties.address_url
						            					});
			            							}
					            				}
			            					}
					            			if(firstLevelData[i0].key === 'connected_companies'){
					            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
					            					if(firstLevelData[i0].values[v0].entity.properties.company_name !== '' && firstLevelData[i0].values[v0].entity.properties.company_name &&
					            							firstLevelData[i0].values[v0].entity.properties.role !== '' && firstLevelData[i0].values[v0].entity.properties.role){
					            						$scope.entitySearchResult.list['corporationwiki']['connected_companies'].push({
						            						number_of_keypeople: firstLevelData[i0].values[v0].entity.properties.number_of_keypeople,
						            						role: firstLevelData[i0].values[v0].entity.properties.role,
						            						company_name: firstLevelData[i0].values[v0].entity.properties.company_name,
						            						incorporated: firstLevelData[i0].values[v0].entity.properties.incorporated,
						            						company_status: firstLevelData[i0].values[v0].entity.properties.company_status,
						            						company_url: firstLevelData[i0].values[v0].entity.properties.company_url
						            					});
			            							}
					            				}
					            			}
					            			if(firstLevelData[i0].key === 'connecitons'){
					            				for(var v0 = 0; v0 < firstLevelData[i0].values.length; v0++){
					            					if(firstLevelData[i0].values[v0].entity.name !== '' && firstLevelData[i0].values[v0].entity.name){
						            					var connectionObject = {
						            						name: firstLevelData[i0].values[v0].entity.name,
						            						number_of_keypeople: '',
						            						role: '',
						            						company_name: '',
						            						incorporated: '',
						            						company_status: '',
						            						company_url: ''
						            					};
							            				var secondLevelData =d3.nest().key(function(d){return d.relationshipName}).entries(firstLevelData[i0].values[v0].entity.edges);
					            						for(var i1 = 0; i1 < secondLevelData.length; i1++){
						            						if(secondLevelData[i1].key === 'connected_companies'){
							            						for(var e0 = 0; e0 < secondLevelData[i1].values.length; e0++){
							            							if(secondLevelData[i1].values[e0].entity.properties.company_name !== '' && secondLevelData[i1].values[e0].entity.properties.company_name &&
								            								secondLevelData[i1].values[e0].entity.properties.role !== '' && secondLevelData[i1].values[e0].entity.properties.role){
								            							connectionObject.number_of_keypeople = secondLevelData[i1].values[e0].entity.properties.number_of_keypeople;
								            							connectionObject.role = secondLevelData[i1].values[e0].entity.properties.role;
								            							connectionObject.company_name = secondLevelData[i1].values[e0].entity.properties.company_name;
								            							connectionObject.incorporated = secondLevelData[i1].values[e0].entity.properties.incorporated;
								            							connectionObject.company_status = secondLevelData[i1].values[e0].entity.properties.company_status;
								            							connectionObject.company_url = secondLevelData[i1].values[e0].entity.properties.company_url;

								            							var indexRole = -1;
									                					for( var i=0; i<professionalConnectionForPieChart.length; i++){
									                						if(professionalConnectionForPieChart[i].key === secondLevelData[i1].values[e0].entity.properties.role)
									                							indexRole = key
									                					};
									                					if(indexRole === -1){
									                						professionalConnectionForPieChart.push({
											            						key: secondLevelData[i1].values[e0].entity.properties.role,
											            						doc_count: 1
											            					});
									                					} else {
									                						professionalConnectionForPieChart[indexRole].doc_count++;
									                					}
							            							}
							            						}
							            					}
					            						}
					            						$scope.entitySearchResult.list['corporationwiki']['connecitons'].push(connectionObject);
					            					}
					            				}
					            			}
					            		}
			            			}
			            		}
			            		if($scope.mainInfoTabType === 'Career' && roleDataForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
			            		if($scope.mainInfoTabType === 'Career' && professionalConnectionForPieChart.length > 0)
			            			$scope.InitializeandPlotPie(professionalConnectionForPieChart, 'connection-role-based-pie-chart');
			            		if($scope.mainInfoTabType === 'Career' && careerLocationList.length > 0){
			            			worldCareerChartOptions.markers = careerLocationList;
			        		   	 	/*calling world map chart for academics function*/
			        		        World(worldCareerChartOptions);
								}
								if($scope.entitySearchResult.list['corporationwiki']['connected_companies'].length === 0){
									$scope.entitySearchResult.data_not_found.other_membership =false;
								}
							}
							if (subfetcherData.fetcher === 'geni.com') {
								$scope.entitySearchResult.list.geni.timeline.push(subfetcherData.entities[0].properties);
								for (var i = 0; i < subfetcherData.entities[0].edges.length; i++) {
									var edgevalue = subfetcherData.entities[0].edges[i];
									if (edgevalue.relationshipName === 'family') {
										$scope.entitySearchResult.list.geni.family.push({
											entityType: edgevalue.entity.entityType,
											name: edgevalue.entity.properties.name,
											relationship: edgevalue.entity.properties.relationship,
											image: edgevalue.entity.properties.image
										});
									}
									 
								}
							}
							if (subfetcherData.fetcher === 'person.forbes.com') {
								$scope.entitySearchResult.list.forbes.details.push(subfetcherData.entities[0].properties);
								if (subfetcherData.entities[0] && subfetcherData.entities[0].properties) {
									$scope.entitySearchResult.list.citizenship.push(subfetcherData.entities[0].properties.citizenship);
								}
								var forbes_data = d3.nest().key(function (d) {
									return d.relationshipName;
								}).entries(subfetcherData.entities[0].edges);
								for (var fl = 0; fl < forbes_data.length; fl++) {
									if (forbes_data[fl].key === "connections") {
									}
									if (forbes_data[fl].key === "special_notes"){
									}
									if (forbes_data[fl].key === "biography") {
									}
									if (forbes_data[fl].key === "related_news") {
									}
								} //fl  for loop ends
							}
			            } else {
		            		var fetcherName = subfetcherData['fetcher'];
							fetcherName !== '' && fetcherName ? entityPersonEmptyObject[fetcherName] = {} : '';
							if(subfetcherData.fetcher === 'instagram.com'){
								$scope.entitySearchResult.data_not_found.socialmedia_follower =false;
								$scope.entitySearchResult.data_not_found.instagram = false;
							}
							if(subfetcherData.fetcher === 'twitter.com'){
								$scope.entitySearchResult.data_not_found.socialmedia_follower =false;
								$scope.entitySearchResult.data_not_found.recent_tweets = false;
								$scope.entitySearchResult.data_not_found.twitter_tags = false;
								$scope.entitySearchResult.data_not_found.twitter_fol_trend = false;
								$scope.entitySearchResult.data_not_found.twitter_inter_ratio = false;

							}
							if(subfetcherData.fetcher === 'people.gov.uk') {
								$scope.entitySearchResult.data_not_found.gov_people =false;
							}
							if(subfetcherData.fetcher === 'person.corporationwiki.com') {
								$scope.entitySearchResult.data_not_found.other_membership =false;
								$scope.entitySearchResult.data_not_found.professional_connection =false;
							}
							if(subfetcherData.fetcher === 'person.wikipedia.com') {
								$scope.entitySearchResult.data_not_found.carrerProgression =false;
								$scope.entitySearchResult.data_not_found.academic_timeline =false;
								$scope.entitySearchResult.data_not_found.hasCertificates =false;
								data_parent =false;
								data_relative =false;
								familymember();
							}
							if(subfetcherData.fetcher === 'stocks.bloomberg.com') {
								$scope.entitySearchResult.data_not_found.role_ratio =false;
								$scope.entitySearchResult.data_not_found.stocks_prices =false;
							}
							if(subfetcherData.fetcher === 'profiles.littlesis.com') {
								$scope.entitySearchResult.data_not_found.holdings = false;
								$scope.entitySearchResult.data_not_found.donation_politician_data =false;
								$scope.entitySearchResult.data_not_found.orgdination_data =false;
								littlesis_family =false;
								litllesis_friends =false;
								familymember();
							}
							if(subfetcherData.fetcher === 'plus.google.com') {
								$scope.entitySearchResult.data_not_found.google_plus =false;
							}
							if(subfetcherData.fetcher === 'profiles.bloomberg.com') {
								$scope.entitySearchResult.data_not_found.competitor_compensation_data =false;
								$scope.entitySearchResult.data_not_found.earning_ratio = false;
								$scope.entitySearchResult.data_not_found.holding_ratio = false;
								$scope.entitySearchResult.data_not_found.dividents_data =false;
							}
							if(subfetcherData.fetcher === 'deepdotweb.com') {
								$scope.entitySearchResult.data_not_found.deepweb =false;
							}
							if(subfetcherData.fetcher === 'documents.google.com') {
								$scope.entitySearchResult.data_not_found.documents_data =false;
								$scope.entitySearchResult.data_not_found.asserts_ratio = false;
								$scope.entitySearchResult.data_not_found.popular_tags = false;
							}
							if(subfetcherData.fetcher === 'personal-profiles.bloomberg.com') {
								$scope.entitySearchResult.data_not_found.schooling =false;
								$scope.entitySearchResult.data_not_found.awards_data =false;
								$scope.entitySearchResult.data_not_found.honoray_data =false;
								$scope.entitySearchResult.data_not_found.Similarprofile =false;
							}
							if(subfetcherData.fetcher === 'supplierblacklist.com') {
								$scope.entitySearchResult.data_not_found.supplier_data =false;

							}
							if(subfetcherData.fetcher === 'channels.youtube.com'){
								$scope.entitySearchResult.data_not_found.video_data =false;
								$scope.entitySearchResult.data_not_found.asset_data =false;
							}
							if(subfetcherData.fetcher === 'spokeo.com'){
								$scope.entitySearchResult.data_not_found.coworker_data =false;
							}
			            }
					});
				 if(key === fetcherIds.length - 1){
					 deffered.resolve($scope.entitySearchResult);
				 }
			 }, function(error){
				//console.log(error);
				deffered.reject(error);
			 });
		});
		return deffered.promise;
	 } 
	 $scope.mainInfoTabType = 'Overview';
	 /*
	  * @purpose: onClickInfoTabs function
	  * @created: 20 oct 2017
	  * @params: type(string)
	  * @returns: no
	  * @author: sandeep
	  */
	 $scope.onClickInfoTabs = function(type){
		 $scope.mainInfoTabType = type;
		if(type == 'Overview'){
			if(vlaDataArr["networkGraphOverview"] &&  vlaDataArr["networkGraphOverview"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphOverview"],
					  "id":"networkGraphOverview"
				  });
			}
		}
		else if(type == 'Duediligence'){
			 $("#identificationInfo").mCustomScrollbar({
		     	axis : "y",
				theme : "minimal"
		     });
			 angular.element("#identificationInfo").css({
				'height': '250px',
				'overflow': 'hidden'
			 });
			 $("#accordion").mCustomScrollbar({
		     	axis : "y",
				theme : "minimal"
		     });
			 angular.element("#accordion").css({
				'height': '250px',
				'overflow': 'hidden'
			 });
			if(vlaDataArr["networkGraphCdd"] &&  vlaDataArr["networkGraphCdd"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphCdd"],
					  "id":"networkGraphCdd"
				  });
			}
		}else if(type == 'Career'){
			if(vlaDataArr["networkGraphCareers"] &&  vlaDataArr["networkGraphCareers"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphCareers"],
					  "id":"networkGraphCareers"
				  });
			}
			loadAllChartsForCareer();
		}else if(type == 'Academics'){
			if(vlaDataArr["networkGraphAcademics"] &&  vlaDataArr["networkGraphAcademics"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphAcademics"],
					  "id":"networkGraphAcademics"
				  });
			}
			loadAllChartsForAcademics();
		}else if(type == 'Finance'){
			if(vlaDataArr["networkGraphFinance"] &&  vlaDataArr["networkGraphFinance"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphFinance"],
					  "id":"networkGraphFinance"
				  });
			}
			loadAllChartsForFinace();
		}else if(type == 'Risks'){
			if(vlaDataArr["networkGraphRisks"] &&  vlaDataArr["networkGraphRisks"].vertices.length>0)	{
				  $scope.loadDataAndPlotGraphForEntity({
					  "data":vlaDataArr["networkGraphRisks"],
					  "id":"networkGraphRisks"
				  });
			}
			loadAllChartsForRisks();
		}else if(type == 'Social Media'){
			increment(newsSearchlength,"tab");
			loadAllChartsForSocialMedia();
		}else if(type == 'Transactions'){
			loadAllChartsForTransactions();
		} else if(type === 'Documents'){
			loadAllChartsForDocuments();
		}
	 }
	  $scope.dateobj ={};
	  $scope.transcationSearchon =false;
	  $scope.newdata =[];
	$scope.searchPersonTranx =function (dateobj) {
	        $scope.transcationSearchon =true;
		    $scope.newdata=[];
			var from  =moment(dateobj.searchFrom).format('YYYY-MM-DD');
			var after =moment(dateobj.searchTo).format('YYYY-MM-DD');
		   for (var i = 0; i < $scope.entitySearchResult.list['person_transaction_list'].length; i++) {
		   	var specificdate = $scope.entitySearchResult.list['person_transaction_list'][i].date;
		   		if( ( moment(specificdate).isAfter(from) ||(specificdate == from ) ) && ( moment(specificdate).isBefore(after) || (specificdate == after) ) ){
		   			$scope.newdata.push({
			 			date:$scope.entitySearchResult.list['person_transaction_list'][i].date,
			 			amount:$scope.entitySearchResult.list['person_transaction_list'][i].amount,
			 			recipcode:$scope.entitySearchResult.list['person_transaction_list'][i].recipcode,
			 			recipient_detail:$scope.entitySearchResult.list['person_transaction_list'][i].recipient_detail,
			 			recipient_name:$scope.entitySearchResult.list['person_transaction_list'][i].recipient_name,
			 			risk_score:$scope.entitySearchResult.list['person_transaction_list'][i].risk_score,
			 			source:$scope.entitySearchResult.list['person_transaction_list'][i].source,
			 			tranx_type:$scope.entitySearchResult.list['person_transaction_list'][i].tranx_type
			 		});

		   		}

			}
	}
	$scope.resetDate =function() {
		 $scope.newdata=[];
		$scope.dateobj ={};
		$scope.transcationSearchon =false;
		$scope.searchTranscation = '';
	}
	$scope.dateInitialize = function (argument) {
			var minDate = $( "#transSearchTo" ).datepicker( "option", "minDate" );
			$( "#transSearchTo" ).datepicker( "option", "minDate", new Date($scope.dateobj.searchFrom) );
	}
	 function financePersonModal(){
		 var financePersonModalInstance = $uibModal.open({
	            templateUrl: './modal/view/finance-person-modal.html',
	            controller: 'FinancePersonModalController',
	            windowClass: 'custom-modal update-entities-modal related-person-modal entity-overview-modal finance-modal',
	            resolve:{
	            	entitySearchResult:function(){
	            		return $scope.entitySearchResult;
	            	}
	            }
	        });

		 financePersonModalInstance.result.then(function (response) {
	        }, function (reject) {
	        });
	 }
	 /* read more Overview header*/
	 function overviewPersonModal(){
		 var overviewPersonModalInstance = $uibModal.open({
	            templateUrl: 'overview-person-modalId.html',
	            controller: 'OverviewPersonModalController',
	            windowClass: 'custom-modal update-entities-modal related-person-modal entity-overview-modal',
	            resolve:{
	            	entitySearchResult:function(){
	            		return $scope.entitySearchResult;
	            	}
	            }
	        });

		 overviewPersonModalInstance.result.then(function (response) {
	        }, function (reject) {
	        });
	 }
	 /*read more Risk Criminal Records*/

	  function riskOffenceModal(){
		 var riskOffenceModalInstance = $uibModal.open({
	            templateUrl: './modal/view/offence-records-person-modal.html',
	            controller: 'RiskOffenceModalController',
	            windowClass: 'custom-modal update-entities-modal related-person-modal entity-overview-modal criminal-records-modal',
	            resolve:{
	            	entitySearchResult:function(){
	            		return $scope.entitySearchResult;
	            	}
	            }
	        });

		 riskOffenceModalInstance.result.then(function (response) {
	        }, function (reject) {
	        });
	 }
	 function socialFollwersModal(){
	 		 var socialFollwersModalInstance = $uibModal.open({
	            templateUrl: './modal/view/social-media-follwers-modal.html',
	            controller: 'SocialFollwersModalController',
	            windowClass: 'custom-modal update-entities-modal related-person-modal entity-overview-modal socialfollowers-modal-controller',
	            resolve:{
	            	entitySearchResult:function(){
	            		return $scope.entitySearchResult;
	            	}
	            }
	        });

		 socialFollwersModalInstance.result.then(function (response) {
	        }, function (reject) {
	        });
	 }
	 /*readmore for whom am i*/
	 function whoamiModal(){
		 var whoamiModalInstance = $uibModal.open({
	            templateUrl: 'scripts/entity/modal/view/whoami-person-modal.html',
	            controller: 'WhoamiPersonModalController',
	            windowClass: 'custom-modal related-person-modal entity-visualiser-modal overview-modal-controller',
	            resolve:{
	            	entitySearchResult:function(){
	            		return $scope.entitySearchResult;
	            	}
	            }
	        });

		 whoamiModalInstance.result.then(function (response) {
	        }, function (reject) {
	        });
	 }
	/*-------------------------world location chart----------------------------------*/

	 function World(options) {
	     var worldmapHeader = options.header;
	     var worldmapId = options.container;
	     var worldmapuri1 = options.uri1;
	     var worldmapuri2 = options.uri2;
	     loadWorldChart(options);
/*	     $(window).on("resize", function () {
	         if ($(options.container).find("svg").length != 0) {
	             $(options.container).empty();
	             var exampleChart = new WorldChart(options);
	             EntityGraphService.plotWorldLocationChart(options);
	         }
	     }) */

	     /**
	      *Function to load data to plot WorldMap chart
	      */

	     function loadWorldChart(options) {
	         var WorldData = worldCountryDetailList;
	         var populationData = [];
	         var flag_for_req = 0;
			 options.data=[WorldData,populationData];
			 EntityGraphService.plotWorldLocationChart(options);

	        /*  d3.json(options.uri1, function (error, data) {

	             flag_for_req++;
				 WorldData = data;
	             options.data=[WorldData,populationData];
	             if (flag_for_req == 2) { */
/*	            	 var exampleChart = new WorldChart(options);*/
	            	/*  EntityGraphService.plotWorldLocationChart(options);
	             }
	         }); */
/* 	         d3.json(options.uri2, function (error, data) {
	             flag_for_req++;
	             populationData = handleMapData(data.locations);
	             options.data=[WorldData,populationData];
	             if (flag_for_req == 2) {
            	 var exampleChart = new WorldChart(options);
	            	 EntityGraphService.plotWorldLocationChart(options);
	             }
	         }); */
	         if(options.container === '#person-location-chart')
	        	 $scope.entitySearchResult.list['is_overview_location_chart'] = false;
	         if(options.container === '#documents-locations-chart')
	        	 $scope.entitySearchResult.list['is_document_location_chart'] = false;
	         if(options.container === '#transactions-locations-chart')
	        	 $scope.entitySearchResult.list['is_transaction_location_chart'] = false;
	         if(options.container === '#person-career-location-chart')
	        	 $scope.entitySearchResult.list['is_career_location_chart'] = false;
	         if(options.container === '#person-academic-location-chart')
	        	 $scope.entitySearchResult.list['is_person_academic_location_chart'] = false;
	         if(options.container === '#person-social-location-chart')
	        	 $scope.entitySearchResult.list['is_person_social_location_chart'] = false;

	     }

	     /**
	      *Function to handle data according to format of WorldMap Chart library
	      */
	     function handleMapData(data) {
	         var finalData = [];
	         $.each(data, function (i, d) {
	             finalData.push({
	                 name: i,
	                 population: d
	             });
	         });
	         return finalData;
	     }
	 }
	/*-------------------------world location chart END----------------------------------*/

	 /*
	  * @purpose: onClickSocialMediaTab function to change tab
	  * @created: 29 aug 2017
	  * @params: tabName(string)
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickSocialMediaTab = function(tabName){
		 $scope.socialMediaTab = tabName;
	 };

	 /*
	  * @purpose: onClickRelatedPersonTab function to change tab
	  * @created: 30 aug 2017
	  * @params: tabName(string)
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickRelatedPersonTab = function(tabName){
		 $scope.relatedPersonTab = tabName;
	 }

	 $scope.onChangeBloombergKey = function(bloombergDataKey){
		$scope.bloombergSelectedTab = bloombergDataKey;
		$('#bloomberg-line-chart').empty();
		angular.forEach(bloombergStockLineChartData[bloombergDataKey], function(chartData, chartKey){
			chartData.year = new Date(chartData.year);
		});
		if(bloombergDataKey === 'current day stock prices')
			bloombergChartOptions.dateFormat = 'HH:mm';
		else
			delete bloombergChartOptions.dateFormat;
   	 	bloombergChartOptions.data = bloombergStockLineChartData[bloombergDataKey];
   		loadlineData(bloombergChartOptions);
	 };

	 $scope.onChangeBloombergDetailsKey = function(bloombergDataKey){
			$scope.bloombergSelectedTab = bloombergDataKey;
			$('#bloomberg-line-chart-details').empty();
			angular.forEach(bloombergStockLineChartData[bloombergDataKey], function(chartData, chartKey){
				chartData.year = new Date(chartData.year);
			});
			if(bloombergDataKey === 'current day stock prices')
				bloombergChartDetailsOptions.dateFormat = 'HH:mm';
			else
				delete bloombergChartDetailsOptions.dateFormat;
	   	 	bloombergChartDetailsOptions.data = bloombergStockLineChartData[bloombergDataKey];
	   		loadlineData(bloombergChartDetailsOptions);
		 };
	 /*-------------------------bloomberg chart line----------------------------------*/

	 function getAdverseNewsByPerson(entityType, searchText){
		// EntityApiService.getNewsByEntityPerson(entityType, searchText).then(function(response){
			EntityApiService.getNewsByEntityTypeID(entityType, searchText).then(function(response){
				if (!_.isEmpty(response) &&(response.data)&& (response.data.articles.length>0) ) {
					angular.forEach(response.data.articles, function (value, key) {
						if(value.title && value.url)
						$scope.entitySearchResult.list['adverse_news'].push({
							title: value.title,
							text: value.text,
							url: value.url,
							published: new Date(value.published),
							source: value.source
						});
					});
				}
			}, function(error){
				
			});
			if($scope.entitySearchResult.list['adverse_news'].length === 0){
				$scope.entitySearchResult.data_not_found.data_adverse_news =false;
			}
	 }

	 function getAlexaDetailsByWebsite(websiteName){
		 var alexaFetcherIds = ['1022'];
		 var requestData = {
			fetchers: alexaFetcherIds,
			keyword: websiteName,
	        searchType: 'Person'
		 };
		 EntityApiService.getEntityDataByTextId(requestData).then(function(subResponse) {
			for(var i=0; i < subResponse.data.results.length; i++){
	            if (subResponse.data.results[i].status) {
	            	var subfetcherData = subResponse.data.results[0];
            		if(subfetcherData.entities.length ==0 || subfetcherData.entities[0].edges.length==0){
            			// show error messages that did not found
						// data
            			return;
            		}
            		var nested_data =d3.nest().key(function(d){return d.relationshipName}).entries(subfetcherData.entities[0].edges)
            		angular.forEach(nested_data,function(d,i){
            			var newData =[];
            			angular.forEach(d.values,function(innerVal,innerKey){
            				if(d.key == "traffic_by_countries" && innerVal.entity.properties.precent_of_visitors){
            					newData.push(innerVal);
            				}else if(d.key == "search_keywords" && innerVal.entity.properties.precentage_of_search_traffic){
            					newData.push(innerVal);
            				}else if(d.key == "local_ranking" ){
            					newData.push(innerVal);
            				}else if(d.key == "traffic_by_domain" && innerVal.entity.properties.precent_of_visitors){
            					newData.push(innerVal);
            				}
            			})
            			if(d.key == "traffic_by_countries"){
            				$scope.entitySearchResult.list['traffic_by_countries'] = newData;
            			}
            			if(d.key == "search_keywords"){
            				$scope.entitySearchResult.list['search_keywords'] = newData;
            			}
            			if(d.key == "local_ranking"){
            				$scope.entitySearchResult.list['local_ranking'] = newData;
            			}
            			if(d.key == "traffic_by_domain"){
            				$scope.entitySearchResult.list['traffic_by_domain'] = newData;
            			}
            		});
            		if($scope.mainInfoTabType == 'Social Media'){
            			loadAllChartsForSocialMedia();
    			    }
	            }
			}
		 });
	 }

	 function callSanction(keyword, entityType){
		 EntityApiService.getSanctions(keyword, entityType).then(function(response){
			 if(angular.isArray(response.data.hits) &&  response.data.hits.length > 0 &&  response.data.hits[0]['@identifier']){
				 var entityName= response.data.hits[0]['@identifier'];
     	    	 sanctionEntityLists(entityName, entityType);
			 }
		 },function(error){
			 
		 })
	 }

	 function sanctionEntityLists(entityName, entityListType){
		 EntityApiService.getEntityList(entityName, entityListType).then(function(response){
		 
			 var sanction = JsonParse(response.data.watchlist.sanctions);
				var pep  = JsonParse(response.data.watchlist.peps);
			 angular.forEach(sanction,function(sancList, key){
				 
					$scope.entitySearchResult.list['sanctionList'].push({
						'list_type' : sancList.type,
						'list_name' : sancList.name,
						'summary' : sancList.function,
						'link': sancList.source,
						'place':sancList.birth_places,
						'first_name': sancList.first_name,
						'last_name': sancList.last_name,
						'program':sancList.program,
						'type':sancList.type,
						'aliases':sancList.aliases,
						'birth_dates':sancList.birth_dates,
						'source':sancList.source,
						'updated_at':sancList.updated_at
					})
				});
				console.log('$scope.entitySearchResu)',	$scope.entitySearchResult.list['sanctionList']);
			angular.forEach(pep,function(pep, key){
				$scope.entitySearchResult.list['pep'].push({
					'list_type' : pep.type,
					'list_name' : pep.name,
					'summary' : pep.function,
					'link': pep.url,
				})
			});
			var pepParsedData = response.data.watchlist ? (response.data.watchlist.peps.length > 0 ? JSON.parse(response.data.watchlist.peps[0].raw) : '') : '';
			var ContryRisk = '';
			if (!_.isEmpty(response.data.basic) && !_.isEmpty(response.data.basic["vcard:hasAddress"]) && response.data.basic["vcard:hasAddress"].country) {
				ContryRisk = calculateCountryRisk(response.data.basic["vcard:hasAddress"].country);
			}
			var financeCrimeData = financeCrimeNews(response.data);
			$scope.screeningData.push({
				"mdaas:RegisteredAddress": response.data.basic ? (response.data.basic["vcard:hasAddress"] ? response.data.basic["vcard:hasAddress"] : '') : '',
				"name": response.data.basic["vcard:hasName"] ? response.data.basic["vcard:hasName"] : '',
				"sanction_bst:description": response.data.watchlist.sanctions.length > 0 ? (response.data.watchlist.sanctions[0]['bst:description'] ? response.data.watchlist.sanctions[0]['bst:description'] : '') : '',
				"listId": response.data.watchlist.sanctions.length > 0 ? (response.data.watchlist.sanctions[0]['listId'] ? response.data.watchlist.sanctions[0]['listId'] : '') : '',
				"@source-id": response.data.basic ? (response.data.basic["@source-id"] ? response.data.basic["@source-id"] : '') : '',
				"high_risk_jurisdiction": ContryRisk ? ContryRisk : '',
				"pep_url": pepParsedData.url ? pepParsedData.url : '',
				'sanction_url': response.data.watchlist.sanctions.length > 0 ? (response.data.watchlist.sanctions[0].url ? response.data.watchlist.sanctions[0].url : '') : '',
				'sanction_available' :response.data.watchlist.sanctions.length > 0 ? (response.data.watchlist.sanctions.length > 0 ? true :false) : false,
				'pep_available' :pepParsedData ?( pepParsedData ? true : false) :false,
				"finance_Crime_url" : financeCrimeData.url ?  financeCrimeData.url  :'',
				"finance_availble":	financeCrimeData ?( financeCrimeData ? true : false) :false,			
			});
			if (response.data.watchlist.sanctions.length > 0) {
				$scope.identifiedRisk = $scope.identifiedRisk + 1;
			}
			if(pepParsedData){
				$scope.identifiedRisk = $scope.identifiedRisk + 1;
			}
			if(financeCrimeData){
				$scope.identifiedRisk = $scope.identifiedRisk + 1;
			}
			$scope.entitySearchResult.data_not_found.blacklist = $scope.entitySearchResult.list['sanctionList'].length===0 ? false: true;
			/*  if(response){
				 if(angular.isArray(response.data.lists)){
					if(response.data.lists.length > 0){
						angular.forEach(response.data.lists,function(sancList, key){
							if(sancList.list_type == 'sanctions'){
								$scope.entitySearchResult.list['sanctionList'].push({
									'list_type' : sancList.list_type,
									'list_name' : sancList.list_name,
									'summary' : sancList.summary,
									'link': sancList.url
								})
							}
							if(sancList.list_type == 'pep'){
								$scope.entitySearchResult.list['pep'].push({
									'list_type' : sancList.list_type,
									'list_name' : sancList.list_name,
									'summary' : sancList.summary,
									'link': sancList.url
								})
							}
						});
					}
				 }
				 if($scope.mainInfoTabType == 'Risk Alerts'){
	   		    	PlotriskAlertCharts();
	   		     }
			 } */
		 });
	 }
	 function JsonParse(data){
		 var parsed =[];
		for (var index = 0; index < data.length; index++) {		
			parsed[index] =JSON.parse(data[index].raw);
		}
		return parsed;
	 }
	 function financeCrimeNews(financeCrimeData) {
		var empty ='';
		if (financeCrimeData.news.length > 0) {
			for (var i = 0; i < financeCrimeData.news.length; i++) {
				if (financeCrimeData.news[i].class === 'Financial Crime') {
					return financeCrimeData.news[i];
				} else {
					return empty;
				}
			}
		} else {
			return empty;
		}

	}
	 function loadlineData(data) {
	    var current_options = data;
   	 	EntityGraphService.bloombergLineChart(current_options);
	 }
	 function familymember(){
		if(!data_parent && !littlesis_family && !data_relative && !litllesis_friends){
			$scope.entitySearchResult.data_not_found.familydata =false;
		}
	 }
	 function calculateCountryRisk(country) {
		var captialCaseCountry = toTitleCase(country);
		var risk;
		if (captialCaseCountry) {
			//	d3.json("./constants/countryrisk.json", function (error, root) {
			risk = _.find(chartsConst.countryRisk, ['COUNTRY', captialCaseCountry]);
			if (risk && risk["FATF Risk"] < 30) {
				risk["FATF Risk"] = 'Low';
			} else if (risk &&( risk["FATF Risk"] > 31 && risk["FATF Risk"] < 60)) {
				risk["FATF Risk"] = 'Medium';
			} else if ( risk && (risk["FATF Risk"] > 61 && risk["FATF Risk"] < 100)) {
				risk["FATF Risk"] = 'High';
			}
			return (risk ? risk["FATF Risk"] : '');
			//});
		}
	}
	function toTitleCase(str) {
		return str.replace(/\w\S*/g, function (txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}
	 /*-------------------------------------------------------------------------------*/

	 /*
	  * @purpose: onViewClick open modal for specific category
	  * @created: 19 aug 2017
	  * @params: categoryName(string)
	  * @returns: no
	  * @author: sandeep
	  */
	 $scope.onViewClick = function(categoryName){
		if(categoryName === 'general'){
			var generalModalInstance = $uibModal.open({
				templateUrl: './modal/view/general-entity-modal.html',
				controller: 'GeneralEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					generalData: function(){
						return $scope.entitySearchResult;
					}
				}
			});

			generalModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'social-media-person'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/social-media-person-modal.html',
				controller: 'SocialMediaPersonController',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					searchText: function(){
						return $scope.entitySearchResult.name;
					},
					searchType: function(){
						return 'Person';
					}
				}
			});
			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'general-info'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/general-info-entity-modal.html',
				controller: 'GeneralInfoModalConteoller',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					generalDetailList: function(){
						return $scope.personDetailList;
					}
				}
			});

			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'person-interests'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/person-interests-modal.html',
				controller: 'PersonInterestsModalController',
				windowClass: 'update-entities-modal related-person-modal person-interests-wrapper',
				resolve: {
					personInterestsData : function(){
						return $scope.entitySearchResult.list['interests'];
					}
				}
			});

			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'person-education'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/person-education-modal.html',
				controller: 'PersonEducationModalController',
				windowClass: 'update-entities-modal related-person-modal person-education-wrapper',
				resolve: {
					personEducationData : function(){
						return $scope.entitySearchResult.list['has_education'];
					}
				}
			});

			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'previous-location'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/previous-location-modal.html',
				controller: 'PreviousLocationModalController',
				windowClass: 'update-entities-modal related-person-modal previous-location-wrapper',
				resolve: {
					previousLocationData : function(){
						return $scope.entitySearchResult.list['previous_work_places'];
					}
				}
			});

			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'employment-history'){
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/employment-history-modal.html',
				controller: 'EmploymentHistoryModalController',
				windowClass: 'update-entities-modal related-person-modal employment-history-wrapper',
				resolve: {
					employmentHistoryData : function(){
						return $scope.entitySearchResult.list['employment_history'];
					}
				}
			});

			newsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'followers'){
			var followersModalInstance = $uibModal.open({
				templateUrl: './modal/view/followers-entity-modal.html',
				controller: 'FollowersEntityModalController',
				windowClass: 'update-entities-modal related-person-modal followers-entity-modal',
				backdrop: 'static',
				resolve: {
					followersModalData: function(){
						return $scope.entitySearchResult.list.followers;
					}
				}
			});
			followersModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'following'){
			var followingModalInstance = $uibModal.open({
				templateUrl: './modal/view/following-entity-modal.html',
				controller: 'FollowingEntityModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					followingModalData: function(){
						return $scope.entitySearchResult.list.following;
					}
				}
			});
			followingModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'blacklist'){
			var latestReleaseListModalInstance = $uibModal.open({
				templateUrl: './modal/view/supplier-black-list-entity-modal.html',
				controller: 'SupplierBlackListModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					supplierBlackData: function(){
						return $scope.entitySearchResult.list['supplierblacklist'];
					}
				}
			});
			latestReleaseListModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'organizations'){
			var organizationsListModalInstance = $uibModal.open({
				templateUrl: './modal/view/organizations-list-entity-modal.html',
				controller: 'OrganizationsListModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					organizationsData: function(){
						return $scope.entitySearchResult.list['organizations'];
					}
				}
			});
			organizationsListModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'thumbnails'){
			var thumbnailsModalInstance = $uibModal.open({
				templateUrl: './modal/view/thumbnails-entity-modal.html',
				controller: 'ThumbnailsModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					thumbnailsData: function(){
						return $scope.entitySearchResult.list['thumbnails'];
					}
				}
			});
			thumbnailsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'realted-person'){
			var realtedPersonModalInstance = $uibModal.open({
				templateUrl: './modal/view/related-person-people-modal.html',
				controller: 'RealtedPersonPeopleModalConteoller',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					personsData: function(){
						return {
							entityKeyPersonList : $scope.entitySearchResult.list['people_details'],
							entityKeyExecutiveList : $scope.entitySearchResult.list['keyExecutiveList'],
							entityBoardmemberList : $scope.entitySearchResult.list['boardmemberList'],
							entityCoworkersList : $scope.entitySearchResult.list['coworkers']
						};
					}
				}
			});

			realtedPersonModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'open-payments'){
			var openPaymentsModalInstance = $uibModal.open({
				templateUrl: './modal/view/open-payments-modal.html',
				controller: 'OpenPaymentsModalConteoller',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					openPaymentsData: function(){
						return $scope.entitySearchResult.list['open-payments'];
					}
				}
			});

			openPaymentsModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'scam-report'){
			var scamReportModalInstance = $uibModal.open({
				templateUrl: './modal/view/scam-report-modal.html',
				controller: 'ScamReportModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					scamReportData: function(){
						return $scope.entitySearchResult.list.scam;
					}
				}
			});
			scamReportModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'unlimitedcriminalchecks'){
			var unlimitedcriminalchecksModalInstance = $uibModal.open({
				templateUrl: './modal/view/unlimitedcriminalchecks-modal.html',
				controller: 'UnlimitedcriminalchecksModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					unlimitedcriminalchecksData: function(){
						return $scope.entitySearchResult.list['unlimitedcriminalchecks'];
					}
				}
			});
			unlimitedcriminalchecksModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'crimestoppers-uk'){
			var crimestoppersukModalInstance = $uibModal.open({
				templateUrl: './modal/view/crimestoppers-uk-modal.html',
				controller: 'CrimestoppersukModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					crimestoppersukData: function(){
						return $scope.entitySearchResult.list['crimestoppers-uk'];
					}
				}
			});
			crimestoppersukModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'badbuyerslist'){
			var badbuyerslistModalInstance = $uibModal.open({
				templateUrl: './modal/view/bad-buyers-person-list-modal.html',
				controller: 'BadbuyersPersonlistModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					badbuyerslistData: function(){
						return $scope.entitySearchResult.list['badbuyerslist'];
					}
				}
			});
			badbuyerslistModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'imsasllc'){
			var imsasllclistModalInstance = $uibModal.open({
				templateUrl: './modal/view/imsasllc-list-modal.html',
				controller: 'ImsasllclistModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					imsasllclistData: function(){
						return $scope.entitySearchResult.list['imsasllc'];
					}
				}
			});
			imsasllclistModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'fbi_data'){
			var fbilistModalInstance = $uibModal.open({
				templateUrl: './modal/view/fbi-list-modal.html',
				controller: 'FBIlistModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					fbiData: function(){
						return $scope.entitySearchResult.list['fbi_data'];
					}
				}
			});
			fbilistModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'sanctionssearch'){
			var sanctionssearchlistModalInstance = $uibModal.open({
				templateUrl: './modal/view/sanctionssearch-list-modal.html',
				controller: 'SanctionssearchlistModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					sanctionssearchData: function(){
						return $scope.entitySearchResult.list['sanctionssearch'];
					}
				}
			});
			sanctionssearchlistModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'news'){
			var newslistModalInstance = $uibModal.open({
				templateUrl: './modal/view/news-list-modal.html',
				controller: 'NewslistModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					newsDeepWebData: function(){
						return $scope.entitySearchResult.list['news_deep_web'];
					}
				}
			});
			newslistModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'related-organization'){
			var relatedOrdPersonModalInstance = $uibModal.open({
				templateUrl: './modal/view/related-organization-person-modal.html',
				controller: 'RelatedOrganizationPersonModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					relatedOrgPersonData: function(){
						return {
							memberships: $scope.entitySearchResult.list['memberships'],
							board_memberships: $scope.entitySearchResult.list['board_memberships']
						}
					}
				}
			});
			relatedOrdPersonModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
		if(categoryName === 'sex-offenders'){
			var sexOffendersPersonModalInstance = $uibModal.open({
				templateUrl: './modal/view/sex-offenders-person-modal.html',
				controller: 'SexOffendersPersonModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					sexOffendersPersonData: function(){
						return $scope.entitySearchResult.list['sex-offenders'];
					}
				}
			});
			sexOffendersPersonModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
	 };
	 /*
	  * @purpose: loadRelatedPersonData for tag cloud
	  * @created: 24 aug 2017
	  * @params: no
	  * @return: no
	  * @auhtor: sandeep
	  */
	 function loadRelatedPersonData(){
		
		if(tagCloudNameList.length > 0) {
			if(tagCloudNameList.length < 30)
				tagCloudPersonOptions.data = tagCloudNameList;
			else
				tagCloudPersonOptions.data = tagCloudNameList.splice(0,30);

			tagCloudPersonOptions.domain.y = maxPersonDomainRangeValue;
		    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudPersonOptions);
		}
	 };

	 /*
	  * @purpose: onViewClickReplies open modal for scam replies
	  * @created: 20 sep 2017
	  * @params: index(number)
	  * @returns: no
	  * @author: sandeep
	  */
	 $scope.onViewClickReplies = function(index){
		if($scope.entitySearchResult.list['scam']['replies'].length > 0) {
			 var repliesModalInstance = $uibModal.open({
				templateUrl: './modal/view/replies-entity-modal.html',
				controller: 'RepliesEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal',
				resolve: {
					repliesData: function(){
						return $scope.entitySearchResult.list['scam'][index];
					}
				}
			});

			 repliesModalInstance.result.then(function(response){
				
			}, function(error){
				
			});
		}
	 };

	 /* @purpose: onClickLoadLiveFeed nav to social-media-content page
	  * @created: 20 sep 2017
	  * @params: no
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickLoadLiveFeed = function(){
		/* var url = $state.href('social-live-feed', {searchText: $scope.entitySearchResult.name});
		window.open(url, '_blank'); */
		var indexOfbackSlash = window.location.href.split("/#!/")[0].lastIndexOf('/');
		 var url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/live-feed/"+$scope.entitySearchResult.name;
		 window.open(url, '_blank');
	 };

	 /*
	  * @purpose: loadRelatedOrganizationData function
	  * @created: 22 sep 2017
	  * @params: no
	  * @return: no
	  * @auhtor: sandeep
	  */
	 function loadRelatedOrganizationData(){
		
		  var newarray =[];
		for(var i=0;i<tagCloudOrganizationNameList.length;i++){
			if(tagCloudOrganizationNameList[i].text!==undefined)
			newarray.push(tagCloudOrganizationNameList[i]);
		}
		
		if(newarray.length > 0) {
			tagCloudOrganizationOptions.data = newarray.splice(0, 29);
			tagCloudOrganizationOptions.domain.y = maxOrganizationDomainRangeValue;
			EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOrganizationOptions);
			$scope.entitySearchResult.list['is_overview_entities_tag_chart'] = false;
		}else{
			$scope.entitySearchResult.data_not_found.relatedentity =false;
		}
	 }

	 /*
	  * @purpose: loadRelatedSkillsData for tag cloud
	  * @created: 07 nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: sandeep
	  */
	 function loadRelatedSkillsData(){
		 
		if( $('#skills-tag-cloud-chart').find("svg").length === 0){
			if(tagCloudSkillsNameList.length > 0) {
				if(tagCloudSkillsNameList.length < 30)
					tagCloudSkillsOptions.data = tagCloudSkillsNameList;
				else
					tagCloudSkillsOptions.data = tagCloudSkillsNameList.splice(0,30);

				tagCloudSkillsOptions.domain.y = maxSkillsDomainRangeValue;
				$scope.entitySearchResult.list['is_skills_tag_cloud_chart'] = false;
			    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudSkillsOptions);
			} else{
				$scope.entitySearchResult.data_not_found.skill_data = false;
			}
		}
	 };
	 var overallsentiments = [], type = ''
	 var countNews=0;
	 var newsSearchlength;
	 $scope.sentimentsLoader = false;
	 function loadNewsSearch(searchText){
		 EntityApiService.newsSearchDetail(searchText).then(function(response){
				 if(angular.isArray(response.data.value)){
					  newsSearchlength = response.data.value.length;
						 angular.forEach(response.data.value,function (newsVal, key){
							  var data = {
								    	"id": newsVal.name,
								    	"language": "en",
								    	"text": newsVal.description + " " + newsVal.name
							    }
								 setTimeout(function() {
									  textAnalysticsDetail(data,newsVal, newsSearchlength);
								  }, key * 1000);
						 });

			 }
		 },function(err){
			 $("#pieChartGeneralSentiment").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Something Went Wrong Try Again Later</div></div>")
				$('.overallSentimentChart-wrapper').append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Something Went Wrong Try Again Later</div></div>")
		 });
	 }

	 function textAnalysticsDetail(data,newsVal, newsSearchlength){
		 EntityApiService.textAnalystics(data).then(function(subresponse){
					 if(subresponse){
						 $scope.sentimentsLoader = true;
						    if(subresponse.data.score < 0.35)
								type = 'negative';
							else if(subresponse.data.score >= 0.35 && subresponse.data.score < 0.70)
								type = 'neutral';
							else if(subresponse.data.score >= 0.70)
								type = 'positive';
						// overallsentiments = []
							overallsentiments.push({
								name: subresponse.data.id,
								time: newsVal.datePublished,
								amount: subresponse.data.score,
								type: type,
								'txt':'<table class="table data-custom-table"><tr><th>Name</td><td>'+newsVal.name+'</td></tr><tr><th>Date Published</td><td>'+newsVal.datePublished+'</td></tr><tr><th>Score</td><td>'+subresponse.data.score+'</td></tr><tr><th>Category</td><td>'+ (newsVal.category ? newsVal.category : 'N/A') + '</td></tr><tr><th>Url</td><td>'+newsVal.url+'</td></tr><tr><th>Description</td><td>'+newsVal.description+'</td></tr></table>'
							});
							increment(newsSearchlength);
					 }
		 },function(err){
			$("#pieChartGeneralSentiment").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Something Went Wrong Try Again Later</div></div>")
			$('.overallSentimentChart-wrapper').append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Something Went Wrong Try Again Later</div></div>")
			// 
		 });
	 }

	 function increment(newsSearchlength,tab){
		 var colorsObj ={
	   				"negative":"#b753cd",
	   				"positive":"#69ca6b",
	   				"neutral":"#5d96c8"

	   		}
		 if(!tab)
		 countNews += 1;
		 $timeout(function(){
			  var timelineOptions = {
				        container: ".overallSentimentChart-wrapper",
				        height: 50,
				        colors:["#a75dc2","#5D97C9","#399034"],
				        data:overallsentiments,
				        colorsObj:colorsObj
					};

			       	var setimentsPieData = d3.nest()
			       	  .key(function(d) { return d.type; })
			       	  .rollup(function(sentiments) { return sentiments.length; })
			       	  .entries(overallsentiments);

			       	setimentsPieData.map(function(d){
			       		return d['doc_count'] = d.value;
			       	})
					   $timeout(function(){
// if($scope.mainInfoTabType == 'Latest News'){
							   loadtimeLineColumnChart(timelineOptions);
							 // calling General Sentiment pie chart

							   $scope.InitializeandPlotPie(setimentsPieData, 'pieChartGeneralSentiment',colorsObj);
// }
					   },100)
		  },1000);

		  if(countNews==newsSearchlength){
			  $scope.sentimentsLoader = false;
			  if(overallsentiments.length === 0){
				  $timeout(function(){
					  $("#pieChartGeneralSentiment").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
					  $('.overallSentimentChart-wrapper').append("<div class=\"alert-message-wrapper\"><div class=\"alert-message \">Data Not Found</div></div>");
				  },2000)

			  }
		  }

		}
	 /*
	  * @purpose: load overview VLA
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function onpageLoadCharts(){
		$timeout(function(){
			
			var data = {
	    		 "fetchers": ['3','8','1008'],
	    		 "keyword": searchText,
	    		 "searchType": "Person",
	    		 "lightWeight": true,
	    		 "limit": 2,
	    		"iterations": 1,
	    		"alias": "Full",
	    		"create_new_graph": true,
	    		"requires_expansion": false,
	    		"entity_resolution": false
    		};
			loadoverviewVLA("networkGraphOverview", data);

			var data = {
		    		 "fetchers": ["3","1013","1"],
		    		 "keyword": searchText,
		    		 "searchType": "Person",
		    		 "lightWeight": true,
		    		 "limit": 2,
		    		"iterations": 1,
		    		"alias": "Full",
		    		"create_new_graph": true,
		    		"requires_expansion": false,
		    		"entity_resolution": false
	    		};
				loadoverviewVLA("networkGraphCareers", data);

				var data = {
			    		 "fetchers": ["25","3"],
			    		 "keyword": searchText,
			    		 "searchType": "Person",
			    		 "lightWeight": true,
			    		 "limit": 2,
			    		"iterations": 1,
			    		"alias": "Full",
			    		"create_new_graph": true,
			    		"requires_expansion": false,
			    		"entity_resolution": false
		    		};
				loadoverviewVLA("networkGraphAcademics", data);

				var data = {
			    		 "fetchers": ["8","1009", "26"],
			    		 "keyword": searchText,
			    		 "searchType": "Person",
			    		 "lightWeight": true,
			    		 "limit": 2,
			    		"iterations": 1,
			    		"alias": "Full",
			    		"create_new_graph": true,
			    		"requires_expansion": false,
			    		"entity_resolution": false
		    		};
					loadoverviewVLA("networkGraphFinance", data);

					var data = {
				    		 "fetchers": ["15", "9", "14", "16", "17", "11", "8001", "13", "12", "6", "10"],
				    		 "keyword": searchText,
				    		 "searchType": "Person",
				    		 "lightWeight": true,
				    		 "limit": 2,
				    		"iterations": 1,
				    		"alias": "Full",
				    		"create_new_graph": true,
				    		"requires_expansion": false,
				    		"entity_resolution": false
			    		};
						loadoverviewVLA("networkGraphRisks", data);
						
						loadNewsSearch(searchText);

		},3000);
	 };

	 /*
	  * @purpose: load career  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function loadAllChartsForCareer(){
	 //load vla for careers
		$timeout(function(){

			/*calling to plot role pie chart*/
	        if (roleDataForPieChart.length > 0) {
	        	$scope.InitializeandPlotPie(roleDataForPieChart, 'role-based-pie-chart');
	        } else {
	        	$scope.entitySearchResult.data_not_found.role_ratio = false;
	        }
	        if (professionalConnectionForPieChart.length > 0) {
	        	$scope.InitializeandPlotPie(professionalConnectionForPieChart, 'connection-role-based-pie-chart');
	        } else {
	        	$scope.entitySearchResult.data_not_found.professional_connection = false;
	        }

			/*Carreer timeline chart starts*/
        	if($("#Career-timeline").find("svg").length == 0){
        		angular.forEach($scope.entitySearchResult.list['previous_work_places'],function (edu,key) {
				if(edu.employed_dates !='' && edu.employed_dates != undefined &&  edu.employed_dates != null){
					var numb = edu.employed_dates.match(/\d/g).join("").slice(0,4);
        					activityCareerData.push({
        					"time": numb,
				    		"amount": Math.floor(Math.random() * 20),
				    		"type": "role",
				     		'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Company:</b> '+edu.company+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Role:</b> '+ edu.title +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px"> Date:</b>  '+ edu.employed_dates +'</span></br>'
				     	});
        		}
        		});
        	}
        	
        	if(activityCareerData.length > 0){
				 $scope.entitySearchResult.list['is_career_timelinechart'] =false;
				timelineCareerRole.data = activityCareerData;
				$timeout(function(){
					loadtimeLineColumnChart(timelineCareerRole);
				},10);
			}else{
				$scope.entitySearchResult.data_not_found.carrerProgression = false;
			}
			/*Carreer timeline chart Ends*/
			if(careerLocationList.length > 0){
		        worldCareerChartOptions.markers = careerLocationList;
	        	
		   	 	/*calling world map chart for academics function*/
		        World(worldCareerChartOptions);
			}
		},10);
	 };
	 /*
	  * @purpose: load academics  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	function loadAllChartsForAcademics(){
		 //load vla for careers
		$timeout(function(){
			if(academicLocationList.length > 0){
		        worldAcademicChartOptions.markers = academicLocationList;
		        
		   	 	/*calling world map chart for academics function*/
		        World(worldAcademicChartOptions);
			}

	        /*getting tag cloud chart for skills*/
	        loadRelatedSkillsData();
	        /*load chart for academicbackground and academic header degreecount*/
	      	var	degreeType = d3.nest().key(function(d){ return d.type; })
  				.entries($scope.entitySearchResult.list['has_education']);
			if($("#academic-based-pie-chart").find("svg").length == 0){
		    	angular.forEach(degreeType, function ( edu,i) {
		        	if(edu.key!='' || undefined){
		        		$scope.entitySearchResult.list['has_degree'].push({
		        			degree:edu.key,
		        			degreecount:edu.values.length
		        		});
		        		acdemicDataForPieChart.push({
	        				key: edu.key,
							doc_count: edu.values.length
						});
		        	}
		      	});
	    	}
	    	if(acdemicDataForPieChart.length>0){
	        		$scope.entitySearchResult.list['academic-based-pie-chart'] =false;
	        		$scope.InitializeandPlotPie(acdemicDataForPieChart, 'academic-based-pie-chart');
	        	} else{
					$scope.entitySearchResult.data_not_found.academic_background=false;
				}

	        	/*Academic timeline chart starts*/
	        	if($("#academic-timeline").find("svg").length == 0){
	        		angular.forEach($scope.entitySearchResult.list['has_education'],function (edu,key) {
	        			if(edu.start_time !=''){
	        					activityAcademicData.push({
	        					"time": edu.start_time,
					    		"amount": Math.floor(Math.random() * 20),
					    		"type": "degree",
					     		'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b> '+edu.school_name+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Start Date:</b> '+ new Date(edu.start_time) +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">End Date:</b>  '+ new Date(edu.end_time) +'</span></br>'
					     	});
	        			}
	        		});
	        	}
	        	if(activityAcademicData.length > 0){
	        		$scope.entitySearchResult.list['is_academic_timelinechart'] =false;
					timelineAcademics.data = activityAcademicData;
					$timeout(function(){
						loadtimeLineColumnChart(timelineAcademics);
					},10);
				} else{
					$scope.entitySearchResult.data_not_found.academic_timeline =false;
				}
				/*Academic timeline chart Ends*/

		},10);
	};
	 /*
	  * @purpose: load finance  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function loadAllChartsForFinace(){
	 //load vla for careers
		$timeout(function(){
		   var holdingData = d3.nest().key(function(d){return d.relationship}).entries($scope.entitySearchResult.list['associated_companies']['holdings']);
		   var holdingPieChartData = [];
		   /*plotting transaction type pie chart*/
		   for(var h0 = 0; h0 < holdingData.length; h0++){
			   holdingPieChartData.push({
				"key": holdingData[h0].key,
				"doc_count": holdingData[h0].values.length
			   });
		   }
	   	   
	   	   if(holdingPieChartData.length > 0){
	   		   $scope.InitializeandPlotPie(holdingPieChartData, 'holding-pie-chart');
	   	   }else{
	   	   	$scope.entitySearchResult.data_not_found.holding_ratio = false;
	   	   }
	   	   /* 
		   if(bubbleCompittitorsFinanceOptions.data.children.length > 0){
		       socialBubbleChart(bubbleCompittitorsFinanceOptions);
			} */
			
			if (earningRatioPieChart.length > 0) {
				$scope.InitializeandPlotPie(earningRatioPieChart, 'earning-ratio-pie-chart');
			} else {
				$scope.entitySearchResult.data_not_found.earning_ratio = false;
			}
		},10);
	 };
	 /*
	  * @purpose: load risk  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function loadAllChartsForRisks(){
	 //load vla for careers
		$timeout(function(){
		  PlotriskAlertCharts();
		  PlotPoliticalChart();
		  /*Donation to ploitician  PiechartRatio starts*/
		  if($scope.entitySearchResult.list['associated_companies']['politicians'].length > 0){
    			var DonationRatio=[];
    				for(var i = 0; i < $scope.entitySearchResult.list['associated_companies']['politicians'].length; i++) {
    					if($scope.entitySearchResult.list['associated_companies']['politicians'][i].recipient_name !='' &&  $scope.entitySearchResult.list['associated_companies']['politicians'][i].recipient_name){
		    				DonationRatio.push({
				   	   			"key":$scope.entitySearchResult.list['associated_companies']['politicians'][i].recipient_name,
				   	   			"doc_count":parseFloat($scope.entitySearchResult.list['associated_companies']['politicians'][i].amount)
				   	   		});
    					}
    				}
    				$scope.InitializeandPlotPie(DonationRatio,"pieDonationRatio");
    		}else{
    			$scope.entitySearchResult.data_not_found.donation_politician_data =false;
    		}
    		/*Donation to ploitician  Ratio ends*/
    		 /*Donation to Organisation  PiechartRatio starts*/
		  if($scope.entitySearchResult.list['associated_companies']['political_organizations'].length > 0){
    			var OrgaDonationRatio=[];
    				for(var i = 0; i < $scope.entitySearchResult.list['associated_companies']['political_organizations'].length; i++) {
    					if($scope.entitySearchResult.list['associated_companies']['political_organizations'][i].org_name !='' && $scope.entitySearchResult.list['associated_companies']['political_organizations'][i].org_name){
		    				OrgaDonationRatio.push({
				   	   			"key":$scope.entitySearchResult.list['associated_companies']['political_organizations'][i].org_name,
				   	   			"doc_count":parseFloat($scope.entitySearchResult.list['associated_companies']['political_organizations'][i].amount)
				   	   		});
    					}
    				}
    				$scope.InitializeandPlotPie(OrgaDonationRatio,"organDonationRatio");
    		}else{
				$scope.entitySearchResult.data_not_found.orgdination_data =false;
			}
    		/*Donation to organisation  Ratio ends*/

		},10);
	 };
	 /*
	  * @purpose: load social media  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function loadAllChartsForSocialMedia(){
		 //load vla for careers
		 //load chart for twitter interaction ratio
		$timeout(function(){
			if ($scope.entitySearchResult.list['follower_following_count'].length > 0 && twitter_followercount > 0) {
				var instanceData = [{
						"key": "FOLLOWING",
						"doc_count": $scope.entitySearchResult.list['follower_following_count'][0].twitter_following_number
					},
					{
						"key": "FOLLOWER",
						"doc_count": twitter_followercount
					}
				];
				$scope.InitializeandPlotPie(instanceData, "interaction-ratio");
			}
			else{
				$scope.entitySearchResult.data_not_found.twitter_inter_ratio = false;
			}
			
			// time line starts here for activity feed
    		if(activitySocialData.length > 0){
  		   	  	timelineSocialOptions.data = activitySocialData;
     			loadtimeLineColumnChart(timelineSocialOptions);
    		}
    		//start fetching twitter activities from socket io
    		loadtwitterchart();

    		//profile.lnkedin Interest Ratio Starts
    		if($scope.entitySearchResult.list["interests"].length > 0){
    			var InterestRatio=[];
    				for(var i = 0; i < $scope.entitySearchResult.list["interests"].length; i++) {
    					if($scope.entitySearchResult.list["interests"][i].entity_name !='' &&  $scope.entitySearchResult.list["interests"][i].entity_name){
		    				InterestRatio.push({
				   	   			"key":$scope.entitySearchResult.list["interests"][i].entity_name,
				   	   			"doc_count":parseFloat($scope.entitySearchResult.list["interests"][i].followers_count.split(" ")[0].replace(/,/g, ""))
				   	   		});
    					}
    				}
    				$scope.InitializeandPlotPie(InterestRatio,"interest_ratio");
    		} else {
				$scope.entitySearchResult.data_not_found.interest_data =false;
			}
    		//profile.lnkedin Interest Ratio ends
			 //load data for trend followers
			if ($scope.entitySearchResult.list['followers']) {
			    if ($scope.entitySearchResult.list['followers'].length > 0) {
			        var trendLineData = [];
			        var date = '';
			        var year = [];
			        var obj = {};
			        angular.forEach($scope.entitySearchResult.list['followers'], function(d, i){
			            date = $filter('date')(d.createdOn, 'yyyy');
			            year.push(date);
			        });
			        for (var i = 0, j = year.length; i < j; i++) {
			            obj[year[i]] = (obj[year[i]] || 0) + 1;
			        }
			        Object.getOwnPropertyNames(obj).forEach(
			            function(val, idx, array) {
			                trendLineData.push({
			                    date: val,
			                    sum: obj[val]
						})
					});
					/* calling Overall Trend line chart */
					$scope.entitySearchResult.list['is_twitter_follower_trends'] = false;
					$timeout(function(){
						plotLineChart(trendLineData, "#lineChartFollowersTrend", $("#lineChartFollowersTrend").width(), '200');
					},10);
				}else{
					$scope.entitySearchResult.data_not_found.recent_follower =false;
					$scope.entitySearchResult.data_not_found.twitter_fol_trend =false;
				}
			    // trend line ends
			}

        	// traffic by countries pie starts here
		   	 if($scope.entitySearchResult.list['traffic_by_countries'] && 	$scope.entitySearchResult.list['traffic_by_countries'].length>0){
			   	var traffic_by_countriesData=[];
	   	   		angular.forEach($scope.entitySearchResult.list['traffic_by_countries'],function(d,i){
		   	   		traffic_by_countriesData.push({
		   	   			"key":d.entity.properties.country,
		   	   			"doc_count":parseFloat(d.entity.properties.precent_of_visitors.split("%")[0]).toFixed(2)
		   	   		})
	   	   		})
	   	   		$scope.InitializeandPlotPie(traffic_by_countriesData,"traffic_by_countries")
		   	 } else {
		   	 	$scope.entitySearchResult.data_not_found.tra_by_countries = false;
		   	 }
		   	 // traffic by search key word
		   	if($scope.entitySearchResult.list['search_keywords'] && 	$scope.entitySearchResult.list['search_keywords'].length>0){
			   	var search_keywords=[];
	   	   		angular.forEach($scope.entitySearchResult.list['search_keywords'],function(d,i){
	   	   		search_keywords.push({
		   	   			"key":d.entity.properties.keyword,
		   	   			"doc_count":parseFloat(d.entity.properties.precentage_of_search_traffic.split("%")[0]).toFixed(2)
		   	   		})
	   	   		})
	   	   		$scope.InitializeandPlotPie(search_keywords,"search_keywords");
				} else {
					$scope.entitySearchResult.data_not_found.sear_keywords = false;
				}
        	// traffic by domain pie starts here
			if($scope.entitySearchResult.list['traffic_by_domain'] && 	$scope.entitySearchResult.list['traffic_by_domain'].length>0){
			   	var traffic_by_domain=[];
	   	   		angular.forEach($scope.entitySearchResult.list['traffic_by_domain'],function(d,i){
	   	   		traffic_by_domain.push({
		   	   			"key":d.entity.properties.domain,
		   	   			"doc_count":parseFloat(d.entity.properties.precent_of_visitors.split("%")[0]).toFixed(2)
		   	   		})
	   	   		})
	   	   		$scope.InitializeandPlotPie(traffic_by_domain,"traffic_by_domain");
		   	 }else {
				$scope.entitySearchResult.data_not_found.tra_by_domain = false;
			}
        	// traffic by domain pie ends here
        	/*Associated followers location chart*/
        	loadSocialtablocationmap();

		},10);
	 };
	 /*
	  * @purpose: load transaction  charts
	  * @created: 6 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: prasanthi
	  */
	 function loadAllChartsForTransactions(){
		//load vla for careers
		$timeout(function(){
		   var colorsObj ={
   			 "Person":"#a75dc2",
   			 "Organization":"#3790ce"
		   }
	   	   /*plotting transaction type pie chart*/
		   var transactionTypesPieData = [{
				"key":"Person",
				"doc_count":$scope.entitySearchResult.list['associated_companies']['politicians'].length
			},
			{
				"key":"Organization",
				"doc_count":$scope.entitySearchResult.list['associated_companies']['political_organizations'].length
			}
		   ];
	   	   
	   	   if($scope.entitySearchResult.list['associated_companies']['politicians'].length > 0 || $scope.entitySearchResult.list['associated_companies']['political_organizations'].length > 0)
			  $scope.InitializeandPlotPie(transactionTypesPieData, 'transaction-type-pie-chart',colorsObj);
			else{
				$scope.entitySearchResult.data_not_found.transaction_types = false;
			}
		   /*plotting transaction type based amount pie chart*/
		   var transactionAmountByTypesPieData = [{
				"key":"Person",
				"doc_count":  d3.sum($scope.entitySearchResult.list['associated_companies']['politicians'], function(d) {return parseFloat(d.amount);})
			},
			{
				"key":"Organization",
				"doc_count": d3.sum($scope.entitySearchResult.list['associated_companies']['political_organizations'], function(d) {return parseFloat(d.amount);})
			}
		   ];
	   	   
		   if(d3.sum($scope.entitySearchResult.list['associated_companies']['politicians'], function(d) {return parseFloat(d.amount);}) > 0 &&
				   d3.sum($scope.entitySearchResult.list['associated_companies']['political_organizations'], function(d) {return parseFloat(d.amount);}) > 0)
			   $scope.InitializeandPlotPie(transactionAmountByTypesPieData, 'transaction-amount-type-pie-chart',colorsObj);
			   else{
			   	$scope.entitySearchResult.data_not_found.amount_ratio = false;
			   }
		},10);
	 };
	 /*
	  * @purpose: load documents  charts
	  * @created: 16 Nov 2017
	  * @params: no
	  * @return: no
	  * @auhtor: sandeep
	  */
	 function loadAllChartsForDocuments(){
   		var colorsObj ={
			"Videos":"#a75dc2",
			"Documents":"#3790ce"
//			"Crime list":"#e81f0f"
   		}
		//load vla for documents
		$timeout(function(){
	   		if(assetsRatioPieData.length > 0){
				   $scope.InitializeandPlotPie(assetsRatioPieData, 'pieChartAssetsRatio',colorsObj);
			} else {
				$scope.entitySearchResult.data_not_found.asserts_ratio = false;
			}
			if(assetsTrendLineChart.length > 0){
				plotLineChart(assetsTrendLineChart, "#assets-trend-line-chart", $("#assets-trend-line-chart").width(), '300');
			} else{
				$scope.entitySearchResult.data_not_found.asset_data =false;
			}
		},10);
	 };
	 function loadSocialtablocationmap() {
	 	for (var i = 0; i < $scope.entitySearchResult.list['followers'].length; i++) {
        		if($scope.entitySearchResult.list['followers'][i].location !='' && $scope.entitySearchResult.list['followers'][i].location){
        			var filter = _.find(sociallocationlist, { name: $scope.entitySearchResult.list['followers'][i].location });
	        		//using lodash to check for duplicates if no enters loop
	        		if(!filter){
	        			sociallocationlist.push({
		    				name: $scope.entitySearchResult.list['followers'][i].location,
		    				long: '',
		    				lat: '',
		    				mark: 'assets/images/redpin.png',
		    				source: $scope.entitySearchResult.list['followers'][i].source,
		    				title: $scope.entitySearchResult.list['followers'][i].name || $scope.entitySearchResult.list['followers'][i].source
	        	   		});
	        		}
        		}
        	}
        	 worldSocialChartOptions.markers = sociallocationlist;
        	 
			World(worldSocialChartOptions);
	 };
	 function getTruecallerPersonDetails(mobileNumber){
		//code for audit list to save in local-storage
        /*  var auditList =[];
         if($localStorage.get('auditList')){
         	auditList = JSON.parse($localStorage.get('auditList'));
         } */
         var copyOfName = $scope.entitySearchResult.name;
		 var names = copyOfName.split(' ');
		 var searchString="Searched for "+$scope.entitySearchResult.name+" on linkedin.com,openpaymentsdata.cms.gov, twitter.com, zabasearch.com, spokeo.com, Interpol wanted list, sanctions_wrapper, sanctionssearch.ofac.treas.gov, deepdotweb.com, bloomberg.com profile search, bloomberg stocks search, unlimitedcriminalchecks.com, fbi.gov, denied_personlist.gov, https://data.cityofchicago.org/Public-Safety/Sex-Offenders/vc9r-bqvy, crimestoppers-uk.org,https://www.ptc-top.com/2016/01/21/ptc-scam-list-the-greatest-in-the-web-update-daily/, supplierblacklist.com, wikipedia.com, imsasllc.com, people.gov.uk, instagram.com, person.corporationwiki.com, google plus, google searchVyoutube.com, personal-profiles.bloomberg.com, littlesis.com  "
		 if( $scope.entitySearchResult.current_current_work_place &&
				 $scope.entitySearchResult.current_current_work_place !== '') {
			 var currentCompany = $scope.entitySearchResult.current_current_work_place.trim().replace(/\↵/g, '');
				 /* auditList.push({
			         	title: 'Searched for work place',
			         	timestamp: Date.now(),
			         	name: $scope.entitySearchResult.name,
			         	description:"Searched for "+($scope.entitySearchResult.name+','+currentCompany)+" on hunter.io,littlesis.com,exclusions.oig.hhs.gov "

				 }); */
			 }else{
			 searchString = searchString+"hunter.io,exclusions.oig.hhs.gov";
		 }

        /*  auditList.push({
         	title: 'Searched for '+ $scope.entitySearchResult.name,
         	timestamp: Date.now(),
         	name: $scope.entitySearchResult.name,
         	description:searchString
         })
          auditList.push({
         	title: 'Searched for phone number',
         	timestamp: Date.now(),
         	name: $scope.entitySearchResult.name,
         	description:"Searched for "+mobileNumber+" on truecaller"
         }) */
       //  $localStorage.put('auditList', JSON.stringify(auditList));
         //code for audit ends here
		 var truecallerFetcherIds = ['30'];
		 var requestData = {
			fetchers: truecallerFetcherIds,
			keyword: mobileNumber,
	        searchType: 'Person'
		 };
		 EntityApiService.getEntityDataByTextId(requestData).then(function(subResponse) {
			for(var i=0; i < subResponse.data.results.length; i++){
	            if (subResponse.data.results[i].status) {
	            	for(var j=0; j<subResponse.data.results[i].entities.length; j++){
            			if(subResponse.data.results[i].entities[j].name !== '' && subResponse.data.results[i].entities[j].name){
            				$scope.entitySearchResult.list['truecaller'].push({
            					name: subResponse.data.results[i].entities[j].properties.name,
            					image: subResponse.data.results[i].entities[j].properties.image,
            					number: subResponse.data.results[i].entities[j].properties.number,
            					location: subResponse.data.results[i].entities[j].properties.location,
            					service_provider: subResponse.data.results[i].entities[j].properties.service_provider,
            					details: subResponse.data.results[i].entities[j].properties.details,
            					title: subResponse.data.results[i].entities[j].properties.title,
            					email: subResponse.data.results[i].entities[j].properties.email,
            					type: subResponse.data.results[i].entities[j].properties.type_,
            					source: subResponse.data.results[i].entities[j].source,
            					risk_score: subResponse.data.results[i].entities[j].properties.risk_score
            				});
            				if(subResponse.data.results[i].entities[j].properties.location !== '' && subResponse.data.results[i].entities[j].properties.location){
	            				locationList.push({
	                				name: subResponse.data.results[i].entities[j].properties.location,
	                				long: '',
	                				lat: '',
	                				mark: 'assets/images/redpin.png',
	                				source: subentity.source,
	                				title: subentity.properties.name || subentity.source
	                			});
            				}
            			}
        			}
	            }
			}
		 });
	  }
	  function loadtwitterTags() {
	  	//load charts for twitter tag
	  	if (twitter_tag_words.length > 0) {
	  		$scope.entitySearchResult.list['is_twitter_tag_chart'] = false;
	  		tagCloudTwitterTagsOptions.data = twitter_tag_words;
	  		EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudTwitterTagsOptions);
	  	}
	  	if (twitter_tag_words.length === 0) {
	  		$scope.entitySearchResult.data_not_found.twitter_tags = false;
	  	}
	  }
	 /*---------------------------------------------- pie chart Starts---------------------------------*/
	    $scope.InitializeandPlotPie = function(data, id,colorsObj) {
		    data.map(function (d) {
		        return d.value = d.doc_count;
		    });
		    var newData = [];
		    var keys = [];
		    angular.forEach(data, function(d, i){
		    	if(d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(),keys)== -1){
		    		newData.push(d);
		    		keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
		    	}
		    });
		    var maxval = d3.max(data,function(d){
	   	    	return d.value;
	   		 });
		    var sum = d3.sum(data,function(d){
	   	    	return d.value;
	   		 });
		    var colors = ["#5d96c8" , "#b753cd" , "#69ca6b" , "#69ca6b","#c1bd4f" , "#db3f8d" , "#669900","#334433"]
		    var options = {
		        container: "#" + id,
		        data: newData,
		        height:160,
		        colors:colors,
		        colorsObj:colorsObj,
		        islegends:true,
		        islegendleft:true,
		        legendwidth:30,
		        istxt: sum ==0?0:parseInt((maxval/sum)*100)+"%",
		        legendmargintop:30
		    };
		    setTimeout(function () {
		        new reusablePie(options);
		    });
		    if(id === 'role-based-pie-chart')
		    	$scope.entitySearchResult.list['is_role_based_pie_chart'] = false;
		    if(id === 'interaction-ratio')
		    	$scope.entitySearchResult.list['is_twitter_interaction_ratio'] = false;
		    if(id === 'pieChartAssetsRatio')
		    	$scope.entitySearchResult.list['is_assets_ration_pie_chart'] = false;
		    if(id === 'transaction-type-pie-chart')
		    	$scope.entitySearchResult.list['is_transaction_type_pie_chart'] = false;
		    if(id === 'transaction-amount-type-pie-chart')
		    	$scope.entitySearchResult.list['is_transaction_amount_type_pie_chart'] = false;
		    if(id === 'holding-pie-chart')
		    	$scope.entitySearchResult.list['is_holding_pie_chart'] = false;
		    if(id === 'connection-role-based-pie-chart')
		    	$scope.entitySearchResult.list['is_connection_role_based_pie_chart'] = false;
		    if(id === 'pieChartRiskRatio')
		    	$scope.entitySearchResult.list['is_risk_alters_pie_chart'] = false;
		    if(id === 'traffic_by_countries')
		    	$scope.entitySearchResult.list['is_traffic_by_countries'] = false;
		    if(id === 'search_keywords')
		    	$scope.entitySearchResult.list['is_search_keywords'] = false;
		    if(id === 'traffic_by_domain')
		    	$scope.entitySearchResult.list['is_traffic_by_domain'] = false;
		    if(id === 'earning-ratio-pie-chart')
		    	$scope.entitySearchResult.list['is_earning_ratio_pie_chart'] = false;
		    if(id === 'interest_ratio')
		    	$scope.entitySearchResult.list['is_interest_ratio'] = false;
		     if(id === 'pieDonationRatio')
		    	$scope.entitySearchResult.list['is_donation_ratio'] = false;
		    if(id === 'organDonationRatio')
		    	$scope.entitySearchResult.list['is_org_donation_ratio'] = false;

		};
		/*---------------------------------------------- pie chart ends--------------------------------*/
		function checkforAvailableMember(name){
			if(name!==''|| name !== undefined || name !==null){
			   var company_boardmembers = _.find($scope.entitySearchResult.list['personal_profiles_bloomberg']['board_memberships'],{entity_name:  name});
			   var company_key_executives = _.find($scope.entitySearchResult.list['boardmemberList'],{name:  name});
			   var founders = _.find($scope.entitySearchResult.list['government_positions_list'],{name:  name});
			   var has_company_members	= _.find($scope.entitySearchResult.list['has_company_members'],{name: name});
			   var key_persons	= _.find($scope.entitySearchResult.list['memberships'],{name:  name});
			   if(company_boardmembers || company_key_executives || founders || has_company_members ||key_persons ){
				  return true;
			   }else{
			   return false;
			  }
			}
		}
		/*---------------------------------------------- network chart Starts---------------------------------------------*/
		function loadoverviewVLA(id,data){
			
			if($("#"+id).find("canvas").length ==0){
				if(id == "networkGraphOverview"){
					$('#networkGraphCdd')[0].style.display = "none";
    				$('#networkGraphCdd').siblings()[0].style.display = "";
    				$('#networkGraphCdd').siblings()[1].style.display = "none";
    				$('#networkGraphCdd').parent().siblings('a')[0].style.display = "none";
				}
					$('#' + id)[0].style.display = "none";
					$('#' + id).siblings()[0].style.display = "";
					$('#' + id).siblings()[1].style.display = "none";
					$('#' + id).parent().siblings('a')[0].style.display = "none";
					var data = data;
					//dummy code starts here
					if(id == "networkGraphOverview" && (($stateParams.query).toUpperCase() == "BERNARD MADOFF" || ($stateParams.query).toUpperCase() == "BARNY MADDOF" || ($stateParams.query).toUpperCase() == "BERNARD LAWRENCE MADOFF")){
						vlaDataArr["networkGraphCdd"] = mockedCompany.relationAnalysisData;            			
        				vlaDataArr[id] = mockedCompany.relationAnalysisData;
        				var options = {
            					data: mockedCompany.relationAnalysisData,
            					id: id
            			}
        				$('#' + id)[0].style.display = "";
        				$('#' + id).siblings()[0].style.display = "none";
        				$('#' + id).parent().siblings('a')[0].style.display = "";        				
        				$scope.loadDataAndPlotGraphForEntity(options);
        				$('#networkGraphCdd')[0].style.display = "";
        				$('#networkGraphCdd').siblings()[0].style.display = "none";
        				$('#networkGraphCdd').parent().siblings('a')[0].style.display = ""; 
        				var options1 = {
            					data: mockedCompany.relationAnalysisData,
            					id: "networkGraphCdd"
            			}
        				$scope.loadDataAndPlotGraphForEntity(options1);
    				//dummy code starts here
                	}else{
					EntityApiService.getGraphData(data).then(function(response){
		                if (response !== void 0) {
		                	var caseId = response.data.caseId;
		                	
		                		EntityApiService.getVLAdata(caseId).then(function(resp){	                	
		                		
		                			if(id == "networkGraphOverview"){
		                				vlaDataArr["networkGraphCdd"] =resp.data 
		                			}		                		
		                		
		                		vlaDataArr[id] = resp.data;
		                		if(resp != void 0){
		                			var options = {
		                					data: resp.data,
		                					id: id
		                			}
		                			if(resp.data.edges.length > 0 && resp.data.vertices.length > 0){
		                				$('#' + id)[0].style.display = "";
				        				$('#' + id).siblings()[0].style.display = "none";
				        				$('#' + id).parent().siblings('a')[0].style.display = "";
				        				//handling for due deligence starts
				        				if(id == "networkGraphOverview"){
				        					$('#networkGraphCdd')[0].style.display = "";
					        				$('#networkGraphCdd').siblings()[0].style.display = "none";
					        				$('#networkGraphCdd').parent().siblings('a')[0].style.display = "";
					        				var options1 = {
				                					data: resp.data,
				                					id: "networkGraphCdd"
				                			}
					        				$scope.loadDataAndPlotGraphForEntity(options1);
				        				}
				        				//handling for due deligence ends 
		                				$scope.loadDataAndPlotGraphForEntity(options);

		                			}else{
		                				if(id == "networkGraphOverview"){
		                					$('#networkGraphCdd').siblings()[1].style.display = "";
			                				$('#networkGraphCdd').siblings()[0].style.display = "none";
		                				}
		                				$('#' + id).siblings()[1].style.display = "";
		                				$('#' + id).siblings()[0].style.display = "none";
		                			}
		                		}
		                	},function(){
		                		$('#' + id).siblings()[1].style.display = "";
                				$('#' + id).siblings()[0].style.display = "none";
                				//handling for due deligence starts
		        				if(id == "networkGraphOverview"){
			        				$('#networkGraphCdd').siblings()[1].style.display = "";
			        				$('#networkGraphCdd').siblings()[0].style.display = "none";
		        				}
		        				//handling for due deligence ends 
		                	});
		                }
			       },function(error){
			    	   $('#' + id).siblings()[1].style.display = "";
       					$('#' + id).siblings()[0].style.display = "none";
       				//handling for due deligence starts
        				if(id == "networkGraphOverview"){
        					$('#networkGraphCdd').siblings()[1].style.display = "";
	        				$('#networkGraphCdd').siblings()[0].style.display = "none";
        				}
        				//handling for due deligence ends 
			       });
                	}
			}
		}
   /**
	 * Function to load data for network chart
	 */
	 $scope.loadDataAndPlotGraphForEntity = function(options){

			handleGraphData(options.data, options)

        function handleGraphData(data, options) {
				
		      var finalData = {};
		      var edges = [];
		      var nodeIDS = [];
		      var nodes = [];
		      angular.forEach(data.vertices, function ( val,i) {
		          nodeIDS.push(val.id)
		           val.weight=10;
		          nodes.push({
		              data: val
		          });
		      });
		      angular.forEach(data.edges, function ( val,i) {
		          if ($.inArray(val.from, nodeIDS) != -1 && $.inArray(val.to, nodeIDS) != -1)
		              edges.push({
		                  data: {
		                      source: val.from,
		                      target: val.to,
		                      labelE: val.labelE,
		                      id: val.id
		                  }});
		      });
		      finalData.nodes = nodes;
		      finalData.edges = edges;
		     
		      if(($stateParams.query).toUpperCase() == "BERNARD MADOFF" ||  (  $stateParams.query).toUpperCase() == "BARNY MADDOF" || ($stateParams.query).toUpperCase() == "BERNARD LAWRENCE MADOFF"){
		    	  loadEntityNetworkChart(finalData, options,"Bernard Madoff");
				}else{
					 loadEntityNetworkChart(finalData, options);
				}
		 }
	}
	/*---------------------------------------------- network chart ends--------------------------------*/

	/*---------------------------------------------- bubble chart Starts--------------------------------*/
	function socialBubbleChart(bubbleOptions){
	    $timeout(function(){
	    	if(bubbleOptions.container === '#socialBubbleChart'){
				$scope.entitySearchResult.list['is_overview_social_bubble_chart'] = false;
			}
	    	if(bubbleOptions.container === '#bubbleCompittitorsFinanceChart')
	    		$scope.entitySearchResult.list['is_bubble_compittitors_finance_chart'] = false;
	        var exampleChart = new BubbleHierarchyChart(bubbleOptions);
	    },1000);
	}
	/*---------------------------------------------- bubble chart ends--------------------------------*/

	 /*----------------------------------------------Performance Line chart Starts---------------------------------------------*/
	 function plotLineChart(lineChartData, id, lineWidth, height){
		 var lineData = [];
		 if(id == '#lineChartFollowersTrend'){
			 $scope.entitySearchResult.list['is_twitter_follower_trends'] = false;
				var sum =0;
				angular.forEach(lineChartData, function(d,key){
					d.x = new Date(d.date);
					d.value = d.sum+sum;
					sum = d.value;
						return d.time = d.date;
				});
		 }
		 else{
			 lineChartData.map(function(d){
				d.x = new Date(d.date);
				d.value = d.sum;
				return d.time = d.date;
			});
		 }
		 lineData =[{
				"key": "",
				"values": lineChartData
			 }];
		 var options = {
		    container: id,
		    height: height != undefined ? height: '',
		    width: lineWidth != undefined ? lineWidth: '',
		    axisX: true,
		    axisY: true,
		    gridy: true ,
		    tickformat:"year",
		    data: lineData,
		    marginRight:20,
		    gridcolor:"#2e424b",
		    actualData: lineData[0].values,
		    ystartsFromZero: true
		 };
		 if(id === '#assets-trend-line-chart')
			 $scope.entitySearchResult.list['is_assets_trend_line_chart'] = false;
		 new InitializeandPlotLine(options);
	 }
	 /*----------------------------------------------Performance Line chart Ends ---------------------------------------------*/
	// ---------------------------------------Network chart-----------------------------------------------------------//
		function loadtwitterchart(){
			var WebSocketTwitterUrl= EntityCompanyConst.WebSocketTwitterUrl;
			var nodes = [], links = [], tweetList = [], retweetList = [], mentionList = [], tagCloudNameList = [],
			locationList = [], finalLocationList = [], locationNames = [], randomGeneratedId = Math.floor((Math.random() * 10) + 19999),maxTwitterTagDomainRangeValue=5;;
			var socket = {};
			$scope.liveFeed = {
				nodeList: []
			};

			function getWebSocketTwitterData(name){
				var tweetObjectOneOrigin = {
					id: randomGeneratedId,
					name: $scope.entitySearchResult.name,
					screen_name: $scope.entitySearchResult.name,
					text: $scope.entitySearchResult.name,
					profile_image: '',
					createdOn: new Date(),
					type: 'main'
				 };
				 nodes.push(tweetObjectOneOrigin);
				 socket = io.connect(WebSocketTwitterUrl, {'forceNew': true});

				 socket.on('message', function(response){
					$scope.disableSearchButton = false;
				    $('.custom-spinner').css('display', 'none');
					var isString = false;
					if(response.indexOf('{') === -1){
						isString = true;
					} else {
						response = JSON.parse(response);
						getSourceTargetData(response);
					}
				});

				socket.on('connect', function() {
					socket.send(name);
				});

			    socket.on('disconnect', function() {
			    	
				});
			 }
			/*Initiate twitter live feed socket ui call*/
			getWebSocketTwitterData($scope.entitySearchResult.name);

			 function getSourceTargetData(tweetData){
				 var linkDataOrigin = {
					source: null,
					target: null
				 };
				 if(tweetList.indexOf(tweetData.user.screen_name) === -1) {
					 var tweetObjectOne = {
						id: tweetData.id,
						name: tweetData.user.name,
						screen_name: tweetData.user.screen_name,
						text: tweetData.text,
						profile_image: tweetData.user.profile_image_url,
						createdOn: new Date(tweetData.user.created_at),
						likeCounts: tweetData.user.favourites_count || 0,
						retweetCounts: tweetData.user.retweet_count || 0,
						replyCounts: tweetData.user.reply_count || 0,
						type: 'tweet'
					};
					 tagCloudNameList.splice(0, 0, {
						 text: tweetData.user.name,
						 size: tweetData.user.favourites_count || 5
					 });
					if(tagCloudNameList.length === 21) {
						socket.close();
						tagCloudNameList.pop();
					}
					 if(tweetData.user.location !== '' && tweetData.user.location) {
						 locationList.push({
		     				name: tweetData.user.location,
		    				long: '',
		    				lat: '',
		    				mark: 'assets/images/redpin.png'
		    			});
					 }
					 nodes.push(tweetObjectOne);
					 $timeout(function(){
						 $scope.entitySearchResult.tweetCounts++;
						 $scope.liveFeed.nodeList.push(tweetObjectOne);
					 },0);
					 tweetList.push(tweetData.user.screen_name);
					 linkDataOrigin.source = randomGeneratedId;
					 linkDataOrigin.target = tweetData.id;
					 links.push(linkDataOrigin);
				 }
				 if(tweetData.entities['user_mentions'].length > 0){
					 angular.forEach(tweetData.entities['user_mentions'], function(mention, mentionKey){
						if(mentionList.indexOf(mention.screen_name) === -1) {
							var mentionObject = {
								id: 'mention-' + mention.id,
								name: mention.name,
								screen_name: mention.screen_name,
								type: 'mention'
							};
							 $timeout(function(){
								 $scope.entitySearchResult.mentionCounts++;
							 },0);
							nodes.push(mentionObject);
						    mentionList.push(mention.screen_name);
						}
						links.push({
							source: tweetData.id,
							target: 'mention-' + mention.id
						});
					 });
				 }
				 var linkData = {
					source: null,
					target: null
				 };
				 linkData.source = tweetData.id;
				 if(_.isObject(tweetData.retweeted_status)){
					 if(retweetList.indexOf(tweetData.retweeted_status.user.screen_name) === -1) {
						var tweetObject = {
							id: tweetData.retweeted_status.id,
							name: tweetData.retweeted_status.user.name,
							screen_name: tweetData.retweeted_status.user.screen_name,
							text: tweetData.retweeted_status.text,
							profile_image: tweetData.retweeted_status.user.profile_image_url,
							createdOn: new Date(tweetData.retweeted_status.user.created_at),
							likeCounts: tweetData.retweeted_status.user.favourites_count || 0,
							retweetCounts: tweetData.retweeted_status.user.retweet_count || 0,
							replyCounts: tweetData.retweeted_status.user.reply_count || 0,
							type: 'retweet'
						};
						tagCloudNameList.splice(0, 0, {
							 text: tweetData.retweeted_status.user.name,
							 size: tweetData.retweeted_status.user.favourites_count || 5
						});
						if(tagCloudNameList.length === 21) {
							tagCloudNameList.pop();
						}
						 if(tweetData.retweeted_status.user.location !== '' && tweetData.retweeted_status.user.location){
							 locationList.push({
			     				name: tweetData.retweeted_status.user.location,
			    				long: '',
			    				lat: '',
			    				mark: 'assets/images/redpin.png'
			    			 });
						 }
						nodes.push(tweetObject);
						 $timeout(function(){
							 $scope.entitySearchResult.reTweetCounts++;
							 $scope.liveFeed.nodeList.push(tweetObject);
						 },0);
						retweetList.push(tweetData.retweeted_status.user.screen_name);
					 }
					 linkData.target = tweetData.retweeted_status.id;
				 }
				 if(linkData.source && linkData.target)
					 links.push(linkData);
				 var twitterFinalData = {'links': links, 'nodes': nodes};
				 var twitterFinalDataNew = jQuery.extend(true, [], twitterFinalData);
				 $scope.entitySearchResult.list['twitter_socket_network_chart'] = false;
				 twitterPlotNetworkChart(twitterFinalDataNew);
			 }

			// ----------------------------Twitter Network Chart----------------------------------------
			 var tool_tip = $('body').append('<div class="CaseTimeLine_Chart_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
//			 var legendtool = $('body').append('<div id="socialMedialLegend" class="legend"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
			 var width = $("#twitter-socket-network-chart").width(),
			 height =300;
		 	  var svg = d3.select("#twitter-socket-network-chart").append("svg")
		 	  .attr("width", width)
		 	    .attr("height", height),
		       color = d3.scaleOrdinal(d3.schemeCategory10);
			 /*build the arrow. */
			 svg.append("svg:defs").selectAll("marker")
			       .data(["end"])      /* Different link/path types can be defined here */
			       .enter().append("svg:marker")    /* This section adds in the arrows */
			       .attr("id", String)
			       .attr("viewBox", "0 -5 10 10")
			       .attr("refX", 15)
			       .attr("refY", -1.5)
			       .attr("markerWidth", 6)
			       .attr("markerHeight", 6)
			       .attr("orient", "auto")
			       .append("svg:path")
			       .attr("d", "M0,-5L10,0L0,5")
			       .style("fill", "#666");

			 var a = {id: "Sarah"},
			       b = {id: "Alice"},
			       c = {id: "Eveie"},
			       d = {id: "Peter"},
			       nodesnew = [],
			        linksnew = [];
			 var simulation = d3.forceSimulation(nodesnew)
			       .force("charge", d3.forceManyBody().strength(-300))
			       .force("link", d3.forceLink(linksnew).distance(60))
			       .force("x", d3.forceX())
			       .force("y", d3.forceY())
			       .alphaTarget(1)
			       .on("tick", ticked);

			 var g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
			       link = g.append("g").attr("stroke", "#000").attr("stroke-width", 1.5).selectAll(".link"),
			       node = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");

			 function twitterPlotNetworkChart(data) {
			 	nodesnew = data.nodes;
			 	linksnew=[];

				data.links.forEach(function(d) {
			       linksnew.push({
			           "source":nodesnew[nodesnew.findIndex( function (el) { return el.id == d.source; })],
			           "target":nodesnew[nodesnew.findIndex( function (el) { return el.id == d.target})]
			       });
				});
				restart();
			 }
			 restart();

			 var colornew;
			 var radius;
			 function restart() {
				 /*legend(nodesnew);*/
				   /* Apply the general update pattern to the nodes. */
				   node = node.data(nodesnew, function (d) {
				       return d.id;
				   });
				   node.exit().remove();
				   node = node.enter().append("circle").attr("fill", function (d) {
				      if(d.type=="main"){
						   colornew="#000080"/*blue*/
							}
						else if(d.type=="tweet"){
							colornew="#FFD700"/*yellow*/
						}
						else if(d.type=="mention"){
							colornew="#800000"
						}
						else if(d.type=="retweet"){
							colornew="#008000"/*green*/
						}
				       return colornew;
				   }).attr("r",  function (d) {
				      if(d.type=="main"){
						   radius="10"/*blue*/
							}
						else {
							radius="6"/*green*/
						}
				       return radius;
				   })
				   .on("mouseover", function (d) {
					   var appendHTMLTooltip = '';
					   if(d.type === 'tweet' || d.type === 'retweet')
						   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p><p><i class="fa fa-retweat icon"></i>' +d.text + '</p><p class="progress-text"><i class="fa fa-wechat icon"></i><span>'+d.replyCounts+'</span><img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" /><span>'+d.retweetCounts+'</span><i class="fa fa-heart icon"></i><span>'+d.likeCounts+'</span></p></div>';
					   else
						   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p></div>';
				      $(".CaseTimeLine_Chart_tooltip").html(appendHTMLTooltip);
				      return $(".CaseTimeLine_Chart_tooltip").css("display", "block");
				   }).on("mousemove", function (event) {
					  var value= $(this).offset();
					 var top=value.top
					 var left=value.left+20
				      $(".CaseTimeLine_Chart_tooltip").css("top", top + "px")
				      return  $(".CaseTimeLine_Chart_tooltip").css("left", left+ "px");

				   }).on("mouseout", function () {
				     /* $(this).css("opacity", 0.4); */
				      /*hide tool-tip*/
				      return $(".CaseTimeLine_Chart_tooltip").css("display", "none");
				   }).merge(node);

				   /* Apply the general update pattern to the links.*/
				   link = link.data(linksnew, function (d) {
				       return d.source.id + "-" + d.target.id;
				   });
				   link.exit().remove();
				   link = link.enter().append("line").merge(link);

				   /* Update and restart the simulation.*/
				   simulation.nodes(nodesnew);
				   simulation.force("link").links(linksnew);
				   simulation.alpha(1).restart();
			 }

			 function legend(nodesnew)
			 {
				 $(".legend").empty()
				 var type=[];
				 nodesnew.forEach(function(d)
				 {
					 type.push(d.type)
				 })
				 var outputArray = [];
				 for (var i = 0; i < type.length; i++)
				    {
				        if ((jQuery.inArray(type[i], outputArray)) == -1)
				        {
				            outputArray.push(type[i]);
				        }
				    }
			     var legendcolor;
				 outputArray.forEach(function(d){
					if(d=="main"){
					   d="Search query"
					   legendcolor="#000080"
					} else if(d=="tweet"){
						legendcolor="#FFD700"
					}
					else if(d=="mention"){
						legendcolor="#800000"
					}
					else if(d=="retweet"){
						legendcolor="#008000"
					}
					else {
						legendcolor="#000"
					}
					 $(".legend").append('<div style="display: inline-block;vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;text-transform: capitalize;margin: 0 10px 0 0">'+d+'</span><span style="margin: 0 10px 0 0;vertical-align: middle;border-radius: 50%;display: inline-block;width: 15px;height: 15px;color: #435561;font-size: 12px;line-height: 20px;text-align: center;padding: 1px 4px;border: 2px solid #435561;background-color: '+legendcolor+'"></span></div>')
				 })
				 return $(".legend").css("visibility", "hidden");
			 }

			 function ticked() {
			   node.attr("cx", function (d) {
			       return d.x;
			   })
	           .attr("cy", function (d) {
	               return d.y;
	           })

			   link.attr("x1", function (d) {
			       return d.source.x;
			   })
	           .attr("y1", function (d) {
	               return d.source.y;
	           })
	           .attr("x2", function (d) {
	               return d.target.x;
	           })
	           .attr("y2", function (d) {
	               return d.target.y;
	           }).attr("marker-end", "url(#end)");
			 }
		}
	// ---------------------------------------Network chart ends-----------------------------------------------------------//

	// ---------------------------------------Alert tab charts starts-----------------------------------------------------------//
		function PlotriskAlertCharts(){

   		// time line starts here
	   		 var alertTimelineData =[];

	   		 if($scope.entitySearchResult.list.sanctionList.length>0){
	   			 angular.forEach($scope.entitySearchResult.list.sanctionList,function(d,i){
	   				if(d.reported)
	   				 alertTimelineData.push({
	   					"time":d.reported,
	   					"type":"Black list",
	   					"amount":Math.floor(Math.random() * 20),
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b> '+d.list_name+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Sanction Type:</b> '+d.list_type+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">link:</b>  '+d.link+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b> Black List</span>'
	   				})
	   			 });
	   		 }
	   		 if($scope.entitySearchResult.list.supplierblacklist.Source){
   				if($scope.entitySearchResult.list.supplierblacklist['reported time'] !== '' && $scope.entitySearchResult.list.supplierblacklist['reported time']){
	   				alertTimelineData.push({
	   					"time": new Date($scope.entitySearchResult.list.supplierblacklist['reported time']),
	   					"type":"Black list",
	   					"amount":Math.floor(Math.random() * 20),
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">seller:</b> '+$scope.entitySearchResult.list.supplierblacklist.seller+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type of complaints:</b> '+$scope.entitySearchResult.list.supplierblacklist['type of complaints']+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">seller address:</b>  '+$scope.entitySearchResult.list.supplierblacklist['seller address']+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">reported time:</b>  '+ moment(new Date($scope.entitySearchResult.list.supplierblacklist['reported time'])).format('DD-MM-YYYY')+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b> Black List</span>'
	   				});
   				}
	   		 }
	   		 if($scope.entitySearchResult.list.pep.length>0){
	   			angular.forEach($scope.entitySearchResult.list.pep,function(d,i){
	   				if(d.reported)
	   				alertTimelineData.push({
	   					"time":d.reported,
	   					"type":"Pep Alert",
	   					"amount":Math.floor(Math.random() * 20),
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b> '+d.list_name+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Sanction Type:</b> '+d.list_type+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">link:</b>  '+d.link+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b> PEP Alerts</span>'
	   				})
	   			 });

	   		 }
	   		if($scope.entitySearchResult.list.scam.length>0){
	   			angular.forEach($scope.entitySearchResult.list.scam,function(d,i){
	   				if(d.reported)
	   				alertTimelineData.push({
	   					"time":d.reported,
	   					"amount":Math.floor(Math.random() * 20),
	   					"type":"Fraud Alerts",
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Title:</b> '+d.root.title+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Reported By:</b> '+d.root.reported_by+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Report Time:</b>  '+d.root.report_time+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">source:</b>  '+d.source+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b>Fraud Alerts</span>'
	   				})
	   			});
	   		 }
	   		if($scope.entitySearchResult.list['adverse_news'].length>0){
	   			angular.forEach($scope.entitySearchResult.list.adverse_news,function(d,i){
	   				alertTimelineData.push({
	   					"time":d.published,
	   					"amount":Math.floor(Math.random() * 20),
	   					"type":"Adverse News",
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">title:</b> '+d.title+'</span><span style="margin-bottom:10px"><b style="display:block;font-size:13px">published:</b>  '+d.published+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">source:</b>  '+d.source+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">url:</b>  '+d.url+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b>News</span>'
	   				});
	   			});
	   		 }

	   		if($scope.entitySearchResult.list['denied_personlist'].length>0){
	   			angular.forEach($scope.entitySearchResult.list.denied_personlist,function(d,i){
	   				alertTimelineData.push({
	   					"time":d.effective_date,
	   					"amount":Math.floor(Math.random() * 20),
	   					"type":"Fraud Alerts",
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">name:</b> '+d.name+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">action:</b> '+d.action+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">effective date:</b>  '+d.effective_date+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">expiration date:</b>  '+d.expiration_date+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">fr citation:</b>  '+d.fr_citation+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">risk score:</b>  '+d.risk_score+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b> Fraud Alerts</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">address:</b>  '+d.address+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">source:</b>  '+d.source+'</span>'
					});
	   			});
	   		 }

	   		 if($scope.entitySearchResult.list['exclusions']['person']){
   				alertTimelineData.push({
   					"time": new Date($scope.entitySearchResult.list['exclusions']['person'].excl_date),
   					"amount":Math.floor(Math.random() * 20),
   					"type":"Fraud Alerts",
   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">name:</b> '+ $scope.entitySearchResult.list['exclusions']['person'].first_name + ' ' + $scope.entitySearchResult.list['exclusions']['person'].last_name +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">specialty:</b> '+d.specialty+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">exclusion date:</b>  '+ moment(new Date(d.excl_date)).format('DD-MM-YYYY')+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b>Fraud Alerts</span>'
				});
	   		 }
	   		if($scope.entitySearchResult.list['badbuyerslist'].length > 0){
	   			angular.forEach($scope.entitySearchResult.list['badbuyerslist'],function(d,i){
	   				if(d.reported_at !== '' && d.reported_at){
		   				alertTimelineData.push({
		   					"time": d.reported_at,
		   					"amount":Math.floor(Math.random() * 20),
		   					"type":"Fraud Alerts",
		   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Name:</b> '+d.name+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Reported Date:</b> '+ moment(d.reported_at).format('DD-MM-YYYY')+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Email:</b>  '+d.email+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Payment method:</b>  '+d.payment_method+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">purchase date:</b>  '+d.purchase_date+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b>Fraud Alerts</span></br>'
						});
	   				}
	   			});
	   		}
	   		if($scope.entitySearchResult.list['imsasllc_offenses'].length>0){
	   			angular.forEach($scope.entitySearchResult.list.imsasllc_offenses,function(d,i){
	   				if(d.DateOfCrime !== '' && d.DateOfCrime){
		   				alertTimelineData.push({
		   					"time": new Date(d.DateOfCrime),
		   					"amount":Math.floor(Math.random() * 20),
		   					"type":"Offenses",
		   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Description:</b> '+d.Description+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Date of crime:</b> '+d.DateOfCrime+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Case Number:</b>  '+d.CaseNumber+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Code:</b>  '+d.Code+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">risk score:</b>  '+d.risk_score+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">type:</b>Criminal Offenses</span></br>'
						});
	   				}
	   			});
	   		 }
	   		if($scope.entitySearchResult.list['news_deep_web'].length>0){
	   			 // no count
	   			angular.forEach($scope.entitySearchResult.list.news_deep_web,function(d,i){
	   				alertTimelineData.push({
	   					"time":d.postedOn,
	   					"amount":Math.floor(Math.random() * 20),
	   					"type":"Dark Web",
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">title:</b> '+d.title+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">posted On:</b> '+d.postedOn+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">details:</b>  '+d.details+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Type:</b>Dark Web</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">source:</b>  '+d.source+'</span>'
	   						})
	   			});
	   		 }
	   		 if($scope.entitySearchResult.list['sci_ccc_nashville_gov'].length > 0){
	   		 	for (var i = 0; i < $scope.entitySearchResult.list['sci_ccc_nashville_gov'].length; i++) {
	   		 		alertTimelineData.push({
	   					"time":$scope.entitySearchResult.list['sci_ccc_nashville_gov'][i].offense_date,
	   					"amount":Math.floor(Math.random() * 20),
	   					"type":"Offenses",
	   					'txt':'<span style="margin-bottom:10px"><b style="display:block;font-size:13px">Charged Offense:</b> '+$scope.entitySearchResult.list['sci_ccc_nashville_gov'][i].charged_offense+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Offense Date:</b> '+$scope.entitySearchResult.list['sci_ccc_nashville_gov'][i].offense_date+'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">details:</b>  '+$scope.entitySearchResult.list['sci_ccc_nashville_gov'][i].disposition +'</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">Type:</b>Criminal Offence</span></br><span style="margin-bottom:10px"><b style="display:block;font-size:13px">source:</b>  '+$scope.entitySearchResult.list['sci_ccc_nashville_gov'][i].source+'</span>'

	   				})
	   		 	}
	   		 }
	   		var colorsObj ={
   				"Pep Alert":"#a75dc2",
   				"Black list":"#337ab7",
   				"Fraud Alerts":"#399034",
   				"Dark Web":"#e81f0f",
   				"Adverse News":"#69ca6b",
   				"Offenses":"#dd9d22"
	   		}
	   		
	   		var timelineOptions = {
		        container: "#riskyTimelineChart",
		        height: 50,
		        colors:["#3790ce","#a75dc2","#399034","#69ca6b","#dd9d22",'#3778be'],
		        data:alertTimelineData,
		        colorsObj:colorsObj,
			};
	   	   loadtimeLineColumnChart(timelineOptions);

	   	   var riskPieData = [];
	   	   if($scope.entitySearchResult.list.adverse_news.length > 0){
		   		riskPieData.push({
					"key":"Adverse News",
					"doc_count":$scope.entitySearchResult.list.adverse_news.length
				});
	   	   }
   		   if($scope.entitySearchResult.list['badbuyerslist'].length > 0 || $scope.entitySearchResult.list.scam.length > 0 || $scope.entitySearchResult.list['denied_personlist'].length > 0){
   				riskPieData.push({
					"key":"Fraud Alerts",
					"doc_count":$scope.entitySearchResult.list['badbuyerslist'].length + $scope.entitySearchResult.list.scam.length + $scope.entitySearchResult.list['denied_personlist'].length
				});
   		   }
   		   if($scope.entitySearchResult.list.pep.length > 0){
   				riskPieData.push({
					"key":"Pep Alert",
					"doc_count":$scope.entitySearchResult.list.pep.length
				});
   		   }
   		   if($scope.entitySearchResult.list.sanctionList.length > 0 || $scope.entitySearchResult.list.supplierblacklist.Source){
   				riskPieData.push({
					"key":"Black list",
					"doc_count": $scope.entitySearchResult.list.sanctionList.length + ($scope.entitySearchResult.list.supplierblacklist.Source ? 1 : 0)
				});
   		   }
   		   if($scope.entitySearchResult.list['imsasllc_offenses'].length > 0){
   			   riskPieData.push({
					"key":"Offenses",
					"doc_count":$scope.entitySearchResult.list['imsasllc_offenses'].length
			   });
		   }
   		   if($scope.entitySearchResult.list['news_deep_web'].length > 0){
   			   riskPieData.push({
					"key":"Dark Web",
					"doc_count":$scope.entitySearchResult.list['news_deep_web'].length
			   });
		   }
		    if($scope.entitySearchResult.list['sci_ccc_nashville_gov'].length > 0){
   			   riskPieData.push({
					"key":"Offenses",
					"doc_count":$scope.entitySearchResult.list['sci_ccc_nashville_gov'].length
			   });
		   }
	   	   
		   if(riskPieData.length > 0){
				$scope.InitializeandPlotPie(riskPieData, 'pieChartRiskRatio',colorsObj);
		   } else{
		   	$scope.entitySearchResult.data_not_found.risk_ratio_data =false;
		   }
   		}
		// ---------------------------------------Alert tab charts ends-----------------------------------------------------------//

		// ---------------------------------------political donation charts ends-----------------------------------------------------------//
		function PlotPoliticalChart(){
		 if($("#politicaldonations").find("svg").length == 0){
			 $("#politicaldonations").empty();
			 var data = [];
			 var all_dts=[],politicians_dt=[],orgs_dt=[],finan_dt=[];
			var nested_data_politicians = d3.nest()
				 .key(function(d){ return new Date(d.date).getFullYear();})
				 .rollup(function(d) { return  d3.sum(d, function(d) {return parseFloat(d.amount);}) })
				 .entries($scope.entitySearchResult['list']['associated_companies']['politicians'])
			 var nested_data_orgs = d3.nest()
				 .key(function(d){ return new Date(d.date).getFullYear();})
				 .rollup(function(d) { return  d3.sum(d, function(d) {return parseFloat(d.amount);}) })
			   	 .entries($scope.entitySearchResult['list']['associated_companies']['political_organizations']);
			 var politicians =[]; var politiciansOrgs=[];
			 angular.forEach(nested_data_politicians,function(d,i){
				 all_dts.push(d.key);
				 politicians_dt.push(d.key)
				 politicians.push({
					 "time":d.key,
					 "y":d.value,
					 type:"Politician"
				 })
			 });
			 angular.forEach(nested_data_orgs,function(d,i){
				 all_dts.push(d.key);
				 orgs_dt.push(d.key)
				 politiciansOrgs.push({
					 "time":d.key,
					 "y":d.value,
					 type:"Political organization"
				 })
			 });
			 //should have equal keys for stack chart
			 angular.forEach(all_dts,function(dt,k){
				if( $.inArray(dt,politicians_dt) == -1){
					politicians.push({
						"time":dt,
						"y":0,
						type:"Politician"
					})
				}
				if( $.inArray(dt,orgs_dt) == -1){
					politiciansOrgs.push({
						"time":dt,
						"y":0,
						type:"Political organization"
					})
				}

			 });
			 politicians.sort(function(a,b){
				 return parseInt(a.time)-parseInt(b.time);
			 })
			  politiciansOrgs.sort(function(a,b){
				  return parseInt(a.time)-parseInt(b.time);
			 })
			  $("#politicaldonations").append("<div class=\"right-section-item alert-message-wrapper\"><div class=\"alert-message loading-message\">Loading...</div></div>")
			 if((politicians && politicians.length>0) || (politiciansOrgs && politiciansOrgs.length>0) ){
				 data[0] = politicians;
				 data[1] = politiciansOrgs;

				 var options={
			         		container:"#politicaldonations",
			         		width:$("#politicaldonations").width(),
			         		height:"200",
			         		data:data,
			         		marginTop:0,
			         		marginBottom:20,
			         		marginRight:0,
			         		marginLeft:5,
			         		tickColor:"rgb(51, 69, 81)"
					}
				 stackedbarTimelinechart(options);
			 }
			 else{
				 $("#politicaldonations").html("<div class=\"right-section-item alert-message-wrapper\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>")
			 }
		 }
		 //plot pie chart

		 if($("#politicalOrgs").find("svg").length == 0){
			$("#politicalOrgs").append("<div class=\"right-section-item alert-message-wrapper\"><div class=\"alert-message loading-message\">Loading...</div></div>")
			 $("#politicalOrgs").empty();
			 var nested_data = d3.nest().key(function(d){return d.org_name}).rollup(function(d) { return  d3.sum(d, function(d) {return parseFloat(d.amount);}) }).entries($scope.entitySearchResult['list']['associated_companies']['political_organizations'])
			 var nested_data1 = d3.nest().key(function(d){return d.recipient_name}).rollup(function(d) { return  d3.sum(d, function(d) {return parseFloat(d.amount);}) })
			 .entries($scope.entitySearchResult['list']['associated_companies']['politicians'])

			 Array.prototype.push.apply(nested_data,nested_data1)
			 var finalPieData = nested_data;
			 if(finalPieData.length>0){
				nested_data.map(function(d){
					return d.doc_count = d.value
				})
				nested_data.sort(function(a,b){
					return b.value-a.value;
				})
				 var colors = ["#E81F0F","#3132FE","#B2CCE7","#5d96c8" , "#b753cd" , "#69ca6b" , "#69ca6b","#c1bd4f" , "#db3f8d" , "#669900","#334433"]

	   	   		$scope.InitializeandPlotPie(nested_data.slice(0,10),"politicalOrgs","",colors)
			}else{
				 $("#politicalOrgs").html("<div class=\"right-section-item alert-message-wrapper\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>")
			};
		 }
	 };
	
	// ---------------------------------------political donation charts ends-----------------------------------------------------------//
	
//	 EntityApiService.getScreeningDetails(data).then(function(subResponse) {
//		 
//	 }, function(error){
//		 
//	 });
	 $scope.corporatePopup = function (source,type,dummy) {
		if(source){
			var data ={};
			if(dummy){
				data.txt = '<table class="table data-custom-table"><tr><td>It is from a scanned source</td></tr></table>';
				
			}else
			if(type==='associateddocument'){
				data.txt = '<table class="table data-custom-table"><tr><th>Source</td><td><a href="https://beta.companieshouse.gov.uk/company/'+ source+'target="_blank">'+'https://beta.companieshouse.gov.uk/company/'+source+'</a></td></tr></tr></table>';
			}
			else{
			data.txt = '<table class="table data-custom-table"><tr><th>Source</td><td><a href="'+source+'" target="_blank">'+source+'</a></td></tr></tr></table>';
			}
		//data.txt = '<table class="table data-custom-table"><tr><th>Source</td><td><a href="https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322" target="_blank">https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322</a></td></tr></tr></table>';

		var dataModal = $uibModal.open({
				templateUrl: '../scripts/common/modal/views/dataPopUp.modal.html',
				controller: 'DataModalController',
				size: 'xs',
				// backdrop: 'static',
				windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',
				resolve: {
					data: function () {
						return data;
					}
				}
			});
			dataModal.result.then(function (response) {}, function (reject) {});
		}
	};
	$("body").on("mouseover", ".showTooltip", function () {
		$(".Screening_new_tooltip").css("display", "block");
	}).on("mousemove", ".showTooltip", function (event) {
		var classlist = $(this).attr("class").split(' ');
		if (classlist[1] == "fa-database") {
			$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-yellow" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">No Financial Crime Found</div>');
		} else if (classlist[1] == "fa-street-view") {
			$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-yellow" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">' + toolTipData.pep_url + '</div>');
		} else if (classlist[1] == "fa-gavel") {
			$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-yellow" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">' + toolTipData.finance_Crime_url + '</div>');
		} else if (classlist[1] == "fa-ban") {
			$(".Screening_new_tooltip").html('<div class="row mar-x0" style=""><div class="col-sm-12 pad-x0"><div style="" class="top-heading"><h3 class="text-yellow" style=""><span class="heading-icon"style=""></span>' + toolTipData.name + '</h3></div><div class="secondary-section details-tooltip row" style=""><div class="col-sm-6"><p style=""><span class="fa fa-address-book pull-left"></span>Address <span style=" ">:' + toolTipData.address.address_line_1 + '</span></p> <p style="" ><span class="fa fa-globe pull-left"></span>Country <span style=" ">:' + toolTipData.country + '</span></p> </div><div class="col-sm-6"><p style=""><span class="fa fa-map-marker pull-left"></span>Locality<span style="">:' + toolTipData.address.locality + '</span> </p><p style="" ><span class="fa fa-link pull-left"></span>Source <span >:' + toolTipData.source + '</span> </p> </div></div></div></div></div>')
		} else if (classlist[1] == "fa-globe") {
			if (toolTipData.country && toolTipData.high_risk_jurisdiction) {
				$(".Screening_new_tooltip").html('	<div class="row mar-x0" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-yellow" style=""><span class="heading-icon" style=""></span>' + toolTipData.name + '</h3></div><div class="secondary-section row" style=""><div class="col-sm-6"><p style="">Jurisdiction<span class="">:' + toolTipData.high_risk_jurisdiction + '</span> </p></div><div class="col-sm-6"> <p style="" >Country  <span class="">:' + toolTipData.country + '</span></p></div></div></div></div></div>')
			} else {
				$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-yellow" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase"> Risk Jurisdiction Not Found</div>');
			}
		}
		var p = $(".entity-page")
		var position = p.offset();
		var windowWidth = window.innerWidth;
		var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		var cursor = event.pageX;
		if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX - 30 - $(".Screening_new_tooltip").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX)  + "px");
		}
		return $(".Screening_new_tooltip").css("top", event.pageY + "px")
	}).on("mouseout", ".showTooltip", function () {
		$(".Screening_new_tooltip").css("display", "none");
	})
	var div = d3.select("body").append("div").attr("class", "Screening_new_tooltip").style("position", "absolute").style("width", "auto").style("width", "auto").style("min-width", "300px").style("background", "rgb(27, 39, 53)").style("padding", "10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none");
	
	$scope.mouseover = function (sourceLink, classstype,secondary) {
		$scope.myStyle2 = {
			cursor: 'pointer'
		};
		onHoverShow(sourceLink, classstype,secondary);
	}
	function onHoverShow(sourceLink, classstype,secondary) {
		if(!secondary)
		secondary ='PRIMARY';
		
		$("body")
			.on("mouseover", '.' + classstype, function () {
				$(".Screening_new_tooltip").css("display", "block");
			})
			.on("mousemove",'.' + classstype,function (event) {
			var classlist = $(this).attr("class").split(' ');
			if (classstype == "associatedDocTooltip") {
				$(".Screening_new_tooltip")
					.html(
						'<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4 style=" " class="text-yellow">'+secondary+' Link</h4></div><a style="">https://beta.companieshouse.gov.uk/company/' +sourceLink +'</a></div></div><div class="secondary-section"></div></div></div>');
		 }else {
				$(".Screening_new_tooltip")
				.html(
					'<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4 style=" " class="text-yellow">'+secondary+' Link</h4></div><a style="text-transform: capitalize" >'+sourceLink+'</a></div></div><div class="secondary-section"></div></div></div>');
		 }
		 var p = $(".entity-page")
		 var position = p.offset();
		 var windowWidth = window.innerWidth;
		 var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		 var cursor = event.pageX;
		 if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			 var element = document.getElementsByClassName("Screening_new_tooltip");
			 for (var i = 0; i < element.length; i++) {
				 element[i].classList.remove("tooltip-left");
				 element[i].classList.add("tooltip-right");
			 }
			 $(".Screening_new_tooltip").css("left", (event.pageX - 30 - $(".Screening_new_tooltip").width()) + "px");
		 } else {
			 var element = document.getElementsByClassName("Screening_new_tooltip");
			 for (var i = 0; i < element.length; i++) {
				 element[i].classList.remove("tooltip-right");
				 element[i].classList.add("tooltip-left");
			 }
			 $(".Screening_new_tooltip").css("left", (event.pageX) + 10+"px");
		 }
		 return $(".Screening_new_tooltip").css("top", event.pageY + "px")
		}).on("mouseout", '.' + classstype, function () {
		$(".Screening_new_tooltip").css("display", "none");
	})
	}
	var toolTipData = {};
	$scope.tooltipdtata = function (item) {
		toolTipData = {
			address: {
				address_line_1: '',
				address_line_2: '',
				locality: ''
			},
			country: '',
			high_risk_jurisdiction: '',
			source: '',
			pep_url: '',
			finance_Crime_url:''
		};
		toolTipData.country = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].country ? item["mdaas:RegisteredAddress"].country : '') : '';
		toolTipData.address.address_line_1 = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].streetAddress ? item["mdaas:RegisteredAddress"].streetAddress : '') : '';
		toolTipData.address.locality = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].city ? item["mdaas:RegisteredAddress"].city : '') : '';
		toolTipData.name = item.name === undefined ? '' : item.name;
		toolTipData.high_risk_jurisdiction = item.high_risk_jurisdiction ? item.high_risk_jurisdiction : '';
		toolTipData.source = item.sanction_url ? item.sanction_url : (item.sanction_source ? item.sanction_source : (item["@source-id"] ? item["@source-id"] : ''));
		toolTipData.pep_url = item.pep_url ? item.pep_url : 'Not Found In Pep List';
		toolTipData.finance_Crime_url = item.finance_Crime_url ? item.finance_Crime_url : 'Finance Crime Not Found';
		
	}
	$scope.openmodal = function (data, type, index) {
		data.source = data.sanction_url	? data.sanction_url	:(data.sanction_source ? data.sanction_source : (data["@source-id"]? data["@source-id"]:''));
		if (type === 'sanction' &&  data.sanction_url) {
			//data.country = data.country === undefined ? data.addresses[0].country : data.country;
			data.country = data["mdaas:RegisteredAddress"] ? (data["mdaas:RegisteredAddress"].country ? data["mdaas:RegisteredAddress"].country : '') : '';
			data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Source</td><td><a href="' + data.source + '" target="_blank">' + data.source + '</a></td></tr></tr><tr><th>Country</td><td>' + data.country + '</td></tr></table>';
			openmodal(data);
		} else if (type === "high_risk_jurisdiction" && data.high_risk_jurisdiction) {
			data.high_risk_jurisdiction = data.high_risk_jurisdiction ? data.high_risk_jurisdiction : '';
			//data.country = data.country === undefined ? data.addresses[0].country : data.country;
			data.country = data["mdaas:RegisteredAddress"] ? (data["mdaas:RegisteredAddress"].country ? data["mdaas:RegisteredAddress"].country : '') : '';
			if(data.high_risk_jurisdiction &&  data.country){
				data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Risk Jurisdiction</td><td>' + data.high_risk_jurisdiction + '</td></tr></tr><tr><th>Country</td><td>' + data.country + '</td></tr></table>';
			}
			openmodal(data);
		}
		else if (type === "pepalert" && data.pep_url) {
			data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Source</td><td><a href="'+ data.pep_url +'" target="_blank">' + data.pep_url + '</td></tr></tr></table>';
			openmodal(data);
		}
		else if (type === "financeCrime" && data.finance_Crime_url) {
			data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Source</td><td><a href="'+ data.finance_Crime_url +'" target="_blank">' + data.finance_Crime_url + '</td></tr></tr></table>';
			openmodal(data);
		}
		//window.popUpdata(item, type);
	}
	$window.popUpdata = function (data, type) {
		openmodal(data);
	}

	function openmodal(data) {
		var dataModal = $uibModal.open({
			templateUrl: '../scripts/common/modal/views/dataPopUp.modal.html',
			controller: 'DataModalController',
			size: 'xs',
			// backdrop: 'static',
			windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		dataModal.result.then(function (response) {}, function (reject) {});
	};
}