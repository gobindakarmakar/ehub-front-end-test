'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyFinancialGrowthNewController', entityCompanyFinancialGrowthNewController);
entityCompanyFinancialGrowthNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyFinancialGrowthNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
		var bloombergChartDetailsOptions  = EntityCommonTabService.basicsharedObject.bloombergChartDetailsOptions;
		$scope.readmoreModal = {
			balanceSheetModal: balanceSheetModal,
			incomeSheetModal: incomeSheetModal,
			cashflowSheetModal: cashflowSheetModal,
			financeModal: financeModal
		};
	var finacialIncomeStatementData = [];
	var finacialBalanceSheetData = [];
	var localBlncSheet = []
	$scope.$on('financialGrowthOnload', function (e) {
		if (EntityCommonTabService.lazyLoadingentity.financeTabFirsttime) {
			var url = {
				identifier: encodeURIComponent($stateParams.query),
				type: 'finance_info',
				jurisdiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : '',
			};
			linkcall(url, 'finance_info', false);
			financepgraph();
			EntityCommonTabService.lazyLoadingentity.financeTabFirsttime =false;
		} else {
			loadFinacialWidget(EntityCommonTabService.entityChartSharedObject.financeData);
			if (EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphFinancialGrowth"] && EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphFinancialGrowth"].vertices.length > 0) {
				EntityCommonTabService.loadDataAndPlotGraphForEntity({
					"data": EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphFinancialGrowth"],
					"id": "networkGraphFinancialGrowth"
				});
			}
		}

	});

	function linkcall(url, type, json) {
		EntityApiService.complianceFieldsUrl(url, json).then(function (response) {
			if (response) {
				if (type == "finance_info") { //this section is for financial Information  section 
					if (response && response.data && response.data.results && response.data.results.length > 0 && response.data.results[0].finance_info) {
						var value = (response.data.results[0].finance_info.Finanace_Information) ? response.data.results[0].finance_info.Finanace_Information : {};
						value.shareholders = response.data.results[0].finance_info.shareholders ? response.data.results[0].finance_info.shareholders : [];
						var data = [{
							value: [value]
						}];
						EntityCommonTabService.entityChartSharedObject.financeData = data;						
						loadFinacialWidget(data);
						EntityCommonTabService.lazyLoadingentity.financeTabFirsttime = false;
					}
				} //fainance  ends
			}
		}, function (response) {//if API fails

		});
	}
	function balanceSheetModal() {
		var balanceSheetModalInstance = $uibModal.open({
			templateUrl: './modal/view/balance-sheet-modal.html',
			controller: 'BalanceSheetModalController',
			windowClass: 'custom-modal bst_modal c-arrow related-person-modal entity-visualiser-modal balance-sheet-modal',
			resolve: {
				yahooStockData: function () {
					return yahooStockData;
				},
				stockModalData: function () {
					return stockModalData;
				}
			}
		});
		balanceSheetModalInstance.result.then(function (response) {
		}, function (reject) {
		});
	}
	function incomeSheetModal() {
		var incomeSheetModalInstance = $uibModal.open({
			templateUrl: './modal/view/income-sheet-modal.html',
			controller: 'IncomeSheetModalController',
			windowClass: 'custom-modal bst_modal c-arrow related-person-modal entity-visualiser-modal income-sheet-modal',
			resolve: {
				yahooStockData: function () {
					return yahooStockData;
				},
				stockModalData: function () {
					return stockModalData;
				}
			}
		});
		incomeSheetModalInstance.result.then(function (response) {
		}, function (reject) {
		});
	}
	function cashflowSheetModal() {
		var cashflowSheetModalInstance = $uibModal.open({
			templateUrl: './modal/view/cashflow-sheet-modal.html',
			controller: 'CashFlowSheetModalController',
			windowClass: 'custom-modal bst_modal  c-arrow related-person-modal entity-visualiser-modal cashflow-sheet-modal',
			resolve: {
				yahooStockData: function () {
					return yahooStockData;
				},
				stockModalData: function () {
					return stockModalData;
				}
			}
		});
		cashflowSheetModalInstance.result.then(function (response) {
		}, function (reject) {
		});
	}
	$scope.onChangeBloombergDetailsKey = function (bloombergDataKey) {
		$scope.bloombergSelectedTab = bloombergDataKey;
		$('#bloomberg-line-chart-details').empty();
		bloombergChartDetailsOptions.data = bloombergStockLineChartData[bloombergDataKey];
		if (bloombergChartDetailsOptions.data.length > 0) {
			$scope.entitySearchResult.is_data_not_found.stockprice_div = true;
		} else {
			$scope.entitySearchResult.is_data_not_found.stockprice_div = false;
			$scope.entitySearchResult.is_data_not_found.is_stock_performance = false;
		}
		loadlineData(bloombergChartDetailsOptions);
	};
	function financeModal() {
		var financeModalInstance = $uibModal.open({
			templateUrl: './modal/view/finance-modal.html',
			controller: 'FinanceModalController',
			windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal entity-visualiser-modal finance-modal-wrapper',
			resolve: {
				yahooStockData: function () {
					return yahooStockData;
				},
				stockModalData: function () {
					return stockModalData;
				},
				entitySearchResult: function () {
					return $scope.entitySearchResult;
				}
			}
		});
		financeModalInstance.result.then(function (response) {
		}, function (reject) {
		});
	}
	/*
* @purpose: Load Finacial Tab widgets 
* @created: 10th Oct 2018
* @params: Finacial data
* @returns: none 
* @author:varsha
*/
	var date = new Date;
	var stockCurrency;
	$scope.topHolder_Enhance = [];
	function loadFinacialWidget(data) {
		if (data && data[0] && data[0]['value'] && data[0]['value'][0]) {
			stockCurrency = data[0]['value'][0]['stock_currency']
			if (data[0]['value'][0]['financial_statements']) {
				if (data[0]['value'][0]['financial_statements']['balance_sheet'] && data[0]['value'][0]['financial_statements']['balance_sheet'].length > 0) {
					$scope.balanceSheet = false;
					loadBalanceSheetWidget(data);
				} else {
					$scope.balanceSheet = true;
					$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
				}
				if (data[0]['value'][0]['financial_statements']['income_statement'] && data[0]['value'][0]['financial_statements']['income_statement'].length > 0) {
					$scope.incomeSheet = false;
					loadIncomeStatementWidget(data);
				} else {
					$scope.incomeSheet = true;
					$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
				}
				if (data[0]['value'][0]['financial_statements']['cash_flow_statement'] && data[0]['value'][0]['financial_statements']['cash_flow_statement'].length > 0) {
					$scope.netCashSheet = true;
					loadCashFlowWidget(data);
				} else {
					$scope.netCashSheet = true;
					$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
				}
			} else {
				$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
				$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
				$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
				$scope.entitySearchResult.is_data_not_found.is_topholders = false;
			}
			/* 	if(data[0]['value'][0]['shareholders'] && data[0]['value'][0]['shareholders'].length >0){
					$scope.shareHolderTypeStatus = false;
					$scope.topHolder_Enhance = data[0]['value'][0]['shareholders'];
				loadShareHolderTypePie(data[0]['value'][0]['shareholders']);
			} */
			if (data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information.historical_prices && data[0]['value'][0].stocks_information.historical_prices.length > 0) {
				loadStockHistoryWidget(data); 
			} else {
				$scope.dataWithDate = {
					'lastOneYearDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastFiveYearDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastSixMonthDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastOneMonthDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastDay': {
						'close_price': [],
						'open_price': [],
					},
					'lastFiveDay': {
						'close_price': [],
						'open_price': [],
					},
				}
				$scope.entitySearchResult.is_data_not_found.is_stockHistory = false;
			}
			if (data[0]['value'][0]['shareholders']) {
				if (data[0]['value'][0]['shareholders'].length > 0) {
					$scope.shareHolderTypeStatus = false;
					$scope.topHolder_Enhance = data[0]['value'][0]['shareholders'];
					loadShareHolderTypePie(data[0]['value'][0]['shareholders']);
				} else {
					$scope.shareHolderTypeStatus = true;
					$scope.entitySearchResult.is_data_not_found.is_topholders = false;
					$scope.entitySearchResult.is_data_not_found.shareholder_div = false;
				}
			} else {
				$scope.shareHolderTypeStatus = true;
				$scope.entitySearchResult.is_data_not_found.shareholder_div = false;
				$scope.entitySearchResult.is_data_not_found.is_topholders = false;
			}
			if (data[0]['value'][0]['price_earning_ratio'] && data[0]['value'][0]['price_earning_ratio'].length > 0) {
				$scope.revenueTypeStatus = false;
				loadRevenueVsYear(data[0]['value'][0]['price_earning_ratio']);
			} else {
				$scope.revenueTypeStatus = true;
			}
			if (data[0]['value'][0]['summary']) {
				$scope.financialHeaderData = [];
				$scope.financialHeaderData.push(data[0]['value'][0]['summary'])
			} else {
			}
			$scope.financialCurrentStatus = [];
			if (data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information.current) {
				$scope.financialCurrentStatus.push(data[0]['value'][0].stocks_information.current)
			} else {
			}
			$scope.stockData = {}
			if (data[0]['value'][0]) {
				if (data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information) {
					$scope.stockData['stock_id'] = data[0]['value'][0].stocks_information.stock_id;
					$scope.stockData['stock_name'] = data[0]['value'][0].stocks_information.stock_name;
				}
				$scope.stockData['capitalization'] = data[0]['value'][0]['capitalization'];
			}
		}
	}
	/*
* @purpose: Load Finacial Tab  Balance sheet widgets 
* @created: 10th Oct 2018
* @params: Finacial data
* @returns: none 
* @author:varsha
*/
	
	function loadBalanceSheetWidget(data) {
		finacialBalanceSheetData = data[0]['value'][0]['financial_statements']['balance_sheet'];
		data[0]['value'][0]['financial_statements']['balance_sheet'].map(function (d) {
			if (localBlncSheet.indexOf(d.section) == -1) {
				localBlncSheet.push(d.section)
			}
		})
		localBlncSheet.map(function (d) {
			var balance = {};
			$scope.newbalanceSheet = [];
			angular.forEach(finacialBalanceSheetData, function (val, key) {
				if (val.section == d && val.calendar_year <= date.getFullYear() && val.line_item_amount) {
					balance['date'] = val.calendar_year;
					balance['sum'] = val.line_item_amount
					$scope.newbalanceSheet.push(balance);
					balance = {};
				}
			});
			if ($scope.newbalanceSheet && $scope.newbalanceSheet.length > 1) {
				$scope.balance_sheet.push(d)
			} else {
				$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
			}
		})
		$scope.selectedbalanceStatement = $scope.balance_sheet[0];
		$scope.onchangebalance_Enhance($scope.selectedbalanceStatement);
	}

	/*
 * @purpose: On Change of Balance sheet select Box
 * @created: 10th Oct 2018
 * @params: Selected value
 * @returns: none 
 * @author:varsha
 */
	$scope.onchangebalance_Enhance = function (selectedbalanceStatement) {
		var balance = {};
		$scope.newbalanceSheet = [];
		angular.forEach(finacialBalanceSheetData, function (val, key) {
			if (val.section == selectedbalanceStatement && val.calendar_year <= date.getFullYear() && val.line_item_amount) {
				if (val.line_item_amount !== "—") {
					balance['date'] = val.calendar_year && val.calendar_year.replace(/,/g, '') ? parseFloat(val.calendar_year.replace(/,/g, '')) : '';
					balance['sum'] = val.line_item_amount && typeof val.line_item_amount === 'string' ? parseFloat(val.line_item_amount.replace(/,/g, '')) : (typeof val.line_item_amount === 'number') ? val.line_item_amount : ''
					$scope.newbalanceSheet.push(balance);
					balance = {};
				}
			}
		});
		if ($scope.newbalanceSheet.length > 0) {
			/* calling $scope.newbalanceSheet line chart */
			$timeout(function () {
				EntityCommonTabService.plotLineChart($scope.newbalanceSheet, "#lineChartBalanceSheet", '', '200', stockCurrency);
			},100)
		} else {
			$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
		}
	}
	/*
* @purpose: Load Finacial Tab  Income statement widgets 
* @created: 10th Oct 2018
* @params: Finacial data
* @returns: none 
* @author:varsha
*/
	function loadIncomeStatementWidget(data) {
		finacialIncomeStatementData = data[0]['value'][0]['financial_statements']['income_statement'];
		data[0]['value'][0]['financial_statements']['income_statement'].map(function (d) {
			if ($scope.income_statement.indexOf(d.section) == -1) {
				$scope.income_statement.push(d.section)
			}
		})
		$scope.selectedincomeStatement = $scope.income_statement[0];
		$scope.onchangeincome_Enhance($scope.selectedincomeStatement);
	}
	/*
 * @purpose: On Change of Income Statement select Box
 * @created: 10th Oct 2018
 * @params: Selected value
 * @returns: none 
 * @author:varsha
 */
	$scope.onchangeincome_Enhance = function (selectedincomeStatement, revenue) {
		var income = {};
		$scope.newIncomeSheet = [];
		angular.forEach(finacialIncomeStatementData, function (val, key) {
			if (val.section == selectedincomeStatement && parseInt(val.year) <= date.getFullYear() && val.line_item_amount) {
				if (val.line_item_amount !== "—") {
					income['date'] = parseInt(val.year);
					income['sum'] = parseInt(val.line_item_amount)//value is chhanged to line_item_amount
					$scope.newIncomeSheet.push(income);
					income = {};
				}
			}
		});
		/* calling Income statement line chart */
		if ($scope.newIncomeSheet.length > 0) {
			$timeout(function () {
				EntityCommonTabService.plotLineChart($scope.newIncomeSheet, "#lineChartIncomeStatement", $("#lineChartIncomeStatement").width(), '200', stockCurrency);
			},100)
		} else {
			$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
		}
	}
	/*
	 * @purpose: Load Finacial Tab Net Cash Flow widgets 
	 * @created: 10th Oct 2018
	 * @params: Finacial data
	 * @returns: none 
	 * @author:varsha
	 */
	var finacialCashFlowData = [];
	function loadCashFlowWidget(data) {
		finacialCashFlowData = data[0]['value'][0]['financial_statements']['cash_flow_statement'];
		data[0]['value'][0]['financial_statements']['cash_flow_statement'].map(function (d) {
			if (!$scope.cash_flow_statement.includes(d.section)) {
				$scope.cash_flow_statement.push(d.section)
			}
		})
		if ($scope.cash_flow_statement.length === 0) {
			$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
		}
		$scope.selectedcashflowStatement = $scope.cash_flow_statement[0];
		$scope.onchangecashflow_Enhance($scope.selectedcashflowStatement);
	}
	/*
	 * @purpose: On Change of Cash Flow select Box
	 * @created: 10th Oct 2018
	 * @params: Selected value
	 * @returns: none 
	 * @author:varsha
	 */
	$scope.onchangecashflow_Enhance = function (selectedcashflowStatement, investment) {
		var cashflow = {};
		$scope.newcashflowSheet_Enhance = [];
		angular.forEach(finacialCashFlowData, function (val, key) {
			if (val.section == selectedcashflowStatement && val.calendar_year <= date.getFullYear() && val.line_item_amount) {
				if (val.line_item_amount !== "—") {
					cashflow['date'] = val.calendar_year;
					cashflow['sum'] = val.line_item_amount
					$scope.newcashflowSheet_Enhance.push(cashflow);
					cashflow = {};
				}
			}
		});
		if ($scope.newcashflowSheet_Enhance.length > 0) {
			/* calling Cash statement line chart */
			$timeout(function () {
				EntityCommonTabService.plotLineChart($scope.newcashflowSheet_Enhance, "#lineChartCashFlow_Enhance", $("#lineChartCashFlow_Enhance").width(), '180', stockCurrency);
			},100)
				
		}
	};
	var revenueDataSheet_Enhance = [];
	function loadRevenueVsYear(data) {
		data.map(function (d) {
			d.date = d.year;
			d.sum = d.value;
		})
		revenueDataSheet_Enhance = data
	}
	var shareHolderTypePieData = [];
	var shareHolderTypePieDataColorObj = [];
	var colorList = ["#a75dc2", "#3790ce", "#399034"]
	function loadShareHolderTypePie(data) {
		shareHolderTypePieData = [];
		shareHolderTypePieDataColorObj = [];
		var nestedData = d3.nest()
			.key(function (d) { return d.holder_type; })
			.entries(data);
		nestedData = nestedData.filter(function (d) {
			return d.key !== "undefined";
		})
		nestedData.map(function (d, k1) {
			shareHolderTypePieData.push({
				'key': d.key,
				'doc_count': d.values.length
			})
			shareHolderTypePieDataColorObj[d.key] = colorList[k1];
		})
		if (shareHolderTypePieData.length == 0) {
			$scope.entitySearchResult.is_data_not_found.shareholder_div = false;
			$scope.shareHolderTypeStatus = true;
		}
		/* calling financial growth line chart */
		var colorsObj = {
			"company": "#a75dc2",
			"person": "#3790ce",
			"institute": "#399034",
		}
		/* calling investment trends pie chart */
		var find = _.every(shareHolderTypePieData, {
			doc_count: NaN
		});
		if (shareHolderTypePieData.length > 0 && !find) {
			$scope.entitySearchResult.is_data_not_found.shareholder_div = true;
			EntityCommonTabService.InitializeandPlotPie(shareHolderTypePieData, 'pieChartInvestment', shareHolderTypePieDataColorObj);
		} else {
			$scope.entitySearchResult.is_data_not_found.shareholder_div = false;
			$scope.entitySearchResult.is_data_not_found.is_shareholder = false;
		}
	}
	/*
	 * @purpose: Loads only on click of finance  tab
	 * 
	 */
	function financepgraph() {
		var data = {
			"fetchers": ["1008", "1006", "1013", "1021"],
			// "fetchers": ["32", "34", "35"],
			"keyword": $scope.entitySearchResult.name ? $scope.entitySearchResult.name : EntityCommonTabService.basicsharedObject.companyName,
			"searchType": "Company",
			"lightWeight": true,
			"limit": 2,
			"iterations": 1,
			"alias": "Stock",
			"create_new_graph": false,
			"requires_expansion": true,
			"entity_resolution": true,
			"expansion_fetchers_lightweight": true,
			"expansion_fetchers": ["1005", "1009", "1010", "1011", "1012"]
		};
		EntityCommonTabService.loadoverviewVLA("networkGraphFinancialGrowth", data, EntityCommonTabService.entityChartSharedObject.vlaDataArr);
	}
		/*
		* @purpose: Load Stock History Widget
		* @created: 12th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
		var presentDate;
		var dateDivisionObj;
		var dataWithDate = {
			'lastOneYearDate': {
				'close_price': [],
				'open_price': [],
			},
			'lastFiveYearDate': {
				'close_price': [],
				'open_price': [],
			},
			'lastSixMonthDate': {
				'close_price': [],
				'open_price': [],
			},
			'lastOneMonthDate': {
				'close_price': [],
				'open_price': [],
			},
			'lastDay': {
				'close_price': [],
				'open_price': [],
			},
			'lastFiveDay': {
				'close_price': [],
				'open_price': [],
			},
		}
		function loadStockHistoryWidget(data) {
			presentDate = moment()._d;
			dateDivisionObj = {
				'lastOneYearDate': moment(presentDate).subtract(1, 'year')._d,
				'lastFiveYearDate': moment(presentDate).subtract(5, 'year')._d,
				'lastSixMonthDate': moment(presentDate).subtract(6, 'month')._d,
				'lastOneMonthDate': moment(presentDate).subtract(1, 'month')._d,
				'lastDay': moment().add(-1, 'days')._d,
				'lastFiveDay': moment().add(-5, 'days')._d
			}
			arrangeDataByMeasure(data, 'close_price');
			arrangeDataByMeasure(data, 'open_price');
			$scope.dataWithDate = dataWithDate;
			EntityCommonTabService.entityChartSharedObject.dataWithDate = dataWithDate;

		}
			/*
		* @purpose: Arrange data array by different measure
		* @created: 15th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
	function arrangeDataByMeasure(data, measure) {
		data[0]['value'][0].stocks_information.historical_prices.map(function (d) {
			var dataDate = new Date(d.data_date);
			var eachData = jQuery.extend(true, {}, d);
			eachData.value = d[measure];
			eachData.year = dataDate;
			eachData.currency = stockCurrency;
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastFiveYearDate']) {
				dataWithDate['lastFiveYearDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastOneYearDate']) {
				dataWithDate['lastOneYearDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastSixMonthDate']) {
				dataWithDate['lastSixMonthDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastOneMonthDate']) {
				dataWithDate['lastOneMonthDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastDay']) {
				dataWithDate['lastDay'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastFiveDay']) {
				dataWithDate['lastFiveDay'][measure].push(eachData);
			}

		})
		if (measure == 'open_price') {
			setTimeout(function () {
				$scope.onChangestockhistoryKey_Enhance('lastFiveYearDate', 'close_price')
			}, 3000)
		}
	}

	/*
		* @purpose: Load Stock History Widget On Change of selected option
		* @created: 12th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
		$scope.stockHistorySelectedTab_Enhance = 'lastFiveYearDate';
		$scope.stock_measures_Enhance = ['open_price', 'close_price']
		$scope.selectedstockMeasure = 'close_price'
		$scope.onChangestockhistoryKey_Enhance = function (selectedType, selectedstockMeasure) {
			$scope.stockHistorySelectedTab_Enhance = selectedType;
			dataWithDate = EntityCommonTabService.entityChartSharedObject.dataWithDate;
			if (dataWithDate && dataWithDate[selectedType] && dataWithDate[selectedType][selectedstockMeasure] && dataWithDate[selectedType][selectedstockMeasure].length >= 1) {
				setTimeout(function () {
					if ($("#stockhistory-line-chart_Enhance").width() > 0) {
						$('#stockhistory-line-chart_Enhance').empty();
						var stockhistoryOptions = {
							container: "#stockhistory-line-chart_Enhance",
							height: '200',
							width: $("#stockhistory-line-chart_Enhance").width(),
							uri: "../vendor/data/bloombergdata.json",
							data: dataWithDate[selectedType][selectedstockMeasure]
						};
						loadlineData(stockhistoryOptions);
					}
					if ($("#stockhistory-line-chart_Enhance_overview").width() > 0) {
						$('#stockhistory-line-chart_Enhance_overview').empty();
						var stockhistoryOptions = {
							container: "#stockhistory-line-chart_Enhance_overview",
							height: '240',
							width: $("#stockhistory-line-chart_Enhance_overview").width(),
							uri: "../vendor/data/bloombergdata.json",
							data: dataWithDate[selectedType][selectedstockMeasure]
						};
						loadlineData(stockhistoryOptions);
					}
				}, 0)
			} else {
				return false;
			}
		}
		// -------------------------bloomberg chart
	// line----------------------------------
	function loadlineData(data) {
		var current_options = data;
		if (current_options && current_options.data && current_options.data.length > 0) {
			EntityGraphService.bloombergLineChart(current_options);
		}
	}
}