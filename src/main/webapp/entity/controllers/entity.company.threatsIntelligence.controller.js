'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyAutodeskNewController', entityCompanyAutodeskNewController);
entityCompanyAutodeskNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyAutodeskNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
	var threatsRadarData = [], attacksData = [], attacksDataForCompany = [];
	var attackAggregates = [];
	$scope.threatsRadarList = [];
	$scope.checkRadarParams ={};
	$scope.checkOptions = {
		'peerIndustry': false,
		'peerTechnology': false,
		'peerLocation': false,
		'peerLegalType': false
	};
	var radarParams = {
		"country-code": {
			"terms_set": [""],
			"minimum_should_match": 0.2
		},
		"industry": {
			"terms_set": [""],
			"minimum_should_match": 0.2
		},
		"technology": {
			"terms_set": [""],
			"minimum_should_match": 0.2
		}
	};
	$scope.threatIntelligenceTabData = {
		'newsList': [],
		'newsListLength': 0,
		'trendsList': [],
		'trendsListLength': 0,
		'industriesList': [],
		'filteredIndustries': [],
		'topCountriesList': [],
		'topFilteredCountriesList': [],
		'technologiesList': [],
		'filteredTechnologies': [],
		'tagCloudTechnologiesList': [],
		'newsListLoader': true,
		'trendLoader': true,
		'classificatinLoader': true,
		'worldMapLoader': true,
		'industriesLoader': true,
		'incidentsTableLoader': true,
		'topCountriesLoader': true,
		'topTechnologiesLoader': true,
		'tagCloudLoader': true,
		'radarLoader': true,
		'heatMapLoader': true,
		'timelineData': [],
		'attackTypesList': [],
		'classificationList': []
	};
	var countriesData = [], technologiesData = [], threatMarkersList = [];
	var todayDt = new Date();
	var lastYr = new Date();
	lastYr = lastYr.setFullYear(lastYr.getFullYear() - 1);
	var lastSixYrs = new Date();
	lastSixYrs = lastSixYrs.setFullYear(lastSixYrs.getFullYear() - 6);
	var yesterdaydt = new Date();
	yesterdaydt = yesterdaydt.setDate(yesterdaydt.getDate() - 1);
	var lastMonth = new Date();
	lastMonth = lastMonth.setMonth(lastMonth.getMonth() - 1);
	var lastMonthFirstDay = new Date(lastMonth);
	lastMonthFirstDay = lastMonthFirstDay.setDate(1);
	var lastThirteenMonths = new Date();
	lastThirteenMonths = lastThirteenMonths.setMonth(lastThirteenMonths.getMonth() - 13);
	var lastYrFromDate = new Date(lastYr);
	var fromMonth = String(lastYrFromDate.getMonth() + 1);
	var fromDay = String(lastYrFromDate.getDate());
	var toMonth = String(todayDt.getMonth() + 1);
	var toDay = String(todayDt.getDate());
	if (fromMonth.length < 2) fromMonth = '0' + fromMonth;
	if (fromDay.length < 2) fromDay = '0' + fromDay;
	if (toMonth.length < 2) toMonth = '0' + toMonth;
	if (toDay.length < 2) toDay = '0' + toDay;
	var fromDate = lastYrFromDate.getFullYear() + '-' + fromMonth + '-' + fromDay;
	var toDate = todayDt.getFullYear() + '-' + toMonth + '-' + toDay;
	var countriesListByCodes = CountryCodesConst.CountryCodesArr;
	var osintCountry, osintIndustry, osintName, osintWebsite, osintCity, osintTel, osintEmail, osintAddress;
	var companyWebsiteUrl;
	var threatsClassificatinoObj;
	var vlaIdentifier;

	$scope.$on('threatsIntelligence', function () {
		if (EntityCommonTabService.lazyLoadingentity.threatsTabFirsttime) {
			EntityCommonTabService.lazyLoadingentity.threatsTabFirsttime = false;
			threatsIntelligenceTab(EntityCommonTabService.lazyLoadingentity.threatrstabData); //this is for threatsintelligence Tab
		} else {
			setTimeout(function () {
				plotNewsBars();
				plotautoDeskCloudChart();
				//				plotChartforThreatClassification(threatsClassificatinoObj);
				plotRadarChart(threatsRadarData);
				autodeskThreattimeLine($scope.trendName);
				autodeskHeatMap();
				PlotChartForReport(attackAggregates);
				if (filteredMap && filteredMap.length > 0) {
					var domain = d3.extent(threatMarkersList, function (d) {
						return d.incidentsCount
					});
					var worldChartOptions = {
						container: "#threatWorldMap",
						uri1: "../vendor/data/worldCountries.json",
						uri2: "../vendor/data/worldMapData.json",
						height: 180,
						domain: {
							x: domain[0],
							y: domain[1]
						},
						ChangeRadius: 'true',
						opacity: 0.5,
						markers: filteredMap
					};
					EntityCommonTabService.World(worldChartOptions);
				} else {
					var worldChartOptions = {
						container: "#threatWorldMap",
						uri1: "../vendor/data/worldCountries.json",
						uri2: "../vendor/data/worldMapData.json",
						height: 180,
						ChangeRadius: 'true',
						opacity: 0.5,
						markers: []
					};
					EntityCommonTabService.World(worldChartOptions);
				}
				$('#autoDeskVLA').empty();
				if (vlaAutoDeskOptions){
					EntityCommonTabService.loadDataAndPlotGraphForEntity(vlaAutoDeskOptions);
				}
			}, 0);
		}
	});

	/*-----------------------------------------------THREAT INTELLIGENCE TAB STARTS----------------------------------------*/

	/*
			* @purpose: get Adverse News For Table
			* @created: 12 Jul 2018
			* @author: swathi
		 */
	function getInitialData() {
		var data = {
			"date-from": fromDate,
			"date-to": toDate,
			"offset": 1
		}
		var initialNewsData = [];
		$scope.totalNews = [];
		$scope.initialNewsData = [];
		EntityApiService.getAdverseNewsForThreat(data).then(function (resp) {
			initialNewsData = resp.data.hits;
			$scope.totalNews = resp.data.total;
			$scope.initialNewsData = [];
			initialNewsData.map(function (news, i) {
				countriesListByCodes.map(function (d) {
					if (d.iso2Code && news.location && news.location['country-code'] != null && d.iso2Code == news.location['country-code']) {
						news.location['country-name'] = d.name;
					}
				});
				var t = news.published.split('T')[1].split('.')[0].split(':');
				$scope.initialNewsData.push({
					source: news.source,
					text: news.text.length > 50 ? news.text.substring(0, 50) + '...' : news.text,
					date: moment(news.published.split('T')[0]).format('DD/MM/YYYY'),
					time: t[0] + ':' + t[1],
					location: news.location ? news.location['country-name'] : null,
					technologies: news.technologies,
					title: news.title.substring(0, 31),
					newsclass: news.class,
					industry: news.industry
				});
			});
		}).catch();
	};
	getInitialData();
	var tableData = {
		'offset': 1,
		'date-from': fromDate,
		'date-to': toDate,
		'country-code': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'industry': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'technology': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'attack-type': {
			'terms_set': [],
			"minimum_should_match": 0.2
		}
	};
	var newNewsData;
	$scope.newsToDisplay = [];
	$scope.newsTotalLength = undefined;
	function getAdverseNewsForTable(data) {
		$scope.threatIntelligenceTabData.incidentsTableLoader = true;
		if (data['country-code'] && data['country-code']['terms_set'].length == 0)
			delete data['country-code'];
		if (data['industry'] && data['industry']['terms_set'].length == 0)
			delete data['industry'];
		if (data['technology'] && data['technology']['terms_set'].length == 0)
			delete data['technology'];
		if (data['attack-type'] && data['attack-type']['terms_set'].length == 0)
			delete data['attack-type'];
		attackAggregates = [];
		threatsClassificatinoObj = {};
		getNewsList(data);
		getTrendsList(data);
		getThreatsClassificatinList(data);
		getThreatIndustries(data);
		getThreatCountries(data);
		getThreatTechnologies(data);
		getThreatAttackType(data);
		loadDocumentData(vlaIdentifier);
		//		 getRadarThreatAttackType(data);
		$(".remote-filters-wrapper").mThumbnailScroller("scrollTo", 0);
		if (threatMarkersList.length > 0) {
			var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
			var worldChartOptions = {
				container: "#threatWorldMap",
				uri1: "../vendor/data/worldCountries.json",
				uri2: "../vendor/data/worldMapData.json",
				height: 180,
				domain: { x: domain[0], y: domain[1] },
				ChangeRadius: 'true',
				opacity: 0.5,
				markers: []
			};
			World(worldChartOptions);
		}
		$("#bar_1").empty();
		$('#autodeskTimeline').empty();
		$('#tagCloudAutoDeskTags').empty();
		$('#autodeskHeatMap').empty();
		$("#threadsClassificationChart").empty();
		$scope.totalNews = [];
		$scope.tempNews = [];
		var newNewsData;
		EntityApiService.getAdverseNewsForThreat(data).then(function (response) {
			if ($scope.newsTotalLength == undefined) {
				$scope.newsTotalLength = response.data.total;
			}
			newNewsData = response.data.hits;
			$scope.tempNews = [];
			newNewsData.map(function (news, i) {
				countriesListByCodes.map(function (d) {
					if (d.iso2Code && news.location && news.location['country-code'] != null && d.iso2Code == news.location['country-code']) {
						news.location['country-name'] = d.name;
					}
				});
				var t = news.published.split('T')[1].split('.')[0].split(':');
				$scope.tempNews.push({
					source: news.url,
					text: news.text.length > 50 ? news.text.substring(0, 50) + '...' : news.text,
					date: moment(news.published.split('T')[0]).format('DD/MM/YYYY'),
					time: t[0] + ':' + t[1],
					location: news.location ? news.location['country-name'] : null,
					technologies: news.technologies,
					title: news.title.substring(0, 31),
					newsclass: news.class,
					industry: news.industry
				});
			});
			$scope.newsToDisplay = $scope.tempNews;
			//		       	setTimeout(function(){
			//		       		$scope.$apply();
			//		       	}, 0);
			if ($scope.newsToDisplay.length > 0) {
				$("#autoDesk").mCustomScrollbar({
					theme: "minimal-dark"
				});
			};
			$scope.threatIntelligenceTabData.incidentsTableLoader = false;
		}, function (error) {
			$scope.threatIntelligenceTabData.incidentsTableLoader = false;
		});
	}
	/*
		* @purpose: get Adverse News For Threat
		* @created: 12 Jul 2018
		* @author: swathi
	 */
	var threatData = {
		//		'size': 9,
		'offset': 1,
		'date-from': fromDate,
		'date-to': toDate
	};
	function getAdverseNewsForThreat(data) {
		EntityApiService.getAdverseNewsForThreat(data).then(function (response) {
			$scope.threatIntelligenceTabData.incidentsTableLoader = false;
		}, function (failure) {
			$scope.threatIntelligenceTabData.incidentsTableLoader = false;
		});
	}
	getAdverseNewsForThreat(threatData);
	/* 
	 * @purpose: get aggregates For Threat
     * @created: 19th Jul 2018
     * @author: swathi
    */
	/*----------------------------------------------------  news list Starts ----------------------------------------------------*/
	function getNewsList(params) {
		$scope.threatIntelligenceTabData.newsListLoader = true;
		var newsDataArray = [];
		var percentageData = [], yesterdayIncidentAvg = 0;
		$scope.threatIntelligenceTabData.newsList = [];
		$scope.threatIntelligenceTabData.newsListLength = 0;
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "class",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			if (response.data.aggregations[0].buckets.length > 0) {
				angular.forEach(response.data.aggregations[0].buckets, function (v, k) {
					if (v.children[0].buckets.length > 0) {
						angular.forEach(v.children[0].buckets, function (val, key) {
							val.date = val.key;
							val.key = v.key;
							val.count = v.doc_count;
							val.formattedDate = val.date.split('T')[0];
							var currentDt = new Date(val.formattedDate);
							if (currentDt >= lastYrFromDate && currentDt <= todayDt) {
								newsDataArray.push(val);
							}
						});
					};
				});
			}
			$scope.threatIntelligenceTabData.newsList = d3.nest().key(function (d) {
				return d.key;
			}).entries(jQuery.extend(true, [], newsDataArray));
			$scope.threatIntelligenceTabData.newsListLength = Object.keys($scope.threatIntelligenceTabData.newsList).length;
			$scope.threatIntelligenceTabData.newsListLoader = false;
			if ($scope.threatIntelligenceTabData.newsListLength > 0) {
				angular.forEach($scope.threatIntelligenceTabData.newsList, function (v, k) {
					percentageData = [];
					angular.forEach(v.values, function (d, key) {
						var currentDt = new Date(d.formattedDate);
						if (currentDt >= lastYrFromDate && currentDt <= todayDt) {
							percentageData.push({
								"x": d.formattedDate,
								"y": d.doc_count,
								'count': d.doc_count
							});
						}
					});
					var totIncidentsCount = d3.sum(percentageData, function (d) {
						return d.count;
					});
					v.incidentsCount = nFormatter(totIncidentsCount, 2);
					var totIncidentAvg = (totIncidentsCount / 12);
					angular.forEach(percentageData, function (Val, Key) {
						if (new Date(Val.x).toDateString() == new Date(lastMonthFirstDay).toDateString()) {
							yesterdayIncidentAvg = Val.y;
						}
					});
					if (yesterdayIncidentAvg == 0)
						v.incidentAvg = 0;
					else
						v.incidentAvg = (((yesterdayIncidentAvg - totIncidentAvg) / totIncidentAvg) * 100).toFixed(2);
				});
				if ($scope.mainInfoTabType == 'Autodesk') {
					setTimeout(function () {
						plotNewsBars();
					}, 0);
				}
				$("#barsTopRow").mThumbnailScroller({
					axis: "x"
				});
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.newsListLoader = false;
		});
	}
	/*-------------------------------------------------- news list Ends --------------------------------------------------------------*/
	/*----------------------------------------------------  threat trend Starts ----------------------------------------------------*/
	function getTrendsList(params) {
		$scope.threatIntelligenceTabData.trendsList = [];
		$scope.threatIntelligenceTabData.trendsListLength = 0;
		$scope.threatIntelligenceTabData.trendLoader = true;
		var trendsDataArray = [];
		var data = {
			"query": {},
			"agg": {
				"key": "attack-type",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.trendLoader = false;
			if (response.data.aggregations[0].buckets.length > 0) {
				angular.forEach(response.data.aggregations[0].buckets, function (v, k) {
					if (v.children[0].buckets.length > 0) {
						angular.forEach(v.children[0].buckets, function (val, key) {
							val.date = val.key;
							val.key = v.key;
							val.count = v.doc_count;
							val.formattedDate = val.date.split('T')[0];
							trendsDataArray.push(val);
						});
					};
				});
				$scope.threatIntelligenceTabData.trendsList = d3.nest().key(function (d) {
					return d.key;
				}).entries(jQuery.extend(true, [], trendsDataArray));
				$scope.threatIntelligenceTabData.trendsListLength = Object.keys($scope.threatIntelligenceTabData.trendsList).length;
				if ($scope.threatIntelligenceTabData.trendsListLength > 0) {
					if ($scope.mainInfoTabType == 'Autodesk') {
						setTimeout(function () {
							autodeskThreattimeLine($scope.trendName);
						}, 0);
					}
				}
			} else {
				$('#autodeskTimeline').empty();
				$scope.threatIntelligenceTabData.trendsListLength = 0;
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.trendLoader = false;
		});
	}
	/*-------------------------------------------------- threat trend Ends --------------------------------------------------------------*/
	/*---------------------------------------------- threat Incidents classification Starts --------------------------------------------*/

	function getThreatsClassificatinList(params) {
		$scope.IsthreatsClassificatin = false;
		$scope.threatIntelligenceTabData.classificatinLoader = true;
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "attack-type",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.classificatinLoader = false;
			attackAggregates.push(response.data.aggregations[0]);
			if (response.data.aggregations[0].buckets.length > 0) {
				if ($scope.mainInfoTabType == 'Autodesk') {
					PlotChartForReport(attackAggregates);
				}
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.classificatinLoader = false;
			$scope.IsthreatsClassificatin = false;
		});
	}
	/*------------------------------------------------ threat Incidents classification Ends -------------------------------------*/
	/*----------------------------------------------------  Industries Starts ----------------------------------------------------*/
	function getThreatIndustries(params) {
		$scope.threatIntelligenceTabData.industriesLoader = true;
		$scope.threatIntelligenceTabData.industriesList = [];
		$scope.threatIntelligenceTabData.filteredIndustries = [];
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "industry",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.industriesLoader = false;
			$scope.threatIntelligenceTabData.industriesList = response.data.aggregations[0].buckets;
			if ($scope.threatIntelligenceTabData.industriesList.length > 0) {
				var maxIndustriesCount = d3.max($scope.threatIntelligenceTabData.industriesList, function (d) {
					return d.doc_count;
				});
				angular.forEach($scope.threatIntelligenceTabData.industriesList, function (val, key) {
					val.name = val.key;
					angular.forEach(isicCodeConstants, function (codeV, codeK) {
						if (codeK == val.key) {
							val.name = codeV;
							val.label = codeV;
							val.id = codeV;
						}
					});
					if (val.doc_count > 0)
						val.percent = (val.doc_count / maxIndustriesCount) * 100;
					else
						val.percent = 0;
				});
				$scope.threatIntelligenceTabData.industriesList = _.sortBy($scope.threatIntelligenceTabData.industriesList, 'name');
				if (defaultIndustryName && defaultIndustryNameExisted == true) {
					$scope.threatIntelligenceTabData.filteredIndustries = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
						return d.name == defaultIndustryName;
					});
					if ($scope.threatIntelligenceTabData.filteredIndustries.length > 0) {
						previousFilteredIndustry = $scope.threatIntelligenceTabData.filteredIndustries;
						$scope.industryName = $scope.threatIntelligenceTabData.filteredIndustries;
					}
					defaultIndustryNameExisted = false;
				} else {
					$scope.threatIntelligenceTabData.filteredIndustries = $scope.threatIntelligenceTabData.industriesList;
				}
				$("#industriesList").mThumbnailScroller({
					axis: "y",
					theme: "minimal"
				});
			} else {
				$scope.industryName = [];
				defaultIndustryNameExisted = false;
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.industriesLoader = false;
		});
	}
	/*-------------------------------------------------- Industries Ends --------------------------------------------------------------*/
	/*-------------------------------------------------  Top Countries Starts ------------------------------------------------------------------*/
	var topTenCountries = [];
	function getThreatCountries(params) {
		$scope.threatIntelligenceTabData.worldMapLoader = true;
		topTenCountries = [], threatMarkersList = [];
		$scope.threatIntelligenceTabData.topCountriesList = [];
		$scope.threatIntelligenceTabData.topFilteredCountriesList = [];
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "country-code",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.worldMapLoader = false;
			$scope.threatIntelligenceTabData.topCountriesList = response.data.aggregations[0].buckets;
			angular.forEach($scope.threatIntelligenceTabData.topCountriesList, function (v, k) {
				countriesListByCodes.map(function (d) {
					if (d.iso2Code && d.iso2Code == v.key) {
						v.name = d.name;
						v.latitude = d.latitude;
						v.longitude = d.longitude;
						v.label = d.name;
						v.id = d.name;
					}
				});
			});
			var mappedCountries = {
				'name': 'event_location.countryCode',
				'buckets': $scope.threatIntelligenceTabData.topCountriesList
			};
			attackAggregates.push(mappedCountries);
			if ($scope.threatIntelligenceTabData.topCountriesList.length > 0) {
				$scope.threatIntelligenceTabData.topCountriesList.map(function (d, k) {
					if (d.key) {
						d.count = d.doc_count;
						threatMarkersList.push({
							'name': d.name,
							'label': d.label,
							'id': d.label,
							'locationCode': d.key,
							'long': d.longitude,
							'lat': d.latitude,
							'mark': 'assets/images/redpin.png',
							"incidentsCount": d.doc_count,
							'amount': d.doc_count,
							"isIncident": true
						});
					} else {
						$scope.threatIntelligenceTabData.topCountriesList.splice(k, 1);
					}
				});
				var maxIndustriesCount = d3.max($scope.threatIntelligenceTabData.topCountriesList, function (d) {
					return d.count;
				});
				angular.forEach($scope.threatIntelligenceTabData.topCountriesList, function (val, key) {
					if (val.count > 0)
						val.percent = (val.count / maxIndustriesCount) * 100;
					else
						val.percent = 0;
				});
				$scope.threatIntelligenceTabData.topFilteredCountriesList = $scope.threatIntelligenceTabData.topCountriesList;
				$scope.threatIntelligenceTabData.topCountriesList = _.sortBy($scope.threatIntelligenceTabData.topCountriesList, 'label');
				$scope.threatIntelligenceTabData.topFilteredCountriesList.sort(function (p1, p2) { return p2.count - p1.count; });
				topTenCountries = $scope.threatIntelligenceTabData.topFilteredCountriesList.slice(0, 10);
				$scope.threatIntelligenceTabData.topFilteredCountriesList = topTenCountries;
				if (defaultCountryCode && defaultCountryCodeExisted == true) {
					defaultCountryCodeExisted = false;
					filteredMap = threatMarkersList.filter(function (d) {
						return d.locationCode == defaultCountryCode;
					});
					if (filteredMap.length > 0) {
						previousFilteredMap = filteredMap;
						setTimeout(function () {
							//								 $scope.locationName = [filteredMap[0].name];
							$scope.locationName = $scope.threatIntelligenceTabData.topCountriesList.filter(function (d) {
								return d.key == defaultCountryCode;
							});
							if ($scope.mainInfoTabType == 'Autodesk') {
								var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
								var worldChartOptions = {
									container: "#threatWorldMap",
									uri1: "../vendor/data/worldCountries.json",
									uri2: "../vendor/data/worldMapData.json",
									height: 180,
									domain: { x: domain[0], y: domain[1] },
									ChangeRadius: 'true',
									opacity: 0.5,
									markers: filteredMap
								};
								World(worldChartOptions);
							}
						}, 0);
						$scope.threatIntelligenceTabData.topFilteredCountriesList = [];
						previousFilteredMap = arrUnique1(previousFilteredMap);
						angular.forEach(previousFilteredMap, function (v, k) {
							var filteredTopLoc = topTenCountries.filter(function (d) {
								return d.name == v.name;
							});
							$scope.threatIntelligenceTabData.topFilteredCountriesList.push(filteredTopLoc[0]);
						});
					}
				} else {
					filteredMap = threatMarkersList;
					//						 $scope.locationName = ['all'];
					$scope.threatIntelligenceTabData.topFilteredCountriesList = topTenCountries;
					if ($scope.mainInfoTabType == 'Autodesk') {
						var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
						var worldChartOptions = {
							container: "#threatWorldMap",
							uri1: "../vendor/data/worldCountries.json",
							uri2: "../vendor/data/worldMapData.json",
							height: 180,
							domain: { x: domain[0], y: domain[1] },
							ChangeRadius: 'true',
							opacity: 0.5,
							markers: filteredMap
						};
						World(worldChartOptions);
					}
				}
				$("#byTopCountriesList").mThumbnailScroller({
					axis: "y",
					theme: "minimal"
				});
				//					PlotChartForReport(attackAggregates);
			} else {
				//					$('#threatWorldMap').empty();
				var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
				var worldChartOptions = {
					container: "#threatWorldMap",
					uri1: "../vendor/data/worldCountries.json",
					uri2: "../vendor/data/worldMapData.json",
					height: 180,
					domain: { x: domain[0], y: domain[1] },
					ChangeRadius: 'true',
					opacity: 0.5,
					markers: []
				};
				World(worldChartOptions);
				$scope.locationName = [];
				defaultCountryCodeExisted = false;
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.worldMapLoader = false;
		});
	}
	/*------------------------------------------- Top Countries Ends -----------------------------------------------------------------------*/
	/*------------------------------------------  Top Technologies Starts ---------------------------------------------------------------*/
	function getThreatTechnologies(params) {
		$scope.threatIntelligenceTabData.topTechnologiesLoader = true;
		$scope.threatIntelligenceTabData.technologiesList = [];
		$scope.threatIntelligenceTabData.tagCloudTechnologiesList = [];
		$scope.threatIntelligenceTabData.filteredTechnologies = [];
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "technology",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.topTechnologiesLoader = false;
			attackAggregates.push(response.data.aggregations[0]);
			$scope.threatIntelligenceTabData.technologiesList = response.data.aggregations[0].buckets;
			if ($scope.threatIntelligenceTabData.technologiesList.length > 0) {
				angular.forEach($scope.threatIntelligenceTabData.technologiesList, function (v, k) {
					if (v.key) {
						v.name = v.key;
					}
				});
				angular.forEach(technologyConstant, function (d, i) {
					$scope.threatIntelligenceTabData.technologiesList.map(function (v, k) {
						if (i == v.key) {
							v.name = d;
						}
					});
				});
				$scope.threatIntelligenceTabData.technologiesList = _.sortBy($scope.threatIntelligenceTabData.technologiesList, 'key');
				$scope.threatIntelligenceTabData.technologiesList.map(function (d) {
					d.count = d.doc_count;
					d.label = d.name;
					d.id = d.name;
					$scope.threatIntelligenceTabData.tagCloudTechnologiesList.push({
						"text": d.name,
						"size": d.doc_count,
						"key": d.key
					});
				});
				if ($scope.mainInfoTabType == 'Autodesk') {
					setTimeout(function () {
						plotautoDeskCloudChart();
						$("#technologiesList").mThumbnailScroller({
							axis: "y",
							theme: "minimal"
						});
					}, 0);
				}
				$scope.threatIntelligenceTabData.filteredTechnologies = $scope.threatIntelligenceTabData.technologiesList;
				var maxTechnologiesCount = d3.max($scope.threatIntelligenceTabData.technologiesList, function (d) {
					return d.count;
				});
				angular.forEach($scope.threatIntelligenceTabData.technologiesList, function (val, key) {
					val.percent = (val.count / maxTechnologiesCount) * 100;
				});
			} else {
				$('#tagCloudAutoDeskTags').empty();
				$scope.technologyName = [];
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.topTechnologiesLoader = false;
		});
	}
	/*-------------------------------------------------- Top Technologies Ends -------------------------------------------------------*/
	/*---------------------------------------------------- attack Types Starts --------------------------------------------------------*/
	function getThreatAttackType(params) {
		$scope.threatIntelligenceTabData.heatMapLoader = true;
		$scope.threatIntelligenceTabData.attackTypesList = [];
		var data = {
			"query": {
				"date-from": fromDate,
				"date-to": toDate
			},
			"agg": {
				"key": "attack-type",
				"subkey": "industry",
				"time-agg": "month"
			}
		};
		angular.forEach(params, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0) {
				data.query[k] = v;
			}
		});
		EntityApiService.getThreatAggregates(data).then(function (response) {
			$scope.threatIntelligenceTabData.heatMapLoader = false;
			if (response.data.aggregations[0].buckets.length > 0) {
				angular.forEach(response.data.aggregations[0].buckets, function (v, k) {
					if (v.children[0].buckets.length > 0) {
						angular.forEach(v.children[0].buckets, function (val, key) {
							val.country = v.key;
							val.product = val.key;
							val.value = val.doc_count;
							angular.forEach(isicCodeConstants, function (codeV, codeK) {
								if (codeK == val.product) {
									val.product = codeV;
								}
							});
							$scope.threatIntelligenceTabData.attackTypesList.push(val);
						});
					};
				});
			}
			if ($scope.threatIntelligenceTabData.attackTypesList.length > 0) {
				if ($scope.mainInfoTabType == 'Autodesk') {
					setTimeout(function () {
						autodeskHeatMap();
					}, 0);
				}
			} else {
				$('#autodeskHeatMap').empty();
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.heatMapLoader = false;
		});
	}
	/*-------------------------------------------------- attack Types Ends --------------------------------------------------------------*/
	/*----------------------------------------------------radar chart attack Types Starts --------------------------------------------------------*/

	/*
     * @purpose: get Peer Options
     * @created: 21st august 2018
     * @author: swathi
     */
	$scope.getPeerOptions = function (isChecked, key, bool, type, name, selectedType) {
		var finalRadarParams = {};
		var obj = {
			'industry': $scope.checkOptions.peerIndustry,
			'technology': $scope.checkOptions.peerTechnology,
			'country-code': $scope.checkOptions.peerLocation,
			'legal-type': $scope.checkOptions.peerLegalType
		};
		angular.forEach(radarParams, function (v, k) {
			if (v['terms_set'] && v['terms_set'].length > 0 && obj[k] == true) {
				finalRadarParams[k] = v;
			}
			if (k == "legal-type" && v && obj[k] == true) {
				finalRadarParams[k] = v;
			}
		});
		getRadarThreatAttackType(finalRadarParams);
		$scope.pageNumber = 1;
		getTableData($scope.pageNumber);
	};
	/*
     * @purpose: plot peer group without attributes
     * @created: 21st august 2018
     * @params: params(object)
     * @author: swathi
     */
	function getRadarThreatAttackType(params) {
		$("#global_company_radar_chart").empty();
		$("#peerGroupanalysisForRadar").empty();
		threatsRadarData = [], attacksData = [], attacksDataForCompany = [];
		$scope.threatsRadarList = [];
		$scope.threatIntelligenceTabData.radarLoader = true;
		var isAttributesExist = false;
		var data = {
			"query": {
				'date-from': fromDate,
				'date-to': toDate
			},
			"agg": {
				"key": "attack-type"
			}
		};
		var data1 = {
			"query": {
				'date-from': fromDate,
				'date-to': toDate
			},
			"agg": {
				"key": "attack-type"
			}
		};
		if (params) {
			angular.forEach(params, function (v, k) {
				if (v['terms_set'] && v['terms_set'][0].length > 0) {
					isAttributesExist = true;
					data1.query[k] = v;
				}
				if (k == "legal-type" && v) {
					isAttributesExist = true;
					data1.query[k] = v;
				}
			});
		}
		EntityApiService.getThreatAggregates(data).then(function (response) {
			if (response.data.aggregations[0].buckets.length > 0) {
				attacksData = response.data.aggregations[0].buckets;
			}
			if (attacksData.length > 0) {
				var totIncidentsCount = d3.sum(attacksData, function (d) {
					return d.doc_count;
				});
				//map data for chart
				attacksData.map(function (d) {
					d.axis = d.key;
					d.value = parseInt(((d.doc_count / totIncidentsCount) * 100).toFixed(2));
					d.actualValue = d.doc_count;
					d.group = "global";
					d.totIncidentsCount = totIncidentsCount;
					d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
				});
				//push global data
				if (attacksData && attacksData.length > 0)
					threatsRadarData.push(attacksData);
				if (!isAttributesExist) {
					$scope.threatsRadarList = threatsRadarData;
					$scope.threatIntelligenceTabData.radarLoader = false;
					plotRadarChart(threatsRadarData);
				}
				else {
					plotPeerGroupByAttributes(data1);
				}
			} else {
				$scope.threatIntelligenceTabData.radarLoader = false;
				$("#global_company_radar_chart").empty();
				$("#peerGroupanalysisForRadar").empty();
			}
		}, function (failure) {
			$scope.threatIntelligenceTabData.radarLoader = false;
			$("#global_company_radar_chart").empty();
			$("#peerGroupanalysisForRadar").empty();
		});
	}
	/*
     * @purpose: plot peer group by industry/country/technology attributes
     * @created: 21st august 2018
     * @params: data(object)
     * @author: swathi
     */
	function plotPeerGroupByAttributes(data) {
		EntityApiService.getThreatAggregates(data).then(function (response) {
			if (response.data.aggregations[0].buckets.length > 0) {
				attacksDataForCompany = response.data.aggregations[0].buckets;
			}
			if (attacksDataForCompany && attacksDataForCompany.length > 0) {
				var totIncidentsCount = d3.sum(attacksDataForCompany, function (d) {
					return d.doc_count;
				});
				//map data for chart for industry 
				attacksDataForCompany.map(function (d) {
					d.axis = d.key;
					d.value = parseInt(((d.doc_count / totIncidentsCount) * 100).toFixed(2));
					d.actualValue = d.doc_count;
					d.group = "company";
					d.totIncidentsCount = totIncidentsCount;
					d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
				});
				threatsRadarData.push(attacksDataForCompany);
				$scope.threatIntelligenceTabData.radarLoader = false;
				// call funciton to plot radar chart
				$scope.threatsRadarList = threatsRadarData;
				plotRadarChart(threatsRadarData);
			} else {
				$scope.threatsRadarList = threatsRadarData;
				plotRadarChart(threatsRadarData);
				$scope.threatIntelligenceTabData.radarLoader = false;
			}
		}, function (failure) {
			// call funciton to plot radar chart
			$scope.threatsRadarList = threatsRadarData;
			plotRadarChart(threatsRadarData);
			$scope.threatIntelligenceTabData.radarLoader = false;
		});
	}
	/*--------------------------------------------------radar chart attack Types Ends --------------------------------------------------------------*/
	function plotRadarChart(data) {
		if (!data || data.length == 0) {
			return false
		}
		var d = jQuery.extend(true, [], data);
		//Options for the Radar chart, other than default
		var options = {
			container: "#global_company_radar_chart",
			width: $('#global_company_radar_chart').width(),
			height: 180,
			ExtraWidthX: 230,
			circleRadius: 1.5,
			strokeWidth: 0.1,
			opacityArea: 0.5,
			marginTop: 30,
			marginBottom: 30,
			iswhiteBackground: true,
			colorObj: { "global": "#BA4441", "company": "#BB9800" }
		};
		//Call function to draw the Radar chart
		//Will expect that data is in %'s
		RadarChart("#global_company_radar_chart", d, options);
		var d1 = jQuery.extend(true, [], data);
		var options1 = {
			container: "#peerGroupanalysisForRadar",
			width: 600,
			height: 300,
			ExtraWidthX: 230,
			circleRadius: 1.5,
			strokeWidth: 0.1,
			opacityArea: 0.5,
			marginTop: 50,
			marginBottom: 20,
			colorObj: { "global": "#BA4441", "company": "#BB9800" }
		};
		RadarChart("#peerGroupanalysisForRadar", d1, options1);
		$("#peerGroupanalysisForRadar").find("svg").find("line").css("stroke", "#fff");
		$("#peerGroupanalysisForRadar").find("svg").find(".gridCircle").css("fill", "rgb(205, 205, 205)");
		$("#peerGroupanalysisForRadar").find("svg").find(".gridCircle").css("stroke", "rgb(205, 205, 205)");
		$("#peerGroupanalysisForRadar").find("svg").find(".gridCircle").css("filter", "");
	}
	/*--------------------------------------------------table handling starts here--------------------------------------------------------------*/
	$scope.pageNumber = 1;
	$scope.recordsPerPage = 4;
	/*
		* @purpose: on page change
		* @created: 24 aug 2017
		* @params: page(number)
		* @author: swathi
	 */
	$scope.pageChanged = function (page) {
		$scope.pageNumber = page;
		getTableData(page);
	};
	/*
     * @purpose: funciton to get data for table
     * @created: 16 Aug 2018
     * @params: null
     * @return: null
     * @author: Prasanthi
    */
	function getTableData(page) {
		$scope.relatedCompaniesData = [];
		$scope.relatedCompaniesPreloader = true;
		var data = {
			"actor-a-type": "organization",
			"class": "Cyber Security",
			"size": $scope.recordsPerPage,
			"offset": page ? (page - 1) : 0
		};
		var multiData = {
			"country-code": {
				"terms_set": [""],
				"minimum_should_match": 0.2
			},
			"industry": {
				"terms_set": [""],
				"minimum_should_match": 0.2
			},
			"technology": {
				"terms_set": [""],
				"minimum_should_match": 0.2
			}
		};
		if ($scope.checkOptions.peerTechnology && radarParams["technology"]["terms_set"][0]) {
			multiData["technology"]["terms_set"] = radarParams["technology"]["terms_set"];
		}
		if ($scope.checkOptions.peerIndustry && radarParams["industry"]["terms_set"][0]) {
			multiData["industry"]["terms_set"] = radarParams["industry"]["terms_set"];
		}
		if ($scope.checkOptions.peerLocation && radarParams["country-code"]["terms_set"][0]) {
			multiData["country-code"]["terms_set"] = radarParams["country-code"]["terms_set"];
		}
		if ($scope.checkOptions.peerLegalType && radarParams["legal-type"]) {
			multiData["legal-type"] = radarParams["legal-type"];
		}
		angular.forEach(multiData, function (v, k) {
			if (v['terms_set'] && v['terms_set'][0].length > 0) {
				data[k] = v;
			}
			if (k == "legal-type" && v) {
				data[k] = v;
			}
		});
		ActApiService.getRelatedCompanies(data).then(function (response) {
			$scope.relatedCompaniesPreloader = false;
			if (response && response.data) {
				$scope.similarPeerLength = response.data.totalResults;
				if (response.data.peerDataDto && response.data.peerDataDto.length > 0) {
					$scope.relatedCompaniesData = response.data.peerDataDto;
					$scope.avgScore = ((d3.sum(response.data.peerDataDto, function (d) {
						return d.riskScore;
					})) / $scope.relatedCompaniesData.length);
				}
			}
			else {
				//data not found
				$scope.relatedCompaniesData = [];
			}
		}, function (failure) {
			$scope.relatedCompaniesData = [];
			$scope.relatedCompaniesPreloader = false;
			//data not found
		})
	}
	/*
     * @purpose: function to open scoring details
     * @created: 16 Aug 2018
     * @params: null
     * @return: null
     * @author: Prasanthi
    */
	$scope.openriskModal = function (riskDetails) {
		var modalInstance = $uibModal.open({
			templateUrl: '../scripts/common/modal/views/entityRiskScore.modal.html',
			controller: 'EntityRiskScoreModalController',
			windowClass: 'custom-modal bst_modal  c-arrow entity-risk-score-modal',
			resolve: {
				overviewRisks: function () {
					return riskDetails;
				},
				caseID: function () {
					return "";
				},
				caseName: function () {
					return riskDetails.name;
				},
				riskScoreData: function () {
					return JSON.parse(riskDetails.riskScoreData).latest.entityRiskModel;
				}
			}
		});
		modalInstance.result.then(function (result) {
		}, function (err) {
		});
	}
	/*
     * @purpose: function to open entity page
     * @created: 16 Aug 2018
     * @params: null
     * @return: null
     * @author: Prasanthi
    */
	$scope.openEntityPage = function (data) {
		var url = window.location.href.split("/entity/#!/")[0] + "/entity/#!/company/" + data["name"] + '?eid=' + data["identifier"];
		if ($scope.hyperLinksNewTab) {
			window.open(url, '_blank');
		} else {
			window.open(url, '_self');
		}
	}
	/*
     * @purpose: load more data for pagination
     * @created: 12 Jul 2018
     * @author: swathi
    */
	$scope.getPaginationResults = function (pageNum) {
		filtersToApply.offset = pageNum;
		getAdverseNewsForTable(filtersToApply);
	};
	/*
			* @purpose: Apply filters
			* @created: 11 Jul 2018
			* @author: swathi
		 */
	$scope.filterList = [];
	var filtersToApply = {
		'offset': 1,
		'date-from': fromDate,
		'date-to': toDate,
		'country-code': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'industry': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'technology': {
			'terms_set': [],
			"minimum_should_match": 0.2
		},
		'attack-type': {
			'terms_set': [],
			"minimum_should_match": 0.2
		}
	};
	function arrUnique(arr) {
		var newarr = [];
		var unique = {};
		angular.forEach(arr, function (item) {
			if (!unique[item.value]) {
				newarr.push(item);
				unique[item.value] = item;
			}
		});
		return newarr;
	}
	function arrUnique1(arr) {
		var newarr = [];
		var unique = {};
		angular.forEach(arr, function (item) {
			if (item && !unique[item.locationCode]) {
				newarr.push(item);
				unique[item.locationCode] = item;
			}
		});
		return newarr;
	}
	function arrUnique2(arr) {
		var newarr = [];
		var unique = {};
		angular.forEach(arr, function (item) {
			if (!unique[item.key]) {
				newarr.push(item);
				unique[item.key] = item;
			}
		});
		return newarr;
	}
	function arrUnique3(arr) {
		var newarr = [];
		var unique = {};
		angular.forEach(arr, function (item) {
			if (!unique[item.name]) {
				newarr.push(item);
				unique[item.name] = item;
			}
		});
		return newarr;
	}
	var previousFilteredAttackType = [];
	window.pushFilters = function (filter, isLocationFilter, type, locationName, multipleFilters, finalApiCall) {
		$(".Screening_new_tooltip").css("display", "none");
		filtersToApply['offset'] = 1;
		$scope.pageNum = 1;
		if (multipleFilters && $scope.filterList.length > 0) {
			if (type == 'technology') {
				var filtered = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
					return d.key == filter;
				});
				if (filtered.length == 0)
					return false;
			}
			if (multipleFilters == 'locationMultiple') {
				var filtered = threatMarkersList.filter(function (d) {
					return d.locationCode == filter;
				});
				if (previousFilteredMap && previousFilteredMap.length > 0) {
					previousFilteredMap.push(filtered[0]);
					previousFilteredMap = arrUnique1(previousFilteredMap);
					angular.forEach($scope.threatIntelligenceTabData.topCountriesList, function (v, k) {
						previousFilteredMap.map(function (d, i) {
							if (v.key == d.locationCode)
								$scope.locationName.push(v);
						});
					});
					$scope.locationName = arrUnique2($scope.locationName);
				} else {
					previousFilteredMap = threatMarkersList.filter(function (d) {
						return d.locationCode == filter;
					});
					$scope.locationName = $scope.threatIntelligenceTabData.topCountriesList.filter(function (d) {
						return d.key == filter;
					});
				}
			}
			if (multipleFilters == 'selectedMarker') {
				previousFilteredMap = threatMarkersList.filter(function (d) {
					return d.locationCode == filter;
				});
				$scope.locationName = $scope.threatIntelligenceTabData.topCountriesList.filter(function (d) {
					return d.key == filter;
				});
			}
			if (type == 'location') {
				setTimeout(function () {
					var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
					var worldChartOptions = {
						container: "#threatWorldMap",
						uri1: "../vendor/data/worldCountries.json",
						uri2: "../vendor/data/worldMapData.json",
						height: 180,
						domain: { x: domain[0], y: domain[1] },
						ChangeRadius: 'true',
						opacity: 0.5,
						markers: previousFilteredMap
					};
					World(worldChartOptions);
				});
			}
			if (type == 'location' && previousFilteredMap && previousFilteredMap.length > 0) {
				var tempFilterListArr = $scope.filterList;
				tempFilterListArr = arrUnique(tempFilterListArr);
				var filters = [], locations = [];
				angular.forEach(previousFilteredMap, function (d, i) {
					tempFilterListArr.map(function (v, k) {
						if (v.key == 'location') {
							if (v.value == d.locationCode) {
								filters.push(v);
							}
						} else {
							filters.push(v);
						}
					});
				});
				$scope.filterList = filters;
				if (filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
					angular.forEach(filtersToApply['country-code']['terms_set'], function (v, k) {
						previousFilteredMap.map(function (d, i) {
							if (v == d.locationCode) {
								locations.push(v);
							}
						});
					});
					filtersToApply['country-code']['terms_set'] = locations;
				}
				angular.forEach(previousFilteredMap, function (v, k) {
					$scope.filterList.push({
						'key': type,
						'value': v.locationCode,
						'locationName': v.name
					});
					if (filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
						filtersToApply['country-code']['terms_set'].push(v.locationCode);
					} else if (type == 'location') {
						filtersToApply['country-code'] =
							{
								"terms_set": [v.locationCode],
								"minimum_should_match": 0.2
							};
					}
				});
				var uniqueStandards = arrUnique($scope.filterList);
				$scope.filterList = uniqueStandards;
				if (filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
					var result = [];
					angular.forEach(filtersToApply['country-code']['terms_set'], function (e, i) {
						if ($.inArray(e, result) == -1) result.push(e);
					});
					filtersToApply['country-code']['terms_set'] = result;
					$scope.locationName = arrUnique2($scope.locationName);
				}
				$scope.threatIntelligenceTabData.topFilteredCountriesList = [];
				previousFilteredMap = arrUnique1(previousFilteredMap);
				angular.forEach(previousFilteredMap, function (v, k) {
					var filteredTopLoc = topTenCountries.filter(function (d) {
						return d.name == v.name;
					});
					$scope.threatIntelligenceTabData.topFilteredCountriesList.push(filteredTopLoc[0]);
				});
			}
			if (multipleFilters == 'selectedIndustry') {
				previousFilteredIndustry = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
					return d.key == filter;
				});
				$scope.industryName = previousFilteredIndustry;
				$scope.threatIntelligenceTabData.filteredIndustries = previousFilteredIndustry;
			}
			if (multipleFilters == 'industryMultiple') {
				var filtered = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
					return d.key == filter;
				});
				if (previousFilteredIndustry && previousFilteredIndustry.length > 0) {
					previousFilteredIndustry.push(filtered[0]);
					previousFilteredIndustry = arrUnique2(previousFilteredIndustry);
					angular.forEach(previousFilteredIndustry, function (v, k) {
						$scope.industryName.push(v);
					});
					$scope.industryName = arrUnique2($scope.industryName);
				} else {
					previousFilteredIndustry = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
						return d.key == filter;
					});
					$scope.industryName = previousFilteredIndustry;
				}
			}
			if (type == 'industry' && previousFilteredIndustry && previousFilteredIndustry.length > 0) {
				var tempFilterListArr = $scope.filterList;
				tempFilterListArr = arrUnique(tempFilterListArr);
				var filters = [], industries = [];
				angular.forEach(previousFilteredIndustry, function (d, i) {
					tempFilterListArr.map(function (v, k) {
						if (v.key == 'industry') {
							if (v.value == d.key) {
								filters.push(v);
							}
						} else {
							filters.push(v);
						}
					});
				});
				$scope.filterList = filters;
				if (filtersToApply['industry'] && filtersToApply['industry']['terms_set']) {
					angular.forEach(filtersToApply['industry']['terms_set'], function (v, k) {
						previousFilteredIndustry.map(function (d, i) {
							if (v == d.key) {
								industries.push(v);
							}
						});
					});
					filtersToApply['industry']['terms_set'] = industries;
				}
				angular.forEach(previousFilteredIndustry, function (v, k) {
					$scope.filterList.push({
						'key': type,
						'value': v.key,
						'locationName': v.name
					});
					if (filtersToApply['industry'] && filtersToApply['industry']['terms_set']) {
						filtersToApply['industry']['terms_set'].push(v.key);
					} else if (type == 'industry') {
						filtersToApply['industry'] =
							{
								"terms_set": [v.key],
								"minimum_should_match": 0.2
							};
					}
				});
				var uniqueStandards = arrUnique($scope.filterList);
				$scope.filterList = uniqueStandards;
				if (filtersToApply['industry'] && filtersToApply['industry']['terms_set']) {
					var result = [];
					angular.forEach(filtersToApply['industry']['terms_set'], function (e, i) {
						if ($.inArray(e, result) == -1) result.push(e);
					});
					filtersToApply['industry']['terms_set'] = result;
					$scope.industryName = arrUnique2($scope.industryName);
				}
			}
			if (multipleFilters == 'selectedTechnology') {
				previousFilteredTechnology = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
					return d.key == filter;
				});
				$scope.technologyName = previousFilteredTechnology;
				$scope.threatIntelligenceTabData.filteredTechnologies = previousFilteredTechnology;
			}
			if (multipleFilters == 'technologyMultiple') {
				var filtered = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
					return d.key == filter;
				});
				if (previousFilteredTechnology && previousFilteredTechnology.length > 0 && filtered && filtered.length > 0) {
					previousFilteredTechnology.push(filtered[0]);
					previousFilteredTechnology = arrUnique3(previousFilteredTechnology);
					angular.forEach(previousFilteredTechnology, function (v, k) {
						$scope.technologyName.push(v);
					});
					$scope.technologyName = arrUnique3($scope.technologyName);
				} else {
					previousFilteredTechnology = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
						return d.key == filter;
					});
					$scope.technologyName = previousFilteredTechnology;
				}
			}
			if (type == 'technology' && previousFilteredTechnology && previousFilteredTechnology.length > 0) {
				$scope.threatIntelligenceTabData.tagCloudTechnologiesList = [];
				angular.forEach($scope.threatIntelligenceTabData.filteredTechnologies, function (v, k) {
					$scope.threatIntelligenceTabData.tagCloudTechnologiesList.push({
						"text": v.name,
						"size": v.doc_count,
						"key": v.key
					});
				});
				setTimeout(function () {
					plotautoDeskCloudChart();
				}, 0);
				var tempFilterListArr = $scope.filterList;
				tempFilterListArr = arrUnique(tempFilterListArr);
				var filters = [], technologies = [];
				angular.forEach(previousFilteredTechnology, function (d, i) {
					tempFilterListArr.map(function (v, k) {
						if (v.key == 'technology') {
							if (v.value == d.key) {
								filters.push(v);
							}
						} else {
							filters.push(v);
						}
					});
				});
				$scope.filterList = filters;
				if (filtersToApply['technology'] && filtersToApply['technology']['terms_set']) {
					angular.forEach(filtersToApply['technology']['terms_set'], function (v, k) {
						previousFilteredTechnology.map(function (d, i) {
							if (v == d.key) {
								technologies.push(v);
							}
						});
					});
					filtersToApply['technology']['terms_set'] = technologies;
				}
				angular.forEach(previousFilteredTechnology, function (v, k) {
					$scope.filterList.push({
						'key': type,
						'value': v.key,
						'locationName': v.name
					});
					if (filtersToApply['technology'] && filtersToApply['technology']['terms_set']) {
						filtersToApply['technology']['terms_set'].push(v.key);
					} else if (type == 'technology') {
						filtersToApply['technology'] =
							{
								"terms_set": [v.key],
								"minimum_should_match": 0.2
							};
					}
				});
				var uniqueStandards = arrUnique($scope.filterList);
				$scope.filterList = uniqueStandards;
				if (filtersToApply['technology'] && filtersToApply['technology']['terms_set']) {
					var result = [];
					angular.forEach(filtersToApply['technology']['terms_set'], function (e, i) {
						if ($.inArray(e, result) == -1) result.push(e);
					});
					filtersToApply['technology']['terms_set'] = result;
					$scope.technologyName = arrUnique3($scope.technologyName);
				}
			}
			if (multipleFilters == 'selectedAttackType') {
				previousFilteredAttackType.push({
					'key': filter,
					'type': type
				});
			}
			if (type == 'attackType' && previousFilteredAttackType && previousFilteredAttackType.length > 0) {
				var tempFilterListArr = $scope.filterList;
				tempFilterListArr = arrUnique(tempFilterListArr);
				var filters = [], attacks = [];
				angular.forEach(previousFilteredAttackType, function (d, i) {
					tempFilterListArr.map(function (v, k) {
						if (v.key == 'attackType') {
							if (v.value == d.key) {
								filters.push(v);
							}
						} else {
							filters.push(v);
						}
					});
				});
				$scope.filterList = filters;
				if (filtersToApply['attack-type'] && filtersToApply['attack-type']['terms_set']) {
					angular.forEach(filtersToApply['attack-type']['terms_set'], function (v, k) {
						previousFilteredAttackType.map(function (d, i) {
							if (v == d.key) {
								attacks.push(v);
							}
						});
					});
					filtersToApply['attack-type']['terms_set'] = attacks;
				}
				angular.forEach(previousFilteredAttackType, function (v, k) {
					$scope.filterList.push({
						'key': type,
						'value': v.key,
						'locationName': v.key
					});
					if (filtersToApply['attack-type'] && filtersToApply['attack-type']['terms_set']) {
						filtersToApply['attack-type']['terms_set'].push(v.key);
					} else if (type == 'attackType') {
						filtersToApply['attack-type'] =
							{
								"terms_set": [v.key],
								"minimum_should_match": 0.2
							};
					}
				});
				var uniqueStandards = arrUnique($scope.filterList);
				$scope.filterList = uniqueStandards;
				if (filtersToApply['attack-type'] && filtersToApply['attack-type']['terms_set']) {
					var result = [];
					angular.forEach(filtersToApply['attack-type']['terms_set'], function (e, i) {
						if ($.inArray(e, result) == -1) result.push(e);
					});
					filtersToApply['attack-type']['terms_set'] = result;
				}
			}
		} else {
			$scope.filterList.push({
				'key': type,
				'value': filter,
				'locationName': locationName
			});
			if (type === 'industry' && filtersToApply['industry'] && filtersToApply['industry']['terms_set']) {
				filtersToApply['industry']['terms_set'].push(filter);
			} else if (type == 'industry') {
				filtersToApply['industry'] =
					{
						"terms_set": [filter],
						"minimum_should_match": 0.2
					};
				previousFilteredIndustry = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
					return d.key == filter;
				});
				$scope.industryName = previousFilteredIndustry;
			}
			else if (type === 'technology' && filtersToApply['technology'] && filtersToApply['technology']['terms_set']) {
				filtersToApply['technology']['terms_set'].push(filter);
			} else if (type == 'technology') {
				filtersToApply['technology'] =
					{
						"terms_set": [filter],
						"minimum_should_match": 0.2
					};
				previousFilteredTechnology = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
					return d.key == filter;
				});
				$scope.technologyName = previousFilteredTechnology;
			}
			else if (type == 'location' && filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
				filtersToApply['country-code']['terms_set'].push(filter);
			} else if (type == 'location') {
				filtersToApply['country-code'] =
					{
						"terms_set": [filter],
						"minimum_should_match": 0.2
					};
				previousFilteredMap = threatMarkersList.filter(function (d) {
					return d.locationCode == filter;
				});
				$scope.locationName = $scope.threatIntelligenceTabData.topCountriesList.filter(function (d) {
					return d.key == filter;
				});
			}
			if (type === 'attackType' && filtersToApply['attack-type'] && filtersToApply['attack-type']['terms_set']) {
				filtersToApply['attack-type']['terms_set'].push(filter);
			} else if (type == 'attackType') {
				filtersToApply['attack-type'] =
					{
						"terms_set": [filter],
						"minimum_should_match": 0.2
					};
				previousFilteredAttackType.push({
					'key': filter,
					'type': type
				});
			}
		}
		if (finalApiCall == 'locationMultipleEnd') {
			getAdverseNewsForTable(filtersToApply);
		} else {
			if (multipleFilters != 'selectedAttackType' && multipleFilters != 'locationMultiple' && multipleFilters != 'industryMultiple' && multipleFilters != 'technologyMultiple')
				getAdverseNewsForTable(filtersToApply);
		}
		if (isLocationFilter) {
			$scope.$digest();
		}
	};
	$scope.applyFilters = pushFilters;
	$scope.removeFilter = function (index, filter) {
		if (previousFilteredMap && previousFilteredMap.length > 0) {
			angular.forEach(previousFilteredMap, function (d, i) {
				if (d.locationCode == filter) {
					$scope.locationName.splice(i, 1);
					previousFilteredMap.splice(i, 1);
					if (previousFilteredMap.length == 0) {
						$scope.locationName = [];
						previousFilteredMap = [];
						$scope.threatIntelligenceTabData.topFilteredCountriesList = topTenCountries;
					}
					var domain = d3.extent(threatMarkersList, function (d) { return d.incidentsCount });
					var worldChartOptions = {
						container: "#threatWorldMap",
						uri1: "../vendor/data/worldCountries.json",
						uri2: "../vendor/data/worldMapData.json",
						height: 180,
						domain: { x: domain[0], y: domain[1] },
						ChangeRadius: 'true',
						opacity: 0.5,
						markers: previousFilteredMap
					};
					World(worldChartOptions);
				}
			});
			if (previousFilteredMap.length > 0) {
				previousFilteredMap = arrUnique1(previousFilteredMap);
				$scope.threatIntelligenceTabData.topFilteredCountriesList = [];
				angular.forEach(previousFilteredMap, function (v, k) {
					var filteredTopLoc = topTenCountries.filter(function (d) {
						return d.name == v.name;
					});
					$scope.threatIntelligenceTabData.topFilteredCountriesList.push(filteredTopLoc[0]);
				});
			}
		}
		if (previousFilteredIndustry && previousFilteredIndustry.length > 0) {
			previousFilteredIndustry.map(function (d, i) {
				if (d.key == filter) {
					$scope.industryName.splice(i, 1);
					previousFilteredIndustry.splice(i, 1);
					if (previousFilteredIndustry.length == 0) {
						$scope.industryName = [];
						$scope.threatIntelligenceTabData.filteredIndustries = $scope.threatIntelligenceTabData.industriesList;
					} else {
						$scope.threatIntelligenceTabData.filteredIndustries = previousFilteredIndustry;
					}
				}
			});
		}
		if (previousFilteredTechnology && previousFilteredTechnology.length > 0) {
			previousFilteredTechnology.map(function (d, i) {
				if (d.key == filter) {
					$scope.technologyName.splice(i, 1);
					previousFilteredTechnology.splice(i, 1);
					if (previousFilteredTechnology.length == 0) {
						$scope.technologyName = [];
						$scope.threatIntelligenceTabData.filteredTechnologies = $scope.threatIntelligenceTabData.technologiesList;
					} else {
						$scope.threatIntelligenceTabData.filteredTechnologies = previousFilteredTechnology;
					}
				}
			});
			$scope.threatIntelligenceTabData.tagCloudTechnologiesList = [];
			angular.forEach($scope.threatIntelligenceTabData.filteredTechnologies, function (v, k) {
				$scope.threatIntelligenceTabData.tagCloudTechnologiesList.push({
					"text": v.name,
					"size": v.doc_count,
					"key": v.key
				});
			});
			setTimeout(function () {
				plotautoDeskCloudChart();
			}, 0);
		}
		if (previousFilteredAttackType && previousFilteredAttackType.length > 0) {
			previousFilteredAttackType.map(function (d, i) {
				if (d.key == filter) {
					previousFilteredAttackType.splice(i, 1);
				}
			});
		}
		filtersToApply['offset'] = 1;
		$scope.pageNum = 1;
		if ($scope.filterList.length > 0) {
			angular.forEach(filtersToApply, function (v, k) {
				if (v['terms_set'] && v['terms_set'].indexOf(filter) >= 0) {
					var removableIndex = v['terms_set'].indexOf(filter);
					v['terms_set'].splice(removableIndex, 1);
				}
			});
			$scope.filterList.splice(index, 1);
			getAdverseNewsForTable(filtersToApply);
		} else {
			getInitialData();
			$scope.newsToDisplay = $scope.initialNewsData;
		}
	};
	var newsData;
	/*
		* @purpose: plot news bars
		* @created: 05 Jul 2018
		* @author: swathi
	 */
	var plotBarsData = [], barOptions1 = {};
	function plotNewsBars() {
		plotBarsData = [], barOptions1 = {};
		if (!$scope.threatIntelligenceTabData || !$scope.threatIntelligenceTabData.newsList || $scope.threatIntelligenceTabData.newsList.length == 0) {
			return false;
		}
		angular.forEach($scope.threatIntelligenceTabData.newsList, function (v, k) {
			plotBarsData = [];
			angular.forEach(v.values, function (d, key) {
				var currentDt = new Date(d.formattedDate);
				if (currentDt >= lastYrFromDate && currentDt <= todayDt) {
					plotBarsData.push({
						"x": d.formattedDate,
						"y": d.doc_count,
						'count': d.doc_count
					});
				}
			});
			barOptions1 = {
				id: '#bar_1',
				height: 125,
				color: "#337ab7"
			};
			new verticalBarGraph(plotBarsData, barOptions1);
		});
	}
	/* @purpose:  plot autodesk source PieChart
	* 			 
	* @created: 06 july 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	function plotautoDeskbySource() {
		adverseNewsPlotPie = [];
		$("#autodeskBysource").empty();
		angular.forEach($scope.threatIntelligenceTabData.newsList, function (news, key) {
			angular.forEach(news.values, function (d, key) {
				if (!(_.find(adverseNewsPlotPie, { 'key': d.source }))) {
					var valuelength = _.filter($scope.entitySearchResult.list['adverse_news'], { 'source': d.source });
					adverseNewsPlotPie.push({
						"key": d.source,
						"doc_count": valuelength.length
					})
				}
			});
		});
		$scope.entitySearchResult.is_data_not_found.adverseNewsPlotPie = adverseNewsPlotPie.length === 0 ? false : true;
		EntityCommonTabService.InitializeandPlotPie(adverseNewsPlotPie, "autodeskBysource");
	}
	/*
		* @purpose: plot tag cloud
		* @created: 12 Jul 2018
		* @author: swathi
	 */
	var tagCloudAutoDeskOptions = {
		header: "IDUSTRY",
		container: "#tagCloudAutoDeskTags",
		height: 180,
		width: 300,
		data: [],
		margin: {
			bottom: 10,
			top: 10,
			right: 10,
			left: 10
		}
	};
	function plotautoDeskCloudChart() {
		if (!$scope.threatIntelligenceTabData || !$scope.threatIntelligenceTabData.tagCloudTechnologiesList || $scope.threatIntelligenceTabData.tagCloudTechnologiesList.length == 0) {
			return false;
		}
		$('#tagCloudAutoDeskTags').empty();
		tagCloudAutoDeskOptions.data = $scope.threatIntelligenceTabData.tagCloudTechnologiesList;
		EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudAutoDeskOptions);
	}
	var vlaAutoDeskOptions;
	function loadDocumentData(identifier) {
		//	window.global.page = "doc";
		$('#autoDeskVLA').empty();
		$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = true;
		if (identifier) {
			ActApiService.getNews(identifier).then(function (response) {
				var data = {
					"basic": response.data.basic
					//				"technology": "internet-of-things"
				};
				var Isic = {};
				if (data && data.basic && data.basic["bst:businessClassifier"] && data.basic["bst:businessClassifier"].length > 0) {
					Isic = _.find(data.basic["bst:businessClassifier"], {
						standard: 'ISIC'
					});
				}
				if (!_.isEmpty(Isic)) {
					data.basic["bst:businessClassifier"] = [];
					data.basic["bst:businessClassifier"][0] = Isic;
					if (Object.keys(filtersToApply).length > 0) {
						angular.forEach(filtersToApply, function (val, key) {
							if (key == "country-code" && val['terms_set'].length > 0) {
								data["country-code"] = val['terms_set'];
								data["country-code-min-match"] = 1;
							} else if (key == "industry" && val['terms_set'].length > 0) {
								data["industry"] = val['terms_set'];
								data["industry-min-match"] = 1;
							} else if (key == "technology" && val['terms_set'].length > 0) {
								data["technology"] = val['terms_set'];
								data["technology-min-match"] = 1;
							} else if (key == "attack-type" && val['terms_set'].length > 0) {
								data["attack-type"] = val['terms_set'];
								data["attack-type-min-match"] = 1;
							}
						});
					}
					ActApiService.getGraphData(data).then(function (response) {
						if (response.data && response.data.vertices && response.data.vertices.length > 0) {
							var currentCompany = data.basic['vcard:organization-name'];
							var event_count_range = d3.extent(response.data.vertices, function (d) {
								if (d.properties.event_count && d.labelV != 'Event') {
									return parseFloat(d.properties.event_count);
								}
							});
							var colorScale = d3.scaleLinear().domain([1, 10]).range(["#ed8080", "#e80202"]);
							response.data.vertices.map(function (d, i) {
								if (d.properties.event_count && d.labelV != 'Event') {
									d.evt_color = parseFloat(d.properties.event_count) < 10 ? colorScale(parseFloat(d.properties.event_count)) : "#e80202";
								}
								if (d.labelV == 'Company' && d.name.toLowerCase() == currentCompany.toLowerCase()) {
									d.evt_color = "#53e06a";
								}
							});
							var options = {
								data: response.data,
								id: 'autoDeskVLA'
							};
							var vla_options = {
								"target_html_element": "vla",
								"userId": 21220,
								"layout": "cola",
								"tip_event": "hover",
								"autoload": {
									"type": "json",
									"caseId": "",
									"limit": 100,
									"queryParams": true,
									"queryPath": EHUB_API,
									"token": $rootScope.ehubObject.token
								}
							};
							if (typeof custom_config != 'undefined')
								vla_options = $.extend({}, custom_config, vla_options);
							if (response.data)
								vla_options.data = response.data;
							vlaAutoDeskOptions = options;
							$('#autoDeskVLA').empty();
							EntityCommonTabService.loadDataAndPlotGraphForEntity(options);
							$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = false;
							$scope.entitySearchResult.is_data_not_found.is_autodesk_chart = false;
						} else {
							$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = false;
							$scope.entitySearchResult.is_data_not_found.is_autodesk_chart = true;
						}
					}, function (failure) {
						HostPathService.FlashErrorMessage('No Data Found', '');
						$scope.docDetailsPreloader = false;
						$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = false;
						$scope.entitySearchResult.is_data_not_found.is_autodesk_chart = true;
					})
				} else {
					$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = false;
					$scope.entitySearchResult.is_data_not_found.is_autodesk_chart = true;
				}
			}, function (failure) {
				$scope.entitySearchResult.is_data_not_found.is_autodesk_Vla = false;
				$scope.entitySearchResult.is_data_not_found.is_autodesk_chart = true;
			});
		}
	}
	/*
	 * @purpose: plot incidents classification trend timeline
	 * @created: 18th Jul 2018
	 * @author: swathi
	 */
	var timelineData = [],
		durationFrom = lastSixYrs;
	function autodeskThreattimeLine(timelineDuration) {
		if (!$scope.threatIntelligenceTabData || !$scope.threatIntelligenceTabData.trendsList || $scope.threatIntelligenceTabData.trendsListLength == 0) {
			return false;
		}
		timelineData = [];
		if (timelineDuration == 'By Year') {
			durationFrom = lastSixYrs;
		} else {
			durationFrom = lastThirteenMonths;
		}
		angular.forEach($scope.threatIntelligenceTabData.trendsList, function (v, k) {
			angular.forEach(v.values, function (d, key) {
				var currentDt = new Date(d.formattedDate);
				if (currentDt >= new Date(durationFrom) && currentDt <= todayDt) {
					var myDate = d.formattedDate;
					myDate = myDate.split("-");
					var newDate = myDate[2] + "/" + myDate[1] + "/" + myDate[0];
					timelineData.push({
						"date": d.formattedDate,
						"size": d.doc_count,
						"entityName": d.key,
						"formattedDate": d.formattedDate,
						"formattedCount": nFormatter(d.doc_count, 1)
					});
				}
			});
		});
		$scope.threatIntelligenceTabData.timelineData = timelineData;
		$('#autodeskTimeline').empty();
		setTimeout(function () {
			var domain = d3.extent(timelineData, function (d) {
				return d.size
			});
			var exampleChart = new timescalebubbleChart({
				container: '#autodeskTimeline',
				data: timelineData,
				header: '',
				height: 220,
				isheader: '',
				xticks: 5,
				domain: {
					x: domain[0],
					y: domain[1]
				},
				margin: {
					top: 20,
					right: 20
				},
				show_YAxis: false,
				show_XAxisPath: false,
				show_XAxisTicks: false
			});
		}, 0);
	}
	/*
	 * @purpose: plot incidents classification by industry graph
	 * @created: 18th Jul 2018
	 * @author: swathi
	 */
	$(window).resize(function () {
		if ($scope.mainInfoTabType === 'Autodesk') {
			autodeskThreattimeLine($scope.trendName);
			plotChartforThreatClassification(threatsClassificatinoObj);
			autodeskHeatMap();
			if (threatMarkersList.length > 0) {
				var domain = d3.extent(threatMarkersList, function (d) {
					return d.incidentsCount
				});
				var worldChartOptions = {
					container: "#threatWorldMap",
					uri1: "../vendor/data/worldCountries.json",
					uri2: "../vendor/data/worldMapData.json",
					height: 180,
					domain: {
						x: domain[0],
						y: domain[1]
					},
					ChangeRadius: 'true',
					opacity: 0.5,
					markers: filteredMap
				};
				World(worldChartOptions);
			} else {
				var worldChartOptions = {
					container: "#threatWorldMap",
					uri1: "../vendor/data/worldCountries.json",
					uri2: "../vendor/data/worldMapData.json",
					height: 180,
					ChangeRadius: 'true',
					opacity: 0.5,
					markers: []
				};
				World(worldChartOptions);
			}
			setTimeout(function () {
				plotautoDeskCloudChart();
			}, 0);
		}
	});
	function autodeskHeatMap() {
		if (!$scope.threatIntelligenceTabData || !$scope.threatIntelligenceTabData.attackTypesList || $scope.threatIntelligenceTabData.attackTypesList.length == 0) {
			return false;
		}
		setTimeout(function () {
			var options = {
				container: "#autodeskHeatMap",
				marginTop: 40,
				marginRight: 20,
				marginBottom: 20,
				marginLeft: 110,
				height: 200,
				itemSize: 30,
				show_XAxisPath: false,
				show_XAxisTicks: false,
				show_YAxisPath: false,
				show_YAxisTicks: false,
				data: jQuery.extend(true, [], $scope.threatIntelligenceTabData.attackTypesList)
			};
			new heatMap(options);
		}, 0);
	}
	/*
	 * @purpose: show Tooltip By Industry and Top Countries and Top Technologies
	 * @created: 10 Jul 2018
	 * @author: swathi
	 */
	$scope.showTooltipByIndustry = function (val, count, type) {
		if (type == 'topCountries' || type == 'topTechnologies') {
			$scope.topCountriesName = val;
			$scope.incidentsByTopCountriesNameCount = nFormatter(count, 1);
		} else {
			$scope.industriesName = val;
			$scope.incidentsByIndustryCount = nFormatter(count, 1);
		}
	};
	/*
	 * @purpose: update Industry
	 * @created: 10 Jul 2018
	 * @author: swathi
	 */
	$scope.industryName = [];
	var previousFilteredIndustry = [],
		tempIndustryArr = [],
		filteredIndustry;
	$scope.updateIndustry = function () {
		if ($scope.industryName.length == 0) {
			if ($scope.filterList.length > 0 && previousFilteredIndustry) {
				$scope.threatIntelligenceTabData.filteredIndustries = $scope.threatIntelligenceTabData.industriesList;
				var filterListArr = $scope.filterList;
				$scope.filterList = [];
				angular.forEach(filterListArr, function (d, k) {
					if (d.key != 'industry') {
						$scope.filterList.push(d);
					}
				});
				if (filtersToApply['industry'] && filtersToApply['industry']['terms_set']) {
					filtersToApply['industry']['terms_set'] = [];
					getAdverseNewsForTable(filtersToApply);
				}
			}
		} else {
			$scope.industryName = arrUnique2($scope.industryName);
			previousFilteredIndustry = [], tempIndustryArr = [];
			angular.forEach($scope.industryName, function (v, k) {
				filteredIndustry = $scope.threatIntelligenceTabData.industriesList.filter(function (d) {
					return d.name == v.name;
				});
				tempIndustryArr.push(filteredIndustry[0]);
			});
			if (tempIndustryArr.length > 0) {
				var dummyPreviousFilteredIndustry = tempIndustryArr;
				angular.forEach(tempIndustryArr, function (v, k) {
					if (!v) {
						dummyPreviousFilteredIndustry.splice(k, 1);
					}
				});
				tempIndustryArr = dummyPreviousFilteredIndustry;
				previousFilteredIndustry = tempIndustryArr;
				filteredIndustry = previousFilteredIndustry;
				$scope.threatIntelligenceTabData.filteredIndustries = previousFilteredIndustry;
				$scope.threatIntelligenceTabData.filteredIndustries = arrUnique2($scope.threatIntelligenceTabData.filteredIndustries);
				angular.forEach(tempIndustryArr, function (v, k) {
					if (k == $scope.industryName.length - 1) {
						$scope.applyFilters(v.key, false, 'industry', v.name, 'industryMultiple', 'locationMultipleEnd');
					} else {
						$scope.applyFilters(v.key, false, 'industry', v.name, 'industryMultiple');
					}
				});
			}
		}
	};
	$scope.example24settings = {
		scrollableHeight: '100px',
		scrollable: true,
		displayProp: 'id',
		idProperty: 'id'
	};
	$scope.selectIndustry = {
		onInitDone: function (item) {
		},
		onItemDeselect: function (item) {
			$scope.updateIndustry();
		},
		onItemSelect: function (item) {
			$scope.updateIndustry();
		},
		onSelectAll: function (item) {
			previousFilteredIndustry = [];
			$scope.industryName = $scope.threatIntelligenceTabData.industriesList;
			$scope.updateIndustry();
		},
		onDeselectAll: function (item) {
			previousFilteredIndustry = [];
			$scope.industryName = [];
			$scope.updateIndustry();
		}
	};
	/*
	 * @purpose: update technology
	 * @created: 11 Jul 2018
	 * @author: swathi
	 */
	$scope.technologyName = [];
	var previousFilteredTechnology, tempTechnologyArr = [],
		filteredTechnology;
	$scope.updateTechnology = function () {
		$scope.threatIntelligenceTabData.filteredTechnologies = [];
		if ($scope.technologyName.length == 0) {
			$scope.threatIntelligenceTabData.filteredTechnologies = $scope.threatIntelligenceTabData.technologiesList;
			$scope.threatIntelligenceTabData.tagCloudTechnologiesList = [];
			angular.forEach($scope.threatIntelligenceTabData.filteredTechnologies, function (v, k) {
				$scope.threatIntelligenceTabData.tagCloudTechnologiesList.push({
					"text": v.name,
					"size": v.doc_count,
					"key": v.key
				});
			});
			setTimeout(function () {
				plotautoDeskCloudChart();
			}, 0);
			if ($scope.filterList.length > 0 && previousFilteredTechnology) {
				var filterListArr = $scope.filterList;
				$scope.filterList = [];
				angular.forEach(filterListArr, function (d, k) {
					if (d.key != 'technology') {
						$scope.filterList.push(d);
					}
				});
				if (filtersToApply['technology'] && filtersToApply['technology']['terms_set']) {
					filtersToApply['technology']['terms_set'] = [];
					getAdverseNewsForTable(filtersToApply);
				}
			}
		} else {
			$scope.technologyName = arrUnique3($scope.technologyName);
			previousFilteredTechnology = [], tempTechnologyArr = [];
			angular.forEach($scope.technologyName, function (v, k) {
				filteredTechnology = $scope.threatIntelligenceTabData.technologiesList.filter(function (d) {
					return d.name == v.name;
				});
				tempTechnologyArr.push(filteredTechnology[0]);
			});
			if (tempTechnologyArr.length > 0) {
				var dummyPreviousFilteredTechnology = tempTechnologyArr;
				angular.forEach(tempTechnologyArr, function (v, k) {
					if (!v) {
						dummyPreviousFilteredTechnology.splice(k, 1);
					}
				});
				tempTechnologyArr = dummyPreviousFilteredTechnology;
				previousFilteredTechnology = tempTechnologyArr;
				filteredTechnology = previousFilteredTechnology;
				$scope.threatIntelligenceTabData.filteredTechnologies = previousFilteredTechnology;
				$scope.threatIntelligenceTabData.filteredTechnologies = arrUnique3($scope.threatIntelligenceTabData.filteredTechnologies);
				angular.forEach(tempTechnologyArr, function (v, k) {
					if (k == $scope.technologyName.length - 1) {
						$scope.applyFilters(v.key, false, 'technology', v.name, 'technologyMultiple', 'locationMultipleEnd');
					} else {
						$scope.applyFilters(v.key, false, 'technology', v.name, 'technologyMultiple');
					}
				});
			}
		}
	};
	$scope.example34settings = {
		scrollableHeight: '100px',
		scrollable: true,
		displayProp: 'id',
		idProperty: 'id'
	};
	$scope.selectTechnology = {
		onInitDone: function (item) {
		},
		onItemDeselect: function (item) {
			$scope.updateTechnology();
		},
		onItemSelect: function (item) {
			$scope.updateTechnology();
		},
		onSelectAll: function (item) {
			previousFilteredTechnology = [];
			$scope.technologyName = $scope.threatIntelligenceTabData.technologiesList;
			$scope.updateTechnology();
		},
		onDeselectAll: function (item) {
			previousFilteredTechnology = [];
			$scope.technologyName = [];
			$scope.updateTechnology();
		}
	};
	/*
	 * @purpose: update location
	 * @created: 10 Jul 2018
	 * @author: swathi
	 */
	$scope.locationName = [];
	var tempMapArr = [];
	var filteredMap, previousFilteredMap = [];
	$scope.updateLocation = function () {
		if ($scope.locationName.length == 0) {
			if ($scope.filterList.length > 0 && previousFilteredMap) {
				$scope.threatIntelligenceTabData.topFilteredCountriesList = topTenCountries;
				filteredMap = threatMarkersList;
				var filterListArr = $scope.filterList;
				$scope.filterList = [];
				angular.forEach(filterListArr, function (d, k) {
					if (d.key != 'location') {
						$scope.filterList.push(d);
					}
				});
				if (filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
					filtersToApply['country-code']['terms_set'] = [];
					getAdverseNewsForTable(filtersToApply);
				}
			}
			previousFilteredMap = [];
		} else {
			$scope.locationName = arrUnique2($scope.locationName);
			previousFilteredMap = [], tempMapArr = [], $scope.threatIntelligenceTabData.topFilteredCountriesList = [];
			angular.forEach($scope.locationName, function (v, k) {
				filteredMap = threatMarkersList.filter(function (d) {
					return d.name == v.name;
				});
				tempMapArr.push(filteredMap[0]);
				var filteredTopLoc = topTenCountries.filter(function (d) {
					return d.name == v.name;
				});
				$scope.threatIntelligenceTabData.topFilteredCountriesList.push(filteredTopLoc[0]);
			});
			if (tempMapArr.length > 0) {
				var dummyPreviousFilteredMap = tempMapArr;
				angular.forEach(tempMapArr, function (v, k) {
					if (!v) {
						dummyPreviousFilteredMap.splice(k, 1);
					}
				});
				tempMapArr = dummyPreviousFilteredMap;
				previousFilteredMap = tempMapArr;
				filteredMap = previousFilteredMap;
				angular.forEach(tempMapArr, function (v, k) {
					if (k == $scope.locationName.length - 1) {
						$scope.applyFilters(v.locationCode, false, 'location', v.name, 'locationMultiple', 'locationMultipleEnd');
					} else {
						$scope.applyFilters(v.locationCode, false, 'location', v.name, 'locationMultiple');
					}
				});
			}
		}
		var domain = d3.extent(threatMarkersList, function (d) {
			return d.incidentsCount
		});
		var worldChartOptions = {
			container: "#threatWorldMap",
			uri1: "../vendor/data/worldCountries.json",
			uri2: "../vendor/data/worldMapData.json",
			height: 180,
			domain: {
				x: domain[0],
				y: domain[1]
			},
			ChangeRadius: 'true',
			opacity: 0.5,
			markers: filteredMap
		};
		World(worldChartOptions);
	};
	$scope.example14settings = {
		scrollableHeight: '100px',
		scrollable: true,
		displayProp: 'id',
		idProperty: 'id'
	};
	$scope.selectLocation = {
		onInitDone: function (item) {
		},
		onItemDeselect: function (item) {
			$scope.updateLocation();
		},
		onItemSelect: function (item) {
			$scope.updateLocation();
		},
		onSelectAll: function (item) {
			previousFilteredMap = [];
			//			$scope.locationName = $scope.threatIntelligenceTabData.topCountriesList;
			if ($scope.threatIntelligenceTabData.topCountriesList.length > 0) {
				threatMarkersList = [];
				angular.forEach($scope.threatIntelligenceTabData.topCountriesList, function (v, k) {
					countriesListByCodes.map(function (d) {
						if (d.iso2Code && d.iso2Code == v.key) {
							v.name = d.name;
							v.latitude = d.latitude;
							v.longitude = d.longitude;
							v.label = d.name;
							v.id = d.name;
							$scope.locationName.push(v);
							threatMarkersList.push({
								'name': d.name,
								'label': d.name,
								'id': d.name,
								'locationCode': d.iso2Code,
								'long': d.longitude,
								'lat': d.latitude,
								'mark': 'assets/images/redpin.png',
								"incidentsCount": v.doc_count,
								'amount': v.doc_count,
								"isIncident": true
							});
						}
					});
				});
			} else {
				$scope.locationName = [];
			}
			$scope.updateLocation();
		},
		onDeselectAll: function (item) {
			$scope.locationName = [];
			$scope.updateLocation();
		}
	};
	/*
	 * @purpose: update trend
	 * @created: 23 Jul 2018
	 * @author: swathi
	 */
	$scope.trendName = 'By Year';
	$scope.updateTrendName = function () {
		autodeskThreattimeLine($scope.trendName);
	};
	$("body").on("mouseover", ".riskScoreTooltip", function () {
		$(".Screening_new_tooltip").css("display", "block");
		$(".Screening_new_tooltip").css("min-width", 0)
		$(".Screening_new_tooltip").html('<div class="">Click For Details</div>');
	}).on("mousemove", ".riskScoreTooltip", function (event) {
		$(".Screening_new_tooltip").css("display", "block");
		$(".Screening_new_tooltip").css("min-width", 0)
		// var p = $(".entity-company-content-right")
		var p = $(".entity-company-risk");
		var position = p.offset();
		var windowWidth = window.innerWidth;
		var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		var cursor = event.pageX;
		if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX - 50 - $(".Screening_new_tooltip").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX) - 20 + "px");
		}
		return $(".Screening_new_tooltip").css("top", (event.pageY) + 20 + "px")
		$(".Screening_new_tooltip").html('<div class="">Click For Details</div>');
	}).on("mouseout", ".riskScoreTooltip", function () {
		$(".Screening_new_tooltip").css("display", "none");
	})
	/*-----------------------------------------------THREAT INTELLIGENCE TAB ENDS------------------------------------------*/
	/*
* @purpose: threats INtelligence TaB starts 
* pDate : 04-dec-2018 
* @author: Ram Singh
*/
	function threatsIntelligenceTab(response) {
		if (response.basic && response.basic.length > 0 && response.basic[0].value["vcard:organization-name"]) {
			if ($scope.entitySearchResult.list.topHeaderObject) {
				var keepGoingLegal = true;
				response.basic.map(function (d) {
					if (d.value && d.value["lei:legalForm"] && d.value["lei:legalForm"].code && keepGoingLegal) {
						keepGoingLegal = false;
						radarParams["legal-type"] = d.value["lei:legalForm"].code;
						$scope.checkRadarParams["legal-type"] = true;
						$scope.checkOptions.peerLegalType = true;
					}
				});
				if ($scope.entitySearchResult.list.topHeaderObject['isDomiciledIn']) {
					radarParams['country-code']['terms_set'] = [$scope.entitySearchResult.list.topHeaderObject['isDomiciledIn']];
					$scope.checkRadarParams['country-code'] = true;
					$scope.checkOptions.peerLocation = true;
					countriesListByCodes.map(function (d) {
						if (d.iso2Code == $scope.entitySearchResult.list.topHeaderObject['isDomiciledIn']) {
							$scope.checkRadarParams['countryData'] = {
								'key': $scope.entitySearchResult.list.topHeaderObject['isDomiciledIn'],
								'name': d.name
							};
						}
					});
				}
				var Isic = {};
				var keepGoingUrl = true;
				response.basic.map(function (d) {
					if (d.value && d.value["hasURL"] && d.value["hasURL"].length > 0 && keepGoingUrl) {
						keepGoingUrl = false;
						if (Array.isArray(d.value["hasURL"])) {
							companyWebsiteUrl = d.value["hasURL"][0];
						} else {
							companyWebsiteUrl = d.value["hasURL"];
						}
						companyWebsiteUrl = companyWebsiteUrl.replace(/(^\w+:|^)\/\//, '');
					}
					if (d.value && d.value["bst:businessClassifier"] && d.value["bst:businessClassifier"].length > 0) {
						Isic = (d.value && d.value["bst:businessClassifier"]) ? d.value["bst:businessClassifier"] : [];
						if (Isic.length > 0) {
							var keepGoingCode = true;
							angular.forEach(Isic, function (v, k) {
								if (v.standard && v.standard == 'ISIC' && keepGoingCode) {
									var code = v.code.charAt(0);
									keepGoingCode = false;
									radarParams['industry']['terms_set'] = [code];
									$scope.checkRadarParams['industry'] = true;
									$scope.checkOptions.peerIndustry = true;
									angular.forEach(isicCodeConstants, function (codeV, codeK) {
										if (codeK == code && codeV) {
											osintIndustry = codeV;
											$scope.entitySearchResult.list.topHeaderObject.industry = codeV;
											$scope.checkRadarParams['industryData'] = {
												'key': codeK,
												'name': codeV
											};
										}
									});
								}
							});
						}
					}
				});
			}
			osintAddress = $scope.entitySearchResult.list.topHeaderObject["mdaas:RegisteredAddress"] ? $scope.entitySearchResult.list.topHeaderObject["mdaas:RegisteredAddress"]["fullAddress"] : '';
			osintTel = $scope.entitySearchResult.list.topHeaderObject["mdaas:RegisteredAddress"] ? $scope.entitySearchResult.list.topHeaderObject["mdaas:RegisteredAddress"]["tr-org:hasRegisteredPhoneNumber"] : '';
			osintCountry = $scope.entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'] ? $scope.entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'].country : '';
			osintCity = $scope.entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'] ? $scope.entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'].city : '';
		}
		getRadarThreatAttackType(radarParams);
		getTableData($scope.pageNumber);
		getdefaultFilterForThreat(radarParams);
		//}
	}//function ends
	var defaultIndustryName, defaultCountryCode, defaultCountryName, defaultCountryCodeExisted = false, defaultIndustryNameExisted = false;
	function getdefaultFilterForThreat(defaultFilterParams) {
		if (defaultFilterParams) {
			if (defaultFilterParams["industry"]["terms_set"][0].length > 0) {
				var code = defaultFilterParams["industry"]["terms_set"][0];
				defaultIndustryNameExisted = true;
				angular.forEach(isicCodeConstants, function (codeV, codeK) {
					if (codeK == code) {
						defaultIndustryName = codeV;
					}
				});
				$scope.filterList.push({
					'key': 'industry',
					'value': code,
					'locationName': defaultIndustryName
				});
				tableData['industry']['terms_set'] = [code];
				filtersToApply['industry']['terms_set'] = [code];
			}
			if (defaultFilterParams["country-code"]["terms_set"][0].length > 0) {
				defaultCountryCodeExisted = true;
				defaultCountryCode = defaultFilterParams["country-code"]["terms_set"][0];
				countriesListByCodes.map(function (d) {
					if (d.name) {
						if (d.iso2Code == defaultCountryCode) {
							defaultCountryName = d.name;
						}
					}
				});
				$scope.filterList.push({
					'key': 'location',
					'value': defaultCountryCode,
					'locationName': defaultCountryName
				});
				if (tableData['country-code'] && tableData['country-code']['terms_set']) {
					tableData['country-code']['terms_set'] = [defaultCountryCode];
				}
				if (filtersToApply['country-code'] && filtersToApply['country-code']['terms_set']) {
					filtersToApply['country-code']['terms_set'] = [defaultCountryCode];
				}
			}
		}
		getAdverseNewsForTable(tableData);
	}
	/*
		* @purpose: plot threat chart for report
		*  @created: 5th July 2018
		* @params: params
		* @returns: 
		* @author: Prasanthi
		*/
	// var threatsClassificatinoObj;
	function PlotChartForReport(data) {
		if (!data || data.length == 0) {
			return false;
		}
		var chartDatachildObj = {};
		chartDatachildObj['country-code'] = [];
		chartDatachildObj['attack-types'] = [];
		chartDatachildObj['technology'] = [];
		var charDataObj = {
			parentId: 0,
			id: 1,
			name: "",
			children: [],
			"data": {
				"size": 1
			}
		};
		var charDataObjThreat = {
			parentId: 0,
			id: 1,
			name: "",
			children: [],
			"data": {
				"size": 1
			}
		};
		var checkObj = {};
		checkObj['country'] = {};
		checkObj['technology'] = {};
		var idInc = 5;
		angular.forEach(data, function (val, key) {
			if (val.name == "event_location.countryCode") {
				val.name = "country-code";
			} else if (val.name == "attack_types") {
				val.name = "attack-types";
			} else if (val.name == "technologies.text") {
				val.name = "technology";
			}
			if (val.name == "country-code" || val.name == "attack-types" || val.name == "technology") {
				angular.forEach(val.buckets, function (v1, k1) {
					chartDatachildObj[val.name].push({
						"name": val.name == "country-code" ? v1.name : v1.key,
						"id": idInc,
						"parentId": (val.name == "country-code") ? 2 : (val.name == "attack-types" ? 3 : 4),
						"data": {
							"size": v1.doc_count
						}
					});
					idInc++;
				})
			}
		});
		if (chartDatachildObj['country-code'].length > 0) {
			charDataObj.children.push({
				"name": "Country",
				"id": 2,
				"parentId": 0,
				"data": {
					"size": 1
				},
				"children": chartDatachildObj['country-code']
			});
		}
		if (chartDatachildObj['attack-types'].length > 0) {
			charDataObj.children.push({
				"name": "Threats",
				"id": 3,
				"parentId": 0,
				"data": {
					"size": 1
				},
				"children": chartDatachildObj['attack-types']
			});
			charDataObjThreat.children.push({
				"name": "Threats",
				"id": 3,
				"parentId": 0,
				"data": {
					"size": 1
				},
				"children": chartDatachildObj['attack-types']
			});
		}
		if (chartDatachildObj['technology'].length > 0) {
			charDataObj.children.push({
				"name": "Technology",
				"id": 4,
				"parentId": 0,
				"data": {
					"size": 1
				},
				"children": chartDatachildObj['technology']
			});
		}
		if (chartDatachildObj['country-code'].length != 0 || chartDatachildObj['attack-types'].length != 0 || chartDatachildObj['technology'].length != 0) {
			if (charDataObjThreat.children.length > 0) {
				threatsClassificatinoObj = jQuery.extend(true, {}, charDataObjThreat);
				plotChartforThreatClassification(threatsClassificatinoObj);
			} else {
				$("#threadsClassificationChart").empty();
			}
			var options = {
				container: "#threadsKnowledgeChartforReport",
				height: 300,
				data: charDataObj,
				width: $('#threadsClassificationChart').parent().width(),
				color: ["#697AC8", "#CB524F", "#94D45B"],
				headerColor: "#A6A4A6"
			};
			treeMap(options);
			setTimeout(function () {
				$("#threadsKnowledgeChartforReport").find(".parent:first").remove();
			}, 100)
		} else {
			$("#threadsKnowledgeChartforReport").html("Data Not Found")
		}
	}
		/*
		* @purpose: to plot threat classification
		*  @created: 11th June 2018
		* @params: params
		* @returns: report (pdf)
		* @author: Prasanthi
		*/
		function plotChartforThreatClassification(data) {
			if ($scope.mainInfoTabType == 'Autodesk' && data && data.children && data.children.length > 0) {
				setTimeout(function () {
					$scope.IsthreatsClassificatin = true;
					$scope.$apply();
				}, 0);
				var options = {
					container: "#threadsClassificationChart",
					height: 160,
					data: jQuery.extend(true, {}, data),
					isChildClass: true,
					//		        width:600,
					color: ["#8C008F", "#529640", "#4866AA", "#748633", "#AF0067", "#40126A"],
					headerColor: "#A6A4A6",
					headerHeight: 0,
					clickfunction: true
				};
				treeMap(options);
				setTimeout(function () {
					$("#threadsClassificationChart").find(".parent").remove();
				}, 10)
				$("#threadsClassificationChart").find(".parent").remove();
				setTimeout(function () {
					$("#threadsClassificationChart").find(".parent").remove();
				}, 100)
			} else {
				$("#threadsClassificationChart").empty();
			}
		}
}