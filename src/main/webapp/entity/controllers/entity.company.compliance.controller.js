'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyDueDiligenceNewController', entityCompanyDueDiligenceNewController);
entityCompanyDueDiligenceNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyDueDiligenceNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
	$scope.stickyListForReport = [];
	$scope.documentsListForReport = [];
	$scope.screeningDataforReport = [];
	$scope.screeningCheckedData = [];
	$scope.reportv2Data = [];
	var worldComplianceLocationsOptionsForReport = {
		container: "#companyexampleReport",
		uri1: "../vendor/data/worldCountries.json",// Url of data
		uri2: "../vendor/data/worldMapData.json",// Url of data
		height: 700,
		markers: {}
	};
	$scope.$on('dueDiligenceOnload', function (e) {
		complianceCountriesofOperation(EntityCommonTabService.entityChartSharedObject.comapnyInfo);
		/*Initialize the sliders*/
		sliderAvgExpen($scope.sliderMinValue, $scope.sliderMaxValue);
		yearsliderfilter($scope.filteredChart.yearliderMinValue, $scope.filteredChart.yearsliderMaxValue);
		sanctionsliderfilter($scope.filteredChart.sanction_slider_min, $scope.filteredChart.sanction_slider_max);
		pepsliderfilter($scope.filteredChart.pep_slider_min, $scope.filteredChart.pep_slider_max);
		if ($scope.pageloader.treeGraphloader) {
			$scope.pageloader.disableFilters = true;
			$("#yearsliderCompliance").slider({ disabled: true });
			$("#sliderAvgExpenCompliance").slider({ disabled: true });//filter disable is off when API is completed
			$("#sanctionsliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
			$("#pepliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
		}
		if (EntityCommonTabService.lazyLoadingentity.compliancetabFirsttime) {
			getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.complianceFetchers).then(function (responsemessage) {
				var deffered = $q.defer();
				console.log('responsemessage: ', responsemessage);
				deffered.resolve($scope.entitySearchResult);
				return deffered.promise;

			}, function (err) {
				deffered.reject(error.responsemessage);
			});
			var doc_url = {
				identifier: encodeURIComponent($stateParams.query),
				type: "documents",
				jurisdiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : ''
			};
			linkcall(doc_url, "documents");

			EntityCommonTabService.lazyLoadingentity.compliancetabFirsttime = false;
		} else {
			if (EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions && EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions.markers && EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions.markers.length > 0) {
				EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions);
			}
			EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptionsAtBottom);
			EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptionsAtBottom, $scope.CountryOperations);

		}

	});
	function complianceCountriesofOperation(response) {
		var ComplianceLocations = [];
		var CountriesData = {
			allcountries: [],
			finallist: []
		}
		if (response && response.CountryOperations && response.CountryOperations.length) {
			var countryValue = response.CountryOperations;
			//				countryValue = [countryValue].flat();
			for (var index = 0; index < countryValue.length; index++) {
				var camelCase = countryValue[index].country ? countryValue[index].country.toUpperCase() : '';
				var countryName = _.find(chartsConst.countryRisk, ['COUNTRY', camelCase]);
				if (countryName) {
					countryValue[index].CountryRisk = EntityCommonTabService.calculateCountryRisk(countryName.COUNTRY);
					if (countryValue[index].CountryRisk === 'UHRC' || countryValue[index].CountryRisk === 'High') {
						$scope.entitySearchResult.list.highcountryRisk += 1;
					}
					countryValue[index].country_code = camelCase;
					countryValue[index].country_name = countryName.COUNTRY;
					countryValue[index].primary_url = countryValue[index].primarySource ? countryValue[index].primarySource : [];
					countryValue[index].secondary_url = countryValue[index].secondarySource ? countryValue[index].secondarySource : [];
					var camelCase = EntityCommonTabService.toTitleCase(countryValue[index].country_name);
					ComplianceLocations.push({ 'name': camelCase });
					CountriesData.allcountries.push(countryValue[index])
				}
			}
		}
		if (ComplianceLocations.length > 0) {
			EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions.markers = ComplianceLocations;
			var sortedCountries = sortCountryOfOperations(CountriesData.allcountries);
			$scope.CountryOperations = sortedCountries;
			$scope.CountryOperationsViewForMap = formateCountriesOperationsForWorldMapInReport($scope.CountryOperations);
			$scope.nested_countries_report_data = gorupedCountriesBasedOnSources($scope.CountryOperationsViewForMap);
		}
		EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptions);
		EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldComplianceLocationsOptionsAtBottom, $scope.CountryOperations);
		$scope.pageloader.countriesloader = false;

	}
	/* @purpose: Sorting contries of operations
		* @created:04/02/2019
		* @author: karnakar 
	*/
	function sortCountryOfOperations(countryOperations) {
		var highRiskCountries = [];
		var lowRiskCountries = [];
		var mediumRiskCountries = [];
		var uhrcRiskCountries = [];
		countryOperations.forEach(function (val) {
			if ((val.Risk && val.Risk.toLowerCase() === 'uhrc') || (val.CountryRisk && val.CountryRisk.toLowerCase() === 'uhrc')) {
				uhrcRiskCountries.push(val);
				sortEachRiskOfContries(uhrcRiskCountries);
			}
			else if ((val.Risk && val.Risk.toLowerCase() === 'high') || (val.CountryRisk && val.CountryRisk.toLowerCase() === 'high')) {
				highRiskCountries.push(val);
				sortEachRiskOfContries(highRiskCountries);
			}
			else if ((val.Risk && val.Risk.toLowerCase() === 'medium') || (val.CountryRisk && val.CountryRisk.toLowerCase() === 'medium')) {
				mediumRiskCountries.push(val);
				sortEachRiskOfContries(mediumRiskCountries);
			}
			else {
				lowRiskCountries.push(val);
				sortEachRiskOfContries(lowRiskCountries);
			}
		});
		var sortedData = uhrcRiskCountries.concat(highRiskCountries, mediumRiskCountries, lowRiskCountries);
		return sortedData;
	}
	function sortEachRiskOfContries(riskContries) {
		riskContries.sort(function (a, b) {
			if ((a.country_name && b.country_name && (a.country_name < b.country_name)) || (a.Country && b.Country && (a.Country < b.Country))) { return -1; }
			if ((a.country_name && b.country_name && (a.country_name > b.country_name)) || (a.Country && b.Country && (a.Country > b.Country))) { return 1; }
			return 0;
		});
	}
	function formateCountriesOperationsForWorldMapInReport(countriesOfOperations) {
		var tempOperationsForReport = [];
		angular.forEach(countriesOfOperations, function (val) {
			if (val.Risk == 'UHRC' || val.Risk == 'High' || val.Risk == 'Medium') {
				tempOperationsForReport.push(val);
			}
		});
		return tempOperationsForReport;
	}
	function gorupedCountriesBasedOnSources(tempOperationsForReport) {
		var groupedCountries = d3.nest()
			.key(function (d) { return d.source_url })
			.key(function (d) { return d.Risk })
			.entries(tempOperationsForReport);
		return groupedCountries;
	}
	function linkcall(url, type, json) {
		var org_structure_link = '';
		EntityApiService.complianceFieldsUrl(url, json).then(function (response) {
			// $scope.disableSearchButton = false;
			if (type == "documents") {
				var data = formatJson(response, type)
				complianceDocuments({ documents: data }); //this section is for compliance documents section 
			} //documents ends

		}, function (response) {//if API fails

		});
	} //linkcall function ends
	/* @purpose:  Company Management Data
		* 			 
		* @created: 07 may 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
	function complianceDocuments(data) {
		var documentsData = {
			alldocument: [],
			finallist: []
		}
		if (data.documents && data.documents.length > 0) {
			var addSourceKey = sourceKeys(data.documents);
			for (var i = 0; i < data.documents.length; i++) {
				if (data.documents[i] && data.documents[i].source && data.documents[i].source.toLowerCase() === 'website') {
					data.documents[i].source = $scope.ceriSearchResultObject.hasURL.value ? $scope.ceriSearchResultObject.hasURL.value : data.documents[i].source;
				}
				for (var j = 0; j < data.documents[i].value.length; j++) {
					documentsData.alldocument.push(data.documents[i].value[j]);
				}
			}
			for (var i = 0; i < documentsData.alldocument.length; i++) {
				var filtername = _.find(documentsData.finallist, { 'description': documentsData.alldocument[i].description, 'date': documentsData.alldocument[i].date })
				var filterindex = _.findIndex(documentsData.finallist, {
					description: documentsData.alldocument[i].description
				})
				if (!filtername) {
					documentsData.finallist.push(documentsData.alldocument[i]);
				} else {
					if (filtername.source_type === "primary") {
						(documentsData.finallist[filterindex].primary_url).push(documentsData.alldocument[i].source_url);
						documentsData.finallist[filterindex].primary_url = _.uniq(documentsData.finallist[filterindex].primary_url);
					} else {
						(documentsData.finallist[filterindex].secondary_url).push(documentsData.alldocument[i].source_url);
						documentsData.finallist[filterindex].secondary_url = _.uniq(documentsData.finallist[filterindex].secondary_url);
					}
				}
			}
			$scope.companyAssociatedDocDetailsInfo = documentsData.finallist;
			var sorteddate = _.sortBy(documentsData.finallist, function (value) { return new Date(value); });
			if (sorteddate.length > 0) {
				var isannualfound = false;
				angular.forEach(sorteddate, function (valX, keyX) {
					if (!isannualfound)
						if (valX.description && valX.description.toLowerCase().indexOf("annual return") == 0) {
							isannualfound = true;
							//ComplianceGetShareHoldersData(valX);
						}
				})
				if (!isannualfound) {
					//shareHolderfromMultisource();
				}
			}
		} else {
			//shareHolderfromMultisource();
		}
		$scope.pageloader.associatedDocumentloader = false;
	}
	function formatJson(response, type) {
		var data = [];
		if (response.data && response.data.data && response.data.data.results && response.data.data.results.length > 0) {
			//taking the first result
			var currentResult = response.data.data.results[0].result ? (response.data.data.results[0].result.data ? response.data.data.results[0].result.data : {}) : {};
			for (var fetcher in currentResult) {
				for (var i = 0; i < currentResult[fetcher].length; i++) {
					if (!_.isEmpty(currentResult[fetcher][i])) {
						currentResult[fetcher][i][type].identifier = response.data.data.results[0].identifier ? response.data.data.results[0].identifier : '';
						currentResult[fetcher][i][type].source = fetcher;
						var finalObject = {
							source: currentResult[fetcher][i]['bst:registryURI'] ? currentResult[fetcher][i]['bst:registryURI'] : (currentResult[fetcher][i][type].source),
							value: currentResult[fetcher][i][type]
						}
						data.push(finalObject);
					}
				}//loop each fetcher
			}//for loop each obect
		}//if conditon for data 
		else if (response && response.data && response.data.results && response.data.results.length > 0) {
			var currentResult = response.data.results[0] ? (response.data.results[0]['overview'] ? response.data.results[0]['overview'] : (type === 'documents' ? response.data.results[0].documents ? response.data.results[0].documents : {} : {})) : {};
			for (var fetcher in currentResult) {
				if (fetcher !== "comapnyInfo") {
					//	for (var i = 0; i < currentResult[fetcher].length; i++) {
					if (!_.isEmpty(currentResult[fetcher])) {
						currentResult[fetcher].identifier = response.data.results[0].identifier ? response.data.results[0].identifier : '';
						currentResult[fetcher].source = fetcher;
						var finalObject = {
							source: fetcher,
							value: currentResult[fetcher]
						};
						data.push(finalObject);
					}
				}
				//}//loop each fetcher
			}
		}
		return data;
	}
	/* @purpose:  Adding Primary or secondarykeys
 * 			 
 * @created: 07 may 2018
 * @params: type(string)
 * @returns: no
 * @author: Ram Singh */
	function sourceKeys(data) {
		if (data && data.length && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (data[i] && data[i].source && data[i].source.toLowerCase() === 'website') {
					data[i].source = $scope.ceriSearchResultObject.hasURL.value ? $scope.ceriSearchResultObject.hasURL.value : data[i].source;
				}
				if (data[i] && data[i].source && $scope.overViewDataLinksUrls[data[i].source]) {
					data[i].source = $scope.overViewDataLinksUrls[data[i].source]['bst:registryURI'] ? $scope.overViewDataLinksUrls[data[i].source]['bst:registryURI'] : data[i].source
				}
				if ((data[i].source.indexOf('companieshouse') !== -1 || (data[i].source.indexOf('ec.europa') !== -1) || (data[i].source.indexOf('business.data') !== -1) || (data[i].source.indexOf('handelsregister') !== -1))) {
					data[i].value.PrimarySource = data[i].source;
					data[i].source_type = 'primary';
					for (var j = 0; j < data[i].value.length; j++) {
						data[i].value[j].primary_url = [];
						data[i].value[j].source_type = 'primary';
						data[i].value[j].primary_url.push(data[i].source);
						data[i].value[j].source_url = data[i].source;
						data[i].value[j].conflict = [];
					}
				} else {
					data[i].value.SecondarySource = data[i].source;
					data[i].source_type = 'secondary';
					for (var j = 0; j < data[i].value.length; j++) {
						data[i].value[j].secondary_url = [];
						data[i].value.PrimarySource = data[i].source;
						data[i].value[j].source_type = 'secondary';
						if ((data[i].source).toLowerCase() === 'bst') {
							data[i].value[j].secondary_url = data[i].value[j] ? (data[i].value[j]['source-url'] ? (JSON.parse(data[i].value[j]['source-url'])).results : []) : [];
						} else {
							data[i].value[j].secondary_url.push(data[i].source);
						}
						data[i].value[j].source_url = data[i].source;
						data[i].value[j].conflict = [];
					}
				}
			}
			return data;
		}
	}
	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company',
				limit: 1
			};
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						if (subfetcherData.fetcher === 'logo.google.com') {
							$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
						}
						if (key === fetcherIds.length - 1) {
							deffered.resolve($scope.entitySearchResult);
						}
					}

				});
			});
		});
		return deffered.promise;
	}
	/* @purpose: Initialize the value of Slider
* @created: 3 oct 2018
* @returns: none 
* @author:Ram
*/
	function yearsliderfilter(min, max) {
		$scope.filteredChart.yearliderMinValue = min;
		$scope.filteredChart.yearsliderMaxValue = max;
		$("#yearsliderCompliance").find(".price-range-min").html(min);
		$("#yearsliderCompliance").find(".price-range-max").html(max);
		$('#yearsliderCompliance').slider({
			range: true,
			min: 0,
			max: 7,
			values: [min, max],
			slide: function (event, ui) {
				adverseNewsDatefilter(ui.values[0], ui.values[1])
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#yearsliderCompliance .price-range-both i').css('display', 'none');
				} else {
					$('#yearsliderCompliance .price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#yearsliderCompliance .price-range-min, #yearsliderCompliance .price-range-max').css('opacity', '0');
					$('#yearsliderCompliance .price-range-both').css('display', 'block');
				} else {
					$('#yearsliderCompliance .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#yearsliderCompliance .price-range-both').css('display', 'none');
				}
			}
		});
	}
	function pepsliderfilter(min, max) {
		$scope.filteredChart.pep_slider_min = min;
		$scope.filteredChart.pep_slider_max = max;
		$("#pepliderCompliance").find(".price-range-min").html(min);
		$("#pepliderCompliance").find(".price-range-max").html(max);
		$('#pepliderCompliance').slider({
			range: true,
			min: 0,
			max: 100,
			values: [min, max],
			slide: function (event, ui) {
				percentageRange(ui.values, 'pep');
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#pepliderCompliance .price-range-both i').css('display', 'none');
				} else {
					$('#pepliderCompliance .price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#pepliderCompliance .price-range-min, #pepliderCompliance .price-range-max').css('opacity', '0');
					$('#pepliderCompliance .price-range-both').css('display', 'block');
				} else {
					$('#pepliderCompliance .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#pepliderCompliance .price-range-both').css('display', 'none');
				}
			}
		});
	}
	function sanctionsliderfilter(min, max) {
		$scope.filteredChart.sanction_slider_min = min;
		$scope.filteredChart.sanction_slider_max = max;
		$("#sanctionsliderCompliance").find(".price-range-min").html(min);
		$("#sanctionsliderCompliance").find(".price-range-max").html(max);
		$('#sanctionsliderCompliance').slider({
			range: true,
			min: 0,
			max: 100,
			values: [min, max],
			slide: function (event, ui) {
				percentageRange(ui.values, 'sanction');
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#sanctionsliderCompliance .price-range-both i').css('display', 'none');
				} else {
					$('#sanctionsliderCompliance .price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#sanctionsliderCompliance .price-range-min, #sanctionsliderCompliance .price-range-max').css('opacity', '0');
					$('#sanctionsliderCompliance .price-range-both').css('display', 'block');
				} else {
					$('#sanctionsliderCompliance .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sanctionsliderCompliance .price-range-both').css('display', 'none');
				}
			}
		});
	}
	/* @purpose: Initialize the value of Slider
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function sliderAvgExpen(min, max) {
		$scope.sliderMinValue = min;
		$scope.sliderMaxValue = max;
		$("#sliderAvgExpenCompliance").find(".price-range-min").html(min);
		$("#sliderAvgExpenCompliance").find(".price-range-max").html(max);
		$('#sliderAvgExpenCompliance').slider({
			range: true,
			min: 0,
			max: 100,
			values: [min, max],
			slide: function (event, ui) {
				percentageRange(ui.values, false);
				$scope.activateDisablebutton(ui.values[0], ui.values[1]);
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#sliderAvgExpenCompliance .price-range-both i').css('display', 'none');
				} else {
					$('#sliderAvgExpenCompliance .price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#sliderAvgExpenCompliance .price-range-min, #sliderAvgExpenCompliance .price-range-max').css('opacity', '0');
					$('#sliderAvgExpenCompliance .price-range-both').css('display', 'block');
				} else {
					$('#sliderAvgExpenCompliance .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sliderAvgExpenCompliance .price-range-both').css('display', 'none');
				}
			}
		});
	}
	/* @purpose: onchange of risk value initalize the slider values
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.riskChange = function () {
		if ($scope.filteredChart.riskRatio !== 1) {
			$timeout(function () {
				sliderAvgExpen(25, 100);
			}, 500);
		}
		else {
			sliderAvgExpen(10, 100);
			//				$( "#sliderAvgExpen" ).slider( "destroy" );
		}
		$scope.activateDisablebutton($scope.sliderMinValue, $scope.sliderMaxValue);
	}
	/* @purpose: Slider function hides when slideer key collide
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function collision($div1, $div2) {
		//var x1 = $div1.offset().left;
		var x1 = $div1;
		var w1 = 0.5;
		var r1 = x1 + w1;
		//var x2 = $div2.offset().left;
		var x2 = $div2;
		var w2 = 0.5;
		var r2 = x2 + w2;
		if (r1 < x2 || x1 > r2) return false;
		return true;
	}
		/* @purpose: get the values of slider and show on the slider move
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function percentageRange(values, yearslider) {
		if (yearslider && yearslider === 'sanction') {
			$timeout(function () {
				$scope.filteredChart.sanction_slider_min = values[0];
				$scope.filteredChart.sanction_slider_max = values[1];
				$scope.checkenableOrDisableApplybutton();
			}, 0);
		} else if (yearslider && yearslider === 'pep') {
			$timeout(function () {
				$scope.filteredChart.pep_slider_min = values[0];
				$scope.filteredChart.pep_slider_max = values[1];
				$scope.checkenableOrDisableApplybutton();
			}, 0);
		}
		else {
			$timeout(function () {
				$("#sliderAvgExpenCompliance").find(".price-range-min").html(values[0]);
				$("#sliderAvgExpenCompliance").find(".price-range-max").html(values[1]);
			}, 0);
		}
	}
	function adverseNewsDatefilter(from, to) {
		var today = new Date();
		$scope.filteredChart.startYear = from;
		$scope.filteredChart.endYear = to;
		var EndDate = (today.getFullYear() - from) + '-' + (("0" + (today.getMonth() + 1)).slice(-2)) + '-' + (("0" + (today.getDate())).slice(-2));
		$scope.filteredChart.fromyear = EndDate;
		var startDate = (today.getFullYear() - to) + '-' + (("0" + (today.getMonth() + 1)).slice(-2)) + '-' + (("0" + (today.getDate())).slice(-2));
		$scope.filteredChart.toyear = startDate;
		$("#yearsliderCompliance").find(".price-range-min").html(from);
		$("#yearsliderCompliance").find(".price-range-max").html(to);
		$scope.activateDisablebutton(from, to, true);
	}
	$scope.activateDisablebutton = function (val1, val2, yearslider) {
		if (yearslider) {
			$timeout(function () {
				$scope.filteredChart.yearliderMinValue = val1;
				$scope.filteredChart.yearsliderMaxValue = val2;
			}, 0);
		} else {
			$scope.sliderMinValue = val1;
			$scope.sliderMaxValue = val2;
		}
		$scope.checkenableOrDisableApplybutton();
	}//function ends
		/* @purpose: check to Enable or Diable APply button
		* @created: 3 oct 2018
		* @returns: none 
		* @author:Ram
		*/
		$scope.checkenableOrDisableApplybutton = function () {
			var EndDate = ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + new Date().getDate());
			var startDate = ((new Date().getFullYear() - 2) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + new Date().getDate());
			if (($scope.filteredChart.selectedNumberofLevel !== 5) || ($scope.filteredChart.riskRatio !== 1) || ($scope.filteredChart.numberofcomapnies !== 5) || ($scope.sliderMinValue !== $scope.appliedOwnerShipControlPercentageMinVal) || ($scope.sliderMaxValue !== $scope.appliedOwnerShipControlPercentageMaxVal) || ($scope.filteredChart.fromyear !== EndDate) || ($scope.filteredChart.toyear !== startDate) || $scope.showSubsidaries) {
				$scope.filteredChart.applyButton = false;
				$scope.filteredChart.not_screening = true;
			} else if ($scope.filteredChart.pep_slider_min !== 85 || $scope.filteredChart.pep_slider_max !== 100 || $scope.filteredChart.sanction_slider_min !== 70 || $scope.filteredChart.sanction_slider_max !== 100) {
				$scope.filteredChart.applyButton = false;
				$scope.filteredChart.not_screening = false;
			} else {
				$scope.filteredChart.applyButton = true;
				$scope.filteredChart.not_screening = true;
			}
		};

/* ======================generate report starts===============================*/
var sortFunction = function (a, b) {
	return a.createdDate > b.createdDate ? -1 : 1;
};
			/*
		* @purpose: generate report for EDD
		*  @created: 11th June 2018
		* @params: params
		* @returns: report (pdf)
		* @author: Prasanthi
		*/
	$window.generateReport = function () {
		// entityCompanyNewController($scope);
		$scope.$parent.pdfLoader = true;
		var ajax1 = getEntityQuestionsByEntityId();
		var ajax2 = getAllSignificantNewsIn();
		var ajax3 = getAllDocuments();
		var ajax4 = getAllStickyNotes();
		$q.all([ajax1, ajax2, ajax3, ajax4]).then(function (resp) {
			if (_.every([ajax1, ajax2, ajax3, ajax4])) {
				$window.generateReportPdf();
			}
			auditLogGeneratedReport();
			/* all done, hide loader*/
		}, function (err) {
			//			$window.generateReportPdf();
			console.log("error cannot be generated : ", err);
		});
	}
	/*
     * @purpose:get Entity Questions By Entity Id
     * @created: 8th feb 2019
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
	$scope.generateReportData = [];
	$scope.genpact_summaryresults = [];
	function getEntityQuestionsByEntityId() {
		return new Promise(function (resolve, reject) {
			EntityApiService.getEntityQuestionsByEntityId($scope.entitySearchResult["@identifier"]).then(function (response) {
				if (response && response.data) {
					if (response.data.length > 0 && response.data[response.data.length - 1].questionName == "Entity Name") {
						response.data = response.data.reverse();
					}
					// try{
					$scope.generateReportData = [];
					$scope.genpact_summaryresults = [];
					if ($rootScope.complianceReport || $rootScope.genpactReportV2) {
						var industryIndex = $scope.conplianceMapKeys.findIndex(function (d) { return d.key == 'industryType' });
						var industryObj = $scope.conplianceMapKeys[industryIndex];
						var industrydata = $scope.ceriSearchResultObject[industryObj.key];
					}
					var nestedData = d3.nest()
						.key(function (d) { return d.groupName; })
						.key(function (d) { return d.questionName; })
						.entries(response.data);
					angular.forEach(nestedData, function (val1, key1) {
						angular.forEach(val1.values, function (val2, key2) {
							val2.values = val2.values.sort(sortFunction);
						})
					})
					var finalData = {};
					nestedData.map(function (d) {
						var tempData = d.values.map(function (obj) {
							obj.values.map(function (val) {
								if (val.questionOptions) {
									val.questionOptions = JSON.parse(val.questionOptions);
								}
							});
							return obj;
						});
						finalData[d.key] = tempData;
						// finalData[d.key]=d.values;
					});
					if (finalData['#Principals'] || finalData['#Principals- Only applicable if country risk is High']) {
						finalData['#Principals'] = finalData['#Principals'] ? finalData['#Principals'] : finalData['#Principals- Only applicable if country risk is High'];
					}
					finalData['#Customer Related Information - Business Activities'] = finalData['#Customer Related Information - Business Activities'] ? finalData['#Customer Related Information - Business Activities'] : [];
					finalData['#Customer Related Information - Nature of Relationship & Third Party Identification'] = finalData['#Customer Related Information - Nature of Relationship & Third Party Identification'] ? finalData['#Customer Related Information - Nature of Relationship & Third Party Identification'] : [];
					finalData['#Customer Related Information - Source of Funds'] = finalData['#Customer Related Information - Source of Funds'] ? finalData['#Customer Related Information - Source of Funds'] : [];
					finalData['#KYC Representative'] = finalData['#KYC Representative'] ? finalData['#KYC Representative'] : [];
					// concatinating due_deligance into one attibute
					var standard_due_deligance = finalData['#Customer Related Information - Business Activities'].concat(finalData['#Customer Related Information - Nature of Relationship & Third Party Identification'], finalData['#Customer Related Information - Source of Funds'], finalData['#KYC Representative'], finalData['#Proof of Listing'], finalData['#Proof of Regulation']);
					finalData['#Standard Due Diligence: Identity and Legal Existence'] = standard_due_deligance;
					$scope.generateReportData = finalData;
					if ($rootScope.complianceReport) {
						var customerOutreachdata = false;
						if ($scope.createNewArrayReportData && $scope.createNewArrayReportData.length > 0) {
							customerOutreachdata = true;
						}
						$scope.genpact_summaryresults.push({
							questionAnswer: (customerOutreachdata) ? 'Yes' : 'No',
							questionName: 'Out Reach Required:',
							questionOptions: [{ text: 'Yes', selected: ((customerOutreachdata) ? 'Yes' : '') },
							{ text: 'No', selected: ((!customerOutreachdata) ? 'No' : '') }],
							questionType: 'radioButton'
						});
						// 
					}
					if ($rootScope.genpactReportV2) {
						generateGenpactReportv2();
					} else {
						angular.forEach($scope.conplianceMapKeysReport, function (mapkeys, mapindex) {
							if ($scope.ceriSearchResultObject[mapkeys.key]) {
								$scope.ceriSearchResultObject[mapkeys.key] = addingIsTackenScreenShotKeyForOldReport($scope.ceriSearchResultObject[mapkeys.key], 2, "oldreport");
							}
						});
					}
				}
				// getAllSignificantNewsIn();
				resolve(true);
			}, function (error) {
				// getAllSignificantNewsIn();
				reject(false);
			});
		});
	}
	/*
     * @purpose: getAllSignificantNewsIn
     * @created: 07th Mar 2019
     * @params: params
     * @returns: success error functions
     * @author: karunakar
     */
	function getAllSignificantNewsIn() {
		var params = {
			identifier: $scope.entitySearchResult["@identifier"]
		};
		return new Promise(function (resolve, reject) {
			EntityApiService.getAllSignificantNews(params).then(function (response) {
				if (response.data) {
					$scope.generateReportofficernews = response.data;
					$scope.generateReportofficernews.map(function (d) {
						d.search_query = d.results.search_query;
					});
					$scope.generateReportofficernewsByGroup = d3.nest().key(function (d) { return d.search_query }).entries($scope.generateReportofficernews);
				}
				// $window.generateReportPdf() ;
				// 
				resolve(true);
			}, function (failure) {
				// $window.generateReportPdf();
				reject(false);
			});
		});
	}
	/*
	* @purpose : Get all documents list 
	* @author : Amarjith Kumar
	* @Date : 30-March-2019
	*/
	
	function getAllDocuments() {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": 9,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		$scope.documentsListForReport = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllDocuments(params).then(function (res) {
				var reportDocumentpromise = getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'docs');
				reportDocumentpromise.then(function (response) {
					getEvidenceListDocs(params).then(function (response) {
						resolve(true);
					});
				});
			}).catch(function (err) {
				reject(false);
			});
		});
	}
		/*
	* @purpose : Get all evidence documents list 
	* @author : karnakar
	* @Date : 01-july-2019
	*/
	function getEvidenceListDocs(paramFromApiFunc) {
		var params = paramFromApiFunc;
		params.docFlag = 6;
		// $scope.documentsListForReport = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllEvidenceDocuments(params).then(function (res) {
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'evidence_docs').then(function (res) {
					resolve(true);
				});
			}).catch(function (err) {
				reject(false);
			});
		});
	}
	function getAllStickyNotes() {
		var params = {
			"token": $rootScope.ehubObject.token,
			"docFlag": 5,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": 9,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		// $scope.documentsListForReport = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllDocuments(params).then(function (res) {
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'sticky').then(function (res) {
					resolve(true);
				});
			}).catch(function (err) {
				reject(false)
			});
		});
	}
		/*
	*@function to generate genpact v2 report
	*
	*/
	function generateGenpactReportv2() {
		$scope.reportv2Data = utilityConstant.reportv2Data;
		// Source of Funds checklist answer
		if ($scope.generateReportData['#Source of Funds']) {
			var sourceFundOutreachAns = null;
			sourceFundOutreachAns = getCheckListAnswer($scope.generateReportData['#Source of Funds'], 'Is the source of fund info available from public sources.');
			$scope.reportv2Data['sourceOfFundOutreach'] = sourceFundOutreachAns.text ? sourceFundOutreachAns.text : "";
			var sourceFundOutreachInfoAns = null;
			sourceFundOutreachInfoAns = getCheckListAnswer($scope.generateReportData['#Source of Funds'], 'Please provide the details');
			$scope.reportv2Data['sourceOfFundOutreachInfo'] = sourceFundOutreachInfoAns ? sourceFundOutreachInfoAns : "";
		}
		// Main Principle creating table from checklist
		if ($scope.generateReportData['#Main Principals']) {
			var sourceFundOutreachAns = '';
			var main_principal_identified = getCheckListAnswer($scope.generateReportData['#Main Principals'], 'All the main principals identified?');
			if (main_principal_identified && main_principal_identified.text && main_principal_identified.text.toLowerCase() === 'yes') {
				sourceFundOutreachAns = getCheckListAnswer($scope.generateReportData['#Main Principals'], 'Please mention Main Principals information');
			} else if (main_principal_identified && main_principal_identified.text && main_principal_identified.text.toLowerCase() === 'partial') {
				sourceFundOutreachAns = getCheckListAnswer($scope.generateReportData['#Main Principals'], 'Please state the document/s required from client to capture Main Principal info');
			} else if (main_principal_identified && main_principal_identified.text && main_principal_identified.text.toLowerCase() === 'no') {
				sourceFundOutreachAns = getCheckListAnswer($scope.generateReportData['#Main Principals'], 'Please state the document/s required from client to capture Main Principal info');
			}
			var main_principalsDatafilter = $scope.screeningData.filter(function (d) {
				return d.classification && d.classification.length && d.classification.includes('Main Prinicipals');
			});
			$scope.reportv2Data['main_priociples_data'] = main_principalsDatafilter;
		};
		var principalsDatafilter = $scope.screeningData.filter(function (d) {
			return d.classification && d.classification.length && d.classification.includes("Principals");
		});
		$scope.reportv2Data['Principals_Details_High_Risk'] = principalsDatafilter;
		var ubo_psudeoUbo_Datafilter = $scope.screeningData.filter(function (d) {
			return d.classification && d.classification.length && (d.classification.includes("UBO") || d.classification.includes("Pesudo UBO"));
		});
		$scope.reportv2Data['UBO_Details'] = ubo_psudeoUbo_Datafilter;
		//legal existence section details;
		if ($scope.generateReportData['#Legal Existence']) {
			var legal_existenceAns = getCheckListAnswer($scope.generateReportData['#Legal Existence'], 'Legal existence verified?');
			var legal_existenceVerified_Ans = getCheckListAnswer($scope.generateReportData['#Legal Existence'], 'Verification Details');
			var trade_registration_Ans = getCheckListAnswer($scope.generateReportData['#Legal Existence'], 'Grid ID');
			$scope.reportv2Data['legal_existence_verified'] = {
				question: 'Legal existence verified?',
				answer: legal_existenceAns ? legal_existenceAns.text : ''
			}
			$scope.reportv2Data['legal_existence_verificationDetails'] = {
				question: 'Verification Details',
				answer: legal_existenceVerified_Ans ? legal_existenceVerified_Ans : ''
			}
			$scope.reportv2Data['#Trade_Registration_No'] = trade_registration_Ans ? trade_registration_Ans : '';
		}
		// Associated parties details section
		if ($scope.generateReportData['#Associated Parties']) {
			var assc_partiesquesAns = getCheckListAnswer($scope.generateReportData['#Associated Parties'], 'Are there any Intermediate Parents or related parties identified in the ownership structure ?');
			$scope.reportv2Data['Associated_Parties_QuesAns'] = {
				question: 'Are there any Intermediate Parents or related parties identified in the ownership structure ?',
				answer: assc_partiesquesAns ? assc_partiesquesAns.text : ''
			}
		}
		// Set and get Outreach Required 'yes' or 'no'
		if ($scope.generateReportData['#Ownership Structure'] || $scope.generateReportData['#Source of Funds'] || $scope.generateReportData['#Complex Ownership Structure']) {
			var ownership_ans = getCheckListAnswer($scope.generateReportData['#Ownership Structure'], 'Is the ownership identified upto the UBO level?', 'Outreach Required');
			var complex_ownership_ans = getCheckListAnswer($scope.generateReportData['#Complex Ownership Structure'], 'Is there a complex ownership structure identified?', 'Outreach Required');
			var sorceofFunds_ans = getCheckListAnswer($scope.generateReportData['#Source of Funds'], 'Is the source of fund info available from public sources.', 'Outreach Required');
			ownership_ans = ownership_ans ? ownership_ans.text : '';
			complex_ownership_ans = complex_ownership_ans ? complex_ownership_ans.text : '';
			sorceofFunds_ans = sorceofFunds_ans ? sorceofFunds_ans.text : '';
			var concatoutreach = [ownership_ans, complex_ownership_ans, sorceofFunds_ans];
			if (concatoutreach.indexOf('Outreach Required') !== -1) {
				$scope.reportv2Data['outreach_required'] = 'Yes';
			} else {
				$scope.reportv2Data['outreach_required'] = 'No';
			}
		}
		// get level of due diligence value from checklist
		if ($scope.generateReportData['#Profile Summary']) {
			var level_due_diligence = getCheckListAnswer($scope.generateReportData['#Profile Summary'], 'Level of Due Diligence Applied', 'Level of Due Diligence');
			$scope.reportv2Data['level_due_diligence'] = level_due_diligence ? level_due_diligence.value : '';
		}
		// get mlro_management_approval value from checklist
		if ($scope.generateReportData['#Screening']) {
			var mlro_required = getCheckListAnswer($scope.generateReportData['#Screening'], 'MLRO involvement or Senior Management approval required ?', 'Yes');
			$scope.reportv2Data['mlro_management_approval'] = mlro_required ? mlro_required.text : 'No';
		}
		//get regulated by , stock exchane and ticker code from checkist
		if ($scope.generateReportData['#Listing & Regulation Status']) {
			var regulated = getCheckListAnswer($scope.generateReportData['#Listing & Regulation Status'], 'Name of the regulator');
			$scope.reportv2Data['regulated_by'] = regulated ? regulated : '';
			var stock_exchange = getCheckListAnswer($scope.generateReportData['#Listing & Regulation Status'], 'Name of the stock exchange');
			$scope.reportv2Data['stock_exchange'] = stock_exchange ? stock_exchange : '';
			var ticker_code = getCheckListAnswer($scope.generateReportData['#Listing & Regulation Status'], 'Ticker code of the stock listing');
			$scope.reportv2Data['ticker_code'] = ticker_code ? ticker_code : '';
		}
		// get country risk and risk rating
		if ($scope.generateReportData['#INITIAL_RISK_RATING']) {
			var risk_level_ans = getCheckListAnswer($scope.generateReportData['#INITIAL_RISK_RATING'], 'Initial Risk Rating per Risk Classifcation Methodology.');
			$scope.reportv2Data['#RiskRating'] = risk_level_ans ? risk_level_ans.text : '';
			var countryrisk_ans = getCheckListAnswer($scope.generateReportData['#INITIAL_RISK_RATING'], 'Select Country Risk:');
			$scope.reportv2Data['#CountryRisk'] = countryrisk_ans ? countryrisk_ans.text : '';
		}
		// get legal_ultimate_parent from ownership checklist
		if ($scope.generateReportData['#Ownership Structure']) {
			var legal_ultimate_parent = getCheckListAnswer($scope.generateReportData['#Ownership Structure'], 'Details of ultimate parent/s or holding company');
			$scope.reportv2Data['legal_ultimate_parent'] = legal_ultimate_parent ? legal_ultimate_parent : "";
		}
		// get uhrc from checklist
		var uhrc_nexus = $scope.generateReportData['#UHRC'] ? $scope.generateReportData['#UHRC'] : [];
		if (uhrc_nexus && uhrc_nexus.length) {
			angular.forEach(uhrc_nexus, function (val, key) {
				angular.forEach(val.values, function (val1, key1) {
					if (val1.questionType === "radioButton") {
						$scope.reportv2Data['#UHRC'].selected = val1.questionOptions.find(function (opt) {
							return opt.selected
						}).text;
					}
					if (val1.questionType === "checklist") {
						$scope.reportv2Data['#UHRC'].values = val1.questionOptions ? val1.questionOptions.map(function (d) { return d.selected }) : [];
						$scope.reportv2Data['#UHRC'].values = _.compact($scope.reportv2Data['#UHRC'].values);
					}
				});
			});
		}
		var cdd_proces_and_cdd_results_keys = [
			{ label: 'Level of Due Diligence', value: $scope.reportv2Data['level_due_diligence'] },
			{ label: 'Initial Risk Rating', value: $scope.reportv2Data['#RiskRating'] },
			{ label: 'Outreach Required', value: ($scope.reportv2Data['outreach_required'] ? 'Yes' : 'No') },
			// {label: 'MLRO/Management Approval',value: $scope.reportv2Data['mlro_management_approval']},
			{ label: 'MLRO/Management Approval', value: "" },
			{ label: 'Analyst Name', value: $scope.ehubObject.fullName },
			{ label: 'Processing Date', value: moment().format('MM-DD-YYYY') }
		];
		$scope.reportv2Data['CDD_Process_and_CDD_Results_Fields'] = _.chunk(cdd_proces_and_cdd_results_keys, 3); // 3=> size to chunk the array data
		// get Party/Customer Product Information
		var ing_product_types = [];
		if ($scope.generateReportData['#Party/Customer Product Information']) {
			var productTypeAnswer = getCheckListAnswer($scope.generateReportData['#Party/Customer Product Information'], 'Please provide the product related information if received from ING');
			productTypeAnswer = productTypeAnswer ? productTypeAnswer.text : "";
			if (productTypeAnswer.toLowerCase() == "yes") {
				ing_product_types = getCheckListAnswer($scope.generateReportData['#Party/Customer Product Information'], 'High Risk Product', '', 'productSection');
			}
		}
		var information_recevied_from_ing_keys = [
			{ label: 'Name of Client', value: ($scope.ceriSearchResultObject['vcard:organization-name'].value ? $scope.ceriSearchResultObject['vcard:organization-name'].value : '') },
			{ label: 'Registered Address', value: ($scope.ceriSearchResultObject['fullAddress'].value ? $scope.ceriSearchResultObject['fullAddress'].value : '') },
			{ label: 'Product Type', value: (ing_product_types && ing_product_types.length ? ing_product_types : []) }
		];
		$scope.reportv2Data['information_recevied_from_ing'] = _.chunk(information_recevied_from_ing_keys, 3);
		var party_information_data = [
			{
				label: 'Legal Name',
				value: getSourceInfo($scope.ceriSearchResultObject['vcard:organization-name']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['vcard:organization-name']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['vcard:organization-name']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['vcard:organization-name']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['vcard:organization-name']).docId
			},
			{
				label: 'Legal Ultimate Parent',
				value: $scope.reportv2Data['legal_ultimate_parent'],
				source: '',
				sourceUrl: '',
				sourceType: '',
				docId: ''
			},
			{
				label: 'Trading Name/Alias Name',
				value: getSourceInfo($scope.ceriSearchResultObject['bst:aka']).value.toString(),
				source: getSourceInfo($scope.ceriSearchResultObject['bst:aka']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['bst:aka']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['bst:aka']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['bst:aka']).docId
			},
			{
				label: 'Company Status',
				value: getSourceInfo($scope.ceriSearchResultObject['hasActivityStatus']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['hasActivityStatus']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['hasActivityStatus']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['hasActivityStatus']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['hasActivityStatus']).docId
			},
			{
				label: 'Legal Form/Type',
				value: getSourceInfo($scope.ceriSearchResultObject['lei:legalForm']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['lei:legalForm']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['lei:legalForm']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['lei:legalForm']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['lei:legalForm']).docId
			},
			{
				label: 'Industry',
				value: getSourceInfo($scope.ceriSearchResultObject['industryType']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['industryType']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['industryType']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['industryType']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['industryType']).docId
			},
			{
				label: 'Regulated By',
				value: getSourceInfo($scope.ceriSearchResultObject['RegulationStatus']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['RegulationStatus']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['RegulationStatus']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['RegulationStatus']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['RegulationStatus']).docId
			},
			{
				label: 'Listed on Stock Ex',
				value: getSourceInfo($scope.ceriSearchResultObject['main_exchange']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['main_exchange']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['main_exchange']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['main_exchange']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['main_exchange']).docId
			},
			{
				label: 'Website',
				value: getSourceInfo($scope.ceriSearchResultObject['hasURL']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['hasURL']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['hasURL']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['hasURL']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['hasURL']).docId
			},
			{
				label: 'Ticker Code',
				value: getSourceInfo($scope.ceriSearchResultObject['ticket_symbol']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['ticket_symbol']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['ticket_symbol']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['ticket_symbol']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['ticket_symbol']).docId
			},
			{
				label: 'Swift Codes',
				value: getSourceInfo($scope.ceriSearchResultObject['swift_code']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['swift_code']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['swift_code']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['swift_code']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['swift_code']).docId
			},
		];
		$scope.reportv2Data['party_information'] = _.chunk(party_information_data, 2);
		var party_address_details_data = [
			{
				label: 'Street Name and Number',
				value: getSourceInfo($scope.ceriSearchResultObject['streetAddress']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['streetAddress']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['streetAddress']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['streetAddress']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['streetAddress']).docId
			},
			{
				label: 'ZIP Code/Postal Code',
				value: getSourceInfo($scope.ceriSearchResultObject['zip']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['zip']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['zip']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['zip']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['zip']).docId
			},
			{
				label: 'City/Town',
				value: getSourceInfo($scope.ceriSearchResultObject['city']).value.toString(),
				source: getSourceInfo($scope.ceriSearchResultObject['city']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['city']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['city']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['city']).docId
			},
			{
				label: 'Country',
				value: getSourceInfo($scope.ceriSearchResultObject['country']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['country']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['country']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['country']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['country']).docId
			},
			{
				label: 'Country Risk Level',
				value: $scope.reportv2Data['#CountryRisk'],
				source: '',
				sourceUrl: '',
				sourceType: '',
				docId: ''
			}
		];
		$scope.reportv2Data['Party_Address_Details'] = _.chunk(party_address_details_data, 2);
		var party_registration_details_data = [
			{
				label: 'Trade Registration',
				value: getSourceInfo($scope.ceriSearchResultObject['trade_register_number']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['trade_register_number']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['trade_register_number']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['trade_register_number']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['trade_register_number']).docId
			},
			{
				label: 'TIN/VAT',
				value: getSourceInfo($scope.ceriSearchResultObject['vat_tax_number']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['vat_tax_number']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['vat_tax_number']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['vat_tax_number']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['vat_tax_number']).docId
			},
			{
				label: 'LEI',
				value: getSourceInfo($scope.ceriSearchResultObject['legal_entity_identifier']).value.toString(),
				source: getSourceInfo($scope.ceriSearchResultObject['legal_entity_identifier']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['legal_entity_identifier']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['legal_entity_identifier']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['legal_entity_identifier']).docId
			},
			{
				label: 'Date of Incorporation',
				value: getSourceInfo($scope.ceriSearchResultObject['isIncorporatedIn']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['isIncorporatedIn']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['isIncorporatedIn']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['isIncorporatedIn']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['isIncorporatedIn']).docId
			},
			{
				label: 'ISIN #',
				value: getSourceInfo($scope.ceriSearchResultObject['international_securities_identifier']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['international_securities_identifier']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['international_securities_identifier']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['international_securities_identifier']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['international_securities_identifier']).docId
			},
			{
				label: 'Phone #',
				value: getSourceInfo($scope.ceriSearchResultObject['tr-org:hasHeadquartersPhoneNumber']).value,
				source: getSourceInfo($scope.ceriSearchResultObject['tr-org:hasHeadquartersPhoneNumber']).source,
				sourceUrl: getSourceInfo($scope.ceriSearchResultObject['tr-org:hasHeadquartersPhoneNumber']).sourceUrl,
				sourceType: getSourceInfo($scope.ceriSearchResultObject['tr-org:hasHeadquartersPhoneNumber']).sourceType,
				docId: getSourceInfo($scope.ceriSearchResultObject['tr-org:hasHeadquartersPhoneNumber']).docId
			}
		];
		$scope.reportv2Data['Registration_and_TAX_Details'] = _.chunk(party_registration_details_data, 3);
		var LegalQuestionnaire = $scope.generateReportData['#Profile Summary'] ? $scope.generateReportData['#Profile Summary'].map(function (d) { return d.values }).flat() : [];
		$scope.reportv2Data['#LegalQuestionnaire'] = getJsonParse(LegalQuestionnaire).reverse();
		$scope.reportv2Data['associated_parties'] = $scope.screeningData.filter(function (d) { return ((d.entity_id && d.entity_id === "orgChartParentEntity") && (_.find(EntityCommonTabService.basicsharedObject.entites_renderedIn_chart, { 'name': d.name }))) });
		// sources of funds
		var companyDocuments = jQuery.extend(true, [], $scope.companyAssociatedDocDetailsInfo);
		$scope.reportv2Data['sourcesOfFunds'] = companyDocuments.sort(function (a, b) {
			if (b.date && a.date) {
				return new Date(b.date) - new Date(a.date);
			}
		});
		$scope.reportv2Data['sourcesOfFunds'] = $scope.reportv2Data['sourcesOfFunds'] ? $scope.reportv2Data['sourcesOfFunds'].filter(function (d) {
			if (d.description) {
				return (d.description.includes('Annual') || d.description.includes('Annual Report') || d.description.includes('Annual Return') || d.description.includes('BVD') || d.description.includes('BA'))
			}
		}) : [];
		$scope.reportv2Data['sourcesOfFunds'] = $scope.reportv2Data['sourcesOfFunds'] && $scope.reportv2Data['sourcesOfFunds'].length ? [$scope.reportv2Data['sourcesOfFunds'][0]] : [];
		$scope.reportv2Data['ubo_greater_threshold'] = $scope.screeningData.some(function (d) {
			return (d.Ubo_ibo && d.Ubo_ibo.toLowerCase().trim() === "ubo") && (d.indirectPercentage && d.indirectPercentage > $scope.sliderMinValue);
		});
		if ($scope.generateReportData['#Complex Ownership Structure']) {
			var complex_structure = $scope.generateReportData['#Complex Ownership Structure'].find(function (d) { return d.key == 'Which of the following complex ownership parameters are identified.' });
			if (complex_structure) {
				$scope.reportv2Data['complex ownership structure'] = getJsonParse(complex_structure.values);
				$scope.reportv2Data['complex ownership structure'] = _.groupBy($scope.reportv2Data['complex ownership structure'], function (d) { return d.questionName });
			} else {
				$scope.reportv2Data['complex ownership structure'] = [];
			}
		}
		if ($scope.generateReportData['#Party/Customer Product Information']) {
			var dataObj = $scope.generateReportData['#Party/Customer Product Information'].find(function (d) { return d.key == 'High Risk Product' });
			if (dataObj && Object.keys(dataObj).length) {
				var parseData = getJsonParse(dataObj.values);
				var groupData = _.groupBy(parseData, function (d) { return d.questionName });
				$scope.reportv2Data['#Party/Customer Product Information'] = groupData
			} else {
				$scope.reportv2Data['#Party/Customer Product Information'] = [];
			}
			var dataAns = getCheckListAnswer($scope.generateReportData['#Party/Customer Product Information'], 'Please provide the product related information if received from ING');
			$scope.reportv2Data['#Party/Customer_Product_InformationQuestionAns'] = dataAns ? dataAns.text : 'No';
		}
		if ($scope.generateReportData['#Party/Customer Activity – High Risk']) {
			var dataObj = $scope.generateReportData['#Party/Customer Activity – High Risk'].find(function (d) { return d.key == 'Activity Type' });
			if (dataObj && Object.keys(dataObj).length) {
				var parseData = getJsonParse(dataObj.values);
				var groupData = _.groupBy(parseData, function (d) { return d.questionName });
				$scope.reportv2Data['#Party/Customer Activity – High Risk'] = groupData
			} else {
				$scope.reportv2Data['#Party/Customer Activity – High Risk'] = [];
			}
			var dataAns = getCheckListAnswer($scope.generateReportData['#Party/Customer Activity – High Risk'], 'Does customer/party have any high-risk risk business activity as part of its portfolio');
			$scope.reportv2Data['#Party/Customer_Activity_High_RiskQuestionAns'] = dataAns ? dataAns.text : 'No';;
		}
		if ($scope.generateReportData['#UBO']) {
			var dataAns = getCheckListAnswer($scope.generateReportData['#UBO'], 'Are there UBOs identified in the ownership structure?');
			$scope.reportv2Data['#UBO_Identified_in_ownership'] = dataAns ? dataAns.text : 'No';
		}
		if ($scope.generateReportData['#Main Principals']) {
			var dataAns = getCheckListAnswer($scope.generateReportData['#Main Principals'], 'All the main principals identified?');
			$scope.reportv2Data['#Main_principals_question'] = dataAns ? dataAns.text : 'No';
		}
		if ($scope.generateReportData['#Principals']) {
			var dataAns = getCheckListAnswer($scope.generateReportData['#Principals'], 'Is the entity incorporated in a high risk country?');
			$scope.reportv2Data['#Principals_question'] = dataAns ? dataAns.text : 'No';
		}
		if ($scope.generateReportData['#Screening']) {
			var dataAns = getCheckListAnswer($scope.generateReportData['#Screening'], 'Are all the main principals/principals, UBO, associated/related parties screened against Sanctions & PEP lists ?');
			$scope.reportv2Data['#screeing_question'] = dataAns ? dataAns.text : 'No';
		}
		$scope.reportv2Data['party_information'] = addingIsTackenScreenShotKey($scope.reportv2Data['party_information'], 2);
		$scope.reportv2Data['Party_Address_Details'] = addingIsTackenScreenShotKey($scope.reportv2Data['Party_Address_Details'], 2);
		$scope.reportv2Data['Registration_and_TAX_Details'] = addingIsTackenScreenShotKey($scope.reportv2Data['Registration_and_TAX_Details'], 3);
	}
	function getSourceInfo(obj) {
			return EntityCommonTabService.getSourceInfo(obj);
	}
	function getCheckListAnswer(checklistArr, question_name, selectedValue, productSection) {
		var obj_ans = null;
		if (checklistArr && checklistArr.length) {
			// var findQuesObj = _.find(checklistArr,function(d){ return d.questionName.includes(question_name)});
			var findQuesObj = checklistArr.find(function (d) { return d.key == question_name });
			if (findQuesObj) {
				if (findQuesObj.values.length && (findQuesObj.values[0].questionType === "radioButton" || findQuesObj.values[0].questionType === "checklist")) {
					obj_ans = [];
					if (selectedValue) {
						obj_ans = findQuesObj.values[0].questionOptions.find(function (d) {
							return d.selected === selectedValue;
						});
					} else {
						if (productSection) {
							obj_ans = findQuesObj.values[0].questionOptions.filter(function (d) {
								return d.selected;
							});
						} else {
							obj_ans = findQuesObj.values[0].questionOptions.find(function (d) {
								return d.selected;
							});
						}
					}
				} else if (findQuesObj.values.length && findQuesObj.values[0].questionType === "text") {
					obj_ans = '';
					obj_ans = findQuesObj.values[0].questionAnswer;
				} else if (findQuesObj.values.length && findQuesObj.values[0].questionType === "multiple-short-txt") {
					obj_ans = [];
					if (selectedValue) {
						obj_ans = findQuesObj.values[0].questionOptions.find(function (k) {
							return k.label && k.label === selectedValue;
						});;
					} else {
						obj_ans = findQuesObj.values[0].questionOptions;
					}
				}
			}
			return obj_ans;
		}
	}
	function addingIsTackenScreenShotKey(values, arrSize, oldreport) {
		if (values.length) {
			var legal_data = values.flat();
			angular.forEach(legal_data, function (val) {
				var findsrcIndx = $scope.sourceWithBSTRegistry.findIndex(function (d) {
					return d.sourceName === val.source
				});
				if (findsrcIndx !== -1) {
					if ($scope.sourceWithBSTRegistry[findsrcIndx].showHideAddtoPage) {
						var docname = $scope.sourceWithBSTRegistry[findsrcIndx].SourceValue.source_screenshot;
						if ($scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName) {
							docname = $scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName;
						}
						var docIndex = (docname.lastIndexOf("/")) + 1;
						docname = docname.slice(docIndex, docname.length);
						var docid = $scope.sourceWithBSTRegistry[findsrcIndx].docid;
						if ($scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName) {
							docname = $scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName;
						}
						if (docname) {
							var pointIndex = (docname.lastIndexOf(".")) + 1;
							var srcType = docname.slice(pointIndex, docname.length);
							val.sourceType = srcType;
							val.sourceUrl = docname;
							val.docId = docid;
						}
					}
				}
			});
			if (!oldreport) {
				values = _.chunk(legal_data, arrSize);
			}
			return values;
		}
	}
	function addingIsTackenScreenShotKeyForOldReport(values, arrSize, oldreport) {
		if (values) {
			var val = values;//.flat();
			// angular.forEach(legal_data, function(val){
			var findsrcIndx = $scope.sourceWithBSTRegistry.findIndex(function (d) {
				return d.sourceName === val.source;
			});
			if (findsrcIndx !== -1) {
				if ($scope.sourceWithBSTRegistry[findsrcIndx].showHideAddtoPage) {
					var docname = $scope.sourceWithBSTRegistry[findsrcIndx].SourceValue.source_screenshot;
					if ($scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName) {
						docname = $scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName;
					}
					var docIndex = (docname.lastIndexOf("/")) + 1;
					docname = docname.slice(docIndex, docname.length);
					var docid = $scope.sourceWithBSTRegistry[findsrcIndx].docid;
					if ($scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName) {
						docname = $scope.sourceWithBSTRegistry[findsrcIndx].uploadedFileName;
					}
					if (docname) {
						var pointIndex = (docname.lastIndexOf(".")) + 1;
						var srcType = docname.slice(pointIndex, docname.length);
						val.sourceType = srcType;
						val.sourceUrl = docname;
						val.docId = docid;
					}
				}
			}
			// });
			return val;
		}
	}
	function getJsonParse(data) {
		var newData = [];
		angular.forEach(data, function (val, key) {
			if (val.questionType == "multiple-short-txt") {
				// val.questionOptions = JSON.parse(val.questionOptions);
				val.questionOptions = val.questionOptions
			}
			if (val.questionType == 'checklist') {
				// var all_options = JSON.parse(val.questionOptions);
				var all_options = (val.questionOptions);
				var options = (all_options).filter(function (d) {
					return (!d.commenthead && d.selected);
				});
				var comments = (all_options).filter(function (d) {
					return (d.commenthead);
				});
				val.questionOptions = _.chunk(options, 2);
				val.comments = comments;
			}
			newData.push(val);
		});
		return newData;
	}
	function getSuccessReportDocuments(pagerecords, docOrSticky) {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		var params2 = {
			"token": $rootScope.ehubObject.token,
			"docFlag": 5,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		var params3 = {
			"token": $rootScope.ehubObject.token,
			"docFlag": 6,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		if (docOrSticky === 'docs') {
			return new Promise(function (resolve, reject) {
				TopPanelApiService.getAllDocuments(params).then(function (res) {
					$scope.documentsListForReport = res.data.result;
					resolve(true);
				}).catch(function (err) {
					reject(false);
				});
			});
		} else if (docOrSticky === 'sticky') {
			return new Promise(function (resolve, reject) {
				TopPanelApiService.getAllDocuments(params2).then(function (res) {
					$scope.stickyListForReport = res.data.result;
					var numberOfdocuments = 0;
					angular.forEach(res.data.result, function (val, key) {
						var params = {
							"token": $rootScope.ehubObject.token,
							"docId": val.docId
						};
						// return new Promise(function (resolve, reject) {
						TopPanelApiService.downloadDocument(params).then(function (docContent) {
							var blob = new Blob([docContent.data], {
								type: "application/text",
							});
							var reader = new FileReader();
							reader.addEventListener('loadend', function (e) {
								var text = e.srcElement.result;
								var index = $scope.stickyListForReport.findIndex(function (d) { return d.docId === val.docId });
								$scope.stickyListForReport[index].content = text;
							});
							// Start reading the blob as text.
							var x = reader.readAsText(blob);// jshint ignore:line
							if (res.data.result.length - 1 === numberOfdocuments) {
								resolve(true);
							}
							numberOfdocuments = numberOfdocuments + 1;
						}, function () {
							if (res.data.result.length - 1 === numberOfdocuments) {
								resolve(true);
							}
							numberOfdocuments = numberOfdocuments + 1;
						});
					});
					if (res.data.result.length === 0) {
						resolve(true)
					}
					// resolve(true);
				}).catch(function (err) {
					reject(true);
				});
			});
		} else if (docOrSticky === "evidence_docs") {
			return new Promise(function (resolve, reject) {
				TopPanelApiService.getAllDocuments(params3).then(function (res) {
					// $scope.evidenceDocumentsListForReport = res.data.result;
					$scope.evidenceDocumentsListForReport = res.data.result.filter(function (value) {
						return value.type.toLowerCase() == "png"
					})
					$scope.evidenceDocumentsListDocotherThanPNG = res.data.result.filter(function (value) {
						return value.type.toLowerCase() != "png"
					})
					var numberOfdocuments = 0;
					angular.forEach(res.data.result, function (val, key) {
						var params = {
							"token": $rootScope.ehubObject.token,
							"docId": val.docId
						};
						// return new Promise(function (resolve, reject) {
						TopPanelApiService.downloadDocument(params).then(function (docContent) {
							var blob = new Blob([docContent.data], {
								type: "application/text",
							});
							var urlCreator = window.URL || window.webkitURL;
							var imageUrl = urlCreator.createObjectURL(blob);
							var index = $scope.evidenceDocumentsListForReport.findIndex(function (d) { return d.docId === val.docId });
							if (index || index == 0) {
								$scope.sourceWithBSTRegistry.filter(function (d) {
									if (d.sourceName && $scope.evidenceDocumentsListForReport[index] && $scope.evidenceDocumentsListForReport[index].docName && d.sourceName == $scope.evidenceDocumentsListForReport[index].docName.split('.png')[0]) {
										if (d) {
											$scope.evidenceDocumentsListForReport[index].title = d.sourceName;
										}
									}
								});
							}
							if (index !== -1) {
								$scope.evidenceDocumentsListForReport[index]['url'] = imageUrl;
								$scope.evidenceDocumentsListForReport[index]['sourceId'] = val.title;
							}
							if (res.data.result.length - 1 === numberOfdocuments) {
								resolve(true);
							}
							numberOfdocuments = numberOfdocuments + 1;
						}, function () {
							if (res.data.result.length - 1 === numberOfdocuments) {
								resolve(true);
							}
							numberOfdocuments = numberOfdocuments + 1;
							//								HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
							// reject(false);
						});
						// });
					});
					if (res.data.result.length === 0) {
						resolve(true)
					}
					// resolve(true);
				}).catch(function (err) {
					reject(false);
				});
			});
		}
	}
	
	$window.generateReportPdf = function () {
		$scope.$parent.pdfLoader = true;
		var reportAdverseObject = {};
		var reportAdverseNews = [];
		$scope.alertSummary = {
			pep: 0,
			sanction: 0,
			financeCrime: 0,
			adversenews: 0,
			total: 0
		};
		if ($scope.complianceRightAdverseNews.length > 0) {
			var adversenews_link = jQuery.extend($scope.complianceRightAdverseNews, [], true);
			var entities = adversenews_link.map(function (val) {
				return val.name
			}).filter(function (value, index, self) {
				if (self.indexOf(value) === index) return value
			});
			for (var i = 0; i < entities.length; i++) {
				reportAdverseObject = {
					name: entities[i],
					news: adversenews_link.filter(function (value) {
						return value.name === entities[i]
					})
				}
				reportAdverseObject.company = reportAdverseObject.news.length > 0 ? reportAdverseObject.news[0].officersCompany : '';
				reportAdverseNews.push(reportAdverseObject);
			};
		}
		var show_sub = $scope.showSubsidaries;
		if ($scope.screeningData && $scope.screeningData.length > 0) {
			$scope.screeningDataforReport = jQuery.extend(true, [], $scope.screeningData);
			$scope.screeningCheckedData = jQuery.extend(true, [], $scope.screeningData);
			$scope.screeningDataforReport = $scope.screeningDataforReport.filter(function (d) {
				return (!d['no-screening']);
			});
			$scope.screeningCheckedData = $scope.screeningCheckedData.filter(function (d) {
				return (d['isChecked']);
			});
			angular.forEach($scope.screeningDataforReport, function (val, key) {
				val.anySanction__significant = false;
				val.anyPep_significant = false;
				val.officer_role = typeof val.officer_role === 'string' ? val.officer_role : _.uniq(_.uniq(val.officer_role).toString().split(','));
				val.pepUrldata = { name: [], count: 0 };
				val.sanctionUrldata = { name: [], count: 0 };
				val.reason = [];
				var reason = [];
				angular.forEach(val.pep_url, function (d, i) {
					if (d.pep) {
						angular.forEach(d.entries, function (d1, k) {
							val.anyPep_significant = true;
							// val.pepUrldata.name[d1.watchlist_id] = !val.pepUrldata.name[d1.watchlist_id] ? 1 : val.pepUrldata.name[d1.watchlist_id]+1;
							if (val.pepUrldata.name.indexOf(d1.watchlist_id) == -1) {
								val.pepUrldata.name.push(d1.watchlist_id);
							}
						});
					}
					if (d.reason) {
						reason.push(d.reason);
					}
					if (d.comments && d.comments.length > 0) {
						for (var i = 0; i < d.comments.length; i++) {
							if (d.comments[i].comment) {
								reason.push(d.comments[i].comment);
							}
						}//for loop ends
					}//comment if loop
				});
				angular.forEach(val['sanction_bst:description'], function (d, i) {
					if (d.sanction) {
						angular.forEach(d.entries, function (d1, k) {
							val.anySanction__significant = true;
							// val.sanctionUrldata.name[d1.watchlist_id] = !val.sanctionUrldata.name[d1.watchlist_id] ? 1 : val.sanctionUrldata.name[d1.watchlist_id]+1;
							if (val.sanctionUrldata.name.indexOf(d1.watchlist_id) == -1) {
								val.sanctionUrldata.name.push(d1.watchlist_id);
							}
						});
					}
					if (d.reason) {
						reason.push(d.reason);
					}
					if (d.comments && d.comments.length > 0) {
						for (var i = 0; i < d.comments.length; i++) {
							if (d.comments[i].comment) {
								reason.push(d.comments[i].comment);
							}
						}//for loop ends
					}//comment if loop
				});
				val.reason = _.uniq(reason);
			});
			$scope.screeningDataforReport = $scope.screeningDataforReport.filter(function (val) {
				if (show_sub) {
					return val;
				} else {
					if (val.entity_id !== "orgChartsubEntity") {
						return val;
					}
				}
			});
			//here(d.pep) we are checking whether they are marked as significant or not
			$scope.alertSummary.pep = _.compact(_.flatMap($scope.screeningDataforReport.map(function (d) { if (d.pep_url) { return d.pep_url } })).map(function (d) { if (d.pep) { return d.pep } })).length;
			//here(d.sanction) we are checking whether they are marked as significant or not
			$scope.alertSummary.sanction = _.compact(_.flatMap($scope.screeningDataforReport.map(function (d) {
				if (d && d['sanction_bst:description']) {
					return d['sanction_bst:description']
				}
			})).map(function (d) {
				if (d && d.sanction) {
					return d.sanction
				}
			})).length;
			$scope.alertSummary.financeCrime = _.compact(_.flatMap($scope.screeningDataforReport.map(function (d) { if (d.finance_Crime_url) { return d.finance_Crime_url } }))).length;
			$scope.alertSummary.jurisdictions = _.compact($scope.screeningDataforReport.map(function (d) { return d.high_risk_jurisdiction && d.high_risk_jurisdiction.toLowerCase() == "high" })).length;
			//$scope.alertSummary.adversenews = _.compact(_.flatMap($scope.generateReportofficernews.map(function(d){return d.news}))).length;
			$scope.alertSummary.adversenews = $scope.generateReportofficernews.length ? $scope.generateReportofficernews.length : 0;
			$scope.alertSummary.total = $scope.alertSummary.pep + $scope.alertSummary.sanction + $scope.alertSummary.financeCrime + $scope.alertSummary.jurisdictions + $scope.alertSummary.adversenews;
		}
		if (!$rootScope.genpactReportV2) {
			worldComplianceLocationsOptionsForReport.width = $("#reportchartRefDiv").width();
		EntityCommonTabService.World(worldComplianceLocationsOptionsForReport);
		}
		if (!$rootScope.complianceReport && !$rootScope.genpactReportV2) { //if the compliance report is bst
			plotChart();
		}
		setTimeout(function () {
			// var mainNode  =  $("#vlaContainer").html();	
			var mainNode = $("#" + $scope.entitySearchResult.list.currentVLArendered).html();
			$("#vlaContainer1").html(mainNode);//copying the html to another div as we might lose the content when teh chart is loading
			$("#vlaContainer1").find(".placeholder.orgdiagram").attr("id", "ownershipStructChartOrgDiagram");
			var node = document.getElementById("ownershipStructChartOrgDiagram");
			if (node) {
				domtoimage.toPng(node).then(function (dataUrl) {
					var img = new Image();
					img.src = dataUrl;
					$("#vlaContainer1").html("");
					$('#corporate_structure_diagram').html(img);
					$('#corporate_structure_diagram').find("img").addClass("img-responsive").css("margin", "auto");
					$scope.$parent.pdfLoader = false;
					$scope.$apply();
					var getNews_name = $scope.generateReportofficernewsByGroup.map(function (d) { return d.key });
					getNews_name.map(function (d) {
						$('.highlightWordSpan').highlight(d, {
							class: "report_highlight_text"
						});
					});
					if (currentDocID) {
						$("#questionnaireReportDiv").css("display", "block");
					} else {
						$("#questionnaireReportDiv").css("display", "none");
					}
					var dataTopics_report = jQuery.extend(true, [], $scope.TopicKPIData);
					var colorsObj = {
						"unevaluated": "rgb(150, 165, 162)",
						"met": "#6892b4",
						"not met": "#d28085",
						"overachieved": "#6892b4",
						"underachieved": "#d28085",
						"no info": "#667f8b",
						"not applicable": "#667f8b"
					}
					angular.forEach(dataTopics_report, function (val, key) {
						val.values.map(function (d) {
							d.key = (d.key == 'not_met_at_all' || d.key == 'not_met') ? 'not met' : d.key;
							d.value = d.values.length;
							d.doc_count = d.values.length;
						});
						EntityCommonTabService.InitializeandPlotPie(val.values, 'topicSummaryPie_' + key, colorsObj, '', true);
					})
					setTimeout(function () {
						var divContents = $("#report_generation_div").html();
						complianceReportTemplate($scope.entitySearchResult.name, divContents);
					}, 1000);
				}).catch(function (error) {
					$scope.$parent.pdfLoader = false;
					$scope.$apply();
					setTimeout(function () {
						var getNews_name = $scope.generateReportofficernewsByGroup.map(function (d) { return d.key });
						if ($('.highlightWordSpan').length) {
							getNews_name.map(function (d) {
								$('.highlightWordSpan').highlight(d, {
									class: "report_highlight_text"
								});
							});
						}
						if ($("#report_generation_div").find(".sticky-link-popover").length) {
							$("#report_generation_div").find(".sticky-link-popover").remove();
						}
						var divContents = $("#report_generation_div").html();
						complianceReportTemplate($scope.entitySearchResult.name, divContents);
					}, 1000);
				});
			} else {
				setTimeout(function () {
					$scope.$parent.pdfLoader = false;
					$scope.$apply();
					var getNews_name = $scope.generateReportofficernewsByGroup.map(function (d) { return d.key });
					if ($('.highlightWordSpan').length) {
						getNews_name.map(function (d) {
							$('.highlightWordSpan').highlight(d, {
								class: "report_highlight_text"
							});
						});
					}
					if ($("#report_generation_div").find(".sticky-link-popover").length) {
						$("#report_generation_div").find(".sticky-link-popover").remove();
					}
					var divContents = $("#report_generation_div").html();
					complianceReportTemplate($scope.entitySearchResult.name, divContents);
				}, 1000);
			}
		}, 100);
	}
	function complianceReportTemplate(entytiName, divContent) {
		var printWindow = window.open('', '', 'height=600,width=1000');
		printWindow.document.write('<html><head><title>' + (!$rootScope.genpactReportV2 ? ($scope.entitySearchResult && $scope.entitySearchResult.name ? ($scope.entitySearchResult.name + "Compliance Report") : '') : '') + '</title>');
		printWindow.document.write('<script src="../vendor/jquery/js/jquery.min.js"></script>');
		printWindow.document.write('<link href="../entity/assets/css/entity-report-styles.css" rel="stylesheet">');
		printWindow.document.write('<link href="../entity/assets/css/entity-reportv2-style.css" rel="stylesheet">');
		printWindow.document.write('<link href="../entity/assets/css/entity-styles.css" rel="stylesheet">');
		printWindow.document.write('<link href="../vendor/jquery/css/flag-icon.min.css" rel="stylesheet">');
		//printWindow.document.write('<link href="../entity/assets/css/font-awesome.css" rel="stylesheet">');
		printWindow.document.write('<style>@font-face {font-family: opensans-light;src: url(/assets/fonts/open-sans/OpenSans-Light.ttf);}</style>');
		if ($rootScope.ehubObject.adminUser) {
			printWindow.document.write('</head><body style="position: relative;padding-top: 0!important;">');
		} else {
			printWindow.document.write('</head><body oncontextmenu="return false;" style="position: relative;padding-top: 0!important;">');
			printWindow.document.onkeydown = function (e) {
				if (event.keyCode == 123) {
					return false;
				}
				if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
					return false;
				}
				if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
					return false;
				}
				if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
					return false;
				}
				if (e.ctrlKey && e.keyCode == 80) {
					return false;
				}
			}
		}
		printWindow.document.write(divContent);
		printWindow.document.write('</body>');
		printWindow.document.write('</html>');
		printWindow.document.close();
		if ($rootScope.ehubObject.adminUser) {
			setTimeout(function () {
				printWindow.print();
			}, 5000);
		}
	}
	function plotChart() {
		$scope.cumulativeRisk = EntityCommonTabService.basicsharedObject.riskScoreData['overall-score'] * 100;
		$scope.isloading = true;
		if (!EntityCommonTabService.basicsharedObject.riskScoreData && !EntityCommonTabService.basicsharedObject.riskScoreData.attributeRiskScores && !EntityCommonTabService.basicsharedObject.riskScoreData.attributeRiskScores.length > 0) {
			$scope.noData = true;
			$scope.isloading = false;
			return;
		}
		$scope.isloading = false;
		var finalData = { "nodes": [], "links": [] };
		var nodesId = 0;
		finalData.nodes.push({
			"name": $scope.entitySearchResult["title"] ? $scope.entitySearchResult["title"].split("_").join(" ") : "",
			"id": nodesId
		});
		nodesId++;
		finalData.nodes.push({
			"name": "",
			"id": nodesId
		});
		nodesId++;
		if (EntityCommonTabService.basicsharedObject.riskScoreData && EntityCommonTabService.basicsharedObject.riskScoreData.attributeRiskScores && EntityCommonTabService.basicsharedObject.riskScoreData.attributeRiskScores.length > 0) {
			angular.forEach(EntityCommonTabService.basicsharedObject.riskScoreData.attributeRiskScores, function (v, k) {
				if (v.riskFactor.score) {
					finalData.nodes.push({
						"name": v.attributeName ? v.attributeName.split("_").join(" ") : "",
						"id": nodesId
					})
					if (Math.abs(v.riskFactor.score) > 1) {
						v.riskFactor.score = Math.abs(v.riskFactor.score) / 100;
					}
					finalData.links.push({
						"source": 0,
						"target": nodesId,
						"value": v.riskFactor.score,
						"label": "",
						"txt": "<div style='word-wrap:break-word;color':'#FFFFFF'>" + v.attributeName + " : " + (v.riskFactor.score * 100).toFixed(2) + "%</div>"
					});
					finalData.links.push({
						"source": nodesId,
						"target": 1,
						"value": v.riskFactor.score,
						"label": "",
						"txt": "<div style='word-wrap:break-word;color':'#FFFFFF'>" + v.attributeName + " : " + (v.riskFactor.score * 100).toFixed(2) + "%</div>"
					});
					nodesId++;
				}
			});
		}
		if (!$scope.noData) {
			setTimeout(function () {
				var options = {
					entityName: $scope.entitySearchResult["name"],
					container: "#reportchartContentDiv",
					height: 300,
					width: 800,
					rectWidth: 5,
					rectColor: "#4294D9",
					isfromOtherModules: true,
					text_x_pos: 1
					// path_labels :['FFB','CPO','CPO','PK','CPKO']
				}
				var nodesList = {};
				finalData['nodes'].map(function (d, i) {
					d.nodeId = d.id;
					d.id = i;
					nodesList[d.nodeId] = i;
				})
				finalData['links'].map(function (d) {
					d.value = d['value'];
					d.source = nodesList[d.source];
					d.target = nodesList[d.target];
				})
				// options.data = data;
				options.data = (finalData)
				options.edge_labels = $.map(finalData.links, function (i, d) { return i.label });
				options.translateX = 50;
				options.translateY = 50;
				options.isReport = true;
				options.companyLogo = $scope.entitySearchResult.comapnyLogo;
				new sankey_chart_risk_score(options);
				setTimeout(function () {
					new loadRiskpieChart("", options);
				}, 100)
			}, 1000)
		}
	}
	window.loadRiskpieChart = function (setTitle, options) {
		EntityCommonTabService.loadRiskpieChart(setTitle, options,$scope.cumulativeRisk);
	}
/* ======================generate report Ends===============================*/
/*
     * @purpose: Log about generated Report in the audit Trail
     * @created: 10 Sep 2019
     * @params: null
     * @return: no
     * @author: Ram Singh
     */
	function auditLogGeneratedReport(){
			var data = {
				name:$scope.ceriSearchResultObject['vcard:organization-name'].value,
				identifier:$stateParams.query
			}
			EntityApiService.auditLogGenerateReport(data).then(function(response){
	
			});
	}
}