'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyLeadershipsNewController', entityCompanyLeadershipsNewController);
entityCompanyLeadershipsNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyLeadershipsNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
		var bloombergChartDetailsOptions  = EntityCommonTabService.basicsharedObject.bloombergChartDetailsOptions;
		var tagCloudPersonOptions  =  EntityCommonTabService.basicsharedObject.tagCloudPersonOptions;
		var shodanData = {
			gps_locations: [],
			open_ports: []
		};
		var bloombergChartOptions = {
			container: "#bloomberg-line-chart",
			height: '300',
			width: ($(window).width() - 425) / 2,
			uri: "../vendor/data/bloombergdata.json",
			data: {}
		};
		var stockhistoryOptions = {
			container: "#stockhistory-line-chart",
			height: '240',
			width: $("#stockhistory-line-chart").width(),
			uri: "../vendor/data/bloombergdata.json",
			data: {}
		};
	// var tagCloudNameList = EntityCommonTabService.entityChartSharedObject. EntityCommonTabService.entityChartSharedObject.tagCloudNameList;
	$scope.$on('leadershipOnload', function (e) {
	$scope.commiteeTabType = 'Nominating';
	if(EntityCommonTabService.lazyLoadingentity.leadershipTabFirsttime){
		getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.leadershipFetchers).then(function (responsemessage) {
			console.log('responsemessage: ', responsemessage);
			if ($scope.entitySearchResult.list['people_also_viewd'].length === 0) {
				$scope.entitySearchResult.is_data_not_found.is_people_visited = false;
			}
			var leadership = [];
			leadership.concat($scope.entitySearchResult.list['company_boardmembers']);
			leadership.concat($scope.entitySearchResult.list['company_key_executives']);
			leadership = leadership.concat($scope.entitySearchResult.list['key_persons']);
			angular.forEach(leadership, function (v, k) {
				$scope.leadership.push({
					name: v,
					percentage: Math.floor(Math.random() * (80 - 60) + 60)
				});
			});
		});
		leadershipgraph();
		CompanyBoardndKeyMembers(EntityCommonTabService.entityChartSharedObject.officerData);
		compilanceManagement(EntityCommonTabService.entityChartSharedObject.officerData);
		EntityCommonTabService.lazyLoadingentity.leadershipTabFirsttime = false;
		$scope.onClickCommiteeTypeTabs('Audit');
		$scope.loadRelatedPersonData();
	}else{
		$scope.onClickCommiteeTypeTabs('Audit');
		if (EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLeadersips"] && EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLeadersips"].vertices.length > 0) {
			EntityCommonTabService.loadDataAndPlotGraphForEntity({
				"data": EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLeadersips"],
				"id": "networkGraphLeadersips"
			});
		}
		plotAllChartsForLeaderShip();
		$scope.loadRelatedPersonData();
	}
	});
	$scope.$on('overviewleadershipOnload', function (e) {
		CompanyBoardndKeyMembers(EntityCommonTabService.entityChartSharedObject.officerData);
		compilanceManagement(EntityCommonTabService.entityChartSharedObject.officerData);
	});
	/* @purpose:  Set the Primary and Secondary BoardMEmbers
* 			 
* @created: 07 may 2018
* @params: type(string)
* @returns: no
* @author: Ram Singh */
var totalOfficers_link = [];
	function CompanyBoardndKeyMembers(data) {
		var totalMembers = EntityCommonTabService.basicsharedObject.totalOfficers_link;
		var reversed = EntityCommonTabService.basicsharedObject.totalOfficers_link.slice().reverse();
		if (data && data.officership && data.officership.length > 0) {
			// for (var index = 0; index < data.officership.length; index++) {
			// 	if (data.officership[index].value.length > 0)
			// 		totalMembers = totalMembers.concat(data.officership[index].value);
			// 	totalOfficers_link = totalOfficers_link.concat(data.officership[index].value);
			// }
			// totalOfficers_link = totalOfficers_link.reverse();
			// EntityCommonTabService.basicsharedObject.totalOfficers_link = totalOfficers_link;
			var filteredMembers = reversed.filter(function (val) {
				if (val.status && typeof val.status === 'string' && val.status.toLowerCase() !== "resigned") {
					return val;
				} else {
					return val;
				}
			});
			filteredMembers = _.uniqBy(filteredMembers, 'name');
			$scope.entitySearchResult.list['mixed_company_key_executives'] = $scope.entitySearchResult.list['mixed_company_key_executives'].length === 0 ? filteredMembers : $scope.entitySearchResult.list['mixed_company_key_executives'].concat(filteredMembers);
			var secetary = _.filter(filteredMembers, {
				'officer_role': 'Secretary'
			});
			$scope.entitySearchResult.list['has_company_members'] = $scope.entitySearchResult.list['has_company_members'].length === 0 ? secetary : $scope.entitySearchResult.list['has_company_members'].concat(secetary);
			$scope.entitySearchResult.list['has_company_members'] = $scope.entitySearchResult.list['has_company_members'].concat($scope.entitySearchResult.list['mixed_company_key_executives']);
			$scope.entitySearchResult.list['mixed_company_key_executives'] = _.uniqBy($scope.entitySearchResult.list['mixed_company_key_executives'], 'name');
			$scope.entitySearchResult.list['has_company_members'] = _.uniqBy($scope.entitySearchResult.list['has_company_members'], 'name');
			if ($scope.entitySearchResult.list['mixed_company_key_executives'].length === 0) {
				$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
			}
			if ($scope.entitySearchResult.list['has_company_members'].length === 0) {
				$scope.entitySearchResult.is_data_not_found.is_company_member = false;
			}
		} else {
			$scope.entitySearchResult.is_data_not_found.is_company_member = false;
			$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
		}
	}
		/* @purpose:  Company Management Data 1. IN this function all the items are pushed in allmanagement array 
	2.Finallist is made by filter the already exisiting  name in  the list if yes primary,secondary and conflict array are updated
	* 			 
		* @created: 07 may 2018
		* @params: type(string)
		* @returns: no
		* @author: Ram Singh */
		function compilanceManagement(data, LinkMainCompany) {
			var mainCompany = {};
			if (data.basic && data.basic.length > 0 && data.basic["0"].value && data.basic["0"].value["vcard:organization-name"]) {
				mainCompany.name = data.basic["0"].value["vcard:organization-name"];
				mainCompany.hasURL = data.basic["0"].value["hasURL"] ? data.basic["0"].value["hasURL"] : "";
				mainCompany.identifier = data['@identifier'] ? data['@identifier'] : "";
			}
			if (_.isEmpty(mainCompany)) {
				mainCompany = LinkMainCompany ? LinkMainCompany : '';
			}
			var totalManagementData = {
				allmanagement: [],
				finallist: []
			};
			var addSourceKey = [];
			if (data && data.officership) {
				addSourceKey = sourceKeys(data.officership);
			}
			/* all item puhed in one array */
			for (var i = 0; i < addSourceKey.length; i++) {
				for (var j = 0; j < addSourceKey[i].value.length; j++) {
					if (addSourceKey[i].value[j].primary_url && addSourceKey[i].value[j].primary_url.length === 0 && addSourceKey[i].value[j].source && (addSourceKey[i].value[j].source.indexOf('companieshouse') !== -1 || (addSourceKey[i].value[j].source.indexOf('ec.europa') !== -1) || (addSourceKey[i].value[j].source.indexOf(' business.data') !== -1) || (addSourceKey[i].value[j].source.indexOf('handelsregister') !== -1))) {
						addSourceKey[i].value[j].primary_url[0] = addSourceKey[i].value[j].source_url;
					} else if (addSourceKey[i].value[j].secondary_url && addSourceKey[i].value[j].secondary_url.length === 0) {
						addSourceKey[i].value[j].secondary_url[0] = addSourceKey[i].value[j].source_url ? addSourceKey[i].value[j].source_url : '';
					}
					addSourceKey[i].value[j].mainCompany = "";
					addSourceKey[i].value[j].mainCompany = mainCompany.name;
					totalManagementData.allmanagement.push(addSourceKey[i].value[j]);
				}
			}
			/* checking the conditions before pushing into final array */
			for (var i = 0; i < totalManagementData.allmanagement.length; i++) {
				var filtername = _.find(totalManagementData.finallist, ['name', totalManagementData.allmanagement[i].name])
				var filterindex = _.findIndex(totalManagementData.finallist, {
					name: totalManagementData.allmanagement[i].name
				})
				if (!filtername) {
					totalManagementData.finallist.push(totalManagementData.allmanagement[i]);
				} else {/* if name found in the final list conflict and sources updated */
					if (filtername.officer_role !== totalManagementData.finallist[filterindex].officer_role) {
						totalManagementData.finallist[filterindex].conflict.push({
							'source2': filtername.source_url,
							'source1': totalManagementData.finallist[filterindex].source_url,
							'value1': totalManagementData.finallist[filterindex].officer_role,
							'value2': filtername.officer_role
						});
					}
					if (filtername.source_type === "primary") {
						var primary = totalManagementData.finallist[filterindex].primary_url.concat(filtername.primary_url);
						totalManagementData.finallist[filterindex].primary_url = primary;
						totalManagementData.finallist[filterindex].primary_url = _.uniq(totalManagementData.finallist[filterindex].primary_url);
					} else {
						var secondary = totalManagementData.finallist[filterindex].secondary_url.concat(filtername.secondary_url);
						totalManagementData.finallist[filterindex].secondary_url = secondary;
						totalManagementData.finallist[filterindex].secondary_url = _.uniq(totalManagementData.finallist[filterindex].secondary_url);
					}
				}
			}
			var UniqManagement = _.uniqBy(totalManagementData.finallist, 'name');
			$scope.companyManagementDetailsInfo = $scope.companyManagementDetailsInfo.length === 0 ? UniqManagement : $scope.companyManagementDetailsInfo.concat(UniqManagement);
			$scope.companyManagementDetailsInfo = _.uniqBy($scope.companyManagementDetailsInfo, 'name');
			if ($scope.companyManagementDetailsInfo.length === 0) {
				$scope.pageloader.screeningLoader = false;
			} else {
				companyPositionHolders(UniqManagement);
				if (UniqManagement.length > 0) {
					var intial = 0;
					if (UniqManagement.length > 20) {
						var loop = Math.ceil(UniqManagement.length / 20);
						for (var index = 0; index < loop; index++) {
							var batch = UniqManagement.slice(intial, intial + 20);
							intial = intial + 20;
							//	complianceScreeningResult(batch,mainCompany, true);
						}
					} else {
						//	complianceScreeningResult(UniqManagement,mainCompany, true);
					}
				}
				//complianceScreeningResult(UniqManagement, mainCompany,true);
			}
			$scope.pageloader.directorsloader = false;
		}
			/* @purpose:  Adding Primary or secondarykeys
	 * 			 
	 * @created: 07 may 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function sourceKeys(data) {
		if (data && data.length && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if(data[i] && data[i].source && data[i].source.toLowerCase() === 'website'){
					data[i].source = $scope.ceriSearchResultObject.hasURL.value ? $scope.ceriSearchResultObject.hasURL.value : data[i].source;
				}
				if(data[i] && data[i].source && $scope.overViewDataLinksUrls[data[i].source]){
					data[i].source = $scope.overViewDataLinksUrls[data[i].source]['bst:registryURI'] ? $scope.overViewDataLinksUrls[data[i].source]['bst:registryURI'] : data[i].source 
				}
				if ((data[i].source.indexOf('companieshouse') !== -1 || (data[i].source.indexOf('ec.europa') !== -1) || (data[i].source.indexOf('business.data') !== -1) || (data[i].source.indexOf('handelsregister') !== -1))) {
					data[i].value.PrimarySource = data[i].source;
					data[i].source_type = 'primary';
					for (var j = 0; j < data[i].value.length; j++) {
						data[i].value[j].primary_url = [];
						data[i].value[j].source_type = 'primary';
						data[i].value[j].primary_url.push(data[i].source);
						data[i].value[j].source_url = data[i].source;
						data[i].value[j].conflict = [];
					}
				} else {
					data[i].value.SecondarySource = data[i].source;
					data[i].source_type = 'secondary';
					for (var j = 0; j < data[i].value.length; j++) {
						data[i].value[j].secondary_url = [];
						data[i].value.PrimarySource = data[i].source;
						data[i].value[j].source_type = 'secondary';
						if ((data[i].source).toLowerCase() === 'bst') {
							data[i].value[j].secondary_url = data[i].value[j] ? (data[i].value[j]['source-url'] ? (JSON.parse(data[i].value[j]['source-url'])).results : []) : [];
						} else {
							data[i].value[j].secondary_url.push(data[i].source);
						}
						data[i].value[j].source_url = data[i].source;
						data[i].value[j].conflict = [];
					}
				}
			}
			return data;
		}
	}
	/* @purpose: dividing data depending on the roles
* 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	function companyPositionHolders(totalData) {
		for (var index = 0; index < totalData.length; index++) {
			if (totalData[index].officer_role) {
				if ((totalData[index].officer_role.toLowerCase()).indexOf("board") !== -1) {
					$scope.entitySearchResult.list['company_boardmembers'].push(totalData[index]);
				} else {
					$scope.entitySearchResult.list['company_key_executives'].push(totalData[index]);
				}
			} else {
				if (totalData[index].roles && totalData[index].roles.length > 0) {
					totalData[index].officer_role = totalData[index].roles[0].officer_role;
					if ((totalData[index].officer_role.toLowerCase()).indexOf("board") !== -1) {
						$scope.entitySearchResult.list['company_boardmembers'].push(totalData[index]);
					} else {
						$scope.entitySearchResult.list['company_key_executives'].push(totalData[index]);
					}
				}
			}
			if (totalData[index].title) {
				if ((totalData[index].title.toLowerCase()).indexOf("board") !== -1) {
					$scope.entitySearchResult.list['company_boardmembers'].push(totalData[index]);
				} else {
					$scope.entitySearchResult.list['company_key_executives'].push(totalData[index]);
				}
			}
		}
		$scope.entitySearchResult.list['company_key_executives'] = _.uniqBy($scope.entitySearchResult.list['company_key_executives'], 'name');
		$scope.entitySearchResult.list['company_boardmembers'] = _.uniqBy($scope.entitySearchResult.list['company_boardmembers'], 'name');
		if ($scope.entitySearchResult.list['company_boardmembers'].length === 0 &&
			$scope.entitySearchResult.list['company_key_executives'].length === 0) {
			$scope.entitySearchResult.is_data_not_found.is_data_leadership = false;
		}
		if ($scope.entitySearchResult.list['company_boardmembers'].length === 0) {
			$scope.entitySearchResult.is_data_not_found.board_of_director = false;
		}
	}
	$scope.onClickCommiteeTypeTabs = function (type) {
		$scope.commiteeTabType = type;
	};
	function leadershipgraph() {
		var data = {
			"fetchers": ["1008", "1006", "1013", "1021"],
			"keyword": $scope.entitySearchResult.name ? $scope.entitySearchResult.name : EntityCommonTabService.basicsharedObject.companyName,
			"searchType": "Company",
			"lightWeight": true,
			"limit": 2,
			"iterations": 1,
			"alias": "networkLeadership",
			"create_new_graph": false,
			"requires_expansion": true,
			"entity_resolution": true,
			"filter_entities": ["Company", "Person"],
			"filter_mode": "Keep",
			"expansion_fetchers_lightweight": true,
			"expansion_fetchers": ["2", "2001", "1005", "1002", "1003", "3001", "1007", "1009", "29", "1010", "1011", "1012", "3002", "3003", '10', '11', '12', '13', '46', '16', '17', '1021']
		};
		/* calling network chart */
		EntityCommonTabService.loadoverviewVLA("networkGraphLeadersips", data,EntityCommonTabService.entityChartSharedObject.vlaDataArr);
	}
	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company'
			};
		
			/* 
			*/
			var temp = _.find(handlingCompanyConstantName, { 'name': data.keyword, 'fetcher': id });
			data.keyword = temp === undefined ? data.keyword : temp.alt_name;
			data.limit = temp === undefined ? data.limit : temp.limit;
			/* if(data.keyword  === 'Nestle' && id=== '1006'){
				data.keyword  = 'Nestle S A';
			} */
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				var currentSubFetcher = id;
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							if (subentityKey === 0) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
										angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
											// subsubEnK == 'name' && subsubEnV !== '' ?
											// $scope.entitySearchResult.list[subedv.relationshipName].push(subsubEnV) : '';
											if (d3.keys($scope.entitySearchResult.list["resolve_related_similar_companies"]).length > 0) {
												if (subsubEnK == 'name' && subsubEnV !== '' && $scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											} else {
												if (subsubEnK == 'name' && subsubEnV !== '' && !subenvFlag) {
													if (!$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
														$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher] = [];
													}
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													subenvFlag = true;
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											}
										});
									}
									if ((subedv.relationshipName) == 'specialities') {
										subedv.entity.name !== '' && subedv.entity.name ? specialities.push(subedv.entity.name) : '';
									}
									if ((subedv.relationshipName) == 'people_also_viewd') {
										if (subedv.entity.name !== '' && subedv.entity.name) {
											$scope.entitySearchResult.list['people_also_viewd'].push({
												name: subedv.entity.properties.company_name,
												company_size: subedv.entity.properties.company_size,
												industry: subedv.entity.properties.industry,
												link: subedv.entity.properties.link,
												logo: subedv.entity.properties.logo,
												risk_score: subedv.entity.properties.risk_score,
												source: subedv.entity.source
											})
										}
										if ($scope.entitySearchResult.list['people_also_viewd'].length === 0) {
											$scope.entitySearchResult.is_data_not_found.is_people_visited = false;
										}
									}
								});
							}
						});
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							angular.forEach(subentity.edges, function (subedv, subedk) {
								if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
									angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
										if (subsubEnK == 'name' && subsubEnV !== '') {
											$scope.entitySearchResult.list[subedv.relationshipName].push({
												name: subsubEnV,
												source: subentity.source
											});
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subsubEnV)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subsubEnV,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
									});
								}
							});
						});
						if (subfetcherData.fetcher === 'stocks.money.cnn.com') {
							$scope.isStockDataFound = true;
							var finalLocationList = [], locationNames = [];
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'institutional_ownerships' || subedv.relationshipName === 'insider_shareholders') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'top_owners') {
													stockModalData['top_owners'].push(subedgev.entity.properties);
													$scope.entitySearchResult.list['top_holders'] = stockModalData['top_owners'];
												}
												if (subedgev.relationshipName === 'top_mutual_holding') {
													stockModalData['top_mutual_holding'].push(subedgev.entity.properties);
												}
												if (subedgev.relationshipName === 'recent_institutional_activity') {
													stockModalData['recent_institutional_activity'].push(subedgev.entity.properties);
												}
											});
										}
										if (subedv.relationshipName === 'press_releases') {
											subedv.entity.properties.time = new Date(subedv.entity.properties.time);
											stockModalData['press_releases'].push(subedv.entity.properties);
											$scope.entitySearchResult.list['press_releases'] = stockModalData['press_releases'];
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'locations') {
													stockModalData['locations'].push({
														name: subedgev.entity.name,
														risk_score: subedgev.entity.properties.risk_score
													});
													if (subedgev.entity.name !== '' && subedgev.entity.name) {
														newsLocationList.push({
															name: subedgev.entity.name,
															long: '',
															lat: '',
															mark: 'assets/images/redpin.png',
															source: subedgev.entity.source,
															title: subedgev.entity.properties.title || subedgev.entity.source
														});
													}
													$scope.entitySearchResult.list['locations'] = stockModalData['locations'];
												}
											});
											angular.forEach(newsLocationList, function (locationValue, locationKey) {
												if (locationNames.indexOf(locationValue.name) === -1) {
													locationNames.push(locationValue.name);
													finalLocationList.push(locationValue);
												}
											});
										}
										if (subedv.relationshipName === 'financial_statements') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'balance_sheet') {
													angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
														stockModalData['balance_sheet'][subedgev2.relationshipName] = {};
														angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
															stockModalData['balance_sheet'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
														});
													});
												}
												if (subedgev.relationshipName === 'income_statement') {
													angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
														stockModalData['income_statement'][subedgev2.relationshipName] = {};
														angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
															stockModalData['income_statement'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
														});
													});
												}
												if (subedgev.relationshipName === 'cash_flow_statement') {
													angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
														stockModalData['cash_flow_statement'][subedgev2.relationshipName] = {};
														angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
															stockModalData['cash_flow_statement'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
														});
													});
												}
											});
										}
										if (subedv.relationshipName === 'stock_profile') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'growth_and_valuation') {
													stockModalData['stock_profile']['growth_and_valuation'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'today_trading') {
													stockModalData['stock_profile']['today_trading'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'company_profile') {
													stockModalData['stock_profile']['company_profile'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'competitors') {
													stockModalData['stock_profile']['competitors'].push(subedgev.entity.properties);
												}
												if (subedgev.relationshipName === 'other_values') {
													stockModalData['stock_profile']['other_values'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'financials') {
													stockModalData['stock_profile']['financials'] = subedgev.entity.properties;
												}
											});
										}
										if (subedv.relationshipName === 'company_profile') {
											stockModalData['company_profile']['data'] = subedv.entity.properties;
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'shareholders') {
													stockModalData['company_profile']['shareholders'] = subedgev.entity.properties;
												}
											});
										}
									});
								}
								angular.forEach(subentity.properties, function (subValue, subKey) {
									$scope.entitySearchResult['stocks']['cnn_prop'][subKey] = subValue
								});
							});
							worldChartNewsLocationsOptions.markers = finalLocationList;
							/* calling world map chart function */
							World(worldChartNewsLocationsOptions);
							$scope.stockModalData = stockModalData;
							if (!_.isEmpty(stockModalData)) {
								handle_modalData_statement(stockModalData);
								calculateStatementInfo(stockModalData);
								//   loadfinancialpie(stockModalData);
							} else {
								$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
								$scope.entitySearchResult.is_data_not_found.is_investment = false;
							}
							if ($scope.entitySearchResult.list['top_holders'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_topholders = false;
							}
							if ($scope.entitySearchResult.list['press_releases'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
							}
							if (_.isEmpty(stockModalData) || _.isEmpty(stockModalData['company_profile']['shareholders'])) {
								$scope.entitySearchResult.is_data_not_found.is_investment = false;
							}
							if (_.isEmpty(stockModalData) || _.isEmpty(stockModalData['cash_flow_statement'])) {
								$scope.entitySearchResult.is_data_not_found.is_investment = false;
							}
						}
						if (subfetcherData.fetcher === 'finance.yahoo.com') {
							$scope.isYahooStockDataFound = true;
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'recent_user_comments' && subedv.entity.properties.name) {
											if (!yahooStockData['recent_user_comments']) {
												yahooStockData['recent_user_comments'] = [];
											}
											yahooStockData['recent_user_comments'].push(subedv.entity.properties)
											//
										}
										if (subedv.relationshipName === 'stock_holders') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (!yahooStockData['stock_holders'][subedgev.relationshipName]) {
													yahooStockData['stock_holders'][subedgev.relationshipName] = [];
												}
												yahooStockData['stock_holders'][subedgev.relationshipName].push(subedgev.entity.properties)
											});
										}
										if (subedv.relationshipName === 'company_profile') {
											yahooStockData['company_profile']['data'] = subedv.entity.properties;
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'key_staff') {
													yahooStockData['company_profile']['keyStaff'].push(subedgev.entity.properties);
													if ($.inArray(subedgev.entity.properties.name, key_staff_arr) == -1) {
														key_staff_arr.push(subedgev.entity.properties.name);
														subedgev.entity.properties['desgination'] = 'Chairman of the Board of Management & CEO';
														$scope.entitySearchResult.list['key_staff'].push(subedgev.entity.properties);
													}
												}
											});
											/* if($scope.entitySearchResult.list['company_key_executives'].length === 0 && $scope.entitySearchResult.list['founders'].length === 0 && $scope.entitySearchResult.list['key_staff'].length > 0){
												$scope.entitySearchResult.list['mixed_company_key_executives'] = $scope.entitySearchResult.list['key_staff'];
											} */
										}
										if (subedv.relationshipName === 'stock_graphs') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'last_5year_stock_history' || subedgev.relationshipName === 'last_6month_stock_history' ||
													subedgev.relationshipName === 'last_day_stock_history' || subedgev.relationshipName === 'last_year_stock_history' ||
													subedgev.relationshipName === 'last_5d_stock_history' || subedgev.relationshipName === 'last_month_stock_history') {
													var entitystockhistoryKey = subedgev.relationshipName.replace(/_/g, ' ');
													if (!stockhistoryLineChartData.hasOwnProperty(entitystockhistoryKey)) {
														stockhistoryLineChartData[entitystockhistoryKey] = [];
													}
													if (!stockhistoryLineChartData1.hasOwnProperty(entitystockhistoryKey)) {
														stockhistoryLineChartData1[entitystockhistoryKey] = [];
													}
													angular.forEach(subedgev.entity.edges, function (edge, edgekey) {
														if (edge.relationshipName === 'quote') {
															var date = '', volume = '', high = '', low = '', close = '', open = '';
															angular.forEach(edge.entity.properties, function (propValue, propKey) {
																if (propKey != 'risk_score' && propKey != 'date')
																	$scope.stock_measures.push(propKey);
																if (propKey.indexOf('date') !== -1)
																	date = propValue;
																if (propKey.indexOf('volume') !== -1)
																	volume = propValue;
																if (propKey.indexOf('high') !== -1)
																	high = propValue;
																if (propKey.indexOf('low') !== -1)
																	low = propValue;
																if (propKey.indexOf('close') !== -1)
																	close = propValue;
																if (propKey.indexOf('open') !== -1)
																	open = propValue;
															});
															stockhistoryLineChartData1[entitystockhistoryKey].push({
																'year': new Date(date),
																'volume': volume,
																'high': high,
																'low': low,
																'close': close,
																'open': open
															});
															stockhistoryLineChartData[entitystockhistoryKey].push({
																'year': new Date(date),
																'value': volume
															});
														}
													});
													// $scope.entitySearchResult['stocks'][subedv.relationshipName] =
													// subedv.entity.properties;
												}
											});
										}
										if (subedv.relationshipName === 'stock_statistics') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'dividends_Splits') {
													yahooStockData['stock_statistics']['dividends_Splits'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'stock_price_history') {
													yahooStockData['stock_statistics']['stock_price_history'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'fiscal_year') {
													yahooStockData['stock_statistics']['fiscal_year'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'profitability') {
													yahooStockData['stock_statistics']['profitability'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'balance_sheet') {
													yahooStockData['stock_statistics']['balance_sheet'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'management_effectiveness') {
													yahooStockData['stock_statistics']['management_effectiveness'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'other_stock_values') {
													yahooStockData['stock_statistics']['other_stock_values'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'cash_flow_statement') {
													yahooStockData['stock_statistics']['cash_flow_statement'] = subedgev.entity.properties;
												}
												if (subedgev.relationshipName === 'income_statement') {
													yahooStockData['stock_statistics']['income_statement'] = subedgev.entity.properties;
												}
											});
										}
										if (subedv.relationshipName === 'financial_statements') {
											angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
												if (subedgev.relationshipName === 'values') {
													yahooStockData['financial_statements']['financedates'].push(subedv.entity.properties.statement_date);
													yahooStockData['financial_statements']['financevalues'].push(subedgev.entity.properties);
												}
											});
										}
									});
								}
								angular.forEach(subentity.properties, function (subValue, subKey) {
									$scope.entitySearchResult['stocks']['yahoo_prop'][subKey] = subValue;
								});
							});
							angular.forEach(stockhistoryLineChartData, function (stockhistoryData, stockhistoryKey) {
								stockhistoryData = stockhistoryData.sort(function (a, b) {
									return moment(a.year).isAfter(b.year) ? 1 : -1;
								});
							});
							if (!_.isEmpty(stockhistoryLineChartData)) {
								angular.forEach(stockhistoryLineChartData, function (stockhistoryData, stockhistoryKey) {
									stockhistoryData = stockhistoryData.sort(function (a, b) {
										return moment(a.year).isAfter(b.year) ? 1 : -1;
									});
								});
								$scope.stock_measures = $scope.stock_measures.filter(onlyUnique);
								function onlyUnique(value, index, self) {
									return self.indexOf(value) === index;
								}
								$scope.selectedstockMeasure = $scope.stock_measures[0];
								$scope.stockhistoryLineChartData = stockhistoryLineChartData;
								$scope.entitystockhistoryKey = Object.keys(stockhistoryLineChartData)[0];
								stockhistoryOptions.data = stockhistoryLineChartData['last 5year stock history'];
								$scope.stockHistorySelectedTab = 'last 5year stock history';
								stockhistoryOptions.y_axis_title = $scope.entitySearchResult.exchange_currency;
								if (stockhistoryLineChartData['last 5year stock history'].length > 0) {
									EntityCommonTabService.loadlineData(stockhistoryOptions);
								}
							}
							$scope.yahooStockData = yahooStockData;
							var Empty_yahooStockData = false;
							if (_.isEmpty($scope.yahooStockData)) {
								Empty_yahooStockData = true;
							}
							if (($scope.yahooStockData && $scope.yahooStockData['financial_statements'] && $scope.yahooStockData['financial_statements']['financevalues'] && $scope.yahooStockData['financial_statements']['financevalues'].length === 0) || Empty_yahooStockData) {
								$scope.entitySearchResult.is_data_not_found.is_financialStatement = false;
							}
							if (($scope.stockhistoryLineChartData && $scope.stockhistoryLineChartData['last 5year stock history'] && $scope.stockhistoryLineChartData['last 5year stock history'].length === 0) || Empty_yahooStockData) {
								$scope.entitySearchResult.is_data_not_found.is_stockHistory = false;
							}
						}
						if (subfetcherData.fetcher === 'stocks.bloomberg.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'one_year_stock_prices' || subedv.relationshipName === 'one_month_stock_prices' ||
											subedv.relationshipName === 'five_year_stock_prices' || subedv.relationshipName === 'current_day_stock_prices') {
											var entityBloombergKey = subedv.relationshipName.replace(/_/g, ' ');
											if (!bloombergStockLineChartData.hasOwnProperty(entityBloombergKey)) {
												bloombergStockLineChartData[entityBloombergKey] = [];
											}
											angular.forEach(subedv.entity.edges, function (edge, edgekey) {
												if (edge.relationshipName === 'price') {
													var date = '', value = '';
													angular.forEach(edge.entity.properties, function (propValue, propKey) {
														if (propKey.indexOf('date') !== -1)
															date = propValue;
														if (propKey.indexOf('value') !== -1)
															value = propValue;
													});
													bloombergStockLineChartData[entityBloombergKey].push({
														'year': new Date(date),
														'value': value
													});
												}
											});
											$scope.entitySearchResult['stocks'][subedv.relationshipName] = subedv.entity.properties;
										}
										if (subedv.relationshipName === 'company_key_executives') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['stock_company_key_executives'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													website: subentity.properties.company_website,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subentity.source
												});
												var index = -1;
												angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
										if (subedv.relationshipName === 'company_boardmembers') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['stock_company_boardmembers'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													website: subentity.properties.company_website,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subentity.source
												});
												var index = -1;
												angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
									});
									angular.forEach(subentity.properties, function (subValue, subKey) {
										$scope.entitySearchResult['stocks']['bloomberg_prop'][subKey] = subValue;
									});
								}
							});
							angular.forEach(bloombergStockLineChartData, function (bloombergStockData, bloombergStockKey) {
								bloombergStockData = bloombergStockData.sort(function (a, b) {
									return moment(a.year).isAfter(b.year) ? 1 : -1;
								});
							});
							if (!_.isEmpty(bloombergStockLineChartData)) {
								$scope.entitySearchResult.is_data_not_found.stockprice_div = true;
								angular.forEach(bloombergStockLineChartData, function (bloombergStockData, bloombergStockKey) {
									bloombergStockData = bloombergStockData.sort(function (a, b) {
										return moment(a.year).isAfter(b.year) ? 1 : -1;
									});
								});
								$scope.bloombergStockLineChartData = bloombergStockLineChartData;
								$scope.bloombergDataKey = Object.keys(bloombergStockLineChartData)[0];
								bloombergChartOptions.data = bloombergStockLineChartData['five year stock prices'];
								bloombergChartDetailsOptions.data = bloombergStockLineChartData['five year stock prices'];
								$scope.bloombergSelectedTab = 'five year stock prices';
								bloombergChartOptions.y_axis_title = $scope.entitySearchResult.exchange_currency;
								bloombergChartDetailsOptions.y_axis_title = $scope.entitySearchResult.exchange_currency;
								if (bloombergStockLineChartData['five year stock prices'].length > 0) {
									EntityCommonTabService.loadlineData(bloombergChartOptions);
									EntityCommonTabService.loadlineData(bloombergChartDetailsOptions);
								} else {
									$scope.entitySearchResult.is_data_not_found.is_stock_performance = false;
								}
								if (dataWithDate['lastFiveYearDate']['close_price'].length > 0) {
									$scope.onChangestockhistoryKey_Enhance('lastFiveYearDate', 'close_price');
								}
							}
							if (_.isEmpty(bloombergStockLineChartData) || bloombergStockLineChartData['five year stock prices'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_stock_performance = false;
								$scope.entitySearchResult.is_data_not_found.stockprice_div = false;
							}
						}
						if (subfetcherData.fetcher === 'techcrunch.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								$scope.entityNewsList.push({
									title: subentity.properties.title,
									updatedOn: new Date(subentity.properties.updated_at),
									image: subentity.properties.image,
									details: subentity.properties.details,
									link: subentity.properties.link,
									source: subentity.source
								});
							});
							if ($scope.entitySearchResult.list["all_news_articles"].length > 0) {
								angular.forEach($scope.entitySearchResult.list["all_news_articles"], function (object, key) {
									$scope.entityNewsList.push(object);
								});
								$scope.entityNewsList = $scope.entityNewsList.sort(function (a, b) {
									return moment(a.updatedOn).isBefore(b.updatedOn) ? 1 : -1;
								});
							}
							if ($scope.entityNewsList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_latestnews = false;
							}
						}
						if (subfetcherData.fetcher === 'meetup.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									var meetupObject = {
										title: subentity.properties.title,
										group_name: subentity.properties.group_name,
										group_url: subentity.properties.group_url,
										date: new Date(subentity.properties.date),
										attend_count: subentity.properties.attend_count,
										details: subentity.properties.details,
										url: subentity.properties.url,
										source: subentity.source,
										groups: {},
										events: {}
									};
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'group') {
											var groupObject = {
												name: edgeValue.entity.properties.found,
												city: edgeValue.entity.properties.city,
												members: edgeValue.entity.properties.members,
												details: edgeValue.entity.properties.details,
												recent_events: []
											};
											angular.forEach(edgeValue.entity.edges, function (subsubEdv, subsubKey) {
												if (subsubEdv.relationshipName === 'recent_events') {
													var eventObject = {
														title: subsubEdv.entity.properties.title,
														held_on: subsubEdv.entity.properties.held_on,
														link: subsubEdv.entity.properties.link,
														details: subsubEdv.entity.properties.details
													};
													groupObject.recent_events.push(eventObject);
												}
											});
											meetupObject.groups = groupObject;
										}
										if (edgeValue.relationshipName === 'event') {
											var groupEventObject = {
												organizer: edgeValue.entity.properties.organizer,
												address: edgeValue.entity.properties.address,
												event_date: edgeValue.entity.properties.event_date,
												details: edgeValue.entity.properties.details,
												going: edgeValue.entity.properties.going,
												going_members: []
											};
											angular.forEach(edgeValue.entity.edges, function (subsubEdv, subsubKey) {
												if (subsubEdv.relationshipName === 'going_members') {
													var memberObject = {
														name: subsubEdv.entity.properties.name,
														image: subsubEdv.entity.properties.image,
													};
													groupEventObject.going_members.push(memberObject);
												}
											});
											meetupObject.events = groupEventObject;
										}
									});
									$scope.entitySearchResult.list["meetupData"].push(meetupObject);
								}
							});
							if ($scope.entitySearchResult.list["meetupData"].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_recent_events = false;
							}
						}
						if (subfetcherData.fetcher === 'hunter.io') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									$scope.entitySearchResult.list["hunterData"].title = subentity.properties.title;
									$scope.entitySearchResult.list["hunterData"].email = subentity.properties.details;
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'data') {
											$scope.entitySearchResult.list["hunterData"].domain = subedv.entity.properties.domain;
											$scope.entitySearchResult.list["hunterData"].company = subedv.entity.properties.company;
										}
										if (subedv.entity.edges.length > 0)
											$scope.entitySearchResult.list["hunterData"].sources = [];
										angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
											if (subsubedv.relationshipName === 'sources') {
												if (subsubedv.entity.properties.domain !== '' && subsubedv.entity.properties.domain) {
													$scope.entitySearchResult.list["hunterData"].sources.push({
														domain: subsubedv.entity.properties.domain,
														extracted_on: new Date(subsubedv.entity.properties.extracted_on),
														uri: subsubedv.entity.properties.uri
													});
												}
											}
										});
									});
									$scope.entitySearchResult.list["hunterData"].source = subentity.source;
								}
							});
						}
						if (subfetcherData.fetcher === 'exclusions.oig.hhs.gov') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'person') {
										$scope.entitySearchResult.list['exclusion_fraud'].push({
											name: subedv.entity.name,
											exclOn: new Date(subedv.entity.properties.excl_date),
											address: subedv.entity.properties.address1 + ' ' + subedv.entity.properties.address2,
											excl_type: subedv.entity.properties.excl_type,
											general: subedv.entity.properties.general,
											specialty: subedv.entity.properties.specialty,
											waiver: subedv.entity.properties.waiver,
											source: subentity.source
										});
									}
								});
							});
						}
						if (subfetcherData.fetcher === 'badbuyerslist.org') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0 && subentity.properties.details && subentity.properties.details !== '') {
									angular.forEach(subentity.properties, function (value, key) {
										if (value !== '' && value) {
											key = key.replace(/_/g, ' ');
											$scope.entitySearchResult.list['badbuyerslist'][key] = value;
										}
									});
									$scope.entitySearchResult.list['badbuyerslist']['source'] = subentity.source;
								}
							});
							if (_.isEmpty($scope.entitySearchResult.list['badbuyerslist'])) {
								$scope.entitySearchResult.list['is_badbuyerslist'] = false;
								$scope.entitySearchResult.list.badbbuyer_initial = false;
							}
						}
						if (subfetcherData.fetcher === 'blackhatworld.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								$scope.entitySearchResult.list['blackhatworld'].push({
									name: subentity.name,
									source: subentity.source,
									risk_score: subentity.properties.risk_score
								});
							});
						}
						if (subfetcherData.fetcher === 'plus.google.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'recent_public_activity') {
											$scope.entityGooglePlusList.push({
												name: subentity.properties.name,
												url: subentity.properties.url,
												title: edgeValue.entity.properties.title,
												coverPhoto: subentity.properties.cover_photo,
												lastUpdatedDate: new Date(edgeValue.entity.properties.last_updated_date),
												publishedDate: new Date(edgeValue.entity.properties.published_date),
												type: 'google'
											});
										}
									});
								}
							});
							if ($scope.entityGooglePlusList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_googlepost = false;
							}
							if ($scope.entityGooglePlusList.length > 0)
								$scope.socialMediaActiveCount = 2;
						}
						
						if (subfetcherData.fetcher === 'logo.google.com') {
							$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									if (!$scope.entitySearchResult['description'] || $scope.entitySearchResult['description'] === '') {
										$scope.entitySearchResult['description'] = subentity.properties['details'];
									}
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'social_feed') {
											$scope.entityLinkedinList.push({
												commentsCount: edgeValue.entity.properties.comments_count !== '' && edgeValue.entity.properties.comments_count ? edgeValue.entity.properties.comments_count : 0,
												likesCount: edgeValue.entity.properties.likes_count !== '' && edgeValue.entity.properties.likes_count ? edgeValue.entity.properties.likes_count : 0,
												postedOn: edgeValue.entity.properties.posted_at,
												text: edgeValue.entity.properties.text,
												image: subentity.properties.image,
												source: subentity.source
											});
										}
									});
								}
							});
							if ($scope.entityLinkedinList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
							}
							if ($scope.entityLinkedinList.length > 0)
								$scope.socialMediaActiveCount = 3;
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							}
						}
		
						if (subfetcherData.fetcher === 'deepdotweb.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.properties.author_name !== '' && subentity.properties.author_name &&
									subentity.properties.author_url !== '' && subentity.properties.author_url) {
									$scope.entitySearchResult.list['news_deep_web'].push({
										name: subentity.properties.author_name,
										postedOn: new Date(subentity.properties.post_date),
										url: subentity.properties.author_url,
										title: subentity.properties.title,
										details: subentity.properties.details,
										source: subentity.source
									});
								}
							});
						}
						if (subfetcherData.fetcher === 'hl.co.uk') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'key_persons') {
											if (edgeValue.entity.properties.name !== '') {
												$scope.entitySearchResult.list['key_persons'].push({
													name: edgeValue.entity.properties.name,
													designation: edgeValue.entity.properties.position,
													extented_properties: subentity.properties,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
												var index = -1;
												angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === edgeValue.entity.properties.name)
														index = key
												});
												if (index === -1) {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: edgeValue.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
									});
								}
							});
						}
						if (subfetcherData.fetcher === 'companieslist.co.uk') {
							if (subfetcherData.entities[0]) {
								$scope.entitySearchResult['advanceCompanyDetails'] = subfetcherData.entities[0].properties
							}
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'near_by_companies') {
											if (edgeValue.entity.properties.company_name !== '' && edgeValue.entity.properties.company_name) {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													name: edgeValue.entity.properties.company_name,
													address: edgeValue.entity.properties.address,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'has_accounts') {
											if (edgeValue.entity.properties.account_category !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName] = edgeValue.entity.properties;
											}
										}
										if (edgeValue.relationshipName === 'has_returns') {
											$scope.entitySearchResult.list[edgeValue.relationshipName] = edgeValue.entity.properties;
										}
										/* 	if(edgeValue.relationshipName === 'has_company_members') {
												var filter = _.find($scope.entitySearchResult.list['has_company_members'], { name: edgeValue.entity.properties.name });
												if(edgeValue.entity.properties.name !== '' && !filter){
															$scope.entitySearchResult.list[edgeValue.relationshipName].push({
																name: edgeValue.entity.properties.name,
																description: edgeValue.entity.properties.description,
																status: edgeValue.entity.properties.status,
																	source: subentity.source
													});
												}
											} */
										if (edgeValue.relationshipName === 'has_address') {
											if (edgeValue.entity.name !== '' && edgeValue.entity.properties.Address !== '') {
												EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.locationList.push({
													name: edgeValue.entity.properties.Address,
													long: '',
													lat: '',
													mark: 'assets/images/redpin.png',
													source: subentity.source,
													title: subentity.properties.title || subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'located_at') {
											if (edgeValue.entity.name !== '' && edgeValue.entity.properties.Location !== '') {
												EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.locationList.push({
													name: edgeValue.entity.properties.Location,
													long: '',
													lat: '',
													mark: 'assets/images/redpin.png',
													source: subentity.source,
													title: subentity.name || subentity.source
												});
											}
										}
									});
								}
							});
							if ($scope.entitySearchResult.list['near_by_companies'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_nearby_company = false;
							}
							/* if($scope.entitySearchResult.list['has_company_members'].length ===0){
								$scope.entitySearchResult.is_data_not_found.is_company_member =false;
							} */
						}
						if (subfetcherData.fetcher === 'risidata.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.name !== '' && subentity.properties.eventImpact !== '' && subentity.properties.eventImpact) {
									$scope.entitySearchResult.list['risidata'].push({
										title: subentity.properties.title,
										eventImpact: subentity.properties.eventImpact,
										details: subentity.properties.details,
										industryType: subentity.properties.industryType,
										reliability: subentity.properties.reliability,
										country: subentity.properties.country,
										year: subentity.properties.year,
										risk_score: subentity.properties.risk_score,
										source: subentity.source
									});
								}
							});
							if ($scope.entitySearchResult.list['risidata'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_indus_security_inc = false;
							}
						}
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							var interlock = {};
							var subentity = subfetcherData.entities[0];
							var nested_data = d3.nest().key(function (d) { return d.relationshipName }).entries(subfetcherData.entities[0].edges);
							for (var i = 0; i < nested_data.length; i++) {
								if (nested_data[i].key === "org_interlocks") {
									for (var vl = 0; vl < nested_data[i].values.length; vl++) {
										var edgevalue = nested_data[i].values[vl];
										if (edgevalue.relationshipName === "org_interlocks") {
											if (edgevalue.entity.properties.connected_entity_name !== '') {
												var mainentity = edgevalue.entity.properties.connected_entity_name;
												interlock[mainentity] = [];
												for (var j = 0; j < edgevalue.entity.edges.length; j++) {
													var subedgevalue = edgevalue.entity.edges[j];
													if (subedgevalue.relationshipName === 'connecting_entities') {
														var connected_entity = subedgevalue.entity.name;
														interlock[mainentity].push(connected_entity);
													} // connecting
													// entities
													// ends if
												} // j loops ends
											}// checking for emty
											// name if ends
										}// org interlocks ends
									}// vl for loop ends
								}// interocks if loop ends
							}// nested for loop loop ends
							var result = _.map(interlock, function (value, prop) {
								return { prop: prop, value: value };
							});
							if (result.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							} else {
								$scope.interlock = result;
							}
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									// $scope.entitySearchResult['revenue'] = subentity.properties.revenue;
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'key_staff_attended_schools') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													person: edgeValue.entity.properties.person,
													person_url: edgeValue.entity.properties.person_url,
													school_attended: edgeValue.entity.properties.school_attended,
													school_url: edgeValue.entity.properties.school_url,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'connected_industries') {
											$scope.entitySearchResult.list['connected_industries'].push(edgeValue);
										}
										if (edgeValue.relationshipName === 'key_relationships' || edgeValue.relationshipName === 'political_financial_supports'
											|| edgeValue.relationshipName === 'connection') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list['key_relationships'].push({
													name: edgeValue.entity.name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
											angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
												if (edgeValue.relationshipName === 'connection') {
													$scope.entitySearchResult.list['associated_persons'][subedv.relationshipName].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'memberships') {
													$scope.entitySearchResult.list['associated_companies']['memberships'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'transaction') {
													$scope.entitySearchResult.list['associated_companies']['transaction'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_financial_supports') {
													$scope.entitySearchResult.list['associated_companies']['political_financial_supports'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'politicians') {
													$scope.entitySearchResult.list['associated_companies']['politicians'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_organizations') {
													$scope.entitySearchResult.list['associated_companies']['political_organizations'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'owners') {
													$scope.entitySearchResult.list['associated_companies']['owners'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'leadership_&_staff') {
													$scope.entitySearchResult.list['associated_companies']['leadership & staff'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'services_transactions') {
													$scope.entitySearchResult.list['associated_companies']['services transactions'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'child_organizations') {
													$scope.entitySearchResult.list['associated_companies']['child_organizations'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'holdings') {
													$scope.entitySearchResult.list['associated_companies']['holdings'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'donation_grant_recipients') {
													$scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'key_staff') {
													if ($.inArray(subedgev.entity.properties.name, key_staff_arr) == -1) {
														key_staff_arr.push(subedgev.entity.properties.name);
														$scope.entitySearchResult.list['key_staff'].push(subedgev.entity.properties);
													}
												}
											});
										}
									});
								}
							});
							if ($scope.mainInfoTabType == 'Risk Alerts') {
								PlotPoliticalChart();
							}
							if ($scope.entitySearchResult.list['associated_companies']['child_organizations'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['holdings'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['memberships'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['owners'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['services transactions'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.associated_company = false;
							}
						}
						if (subfetcherData.fetcher === 'supplierblacklist.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0 && subentity.properties.type_of_complaints !== '' && subentity.properties.type_of_complaints) {
									angular.forEach(subentity.properties, function (value, key) {
										if (value !== '' && value) {
											key = key.replace(/_/g, ' ');
											$scope.entitySearchResult.list['supplierblacklist'][key] = value;
										}
									});
									$scope.entitySearchResult.list['supplierblacklist'].Source = subentity.source;
								}
							});
							if (_.isEmpty($scope.entitySearchResult.list['supplierblacklist'])) {
								$scope.entitySearchResult.is_data_not_found.is_supplierlist = false;
							}
						}
						if (subfetcherData.fetcher === 'scam.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.properties.title && subentity.properties.title !== '') {
									$scope.entitySearchResult.list['scam'].push({
										root: subentity.properties,
										replies: [],
										source: subentity.source
									});
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'replies') {
											$scope.entitySearchResult.list['scam'][subentityKey]['replies'].push(subedv.entity.properties);
										}
									});
								}
							});
							if ($scope.mainInfoTabType == 'Risk Alerts') {
								PlotriskAlertCharts();
							}
							if ($scope.entitySearchResult.list['scam'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_scam = false;
							}
						}
						if (subfetcherData.fetcher === 'xforce.ibmcloud.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								$scope.entitySearchResult.list['xforceibmcloud'].push({
									title: subentity.properties.title,
									link: subentity.properties.link,
									details: subentity.properties.details,
									risk_level: subentity.properties.risk_level,
									tag: subentity.properties.tag,
									reported: new Date(subentity.properties.reported),
									source: subentity.source
								});
							});
							if ($scope.mainInfoTabType == 'Risk Alerts') {
								PlotriskAlertCharts();
							}
							if ($scope.entitySearchResult.list['xforceibmcloud'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_cyberalert = false;
							}
						}
						if (subfetcherData.fetcher === 'jsonwhois.com' || subfetcherData.fetcher === 'whois.com' || subfetcherData.fetcher === 'who.is' || subfetcherData.fetcher === 'nic.ru') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								var objectData = {
									domain: subentity.properties.domain || subentity.properties._domain_name,
									expired_on: subentity.properties.expire ? new Date(subentity.properties.expire) :
										subentity.properties.expiration_date ? new Date(subentity.properties.expiration_date) :
											subentity.properties.expires_on ? new Date(subentity.properties.expires_on) :
												subentity.properties._registry_expiry_date ? new Date(subentity.properties._registry_expiry_date) : '',
									registered_on: subentity.properties.created_on ? new Date(subentity.properties.created_on) :
										subentity.properties.registration_date ? new Date(subentity.properties.registration_date) :
											subentity.properties.registered_on ? new Date(subentity.properties.registered_on) :
												subentity.properties._creation_date ? new Date(subentity.properties._creation_date) : '',
									status: subentity.properties.status || 'Registered',
									nameServers: subentity.properties.name_servers || subentity.properties._registrar_whois_server || '',
									admin_contacts: '',
									admin_phone: '',
									registrar: subentity.properties._registrar || '',
									technical_contacts: '',
									technical_phone: '',
									registrant_contacts: '',
									registrant_phone: '',
									source: subentity.source
								};
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'nameservers') {
										objectData.nameServers === '' ? objectData.nameServers = subedv.entity.name :
											objectData.nameServers += ', ' + subedv.entity.name;
									}
									if (subedv.relationshipName === 'registrar') {
										(subedv.entity.name !== '' && subedv.entity.name) ? objectData.registrar += '| Name: ' + subedv.entity.name : '';
										(subedv.entity.properties.url !== '' && subedv.entity.properties.url) ? objectData.registrar += '| URL: ' + subedv.entity.properties.url : '';
										(subedv.entity.properties.organization !== '' && subedv.entity.properties.organization) ? objectData.registrar += '| Organization: ' + subedv.entity.properties.organization : '';
									}
									if (subedv.relationshipName === 'admin_contacts' || subedv.relationshipName === 'administrative_contact') {
										subedv.entity.name !== '' ? objectData.admin_contacts += subedv.entity.name + ' | ' : '';
										objectData.admin_contacts = (subedv.entity.properties.street || '') + (subedv.entity.properties.address || '') + subedv.entity.properties.city +
											subedv.entity.properties.country + subedv.entity.properties.country_code + (subedv.entity.properties.state || subedv.entity.properties.state__province) +
											'-' + (subedv.entity.properties.zip || subedv.entity.properties.postal_code);
										(subedv.entity.properties.email !== '' && subedv.entity.properties.email) ? objectData.admin_contacts += '| Email: ' + subedv.entity.properties.email : '';
										(subedv.entity.properties.phone !== '' && subedv.entity.properties.phone) ? objectData.admin_phone = subedv.entity.properties.phone : '';
										(subedv.entity.properties.fax !== '' && subedv.entity.properties.fax) ? objectData.admin_contacts += '| Fax: ' + subedv.entity.properties.fax : '';
										(subedv.entity.properties.organization !== '' && subedv.entity.properties.organization) ? objectData.admin_contacts += '| Organization: ' + subedv.entity.properties.organization : '';
									}
									if (subedv.relationshipName === 'technical_contacts' || subedv.relationshipName === 'technical_contact') {
										subedv.entity.name !== '' ? objectData.technical_contacts += subedv.entity.name + ' | ' : '';
										objectData.technical_contacts = (subedv.entity.properties.street || '') + (subedv.entity.properties.address || '') + subedv.entity.properties.city +
											subedv.entity.properties.country + subedv.entity.properties.country_code + (subedv.entity.properties.state || subedv.entity.properties.state__province) +
											'-' + (subedv.entity.properties.zip || subedv.entity.properties.postal_code);
										(subedv.entity.properties.email !== '' && subedv.entity.properties.email) ? objectData.technical_contacts += '| Email: ' + subedv.entity.properties.email : '';
										(subedv.entity.properties.phone !== '' && subedv.entity.properties.phone) ? objectData.technical_phone = subedv.entity.properties.phone : '';
										(subedv.entity.properties.fax !== '' && subedv.entity.properties.fax) ? objectData.technical_contacts += '| Fax: ' + subedv.entity.properties.fax : '';
										(subedv.entity.properties.organization !== '' && subedv.entity.properties.organization) ? objectData.technical_contacts += '| Organization: ' + subedv.entity.properties.organization : '';
									}
									if (subedv.relationshipName === 'registrant_contacts' || subedv.relationshipName === 'registrant_contact' || subedv.relationshipName === 'registant_contact') {
										subedv.entity.name !== '' ? objectData.registrant_contacts += subedv.entity.name + ' | ' : '';
										objectData.registrant_contacts = (subedv.entity.properties.street || '') + (subedv.entity.properties.address || '') + (subedv.entity.properties.city || '') +
											subedv.entity.properties.country + subedv.entity.properties.country_code + (subedv.entity.properties.state || subedv.entity.properties.state__province) +
											'-' + (subedv.entity.properties.zip || subedv.entity.properties.postal_code);
										(subedv.entity.properties.email !== '' && subedv.entity.properties.email) ? objectData.registrant_contacts += '| Email: ' + subedv.entity.properties.email : '';
										(subedv.entity.properties.phone !== '' && subedv.entity.properties.phone) ? objectData.registrant_phone = subedv.entity.properties.phone : '';
										(subedv.entity.properties.fax !== '' && subedv.entity.properties.fax) ? objectData.registrant_contacts += '| Fax: ' + subedv.entity.properties.fax : '';
										(subedv.entity.properties.organization !== '' && subedv.entity.properties.organization) ? objectData.registrant_contacts += '| Organization: ' + subedv.entity.properties.organization : '';
										var index = -1;
										angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
											if (object.text === subedv.entity.name)
												index = key
										});
										if (index === -1) {
											 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
												text: subedv.entity.name,
												size: 5,
												type: 'person'
											});
										} else {
											 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
											 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
										}
									}
								});
								$scope.entitySearchResult.list['domain_servers'].push(objectData);
							});
							if ($scope.entitySearchResult.list['domain_servers'][0]['registrant_phone'] !== '') {
								var string = $scope.entitySearchResult.list['domain_servers'][0]['registrant_phone'].replace(/[&\/\\#,()$~%.'":*?<>{}]/g, '');
								var phonefind = _.indexOf($scope.entitySearchResult.list["phoneno"], string);
								if (phonefind === -1) {
									$scope.entitySearchResult.list["phoneno"].push(string);
								}
							}
							if ($scope.entitySearchResult.list['domain_servers'][0]['technical_phone'] !== '') {
								var string = $scope.entitySearchResult.list['domain_servers'][0]['technical_phone'].replace(/[&\/\\#,()$~%.'":*?<>{}]/g, '');
								var phonefind = _.indexOf($scope.entitySearchResult.list["phoneno"], string);
								if (phonefind === -1) {
									$scope.entitySearchResult.list["phoneno"].push(string);
								}
							}
							if ($scope.entitySearchResult.list['domain_servers'][0]['admin_phone'] !== '') {
								var string = $scope.entitySearchResult.list['domain_servers'][0]['admin_phone'].replace(/[&\/\\#,()$~%.'":*?<>{}]/g, '');
								var phonefind = _.indexOf($scope.entitySearchResult.list["phoneno"], string);
								if (phonefind === -1) {
									$scope.entitySearchResult.list["phoneno"].push(string);
								}
							}
						}
						if (subfetcherData.fetcher === 'reddit.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								var objectData = {
									title: subentity.properties.title,
									url: subentity.properties.url,
									details: subentity.properties.details,
									user: subentity.properties.user,
									risk_score: subentity.properties.risk_score,
									author: subentity.properties.author,
									dated: new Date(subentity.properties.date),
									category: subentity.properties.category,
									comments: [],
									source: subentity.source
								};
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'reply') {
										var commentObject = {
											reply: {},
											replies: []
										};
										commentObject.reply['body'] = subedv.entity.properties.body;
										commentObject.reply['dated'] = new Date(subedv.entity.properties.date);
										commentObject.reply['author'] = subedv.entity.properties.author;
										commentObject.reply['tags'] = subedv.entity.properties.tags;
										angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
											if (subsubedv.relationshipName === 'replies') {
												commentObject.replies.push({
													body: subsubedv.entity.properties.body,
													dated: new Date(subsubedv.entity.properties.date),
													author: subsubedv.entity.properties.author,
													tags: subsubedv.entity.properties.tags,
												});
											}
										});
										objectData.comments.push(commentObject);
									}
								});
								$scope.entitySearchResult.list['reddit'].push(objectData);
							});
						}
						if (subfetcherData.fetcher === 'cypherpunk.at') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.properties.title !== '' && subentity.properties.title &&
									subentity.properties.details !== '' && subentity.properties.details) {
									var objectData = {
										title: subentity.properties.title,
										author_url: subentity.properties.author_url,
										details: subentity.properties.details,
										about_author: subentity.properties.about_author,
										risk_score: subentity.properties.risk_score,
										author: subentity.properties.author,
										dated: new Date(subentity.properties.date),
										tags: '',
										source: subentity.source
									};
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'tags') {
											objectData.tags === '' ? objectData.tags = subedv.entity.name :
												objectData.tags += ', ' + subedv.entity.name;
										}
									});
									$scope.entitySearchResult.list['cypherpunk'].push(objectData);
								}
							});
						}
						if (subfetcherData.fetcher === 'inc.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								var objectData = {};
								angular.forEach(subentity.properties, function (value, key) {
									if (value !== '' && value) {
										key = key.replace(/_/g, ' ');
										objectData[key] = value;
									}
								});
								objectData['source'] = subentity.source;
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'rank') {
										objectData['rank'] = subedv.entity.properties.rank;
										objectData['rank year'] = subedv.entity.properties.rank_year;
									}
								});
								$scope.entitySearchResult.list['inc-us-eu'].push(objectData);
							});
						}
						if (subfetcherData.fetcher === 'companies.corporationwiki.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								var objectData = {
									name: subentity.properties.name,
									description: subentity.properties.description,
									located_at: subentity.properties.located_at,
									corporate_filings: [],
									known_addresses: [],
									key_people: [],
									risk_score: subentity.properties.risk_score,
									source: subentity.source
								};
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'corporate_filings') {
										var objectCorporateData = {};
										angular.forEach(subedv.entity.properties, function (value, key) {
											if (value !== '' && value) {
												key = key.replace(/_/g, ' ');
												objectCorporateData[key] = value;
											}
										});
										objectData.corporate_filings.push(objectCorporateData);
									}
									if (subedv.relationshipName === 'known_addresses') {
										objectData.known_addresses.push({
											address: subedv.entity.properties.street_address + ' ' + subedv.entity.properties.city + ' ' +
												subedv.entity.properties.region + '-' + subedv.entity.properties.post_code,
											address_url: subedv.entity.properties.address_url
										});
									}
									if (subedv.relationshipName === 'key_people') {
										var objectPeopleData = {
											url: subedv.entity.properties.url,
											name: subedv.entity.properties.name,
											roles: ''
										};
										angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
											if (subsubedv.relationshipName === 'roles') {
												objectPeopleData.roles === '' ? objectPeopleData.roles = subsubedv.entity.properties.role + '(' + subsubedv.entity.properties.current_status + ')' :
													objectPeopleData.roles += ', ' + subsubedv.entity.properties.role + '(' + subsubedv.entity.properties.current_status + ')';
											}
										});
										objectData.key_people.push(objectPeopleData);
									}
								});
								$scope.entitySearchResult.list['companies-corporationwiki'].push(objectData);
							});
						}
						if (subfetcherData.fetcher === 'bloomberg.com' || subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if ((subedv.relationshipName) == 'news_articles') {
											$scope.entitySearchResult.list["all_news_articles"].push({
												title: subedv.entity.properties.title,
												details: subedv.entity.properties.body,
												updatedOn: new Date(subedv.entity.properties.date),
												image: subedv.entity.properties.image != undefined ? subedv.entity.properties.image : '',
												link: subedv.entity.properties.link != undefined ? subedv.entity.properties.link : '',
												source: subentity.source
											});
										}
										if (subedv.relationshipName === 'key_executives') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												/* $scope.entitySearchResult.list['company_key_executives'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													website: subentity.properties.website,
														extented_properties: subentity.properties,
														risk_score: subedv.entity.properties.risk_score,
														source: subedv.entity.source,
														age:subedv.entity.properties.age
												}); */
												var index = -1;
												angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
										if (subedv.relationshipName === 'mainboard_members') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												/* $scope.entitySearchResult.list['company_boardmembers'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													website: subedv.entity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subentity.source,
													age: subedv.entity.properties.age,
												}); */
											}
										}
										if (subedv.relationshipName === 'other_board_members') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												/* 	$scope.entitySearchResult.list['other_board_members'].push({
														name: subedv.entity.properties.name,
														designation: subedv.entity.properties.designation,
														age: subedv.entity.properties.age,
														primary_company: subentity.primary_company,
														risk_score: subedv.entity.properties.risk_score,
														profile_link: subedv.entity.properties.profile_link,
															source: subedv.entity.source
													}); */
											}
										}
										if (subedv.relationshipName === 'compensation_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['compensation_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'nominating_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['nominating_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['corporate_governance_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													risk_score: subedv.entity.properties.risk_score,
													profile_link: subedv.entity.properties.profile_link,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === 'audit_committee') {
											if (subedv.entity.properties.name !== '') {
												$scope.entitySearchResult.list['audit_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subedv.entity.properties.profile_link,
													profile_link: subedv.entity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === "similar_companies") {
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subedv.entity.properties.name)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subedv.entity.properties.name,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
										if (subedv.relationshipName === 'compensation_committee' || subedv.relationshipName === 'nominating_committee' || subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entityOtherMemberList.push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subentity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source
												});
												var index = -1;
												angular.forEach( EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													 EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
									});
									if ($scope.entityNewsList.length > 0 && subfetcherData.fetcher === 'bloomberg.com') {
										angular.forEach($scope.entitySearchResult.list["all_news_articles"], function (object, key) {
											$scope.entityNewsList.push(object);
										});
										$scope.entityNewsList = $scope.entityNewsList.sort(function (a, b) {
											return moment(a.updatedOn).isBefore(b.updatedOn) ? 1 : -1;
										});
									}
								}
							});
							/* 	if($scope.entitySearchResult.list['company_key_executives'].length > 0){
								//	$scope.entitySearchResult.list['mixed_company_key_executives'] = $scope.entitySearchResult.list['company_key_executives'];
								} */
							/* if($scope.entitySearchResult.list['company_boardmembers'].length === 0
								&& $scope.entitySearchResult.list['other_board_members'].length === 0
								&& $scope.entitySearchResult.list['company_key_executives'].length === 0){
								$scope.entitySearchResult.is_data_not_found.is_data_leadership =false;
							} */
							/* 	if($scope.entitySearchResult.list['company_boardmembers'].length === 0
									&& $scope.entitySearchResult.list['other_board_members'].length === 0){
						$scope.entitySearchResult.is_data_not_found.board_of_director=false;
								} */
							if ($scope.entitySearchResult.list['audit_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_audit = false;
							}
							if ($scope.entitySearchResult.list['nominating_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_nominating = false;
							}
							if ($scope.entitySearchResult.list['corporate_governance_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_corporate = false;
							}
							if ($scope.entitySearchResult.list['compensation_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_compensation = false;
							}
							/* if ($scope.entitySearchResult.list['mixed_company_key_executives'].length === 0) {
						$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
					} */
						}
						if (subfetcherData.fetcher === 'glassdoor.com') {
							if (subfetcherData.entities.length == 0) {
								// show error messages that did not found
								// data
								return;
							}
							var nested_data = d3.nest().key(function (d) { return d.relationshipName }).entries(subfetcherData.entities[0].edges)
							var rating_insights;
							angular.forEach(nested_data, function (d, i) {
								if (d.key == "rating_insights") {
									rating_insights = d.values;
								}
								if (d.key == "current_job_openings") {
									$scope.entitySearchResult.list['current_job_openings'] = d.values;
								}
								if (d.key == "ratings_trends") {
									$scope.entitySearchResult.list['ratings_trends'] = d.values;
								}
							});
							$scope.entitySearchResult.list['glassdoor'] = {};
							angular.forEach(rating_insights, function (d, i) {
								var valPercent = parseFloat((parseFloat(d.entity.properties.value) / 5) * 100).toFixed(1); // out
								valPercent = valPercent < 0 ? 0 : valPercent;
								$scope.entitySearchResult.list['glassdoor'][d.entity.properties['type_']] = {
									percent: valPercent,
									percentRounded: parseInt(valPercent)
								};
							})
							if ($scope.mainInfoTabType == 'Leaderships') {
								plotAllChartsForLeaderShip()
							}
							if ($scope.entitySearchResult.list['ratings_trends'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_ratings_trends = false;
							}
							if ($scope.entitySearchResult.list['current_job_openings'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_carrer_opportuny = false;
							}
						}
						if (subfetcherData.fetcher === 'channels.youtube.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ((subedv.relationshipName) == 'videos') {
										angular.forEach(subedv.entity.edges, function (subedvVal, subedvKey) {
											if ((subedvVal.relationshipName) == 'thumbnails') {
												angular.forEach(subedvVal.entity.edges, function (subedvEntityVal, subedvEntityKey) {
													if ((subedvEntityVal.relationshipName) == 'high') {
														videos_list.push({
															title: subedv.entity.properties.title,
															watch_url: subedv.entity.properties.watch_url,
															thumbnail: subedvEntityVal.entity.name,
															description: subedv.entity.properties.description
														});
													}
												});
											}
										});
									}
								});
							});
						}
						if (subfetcherData.fetcher === 'shodan.com') {
							if (subfetcherData.entities.length == 0 || subfetcherData.entities[0].edges.length == 0) {
								// show error messages that did not found
								// data
								return;
							}
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'gps_location') {
										shodanData.gps_locations.push({
											lat: subedv.entity.properties.latitude,
											long: subedv.entity.properties.longitude,
											mark: 'assets/images/redpin.png',
											name: subentity.properties.ip,
											source: subedv.entity.source,
											title: subentity.properties.ip,
											country_name: subentity.properties.country_name,
											details: subentity.properties.details,
											organization: subentity.properties.organization,
											postal_code: subentity.properties.postal_code,
											isp: subentity.properties.isp,
											'txt': '<span>Title: ' + subentity.properties.title + '</span></br><span>organization: ' + subentity.properties.organization + '</span></br><span>details: ' + subentity.properties.details + '</span></br><span>IP: ' + subentity.properties.ip + '</span></br><span>isp: ' + subentity.properties.isp + '</span></br><span>last update: ' + subentity.properties.last_update + '</span></br><span>city: ' + subentity.properties.city + '</span></br><span>country name: ' + subentity.properties.country_name + '</span></br><span>Source: ' + subedv.entity.source + '</span>'
										})
									}
									if (subedv.relationshipName === 'open_ports') {
										shodanData.open_ports.push({
											name: subedv.entity.name
										})
									}
								});
							});
							digitalFootPrintChart.markers = shodanData['gps_locations'];
							World(digitalFootPrintChart);
						}
					} else {
						var fetcherName = subfetcherData.message.split(' ')[1];
						if (subfetcherData.fetcher === 'bloomberg.com') {
							$scope.entitySearchResult.is_data_not_found.is_data_leadership = false;
							$scope.entitySearchResult.is_data_not_found.board_of_director = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_audit = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_compensation = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_nominating = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_corporate = false;
						}
						if (subfetcherData.fetcher === 'stocks.bloomberg.com') {
							// if (_.isEmpty(bloombergStockLineChartData)) {
								$scope.entitySearchResult.is_data_not_found.is_stock_performance = false;
							// }
							$scope.entitySearchResult.is_data_not_found.stockprice_div = false;
						}
						/* 	if(subfetcherData.fetcher === 'alexa.com') {
								$scope.entitySearchResult.is_data_not_found.is_traffic_by_country =false;
								$scope.entitySearchResult.is_data_not_found.is_traffic_key_word =false;
								$scope.entitySearchResult.is_data_not_found.is_traffic_by_domain =false;
							} */
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							$scope.entitySearchResult.is_data_not_found.associated_company = false;
							$scope.entitySearchResult.is_data_not_found.is_politcal_donation = false;
							$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							$("#politicalOrgs").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
							$("#politicaldonations").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
						}
						if (subfetcherData.fetcher === 'companieslist.co.uk') {
							$scope.entitySearchResult.is_data_not_found.is_nearby_company = false;
							$scope.entitySearchResult.is_data_not_found.is_company_member = false;
						}
						if (subfetcherData.fetcher === 'stocks.money.cnn.com') {
							$scope.entitySearchResult.is_data_not_found.is_topholders = false;
							$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
							$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
							$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
							$scope.entitySearchResult.is_data_not_found.is_shareholder = false;
							$scope.entitySearchResult.is_data_not_found.is_investment = false;
						}
						if (subfetcherData.fetcher === 'finance.yahoo.com') {
							var Empty_yahooStockData = false;
							if (_.isEmpty($scope.yahooStockData)) {
								Empty_yahooStockData = true;
							}
							if (Empty_yahooStockData) {
								$scope.entitySearchResult.is_data_not_found.is_financialStatement = false;
								$scope.entitySearchResult.is_data_not_found.is_stockHistory = false;
							}
							$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
						}
						if (subfetcherData.fetcher === 'xforce.ibmcloud.com') {
							$scope.entitySearchResult.is_data_not_found.is_cyberalert = false;
						}
						if (subfetcherData.fetcher === 'risidata.com') {
							$scope.entitySearchResult.is_data_not_found.is_indus_security_inc = false;
						}
						if (subfetcherData.fetcher === 'techcrunch.com') {
							$scope.entitySearchResult.is_data_not_found.is_latestnews = false;
						}
						if (subfetcherData.fetcher === 'meetup.com') {
							$scope.entitySearchResult.is_data_not_found.is_recent_events = false;
						}
						if (subfetcherData.fetcher === 'plus.google.com') {
							$scope.entitySearchResult.is_data_not_found.is_googlepost = false;
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							$('#tagcloudCompanyTwitterTags').html('<div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper cloud-tag-ditto"><div class="alert-message-wrapper alert-message">Data Not Found</div></div>');
						}
						if (subfetcherData.fetcher === 'instagram.com') {
							$scope.entitySearchResult.is_data_not_found.is_instagramlist = false;
						}
						if (subfetcherData.fetcher === 'pages.facebook.com') {
							$scope.entitySearchResult.is_data_not_found.is_facebook_list = false;
						}
						if (subfetcherData.fetcher === 'glassdoor.com') {
							$scope.entitySearchResult.is_data_not_found.is_ratings_trends = false;
							$scope.entitySearchResult.is_data_not_found.is_carrer_opportuny = false;
						}
						if (subfetcherData.fetcher === 'answerstedhctbek.onion') {
							$scope.entitySearchResult.is_data_not_found.is_populartags = false;
						}
						if (subfetcherData.fetcher === 'companies.corporationwiki.com') {
							$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
						}
						if (subfetcherData.fetcher === 'badbuyerslist.org') {
							$scope.entitySearchResult.list['is_badbuyerslist'] = false;
							$scope.entitySearchResult.list.badbbuyer_initial = false;
						}
						if (subfetcherData.fetcher === 'supplierblacklist.com') {
							$scope.entitySearchResult.is_data_not_found.is_supplierlist = false;
						}
						if (subfetcherData.fetcher === 'scam.com') {
							$scope.entitySearchResult.is_data_not_found.is_scam = false;
						}
					}
				});
				if (key === fetcherIds.length - 1) {
					deffered.resolve($scope.entitySearchResult);
				}
			}, function (error) {
				deffered.reject(error.responsemessage);
			});
		});
		// calling web socket
		return deffered.promise;
	}
	var RiskRatioflag = false;
	// $("#sentiments-chart-wrapper").empty();
	function plotAllChartsForLeaderShip() {
		$timeout(function () {
			// trend line starts
			var recentTrend;
			if ($scope.entitySearchResult.list['ratings_trends']) {
				if ($scope.entitySearchResult.list['ratings_trends'].length > 0) {
					var trendLineData = [];
					angular.forEach($scope.entitySearchResult.list['ratings_trends'], function (d, i) {
						if (!recentTrend) {
							recentTrend = {
								date: d.entity.properties.date,
								sum: parseFloat(d.entity.properties.rating).toFixed(2)
							}
						} else {
							if (new Date(recentTrend.date) < new Date(d.entity.properties.date)) {
								recentTrend = {
									date: d.entity.properties.date,
									sum: parseFloat(d.entity.properties.rating).toFixed(2)
								}
							}
						}
						trendLineData.push({
							date: d.entity.properties.date,
							sum: parseFloat(d.entity.properties.rating).toFixed(2)
						})
					});
					$scope.entitySearchResult.list.recentRatingTrend = recentTrend.sum;
					/* calling Overall Trend line chart */
					EntityCommonTabService.plotLineChart(trendLineData, "#lineChartOverallTrend", $("#lineChartOverallTrend").width(), '200');
				}
				// trend line ends
			}
			$timeout(function () {
				// tag cloud variables initializations for person
				$scope.loadRelatedPersonData();
			}, 10)
		}, 10)
	}
	$scope.loadRelatedPersonData = function () {
		tagCloudPersonOptions.domain.y = 5000;
		if (EntityCommonTabService.entityChartSharedObject.tagCloudNameList.length > 0) {
			$scope.entitySearchResult.list['related_persons_tags'] = false;
			// tagCloudPersonOptions.data =  EntityCommonTabService.entityChartSharedObject.tagCloudNameList;
			// else
			tagCloudPersonOptions.data = EntityCommonTabService.entityChartSharedObject.tagCloudNameList.splice(0, 30);
			
			
		}else{
			let tempName =  EntityCommonTabService.basicsharedObject.totalOfficers_link.slice(0,30);
			tagCloudPersonOptions.data = tempName.map((val) => {
				var person = {
					'size': 5,
					'text': val.name ? val.name : '',
					'type': 'person'
				};
				return person;
			});
		}
		if( !$("#tagcloudCompanyPerson").width() || $("#tagcloudCompanyPerson").width() === 0){
			$timeout(()=>{
				$scope.loadRelatedPersonData();
			},2000);
			}else{
				EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudPersonOptions);
			}
	};
}