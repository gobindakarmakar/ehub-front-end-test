'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyLatestNewsController', entityCompanyLatestNewsController);
entityCompanyLatestNewsController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
    'TopPanelApiService',
    'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyLatestNewsController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
    TopPanelApiService,
    EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
		var popular_tag_words = [];
		var overallsentiments = [], type = ''
	var countNews = 0;
	var sentimentAdverseNews = [];
	var newsSearchlength;
	$scope.sentimentsLoader = false;
		var tagCloudPopularOptions =EntityCommonTabService.basicsharedObject.tagCloudPopularOptions;
		$scope.$on('latestNewsOnload', function(e) {  
			if(EntityCommonTabService.lazyLoadingentity.newsTabFirsttime){
				getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.newsTabFetchers).then(function (responsemessage) {
					console.log('responsemessage: ', responsemessage);
				})
				EntityCommonTabService.lazyLoadingentity.newsTabFirsttime = false;
				if (EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLatestNews"] && EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLatestNews"].vertices.length > 0) {
					$scope.entitySearchResult.is_data_not_found.is_networkgraph = false;
					EntityCommonTabService.loadDataAndPlotGraphForEntity({
						"data": EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphLatestNews"],
						"id": "networkGraphLatestNews"
					});
				}
			}else{
				if (popular_tag_words.length > 0) {
					$scope.entitySearchResult.list['is_popular_tag_chart'] = false;
					/* plotting popular tag cloud chart */
					$("#tagCloudPopularTags").empty();
					tagCloudPopularOptions.data = popular_tag_words;
					EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudPopularOptions);
				}
				increment(newsSearchlength,"tab");
				if (popular_tag_words.length === 0) {
					$scope.entitySearchResult.is_data_not_found.is_populartags = false;
				}
			}
		
			
        });
		function getEntityCompanyDataByName(name, fetchers) {
			var deffered = $q.defer();
			var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
			var social_activity_locations = [];
			var maxTwitterTagDomainRangeValue = 5;
			var companyNameSplitted = name.split(' ');
			var copyOfCompanyName = '';
			var twitter_tag_words = EntityCommonTabService.entityChartSharedObject.twitter_tag_words;
			angular.forEach(companyNameSplitted, function (word, key) {
				var index = Company_Unwanted_End_Points.indexOf(word.toLowerCase());
				if (index === -1) {
					copyOfCompanyName += word + ' ';
				}
			});
			copyOfCompanyName = copyOfCompanyName.trim();
			angular.forEach(fetcherIds, function (id, key) {
				var ids = [];
				ids.push(id);
				var data = {
					fetchers: ids,
					keyword: name,
					searchType: 'Company'
				};
				// if(ids.indexOf('13') !== -1)
				// data.lightWeight = true;
				if (ids.indexOf('2') !== -1 || ids.indexOf('11') !== -1) {
					fullNameOfCompanyFounder !== '' ? data.keyword = fullNameOfCompanyFounder + ',' + name : '';
				}
				if (ids.indexOf('3004') !== -1 || ids.indexOf('3005') !== -1) {
					data.keyword = $stateParams['website'] ? $stateParams['website'] : websiteOfCompanyFounder !== '' ? websiteOfCompanyFounder : 'www.' + name + '.com';
				}
				if (ids.indexOf('3001') !== -1 || ids.indexOf('3002') !== -1 || ids.indexOf('3003') !== -1 || ids.indexOf('3008') !== -1) {
					data.keyword = $stateParams['website'] ? getWebsiteHostName($stateParams['website']) : websiteOfCompanyFounder !== '' ? getWebsiteHostName(websiteOfCompanyFounder) : name + '.com';
				}
				if (id == '1022') {
					data.keyword = $stateParams['website'] ? $stateParams['website'] : websiteOfCompanyFounder !== '' ? websiteOfCompanyFounder : 'www.' + name + '.com';
				}
				if (id == '2001' || id == '1008' || id == '21' || id == '1016' || id == '1017') {
					data.keyword = copyOfCompanyName;
				}
				if (id == '2001' || id == '2' || id == '1004' || id == '1005' || id == '1006' || id == '1008' || id == '4001' ||
					id == '1009' || id == '33' || id == '1010' || id == '1011' || id == '1012' || id == '48' || id == '16' || id == '21' || id == '23' ||
					id == '1017' || id == '1021' || id == '1022' || id == '1016') {
					data.limit = '1';
				}
				/* 
				*/
				var temp = _.find(handlingCompanyConstantName, { 'name': data.keyword, 'fetcher': id });
				data.keyword = temp === undefined ? data.keyword : temp.alt_name;
				data.limit = temp === undefined ? data.limit : temp.limit;
				EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
					var currentSubFetcher = id;
					angular.forEach(subResponse.data.results, function (subfetcherData, key) {
						if (subfetcherData.status) {
							if (subfetcherData.fetcher === 'stocks.money.cnn.com') {
								$scope.isStockDataFound = true;
								var finalLocationList = [], locationNames = [];
								angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									if (subentityKey === 0) {
										angular.forEach(subentity.edges, function (subedv, subedk) {
											if (subedv.relationshipName === 'institutional_ownerships' || subedv.relationshipName === 'insider_shareholders') {
												angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
													if (subedgev.relationshipName === 'top_owners') {
														stockModalData['top_owners'].push(subedgev.entity.properties);
														$scope.entitySearchResult.list['top_holders'] = stockModalData['top_owners'];
													}
													if (subedgev.relationshipName === 'top_mutual_holding') {
														stockModalData['top_mutual_holding'].push(subedgev.entity.properties);
													}
													if (subedgev.relationshipName === 'recent_institutional_activity') {
														stockModalData['recent_institutional_activity'].push(subedgev.entity.properties);
													}
												});
											}
											if (subedv.relationshipName === 'press_releases') {
												subedv.entity.properties.time = new Date(subedv.entity.properties.time);
												stockModalData['press_releases'].push(subedv.entity.properties);
												$scope.entitySearchResult.list['press_releases'] = stockModalData['press_releases'];
												angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
													if (subedgev.relationshipName === 'locations') {
														stockModalData['locations'].push({
															name: subedgev.entity.name,
															risk_score: subedgev.entity.properties.risk_score
														});
														if (subedgev.entity.name !== '' && subedgev.entity.name) {
															newsLocationList.push({
																name: subedgev.entity.name,
																long: '',
																lat: '',
																mark: 'assets/images/redpin.png',
																source: subedgev.entity.source,
																title: subedgev.entity.properties.title || subedgev.entity.source
															});
														}
														$scope.entitySearchResult.list['locations'] = stockModalData['locations'];
													}
												});
												angular.forEach(newsLocationList, function (locationValue, locationKey) {
													if (locationNames.indexOf(locationValue.name) === -1) {
														locationNames.push(locationValue.name);
														finalLocationList.push(locationValue);
													}
												});
											}
											if (subedv.relationshipName === 'financial_statements') {
												angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
													if (subedgev.relationshipName === 'balance_sheet') {
														angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
															stockModalData['balance_sheet'][subedgev2.relationshipName] = {};
															angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
																stockModalData['balance_sheet'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
															});
														});
													}
													if (subedgev.relationshipName === 'income_statement') {
														angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
															stockModalData['income_statement'][subedgev2.relationshipName] = {};
															angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
																stockModalData['income_statement'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
															});
														});
													}
													if (subedgev.relationshipName === 'cash_flow_statement') {
														angular.forEach(subedgev.entity.edges, function (subedgev2, subedgek2) {
															stockModalData['cash_flow_statement'][subedgev2.relationshipName] = {};
															angular.forEach(subedgev2.entity.edges, function (subedgev3, subedgek3) {
																stockModalData['cash_flow_statement'][subedgev2.relationshipName][subedgev3.relationshipName] = subedgev3.entity.properties.value;
															});
														});
													}
												});
											}
											if (subedv.relationshipName === 'stock_profile') {
												angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
													if (subedgev.relationshipName === 'growth_and_valuation') {
														stockModalData['stock_profile']['growth_and_valuation'] = subedgev.entity.properties;
													}
													if (subedgev.relationshipName === 'today_trading') {
														stockModalData['stock_profile']['today_trading'] = subedgev.entity.properties;
													}
													if (subedgev.relationshipName === 'company_profile') {
														stockModalData['stock_profile']['company_profile'] = subedgev.entity.properties;
													}
													if (subedgev.relationshipName === 'competitors') {
														stockModalData['stock_profile']['competitors'].push(subedgev.entity.properties);
													}
													if (subedgev.relationshipName === 'other_values') {
														stockModalData['stock_profile']['other_values'] = subedgev.entity.properties;
													}
													if (subedgev.relationshipName === 'financials') {
														stockModalData['stock_profile']['financials'] = subedgev.entity.properties;
													}
												});
											}
											if (subedv.relationshipName === 'company_profile') {
												stockModalData['company_profile']['data'] = subedv.entity.properties;
												angular.forEach(subedv.entity.edges, function (subedgev, subedgek) {
													if (subedgev.relationshipName === 'shareholders') {
														stockModalData['company_profile']['shareholders'] = subedgev.entity.properties;
													}
												});
											}
										});
									}
									angular.forEach(subentity.properties, function (subValue, subKey) {
										$scope.entitySearchResult['stocks']['cnn_prop'][subKey] = subValue
									});
								});
								worldChartNewsLocationsOptions.markers = finalLocationList;
								/* calling world map chart function */
								World(worldChartNewsLocationsOptions);
								$scope.stockModalData = stockModalData;
								if (!_.isEmpty(stockModalData)) {
									handle_modalData_statement(stockModalData);
									calculateStatementInfo(stockModalData);
									//   loadfinancialpie(stockModalData);
								} else {
									$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
									$scope.entitySearchResult.is_data_not_found.is_investment = false;
								}
								if ($scope.entitySearchResult.list['top_holders'].length === 0) {
									$scope.entitySearchResult.is_data_not_found.is_topholders = false;
								}
								if ($scope.entitySearchResult.list['press_releases'].length === 0) {
									$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
								}
								if (_.isEmpty(stockModalData) || _.isEmpty(stockModalData['company_profile']['shareholders'])) {
									$scope.entitySearchResult.is_data_not_found.is_investment = false;
								}
								if (_.isEmpty(stockModalData) || _.isEmpty(stockModalData['cash_flow_statement'])) {
									$scope.entitySearchResult.is_data_not_found.is_investment = false;
								}
							}
							
							if (subfetcherData.fetcher === 'techcrunch.com') {
								angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									$scope.entityNewsList.push({
										title: subentity.properties.title,
										updatedOn: new Date(subentity.properties.updated_at),
										image: subentity.properties.image,
										details: subentity.properties.details,
										link: subentity.properties.link,
										source: subentity.source
									});
								});
								if ($scope.entitySearchResult.list["all_news_articles"].length > 0) {
									angular.forEach($scope.entitySearchResult.list["all_news_articles"], function (object, key) {
										$scope.entityNewsList.push(object);
									});
									$scope.entityNewsList = $scope.entityNewsList.sort(function (a, b) {
										return moment(a.updatedOn).isBefore(b.updatedOn) ? 1 : -1;
									});
								}
								if ($scope.entityNewsList.length === 0) {
									$scope.entitySearchResult.is_data_not_found.is_latestnews = false;
								}
							}
							if (subfetcherData.fetcher === 'meetup.com') {
								angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									if (subentityKey === 0) {
										var meetupObject = {
											title: subentity.properties.title,
											group_name: subentity.properties.group_name,
											group_url: subentity.properties.group_url,
											date: new Date(subentity.properties.date),
											attend_count: subentity.properties.attend_count,
											details: subentity.properties.details,
											url: subentity.properties.url,
											source: subentity.source,
											groups: {},
											events: {}
										};
										angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
											if (edgeValue.relationshipName === 'group') {
												var groupObject = {
													name: edgeValue.entity.properties.found,
													city: edgeValue.entity.properties.city,
													members: edgeValue.entity.properties.members,
													details: edgeValue.entity.properties.details,
													recent_events: []
												};
												angular.forEach(edgeValue.entity.edges, function (subsubEdv, subsubKey) {
													if (subsubEdv.relationshipName === 'recent_events') {
														var eventObject = {
															title: subsubEdv.entity.properties.title,
															held_on: subsubEdv.entity.properties.held_on,
															link: subsubEdv.entity.properties.link,
															details: subsubEdv.entity.properties.details
														};
														groupObject.recent_events.push(eventObject);
													}
												});
												meetupObject.groups = groupObject;
											}
											if (edgeValue.relationshipName === 'event') {
												var groupEventObject = {
													organizer: edgeValue.entity.properties.organizer,
													address: edgeValue.entity.properties.address,
													event_date: edgeValue.entity.properties.event_date,
													details: edgeValue.entity.properties.details,
													going: edgeValue.entity.properties.going,
													going_members: []
												};
												angular.forEach(edgeValue.entity.edges, function (subsubEdv, subsubKey) {
													if (subsubEdv.relationshipName === 'going_members') {
														var memberObject = {
															name: subsubEdv.entity.properties.name,
															image: subsubEdv.entity.properties.image,
														};
														groupEventObject.going_members.push(memberObject);
													}
												});
												meetupObject.events = groupEventObject;
											}
										});
										$scope.entitySearchResult.list["meetupData"].push(meetupObject);
									}
								});
								if ($scope.entitySearchResult.list["meetupData"].length === 0) {
									$scope.entitySearchResult.is_data_not_found.is_recent_events = false;
								}
							}
							
							if (subfetcherData.fetcher === 'logo.google.com') {
								$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
							}
							if (subfetcherData.fetcher === 'answerstedhctbek.onion') {
								angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									var blogObject = {
										name: subentity.name,
										question: subentity.properties.question,
										title: subentity.properties.title,
										date: new Date(subentity.properties.date),
										details: subentity.properties.details,
										asked_by: subentity.properties.asked_by,
										url: subentity.properties.url,
										risk_score: subentity.properties.risk_score,
										source: subentity.source,
										answers: []
									};
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'answers' && subedv.entity.properties.answer !== '' && subedv.entity.properties.answer) {
											var answerObject = {
												answer: subedv.entity.properties.answer,
												risk_score: subedv.entity.properties.risk_score,
												answerd_by: subedv.entity.properties.answerd_by,
												answerd_date: new Date(subedv.entity.properties.answerd_date),
												comments: []
											};
											angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
												if (subsubedv.relationshipName === 'comments' && subsubedv.entity.properties.text !== '' && subsubedv.entity.properties.text) {
													var commentObject = {
														text: subsubedv.entity.properties.text,
														risk_score: subsubedv.entity.properties.risk_score,
														comment_by: subsubedv.entity.properties.comment_by,
														date: new Date(subsubedv.entity.properties.date)
													};
													answerObject.comments.push(commentObject);
												}
											});
											blogObject.answers.push(answerObject);
										}
										if (subedv.relationshipName === 'tags' && subedv.entity.name !== '' && subedv.entity.name) {
											var index = -1;
											angular.forEach(popular_tag_words, function (object, key) {
												if (object.text === subedv.entity.name)
													index = key
											});
											if (index === -1) {
												popular_tag_words.push({
													text: subedv.entity.name,
													size: 5,
													type: 'tags'
												});
											} else {
												popular_tag_words[index].size += 5;
												popular_tag_words[index].size > maxTwitterTagDomainRangeValue ? maxTwitterTagDomainRangeValue = popular_tag_words[index].size : '';
											}
										}
									});
									$scope.entitySearchResult.list['answerstedhctbek'].push(blogObject);
								});
							}
							if (subfetcherData.fetcher === 'twitter.com') {
								angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
									angular.forEach(subentity.properties, function (subev, subek) {
										if (subek === 'location' && subev !== '' && subev) {
											social_activity_locations.push({
												name: subev,
												long: '',
												lat: '',
												mark: 'assets/images/redpin.png',
												source: subentity.source,
												title: subentity.properties.title || subentity.source,
												'txt': '<span>location: ' + subentity.properties.location + '</span></br><span>Title: ' + subentity.properties.title + '</span></br><span>screen name: ' + subentity.properties.screen_name + '</span></br><span>details: ' + subentity.properties.details + '</span></br><span>profile created at: ' + subentity.properties.profile_created_at + '</span></br><span>source: ' + subentity.properties.source + '</span>'
											});
										}
										!$scope.entitySearchResult.hasOwnProperty(subek) && UnWantedKeyList.indexOf(subek) === -1 && subev !== '' && subev ? $scope.entitySearchResult[subek] = subev : '';
									});
									if (subentityKey === 0) {
										angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
											if (edgeValue.relationshipName === 'tweets') {
												var objectData = {
													title: subentity.properties.title,
													createdOn: new Date(edgeValue.entity.properties.created_at),
													image: subentity.properties.image,
													text: edgeValue.entity.properties.text,
													retweetCount: edgeValue.entity.properties.retweet_count,
													tags: [],
													type: 'twitter'
												};
												angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
													if (subedv.relationshipName === 'user_mentions') {
														objectData.tags.push({
															name: subedv.entity.properties.name,
															screen_name: subedv.entity.properties.screen_name,
															risk_score: subedv.entity.properties.risk_score
														});
														var index = -1;
														angular.forEach(twitter_tag_words, function (object, key) {
															if (object.text === subedv.entity.properties.name)
																index = key
														});
														if (index === -1) {
															twitter_tag_words.push({
																text: subedv.entity.properties.name,
																size: 5,
																type: 'person'
															});
														} else {
															twitter_tag_words[index].size += 5;
															twitter_tag_words[index].size > maxTwitterTagDomainRangeValue ? maxTwitterTagDomainRangeValue = twitter_tag_words[index].size : '';
														}
													}
												});
												$scope.entityTwitterList.push(objectData);
												$scope.entitySearchResult['twitter_retweets_count'] += parseInt(edgeValue.entity.properties.retweet_count);
												$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1)
											}
											if (edgeValue.relationshipName === 'followers') {
												if (edgeValue.entity.name !== '') {
													$scope.entitySearchResult.list[edgeValue.relationshipName].push({
														name: edgeValue.entity.name,
														createdOn: new Date(edgeValue.entity.properties.profile_created_at),
														image: edgeValue.entity.properties.profile_image_url,
														screen_name: edgeValue.entity.properties.screen_name,
														risk_score: edgeValue.entity.properties.risk_score,
														source: subentity.source
													});
												}
											}
											if (edgeValue.relationshipName === 'following') {
												if (edgeValue.entity.name !== '') {
													$scope.entitySearchResult.list[edgeValue.relationshipName].push({
														name: edgeValue.entity.name,
														createdOn: new Date(edgeValue.entity.properties.profile_created_at),
														image: edgeValue.entity.properties.profile_image_url,
														screen_name: edgeValue.entity.properties.screen_name,
														risk_score: edgeValue.entity.properties.risk_score,
														source: subentity.source
													});
												}
											}
											if (edgeValue.relationshipName === 'follower_following_count') {
												$scope.entitySearchResult.list['follower_following_count'].push({
													twitter_followers_number: edgeValue.entity.properties.followers,
													twitter_following_number: edgeValue.entity.properties.following
												});
											}
										});
										if ($scope.entityTwitterList.length > 0)
											$scope.socialMediaActiveCount = 0;
									}
								});
								$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1);
								if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
									$scope.entitySearchResult.list['followers_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number), 1);
									$scope.entitySearchResult.list['twitter_following_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_following_number), 1);
								}
								if ($scope.entitySearchResult.list['follower_following_count'].length === 0) {
									$scope.entitySearchResult.is_data_not_found.is_twitter_list = false;
								}
								if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
									var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
										return d.name == 'twitter'
									})
									if (index < 0) {
										EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
											"name": "twitter",
											"size": $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number
										});
									}
								}
								EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.markers = social_activity_locations;
								// calling world map chart function
							}

						} else {
							var fetcherName = subfetcherData.message.split(' ')[1];
							
							if (subfetcherData.fetcher === 'stocks.bloomberg.com') {
								if (_.isEmpty(bloombergStockLineChartData)) {
									$scope.entitySearchResult.is_data_not_found.is_stock_performance = false;
								}
								$scope.entitySearchResult.is_data_not_found.stockprice_div = false;
							}
						
							if (subfetcherData.fetcher === 'stocks.money.cnn.com') {
								$scope.entitySearchResult.is_data_not_found.is_topholders = false;
								$scope.entitySearchResult.is_data_not_found.is_balancesheet = false;
								$scope.entitySearchResult.is_data_not_found.is_incomeStatement = false;
								$scope.entitySearchResult.is_data_not_found.is_press_releases = false;
								$scope.entitySearchResult.is_data_not_found.is_shareholder = false;
								$scope.entitySearchResult.is_data_not_found.is_investment = false;
							}
							
							if (subfetcherData.fetcher === 'answerstedhctbek.onion') {
								$scope.entitySearchResult.is_data_not_found.is_populartags = false;
							}
						}
					});
					if (key === fetcherIds.length - 1) {
						deffered.resolve($scope.entitySearchResult);
					}
				}, function (error) {
					deffered.reject(error.responsemessage);
				});
			});
			// calling web socket
			return deffered.promise;
		
	}
	
	function increment(newsSearchlength, tab) {
		var colorsObj = {
			"negative": "#b753cd",
			"positive": "#69ca6b",
			"neutral": "#5d96c8"
		}
		if (!tab)
			countNews += 1;
		$timeout(function () {
			var timelineOptions = {
				container: ".overallSentimentChart-wrapper",
				height: 50,
				colors: ["#a75dc2", "#5D97C9", "#399034"],
				data: overallsentiments,
				colorsObj: colorsObj
			};
			var setimentsPieData = d3.nest()
				.key(function (d) { return d.type; })
				.rollup(function (sentiments) { return sentiments.length; })
				.entries(overallsentiments);
			setimentsPieData.map(function (d) {
				return d['doc_count'] = d.value;
			})
			$timeout(function () {
				// if($scope.mainInfoTabType == 'Latest News'){
				loadtimeLineColumnChart(timelineOptions);
				// calling General Sentiment pie chart
				EntityCommonTabService.InitializeandPlotPie(setimentsPieData, 'pieChartGeneralSentiment', colorsObj);
				// }
			}, 100)
		}, 1000);
		if (countNews == newsSearchlength) {
			$scope.sentimentsLoader = false;
			if ($scope.actualScreeningData.length > 0) {
				$scope.actualScreeningData[0].non_negativenews = [];
				$scope.actualScreeningData[0].non_negativenews = sentimentAdverseNews;
			}
			if (overallsentiments.length === 0) {
				$timeout(function () {
					$("#pieChartGeneralSentiment").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
					$('.overallSentimentChart-wrapper').append("<div class=\"alert-message-wrapper\"><div class=\"alert-message \">Data Not Found</div></div>");
				}, 2000)
			}
		}
	}
}