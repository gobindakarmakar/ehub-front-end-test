'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyRiskAlertsNewController', entityCompanyRiskAlertsNewController);
entityCompanyRiskAlertsNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'EntityCommonTabService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant'
];
function entityCompanyRiskAlertsNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	EntityCommonTabService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant) {
		var colors = {
			"POSITIVE": "69CA6B",
			"NEUTRAL": "#5D97C9",
			"NEGATIVE": "EB2481"
		};
		var colorsObj = colors;
		var RiskRatioflag = false;
	$scope.$on('riskAlertsOnload', function (e) {
		if (EntityCommonTabService.lazyLoadingentity.riskTabFirsttime) {
			getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.riskFetchers).then(function (responsemessage) {
				console.log('responsemessage: ', responsemessage);
				if ($scope.entitySearchResult.list['sanctionList'].length === 0) {
					$scope.entitySearchResult.is_data_not_found.is_sanctionedlist = false;
				}
				if ($scope.entitySearchResult.list['denied_personlist'].length === 0) {
					$scope.entitySearchResult.is_data_not_found.is_deninedperson = false;
				}
                if ($scope.entitySearchResult.list['fbi_data'].length  === 0) {
					$scope.entitySearchResult.is_data_not_found.is_fbidata = false;
				}
                if ($scope.entitySearchResult.list['adverse_news'].length  === 0) {
					$scope.entitySearchResult.is_data_not_found.is_adversenews = false;
				}
				
			});
			risktabGraph();
			EntityCommonTabService.lazyLoadingentity.riskTabFirsttime = false;
		}else{			
			$('#politicaldonations').html('<div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>');
			$('#politicalOrgs').html('<div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>');
			if (EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphRiskAlerts"] && EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphRiskAlerts"].vertices.length > 0) {
				EntityCommonTabService.loadDataAndPlotGraphForEntity({
					"data": EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphRiskAlerts"],
					"id": "networkGraphRiskAlerts"
				});
			}
			$timeout(function () {
				PlotriskAlertCharts();
				PlotPoliticalChart();
			}, 10)
		
			$scope.onClickFraudTypeTabs('FBI Data');

		}
	})

	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		var companyNameSplitted = name.split(' ');
		var copyOfCompanyName = '';
		angular.forEach(companyNameSplitted, function (word, key) {
			var index = Company_Unwanted_End_Points.indexOf(word.toLowerCase());
			if (index === -1) {
				copyOfCompanyName += word + ' ';
			}
		});
		copyOfCompanyName = copyOfCompanyName.trim();
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company'
			};
			var temp = _.find(handlingCompanyConstantName, { 'name': data.keyword, 'fetcher': id });
			data.keyword = temp === undefined ? data.keyword : temp.alt_name;
			data.limit = temp === undefined ? data.limit : temp.limit;
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				var currentSubFetcher = id;
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							if (subentityKey === 0) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
										angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
											// subsubEnK == 'name' && subsubEnV !== '' ?
											// $scope.entitySearchResult.list[subedv.relationshipName].push(subsubEnV) : '';
											if (d3.keys($scope.entitySearchResult.list["resolve_related_similar_companies"]).length > 0) {
												if (subsubEnK == 'name' && subsubEnV !== '' && $scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											} else {
												if (subsubEnK == 'name' && subsubEnV !== '' && !subenvFlag) {
													if (!$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
														$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher] = [];
													}
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													subenvFlag = true;
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											}
										});
									}
									if ((subedv.relationshipName) == 'specialities') {
										subedv.entity.name !== '' && subedv.entity.name ? specialities.push(subedv.entity.name) : '';
									}
									if ((subedv.relationshipName) == 'people_also_viewd') {
										if (subedv.entity.name !== '' && subedv.entity.name) {
											$scope.entitySearchResult.list['people_also_viewd'].push({
												name: subedv.entity.properties.company_name,
												company_size: subedv.entity.properties.company_size,
												industry: subedv.entity.properties.industry,
												link: subedv.entity.properties.link,
												logo: subedv.entity.properties.logo,
												risk_score: subedv.entity.properties.risk_score,
												source: subedv.entity.source
											})
											if ($scope.entitySearchResult.list['people_also_viewd'].length === 0) {
												$scope.entitySearchResult.is_data_not_found.is_people_visited = false;
											}
										}
									}
								});
							}
						});
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							angular.forEach(subentity.edges, function (subedv, subedk) {
								if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
									angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
										if (subsubEnK == 'name' && subsubEnV !== '') {
											$scope.entitySearchResult.list[subedv.relationshipName].push({
												name: subsubEnV,
												source: subentity.source
											});
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subsubEnV)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subsubEnV,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
									});
								}
							});
						});
						if (subfetcherData.fetcher === 'badbuyerslist.org') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0 && subentity.properties.details && subentity.properties.details !== '') {
									angular.forEach(subentity.properties, function (value, key) {
										if (value !== '' && value) {
											key = key.replace(/_/g, ' ');
											$scope.entitySearchResult.list['badbuyerslist'][key] = value;
										}
									});
									$scope.entitySearchResult.list['badbuyerslist']['source'] = subentity.source;
								}
							});
							if (_.isEmpty($scope.entitySearchResult.list['badbuyerslist'])) {
								$scope.entitySearchResult.list['is_badbuyerslist'] = false;
								$scope.entitySearchResult.list.badbbuyer_initial = false;
							}
						}
						if (subfetcherData.fetcher === 'logo.google.com') {
							$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
						}
						if (subfetcherData.fetcher === 'deepdotweb.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.properties.author_name !== '' && subentity.properties.author_name &&
									subentity.properties.author_url !== '' && subentity.properties.author_url) {
									$scope.entitySearchResult.list['news_deep_web'].push({
										name: subentity.properties.author_name,
										postedOn: new Date(subentity.properties.post_date),
										url: subentity.properties.author_url,
										title: subentity.properties.title,
										details: subentity.properties.details,
										source: subentity.source
									});
								}
							});
						}
						if (subfetcherData.fetcher === 'risidata.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.name !== '' && subentity.properties.eventImpact !== '' && subentity.properties.eventImpact) {
									$scope.entitySearchResult.list['risidata'].push({
										title: subentity.properties.title,
										eventImpact: subentity.properties.eventImpact,
										details: subentity.properties.details,
										industryType: subentity.properties.industryType,
										reliability: subentity.properties.reliability,
										country: subentity.properties.country,
										year: subentity.properties.year,
										risk_score: subentity.properties.risk_score,
										source: subentity.source
									});
								}
							});
							if ($scope.entitySearchResult.list['risidata'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_indus_security_inc = false;
							}
						}
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							var interlock = {};
							var subentity = subfetcherData.entities[0];
							var nested_data = d3.nest().key(function (d) { return d.relationshipName }).entries(subfetcherData.entities[0].edges);
							for (var i = 0; i < nested_data.length; i++) {
								if (nested_data[i].key === "org_interlocks") {
									for (var vl = 0; vl < nested_data[i].values.length; vl++) {
										var edgevalue = nested_data[i].values[vl];
										if (edgevalue.relationshipName === "org_interlocks") {
											if (edgevalue.entity.properties.connected_entity_name !== '') {
												var mainentity = edgevalue.entity.properties.connected_entity_name;
												interlock[mainentity] = [];
												for (var j = 0; j < edgevalue.entity.edges.length; j++) {
													var subedgevalue = edgevalue.entity.edges[j];
													if (subedgevalue.relationshipName === 'connecting_entities') {
														var connected_entity = subedgevalue.entity.name;
														interlock[mainentity].push(connected_entity);
													} // connecting
													// entities
													// ends if
												} // j loops ends
											}// checking for emty
											// name if ends
										}// org interlocks ends
									}// vl for loop ends
								}// interocks if loop ends
							}// nested for loop loop ends
							var result = _.map(interlock, function (value, prop) {
								return { prop: prop, value: value };
							});
							if (result.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							} else {
								$scope.interlock = result;
							}
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									// $scope.entitySearchResult['revenue'] = subentity.properties.revenue;
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'key_staff_attended_schools') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													person: edgeValue.entity.properties.person,
													person_url: edgeValue.entity.properties.person_url,
													school_attended: edgeValue.entity.properties.school_attended,
													school_url: edgeValue.entity.properties.school_url,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'connected_industries') {
											$scope.entitySearchResult.list['connected_industries'].push(edgeValue);
										}
										if (edgeValue.relationshipName === 'key_relationships' || edgeValue.relationshipName === 'political_financial_supports'
											|| edgeValue.relationshipName === 'connection') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list['key_relationships'].push({
													name: edgeValue.entity.name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
											angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
												if (edgeValue.relationshipName === 'connection') {
													$scope.entitySearchResult.list['associated_persons'][subedv.relationshipName].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'memberships') {
													$scope.entitySearchResult.list['associated_companies']['memberships'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'transaction') {
													$scope.entitySearchResult.list['associated_companies']['transaction'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_financial_supports') {
													$scope.entitySearchResult.list['associated_companies']['political_financial_supports'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'politicians') {
													$scope.entitySearchResult.list['associated_companies']['politicians'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_organizations') {
													$scope.entitySearchResult.list['associated_companies']['political_organizations'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'owners') {
													$scope.entitySearchResult.list['associated_companies']['owners'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'leadership_&_staff') {
													$scope.entitySearchResult.list['associated_companies']['leadership & staff'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'services_transactions') {
													$scope.entitySearchResult.list['associated_companies']['services transactions'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'child_organizations') {
													$scope.entitySearchResult.list['associated_companies']['child_organizations'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'holdings') {
													$scope.entitySearchResult.list['associated_companies']['holdings'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'donation_grant_recipients') {
													$scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'key_staff') {
													if ($.inArray(subedgev.entity.properties.name, key_staff_arr) == -1) {
														key_staff_arr.push(subedgev.entity.properties.name);
														$scope.entitySearchResult.list['key_staff'].push(subedgev.entity.properties);
													}
												}
											});
										}
									});
								}
							});
							if ($scope.mainInfoTabType == 'Risk Alerts') {
								PlotPoliticalChart();
							}
							if ($scope.entitySearchResult.list['associated_companies']['child_organizations'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['holdings'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['memberships'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['owners'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['services transactions'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.associated_company = false;
							}
						}
						if (subfetcherData.fetcher === 'supplierblacklist.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0 && subentity.properties.type_of_complaints !== '' && subentity.properties.type_of_complaints) {
									angular.forEach(subentity.properties, function (value, key) {
										if (value !== '' && value) {
											key = key.replace(/_/g, ' ');
											$scope.entitySearchResult.list['supplierblacklist'][key] = value;
										}
									});
									$scope.entitySearchResult.list['supplierblacklist'].Source = subentity.source;
								}
							});
							if (_.isEmpty($scope.entitySearchResult.list['supplierblacklist'])) {
								$scope.entitySearchResult.is_data_not_found.is_supplierlist = false;
							}
						}
						if (subfetcherData.fetcher === 'cypherpunk.at') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentity.properties.title !== '' && subentity.properties.title &&
									subentity.properties.details !== '' && subentity.properties.details) {
									var objectData = {
										title: subentity.properties.title,
										author_url: subentity.properties.author_url,
										details: subentity.properties.details,
										about_author: subentity.properties.about_author,
										risk_score: subentity.properties.risk_score,
										author: subentity.properties.author,
										dated: new Date(subentity.properties.date),
										tags: '',
										source: subentity.source
									};
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'tags') {
											objectData.tags === '' ? objectData.tags = subedv.entity.name :
												objectData.tags += ', ' + subedv.entity.name;
										}
									});
									$scope.entitySearchResult.list['cypherpunk'].push(objectData);
								}
							});
						}
					} else {
						var fetcherName = subfetcherData.message.split(' ')[1];
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							$scope.entitySearchResult.is_data_not_found.associated_company = false;
							$scope.entitySearchResult.is_data_not_found.is_politcal_donation = false;
							$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							$("#politicalOrgs").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
							$("#politicaldonations").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
						}
						if (subfetcherData.fetcher === 'xforce.ibmcloud.com') {
							$scope.entitySearchResult.is_data_not_found.is_cyberalert = false;
						}
						if (subfetcherData.fetcher === 'risidata.com') {
							$scope.entitySearchResult.is_data_not_found.is_indus_security_inc = false;
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							$('#tagcloudCompanyTwitterTags').html('<div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper cloud-tag-ditto"><div class="alert-message-wrapper alert-message">Data Not Found</div></div>');
						}
						if (subfetcherData.fetcher === 'badbuyerslist.org') {
							$scope.entitySearchResult.list['is_badbuyerslist'] = false;
							$scope.entitySearchResult.list.badbbuyer_initial = false;
						}
						if (subfetcherData.fetcher === 'supplierblacklist.com') {
							$scope.entitySearchResult.is_data_not_found.is_supplierlist = false;
						}
					}
				});
				if (key === fetcherIds.length - 1) {
					deffered.resolve($scope.entitySearchResult);
					PlotriskAlertCharts();
				}
			}, function (error) {
				deffered.reject(error.responsemessage);
			});
		});
		// calling web socket
		return deffered.promise;
	}
	function risktabGraph() {
		var data = {
			// "fetchers": ["33"],
			"fetchers": ["1008", "1006", "1013", "1021"],
			"keyword": $scope.entitySearchResult.name ? $scope.entitySearchResult.name : EntityCommonTabService.basicsharedObject.companyName,
			"searchType": "Company",
			"lightWeight": true,
			"limit": 2,
			"iterations": 1,
			"alias": "riskNetwork",
			"create_new_graph": false,
			"requires_expansion": true,
			"entity_resolution": true,
			"expansion_fetchers_lightweight": true,
			"expansion_fetchers": ["1012", "15", "19"]
		};
		loadoverviewVLA("networkGraphRiskAlerts", data);
	}
	var overviewCaseId;
	function loadoverviewVLA(id, data) {
		if ($("#" + id).find("canvas").length == 0) {
			$('#' + id)[0].style.display = "none";
			$('#' + id).siblings()[0].style.display = "";
			$('#' + id).siblings()[1].style.display = "none";
			$('#' + id).parent().siblings('a')[0].style.display = "none";
			var data = data;
			EntityApiService.getGraphData(data).then(function (response) {
				$('#' + id).html("");
				if (response !== void 0) {
					if (data.alias === "companyOverview") {
						overviewCaseId = response.data.caseId;
					}
					var caseId = response.data.caseId;
					EntityApiService.getVLAdata(caseId).then(function (resp) {
						if (resp != void 0) {
							EntityCommonTabService.entityChartSharedObject.vlaDataArr[id] = resp.data;
							var options = {
								data: resp.data,
								id: id
							}
							if (resp.data.vertices.length > 0) {
								$('#' + id)[0].style.display = "";
								$('#' + id).siblings()[0].style.display = "none";
								$('#' + id).parent().siblings('a')[0].style.display = "";
								$scope.loadDataAndPlotGraphForEntity(options);
							} else {
								$('#' + id).siblings()[1].style.display = "";
								$('#' + id).siblings()[0].style.display = "none";
							}
						}
					}, function () {
						$('#' + id).siblings()[1].style.display = "";
						$('#' + id).siblings()[0].style.display = "none";
					});
				}
			}, function (error) {
				$('#' + id).siblings()[1].style.display = "";
				$('#' + id).siblings()[0].style.display = "none";
			});
		}
	}
	$scope.loadDataAndPlotGraphForEntity = function (options, layout) {
		$('#' + options.id).html("");
		handleGraphData(options.data, options, layout)
		function handleGraphData(data, options, layout) {
			var finalData = {};
			var edges = [];
			var nodeIDS = [];
			var nodes = [];
			angular.forEach(data.vertices, function (val, i) {
				nodeIDS.push(val.id)
				val.weight = 10;
				nodes.push({
					data: val
				});
			});
			angular.forEach(data.edges, function (val, i) {
				if ($.inArray(val.from, nodeIDS) != -1 && $.inArray(val.to, nodeIDS) != -1)
					edges.push({
						data: {
							source: val.from,
							target: val.to,
							labelE: val.labelE,
							id: val.id
						}
					});
			});
			finalData.nodes = nodes;
			finalData.edges = edges;
			loadEntityNetworkChart(finalData, options, ($scope.entitySearchResult.name ? $scope.entitySearchResult.name : searchText), layout);
		}
	}

	function PlotriskAlertCharts() {
		// time line starts here
		var alertTimelineData = [];
		if ($scope.entitySearchResult.list.sanctionList.length > 0) {
			angular.forEach($scope.entitySearchResult.list.sanctionList, function (d, i) {
				if (d.reported)
					alertTimelineData.push({
						"time": d.reported,
						"type": "sanction list",
						"amount": Math.floor(Math.random() * 20)
					})
			});
		}
		if ($scope.entitySearchResult.list.pep.length > 0) {
			angular.forEach($scope.entitySearchResult.list.pep, function (d, i) {
				if (d.reported)
					alertTimelineData.push({
						"time": d.reported,
						"type": "Pep Alert",
						"amount": Math.floor(Math.random() * 20)
					})
			});
		}
		if ($scope.entitySearchResult.list.xforceibmcloud.length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.xforceibmcloud, function (d, i) {
				if (d.reported)
					alertTimelineData.push({
						"time": d.reported,
						"amount": Math.floor(Math.random() * 20),
						"type": "Cyber Alerts",
						"txt": '<table class="table data-custom-table"><tr><th>Title</td><td>' + d.title + '</td></tr><tr><th>Tag</td><td>' + d.tag + '</td></tr><tr><th>Reported</td><td>' + d.reported + '</td></tr><tr><th>Risk level</td><td>' + d.risk_level + '</td></tr></tr><tr><th>Details</td><td>' + d.details + '</td></tr></table>'
					})
			});
		}
		if ($scope.entitySearchResult.list.scam.length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.scam, function (d, i) {
				if (d.reported)
					alertTimelineData.push({
						"time": d.reported,
						"amount": Math.floor(Math.random() * 20),
						"type": "Fraud Alerts"
					})
			});
		}
		if ($scope.entitySearchResult.list['adverse_news'].length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.adverse_news, function (d, i) {
				alertTimelineData.push({
					"time": d.published,
					"amount": Math.floor(Math.random() * 20),
					"type": "Adverse News",
					"txt": '<table class="table data-custom-table"><tr><th>Title</td><td>' + d.title + '</td></tr><tr><th>Published</td><td>' + d.published + '</td></tr><tr><th>Source</td><td>' + d.source + '</td></tr><tr><th>Url</td><td>' + d.url + '</td></tr></table>'
				})
			});
		}
		if ($scope.entitySearchResult.list['denied_personlist'].length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.denied_personlist, function (d, i) {
				alertTimelineData.push({
					"time": d.effective_date,
					"amount": Math.floor(Math.random() * 20),
					"type": "Fraud Alerts",
					"txt": '<table class="table data-custom-table"><tr><th>Name</td><td>' + d.name + '</td></tr><tr><th>Action</td><td>' + d.action + '</td></tr><tr><th>Effective date</td><td>' + d.effective_date + '</td></tr><tr><th>Expiration date</td><td>' + d.expiration_date + '</td></tr><tr><th>Fr citation</td><td>' + d.fr_citation + '</td></tr><tr><th>Risk score</td><td>' + d.risk_score + '</td></tr><tr><th>Type</td><td>Fraud</td></tr><tr><th>Address</td><td>' + d.address + '</td></tr><tr><th>Source</td><td>' + d.source + '</td></tr></table>'
				})
			});
		}
		if ($scope.entitySearchResult.list['news_deep_web'].length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.news_deep_web, function (d, i) {
				alertTimelineData.push({
					"time": d.postedOn,
					"amount": Math.floor(Math.random() * 20),
					"type": "Dark Web",
					"txt": '<table class="table data-custom-table"><tr><th>Title</td><td>' + d.title + '</td></tr><tr><th>Posted On</td><td>' + d.postedOn + '</td></tr><tr><th>Details</td><td>' + d.details + '</td></tr><tr><th>Source</td><td>' + d.source + '</td></tr></table>'
				})
			});
		}
		if ($scope.entitySearchResult.list['cypherpunk'].length > 0) {
			// no count
			angular.forEach($scope.entitySearchResult.list.cypherpunk, function (d, i) {
				alertTimelineData.push({
					"time": d.dated,
					"amount": Math.floor(Math.random() * 20),
					"type": "Dark Web",
					"txt": '<table class="table data-custom-table"><tr><th>Title</td><td>' + d.title + '</td></tr><tr><th>Date</td><td>' + d.dated + '</td></tr><tr><th>Details</td><td>' + d.details + '</td></tr><tr><th>Source</td><td>' + d.source + '</td></tr></table>'
				})
			});
		}
		var colorsObj = {
			"Pep Alert": "#a75dc2",
			"sanction list": "#3790ce",
			"Fraud Alerts": "#399034",
			"Cyber Alerts": "#3778be",
			"Adverse News": "#69ca6b",
			"Dark Web": "#dd9d22",
			"Crime list": "#e81f0f"
		}
		var timelineOptions = {
			container: "#riskyTimelineChart",
			height: 50,
			colors: ["#46a2de", "#a75dc2", "#399034", "#3778be"],
			data: alertTimelineData,
			colorsObj: colorsObj,
		};
		loadtimeLineColumnChart(timelineOptions);
		var riskPieData = [{
			"key": "Adverse News",
			"doc_count": $scope.entitySearchResult.list.adverse_news.length
		},
		{
			"key": "Cyber Alerts",
			"doc_count": $scope.entitySearchResult.list.xforceibmcloud.length
		},
		{
			"key": "Fraud Alerts",
			"doc_count": $scope.entitySearchResult.list['sex-offenders'].length + $scope.entitySearchResult.list.scam.length + $scope.entitySearchResult.list['denied_personlist'].length + $scope.entitySearchResult.list['imsasllc'].length + $scope.entitySearchResult.list['fbi_data'].length
		},
		{
			"key": "Pep Alert",
			"doc_count": $scope.entitySearchResult.list.pep.length
		},
		{
			"key": "sanction list",
			"doc_count": $scope.entitySearchResult.list.sanctionList.length
		}, {
			"key": "Crime list",
			"doc_count": $scope.entitySearchResult.list['crimeList']['interpol'].length + $scope.entitySearchResult.list['crimestoppers_uk'].length
		}, {
			"key": "Dark Web",
			"doc_count": $scope.entitySearchResult.list['news_deep_web'].length + $scope.entitySearchResult.list['cypherpunk'].length
		}]
		if ($("#pieChartRiskRatio").find("svg").length === 0) {
			$("#pieChartRiskRatio").empty();
			RiskRatioflag = _.every(riskPieData, { 'doc_count': 0 });
			if (!RiskRatioflag) {
				$scope.entitySearchResult.is_data_not_found.is_risk_ratio = false;
				EntityCommonTabService.InitializeandPlotPie(riskPieData, 'pieChartRiskRatio', colorsObj);
			}
			else {
				$scope.entitySearchResult.is_data_not_found.is_risk_ratio = false;
				$("#pieChartRiskRatio").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>")
			}
		}
		$timeout(function () {
			$('#politicalOrgsWrapper').mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 10)
		// createBrush(timelinebrushOptions);
	}
	function PlotPoliticalChart() {
		if ($("#politicaldonations").find("svg").length == 0) {
			$("#politicaldonations").empty();
			var data = [];
			var all_dts = [], politicians_dt = [], orgs_dt = [], finan_dt = [];
			var nested_data_politicians = d3.nest()
				.key(function (d) { return new Date(d.date).getFullYear(); })
				.rollup(function (d) { return d3.sum(d, function (d) { return parseFloat(d.amount); }) })
				.entries($scope.entitySearchResult['list']['associated_companies']['politicians'])
			var nested_data_orgs = d3.nest()
				.key(function (d) { return new Date(d.date).getFullYear(); })
				.rollup(function (d) { return d3.sum(d, function (d) { return parseFloat(d.amount); }) })
				.entries($scope.entitySearchResult['list']['associated_companies']['political_organizations']);
			var politicians = []; var politiciansOrgs = [];
			angular.forEach(nested_data_politicians, function (d, i) {
				all_dts.push(d.key);
				politicians_dt.push(d.key)
				politicians.push({
					"time": d.key,
					"y": d.value,
					type: "Politician"
				})
			});
			angular.forEach(nested_data_orgs, function (d, i) {
				all_dts.push(d.key);
				orgs_dt.push(d.key)
				politiciansOrgs.push({
					"time": d.key,
					"y": d.value,
					type: "Political organization"
				})
			});
			// should have equal keys for stack chart
			angular.forEach(all_dts, function (dt, k) {
				if ($.inArray(dt, politicians_dt) == -1) {
					politicians.push({
						"time": dt,
						"y": 0,
						type: "Politician"
					})
				}
				if ($.inArray(dt, orgs_dt) == -1) {
					politiciansOrgs.push({
						"time": dt,
						"y": 0,
						type: "Political organization"
					})
				}
			});
			politicians.sort(function (a, b) {
				return parseInt(a.time) - parseInt(b.time);
			})
			politiciansOrgs.sort(function (a, b) {
				return parseInt(a.time) - parseInt(b.time);
			})
			if ((politicians && politicians.length > 0) || (politiciansOrgs && politiciansOrgs.length > 0)) {
				data[0] = politicians;
				data[1] = politiciansOrgs;
				var options = {
					container: "#politicaldonations",
					width: $("#politicaldonations").width(),
					height: "200",
					data: data,
					marginTop: 1,
					marginBottom: 1,
					marginRight: 5,
					marginLeft: 5,
					tickColor: "rgb(51, 69, 81)"
				}
				stackedbarTimelinechart(options);
			} else {
				if ($scope.entitySearchResult['list']['associated_companies']['politicians'].length === 0 && $scope.entitySearchResult['list']['associated_companies']['political_organizations'].length === 0) {
					$("#politicaldonations").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>")
				}
			}
		}
		// plot pie chart
		if ($("#politicalOrgs").find("svg").length == 0) {
			$("#politicalOrgs").empty();
			var nested_data = d3.nest().key(function (d) { return d.recip_name }).rollup(function (d) { return d3.sum(d, function (d) { return parseFloat(d.amount); }) }).entries($scope.entitySearchResult['list']['associated_companies']['political_organizations'])
			var nested_data1 = d3.nest().key(function (d) { return d.recip_name }).rollup(function (d) { return d3.sum(d, function (d) { return parseFloat(d.amount); }) })
				.entries($scope.entitySearchResult['list']['associated_companies']['politicians'])
			Array.prototype.push.apply(nested_data, nested_data1)
			var finalPieData = nested_data;
			if (finalPieData.length > 0) {
				nested_data.map(function (d) {
					return d.doc_count = d.value
				})
				nested_data.sort(function (a, b) {
					return b.value - a.value;
				})
				var colors = ["#E81F0F", "#3132FE", "#B2CCE7", "#5d96c8", "#b753cd", "#69ca6b", "#69ca6b", "#c1bd4f", "#db3f8d", "#669900", "#334433"]
				EntityCommonTabService.InitializeandPlotPie(nested_data.slice(0, 10), "politicalOrgs", "", colors)
				$scope.entitySearchResult.is_data_not_found.is_politcal_donation = false;
			} else {
				$scope.entitySearchResult.is_data_not_found.is_politcal_donation = false;
				if ($scope.entitySearchResult['list']['associated_companies']['politicians'].length === 0 && $scope.entitySearchResult['list']['associated_companies']['political_organizations'].length === 0) {
					$("#politicalOrgs").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>")
				}
			};
		}
	};
	$scope.onClickFraudTypeTabs = function (type) {
		$scope.fraudTabType = type;
	};
}