'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyOverviewNewController', entityCompanyOverviewNewController);
entityCompanyOverviewNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'FetcherImageList',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'EntityCompanyConst',
	'$timeout',
	'$interval',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'Flash',
	'UploadFileService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$http',
	'$sce',
	'EHUB_API',
	'isicCodeConstants',
	'TopPanelApiService',
	'CountryCodesConst',
	'technologyConstant',
	'handlePepofficers',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant',
	'EntityCommonTabService'
];
function entityCompanyOverviewNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	FetcherImageList,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	EntityCompanyConst,
	$timeout,
	$interval,
	Company_Unwanted_End_Points,
	HostPathService,
	Flash,
	UploadFileService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$http,
	$sce,
	EHUB_API,
	isicCodeConstants,
	TopPanelApiService,
	CountryCodesConst,
	technologyConstant,
	handlePepofficers,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant,
	EntityCommonTabService) {
	/*
* @purpose: Load Stock History Widget
* @created: 12th Oct 2018
* @params: Finacial data
* @returns: none 
* @author:varsha
*/
	var presentDate;
	var dateDivisionObj;
	var maxTwitterTagDomainRangeValue = 5;
	var dataWithDate = {
		'lastOneYearDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastFiveYearDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastSixMonthDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastOneMonthDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastDay': {
			'close_price': [],
			'open_price': [],
		},
		'lastFiveDay': {
			'close_price': [],
			'open_price': [],
		},
	};
	var shodanData = {};
	$scope.$on('overviewtabOnload', function (event) {
		console.log('event: ', event);
		if (EntityCommonTabService.lazyLoadingentity.financeTabFirsttime) {
			var url = {
				identifier: encodeURIComponent($stateParams.query),
				type: 'finance_info',
				jurisdiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : '',
			};
			linkcall(url, 'finance_info', false);
			EntityCommonTabService.lazyLoadingentity.financeTabFirsttime = false;
		}
		if (EntityCommonTabService.lazyLoadingentity.overviewTabFirsttime) {
			EntityCommonTabService.lazyLoadingentity.overviewTabFirsttime = false;
			getEntityCompanyDataByName(EntityCommonTabService.basicsharedObject.companyName, EntityCommonTabService.lazyLoadingentity.overviewFetchers).then(function (responsemessage) {
				console.log('responsemessage: ', responsemessage);
				var Website = $scope.entitySearchResult.list.topHeaderObject.hasURL ? $scope.entitySearchResult.list.topHeaderObject.hasURL : ($scope.entitySearchResult.website !== '' ? $scope.entitySearchResult.website : ($scope.entitySearchResult.company_url !== '' ? $scope.entitySearchResult.company_url : ($scope.entitySearchResult.company_website !== "" ? $scope.entitySearchResult.company_website : "")));
				if (Website) {
					getUrlAnalysisData(Website);
					Website = Website ? ('http://' + Website.replace(/(^\w+:|^)\/\//, '')) : "";
					$scope.entitySearchResult.website = Website;
				} else {
					$scope.entitySearchResult.is_data_not_found.is_traffic_by_country = false;
					$scope.entitySearchResult.is_data_not_found.is_traffic_key_word = false;
					$scope.entitySearchResult.is_data_not_found.is_traffic_bydomain = false;
				}
				if (($stateParams['phone'] && $stateParams['phone'] !== '') || $scope.entitySearchResult['telephone'] || $scope.entitySearchResult['phone'] || $scope.entitySearchResult.list['domain_servers'].length > 0) {
					var number = ($stateParams['phone'] && $stateParams['phone'] !== '') ? $stateParams['phone'] :
						$scope.entitySearchResult['telephone'] ? $scope.entitySearchResult['telephone'] : $scope.entitySearchResult['phone'] ? $scope.entitySearchResult['phone'] :
							$scope.entitySearchResult.list['domain_servers'][0]['admin_phone'] !== '' ? $scope.entitySearchResult.list['domain_servers'][0]['admin_phone'] :
								$scope.entitySearchResult.list['domain_servers'][0]['registrant_phone'] !== '' ? $scope.entitySearchResult.list['domain_servers'][0]['registrant_phone'] :
									$scope.entitySearchResult.list['domain_servers'][0]['technical_phone'] !== '' ? $scope.entitySearchResult.list['domain_servers'][0]['technical_phone'] : '';
				}
				var finalLocationList = [], locationNames = [];
				angular.forEach(EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.locationList, function (locationValue, locationKey) {
					if (locationNames.indexOf(locationValue.name) === -1) {
						locationNames.push(locationValue.name);
						finalLocationList.push(locationValue);
					}
				});
				$timeout(function(){
					EntityCommonTabService.tabsWorldMap.worldChartOptions1.markers = finalLocationList;
					EntityCommonTabService.tabsWorldMap.worldChartOptions2.markers = finalLocationList;
				},10)
				$scope.entitySearchResult.list['company_locations'] = finalLocationList;
				// calling world map chart function
				EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartOptions1);
				EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartOptions2);
				$scope.entitySearchResult.list['company_locations'] = finalLocationList;
				
			});
			onpageLoadCharts();
		} else {
			if (EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphOverview"] && EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphOverview"].vertices.length > 0) {
				EntityCommonTabService.loadDataAndPlotGraphForEntity({
					"data": EntityCommonTabService.entityChartSharedObject.vlaDataArr["networkGraphOverview"],
					"id": "networkGraphOverview"
				});
			}
			loadFinacialWidget(EntityCommonTabService.entityChartSharedObject.financeData);
			loadOverviewurlcharts();
		}
		$scope.entitySearchResult.is_data_not_found.is_data_industry  = $scope.entitySearchResult.list['industry'].length > 0 ? true : false; 
	});
	$scope.$on('overviewFinanceData', function (event) {
		loadFinacialWidget(EntityCommonTabService.entityChartSharedObject.financeData);
	});
	function linkcall(url, type, json) {
		EntityApiService.complianceFieldsUrl(url, json).then(function (response) {
			if (response) {
				if (type == "finance_info") { //this section is for financial Information  section 
					if (response && response.data && response.data.results && response.data.results.length > 0 && response.data.results[0].finance_info) {
						var value = (response.data.results[0].finance_info.Finanace_Information) ? response.data.results[0].finance_info.Finanace_Information : {};
						value.shareholders = response.data.results[0].finance_info.shareholders ? response.data.results[0].finance_info.shareholders : [];
						var data = [{
							value: [value]
						}];
						EntityCommonTabService.entityChartSharedObject.financeData = data;
						loadFinacialWidget(data);
						EntityCommonTabService.lazyLoadingentity.financeTabFirsttime = false;
					}
				} //fainance  ends
			}
		}, function (response) {//if API fails
		});
	}
	function onpageLoadCharts() {
		// load all vla data on page load
		$timeout(function () {
			var data = {
				"fetchers": ["1008", "1006", "1013", "1021"],
				"keyword": $scope.entitySearchResult.name ? $scope.entitySearchResult.name : EntityCommonTabService.basicsharedObject.companyName,
				"searchType": "Company",
				"lightWeight": true,
				"limit": 2,
				"iterations": 1,
				"alias": "companyOverview",
				"create_new_graph": false,
				"requires_expansion": true,
				"entity_resolution": true,
				"expansion_fetchers_lightweight": true,
				"expansion_fetchers": ["2", "2001", "1005", "1002", "1003", "3001", "1007", "29", "2003", "1009", "1010", "1011", "1012", "3002", "3003", '10', '11', '12', '13', '46', '16', '17', '1021']
			};
			EntityCommonTabService.loadoverviewVLA("networkGraphOverview", data, EntityCommonTabService.entityChartSharedObject.vlaDataArr);
			/* calling network chart */
			EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartOptions1);
			EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.worldChartOptions2);
			EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.digitalFootPrintChart);

			// World(EntityCommonTabService.tabsWorldMap.worldChartNewsLocationsOptions);
		}, 3000);
	};
	var stockCurrency;
	function loadFinacialWidget(data) {
		EntityCommonTabService.lazyLoadingentity.financeTabFirsttime = false;
		if (data && data[0] && data[0]['value'] && data[0]['value'][0]) {
			stockCurrency = data[0]['value'][0]['stock_currency']
			if (data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information && data[0]['value'][0].stocks_information.historical_prices && data[0]['value'][0].stocks_information.historical_prices.length > 0) {
				loadStockHistoryWidget(data);
			} else {
				$scope.dataWithDate = {
					'lastOneYearDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastFiveYearDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastSixMonthDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastOneMonthDate': {
						'close_price': [],
						'open_price': [],
					},
					'lastDay': {
						'close_price': [],
						'open_price': [],
					},
					'lastFiveDay': {
						'close_price': [],
						'open_price': [],
					},
				}
				$scope.entitySearchResult.is_data_not_found.is_stockHistory = false;
			}
		}
	}
	function loadStockHistoryWidget(data) {
		presentDate = moment()._d;
		dateDivisionObj = {
			'lastOneYearDate': moment(presentDate).subtract(1, 'year')._d,
			'lastFiveYearDate': moment(presentDate).subtract(5, 'year')._d,
			'lastSixMonthDate': moment(presentDate).subtract(6, 'month')._d,
			'lastOneMonthDate': moment(presentDate).subtract(1, 'month')._d,
			'lastDay': moment().add(-1, 'days')._d,
			'lastFiveDay': moment().add(-5, 'days')._d
		}
		arrangeDataByMeasure(data, 'close_price');
		arrangeDataByMeasure(data, 'open_price');
		$scope.dataWithDate = dataWithDate;
		EntityCommonTabService.entityChartSharedObject.dataWithDate = dataWithDate;
	}
	/*
		* @purpose: Arrange data array by different measure
		* @created: 15th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
	function arrangeDataByMeasure(data, measure) {
		data[0]['value'][0].stocks_information.historical_prices.map(function (d) {
			var dataDate = new Date(d.data_date);
			var eachData = jQuery.extend(true, {}, d);
			eachData.value = d[measure];
			eachData.year = dataDate;
			eachData.currency = stockCurrency;
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastFiveYearDate']) {
				dataWithDate['lastFiveYearDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastOneYearDate']) {
				dataWithDate['lastOneYearDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastSixMonthDate']) {
				dataWithDate['lastSixMonthDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastOneMonthDate']) {
				dataWithDate['lastOneMonthDate'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastDay']) {
				dataWithDate['lastDay'][measure].push(eachData);
			}
			if (dataDate < presentDate && dataDate >= dateDivisionObj['lastFiveDay']) {
				dataWithDate['lastFiveDay'][measure].push(eachData);
			}
		})
		if (measure == 'open_price') {
			setTimeout(function () {
				$scope.onChangestockhistoryKey_Enhance('lastFiveYearDate', 'close_price')
			}, 3000)
		}
	}
	/*
	* @purpose: Load Stock History Widget On Change of selected option
	* @created: 12th Oct 2018
	* @params: Finacial data
	* @returns: none 
	* @author:varsha
	*/
	$scope.stockHistorySelectedTab_Enhance = 'lastFiveYearDate';
	$scope.stock_measures_Enhance = ['open_price', 'close_price']
	$scope.selectedstockMeasure = 'close_price'
	$scope.onChangestockhistoryKey_Enhance = function (selectedType, selectedstockMeasure) {
		$scope.stockHistorySelectedTab_Enhance = selectedType;
		dataWithDate = EntityCommonTabService.entityChartSharedObject.dataWithDate;
		if (dataWithDate && dataWithDate[selectedType] && dataWithDate[selectedType][selectedstockMeasure] && dataWithDate[selectedType][selectedstockMeasure].length >= 1) {
				setTimeout(function () {
						if ($("#stockhistory-line-chart_Enhance_overview").width() > 0) {
							$('#stockhistory-line-chart_Enhance_overview').empty();
							var stockhistoryOptions = {
								container: "#stockhistory-line-chart_Enhance_overview",
								height: '240',
								width: $("#stockhistory-line-chart_Enhance_overview").width(),
								uri: "../vendor/data/bloombergdata.json",
								data: dataWithDate[selectedType][selectedstockMeasure]
							};
							EntityCommonTabService.loadlineData(stockhistoryOptions);
						} else if ($("#stockhistory-line-chart_Enhance_overview").width() === 0 && dataWithDate[selectedType][selectedstockMeasure].length > 0 && $scope.mainInfoTabType === 'Overview'){
							setTimeout(function () {
						$scope.onChangestockhistoryKey_Enhance(selectedType, selectedstockMeasure);
							},2000);
					}
				}, 500)
		}
		else {
			return false;
		}
	}
	function getEntityCompanyDataByName(name, fetchers) {
		var deffered = $q.defer();
		var fetcherIds = EntityCommonTabService.checkFetcherStatus(fetchers);
		var companyNameSplitted = name.split(' ');
		var copyOfCompanyName = '';
		angular.forEach(companyNameSplitted, function (word, key) {
			var index = Company_Unwanted_End_Points.indexOf(word.toLowerCase());
			if (index === -1) {
				copyOfCompanyName += word + ' ';
			}
		});
		copyOfCompanyName = copyOfCompanyName.trim();
		angular.forEach(fetcherIds, function (id, key) {
			var ids = [];
			ids.push(id);
			var data = {
				fetchers: ids,
				keyword: name,
				searchType: 'Company',
				limit: '1'
			};
			/* 
			*/
			var temp = _.find(handlingCompanyConstantName, { 'name': data.keyword, 'fetcher': id });
			data.keyword = temp === undefined ? data.keyword : temp.alt_name;
			data.limit = temp === undefined ? data.limit : temp.limit;
			EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
				var currentSubFetcher = id;
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
					if (subfetcherData.status) {
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							if (subentityKey === 0) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
										angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
											if (d3.keys($scope.entitySearchResult.list["resolve_related_similar_companies"]).length > 0) {
												if (subsubEnK == 'name' && subsubEnV !== '' && $scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											} else {
												if (subsubEnK == 'name' && subsubEnV !== '' && !subenvFlag) {
													if (!$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher]) {
														$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher] = [];
													}
													$scope.entitySearchResult.list["resolve_related_similar_companies"][currentSubFetcher].push({
														name: subsubEnV,
														source: subentity.source
													});
													subenvFlag = true;
													var index = -1;
													angular.forEach(tagCloudOrganizationNameList, function (object, key) {
														if (object.text === subsubEnV)
															index = key
													});
													if (index === -1) {
														tagCloudOrganizationNameList.push({
															text: subsubEnV,
															size: 5,
															type: 'organization'
														});
													} else {
														tagCloudOrganizationNameList[index].size += 5;
														tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
													}
												}
											}
										});
									}
									if ((subedv.relationshipName) == 'specialities') {
										subedv.entity.name !== '' && subedv.entity.name ? specialities.push(subedv.entity.name) : '';
									}
									if ((subedv.relationshipName) == 'people_also_viewd') {
										if (subedv.entity.name !== '' && subedv.entity.name) {
											$scope.entitySearchResult.list['people_also_viewd'].push({
												name: subedv.entity.properties.company_name,
												company_size: subedv.entity.properties.company_size,
												industry: subedv.entity.properties.industry,
												link: subedv.entity.properties.link,
												logo: subedv.entity.properties.logo,
												risk_score: subedv.entity.properties.risk_score,
												source: subedv.entity.source
											})
										}
										if ($scope.entitySearchResult.list['people_also_viewd'].length === 0) {
											$scope.entitySearchResult.is_data_not_found.is_people_visited = false;
										}
									}
								});
							}
						});
						angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
							angular.forEach(subentity.edges, function (subedv, subedk) {
								if ($scope.entitySearchResult.list.hasOwnProperty(subedv.relationshipName) && (subedv.relationshipName) == 'similar_companies') {
									angular.forEach(subedv.entity, function (subsubEnV, subsubEnK) {
										if (subsubEnK == 'name' && subsubEnV !== '') {
											$scope.entitySearchResult.list[subedv.relationshipName].push({
												name: subsubEnV,
												source: subentity.source
											});
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subsubEnV)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subsubEnV,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
									});
								}
							});
						});
						if (subfetcherData.fetcher === 'wikipedia.com') {
							console.log(subfetcherData.entities);
							if(subfetcherData.entities && subfetcherData.entities.length > 0 && subfetcherData.entities[0].properties){
								for (const key in subfetcherData.entities[0].properties) {
									if (subfetcherData.entities[0].properties.hasOwnProperty(key) && !$scope.$parent.entitySearchResult[key] ) {
										$scope.$parent.entitySearchResult[key] = subfetcherData.entities[0].properties[key];
									}
								}									
							}
							$scope.$parent.entitySearchResult.industry =  typeof $scope.entitySearchResult.list.industry === 'string' ?  [$scope.entitySearchResult.list.industry] : $scope.entitySearchResult.list.industry;
						}
						if (subfetcherData.fetcher === 'instagram.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								//	if(subentityKey === 0) {
								angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
									if (edgeValue.relationshipName === 'media') {
										$scope.entityInstagramList.push({
											caption: edgeValue.entity.properties.caption,
											thumbnail: edgeValue.entity.properties.thumbnail,
											image: subentity.properties.image,
											name: subentity.properties.name,
											followingCount: subentity.properties.following_count,
											followersCount: subentity.properties.followers_count,
											followersCountNumberic: 0,
											connectionNumberic: 0,
											type: 'instagram'
										});
									}
								});
								//}
							});
							if ($scope.entityInstagramList.length !== 0) {
								$scope.entityInstagramList[0].followersCountNumberic = nFormatter(parseInt($scope.entityInstagramList[0]['followersCount']), 1);
								$scope.entityInstagramList[0]['connectionNumberic'] = nFormatter(parseInt($scope.entityInstagramList.length), 1);
							}
							if ($scope.entityInstagramList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_instagramlist = false;
							}
							if ($scope.entityInstagramList.length > 0) {
								$scope.socialMediaActiveCount = 1;
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'instagram'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "instagram",
										"size": $scope.entityInstagramList[0]['followersCount']
									});
								}
								$scope.entitySearchResult.list.social_media_count =parseInt($scope.entityInstagramList[0]['followersCount']) + $scope.entitySearchResult.list.social_media_count; 
								$scope.entitySearchResult.list.social_followers_bubble_chart = false;
								socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
						}
						if (subfetcherData.fetcher === 'logo.google.com') {
							$scope.entitySearchResult.comapnyLogo = subfetcherData.entities[0].properties.url;
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									if (!$scope.entitySearchResult['description'] || $scope.entitySearchResult['description'] === '') {
										$scope.entitySearchResult['description'] = subentity.properties['details'];
									}
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'social_feed') {
											$scope.entityLinkedinList.push({
												commentsCount: edgeValue.entity.properties.comments_count !== '' && edgeValue.entity.properties.comments_count ? edgeValue.entity.properties.comments_count : 0,
												likesCount: edgeValue.entity.properties.likes_count !== '' && edgeValue.entity.properties.likes_count ? edgeValue.entity.properties.likes_count : 0,
												postedOn: edgeValue.entity.properties.posted_at,
												text: edgeValue.entity.properties.text,
												image: subentity.properties.image,
												source: subentity.source
											});
										}
									});
								}
							});
							if ($scope.entityLinkedinList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
							}
							if ($scope.entityLinkedinList.length > 0)
								$scope.socialMediaActiveCount = 3;
							if ($scope.mainInfoTabType == 'Social Media') {
								InteractionPieChart();
							}
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.properties, function (subev, subek) {
									if (subek === 'location' && subev !== '' && subev) {
										EntityCommonTabService.entityChartSharedObject.social_activity_locations.push({
											name: subev,
											long: '',
											lat: '',
											mark: 'assets/images/redpin.png',
											source: subentity.source,
											title: subentity.properties.title || subentity.source,
											'txt': '<span>location: ' + subentity.properties.location + '</span></br><span>Title: ' + subentity.properties.title + '</span></br><span>screen name: ' + subentity.properties.screen_name + '</span></br><span>details: ' + subentity.properties.details + '</span></br><span>profile created at: ' + subentity.properties.profile_created_at + '</span></br><span>source: ' + subentity.properties.source + '</span>'
										});
									}
									!$scope.entitySearchResult.hasOwnProperty(subek) && UnWantedKeyList.indexOf(subek) === -1 && subev !== '' && subev ? $scope.entitySearchResult[subek] = subev : '';
								});
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'tweets') {
											var objectData = {
												title: subentity.properties.title,
												createdOn: new Date(edgeValue.entity.properties.created_at),
												image: subentity.properties.image,
												text: edgeValue.entity.properties.text,
												retweetCount: edgeValue.entity.properties.retweet_count,
												tags: [],
												type: 'twitter'
											};
											angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
												if (subedv.relationshipName === 'user_mentions') {
													objectData.tags.push({
														name: subedv.entity.properties.name,
														screen_name: subedv.entity.properties.screen_name,
														risk_score: subedv.entity.properties.risk_score
													});
													var index = -1;
													angular.forEach(EntityCommonTabService.entityChartSharedObject.twitter_tag_words, function (object, key) {
														if (object.text === subedv.entity.properties.name)
															index = key
													});
													if (index === -1) {
														EntityCommonTabService.entityChartSharedObject.twitter_tag_words.push({
															text: subedv.entity.properties.name,
															size: 5,
															type: 'person'
														});
													} else {
														EntityCommonTabService.entityChartSharedObject.twitter_tag_words[index].size += 5;
														EntityCommonTabService.entityChartSharedObject.twitter_tag_words[index].size > maxTwitterTagDomainRangeValue ? maxTwitterTagDomainRangeValue = EntityCommonTabService.entityChartSharedObject.twitter_tag_words[index].size : '';
													}
												}
											});
											$scope.entityTwitterList.push(objectData);
											$scope.entitySearchResult['twitter_retweets_count'] += parseInt(edgeValue.entity.properties.retweet_count);
											$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1)
										}
										if (edgeValue.relationshipName === 'followers') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													name: edgeValue.entity.name,
													createdOn: new Date(edgeValue.entity.properties.profile_created_at),
													image: edgeValue.entity.properties.profile_image_url,
													screen_name: edgeValue.entity.properties.screen_name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'following') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													name: edgeValue.entity.name,
													createdOn: new Date(edgeValue.entity.properties.profile_created_at),
													image: edgeValue.entity.properties.profile_image_url,
													screen_name: edgeValue.entity.properties.screen_name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'follower_following_count') {
											$scope.entitySearchResult.list['follower_following_count'].push({
												twitter_followers_number: edgeValue.entity.properties.followers,
												twitter_following_number: edgeValue.entity.properties.following
											});
										}
									});
									if ($scope.entityTwitterList.length > 0)
										$scope.socialMediaActiveCount = 0;
								}
							});
							$scope.entitySearchResult['twitter_retweets_count'] = nFormatter(parseInt($scope.entitySearchResult['twitter_retweets_count']), 1);
							if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
								$scope.entitySearchResult.list['followers_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number), 1);
								$scope.entitySearchResult.list['twitter_following_numberic'] = nFormatter(parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_following_number), 1);
							}
							if ($scope.entitySearchResult.list['follower_following_count'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_twitter_list = false;
							}
							if ($scope.entitySearchResult.list['follower_following_count'].length > 0) {
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'twitter'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "twitter",
										"size": $scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number
									});
								}
								$scope.entitySearchResult.list.social_media_count = parseInt($scope.entitySearchResult.list['follower_following_count'][0].twitter_followers_number )+ $scope.entitySearchResult.list.social_media_count; 
								$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
								socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
							EntityCommonTabService.tabsWorldMap.worldChartTwitterLocationsOptions.markers = EntityCommonTabService.entityChartSharedObject.social_activity_locations;
							// calling world map chart function
							if ($scope.entityTwitterList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							}
							if ($scope.entitySearchResult.list['follower_following_count'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							}
						}
						if (subfetcherData.fetcher === 'pages.facebook.com') {
							var matchedName = '';
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if ((matchedName === '' || (matchedName !== '' && matchedName === subentity.name))
									&& subentity.name !== '' && subentity.name) {
									$scope.entitySearchResult.list['fan_counts'] = parseInt(subentity.properties.fan_count);
									$scope.entitySearchResult.list['fan_counts_numbric'] = nFormatter(parseInt(subentity.properties.fan_count), 1);
									matchedName === '' ? matchedName = subentity.name : '';
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if (subedv.relationshipName === 'recent_posts') {
											angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
												if (subsubedv.relationshipName === 'data') {
													$scope.entityFacebookList.push({
														text: subsubedv.entity.properties.message || subsubedv.entity.properties.story,
														created_time: new Date(subsubedv.entity.properties.created_time),
														website: subsubedv.entity.properties.website,
														risk_score: subsubedv.entity.properties.risk_score
													});
												}
											});
										}
										if (subedv.relationshipName === 'milstones') {
											angular.forEach(subedv.entity.edges, function (subsubedv, subsubedk) {
												if (subsubedv.relationshipName === 'data' && subsubedv.entity.properties.is_hidden === 'false') {
													$scope.entitySearchResult.list.latestReleases.push({
														title: subsubedv.entity.properties.title,
														start_time: new Date(subsubedv.entity.properties.start_time),
														created_time: new Date(subsubedv.entity.properties.created_time),
														risk_score: subsubedv.entity.properties.risk_score,
														source: subentity.source
													});
												}
											});
										}
									});
								}
							});
							if ($scope.entityFacebookList.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_facebook_list = false;
							}
							if ($scope.entityFacebookList.length > 0) {
								$scope.socialMediaActiveCount = 4;
								var index = EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.findIndex(function (d) {
									return d.name == 'facebook'
								})
								if (index < 0) {
									EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions.data.children.push({
										"name": "facebook",
										"size": $scope.entitySearchResult.list['fan_counts']
									});
								}
								$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
								$scope.entitySearchResult.list.social_media_count = parseInt($scope.entitySearchResult.list['fan_counts']) + $scope.entitySearchResult.list.social_media_count; 
								socialBubbleChart(EntityCommonTabService.entityChartSharedObject.bubbleSocialOptions);
							}
						}
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							var interlock = {};
							var subentity = subfetcherData.entities[0];
							var nested_data = d3.nest().key(function (d) { return d.relationshipName }).entries(subfetcherData.entities[0].edges);
							for (var i = 0; i < nested_data.length; i++) {
								if (nested_data[i].key === "org_interlocks") {
									for (var vl = 0; vl < nested_data[i].values.length; vl++) {
										var edgevalue = nested_data[i].values[vl];
										if (edgevalue.relationshipName === "org_interlocks") {
											if (edgevalue.entity.properties.connected_entity_name !== '') {
												var mainentity = edgevalue.entity.properties.connected_entity_name;
												interlock[mainentity] = [];
												for (var j = 0; j < edgevalue.entity.edges.length; j++) {
													var subedgevalue = edgevalue.entity.edges[j];
													if (subedgevalue.relationshipName === 'connecting_entities') {
														var connected_entity = subedgevalue.entity.name;
														interlock[mainentity].push(connected_entity);
													} // connecting
													// entities
													// ends if
												} // j loops ends
											}// checking for emty
											// name if ends
										}// org interlocks ends
									}// vl for loop ends
								}// interocks if loop ends
							}// nested for loop loop ends
							var result = _.map(interlock, function (value, prop) {
								return { prop: prop, value: value };
							});
							if (result.length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							} else {
								$scope.interlock = result;
							}
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									// $scope.entitySearchResult['revenue'] = subentity.properties.revenue;
									angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
										if (edgeValue.relationshipName === 'key_staff_attended_schools') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list[edgeValue.relationshipName].push({
													person: edgeValue.entity.properties.person,
													person_url: edgeValue.entity.properties.person_url,
													school_attended: edgeValue.entity.properties.school_attended,
													school_url: edgeValue.entity.properties.school_url,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
										}
										if (edgeValue.relationshipName === 'connected_industries') {
											$scope.entitySearchResult.list['connected_industries'].push(edgeValue);
										}
										if (edgeValue.relationshipName === 'key_relationships' || edgeValue.relationshipName === 'political_financial_supports'
											|| edgeValue.relationshipName === 'connection') {
											if (edgeValue.entity.name !== '') {
												$scope.entitySearchResult.list['key_relationships'].push({
													name: edgeValue.entity.name,
													risk_score: edgeValue.entity.properties.risk_score,
													source: subentity.source
												});
											}
											angular.forEach(edgeValue.entity.edges, function (subedv, subedk) {
												if (edgeValue.relationshipName === 'connection') {
													$scope.entitySearchResult.list['associated_persons'][subedv.relationshipName].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'memberships') {
													$scope.entitySearchResult.list['associated_companies']['memberships'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'transaction') {
													$scope.entitySearchResult.list['associated_companies']['transaction'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_financial_supports') {
													$scope.entitySearchResult.list['associated_companies']['political_financial_supports'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'politicians') {
													$scope.entitySearchResult.list['associated_companies']['politicians'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'political_organizations') {
													$scope.entitySearchResult.list['associated_companies']['political_organizations'].push(subedv.entity.properties);
												}
												if (subedv.relationshipName === 'owners') {
													$scope.entitySearchResult.list['associated_companies']['owners'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'leadership_&_staff') {
													$scope.entitySearchResult.list['associated_companies']['leadership & staff'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'services_transactions') {
													$scope.entitySearchResult.list['associated_companies']['services transactions'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'child_organizations') {
													$scope.entitySearchResult.list['associated_companies']['child_organizations'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'holdings') {
													$scope.entitySearchResult.list['associated_companies']['holdings'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'donation_grant_recipients') {
													$scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].push({
														name: subedv.entity.properties.entity_name,
														risk_score: subedv.entity.properties.risk_score,
														url: subedv.entity.properties.url,
														relationship: subedv.entity.properties.relationship,
														summary: subedv.entity.properties.summary,
														source: subentity.source
													});
												}
												if (subedv.relationshipName === 'key_staff') {
													if ($.inArray(subedgev.entity.properties.name, key_staff_arr) == -1) {
														key_staff_arr.push(subedgev.entity.properties.name);
														$scope.entitySearchResult.list['key_staff'].push(subedgev.entity.properties);
													}
												}
											});
										}
									});
								}
							});
							if ($scope.entitySearchResult.list['associated_companies']['child_organizations'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['holdings'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['donation_grant_recipients'].length == 0
								&& $scope.entitySearchResult.list['associated_companies']['memberships'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['owners'].length === 0
								&& $scope.entitySearchResult.list['associated_companies']['services transactions'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.associated_company = false;
							}
						}
						if (subfetcherData.fetcher === 'bloomberg.com' || subfetcherData.fetcher === 'company.linkedin.com') {
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								if (subentityKey === 0) {
									angular.forEach(subentity.edges, function (subedv, subedk) {
										if ((subedv.relationshipName) == 'news_articles') {
											$scope.entitySearchResult.list["all_news_articles"].push({
												title: subedv.entity.properties.title,
												details: subedv.entity.properties.body,
												updatedOn: new Date(subedv.entity.properties.date),
												image: subedv.entity.properties.image != undefined ? subedv.entity.properties.image : '',
												link: subedv.entity.properties.link != undefined ? subedv.entity.properties.link : '',
												source: subentity.source
											});
										}
										if (subedv.relationshipName === 'key_executives') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												var index = -1;
												angular.forEach(EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
										if (subedv.relationshipName === 'compensation_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['compensation_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'nominating_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['nominating_committee'].push(subedv.entity.properties);
											}
										}
										if (subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entitySearchResult.list['corporate_governance_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													risk_score: subedv.entity.properties.risk_score,
													profile_link: subedv.entity.properties.profile_link,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === 'audit_committee') {
											if (subedv.entity.properties.name !== '') {
												$scope.entitySearchResult.list['audit_committee'].push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subedv.entity.properties.profile_link,
													profile_link: subedv.entity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source,
													key_developments: subedv.entity.properties.key_developments
												});
											}
										}
										if (subedv.relationshipName === "similar_companies") {
											var index = -1;
											angular.forEach(tagCloudOrganizationNameList, function (object, key) {
												if (object.text === subedv.entity.properties.name)
													index = key
											});
											if (index === -1) {
												tagCloudOrganizationNameList.push({
													text: subedv.entity.properties.name,
													size: 5,
													type: 'organization'
												});
											} else {
												tagCloudOrganizationNameList[index].size += 5;
												tagCloudOrganizationNameList[index].size > maxOrganizationDomainRangeValue ? maxOrganizationDomainRangeValue = tagCloudOrganizationNameList[index].size : '';
											}
										}
										if (subedv.relationshipName === 'compensation_committee' || subedv.relationshipName === 'nominating_committee' || subedv.relationshipName === 'corporate_governance_committee') {
											if (subedv.entity.properties.name !== '' && subedv.entity.properties.designation !== '') {
												$scope.entityOtherMemberList.push({
													name: subedv.entity.properties.name,
													designation: subedv.entity.properties.designation,
													link: subentity.properties.profile_link,
													extented_properties: subentity.properties,
													risk_score: subedv.entity.properties.risk_score,
													source: subedv.entity.source
												});
												var index = -1;
												angular.forEach(EntityCommonTabService.entityChartSharedObject.tagCloudNameList, function (object, key) {
													if (object.text === subedv.entity.properties.name)
														index = key
												});
												if (index === -1) {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList.push({
														text: subedv.entity.properties.name,
														size: 5,
														type: 'person'
													});
												} else {
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size += 5;
													EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size > maxPersonDomainRangeValue ? maxPersonDomainRangeValue = EntityCommonTabService.entityChartSharedObject.tagCloudNameList[index].size : '';
												}
											}
										}
									});
									if ($scope.entityNewsList.length > 0 && subfetcherData.fetcher === 'bloomberg.com') {
										angular.forEach($scope.entitySearchResult.list["all_news_articles"], function (object, key) {
											$scope.entityNewsList.push(object);
										});
										$scope.entityNewsList = $scope.entityNewsList.sort(function (a, b) {
											return moment(a.updatedOn).isBefore(b.updatedOn) ? 1 : -1;
										});
									}
								}
							});
							if ($scope.entitySearchResult.list['audit_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_audit = false;
							}
							if ($scope.entitySearchResult.list['nominating_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_nominating = false;
							}
							if ($scope.entitySearchResult.list['corporate_governance_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_corporate = false;
							}
							if ($scope.entitySearchResult.list['compensation_committee'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_committe_compensation = false;
							}
						}
						if (subfetcherData.fetcher === 'glassdoor.com') {
							if (subfetcherData.entities.length == 0) {
								// show error messages that did not found
								// data
								return;
							}
							var nested_data = d3.nest().key(function (d) { return d.relationshipName }).entries(subfetcherData.entities[0].edges)
							var rating_insights;
							angular.forEach(nested_data, function (d, i) {
								if (d.key == "rating_insights") {
									rating_insights = d.values;
								}
								if (d.key == "current_job_openings") {
									$scope.entitySearchResult.list['current_job_openings'] = d.values;
								}
								if (d.key == "ratings_trends") {
									$scope.entitySearchResult.list['ratings_trends'] = d.values;
								}
							});
							$scope.entitySearchResult.list['glassdoor'] = {};
							angular.forEach(rating_insights, function (d, i) {
								var valPercent = parseFloat((parseFloat(d.entity.properties.value) / 5) * 100).toFixed(1); // out
								valPercent = valPercent < 0 ? 0 : valPercent;
								$scope.entitySearchResult.list['glassdoor'][d.entity.properties['type_']] = {
									percent: valPercent,
									percentRounded: parseInt(valPercent)
								};
							})
							if ($scope.entitySearchResult.list['ratings_trends'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_ratings_trends = false;
							}
							if ($scope.entitySearchResult.list['current_job_openings'].length === 0) {
								$scope.entitySearchResult.is_data_not_found.is_carrer_opportuny = false;
							}
						}
						if (subfetcherData.fetcher === 'shodan.com') {
							
							angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
								angular.forEach(subentity.edges, function (subedv, subedk) {
									if (subedv.relationshipName === 'gps_location') {
										shodanData.gps_locations.push({
											lat: subedv.entity.properties.latitude,
											long: subedv.entity.properties.longitude,
											mark: 'assets/images/redpin.png',
											name: subentity.properties.ip,
											source: subedv.entity.source,
											title: subentity.properties.ip,
											country_name: subentity.properties.country_name,
											details: subentity.properties.details,
											organization: subentity.properties.organization,
											postal_code: subentity.properties.postal_code,
											isp: subentity.properties.isp,
											'txt': '<span>Title: ' + subentity.properties.title + '</span></br><span>organization: ' + subentity.properties.organization + '</span></br><span>details: ' + subentity.properties.details + '</span></br><span>IP: ' + subentity.properties.ip + '</span></br><span>isp: ' + subentity.properties.isp + '</span></br><span>last update: ' + subentity.properties.last_update + '</span></br><span>city: ' + subentity.properties.city + '</span></br><span>country name: ' + subentity.properties.country_name + '</span></br><span>Source: ' + subedv.entity.source + '</span>'
										})
									}
									if (subedv.relationshipName === 'open_ports') {
										shodanData.open_ports.push({
											name: subedv.entity.name
										})
									}
								});
							});
							EntityCommonTabService.tabsWorldMap.digitalFootPrintChart.markers = shodanData && shodanData['gps_locations'] ?shodanData['gps_locations'] :  {};
							EntityCommonTabService.World(EntityCommonTabService.tabsWorldMap.digitalFootPrintChart);
						}
					} else {
						if (subfetcherData.fetcher === 'bloomberg.com') {
							$scope.entitySearchResult.is_data_not_found.is_data_leadership = false;
							$scope.entitySearchResult.is_data_not_found.board_of_director = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_audit = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_compensation = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_nominating = false;
							$scope.entitySearchResult.is_data_not_found.is_committe_corporate = false;
						}
						if (subfetcherData.fetcher === 'company.littlesis.com') {
							$scope.entitySearchResult.is_data_not_found.associated_company = false;
							$scope.entitySearchResult.is_data_not_found.is_politcal_donation = false;
							$scope.entitySearchResult.is_data_not_found.is_interlockdata = false;
							$("#politicalOrgs").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
							$("#politicaldonations").append("<div class=\"right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right\"><div class=\"alert-message alert-message-wrapper\">Data Not Found</div></div>");
						}
						if (subfetcherData.fetcher === 'company.linkedin.com') {
							$scope.entitySearchResult.is_data_not_found.is_linkedin_post = false;
						}
						if (subfetcherData.fetcher === 'twitter.com') {
							$scope.entitySearchResult.is_data_not_found.is_recentTweets = false;
							$scope.entitySearchResult.is_data_not_found.is_interactionration = false;
							$('#tagcloudCompanyTwitterTags').html('<div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper cloud-tag-ditto"><div class="alert-message-wrapper alert-message">Data Not Found</div></div>');
						}
						if (subfetcherData.fetcher === 'instagram.com') {
							$scope.entitySearchResult.is_data_not_found.is_instagramlist = false;
						}
						if (subfetcherData.fetcher === 'pages.facebook.com') {
							$scope.entitySearchResult.is_data_not_found.is_facebook_list = false;
						}
						if (subfetcherData.fetcher === 'glassdoor.com') {
							$scope.entitySearchResult.is_data_not_found.is_ratings_trends = false;
							$scope.entitySearchResult.is_data_not_found.is_carrer_opportuny = false;
						}
					}
				});
				if (key === fetcherIds.length - 1) {
					deffered.resolve($scope.entitySearchResult);
				}
			}, function (error) {
				deffered.reject(error.responsemessage);
			});
		});
		// calling web socket
		return deffered.promise;
	}
	function socialBubbleChart(bubbleOptions) {	
		$timeout(function () {
			$scope.entitySearchResult.list['social_followers_bubble_chart'] = false;
			var exampleChart = new BubbleHierarchyChart(bubbleOptions);
		}, 10000);
	}
	function getUrlAnalysisData(website) {
		var data = {
			"fetchers": ["1022"],
			"keyword": website,
			"searchType": "Company",
			"limit": "1"
		}
		EntityApiService.getEntityDataByTextId(data).then(function (subResponse) {
			angular.forEach(subResponse.data.results, function (subfetcherData, key) {
				if (subfetcherData.fetcher === 'alexa.com') {
					angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
						$scope.entitySearchResult.list['url_analysis'].push(subentity.properties);
						if (subentity.name !== '' && subentity.name) {
						}
					});
					if (subfetcherData.entities.length == 0 || subfetcherData.entities[0].edges.length == 0) {
						// show error messages that did not found
						// data
						return;
					}
					var nested_data = d3.nest().key(function (d) {
						return d.relationshipName
					}).entries(subfetcherData.entities[0].edges)
					angular.forEach(nested_data, function (d, i) {
						var newData = [];
						angular.forEach(d.values, function (innerVal, innerKey) {
							if (d.key == "traffic_by_countries" && innerVal.entity.properties.precent_of_visitors) {
								newData.push(innerVal);
							} else if (d.key == "search_keywords" && innerVal.entity.properties.precentage_of_search_traffic) {
								newData.push(innerVal);
							} else if (d.key == "local_ranking") {
								newData.push(innerVal);
							} else if (d.key == "traffic_by_domain" && innerVal.entity.properties.precent_of_visitors) {
								newData.push(innerVal);
							}
						})
						if (d.key == "traffic_by_countries") {
							$scope.entitySearchResult.list['traffic_by_countries'] = newData;
						}
						if (d.key == "search_keywords") {
							$scope.entitySearchResult.list['search_keywords'] = newData;
						}
						if (d.key == "local_ranking") {
							$scope.entitySearchResult.list['local_ranking'] = newData;
						}
						if (d.key == "traffic_by_domain") {
							$scope.entitySearchResult.list['traffic_by_domain'] = newData;
						}
					});
					if ($scope.entitySearchResult.list['traffic_by_countries'].length > 0)
						$scope.entitySearchResult.list['is_traffic_by_countries'] = false;
					if ($scope.entitySearchResult.list['search_keywords'].length > 0)
						$scope.entitySearchResult.list['is_search_keywords'] = false;
					if ($scope.entitySearchResult.list['traffic_by_domain'].length > 0)
						$scope.entitySearchResult.list['is_traffic_by_domain'] = false;
					if ($scope.mainInfoTabType == 'Overview') {
						loadOverviewurlcharts();
					}
					if ($scope.entitySearchResult.list['traffic_by_countries'].length === 0)
						$scope.entitySearchResult.is_data_not_found.is_traffic_by_country = false;
					if ($scope.entitySearchResult.list['search_keywords'].length === 0)
						$scope.entitySearchResult.is_data_not_found.is_traffic_key_word = false;
					if ($scope.entitySearchResult.list['traffic_by_domain'].length === 0)
						$scope.entitySearchResult.is_data_not_found.is_traffic_bydomain = false;
				}
			});
		}, function (error) {
			$scope.entitySearchResult.is_data_not_found.is_traffic_by_country = false;
			$scope.entitySearchResult.is_data_not_found.is_traffic_key_word = false;
			$scope.entitySearchResult.is_data_not_found.is_traffic_bydomain = false;
		});
	}
	function loadOverviewurlcharts() {
		if (($("#traffic_by_countries").width() == 0 && $scope.entitySearchResult.list['traffic_by_countries'].length > 0 && $("#traffic_by_countries").find("svg").length == 0) ||
			($("#search_keywords").width() == 0 && $scope.entitySearchResult.list['search_keywords'].length > 0 && $("#search_keywords").find("svg").length == 0) ||
			($("#traffic_by_domain").width() == 0 && $scope.entitySearchResult.list['traffic_by_domain'].length > 0 && $("#traffic_by_domain").find("svg").length == 0)) {
			$timeout(function () {
				loadOverviewurlcharts();
			}, 1000);
			return;
		}
		// traffic by countries pie starts here
		if ($scope.entitySearchResult.list['traffic_by_countries'] && $scope.entitySearchResult.list['traffic_by_countries'].length > 0) {
			var traffic_by_countriesData = [];
			angular.forEach($scope.entitySearchResult.list['traffic_by_countries'], function (d, i) {
				traffic_by_countriesData.push({
					"key": d.entity.properties.country,
					"doc_count": parseFloat(d.entity.properties.precent_of_visitors.split("%")[0]).toFixed(2)
				})
			})
			EntityCommonTabService.InitializeandPlotPie(traffic_by_countriesData, "traffic_by_countries")
		}
		// traffic by search key word
		if ($scope.entitySearchResult.list['search_keywords'] && $scope.entitySearchResult.list['search_keywords'].length > 0) {
			var search_keywords = [];
			angular.forEach($scope.entitySearchResult.list['search_keywords'], function (d, i) {
				search_keywords.push({
					"key": d.entity.properties.keyword,
					"doc_count": parseFloat(d.entity.properties.precentage_of_search_traffic.split("%")[0]).toFixed(2)
				})
			})
			EntityCommonTabService.InitializeandPlotPie(search_keywords, "search_keywords");
		}
		if ($scope.entitySearchResult.list['traffic_by_domain'] && $scope.entitySearchResult.list['traffic_by_domain'].length > 0) {
			var traffic_by_domain = [];
			angular.forEach($scope.entitySearchResult.list['traffic_by_domain'], function (d, i) {
				traffic_by_domain.push({
					"key": d.entity.properties.domain,
					"doc_count": parseFloat(d.entity.properties.precent_of_visitors.split("%")[0]).toFixed(2)
				})
			})
			EntityCommonTabService.InitializeandPlotPie(traffic_by_domain, "traffic_by_domain");
		}
	}
}