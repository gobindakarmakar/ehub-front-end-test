'use strict';
angular.module('ehubEntityApp')
	.controller('EntityCompanyNewController', entityCompanyNewController);
entityCompanyNewController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$stateParams',
	'EntityApiService',
	'$q',
	'UnWantedKeyList',
	'EntityGraphService',
	'$uibModal',
	'$window',
	'EnrichSearchGraph',
	'$timeout',
	'Company_Unwanted_End_Points',
	'HostPathService',
	'ActApiService',
	'$localStorage',
	'handlingCompanyConstantName',
	'chartsConst',
	'worldCountryDetailList',
	'EHUB_FE_API',
	'$sce',
	'EHUB_API',
	'TopPanelApiService',
	'customEntites',
	'EntityorgChartService',
	'$filter',
	'utilityConstant',
	'EntityCommonTabService'
];
function entityCompanyNewController(
	$scope,
	$state,
	$rootScope,
	$stateParams,
	EntityApiService,
	$q,
	UnWantedKeyList,
	EntityGraphService,
	$uibModal,
	$window,
	EnrichSearchGraph,
	$timeout,
	Company_Unwanted_End_Points,
	HostPathService,
	ActApiService,
	$localStorage,
	handlingCompanyConstantName,
	chartsConst,
	worldCountryDetailList,
	EHUB_FE_API,
	$sce,
	EHUB_API,
	TopPanelApiService,
	customEntites,
	EntityorgChartService,
	$filter,
	utilityConstant,
	EntityCommonTabService) {
	var subsidariesfilteredData = [];
	$scope.ehub_Api = EHUB_API;
	$scope.hyperLinksNewTab = $rootScope.hyperLinksNewTab;
	$scope.hideTabs = false;
	$rootScope.currentStateMyEntity = $state.current.name;
	$scope.editUBOValue = false;
	$scope.formAnalysisRefreshISO = {
		isopen: false
	};
	$scope.customSelectedAddCompany = "";
	$scope.customSelectedAddPerson = "";
	$scope.editCustomSelected = {
		country: ''
	};
	$scope.customSelectedScreeningPerson = "";
	$scope.customSelectedScreeningCompany = "";
	$scope.sourceList=[];
	$scope.fiteredSourceList = [];
	$scope.addsourceInput = {
		searchedSourceInputCompany: "",
		searchedSourceInputPerson: ""
	};
	$scope.showfiteredSourceList = false;
	$scope.showAddNew = false;
	$scope.sourceWithBSTRegistry = [];
	$scope.getMultiScreenShotSelected = [];
	$scope.evidenceReportAddTopage = [];
	$scope.isFromAddToPage = false;
	$scope.addModelClassification =EntityCommonTabService.basicsharedObject.addModelClassification;
	$scope.editModelClassification = EntityCommonTabService.basicsharedObject.editModelClassification;
	$scope.screeningModelClassification = EntityCommonTabService.basicsharedObject.screeningModelClassification;
	$scope.showDirectIndirectPercentageCompany = true;
	$scope.showDirectIndirectPercentagePerson = true;
	$scope.editshowDirectIndirectPercentagePerson = true;
	$scope.example5customTexts = { buttonDefaultText: 'Classification' };
	$scope.evidenceDocumentsListForReport = [];
	$scope.evidenceDocumentsListDocotherThanPNG = [];
	var fetchLink_officershipScreeningData = [];
	$scope.financialHeaderData = [];

	$scope.isJson = function (str) {
		EntityCommonTabService.isJson (str);
	};
	$scope.makeUrlSecureToWork = function (val) {
		return EntityCommonTabService.makeUrlSecureToWork (val);
	};
	if ($rootScope.companyViewSelected == "compliance") {
		$scope.hideTabs = true;
	}
	if ($stateParams.qId != undefined && $stateParams.qId != '' && $stateParams.qId != null) {
		$scope.qId = $stateParams.qId;
		$scope.isQuessionaire = true;
		var caseDocsparams = {
			caseId: $stateParams.qId,
			token: $rootScope.ehubObject.token
		};
		ActApiService.getCaseDetails(caseDocsparams).then(function (response) {
			if (response.data.name) {
				$scope.currentCaseName = response.data.name;
				//					$scope.getCaseByRiskDetails();
				$scope.questionnaireDocId = response.data.questionnaireDocId ? response.data.questionnaireDocId : 0;
				$scope.fetchDocumentDetails(response.data.questionnaireDocId);
				$scope.fetchFileDetails(response.data.questionnaireDocId);
			} else {
				getRadarThreatAttackType();
				getTableData($scope.pageNumber);
			}
		}, function () {
		});
	} else {
		$scope.isQuessionaire = false;
	}
	var allLevelData = [];
	var intialSelectedRoles = [];
	$scope.SelectedRolesfilter = [];
	// Commitee Tab Section
	$scope.commiteeTabType = 'Nominating';
	$scope.onClickCommiteeTypeTabs = function (type) {
		$scope.commiteeTabType = type;
	};
	// Fraud Tab Section
	$scope.fraudTabType = 'Scam List';
	// $scope.onClickFraudTypeTabs = function (type) {
	// 	$scope.fraudTabType = type;
	// };
	$scope.officersAdverseNews = [];
	$scope.generateReportofficernews = [];
	$scope.splittedScreeningData = [];
	$scope.addEntityfrommodal = false;
	$scope.showSubsidaries = false;
	$scope.generateReportofficernewsByGroup = [];
	$scope.ceriSearchResultObject = {};
	$scope.shareholderCount = 0;
	$scope.sortedCount = 0;
	$scope.subsidiaries= EntityCommonTabService.basicsharedObject.subsidiaries;
	$scope.unique_officer_roles = [];
	$scope.unique_officer_roles_array = [];
	$scope.complianceRightAdverseNews = [];
	var vlaDataArr = {};
	EntityCommonTabService.entityChartSharedObject.vlaDataArr = vlaDataArr;
	$scope.countryNames = [];
	$scope.ScreeningRisk = 0;
	var mainAdverseNews = [];
	var mainofficersNews = [];
	var today_date = ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2)));
		var searchText = $stateParams.query.trim();
	$scope.searchText = searchText;
	$scope.scaledNumArr = [];
	$scope.cumulativeRisk = 0;
	var IR, DR, TR;
	$scope.genRepDate = new Date();
	$scope.loadLinkAnalysis = function (isadverse, VLAId) {
		var indexOfbackSlash = window.location.href.split("/#!/")[0].lastIndexOf('/');
		var entitySearchResult = $scope.entitySearchResult.name ? $scope.entitySearchResult.name : searchText;
		var url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/linkAnalysis?q=" + entitySearchResult;
		if (isadverse != '') {
			url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/linkAnalysis?q=" + entitySearchResult + '&p=Adverse News&type=company';
		}
		var newWindow = window.open(url, '_blank');
		newWindow.current_data = vlaDataArr[VLAId];
	};
	// initializing entity company related variables
	var entityCompanySearchObject = {};
	var entityCompanyEmptyObject = {}, locationList = [];
	$scope.disableSearchButton = true;
	$scope.leadership = [];
	$scope.entityNewsList = [];
	$scope.entityTwitterList = [];
	$scope.entityLinkedinList = [];
	$scope.entityInstagramList = [];
	var entityInstagramList = [];
	var videos_list = [];
	$scope.entityGooglePlusList = [];
	$scope.entityFacebookList = [];
	$scope.relatedPersonTab = 'key Person';
	$scope.income_statement = [];
	$scope.balance_sheet = [];
	$scope.cash_flow_statement = [];
	var WorldData = worldCountryDetailList;
	$scope.entityOtherMemberList = [];
	$scope.stock_measures = [];
	$scope.companyDetailList = [];
	$scope.isStockDataFound = false;
	var stockModalData = EntityCommonTabService.basicsharedObject.stockModalData;
	$scope.socialMediaActiveCount = 0;
	$scope.companyDetailsInfo = {};
	$scope.isYahooStockDataFound = false;
	var yahooStockData = yahooStockData ;
	$scope.screeningpopover = false;
	var tagCloudPopularOptions = tagCloudPopularOptions, popular_tag_words = [], maxPopularTagDomainRangeValue = 5;
	var bloombergStockLineChartData = EntityCommonTabService.basicsharedObject.bloombergStockLineChartData;
	var stockhistoryLineChartData1 = {};
	var tradedAs = [], founders = [], services = [], industry = [], key_people = [], products = [], specialities = [], social_activity_locations = [], people_also_viewd = [];
	$scope.EditEntityTypeList = [];
	$scope.addEntityTypeList = [];
	$scope.entitySearchResult = {
		complianceWidgets: {
			description: false,
			popoverMsg: '',
			popoverDate: '',
			updateOneWidget: updateOneWidget,
			setPopoverMsg: setPopoverMsg
		},
		twitter_retweets_count: 0,
		list: EntityCommonTabService.basicsharedObject.list,
		is_data_not_found: EntityCommonTabService.basicsharedObject.is_data_not_found,
		riskdata: [],
		'stocks': {
			'bloomberg_prop': {},
			'yahoo_prop': {},
			'cnn_prop': {}
		}
	};
	$scope.screeningData = [];
	$scope.actualScreeningData = [];
	$scope.summaryScreenig = [];
	$scope.conplianceMapKeys = utilityConstant.conplianceMapKeys;
	//other_company_id_number
	$scope.conplianceIdentifiers = utilityConstant.conplianceIdentifiers;
	// Define map keys for the report starts
	$scope.conplianceMapKeysReport = utilityConstant.conplianceMapKeysReport;
	//other_company_id_number
	$scope.conplianceIdentifiersReport = [
	];
	//ending of define map keys
	$scope.ownershipCount = {
		parentCount: 0,
		subsidaryCount: 0
	};
	$scope.CountryShareHolders = [];
	$scope.selectedMultipleeEntities = [];
	$scope.companyAssociatedDocDetailsInfo = [{
		'description': '',
		'date': '',
		'url': '',
		'links': {
			'self': ''
		}
	}];
	$scope.companyInfo = {
		key: 'bst:description',
		value: 'Company Informantion',
		class: '',
		edit: false,
		texts: '',
		title: 'Registerd Company Information Sources'
	};
	var toolTipData = {};
	var filteredApplied = 0;
	$scope.companyAssociatedDocDetailsInfo = [];
	$scope.companyManagementDetailsInfo = [];
	$scope.CountryOperations = [];
	$scope.CountryOperationsViewForMap = [];
	$scope.identifiedRisk = 0;
	$scope.pageloader = EntityCommonTabService.basicsharedObject.pageloader;
	var lazyLoadingentity = EntityCommonTabService.lazyLoadingentity;
	$scope.ErrorHandling = {
		flowchart: false,
		companyInformation: false
	}
	$scope.interlock = [];
	$scope.shareHolderTypeStatus = false;
	var personEntitytype = ['person'];
	$scope.personEntitytypeForReport = ['i', 'm', 'h', 'd', 'l'];
	$scope.Source_url_tuna = '';
	var div = d3.select("body").append("div").attr("class", "Screening_new_tooltip").style("position", "absolute").style("width", "auto").style("min-width", "300px").style("background", "rgba(84, 84, 84,1)").style("padding", "10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none").style("z-index", "99999");
	var div2 = d3.select("body").append("div").attr("class", "percentage_tooltip").style("position", "absolute").style("width", "auto").style("min-width", "300px").style("max-width", "370px").style("background", "rgba(84, 84, 84,1)").style("padding", "10px").style("border-radius", "10px").style("font-size", "10px").style("display", "none").style("z-index", "99999").style("box-shadow", "0 2px 6px 0 rgba(0, 0, 0, 0.71);");
	var div3 = d3.select("body").append("div").attr("class", "entity_popover").style("position", "absolute").style("width", "auto").style("max-height", "none").style("height", "auto").style("min-width", "300px").style("max-width", "376px").style("background", "rgba(84, 84, 84,1)").style("border-radius", "10px").style("display", "none").style("z-index", "99999").style("box-shadow", "0 2px 6px 0 rgba(0, 0, 0, 0.71);");
	var div4 = d3.select("body").append("div").attr("class", "selectedEntityOptions").style("position", "absolute").style("width", "auto").style("max-height", "none").style("height", "auto").style("min-width", "auto").style("max-width", "376px").style("background", "rgba(84, 84, 84,1)").style("border-radius", "10px").style("display", "block").style("z-index", "99999").style("box-shadow", "0 2px 6px 0 rgba(0, 0, 0, 0.71);");
	$scope.chartLevel = 5;
	$scope.sliderMinValue = 25;
	$scope.sliderMaxValue = 100;
	$scope.appliedOwnerShipControlPercentageMinVal = 25;
	$scope.appliedOwnerShipControlPercentageMaxVal = 100;
	$scope.filteredChart = EntityCommonTabService.basicsharedObject.filteredChart;
	$scope.newOfficer =  EntityCommonTabService.basicsharedObject.newOfficer;
	$scope.mainInfoTabType = $rootScope.complianceReport ? 'Duediligence' : 'Overview';//For Genpact we set Compliance as default	
	// $scope.mainInfoTabType = 'Overview';//For Genpact we set Compliance as default
	$scope.industryListsData = [];
	// ---------------------------------------Piechart--------------------------------------------------//
	// var worldComplianceLocationsOptionsForReport = {
	// 	container: "#companyexampleReport",
	// 	uri1: "../vendor/data/worldCountries.json",// Url of data
	// 	uri2: "../vendor/data/worldMapData.json",// Url of data
	// 	// "proxy/api/cases/all"
	// 	height: 700,
	// 	markers: {}
	// };
	// tag cloud variables initializations for persons
	var tagCloudNameList = [], maxPersonDomainRangeValue = 5;
	EntityCommonTabService.entityChartSharedObject.tagCloudNameList = tagCloudNameList;
	// tag cloud variables initializations for twitter tag words
	var  twitter_tag_words = [], maxTwitterTagDomainRangeValue = 5;
	EntityCommonTabService.entityChartSharedObject.twitter_tag_words = twitter_tag_words;
	var mainFetcherIds = ['1013', '1006', '1008', '1021'], fetcherIds = ['1013', '1006', '1008', '1021', '2001', '2', '1002', '1003', '1004', '1005', '1001', '3001', '1007', '4001', '2002', '2003', '2005', '1010', '1012', '3002', '3003', '11', '3004', '3005', '16', '3008', '1014', '23', '21', '1015', '1016', '1017', '1023', '15', '8001'], foundData = false;
	var copyMainFetcherIds = mainFetcherIds;
	$scope.dummyCompanies = false;
	var initateData = {};
	var query = $stateParams.query;
	if (query.indexOf('&eid=') !== -1) {
		$stateParams.query = query.split('&eid=')[0];
		$stateParams.eid = query.split('&eid=')[1];
	}
	if ($stateParams.eid && $stateParams.website) {
		initateData = {
			keyword: $stateParams.query,
			searchType: 'Company',
			//	limit: 1,
			"jurisdiction": $stateParams.cid ? $stateParams.cid : "",
			"light-weight": false,
			"identifier": $stateParams.eid,
			//"query_parameters":{"website":$stateParams.website}
			"website": $stateParams.website
		};
	}
	else if ($stateParams.eid) {
		initateData = {
			keyword: $stateParams.query,
			searchType: 'Company',
			//	limit: 1,
			"jurisdiction": $stateParams.cid ? $stateParams.cid : "",
			"light-weight": false,
			"identifier": $stateParams.eid
		};
	} else {
		initateData = {
			keyword: searchText,
			searchType: 'Company',
			jurisdiction: "gb",
		};
	}
	var FlagTooltipData = {};
	var entityDataOnClikOfOdg = {};
	var deleteSelectedEntity = {};
	var fields = ["overview", "officership"];//Compliance Sections 
	if ($scope.mainInfoTabType === 'Overview') {
		fields.push('finance_info')
	}
	var findJSon = _.find(handlingCompanyConstantName, { 'name': 'json', 'fetcher': encodeURIComponent(initateData.keyword) });
	findJSon = false;
	if (findJSon) {//Enter loop  for mockdata 
		var fields = ["overview", "officers"];
		if ($scope.mainInfoTabType === 'Overview') {
			fields.push('finance_info')
		}
		for (var f = 0; f < fields.length; f++) {
			var url = {
				name: findJSon.alt_name,
				fileld: fields[f],
				jurisdiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : '',
			}
			linkcall(url, fields[f], true);
		}//for loop ends
	} else if ($stateParams.qId && initateData.keyword) { //Enter loop  for redirected with name and case ID
		EntityApiService.getEntityResolvedV2(initateData).then(function (response) {
			if (response && response.data && response.data && response.data.results && response.data.results.length > 0 && response.data.results[0].identifier) {
				EntityApiService.selectEntity(response.data.results[0].identifier).then(function (seletresponse) {
					callFetchDatalink(response.data.results[0].identifier);
				}, function () {
					dataNotFound();
				});
			} else {
				dataNotFound();
			}
		}, function () {
			dataNotFound();
		});
	} else if ($stateParams.eid && initateData.keyword) { //Enter loop for select entity
		makeSelectentityrecurssive(initateData, $stateParams.eid);
	}
	function makeSelectentityrecurssive(initateData, eid) {
		EntityApiService.selectEntity(initateData.keyword, eid).then(function (response) {
			if (response && response.data && response.data["is-completed"]) {
				callFetchDatalink(initateData.keyword);
			} else {
				setTimeout(function () {
					makeSelectentityrecurssive(initateData, eid);
				}, 5000);
			}
		}, function () {
			dataNotFound();
		});
	}
	/*
	 * @purpose: call the getIndustryLists to get the industry lists
	 * @created:19 -02 - 2019
	 * @params: no
	 * @returns: no
	 * @author: Amarjith Kumar
	 */
	function getIndustryLists() {
		EntityApiService.getSourceIndustryList().then(function (res) {
			$scope.industryListsData = res.data.map(function (d) {
				return d.industryName;
			});
		}).catch(function (err) {
			console.log(err);
		});
	}
	getIndustryLists();
	/*
	 * @purpose: Constructor function for Screening
	 * @created:20 APril 2019
	 * @params: no
	 * @returns: no
	 * @author: Ram singh
	 */
	function Screeningconstructor(object) {
		var country_risk = object.country ? calculateCountryRisk(object.country_of_residence || object.country) : object.jurisdiction ? calculateCountryRisk($scope.getCountryName(object.jurisdiction)) : '';
		this["mdaas:RegisteredAddress"] = object.address ?object.address : '';
		this["sanction_bst:description"] = (object.sanctions && object.sanctions.length > 0) ? object.sanctions : [];
		this["@source-id"] = '';
		this["vcard:bday"] = '';
		this["@identifier"] = object.identifier ? object.identifier :'';
		this['no-screening'] =  object['no-screening'];
		this.name = object.name;
		this.Ubo_ibo = object.Ubo_ibo ? object.Ubo_ibo : '';
		this.listId  = '';
		this.high_risk_jurisdiction = object.high_risk_jurisdiction ? object.high_risk_jurisdiction.toLowerCase() : '';
		this.pep_url = (object.pep && object.pep.length > 0) ? object.pep : [];
		this.finance_Crime_url = object.finance_Crime_url && object.finance_Crime_url.length > 0 ? object.finance_Crime_url : [];
		this.adverseNews_url =   object.adverseNews_url &&  object.adverseNews_url.length >0 ? object.adverseNews_url:[];
		this.FalseNews = [];
		this.non_negativenews = [];
		this.MainCompany = '';
		this.hasURL = object.hasURL ? object.hasURL : '';
		this.sourceUrl = object.sourceUrl ? object.sourceUrl : '';
		this.from = object.from ? object.from : '';
		this.source_evidence = object.source_evidence ? object.source_evidence : '';
		this.identifier = object.identifier;
		this.jurisdiction = object.jurisdiction ? object.jurisdiction : '';
		this.juridiction = object.juridiction ? object.juridiction : '';
		this.officer_role = object.officer_role ? object.officer_role : '';
		this.officer_roles = object.officer_roles ? object.officer_roles : [];
		this.entity_type = object.entity_type ? object.entity_type : '';
		this.date_of_birth = object.date_of_birth ? object.date_of_birth : '';
		this.country = (object.country && object.country.toLowerCase() !== 'select country') ? object.country : '';
		this.showdeleteIcon = (object.showdeleteIcon === false) ? object.showdeleteIcon :(!object.showdeleteIcon ? true : true);
		this.showspinner = object.showspinner ? object.showspinner : false;
		this.isChecked = object.isChecked;
		this.level = object.level ? object.level : 0;
		this.childLevel = object.childLevel;
		this.childIdentifier = object.childIdentifier;
		this.bvdId = object.bvdId ? object.bvdId : '';
		this.entity_id = object.entity_id ? object.entity_id : '';
		this.classification = object.classification ? object.classification : [];
		this.report_jurisdiction_risk = object.report_jurisdiction_risk ? object.report_jurisdiction_risk : country_risk;
		this.officerIdentifier = object.officerIdentifier ? object.officerIdentifier : '';
		this.isCustom = object.isCustom ? object.isCustom : false;
		this.id = object.id ? object.id : '';
		this.highCredibilitySource = object.highCredibilitySource ? object.highCredibilitySource : '';
		this.isUbo = object.isUbo ? object.isUbo : '';
		this.indirectPercentage =  object.indirectPercentage ? object.indirectPercentage : '';
		this.sources =  object.sources ? object.sources:[];
		this.information_provider =  object.information_provider ? object.information_provider:'';
		this.entityRefernce =  object.entityRefernce &&  object.entityRefernce.length > 0 ?  object.entityRefernce : [];
		this.screeningresultsloader =  object.screeningresultsloader;
	}
	/*
	 * @purpose: call the fetchdatalink finance,overview,document,offficers
	 * @created:2 JAN 2019
	 * @params: no
	 * @returns: no
	 * @author: Ram singh
	 */
	function callFetchDatalink(identifier) {
		var jurisdiction = $stateParams.cid ? ($stateParams.cid).toUpperCase() : '';
		for (var f = 0; f < fields.length; f++) {
			var url = {
				identifier: encodeURIComponent(identifier),
				type: fields[f],
				jurisdiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : '',
			};
			linkcall(url, fields[f], false);
		} //for loop ends
	}
	/*
	 * @purpose: Data NOt Found
	 * @created:2 JAN 2019
	 * @params: no
	 * @returns: no
	 * @author: Ram singh
	 */
	function dataNotFound() {
		$('#entity-company').html('<span class="enrich-error-message">' + "RESULT NOT FOUND" + '</span>');
		$scope.disableSearchButton = false;
	}
	function getFormattedDateTime(date) {
		date = date.split(' ');
		date.splice(3, 3, date[5], date[3]).join(' ');
		return (new Date(date).getDate() + '-' + (new Date(date).getMonth() + 1) + '-' + (new Date(date).getFullYear()) + ' ' + (new Date(date).getHours()) + ':' + (new Date(date).getMinutes()) + ':' + (new Date(date).getSeconds()));
	}
	/*
			 * @purpose: New Advanced Search using V2 multisource API
			 * @created:12 Dev 2018
			 * @params: no
			 * @returns: no
			 * @author: Ram singh
			 */
	$scope.evidenceReportData = [];
	$scope.overViewDataLinksUrls = {};
	function linkcall(url, type, json) {
		var org_structure_link = '';
		EntityApiService.complianceFieldsUrl(url, json).then(function (response) {
			// $scope.disableSearchButton = false;
			if (response) {
				if (response.data && response.data.data && response.data.data.results && response.data.data.results.length > 0) {
					org_structure_link = (response.data.data.results[0].result['_links'] && response.data.data.results[0].result['_links']['graph:shareholders']) ? response.data.data.results[0].result['_links']['graph:shareholders'] : '';
				} else if (response && response.data && response.data.results && response.data.results.length > 0) {
					org_structure_link = (response.data.results[0]['links'] && response.data.results[0]['links']['graph:shareholders']) ? response.data.results[0]['links']['graph:shareholders'] : '';
				}
				if (type == "overview") {
					if (response && response.data && response.data.results && response.data.results.length > 0 && response.data.results[0].overview) {
						for (var i = 0; i < Object.keys(response.data.results[0].overview).length; i++) {
							if (Object.keys(response.data.results[0].overview)[i] != 'comapnyInfo') {
								$scope.evidenceReportData.push({
									'source': response.data.results[0].overview[Object.keys(response.data.results[0].overview)[i]]['@source-id'] ? response.data.results[0].overview[Object.keys(response.data.results[0].overview)[i]]['@source-id'] : Object.keys(response.data.results[0].overview)[i],
									'query': response.data.results[0].overview[Object.keys(response.data.results[0].overview)[i]]['bst:registryURI'],
									'parameter': response.data.results[0].overview[Object.keys(response.data.results[0].overview)[i]]['vcard:organization-name'],
									'dateAndTime': getFormattedDateTime(response.data.results[0].overview[Object.keys(response.data.results[0].overview)[i]].date),
								})
							}
						}
						$.each(response.data.results[0].overview, function (key, value) {
							if (key !== 'comapnyInfo') {
								$scope.overViewDataLinksUrls[key] = value;
								var splittedsource_screenshot = value.source_screenshot ? value.source_screenshot.split("_") : "";
								$scope.sourceWithBSTRegistry.push({
									sourceName: key,
									SourceValue: value,
									checked: false,
									splitSourceScreenshot: splittedsource_screenshot ? splittedsource_screenshot[1] + "_" + splittedsource_screenshot[3] + "_" + splittedsource_screenshot[4] : "",
									showHideScreenIcon: value.source_file_url ? true : false,
									disable: (value && value['bst:registryURI']) ? false : true,
									row_spinner: false,
									addtopageloader: false,
									showHideAddtoPage: value.isAddToPage ? true : false,
									downloadLink: value.source_file_url ? value.source_file_url : '',
									uploadedFileName: value.source_file_name ? value.source_file_name : '',
									showUploadIcon: value.source_file_name ? false : true,
									docid: value.docId ? value.docId : ""
								});
							}//if ends
						})
						var data = formatJson(response, type);
						if (data && data.length > 0) {
							$scope.entitySearchResult["@identifier"] = response.data.results[0].identifier;
							data[0].value.org_structure_link = org_structure_link;
							if (response.data.results.length > 0 && response.data.results[0] && response.data.results[0].overview && response.data.results[0].overview.comapnyInfo) {
								var topObject = {};
								for (var obj in response.data.results[0].overview.comapnyInfo) {
									topObject[obj] = response.data.results[0].overview.comapnyInfo[obj].value ? response.data.results[0].overview.comapnyInfo[obj].value : '';
								}
								$scope.entitySearchResult.list.topHeaderObject = topObject;
								$scope.entitySearchResult.list.topHeaderObject["@identifier"] = response.data.results[0].identifier;;
								$scope.entitySearchResult.list.topHeaderObject.identifier = response.data.results[0].identifier;;
								$scope.entitySearchResult.name = topObject['vcard:organization-name'] ? topObject['vcard:organization-name'] : '';
								EntityCommonTabService.basicsharedObject.companyName = $scope.entitySearchResult.name;
								$rootScope.companyOrPersonName = $scope.entitySearchResult.name;
								$scope.getCaseByRiskDetails();
								$scope.entitySearchResult.list.isSignificant = data[0].value.entitySiginificance;
								$rootScope.entityObj = $scope.entitySearchResult;
								apiCallGetSources();
								EntityCommonTabService.entityChartSharedObject.comapnyInfo = response.data.results[0].overview.comapnyInfo;
								complianeCompanyDetails(response.data.results[0].overview.comapnyInfo); //this section is for compliance company details section\
								// complianceCountriesofOperation(response.data.results[0].overview.comapnyInfo)//this section is for Compliance Countries operation
								topHeaderWebsite(response.data.results[0].overview.comapnyInfo); //this section is for website
								apiCallGetSources();
								//    linkcall(doc_url, "documents");
							} else {
								$scope.pageloader.companyDetailsloader = false;
								$scope.pageloader.screeningLoader = false;
								$scope.pageloader.treeGraphloader = false;
							}
							complianceNewOwnership(data[0].value, 'p0', true);
							if (json) {
								// complianceCountriesofOperation(data, json)//this section is for Compliance Countries operation
							}
							lazyLoadingentity.fetcher_data = topObject['vcard:organization-name'] ? topObject['vcard:organization-name'] : '';
							lazyLoadingentity.threatrstabData = { basic: data };
							if ($scope.mainInfoTabType === 'Duediligence' && lazyLoadingentity.compliancetabFirsttime) {
								$scope.$broadcast('dueDiligenceOnload');
								$("#generateReportMainDiv").css("display", "");
								$("#entitiesSelectedMainDev").css("display", "");
								if($scope.pageloader.treeGraphloader ||  $scope.pageloader.loadingText){
									$("#generateReport").parent().addClass("c-ban");
									$("#generateReport").addClass("pe-none");
								}else{
									$("#generateReport").parent().removeClass("c-ban");
									$("#generateReport").removeClass("pe-none");
								}
							}
							$scope.disableSearchButton = false;
						} else {
							//if there is no Data Show the Error Message
							getdefaultFilterForThreat();
							getRadarThreatAttackType();
							getTableData($scope.pageNumber);
							$('#entity-company').html('<span class="enrich-error-message">' + "RESULT NOT FOUND" + '</span>');
							$scope.disableSearchButton = false;
						}
						$scope.disableSearchButton = false;
					} else if ((response && response.data && response.data.results && response.data.results.length === 0 && !response.data["is-completed"]) || (!response || !response.data || !response.data.results)) {
						linkcall(url, type, json);
					} else {
						$('#entity-company').html('<span class="enrich-error-message">' + "RESULT NOT FOUND" + '</span>');
						$scope.disableSearchButton = false;
					}
				} //overview if ends
				if (type == "officers" || type == "officership") {
					var data = [];
					if (response.data && response.data && (!_.isEmpty(response.data))) {
						//taking the first result
						var currentResult = response.data ? response.data : {};
						for (var fetcher in currentResult) {
							if (currentResult[fetcher] && currentResult[fetcher].length > 0 && fetcher !== "officershipInfo") {
								var fetcherData = currentResult[fetcher].map(function (val) {
									val.source = val.customSource ? val.customSource : val.source ? val.source : '';
									return val;
								}); ``
								var finalObject = {
									source: fetcher,
									value: fetcherData
								}
								data.push(finalObject);
							} else if (fetcher === "officershipInfo") {
								if (currentResult['officershipInfo'].length > 0) {
									currentResult['officershipInfo'] = currentResult['officershipInfo'].sort(function (a, b) {
										if (a.name && b.name) {
											if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
											if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
											return 0;
										}
									})
									fetchLink_officershipScreeningData = currentResult['officershipInfo'].map(function (officerval) {
										officerval.name = officerval.name ? officerval.name : (officerval.keyword ? officerval.keyword : '');
										officerval.identifier = officerval.officerIdentifier ? officerval.officerIdentifier : (officerval.identifier ? officerval.identifier : (officerval.entity_id ? officerval.entity_id : ''));
										officerval.officer_role = officerval.officer_role ? officerval.officer_role : (officerval.role ? officerval.role : '');
										officerval.hasURL = officerval.hasURL ? officerval.hasURL : '';
										officerval.source = officerval.customSource ? officerval.customSource : officerval.source ? officerval.source : '';
										officerval.entity_type = ((officerval.hasOwnProperty('officer_role')) || (officerval.type && (officerval.type == "individual" || officerval.type == "person"))) ? 'person' : "organization";
										return officerval;
									});
									companyScreening(fetchLink_officershipScreeningData, 'officers');
									$scope.screeningData = jQuery.extend(true, [], $scope.actualScreeningData);
									$scope.pageloader.screeningLoader = $scope.screeningData.length > 0 ? false : true;
									var conbinedSreening = $scope.actualScreeningData.concat(customEntites.screeningaddition);//this line of code is for total Enties
									screeningTableOriginal_custom(conbinedSreening);
								}
							}
						} //for loop each obect
					} //if ends
					if (data.length > 0) {
						var totalOfficers_link = [];
						EntityCommonTabService.entityChartSharedObject.officerData = { officership: data };
						for (var index = 0; index < EntityCommonTabService.entityChartSharedObject.officerData.officership.length; index++) {
							if (EntityCommonTabService.entityChartSharedObject.officerData.officership[index].value.length > 0) {
								totalOfficers_link = totalOfficers_link.concat(EntityCommonTabService.entityChartSharedObject.officerData.officership[index].value);
							}
						}
						totalOfficers_link = totalOfficers_link.reverse();
						EntityCommonTabService.basicsharedObject.totalOfficers_link = totalOfficers_link;
						// compilanceManagement({ officership: data });
					} else {
						$scope.entitySearchResult.is_data_not_found.is_data_leadership = false;
						$scope.entitySearchResult.is_data_not_found.is_company_member = false;
						$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
					}
					EntityCommonTabService.basicsharedObject.totalOfficers_link.forEach(function (val) {
						if (val && val.name) {
							var regex = /^(Mr.|MR.|Dr.|mr.|DR.|dr.|ms.|Ms.|MS.|Miss.|Mrs.|mrs.|miss.|MR|mr|Mr|Dr|DR|dr|ms|Ms|MS|miss|Miss|Mrs|mrs)[\.\s]*/
							val.name = val.name.replace(regex, '');
						}
					});
					if($scope.mainInfoTabType === 'Overview' && EntityCommonTabService.lazyLoadingentity.leadershipTabFirsttime){
						$scope.$broadcast('overviewleadershipOnload');
					}
				} //officers ends
				if (type == "finance_info") { //this section is for financial Information  section 
					if (response && response.data && response.data.results && response.data.results.length > 0 && response.data.results[0].finance_info) {
						var value = (response.data.results[0].finance_info.Finanace_Information) ? response.data.results[0].finance_info.Finanace_Information : {};
						value.shareholders = response.data.results[0].finance_info.shareholders ? response.data.results[0].finance_info.shareholders : [];
						var data = [{
							value: [value]
						}];
						EntityCommonTabService.entityChartSharedObject.financeData = data;
						EntityCommonTabService.lazyLoadingentity.financeTabFirsttime = false;
						if ($scope.mainInfoTabType === 'Overview' && EntityCommonTabService.lazyLoadingentity.overviewTabFirsttime) {
							$scope.$broadcast('overviewFinanceData');
						}
						// loadFinacialWidget(data);
					}
				} //fainance  ends
				if (response.data.message === "Field 'officers' not found ...") {
					$scope.entitySearchResult.is_data_not_found.board_of_director = false;
					$scope.entitySearchResult.is_data_not_found.is_company_member = false;
					$scope.entitySearchResult.is_data_not_found.is_keyExecutive = false;
				}
			} else {
				$('#entity-company').html('<span class="enrich-error-message">' + "RESULT NOT FOUND" + '</span>');
				//$scope.disableSearchButton = false;
			}
		}, function (response) {//if API fails
			$scope.entitySearchResult.is_data_not_found.is_data_leadership = false;
			$scope.disableSearchButton = false;
		});
	} //linkcall function ends
	/*
			 * @purpose: format the JSON structure for  V2 multisource API works for document and company details Section
			 * @created:12 Dev 2018
			 * @params: no
			 * @returns: array
			 * @author: Ram singh
			 */
	function formatJson(response, type) {
		var data = [];
		if (response.data && response.data.data && response.data.data.results && response.data.data.results.length > 0) {
			//taking the first result
			var currentResult = response.data.data.results[0].result ? (response.data.data.results[0].result.data ? response.data.data.results[0].result.data : {}) : {};
			for (var fetcher in currentResult) {
				for (var i = 0; i < currentResult[fetcher].length; i++) {
					if (!_.isEmpty(currentResult[fetcher][i])) {
						currentResult[fetcher][i][type].identifier = response.data.data.results[0].identifier ? response.data.data.results[0].identifier : '';
						currentResult[fetcher][i][type].source = fetcher;
						var finalObject = {
							source: currentResult[fetcher][i]['bst:registryURI'] ? currentResult[fetcher][i]['bst:registryURI'] : (currentResult[fetcher][i][type].source),
							value: currentResult[fetcher][i][type]
						}
						data.push(finalObject);
					}
				}//loop each fetcher
			}//for loop each obect
		}//if conditon for data 
		else if (response && response.data && response.data.results && response.data.results.length > 0) {
			var currentResult = response.data.results[0] ? (response.data.results[0]['overview'] ? response.data.results[0]['overview'] : (type === 'documents' ? response.data.results[0].documents ? response.data.results[0].documents : {} : {})) : {};
			for (var fetcher in currentResult) {
				if (fetcher !== "comapnyInfo") {
					//	for (var i = 0; i < currentResult[fetcher].length; i++) {
					if (!_.isEmpty(currentResult[fetcher])) {
						currentResult[fetcher].identifier = response.data.results[0].identifier ? response.data.results[0].identifier : '';
						currentResult[fetcher].source = fetcher;
						var finalObject = {
							source: fetcher,
							value: currentResult[fetcher]
						};
						data.push(finalObject);
					}
				}
				//}//loop each fetcher
			}
		}
		return data;
	}
	/*
		* @purpose: Load Line chart on changing dropdown @created: 18 aug 2017
		* @params: selectedincomeStatement(string) @returns: none @author:
		* Ankit
		*/
	var revenueDataSheet = [], investmentDataSheet = [];
	$scope.newbalanceSheet = [];
	$scope.newIncomeSheet = [];
	var newcashflowSheet = [
		{
			key: "Net Cash Flow - Operating Activities",
			values: []
		}
	];
	$scope.newcashflowSheet_Enhance = [];
	function is_numeric(num) {
		return !isNaN(parseFloat(num)) && isFinite(num);
	}

	/*-------------------------world location chart----------------------------------*/
	// function World(options) {
	// 	var worldmapId = options.container;
	// 	var worldmapuri1 = options.uri1;
	// 	options.data = [WorldData, []];
	// 	options.countryOperations = $scope.CountryOperations;
	// 	EntityGraphService.plotWorldLocationChart(options);
	// 	//}
	// }
	/*-------------------------world location chart END----------------------------------*/
	/*---------------------------------------------- pie chart Starts--------------------------------*/
	/*---------------------------------------------- network chart Starts---------------------------------------------*/
	var riskScoreData = [];
	$scope.getCaseByRiskDetails = function () {
		EntityApiService.getRiskScoreData($scope.entitySearchResult["@identifier"]).then(function (response) {
			if (Math.abs(response.data.latest.entityRiskModel['overall-score']) > 1) {
				response.data.latest.entityRiskModel['overall-score'] = Math.abs(response.data.latest.entityRiskModel['overall-score']) / 100;
			}
			$scope.cumulativeRisk = response.data.latest.entityRiskModel['overall-score'] * 100;
			$scope.overviewRisks = response.data.latest.entityRiskModel;
			$scope.overviewRisks.cumulativeRisk = response.data.latest.entityRiskModel['overall-score'];
			$scope.riskScoreData = response.data.latest.entityRiskModel;
			riskScoreData = response.data.latest.entityRiskModel;
			EntityCommonTabService.basicsharedObject.riskScoreData = riskScoreData;
			$scope.scaledNumArr[0] = 0;
			$scope.scaledNumArr[1] = response.data.latest.entityRiskModel['overall-score'] * 100;
			$scope.scaledNumArr[2] = 0;
		}, function () {
		});
	}
	$scope.isDefined = function (value) {
		if (typeof value == 'undefined') {
			return false;
		}
		else {
			return true;
		}
	};

	/*----------------------------------------------Performance Line chart Ends ---------------------------------------------*/
	/*----------------------------------------------Social media bubble chart Starts---------------------------------------------*/
	/*
	* @purpose: load entity visualiser modal @created: 18 aug 2017 @params:
	* details(string), title(string), url(string) @returns: none @author:
	* swathi
	*/
	$scope.showBstNamedEntity = function (details, title, url) {
		var openEntityVisualiserModal = $uibModal.open({
			templateUrl: '../scripts/common/modal/views/entity.visualiser.modal.html',
			controller: 'EntityVisualiserController',
			size: 'lg',
			// backdrop: static,
			windowClass: 'entity-visualiser-modal bst_modal  update-entities-modal related-person-modal',
			resolve: {
				getDetails: function () {
					return details;
				},
				getTitle: function () {
					return title;
				},
				getUrl: function () {
					return url;
				}
			}
		});
		openEntityVisualiserModal.result.then(function (response) {
		}, function (reject) {
		});
	};
	/*
		* @purpose: onClickLoadLiveFeed nav to social-media-content page
		* @created: 21 aug 2017 @params: no @return: no @auhtor: sandeep
		*/
	$scope.onClickLoadLiveFeed = function () {
		var indexOfbackSlash = window.location.href.split("/#!/")[0].lastIndexOf('/');
		var url = window.location.href.split("/#!/")[0].substring(0, indexOfbackSlash) + "/#/live-feed/" + $scope.entitySearchResult.name;
		window.open(url, '_blank');
	};
	
	
	/*
		* @purpose: onClickRelatedPersonTab function to change tab @created: 22
		* aug 2017 @params: tabName(string) @return: no @auhtor: sandeep
		* @modified: 13 sep 2017 @modified by: sandeep (kept condition for tag
		* cloud)
		*/
	$scope.onClickRelatedPersonTab = function (tabName) {
		$scope.relatedPersonTab = tabName;
	};

	/*
	* @purpose: To navigate to Entity company page.
	* @created: 8 june 2018		
	* @author: Ram Singh
	*/
	$scope.ceriToEntityCompany = function (item, screening) {
		var name = ''
		if (screening === 'screening') {
			name = item.MainCompany ? item.MainCompany : "";
		} else {
			name = item.name ? item.name : "";
		}
		var website = item.hasURL ? item.hasURL : "";
		var id = item['@identifier'] ? item['@identifier'] : (item['identifier'] ? item['identifier'] : '');
		var url = $state.href('entityCompanyNew', { query: name, eid: id, website: website });
		if (website) {
			url = window.location.href.split("/entity/#!/")[0] + "/entity#!/company/" + id;
		} else {
			url = window.location.href.split("/entity/#!/")[0] + "/entity#!/company/" + id;
		}
		window.open(url, '_blank');
	};
	/*
	 * @purpose: To navigate to Entity Person page.
	 * @created: 08 june 2018		
	 * @author: Ram Singh
	 */
	$scope.ceriToEntityPerson = function (item) {
		var url = EHUB_FE_API + 'entity/#!/person/' + item;
		window.open(url, '_blank');
	};
	/*
		* @purpose: onViewClick open modal for specific category @created: 19
		* aug 2017 @params: categoryName(string) @returns: no @author: sandeep
		*/
	$scope.onViewClick = function (categoryName) {
		if (categoryName === 'overview') {
			var commentsModalInstance = $uibModal.open({
				templateUrl: 'overviewModalId.html',
				controller: 'OverviewModalController',
				windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal entity-overview-modal',
				// windowClass: 'entity-search-modal-wrapper entity-overview-modal',
				resolve: {
					data1: function () {
						return $scope.entitySearchResult;
					},
					stockModalData: function () {
						return stockModalData;
					}
				}
			});
			commentsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'leadershipAdditional') {
			var commentsModalInstance = $uibModal.open({
				templateUrl: './modal/view/leadershipAdditional-modal.html',
				controller: 'LeadershipAdditionalModalController',
				windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal entity-associated-modal',
				// windowClass: 'entity-search-modal-wrapper entity-associated-modal',
				resolve: {
					keyStaff: function () {
						return $scope.entitySearchResult.list['key_staff'];
					},
					hunterData: function () {
						return $scope.entitySearchResult.list['hunterData'];
					}
				}
			});
			commentsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'twitterFollowingFollowers') {
			var commentsModalInstance = $uibModal.open({
				templateUrl: './modal/view/twitterFollowingFollowers.html',
				controller: 'TwitterFollowingFollowersController',
				windowClass: 'custom-modal  bst_modal  c-arrow update-entities-modal entity-twitter-modal',
				// windowClass: 'entity-search-modal-wrapper entity-associated-modal',
				resolve: {
					followings: function () {
						return $scope.entitySearchResult.list['following'];
					},
					followers: function () {
						return $scope.entitySearchResult.list['followers'];
					}
				}
			});
			commentsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'comments') {
			var commentsModalInstance = $uibModal.open({
				templateUrl: './modal/view/comments-entity-modal.html',
				controller: 'CommentsEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal '
			});
			commentsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'products') {
			var productsModalInstance = $uibModal.open({
				templateUrl: './modal/view/products-entity-modal.html',
				controller: 'ProductsEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
			});
			productsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'industrial-security') {
			var industryModalInstance = $uibModal.open({
				templateUrl: './modal/view/industry-entity-modal.html',
				controller: 'IndustryEntityModalController',
				windowClass: 'update-entities-modal related-person-modal industryEntity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					industryModalData: function () {
						return $scope.entitySearchResult.list.risidata;
					}
				}
			});
			industryModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'news-articles') {
			var newsArticlesModalInstance = $uibModal.open({
				templateUrl: './modal/view/news-articles-entity-modal.html',
				controller: 'NewsArticlesEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
			});
			newsArticlesModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'related-organization') {
			var relatedOrganizationModalInstance = $uibModal.open({
				templateUrl: './modal/view/related-organization-entity-modal.html',
				controller: 'RelatedOrganizationEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
				resolve: {
					relatedOrganizationData: function () {
						return $scope.entitySearchResult.list.similar_companies;
					}
				}
			});
			relatedOrganizationModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'realted-person') {
			var realtedPersonModalInstance = $uibModal.open({
				templateUrl: './modal/view/related-person-entity-modal.html',
				controller: 'RealtedPersonEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
				resolve: {
					personsData: function () {
						return {
							entityKeyPersonList: $scope.entitySearchResult.list['key_persons'],
							entityCompanyKeyExecutiveList: $scope.entitySearchResult.list['company_key_executives'],
							entityBoardmemberList: $scope.entitySearchResult.list['company_boardmembers'],
							entityOtherMemberList: $scope.entityOtherMemberList,
							entityCompanyMemebrs: $scope.entitySearchResult.list['has_company_members']
						};
					}
				}
			});
			realtedPersonModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'general') {
			var generalModalInstance = $uibModal.open({
				templateUrl: './modal/view/general-entity-modal.html',
				controller: 'GeneralEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
				resolve: {
					generalData: function () {
						return $scope.entitySearchResult;
					}
				}
			});
			generalModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'news') {
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/news-entity-modal.html',
				controller: 'NewsEntityModalConteoller',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
				resolve: {
					newsData: function () {
						return $scope.entityNewsList;
					}
				}
			});
			newsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'general-info') {
			var newsModalInstance = $uibModal.open({
				// templateUrl: './modal/view/general-info-entity-modal.html',
				templateUrl: 'genralinfoId.html',
				controller: 'GeneralInfoModalConteoller',
				windowClass: 'custom-modal c-arrow bst_modal ',
				resolve: {
					generalDetailList: function () {
						return $scope.companyDetailList;
					}
				}
			});
			newsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'social-media') {
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/social-media-articles-modal.html',
				controller: 'SocialMediaArticlesController',
				windowClass: 'update-entities-modal related-person-modal bst_modal ',
				resolve: {
					searchText: function () {
						return $scope.entitySearchResult.name;
					},
					searchType: function () {
						return 'Company';
					}
				}
			});
			newsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'stocks') {
			var newsModalInstance = $uibModal.open({
				templateUrl: './modal/view/stocks-entity-modal.html',
				controller: 'StocksEntityModalConteoller',
				windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal  entity-stocks-modal',
				backdrop: 'static',
				resolve: {
					stockModalData: function () {
						return $scope.isStockDataFound ? stockModalData : $scope.isStockDataFound;
					},
					yahooStockData: function () {
						return $scope.isYahooStockDataFound ? yahooStockData : $scope.isYahooStockDataFound;
					}
				}
			});
			newsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'followers') {
			var followersModalInstance = $uibModal.open({
				templateUrl: './modal/view/followers-entity-modal.html',
				controller: 'FollowersEntityModalController',
				windowClass: 'update-entities-modal related-person-modal followers-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					followersModalData: function () {
						return $scope.entitySearchResult.list.followers;
					}
				}
			});
			followersModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'following') {
			var followingModalInstance = $uibModal.open({
				templateUrl: './modal/view/following-entity-modal.html',
				controller: 'FollowingEntityModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					followingModalData: function () {
						return $scope.entitySearchResult.list.following;
					}
				}
			});
			followingModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'latest-release-list') {
			var latestReleaseListModalInstance = $uibModal.open({
				templateUrl: './modal/view/latest-release-list-modal.html',
				controller: 'LatestReleaseListModalController',
				windowClass: 'update-entities-modal related-person-modal latestReleases-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					latestReleaseData: function () {
						return $scope.entitySearchResult.list.latestReleases;
					}
				}
			});
			latestReleaseListModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'supplier-black-list') {
			var latestReleaseListModalInstance = $uibModal.open({
				templateUrl: './modal/view/supplier-black-list-entity-modal.html',
				controller: 'SupplierBlackListModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					supplierBlackData: function () {
						return $scope.entitySearchResult.list.supplierblacklist;
					}
				}
			});
			latestReleaseListModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'exclusions-fraud') {
			var latestReleaseListModalInstance = $uibModal.open({
				templateUrl: './modal/view/exclusions-fraud-modal.html',
				controller: 'ExclusionsFraudModalController',
				windowClass: 'update-entities-modal bst_modal  related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					exclusionsFraudData: function () {
						return $scope.entitySearchResult.list.exclusion_fraud;
					}
				}
			});
			latestReleaseListModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'key-relationships') {
			var keyRelationshipsModalInstance = $uibModal.open({
				templateUrl: './modal/view/key-relationships-modal.html',
				controller: 'KeyRelationshipsModalController',
				windowClass: 'bst_modal  update-entities-modal related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					keyRelationshipsData: function () {
						return $scope.entitySearchResult.list.key_relationships;
					}
				}
			});
			keyRelationshipsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'staff-attended-schools') {
			var keyStaffAttendedSchoolsModalInstance = $uibModal.open({
				templateUrl: './modal/view/staff-attended-schools-modal.html',
				controller: 'StaffAttendedSchoolsModalController',
				windowClass: 'update-entities-modal related-person-modal staff-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					keyStaffAttendedSchoolsData: function () {
						return $scope.entitySearchResult.list.key_staff_attended_schools;
					}
				}
			});
			keyStaffAttendedSchoolsModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'bad-buyers-list') {
			var badBuyersListModalInstance = $uibModal.open({
				templateUrl: './modal/view/bad-buyers-list-modal.html',
				controller: 'BadBuyersListModalController',
				windowClass: 'update-entities-modal related-person-modal following-entity-modal bst_modal ',
				backdrop: 'static',
				resolve: {
					badBuyersListData: function () {
						return $scope.entitySearchResult.list.badbuyerslist;
					}
				}
			});
			badBuyersListModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'domains-for-company') {
			var domainCompanyModalInstance = $uibModal.open({
				templateUrl: './modal/view/domains-for-company-modal.html',
				controller: 'DomainsCompanyModalController',
				windowClass: 'update-entities-modal related-person-modal bst_modal  domains-entity-modal',
				backdrop: 'static',
				resolve: {
					domainsCompanyData: function () {
						return $scope.entitySearchResult.list.hunterData;
					}
				}
			});
			domainCompanyModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'scam-website-list') {
			var scamWebsiteListModalInstance = $uibModal.open({
				templateUrl: './modal/view/scam-website-list-modal.html',
				controller: 'ScamWebsiteListModalController',
				windowClass: 'update-entities-modal related-person-modal bst_modal  following-entity-modal',
				backdrop: 'static',
				resolve: {
					scamWebsiteListData: function () {
						return $scope.entitySearchResult.list.blackhatworld;
					}
				}
			});
			scamWebsiteListModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
		if (categoryName === 'scam-report') {
			var scamReportModalInstance = $uibModal.open({
				templateUrl: './modal/view/scam-report-modal.html',
				controller: 'ScamReportModalController',
				windowClass: 'update-entities-modal bst_modal  related-person-modal following-entity-modal',
				backdrop: 'static',
				resolve: {
					scamReportData: function () {
						return $scope.entitySearchResult.list.scam;
					}
				}
			});
			scamReportModalInstance.result.then(function (response) {
			}, function (error) {
			});
		}
	};

	/*
		* @purpose: onClickInfoTabs function @created: 20 oct 2017 @params:
		* type(string) @returns: no @author: sandeep
		*/
	$scope.onClickInfoTabs = function (type) {
		$(".entity-company-wrapper .entity-page .entity-company-content-inner-wrapper .entity-company-content-right").removeClass("d-none")
		$(".entity-company-content-inner-wrapper.entityCompanyNew-inner-wrapper").removeClass("padding-0")
		if (type == 'Duediligence') {
			$("#generateReportMainDiv").css("display", "");
			$("#entitiesSelectedMainDev").css("display", "");
			if($scope.pageloader.treeGraphloader ||  $scope.pageloader.loadingText){
				$("#generateReport").parent().addClass("c-ban");
				$("#generateReport").addClass("pe-none");
			}else{
				$("#generateReport").parent().removeClass("c-ban");
				$("#generateReport").removeClass("pe-none");
			}
		} else {
			setTimeout(function () {
				$scope.$apply(function () {
					$rootScope.showMyEntityClipboard = false;
				});
			}, 0);
			$("#generateReportMainDiv").css("display", "none");
			$("#entitiesSelectedMainDev").css("display", "none");
			$scope.myPopover.close();
		}
		$scope.mainInfoTabType = type;
		var width = 300;
		if (type == 'Overview') {
			$scope.$broadcast('overviewtabOnload');
		}
		if (type == 'Duediligence') {
			$scope.$broadcast('dueDiligenceOnload');
			if (hierarchyData && hierarchyData.length > 0) {
				// $scope.splittedScreeningData = $scope.actualScreeningData.slice(0, 20);
				var currentOPts = jQuery.extend(true, {}, options);
				var currentRefreshData = hierarchyData;
				if ($scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_originalView' || $scope.entitySearchResult.list.currentVLArendered === 'vlaContainer') {
					currentRefreshData = originalHierrachyData;
				}
				var divId = "vlaContainer";
				if ($scope.isCustomAvailable) {
					divId = 'vlaContainer_customView';
					$scope.entitySearchResult.list.currentVLArendered = 'vlaContainer_customView';
					$scope.entitySearchResult.list.graphviewIndex = 1;
					$scope.customChartView = true;
				}
				currentOPts.items = jQuery.extend(true, [], currentRefreshData);
				var linesannotatios = [];
				angular.forEach(currentOPts.items, function (val, key) {
					// var lineann = new primitives.famdiagram.HighlightPathAnnotationConfig();
					if (val.entity_type === 'person') {
						if (val.indirectChilds && val.indirectChilds.length) {
							angular.forEach(val.indirectChilds, function (data, index) {
								var lineann = new primitives.orgdiagram.ConnectorAnnotationConfig();
								lineann.fromItem = data.id;
								lineann.toItem = val.id;
								lineann.label = "<div class='bp-badge' style='width:50px; height:17px;background-color:#388E3C; color: white;line-height:0.9;top:6px;position:relative;'>" + data.indirectPercentage.toFixed(2) + "%</div>";
								lineann.labelSize = new primitives.common.Size(80, 30);
								lineann.offset = 0;
								lineann.connectorShapeType = primitives.common.ConnectorShapeType.OneWay;
								lineann.color = '#388E3C';
								lineann.lineWidth = 2;
								lineann.lineType = primitives.common.LineType.Dashed;
								lineann.selectItems = false;
								lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Offbeat;
								if (data.isDirect) {
									lineann.lineWidth = 1;
									lineann.lineType = primitives.common.LineType.Solid;
									lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Straight;
									lineann.color = "rgb(88, 107, 113)";
								}
								var lineannAlreadyExist = _.find(linesannotatios, {
									'fromItem':  data.id,
									'toItem': val.id
								});
								if(!lineannAlreadyExist){
									linesannotatios.push(lineann);
								}
							});
						}
					}
				});
				currentOPts.annotations = linesannotatios;
				$rootScope.ownershipScope = currentOPts.items;
				filteredApplied = 0;
				//	options.items = hierarchyData;
				$('#' + divId).find('.orgdiagram .custom-scroll-wrapper').remove();
				setTimeout(function () {
					jQuery("#" + divId).famDiagram(currentOPts);
					setTimeout(function () {
						$("#" + divId).children().css("width", "100%");
						$("#" + divId).children().addClass('custom-scroll-wrapper');
						EntityCommonTabService.AppendOrgGraphIcon(divId);
					}, 0)
				}, 2000);
				$scope.pageloader.treeGraphloader = false;
			}
		}
		if (type == 'Leaderships') {
			$scope.$broadcast('leadershipOnload');
		}
		if (type == 'Financial Growth') {
			$scope.$broadcast('financialGrowthOnload');
		}
		if (type == 'Risk Alerts') {
			$scope.$broadcast('riskAlertsOnload');
		}
		if (type == 'Latest News') {
			/* calling network chart */
			$scope.$broadcast('latestNewsOnload');
		}
		if (type == 'Social Media') {
			$scope.$broadcast('socialMediaOnload');
		}
		if (type == 'Medias') {
			$scope.$broadcast('mediasOnload');
		}
		if (type == 'Upload') {
			$scope.$broadcast('formAnalysis');
			$(".entity-company-wrapper .entity-page .entity-company-content-inner-wrapper .entity-company-content-right").addClass("d-none");
			$(".entity-company-content-inner-wrapper.entityCompanyNew-inner-wrapper").addClass("padding-0")
		}
		if (type == 'Autodesk') {
			$scope.$broadcast('threatsIntelligence');
		}
	}

	var docId, caseDocumentsList = [];
	$scope.caseDocuments = [];
	$scope.mediaLoader = false;
	/*
	 * @purpose: download Document @created: 11 nov 2017 @params: docId(string),
	 * fileTitle(string, fileType(string) @return: no @author: swathi
	 */
	$scope.downloadDoc = function (docId, fileTitle, fileType) {
		$scope.mediaLoader = true;
		var params = {
			docId: docId,
			token: $rootScope.ehubObject.token
		};
		EntityApiService.downloadDocument(params).then(function (response) {
			$scope.mediaLoader = false;
			var blob = new Blob([response.data], {
				type: "application/" + fileType,
			});
			saveAs(blob, fileTitle);
			HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title: ' + fileTitle);
		}, function (e) {
			$scope.mediaLoader = false;
			HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + fileTitle);
		});
	};
	/*
	 * @purpose:Show more data in model @created: 8 jan 2018 @params:
	 * data(string) @return: no @author: varsha
	 */
	$window.popUpdata = function (data) {
		var dataModal = $uibModal.open({
			templateUrl: '../scripts/common/modal/views/dataPopUp.modal.html',
			controller: 'DataModalController',
			size: 'xs',
			// backdrop: 'static',
			windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal  data-popup-wrapper',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		dataModal.result.then(function (response) {
		}, function (reject) {
		});
	};
	var id;
	/*
	 * @purpose: open fullscreen modal
	 * @created: 18 may 2018		
	 * @author: swathi
	 */
	$scope.openMaximizeFlowModal = function (Chart) {
		var maximizeModal = $uibModal.open({
			//	templateUrl: './modal/view/flow.maximize.modal.html',
			templateUrl: 'ownershipModal.html',
			controller: 'FlowMaximizeModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow capture-modal ownership-vla bst_modal ',
			keyboard: false,
			resolve: {
				Id: function () {
					return Chart;
				},
				ChartOptions: function () {
					return subsidariesfilteredData;
				},
				entitySearchResult: function () {
					return $scope.entitySearchResult;
				},
				allLevelData: function () {
					return $scope.actualScreeningData
				},
				summaryScreenig: function () {
					return $scope.summaryScreenig;
				},
				scope: function () {
					return $scope
				}
			}
		});
		maximizeModal.result.then(function (response) {
			response.map(function (val) {
				if (!(_.find(subsidaryFilterData, ['identifier', val.identifier]))) {
					subsidaryFilterData.push(val);
					allLevelData.push(val);
				}
			});
			$scope.addEntityfrommodal = false;
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = jQuery.extend(true, [], response);
			var linesannotatios = [];
			angular.forEach(currentOPts.items, function (val, key) {
				// var lineann = new primitives.famdiagram.HighlightPathAnnotationConfig();
				if (val.entity_type === 'person') {
					if (val.indirectChilds && val.indirectChilds.length) {
						angular.forEach(val.indirectChilds, function (data, index) {
							var lineann = new primitives.orgdiagram.ConnectorAnnotationConfig();
							lineann.fromItem = data.id;
							lineann.toItem = val.id;
							lineann.label = "<div class='bp-badge' style='width:50px; height:17px;background-color:#388E3C; color: white;line-height:0.9;top:6px;position:relative;'>" + data.indirectPercentage.toFixed(2) + "%</div>";
							lineann.labelSize = new primitives.common.Size(80, 30);
							lineann.offset = 0;
							lineann.connectorShapeType = primitives.common.ConnectorShapeType.OneWay;
							lineann.color = '#388E3C';
							lineann.lineWidth = 2;
							lineann.lineType = primitives.common.LineType.Dashed;
							lineann.selectItems = false;
							lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Offbeat;
							if (data.isDirect) {
								lineann.lineWidth = 1;
								lineann.lineType = primitives.common.LineType.Solid;
								lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Straight;
								lineann.color = "rgb(88, 107, 113)";
							}
							var lineannAlreadyExist = _.find(linesannotatios, {
								'fromItem':  data.id,
								'toItem': val.id
							});
							if(!lineannAlreadyExist){
								linesannotatios.push(lineann);
							}
						});
					}
				}
			});
			currentOPts.annotations = linesannotatios;
			$('#vlaContainer').find('.orgdiagram .custom-scroll-wrapper').remove();
			setTimeout(function () {
				jQuery("#vlaContainer").famDiagram(currentOPts);
				setTimeout(function () {
					orgChartRemake(currentOPts);
				}, 1000);
			}, 1000);
		}, function (reject) { });
	}
	$scope.openmodal = function (data, type, index) {
		data.source = data.sanction_url ? data.sanction_url : (data.sanction_source ? data.sanction_source : (data["@source-id"] ? data["@source-id"] : ''));
		if (type === 'sanction' && data['sanction_bst:description'] && data['sanction_bst:description'].length > 0) {
			data.country = data["mdaas:RegisteredAddress"] ? (data["mdaas:RegisteredAddress"].country ? data["mdaas:RegisteredAddress"].country : '') : '';
			$scope.openAdverseDirectly(index, data, 'sanction');
		} else if (type === 'sanction' && data["sanction_bst:description"] && data["sanction_bst:description"].length > 0) {
			$scope.openAdverseDirectly(index, data, 'sanction');
		} else if (type === "high_risk_jurisdiction" && data.high_risk_jurisdiction && data.high_risk_jurisdiction.toLowerCase() !== 'low') {
			$scope.openAdverseDirectly(index, data, 'highRisk');
		}
		else if (type === "pepalert" && data.pep_url && data.pep_url.length > 0) {
			$scope.openAdverseDirectly(index, data, 'pep');
		} else if (type === "financeCrime" && data.finance_Crime_url && data.finance_Crime_url.length > 0) {
			$scope.openAdverseDirectly(index, data, 'finance');
		}
		else if (type === "AdverseNews" && data.adverseNews_url.length > 0) {
			$scope.openAdverseDirectly(index, data, 'adverse');
		}
		else if (type === "AdverseNews" && data.adverseNews_url.length === 0 && data.non_negativenews.length > 0) {
			var adversecontent = '';
			angular.forEach(data.non_negativenews, function (val, key) {
				adversecontent = adversecontent + '<tr class= "border-b0"><td><a href="' + val.url + '" target="_blank"> ' + val.title + '</a></td></tr>';
			});
			data.txt = '<table class="table data-custom-table"><tr  class= "border-b0"><td>' + data.name + '</td></tr>' + adversecontent + '</table>';
			// openmodal(data);
			$scope.openAdverseDirectly(index, data, 'adverse');
		}
		else if (type === "AdverseNews" && data.FalseNews.length > 0) {
			$scope.openAdverseDirectly(index, data, 'adverse');
		}
		else if (type === "Birth Date" && data['vcard:bday']) {
			data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Birth Date</td><td>' + data['vcard:bday'] + '</td></tr></tr></table>';
			openmodal(data);
		}
		//window.popUpdata(item, type);
	}
	function openmodal(data) {
		var dataModal = $uibModal.open({
			templateUrl: 'dataPopUp.modal.html',
			controller: 'DataModalController',
			size: 'xs',
			// backdrop: 'static',
			windowClass: 'custom-modal bst_modal  c-arrow update-entities-modal data-popup-wrapper',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		dataModal.result.then(function (response) { }, function (reject) { });
	};
	function calculateCountryRisk(country) {
		var captialCaseCountry = toTitleCase(country);
		var risk;
		if (captialCaseCountry) {
			//	d3.json("./constants/countryrisk.json", function (error, root) {
			risk = _.find(chartsConst.countryRisk, ['COUNTRY', captialCaseCountry.toUpperCase()]);
			if (risk) {
				return risk["FEC/ESR Risk Level"];
			} else {
				return '';
			}
		}
	}
	$scope.calculateCountryRiskForReport = function (country) {
		calculateCountryRisk(country);		
	};
	$scope.openComplianceModal = function (data) {
		var commentsModalInstance = $uibModal.open({
			templateUrl: 'companySourceDetails.html',
			controller: 'CompanySourcedetailsController',
			windowClass: 'custom-modal c-arrow bst_modal entity_sources_modal bst_modal ',
			// windowClass: 'entity-search-modal-wrapper entity-overview-modal',
			resolve: {
				data1: function () {
					return data;
				},
			}
		});
		commentsModalInstance.result.then(function (response) {
		}, function (error) {
		});
	}
	$scope.tooltipdtata = function (item) {
		toolTipData = {
			address: {
				address_line_1: '',
				address_line_2: '',
				locality: ''
			},
			country: '',
			high_risk_jurisdiction: '',
			source: '',
			pep_url: '',
			finance_Crime_url: '',
			adverseNews_url: [],
			FalseNewsurl: [],
			FalseName: "",
			non_negativenews: [],
			'vcard:bday': '',
			'no-screening': item['no-screening']
		};
		toolTipData.country = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].country ? item["mdaas:RegisteredAddress"].country : '') : (item.country ? item.country : '');
		toolTipData.address.address_line_1 = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].streetAddress ? item["mdaas:RegisteredAddress"].streetAddress : '') : '';
		toolTipData.address.locality = item["mdaas:RegisteredAddress"] ? (item["mdaas:RegisteredAddress"].city ? item["mdaas:RegisteredAddress"].city : '') : '';
		toolTipData.name = item.name === undefined ? '' : item.name;
		toolTipData.high_risk_jurisdiction = item.high_risk_jurisdiction ? item.high_risk_jurisdiction.toLowerCase() : '';
		toolTipData.source = item['sanction_bst:description'] ? item['sanction_bst:description'].length : 'Sanctions Not Found';
		toolTipData.pep_url = item.pep_url ? item.pep_url.length : 'Not Found In Pep List';
		toolTipData.finance_Crime_url = item.finance_Crime_url ? item.finance_Crime_url : [];
		toolTipData.adverseNews_url = item.adverseNews_url ? (item.adverseNews_url.length > 0 ? item.adverseNews_url : []) : [];
		toolTipData.FalseNewsurl = item.FalseNews;
		toolTipData.FalseName = item.FalseName;
		toolTipData.non_negativenews = item.non_negativenews ? ((item.non_negativenews.length > 0) ? item.non_negativenews : []) : [];
		toolTipData['vcard:bday'] = item['vcard:bday'] ? item['vcard:bday'] : "";
	}
	$scope.corporatePopup = function (source, type, currentAns) {
		if (source) {
			var data = {};
			if (source == 'OverrideAnswer') {
				data.txt = '<div class="col-sm-12 pad-x0"><div class="primary-section"><p style=""><b>OSNIT Data:</b> ' + type + '</p><p style=""><b>Form Data:</b> ' + currentAns + '</p></div></div>';
			} else {
				if (typeof source === 'string') {
					var sourceArray = source.split();
					var appendcontent = '<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4  style=" " class="text-cream f-14">' + type + '</h4></div>';
					angular.forEach(sourceArray, function (val, index) {
						val = EntityCommonTabService.makeUrlSecureToWork(val);
						appendcontent = appendcontent + '<p><a style="" href="' + val + '" target="_blank">' + val + '</a> </p>';
					});
				} else {
					var appendcontent = '<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4  style=" " class="text-cream f-14">' + type + '</h4></div>';
					angular.forEach(source, function (val, index) {
						if (val.toLowerCase() === 'bst' || val.toLowerCase() === 'corporationwiki') {
							appendcontent = appendcontent + '<p><a style="" href="javascript:void(0);" class = "c-arrow">' + val + '</a> </p>';
						} else {
							val = EntityCommonTabService.makeUrlSecureToWork(val);
							appendcontent = appendcontent + '<p><a style="" href="' + val + '" target="_blank">' + val + '</a> </p>';
						}
					});
				}
				data.txt = appendcontent;
			}
			var dataModal = $uibModal.open({
				templateUrl: '../scripts/common/modal/views/dataPopUp.modal.html',
				controller: 'DataModalController',
				size: 'xs',
				// backdrop: 'static',
				windowClass: 'custom-modal c-arrow bst_modal ',
				resolve: {
					data: function () {
						return data;
					}
				}
			});
			dataModal.result.then(function (response) { }, function (reject) { });
		}
	};
	function toTitleCase(str) {
		return EntityCommonTabService.toTitleCase(str);
	}
	$("body").on("mouseover", ".showTooltip", function () {
		if (toolTipData['no-screening']) {
			return false;
		}
		if (!$(".Screening_new_tooltip").html()) {
			$(".Screening_new_tooltip").css("display", "none");
			return;
		}
		$(".Screening_new_tooltip").css("display", "block");
	}).on("mousemove", ".showTooltip", function (event) {
		if (toolTipData['no-screening']) {
			return false;
		}
		var classlist = $(this).attr("class").split(' ');
		var count = 0;
		if (classlist[1] == "fa-database") {
			$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">No Financial Crime Found</div>');
		} else if (classlist[1] == "fa-street-view") {
			//$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">' + toolTipData.pep_url + '</div>');
			$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.name + '<span class="pull-right text-cream mar-l30">' + toolTipData.pep_url + '</span></h3></div></div>');
		} else if (classlist[1] == "fa-gavel") {
			if (toolTipData.finance_Crime_url && toolTipData.finance_Crime_url.length > 0) {
				var finaceCrimecontent = '';
				angular.forEach(toolTipData.finance_Crime_url, function (val, key) {
					//finaceCrimecontent = finaceCrimecontent+'<p>'+ val.title+'</p>';
					count = count + (val.count ? val.count : 0);
				});
				finaceCrimecontent = '';//Empty the tooltipdata 
				$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.name + '<span class="pull-right text-cream mar-l30">' + count + '</span></h3></div></div>');//<div class="col-sm-12"><h4> Financial Crime</h4> ' +finaceCrimecontent+ '</div><div class="secondary-section row" style=""></div>
			} else {
				$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">Finance Crime Not Found</div>');
			}
		} else if (classlist[1] == "fa-ban") {
			//$(".Screening_new_tooltip").html('<div class="row mar-x0" style=""><div class="col-sm-12 pad-x0"><div style="" class="top-heading"><h3 class="text-cream f-14" style=""><span class="heading-icon"style=""></span>' + toolTipData.name + '</h3></div><div class="secondary-section details-tooltip row" style=""><div class="col-sm-6"><p style=""><span class="fa fa-address-book pull-left"></span>Address <span style=" ">:' + toolTipData.address.address_line_1 + '</span></p> <p style="" ><span class="fa fa-globe pull-left"></span>Country <span style=" ">:' + toolTipData.country + '</span></p> </div><div class="col-sm-6"><p style=""><span class="fa fa-map-marker pull-left"></span>Locality<span style="">:' + toolTipData.address.locality + '</span> </p><p style="" ><span class="fa fa-link pull-left"></span>Source <span >:' + toolTipData.source + '</span> </p> </div></div></div></div></div>')
			$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.name + '<span class="pull-right text-cream mar-l30">' + toolTipData.source + '</span></h3></div></div>');
		} else if (classlist[1] == "fa-globe") {
			if (toolTipData.country || toolTipData.high_risk_jurisdiction) {
				$(".Screening_new_tooltip").html('	<div class="row mar-x0" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style=""><span class="heading-icon" style=""></span>' + toolTipData.name + '</h3></div><div class="secondary-section row" style=""><div class="col-sm-6"><p style="">Jurisdiction<span class="">:' + count + '</span> </p></div><div class="col-sm-6"> <p style="" >Country  <span class="">:' + toolTipData.country + '</span></p></div></div></div></div></div>')
			} else {
				$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase"> Risk Jurisdiction Not Found</div>');
			}
		} else if (classlist[1] == "fa-newspaper-o") {
			if (toolTipData.adverseNews_url && toolTipData.adverseNews_url.length > 0) {
				var adversecontent = '';
				var count = 0;
				angular.forEach(toolTipData.adverseNews_url, function (val, key) {
					count = count + (val.count ? val.count : 0);
					//adversecontent = adversecontent+'<p>'+ val.title+'</p>';
				});
				adversecontent = '';//Empty the tooltipdata 
				$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.name + '<span class="pull-right text-cream mar-l30">' + adversecontent + '</span></h3></div></div>');//<div class="col-sm-12"><h4>Adverse News</h4> ' +adversecontent+ '</div><div class="secondary-section row" style=""></div>
			}
			else if (toolTipData.FalseNewsurl && toolTipData.FalseNewsurl.length > 0) {
				var adversecontent = '';
				angular.forEach(toolTipData.FalseNewsurl, function (val, key) {
					adversecontent = adversecontent + '<p>' + val.title + '</p>';
				});
				$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.FalseName + '</h3></div><div class="secondary-section row" style=""><div class="col-sm-12"><h4>News</h4> ' + adversecontent + '</div></div></div>');
			} else if (toolTipData.adverseNews_url && toolTipData.adverseNews_url.length === 0 && toolTipData.non_negativenews && toolTipData.non_negativenews.length > 0) {
				var adversecontent = '';
				angular.forEach(toolTipData.non_negativenews, function (val, key) {
					adversecontent = adversecontent + '<p>' + val.title + '</p>';
				});
				$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + toolTipData.name + '</h3></div><div class="secondary-section row" style=""><div class="col-sm-12"><h4>News</h4> ' + adversecontent + '</div></div></div>');
			}
			else {
				$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase">No News Found</div>');
			}
		}
		else if (classlist[1] == "fa-birthday-cake") {
			if (toolTipData['vcard:bday']) {
				$(".Screening_new_tooltip").html('	<div class="row mar-x0" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style=""><span class="heading-icon" style=""></span>' + toolTipData.name + '</h3></div><div class="secondary-section row" style=""><div class="col-sm-12"><p style="">Birth Date<span class="">:' + toolTipData['vcard:bday'] + '</span> </p></div></div></div></div></div>')
			}
		} else if (classlist[1] == "fa-na") {
			$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + toolTipData.name + '</h3></div><div class="mar-t15 text-uppercase"> Date Not Found</div>');
		}
		else if (classlist[1] == "selectIndustryToolTip") {
			$(".Screening_new_tooltip").css("min-width", "200px");
			$(".Screening_new_tooltip").html('<div><span class="tool-tip-heading mar-b0 text-gray-ta text-uppercase">' + $scope.industriesName + '</span></div><div><b>NUMBER OF INCIDENTS : </b>' + $scope.incidentsByIndustryCount + '</div>');
		}
		else if (classlist[1] == "topConutriesAndTechToolTip") {
			$(".Screening_new_tooltip").css("min-width", "200px");
			$(".Screening_new_tooltip").html('<div><span class="tool-tip-heading mar-b0 text-gray-ta text-uppercase">' + $scope.topCountriesName + '</span></div><div><b>NUMBER OF INCIDENTS : </b>' + $scope.incidentsByTopCountriesNameCount + '</div>');
		}
		var p = $(".entity-page")
		var position = p.offset();
		var windowWidth = window.innerWidth;
		var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		var cursor = event.pageX;
		if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX - 50 - $(".Screening_new_tooltip").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX) - 20 + "px");
		}
		return $(".Screening_new_tooltip").css("top", (event.pageY) + 20 + "px")
	}).on("mouseout", ".showTooltip", function () {
		$(".Screening_new_tooltip").css("display", "none");
	});
	$scope.toolTipforSummary = {
		tooltipCount: 0,
		tooltiptitle: ''
	};
	$scope.tooltipSummaryCountFunction = function (title, count) {
		$scope.toolTipforSummary.tooltiptitle = title;
		$scope.toolTipforSummary.tooltipCount = count;
	}
	$("body").on("mouseover", ".showTooltipforSummary", function () {
		$(".Screening_new_tooltip").css("display", "block");
		$(".Screening_new_tooltip").html('<div class="row mar-x0" style="max-width :500px" ><div class="col-sm-12 pad-x0"><div class="top-heading" style=""><h3 class="tool-tip-heading text-cream" style="">' + $scope.toolTipforSummary.tooltiptitle + '<span class="pull-right text-cream mar-l30">' + $scope.toolTipforSummary.tooltipCount + '</span></h3></div></div>');
	}).on("mousemove", ".showTooltipforSummary", function (event) {
		var classlist = $(this).attr("class").split(' ');
		var p = $(".entity-page")
		var position = p.offset();
		var windowWidth = window.innerWidth;
		var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		var cursor = event.pageX;
		if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX - 50 - $(".Screening_new_tooltip").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX) - 20 + "px");
		}
		return $(".Screening_new_tooltip").css("top", (event.pageY) + 20 + "px")
	}).on("mouseout", ".showTooltipforSummary", function () {
		$(".Screening_new_tooltip").css("display", "none");
		$scope.toolTipforSummary = {
			tooltipCount: 0,
			tooltiptitle: ''
		};
	});
	$scope.mouseover = function (sourceLink, classstype, linktype) {
		if (!linktype) {
			linktype = "Source not Found";
		}
		$scope.myStyle2 = {
			cursor: 'pointer'
		};
		onHoverShow(sourceLink, classstype, linktype);
	}
	function onHoverShow(sourceLink, classstype, linktype) {
		var tempArray = [];
		if (typeof sourceLink === "string") {
			tempArray = sourceLink.split();
			$scope.newLink = tempArray;
		} else {
			$scope.newLink = sourceLink;
		}
		var appendcontent = '<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4  style=" " class="text-cream f-14">' + linktype + '</h4></div>';
		angular.forEach($scope.newLink, function (val, index) {
			if (val && val.toLowerCase() === 'bst') {
				appendcontent = appendcontent + '<a style=" text-transform : uppercase" >' + val + '</a>';
			} else {
				val = EntityCommonTabService.makeUrlSecureToWork(val);
				appendcontent = appendcontent + '<a style="">' + val + '</a>';
			}
		});
		appendcontent = appendcontent + '</div></div><div class="secondary-section"></div></div></div>';
		$("body")
			.on("mouseover", '.' + classstype, function () {
				$(".Screening_new_tooltip").css("display", "block");
			})
			.on("mousemove", '.' + classstype, function (event) {
				var classlist = $(this).attr("class").split(' ');
				if (classstype == "associatedDocTooltip") {
					$(".Screening_new_tooltip")
						.html(appendcontent);
				} else {
					$(".Screening_new_tooltip")
						.html(appendcontent);
				}
				var p = $(".entity-page")
				var position = p.offset();
				var windowWidth = window.innerWidth;
				var tooltipWidth = $(".Screening_new_tooltip").width() + 50
				var cursor = event.pageX;
				if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
					var element = document.getElementsByClassName("Screening_new_tooltip");
					for (var i = 0; i < element.length; i++) {
						element[i].classList.remove("tooltip-left");
						element[i].classList.add("tooltip-right");
					}
					$(".Screening_new_tooltip").css("left", (event.pageX - 30 - $(".Screening_new_tooltip").width()) + "px");
				} else {
					var element = document.getElementsByClassName("Screening_new_tooltip");
					for (var i = 0; i < element.length; i++) {
						element[i].classList.remove("tooltip-right");
						element[i].classList.add("tooltip-left");
					}
					$(".Screening_new_tooltip").css("left", (event.pageX) + "px");
				}
				return $(".Screening_new_tooltip").css("top", event.pageY + "px")
			}).on("mouseout", '.' + classstype, function () {
				$(".Screening_new_tooltip").css("display", "none");
			})
	}
	/* @purpose:  Compilance Company Details
* 			 
* @created: 07 may 2018
* @params: type(string)
* @returns: no
* @author: Ram Singh */
	function complianeCompanyDetails(data, json) {
		if (!json) {
			if (!_.isEmpty(data)) {
				if (data && data['lei:legalForm'] && !_.isEmpty(data['lei:legalForm']) && data['lei:legalForm'].value) {
					data['lei:legalForm'].value = $scope.isJson(data['lei:legalForm'].value) ? JSON.parse(data['lei:legalForm'].value) : data['lei:legalForm'].value;
					data['lei:legalForm'].value = data['lei:legalForm'] && data['lei:legalForm'] && data['lei:legalForm'].value && data['lei:legalForm'].value.hasOwnProperty('label') ? data['lei:legalForm'].value.label : (data['lei:legalForm'].value ? data['lei:legalForm'].value : '');
				}
				// - handling duplicates for primary and secondary sources 
				angular.forEach(data, function (val, key) {
					val.primarySource = _.compact(_.uniq(val.primarySource));
					val.secondarySource = _.compact(_.uniq(val.secondarySource));
				});
				$scope.ceriSearchResultObject = data;
				if ($scope.ceriSearchResultObject['bst:stock_info']) {
					var bst_Stock_value = $scope.ceriSearchResultObject['bst:stock_info'] ? $scope.ceriSearchResultObject['bst:stock_info'].value ? JSON.parse($scope.ceriSearchResultObject['bst:stock_info'].value) : $scope.ceriSearchResultObject['bst:stock_info'].value : '';
					var bst_Stock_valueRef = $scope.ceriSearchResultObject['bst:stock_info'] ? $scope.ceriSearchResultObject['bst:stock_info'].valueRef ? JSON.parse($scope.ceriSearchResultObject['bst:stock_info'].valueRef) : $scope.ceriSearchResultObject['bst:stock_info'].valueRef : '';
					$scope.ceriSearchResultObject['bst:stock_info'].value = bst_Stock_value;
					$scope.ceriSearchResultObject['bst:stock_info'].value = bst_Stock_valueRef;
				}
				if ($scope.ceriSearchResultObject['bst:aka']) {
					if ($scope.ceriSearchResultObject['bst:aka'].valueRef.charAt(0) == "[") {
						$scope.ceriSearchResultObject['bst:aka'].valueRef = JSON.parse($scope.ceriSearchResultObject['bst:aka'].valueRef);
					} else {
						var generateValidArry = [];
						generateValidArry.push($scope.ceriSearchResultObject['bst:aka'].valueRef);
						$scope.ceriSearchResultObject['bst:aka'].valueRef = generateValidArry;
						$scope.ceriSearchResultObject['bst:aka'].value = JSON.stringify(generateValidArry);
					}
				}
				if ($scope.ceriSearchResultObject['bst:description']) {
					$scope.companyInfo.texts = $scope.ceriSearchResultObject['bst:description'].value ? $scope.ceriSearchResultObject['bst:description'].value : '';
				}
				if ((!$scope.ceriSearchResultObject['isIncorporatedIn'] || _.isEmpty($scope.ceriSearchResultObject['isIncorporatedIn'])) || ($scope.ceriSearchResultObject['isIncorporatedIn'] && !$scope.ceriSearchResultObject['isIncorporatedIn'].value)) {
					$scope.ceriSearchResultObject['isIncorporatedIn'] = $scope.ceriSearchResultObject['hasLatestOrganizationFoundedDate'] && $scope.ceriSearchResultObject['hasLatestOrganizationFoundedDate'].value ? $scope.ceriSearchResultObject['hasLatestOrganizationFoundedDate'] : {};
				}
				$scope.ceriSearchResultObject.flag = $scope.ceriSearchResultObject['isDomiciledIn'] ? ($scope.ceriSearchResultObject['isDomiciledIn'].value ? $scope.ceriSearchResultObject['isDomiciledIn'].value.toLowerCase() : '') : '';
				var aliasval = ($scope.ceriSearchResultObject['bst:aka'] && $scope.ceriSearchResultObject['bst:aka'].value) ? ($scope.isJson($scope.ceriSearchResultObject['bst:aka'].value) ? JSON.parse($scope.ceriSearchResultObject['bst:aka'].value) : $scope.ceriSearchResultObject['bst:aka'].value) : '';
				var alias_name = aliasval;
				if (alias_name) {
					$scope.ceriSearchResultObject['bst:aka'].value = alias_name;
				}
			} else {
				//	$scope.entitySearchResult.is_data_not_found.is_complianceDetails = true;
			}
			$scope.pageloader.companyDetailsloader = false;
		}
	}
	$scope.addOfficerScreening = function () {
		// if($scope.newOfficer.ScreeeningModal === 'Company'){
		$scope.pageloader.screeningLoader = true;
		if ($scope.newOfficer.CompanyName || $scope.newOfficer.firstName) {
			var query = $scope.newOfficer.ScreeeningModal === 'organization' ? $scope.newOfficer.CompanyName : ($scope.newOfficer.firstName + '' + ($scope.newOfficer.lastName !== '' ? ' ' + $scope.newOfficer.lastName : ''));
			var queryCompany = {
				name: query,
				website: $scope.newOfficer.website,
				// country:$scope.countryJurisdiction.jurisdictionName,
				entityType: $scope.newOfficer.ScreeeningModal,
				dob: $scope.newOfficer.dateOfBirth ? $scope.newOfficer.dateOfBirth : ''
			}
			if ($scope.countryJurisdiction && $scope.countryJurisdiction.jurisdictionName) {
				queryCompany.country = $scope.countryJurisdiction.jurisdictionName;
			}
			var entity_identifier = Math.random().toString(36).substring(5);
			var classsification = [];
			if ($scope.newOfficer.ScreeeningModal === 'organization' && $scope.screeningModelClassification && $scope.screeningModelClassification.screeningCompanyModel) {
				classsification = $scope.screeningModelClassification.screeningCompanyModel.map(function (val) { return val.label });
			} else if ($scope.screeningModelClassification && $scope.screeningModelClassification.screeningPersonModel.length) {
				classsification = $scope.screeningModelClassification.screeningPersonModel.map(function (val) { return val.label });
			}
			var addScreeningObject = {
				"mdaas:RegisteredAddress": '',
				"name": query,
				"sanction_bst:description": [],
				"listId": '',
				"@source-id": '',
				"high_risk_jurisdiction": '',
				"pep_url": [],
				"finance_Crime_url": [],
				"adverseNews_url": [],
				"news": [],
				"FalseNews": [],
				"non_negativenews": [],
				"MainCompany": '',
				"hasURL": $scope.newOfficer.website ? $scope.newOfficer.website : '',
				// "vcard:bday": $scope.newOfficer.dateOfBirth ? $scope.newOfficer.dateOfBirth : '',
				"date_of_birth": $scope.newOfficer.dateOfBirth ? $scope.newOfficer.dateOfBirth : '',
				"@identifier": entity_identifier,
				"identifier": entity_identifier,
				"officer_role": $scope.screeningModelClassification.screeningPersonRoleModel && $scope.screeningModelClassification.screeningPersonRoleModel.label ? $scope.screeningModelClassification.screeningPersonRoleModel.label : '',
				"officer_roles": $scope.screeningModelClassification.screeningPersonRoleModel && $scope.screeningModelClassification.screeningPersonRoleModel.label ? [$scope.screeningModelClassification.screeningPersonRoleModel.label] : [],
				'no-screening': true,
				"jurisdiction": $scope.countryJurisdiction && $scope.countryJurisdiction.jurisdictionName ? $scope.countryJurisdiction.jurisdictionName : '',
				"country": $scope.countryJurisdiction && $scope.countryJurisdiction.jurisdictionOriginalName ? $scope.countryJurisdiction.jurisdictionOriginalName : '',
				'showdeleteIcon': true,
				"showspinner": false,
				"screeningresultsloader": false,
				"isChecked": false,
				"entity_type": $scope.newOfficer.ScreeeningModal,
				"classification": classsification ? classsification : [],
				"Ubo_ibo": 'officers',
				"officerIdentifier": '',
				"highCredibilitySource": ''
			}
			$scope.screeningData.push(addScreeningObject);
			$scope.actualScreeningData.push(addScreeningObject);
			$scope.pageloader.screeningLoader = false;
			var requestBodyOfficer = {
				"classification": classsification ? JSON.stringify(classsification) : [],
				"dateOfBirth": $scope.newOfficer.dateOfBirth ? $scope.newOfficer.dateOfBirth : '',
				"from": "",
				"mainIdentifier": ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
				"officerCountry": $scope.countryJurisdiction && $scope.countryJurisdiction.jurisdictionOriginalName ? $scope.countryJurisdiction.jurisdictionOriginalName : '',
				// "officerId": "string",
				"officerName": query,
				"officerRole": $scope.screeningModelClassification.screeningPersonRoleModel && $scope.screeningModelClassification.screeningPersonRoleModel.label ? $scope.screeningModelClassification.screeningPersonRoleModel.label : '',
				"source": "",
				"sourceUrl": "",
				"totalPercentage": ""
			}
			EntityApiService.saveUpdateEntityOfficer(requestBodyOfficer).then(function (response) {
				$scope.selectEntitiesForscreening(addScreeningObject);
				$scope.getCustomSelectedScreenig();
			})
			$scope.closeOfficermodalInstance();
		} else {
			$scope.newOfficer.requiredComapnyname = true;
			setTimeout(function () {
				$scope.newOfficer.requiredComapnyname = false;
			}, 1500);
		}
		if ($scope.newOfficer.CompanyName || $scope.newOfficer.firstName) {
			$scope.closeOfficermodalInstance();
		}
	}
	$scope.entityScreeningTabchange = function (tab) {
		$scope.newOfficer.ScreeeningModal = tab;
		$scope.countryJurisdiction = {};
		$scope.newOfficer = {
			firstName: '',
			lastName: '',
			country: '',
			dob: '',
			ScreeeningModal: tab,
			CompanyName: '',
			website: '',
			requiredComapnyname: false,
			requiredPersonname: false,
			dateOfBirth: ''
		};
		$scope.screeningModelClassification.screeningPersonListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }];
		$scope.screeningModelClassification.screeningCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.screeningModelClassification.screeningPersonRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 11, label: "Chairman" }];
		$scope.personRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.companyRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.editModelClassification.editCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.example5customTexts = { buttonDefaultText: 'Classsification' };
		$scope.screeningModelClassification.screeningPersonModel = [];
		$scope.screeningModelClassification.screeningCompanyModel = [];
	}
	/* @purpose:add the screening of company and officers to the main comoany* 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	function addScreeningToMainCompany(cumluativeScreening, index, hirreacheyIndex, subsidaries) {
		if ($scope.actualScreeningData && $scope.actualScreeningData.length > 0) {
			if ($scope.actualScreeningData[index].adverseNews_url) {
				hierarchyData[hirreacheyIndex].adverseNews_url = hierarchyData[hirreacheyIndex].adverseNews_url ? hierarchyData[hirreacheyIndex].adverseNews_url.concat(cumluativeScreening.totalCompanyAdverseNews) : cumluativeScreening.totalCompanyAdverseNews;
				if (subsidaries && subsidaries !== 'officers' && subsidaries !== 'subsidaries') {
					$scope.actualScreeningData[index].adverseNews_url = $scope.actualScreeningData[index].adverseNews_url.concat(cumluativeScreening.totalCompanyAdverseNews);
				}
				if (index === 0) {
					mainofficersNews = mainofficersNews.concat(cumluativeScreening.totalCompanyAdverseNews);
				}
			} else {
				hierarchyData[hirreacheyIndex].adverseNews_url = cumluativeScreening.totalCompanyAdverseNews;
				if (subsidaries && subsidaries !== 'officers' && subsidaries !== 'subsidaries') {
					$scope.actualScreeningData[index].adverseNews_url = cumluativeScreening.totalCompanyAdverseNews;
				}
			}
			if ($scope.actualScreeningData[index].finance_Crime_url) { //checking for key defined
				hierarchyData[hirreacheyIndex].finance_Crime_url = hierarchyData[hirreacheyIndex].finance_Crime_url.concat(cumluativeScreening.totalCompanyFinancialNews);
				if (hierarchyData[hirreacheyIndex].finance_Crime_url && subsidaries && subsidaries !== 'officers' && subsidaries !== 'subsidaries') {
					$scope.actualScreeningData[index].finance_Crime_url = $scope.actualScreeningData[index].finance_Crime_url.concat(cumluativeScreening.totalCompanyFinancialNews);
				} else {
					hierarchyData[hirreacheyIndex].finance_Crime_url = cumluativeScreening.totalCompanyFinancialNews;
				}
			} else { //else key is defined and assigned
				hierarchyData[hirreacheyIndex].finance_Crime_url = cumluativeScreening.totalCompanyFinancialNews;
				if (subsidaries && subsidaries !== 'officers' && subsidaries !== 'subsidaries') {
					$scope.actualScreeningData[index].finance_Crime_url = cumluativeScreening.totalCompanyFinancialNews;
				}
			}
		}
	}//function ends
	$scope.Rolechecklist = {
		all_checkedRole: true,
		singlearray: []
	};
	/* @purpose:select all the roles 
	*	 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.checkallroles = function (all_checkedRole) {
		$scope.Rolechecklist.singlearray.forEach(function (val, index, self) {
			self[index] = all_checkedRole;
		});
		intialSelectedRoles.forEach(function (val, index, self) {
			self[index].value = all_checkedRole;
		});
	}//function ends
	/* @purpose:set to default state
	*	 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.clearallrole = function () {
		$scope.Rolechecklist.singlearray.forEach(function (val, index, self) {
			self[index] = true;
		});
		intialSelectedRoles.forEach(function (val, index, self) {
			self[index].value = true;
		});
		$scope.Rolechecklist.all_checkedRole = true;
	}//function ends
	/* @purpose:Single roles selected
	*	 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.singleroleselection = function (name,object,event) {
		event.preventDefault();
		event.stopPropagation();
		var index = object.pos;
		$scope.SelectedRolesfilter.push(name)
		intialSelectedRoles[index].value = (!intialSelectedRoles[index].value);
		//$scope.Rolechecklist.singlearray[index] = (!$scope.Rolechecklist.singlearray[index]);
		$scope.unique_officer_roles_array[index].value = !$scope.unique_officer_roles_array[index].value;
		checkAllroleSelected(intialSelectedRoles);
	}
	/* @purpose:Apply the Role filter
	*	 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.roleselection = function () {
		var selectedOfficerData =[];
		var companyEntites =[];
		var officerSelected = $scope.unique_officer_roles_array.filter((val)=>val.value);
		officerSelected.forEach((role) => {
			var officerData = $scope.actualScreeningData.filter((val) => val.officer_role === role.name);
			selectedOfficerData = selectedOfficerData.concat(officerData);
		});
		companyEntites =  $scope.actualScreeningData.filter((val) => (val.entity_id == "orgChartParentEntity" || val.entity_id ==="orgChartmainEntity" || val.entity_id ==="orgChartsubEntity" || val.entity_id ==="orgChartParentEntity"));
		selectedOfficerData =  companyEntites.concat(selectedOfficerData);
		$scope.screeningData = $.extend(true, [], selectedOfficerData);
		// var officerUnSelected = [];
		// $scope.unique_officer_roles_array = $.extend(true, [], intialSelectedRoles);
		// var FilteredData = [];
		// var currentscreeningData = $.extend(true, [], $scope.actualScreeningData);
		// if (!$scope.Rolechecklist.all_checkedRole) {//if all is not selected
		// 	for (var rolelength = 0; rolelength < $scope.unique_officer_roles_array.length; rolelength++) {
		// 		if ($scope.unique_officer_roles_array[rolelength].value) { //select officers which are selected 
		// 			$scope.actualScreeningData.forEach(function (val, index) {
		// 				if (val.officer_role && val.officer_role.length > 0 && (val.officer_role.indexOf($scope.unique_officer_roles_array[rolelength].name) !== -1) && (!(_.find(FilteredData, ['name', val.name])))) {
		// 					FilteredData.push(val);
		// 				}
		// 			});
		// 		} //if ends
		// 	} //for loop ends
		// 	$scope.screeningData = $.extend(true, [], FilteredData);
		// } else {// if all selected
		// 	$scope.screeningData = $.extend(true, [], $scope.actualScreeningData);
		// }
		// $('body').click();
		// $scope.screeningtoggle();
		// mainofficersNews = [];
		// if ($scope.screeningData.length > 0) {
		// 	$scope.screeningData.forEach(function (val) {
		// 		if (val.adverseNews_url) {
		// 			mainofficersNews = mainofficersNews.concat(val.adverseNews_url)
		// 		}
		// 	});
		// }
		// var allnews = mainAdverseNews.concat(mainofficersNews);
		// $scope.complianceRightAdverseNews = _.uniqBy(allnews, 'title');
	}//function ends
	/* @purpose:screen toggle open on click of filter icon
	*	 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.screeningtoggle = function () {
		$scope.Rolechecklist.singlearray = $scope.screeningData.map(function (val) {
			if(val.officer_role){
				return val.officer_role
			}
		}).filter((val)=>val);
		setTimeout(function () {
			$("body .scroll-list").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 0);//set the scroll on click after the roles are repeated
		$('body').click();
		checkAllroleSelected($scope.unique_officer_roles_array);
	} //function ends
	/* @purpose:screening Role filter check all the checckboxes are checcked 
	 * 			 
	 * @created: 07 may 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function checkAllroleSelected(array) {
		$scope.Rolechecklist.all_checkedRole = array.every(function (element, index, array) {
			return element.value === true;
		}); //if all the roles are selected then aLL option is selected and viceversa
	} //function ends
	/* @purpose:website shown in the top header  from multi-source API
		* 			 
			* @created: 07 may 2018
			* @params: type(string)
			* @returns: no
			* @author: Ram Singh */
	function topHeaderWebsite(data) {
		var top_headeWebsite = (data && data.hasURL && data.hasURL.value) ? data.hasURL.value : '';
		if (Array.isArray(top_headeWebsite)) {
			$scope.entitySearchResult.list.topHeaderObject.hasURL = top_headeWebsite[0];
			$scope.entitySearchResult.list.topHeaderObject.hasURL = 'http://' + $scope.entitySearchResult.list.topHeaderObject.hasURL.replace(/(^\w+:|^)\/\//, '');
		} else if ((typeof top_headeWebsite) === "string") {
			$scope.entitySearchResult.list.topHeaderObject.hasURL = top_headeWebsite;
			$scope.entitySearchResult.list.topHeaderObject.hasURL = 'http://' + $scope.entitySearchResult.list.topHeaderObject.hasURL.replace(/(^\w+:|^)\/\//, '');
		}
	}
	$scope.ConflicttooltipData = function (sourceLink, classstype, linktype, schema) {
		$scope.newLink = sourceLink;
		var appendcontent = '<div class="col-sm-12 pad-x0"><div class="primary-section"><div class = "custom-heading" style="">  <h4  style=" " class="text-cream f-14">' + linktype + '</h4></div>';
		angular.forEach($scope.newLink, function (val, index) {
			var currval1 = '';
			var currval2 = '';
			if (schema == 'lei:legalForm') {
				currval1 = $scope.isJson(val.value1) ? JSON.parse(val.value1).label : val.value1;
				currval2 = $scope.isJson(val.value2) ? JSON.parse(val.value2).label : val.value2;
			}
			else if (schema == 'bst:stock_info') {
				currval1 = $scope.isJson(val.value1) ? JSON.parse(val.value1).main_exchange : val.value1;
				currval2 = $scope.isJson(val.value2) ? JSON.parse(val.value2).main_exchange : val.value2;
			} else if (schema == "Registed Legal Type Sources") {
				currval1 = $scope.isJson(val.value1) ? JSON.parse(val.value1).label : val.value1.toString();
				currval2 = $scope.isJson(val.value2) ? JSON.parse(val.value2).label : val.value2.toString();
			}
			else {
				currval1 = $scope.isJson(val.value1) ? JSON.parse(val.value1).toString() : val.value1.toString();
				currval2 = $scope.isJson(val.value2) ? JSON.parse(val.value2).toString() : val.value2.toString();
			}
			if (index === 0) {
				appendcontent = appendcontent + '<p class="" ><span  class="text-blue1 bold-anchor d-block f-14 pad-y5 mar-b0">' + getURLshort(val.source1) + '</span><a class="">' + (currval1) + '</a></p><p class=""><a class="text-blue1">' + getURLshort(val.source2) + '</a><a style="">' + (currval2) + '</a></p>';
			} else {
				appendcontent = appendcontent + '<p class=""><a class="text-blue1">' + getURLshort(val.source2) + '</a><a style="">' + (currval2) + '</a></p>';
			}
		});
		appendcontent = appendcontent + '</div></div><div class="secondary-section"></div></div></div>';
		$("body")
			.on("mouseover", '.' + classstype, function () {
				$(".Screening_new_tooltip").css("display", "block");
			})
			.on("mousemove", '.' + classstype, function (event) {
				var classlist = $(this).attr("class").split(' ');
				if (classstype == "associatedDocTooltip") {
					$(".Screening_new_tooltip")
						.html(appendcontent);
				} else {
					$(".Screening_new_tooltip")
						.html(appendcontent);
				}
				if (classstype !== "companyInfoTooltip") {
					tooltipHandlingPosition("entity-page", event)
				} else {
					tooltipHandlingPosition("company-details", event)
					$(".Screening_new_tooltip").css("max-width", 1000 + "px");
				}
				return $(".Screening_new_tooltip").css("top", event.pageY + 20 + "px")
			}).on("mouseout", '.' + classstype, function () {
				$(".Screening_new_tooltip").css("display", "none");
			})
	}
	function tooltipHandlingPosition(classtype, event) {
		var p = $("." + classtype)
		var position = p.offset();
		var tooltipWidth = $(".Screening_new_tooltip").width() + 50
		if ((position.left < event.pageX) && (event.pageX > tooltipWidth)) {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-left");
				element[i].classList.add("tooltip-right");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX - 30 - $(".Screening_new_tooltip").width()) + "px");
		} else {
			var element = document.getElementsByClassName("Screening_new_tooltip");
			for (var i = 0; i < element.length; i++) {
				element[i].classList.remove("tooltip-right");
				element[i].classList.add("tooltip-left");
			}
			$(".Screening_new_tooltip").css("left", (event.pageX) + "px");
		}
	}
	/* @purpose:  Company Management Data
	* 			 
	* @created: 07 may 2018
	* @params: type(string)
	* @returns: no
	* @author: Ram Singh */
	$scope.conflictPopUpmodal = function (source, type, index, schema, title) {
		var data = {
			source: source,
			type: type,
			index: index,
			schema: schema,
			title: title
		};
		if (!data.source) {
			return;
		}
		$scope.pdfLoader = true;
		var dataModal = $uibModal.open({
			templateUrl: 'entityComplianceModal.html',
			controller: 'EntityDataModalController',
			scope: $scope,
			size: 'xs',
			// backdrop: 'static',
			windowClass: 'custom-modal bst_modal  c-arrow bst_modal modal_md full_space_modal',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		dataModal.result.then(function (response) {
			$scope.pdfLoader = false;
		}, function (reject) {
			$scope.pdfLoader = false;
		});
	}
	$("body").on("click", ".panel-group .panel-default .panel-heading", function () {
		$(".entity-overRiding-modal").mCustomScrollbar({
			axis: "y",
			theme: "minimal"
		});
	});
	/* @purpose:  Ownership data Compliance Data
	 * 			 
	 * @created: 07 may 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	var curr_time_to_20min = null;
	function complianceNewOwnership(totalData, Pid, firsttime) {
		var orglink = totalData.org_structure_link ? totalData.org_structure_link : '';
		//	if(orglink){
		$scope.entitySearchResult.org_structure_link = orglink;
		//$scope.entitySearchResult["@identifier"] = totalData["@identifier"];
		var param = {
			identifier: $scope.entitySearchResult["@identifier"],
			url: {
				"url": orglink
			},
			numnberoflevel: 5,
			lowRange: 25,
			highRange: 100,
			organisationName: $scope.ceriSearchResultObject && $scope.ceriSearchResultObject['vcard:organization-name'] && $scope.ceriSearchResultObject['vcard:organization-name'].value ? $scope.ceriSearchResultObject['vcard:organization-name'].value : totalData['vcard:organization-name'] ? totalData['vcard:organization-name'] : '',
			juridiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : $scope.ceriSearchResultObject && $scope.ceriSearchResultObject['isDomiciledIn'] && $scope.ceriSearchResultObject['isDomiciledIn'].value ? $scope.ceriSearchResultObject['isDomiciledIn'].value : totalData.isDomiciledIn ? totalData.isDomiciledIn : '',
			noOfSubsidiaries: 5,
			isSubsidiariesRequired: $scope.showSubsidaries,
			Start_date: $scope.filteredChart.toyear,
			End_date: $scope.filteredChart.fromyear
		};
		EntityApiService.getOwnershipPath(param).then(function (response) {
			if (response && response.data && response.data.path) {
				if (firsttime) {
					curr_time_to_20min = new Date();
					curr_time_to_20min.setMinutes(curr_time_to_20min.getMinutes() + 20);
					updateOwnershipRecurssive(response.data.path, chartTimeIntreval);
				} else {
					$timeout(function () {
						// tag cloud variables initializations for person
						updateOwnershipRecurssive(response.data.path, chartTimeIntreval)
					}, 15000);
				}
			}
			if (lazyLoadingentity.overviewTabFirsttime && $scope.mainInfoTabType === 'Overview') {
				$scope.$broadcast('overviewtabOnload');
			}
		},
			function (err) {
				$scope.pageloader.screeningLoader = false;
				$scope.pageloader.treeGraphloader = false;
				$scope.entitySearchResult.is_data_not_found.is_rightScreening_data = false;
				if (lazyLoadingentity.overviewTabFirsttime && $scope.mainInfoTabType === 'Overview') {
					$scope.$broadcast('overviewtabOnload');
				}
			});
	}
	$scope.openModalForAddingOfficer = function () {
		$scope.countryJurisdiction = {};
		$scope.newOfficer = {
			firstName: '',
			lastName: '',
			country: '',
			dob: '',
			ScreeeningModal: 'person',
			CompanyName: '',
			website: '',
			requiredComapnyname: false,
			requiredPersonname: false,
			dateOfBirth: ''
		};
		var addOfficerModalInstance = $uibModal.open({
			templateUrl: 'add-officer-modal.html',
			controller: 'addofficerModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal  c-arrow people-options-modal bst_modal add-new-officer',
			scope: $scope
		});
		$scope.screeningModelClassification = {
			screeningPersonRoleModel: [],
			screeningPersonRoleData: [],
			screeningPersonListData: [],
			screeningPersonModel: [],
			screeningCompanyListData: [],
			screeningCompanyModel: [],
		};
		$scope.screeningModelClassification.screeningPersonListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }];
		$scope.screeningModelClassification.screeningCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.screeningModelClassification.screeningPersonRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 11, label: "Chairman" }];
		$scope.personRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.companyRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.editModelClassification.editCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.addOfficerModalInstance = addOfficerModalInstance;
	}
	if ($scope.countryNames.length === 0) {
		EntityApiService.getJurisdiction($rootScope.ehubObject.token).then(function (response) {
			if (response.data && response.data.length > 0) {
				for (var i = 0; i < response.data.length; i++) {
					if (response.data[i].jurisdictionOriginalName && response.data[i].jurisdictionOriginalName !== 'All') {
						response.data[i].jurisdictionName = response.data[i].jurisdictionName && response.data[i].jurisdictionName ? response.data[i].jurisdictionName.toLowerCase() : '';
						response.data[i].flag = '../vendor/jquery/flags/1x1/' + response.data[i].jurisdictionName + '.svg';
						if (!(_.find($scope.countryNames, { 'jurisdictionName': response.data[i].jurisdictionName, 'jurisdictionOriginalName': response.data[i].jurisdictionName }))) {
							$scope.countryNames.push(response.data[i]);
						}
					}
				}
			}
		}, function (err) { });
	}
	$scope.changeScreeningModal = function (value) {
		$scope.newOfficer.ScreeeningModal = value;
	}
	$scope.closeOfficermodalInstance = function () {
		$scope.addOfficerModalInstance.close('close');
	}
	function chartTimeIntreval(path) {
		var curr_time = new Date();
		if (curr_time.getTime() <= curr_time_to_20min.getTime()) {
			$timeout(function () {
				// tag cloud variables initializations for person
				updateOwnershipRecurssive(path, chartTimeIntreval)
			}, 15000);
		} else if (subsidaryFilterData.length === 0) {
			$scope.pageloader.screeningLoader = false;
			$scope.pageloader.treeGraphloader = false;
			$scope.entitySearchResult.is_data_not_found.is_rightScreening_data = false;
			$scope.pageloader.loadingText = false;
			var mainCompanyobject = {
				id: 'p0',
				indirectPercentage: 0,
				title: $scope.entitySearchResult.list.topHeaderObject['vcard:organization-name'] ? $scope.entitySearchResult.list.topHeaderObject['vcard:organization-name'] : $scope.entitySearchResult.name ? $scope.entitySearchResult.name : '',
				totalPercentage: 0,
				entity_id: "orgChartmainEntity"
			};
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = [mainCompanyobject];
			$('#vlaContainer').find('.orgdiagram .custom-scroll-wrapper').remove();
			jQuery("#vlaContainer").famDiagram(currentOPts);
			orgChartRemake(currentOPts)
		}
	}
	var firstReload = [];
	var firsttimeChart = true;
	$scope.createNewArrayReportData = [];
	function updateOwnershipRecurssive(path, chartTimeIntreval) {
		EntityApiService.getCorporateStructure(path).then(function (response) {
			if (response.data && response.data && response.data.status && response.data.status !== "completed" && $scope.entitySearchResult.list.sourceFilterChart_not_Started) {
				chartTimeIntreval(path);
				$scope.pageloader.loadingText = true;//on the flag when api is in progress
				$scope.pageloader.disableFilters = true;
				$("#yearsliderCompliance").slider({ disabled: true });
				$("#sliderAvgExpenCompliance").slider({ disabled: true });//filter disable is off when API is completed
				$("#sanctionsliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
				$("#pepliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
			} else {
				//$scope.filteredChart.applyButton =false;
				$scope.pageloader.disableFilters = false;
				$("#yearsliderCompliance").slider({ disabled: false });
				$("#sliderAvgExpenCompliance").slider({ disabled: false });//filter disable is off when API is completed
				$("#sanctionsliderCompliance").slider({ disabled: false });//filter disable is off when API is completed
				$("#pepliderCompliance").slider({ disabled: false });//filter disable is off when API is completed
				$scope.pageloader.loadingText = false;//off  the loading text flag when api is completed
				if ($scope.complianceRightAdverseNews.length === 0) {
					$scope.entitySearchResult.is_data_not_found.is_rightadverseNews = false;
				}	
				$("#generateReport").parent().removeClass("c-ban");
				$("#generateReport").removeClass("pe-none");
			
				//	chartTimeIntreval(path);
			}
			if (response.data && response.data.data && response.data.data.length > 0) {
				angular.forEach(response.data.data, function (val, key) {
					val.country = !val.country ? (val.jurisdiction ? $scope.getCountryName(val.jurisdiction, 'fromNode') : '') : val.country;
				});
				if (firsttimeChart) {
					firstReload = (jQuery.extend(true, [], response.data.data));
					customerOutreachDataForReport(response);
					var divId = 'vlaContainer';
					if ($scope.customChartView && $scope.entitySearchResult.list.graphviewIndex) {
						divId = "vlaContainer_customView";
					} else if ($scope.customChartView && !$scope.entitySearchResult.list.graphviewIndex) {
						divId = 'vlaContainer_originalView';
					}
					$scope.entitySearchResult.list.currentVLArendered = divId;
					CreatefinalJson(response.data.data, '', divId, response.data.status);
					firsttimeChart = false;
				} else {
					var compareData = _.differenceWith(response.data.data, firstReload, _.isEqual);
					if (compareData && compareData.length > 0) {
						firstReload = (jQuery.extend(true, [], response.data.data));
						var divId = 'vlaContainer';
						if ($scope.customChartView && $scope.entitySearchResult.list.graphviewIndex) {
							divId = "vlaContainer_customView";
						} else if ($scope.customChartView && !$scope.entitySearchResult.list.graphviewIndex) {
							divId = 'vlaContainer_originalView';
						}
						$scope.entitySearchResult.list.currentVLArendered = divId;
						CreatefinalJson(response.data.data, '', divId, response.data.status);
					}
				}
				$scope.pageloader.screeningLoader = false;
				$scope.pageloader.coropoateloader = false;
				$scope.pageloader.treeGraphloader = false;
				var curr_time = new Date();
				if (curr_time.getTime() > curr_time_to_20min.getTime()) {
					$scope.pageloader.loadingText = false;
				}
			}
		}, function (err) {
			chartTimeIntreval(path);
			$scope.pageloader.screeningLoader = false;
			$scope.pageloader.coropoateloader = false;
			$scope.pageloader.treeGraphloader = false;
			$scope.pageloader.loadingText = false;
			//ownership flag should be off here 
		});
	}
	var customerOutreachDoc = []
	function customerOutreach() {
		EntityApiService.customerOutreach().then(function (response) {
			customerOutreachDoc = response.data;
			$scope.EditEntityTypeList = response.data;
			$scope.addEntityTypeList = response.data;
		}, function (err) {
		});
	}
	customerOutreach();
	function customerOutreachDataForReport(response) {
		$scope.createNewArrayReportData = [];
		response.data.data.map(function (d) {
			if (d.recognizedEntityType && d.recognizedEntityType != 'UBO') {
				var ind = customerOutreachDoc.map(function (d) {
					return d.entityType;
				}).indexOf(d.recognizedEntityType)
				$scope.createNewArrayReportData.push({
					'name': d.name,
					'recognizedEntityType': d.recognizedEntityType,
					'document': customerOutreachDoc && customerOutreachDoc[ind] && customerOutreachDoc[ind].requirementsList ? customerOutreachDoc[ind].requirementsList : ''
				});
			}
		})
	}
	function companyScreening(screening, subsidaries) {
		for (var j = 0; j < screening.length; j++) {
			if (screening[j].name) {
				var findEntityScreened = _.findIndex(customEntites.screeningaddition, {//check for parent index in the screening Results
					"@identifier": screening[j].identifier
				});
				var checkname_Inscreening = _.find($scope.actualScreeningData, ['name', screening[j].name]);
				var checkname_InscreeningIndex = _.findIndex($scope.actualScreeningData, ['name', screening[j].name]);
				if ((!(checkname_Inscreening)) && findEntityScreened === -1) {
					var address = {
						country: screening[j].country ? screening[j].country : ''
					}
					var currentadverse = [];
					//	var pepParsedData = screening[j].pep_url ? (screening[j].pep_url.length > 0 ? JSON.parse(screening[j].pep_url[0].raw) : '') : '';
					var pepParsedData = (screening[j].pep && screening[j].pep.length > 0) ? screening[j].pep : [];
					var sanctionData = (screening[j].sanctions && screening[j].sanctions.length > 0) ? screening[j].sanctions : [];
					screening[j].sanction_source = sanctionData;
					screening[j].pep_url = pepParsedData;
					if (screening[j].adverseNews_url && screening[j].adverseNews_url.length) {
						screening[j].adverseNews_url.map(function (val) {
							var dateRange = false;
							if (val.published) {
								var publisheddate = new Date(val.published).getFullYear();
								dateRange = (((publisheddate <= $scope.filteredChart.fromyear) || (publisheddate == $scope.filteredChart.fromyear)) && ((publisheddate >= $scope.filteredChart.toyear) || (publisheddate == $scope.filteredChart.toyear))) ? true : false;
								if (dateRange) {
									val.name = screening[j].name;
									val.identifier = screening[j].identifier;
									val.queue = val.queue ? val.queue : 'high';
									val.isSignificant = val.isSignificant ? val.isSignificant : false;
									currentadverse.push(val);
								}
							}
						});
					}
					if (screening[j].finance_Crime_url && screening[j].finance_Crime_url.length) {
						screening[j].finance_Crime_url.map(function (val) {
							var dateRange = false;
							if (val.published) {
								var publisheddate = new Date(val.published).getFullYear();
								dateRange = (((publisheddate <= $scope.filteredChart.fromyear) || (publisheddate == $scope.filteredChart.fromyear)) && ((publisheddate >= $scope.filteredChart.toyear) || (publisheddate == $scope.filteredChart.toyear))) ? true : false;
								if (dateRange) {
									val.name = screening[j].name;
									val.identifier = screening[j].identifier;
									val.queue = val.queue ? val.queue : 'high';
									val.isSignificant = val.isSignificant ? val.isSignificant : false;
									currentadverse.push(val);
								}
							}
						});
					}
					//screening[j].adverseNews_url = currentadverse;
					var Ubo_ibo = '';
					var pop_totalPercentage = (screening[j].totalPercentage) ? screening[j].totalPercentage : 0;
					if (screening[j].isUbo) {
						Ubo_ibo = 'UBO ';// +pop_totalPercentage;
					} else if ((screening[j].entity_type && (personEntitytype.indexOf(screening[j].entity_type.toLowerCase()) === -1)) && ((pop_totalPercentage && parseInt(pop_totalPercentage) > 25) || (pop_totalPercentage && parseInt(pop_totalPercentage) > 10 && screening[j].high_risk_jurisdiction && screening[j].high_risk_jurisdiction.toLowerCase() === 'high'))) {
						Ubo_ibo = 'IBO '; //+pop_totalPercentage;
					} else {
						Ubo_ibo = "Shareholder";
					}
					if (subsidaries === 'officers') {
						Ubo_ibo = "officers";
					}
					if (subsidaries === 'subsidaries') {
						Ubo_ibo = "subsidaries";
					}
					screening[j].Ubo_ibo = Ubo_ibo;
					var adverseNews = screening[j].news ? (screening[j].news.length > 0 ? classtype(screening[j].news, screening[j], 'Neutral') : []) : [];
					var financeNews = screening[j].news ? (screening[j].news.length > 0 ? classtype(screening[j].news, screening[j], 'Financial Crime') : []) : [];
					var showdeleteIcon = true;
					// if(screening[j].entity_id === "orgChartmainEntity" && (!screening[j].parents || screening[j].parents.length == 0 || !screening[j].subsidiaries || !screening[j].subsidiaries.length == 0)){
					if (screening[j].entity_id === "orgChartmainEntity") {
						showdeleteIcon = false;
					}
					if (subsidaries === 'officers') {
						showdeleteIcon = true;
					}
					screening[j].officer_roles = screening[j].officer_roles ? _.uniq(screening[j].officer_roles) : [];
						// screening[j].report_jurisdiction_risk  = screening[j].country ? calculateCountryRisk(screening[j].country )  :'';
						screening[j].report_jurisdiction_risk = screening[j].country ? calculateCountryRisk(screening[j].country_of_residence || screening[j].country) : screening[j].jurisdiction ? calculateCountryRisk($scope.getCountryName(screening[j].jurisdiction)) : '';
					var findsimilarname = _.filter(EntityCommonTabService.basicsharedObject.totalOfficers_link, {
						'name': screening[j].name
					});
					var entityRefernce = _.uniqBy(findsimilarname, 'source');
					// var mappedSources = findsimilarname.map(function (val) { return val.source });
					if (screening[j] && screening[j].highCredibilitySource && screening[j].officerSources && screening[j].officerSources.length > 0) {
						screening[j].officerSources.push(screening[j].highCredibilitySource);
					}
					var sources = screening[j].officerSources && screening[j].officerSources.length > 0 ? (screening[j].officerSources.filter((val) => val !== 'element').filter(function (item, pos, self) {
						return self.indexOf(item) == pos;
					})) : [];
					screening[j].classification = screening[j].classification && typeof screening[j].classification === "string" ? JSON.parse(screening[j].classification) : (screening[j].classification ? screening[j].classification : []);
					if (screening[j].entity_id !== "orgChartmainEntity") {
						screening[j]["mdaas:RegisteredAddress"] = address;
						screening[j].sanctions = sanctionData;
						screening[j].pep = pepParsedData ? jQuery.extend(true, [], pepParsedData) : [];
						screening[j].finance_Crime_url= financeNews;
						screening[j].adverseNews_url = adverseNews;
						screening[j]["no-screening"] = screening[j].screeningFlag ? screening[j].screeningFlag : true;
						screening[j].country = screening[j].country ? screening[j].country : ((screening[j].basic && screening[j].basic["mdaas:RegisteredAddress"] && screening[j].basic["mdaas:RegisteredAddress"].country) ? screening[j].basic["mdaas:RegisteredAddress"].country : '');
						screening[j].Ubo_ibo =Ubo_ibo;
						screening[j].showdeleteIcon = showdeleteIcon;
						screening[j].isChecked= false;
						screening[j].bvdId = screening[j].bvdId ? screening[j].bvdId : (screening[j].identifier ? screening[j].identifier : '');
						screening[j].date_of_birth = screening[j].date_of_birth ? (screening[j].date_of_birth.year ? (screening[j].date_of_birth.year && screening[j].date_of_birth.month ? (screening[j].date_of_birth.year && screening[j].date_of_birth.month && screening[j].date_of_birth.day ? screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month + '-' + screening[j].date_of_birth.day : screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month) : screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month) : screening[j].date_of_birth.year) : '';
						screening[j].sources = sources;
						screening[j].information_provider = screening[j].information_provider ? screening[j].information_provider : screening[j].source ? screening[j].source : '';
						screening[j].source_evidence = screening[j].highCredibilitySource ? screening[j].highCredibilitySource : screening[j].source ? screening[j].source : screening[j].information_provider ? screening[j].information_provider : '';
						screening[j].entityRefernce = entityRefernce && entityRefernce.length > 0 ? entityRefernce : [];
						screening[j].officerIdentifier = subsidaries === 'officers' ? (screening[j].officerIdentifier ? screening[j].officerIdentifier : screening[j].identifier ? screening[j].identifier : '') : '';
						var newscreeningObject = new Screeningconstructor(screening[j]);
						$scope.actualScreeningData.push(newscreeningObject);
					}
					var filteredSanctions = sanctionData.length > 0 ? pep_sanctionScreeningRange($scope.filteredChart.sanction_slider_min, $scope.filteredChart.sanction_slider_max, sanctionData) : [];
					var filteredpep = pepParsedData.length > 0 ? pep_sanctionScreeningRange($scope.filteredChart.pep_slider_min, $scope.filteredChart.pep_slider_max, pepParsedData) : [];
					if ((sanctionData.length > 0) || (financeNews && financeNews.length > 0) || (adverseNews && adverseNews.length > 0) || (screening[j].high_risk_jurisdiction && screening[j].high_risk_jurisdiction !== 'LOW') || (pepParsedData.length > 0)) {
						$scope.identifiedRisk = $scope.identifiedRisk + 1;
						var newscreeningObject = new Screeningconstructor(screening[j]);
						 $scope.summaryScreenig.push(newscreeningObject);
					}
					// if (screening[j].officer_roles && screening[j].officer_roles.length > 0) {
					// 	screening[j].officer_roles.forEach(function (val) {
					// 		if (!(_.find($scope.unique_officer_roles_array, { 'name': val }))) {
					// 			$scope.unique_officer_roles_array.push({
					// 				'name': val,
					// 				'value': true
					// 			});
					// 			$scope.Rolechecklist.singlearray.push(true);
					// 		}
					// 	});
					// }
					if (screening[j].officer_role) {
						if (!(_.find($scope.unique_officer_roles_array, { 'name': screening[j].officer_role }))) {
							$scope.unique_officer_roles_array.push({
								'name': screening[j].officer_role,
								'value': true,
								'pos':$scope.unique_officer_roles_array.length
							});
							$scope.Rolechecklist.singlearray.push(true);
						}
					}
					intialSelectedRoles = $.extend(true, [], $scope.unique_officer_roles_array);
					if (filteredSanctions.length > 0) {
						$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount + filteredSanctions.length;
					}
					if (financeNews && financeNews.length > 0) {
						$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount + (financeNews[0].count ? financeNews[0].count : 0);
					}
					if (adverseNews && adverseNews.length > 0) {
						$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount + (adverseNews[0].count ? adverseNews[0].count : 0);
					}
					if (screening[j].high_risk_jurisdiction && screening[j].high_risk_jurisdiction.toLowerCase() !== 'low') {
						$scope.entitySearchResult.list.summaryCount.highRiskCount = $scope.entitySearchResult.list.summaryCount.highRiskCount + 1;
					}
					if (filteredpep.length > 0) {
						$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount + filteredpep.length;
					}
					if (subsidaries) {//reflecting the subsidaries screening into  parent company
						var financearticle = screening[j].news ? (screening[j].news.length > 0 ? classtype(screening[j].news, screening[j], 'Financial Crime') : []) : [];
						var adverseArticle = screening[j].news ? (screening[j].news.length > 0 ? classtype(screening[j].news, screening[j], 'Neutral') : []) : [];
						var cumulativeScreening = {
							totalCompanyAdverseNews: adverseArticle,
							totalCompanyFinancialNews: financearticle
						}
						var findParentScreeningIndex = _.findIndex($scope.actualScreeningData, {//check for parent index in the screening Results
							"@identifier": screening[j].Paridentifier
						});
						var hirreacheyIndex = _.findIndex(hierarchyData, {//check for parent index in the chart 
							"identifier": screening[j].Paridentifier
						});
						if (findParentScreeningIndex != -1 && hirreacheyIndex != -1) {
							addScreeningToMainCompany(cumulativeScreening, findParentScreeningIndex, hirreacheyIndex);
						}
					}
				} else if (checkname_InscreeningIndex !== -1 && subsidaries === 'officers') {
					var address = {
						country: screening[j].country ? screening[j].country : ''
					}
					$scope.actualScreeningData[checkname_InscreeningIndex]["mdaas:RegisteredAddress"] = address ? address : $scope.actualScreeningData[checkname_InscreeningIndex]["mdaas:RegisteredAddress"] ? $scope.actualScreeningData[checkname_InscreeningIndex]["mdaas:RegisteredAddress"] : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].identifier = screening[j].identifier ? screening[j].identifier : $scope.actualScreeningData[checkname_InscreeningIndex].identifier ? $scope.actualScreeningData[checkname_InscreeningIndex].identifier : '';
					$scope.actualScreeningData[checkname_InscreeningIndex]["@identifier"] = screening[j].identifier ? screening[j].identifier : $scope.actualScreeningData[checkname_InscreeningIndex]["@identifier"] ? $scope.actualScreeningData[checkname_InscreeningIndex]["@identifier"] : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].high_risk_jurisdiction = screening[j].high_risk_jurisdiction ? screening[j].high_risk_jurisdiction.toLowerCase() : $scope.actualScreeningData[checkname_InscreeningIndex].high_risk_jurisdiction ? $scope.actualScreeningData[checkname_InscreeningIndex].high_risk_jurisdiction : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].hasURL = screening[j].hasURL ? screening[j].hasURL : $scope.actualScreeningData[checkname_InscreeningIndex].hasURL ? $scope.actualScreeningData[checkname_InscreeningIndex].hasURL : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].country = screening[j].country ? screening[j].country : ((screening[j].basic && screening[j].basic["mdaas:RegisteredAddress"] && screening[j].basic["mdaas:RegisteredAddress"].country) ? screening[j].basic["mdaas:RegisteredAddress"].country : '');
					$scope.actualScreeningData[checkname_InscreeningIndex].entity_id = screening[j].entity_id ? screening[j].entity_id : $scope.actualScreeningData[checkname_InscreeningIndex].entity_id ? $scope.actualScreeningData[checkname_InscreeningIndex].entity_id : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].jurisdiction = screening[j].jurisdiction ? screening[j].jurisdiction : $scope.actualScreeningData[checkname_InscreeningIndex].jurisdiction ? $scope.actualScreeningData[checkname_InscreeningIndex].jurisdiction : '';
					$scope.actualScreeningData[checkname_InscreeningIndex].isCustom = screening[j].isCustom ? screening[j].isCustom : $scope.actualScreeningData[checkname_InscreeningIndex].isCustom ? $scope.actualScreeningData[checkname_InscreeningIndex].isCustom : false;
					$scope.actualScreeningData[checkname_InscreeningIndex].bvdId = screening[j].bvdId ? screening[j].bvdId : (screening[j].identifier ? screening[j].identifier : $scope.actualScreeningData[checkname_InscreeningIndex].bvdId ? $scope.actualScreeningData[checkname_InscreeningIndex].bvdId : '');
					$scope.actualScreeningData[checkname_InscreeningIndex].classification = screening[j].classification && typeof screening[j].classification === "string" ? JSON.parse(screening[j].classification) : (screening[j].classification ? screening[j].classification : $scope.actualScreeningData[checkname_InscreeningIndex].classification);
					$scope.actualScreeningData[checkname_InscreeningIndex].date_of_birth = screening[j].date_of_birth ? screening[j].date_of_birth : $scope.actualScreeningData[checkname_InscreeningIndex].date_of_birth;
					$scope.actualScreeningData[checkname_InscreeningIndex].officer_role = screening[j].officer_role ? screening[j].officer_role : $scope.actualScreeningData[checkname_InscreeningIndex].officer_role;
				} else if (checkname_InscreeningIndex !== -1 && subsidaries !== 'officers') {
					screening[j].classification = screening[j].classification && typeof screening[j].classification === "string" ? JSON.parse(screening[j].classification) : (screening[j].classification ? screening[j].classification : []);
					if (screening[j].entity_id !== "orgChartmainEntity") 	{
						screening[j]["mdaas:RegisteredAddress"] = address;
						screening[j].sanctions = sanctionData;
						screening[j].pep = pepParsedData ? jQuery.extend(true, [], pepParsedData) : [];
						screening[j].finance_Crime_url= financeNews;
						screening[j].adverseNews_url = adverseNews;
						screening[j]["no-screening"] = screening[j].screeningFlag ? screening[j].screeningFlag : true;
						screening[j].country = screening[j].country ? screening[j].country : ((screening[j].basic && screening[j].basic["mdaas:RegisteredAddress"] && screening[j].basic["mdaas:RegisteredAddress"].country) ? screening[j].basic["mdaas:RegisteredAddress"].country : '');
						screening[j].Ubo_ibo =Ubo_ibo;
						screening[j].showdeleteIcon = showdeleteIcon;
						screening[j].isChecked= false;
						screening[j].bvdId = screening[j].bvdId ? screening[j].bvdId : (screening[j].identifier ? screening[j].identifier : '');
						screening[j].date_of_birth = screening[j].date_of_birth ? (screening[j].date_of_birth.year ? (screening[j].date_of_birth.year && screening[j].date_of_birth.month ? (screening[j].date_of_birth.year && screening[j].date_of_birth.month && screening[j].date_of_birth.day ? screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month + '-' + screening[j].date_of_birth.day : screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month) : screening[j].date_of_birth.year + '-' + screening[j].date_of_birth.month) : screening[j].date_of_birth.year) : '';
						screening[j].sources = sources;
						screening[j].information_provider = screening[j].information_provider ? screening[j].information_provider : screening[j].source ? screening[j].source : '';
						screening[j].source_evidence = screening[j].highCredibilitySource ? screening[j].highCredibilitySource : screening[j].source ? screening[j].source : screening[j].information_provider ? screening[j].information_provider : '';
						screening[j].entityRefernce = entityRefernce && entityRefernce.length > 0 ? entityRefernce : [];
						screening[j].officerIdentifier = subsidaries === 'officers' ? (screening[j].officerIdentifier ? screening[j].officerIdentifier : screening[j].identifier ? screening[j].identifier : '') : '';
						$scope.actualScreeningData.push(new Screeningconstructor(screening[j]));
				}
			} else {
					if (findEntityScreened !== -1) {
						$scope.actualScreeningData.splice(findEntityScreened, 1);
						$scope.actualScreeningData.push(customEntites.screeningaddition[findEntityScreened]);
					}
				}
			} //if condition for name
		} //for loop ends
	}
	$scope.totalSelectedEntiesForScreening = [];
	$scope.totalScreenedEntity = [];
	$scope.activateScreeningEnabled = false;
	$scope.enableSelectUnselect = false;
	$scope.selectEntitiesForscreening = function (entityscreened, index) {
		//below we are checking is alreadyselected
		var findEntityAlreadySelected = _.findIndex($scope.totalSelectedEntiesForScreening, {
			"identifier": entityscreened.identifier
		});
		var findEntityInScreeningData = _.findIndex($scope.screeningData, {
			"identifier": entityscreened.identifier
		});
		if (findEntityAlreadySelected === -1) {
			var screenig_loading = $scope.screeningData.filter(function (d) {
				return d.screeningresultsloader;
			});
			if (!screenig_loading.length) {
				$scope.activateScreeningEnabled = true;
				$scope.enableSelectUnselect = false;
			}
			$scope.totalSelectedEntiesForScreening.push(entityscreened);
			// $scope.screeningData[findEntityInScreeningData]['no-screening'] = false;
		} else {
			$scope.activateScreeningEnabled = true;
			$scope.enableSelectUnselect = false;
			var screened = $scope.screeningData.filter(function (d) {
				return (d['isChecked']);
			});
			if (!screened.length) {
				$scope.activateScreeningEnabled = false;
			}
			$scope.totalSelectedEntiesForScreening.splice(findEntityAlreadySelected, 1);
			$scope.actualScreeningData[findEntityInScreeningData]['isChecked'] = false;
		}
	}//function Ends
	/*
	* @function to select entities from screening table
	* @author : Amarjith Kumar
	*
	*/
	$scope.getCustomSelectedScreenig = function () {
		var data = [];
		angular.forEach($scope.totalSelectedEntiesForScreening, function (val, key) {
			var screeningdataIndex = _.findIndex($scope.screeningData, {
				'identifier': val.identifier
			});
			if (screeningdataIndex !== -1) {
				$scope.screeningData[screeningdataIndex].screeningresultsloader = true;
			}
			if (val.jurisdiction) {
				data.push({
					"entityName": val.name,
					"entityType": val.entity_type,
					"identifier": val.identifier,
					"jurisdiction": val.jurisdiction ? val.jurisdiction.toUpperCase() : ''
				})
			} else {
				data.push({
					"entityName": val.name,
					"entityType": val.entity_type,
					"identifier": val.identifier,
				})
			}
		});
		$scope.activateScreeningEnabled = false;
		$scope.enableSelectUnselect = true;
		var mainentityId = $scope.entityObj['@identifier'];
		var enddate = $scope.filteredChart.fromyear;
		var startdate = $scope.filteredChart.toyear;
		EntityApiService.getRequestIdOfScreening(mainentityId, startdate, enddate, data).then(function (res) {
			var request_id = res.data.requestId;
			getEntitiesSelectedScreening(request_id, data);
		}).catch(function (err) {
			$scope.activateScreeningEnabled = false;
			$scope.enableSelectUnselect = false;
			angular.forEach($scope.screeningData, function (val, key) {
				val.screeningresultsloader = false;
			});
			console.log(err)
		})
	}
	/*
	* @function to send request for screening entities and handling to display the screening
	* @author : Amarjith Kumar
	*
	*/
	function getEntitiesSelectedScreening(request_id, requestedEntities) {
		var mainentityId = $scope.entityObj['@identifier'];
		EntityApiService.getEntitiesScreening(mainentityId, request_id).then(function (response) {
			if (!response.data.status) {
				$timeout(function () {
					getEntitiesSelectedScreening(response.data.requestId, requestedEntities);
				}, 5000);
				// return;
			}
			$scope.getEntitiesSelectedScreening = true;
			//start handle identifier dashes issue 
			requestedEntities.forEach(function (val) {
				if (val.identifier && val.identifier.split("-").length > 1 && response.data[val.identifier.split("-").join("")]) {
					response.data[val.identifier] = response.data[val.identifier] ? response.data[val.identifier] : {};
					Object.assign(response.data[val.identifier], response.data[val.identifier.split("-").join("")]);
					delete response.data[val.identifier.split("-").join("")];
				}
			});
			//end handle identifier dashes issue 
			var responseObjData = response.data;
			var keys = Object.keys(response.data).filter(function (d) { return (d !== 'requestId' && d !== 'status') }); // keys => entitiesID
			$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount ? $scope.entitySearchResult.list.summaryCount.pepCount : 0;
			$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount ? $scope.entitySearchResult.list.summaryCount.sanctionCount : 0;
			$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount ? $scope.entitySearchResult.list.summaryCount.adverseCount : 0;
			$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount ? $scope.entitySearchResult.list.summaryCount.financeCount : 0;
			$scope.summaryScreenig = [];
			var totalarr = $scope.totalScreenedEntity.map(function (d) { return d.identifier });
			var entities = [];
			// pushing keys (entities id ) to new array(enities) in sequence way as per the enitites selection 
			angular.forEach(keys, function (item, k) {
				if (totalarr.indexOf(item) == -1) {
					entities.push(item);
				} else {
					entities.unshift(item);
				}
			});
			// making loader off or showing check
			angular.forEach($scope.totalSelectedEntiesForScreening, function (val, k) {
				$scope.actualScreeningData = _.uniqBy($scope.actualScreeningData, 'identifier');
				var findIdfromactualScreening = _.findIndex($scope.actualScreeningData, {
					"identifier": val.identifier
				});
				var findIdfromScreening = _.findIndex($scope.screeningData, {
					"identifier": val.identifier
				});
				var ent_obj = responseObjData[val.identifier];
				if (ent_obj) {
					var entity_obj_screening_boolen_keys = Object.keys(ent_obj).filter(function (d) {
						return typeof (ent_obj[d]) === 'boolean';
					});
				}
				var current_status = false;
				if (entity_obj_screening_boolen_keys && entity_obj_screening_boolen_keys.length) {
					for (var st = 0; st < entity_obj_screening_boolen_keys.length; st++) {
						if (!ent_obj[entity_obj_screening_boolen_keys[st]]) {
							current_status = false;
							break;
						}
						current_status = true;
					};
				}
				if (findIdfromactualScreening !== -1) {
					if (current_status) {
						$scope.actualScreeningData[findIdfromactualScreening].screeningresultsloader = false;
						$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = true;
					} else {
						$scope.actualScreeningData[findIdfromactualScreening].screeningresultsloader = true;
						$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = false;
					}
					$scope.actualScreeningData[findIdfromactualScreening]['no-screening'] = false;
				}
				if (findIdfromScreening !== -1) {
					if (current_status) {
						$scope.screeningData[findIdfromScreening].screeningresultsloader = false;
						$scope.screeningData[findIdfromScreening]['isChecked'] = true;
					} else {
						$scope.screeningData[findIdfromScreening].screeningresultsloader = true;
						$scope.screeningData[findIdfromScreening]['isChecked'] = false;
					}
					$scope.screeningData[findIdfromScreening]['no-screening'] = false;
				}
			});
			// handling selected enities screening
			angular.forEach(entities, function (id, k) {
				var id = id;
				var ent_obj = responseObjData[id];
				var entity_obj_screening_boolen_keys = Object.keys(ent_obj).filter(function (d) {
					return typeof (ent_obj[d]) === 'boolean';
				});
				var current_status = false;
				for (var st = 0; st < entity_obj_screening_boolen_keys.length; st++) {
					if (!ent_obj[entity_obj_screening_boolen_keys[st]]) {
						current_status = false;
						break;
					}
					current_status = true;
				};
				$scope.actualScreeningData = _.uniqBy($scope.actualScreeningData, 'identifier');
				var findIdfromactualScreening = _.findIndex($scope.actualScreeningData, {
					"identifier": id
				});
				var findIdfromScreening = _.findIndex($scope.screeningData, {
					"identifier": id
				});
				var findIdinchart = _.findIndex(subsidaryFilterData, {
					"identifier": id
				});
				var find_currIndex = $scope.totalScreenedEntity.findIndex(function (d) { return d.identifier === id });
				if (find_currIndex >= 0) {
					$scope.entitySearchResult.list.summaryCount.pepCount = 0;
					$scope.entitySearchResult.list.summaryCount.sanctionCount = 0;
					$scope.entitySearchResult.list.summaryCount.adverseCount = 0;
					$scope.entitySearchResult.list.summaryCount.financeCount = 0;
				} else {
					$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount ? $scope.entitySearchResult.list.summaryCount.pepCount : 0;
					$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount ? $scope.entitySearchResult.list.summaryCount.sanctionCount : 0;
					$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount ? $scope.entitySearchResult.list.summaryCount.adverseCount : 0;
					$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount ? $scope.entitySearchResult.list.summaryCount.financeCount : 0;
				}
				$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount + (response.data[id].pep ? response.data[id].pep.length : 0);
				$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount + (response.data[id].sanctions ? response.data[id].sanctions.length : 0);
				var adverseNews = [];
				var financeCrime = [];
				var responsePep = (response.data[id].pep ? jQuery.extend(true, [], response.data[id].pep) : []);
				var responseSanction = (response.data[id].sanctions ? jQuery.extend(true, [], response.data[id].sanctions) : []);
				if (response.data[id] && response.data[id].news && response.data[id].news.length > 0) {
					var financeUrl = _.find(response.data[id].news, {
						'classification': 'Financial Crime'
					});
					var adverseUrl = _.find(response.data[id].news, {
						'classification': 'Neutral'
					});
					if (financeUrl && financeUrl.count) {
						financeCrime = [{
							article: financeUrl['_links'].articles,
							name: '',
							entity_id: '',
							count: financeUrl.count
						}];
						$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount + (financeUrl.count ? financeUrl.count : 0);
					}
					if (adverseUrl && adverseUrl.count) {
						adverseNews = [{
							article: adverseUrl['_links'].articles,
							name: '',
							entity_id: '',
							count: adverseUrl.count
						}];
						$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount + (adverseUrl.count ? adverseUrl.count : 0);
					}
				}
				if (findIdfromactualScreening !== -1) {
					if (adverseNews.length > 0) {
						adverseNews[0].name = $scope.actualScreeningData[findIdfromactualScreening].name;
						adverseNews[0].entity_id = $scope.actualScreeningData[findIdfromactualScreening].identifier ? $scope.actualScreeningData[findIdfromactualScreening].identifier : $scope.actualScreeningData[findIdfromactualScreening]['@identifier'] ? $scope.actualScreeningData[findIdfromactualScreening]['@identifier'] : '';
					}
					if (financeCrime.length > 0) {
						financeCrime[0].name = $scope.actualScreeningData[findIdfromactualScreening].name;
						financeCrime[0].entity_id = $scope.actualScreeningData[findIdfromactualScreening].identifier ? $scope.actualScreeningData[findIdfromactualScreening].identifier : $scope.actualScreeningData[findIdfromactualScreening]['@identifier'] ? $scope.actualScreeningData[findIdfromactualScreening]['@identifier'] : '';
					}
					$scope.actualScreeningData[findIdfromactualScreening].adverseNews_url = adverseNews;
					$scope.actualScreeningData[findIdfromactualScreening].finance_Crime_url = financeCrime;
					$scope.actualScreeningData[findIdfromactualScreening].pep_url = jQuery.extend(true, [], responsePep);
					$scope.actualScreeningData[findIdfromactualScreening]['sanction_bst:description'] = jQuery.extend(true, [], responseSanction);
					// $scope.actualScreeningData[findIdfromactualScreening]['adverseNews_url'] = (response.data[id].news ? response.data[id].news : []);
					// $scope.actualScreeningData[findIdfromactualScreening]['finance_Crime_url'] = response.data[id].news;
					if (current_status) {
						$scope.actualScreeningData[findIdfromactualScreening]['screeningresultsloader'] = false;
						$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = true;
					} else {
						$scope.actualScreeningData[findIdfromactualScreening]['screeningresultsloader'] = true;
						$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = false;
					}
					$scope.actualScreeningData[findIdfromactualScreening]['no-screening'] = false;
					$scope.summaryScreenig.push($scope.actualScreeningData[findIdfromactualScreening]);
					EntityorgChartService.addscreeningTotable($scope.actualScreeningData[findIdfromactualScreening]);
					var find_currIndex = $scope.totalScreenedEntity.findIndex(function (d) { return d.identifier === $scope.actualScreeningData[findIdfromactualScreening].identifier });
					if (find_currIndex == -1) {
						$scope.totalScreenedEntity.push($scope.actualScreeningData[findIdfromactualScreening]);
					}
				}
				if (findIdfromScreening !== -1) {
					if (adverseNews.length > 0) {
						adverseNews[0].name = $scope.screeningData[findIdfromScreening].name;
						adverseNews[0].entity_id = $scope.screeningData[findIdfromScreening].identifier ? $scope.screeningData[findIdfromScreening].identifier : $scope.screeningData[findIdfromScreening]['@identifier'] ? $scope.screeningData[findIdfromScreening]['@identifier'] : '';
					}
					if (financeCrime.length > 0) {
						financeCrime[0].name = $scope.screeningData[findIdfromScreening].name;
						financeCrime[0].entity_id = $scope.screeningData[findIdfromScreening].identifier ? $scope.screeningData[findIdfromScreening].identifier : $scope.screeningData[findIdfromScreening]['@identifier'] ? $scope.screeningData[findIdfromScreening]['@identifier'] : '';
					}
					$scope.screeningData[findIdfromScreening].entity_type = $scope.screeningData[findIdfromScreening].entity_type;
					$scope.screeningData[findIdfromScreening].jurisdiction = $scope.screeningData[findIdfromScreening].jurisdiction;
					$scope.screeningData[findIdfromScreening].pep_url = jQuery.extend(true, [], responsePep);
					$scope.screeningData[findIdfromScreening].adverseNews_url = adverseNews;
					$scope.screeningData[findIdfromScreening].finance_Crime_url = financeCrime;
					$scope.screeningData[findIdfromScreening]['sanction_bst:description'] = jQuery.extend(true, [], responseSanction);
					if (current_status) {
						$scope.screeningData[findIdfromScreening].screeningresultsloader = false;
						$scope.screeningData[findIdfromScreening]['isChecked'] = true;
					} else {
						$scope.screeningData[findIdfromScreening].screeningresultsloader = true;
						$scope.screeningData[findIdfromScreening]['isChecked'] = false;
					}
					$scope.screeningData[findIdfromScreening]['no-screening'] = false;
				}
				if (findIdinchart !== -1) {
					if (adverseNews.length > 0) {
						adverseNews[0].name = subsidaryFilterData[findIdinchart].name;
						adverseNews[0].entity_id = subsidaryFilterData[findIdinchart].entity_id;
					}
					if (financeCrime.length > 0) {
						financeCrime[0].name = subsidaryFilterData[findIdinchart].name;
						financeCrime[0].entity_id = subsidaryFilterData[findIdinchart].entity_id;
					}
					subsidaryFilterData[findIdinchart].pep_url = (response.data[id].pep ? jQuery.extend(true, [], response.data[id].pep) : []);
					subsidaryFilterData[findIdinchart]['sanction_source'] = (response.data[id].sanctions ? jQuery.extend(true, [], response.data[id].sanctions) : []);
					subsidaryFilterData[findIdinchart]['adverseNews_url'] = adverseNews;
					subsidaryFilterData[findIdinchart]['finance_Crime_url'] = financeCrime;
					if (current_status) {
						subsidaryFilterData[findIdinchart].screeningresultsloader = false;
					} else {
						subsidaryFilterData[findIdinchart].screeningresultsloader = true;
					}
					subsidaryFilterData[findIdinchart]['no-screening'] = false;
					$scope.showSubsidariesInchart($scope.showSubsidaries);
				}
			});
			$scope.activateScreeningEnabled = false;
			$scope.enableSelectUnselect = false;
			// calling filterScreeningWithPep function to display the screening as per the pep/sanction filters
			$scope.filterScreeningWithPep();
		}).catch(function (err) {
			$scope.getEntitiesSelectedScreening = false;
			$scope.activateScreeningEnabled = false;
			$scope.enableSelectUnselect = false;
			angular.forEach($scope.screeningData, function (val, key) {
				val.screeningresultsloader = false;
				val['isChecked'] = false;
			});
			$scope.totalSelectedEntiesForScreening = [];
			HostPathService.FlashErrorMessage('ERROR', err.responseMessage);
			console.log(err)
		})
	}
	$scope.removeCustomSelectedScreenig = function () {
		angular.forEach($scope.totalScreenedEntity, function (val, key) {
			var findIdfromactualScreening = _.findIndex($scope.actualScreeningData, {
				"identifier": val.identifier
			});
			var findIdfromScreening = _.findIndex($scope.screeningData, {
				"identifier": val.identifier
			});
			var findIdfromSummaryScreening = _.findIndex($scope.summaryScreenig, {
				"identifier": val.identifier
			});
			if (findIdfromactualScreening !== -1) {
				$scope.actualScreeningData[findIdfromactualScreening].pep_url = [];
				$scope.actualScreeningData[findIdfromactualScreening]['sanction_bst:description'] = [];
				$scope.actualScreeningData[findIdfromactualScreening]['no-screening'] = true;
				$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = false;
				$scope.actualScreeningData[findIdfromactualScreening]['adverseNews_url'] = [];
			}
			if (findIdfromScreening !== -1) {
				$scope.screeningData[findIdfromScreening].pep_url = [];
				$scope.screeningData[findIdfromScreening]['sanction_bst:description'] = [];
				$scope.screeningData[findIdfromScreening]['no-screening'] = true;
				$scope.screeningData[findIdfromScreening]['isChecked'] = false;
				$scope.screeningData[findIdfromScreening]['adverseNews_url'] = [];
			}
			if (findIdfromSummaryScreening !== -1) {
				$scope.summaryScreenig.splice(findIdfromScreening, 1);
			}
		});
		angular.forEach($scope.totalSelectedEntiesForScreening, function (val, key) {
			var findIdfromactualScreening = _.findIndex($scope.actualScreeningData, {
				"identifier": val.identifier
			});
			var findIdfromScreening = _.findIndex($scope.screeningData, {
				"identifier": val.identifier
			});
			var findIdfromSummaryScreening = _.findIndex($scope.summaryScreenig, {
				"identifier": val.identifier
			});
			if (findIdfromactualScreening !== -1) {
				$scope.actualScreeningData[findIdfromactualScreening].pep_url = [];
				$scope.actualScreeningData[findIdfromactualScreening]['sanction_bst:description'] = [];
				$scope.actualScreeningData[findIdfromactualScreening]['no-screening'] = true;
				$scope.actualScreeningData[findIdfromactualScreening]['isChecked'] = false;
				$scope.actualScreeningData[findIdfromactualScreening]['adverseNews_url'] = [];
			}
			if (findIdfromScreening !== -1) {
				$scope.screeningData[findIdfromScreening].pep_url = [];
				$scope.screeningData[findIdfromScreening]['sanction_bst:description'] = [];
				$scope.screeningData[findIdfromScreening]['no-screening'] = true;
				$scope.screeningData[findIdfromScreening]['isChecked'] = false;
				$scope.screeningData[findIdfromScreening]['adverseNews_url'] = [];
			}
			if (findIdfromSummaryScreening !== -1) {
				$scope.summaryScreenig.splice(findIdfromScreening, 1);
			}
		});
		$scope.totalSelectedEntiesForScreening = [];
		$scope.totalScreenedEntity = [];
		$scope.entitySearchResult.list.summaryCount.pepCount = 0;
		$scope.entitySearchResult.list.summaryCount.sanctionCount = 0;
		$scope.entitySearchResult.list.summaryCount.adverseCount = 0;
	}
	$scope.SelectedDeSelectAllCustomScreenig = function (selectall) {
		if (selectall) {
			$scope.totalSelectedEntiesForScreening = [];
			// $scope.totalScreenedEntity = [];
			$scope.totalSelectedEntiesForScreening = (jQuery.extend(true, [], $scope.screeningData));
			for (var i = 0, len = customEntites.screeningaddition.length; i < len; i++) {
				for (var j = 0, len2 = $scope.totalSelectedEntiesForScreening.length; j < len2; j++) {
					if (customEntites.screeningaddition[i].identifier === $scope.totalSelectedEntiesForScreening[j].identifier) {
						$scope.totalSelectedEntiesForScreening.splice(j, 1);
						len2 = $scope.totalSelectedEntiesForScreening.length;
					}
				}
			}
			angular.forEach($scope.screeningData, function (val, key) {
				val['isChecked'] = true;
			});
			$scope.activateScreeningEnabled = true;
		} else {
			$scope.totalSelectedEntiesForScreening = [];
			// $scope.totalScreenedEntity = [];
			angular.forEach($scope.screeningData, function (val, key) {
				if (!val.adverseNews_url.length && !val.finance_Crime_url.length && !val.pep_url.length)
					val['isChecked'] = false;
			});
			$scope.activateScreeningEnabled = false;
		}
	}
	var hierarchyData;
	var originalHierrachyData = [];
	var subsidaryFilterData = [];
	var subsidaryFilterData_originalView = [];
	var subsidaryFilterData_customView = [];
	var mainCompanyobject = {
		id: 'p0',
		indirectPercentage: 0,
		title: $scope.entitySearchResult.list.topHeaderObject['vcard:organization-name'] ? $scope.entitySearchResult.list.topHeaderObject['vcard:organization-name'] : $scope.entitySearchResult.name ? $scope.entitySearchResult.name : '',
		totalPercentage: 0,
		entity_id: "orgChartmainEntity"
	};
	/* @purpose:  Create final struture
		 * 			 
		 * @created: 07 may 2018
		 * @params: type(string)
		 * @returns: no
		 * @author: Ram Singh */
	function CreatefinalJson(arr, parent, divId, status) {
		angular.forEach(arr, function (val, key) {
			val.innerSource = (val.innerSource && val.innerSource.toLowerCase() !== 'bst') ? val.innerSource : '';
			val.classification = val.classification ? val.classification : [];
		});
		var FinalData = [];
		$scope.screeningData = [];
		$scope.actualScreeningData = [];
		FinalData = arr;
		subsidaryFilterData = arr;
		subsidaryFilterData_originalView = arr;
		subsidaryFilterData_customView = arr;
		$scope.subsidiaries[0].subsidiaries = [];
		var subsidarycount = [];
		var parentCount = [];
		var subsidariesScreening = [];
		$scope.entitySearchResult.list.summaryCount = {
			highRiskCount: $scope.entitySearchResult.list.summaryCount.highRiskCount ? $scope.entitySearchResult.list.summaryCount.highRiskCount : 0,
			financeCount: $scope.entitySearchResult.list.summaryCount.financeCount ? $scope.entitySearchResult.list.summaryCount.financeCount : 0,
			adverseCount: $scope.entitySearchResult.list.summaryCount.adverseCount ? $scope.entitySearchResult.list.summaryCount.adverseCount : 0,
			pepCount: $scope.entitySearchResult.list.summaryCount.pepCount ? $scope.entitySearchResult.list.summaryCount.pepCount : 0,
			sanctionCount: $scope.entitySearchResult.list.summaryCount.sanctionCount ? $scope.entitySearchResult.list.summaryCount.sanctionCount : 0
		};
		$scope.identifiedRisk = 0;
		$scope.ScreeningRisk = 0;
		arr.forEach(function (val, index, array) {
			val.name = val.title;
			if (val.entity_id === "orgChartParentEntity") {
				parentCount.push(val);
				// var financeUrl = _.find(val.news, {
				// 	'classification': 'Financial Crime'
				// });
				// var adverseUrl = _.find(val.news, {
				// 	'classification': 'Neutral'
				// });
				val.finance_Crime_url = [];
				// if (financeUrl && financeUrl.count) {
				// 	val.finance_Crime_url = [{
				// 	article: financeUrl['_links'].articles,
				// 	name: val.name,
				// 	entity_id:val.entity_id,
				// 	count:financeUrl.count
				// 	 }];
				// } else {
				// 	val.finance_Crime_url = [];
				// }
				// if (adverseUrl && adverseUrl.count) {
				// 	val.adverseNews_url = [{
				// 			article: adverseUrl['_links'].articles,
				// 			name: val.name,
				// 			entity_id:val.entity_id,
				// 			count:adverseUrl.count
				// 	}];
				// } else {
				// 	val.adverseNews_url = [];
				// }
				val.adverseNews_url = [];
				val.addCubeIcon = true;
				val.sanction_source = (val.sanctions && val.sanctions.length > 0) ? val.sanctions : [];
				val.pep_url = (val.pep && val.pep.length > 0) ? val.pep : [];
				var Ubo_ibo = '';
				var pop_totalPercentage = (val.totalPercentage) ? val.totalPercentage : 0;
				if (val.isUbo) {
					Ubo_ibo = 'UBO';
				} else if ((!val.isUbo && val.isUbo === false) || (!val.isUbo && val.entity_type === "person")) {
					Ubo_ibo = ''
				}
				else if ((val.entity_type && (personEntitytype.indexOf(val.entity_type.toLowerCase()) === -1)) && ((pop_totalPercentage && parseInt(pop_totalPercentage) > 25) || (pop_totalPercentage && parseInt(pop_totalPercentage) > 10 && val.high_risk_jurisdiction && val.high_risk_jurisdiction.toLowerCase() === 'high'))) {
					Ubo_ibo = 'IBO'; //+pop_totalPercentage;
				}
				// if ((val.entity_type && (personEntitytype.indexOf(val.entity_type.toLowerCase()) !== -1)) && ((pop_totalPercentage && parseInt(pop_totalPercentage) > 25) || (pop_totalPercentage && parseInt(pop_totalPercentage) > 10 && val.high_risk_jurisdiction && val.high_risk_jurisdiction.toLowerCase() === 'high'))) {
				// 	Ubo_ibo = 'UBO';// +pop_totalPercentage;
				// } else if ((val.entity_type && (personEntitytype.indexOf(val.entity_type.toLowerCase()) === -1)) && ((pop_totalPercentage && parseInt(pop_totalPercentage) > 25) || (pop_totalPercentage && parseInt(pop_totalPercentage) > 10 && val.high_risk_jurisdiction && val.high_risk_jurisdiction.toLowerCase() === 'high'))) {
				// 	Ubo_ibo = 'IBO'; //+pop_totalPercentage;
				// }
				val.Ubo_ibo = Ubo_ibo;
				if (val.subsidiaries && val.subsidiaries.length > 0) {
					var findMaincompany = _.findIndex(val.subsidiaries, { entity_id: "orgChartmainEntity" });
					if (findMaincompany !== -1) {
						val.subsidiaries.splice(findMaincompany, 1);
					}
					subsidaryFilterData = subsidaryFilterData.concat(val.subsidiaries);
				}
				if ($scope.showSubsidaries) {
					FinalData = FinalData.concat(subsidaryFilterData);
				}
				// FinalData = FinalData.concat(customEntites.adddeleteEntity);
				for (let index = 0; index < customEntites.adddeleteEntity.length; index++) {
					var findEntityExist =  _.findIndex(FinalData, { 
						"identifier": customEntites.adddeleteEntity[index].identifier
					});
					if(findEntityExist === -1){
						FinalData.push(customEntites.adddeleteEntity[index]);
					}
				}
				isEntityEdited(val, FinalData);
			} else if (val.entity_id === "orgChartsubEntity") {
				// subsidarycount.push(val);
			} else if (val.entity_id === "orgChartmainEntity") {
				$scope.entitySearchResult.list.orgChartSources = val.sources ? val.sources : [];
				if ($scope.entitySearchResult.list.sourceFirsttimeSelected && val.sources && val.sources.length > 0) {
					var getSourceIndexInSources = val.source_evidence ? val.sources.indexOf(val.source_evidence) : '';
					$scope.entitySearchResult.list.graphSourceSelected = val.source_evidence && val.source_evidence === 'Annual Report' ? 'companyhouse.co.uk' : ($scope.entitySearchResult.list.orgChartSources.length > 0 && ($scope.entitySearchResult.list.graphSourceSelected !== $scope.entitySearchResult.list.orgChartSources[0]) ? (getSourceIndexInSources !== -1 ? val.source_evidence : $scope.entitySearchResult.list.orgChartSources[0]) : $scope.entitySearchResult.list.graphSourceSelected);
					$scope.entitySearchResult.list.sourceFirsttimeSelected = false;
					$scope.entitySearchResult.list.intialSourceSelected = $scope.entitySearchResult.list.graphSourceSelected;
					$scope.entitySearchResult.list.previousSourceSelected = $scope.entitySearchResult.list.intialSourceSelected;
				}
				val.FalseNews = [];
				val.non_negativenews = [];
				var address = {
					country: val.country ? val.country : ''
				}
				//var pepParsedData = val.pep_url ? (val.pep_url.length > 0 ? JSON.parse(val.pep_url[0].raw) : '') : '';
				var pepParsedData = (val.pep && val.pep.length > 0) ? val.pep : [];
				var sanctionData = (val.sanctions && val.sanctions.length > 0) ? val.sanctions : [];
				val.sanction_source = sanctionData;
				val.pep_url = jQuery.extend(true, [], pepParsedData);
				var financeUrl = _.find(val.news, {
					'classification': 'Financial Crime'
				});
				var adverseUrl = _.find(val.news, {
					'classification': 'Neutral'
				});
				if (financeUrl && financeUrl.count) {
					val.finance_Crime_url = [{
						article: adverseUrl['_links'].articles,
						name: val.name,
						entity_id: val.entity_id,
						count: financeUrl.count
					}];
				} else {
					val.finance_Crime_url = [];
				}
				val.finance_Crime_url = [];
				if (adverseUrl && adverseUrl.count) {
					val.adverseNews_url = [{
						article: adverseUrl['_links'].articles,
						name: val.name,
						entity_id: val.entity_id,
						count: adverseUrl.count
					}];
				} else {
					val.adverseNews_url = [];
				}
				// val.adverseNews_url = [];
				if (val.subsidiaries && val.subsidiaries.length > 0) {
					subsidaryFilterData = FinalData.concat(val.subsidiaries);
				}
				if ($scope.showSubsidaries) {
					FinalData = FinalData.concat(subsidaryFilterData);
				}
				// FinalData = FinalData.concat(customEntites.adddeleteEntity);

				for (let index = 0; index < customEntites.adddeleteEntity.length; index++) {
					var findEntityExist =  _.findIndex(FinalData, { 
						"identifier": customEntites.adddeleteEntity[index].identifier
					});
					if(findEntityExist === -1){
						FinalData.push(customEntites.adddeleteEntity[index]);
					}
				}
				isEntityEdited(val, FinalData);

				mainAdverseNews = val.adverseNews_url ? mainAdverseNews.concat(val.adverseNews_url) : mainAdverseNews;
				$scope.complianceRightAdverseNews = mainAdverseNews;
				var Ubo_ibo = '';
				var pop_totalPercentage = (val.totalPercentage) ? val.totalPercentage : 0;
				var findEntityScreened = _.findIndex(customEntites.screeningaddition, {//check for parent index in the screening Results
					"@identifier": val.identifier
				});
				var financeNews = val.news ? (val.news.length > 0 ? classtype(val.news, val, 'Financial Crime') : []) : [];
				var adverseNews = val.news ? (val.news.length > 0 ? classtype(val.news, val, 'Neutral') : []) : [];
				var findIdinchart = _.findIndex(subsidaryFilterData, {
					"identifier": val.identifier
				});
				if (findIdinchart !== -1 && findEntityScreened !== -1) {
					subsidaryFilterData[findIdinchart].pep_url = (customEntites.screeningaddition[findEntityScreened].pep ? customEntites.screeningaddition[findEntityScreened].pep : []);
					subsidaryFilterData[findIdinchart]['sanction_bst:description'] = (customEntites.screeningaddition[findEntityScreened].sanctions ? customEntites.screeningaddition[findEntityScreened].sanctions : []);
					subsidaryFilterData[findIdinchart]['adverseNews_url'] = (customEntites.screeningaddition[findEntityScreened].news ? customEntites.screeningaddition[findEntityScreened].news : []);
					$scope.showSubsidariesInchart($scope.showSubsidaries);
				}
				val.report_jurisdiction_risk = val.country ? calculateCountryRisk(val.country_of_residence || val.country) : val.jurisdiction ? calculateCountryRisk($scope.getCountryName(val.jurisdiction)) : '';
				val.screeningresultsloader = false;
				val['mdaas:RegisteredAddress']  = address;
				val.sanctions = sanctionData;
				val.high_risk_jurisdiction = val.high_risk_jurisdiction ? val.high_risk_jurisdiction.toLowerCase() : '';
				val.finance_Crime_url = val.news ? (val.news.length > 0 ? classtype(val.news, val, 'Financial Crime') : []) : [];
				val.adverseNews_url = val.news ? (val.news.length > 0 ? classtype(val.news, val, 'Neutral') : []) : [];
				val.Ubo_ibo = Ubo_ibo;
				val.juridiction = val.jurisdiction;
				val.screeningresultsloader = false;
				val.isChecked = false;
				val['no-screening'] = true;
				var screeningobject = new Screeningconstructor(val);
				if (findEntityScreened === -1) {
					$scope.actualScreeningData.unshift(screeningobject);
				} else {
					$scope.actualScreeningData.unshift(screeningobject);
				}
				var filteredSanctions = sanctionData.length > 0 ? pep_sanctionScreeningRange($scope.filteredChart.sanction_slider_min, $scope.filteredChart.sanction_slider_max, sanctionData) : [];
				var filteredpep = pepParsedData.length > 0 ? pep_sanctionScreeningRange($scope.filteredChart.pep_slider_min, $scope.filteredChart.pep_slider_max, pepParsedData, pepParsedData) : [];
				if (filteredSanctions.length > 0) {
					$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount + filteredSanctions.length;
				}
				if (financeNews && financeNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount + (financeNews[0].count ? financeNews[0].count : 0);
				}
				if (adverseNews && adverseNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount + (adverseNews[0].count ? adverseNews[0].count : 0);
				}
				if (val.high_risk_jurisdiction && val.high_risk_jurisdiction.toLowerCase() !== 'low') {
					$scope.entitySearchResult.list.summaryCount.highRiskCount = $scope.entitySearchResult.list.summaryCount.highRiskCount + 1;
				}
				if (filteredpep.length > 0) {
					$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount + filteredpep.length;
				}
				$scope.entitySearchResult.is_data_not_found.is_rightScreening_data = false;
				$scope.entitySearchResult.is_data_not_found.is_rightadverseNews = false;
				if (sanctionData.length > 0 || (financeNews.length > 0) || (adverseNews.length > 0) || (val.high_risk_jurisdiction && val.high_risk_jurisdiction !== "" && val.high_risk_jurisdiction.toLowerCase() !== 'low') || pepParsedData.length > 0) {
					$scope.summaryScreenig.unshift(screeningobject);
				}
				//counts the risk of the company
				var TotalRisk = (val.adverseNews_url && val.adverseNews_url.length > 0) ? 1 : 0;
				if (sanctionData.length > 0) {
					TotalRisk = TotalRisk + 1;
				}
				if (val.high_risk_jurisdiction && val.high_risk_jurisdiction !== "" && val.high_risk_jurisdiction.toLowerCase() !== 'low') {
					TotalRisk = TotalRisk + 1;
				}
				if (val.finance_Crime_url && val.finance_Crime_url.length > 0) {
					TotalRisk = TotalRisk + 1;
				}
				if (pepParsedData.length > 0) {
					TotalRisk = TotalRisk + 1;
				}
				$scope.ScreeningRisk = TotalRisk;
				mainCompanyobject = {
					id: val.id,
					indirectPercentage: val.indirectPercentage,
					title: val.title,
					totalPercentage: val.totalPercentage,
					entity_id: val.entity_id
				};
			}
			if (val.subsidiaries && val.subsidiaries.length > 0) {
				val.subsidiaries.map(function (subsidaryVal) {
					subsidaryVal.title = subsidaryVal.keyword ? subsidaryVal.keyword : (subsidaryVal.name ? subsidaryVal.name : '');
					subsidaryVal.Paridentifier = val.identifier ? val.identifier : '';
					//	subsidaryVal.title = subsidaryVal.hasURL ? subsidaryVal.hasURL : '';
					return subsidaryVal;
				});
				var findMaincompany = _.findIndex(val.subsidiaries, { entity_id: "orgChartmainEntity" });
				if (findMaincompany !== -1) {
					val.subsidiaries.splice(findMaincompany, 1);
				}
				subsidariesScreening = subsidariesScreening.concat(val.subsidiaries);
			}
		}); // finaliniquelist for  loop ends
		var finaliniquelist = (jQuery.extend(true, [], arr));
		// allLevelData = subsidaryFilterData;
		finaliniquelist = _.uniqBy(finaliniquelist, 'identifier');
		$scope.subsidiaries[0].subsidiaries = finaliniquelist;
		$scope.ownershipCount.parentCount = parentCount.length;
		$scope.ownershipCount.subsidaryCount = subsidarycount.length;
		hierarchyData = FinalData;
		subsidariesfilteredData = hierarchyData;
		var filterOriginal = FinalData.filter(function (entites) {
			if ((!entites.isCustom && entites.isCustom === false) || (!entites.isCustom)) {
				return !entites.isCustom;
			}
		})
		var filterCustom = FinalData.filter(function (entites) {
			return entites.isCustom || entites.id == "p00";
		})
		$scope.isCustomAvailable = filterCustom.length > 1 ? true : false;
		if ($scope.isCustomAvailable) {
			divId = 'vlaContainer_customView';
			$scope.entitySearchResult.list.currentVLArendered = 'vlaContainer_customView';
			$scope.entitySearchResult.list.graphviewIndex = 1;
			$scope.customChartView = true;
		}
		originalHierrachyData = (jQuery.extend(true, [], filterOriginal));
		companyScreening(arr);
		if (subsidariesScreening.length > 0) {
			//			subsidaryFilterData = hierarchyData.concat(subsidariesScreening); 
			companyScreening(subsidariesScreening, 'subsidaries');//this function is used for shareholders  and subsidaries but screening reflection is for subsidaries
		} else {
			$scope.pageloader.screeningLoader = false;
			//			subsidaryFilterData = hierarchyData; 
		}
		if (fetchLink_officershipScreeningData.length > 0) {
			//	complianceScreeningResult(officershipScreeening)
			companyScreening(fetchLink_officershipScreeningData, 'officers');
		} else {
			$scope.screeningData = jQuery.extend(true, [], $scope.actualScreeningData);
			//  $scope.screeningData	= jQuery.extend(true, [],$scope.summaryScreenig );
			$scope.pageloader.screeningLoader = false;
		}
		// var conbinedSreening = $scope.summaryScreenig.concat(customEntites.screeningaddition);
		var conbinedSreening = $scope.actualScreeningData.concat(customEntites.screeningaddition);//this line of code is for total Enties
		screeningTableOriginal_custom(conbinedSreening);
		screeningTable_OfficersPrioritySources().then(function (response) {
		});
		$rootScope.ownershipScope = hierarchyData;
		if ($scope.mainInfoTabType == 'Duediligence') {
			var currentOPts = jQuery.extend(true, {}, options);
			var graphschartData = originalHierrachyData;
			if ($scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_customView') {
				graphschartData = filterCustom;
			} else if ($scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_originalView' || $scope.entitySearchResult.list.currentVLArendered === 'vlaContainer') {
				graphschartData = originalHierrachyData;
			}
			angular.forEach(graphschartData, function (subSidaryVal, key, self) {
				if (subSidaryVal.parents && subSidaryVal.parents.length > 0) {
					var total_percentage_all = 0;
					var allParentsIndexs = [];
					for (var i = 0; i < subSidaryVal.parents.length; i++) {
						var parentTotalIndex = _.findIndex(self, {
							id: subSidaryVal.parents[i]
						});
						if (parentTotalIndex !== -1 && self[parentTotalIndex].totalPercentage) {
							allParentsIndexs.push(parentTotalIndex);
							total_percentage_all = total_percentage_all + self[parentTotalIndex].totalPercentage;
						} //inner if 
					} //for loops ends
					// if (total_percentage_all > 100) {
					allParentsIndexs.forEach(function (val) {
						graphschartData[val].isConflict = total_percentage_all > 100 ? true : false;
					});
					// }//inner if ends
				} //if ends
			});
			currentOPts.items = jQuery.extend(true, [], graphschartData);
			$rootScope.ownershipScope = graphschartData;
			var linesannotatios = [];
			entites_renderedIn_chart = currentOPts.items;
			EntityCommonTabService.basicsharedObject.entites_renderedIn_chart = entites_renderedIn_chart;
			var entities_items = _.uniqBy(currentOPts.items.filter(function (d) { return d.entity_type === 'person' }), 'identifier')
			angular.forEach(entities_items, function (val, key) {
				// var lineann = new primitives.famdiagram.HighlightPathAnnotationConfig();
				if (val.entity_type === 'person') {
					if (val.indirectChilds && val.indirectChilds.length) {
						angular.forEach(val.indirectChilds, function (data, index) {
							var lineann = new primitives.orgdiagram.ConnectorAnnotationConfig();
							lineann.fromItem = data.id;
							lineann.toItem = val.id;
							lineann.label = "<div class='bp-badge' style='width:50px; height:17px;background-color:#388E3C; color: white;line-height:0.9;top:6px;position:relative;'>" + data.indirectPercentage.toFixed(2) + "%</div>";
							lineann.labelSize = new primitives.common.Size(80, 30);
							lineann.offset = 0;
							lineann.connectorShapeType = primitives.common.ConnectorShapeType.OneWay;
							lineann.color = '#388E3C';
							lineann.lineWidth = 2;
							lineann.lineType = primitives.common.LineType.Dashed;
							lineann.selectItems = false;
							lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Offbeat;
							if (data.isDirect) {
								lineann.lineWidth = 1;
								lineann.lineType = primitives.common.LineType.Solid;
								lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Straight;
								lineann.color = "rgb(88, 107, 113)";
							}
							var lineannAlreadyExist = _.find(linesannotatios, {
								'fromItem':  data.id,
								'toItem': val.id
							});
							if(!lineannAlreadyExist){
								linesannotatios.push(lineann);
							}
						});
					}
				}
			});
			currentOPts.annotations = linesannotatios;
			$('#' + divId).find('.orgdiagram .custom-scroll-wrapper').remove();
			setTimeout(function () {
				jQuery("#" + divId).famDiagram(currentOPts);
				setTimeout(function () {
					// $scope.showSubsidariesInchart($scope.showSubsidaries);
					if (filteredApplied) {
						jQuery("#" + divId).famDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Recreate);
						jQuery("#" + divId).children(".orgdiagram").addClass("custom-scroll-wrapper");
					}
					$("#" + divId).children().css("width", "100%");
					$("#" + divId).children().addClass('custom-scroll-wrapper');
					EntityCommonTabService.AppendOrgGraphIcon(divId);
					if (jQuery("#" + divId).html().length) {
						jQuery("#" + divId).famDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Recreate);
						EntityCommonTabService.AppendOrgGraphIcon(divId);
						jQuery("#" + divId).children(".orgdiagram").addClass("custom-scroll-wrapper");
					}
				}, 2000);
				if (jQuery("#" + divId).html().length) {
					jQuery("#" + divId).famDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Recreate);
					EntityCommonTabService.AppendOrgGraphIcon(divId);
					jQuery("#" + divId).children(".orgdiagram").addClass("custom-scroll-wrapper");
				}
			}, 2000);
			$scope.pageloader.treeGraphloader = false;
			if (status && status === 'completed') {
				getComplexStructureIndicator();
			}
			if (status && status === 'completed' && arr && arr.length == 1 && arr[0]['is-error'] && arr[0].error) {
				HostPathService.FlashErrorMessage('ERROR', arr[0].error);
				// $scope.pageloader.chartFailureMessage =  arr[0].error;
			}
		}
		//		jQuery("#"+divId).orgDiagram("update", primitives.orgdiagram.UpdateMode.Refresh);
	} //function ends
	/*
 * @purpose: Apply the filter values and call API
 * @created: 3 oct 2018
 * @returns: none 
 * @author:Ram
 */
	$scope.changeChartLevels = function () {
		if ($scope.filteredChart.not_screening) {
			$scope.pageloader.coropoateloader = true;
			$scope.pageloader.treeGraphloader = true;
			$scope.pageloader.screeningLoader = true;
			$scope.entitySearchResult.is_data_not_found.is_rightadverseNews = true;
			$scope.pageloader.disableFilters = true;//filter disable is ON
			$scope.pageloader.loadingText = true;//off the loading text when filter is applied
			filteredApplied = 1;
			$scope.complianceRightAdverseNews = [];
			mainofficersNews = [];
			mainAdverseNews = [];
			$scope.filteredChart.applyButton = true;
			$scope.ScreeningRisk = 0;
			$scope.identifiedRisk = 0;
			$scope.unique_officer_roles_array = [];
			$scope.screeningData = [];//empty screening Data  the section that is visible
			$scope.actualScreeningData = [];//empty actualscreening Data
			$scope.summaryScreenig = [];//empty the summary screening
			var param = {
				identifier: $scope.entitySearchResult["@identifier"],
				url: {
					"url": $scope.entitySearchResult.org_structure_link
				},
				numnberoflevel: $scope.filteredChart.selectedNumberofLevel,
				lowRange: $scope.sliderMinValue,
				highRange: $scope.sliderMaxValue,
				organisationName: $scope.entitySearchResult.name ? $scope.entitySearchResult.name : '',
				noOfSubsidiaries: $scope.filteredChart.numberofcomapnies ? $scope.filteredChart.numberofcomapnies : 5,
				// juridiction : $scope.ceriSearchResultObject['isDomiciledIn'] &&  $scope.ceriSearchResultObject['isDomiciledIn'].value ? $scope.ceriSearchResultObject['isDomiciledIn'].value  : '' ,			
				juridiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : $scope.ceriSearchResultObject && $scope.ceriSearchResultObject['isDomiciledIn'] && $scope.ceriSearchResultObject['isDomiciledIn'].value ? $scope.ceriSearchResultObject['isDomiciledIn'].value : ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.isDomiciledIn ? $scope.entitySearchResult.list.topHeaderObject.isDomiciledIn : ''),
				isSubsidiariesRequired: $scope.showSubsidaries,
				Start_date: $scope.filteredChart.toyear,
				End_date: $scope.filteredChart.fromyear,
				source: $scope.entitySearchResult.list.graphSourceSelected
			};
			$("#yearsliderCompliance").slider({ disabled: true });
			$("#sliderAvgExpenCompliance").slider({ disabled: true }); //sfilter sliders are disabled
			$("#sanctionsliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
			$("#pepliderCompliance").slider({ disabled: true });//filter disable is off when API is completed
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = [mainCompanyobject];
			$('#' + $scope.entitySearchResult.list.currentVLArendered).find('.orgdiagram .custom-scroll-wrapper').remove();
			// jQuery("#vlaContainer").famDiagram(currentOPts);
			jQuery("#" + $scope.entitySearchResult.list.currentVLArendered).famDiagram(currentOPts);
			orgChartRemake(currentOPts, $scope.entitySearchResult.list.currentVLArendered);
			// if ($scope.entitySearchResult.org_structure_link){
			EntityApiService.getOwnershipPath(param).then(function (response) {
				if (response && response.data && response.data.path) {
					$scope.appliedOwnerShipControlPercentageMinVal = $scope.sliderMinValue;
					$scope.appliedOwnerShipControlPercentageMaxVal = $scope.sliderMaxValue;
					$timeout(function () {
						// tag cloud variables initializations for person
						updateOwnershipRecurssive(response.data.path, chartTimeIntreval)
					}, 15000);
				}
			}, function (err) {
			});
		}
	};//function ends
	/* @purpose:recreate the  organisation chart
	 * @created: 18 jan 2019
	 * @returns: none 
	 * @author:Ram
	 */
	function orgChartRemake(data, divId) {
		setTimeout(function () {
			jQuery("#" + divId).famDiagram("update", primitives.orgdiagram.UpdateMode.Recreate);
			jQuery("#" + divId).children(".orgdiagram").addClass("custom-scroll-wrapper");
			$("#" + divId).children().css("width", "100%");
			$("#" + divId).children().addClass('custom-scroll-wrapper');
			$("#" + divId).find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-university").remove();
			$("#" + divId).find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-user").remove();
			$("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
			$("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-user").remove();
			$("#" + divId).find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-university").remove();
			$("#" + divId).find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-user").remove();
			$("#" + divId).find(".orgdiagram").find(".orgChartParentEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#4c9d20"></i>');
			$("#" + divId).find(".orgdiagram").find(".orgChartParentEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#4c9d20"></i>');
			$("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().parent().css({
				'background-color': 'none'
			});
			$($("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().prepend('<i class="fa fa-university"></i>')).css({
				'background-color': 'none'
			});
			$($("#" + divId).find(".orgdiagram").find(".orgChartmainEntity.person_icon").parent().prepend('<i class="fa fa-user"></i>')).css({
				'background-color': 'none'
			});
			$("#" + divId).find(".orgdiagram").find(".orgChartsubEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#c7990c"></i>');
			$("#" + divId).find(".orgdiagram").find(".orgChartsubEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#c7990c"></i>');
		}, 0);
	}
	var entites_renderedIn_chart = [];
	$scope.showSubsidariesInchart = function (value, subsidary_level) {
		var divId = $scope.entitySearchResult.list.currentVLArendered;
		$scope.showSubsidaries = value;
		subsidariesfilteredData = [];
		$scope.pageloader.treeGraphloader = true;
		// subsidaryFilterData = subsidaryFilterData.concat(customEntites.adddeleteEntity);
		if (customEntites && customEntites.adddeleteEntity && customEntites.adddeleteEntity.length > 0) {
			for (var i = 0; i < customEntites.adddeleteEntity.length; i++) {
				if (customEntites.adddeleteEntity[i] && customEntites.adddeleteEntity[i].identifier) {
					var checkEntity_Existisence_totalData = _.findIndex(subsidaryFilterData, {
						identifier: customEntites.adddeleteEntity[i].identifier
					});
					if (checkEntity_Existisence_totalData === -1) {
						subsidaryFilterData.push(customEntites.adddeleteEntity[i]);
					}
				}
			}
		}
		if (value) {
			subsidariesfilteredData = subsidaryFilterData;
		} else {
			subsidariesfilteredData = subsidaryFilterData.filter(function (val) {
				if (val.entity_id !== "orgChartsubEntity") {
					return val;
				}
			});
		}
		var currentRefreshData = subsidariesfilteredData;
		if (divId === 'vlaContainer_originalView' || divId === 'vlaContainer') {
			currentRefreshData = originalHierrachyData
		}
		else {
			var filterCustom = subsidariesfilteredData.filter(function (entites) {
				return entites.isCustom || entites.id == "p00";
			})
			currentRefreshData = (jQuery.extend(true, [], filterCustom));
		}
		var uniqSubsidary = _.uniqBy(currentRefreshData, 'identifier');
		$scope.subsidiaries[0].subsidiaries = jQuery.extend(true, [], uniqSubsidary);
		if (currentRefreshData.length > 0) {
			subsidariesfilteredData = _.uniqBy(subsidariesfilteredData, 'identifier');
			//updating the isconflict
			angular.forEach(currentRefreshData, function (subSidaryVal, key, self) {
				if (subSidaryVal.parents && subSidaryVal.parents.length > 0) {
					var total_percentage_all = 0;
					var allParentsIndexs = [];
					for (var i = 0; i < subSidaryVal.parents.length; i++) {
						var parentTotalIndex = _.findIndex(self, {
							id: subSidaryVal.parents[i]
						});
						if (parentTotalIndex !== -1 && self[parentTotalIndex].totalPercentage) {
							allParentsIndexs.push(parentTotalIndex);
							total_percentage_all = total_percentage_all + self[parentTotalIndex].totalPercentage;
						}//inner if 
					} //for loops ends
					// if (total_percentage_all > 100) {
					allParentsIndexs.forEach(function (val) {
						currentRefreshData[val].isConflict = total_percentage_all > 100 ? true : false;
					});
					// }//inner if ends
				}//if ends
			});
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = jQuery.extend(true, [], currentRefreshData);
			// var currentOPts = jQuery.extend(true, {}, options);
			// currentOPts.items = jQuery.extend(true, [], hierarchyData);
			var linesannotatios = [];
			angular.forEach(currentOPts.items, function (val, key) {
				// var lineann = new primitives.famdiagram.HighlightPathAnnotationConfig();
				if (val.entity_type === 'person') {
					if (val.indirectChilds && val.indirectChilds.length) {
						angular.forEach(val.indirectChilds, function (data, index) {
							var lineann = new primitives.orgdiagram.ConnectorAnnotationConfig();
							lineann.fromItem = data.id;
							lineann.toItem = val.id;
							lineann.label = "<div class='bp-badge' style='width:50px; height:17px;background-color:#388E3C; color: white;line-height:0.9;top:6px;position:relative;'>" + data.indirectPercentage.toFixed(2) + "%</div>";
							lineann.labelSize = new primitives.common.Size(80, 30);
							lineann.offset = 0;
							lineann.connectorShapeType = primitives.common.ConnectorShapeType.OneWay;
							lineann.color = '#388E3C';
							lineann.lineWidth = 2;
							lineann.lineType = primitives.common.LineType.Dashed;
							lineann.selectItems = false;
							lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Offbeat;
							if (data.isDirect) {
								lineann.lineWidth = 1;
								lineann.lineType = primitives.common.LineType.Solid;
								lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Straight;
								lineann.color = "rgb(88, 107, 113)";
							}
							var lineannAlreadyExist = _.find(linesannotatios, {
								'fromItem':  data.id,
								'toItem': val.id
							});
							if(!lineannAlreadyExist){
								linesannotatios.push(lineann);
							}
						});
					}
				}
			});
			currentOPts.annotations = linesannotatios;
			$('#' + divId).find('.orgdiagram .custom-scroll-wrapper').remove();
			setTimeout(function () {
				jQuery("#" + divId).famDiagram(currentOPts);
				setTimeout(function () {
					entites_renderedIn_chart = currentOPts.items;
					orgChartRemake(currentOPts, divId);
				}, 2000);
			}, 2000);
			setTimeout(function () {
				$scope.pageloader.treeGraphloader = false;
				$scope.$apply();
			}, 2000);
		} else {
			$scope.pageloader.treeGraphloader = false;
			// $scope.$apply();
		}
	}//function Ends
	$scope.activateDisablebutton = function (val1, val2, yearslider) {
		if (yearslider) {
			$timeout(function () {
				$scope.filteredChart.yearliderMinValue = val1;
				$scope.filteredChart.yearsliderMaxValue = val2;
			}, 0);
		} else {
			$scope.sliderMinValue = val1;
			$scope.sliderMaxValue = val2;
		}
		$scope.checkenableOrDisableApplybutton();
	}//function ends
	/* @purpose: check to Enable or Diable APply button
		* @created: 3 oct 2018
		* @returns: none 
		* @author:Ram
		*/
	$scope.checkenableOrDisableApplybutton = function () {
		var EndDate = ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + new Date().getDate());
		var startDate = ((new Date().getFullYear() - 2) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + new Date().getDate());
		if (($scope.filteredChart.selectedNumberofLevel !== 5) || ($scope.filteredChart.riskRatio !== 1) || ($scope.filteredChart.numberofcomapnies !== 5) || ($scope.sliderMinValue !== $scope.appliedOwnerShipControlPercentageMinVal) || ($scope.sliderMaxValue !== $scope.appliedOwnerShipControlPercentageMaxVal) || ($scope.filteredChart.fromyear !== EndDate) || ($scope.filteredChart.toyear !== startDate) || $scope.showSubsidaries) {
			$scope.filteredChart.applyButton = false;
			$scope.filteredChart.not_screening = true;
		} else if ($scope.filteredChart.pep_slider_min !== 85 || $scope.filteredChart.pep_slider_max !== 100 || $scope.filteredChart.sanction_slider_min !== 70 || $scope.filteredChart.sanction_slider_max !== 100) {
			$scope.filteredChart.applyButton = false;
			$scope.filteredChart.not_screening = false;
		} else {
			$scope.filteredChart.applyButton = true;
			$scope.filteredChart.not_screening = true;
		}
	};
	$scope.checkForsubsidaryApplybutton = function () {
		$scope.showSubsidaries = !$scope.showSubsidaries;
		$scope.checkenableOrDisableApplybutton();
	}
	/* @purpose: Initialize the value of Slider
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function sliderAvgExpen(min, max) {
		$scope.sliderMinValue = min;
		$scope.sliderMaxValue = max;
		$("#sliderAvgExpenCompliance").find(".price-range-min").html(min);
		$("#sliderAvgExpenCompliance").find(".price-range-max").html(max);
		$('#sliderAvgExpenCompliance').slider({
			range: true,
			min: 0,
			max: 100,
			values: [min, max],
			slide: function (event, ui) {
				percentageRange(ui.values, false);
				$scope.activateDisablebutton(ui.values[0], ui.values[1]);
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#sliderAvgExpenCompliance .price-range-both i').css('display', 'none');
				} else {
					$('#sliderAvgExpenCompliance .price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#sliderAvgExpenCompliance .price-range-min, #sliderAvgExpenCompliance .price-range-max').css('opacity', '0');
					$('#sliderAvgExpenCompliance .price-range-both').css('display', 'block');
				} else {
					$('#sliderAvgExpenCompliance .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sliderAvgExpenCompliance .price-range-both').css('display', 'none');
				}
			}
		});
	}
	// yearsliderfilter(0, 2);
	// sanctionsliderfilter(70, 100);
	// pepsliderfilter(85, 100);
	/* @purpose: Slider function hides when slideer key collide
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function collision($div1, $div2) {
		var x1 = $div1;
		var w1 = 0.5;
		var r1 = x1 + w1;
		var x2 = $div2;
		var w2 = 0.5;
		var r2 = x2 + w2;
		if (r1 < x2 || x1 > r2){
			 return false;
			}
		return true;
	}
	/* @purpose: get the values of slider and show on the slider move
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function percentageRange(values, yearslider) {
		if (yearslider && yearslider === 'sanction') {
			$timeout(function () {
				$scope.filteredChart.sanction_slider_min = values[0];
				$scope.filteredChart.sanction_slider_max = values[1];
				$scope.checkenableOrDisableApplybutton();
			}, 0);
		} else if (yearslider && yearslider === 'pep') {
			$timeout(function () {
				$scope.filteredChart.pep_slider_min = values[0];
				$scope.filteredChart.pep_slider_max = values[1];
				$scope.checkenableOrDisableApplybutton();
			}, 0);
		}
		else {
			$timeout(function () {
				$("#sliderAvgExpenCompliance").find(".price-range-min").html(values[0]);
				$("#sliderAvgExpenCompliance").find(".price-range-max").html(values[1]);
			}, 0);
		}
	}
	/* @purpose: onchange of risk value initalize the slider values
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.riskChange = function () {
		if ($scope.filteredChart.riskRatio !== 1) {
			$timeout(function () {
				sliderAvgExpen(25, 100);
			}, 500);
		}
		else {
			sliderAvgExpen(10, 100);
			//				$( "#sliderAvgExpen" ).slider( "destroy" );
		}
		$scope.activateDisablebutton($scope.sliderMinValue, $scope.sliderMaxValue);
	}
	/* @purpose:  marking the  significance and making a API call. 
	* @created: 21 nov 2018
	* @author: Ram singh
	*/
	$scope.makeSignificant = function (significanceStatus) {
		var type = significanceStatus === true ? 'delete' : 'save';
		//for now as API is not available we are temporary caging the significance 
		var data = {
			"entityId": $scope.entitySearchResult["@identifier"],
			"entityName": $scope.entitySearchResult.name
		}
		if (type === 'save') {
			EntityApiService.markEntityAsfavorite(data).then(function (response) {
				$scope.entitySearchResult.list.isSignificant = true;
			}, function (error) {
			});
		}
		if (type === 'delete') {
			EntityApiService.deleteEntityAsfavorite(data).then(function (response) {
				$scope.entitySearchResult.list.isSignificant = false;
			}, function (error) {
			});
		}
	}//function ends
	/* @purpose:  for the Popover message of siigninficance
		* @created: 21 nov 2018
		* @author: Ram singh
		*/
	$scope.checksignificantTitle = function () {
		if (!$scope.entitySearchResult.list.isSignificant) {//check if true
			$scope.entitySearchResult.list.significantTitle = 'Add to favourite Entities';
		} else {
			$scope.entitySearchResult.list.significantTitle = 'Remove from favourite Entities';
		}
	}//function ends
	/* @purpose:  ownershipdownloadAnnualReport
	 * @created: 30 july 2018
	 * @author: swathi
	*/
	$scope.ownershipdownloadAnnualReport = function () {
		$scope.pageloader.treeGraphloader = true;
		EntityApiService.DownloadAnnualReport(downloadOwnershipAnnualUrl).then(function (response) {
			$scope.pageloader.treeGraphloader = false;
			var blob = new Blob([response.data], {
				type: "application/pdf",
			});
			saveAs(blob, 'annual');
		}, function () {
			$scope.pageloader.treeGraphloader = false;
		});
	};
	/*----------------------------------------------------- Questionnaire tab starts ---------------------------------------------------------*/
	var nameOfTheCompany, current_identifier, current_companyId;
	$scope.docDetailsPreloader = false;
	$scope.UploadcurrentState = 'analyzeDetails';
	$scope.headers = [];
	$scope.digitalAssets = [];
	$scope.digi_obj = {};
	$scope.digiHeaders = [];
	
	$scope.overrideAnswer = function () {
		$scope.currentQueData.contentData.answer = $scope.osnitAns;
		$scope.updateValue($scope.osnitAns, $scope.currentQueData, $scope.currentQueData.contentData.questionId)
	};
	/* Upload questionaire Ends */
	$("body").on("click", '.node .content #faCubeId', function () {
		$scope.ownershipdownloadAnnualReport();
	});
	$("body").on("click", '.node .title', function () {
		var url = '#!/company/' + $(this).text();
		window.open(url, '_blank');
	});
	$("body").on("mouseover", '.node .title', function () {
		$(this).css('cursor', 'pointer');
		$(".Screening_new_tooltip").css("display", "block");
	}).on("mousemove", '.node .title', function (event) {
		var findurl = _.find($scope.subsidiaries[0].subsidiaries, { 'name': $(this).text() });
		if (!findurl && $scope.subsidiaries[0].name === $(this).text()) {
			findurl = {}
			findurl.name = $(this).text();
			findurl.source_url = $scope.subsidiaries[0].source_url;
		}
		findurl.source_url = findurl.source_url ? findurl.source_url : '';
		findurl.name = findurl.name ? findurl.name : '';
		var classlist = $(this).attr("class").split(' ');
		$(".Screening_new_tooltip").html('<div class=""><div style="border-bottom:1px solid"><h3 class="tool-tip-heading text-cream" style="margin-bottom:7px">' + $(this).text() + '</h3></div><div class="mar-t15 text-uppercase">' + findurl.source_url + '</div>');
		$(".Screening_new_tooltip").css("left", (event.pageX) + 30 + "px");
		$(".Screening_new_tooltip").css("z-index", "9999");
		return $(".Screening_new_tooltip").css("top", event.pageY + "px")
	}).on("mouseout", '.node .title', function () {
		$(".Screening_new_tooltip").css("display", "none");
	});
	/*
     * @purpose: open adverse news moda; same modal is used in the entityCompany
     *  @created: 22-06-2018
     * @params: params
     * @returns:  
     * @author: Ram
     */
	$scope.openAdverseNewsModal = function (selectedArticle, selectedindex) {
		var articles = $scope.complianceRightAdverseNews;
		var findIndex = _.findIndex(articles, { title: selectedArticle.title });
		selectedArticle = articles[findIndex];
		var articleVisualizerModalInstance = $uibModal.open({
			templateUrl: 'direct-adverse-news-modal.html',
			controller: 'adverseModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow full-analysis-wrapper adverse-full-modal bst_modal ',
			resolve: {
				text: function () {
					return selectedArticle.text;
				},
				articles: function () {
					return articles;
				},
				header: function () {
					return selectedArticle;
				},
				selectedindex: function () {
					return findIndex
				},
				showMainName: function () {
					return false;
				},
				filteredChart: function () {
					return $scope.filteredChart;
				}
			}
		});
		articleVisualizerModalInstance.result.then(function (response) {
		}, function (error) {
		});
	}
	/*
	 * @purpose: open Adverse news pop directly on click newspaper icon
	 * @created: 29 Oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.openAdverseDirectly = function (index, data, type, selectedindex) {
		var filtersummaryScreenig = _.uniqBy($scope.summaryScreenig, function (d) { return d.name });
		var articles = (data.adverseNews_url && data.adverseNews_url.length > 0) ? data.adverseNews_url : (data.FalseNews ? data.FalseNews : []);
		if (type === "finance") {
			articles = (data.finance_Crime_url && data.finance_Crime_url.length > 0) ? data.finance_Crime_url : [];
		}
		var showMainName = (index === 0) ? true : false;
		var articleVisualizerModalInstance = $uibModal.open({
			templateUrl: 'direct-adverse-news-modal.html',
			controller: 'adverseModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow full-analysis-wrapper adverse-full-modal  bst_modal ',
			resolve: {
				/* 	text: function () {
						return  articles[0].length > 0 ? articles[0].text :[];
					}, */
				/* articles: function () {
					return articles;
				}, */
				selectedEntity: function () {
					return data;
				},
				header: function () {
					return articles.length > 0 ? articles[0] : {};
				},
				selectedindex: function () {
					selectedindex = selectedindex ? selectedindex : '';
					return selectedindex;
				},
				showMainName: function () {
					return showMainName;
				},
				type: function () {
					return type;
				},
				summaryScreenig: function () {
					return filtersummaryScreenig;
					// return $scope.summaryScreenig;
				},
				filteredChart: function () {
					return $scope.filteredChart;
				}
			},
			scope: $scope
		});
		articleVisualizerModalInstance.result.then(function (response) {
			for (var i = 0; i < response.length; i++) {
				var false_index = _.findIndex($scope.actualScreeningData[0].adverseNews_url, { title: response[i].title });
				var falseSentimentIndex = _.findIndex($scope.actualScreeningData, { name: response[i].entityName });
				if (false_index != -1) {
					$scope.actualScreeningData[0].adverseNews_url[false_index].isSignificant = response[i].isSignificant;
				}
				if (falseSentimentIndex != -1) {
					var sent_false_index = _.findIndex($scope.actualScreeningData[falseSentimentIndex].adverseNews_url, { 'url': response[i].url, 'title': response[i].title });
					$scope.actualScreeningData[falseSentimentIndex].adverseNews_url[sent_false_index].sentiment = response[i].sentiment ? response[i].sentiment : '';
				}
			}
		}, function (error) { });
	};
	$scope.alertSummary = {};
	$scope.riskScoreColorsForWorldMap = function (riskOrName, type, from) {
		if (type == 'border') {
			if (riskOrName.key && riskOrName.key.toLowerCase() == 'high') {
				if (from == 'fromReport') {
					return { 'border-left-color': '#ef5350' };
				}
				else {
					return { 'border-left-color': '#f67295' };
				}
			} else if (riskOrName.key && riskOrName.key.toLowerCase() == 'medium') {
				if (from == 'fromReport') {
					return { 'border-left-color': '#f7a248' };
				}
				else {
					return { 'border-left-color': '#fbc02d' };
				}
			} else if (riskOrName.key && riskOrName.key.toLowerCase() == 'uhrc') {
				if (from == 'fromReport') {
					return { 'border-left-color': '#ef5350' };
				}
				else {
					return { 'border-left-color': '#f67295' };
				}
			}
		}
		else if (type == 'text') {
			if (riskOrName.Risk && riskOrName.Risk.toLowerCase() == 'high') {
				if (from == 'fromReport') {
					return { 'color': '#ef5350' };
				}
				else {
					return { 'color': '#f67295' };
				}
			} else if (riskOrName.Risk && riskOrName.Risk.toLowerCase() == 'medium') {
				if (from == 'fromReport') {
					return { 'color': '#f7a248' };
				}
				else {
					return { 'color': '#fbc02d' };
				}
			} else if (riskOrName.Risk && riskOrName.Risk.toLowerCase() == 'uhrc') {
				if (from == 'fromReport') {
					return { 'color': '#ef5350' };
				}
				else {
					return { 'color': '#f67295' };
				}
			}
		}
	}
	$scope.getPepSanctionCountforReport = function (data, type) {
		if (data && data.length > 0) {
			if (type === 'pep') {
				return (_.compact(data.map(function (d) {
					data
					return d.pep
				})));
			} else if (type === 'sanction') {
				return (_.compact(data.map(function (d) {
					return d.sanction
				})));
			}
		} else {
			return [];
		}
	}
	/*
		* @purpose: Open modal with all questions and answers filtered by the Met / Not Met requirement.
		* @created: 12th June 2018
		* @params: value
		* @author: Swathi
		*/
	$scope.openQuestionListModal = function (value, questionsList) {
		$scope.metStatus = value;
		$scope.questionsList = questionsList;
		var openModalInstance = $uibModal.open({
			templateUrl: './modal/view/questions.per.answer.status.modal.html',
			controller: 'QuestionsPerAnswersStatusController',
			backdrop: 'static',
			windowClass: 'custom-modal bst_modal  c-arrow ',
			scope: $scope,
			size: 'lg'
		});
		$scope.modalInstance = openModalInstance;
	};
	$scope.goToQuestionnaireTab = function (qid) {
		$scope.onClickInfoTabs('Upload');
		setTimeout(function () {
			$scope.scrollTODiv(qid, 'questions');
		}, 1000);
		$scope.modalInstance.close('close');
	};
	$scope.ToggleSymbol = '';
	$scope.ElementClass = '';
	$scope.toggleSign = function (index) {
		$scope.ToggleSymbol = $scope.ToggleSymbol === index ? false : index;
		$scope.ElementClass = angular.element(document.querySelector('.panel-title'));
	};
	$scope.openRiskScoreModal = function () {
		if (!$scope.cumulativeRisk) {
			return false;
		}
		var modalInstance = $uibModal.open({
			templateUrl: '../scripts/common/modal/views/entityRiskScore.modal.html',
			controller: 'EntityRiskScoreModalController',
			windowClass: 'custom-modal bst_modal  c-arrow entity-risk-score-modal',
			resolve: {
				overviewRisks: function () {
					return $scope.overviewRisks;
				},
				caseID: function () {
					return "";
				},
				caseName: function () {
					return $scope.entitySearchResult;
				},
				riskScoreData: function () {
					return $scope.riskScoreData;
				}
			}
		});
		modalInstance.result.then(function (result) {
		}, function (err) {
		});
	};
	$scope.convertToPositive = function (number) {
		var overrallScore;
		if (Math.abs(number) > 1) {
			overrallScore = Math.abs(number) / 100;
		} else {
			overrallScore = Math.abs(number);
		}
		return overrallScore * 100;
	}
	//-------------------------Implementation of hierarchy chart--------------------------------------------------
	var options = new primitives.famdiagram.Config();
	options.pageFitMode = 0;
	options.cursorItem = 2;
	options.linesWidth = 1;
	options.linesColor = "rgb(88, 107, 113)";
	options.normalLevelShift = 20;
	options.dotLevelShift = 20;
	options.lineLevelShift = 20;
	options.normalItemsInterval = 10;
	options.dotItemsInterval = 10;
	options.lineItemsInterval = 10;
	options.hasSelectorCheckbox = primitives.common.Enabled.False;
	options.defaultTemplateName = "info";
	options.templates = [getInfoTemplate()];
	options.onItemRender = onTemplateRender;
	options.onHighlightChanged = function (e, data) {
		var name = "";
		var subtitile = "";
		if (data && data.context) {
			FlagTooltipData = (data && data.context) ? data.context : '';
		}
		// entityDataOnClikOfOdg = (data && data.context )? data.context :'';
		$("#vlaContainer").find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-university").remove();
		$("#vlaContainer").find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-user").remove();
		$("#vlaContainer").find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
		$("#vlaContainer").find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-user").remove();
		$("#vlaContainer").find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-university").remove();
		$("#vlaContainer").find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-user").remove();
		$("#vlaContainer").find(".orgdiagram").find(".orgChartParentEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#4c9d20"></i>');
		$("#vlaContainer").find(".orgdiagram").find(".orgChartParentEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#4c9d20"></i>');
		$($("#vlaContainer").find(".orgdiagram").find(".orgChartmainEntity")).parent().parent().css({ 'background-color': 'none' });
		$($("#vlaContainer").find(".orgdiagram").find(".orgChartmainEntity").parent().prepend('<i class="fa fa-university"></i>')).css({ 'background-color': 'none' });
		$("#vlaContainer").find(".orgdiagram").find(".orgChartsubEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#c7990c"></i>');
		$("#vlaContainer").find(".orgdiagram").find(".orgChartsubEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#c7990c"></i>');
	};
	var currentClickedData;
	options.onMouseClick = function (e, data) {
		if (data.context) {
			//	currentClickedData = data.context;
			entityDataOnClikOfOdg = (data && data.context) ? data.context : '';
			currentClickedData = _.find($scope.actualScreeningData, {
				'@identifier': data.context.identifier
			});
			if (!entityDataOnClikOfOdg.classification) {
				entityDataOnClikOfOdg.classification = currentClickedData.classification ? currentClickedData.classification : [];
			}
		}
	}
	function handlepop(event) {
		event.preventDefault();
		event.stopPropagation();
		var _this = $(this);
		$timeout(function () {
			var data = jQuery.extend(true, {}, currentClickedData);
			// currentClickedData = undefined;
			if (_this.attr("class").indexOf("newspaper-pop") > -1) {
				var index = (data.entity_id && data.entity_id === 'orgChartmainEntity') ? 0 : 1;
				$scope.openAdverseDirectly(index, data, 'adverse');
				//adversenews
			} else if (_this.attr("class").indexOf("globe-pop") > -1) {
				data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Risk Jurisdiction</td><td>' + data.high_risk_jurisdiction + '</td></tr></tr></table>';
				// openmodal(data);
				$scope.openAdverseDirectly(index, data, 'highRisk');
				//jurrisdiction
			} else if (_this.attr("class").indexOf("gavel-pop") > -1) {
				$scope.openAdverseDirectly(0, data, 'finance');
				//finance_Crime_url
			} else if (_this.attr("class").indexOf("street-pop") > -1) {
				data.txt = '<table class="table data-custom-table"><tr><th>Name</td><td>' + data.name + '</td></tr><tr><th>Source</td><td><a href="' + data.pep_url + '" target="_blank">' + data.pep_url + '</td></tr></tr></table>';
				//	openmodal(data);
				$scope.openAdverseDirectly(index, data, 'pep');
				//pep_url	
			} else if (_this.attr("class").indexOf("user-pop") > -1) {
				var adversecontent = '';
				angular.forEach(data.sanction_source, function (val, key) {
					adversecontent = adversecontent + '<tr class= "border-b0"><td><a href="' + val.url + '"target="_blank"> ' + val.title + '</a></td></tr>';
				});
				data.txt = '<table class="table data-custom-table"><tr  class= "border-b0"><td>' + data.name + '</td></tr>' + adversecontent + '</table>';
				//	openmodal(data);
				$scope.openAdverseDirectly(index, data, 'sanction');
				//sanction_source
			} else if (_this.attr("class").indexOf("multiparentcheck") > -1) {
				multiSelectedEntity(data, _this);
			}
		}, 0)
	}
	$("body").off("click", '.orgdiagram .globe-pop,.orgdiagram .newspaper-pop,.orgdiagram .gavel-pop,.orgdiagram .street-pop,.orgdiagram .user-pop,.entity_popover .newspaper-pop,.entity_popover .globe-pop,.entity_popover .newspaper-pop,.entity_popover .gavel-pop,.entity_popover .street-pop,.entity_popover .user-pop,.orgdiagram .multiparentcheck ', handlepop);
	$("body").on("click", '.orgdiagram .globe-pop,.orgdiagram .newspaper-pop,.orgdiagram .gavel-pop,.orgdiagram .street-pop,.orgdiagram .user-pop,.entity_popover .newspaper-pop,.entity_popover .globe-pop,.entity_popover .newspaper-pop,.entity_popover .gavel-pop,.entity_popover .street-pop,.entity_popover .user-pop,.orgdiagram .multiparentcheck ', handlepop);
	$("body").on("mouseover", ".orgdiagram .flag-pop", function (e) {
		//below section handles the Data for flag tooltip
		var flagCountry = FlagTooltipData.basic && FlagTooltipData.basic['mdaas:RegisteredAddress'] && FlagTooltipData.basic['mdaas:RegisteredAddress'].fullAddress ? FlagTooltipData.basic['mdaas:RegisteredAddress'].fullAddress : (FlagTooltipData.country ? FlagTooltipData.country : '');
		var flag_class_code = FlagTooltipData.juridiction ? ('flag-icon-' + FlagTooltipData.juridiction.toLowerCase()) : ((FlagTooltipData.basic && FlagTooltipData.basic.isDomiciledIn) ? ('flag-icon-' + FlagTooltipData.basic.isDomiciledIn.toLowerCase()) : '');
		if (FlagTooltipData.name && flagCountry) {
			$(".percentage_tooltip").css("display", "block");
			$(".percentage_tooltip").html('<div class="top-heading d-flex">' +
				'<span  style="align-self:baseline" class="f-18 flag-icon flag-icon-squared mar-r10 text-cream  ' + flag_class_code + '"></span><p class="f-12 text-cream">' + flagCountry + '</div>');
			$(".percentage_tooltip").css("left", (e.pageX) + 30 + "px");
			$(".percentage_tooltip").css("z-index", "9999");
			$(".percentage_tooltip").css("display", "block");
			return $(".percentage_tooltip").css("top", e.pageY + "px");
		}
		$(".entity_popover").html('');
		$(".selectedEntityOptions").html('');
		$(".selectedEntityOptions").css("display", "none");
		$(".entity_popover").css("display", "none");
	}).on("mouseout", '.orgdiagram .flag-pop', function () {
		// data = {};
		$(".percentage_tooltip").html('');
		$(".percentage_tooltip").css("display", "none");
	});
	window.getClass = function (clas) {
		return clas;
	}
	$scope.popOverEditDataForOwnership = {};
	$("body").on("click", ".org-chart-item", function (e) {
		e.preventDefault();
		e.stopPropagation();
		if ($scope.entitySearchResult.list.currentVLArendered === "vlaContainer_customView") {
			$(".entity_popover").css("display", "none");
			$(".entity_popover").html("");
			var Ubo_ibo = '';
			if (entityDataOnClikOfOdg.isUbo) {
				Ubo_ibo = 'UBO '; // +pop_totalPercentage;
			}
			var deletebtn = true;
			if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && !$scope.showSubsidaries) {
				deletebtn = false;
			} else if ((entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" || !entityDataOnClikOfOdg.entity_id) && (!entityDataOnClikOfOdg.parents || entityDataOnClikOfOdg.parents.length == 0)) {
				deletebtn = true;
			} else if (entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && $scope.showSubsidaries) {
				deletebtn = false;
			} else if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && (!entityDataOnClikOfOdg.subsidiaries || entityDataOnClikOfOdg.subsidiaries.length > 0) && $scope.showSubsidaries) {
				deletebtn = true;
			} else if (entityDataOnClikOfOdg.entity_id === "orgChartmainEntity") {
				deletebtn = false;
			}
			// deletebtn = entityDataOnClikOfOdg.entity_id !=="orgChartmainEntity" ? true : false;
			$timeout(function () {
				deleteSelectedEntity = entityDataOnClikOfOdg
			}, 500);
			$scope.popOverEditDataForOwnership = entityDataOnClikOfOdg;
			var banSymbol = $scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_customView' ? '' : 'c-ban';
			var penoneSymbol = $scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_customView' ? '' : 'pe-none';
			$(".selectedEntityOptions").css("display", "block");
			$(".selectedEntityOptions").html('<ul class="d-flex ai-c selected-entity-wrapper custom-list pad-l0">' +
				//As UBO cant have Parents Im dviding Person and Org Icon into two parts depending on UBO
				((Ubo_ibo === 'UBO ') ? ('<li class="d-flex ai-c c-ban">Add: <span class="entity-icon-wrapper d-flex ai-c jc-c mar-x5"><i class="fa fa-building addCompanyInchart pe-none"></i></span><span class="entity-icon-wrapper d-flex ai-c jc-c c-ban"><i class="fa fa-user addPersonInchart pe-none"></i></span></li>') : ('<li class="d-flex ai-c ' + (banSymbol) + '">Add: <span class="entity-icon-wrapper d-flex ai-c jc-c mar-x5">' + '<i class="fa fa-building addCompanyInchart ' + (penoneSymbol) + '"></i></span><span class="entity-icon-wrapper d-flex ai-c jc-c ' + (banSymbol) + '"><i class="fa fa-user addPersonInchart ' + (penoneSymbol) + '"></i></span></li>')) +
				'<li class="d-flex ai-c"><i class="fa fa-info-circle mar-r10 entityDetails" ></i><span class="' + (banSymbol) + '"><i class="fa fa-pencil  mar-r10 ' + (penoneSymbol) + '"  onclick="window.showOwnerShipEntityEdit()"></i></span>' +
				(deletebtn ? '<span class ="' + (banSymbol) + '"><i class="fa fa-trash text-coral-red deleteScreening ' + (penoneSymbol) + '"></i></span></li>' : '') +
				'</ul>');
			var windowWidth = window.innerWidth;
			var tooltipWidth = windowWidth - $('.selectedEntityOptions').width() - 100;
			if ((e.pageX > tooltipWidth)) {
				$(".selectedEntityOptions").css("left", ((e.pageX) - $('.selectedEntityOptions').width() - 100) + "px");
			} else {
				$(".selectedEntityOptions").css("left", (e.pageX) + 5 + "px");
			}
			$(".selectedEntityOptions").css("z-index", "9999");
			$(".selectedEntityOptions").css("display", "block");
			return $(".selectedEntityOptions").css("top", e.pageY + "px");
		} else {
			showEntitesDetails(e);
		}
	});
	$("body").on("click", ".entityDetails", function (e) {
		e.preventDefault();
		e.stopPropagation();
		showEntitesDetails(e);
	});
	function showEntitesDetails(e) {
		$(".entity_popover").html('');
		$(".selectedEntityOptions").html('');
		$(".selectedEntityOptions").css("display", "none");
		$(".entity_popover").css("display", "none");
		//below section handles the Data for popover on click of entity
		var flagCountry = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.country) ? entityDataOnClikOfOdg.country : '';
		var flag_class_code = entityDataOnClikOfOdg.jurisdiction ? ('flag-icon-' + entityDataOnClikOfOdg.jurisdiction.toLowerCase()) : entityDataOnClikOfOdg.juridiction ? ('flag-icon-' + entityDataOnClikOfOdg.juridiction.toLowerCase()) : ((entityDataOnClikOfOdg.basic && entityDataOnClikOfOdg.basic.isDomiciledIn) ? ('flag-icon-' + entityDataOnClikOfOdg.basic.isDomiciledIn.toLowerCase()) : '');
		var person_company_icon = entityDataOnClikOfOdg.entity_type ? (((personEntitytype.indexOf(entityDataOnClikOfOdg.entity_type.toLowerCase())) !== -1) ? 'fa-user' : 'fa-university') : 'fa-university'
		var evidenceUrl = entityDataOnClikOfOdg.source_evidence_url ? entityDataOnClikOfOdg.source_evidence_url : (entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : 'javascript:void(0);');
		var evidenceUrl_href = entityDataOnClikOfOdg.source_evidence_url ? entityDataOnClikOfOdg.source_evidence_url : 'javascript:void(0);'
		var evidenceSource = entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : '';
		var evidence_pointer = entityDataOnClikOfOdg.source_evidence_url ? 'c-pointer' : 'c-arrow';
		var evidence_pointer_link = entityDataOnClikOfOdg.source_evidence_url ? '' : 'hidden';
		var pop_indirectPercentage = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.indirectPercentage) ? entityDataOnClikOfOdg.indirectPercentage : 0;
		var pop_totalPercentage = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.totalPercentage) ? entityDataOnClikOfOdg.totalPercentage : 0;
		var fulladdress = entityDataOnClikOfOdg.basic && entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'] && entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'].fullAddress ? entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'].fullAddress : '';
		var entity_role = '';
		var numberOfShares = entityDataOnClikOfOdg.numberOfShares ? (!isNaN(entityDataOnClikOfOdg.numberOfShares) ? Number(entityDataOnClikOfOdg.numberOfShares).formatAmt(0) : entityDataOnClikOfOdg.numberOfShares) : 'N/A';
		if (entityDataOnClikOfOdg.entity_id) {
			entity_role = entityDataOnClikOfOdg.entity_id === 'orgChartmainEntity' ? 'child-container additional-container' : (entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity' ? 'child-container' : '');
		}
		var hide_subsidaries = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity') ? 'd-none' : '';
		var pepclass = (entityDataOnClikOfOdg.pep_url && entityDataOnClikOfOdg.pep_url.length > 0) ? '' : 'd-none';
		var adverseclass = (entityDataOnClikOfOdg.adverseNews_url && entityDataOnClikOfOdg.adverseNews_url.length > 0) ? '' : 'd-none';
		var financeclass = (entityDataOnClikOfOdg.finance_Crime_url && entityDataOnClikOfOdg.finance_Crime_url.length > 0) ? '' : 'd-none';
		var sanctionclass = (entityDataOnClikOfOdg.sanction_source && entityDataOnClikOfOdg.sanction_source.length > 0) ? '' : 'd-none';
		var jurisdictionclass = (entityDataOnClikOfOdg.high_risk_jurisdiction && entityDataOnClikOfOdg.high_risk_jurisdiction.toLowerCase() !== 'low') ? '' : 'd-none';
		var pepLength = (entityDataOnClikOfOdg.pep_url && entityDataOnClikOfOdg.pep_url.length > 0) ? entityDataOnClikOfOdg.pep_url.length : 0;
		var adversenewsLength = 0;
		if (entityDataOnClikOfOdg.adverseNews_url && entityDataOnClikOfOdg.adverseNews_url.length > 0) {
			entityDataOnClikOfOdg.adverseNews_url.map(function (val) {
				adversenewsLength = adversenewsLength + (val.count ? val.count : 0)
			});
		}
		var financenewsLength = 0;
		if (entityDataOnClikOfOdg.finance_Crime_url && entityDataOnClikOfOdg.finance_Crime_url.length > 0) {
			entityDataOnClikOfOdg.finance_Crime_url.map(function (val) {
				financenewsLength = financenewsLength + (val.count ? val.count : 0)
			});
		}
		var sanctionLength = (entityDataOnClikOfOdg.sanction_source && entityDataOnClikOfOdg.sanction_source.length > 0) ? entityDataOnClikOfOdg.sanction_source.length : 0;
		var jurisdictionLength = (entityDataOnClikOfOdg.high_risk_jurisdiction && entityDataOnClikOfOdghigh_risk_jurisdiction.toLowerCase() !== 'low') ? 1 : 0;
		var showScreeningList = (pepLength === 0 && adversenewsLength === 0 && financenewsLength === 0 && sanctionLength === 0 && jurisdictionLength === 0) ? 'd-none' : '';
		var uboClass = '';
		var Ubo_ibo = '';
		if (entityDataOnClikOfOdg.entity_id === 'orgChartmainEntity') {
			entity_role = 'child-container additional-container';
			uboClass = 'd-none';
		} else if (entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity') {
			entity_role = 'child-container subsidery-container';
			uboClass = 'd-none';
		} else if (entityDataOnClikOfOdg.Ubo_ibo === "UBO " || entityDataOnClikOfOdg.Ubo_ibo === "UBO") {
			Ubo_ibo = entityDataOnClikOfOdg.isUbo ? 'UBO ' : ''; // +pop_totalPercentage;
			entity_role = '';
		} else if ((entityDataOnClikOfOdg.Ubo_ibo === "IBO ") || (entityDataOnClikOfOdg.Ubo_ibo === "IBO")) {
			Ubo_ibo = 'IBO '; //+pop_totalPercentage;
			entity_role = 'child-container';
		} else {
			entity_role = 'child-container';
			Ubo_ibo = ''; //pop_totalPercentage;
			uboClass = 'd-none';
		}
		var deletebtn = false;
		if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && !$scope.showSubsidaries) {
			deletebtn = false;
		} else if ((entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" || !entityDataOnClikOfOdg.entity_id) && (!entityDataOnClikOfOdg.parents || entityDataOnClikOfOdg.parents.length == 0)) {
			deletebtn = true;
		} else if (entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && $scope.showSubsidaries) {
			deletebtn = false;
		} else if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && (!entityDataOnClikOfOdg.subsidiaries || entityDataOnClikOfOdg.subsidiaries.length > 0) && $scope.showSubsidaries) {
			deletebtn = true;
		}
		$scope.popOverEditDataForOwnership = entityDataOnClikOfOdg;
		var innerSourceHtml = '';
		var sourceUrl_href = entityDataOnClikOfOdg.sourceUrl ? entityDataOnClikOfOdg.sourceUrl : 'javascript:void(0);';
		var sourceUrl_source = entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : '';
		var sourceUrl_href_pointer = entityDataOnClikOfOdg.sourceUrl ? 'c-pointer' : 'c-arrow';
		var published_date = entityDataOnClikOfOdg.from ? entityDataOnClikOfOdg.from : '';
		var sourceUrl_pointer_link = entityDataOnClikOfOdg.sourceUrl ? '' : 'hidden';
		if (entityDataOnClikOfOdg.innerSource && sourceUrl_source && (entityDataOnClikOfOdg.innerSource == sourceUrl_source)) {
			sourceUrl_source = '';
		}
		if (entityDataOnClikOfOdg.innerSource && sourceUrl_source && published_date) {
			innerSourceHtml = '<a class="text-dark-cream roboto-regular f-12 ' + sourceUrl_href_pointer + '" href="' + sourceUrl_href + '" target="_blank">' + sourceUrl_source + '<span class= "fa mar-x5 fa-external-link ' + sourceUrl_href_pointer + ' ' + sourceUrl_pointer_link + '"> </span></a>';
			innerSourceHtml = innerSourceHtml + '<p class="text-dark-cream mar-b0">' + (entityDataOnClikOfOdg.innerSource) + ' (' + published_date + ')' + '</p>';
		} else if (!entityDataOnClikOfOdg.innerSource && sourceUrl_source && published_date) {
			innerSourceHtml = '<a class="text-dark-cream roboto-regular f-12 ' + sourceUrl_href_pointer + '" href="' + sourceUrl_href + '" target="_blank">' + sourceUrl_source + '<span class= "fa mar-x5 fa-external-link ' + sourceUrl_href_pointer + ' ' + sourceUrl_pointer_link + '"> </span></a>' + ' (' + published_date + ')';
		} else if (entityDataOnClikOfOdg.innerSource && !sourceUrl_source && published_date) {
			innerSourceHtml = '<p class="text-dark-cream mar-b0">' + (entityDataOnClikOfOdg.innerSource) + ' (' + published_date + ')</p>';
		} else if (entityDataOnClikOfOdg.innerSource && !sourceUrl_source && !published_date) {
			innerSourceHtml = '<p class="text-dark-cream mar-b0">' + (entityDataOnClikOfOdg.innerSource) + '</p>';
		} else if (entityDataOnClikOfOdg.innerSource && sourceUrl_source && !published_date) {
			innerSourceHtml = '<a class="text-dark-cream roboto-regular f-12 ' + sourceUrl_href_pointer + '" href="' + sourceUrl_href + '" target="_blank">' + sourceUrl_source + '<span class= "fa mar-x5 fa-external-link ' + sourceUrl_href_pointer + ' ' + sourceUrl_pointer_link + '"> </span></a>';
			innerSourceHtml = innerSourceHtml + '<p class="text-dark-cream mar-b0">' + (entityDataOnClikOfOdg.innerSource) + '</p>';
		} else if (!entityDataOnClikOfOdg.innerSource && sourceUrl_source && !published_date) {
			innerSourceHtml = '<p class="text-dark-cream mar-b0">' + (sourceUrl_source) + '</p>';
		}
		if (entityDataOnClikOfOdg.classification) {
			var showDirectIndirectPercentage = entityDataOnClikOfOdg.classification.find(function (key) {
				return key == "Director" || key == "General Partner"
			})
		}
		entityDataOnClikOfOdg.showDirectIndirectPercentage = showDirectIndirectPercentage ? false : true;
		$timeout(function () {
			deleteSelectedEntity = jQuery.extend({}, true, entityDataOnClikOfOdg);
		}, 500);
		$(".entity_popover").css("display", "block");
		if (entityDataOnClikOfOdg.name) {
			$(".entity_popover").html('<div class="ownership-popover-wrapper right-pop ownership-container ' + entity_role + ' height-100 border-b0">' +
				'<div class="top-content-wrapper pad-b5 border-cream-thin-b">' +
				'<div class="bp-corner-all bt-item-frame bg-transparent">' +
				'<div class="width-100 d-flex detailed-pop-wrapper top-heading">' +
				'<div class="p-rel mar-r5 icon-flag-wrapper">' +
				'<span class="flag-icon mar-x5 text-cream popover-flag flag-icon-squared flag-wrapper flag-pop c-pointer f-9 ' + flag_class_code + '"></span>' +
				'<i class="entity-icon fa ' + person_company_icon + '" style="color:#4c9d20"></i>' +
				'</div>' +
				'<div>' +
				'<div class= "d-flex ai-c jc-sb">' +
				'<h3 name="title" class="bp-item pad-t5 orgChartParentEntity shareCubes" style="display:inline-block;">' +
				entityDataOnClikOfOdg.name +
				' </h3>' +
				((entityDataOnClikOfOdg.entity_id && entityDataOnClikOfOdg.entity_id === 'orgChartParentEntity' && (entityDataOnClikOfOdg.classification.includes('Intermediate Parent') || entityDataOnClikOfOdg.classification.includes('Ultimate Legal Parent'))) ? '<i class="pad-t5 f-16 fa fa-camera text-dark-cream c-pointer shareholderEvidenceIcon"  ></i>' : '') +
				'<div class= "d-if ai-c pad-r25 mar-l10">' +
				// ((Ubo_ibo.trim() == 'UBO')  ? '' : ('<button class="as-fs" style="background: transparent;border:transparent;" onclick="window.addEntityOwnership()" ><i class="fa fa-plus-circle f-16 text-dark-blue c-pointer" style="position: relative;top: 3px;"></i></button>'))+
				// '<button class="as-fs" style="background: transparent;border:transparent;" onclick="window.showOwnerShipEntityEdit()" ><i class="fa fa-pencil f-14 text-dark-cream c-pointer" style="position: relative;top: 3px;"></i></button>'+
				// (deletebtn ? ('<button class="as-fs" style="background: transparent;border:transparent;"><i class="fa fa-trash-o f-16 text-coral-red deleteScreening" style="position: relative;top: 3px;"></i></button>') : '') +
				'</div>' +
				'</div>' +
				'<span class="f-16 c-pointer p-abs t-0 r-0 pull-right text-dark-cream mar-l10" id="closeentityModal">✖</span>' +
				'<p class="mar-t0 mar-b10 text-cream" style="line-height:14px!important">' + fulladdress + '</p>' +
				'</div>' +
				'</div>' +
				'<div class="bottom-list-wrapper text-right ' + showScreeningList + '">' +
				'<ul class="d-if pad-x5 custom-list ai-c">' +
				'<li class="text-capitalize  street-pop ' + pepclass + ' bst_tooltip_wrapper" name="Pep alerts"><i class="fa text-cream fa fa-street-view c-pointer "></i><span>(' + pepLength + ')</span></li>' +
				'<li class="text-capitalize  user-pop ' + sanctionclass + ' bst_tooltip_wrapper" name="Sanction alerts"><i class="fa text-cream fa-ban c-pointer "></i><span>(' + sanctionLength + ')</span></li>' +
				'<li class="text-capitalize  globe-pop ' + jurisdictionclass + ' bst_tooltip_wrapper" name="Jurisdiction"><i class="fa text-cream fa-globe c-pointer"></i><span>(' + jurisdictionLength + ')</span></li>' +
				'<li class ="text-capitalize  gavel-pop ' + financeclass + ' bst_tooltip_wrapper " name="Finance news"><i class="fa text-cream fa-gavel  c-pointer "></i> <span>(' + financenewsLength + ')</span></li>' +
				'<li class="text-capitalize  newspaper-pop ' + adverseclass + '  bst_tooltip_wrapper" name="news"><i class="fa text-cream fa-newspaper-o c-pointer"></i><span>(' + adversenewsLength + ')</span></li>' +
				'</ul>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<div class="bottom-content-wrapper border-cream-thin-t">' +
				'<div class="' + hide_subsidaries + '">' +
				'<p class="text-cream d-flex mar-b5 mar-t10  ai-c"><span name="description" class="text-cream mar-r5 f-10 pad-x5 description percentageInfo ' + uboClass + '">' + Ubo_ibo + '</span><span>Number of shares : ' + numberOfShares + '</span></p>' +
				(entityDataOnClikOfOdg.showDirectIndirectPercentage ? ('<ul class="list-unstyled progressbar-list mar-b0">' +
					'<li class="progressbar-list-item mar-b5 pad-t0">' +
					'<div class="left-col f-12 text-cream" style="width: 120px;">Indirect Ownership </div>' +
					'<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">' +
					'<div class="progress-bar progress-gradient-blue" role="progressbar" aria-valuenow="' + parseInt(pop_indirectPercentage) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseInt(pop_indirectPercentage) + '%">' +
					'<span class="sr-only ">' + parseInt(pop_indirectPercentage).toFixed(2) + '</span></div>' +
					'</div>' +
					'<div class="right-col f-12 text-cream" style="width: 60px;">' + pop_indirectPercentage.toFixed(2) + '%</div>' +
					'</li>' +
					'<li class="progressbar-list-item mar-b5">' +
					'<div class="left-col f-12 text-cream" style="width:120px;">Direct Ownership</div>' +
					'<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">' +
					'<div class="progress-bar progress-gradient-curious-blue" role="progressbar" aria-valuenow="' + parseInt(pop_totalPercentage) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseInt(pop_totalPercentage) + '%">' +
					'<span class="sr-only ">' + pop_totalPercentage.toFixed(2) + '%' + '</span>' +
					'</div>' +
					'</div>' +
					'<div class="right-col f-12 text-cream" style="width: 60px;">' + pop_totalPercentage.toFixed(2) + '%</div>' +
					'</li>') : '') +
				'</ul>' + innerSourceHtml +
				'</div>' +
				'</div>');
			var p = $('#flowChartViewDiv1')
			var position = p.offset();
			var windowWidth = window.innerWidth;
			var tooltipWidth = windowWidth - $('.entity_popover').width() - 100;
			if ((e.pageX > tooltipWidth)) {
				$(".entity_popover").css("left", ((e.pageX) - $('.entity_popover').width() - 100) + "px");
			} else {
				$(".entity_popover").css("left", (e.pageX) + 5 + "px");
			}
			$(".entity_popover").css("z-index", "9999");
			$(".entity_popover").css("display", "block");
			return $(".entity_popover").css("top", e.pageY + "px");
			$(".entity_popover").css("z-index", "9999");
			$(".entity_popover").css("display", "block");
			return $(".entity_popover").css("top", e.pageY + "px");
		}
	}
	$("body").on("click", function (e) {
		if (!$(e.target).parents('.entity_popover').hasClass('entity_popover') || $(e.target).attr("id") == "closeentityModal") {
			$(".entity_popover").html('');
			$(".entity_popover").css("display", "none");
			$(".selectedEntityOptions").html('');
			$(".selectedEntityOptions").css("display", "none");
		}
	})
	/* this section closes the modal on click of screening Icon after 500ms .entity_popover */
	$("#closeentityModal").on("click", function (e) {
		$timeout(function () {
			$(".entity_popover").html('');
			$(".entity_popover").css("display", "none");
			$(".selectedEntityOptions").html('');
			$(".selectedEntityOptions").css("display", "none");
		}, 500);
	});
	//function to delete entity ownership corporatestructure
	$("body").on("click", '.deleteScreening', function () {
		openDeleteConfirmationModal(deleteSelectedEntity);
	});
	//function to delete custom ownership corporatestructure
	$scope.deleteCustomView = function (event) {
		event.stopPropagation();
		event.preventDefault();
		openDeleteConfirmationModal(deleteSelectedEntity, true);
	}
	$("body").on("click", ".addCompanyInchart", function (e) {
		window.addEntityOwnership('', 'organization');
		$scope.addModelClassification.companyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }]; $scope.example9settings = { enableSearch: true };
	});
	$("body").on("click", ".addPersonInchart", function (e) {
		window.addEntityOwnership('', 'person');
		$scope.addModelClassification.personRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 13, label: "Chairman" }];
		$scope.personRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.companyRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.addModelClassification.personListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }]; $scope.personSettings = { enableSearch: true };
	});
	var modalInstanceEdit;
	$scope.pre_popOverEditDataForOwnership = {};
	window.showOwnerShipEntityEdit = function () {
		$scope.editshowDirectIndirectPercentagePerson = true;
		$scope.popOverEditDataForOwnership = jQuery.extend({}, true, entityDataOnClikOfOdg);
		$scope.editModelClassification.editPersonListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }];
		$scope.editModelClassification.editRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 13, label: "Chairman" }];
		$scope.personRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.companyRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.editModelClassification.editCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.example5customTexts = { buttonDefaultText: 'Classification' };
		$scope.showfiteredSourceList = false;
		if (entityDataOnClikOfOdg && entityDataOnClikOfOdg.jurisdiction) {
			var countryEdit = _.find($scope.countryNames, {
				'jurisdictionName': entityDataOnClikOfOdg.jurisdiction.toLowerCase()
			});
			if (countryEdit) {
				$scope.countryJurisdiction = countryEdit;
				$scope.editCustomSelected.country = (entityDataOnClikOfOdg.country || countryEdit.jurisdictionOriginalName) ? countryEdit.jurisdictionOriginalName : '';
				$scope.editSelectedFlagSource = countryEdit.flag;
			} else {
				$scope.countryJurisdiction = {
					jurisdictionOriginalName: 'Select Country'
				};
				$scope.editCustomSelected.country = "";
				$scope.editSelectedFlagSource = "";
			}
		} else {
			$scope.countryJurisdiction = {
				jurisdictionOriginalName: 'Select Country'
			};
		}
		if (entityDataOnClikOfOdg.SelectedEntityType) {
			$scope.entitySearchResult.list.recognized_entity.entityType = entityDataOnClikOfOdg.SelectedEntityType;
		}
		if (entityDataOnClikOfOdg.Ubo_ibo && entityDataOnClikOfOdg.Ubo_ibo === "UBO ") {
			$scope.editUBOValue = true;
		} else {
			$scope.editUBOValue = false;
		}
		if (!entityDataOnClikOfOdg.numberOfShares) {
			var ReportcompanyIndex = _.findIndex($scope.subsidiaries[0].subsidiaries, {
				'identifier': $scope.popOverEditDataForOwnership.identifier
			});
			if (ReportcompanyIndex !== -1) {
				$scope.popOverEditDataForOwnership.numberOfShares = $scope.subsidiaries[0].subsidiaries[ReportcompanyIndex].numberOfShares;
			}
		}
		if ($scope.popOverEditDataForOwnership && $scope.popOverEditDataForOwnership.officer_role) {
			var officerRoleSelected = _.findIndex($scope.editModelClassification.editRoleData, {
				'label': $scope.popOverEditDataForOwnership.officer_role
			});
			$scope.editModelClassification.editRoleModel = officerRoleSelected !== -1 ? $scope.editModelClassification.editRoleData[officerRoleSelected].label : '';
		}
		$scope.popOverEditDataForOwnership.source_evidence1 = entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : entityDataOnClikOfOdg.information_provider ? entityDataOnClikOfOdg.information_provider : '';
		if ($scope.popOverEditDataForOwnership.source_evidence1 && !$scope.popOverEditDataForOwnership.sourceUrl) {
			var value = _.find($scope.sourceList, {
				'sourceName': $scope.popOverEditDataForOwnership.source_evidence1
			});
			if (value) {
				$scope.fillSourceSearchedInput(value, 'edit');
			}
		}
		$scope.popOverEditDataForOwnership.classification = entityDataOnClikOfOdg.classification;
		$scope.popOverEditDataForOwnership.date_of_birth = ($scope.popOverEditDataForOwnership.date_of_birth || $scope.popOverEditDataForOwnership.dateOfBirth || $scope.popOverEditDataForOwnership['vcard:bday'] || entityDataOnClikOfOdg.date_of_birth || entityDataOnClikOfOdg.dateOfBirth || entityDataOnClikOfOdg['vcard:bday']) ? ($scope.popOverEditDataForOwnership.date_of_birth || $scope.popOverEditDataForOwnership.dateOfBirth || $scope.popOverEditDataForOwnership['vcard:bday'] || entityDataOnClikOfOdg.date_of_birth || entityDataOnClikOfOdg.dateOfBirth || entityDataOnClikOfOdg['vcard:bday']) : '';
		$scope.pre_popOverEditDataForOwnership = jQuery.extend(true, {}, $scope.popOverEditDataForOwnership);
		if ($scope.popOverEditDataForOwnership && $scope.popOverEditDataForOwnership.classification && $scope.popOverEditDataForOwnership.classification.length > 0) {
			var editclasification = [];
			if ($scope.popOverEditDataForOwnership && $scope.popOverEditDataForOwnership.entity_type === "person") {
				for (var index = 0; index < $scope.popOverEditDataForOwnership.classification.length; index++) {
					var item = _.find($scope.editModelClassification.editPersonListData, {
						'label': $scope.popOverEditDataForOwnership.classification[index]
					});
					if (item) {
						if (item.label === "Director" || item.label == "General Partner") {
							$scope.editshowDirectIndirectPercentagePerson = false
						}
						editclasification.push({
							label: item.label,
							id: item.id
						});
					}
				}
				$scope.editModelClassification.editPersonModel = editclasification;
			} else {
				for (var index = 0; index < $scope.popOverEditDataForOwnership.classification.length; index++) {
					var item = _.find($scope.editModelClassification.editCompanyListData, {
						'label': $scope.popOverEditDataForOwnership.classification[index]
					});
					if (item) {
						if (item.label === "Director" || item.label == "General Partner") {
							$scope.editshowDirectIndirectPercentagePerson = false
						}
						editclasification.push({
							label: item.label,
							id: item.id
						});
					}
				}
				$scope.editModelClassification.editCompanyModel = editclasification;
			}
		}
		// $scope.entitySearchResult.list.recognized_entity.entityType = entityDataOnClikOfOdg.recognizedEntityType ? entityDataOnClikOfOdg.recognizedEntityType  : '';
		modalInstanceEdit = $uibModal.open({
			templateUrl: 'editOwnership.html',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow  bst_modal add-ownership-modal add-new-officer',
			scope: $scope
		});
		if (!entityDataOnClikOfOdg.country) {
			entityDataOnClikOfOdg.country = entityDataOnClikOfOdg.jurisdiction ? $scope.getCountryName(entityDataOnClikOfOdg.jurisdiction, 'fromNode') : '';
		}
		setTimeout(() => {
			$("#entityOfficeRole").mousedown(function () {
				if ($(".editClassification").children(".open").length) {
					$(".editClassification").children(".open").click();
				}
			});
		}, 100);
	}
	//this Function is for Selecting the Multi Entites from the Chart
	function multiSelectedEntity(selectedEntity, that) {
		var entityAlreadySelected = _.findIndex($scope.selectedMultipleeEntities, { 'name': selectedEntity.name, '@identifier': selectedEntity['@identifier'] });
		if (entityAlreadySelected === -1) {
			$scope.selectedMultipleeEntities.push(selectedEntity);
			that.prop('checked', true);
		} else {
			$scope.selectedMultipleeEntities.splice(entityAlreadySelected, 1);
			that.prop('checked', false);
		}
	}
	function openDeleteConfirmationModal(deleteSelectedEntity, removeCustomView) {
		var modalInstance = $uibModal.open({
			animation: true,
			controller: 'OwnershipstrcutureupdateModalController',
			templateUrl: 'confirmModal.html',
			windowClass: 'custom-modal c-arrow bst_modal',
			resolve: {
				updateData: function () {
					return deleteSelectedEntity;
				},
				removeCustom: function () {
					return removeCustomView ? removeCustomView : false;
				}
			}
		});
		modalInstance.result.then(function (selectedItem) {
			var query = {
				identifier: selectedItem.identifier ? selectedItem.identifier : '',
				field: 'vertex'
			};
			if (selectedItem.removeCustom) {
				query.identifier = ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
					query.field = 'custom_graph';
				subsidaryFilterData = originalHierrachyData.filter((val) => {
					if (!val.isCustom && val.isCustom === false) {
						return val;
					}
				});
				$scope.customChartView = false;
				$scope.isCustomAvailable = false;
				customEntites.adddeleteEntity = [];
				$scope.showSubsidariesInchart($scope.showSubsidaries, "vlaContainer");
				screeningTableOriginal_custom($scope.actualScreeningData);
				EntityApiService.deleteCustomEntites(query).then(function (deleteResponse) {
					$scope.entitySearchResult.list.currentVLArendered = "vlaContainer"
					$scope.showSubsidariesInchart($scope.showSubsidaries);
				});
				$scope.showSubsidariesInchart($scope.showSubsidaries);
			} else {
				removeEntityAndParents(selectedItem);
			}
		}, function () {
		});
	}
	/*
* @purpose: Delete the Entity and Parents from  from Screening,chart and report
* @created: 14 may 2019
* @author: Ram
*/
	function removeEntityAndParents(selectedEntity) {
		subsidaryFilterData = _.uniq(subsidaryFilterData, function (d) { return d['identifier'] }); //temp fix
		var removedCompanyIndex = _.findIndex(subsidaryFilterData, {
			'identifier': selectedEntity.identifier,
			'name': selectedEntity.name
		});
		var actualscreeningCompanyIndex = _.findIndex($scope.actualScreeningData, {
			'@identifier': selectedEntity.identifier,
			'name': selectedEntity.name
		});
		var screeningDatacompanyIndex = _.findIndex($scope.screeningData, {
			'@identifier': selectedEntity.identifier,
			'name': selectedEntity.name
		});
		var reportsubsidaryIndex = _.findIndex($scope.subsidiaries[0].subsidiaries, {
			'identifier': selectedEntity.identifier,
			'name': selectedEntity.name
		});
		var customAddedscreeningIndex = _.findIndex(customEntites.adddeleteEntity, {
			'@identifier': selectedEntity.identifier,
			'name': selectedEntity.name
		});
		if (removedCompanyIndex !== -1) {
			subsidaryFilterData.splice(removedCompanyIndex, 1);
			EntityorgChartService.deleteEntityFomChart(selectedEntity);
		}
		if (actualscreeningCompanyIndex !== -1) {
			$scope.actualScreeningData.splice(actualscreeningCompanyIndex, 1);
			if ($scope.entitySearchResult.list.summaryCount.sanctionCount > 0 && selectedEntity.sanction_source.length > 0) {
				$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount - selectedEntity.sanction_source.length;
			}
			if ($scope.entitySearchResult.list.summaryCount.pepCount > 0 && selectedEntity.pep_url.length > 0) {
				$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount - selectedEntity.pep_url.length;
			}
			if ($scope.entitySearchResult.list.summaryCount.adverseCount > 0 && selectedEntity.adverseNews_url.length > 0) {
				$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount - selectedEntity.adverseNews_url.length;
			}
			if ($scope.entitySearchResult.list.summaryCount.financeCount > 0 && selectedEntity.finance_Crime_url.length > 0) {
				$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount - selectedEntity.finance_Crime_url.length;
			}
		}
		if (screeningDatacompanyIndex !== -1) {
			$scope.screeningData.splice(screeningDatacompanyIndex, 1);
		}
		if (reportsubsidaryIndex !== -1) {
			$scope.subsidiaries[0].subsidiaries.splice(reportsubsidaryIndex, 1);
		}
		if (selectedEntity && selectedEntity.parents && selectedEntity.parents.length > 0) {
			for (var i = 0; i < selectedEntity.parents.length; i++) {
				var parentObject = _.find(subsidaryFilterData, {
					'id': selectedEntity.parents[i],
					'isCustom': true
				});
				var is_multi_parent_entity = [];
				for (var multi_index = 0; multi_index < subsidaryFilterData.length; multi_index++) {
					if (subsidaryFilterData[multi_index] && subsidaryFilterData[multi_index].parents && subsidaryFilterData[multi_index].parents.length > 0) {
						is_multi_parent_entity = subsidaryFilterData[multi_index].parents.filter(function (val) { return val === selectedEntity.parents[i] });
						if (is_multi_parent_entity.length > 0) {
							break;
						}
					}
				}
				if (parentObject && is_multi_parent_entity.length === 0) {
					if (customAddedscreeningIndex !== -1) {
						customEntites.adddeleteEntity.splice(customAddedscreeningIndex, 1)
					}
					var query = {
						identifier: selectedEntity.identifier ? selectedEntity.identifier : '',
						field: 'vertex'
					};
					EntityApiService.deleteCustomEntites(query).then(function (deleteResponse) {
						$scope.showSubsidariesInchart($scope.showSubsidaries);
					});
					removeEntityAndParents(parentObject);
				}
			}
		} else {
			if (customAddedscreeningIndex !== -1) {
				customEntites.adddeleteEntity.splice(customAddedscreeningIndex, 1)
			}
			var query = {
				identifier: selectedEntity.identifier ? selectedEntity.identifier : '',
				field: 'vertex'
			};
			EntityApiService.deleteCustomEntites(query).then(function (deleteResponse) {
				$scope.showSubsidariesInchart($scope.showSubsidaries);
			});
		}
		$scope.showSubsidariesInchart($scope.showSubsidaries, "vlaContainer_customView");
	}//functions Ends
	/*
* @purpose: Delete the Entity from Screening
* @created: 20 MArch 2019
* @author: Ram
*/
	$scope.DeleteEntityFromScreening = function (selectedItem) {
		openDeleteConfirmationModal(selectedItem);
	}
	$scope.addOwnershipPerson = {
		"country": "",
		"firstName": "",
		"lastName": "",
		"identifier": "",
		"numberOfShares": "",
		"jurisdiction": "",
		// "source_evidence":"",
		"parentIds": [],
		"subsidiarieLink": " ",
		"title": "",
		"entity_id": "",
		"entity_ref_id": "",
		"entity_type": "person",
		"screeningFlag": '',
		"name": "",
		"screeningUrl": "",
		"id": "",
		"basic": {},
		"indirectPercentage": 0,
		"totalPercentage": 0,
		"parents": [],
		"status": "inprogress",
		"finance_Crime_url": [],
		"adverseNews_url": [],
		"addCubeIcon": '',
		"Ubo_ibo": "",
		"entityType": '',
		"from": today_date,
		"isUbo": false
	}
	reintializeEditScreening();
	function reintializeEditScreening() {
		$scope.addOwnershipCompany = {
			FalseNews: [],
			adverseNews_url: [],
			basic: {},
			country: "",
			entity_id: "",
			entity_ref_id: "",
			entity_type: "",
			finance_Crime_url: [],
			hasURL: "",
			id: "",
			identifier: "",
			indirectPercentage: '',
			name: "",
			jurisdiction: "",
			news: [],
			non_negativenews: [],
			officership: [],
			parentIds: [],
			parents: [],
			pep: [],
			pep_url: [],
			sanction_source: [],
			sanctions: [],
			screeningFlag: '',
			screeningUrl: "",
			// source_evidence: "",
			subsidiaries: [],
			title: "",
			totalPercentage: '',
			'sanction_bst:description': [],
			'Ubo_ibo': '',
			from: today_date,
			isUbo: false,
			date_of_birth: '',
			officer_role: '',
			classification: []
		};
		$scope.addOwnershipPerson.firstName = '';
		$scope.addOwnershipPerson.lastName = '';
		$scope.countryJurisdiction = {
			jurisdictionOriginalName: 'Select Country'
		};
		$scope.searchedSourceInputValCompany = '';
		$scope.searchedSourceInputValPerson = '';
		$scope.addsourceInput.searchedSourceInputCompany = "";
		$scope.addsourceInput.searchedSourceInputPerson = "";
		$scope.entitySearchResult.list.searchedSourceURLPerson = "";
		$scope.entitySearchResult.list.searchedSourceURLCompany = "";
		$scope.searchedSourceInputDateCompany = today_date;
		$scope.searchedSourceInputDatePerson = today_date;
		$scope.overrideValue = false;
		$scope.addModelClassification.personRoleModel = [];
		$scope.editModelClassification.editRoleModel = '';
		$scope.addModelClassification.companyModel = [];
		$scope.addModelClassification.personModel =[];
	}
	$scope.setUBOvalue = function (value) {
		$scope.overrideValue = value;
	}
	$scope.showNameValidation = false;
	$scope.addNewCompanyOwnership = function (status, selectedCountry, searchedSourceName) {
		if (searchedSourceName) {
			var searchedSourceObj = $scope.sourceList.find(function (value) {
				return value.sourceName == searchedSourceName;
			})
		}
		var name = $scope.entitySearchResult.list.addEntitymodalChart === 'person' ? ($scope.addOwnershipPerson.firstName + '' + ($scope.addOwnershipPerson.lastName ? (' ' + $scope.addOwnershipPerson.lastName) : '')) : $scope.addOwnershipCompany.name;
		var from = $scope.entitySearchResult.list.addEntitymodalChart === 'person' ? $scope.searchedSourceInputDatePerson : $scope.searchedSourceInputDateCompany;
		var source_evidence = $scope.entitySearchResult.list.addEntitymodalChart === 'person' ? $scope.addsourceInput.searchedSourceInputPerson : $scope.addsourceInput.searchedSourceInputCompany;
		var source = $scope.entitySearchResult.list.addEntitymodalChart === 'person' ? $scope.entitySearchResult.list.searchedSourceURLPerson : $scope.entitySearchResult.list.searchedSourceURLCompany;
		if (!status) {
			modalInstanceAdd.dismiss('close');
		}
		if (($scope.addOwnershipPerson.firstName || $scope.addOwnershipCompany.name) && status && name.trim().length > 0 && ($scope.addsourceInput.searchedSourceInputPerson || $scope.addsourceInput.searchedSourceInputCompany)) {
			modalInstanceAdd.dismiss('close');
			var idRandom = makeid();
			$scope.addOwnershipCompany.entity_type = $scope.entitySearchResult.list.addEntitymodalChart;
			$scope.addOwnershipCompany.entity_id = (entityDataOnClikOfOdg.entity_id === "orgChartsubEntity") ? "orgChartsubEntity" : "orgChartParentEntity";
			if ($scope.selectedMultipleeEntities.length > 0) {
				// for (var index = 0; index < $scope.selectedMultipleeEntities.length; index++) {
				// 	var multipleConpanyIndex = _.findIndex(subsidaryFilterData, {
				// 		identifier: $scope.selectedMultipleeEntities[index].identifier
				// 	});
				// 	subsidaryFilterData[multipleConpanyIndex].parents.push(idRandom);
				// 	// $scope.addOwnershipCompany.child = subsidaryFilterData[multipleConpanyIndex].id;
				// }
			} else {
				var addcompanyIndex = -1;
				if (deleteSelectedEntity.identifier) {
					addcompanyIndex = _.findIndex(subsidaryFilterData, {
						identifier: deleteSelectedEntity.identifier
					});
				} else {
					addcompanyIndex = _.findIndex(subsidaryFilterData, {
						name: deleteSelectedEntity.name
					});
				}
				if (subsidaryFilterData[addcompanyIndex].parents && subsidaryFilterData[addcompanyIndex].parents.length > 0) {
					subsidaryFilterData[addcompanyIndex].parents.push(idRandom);
				} else {
					subsidaryFilterData[addcompanyIndex].parents = [idRandom];
				}
				$scope.addOwnershipCompany.child = subsidaryFilterData[addcompanyIndex].id;
			}
			$scope.addOwnershipCompany.id = idRandom;
			$scope.addOwnershipCompany.name = name;
			$scope.addOwnershipCompany.title = $scope.addOwnershipCompany.name;
			$scope.addOwnershipCompany.jurisdiction = $scope.countryJurisdiction.jurisdictionName ? $scope.countryJurisdiction.jurisdictionName.toUpperCase() : '';
			$scope.addOwnershipCompany.juridiction = $scope.addOwnershipCompany.jurisdiction;
			$scope.addOwnershipCompany.country = $scope.countryJurisdiction.jurisdictionOriginalName;
			$scope.addOwnershipCompany.officer_role = $scope.addModelClassification.personRoleModel && $scope.addModelClassification.personRoleModel > 0 ? $scope.addModelClassification.personRoleModel : '';
			$scope.addOwnershipCompany.sourceUrl = source ? source : '';
			// $scope.addOwnershipCompany.from = from ? from :'';
			$scope.addOwnershipCompany.source_evidence = source_evidence ? source_evidence : '';
			if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
				$scope.addOwnershipCompany.Ubo_ibo = $scope.overrideValue === true ? "UBO " : ($scope.addOwnershipCompany.Ubo_ibo ? $scope.addOwnershipCompany.Ubo_ibo : '');
				$scope.addOwnershipCompany.isUbo = $scope.overrideValue === true ? true : false;
			}
			if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
				$scope.addOwnershipCompany.classification = $scope.addModelClassification.personModel.map(function (val) { return val.label });
			} else {
				$scope.addOwnershipCompany.classification = $scope.addModelClassification.companyModel.map(function (val) { return val.label });
			}
			var isGP = $scope.addOwnershipCompany.classification.some((val) => val === "General Partner");
			var isDirector = $scope.addOwnershipCompany.classification.some((val) => val === "Director");
			//	$scope.addOwnershipCompany.identifier = idRandom+'dum';
			$scope.pageloader.treeGraphloader = true;
			$scope.pageloader.screeningLoader = true;
			$scope.addOwnershipCompany.totalPercentage = (isGP || isDirector) ? 100 : $scope.addOwnershipCompany.totalPercentage;
			if ($scope.selectedMultipleeEntities.length > 0 || (deleteSelectedEntity && !deleteSelectedEntity.identifier)) {
				addAndSavemultiSelectEntites($scope.addOwnershipCompany, deleteSelectedEntity, searchedSourceObj);
			}
			else {
				addShareholderinGetCorporate(deleteSelectedEntity, $scope.addOwnershipCompany, 'add', addcompanyIndex, searchedSourceObj);
			}
			window.customerOutreachUpdateData(name, $scope.addOwnershipCompany.entity_type);
		} else if (!$scope.addOwnershipPerson.firstName) {
			$scope.enterFirstname = true;
		}
	}
	window.customerOutreachUpdateData = function (name, entityType) {
		var ind = customerOutreachDoc.map(function (d) {
			return d.entityType;
		}).indexOf(entityType)
		var listData = [];
		if (ind != -1) {
			listData = customerOutreachDoc[ind].requirementsList
		}
		$scope.createNewArrayReportData.push({
			'name': name,
			'recognizedEntityType': entityType,
			'document': listData
		});
	}
	function getTheentityScreningDetails(addOwnershipCompany, deleteSelectedEntity, addoredit) {
		var entity_identifier = addOwnershipCompany.identifier ? addOwnershipCompany.identifier : Math.random().toString(36).substring(5);
		addOwnershipCompany.identifier = entity_identifier;
		var country_risk = addOwnershipCompany.country ? calculateCountryRisk(addOwnershipCompany.country_of_residence || addOwnershipCompany.country) : addOwnershipCompany.jurisdiction ? calculateCountryRisk($scope.getCountryName(addOwnershipCompany.jurisdiction)) : '';
		var object = {
			"news": [],
			"@identifier": addOwnershipCompany.identifier,
			"identifier": addOwnershipCompany.identifier ? addOwnershipCompany.identifier : $scope.addOwnershipCompany.identifier,
			"level": addOwnershipCompany.level ? addOwnershipCompany.level : (deleteSelectedEntity.level),
			"jurisdiction": addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction.toLowerCase() : addOwnershipCompany.jurisdiction ? addOwnershipCompany.jurisdiction.toLowerCase() : '',
			"juridiction": addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction.toLowerCase() : addOwnershipCompany.jurisdiction ? addOwnershipCompany.jurisdiction.toLowerCase() : '',
			"parentIds": [],
			"entity_id": addOwnershipCompany.entity_id,
			"entity_ref_id": '',
			"child": addOwnershipCompany.child,
			"hasURL": addOwnershipCompany.hasURL,
			"entity_type": addOwnershipCompany.entity_type,
			"finance_Crime_url": [],
			"adverseNews_url": [],
			"screeningFlag": true,
			"name": addOwnershipCompany.name,
			"screeningUrl": addOwnershipCompany.screeningUrl ? addOwnershipCompany.screeningUrl : '',
			"id": addOwnershipCompany.id,
			"totalPercentage": addOwnershipCompany.totalPercentage,
			"indirectPercentage": addOwnershipCompany.indirectPercentage,
			"parents": addOwnershipCompany.parents ? addOwnershipCompany.parents : [],
			"status": "",
			"title": addOwnershipCompany.name,
			"Paridentifier": "",
			"sanction_source": [],
			"pep_url": [],
			"Ubo_ibo": addOwnershipCompany.Ubo_ibo ? addOwnershipCompany.Ubo_ibo : '',
			"country": (addOwnershipCompany.country && addOwnershipCompany.country.toLowerCase() !== 'select country') ? addOwnershipCompany.country : '',
			"numberOfShares": addOwnershipCompany.numberOfShares ? addOwnershipCompany.numberOfShares : '',
			"requestId": '',
			"showspinner": false,
			"no-screening": true,
			"sanction_bst:description": [],
			"childIdentifier": addOwnershipCompany.childIdentifier,
			"childLevel": addOwnershipCompany.childLevel,
			"source_evidence": addOwnershipCompany.source_evidence ? addOwnershipCompany.source_evidence : '',
			"isCustom": true,
			"sourceUrl": addOwnershipCompany.sourceUrl ? addOwnershipCompany.sourceUrl : '',
			"from": addOwnershipCompany.from ? addOwnershipCompany.from : '',
			"isUbo": addOwnershipCompany.isUbo ? addOwnershipCompany.isUbo : '',
			'bvdId': (addOwnershipCompany.identifier ? addOwnershipCompany.identifier : ''),
			'officer_role': addOwnershipCompany.officer_role ? addOwnershipCompany.officer_role : '',
			'officer_roles': addOwnershipCompany.officer_role ? [addOwnershipCompany.officer_role] : [],
			'date_of_birth': addOwnershipCompany.date_of_birth ? addOwnershipCompany.date_of_birth : '',
			'classification': addOwnershipCompany.classification ? addOwnershipCompany.classification : [],
			'report_jurisdiction_risk': country_risk,
			'officerIdentifier': addOwnershipCompany.officerIdentifier ? addOwnershipCompany.officerIdentifier : '',
			'highCredibilitySource': addOwnershipCompany.highCredibilitySource ? addOwnershipCompany.highCredibilitySource : ''
		}
		var mainscreeningIndex = _.findIndex(subsidaryFilterData, {
			'name': addOwnershipCompany.name
		});
		var tableScreeningIndex = _.findIndex($scope.screeningData, {
			'name': addOwnershipCompany.name
		});
		var actualcreeningIndex = _.findIndex($scope.actualScreeningData, {
			'name': addOwnershipCompany.name
		});
		addOwnershipCompany.officer_roles = addOwnershipCompany.officer_role ? [addOwnershipCompany.officer_role] : [];
		addOwnershipCompany.isCustom = true;
		addOwnershipCompany['no-screening'] = true;
		addOwnershipCompany['showdeleteIcon'] = true;
		addOwnershipCompany["showspinner"] = false;
		addOwnershipCompany['isChecked'] = false;
		addOwnershipCompany.screeningresultsloader = false;
		subsidaryFilterData.push(object);//////////////////////////////////////////////////////////////////////////
		EntityorgChartService.addEntityToChart(object);
		var screeningObject = new Screeningconstructor(addOwnershipCompany);
		if (tableScreeningIndex === -1) {
			$scope.screeningData.push(screeningObject);
		}
		// if(actualcreeningIndex === -1 ){
		$scope.actualScreeningData.splice(1, 0, screeningObject);
		screeningTableOriginal_custom($scope.actualScreeningData);
		// }
		$scope.showSubsidariesInchart($scope.showSubsidaries, screeningObject.level);
		$scope.pageloader.treeGraphloader = false;
		$scope.pageloader.screeningLoader = false;
		$scope.selectedMultipleeEntities = [];
	}
	function addShareholderinGetCorporate(deleteSelectedEntity, addOwnershipCompany, addoredit, addcompanyIndex, sourceSearchedObj, duplicate_entites, Index) {
		var data = [];
		var query = {};
		if (addoredit === 'add') {
			data = [{
				"entityName": addOwnershipCompany.name,
				"from": addOwnershipCompany.from ? addOwnershipCompany.from : today_date,
				// "identifier": deleteSelectedEntity.bvdId ? deleteSelectedEntity.bvdId : (deleteSelectedEntity.identifier ? deleteSelectedEntity.identifier :''),
				"jurisdiction": addOwnershipCompany.jurisdiction ? addOwnershipCompany.jurisdiction : addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction : '',
				"natureOfControl": addOwnershipCompany.entity_type,
				"entityType": addOwnershipCompany.entity_type,
				"source": addOwnershipCompany.source_evidence ? addOwnershipCompany.source_evidence : '',
				"sourceUrl": addOwnershipCompany.sourceUrl ? addOwnershipCompany.sourceUrl : '',
				"totalPercentage": addOwnershipCompany.totalPercentage ? addOwnershipCompany.totalPercentage : 0,
				"childIdentifier": deleteSelectedEntity.bvdId ? deleteSelectedEntity.bvdId : (deleteSelectedEntity.identifier ? deleteSelectedEntity.identifier : ''),
				"childLevel": deleteSelectedEntity.level ? deleteSelectedEntity.level : 0,
				"classification": addOwnershipCompany.classification && addOwnershipCompany.classification.length > 0 ? JSON.stringify(addOwnershipCompany.classification) : '',
				"dateOfBirth": addOwnershipCompany.date_of_birth ? addOwnershipCompany.date_of_birth : '',
				"hasURL": addOwnershipCompany.hasURL ? addOwnershipCompany.hasURL : '',
				"role": addOwnershipCompany.officer_role ? addOwnershipCompany.officer_role : '',
			}]
			query = {
				isSubsidiare: deleteSelectedEntity.isSubsidiare ? deleteSelectedEntity.isSubsidiare : false,
				mainIdentifier: ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
			}
		} else if (addoredit === 'edit') {
			query = {
				mainIdentifier: ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
				isSubsidiare: deleteSelectedEntity.isSubsidiare ? deleteSelectedEntity.isSubsidiare : false
			}
			if (deleteSelectedEntity && deleteSelectedEntity.childIdentifier && typeof deleteSelectedEntity.childIdentifier === "string") {
				data = [{
					"entityName": deleteSelectedEntity.name,
					"from": deleteSelectedEntity.from ? deleteSelectedEntity.from : ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2))),
					"identifier": (deleteSelectedEntity.identifier ? deleteSelectedEntity.identifier : ''),
					"jurisdiction": deleteSelectedEntity.jurisdiction ? deleteSelectedEntity.jurisdiction : '',
					"natureOfControl": addOwnershipCompany.entity_type ? addOwnershipCompany.entity_type : '',
					"childIdentifier": deleteSelectedEntity.childIdentifier ? deleteSelectedEntity.childIdentifier : '',
					"source": deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : '',
					"sourceUrl": deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : '',
					"totalPercentage": deleteSelectedEntity.totalPercentage ? deleteSelectedEntity.totalPercentage : 0,
					"childLevel": deleteSelectedEntity.childLevel ? deleteSelectedEntity.childLevel : deleteSelectedEntity.level ? deleteSelectedEntity.level - 1 : 0,
					"classification": deleteSelectedEntity.classification && deleteSelectedEntity.classification.length > 0 ? JSON.stringify(deleteSelectedEntity.classification) : '',
					"dateOfBirth": deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : '',
					"hasURL": deleteSelectedEntity.hasURL ? deleteSelectedEntity.hasURL : '',
					"role": addOwnershipCompany.officer_role ? addOwnershipCompany.officer_role : '',
				}]
			} else if (deleteSelectedEntity && deleteSelectedEntity.childIdentifier && deleteSelectedEntity.childIdentifier.length > 0) {
				data = deleteSelectedEntity.childIdentifier.map(function (multipleChild) {
					var object = {
						"entityName": deleteSelectedEntity.name,
						"from": deleteSelectedEntity.from ? deleteSelectedEntity.from : ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2))),
						"identifier": (deleteSelectedEntity.identifier ? deleteSelectedEntity.identifier : ''),
						"jurisdiction": deleteSelectedEntity.jurisdiction ? deleteSelectedEntity.jurisdiction : '',
						"natureOfControl": addOwnershipCompany.entity_type ? addOwnershipCompany.entity_type : '',
						"childIdentifier": multipleChild.identifier,
						"source": deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : deleteSelectedEntity.source ? deleteSelectedEntity.source : '',
						"sourceUrl": deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : '',
						"totalPercentage": deleteSelectedEntity.totalPercentage ? (deleteSelectedEntity.totalPercentage / deleteSelectedEntity.childIdentifier.length) : 0,
						"childLevel": multipleChild.childLevel ? multipleChild.childLevel : multipleChild.level ? multipleChild.level - 1 : 0,
						"classification": deleteSelectedEntity.classification && deleteSelectedEntity.classification.length > 0 ? JSON.stringify(deleteSelectedEntity.classification) : '',
						"dateOfBirth": deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : '',
						"hasURL": deleteSelectedEntity.hasURL ? deleteSelectedEntity.hasURL : '',
						"role": addOwnershipCompany.officer_role ? addOwnershipCompany.officer_role : '',
					}
					return object;
				});
			}
		}
		if (data.length === 0) {
			var deleteSelectedEntityfromChart = _.find(subsidariesfilteredData, { 'identifier': deleteSelectedEntity.identifier });
			if (deleteSelectedEntityfromChart && !_.isEmpty(deleteSelectedEntityfromChart) && (deleteSelectedEntityfromChart.entity_id === "orgChartmainEntity" || deleteSelectedEntityfromChart.entity_id === "orgChartParentEntity")) {
				data = [{
					"entityName": deleteSelectedEntityfromChart.name,
					"from": deleteSelectedEntity.from ? deleteSelectedEntity.from : ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2))),
					"identifier": (deleteSelectedEntityfromChart.identifier ? deleteSelectedEntityfromChart.identifier : ''),
					"jurisdiction": deleteSelectedEntity.jurisdiction ? deleteSelectedEntity.jurisdiction : '',
					"natureOfControl": deleteSelectedEntity.entity_type ? deleteSelectedEntity.entity_type : '',
					"childIdentifier": deleteSelectedEntityfromChart.childIdentifier ? deleteSelectedEntityfromChart.childIdentifier : '',
					"source": deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : '',
					"sourceUrl": deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : '',
					"totalPercentage": deleteSelectedEntity.totalPercentage ? deleteSelectedEntity.totalPercentage : 0,
					"childLevel": deleteSelectedEntityfromChart.childLevel ? deleteSelectedEntityfromChart.childLevel : deleteSelectedEntityfromChart.level ? deleteSelectedEntityfromChart.level - 1 : 0,
					"classification": deleteSelectedEntity.classification && deleteSelectedEntity.classification.length > 0 ? JSON.stringify(deleteSelectedEntity.classification) : '',
					"dateOfBirth": deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : '',
					"hasURL": deleteSelectedEntity.hasURL ? deleteSelectedEntity.hasURL : '',
					"role": deleteSelectedEntity.officer_role ? deleteSelectedEntity.officer_role : '',
				}]
			} else {
				var requestBodyOfficer = {
					"classification": deleteSelectedEntity.classification && deleteSelectedEntity.classification.length > 0 ? JSON.stringify(deleteSelectedEntity.classification) : '',
					"dateOfBirth": deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : deleteSelectedEntity.dateOfBirth ? deleteSelectedEntity.dateOfBirth : '',
					"from": deleteSelectedEntity.from ? deleteSelectedEntity.from : ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2))),
					"mainIdentifier": ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
					"officerCountry": deleteSelectedEntity.country ? deleteSelectedEntity.country : '',
					// "officerId": "string",
					"officerName": deleteSelectedEntity.name,
					"officerRole": deleteSelectedEntity.officer_role ? deleteSelectedEntity.officer_role : '',
					"source": deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : '',
					"sourceUrl": deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : '',
					'officerId': deleteSelectedEntity.officerIdentifier ? deleteSelectedEntity.officerIdentifier : ''
				}
				EntityApiService.saveUpdateEntityOfficer(requestBodyOfficer).then(function (response) {
					if (response && response.data && response.data.message) {
						var mainscreeningIndex = _.findIndex($scope.actualScreeningData, {
							'@identifier': deleteSelectedEntity.identifier
						});
						var shownScreeninIndex = _.findIndex($scope.screeningData, {//check for parent index in the screening Results
							"@identifier": deleteSelectedEntity.identifier
						});
						if (shownScreeninIndex !== -1) {
							$scope.screeningData[shownScreeninIndex].country = deleteSelectedEntity.country ? deleteSelectedEntity.country : '';
							$scope.screeningData[shownScreeninIndex].jurisdiction = deleteSelectedEntity.country ? (deleteSelectedEntity.jurisdiction ? deleteSelectedEntity.jurisdiction : '') : '';
							// $scope.screeningData[shownScreeninIndex].numberOfShares = deleteSelectedEntity.numberOfShares ? deleteSelectedEntity.numberOfShares : ($scope.screeningData[shownScreeninIndex].numberOfShares ?  $scope.screeningData[shownScreeninIndex].numberOfShares :'');
							$scope.screeningData[shownScreeninIndex].SelectedEntityType = deleteSelectedEntity.SelectedEntityType ? deleteSelectedEntity.SelectedEntityType : $scope.screeningData[shownScreeninIndex].SelectedEntityType ? $scope.screeningData[shownScreeninIndex].SelectedEntityType : '';
							$scope.screeningData[shownScreeninIndex].indirectPercentage = deleteSelectedEntity.indirectPercentage ? deleteSelectedEntity.indirectPercentage : $scope.screeningData[shownScreeninIndex].indirectPercentage ? $scope.screeningData[shownScreeninIndex].indirectPercentage : '';
							$scope.screeningData[shownScreeninIndex].totalPercentage = deleteSelectedEntity.totalPercentage ? deleteSelectedEntity.totalPercentage : $scope.screeningData[shownScreeninIndex].totalPercentage ? $scope.screeningData[shownScreeninIndex].totalPercentage : '';
							$scope.screeningData[shownScreeninIndex].hasURL = deleteSelectedEntity.hasURL ? deleteSelectedEntity.hasURL : $scope.screeningData[shownScreeninIndex].hasURL ? $scope.screeningData[shownScreeninIndex].hasURL : '';
							$scope.screeningData[shownScreeninIndex].source_evidence = deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : $scope.screeningData[shownScreeninIndex].source_evidence ? $scope.screeningData[shownScreeninIndex].source_evidence : '';
							$scope.screeningData[shownScreeninIndex].sourceUrl = deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : $scope.screeningData[shownScreeninIndex].sourceUrl ? $scope.screeningData[shownScreeninIndex].sourceUrl : '';
							$scope.screeningData[shownScreeninIndex].from = deleteSelectedEntity.from ? deleteSelectedEntity.from : $scope.screeningData[shownScreeninIndex].from ? $scope.screeningData[shownScreeninIndex].from : '';
							$scope.screeningData[shownScreeninIndex].Ubo_ibo = deleteSelectedEntity.Ubo_ibo ? deleteSelectedEntity.Ubo_ibo : $scope.screeningData[shownScreeninIndex].Ubo_ibo ? $scope.screeningData[shownScreeninIndex].Ubo_ibo : '';
							$scope.screeningData[shownScreeninIndex].date_of_birth = deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : $scope.screeningData[shownScreeninIndex].date_of_birth ? $scope.screeningData[shownScreeninIndex].date_of_birth : '';
							$scope.screeningData[shownScreeninIndex].classification = deleteSelectedEntity.classification;
							$scope.screeningData[shownScreeninIndex].officer_roles = deleteSelectedEntity.officer_role ? [deleteSelectedEntity.officer_role] : $scope.screeningData[shownScreeninIndex].officer_roles ? $scope.screeningData[shownScreeninIndex].officer_roles : [];
							// $scope.screeningData[shownScreeninIndex] =deleteSelectedEntity;
						}
						if (mainscreeningIndex !== -1) {
							$scope.actualScreeningData[mainscreeningIndex].country = deleteSelectedEntity.country ? deleteSelectedEntity.country : '';
							$scope.actualScreeningData[mainscreeningIndex].jurisdiction = deleteSelectedEntity.country ? (deleteSelectedEntity.jurisdiction ? deleteSelectedEntity.jurisdiction : '') : '';
							$scope.actualScreeningData[mainscreeningIndex].numberOfShares = deleteSelectedEntity.numberOfShares ? deleteSelectedEntity.numberOfShares : ($scope.actualScreeningData[mainscreeningIndex].numberOfShares ? $scope.actualScreeningData[mainscreeningIndex].numberOfShares : '');
							$scope.actualScreeningData[mainscreeningIndex].SelectedEntityType = deleteSelectedEntity.SelectedEntityType ? deleteSelectedEntity.SelectedEntityType : $scope.actualScreeningData[mainscreeningIndex].SelectedEntityType ? $scope.actualScreeningData[mainscreeningIndex].SelectedEntityType : '';
							$scope.actualScreeningData[mainscreeningIndex].indirectPercentage = deleteSelectedEntity.indirectPercentage ? deleteSelectedEntity.indirectPercentage : $scope.actualScreeningData[mainscreeningIndex].indirectPercentage ? $scope.actualScreeningData[mainscreeningIndex].indirectPercentage : '';
							$scope.actualScreeningData[mainscreeningIndex].totalPercentage = deleteSelectedEntity.totalPercentage ? deleteSelectedEntity.totalPercentage : $scope.actualScreeningData[mainscreeningIndex].totalPercentage ? $scope.actualScreeningData[mainscreeningIndex].totalPercentage : '';
							$scope.actualScreeningData[mainscreeningIndex].hasURL = deleteSelectedEntity.hasURL ? deleteSelectedEntity.hasURL : $scope.actualScreeningData[mainscreeningIndex].hasURL ? $scope.actualScreeningData[mainscreeningIndex].hasURL : '';
							$scope.actualScreeningData[mainscreeningIndex].source_evidence = deleteSelectedEntity.source_evidence ? deleteSelectedEntity.source_evidence : $scope.actualScreeningData[mainscreeningIndex].source_evidence ? $scope.actualScreeningData[mainscreeningIndex].source_evidence : '';
							$scope.actualScreeningData[mainscreeningIndex].sourceUrl = deleteSelectedEntity.sourceUrl ? deleteSelectedEntity.sourceUrl : $scope.actualScreeningData[mainscreeningIndex].sourceUrl ? $scope.actualScreeningData[mainscreeningIndex].sourceUrl : '';
							$scope.actualScreeningData[mainscreeningIndex].from = deleteSelectedEntity.from ? deleteSelectedEntity.from : $scope.actualScreeningData[mainscreeningIndex].from ? $scope.actualScreeningData[mainscreeningIndex].from : '';
							$scope.actualScreeningData[mainscreeningIndex].Ubo_ibo = deleteSelectedEntity.Ubo_ibo ? deleteSelectedEntity.Ubo_ibo : $scope.actualScreeningData[mainscreeningIndex].Ubo_ibo ? $scope.actualScreeningData[mainscreeningIndex].Ubo_ibo : '';
							$scope.actualScreeningData[mainscreeningIndex].date_of_birth = deleteSelectedEntity.date_of_birth ? deleteSelectedEntity.date_of_birth : $scope.actualScreeningData[mainscreeningIndex].date_of_birth ? $scope.actualScreeningData[mainscreeningIndex].date_of_birth : '';
							$scope.actualScreeningData[mainscreeningIndex].classification = deleteSelectedEntity.classification;
							$scope.actualScreeningData[mainscreeningIndex].officer_roles = deleteSelectedEntity.officer_role ? [deleteSelectedEntity.officer_role] : $scope.actualScreeningData[mainscreeningIndex].officer_roles ? $scope.actualScreeningData[mainscreeningIndex].officer_roles : [];
							// $scope.actualScreeningData[mainscreeningIndex] =$scope.popOverEditDataForOwnership;
						}
						if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
							$scope.editModelClassification.editPersonModel = [];
						} else {
							$scope.editModelClassification.editCompanyModel = [];
						}
					}
				});
				return;
			}
		}
		EntityApiService.addCustomShareHolder(query, data).then(function (shareHolderResponse) {
			if (addoredit === 'add') {
				addOwnershipCompany['identifier'] = shareHolderResponse.data.identifier;
				addOwnershipCompany.childIdentifier = (shareHolderResponse.data.childs && shareHolderResponse.data.childs.length > 0) ? shareHolderResponse.data.childs[0].identifier : '';
				addOwnershipCompany.level = shareHolderResponse.data.level;
				addOwnershipCompany.childLevel = (shareHolderResponse.data.childs && shareHolderResponse.data.childs.length > 0) ? shareHolderResponse.data.childs[0].childLevel : '';
				// if(shareHolderResponse.data.childIdentifier){
				getTheentityScreningDetails(addOwnershipCompany, deleteSelectedEntity, addoredit);
				if (duplicate_entites && duplicate_entites.length > 0) {
					var remove_duplicated_entity = duplicate_entites.splice(Index, 1);
					for (var index = 0; index < duplicate_entites.length; index++) {
						var check_childIdentifier_exist = _.find(subsidaryFilterData, { 'id': duplicate_entites[index].id });
						if (check_childIdentifier_exist) {
							var get_childIdentifier_identifier = _.find(subsidaryFilterData, { 'id': check_childIdentifier_exist.child, 'isCustom': true });
							if (get_childIdentifier_identifier) {
								duplicate_entites[index].childIdentifier = get_childIdentifier_identifier.identifier;
							} else {
								get_childIdentifier_identifier = subsidaryFilterData[0];
								duplicate_entites[index].childIdentifier = subsidaryFilterData[0].bvdId;
							}
							addShareholderinGetCorporate(get_childIdentifier_identifier, duplicate_entites[index], 'add', duplicate_entites[index].childIndex, duplicate_entites[index].searchedSourceObj, duplicate_entites, index);
							break;
						}
					}
				}
				if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
					$scope.addModelClassification.personModel = [];
				} else {
					$scope.addModelClassification.companyModel = [];
				}
				// }
			} else if (deleteSelectedEntity) {
				deleteSelectedEntity['identifier'] = shareHolderResponse.data.identifier ? shareHolderResponse.data.identifier.split("_")[0] : deleteSelectedEntity && deleteSelectedEntity['identifier'] ? deleteSelectedEntity['identifier'] : '';
				deleteSelectedEntity.childIdentifier = shareHolderResponse.data.childs;
				deleteSelectedEntity.level = shareHolderResponse.data.level;
				// getTheentityScreningDetails(deleteSelectedEntity,addOwnershipCompany);
				if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
					$scope.editModelClassification.editPersonModel = [];
				} else {
					$scope.editModelClassification.editCompanyModel = [];
				}
			} else if (addoredit === 'edit') {
				if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
					$scope.editModelClassification.editPersonModel = [];
				} else {
					$scope.editModelClassification.editCompanyModel = [];
				}
			}
		});
	}
	/*
* @purpose: Delete the Entity from Screening
* @created: 20 MArch 2019
* @author: Ram
*/
	function addAndSavemultiSelectEntites(addOwnershipCompany, deleteSelectedEntity, sourceSearchedObj) {
		var query = {
			mainIdentifier: ($scope.entitySearchResult.list.topHeaderObject && $scope.entitySearchResult.list.topHeaderObject.identifier) ? $scope.entitySearchResult.list.topHeaderObject.identifier : ($stateParams.qid ? $stateParams.qid : ''),
			isSubsidiare: deleteSelectedEntity.isSubsidiare ? deleteSelectedEntity.isSubsidiare : false
		}
		var data = $scope.selectedMultipleeEntities.map(function (multipleEntity) {
			var object = {
				"childIdentifier": multipleEntity.bvdId ? multipleEntity.bvdId : (multipleEntity.identifier ? multipleEntity.identifier : ''),
				"childLevel": multipleEntity.level ? multipleEntity.level : 0,
				"entityName": addOwnershipCompany.name,
				"entityType": addOwnershipCompany.entity_type,
				"from": addOwnershipCompany.from ? addOwnershipCompany.from : ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + (("0" + (new Date().getDate())).slice(-2))),
				//"identifier": "12345",//while edit
				"jurisdiction": addOwnershipCompany.jurisdiction ? addOwnershipCompany.jurisdiction : '',
				"natureOfControl": "SSH",
				"source": sourceSearchedObj && sourceSearchedObj.sourceName ? sourceSearchedObj.sourceName : '',
				"sourceUrl": sourceSearchedObj && sourceSearchedObj.sourceUrl ? sourceSearchedObj.sourceUrl : '',
				"totalPercentage": addOwnershipCompany.totalPercentage ? (addOwnershipCompany.totalPercentage / $scope.selectedMultipleeEntities.length) : 0,
				"classification": addOwnershipCompany.classification && addOwnershipCompany.classification.length > 0 ? JSON.stringify(addOwnershipCompany.classification) : '',
				"dateOfBirth": addOwnershipCompany.date_of_birth ? addOwnershipCompany.date_of_birth : '',
				"hasURL": addOwnershipCompany.hasURL ? addOwnershipCompany.hasURL : '',
				"role": addOwnershipCompany.officer_role ? addOwnershipCompany.officer_role : '',
			}
			return object;
		});
		EntityApiService.addCustomShareHolder(query, data).then(function (shareHolderResponse) {
			if (shareHolderResponse && shareHolderResponse.data && shareHolderResponse.data.childs && shareHolderResponse.data.childs.length > 0) {
				for (var index = 0; index < shareHolderResponse.data.childs.length; index++) {
					var multipleConpanyIndex = _.findIndex(subsidaryFilterData, {
						bvdId: shareHolderResponse.data.childs[index].identifier
					});
					if (subsidaryFilterData[multipleConpanyIndex] && subsidaryFilterData[multipleConpanyIndex].parents && subsidaryFilterData[multipleConpanyIndex].parents.length > 0) {
						subsidaryFilterData[multipleConpanyIndex].parents.push(addOwnershipCompany.id);
					} else if (subsidaryFilterData[multipleConpanyIndex]) {
						subsidaryFilterData[multipleConpanyIndex].parents = [addOwnershipCompany.id];
					}
				}
				addOwnershipCompany.identifier = shareHolderResponse.data.identifier;
				addOwnershipCompany.childIdentifier = shareHolderResponse.data.childs;
				addOwnershipCompany.level = shareHolderResponse.data.level;
				getTheentityScreningDetails(addOwnershipCompany, deleteSelectedEntity, 'add');
			}
		});
	}
	function makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 5; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	$scope.saveTheValueOfOwnership = function (status, UBOstatus) {
		//   modalInstanceEdit.dismiss();
		var sourceChanged = true;
		if (!status) {
			modalInstanceEdit.close();
			$scope.popOverEditDataForOwnership = {};
			$scope.popOverEditDataForOwnership.source_evidence1 = '';
			$scope.editModelClassification.editCompanyModel = [];
			$scope.editshowDirectIndirectPercentagePerson = true;
			$scope.editModelClassification.editPersonModel = [];
			$scope.editModelClassification.editRoleModel = '';
			return;
		}
		if ($scope.popOverEditDataForOwnership.name && status && $scope.popOverEditDataForOwnership.source_evidence) {
			$scope.popOverEditDataForOwnership.country = $scope.editCustomSelected.country;
			if (($scope.pre_popOverEditDataForOwnership.country === $scope.popOverEditDataForOwnership.country) && (parseInt($scope.pre_popOverEditDataForOwnership.totalPercentage) === parseInt($scope.popOverEditDataForOwnership.totalPercentage)) && ($scope.pre_popOverEditDataForOwnership.SelectedEntityType === $scope.popOverEditDataForOwnership.SelectedEntityType) && (parseInt($scope.pre_popOverEditDataForOwnership.indirectPercentage) === parseInt($scope.popOverEditDataForOwnership.indirectPercentage)) && ($scope.pre_popOverEditDataForOwnership.numberOfShares === $scope.popOverEditDataForOwnership.numberOfShares) && ($scope.pre_popOverEditDataForOwnership.hasURL === $scope.popOverEditDataForOwnership.hasURL) && ($scope.pre_popOverEditDataForOwnership.source_evidence === $scope.popOverEditDataForOwnership.source_evidence) && ($scope.pre_popOverEditDataForOwnership.from === $scope.popOverEditDataForOwnership.from) && ($scope.popOverEditDataForOwnership.source_evidence1 == $scope.pre_popOverEditDataForOwnership.source_evidence1) && ($scope.popOverEditDataForOwnership.classification.toString() == $scope.pre_popOverEditDataForOwnership.classification.toString())) {
				return;
			}
			modalInstanceEdit.close();
			subsidariesfilteredData = _.uniqBy(subsidariesfilteredData, 'identifier');
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = jQuery.extend(true, [], subsidariesfilteredData);
			var index = subsidariesfilteredData.map(function (d) {
				return d['identifier'];
			}).indexOf($scope.popOverEditDataForOwnership.identifier);
			var subsidary_level = $scope.popOverEditDataForOwnership.level;
			$scope.popOverEditDataForOwnership.totalPercentage = $scope.editshowDirectIndirectPercentagePerson ? $scope.popOverEditDataForOwnership.totalPercentage : 100;
			$scope.popOverEditDataForOwnership.title = $scope.popOverEditDataForOwnership.name;
			$scope.popOverEditDataForOwnership.source_evidence = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : $scope.popOverEditDataForOwnership.source_evidence1;
			$scope.popOverEditDataForOwnership.country = $scope.editCustomSelected.country && $scope.countryJurisdiction.jurisdictionOriginalName !== "Select Country" ? $scope.countryJurisdiction.jurisdictionOriginalName : "";
			$scope.popOverEditDataForOwnership.jurisdiction = $scope.popOverEditDataForOwnership.country ? $scope.countryJurisdiction.jurisdictionName : '';
			$scope.popOverEditDataForOwnership.juridiction = $scope.popOverEditDataForOwnership.country ? $scope.countryJurisdiction.jurisdictionName : '';
			$scope.popOverEditDataForOwnership.classification = [];
			$scope.popOverEditDataForOwnership.officer_role = $scope.editModelClassification && $scope.editModelClassification.editRoleModel ? $scope.editModelClassification.editRoleModel : '';
			$scope.popOverEditDataForOwnership.date_of_birth = ($scope.popOverEditDataForOwnership.date_of_birth || $scope.popOverEditDataForOwnership.dateOfBirth || $scope.popOverEditDataForOwnership['vcard:bday']) ? ($scope.popOverEditDataForOwnership.date_of_birth || $scope.popOverEditDataForOwnership.dateOfBirth || $scope.popOverEditDataForOwnership['vcard:bday']) : '';
			if ($scope.popOverEditDataForOwnership.entity_type === "organization") {
				$scope.popOverEditDataForOwnership.classification = $scope.editModelClassification.editCompanyModel && $scope.editModelClassification.editCompanyModel.length > 0 ? $scope.editModelClassification.editCompanyModel.map(function (val) { return val.label }) : [];
			} else if ($scope.popOverEditDataForOwnership.entity_type === "person") {
				$scope.popOverEditDataForOwnership.classification = $scope.editModelClassification.editPersonModel && $scope.editModelClassification.editPersonModel.length > 0 ? $scope.editModelClassification.editPersonModel.map(function (val) { return val.label }) : [];
			}
			var isGP = $scope.popOverEditDataForOwnership.classification.some((val) => val === "General Partner");
			var isDirector = $scope.popOverEditDataForOwnership.classification.some((val) => val === "Director");
			$scope.popOverEditDataForOwnership.totalPercentage = (isGP || isDirector) ? 100 : $scope.popOverEditDataForOwnership.totalPercentage;
			$scope.popOverEditDataForOwnership.Ubo_ibo = (!$scope.popOverEditDataForOwnership.Ubo_ibo || $scope.popOverEditDataForOwnership.Ubo_ibo === "UBO ") ? (UBOstatus === true ? 'UBO ' : '') : $scope.popOverEditDataForOwnership.Ubo_ibo;
			$scope.popOverEditDataForOwnership.SelectedEntityType = (!_.isEmpty($scope.entitySearchResult.list.recognized_entity) && $scope.entitySearchResult.list.recognized_entity.entityType) ? $scope.entitySearchResult.list.recognized_entity.entityType : '';
			$scope.popOverEditDataForOwnership.basic = {};
			if ($scope.popOverEditDataForOwnership && $scope.popOverEditDataForOwnership.id && index !== -1) {
				subsidaryFilterData[index].country = $scope.popOverEditDataForOwnership.country ? $scope.popOverEditDataForOwnership.country : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].jurisdiction = $scope.popOverEditDataForOwnership.country ? ($scope.popOverEditDataForOwnership.jurisdiction ? $scope.popOverEditDataForOwnership.jurisdiction : '') : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].juridiction = $scope.popOverEditDataForOwnership.country ? ($scope.popOverEditDataForOwnership.jurisdiction ? $scope.popOverEditDataForOwnership.jurisdiction : '') : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].numberOfShares = $scope.popOverEditDataForOwnership.numberOfShares ? $scope.popOverEditDataForOwnership.numberOfShares : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].SelectedEntityType = $scope.popOverEditDataForOwnership.SelectedEntityType ? $scope.popOverEditDataForOwnership.SelectedEntityType : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].indirectPercentage = $scope.popOverEditDataForOwnership.indirectPercentage ? $scope.popOverEditDataForOwnership.indirectPercentage : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].totalPercentage = $scope.popOverEditDataForOwnership.totalPercentage ? $scope.popOverEditDataForOwnership.totalPercentage : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].hasURL = $scope.popOverEditDataForOwnership.hasURL ? $scope.popOverEditDataForOwnership.hasURL : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].source_evidence = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].sourceUrl = $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].from = $scope.popOverEditDataForOwnership.from ? $scope.popOverEditDataForOwnership.from : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].Ubo_ibo = $scope.popOverEditDataForOwnership.Ubo_ibo ? $scope.popOverEditDataForOwnership.Ubo_ibo : '';//here we are assigining respective chart entity the Edited object (-_-)
				subsidaryFilterData[index].classification = $scope.popOverEditDataForOwnership.classification ? $scope.popOverEditDataForOwnership.classification : [];
				subsidaryFilterData[index].title = $scope.popOverEditDataForOwnership.name;
				subsidaryFilterData[index].name = $scope.popOverEditDataForOwnership.name;
				$scope.showSubsidariesInchart($scope.showSubsidaries, subsidary_level);
			}
			EntityorgChartService.editChartEntity($scope.popOverEditDataForOwnership);
			if ($scope.popOverEditDataForOwnership && $scope.popOverEditDataForOwnership.identifier) {
				addShareholderinGetCorporate($scope.popOverEditDataForOwnership, {}, 'edit');
			}
			var mainscreeningIndex = _.findIndex($scope.actualScreeningData, {
				'@identifier': $scope.popOverEditDataForOwnership.identifier
			});
			var shownScreeninIndex = _.findIndex($scope.screeningData, {
				'@identifier': $scope.popOverEditDataForOwnership.identifier
			});
			var ReportcompanyIndex = _.findIndex($scope.subsidiaries[0].subsidiaries, {
				'identifier': $scope.popOverEditDataForOwnership.identifier
			});
			var findEntityScreened = _.findIndex(customEntites.screeningaddition, {//check for parent index in the screening Results
				"@identifier": $scope.popOverEditDataForOwnership.identifier
			});
			if (findEntityScreened !== -1) {
				$scope.popOverEditDataForOwnership['isChecked'] = customEntites.screeningaddition[findEntityScreened]['isChecked'];
				$scope.popOverEditDataForOwnership['no-screening'] = customEntites.screeningaddition[findEntityScreened]['no-screening'];
			} else {
				$scope.popOverEditDataForOwnership['isChecked'] = false;
				$scope.popOverEditDataForOwnership['no-screening'] = true;
			}
			if (shownScreeninIndex !== -1) {
				$scope.screeningData[shownScreeninIndex].name = $scope.popOverEditDataForOwnership.name;
				$scope.screeningData[shownScreeninIndex].country = $scope.popOverEditDataForOwnership.country ? $scope.popOverEditDataForOwnership.country : $scope.screeningData[shownScreeninIndex].country ? $scope.screeningData[shownScreeninIndex].country : '';
				$scope.screeningData[shownScreeninIndex].jurisdiction = $scope.popOverEditDataForOwnership.country ? ($scope.popOverEditDataForOwnership.jurisdiction ? $scope.popOverEditDataForOwnership.jurisdiction : '') : '';
				$scope.screeningData[shownScreeninIndex].numberOfShares = $scope.popOverEditDataForOwnership.numberOfShares ? $scope.popOverEditDataForOwnership.numberOfShares : '';
				$scope.screeningData[shownScreeninIndex].SelectedEntityType = $scope.popOverEditDataForOwnership.SelectedEntityType ? $scope.popOverEditDataForOwnership.SelectedEntityType : '';
				$scope.screeningData[shownScreeninIndex].indirectPercentage = $scope.popOverEditDataForOwnership.indirectPercentage ? $scope.popOverEditDataForOwnership.indirectPercentage : '';
				$scope.screeningData[shownScreeninIndex].totalPercentage = $scope.popOverEditDataForOwnership.totalPercentage ? $scope.popOverEditDataForOwnership.totalPercentage : '';
				$scope.screeningData[shownScreeninIndex].hasURL = $scope.popOverEditDataForOwnership.hasURL ? $scope.popOverEditDataForOwnership.hasURL : '';
				sourceChanged = ($scope.screeningData[shownScreeninIndex].source_evidence) ? (($scope.popOverEditDataForOwnership.source_evidence && $scope.screeningData[shownScreeninIndex].source_evidence && $scope.popOverEditDataForOwnership.source_evidence !== $scope.screeningData[shownScreeninIndex].source_evidence) ? true : false) : true;
				$scope.screeningData[shownScreeninIndex].source_evidence = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '';
				$scope.screeningData[shownScreeninIndex].sourceUrl = $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '';
				$scope.screeningData[shownScreeninIndex].from = $scope.popOverEditDataForOwnership.from ? $scope.popOverEditDataForOwnership.from : '';
				$scope.screeningData[shownScreeninIndex].Ubo_ibo = $scope.popOverEditDataForOwnership.Ubo_ibo ? $scope.popOverEditDataForOwnership.Ubo_ibo : '';
				$scope.screeningData[shownScreeninIndex].date_of_birth = $scope.popOverEditDataForOwnership.date_of_birth ? $scope.popOverEditDataForOwnership.date_of_birth : '';
				$scope.screeningData[shownScreeninIndex].classification = $scope.popOverEditDataForOwnership.classification ? $scope.popOverEditDataForOwnership.classification : [];
				$scope.screeningData[shownScreeninIndex].officer_roles = $scope.editModelClassification && $scope.editModelClassification.editRoleModel ? [$scope.editModelClassification.editRoleModel] : $scope.screeningData[shownScreeninIndex].officer_roles ? $scope.screeningData[shownScreeninIndex].officer_roles : [];
				$scope.screeningData[shownScreeninIndex].officer_role = $scope.editModelClassification && $scope.editModelClassification.editRoleModel ? $scope.editModelClassification.editRoleModel : $scope.screeningData[shownScreeninIndex].officer_role ? $scope.screeningData[shownScreeninIndex].officer_role : '';
				$scope.screeningData[shownScreeninIndex].report_jurisdiction_risk = $scope.screeningData[shownScreeninIndex].country ? calculateCountryRisk($scope.screeningData[shownScreeninIndex].country_of_residence || $scope.screeningData[shownScreeninIndex].country) : $scope.screeningData[shownScreeninIndex].jurisdiction ? calculateCountryRisk($scope.getCountryName($scope.screeningData[shownScreeninIndex].jurisdiction)) : '';
				if ($scope.popOverEditDataForOwnership.source_evidence && $scope.popOverEditDataForOwnership.entity_type === "person") {
					if ($scope.screeningData[shownScreeninIndex].sources.length > 0) {
						var sourceIndex = $scope.screeningData[shownScreeninIndex].sources.indexOf($scope.popOverEditDataForOwnership.source_evidence);
						if (sourceIndex === -1) {
							$scope.screeningData[shownScreeninIndex].sources.push($scope.popOverEditDataForOwnership.source_evidence);
							var findofficerAlreadyExist = _.findIndex(EntityCommonTabService.basicsharedObject.totalOfficers_link, {
								name: $scope.popOverEditDataForOwnership.name,
								source: $scope.popOverEditDataForOwnership.source_evidence
							});
							if (findofficerAlreadyExist !== -1) {
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["officer_role"] = $scope.editModelClassification.editRoleModel ? $scope.editModelClassification.editRoleModel : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["country"] = $scope.popOverEditDataForOwnership.country ? $scope.popOverEditDataForOwnership.country : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["date_of_birth"] = $scope.popOverEditDataForOwnership.date_of_birth;
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["source"] = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["sourceUrl"] = $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["name"] = $scope.popOverEditDataForOwnership.name ? $scope.popOverEditDataForOwnership.name : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["from"] = $scope.popOverEditDataForOwnership.country.from ? $scope.popOverEditDataForOwnership.from : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["customSource"] = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '';
								EntityCommonTabService.basicsharedObject.totalOfficers_link[findofficerAlreadyExist]["source_url"] = $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '';
							} else {
								EntityCommonTabService.basicsharedObject.totalOfficers_link.push({
									"officer_role": $scope.editModelClassification.editRoleModel ? $scope.editModelClassification.editRoleModel : '',
									"country": $scope.popOverEditDataForOwnership.country ? $scope.popOverEditDataForOwnership.country : '',
									"date_of_birth": $scope.popOverEditDataForOwnership.date_of_birth,
									"source": $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '',
									"sourceUrl": $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '',
									"name": $scope.popOverEditDataForOwnership.name ? $scope.popOverEditDataForOwnership.name : '',
									"from": $scope.popOverEditDataForOwnership.country.from ? $scope.popOverEditDataForOwnership.from : '',
									"customSource": $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '',
									"source_url": $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '',
								})
							}
						}
					} else {
						var source = [$scope.popOverEditDataForOwnership.source_evidence];
						$scope.screeningData[shownScreeninIndex].sources = $.extend(true, [], source)
					}
					$scope.screeningData[shownScreeninIndex].information_provider = $scope.popOverEditDataForOwnership.source_evidence;
				}
				// $scope.screeningData[shownScreeninIndex] =$scope.popOverEditDataForOwnership;
			}
			if (mainscreeningIndex !== -1) {
				$scope.actualScreeningData[mainscreeningIndex].name = $scope.popOverEditDataForOwnership.name;
				$scope.actualScreeningData[mainscreeningIndex].country = $scope.popOverEditDataForOwnership.country ? $scope.popOverEditDataForOwnership.country : '';
				$scope.actualScreeningData[mainscreeningIndex].jurisdiction = $scope.popOverEditDataForOwnership.country ? ($scope.popOverEditDataForOwnership.jurisdiction ? $scope.popOverEditDataForOwnership.jurisdiction : '') : '';
				$scope.actualScreeningData[mainscreeningIndex].numberOfShares = $scope.popOverEditDataForOwnership.numberOfShares ? $scope.popOverEditDataForOwnership.numberOfShares : '';
				$scope.actualScreeningData[mainscreeningIndex].SelectedEntityType = $scope.popOverEditDataForOwnership.SelectedEntityType ? $scope.popOverEditDataForOwnership.SelectedEntityType : '';
				$scope.actualScreeningData[mainscreeningIndex].indirectPercentage = $scope.popOverEditDataForOwnership.indirectPercentage ? $scope.popOverEditDataForOwnership.indirectPercentage : '';
				$scope.actualScreeningData[mainscreeningIndex].totalPercentage = $scope.popOverEditDataForOwnership.totalPercentage ? $scope.popOverEditDataForOwnership.totalPercentage : '';
				$scope.actualScreeningData[mainscreeningIndex].hasURL = $scope.popOverEditDataForOwnership.hasURL ? $scope.popOverEditDataForOwnership.hasURL : '';
				$scope.actualScreeningData[mainscreeningIndex].source_evidence = $scope.popOverEditDataForOwnership.source_evidence ? $scope.popOverEditDataForOwnership.source_evidence : '';
				$scope.actualScreeningData[mainscreeningIndex].sourceUrl = $scope.popOverEditDataForOwnership.sourceUrl ? $scope.popOverEditDataForOwnership.sourceUrl : '';
				$scope.actualScreeningData[mainscreeningIndex].from = $scope.popOverEditDataForOwnership.from ? $scope.popOverEditDataForOwnership.from : '';
				$scope.actualScreeningData[mainscreeningIndex].Ubo_ibo = $scope.popOverEditDataForOwnership.Ubo_ibo ? $scope.popOverEditDataForOwnership.Ubo_ibo : '';
				$scope.actualScreeningData[mainscreeningIndex].date_of_birth = $scope.popOverEditDataForOwnership.date_of_birth ? $scope.popOverEditDataForOwnership.date_of_birth : '';
				$scope.actualScreeningData[mainscreeningIndex].officer_roles = $scope.editModelClassification && $scope.editModelClassification.editRoleModel ? [$scope.editModelClassification.editRoleModel] : $scope.actualScreeningData[mainscreeningIndex].officer_roles ? $scope.actualScreeningData[mainscreeningIndex].officer_roles : [];
				$scope.actualScreeningData[mainscreeningIndex].officer_role = $scope.editModelClassification && $scope.editModelClassification.editRoleModel ? $scope.editModelClassification.editRoleModel : $scope.actualScreeningData[mainscreeningIndex].officer_role ? $scope.actualScreeningData[mainscreeningIndex].officer_role : '';
				$scope.actualScreeningData[mainscreeningIndex].classification = $scope.popOverEditDataForOwnership.classification ? $scope.popOverEditDataForOwnership.classification : [];
				$scope.popOverEditDataForOwnership.numberOfShares = $scope.subsidiaries[0].subsidiaries[ReportcompanyIndex] && $scope.subsidiaries[0].subsidiaries[ReportcompanyIndex].numberOfShares ? $scope.subsidiaries[0].subsidiaries[ReportcompanyIndex].numberOfShares : '';
				$scope.actualScreeningData[mainscreeningIndex].report_jurisdiction_risk = $scope.actualScreeningData[mainscreeningIndex].country ? calculateCountryRisk($scope.actualScreeningData[mainscreeningIndex].country_of_residence || $scope.actualScreeningData[mainscreeningIndex].country) : $scope.actualScreeningData[mainscreeningIndex].jurisdiction ? calculateCountryRisk($scope.getCountryName($scope.actualScreeningData[mainscreeningIndex].jurisdiction)) : '';
				if ($scope.popOverEditDataForOwnership.source_evidence && $scope.popOverEditDataForOwnership.entity_type === "person") {
					if ($scope.actualScreeningData[mainscreeningIndex].sources.length > 0) {
						var sourceIndex = $scope.actualScreeningData[mainscreeningIndex].sources.indexOf($scope.popOverEditDataForOwnership.source_evidence);
						if (sourceIndex === -1) {
							$scope.actualScreeningData[mainscreeningIndex].sources.push($scope.popOverEditDataForOwnership.source_evidence);
						}
					} else {
						var sources = [$scope.popOverEditDataForOwnership.source_evidence];
						$scope.actualScreeningData[mainscreeningIndex].sources = $.extend(true, [], sources);
					}
					$scope.actualScreeningData[mainscreeningIndex].information_provider = $scope.popOverEditDataForOwnership.source_evidence;
				}
			}
			var ownIndex = $scope.createNewArrayReportData.findIndex(function (d) { return d.name.toLowerCase() == $scope.popOverEditDataForOwnership.name.toLowerCase() })
			if (ownIndex >= 0) {
				$scope.createNewArrayReportData[ownIndex].recognizedEntityType = $scope.popOverEditDataForOwnership.recognizedEntityType;
			}
			$scope.editModelClassification.editRoleModel = '';//empty The Role Value after submit
			if ($scope.popOverEditDataForOwnership.entity_type === "person") {
				let object = {
					"entityId": $scope.entitySearchResult.list.topHeaderObject.identifier,
					"name": $scope.popOverEditDataForOwnership.name,
					"source": $scope.popOverEditDataForOwnership.source_evidence
				};
				updateSourceRole(object);
			}
		}
		// $scope.popOverEditDataForOwnership.classification =[];
	}
	var owenshipIndex;
	var modalInstanceAdd;
	$scope.overrideValue = false;
	window.addEntityOwnership = function (fromMultiselect, typeOfEntity) {
		$scope.entitySearchResult.list.addEntitymodalChart = typeOfEntity ? typeOfEntity : 'person';
		$scope.entitySearchResult.list.addActiveTab = (typeOfEntity === 'person') ? 0 : 1;
		$scope.enterFirstname = false;
		$scope.example5customTexts = { buttonDefaultText: 'Classification' };
		reintializeEditScreening();
		if (!fromMultiselect) {
			$scope.selectedMultipleeEntities = [];
		}
		modalInstanceAdd = $uibModal.open({
			templateUrl: 'addOwnership.html',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow  bst_modal add-ownership-modal add-new-officer',
			scope: $scope
		});
		owenshipIndex = subsidariesfilteredData.map(function (d) {
			return d['identifier'];
		}).indexOf($scope.popOverEditDataForOwnership.identifier);
		setTimeout(() => {
			$("#addCompanyEntityType").mousedown(function () {
				if ($(".addPerson_CompanyClassification").children(".open").length) {
					$(".addPerson_CompanyClassification").children(".open").click();
				}
			});
			$("#addPersonRole").mousedown(function () {
				if ($(".addPerson_CompanyClassification").children(".open").length) {
					$(".addPerson_CompanyClassification").children(".open").click();
				}
			});
		}, 100);
		modalInstanceAdd.result.then(function (response) {
		}, function (reject) {
		});
	}
	$scope.openAddEntityMoal = function (frommultiselect) {
		$scope.editModelClassification.editCompanyModel = [];
		$scope.addModelClassification.companyModel = [];
		window.addEntityOwnership(frommultiselect, 'organization');
		$scope.addModelClassification.companyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
	}
	// jQuery("#vlaContainer").famDiagram(options);
	function onTemplateRender(event, data) {
		data.renderingMode = filteredApplied;
		switch (data.renderingMode) {
			case primitives.common.RenderingMode.Create:
				/* Initialize widgets here */
				break;
			case primitives.common.RenderingMode.Update:
				/* Update widgets here */
				break;
		}
		var itemConfig = data.context;
		$(data.element[0]).attr('id', itemConfig.id);
		if (itemConfig.classification) {
			itemConfig.classification = $scope.isJson(itemConfig.classification) ? JSON.parse(itemConfig.classification) : itemConfig.classification;
			var showDirectIndirectPercentage = itemConfig.classification.find(function (key) {
				return key == "Director" || key == "General Partner"
			})
		} else if (itemConfig.classification && typeof itemConfig.classification === 'string' && JSON.parse(itemConfig.classification) && (JSON.parse(itemConfig.classification)).length > 0) {
			var showDirectIndirectPercentage = JSON.parse(itemConfig.classification).find(function (key) {
				return key == "Director" || key == "General Partner"
			})
		}
		itemConfig.showDirectIndirectPercentage = showDirectIndirectPercentage ? false : true;
		if (data.templateName == "info") {
			var indirectPercentage = itemConfig.indirectPercentage ? itemConfig.indirectPercentage.toFixed(2) : '';
			var int_percentage = indirectPercentage ? parseFloat(indirectPercentage) : 0;
			var totalstringPercentage = itemConfig.totalPercentage ? itemConfig.totalPercentage.toFixed(2) : '';
			var totalPercentage = totalstringPercentage ? parseFloat(totalstringPercentage) : 0;
			if (!(data.element.find("[name=title]").hasClass("orgChartsubEntity") || data.element.find("[name=title]").hasClass("orgChartParentEntity") || data.element.find("[name=title]").hasClass("orgChartmainEntity"))) {
				var dynamicFontSize = itemConfig.title.length > 50 ? 'f-9' : itemConfig.title.length > 42 ? 'f-10' : itemConfig.title.length > 35 ? 'f-12' : 'f-14';
				data.element.find("[name=title]").text(itemConfig.title);
				data.element.find("[name=title]").addClass(itemConfig.entity_id);
				data.element.find("[name=title]").addClass(dynamicFontSize);
				if ((itemConfig.entity_type && (personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) !== -1))) {
					data.element.find("[name=title]").addClass('person_icon');
				} else {
					data.element.find("[name=title]").addClass('comapny_icon');
				}
				if (itemConfig.isConflict) {
					data.element.css('border', '2px solid #ef5350');
				}
				if ((int_percentage || int_percentage === 0 || totalPercentage || totalPercentage === 0) && (itemConfig.entity_id !== "orgChartsubEntity") && itemConfig.showDirectIndirectPercentage) {
					data.element.find("[name=ownershipPercentages]").append('<li class="progressbar-list-item pad-t0">'
						+ '<div class="progress progress-gradient-grey" style="width:100%"> <span class="p-rel z-99">' + int_percentage + '%</span>'
						+ '<div class="progress-bar z-9 progress-gradient-blue" role="progressbar" aria-valuenow="' + int_percentage + '"'
						+ 'aria-valuemin="0" aria-valuemax="100" style="width:' + int_percentage + '%">'
						+ '<span class="sr-only ">' + int_percentage + '%</span>'
						+ '</div>'
						+ '</div>'
						+ '</li>'
						+ '<li class="progressbar-list-item">'
						+ '<div class="progress progress-gradient-grey" style="width:100%"><span class="p-rel z-99">' + totalPercentage + '%</span>'
						+ ' <div class="progress-bar z-9 progress-gradient-curious-blue" role="progressbar" aria-valuenow="' + totalPercentage + '"'
						+ 'aria-valuemin="0" aria-valuemax="100" style="width:' + totalPercentage + '%">'
						+ '<span class="sr-only ">' + totalPercentage + '</span>'
						+ '</div>'
						+ '</div>'
						+ '</li>');
				}
				//	data.element.find("[name=title]").addClass("orgTooltipclass");
				// https://blackswantechnologies.atlassian.net/browse/EL-3788 as refernce 
				//below Im checking entity as person 		
				var ownershipdependency = '';
				if (itemConfig.entity_id === 'orgChartmainEntity') {
					ownershipdependency = '';
				}
				else if (itemConfig.Ubo_ibo) {
					var trim_ubo = itemConfig.Ubo_ibo.trim();
					var ubo_ibo_value = ((trim_ubo === 'UBO' && itemConfig.isUbo) ? 'UBO' : (trim_ubo === 'IBO' ? trim_ubo : ''));
					data.element.find("[name=description]").text(ubo_ibo_value);
				}
				data.element.find("[name=description]").addClass("indirectpercenOwnship");
				data.element.find("[name=description]").addClass("indirectpercenOwnship");
				if (!itemConfig.source_evidence && itemConfig.source_evidence_url) {
					data.element.find("[name=sourceevidence]").attr('href', itemConfig.source_evidence_url).text(itemConfig.source_evidence_url);
				} else if ((itemConfig.source_evidence && itemConfig.source_evidence_url) || (itemConfig.source_evidence && !itemConfig.source_evidence_url)) {
					data.element.find("[name=sourceevidence]").text(itemConfig.source_evidence);
				}
				if (itemConfig.source_evidence_url) {
					data.element.find("[name=source_evidence_url]").attr('href', itemConfig.source_evidence_url);
				} else if (!itemConfig.source_evidence_url) {
					data.element.find("[name=source_evidence_url]").css('display', 'none');
				}
			}
			if (itemConfig.addCubeIcon) {
				data.element.find("[name=title]").addClass("shareCubes");
			}
			if (itemConfig.showspinner) {
				data.element.find(".showspinner").removeClass("d-none");
			} else {
				data.element.find(".showspinner").addClass("d-none");
			}
			//Hide icon if result is false
			if (itemConfig.jurisdiction) {
				$(data.element[0]).find('span.flag-icon-squared').addClass("flag-icon-" + itemConfig.jurisdiction.toLowerCase()).css('display', 'block')
			} else if (itemConfig.basic && !_.isEmpty(itemConfig.basic) && itemConfig.basic.isDomiciledIn) {
				$(data.element[0]).find('span.flag-icon-squared').addClass("flag-icon-" + itemConfig.basic.isDomiciledIn.toLowerCase()).css('display', 'block')
			} else {
				$(data.element[0]).find('span.flag-icon-squared').css('display', 'none');
			}
			if (!itemConfig.adverseNews_url || itemConfig.adverseNews_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-newspaper-o').parent().css('display', 'none')
			} else if (itemConfig.adverseNews_url && itemConfig.adverseNews_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-newspaper-o').parent().css('display', 'block')
			}
			if (!itemConfig.finance_Crime_url || itemConfig.finance_Crime_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-gavel').parent().css('display', 'none')
			} else if (itemConfig.finance_Crime_url && itemConfig.finance_Crime_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-gavel').parent().css('display', 'block')
			}
			if (!itemConfig.pep_url || itemConfig.pep_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-street-view').parent().css('display', 'none')
			} else if (itemConfig.pep_url && itemConfig.pep_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-street-view').parent().css('display', 'block')
			}
			if (!itemConfig.high_risk_jurisdiction || itemConfig.high_risk_jurisdiction.toLowerCase() === 'low') {
				$(data.element[0]).find('ul').find('.fa-globe').parent().css('display', 'none')
			} else if (itemConfig.high_risk_jurisdiction && itemConfig.high_risk_jurisdiction.toLowerCase() !== 'low') {
				$(data.element[0]).find('ul').find('.fa-globe').parent().css('display', 'block')
			}
			if (!itemConfig.sanction_source || itemConfig.sanction_source.length === 0) {
				$(data.element[0]).find('ul').find('.fa-ban').parent().css('display', 'none')
			} else if (itemConfig.sanction_source && itemConfig.sanction_source.length > 0) {
				$(data.element[0]).find('ul').find('.fa-ban').parent().css('display', 'block')
			}
			if (itemConfig.entity_id === 'orgChartmainEntity') {
				$(data.element[0]).addClass("child-container additional-container");
			}
			else if (itemConfig['entity_id'] == 'orgChartsubEntity') { //yellow side border
				$(data.element[0]).addClass("child-container subsidery-container");
				$(data.element[0]).find(".percentageInfo").remove()
			}  //company and not UBO
			else if (itemConfig.isUbo && itemConfig.entity_id !== 'orgChartmainEntity' && (itemConfig.entity_type && personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) !== -1)) {
				$(data.element[0]).addClass("");
			}
			else if (!itemConfig.isUbo || (!itemConfig.isUbo && itemConfig.entity_id !== 'orgChartmainEntity' && (itemConfig.entity_type && personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) === -1))) { //blue side border
				$(data.element[0]).addClass("child-container");
			}
			var listindirectsubsidary;
			var listindirectshareholder;
			if (itemConfig.indirectEntity == 'indirectsubsidary') { //dashed line in direct entity
				listindirectsubsidary = itemConfig.id;
				$(data.element[0]).addClass("border-red-thin-dashed-t");
			} else if (itemConfig.indirectEntity == 'indirectshareholder') {
				listindirectshareholder = itemConfig.id;
				$(data.element[0]).addClass("border-red-thin-dashed-b");
			}
			if (listindirectsubsidary) {
				$('#' + listindirectsubsidary).addClass('border-red-thin-dashed-t')
			}
			if (listindirectshareholder) {
				$('#' + listindirectshareholder).addClass('border-red-thin-dashed-b')
			}
			if (($scope.entitySearchResult.list.currentVLArendered !== 'vlaContainer_customView' )|| ($scope.entitySearchResult.list.currentVLArendered === 'vlaContainer_customView' &&itemConfig.Ubo_ibo === "UBO " )) {
				$(data.element[0]).find(".multiparentcheck").remove()
				$(data.element[0]).find(".multicheck").remove()
			}
		}
	}
	function getInfoTemplate() {
		var result = new primitives.orgdiagram.TemplateConfig();
		result.name = "info";
		result.itemSize = new primitives.common.Size(250, 61);
		result.minimizedItemSize = new primitives.common.Size(3, 3);
		result.highlightPadding = new primitives.common.Thickness(4, 4, 4, 4);
		var itemTemplate = jQuery(
			'<div class="bp-corner-all org-chart-item bt-item-frame z-999">'
			+ '<div name="title-wrapper"  class="width-100 top-heading">'
			/* +'<i  class="icon-wrapper pad-l5 pad-t10 f-16 text-dark-grey fa fa-building-o"  />'
			+ '</i>' */
			+ '<span  class="flag-icon mar-x5 text-cream  flag-icon-squared flag-wrapper flag-pop c-pointer f-9"></span>'
			+ '<h3 name="title" class="bp-item text-overflow">'
			+ '</h3>'
			+ '<span class="square-16 mar-l10 pad-t3 p-rel showspinner d-none" style="position: absolute;    right: 5%;    bottom: 1%;    transform: translate(-5%,-1%);"><span class="custom-spinner square-16 p-rel ">'
			+ '<i class="fa fa-spinner fa-spin"></i>'
			+ '</span></span>'
			+ '<div class="bottom-details-wrapper" style="padding-left:55px;">'
			+ '<span name= "description" class="text-cream orgTooltipclass description percentageInfo"></span>'
			+ '<ul class="list-unstyled progressbar-list d-flex mar-b0"  name ="ownershipPercentages">'
			+ '</ul>'
			+ '<div class="mar-autol mar-b0  bst-checkbox-wrapper"> <div class="mar-l5 checkbox-inline"> <input  type="checkbox" class="mar-l0 d-block z-99 c-pointer op-0 multiparentcheck" /> <span class="mar-r0"> <i class="fa fa-check multicheck"></i></span> </div> </div>'
			+ '</div>'
			+ '<ul class="d-flex ai-c">'
			+ '<li>'
			+ '<i class= "fa text-cream fa-ban user-pop c-pointer">'
			+ '</i>'
			+ '</li>'
			+ '<li>'
			+ '<i class= "fa text-cream fa-street-view street-pop c-pointer ">'
			+ '</i>'
			+ '</li>'
			+ '<li>'
			+ '<i class= "fa text-cream fa-globe globe-pop c-pointer ">'
			+ '</i>'
			+ '</li>'
			+ '<li>'
			+ '<i class= "fa text-cream fa-gavel gavel-pop c-pointer ">'
			+ '</i>'
			+ '</li>'
			+ '<li>'
			+ '<i class= "fa text-cream fa-newspaper-o newspaper-pop c-pointer ">'
			+ '</i>'
			+ '</li>'
			+ '</ul>'
			+ '</div>'
			+ '</div>'
		).css({
			width: result.itemSize.width + "%",
			height: result.itemSize.height + "px"
		}).addClass("bp-item bp-corner-all bt-item-frame");
		result.itemTemplate = itemTemplate.wrap('<div>').parent().html();
		return result;
	}
	//---------------------------Finacial Tab Enhancement------------------------------>
	/*
		* @purpose: Load Stock History Widget
		* @created: 12th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
	var presentDate;
	var dateDivisionObj;
	var dataWithDate = {
		'lastOneYearDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastFiveYearDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastSixMonthDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastOneMonthDate': {
			'close_price': [],
			'open_price': [],
		},
		'lastDay': {
			'close_price': [],
			'open_price': [],
		},
		'lastFiveDay': {
			'close_price': [],
			'open_price': [],
		},
	}
	/*
		* @purpose: Load Stock History Widget On Change of selected option
		* @created: 12th Oct 2018
		* @params: Finacial data
		* @returns: none 
		* @author:varsha
		*/
	$scope.stockHistorySelectedTab_Enhance = 'lastFiveYearDate';
	$scope.stock_measures_Enhance = ['open_price', 'close_price']
	$scope.selectedstockMeasure = 'close_price'
	$scope.countryJurisdiction = {
		jurisdictionOriginalName: 'Select Country'
	};
	$scope.countrytoggleDropDown = function () {
		// $(".dropdown-toggle").dropdown();
		setTimeout(function () {
			if ($(".editClassification").children(".open").length) {
				$(".editClassification").children(".open").click();
				$("#menu1").click();
			}
		}, 10);
	}
	$scope.countrySelected = function (country, JurisdictionObject, type) {
		if (type) {
			$scope["SelectedFlagSource" + type] = JurisdictionObject.flag;
		} else {
			$scope["editSelectedFlagSource"] = JurisdictionObject.flag;
			$scope.editCustomSelected.country = country;
		}
		$scope.countryJurisdiction = JurisdictionObject;
	}
	$scope.entitySelected = function (entity) {
		$scope.entitySearchResult.list.recognized_entity = entity;
		$scope.popOverEditDataForOwnership.SelectedEntityType = entity;
	}
	$scope.htmlPopover = function (data) { return $scope.complianceCompanyDetailsModal(data); };
	var trusted = {};
	$scope.complianceCompanyDetailsModal = function (data) {
		if (!data || ((!data.primarySource || data.primarySource.length == 0) && (!data.secondarySource || data.secondarySource.length == 0))) {
			return;
		}
		var template = '';
		// Primary Source
		if (data.primarySource) {
			// data.primarySource = _.uniq(data.primarySource);
			template = '<div class="col-sm-6 pad-l0">' +
				'<div class="top-heading border-thin-dark-cream-b d-flex ai-c">' +
				'<h4>Primary</h4>' +
				'<span class="text-dark-pastal-green roboto-regular mar-r5"> <i class="fa fa-link"></i> ' + data.primarySource.length + '</span>' +
				'</div>';
			'<div class="bottom-content-wrapper">' +
				'<ul class="custom-list pad-l0 item-1 panel-scroll">';
			if (data.primarySource && data.primarySource.length > 0) {
				data.primarySource.map(function (d, index) {
					if (d) {
						if (d.source == data.source && !data.isUserData) { //index === 0 && 
							template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + getURLshort(d.sourceDisplayName || d.source) + '</span><i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i></li>'; // 
						} else {
							template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + getURLshort(d.sourceDisplayName || d.source) + '</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
						}
					}
				});
			} else {
				template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">No Sources Available</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
			}
			template = template + '</ul></div></div>';
		}
		// Secondary Source
		if (data.secondarySource) {
			// data.secondarySource = _.uniq(data.secondarySource);
			template = template + '<div class="col-sm-6 pad-r0">' +
				'<div class="top-heading border-thin-dark-cream-b d-flex ai-c">' +
				'<h4>Secondary</h4>' +
				'<span class="text-dark-cream roboto-regular mar-r5 "><i class=" fa fa-link"></i> ' + data.secondarySource.length + '</span>' +
				'</div>' +
				'<div class="bottom-content-wrapper">' +
				'<ul class="custom-list pad-l0 item-1 panel-scroll">';
			if (data.secondarySource && data.secondarySource.length > 0) {
				data.secondarySource.map(function (d, index) {
					var tickcls = '';
					if (d) {
						if (d.source == data.source && !data.isUserData) {
							tickcls = '<i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>';
						}
						template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + getURLshort(d.sourceDisplayName || d.source) + '</span>' + tickcls + '</li>';
					}
				});
			} else {
				template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">No Sources Available</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
			}
		}
		template = template + '</ul></div></div>';
		return trusted[template] || (trusted[template] = $sce.trustAsHtml(template));
	}
	/*
	* @Purpose: Short the long url
	* @Params : urldata <String>
	* @Date : 18-02-2019
	*/
	function getURLshort(urldata) {
		var Url = '';
		if (urldata) {
			Url = urldata.replace('http://', '').replace('https://', '').split(/[/?#]/)[0];
		} else if (urldata === '' && urldata !== undefined) {
			Url = 'user input';
		}
		return Url;
	}
	$scope.complianceCompanyDetailsModalforDummy = function (primary, Secondary) {
		if (!primary && !Secondary) {
			return;
		}
		// Primary Source
		var primaryLength = (primary && primary.length > 0) ? primary.length : 0;
		var secondaryLength = (Secondary && Secondary.length > 0) ? Secondary.length : 0;
		var template = '<div class="col-sm-6 pad-l0">' +
			'<div class="top-heading border-thin-dark-cream-b d-flex ai-c">' +
			'<h4>Primary</h4>' +
			'<span class="text-dark-pastal-green  roboto-regular mar-r5"> <i class="fa fa-link"></i>' + primaryLength + '</span>' +
			'</div>';
		'<div class="bottom-content-wrapper">' +
			'<ul class="custom-list pad-l0 item-1 panel-scroll">';
		if (primary && primary.length > 0) {
			primary.map(function (d, index) {
				if (index === 0) {
					template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + d + '<i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i></span></li>'; // 
				} else {
					template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + d + '</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
				}
			});
		} else {
			template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">No Sources Available</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
		}
		template = template + '</ul></div></div>';
		// Secondary Source
		template = template + '<div class="col-sm-6 pad-r0">' +
			'<div class="top-heading border-thin-dark-cream-b d-flex ai-c">' +
			'<h4>Secondary</h4>' +
			'<span class=" text-dark-cream  roboto-regular mar-r5"><i class="fa fa-link"></i> ' + secondaryLength + '</span>' +
			'</div>' +
			'<div class="bottom-content-wrapper">' +
			'<ul class="custom-list pad-l0 item-1 panel-scroll">';
		if (Secondary && Secondary.length > 0) {
			Secondary.map(function (d, index) {
				if (index === 0 && (!data.primarySource || data.primarySource.length === 0)) {
					template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + d + '<i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i></span></li>'; //
				} else {
					template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">' + d + '</span></li>'; //<i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
				}
			});
		} else {
			template = template + '<li class="d-flex ai-c"><span class="" style="word-break: break-word; white-space:pre-wrap">No Sources Available</span></li>'; // <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11 "></i>
		}
		template = template + '</ul></div></div>';
		return trusted[template] || (trusted[template] = $sce.trustAsHtml(template));
	}
		// -------------------widgets verification section start---------------------------
		(function () {
			EntityApiService.getComplianceWidgets().then(function (response) {
				$scope.entitySearchResult.complianceWidgets.companyDescription = response.data[0] ? response.data[0] : { widgetName: 'COMPANY INFORMATION' };
				$scope.entitySearchResult.complianceWidgets.companyDetails = response.data[1] ? response.data[1] : { widgetName: 'COMPANY DETAILS' };
				$scope.entitySearchResult.complianceWidgets.screeningResults = response.data[2] ? response.data[2] : { widgetName: 'SCREENING RESULTS' };
				$scope.entitySearchResult.complianceWidgets.ownershipStructure = response.data[3] ? response.data[3] : { widgetName: 'OWNERSHIP STRUCTURE' };
				$scope.entitySearchResult.complianceWidgets.countriesOperation = response.data[4] ? response.data[4] : { widgetName: 'COUNTRIES OF OPERATIO' };
				$scope.entitySearchResult.complianceWidgets.documents = response.data[5] ? response.data[5] : { widgetName: 'COMPANY DETAILS' };
				$scope.entitySearchResult.complianceWidgets.companyIdentifiers = response.data[6] ? response.data[6] : { widgetName: 'COMPANY IDENTIFIERS' };
				getAllWidgets();
			}).catch(function (erro) {
			});
		}());
	function updateOneWidget(widget, isApproved, e) {
		if (e && angular.element(e.currentTarget).prop('tagName') === 'INPUT') {
			e.stopPropagation();
		}
		var data = [];
		if (widget.approvedDate) {
			widget.isApproved = isApproved;
			data.push(widget);
		} else {
			data.push({
				'entityId': $stateParams.query,
				'widgetId': widget.widgetId,
				'isApproved': isApproved
			})
		}
		if (widget.widgetName === 'COMPANY INFORMATION') {
			$scope.pageloader.companyInfoReview = true;
		} else if (widget.widgetName === 'COMPANY DETAILS') {
			$scope.pageloader.companyDetailsReview = true;
			$scope.pageloader.companyIdentifiersReview = true;
		}
		else if (widget.widgetName === 'SCREENING RESULTS') {
			$scope.pageloader.companyResultsReview = true;
		} else if (widget.widgetName === 'OWNERSHIP STRUCTURE') {
			$scope.pageloader.companyStructureReview = true;
		} else if (widget.widgetName === 'COUNTRIES OF OPERATION') {
			$scope.pageloader.companyOperationReview = true;
		} else if (widget.widgetName === 'ASSOCIATED DOCUMENTS') {
			$scope.pageloader.companyDocumentsReview = true;
		} else if (widget.widgetName === 'COMPANY IDENTIFIERS') {
			$scope.pageloader.companyDetailsReview = true;
			$scope.pageloader.companyIdentifiersReview = true;
		}
		EntityApiService.markWidgetsVerified(data).then(function (response) {
			getAllWidgets();
		}).catch(function (error) {
			stopAllLoaders();
		});
	}
	function setPopoverMsg(widget) {
		if (widget.approvedDate) {
			$scope.entitySearchResult.complianceWidgets.popoverMsg = 'REVIEWED BY : ' + widget.userName;
			$scope.entitySearchResult.complianceWidgets.popoverDate = widget.approvedDate;
		} else {
			$scope.entitySearchResult.complianceWidgets.popoverMsg = 'Click to mark as Reviewed';
			$scope.entitySearchResult.complianceWidgets.popoverDate = '';
		}
	}
	function getAllWidgets() {
		EntityApiService.getAllWidgetsByEntityId($stateParams.query).then(function (response) {
			if (response.data.every(function (widget) {
				return widget.isApproved;
			})) {
				$scope.checkAll = true;
			} else {
				$scope.checkAll = false;
			}
			stopAllLoaders();
			response.data.forEach(function (widget) {
				if (widget.widgetName === 'COMPANY INFORMATION') {
					$scope.entitySearchResult.complianceWidgets.description = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.companyDescription = widget;
				} else if (widget.widgetName === 'COMPANY DETAILS') {
					$scope.entitySearchResult.complianceWidgets.details = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.companyDetails = widget;
				}
				else if (widget.widgetName === 'SCREENING RESULTS') {
					$scope.entitySearchResult.complianceWidgets.results = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.screeningResults = widget;
				} else if (widget.widgetName === 'OWNERSHIP STRUCTURE') {
					$scope.entitySearchResult.complianceWidgets.structure = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.ownershipStructure = widget;
				} else if (widget.widgetName === 'COUNTRIES OF OPERATION') {
					$scope.entitySearchResult.complianceWidgets.operation = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.countriesOperation = widget;
				} else if (widget.widgetName === 'ASSOCIATED DOCUMENTS') {
					$scope.entitySearchResult.complianceWidgets.isDocument = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.documents = widget;
				} else if (widget.widgetName === 'COMPANY IDENTIFIERS') {
					$scope.entitySearchResult.complianceWidgets.identifiers = widget.isApproved;
					$scope.entitySearchResult.complianceWidgets.companyIdentifiers = widget;
				}
			});
		}).catch(function (error) {
			stopAllLoaders();
		});
	};
	$scope.$on('markWidgets', function (event, flag) {
		if (flag) {
			SendRequestForMarkAll(flag);
			markAllTrue();
		} else {
			SendRequestForMarkAll(flag);
			markAllFalse();
		}
	});
	function markAllTrue() {
		$scope.entitySearchResult.complianceWidgets.description = true;
		$scope.entitySearchResult.complianceWidgets.details = true;
		$scope.entitySearchResult.complianceWidgets.results = true;
		$scope.entitySearchResult.complianceWidgets.structure = true;
		$scope.entitySearchResult.complianceWidgets.operation = true;
		$scope.entitySearchResult.complianceWidgets.isDocument = true;
		$scope.entitySearchResult.complianceWidgets.identifiers = true;
	}
	function stopAllLoaders() {
		$scope.pageloader.companyInfoReview = false;
		$scope.pageloader.companyDocumentsReview = false;
		$scope.pageloader.companyOperationReview = false;
		$scope.pageloader.companyStructureReview = false;
		$scope.pageloader.companyResultsReview = false;
		$scope.pageloader.companyDetailsReview = false;
		$scope.pageloader.companyIdentifiersReview = false;
	}
	function markAllFalse() {
		$scope.entitySearchResult.complianceWidgets.description = false;
		$scope.entitySearchResult.complianceWidgets.details = false;
		$scope.entitySearchResult.complianceWidgets.results = false;
		$scope.entitySearchResult.complianceWidgets.structure = false;
		$scope.entitySearchResult.complianceWidgets.operation = false;
		$scope.entitySearchResult.complianceWidgets.isDocument = false;
		$scope.entitySearchResult.complianceWidgets.identifiers = false;
	}
	function SendRequestForMarkAll(flag) {
		var markAllArray = ["companyDescription", "companyDetails", "screeningResults", "ownershipStructure", "documents", "countriesOperation", "companyIdentifiers"];
		angular.forEach(markAllArray, function (namesToBeChecked) {
			$scope.entitySearchResult.complianceWidgets.updateOneWidget($scope.entitySearchResult.complianceWidgets[namesToBeChecked], flag);
		})
	}
	// -------------------widgets verification section end---------------------------
	/*
	* @Purpose: Function to update the company details and company identifiers attribute values 
	* @Params : value <String>, obj<Object>, tabname<String>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
	$scope.updateCompanyDetails = function (value, obj, tabname) {
		var entityId = $scope.entityObj['@identifier'];
		var data = null, complianceKeyIdex = null;
		if (tabname == 'companyDetails') {
			data = $scope.conplianceMapKeys;
			complianceKeyIdex = $scope.conplianceMapKeys.findIndex(function (d) {
				return d.value == obj.value;
			});
			$scope.conplianceMapKeys[complianceKeyIdex].edit = !obj.edit;
			$scope.conplianceMapKeys[complianceKeyIdex].texts = value;
		} else if (tabname == 'companyIdentifiers') {
			data = $scope.conplianceIdentifiers;
			complianceKeyIdex = $scope.conplianceIdentifiers.findIndex(function (d) {
				return d.value == obj.value;
			});
			$scope.conplianceIdentifiers[complianceKeyIdex].edit = !obj.edit;
			$scope.conplianceIdentifiers[complianceKeyIdex].texts = value;
		} else if (tabname == 'companyInfo') {
			data = [$scope.companyInfo];
			complianceKeyIdex = 0;
			$scope.companyInfo.edit = !obj.edit;
			$scope.companyInfo.texts = value;
		}
		if (!$scope.ceriSearchResultObject[data[complianceKeyIdex].key]) {
			if (obj.value === 'Alias Name') {
				$scope.ceriSearchResultObject[data[complianceKeyIdex].key] = { value: [] }
			} else if (obj.value == 'Stock Exchange') {
				$scope.ceriSearchResultObject[data[complianceKeyIdex].key] = { value: '' };
			} else {
				$scope.ceriSearchResultObject[data[complianceKeyIdex].key] = { value: '' }
			}
		}
		var prev_val = $scope.ceriSearchResultObject[data[complianceKeyIdex].key].value;
		var prev_date = $scope.ceriSearchResultObject[data[complianceKeyIdex].key].modifiedOn;
		var prev_isuserdata = $scope.ceriSearchResultObject[data[complianceKeyIdex].key].isUserData;
		var dataObjVal = null;
		if (obj.value === 'Alias Name') {
			if (!$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value) {
				$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value = [];
			}
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value = []
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value.push(value);
			prev_val = JSON.stringify(prev_val);
			dataObjVal = JSON.stringify($scope.ceriSearchResultObject[data[complianceKeyIdex].key].value);
		} else if (obj.value == 'Stock Exchange') {
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value = value;
			dataObjVal = $scope.ceriSearchResultObject[data[complianceKeyIdex].key].value;
		} else {
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value = value;
			dataObjVal = $scope.ceriSearchResultObject[data[complianceKeyIdex].key].value
		}
		if (!$scope.ceriSearchResultObject[data[complianceKeyIdex].key].userValue) {
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].userValue = value;
		}
		var currentdate = new Date();
		var datetime = + currentdate.getFullYear() + "-"
			+ (currentdate.getMonth() + 1) + "-"
			+ currentdate.getDate() + " "
			+ currentdate.getHours() + ":"
			+ currentdate.getMinutes() + ":"
			+ currentdate.getSeconds() + ":"
			+ currentdate.getMilliseconds();
		var dataObj = {
			entityId: entityId,
			sourceSchema: data[complianceKeyIdex].key,
			value: dataObjVal,
			preValue: prev_val,
			isUserData: true
		}
		if (tabname == 'companyInfo') {
			$scope.pageloader.companyInfoReview = true;
		} else {
			$scope.pageloader.companyDetailsReview = true;
		}
		EntityApiService.saveEntityAttributes(dataObj).then(function (res) {
			if (tabname == 'companyInfo') {
				$scope.pageloader.companyInfoReview = false;
			} else {
				$scope.pageloader.companyDetailsReview = false;
			}
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].modifiedOn = datetime;
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].isUserData = true;
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].modifiedBy = $scope.ehubObject.fullName;
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].preValue = prev_val;
			$scope.getCaseByRiskDetails();
		}).catch(function (err) {
			if (tabname == 'companyInfo') {
				$scope.pageloader.companyInfoReview = false;
			} else {
				$scope.pageloader.companyDetailsReview = false;
			}
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].value = prev_val;
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].modifiedOn = prev_date;
			$scope.ceriSearchResultObject[data[complianceKeyIdex].key].isUserData = prev_isuserdata;
		});
		$scope.filterIndustry = null;
	}
	/*
	* @Purpose: Function to click on edit icon should enable input field
	* @Params :  obj<Object>, tabname<String>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
				// }
	$scope.editCompanyDetailsField = function (obj, tabname) {
		if (tabname == 'companyDetails') {
			let complianceKeyIdex = $scope.conplianceMapKeys.findIndex(function (d) {
				return d.value == obj.value;
			});
			$scope.conplianceMapKeys[complianceKeyIdex].edit = !obj.edit;
		}
		if (tabname == 'companyIdentifiers') {
			let complianceKeyIdex = $scope.conplianceIdentifiers.findIndex(function (d) {
				return d.value == obj.value;
			});
			$scope.conplianceIdentifiers[complianceKeyIdex].edit = !obj.edit;
		}
		if (tabname == 'companyInfo') {
			$scope.companyInfo.edit = !obj.edit;
		}
		if ($scope.ceriSearchResultObject['bst:description'] && $scope.ceriSearchResultObject['bst:description'].value == "") {
			$scope.ceriSearchResultObject['bst:description'].value = "no data";
		}
		$(".Screening_new_tooltip").css('display', 'none');
	}
	/*
	* @Purpose: Function to show the auto complete options for industry and customer type in company details on input change
	* @Params :  value<String>, compkey<String>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
	$scope.onInputCompanyDetails = function (value, compkey, event, obj, tabname) {
		let complianceKeyIdex = $scope.conplianceMapKeys.findIndex(function (d) {
			return d.key == compkey;
		});
		if (compkey == 'industryType') {
			if (complianceKeyIdex >= 0) {
				var output = [];
				angular.forEach($scope.industryListsData, function (industry) {
					if (industry.toLowerCase().indexOf(value.toLowerCase()) >= 0) {
						output.push(industry);
					}
				});
				if (!value) {
					output = null;
				}
				$scope.filterIndustry = output;
				if (event.keyCode === 13) {
					if (output && output.length > 0) {
						$scope.fillIndustryTextbox(output[0], compkey);
						$scope.updateCompanyDetails(output[0], obj, tabname);
					} else if (!output || output.length == 0 || !output[0]) {
						$scope.saveIndustryOption(obj, compkey);
						$scope.fillIndustryTextbox(value, compkey);
						$scope.updateCompanyDetails(value, obj, tabname);
					}
				}
			}
			setTimeout(function () {
				$(".custom-list.auto-complete-list").mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});
			}, 0);
		} else {
			if (event.keyCode === 13) {
				$scope.updateCompanyDetails(value, obj, tabname);
			}
		}
	}
	/*
	* @Purpose: Function to fill  the option for industry and customer type in company details on option select
	* @Params :  value<String>, valtype<String>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
	$scope.origIndustryVal = null;
	$scope.fillIndustryTextbox = function (value, valtype) {
		let complianceKeyIdex = $scope.conplianceMapKeys.findIndex(function (d) {
			return d.key == 'industryType';
		});
		if (complianceKeyIdex >= 0) {
			$scope.conplianceMapKeys[complianceKeyIdex].texts = value;
			$scope.filterIndustry = null;
			if (!$scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key]) {
				$scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key] = { value: '' }
			}
			$scope.origIndustryVal = $scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key].value;
			$scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key].value = value;
		}
	}
	/*
	* @Purpose: Function to cancel  the input field for industry and customer type in company details on click of cancel icon
	* @Params :  value<Object>, tabname<String>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
	$scope.cancelCompanyDetails = function (value, obj, tabname) {
		if (tabname == 'companyDetails') {
			if (obj.key !== 'industryType') {
				return;
			}
			let complianceKeyIdex = $scope.conplianceMapKeys.findIndex(function (d) {
				return d.key == 'industryType';
			});
			if (!$scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key]) {
				$scope.filterIndustry = null;
				return;
			}
			if (complianceKeyIdex >= 0) {
				$scope.ceriSearchResultObject[$scope.conplianceMapKeys[complianceKeyIdex].key].value = $scope.origIndustryVal;
				$scope.origIndustryVal = null;
				$scope.filterIndustry = null;
			}
		}
		if ($scope.ceriSearchResultObject['bst:description'].value == "no data") {
			$scope.ceriSearchResultObject['bst:description'].value = "";
		}
	}
	$scope.isJson = function (str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	};
	/*
	* @Purpose: Function to show the tooltip for edit icons of company details and identifiers
	* @Params :  obj<Object>, tabname<classstype>
	* @Date : 20-02-2019
	* @author: Amarjith Kumar
	*/
	$scope.changedTooltipCompanyDetailsData = function (obj, classstype) {
		var data = null;
		if (classstype == 'companyDetailsTooltip') {
			data = $scope.ceriSearchResultObject[obj.key];
		} else if (classstype == 'companyIdentifierTooltip') {
			data = $scope.ceriSearchResultObject[obj.key];
		} else if (classstype == 'companyInfoTooltip') {
			data = $scope.ceriSearchResultObject['bst:description'];
		}
		if (!data || !data.isUserData) {
			$(".Screening_new_tooltip").html('');
			return;
		}
		var user = data.modifiedBy;
		var date = data.modifiedOn;
		var originalAns = '';
		if (obj.key == "bst:aka") {
			if (!data.preValue) {
				data.preValue = data.preValue ? data.preValue : (data.valueRef ? data.valueRef : '');
			}
			originalAns = $scope.isJson(data.preValue) ? JSON.parse(data.preValue).toString() : data.preValue.toString();
		} else if (obj.key == "bst:stock_info") {
			if (!data.preValue) {
				data.preValue = {};
				data.preValue.main_exchange = '';
			}
			originalAns = data.preValue.main_exchange;
		} else {
			if (!data.preValue) {
				data.preValue = data.preValue ? data.preValue : (data.valueRef ? data.valueRef : '');
			}
			originalAns = data.preValue;
		}
		$(".Screening_new_tooltip").html('<div class="col-sm-12 pad-x0"><div class="primary-section"><p class= "mar-b5 f-12 roboto-light text-cream" style=""><b class= "" text-dark-cream">Change Date:</b> ' + date + '</p><p class= "mar-b5 roboto-light f-12 text-cream"><b class= "" text-dark-cream">Changed by:</b> ' + user + '</p><p class= "mar-b5 roboto-light f-12 text-cream"><b class= "" text-dark-cream">Original Answer:</b> ' + originalAns + '</p></div></div>');
	}
	$scope.saveIndustryOption = function (compkeys, key) {
		var data = {
			"industryName": compkeys.texts
		};
		$scope.pageloader.companyDetailsReview = true;
		EntityApiService.saveSourceIndustryList(data).then(function (res) {
			$scope.industryListsData.push(data.industryName);
			$scope.onInputCompanyDetails(data.industryName, 'industryType');
			$scope.pageloader.companyDetailsReview = false;
			$scope.getCaseByRiskDetails();
		}).catch(function (err) {
			$scope.pageloader.companyDetailsReview = false;
		})
	}
	function classtype(news, val, type) {
		var data = _.find(news, { 'classification': type });
		var news = [];
		if (data && data.count && data['_links'] && data['_links'].articles) {
			news.push({
				article: data['_links'].articles,
				name: val.name,
				entity_id: val.entity_id,
				val: val,
				Ubo_ibo: val.Ubo_ibo,
				count: data.count
			});
		}
		return news;
	}
	$scope.addEntitytabChange = function (value, divId, tabIndex) {
		if (value == 'organization') {
			$scope.addModelClassification.companyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }]; $scope.example9settings = { enableSearch: true }; $scope.example9settings = { enableSearch: true };
			$scope.entitySearchResult.list.addActiveTab = 1;
		} else {
			$scope.addModelClassification.personListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }]; $scope.personSettings = { enableSearch: true };
			$scope.addModelClassification.personRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 11, label: "Chairman" }]; $scope.personSettings = { enableSearch: true };
			$scope.entitySearchResult.list.addActiveTab = 0;
		}
		$scope.entitySearchResult.list.addEntitymodalChart = value;
		$scope.searchedSourceInputValCompany = '';
		$scope.searchedSourceInputValPerson = '';
		$scope.addsourceInput.searchedSourceInputCompany = "";
		$scope.addsourceInput.searchedSourceInputPerson = "";
		$scope.entitySearchResult.list.searchedSourceURLPerson = "";
		$scope.entitySearchResult.list.searchedSourceURLCompany = "";
		reintializeEditScreening();
	}
	/*
	* @Purpose: Function to check for Edited and deleted company from the chart
	* @Params :  obj<Object>, tabname<classstype>
	* @Date : 01-04-2019
	* @author: Ram Singh
	*/
	function isEntityEdited(checkCompany, self) {
		var isCompanydeleted = _.findIndex(customEntites.deleteEntityChart, {
			name: checkCompany.name
		});
		if (isCompanydeleted !== -1) {
			var deletedIndex = entityIndexInchart(self, checkCompany);
			self.splice(deletedIndex, 1);
			var addcompanyIndex = _.findIndex(subsidaryFilterData, {
				identifier: checkCompany.identifier
			});
			subsidaryFilterData.splice(addcompanyIndex, 1);
		};
		var isCompanyEdited = _.find(customEntites.editedEnties, {
			name: checkCompany.name
		});
		if (isCompanyEdited && !_.isEmpty(isCompanyEdited)) {
			var EditedIndex = entityIndexInchart(self, checkCompany);
			if (EditedIndex !== -1) {
				self[EditedIndex].country = isCompanyEdited.jurisdictionOriginalName !== "Select Country" ? isCompanyEdited.jurisdictionOriginalName : "";
				self[EditedIndex].jurisdiction = isCompanyEdited.jurisdiction ? isCompanyEdited.jurisdiction : (isCompanyEdited.juridiction ? isCompanyEdited.juridiction : '');
				self[EditedIndex].juridiction = isCompanyEdited.juridiction ? isCompanyEdited.juridiction : (isCompanyEdited.jurisdiction ? isCompanyEdited.jurisdiction : '');
				self[EditedIndex].indirectPercentage = isCompanyEdited.indirectPercentage ? isCompanyEdited.indirectPercentage : self[EditedIndex].indirectPercentage;
				self[EditedIndex].indirectPercentage = isCompanyEdited.indirectPercentage ? isCompanyEdited.indirectPercentage : self[EditedIndex].indirectPercentage;
				self[EditedIndex].totalPercentage = isCompanyEdited.totalPercentage ? isCompanyEdited.totalPercentage : self[EditedIndex].totalPercentage;
				self[EditedIndex].Ubo_ibo = isCompanyEdited.Ubo_ibo ? isCompanyEdited.Ubo_ibo : self[EditedIndex].Ubo_ibo;
			}
		}
		var isParentAdded = _.find(customEntites.adddeleteEntity, {
			child: checkCompany.id
		});
		if (isParentAdded && !_.isEmpty(isParentAdded)) {
			var parenrAddedto = entityIndexInchart(self, checkCompany);
			if (parenrAddedto !== -1) {
				self[parenrAddedto].parents.push(isParentAdded.id);
			}
		}
	}
	/*
	* @Purpose: Function to check the Index of Main Company
	* @Params :   >
	* @Date : 01-04-2019
	* @author: Ram Singh
	*/
	function entityIndexInchart(self, checkCompany) {
		var EditedIndex = _.findIndex(self, {
			name: checkCompany.name
		});
		return EditedIndex;
	}
	$scope.disabling = function (entitytype, whichKeyTodisable) {
		if (whichKeyTodisable === "" && entitytype === '') {
			return true;
		} else if (whichKeyTodisable === "" || entitytype === '') {
			return true;
		} else {
			return false;
		}
	}
	//below function Filters the pep and Sanction Array within the Range
	function pep_sanctionScreeningRange(minValue, maxValue, totalarray) {
		var array_in_range = totalarray.filter(function (singleObject) {
			if (singleObject.confidence && (singleObject.confidence * 100 > minValue || singleObject.confidence * 100 == minValue) && (singleObject.confidence * 100 < maxValue || singleObject.confidence * 100 == maxValue)) {
				return singleObject;
			} else {
				return;
			}
		});
		return array_in_range;
	}
	$scope.filterScreeningWithPep = function (apply) {
		var rangeFilterArray = [];
		$scope.activateScreeningEnabled = apply ? true : $scope.activateScreeningEnabled;
		$scope.entitySearchResult.list.summaryCount.pepCount = 0;
		$scope.entitySearchResult.list.summaryCount.sanctionCount = 0;
		$scope.actualScreeningData.forEach(function (filterValue) {
			if (!filterValue["no-screening"] && filterValue['isChecked']) {
				// if ((filterValue['sanction_bst:description'] && filterValue['sanction_bst:description'].length > 0) || (filterValue['pep_url'] && filterValue['pep_url'].length > 0)) {
				var sanctionFilter = [];
				var pepFilter = [];
				if (filterValue['sanction_bst:description'] && filterValue['sanction_bst:description'].length > 0) {
					sanctionFilter = pep_sanctionScreeningRange($scope.filteredChart.sanction_slider_min, $scope.filteredChart.sanction_slider_max, filterValue['sanction_bst:description']);
				}
				if (filterValue['pep_url'] && filterValue['pep_url'].length > 0) {
					pepFilter = pep_sanctionScreeningRange($scope.filteredChart.pep_slider_min, $scope.filteredChart.pep_slider_max, filterValue['pep_url']);
				};
				filterValue.address = filterValue["mdaas:RegisteredAddress"];
				filterValue.sanctions =sanctionFilter;
				filterValue.pep  = pepFilter;
				rangeFilterArray.push(new Screeningconstructor(filterValue));
				if (sanctionFilter.length > 0) {
					$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount + sanctionFilter.length;
				}
				if (filterValue.financeNews && filterValue.financeNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount + (filterValue.financeNews[0].count ? filterValue.financeNews[0].count : 0);
				}
				if (filterValue.adverseNews && filterValue.adverseNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount + (filterValue.adverseNews[0].count ? filterValue.adverseNews[0].count : 0);
				}
				if (filterValue.high_risk_jurisdiction && filterValue.high_risk_jurisdiction.toLowerCase() !== 'low') {
					$scope.entitySearchResult.list.summaryCount.highRiskCount = $scope.entitySearchResult.list.summaryCount.highRiskCount + 1;
				}
				if (pepFilter.length > 0) {
					$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount + pepFilter.length;
				}
			} else {
				filterValue.address = filterValue["mdaas:RegisteredAddress"];
				filterValue.sanctions = apply ? filterValue['sanction_bst:description'] : [];
				filterValue.pep  = apply ? filterValue['pep_url'] : [];
				filterValue.finance_Crime_url = apply ? filterValue.finance_Crime_url : [];
				filterValue.adverseNews_url =  apply ? filterValue.adverseNews_url : [];
				filterValue["no-screening"] =  apply ? filterValue["no-screening"] : true;
				rangeFilterArray.push(new Screeningconstructor(filterValue));
				var findIndexinTotalEntitiesScreened = $scope.totalScreenedEntity.findIndex(function (d) {
					return d.identifier == filterValue.identifier;
				});
				if (findIndexinTotalEntitiesScreened !== -1) {
					$scope.totalScreenedEntity.splice(findIndexinTotalEntitiesScreened, 1);
				}
			}
		});
		$scope.screeningData = $.extend(true, [], rangeFilterArray);
		$scope.summaryScreenig = $.extend(true, [], rangeFilterArray);
		//filtering the corporate structure chart
		var screened_entities = rangeFilterArray.filter(function (d) { return !d['no-screening'] })
		angular.forEach(subsidaryFilterData, function (val, key) {
			var chart_index = _.findIndex(screened_entities, function (d) { return (d.identifier == val.identifier) });
			if (chart_index !== -1) {
				val.pep_url = screened_entities[chart_index].pep_url;
				val.sanction_source = screened_entities[chart_index]['sanction_bst:description'];
				val.finance_Crime_url = screened_entities[chart_index].finance_Crime_url;
				val.adverseNews_url = screened_entities[chart_index].adverseNews_url;
			}
		});
		screeningTableOriginal_custom(rangeFilterArray);
		$scope.showSubsidariesInchart($scope.showSubsidaries);
	}
	
	/* @purpose: check the get data API already made a get sources call
			* 			 
			* @created: 17 may 2019
			* @params: type(string)
				* @returns get sources
			* @author: Amritesh */
	// 
	function apiCallGetSources() {
		EntityApiService.getSources().then(function (response) {
			$scope.sourceList = response.data.result.map(function (value) {
				if (value && value.sourceName && value.sourceName.toLowerCase() === 'website') {
					value.sourceName = $scope.ceriSearchResultObject&& $scope.ceriSearchResultObject.hasURL && $scope.ceriSearchResultObject.hasURL.value ? $scope.ceriSearchResultObject.hasURL.value : value.sourceName;
				}
				value.source_type = 'link';
				return value;
			});
			$rootScope.sourcesListsData = jQuery.extend(true, [], $scope.sourceList);
			EntityorgChartService.getllSourceDocuments().then((response) => {
				if (response && response.length > 0) {
					if (response[0].documentListsData && response[0].documentListsData.length > 0) {
						response[0].documentListsData.forEach((val) => {
							var sourceArray = $scope.sourceList.filter((searchedval) => searchedval.sourceName === val.sourceName);
							var sourceObject = sourceArray.filter((searchVal) => searchVal.type === val.type);
							if (sourceObject.length == 0) {
								$scope.sourceList.push(val);
							}
						});
					}
					if (response[1].evidenceDocumentsListsData && response[1].evidenceDocumentsListsData.length > 0) {
						response[1].evidenceDocumentsListsData.forEach((val) => {
							var sourceArray = $scope.sourceList.filter((searchedval) => searchedval.sourceName === val.sourceName);
							var sourceObject = sourceArray.filter((searchVal) => searchVal.type === val.type);
							if (sourceObject.length == 0) {
								$scope.sourceList.push(val);
							}
						});
					}
				}
			});
		})
	}
	$scope.getNewSources = function () {
		apiCallGetSources();
	}
	// function to open add source from sourceManagement
	$scope.addSource = function (type) {
		var url = EHUB_FE_API + 'sourceManagement/#!/sourceManagement';
		var newWin = window.open(url, '_blank');
		localStorage.setItem('toAdd', 'yes');
		$scope.showAddNew = false;
		$scope["searchedSourceInput" + type] = '';
		$scope["searchedSourceInputVal" + type] = '';
		$scope.popOverEditDataForOwnership.source_evidence1 = "";
	}
	$scope.sourceSearchInput = function (value, type) {
		$scope.fiteredSourceList = [];
		if (value) {
			$scope.showfiteredSourceList = true;
			angular.forEach($scope.sourceList, function (source) {
				if (source.sourceName.toLowerCase().indexOf(value.toLowerCase()) >= 0) {
					$scope.fiteredSourceList.push(source.sourceName);
				} else {
					$scope.showAddNew = true;
				}
			});
		} else {
			$scope.showAddNew = false;
			$scope["searchedSourceInput" + type] = '';
			$scope["searchedSourceInputVal" + type] = '';
			$scope["searchedSourceURL" + type] = '';
			$scope["searchedSourceInputDate" + type] = '';
		}
		if (event.keyCode === 13) {
			if ($scope.fiteredSourceList && $scope.fiteredSourceList.length > 0) {
				$scope.fillSourceSearchedInput($scope.fiteredSourceList[0], type);
				// $scope.updateCompanyDetails(output[0],obj,tabname);
			} else if ($scope.fiteredSourceList.length == 0) {
				// $scope.saveIndustryOption(obj,compkey);
				$scope.showAddNew = false;
				// $scope.updateCompanyDetails(value,obj,tabname);
			}
		}
		// 	}
		setTimeout(function () {
			$(".custom-list.auto-complete-list.searchSource").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 0);
	}
	$scope.fillSourceSearchedInput = function (value, type) {
		if (type === "edit") {
			$scope.popOverEditDataForOwnership.source_evidence = value.sourceName ? value.sourceName : '';
			$scope.popOverEditDataForOwnership.source_evidence1 = value.sourceName ? value.sourceName : '';
			$scope.popOverEditDataForOwnership.sourceUrl = value.sourceUrl ? value.sourceUrl : '';
			$scope.showfiteredSourceList = false;
		} else if (value) {
			var sourceIndex = $scope.sourceList.findIndex(function (d) {
				return d.sourceName == value;
			});
			$scope.showfiteredSourceList = false;
			$scope.addsourceInput["searchedSourceInput" + type] = $scope.sourceList[sourceIndex].sourceName;
			$scope["searchedSourceInputVal" + type] = $scope.sourceList[sourceIndex].sourceName;
			$scope.entitySearchResult.list["searchedSourceURL" + type] = $scope.sourceList[sourceIndex].sourceUrl;
			$scope.entitySearchResult.list["searchedSourceInputDate" + type] = $filter('date')(new Date(), 'yyyy-MM-dd');
		}
		//   $scope.showfiteredSourceList = !$scope.showfiteredSourceList;
	}
	// DatePicker Code
	$scope.today = function () {
		$scope.dt = new Date();
	};
	$scope.today();
	$scope.clear = function () {
		$scope.dt = null;
	};
	$scope.inlineOptions = {
		customClass: getDayClass,
		minDate: new Date(),
		showWeeks: true
	};
	$scope.dateOptions = {
		dateDisabled: disabled,
		formatYear: 'yy',
		maxDate: new Date(2020, 5, 22),
		minDate: new Date(),
		startingDay: 1
	};
	// Disable weekend selection
	function disabled(data) {
		var date = data.date,
			mode = data.mode;
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}
	$scope.toggleMin = function () {
		$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
		$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
	};
	$scope.toggleMin();
	$scope.open1 = function () {
		$scope.popup1.opened = true;
	};
	$scope.open2 = function () {
		$scope.popup2.opened = true;
	};
	$scope.setDate = function (year, month, day) {
		$scope.dt = new Date(year, month, day);
	};
	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.popup1 = {
		opened: false
	};
	$scope.popup2 = {
		opened: false
	};
	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);
	var afterTomorrow = new Date();
	afterTomorrow.setDate(tomorrow.getDate() + 1);
	$scope.events = [
		{
			date: tomorrow,
			status: 'full'
		},
		{
			date: afterTomorrow,
			status: 'partially'
		}
	];
	function getDayClass(data) {
		var date = data.date,
			mode = data.mode;
		if (mode === 'day') {
			var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
			for (var i = 0; i < $scope.events.length; i++) {
				var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
				if (dayToCheck === currentDay) {
					return $scope.events[i].status;
				}
			}
		}
		return '';
	}
	/* @purpose:Loads the Custom Charts
		* 			 
		* @created: 07 may 2019
		* @params: type(string)
	* @returns fetcher for API call is not made
		* @author: Ram Singh */
	$scope.customChartView = false;
	$scope.loadCustomChart = function (divId, tabIndex, isCreate) {
		$scope.customChartView = true;
		$scope.selectedMultipleeEntities = [];
		$scope.entitySearchResult.list.currentVLArendered = divId;
		// $scope.screeningData = jQuery.extend(true, [], $scope.actualScreeningData);//here are we assigning the screening Data of the company 
		if (isCreate) {
			//call function to create custom chart 
			$scope.isCustomAvailable = true;
			$scope.entitySearchResult.list.graphviewIndex = 1;
			createCustomChart(divId, isCreate);
		} else {
			$scope.showSubsidariesInchart(!$scope.customChartView, divId);
		}
		screeningTableOriginal_custom($scope.actualScreeningData);
	}
	function createCustomChart(divId, isCreate) {
		var addcustoms = jQuery.extend(true, [], subsidariesfilteredData);
		var duplicate_entites = [];
		var sort_duplicate_entites = [];
		addcustoms.forEach(function (val, key) {
			if (val.id != "p00" && !val.isCustom) {
				//call api to add the current entity
				val.identifier = "";
				var childIndex = _.findIndex(addcustoms, {
					id: val.child
				});
				if (childIndex !== -1) {
					val.childEntity = addcustoms[childIndex]
				};
				if (val.source_evidence) {
					val.searchedSourceObj = $scope.sourceList.find(function (value) {
						return value.sourceName == val.source_evidence;
					})
				}
				duplicate_entites.push(val);
			}
		});
		sort_duplicate_entites = duplicate_entites.sort(function (a, b) {
			return a.level - b.level;
		});
		if (sort_duplicate_entites.length > 0 && sort_duplicate_entites[0]) {
			addShareholderinGetCorporate(sort_duplicate_entites[0].childEntity, sort_duplicate_entites[0], 'add', sort_duplicate_entites[0].childIndex, sort_duplicate_entites[0].searchedSourceObj, sort_duplicate_entites, 0);
		}
		$scope.showSubsidariesInchart(!$scope.customChartView, divId);
	}
	$scope.showGraphView = function (id, tabindex) {
		$scope.entitySearchResult.list.graphviewIndex = tabindex;
		$scope.entitySearchResult.list.currentVLArendered = id;
		$scope.showSubsidariesInchart($scope.showSubsidaries, id);
		$scope.selectedMultipleeEntities = [];
	}
	$scope.sourceChangegraph = function (sourceSelected) {
		if (sourceSelected && $scope.entitySearchResult.list.intialSourceSelected !== sourceSelected) {
			var modalInstance = $uibModal.open({
				animation: true,
				controller: 'sourceChangeModalController',
				templateUrl: 'changeSourceconfirmModal.html',
				windowClass: 'custom-modal c-arrow bst_modal',
			});
			modalInstance.result.then(function (selectedItem) {
				if (selectedItem) {
					stopgetcorporateAPI();
					customEntites.adddeleteEntity = [];
					$scope.entitySearchResult.list.sourceFilterChart_not_Started = false;
					$scope.pageloader.chartFailureMessage = '';
					$scope.entitySearchResult.list.currentVLArendered = "vlaContainer";
					$scope.isCustomAvailable = false;
					$scope.customChartView = false;
					$scope.entitySearchResult.list.graphviewIndex = 0;
					$scope.entitySearchResult.list.intialSourceSelected = sourceSelected;
					$scope.entitySearchResult.list.previousSourceSelected = sourceSelected;
					$scope.pageloader.treeGraphloader = true;
					$("#generateReport").parent().addClass("c-ban");
					$("#generateReport").addClass("pe-none");
					var param = {
						identifier: $scope.entitySearchResult["@identifier"],
						url: {
							"url": $scope.entitySearchResult.org_structure_link
						},
						numnberoflevel: 5,
						lowRange: 25,
						highRange: 100,
						organisationName: $scope.ceriSearchResultObject && $scope.ceriSearchResultObject['vcard:organization-name'] && $scope.ceriSearchResultObject['vcard:organization-name'].value ? $scope.ceriSearchResultObject['vcard:organization-name'].value : totalData['vcard:organization-name'] ? totalData['vcard:organization-name'] : '',
						juridiction: $stateParams.cid ? ($stateParams.cid).toUpperCase() : $scope.ceriSearchResultObject && $scope.ceriSearchResultObject['isDomiciledIn'] && $scope.ceriSearchResultObject['isDomiciledIn'].value ? $scope.ceriSearchResultObject['isDomiciledIn'].value : totalData.isDomiciledIn ? totalData.isDomiciledIn : '',
						noOfSubsidiaries: 5,
						isSubsidiariesRequired: $scope.showSubsidaries,
						Start_date: $scope.filteredChart.toyear,
						End_date: $scope.filteredChart.fromyear,
						source: sourceSelected
					};
					EntityApiService.getOwnershipPath(param).then(function (response) {
						if (response && response.data && response.data.path) {
							$timeout(function () {
								$scope.pageloader.treeGraphloader = false;
								// tag cloud variables initializations for person
								$scope.entitySearchResult.list.sourceFilterChart_not_Started = true;
								updateOwnershipRecurssive(response.data.path, chartTimeIntreval);
							}, 17000);
						}
					}, function (err) {
						$scope.pageloader.treeGraphloader = false;
					});
					$scope.pageloader.treeGraphloader = true;
				} else {
					$scope.entitySearchResult.list.graphSourceSelected = $scope.entitySearchResult.list.previousSourceSelected;
				}
			});
		}
	}
	function stopgetcorporateAPI() {
		EntityApiService.stopGetcorporateAPI();
		var currentOPts = jQuery.extend(true, {}, options);
		currentOPts.items = [mainCompanyobject];
		$('#' + $scope.entitySearchResult.list.currentVLArendered).find('.orgdiagram .custom-scroll-wrapper').remove();
		jQuery("#" + $scope.entitySearchResult.list.currentVLArendered).famDiagram(currentOPts);
		orgChartRemake(currentOPts, $scope.entitySearchResult.list.currentVLArendered);
		$scope.pageloader.treeGraphloader = true;
	}
	$scope.toggleSourcedropDown = function (value, click) {
		if (click) {
			$scope.showfiteredSourceList = !value;
		} else {
			var filterSource = $scope.sourceList.filter(function (d) {
				return (d.sourceName.indexOf($scope.popOverEditDataForOwnership.source_evidence1) > -1);
			});
			if (filterSource && filterSource.length > 0) {
				$scope.showfiteredSourceList = true;
			} else {
				$scope.showfiteredSourceList = false;
			}
		}
	}
	$scope.dateFormatError = false;
	$scope.dateFormatValidate = function (dateString) {
		$scope.dateFormatError = false;
		var regex = /^\d{4}(\-)(((0)[0-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])$/;
		//Check whether valid yyyy-MM-dd Date Format.
		if (regex.test(dateString)) {
			$scope.dateFormatError = false;
		} else {
			$scope.dateFormatError = true;
		}
	}
	var sticky_selected_type = '';
	var sticky_selected_section = '';
	/* @purpose:To open sticky note modal popup from compliance page sections
	* 			 
	* @created: 25 may 2019
	* @params: type(string), events
* @returns none
	* @author: Amritesh*/
	$scope.stickyFilesCompliance = function (event, section) {
		event.stopPropagation();
		event.preventDefault();
		$rootScope.isFromEntitySection = true;
		$rootScope.isUploadFromEntitySection = false;
		entityClipboardSizeandPosition(event, section, 'sticky');
	}
	/* @purpose:To open Doc files modal popup from compliance page sections
	* 			 
	* @created: 25 may 2019
	* @params: type(string), events
* @returns none
	* @author: Amritesh*/
	$scope.UploadFilesCompliance = function (event, section) {
		event.stopPropagation();
		event.preventDefault();
		$rootScope.isFromEntitySection = false;
		$rootScope.isUploadFromEntitySection = true;
		entityClipboardSizeandPosition(event, section, 'file');
	}
	function entityClipboardSizeandPosition(event, section, type) {
		TopPanelApiService.clipBoardObject.file_wraper = 'clipboard_entity_file_wraper';
		$scope.$parent.showMyEntityClipboard = !TopPanelApiService.entityselection ? true : (section === TopPanelApiService.entityselection && sticky_selected_type === type) ? false : ((section === TopPanelApiService.entityselection && sticky_selected_type !== type) ? true : (section !== TopPanelApiService.entityselection && sticky_selected_type !== type ? true : false));
		if (!TopPanelApiService.entityselection || (sticky_selected_type !== type && section === TopPanelApiService.entityselection)) {
			setTimeout(function () {
				$scope.$apply(function () {
					$scope.$parent.showMyEntityClipboard = true;
				});
			}, 0);
			TopPanelApiService.entityselection = section;
		} else if (section !== TopPanelApiService.entityselection) {
			$scope.$parent.showMyEntityClipboard = false;
			TopPanelApiService.entityselection = section;
			setTimeout(function () {
				$scope.$apply(function () {
					$scope.$parent.showMyEntityClipboard = true;
				});
			}, 0);
		} else if (section === TopPanelApiService.entityselection && sticky_selected_type === type) {
			setTimeout(function () {
				$scope.$apply(function () {
					$scope.$parent.showMyEntityClipboard = false;
				});
			}, 0);
			sticky_selected_type = '';
			sticky_selected_section = '';
			TopPanelApiService.entityselection = '';
		} else {
			$scope.$parent.showMyEntityClipboard = false;
			sticky_selected_type = '';
			sticky_selected_section = '';
			TopPanelApiService.entityselection = '';
		}
		sticky_selected_type = type;
		if ($("#entityWrapper").width() && $("#entityWrapper").width() > 0) {
			TopPanelApiService.clipBoardObject.modal_postion.top = (event.pageY + 262) + 'px';
			TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 270) + 'px';
			var p = $(".entity-page")
			var position = p.offset();
			var tooltipWidth = $("#entityWrapper").width() + 50
			var cursor = event.pageX;
			if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
				TopPanelApiService.clipBoardObject.modal_postion.top = (position.top + 262) + 'px';
				TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 30 - ($("#entityWrapper").width() / 2)) + 'px';
			} else {
				TopPanelApiService.clipBoardObject.modal_postion.top = (position.top + 262) + 'px';
				TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 20) + 'px';
			}
		} else {
			setTimeout(function () {
				var p = $(".entity-page")
				var position = p.offset();
				var tooltipWidth = $("#entityWrapper").width() + 50;
				TopPanelApiService.clipBoardObject.modal_postion.top = (position.top + 262) + 'px';
				TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 270) + 'px';
				var cursor = event.pageX;
				if ((position.left < event.pageX) && (cursor > tooltipWidth)) {
					TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 30 - ($("#entityWrapper").width() / 2)) + 'px';
				} else {
					TopPanelApiService.clipBoardObject.modal_postion.left = (event.pageX - 20) + 'px';
				}
			}, 0);
		}
	}
	/* @purpose:To show http value as link in Report
		* 			 
		* @created: 6 June 2019
		* @params: type(string)
	    * @returns URL.
		* @author: Amritesh*/
	$scope.urlify = function (text) {
		if (text) {
			if (text.indexOf("href=") !== -1 && text.indexOf("target=") !== -1) {
				return text;
			}
		}
		// or alternatively
		// return text.replace(urlRegex, '<a href="$1">$1</a>')
	}
	/* @purpose:To map the country based on country code in the Report
	* 			 
	* @created: 7 June 2019
	* @params: type(string)
		* @returns country name.
	* @author: Amarjith*/
	$scope.getCountryName = function (countryCode, fromNode) {
		if (countryCode.toLowerCase() === 'us') {
			countryname = 'United States Of America';
			if (fromNode) {
				countryname = 'United State';
			}
			return countryname;
		}
		var curObj = $scope.countryNames.find(obj => {
			return obj.jurisdictionName.toLowerCase() === countryCode.toLowerCase();
		});
		if (curObj) {
			var countryname = curObj.jurisdictionOriginalName ? curObj.jurisdictionOriginalName : '';
			return countryname;
		}
	}
	/* @purpose:To Call API take screenshot	on click of icon /single	 
	* @created: 7 June 2019
	* @params: type(object)
		* @returns none.
	* @author: Amritesh*/
	$scope.callScreenShotAPI = function (SourceDataObj, index, uploadedFileName) {
		if (!uploadedFileName) {
			$scope.sourceWithBSTRegistry[index].row_spinner = true;
			EntityApiService.APIgetScreenShot(SourceDataObj).then(function (response) {
				if (response && response.data) {
					SourceDataObj.SourceValue.source_screenshot = response.data.screen_url;
					SourceDataObj.checked = false;
					SourceDataObj.showHideScreenIcon = true;
					$scope.sourceWithBSTRegistry[index].showHideAddtoPage = true;
					$scope.sourceWithBSTRegistry[index].row_spinner = false;
					$scope.sourceWithBSTRegistry[index].disable = true;
					$scope.setMultiCheckScreenshot(SourceDataObj);
					$scope.isFromAddToPage = true;
					return false;
				}
			}, function (err) {
				$scope.sourceWithBSTRegistry[index].row_spinner = false;
			})
		} else {
			TopPanelApiService.deleteSourceAddtoPage(SourceDataObj.sourceName, $rootScope.entityObj["@identifier"]).then(function (response) {
				$scope.sourceWithBSTRegistry[index].showHideScreenIcon = false;
				$scope.sourceWithBSTRegistry[index].uploadedFileName = "";
				$scope.sourceWithBSTRegistry[index].downloadLink = "";
				$scope.sourceWithBSTRegistry[index].docid = "";
				$scope.sourceWithBSTRegistry[index].showUploadIcon = true;
				saveAddtoPage(SourceDataObj, index)
			}, function (error) {
			}).catch(function (err) {
			})
		}
	}
	function saveAddtoPage(params, index) {
		var addtopagedata = [{
			"entityId": $rootScope.entityObj["@identifier"],
			"isAddToPage": false,
			"sourceName": params.sourceName
		}];
		TopPanelApiService.saveSourcesAddToPage(addtopagedata).then(function (res) {
			$scope.sourceWithBSTRegistry[index].showHideAddtoPage = false;
		}, function (err) {
		}).catch(function () {
		})
	}
	/* @purpose:To Call API take screenshot	on click of icon /single	 
	* @created: 7 June 2019
	* @params: type(object)
		* @returns none.
	* @author: Amritesh*/
	$scope.downloadEvidenceSourceScreenshot = function (value) {
		var name = value['uploadedFileName'] ? value['uploadedFileName'] : value['sourceName'] + ".png";
		var url = value['SourceValue'].source_file_url ? value['SourceValue'].source_file_url : value['SourceValue'].source_screenshot;
		value.docid ? $scope.downloadDocFromScreenShot(value.docid, value['uploadedFileName'], value['uploadedFileName'].split('.')[1]) : forceDownloadImg(url, name, event);
	}
	/* @purpose:function to downlod the images with url	 
	* @created: 12 June 2019
	* @params: type(object)
	* @returns none.
	* @author: Amritesh*/
	function forceDownloadImg(url, fileName) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.responseType = "blob";
		xhr.onload = function () {
			var urlCreator = window.URL || window.webkitURL;
			var imageUrl = urlCreator.createObjectURL(this.response);
			var tag = document.createElement('a');
			tag.href = imageUrl;
			tag.download = fileName;
			document.body.appendChild(tag);
			tag.click();
			document.body.removeChild(tag);
		}
		xhr.send();
	}
	/* @purpose:To take Image on Icon click 
* @created: 7 June 2019
* @params: type(object)
 * @returns none.
* @author: Amritesh*/
	$scope.addToPage = function (SourceDataObj, index, fromShareholderModal) {
		// $scope.isFromAddToPage = true;
		return new Promise(function (resolve, reject) {
			var file_ext_invalid = false;
			var file_ext_type = '';
			var index_of_point;
			var split_file_type;
			if (!SourceDataObj.uploadedFileName) {
				index_of_point = SourceDataObj.SourceValue.source_screenshot.lastIndexOf('.');
				split_file_type = SourceDataObj.SourceValue.source_screenshot.slice((index_of_point + 1), SourceDataObj.SourceValue.source_screenshot.length);
			} else if (SourceDataObj.uploadedFileName) {
				index_of_point = SourceDataObj.uploadedFileName.lastIndexOf('.');
				split_file_type = SourceDataObj.uploadedFileName.slice((index_of_point + 1), SourceDataObj.uploadedFileName.length);
			}
			if (split_file_type && !$rootScope.uploadDocTypeList.includes(split_file_type)) {
				HostPathService.FlashErrorMessage('ERROR', (split_file_type.toUpperCase() + ' is not supported, Please check System Settings'));
				return;
			}
			var data;
			$scope.evidenceReportAddTopage.push(SourceDataObj.SourceValue)
			TopPanelApiService.getEvidenceArray = $scope.evidenceReportAddTopage;
			var current_srcIndex = $scope.sourceWithBSTRegistry.findIndex(function (d) {
				return d.sourceName && d.sourceName === SourceDataObj.sourceName
			});
			if (SourceDataObj.downloadLink) {
				data = {
					"token": $rootScope.ehubObject.token,
					'docID': SourceDataObj.docid,
					"fileTitle": SourceDataObj.uploadedFileName,
					"remarks": '',
					"docFlag": 6,
					"entityId": $rootScope.entityObj["@identifier"],
					"entitySource": SourceDataObj.sourceName
				}
			} else {
				data = {
					"token": $rootScope.ehubObject.token,
					"url": encodeURIComponent(SourceDataObj.SourceValue.source_screenshot),
					"fileTitle": SourceDataObj.sourceName,
					"remarks": '',
					"docFlag": 6,
					"entityId": $rootScope.entityObj["@identifier"],
					"entityName": $rootScope.entityObj.name,
					"entitySource": SourceDataObj.sourceName
				}
			}
			var addtopagedata = [{
				"entityId": $rootScope.entityObj["@identifier"],
				"isAddToPage": true,
				"sourceName": SourceDataObj.sourceName
			}];
			TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value = SourceDataObj;
			TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex = current_srcIndex;
			if (current_srcIndex !== -1) {
				if (!$scope.sourceWithBSTRegistry[current_srcIndex].addtopageloader) {
					$scope.sourceWithBSTRegistry[current_srcIndex].addtopageloader = true;
				}
			}
			setTimeout(() => {
				TopPanelApiService.saveSourcesAddToPage(addtopagedata).then(function (res) {
					TopPanelApiService.uploadDocumentByScreenShotURLData(data).then(function (response) {
						//$uibModalInstance.close($scope.uploadDocumentFileTitle);
						$scope.addMediaPreloader = false;
						if (fromShareholderModal) {
							return "true";
						}
						if (current_srcIndex !== -1) {
							$scope.sourceWithBSTRegistry[current_srcIndex].addtopageloader = false;
							if (!fromShareholderModal) {
								$scope.sourceWithBSTRegistry[current_srcIndex].showHideAddtoPage = true;
							}
						}
						resolve(true);
					}, function () {
						//$uibModalInstance.dismiss($scope.uploadDocumentFileTitle);
						$scope.addMediaPreloader = false;
						resolve(true);
						HostPathService.FlashErrorMessage('ERROR', 'Send valid data');
					});
				}, function (err) {
					resolve(true);
				}).catch(function () {
					reject(true);
				})
			}, 100);
		});
	}
	/* @purpose: To setMultiCheckScreenshot			 
	* @created: 7 June 2019
	* @params: data
		* @returns null
	* @author: Amarjith*/
	$scope.setMultiCheckScreenshot = function (data) {
		if (data.checked) {
			$scope.getMultiScreenShotSelected.push(data);
			checkEvidenceButtonShowHide($scope.getMultiScreenShotSelected);
		} else {
			var findSrcIndex = $scope.getMultiScreenShotSelected.findIndex(function (d) {
				return d.sourceName == data.sourceName;
			});
			if (findSrcIndex !== -1) {
				$scope.getMultiScreenShotSelected.splice(findSrcIndex, 1);
			}
			if ($scope.getMultiScreenShotSelected.length === 0) {
				$scope.isFromAddToPage = false;
			}
			checkEvidenceButtonShowHide($scope.getMultiScreenShotSelected);
		}
	}
	/* @purpose: To get evidence for multi-select		 
	* @created: 11 June 2019
	* @params: selectEvidentArray(array)
		* @returns null
	* @author: Amritesh*/
	$scope.getMultiEvidence = function (selectEvidentArray) {
		$scope.sourceWithBSTRegistry = $scope.sourceWithBSTRegistry.map(function (el) {
			if (el.checked == true) {
				el.showHideScreenIcon = true;
				el.showHideAddtoPage = true;
			}
			return el
		})
		$scope.isFromAddToPage = true;
	};
	/* @purpose: To Add scroll and delete value from clipboard through my entityclipboard  controller to entity controller
	* @created: 12 June 2019
	* @params: none
		* @returns null
	* @author: Amritesh*/
	$scope.addScrollToEvidencePopup = function () {
		$(".popover-inner").parent().removeClass("d-none");
		if (TopPanelApiService.uploadFromScreenShotPopupUploadIcon && TopPanelApiService.uploadFromScreenShotPopupUploadIcon.hasOwnProperty('deletedSrcObj')) {
			$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideScreenIcon = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showHideScreenIcon;
			$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].uploadedFileName = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.uploadedFileName;
			$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].downloadLink = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.downloadLink;
			$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showUploadIcon = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showUploadIcon;
			$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideAddtoPage = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showHideAddtoPage;
		}
		$scope.$parent.showMyEntityClipboard = false
		setTimeout(function () {
			$('.changed-content-wrapper,.table-scroll-evidence, .original-content-wrapper, .entity-overRiding-modal').mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		})
		if (TopPanelApiService.spliceValue.length > 0) {
			var deleteScreenShot = TopPanelApiService.spliceValue
			var findDelIndex = $scope.sourceWithBSTRegistry.findIndex(function (d) {
				return d.sourceName == deleteScreenShot[0].source;
			});
			var findDelIndexForReport = $scope.evidenceReportAddTopage.findIndex(function (d) {
				return d.source == deleteScreenShot[0].source;
			});
			if (findDelIndexForReport !== -1) {
				$scope.evidenceReportAddTopage.splice(findDelIndexForReport, 1)
				TopPanelApiService.getEvidenceArray = $scope.evidenceReportAddTopage;
			}
			if (findDelIndex !== -1) {
				$scope.sourceWithBSTRegistry[findDelIndex].showHideAddtoPage = true;
			}
		}
		event.preventDefault();
		event.stopPropagation();
	}
	/* @purpose: To Add scroll and delete value through my entityclipboard  controller to ent 
	* @created: 12 June 2019
	* @params: none
		* @returns null
	* @author: Amritesh*/
	$scope.deleteEvidenceSourceScreenshot = function (index) {
		$scope.sourceWithBSTRegistry[index].showHideScreenIcon = false;
		$scope.sourceWithBSTRegistry[index]['SourceValue'].source_screenshot = undefined;
		$scope.sourceWithBSTRegistry[index].showHideAddtoPage = false;
		var findDelIndexForReport = $scope.evidenceReportAddTopage.findIndex(function (d) {
			return d.source == $scope.sourceWithBSTRegistry[index]['SourceValue'].source;
		});
		if (findDelIndexForReport !== -1) {
			$scope.evidenceReportAddTopage.splice(findDelIndexForReport, 1)
			TopPanelApiService.getEvidenceArray = $scope.evidenceReportAddTopage;
		}
		event.preventDefault();
		event.stopPropagation();
	}
	/* @purpose: To check Evidenc eButton Show Hide
* @created: 12 June 2019
* @params: Arrayvalue(array)
	* @returns null
* @author: Amritesh*/
	function checkEvidenceButtonShowHide(Arrayvalue) {
		if (Arrayvalue.length > 0) {
			if (!Arrayvalue.some(checkScreen_urlExistence)) {
				$scope.isFromAddToPage = true;
			} else {
				$scope.isFromAddToPage = false;
			}
		}
	}
	/* @purpose: To check whether Screen_url exist or not
		* @created: 12 June 2019
		* @params: data(obj)
	    * @returns null
		* @author: Amritesh*/
	function checkScreen_urlExistence(data) {
		return data.SourceValue.source_screenshot === undefined;
	}
	/* @purpose: For multiple add to page
	* @created: 12 June 2019
	* @params: none
		* @returns null
	* @author: Amritesh*/
	$scope.multipleAddToPage = function () {
		angular.forEach($scope.getMultiScreenShotSelected, function (values, index) {
			$scope.addToPage(values, index);
		});
		$scope.getMultiScreenShotSelected = [];
		$scope.isFromAddToPage = false;
	}
	// query popover
	$scope.myPopover = {
		isOpen: false,
		templateUrl: 'sourceCountForEntities.html',
		open: function open() {
			$scope.myPopover.isOpen = true;
		},
		close: function close() {
			$scope.myPopover.isOpen = false;
		}
	};
	/* @purpose: For Deselect Entities
	* @created: 14 June 2019
	* @params: none
		* @returns null
	* @author: Amritesh*/
	$scope.deselectEntities = function () {
		$scope.sourceWithBSTRegistry.map(function (mainObj) {
			if (mainObj.checked == true) {
				mainObj.checked = false;
			}
		})
		$scope.isFromAddToPage = false;
		$scope.getMultiScreenShotSelected = [];
	}
	/* @purpose: For select All Entities
	* @created: 14 June 2019
	* @params: none
		* @returns null
	* @author: Amritesh*/
	$scope.selectAllEntities = function () {
		$scope.getMultiScreenShotSelected = $scope.sourceWithBSTRegistry.map(function (mainObj) {
			if (!mainObj.disable) {
				$scope.getMultiScreenShotSelected.push(mainObj);
				return mainObj.checked = true;
			}
		});
		//removing undefind values
		$scope.getMultiScreenShotSelected = $scope.getMultiScreenShotSelected.filter(function (e) { return e });
		$scope.isFromAddToPage = false;
	}
	$('body').on('click', "#flowChartViewDiv1 #btn50", function () {
		onScale(0.5);
	})
	$('body').on('click', "#flowChartViewDiv1 #btn100", function () {
		onScale(1);
	});
	$('body').on('click', "#flowChartViewDiv1 #btn150", function () {
		onScale(1.5);
	});
	function onScale(scale) {
		if (scale != null) {
			jQuery("#" + $scope.entitySearchResult.list.currentVLArendered).famDiagram({
				scale: scale
			});
			setTimeout(function () {
				jQuery("#" + $scope.entitySearchResult.list.currentVLArendered).famDiagram("update", /*Refresh: use fast refresh to update chart*/
					primitives.orgdiagram.UpdateMode.Recreate);
				jQuery("#" + $scope.entitySearchResult.list.currentVLArendered).children(".orgdiagram").addClass("custom-scroll-wrapper");
				$("#" + $scope.entitySearchResult.list.currentVLArendered).children().css("width", "100%");
				$("#" + $scope.entitySearchResult.list.currentVLArendered).children().addClass('custom-scroll-wrapper');
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find('.orgChartParentEntity').find("i").remove();
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartmainEntity").find("i").remove();
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartsubEntity").find("i").remove();
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartParentEntity.comapny_icon").prepend(
					'<i class="fa fa-university" style="color:#4c9d20"></i>');
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartParentEntity.person_icon").prepend(
					'<i class="fa fa-user" style="color:#4c9d20"></i>');
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartmainEntity").parent().parent().css({
					'background-color': 'none'
				});
				$($("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartmainEntity").prepend(
					'<i class="fa fa-university"></i>')).css({
						'background-color': 'none'
					});
				$($("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartmainEntity.person_icon").prepend(
					'<i class="fa fa-user"></i>')).css({
						'background-color': 'none'
					});
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartsubEntity.comapny_icon").prepend(
					'<i class="fa fa-university" style="color:#c7990c"></i>');
				$("#" + $scope.entitySearchResult.list.currentVLArendered).find(".orgdiagram").find(".orgChartsubEntity.person_icon").prepend(
					'<i class="fa fa-user" style="color:#c7990c"></i>');
			}, 0)
		}
	}
	/* @purpose:Populate the source Popover
	* @created: 25 June 2019
	* @params: none
		* @returns null
	* @author: Ram Singh*/
	$scope.screeningTableroleSources = function (selected_screening, index) {
		$scope.entitySearchResult.list.selectedScreeningRow = selected_screening;
		$scope.entitySearchResult.list.selectedScreeningRow.index = index;
		$scope.entitySearchResult.list.keymangerapplybutton = true;
		setTimeout(function () {
			$(".scroll-list").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 50);
	}
	$scope.editScreeningTableEntity = function (screening) {
		entityDataOnClikOfOdg = {};
		entityDataOnClikOfOdg = jQuery.extend(true, {}, screening);
		window.showOwnerShipEntityEdit();
		$scope.editModelClassification.editPersonListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Main Prinicipals" }, { id: 4, label: "Principals" }, { id: 5, label: "Pesudo UBO" }, { id: 6, label: "UBO" }];
		$scope.editModelClassification.editCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
		$scope.editModelClassification.editRoleData = [{ id: 1, label: "Chief Executive Officer" }, { id: 2, label: "Chief Financial Officer" }, { id: 3, label: "Chief Operating Officer" }, { id: 4, label: "Chief Risk Officer" }, { id: 5, label: "Chief Compliance Officer" }, { id: 6, label: "Director" }, { id: 7, label: "Executive Board" }, { id: 8, label: "Management Board" }, { id: 10, label: "Vice President" }, { id: 11, label: "President" }, { id: 12, label: "Branch Manager" }, { id: 11, label: "Chairman" }];
		$scope.personRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.companyRoleSettings = { enableSearch: false, idProperty: 'id' };
		$scope.editModelClassification.editCompanyListData = [{ id: 1, label: "General Partner" }, { id: 2, label: "Director" }, { id: 3, label: "Intermediate Parent" }, { id: 4, label: "Ultimate Legal Parent" }];
	}
	/* @purpose:select the source from the source list
	* @created: 25 June 2019
	* @params: none
		* @returns null
	* @author: Ram Singh*/
	$scope.setTheSourceManager = function (selectedSource) {
		$scope.entitySearchResult.list.keymangerSourceselected = selectedSource;
		$scope.entitySearchResult.list.keymangerapplybutton = ($scope.entitySearchResult.list.selectedScreeningRow && $scope.entitySearchResult.list.selectedScreeningRow.source_evidence !== selectedSource)   ?  false :true;	
	}
	/* @purpose: Change the Row data of screening Table 
* @created: 25 June 2019
* @params: none
	* @returns null
* @author: Ram Singh*/
	$scope.selectedkeymanagerSourceApply = function () {
		var changeManagerValue = _.find(EntityCommonTabService.basicsharedObject.totalOfficers_link, {
			'name': $scope.entitySearchResult.list.selectedScreeningRow.name,
			'source': $scope.entitySearchResult.list.keymangerSourceselected
		});
		if (changeManagerValue) {
			var country = changeManagerValue.country_of_residence ? changeManagerValue.country_of_residence : (changeManagerValue.address && changeManagerValue.address.country) ? changeManagerValue.address.country : '';
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].officer_role = changeManagerValue.officer_role ? changeManagerValue.officer_role : '';
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].officer_roles = changeManagerValue.officer_role ? [changeManagerValue.officer_role] : '';
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].country = country ? country : changeManagerValue.country ? changeManagerValue.country : ((changeManagerValue.basic && changeManagerValue.basic["mdaas:RegisteredAddress"] && changeManagerValue.basic["mdaas:RegisteredAddress"].country) ? changeManagerValue.basic["mdaas:RegisteredAddress"].country : changeManagerValue.country_of_residence ? changeManagerValue.country_of_residence : changeManagerValue.address && changeManagerValue.address.country ? changeManagerValue.address.country : '');
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].date_of_birth = changeManagerValue.dateOfBirth ? changeManagerValue.dateOfBirth : '';
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].information_provider = $scope.entitySearchResult.list.keymangerSourceselected;
			$scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].source_evidence = $scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].information_provider ? $scope.screeningData[$scope.entitySearchResult.list.selectedScreeningRow.index].information_provider : '';
			var findIndex_actualScreening = $scope.actualScreeningData.findIndex(function (d) {
				return d.name == $scope.entitySearchResult.list.selectedScreeningRow.name;
			});
			if (findIndex_actualScreening !== -1) {
				$scope.actualScreeningData[findIndex_actualScreening].officer_role = changeManagerValue.officer_role ? changeManagerValue.officer_role : '';
				$scope.actualScreeningData[findIndex_actualScreening].officer_roles = changeManagerValue.officer_role ? [changeManagerValue.officer_role] : '';
				$scope.actualScreeningData[findIndex_actualScreening].country = country ? country : changeManagerValue.country ? changeManagerValue.country : ((changeManagerValue.basic && changeManagerValue.basic["mdaas:RegisteredAddress"] && changeManagerValue.basic["mdaas:RegisteredAddress"].country) ? changeManagerValue.basic["mdaas:RegisteredAddress"].country : '');
				$scope.actualScreeningData[findIndex_actualScreening].date_of_birth = changeManagerValue.dateOfBirth ? changeManagerValue.dateOfBirth : '';
				$scope.actualScreeningData[findIndex_actualScreening].information_provider = $scope.entitySearchResult.list.keymangerSourceselected;
				$scope.actualScreeningData[findIndex_actualScreening].source_evidence = $scope.actualScreeningData[findIndex_actualScreening].information_provider ? $scope.actualScreeningData[findIndex_actualScreening].information_provider : '';
			}
		}
		$('body').click();
		let object = {
			"entityId": $scope.entitySearchResult.list.topHeaderObject.identifier,
			"name": $scope.entitySearchResult.list.selectedScreeningRow.name,
			"source": $scope.entitySearchResult.list.keymangerSourceselected
		};
		updateSourceRole(object)
	}//functions ENds
	let updateSourceRole = (object) => {
		var data = [
			{
				"entityId": $scope.entitySearchResult.list.topHeaderObject.identifier,
				"officerName": object.name,
				"source": object.source
			}
		]
		EntityApiService.officerSourceRoleManagement(data).then(function (response) {
			console.log('response: ', response);
		});
	};
	$scope.selectPersonListData = {
		onItemDeselect: function (item) {
			// if (item.label == "Director" || item.label == "General Partner") {
			// 	$scope.showDirectIndirectPercentagePerson = true;
			// }
			$scope.showDirectIndirectPercentagePerson = checkEntitesExistinAray($scope.addModelClassification.personModel);
			// $scope.selectedLists.selectedDomainList =  selectDeSelectItems(item,$scope.selectedLists.selectedDomainList,$scope.DomainList,'onItemDeselect','domain');
		},
		onItemSelect: function (item, onEnterTabKey) {
			if (item.label == "Director" || item.label == "General Partner") {
				$scope.showDirectIndirectPercentagePerson = false;
			}
		},
		onSelectAll: function (item) {
			$scope.showDirectIndirectPercentagePerson = false;
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
			$scope.showDirectIndirectPercentagePerson = true;
		}
		//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedDomainList,$scope.DomainList);}
	};
	$scope.selectCompanyListData = {
		onItemDeselect: function (item) {
			$scope.showDirectIndirectPercentageCompany = checkEntitesExistinAray($scope.addModelClassification.companyModel);
		},
		onItemSelect: function (item, onEnterTabKey) {
			if (item.label == "Director" || item.label == "General Partner") {
				$scope.showDirectIndirectPercentageCompany = false;
			}
		},
		onSelectAll: function (item) {
			$scope.showDirectIndirectPercentageCompany = false;
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
			$scope.showDirectIndirectPercentageCompany = true;
		}
		//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedDomainList,$scope.DomainList);}
	};
	$scope.editSelectPersonListData = {
		onItemDeselect: function (item) {
			$scope.popOverEditDataForOwnership.classification.splice(item.label, 1);
			$scope.editshowDirectIndirectPercentagePerson = checkEntitesExistinAray($scope.editModelClassification.editPersonModel);
		},
		onItemSelect: function (item, onEnterTabKey) {
			if (item.label == "Director" || item.label == "General Partner") {
				$scope.editshowDirectIndirectPercentagePerson = false;
			}
			$scope.popOverEditDataForOwnership.classification.push(item.label);
		},
		onSelectAll: function (item, selectedModel, itemNotfound) {
			$scope.editshowDirectIndirectPercentagePerson = false;
			$scope.popOverEditDataForOwnership.classification = $scope.editModelClassification.editPersonListData.map(function (item) {
				return item.label;
			})
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
			$scope.editshowDirectIndirectPercentagePerson = true;
			$scope.popOverEditDataForOwnership.classification = [];
		}
		//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedDomainList,$scope.DomainList);}
	};
	$scope.editSelectCompanyListData = {
		onItemDeselect: function (item) {
			$scope.popOverEditDataForOwnership.classification.splice(item.label, 1);
			$scope.editshowDirectIndirectPercentagePerson = checkEntitesExistinAray($scope.editModelClassification.editCompanyModel);
		},
		onItemSelect: function (item, onEnterTabKey) {
			if (item.label == "Director" || item.label == "General Partner") {
				$scope.editshowDirectIndirectPercentagePerson = false;
			}
			$scope.popOverEditDataForOwnership.classification.push(item.label);
		},
		onSelectAll: function (item, selectedModel, itemNotfound) {
			$scope.editshowDirectIndirectPercentagePerson = false;
			$scope.popOverEditDataForOwnership.classification = $scope.editModelClassification.editCompanyListData.map(function (item) {
				return item.label;
			})
			// $scope.popOverEditDataForOwnership.classification.push(item.label);
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
			$scope.editshowDirectIndirectPercentagePerson = true;
			$scope.popOverEditDataForOwnership.classification = [];
		}
	};
	$scope.screeningSelectedPerson = {
		onItemDeselect: function (item) {
		},
		onItemSelect: function (item, onEnterTabKey) {
		},
		onSelectAll: function (item, selectedModel, itemNotfound) {
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
		}
	}
	$scope.screeningSelectedCompany = {
		onItemDeselect: function (item) {
		},
		onItemSelect: function (item, onEnterTabKey) {
			// $scope.selectedLists.selectedDomainList =  selectDeSelectItems(item,$scope.selectedLists.selectedDomainList,$scope.DomainList,'onItemSelect','domain');
		},
		onSelectAll: function (item, selectedModel, itemNotfound) {
		},
		onDeselectAll: function (item, selectedModel, itemNotfound) {
		}
	}
	/* @purpose : check for item existence
	* @params :  boolean
	*
	*/
	function checkEntitesExistinAray(array) {
		var isGp = array.some((val) => val.label === "General Partner");
		var isdirector = array.some((val) => val.label === "Director");
		if (isGp || isdirector) {
			return false
		} else {
			return true
		}
	}
	//function to delete entity ownership corporatestructure
	$("body").on("click", '.shareholderEvidenceIcon', function () {
		var current_entity_node = deleteSelectedEntity;
		openShareHolderModal(current_entity_node);
	});
	/* @purpose : function to open share holder screen modal
	* @params :  no
	*
	*/
	function openShareHolderModal(current_entity_node) {
		var filter_shareholders = subsidariesfilteredData.filter(function (obj) {
			return (obj.entity_id && obj.entity_id == "orgChartParentEntity" && (obj.classification.includes('Intermediate Parent') || obj.classification.includes('Ultimate Legal Parent')));
		});
		var templateUrl = 'shareHolderEvidenceModal.html';
		var templateSize = 'lg';
		var full_modalClass = 'adverse-full-modal';
		if (filter_shareholders.length === 0) {
			templateUrl = 'EntityShareHolderNoData.html';
			templateSize = 'xs';
			full_modalClass = '';
		}
		var datashareModal = $uibModal.open({
			templateUrl: templateUrl,
			controller: 'EntityShareHolderEvidenceModalController',
			scope: $scope,
			size: templateSize,
			windowClass: 'custom-modal bst_modal  c-arrow bst_modal modal_md evidence-modal-wrapper ' + full_modalClass,
			resolve: {
				shareHolderData: function () {
					return subsidariesfilteredData;
				},
				currentEntityNode: function () {
					return current_entity_node;
				}
			}
		});
		datashareModal.result.then(function (response) {
		}, function (reject) {
		});
	}
	/*
* function to change complex structure toggle on change
* @author : Amarjith Kumar
* @date : 25th June 2019
*/
	$scope.complexObj = {
		complexOwnershipStructure: false
	}
	$scope.complexStructureLoader = false;
	$scope.setComplexOwnership = function (value) {
		$scope.complexOwnershipToggle = value;
		var complexOwnershipModal = $uibModal.open({
			templateUrl: 'complexOwnershipModal.html',
			controller: function ($scope, $uibModalInstance) {
				/*
				* function to change complex structure api on change
                * @author : shivasai
                * @date : 2nd july 2019
                */
				$scope.dataOverOwnerShipModal = function () {
					var params = {
						token: $rootScope.ehubObject.token,
						isComplexStructure: $scope.complexOwnershipToggle,
						entityId: $rootScope.entityObj["@identifier"],
						entitySource: $scope.entitySearchResult.list.graphSourceSelected,
						entityName: $scope.entitySearchResult.name,
					}
					var parentScope = $scope.$parent;
					$scope.$parent.complexStructureLoader = true;
					setTimeout(function () {
						EntityApiService.updateComplexStructureAPI(params).then(function (response) {
							parentScope.complexObj.complexOwnershipStructure = response.data.isComplexStructure;
							if (response && response.data && response.data.isComplexStructure) {
								openShareHolderModal();
							}
							parentScope.complexStructureLoader = false;
						}, function (error) {
							console.log(error);
							parentScope.complexStructureLoader = false;
						});
					}, 100);
					$uibModalInstance.close();
				}
				$scope.ownershipToggleTOIntial = function () {
					$scope.$parent.complexObj.complexOwnershipStructure = false;
					$uibModalInstance.close();
				}
			},
			scope: $scope,
			size: 'xs',
			windowClass: 'custom-modal c-arrow bst_modal',
		});
		complexOwnershipModal.result.then(function (response) {
		}, function (reject) {
		});
	}
	//function for keeping complex structure intial when page was refreshed
	function getComplexStructureIndicator() {
		var params = {
			token: $rootScope.ehubObject.token,
			entityId: $rootScope.entityObj["@identifier"],
			entitySource: $scope.entitySearchResult.list.graphSourceSelected,
			entityName: $scope.entitySearchResult.name,
		}
		EntityApiService.getComplexStructureOnRefresh(params).then(function (response) {
			if (response && response.data) {
				$scope.complexObj.complexOwnershipStructure = response.data.isComplexStructure ? response.data.isComplexStructure : response.data;
			}
		}, function (error) {
		});
	}
	/*
		* @purpose: open add media modal
		* @created: 5 july 2019
		* @params: null
		* @return: no
		* @author: Amritesh
		*/
	$scope.sourceLinkdUploadFile = function (value, index) {
		TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value = value;
		TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex = index;
		if (window.location.hash.indexOf("#!/") >= 0) {
			var modalPath = '../scripts/common/modal/views/add.media.modal.html';
		} else {
			var modalPath = 'scripts/common/modal/views/add.media.modal.html';
		}
		var addMediaModalInstance = $uibModal.open({
			templateUrl: modalPath,
			controller: 'AddMediaModalController',
			scope: $scope,
			backdrop: 'static',
			windowClass: 'custom-modal upload-media-modal fromScreenShot'
		});
		addMediaModalInstance.result.then(function (response) {
			// getListOfDocuments();
			HostPathService.FlashSuccessMessage('SUCCESSFUL UPLOAD DOCUMENT', 'Successfully uploaded document with file title: ' + response);
		}, function (error) {
			if (error !== 'close') {
				HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Failed to upload document with file title: ' + error);
			}
		});
	};
	//function for Showing only Orginal Entites in The Screening
	let screeningTableOriginal_custom = (combinedscreeenig) => {
		$scope.screeningData = combinedscreeenig.filter((val) => {
			if ((val.isCustom === $scope.isCustomAvailable) || val.Ubo_ibo === "officers" || val.entity_id === "orgChartmainEntity" || val.entity_id === "orgChartsubEntity") {
				return val;
			} else if (val.entity_id === "orgChartParentEntity" && !val.isCustom && ($scope.entitySearchResult.list.currentVLArendered === "vlaContainer" || $scope.entitySearchResult.list.currentVLArendered === "vlaContainer_originalView")) {
				return val;
			}
		});
	}
	/*
	* function :checks the screening Table Entites marked as high priporite
	* @author : Ram Singh
	* @date : 11 july 2019
	*/
	$scope.totalOfficers_link = [];
	let screeningTable_OfficersPrioritySources = () => {
		$scope.totalOfficers_link = EntityCommonTabService.basicsharedObject.totalOfficers_link;
		let promise = new Promise(function (resolve, reject) {
			resolve(success());
			reject('syntax issue');
			function success() {
				var highCreditableOfficers = $scope.actualScreeningData.filter((val) => {
					if (val.highCredibilitySource) {
						return val.highCredibilitySource
					}
				});//filter Ends
				if (highCreditableOfficers.length > 0) {
					highCreditableOfficers.forEach((val) => {
						var changeManagerValue = _.find(EntityCommonTabService.basicsharedObject.totalOfficers_link, {
							'name': val.name,
							'source': val.highCredibilitySource
						});
						var findIndex_actualScreening = $scope.actualScreeningData.findIndex(function (d) {
							return (d.name == val.name && d.highCredibilitySource == val.highCredibilitySource);
						});
						var findIndex_ScreeningData = $scope.screeningData.findIndex(function (d) {
							return (d.name == val.name && d.highCredibilitySource == val.highCredibilitySource);
						});
						populateScreeningTable(findIndex_actualScreening, findIndex_ScreeningData, changeManagerValue)
					});	//for eachs Ends
				}
				return $scope.screeningData;
			}
		});
		return promise;
	}/*function ENdss */
	/*
	* function to Manipulate/update the Entites having  Data 
	* @author : Ram Singh
	* @date : 11 july 2019
	*/
	let populateScreeningTable = (actualIndex, screeningIndex, changeManagerValue) => {
		if (changeManagerValue) {
			var country = changeManagerValue.country_of_residence ? changeManagerValue.country_of_residence : (changeManagerValue.address && changeManagerValue.address.country) ? changeManagerValue.address.country : '';
			if (screeningIndex !== -1) {
				$scope.screeningData[screeningIndex].officer_role = changeManagerValue.officer_role ? changeManagerValue.officer_role : '';
				$scope.screeningData[screeningIndex].officer_roles = changeManagerValue.officer_role ? [changeManagerValue.officer_role] : '';
				$scope.screeningData[screeningIndex].country = country ? country : changeManagerValue.country ? changeManagerValue.country : '';
				$scope.screeningData[screeningIndex].date_of_birth = changeManagerValue.dateOfBirth ? changeManagerValue.dateOfBirth : '';
				$scope.screeningData[screeningIndex].information_provider = changeManagerValue.source_url ? changeManagerValue.source_url : '';
			}
			if (actualIndex !== -1) {
				$scope.actualScreeningData[actualIndex].officer_role = changeManagerValue.officer_role ? changeManagerValue.officer_role : '';
				$scope.actualScreeningData[actualIndex].officer_roles = changeManagerValue.officer_role ? [changeManagerValue.officer_role] : '';
				$scope.actualScreeningData[actualIndex].country = country ? country : changeManagerValue.country ? changeManagerValue.country : '';
				$scope.actualScreeningData[actualIndex].date_of_birth = changeManagerValue.dateOfBirth ? changeManagerValue.dateOfBirth : '';
				$scope.actualScreeningData[actualIndex].information_provider = changeManagerValue.source_url ? changeManagerValue.source_url : '';
			}
		}
	}
	/*
		 * @purpose: download it from source screen shot popup
		 * @created: 10 july 2019
		 * @params: null
		 * @return: no
		 * @author: Amritesh
		 */
	$scope.downloadDocFromScreenShot = function (docId, fileTitle, fileType) {
		$scope.mediaLoader = true;
		var params = {
			docId: docId,
			token: $rootScope.ehubObject.token
		};
		TopPanelApiService.downloadDocument(params).then(function (response) {
			$scope.mediaLoader = false;
			var blob = new Blob([response.data], {
				type: "application/" + fileType,
			});
			saveAs(blob, fileTitle);
			HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title: ' + fileTitle);
		}, function (e) {
			$scope.mediaLoader = false;
			HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + fileTitle);
		});
	};
};