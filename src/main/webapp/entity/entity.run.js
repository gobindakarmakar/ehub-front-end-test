"use strict"
angular.module('ehubEntityApp')
		.run(['$rootScope', 'EHUB_FE_API', 'UploadFileService', 'HostPathService', '$location','$stateProvider', '$urlRouterProvider', function($rootScope, EHUB_FE_API, UploadFileService, HostPathService, $location,$stateProvider, $urlRouterProvider){
			var userDetails = getAgainTokenBySession();
			$rootScope.ehubObject = userDetails;
			 window.appName = 'entity';
			 $rootScope.hyperLinksNewTab = true;
			 $rootScope.companyViewSelected = "default";
			 $rootScope.qbServeyID = 612; // default qb(Questionnaire) servey Id
			 $rootScope.customerLogo='';
			 $rootScope.genpactReportV2 = false;
			 $rootScope.complianceReport = false;
			 $rootScope.sourcesListsData = [];
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
					fromState, fromParams) {
				$(".Bubble_Chart_tooltip").css("display", "none");
				if(userDetails.fullName === "DataEntry User"){
					if(toState.name == 'dataCuration' || toState.name == 'addDataCuration'){
					}else{
						$location.path('/404');
					}
				}
			});
			/*
		     * @purpose: Get system settings
		     * @created: 10 Aug 2018
		     * @params: params
		     * @return: no
		     * @author: Prasanthi
		    */
			var url;
			$rootScope.uploadDocTypeList = [];
			function getSystemSettings(){			
		   			
					UploadFileService.getGeneralSettings().then(function(response){	
						if(response.data && response.data['General Settings']){							
							angular.forEach(response.data['General Settings'],function(val,k){
								if(!val.options && val.name == "Open link in a new tab"){
									$rootScope.hyperLinksNewTab = val.selectedValue == "On"?true:false;
								}
								if(val.name == "Company View"){
									$rootScope.companyViewSelected = val.selectedValue;
								}
								if(!val.options && val.name == "Allow checklist"){
									$rootScope.allowChecklist = val.selectedValue == "On"?true:false;
								}
								if(val.name == "Compliance Report"){
									$rootScope.complianceReport = val.selectedValue == 'default' ? false: true;
									if(val.selectedValue && val.selectedValue.toLowerCase() == 'genpact v2'){
										$rootScope.genpactReportV2 = true;
									}
								}
								if(val.defaultValue == "On" && val.selectedValue == "On" && val.section == "File Settings"){
									$rootScope.uploadDocTypeList.push(val.name);
								}
								if(val.name == "Maximum file size allowed" && val.section == "File Settings"){ 
									$rootScope.maxFile_size = val.selectedValue;
									var maxFileSizeNum = $rootScope.maxFile_size.match(/\d+/g).map(Number);
									$rootScope.maxFileSize = maxFileSizeNum[0]*1048576;
								}
								if(val.section.toLowerCase() === 'branding'){
									$rootScope.customerLogo = val.customerLogoImage || val.defaultValue;
								}
								if(val.options && val.options.length && val.name.toLowerCase() == 'questionnaries'){
									angular.forEach(val.options,function(v1,k1){
										if(v1.selected){
												$rootScope.qbServeyID = v1.attributeId;
										}
									});
								}
							})
						}
					},function(error){
						
					});
			}
			getSystemSettings();
			/*var companyName = $location.path();
			$rootScope.companyTitle = companyName.substr(1);
			console.log("-----------------------------------");
			console.log($rootScope.companyTitle);
			/*
		     * @purpose: Get Submenu Data
		     * @created: 2 Feb 2018
		     * @params: params
		     * @return: no
		     * @author: Zameer
		    */
			var url;
			function getSubmenu(){
				if(window.location.hash.indexOf("#!/") < 0 && window.location.hash.indexOf("#") >= 0)
		    		url = 'scripts/common/data/submenu.json';
		   		  else
		   			url = '../scripts/common/data/submenu.json';
					UploadFileService.getSubmenuData(url).then(function(response){	
						if($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)){
							angular.forEach(response.data.dashboarDropDownMenuItems,function(val,key){
								if(val.menu == "Manage"){
									angular.forEach(val.content,function(v,k){
										if(v.name == "System Settings" && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Document Parsing"){
											v.disabled ="no";
										}
										if(v.name == "Source Management"  && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
											v.disabled ="yes";
										}
									});
								}
							});
						}
						HostPathService.setdashboardDisableBydomains(response.data.DisableBydomains);						
						HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
						HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
					},function(error){
						HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
					});
			}
			getSubmenu();
			
			$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
				if (toState.name == '404') {
					$rootScope.topPanelPreview = false;
					if(toState.name == 'domain' && ($rootScope.ehubObject.token === undefined || $rootScope.ehubObject.token === null || $rootScope.ehubObject.token === "")){
						$window.location.href = '';
					}
				} else {
					if($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== "")
					{
						$rootScope.topPanelPreview = true;
						$rootScope.companyOrPersonName = '';
						var subName = (toParams.query).split(' ');
						
						if(subName.length > 0)
						{
							for (var i = 0 ;i < subName.length;i++)
							{
								$rootScope.companyOrPersonName += (subName[i]).charAt(0).toUpperCase() + (subName[i]).substr(1)+" ";
							}
						}
						else
						{
							$rootScope.companyOrPersonName = (subName[i]).charAt(0).toUpperCase() + (subName[i]).substr(1);
						}
					}
					else
					{
						$window.location.href = '';
					}
				}
			});
			 $stateProvider
				.state('entityCompanyNew', {
					url: '/company/:query?phone&website&qId/:caseId/:eid/:cid',
					templateUrl: function(){
						return  EHUB_FE_API + appName + '/views/entityCompanyNew.jsp';
					},
					controller : 'EntityCompanyNewController'
				})
				.state('entityPageNew', {
					url : '/person/:query?phone&email',
					templateUrl : function(){
						return  EHUB_FE_API + appName + '/views/entityPersonNew.jsp';
					},
					controller : 'EntityPersonNewController'
				})
				.state('404', {
					url : '/404',
					templateUrl : function(){
						return EHUB_FE_API + 'error';
					}
				});
//			   $urlRouterProvider.when('', '/mipLanding');
			   $urlRouterProvider.otherwise(function(){
					return '/404';
				});
		}]);


		