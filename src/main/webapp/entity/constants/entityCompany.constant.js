'use strict';
angular.module('ehubEntityApp')
	.constant('EntityCompanyConst', {
		lineChart: {
			height: '200',
			width: '320',
			axisX: true,
			axisY: true,
			gridy: true,
			marginTop: 0,
			marginBottom: 50,
			marginRight: 50,
			marginLeft: 10,
			showxYaxis: true
		},
		WebSocketTwitterUrl: 'http://104.197.164.75'
	}).constant('Company_Unwanted_End_Points', ['ltd.', 'international', 'limited', 'corporation', 'organization', 'pvt.', 'private', 'llc.', 'inc.', 'corp.', 'agency',
		'co.', 'incorporated', 'corporated', 'cooperatives'
	])
	.constant('handlingCompanyConstantName', [{
			name: 'British Airways',
			fetcher: '1011',
			alt_name: 'Boeing Co',
			limit: '1'
		}, {
			name: 'British Airways',
			fetcher: '1006',
			alt_name: 'British Airways Plc',
			limit: '1'
		},
		{
			name: 'Volkswagen',
			fetcher: '1011',
			alt_name: 'Volkswagen Ag',
			limit: '1'
		},
		{
			name: 'Nestle',
			fetcher: '1006',
			alt_name: 'Nestle S A',
			limit: '1'
		},
		{
			name: 'Munich Re',
			fetcher: '1017',
			alt_name: 'Munich Re Group',
			limit: '1'
		},
		{
			name: 'Hapag-Lloyd',
			fetcher: '1008',
			alt_name: 'Hapag-Lloyd AG',
			limit: '1'
		},
		{
			name: 'Audi',
			fetcher: '1008',
			alt_name: 'Audi Ag',
			limit: '1'
		},
		{
			name: 'Audi',
			fetcher: '1012',
			alt_name: 'Audi Ag',
			limit: '1'
		},
		{
			name: 'Nestle',
			fetcher: '1011',
			alt_name: 'Nestle Ag',
			limit: '1'
		},
		{
			name: 'Volkswagen',
			fetcher: '1012',
			alt_name: 'Volkswagen Ag',
			limit: '1'
		}, {
			name: 'Fraport',
			fetcher: '1017',
			alt_name: 'Frankfurt Airport',
			limit: '1'
		},
		{
			name: 'Fraport',
			fetcher: '1008',
			alt_name: 'Fraport USA',
			limit: '1'
		}, {
			name: 'Fraport',
			fetcher: '21',
			alt_name: 'Frankfurtairport',
			limit: '1'
		},
		{
			name: 'Munich Re',
			fetcher: '21',
			alt_name: 'Munich Re',
			limit: '4'
		},
		{
			name: 'Volkswagen',
			fetcher: '1017',
			alt_name: 'vwcanada',
			limit: '4'
		},

		{
			name: 'Volkswagen',
			fetcher: '1009',
			alt_name: 'Volkswagen AG',
			limit: '1'
		},
		{
			name: 'Munich Re',
			fetcher: '1009',
			alt_name: 'MunichRe',
			limit: '1'
		},
		{
			name: 'PNE Wind',
			fetcher: '1009',
			alt_name: 'Pne Wind AG',
			limit: '1'
		},
		{
			name: 'Hapag-Lloyd',
			fetcher: '1009',
			alt_name: 'Hapag-loyd AG',
			limit: '1'
		},
		{
			name: 'DIXONS CARPHONE PLC',
			fetcher: '1011',
			alt_name: 'DSITF',
			limit: '1'
		},
		{
			name: 'KFW IPEX-BANK GMBH',
			fetcher: '1017',
			alt_name: 'KFW IPEX-BANK',
			limit: '1'
		},
		{
			name: 'KFW IPEX-BANK GMBH',
			fetcher: '1008',
			alt_name: 'KFW IPEX-BANK',
			limit: '1'
		},
		{
			name: 'KFW IPEX-BANK GMBH',
			fetcher: '21',
			alt_name: 'KFW IPEX-BANK',
			limit: '1'
		},
		{
			name: 'KFW IPEX-BANK GMBH',
			fetcher: '2001',
			alt_name: 'kfwpress',
			limit: '1'
		},{
			name: 'PRAYON',
			fetcher: '2001',//twitter
			alt_name: 'Prayon_Group',
			limit: '1'
		},
		{
			name: 'PRAYON',
			fetcher: '1008',//linkedinr
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'PRAYON',
			fetcher: '1023',//youtube
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'PRAYON',
			fetcher: '2001',
			alt_name: 'kfwpress',
			limit: '1'
		},
		{
			name: 'Caterpillar Group Services',
			fetcher: '2001',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Caterpillar Group Services',
			fetcher: '1017',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Caterpillar Group Services',
			fetcher: '21',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Caterpillar Special Services Belgium',
			fetcher: '2001',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Caterpillar Special Services Belgium',
			fetcher: '21',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Caterpillar Special Services Belgium',
			fetcher: '1017',
			alt_name: 'CaterpillarInc',
			limit: '1'
		},
		{
			name: 'Prayon Benelux',
			fetcher: '2001',//twitter
			alt_name: 'Prayon_Group',
			limit: '1'
		},
		{
			name: 'Prayon Benelux',
			fetcher: '1008',//link
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'Prayon Benelux',
			fetcher: '1017',//link
			alt_name: 'Prayon',
			limit: '1'
		},{
			name: 'Prayon Benelux',
			fetcher: '21',//link
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'Prayon Technologies SA',
			fetcher: '2001',//twitter
			alt_name: 'Prayon_Group',
			limit: '1'
		},
		{
			name: 'Prayon Technologies SA',
			fetcher: '1008',//link
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'Prayon Technologies SA',
			fetcher: '1017',//link
			alt_name: 'Prayon',
			limit: '1'
		},{
			name: 'Prayon Technologies SA',
			fetcher: '21',//link
			alt_name: 'Prayon',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '529900q1m1f4m8kmtm64',
			alt_name: 'KFW_IPEX_Bank_GMBH',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '549300i7dz2o8hcxyk97',
			alt_name: 'PrayonBeneluxNV',
			limit: '1'
		},{
			name: 'json',
			fetcher: '54930064jcexfa8m0w31',
			alt_name: 'prayon_sa',
			limit: '1'
		},{
			name: 'json',
			fetcher: '0473.585.474',
			alt_name: 'CaterpillarSpecialServicesBelgiumSPRL',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '0428.189.078',
			alt_name: 'CaterpillarGroupServicesSA',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '6593007dz11o8hcxy84',
			alt_name: 'PrayonTechnolgiesSA',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '54930012qjwzmyhnjw95',
			alt_name: 'TommyHilfigerEuropeBV',
			limit: '1'
		},
		{
			name: 'Tommy Hilfiger Europe B.V.',
			fetcher: '1008',//linkedinr
			alt_name: 'tommy-hilfiger',
			limit: '1'
		},{
			name: 'Tommy Hilfiger Europe B.V.',
			fetcher: '2001',//twitter
			alt_name: 'TommyHilfiger',
			limit: '1'
		},{
			name: 'Tommy Hilfiger Europe B.V.',
			fetcher: '21',//insta
			alt_name: 'tommyhilfiger',
			limit: '1'
		},
		{
			name: 'json',
			fetcher: '724500PMK2A2M1SQQ228', 
			alt_name: 'TriodosBankNV',
			limit: '1'
		},{
			name: 'json',
			fetcher: '724500pmk2a2m1sqq228', 
			alt_name: 'TriodosBankNV',
			limit: '1'
		},
		{
			name: 'Triodos Bank NV',
			fetcher: '1008',//linkedinr
			alt_name: 'triodos-bank',
			limit: '1'
		},{
			name: 'Triodos Bank NV',
			fetcher: '2001',//twitter
			alt_name: 'triodosuk',
			limit: '1'
		},{
			name: 'json',
			fetcher: 'TRIODOS%20BANK%20NV', 
			alt_name: 'TriodosBankNV',
			limit: '1'
		},
		{
			name: 'Triodos Bank NV',
			fetcher: '21',//insta
			alt_name: 'triodosuk',
			limit: '1'
		},{
			name: 'json',
			fetcher: '5493007kcyw14yc7bi65',
			alt_name: 'TractionNetherlands',
			limit: '1'
		}
		
	])
	.constant('handlingPersonConstantName', [{
			name: 'Tim Cook',
			fetcher: '26',
			alt_name: 'Timothy D Cook',
			limit: '5'
		},
		{
			name: 'Indra Nooyi',
			fetcher: '26',
			alt_name: 'Indra K Nooyi',
			limit: '4'
		},
		{
			name: 'Bob Iger',
			fetcher: '26',
			alt_name: 'Robert A Iger',
			limit: '4'
		},
		{
			name: 'Elon Musk',
			fetcher: '8',
			alt_name: 'Elon R Musk',
			limit: '4'
		},
		{
			name: 'Indra  Nooyi',
			fetcher: '8',
			alt_name: 'Indra K Nooyi',
			limit: '4'
			// "FEC/ESR Risk Level": "High"
			// "ISO": "AF",
		}
	]).constant('chartsConst', {
		countryRisk: [
			{
			  "ISO": "AF",
			  "COUNTRY": "AFGHANISTAN",
			  "FEC/ESR Risk Level": "UHRC"
			},
			{
			  "ISO": "AX",
			  "COUNTRY": "ÅLAND ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AL",
			  "COUNTRY": "ALBANIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "DZ",
			  "COUNTRY": "ALGERIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AS",
			  "COUNTRY": "AMERICAN SAMOA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AD",
			  "COUNTRY": "ANDORRA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AO",
			  "COUNTRY": "ANGOLA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AI",
			  "COUNTRY": "ANGUILLA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AQ",
			  "COUNTRY": "ANTARCTICA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AG",
			  "COUNTRY": "ANTIGUA AND BARBUDA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AR",
			  "COUNTRY": "ARGENTINA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AM",
			  "COUNTRY": "ARMENIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AW",
			  "COUNTRY": "ARUBA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AU",
			  "COUNTRY": "AUSTRALIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AT",
			  "COUNTRY": "AUSTRIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "AZ",
			  "COUNTRY": "AZERBAIJAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BS",
			  "COUNTRY": "BAHAMAS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BH",
			  "COUNTRY": "BAHRAIN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BD",
			  "COUNTRY": "BANGLADESH",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BB",
			  "COUNTRY": "BARBADOS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BY",
			  "COUNTRY": "BELARUS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BE",
			  "COUNTRY": "BELGIUM",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BZ",
			  "COUNTRY": "BELIZE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BJ",
			  "COUNTRY": "BENIN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BM",
			  "COUNTRY": "BERMUDA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BT",
			  "COUNTRY": "BHUTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BO",
			  "COUNTRY": "BOLIVIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BQ",
			  "COUNTRY": "BONAIRE SINT EUSTATIUS AND SABA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BA",
			  "COUNTRY": "BOSNIA AND HERZEGOVINA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BW",
			  "COUNTRY": "BOTSWANA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BV",
			  "COUNTRY": "BOUVET ISLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BR",
			  "COUNTRY": "BRAZIL",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "IO",
			  "COUNTRY": "BRITISH INDIAN OCEAN TERRITORY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BN",
			  "COUNTRY": "BRUNEI DARUSSALAM",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BG",
			  "COUNTRY": "BULGARIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "BF",
			  "COUNTRY": "BURKINA FASO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BI",
			  "COUNTRY": "BURUNDI",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "Cv",
			  "COUNTRY": "CABO VERDE",
			  "FEC/ESR Risk Level": "Low"
			},{
			  "ISO": "Cv",
			  "COUNTRY": "CAPE VERDE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "KH",
			  "COUNTRY": "CAMBODIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CM",
			  "COUNTRY": "CAMEROON",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CA",
			  "COUNTRY": "CANADA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "KY",
			  "COUNTRY": "CAYMAN ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CF",
			  "COUNTRY": "CENTRAL AFRICAN REPUBLIC",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TD",
			  "COUNTRY": "CHAD",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CL",
			  "COUNTRY": "CHILE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CN",
			  "COUNTRY": "CHINA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CX",
			  "COUNTRY": "CHRISTMAS ISLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CC",
			  "COUNTRY": "COCOS (KEELING) ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CO",
			  "COUNTRY": "COLOMBIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KM",
			  "COUNTRY": "COMOROS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CG",
			  "COUNTRY": "CONGO REP.",
			  "FEC/ESR Risk Level": "Medium"
			},{
			  "ISO": "CD",
			  "COUNTRY": "CONGO DEM. REP.",
			  "FEC/ESR Risk Level": "Medium"
		   },
			{
			  "ISO": "CK",
			  "COUNTRY": "COOK ISLANDS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CR",
			  "COUNTRY": "COSTA RICA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CI",
			  "COUNTRY": "COTE D'IVOIRE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "HR",
			  "COUNTRY": "CROATIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CU",
			  "COUNTRY": "CUBA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "CW",
			  "COUNTRY": "CURACAO",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CY",
			  "COUNTRY": "CYPRUS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CZ",
			  "COUNTRY": "CZECH REPUBLIC",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "DK",
			  "COUNTRY": "DENMARK",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "DJ",
			  "COUNTRY": "DJIBOUTI",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "DM",
			  "COUNTRY": "DOMINICA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "DO",
			  "COUNTRY": "DOMINICAN REPUBLIC",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "EC",
			  "COUNTRY": "ECUADOR",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "EG",
			  "COUNTRY": "EGYPT",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SV",
			  "COUNTRY": "EL SALVADOR",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GQ",
			  "COUNTRY": "EQUATORIAL GUINEA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "ER",
			  "COUNTRY": "ERITREA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "EE",
			  "COUNTRY": "ESTONIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "COUNTRY": "England",
			  "ISO": "Gb",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "ET",
			  "COUNTRY": "ETHIOPIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "FK",
			  "COUNTRY": "FALKLAND ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "FO",
			  "COUNTRY": "FAROE ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "FJ",
			  "COUNTRY": "FIJI",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "FI",
			  "COUNTRY": "FINLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "FR",
			  "COUNTRY": "FRANCE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GF",
			  "COUNTRY": "FRENCH GUIANA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PF",
			  "COUNTRY": "FRENCH POLYNESIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TF",
			  "COUNTRY": "FRENCH SOUTHERN TERRITORIES",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GA",
			  "COUNTRY": "GABON",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GM",
			  "COUNTRY": "GAMBIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GE",
			  "COUNTRY": "GEORGIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "DE",
			  "COUNTRY": "GERMANY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GH",
			  "COUNTRY": "GHANA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GI",
			  "COUNTRY": "GIBRALTAR",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GR",
			  "COUNTRY": "GREECE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GL",
			  "COUNTRY": "GREENLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GD",
			  "COUNTRY": "GRENADA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GP",
			  "COUNTRY": "GUADELOUPE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GU",
			  "COUNTRY": "GUAM",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GT",
			  "COUNTRY": "GUATEMALA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GG",
			  "COUNTRY": "GUERNSEY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GN",
			  "COUNTRY": "GUINEA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GW",
			  "COUNTRY": "GUINEA-BISSAU",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "GY",
			  "COUNTRY": "GUYANA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "HT",
			  "COUNTRY": "HAITI",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "HM",
			  "COUNTRY": "HEARD ISLAND AND MCDONALD ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "VA",
			  "COUNTRY": "VATICAN CITY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "HN",
			  "COUNTRY": "HONDURAS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "HK",
			  "COUNTRY": "HONG KONG",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "HU",
			  "COUNTRY": "HUNGARY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "IS",
			  "COUNTRY": "ICELAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "IN",
			  "COUNTRY": "INDIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "ID",
			  "COUNTRY": "INDONESIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "IR",
			  "COUNTRY": "IRAN",
			  "FEC/ESR Risk Level": "UHRC"
			},
			{
			  "ISO": "IQ",
			  "COUNTRY": "IRAQ",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "IE",
			  "COUNTRY": "IRELAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "IM",
			  "COUNTRY": "ISLE OF MAN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "IL",
			  "COUNTRY": "ISRAEL",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "IT",
			  "COUNTRY": "ITALY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "JM",
			  "COUNTRY": "JAMAICA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "JP",
			  "COUNTRY": "JAPAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "JE",
			  "COUNTRY": "JERSEY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "JO",
			  "COUNTRY": "JORDAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KZ",
			  "COUNTRY": "KAZAKHSTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KE",
			  "COUNTRY": "KENYA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KI",
			  "COUNTRY": "KIRIBATI",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KP",
			  "COUNTRY": "KOREA, NORTH",
			  "FEC/ESR Risk Level": "UHRC"
			},
			{
			  "ISO": "KR",
			  "COUNTRY": "KOREA, SOUTH",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "KW",
			  "COUNTRY": "KUWAIT",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "KG",
			  "COUNTRY": "KYRGYZSTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "LA",
			  "COUNTRY": "LAOS",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "LV",
			  "COUNTRY": "LATVIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LB",
			  "COUNTRY": "LEBANON",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "LS",
			  "COUNTRY": "LESOTHO",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LR",
			  "COUNTRY": "LIBERIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "LY",
			  "COUNTRY": "LIBYA",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "LI",
			  "COUNTRY": "LIECHTENSTEIN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LT",
			  "COUNTRY": "LITHUANIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LU",
			  "COUNTRY": "LUXEMBOURG",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MO",
			  "COUNTRY": "MACAU",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MK",
			  "COUNTRY": "MACEDONIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MG",
			  "COUNTRY": "MADAGASCAR",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MW",
			  "COUNTRY": "MALAWI",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MY",
			  "COUNTRY": "MALAYSIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MV",
			  "COUNTRY": "MALDIVES",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "ML",
			  "COUNTRY": "MALI",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MT",
			  "COUNTRY": "MALTA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MH",
			  "COUNTRY": "MARSHALL ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MQ",
			  "COUNTRY": "MARTINIQUE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MR",
			  "COUNTRY": "MAURITANIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MU",
			  "COUNTRY": "MAURITIUS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "YT",
			  "COUNTRY": "MAYOTTE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MX",
			  "COUNTRY": "MEXICO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "FM",
			  "COUNTRY": "MICRONESIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MD",
			  "COUNTRY": "MOLDOVA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MC",
			  "COUNTRY": "MONACO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MN",
			  "COUNTRY": "MONGOLIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "ME",
			  "COUNTRY": "MONTENEGRO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MS",
			  "COUNTRY": "MONTSERRAT",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MA",
			  "COUNTRY": "MOROCCO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MZ",
			  "COUNTRY": "MOZAMBIQUE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "MM",
			  "COUNTRY": "MYANMAR",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NA",
			  "COUNTRY": "NAMIBIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "NR",
			  "COUNTRY": "NAURU",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NP",
			  "COUNTRY": "NEPAL",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NL",
			  "COUNTRY": "NETHERLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "NC",
			  "COUNTRY": "NEW CALEDONIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "NZ",
			  "COUNTRY": "NEW ZEALAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "NI",
			  "COUNTRY": "NICARAGUA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NE",
			  "COUNTRY": "NIGER",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NG",
			  "COUNTRY": "NIGERIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NU",
			  "COUNTRY": "NIUE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "NF",
			  "COUNTRY": "NORFOLK ISLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MP",
			  "COUNTRY": "NORTHERN MARIANA ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "NO",
			  "COUNTRY": "NORWAY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "OM",
			  "COUNTRY": "OMAN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PK",
			  "COUNTRY": "PAKISTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PW",
			  "COUNTRY": "PALAU",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PS",
			  "COUNTRY": "PALESTINE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PA",
			  "COUNTRY": "PANAMA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PG",
			  "COUNTRY": "PAPUA NEW GUINEA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PY",
			  "COUNTRY": "PARAGUAY",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PE",
			  "COUNTRY": "PERU",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PH",
			  "COUNTRY": "PHILIPPINES",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "PN",
			  "COUNTRY": "PITCAIRN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PL",
			  "COUNTRY": "POLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PT",
			  "COUNTRY": "PORTUGAL",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PR",
			  "COUNTRY": "PUERTO RICO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "QA",
			  "COUNTRY": "QATAR",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "RE",
			  "COUNTRY": "REUNION",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "RO",
			  "COUNTRY": "ROMANIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "RU",
			  "COUNTRY": "RUSSIA",
			  "FEC/ESR Risk Level": "Medium"
			},
		  {
			  "ISO": "RU",
			  "COUNTRY": "RUSSIAN FEDERATION",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "RW",
			  "COUNTRY": "RWANDA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "BL",
			  "COUNTRY": "SAINT BARTHELEMY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SH",
			  "COUNTRY": "SAINT HELENA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "KN",
			  "COUNTRY": "SAINT KITTS & NEVIS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LC",
			  "COUNTRY": "SAINT LUCIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "MF",
			  "COUNTRY": "SAINT MARTIN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "PM",
			  "COUNTRY": "SAINT PIERRE & MIQUELON",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "VC",
			  "COUNTRY": "SAINT VINCENT & GRENADINES",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "WS",
			  "COUNTRY": "SAMOA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SM",
			  "COUNTRY": "SAN MARINO",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "ST",
			  "COUNTRY": "SAO TOME & PRINCIPE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SA",
			  "COUNTRY": "SAUDI ARABIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SN",
			  "COUNTRY": "SENEGAL",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "RS",
			  "COUNTRY": "SERBIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SC",
			  "COUNTRY": "SEYCHELLES",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SL",
			  "COUNTRY": "SIERRA LEONE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SG",
			  "COUNTRY": "SINGAPORE",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SX",
			  "COUNTRY": "SINT MAARTEN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SK",
			  "COUNTRY": "SLOVAKIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SI",
			  "COUNTRY": "SLOVENIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SB",
			  "COUNTRY": "SOLOMON ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SO",
			  "COUNTRY": "SOMALIA",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "ZA",
			  "COUNTRY": "SOUTH AFRICA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GS",
			  "COUNTRY": "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SS",
			  "COUNTRY": "SOUTH SUDAN",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "ES",
			  "COUNTRY": "SPAIN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "LK",
			  "COUNTRY": "SRI LANKA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SD",
			  "COUNTRY": "SUDAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SR",
			  "COUNTRY": "SURINAME",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "SJ",
			  "COUNTRY": "SVALBARD",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SZ",
			  "COUNTRY": "SWAZILAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SE",
			  "COUNTRY": "SWEDEN",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "CH",
			  "COUNTRY": "SWITZERLAND",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "SY",
			  "COUNTRY": "SYRIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TW",
			  "COUNTRY": "TAIWAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TJ",
			  "COUNTRY": "TAJIKISTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TZ",
			  "COUNTRY": "TANZANIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TH",
			  "COUNTRY": "THAILAND",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "COUNTRY": "Timorleste",
			  "ISO": "Tl",
			  "FEC/ESR Risk Level": "Medium"
			},
		   {
			  "COUNTRY": "EAST TIMOR",
			  "ISO": "Tl",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TG",
			  "COUNTRY": "TOGO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TK",
			  "COUNTRY": "TOKELAU",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TO",
			  "COUNTRY": "TONGA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TT",
			  "COUNTRY": "TRINIDAD AND TOBAGO",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TN",
			  "COUNTRY": "TUNISIA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TR",
			  "COUNTRY": "TURKEY",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TM",
			  "COUNTRY": "TURKMENISTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "TC",
			  "COUNTRY": "TURKS AND CAICOS ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "TV",
			  "COUNTRY": "TUVALU",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "UG",
			  "COUNTRY": "UGANDA",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "UA",
			  "COUNTRY": "UKRAINE",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "AE",
			  "COUNTRY": "UNITED ARAB EMIRATES",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "GB",
			  "COUNTRY": "UNITED KINGDOM",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "US",
			  "COUNTRY": "UNITED STATES OF AMERICA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "UM",
			  "COUNTRY": "UNITED STATES MINOR OUTLYING ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "UY",
			  "COUNTRY": "URUGUAY",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "UZ",
			  "COUNTRY": "UZBEKISTAN",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "VU",
			  "COUNTRY": "VANUATU",
			  "FEC/ESR Risk Level": "Medium"
			},
			{
			  "ISO": "VE",
			  "COUNTRY": "VENEZUELA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "VN",
			  "COUNTRY": "VIET NAM",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "VG",
			  "COUNTRY": "VIRGIN ISLANDS (BRITISH)",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "VI",
			  "COUNTRY": "VIRGIN ISLANDS (U.S.)",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "WF",
			  "COUNTRY": "WALLIS & FUTUNA ISLANDS",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "EH",
			  "COUNTRY": "WESTERN SAHARA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "YE",
			  "COUNTRY": "YEMEN",
			  "FEC/ESR Risk Level": "High"
			},
			{
			  "ISO": "ZM",
			  "COUNTRY": "ZAMBIA",
			  "FEC/ESR Risk Level": "Low"
			},
			{
			  "ISO": "ZW",
			  "COUNTRY": "ZIMBABWE",
			  "FEC/ESR Risk Level": "Medium"
			},
		   {
			"ISO": "KX",
			"COUNTRY": "CRIMEA",
			"FEC/ESR Risk Level": "High"
		   },
		   {
			"ISO": "XK",
			"COUNTRY": "KOSOVO",
			"FEC/ESR Risk Level": "Medium"
		   },
		   {
			"ISO": "KK",
			"COUNTRY": "TURKISH REPUBLIC OF NORTHERN CYPRUS",
			"FEC/ESR Risk Level": "High"
		  }],
		entityStatusData: [ //Pie data
			{
				'key': "ACTIVE",
				'value': 231
			}, {

				'key': "DISSOLVE",
				'value': 127
			}, {

				'key': "LIQUIDATION",
				'value': 23
			}
		],
		timeIncorporation: [ //Pie data
			{
				'key': "THIS MONTH",
				'value': 231
			}, {

				'key': "THIS QUARTER",
				'value': 127
			}, {

				'key': "THIS YEAR",
				'value': 23
			}, {

				'key': "YEAR BACK",
				'value': 23
			}
		],
		legalEntity: [ //Pie data
			{
				'key': "PRIVATE ",
				'value': 216
			}, {

				'key': "PUBLIC",
				'value': 278
			}, {

				'key': "GOVERNMENT",
				'value': 192
			}
		],

		employeeStrength: [ //Pie data
			{
				'key': "employeeStrength > 5000",
				'value': 2
			}, {

				'key': "employeeStrength 2000-5000 ",
				'value': 1
			}, {

				'key': "employeeStrength 500-1000",
				'value': 4
			}, {

				'key': "employeeStrength < 500",
				'value': 6
			}
		],
		usagePie: [ //Pie data
			{
				'key': "LOCAL",
				'value': 10
			}, {

				'key': "ABROAD",
				'value': 7
			}

		],
		formPie: [ //Pie data
			{
				'key': "POS",
				'value': 6
			}, {

				'key': "ATM",
				'value': 5
			}, {

				'key': "ONLINE",
				'value': 2
			}
		],
		typePie: [ //Pie data
			{
				'key': "STANDARD",
				'value': 6
			}, {

				'key': "EMI",
				'value': 5
			}, {

				'key': "CASHBACK",
				'value': 6
			}, {

				'key': "GIFT",
				'value': 2
			}
		],
		spendingGenrePie: [ //Pie data
			{
				'key': "HealthCare",
				'value': 600
			}, {

				'key': "Fashion",
				'value': 500
			}, {

				'key': "Electronics",
				'value': 300
			}, {

				'key': "FMCG",
				'value': 250
			}, {
				'key': "Food And Dining",
				'value': 200
			}, {

				'key': "Invesments",
				'value': 150
			}, {

				'key': "Entertainment",
				'value': 100
			}

		],

	}).constant('mockedCompany', {
		entityname: [{
			'name': "ck hutchison holdings ltd2",
			'newname': 'CK Hutchison Holdings',
			'address_line_1': '22nd Floor Hutchison House 10 ',
			'address_line_2': 'Harcourt Road',
			'country': ' Hong Kong',
			'city': 'Hong Kong',
			'logo': 'http://www.aswatson.com//wp-content/uploads/2017/03/CKHH.jpg',
			'fax': '852 2128 1705',
			'industry': 'Conglomerate',
			'type': 'Public',
			'hasLatestOrganizationFoundedDate': '18 March 2015 ',
			'status': ''
		}, {
			'name': "ocbc",
			'newname': 'OCBC Bank',
			'address_line_1': ' OCBC Centre ',
			'address_line_2': 'Southern Bank of the Singapore River',
			'country': ' Singapore',
			'city': 'Singapore',
			'logo': 'http://www.filife.com/wp-content/uploads/2018/02/ocbc-thumb.jpg',
			'hasLatestOrganizationFoundedDate': '1932; 86 years ago (1932)',
			'Regid': '',
			'type': 'Public',
			"industry": 'Financial services',
			'status': ''
		}, {
			'name': "british airways2",
			'newname': 'BRITISH AIRWAYS PLC',
			'address_line_1': 'Waterside',
			'address_line_2': '',
			'country': ' United Kingdom',
			'city': 'Harmondsworth',
			'logo': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsSwXe92keoz69J3XWji-yI_SsqzAdCt2RH34gpRdLEgb2blSr',
			'hasLatestOrganizationFoundedDate': '31 March 1974 ',
			'Regid': '01777777',
			'status': 'Active',
			"source_url":'https://beta.companieshouse.gov.uk/company/01777777/'

		}],
		keypeople: [{
				'newname': 'CK Hutchison Holdings',
				'name': "Li Ka-shing",
				"officer_role": "Chairman Victor Li Tzar-kuoy",
				'country': 'Hong Kong',
				'img_src': 'https://www.irishtimes.com/polopoly_fs/1.3429425.1521191722!/image/image.jpg_gen/derivatives/box_620_330/image.jpg',
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'mdaas:RegisteredAddress': {
					'streetAddress': 'Cheung Kong Center',
					'city': 'Hutchison House',
					'country': 'Hong kong'
				}
			}, {
				'newname': 'CK Hutchison Holdings',
				'name': "Canning Fok Kin-ning",
				"officer_role": "Group Co-managing director",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': 'Hutchison House',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_256,w_256,f_auto,g_faces,z_0.7,q_auto:eco/v1397752313/5b5f6055e0623317976b27f4a5e01772.jpg'
			}, {
				'newname': 'CK Hutchison Holdings',
				'name': " Frank John SIXT",
				"officer_role": "Group Finance Director and Deputy Managing Director",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': 'Hutchison House',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBAQEBAVEBAVECAbEBUVDRsQFRAWIB0iIiAdHx8kKDQsJCYxJx8fLTItMT1AMDAwIytKQEc1NzQuQ0ABCgoKDQ0OFRAQFTcZFSU3KzcrNysrLTc3NzQrNzE3Nys3NzcrKysrOCs3Ny8rLTctKysrKy0rKys4NysrKysrK//AABEIAMgAoAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECBAUGAwj/xAA9EAABAwIDBAgDBwMDBQAAAAABAAIDBBEFITEGEkFRBxMiYXGBkbEyodEUQlJicsHhI0PwFcLiJDM0U4L/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAgMEAQUG/8QAIxEAAgIBBAIDAQEAAAAAAAAAAAECEQMEEiExIkETUWEUBf/aAAwDAQACEQMRAD8Al9ERaDKEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAF4VtZHCwySvDGDUkrX7TY/HQwmR+bj/228XH6KGcV2kmrJC6V5sNBezWjkAq55NvBbjxbuX0dxj/SI8XFJGN0f3JOPg36rn4NusQe6zpm242jA9lyk73SHTLv0VIp2suHsHcQdFncpP2a1jivR11dtBUDPr5Ae6U5/NWU+3tdCR/UEreUjf3Ga5p7iW67zf1X99FivlGlskTYcU/RM+zO3MVWRHIwxSHSzt5hPjwXXL50wyVzXNdG43BuLBSPsxts7rGw1BNjkHHP5q2OX0yjJg9xJERUBvYjQ6KqvMwREQBERAEREAREQBEVr3gAk5AC5QEI9JuLunrpImm7IjuAcLjX53XKscbW4Hu1WVi0nXSSTA2EshPeBfVX4fTGR7WtyFssr7o+qxTl7PRxxuki6OOLcPWgt/Nv9ryH8LDfGGOBjl6wcQY7EePNd1huzEORczePEuN7rfQYHA0WETR/8qv5DV/PXbI2loWvYHNyPHcNx6WWNT4c5xDRe3gpV/0OD/1hBhkbPhaAovI0SWGP2czh+BBsLrjtOGvJaqqtudYRYtdZ1srEG113cgsCB5Lksah3Y5r6Xz7r8fVMcrdHMsEo2iQ9gMWNRS2cbvjdunvHA+i6ZRd0PTESVERP9sEAm+h/lSit+N3E8fKqkwiIrCsIiIAiIgCIiALyqot9j2fiYR6heqoRfJGEQVWYa0EMyAbm62megW0wiiYwZCx4rwxeB8dQWlpaA63pcew+a2tBbI3XmZe6Pc0yVWzb0rtOaz2Fa2mqGA5OB7lsI33tbkoI0y5PQuXm9xVklexhtIbZLz/1SF3wG/PMLrI9HnKFoMea0xuJ13bEc88v871vjI11y0+IWhxynN2AC+pcOTRrf28So4+JEMyuBi9GTwyuZYkhzC3Q5C3HwUyKKui6DdqS57c3MIb3HIlSqvRw9HjahVJBERXFAREQBERAEREAREQEbbaUbm1BDrbrnF0Z7yBcfIrTNkZCSXtkfwAF7u8hp6rt9uY7dTKNWh1vG7SPY+q5mmiEhBvbxysvPzKpM9jSXKCMaOthdY/ZJY8/jD7keV1nYXWjrS0FzgNLDQLMAawHjl5BY+C0wEj3jQmypNii17K1NWLud1RkaDx19FjU+Mzm4ZQNAvqJLfKy3YgAD7i4JzHsr46QgWbpfLsj6orONX7NS+Y5SGMtcNWAi/0XjJE+TtEHtWBbwaL3/wA8Ft6iEAnsi/NYrjZE6ZyULibLZOmb1twPhZmeZK69aXZtmTnWsLAD3/dbpb9OvA8fWO8lfQREV5lCIiAIiIAiIgCIiA5bbr4YTw7X7LQYU8cV1u19Nv0xIGbTfy4rjMIOoWDUrys9jQS8DLxqo3Ijut3ibX7hxWBgGPwkuabBwJycN0qlfWt3gHO3b8zZelIKe5ddt7a2VKNrZsW1xcdwRusdSW2bZZdPUmJwYc2n4D+yxm1sAaAZBkPBeLqxkl2tO8Rm0g3XXwRTvtGVVT3v4rxcF6TMAtfVeAePVR7ZKTVWdbs5fqBcZXy71tFhYPbqI7EHLge9Zq9PGqij57M7m2ERFMrCIiAIiIAiIgCIiAsljDmlrhcEWI7lHNRSupKh0btL3aebeCklaDbShbJTOkJDXxZtJNr/AJfP3sqc0N0TTpc3xy/GcticMUzSHMa4ci268MHtDcMkIaci09ofNY+HV7XDcdkVmwBu8RYW71gVo9uNSRm1LGygb8pk/KMmjy817GJoaAwAbugVvVsyAHjZY9XXhtmsG87jbguys4+EXVNTvLyoYHSyNYDmT6DiVil7nkMYwukccgB7Loqt8eEUUlTKQ6ctswc3HRo9z4KeKDkzPmyqMTnNncbZR4zVUW+fs0snZu64ZMQCfU3HopPXyzNUvfI6RziXudvOdxJJuSpY6P8ApDa8Npa5+68ZRzONg4cnHn3ren6PKnG+STkVGuBAINwdCNCqqRSEREAREQBERAEWi2j2spKAHrpLyW7Mbe08+XDzUS7S9JVZVbzIT9mhOVmHtkd7vpZcbokoNks47tfRUd2yyh0g/ts7b/Pl52UYYxttJiVfRwMBjpm1LSWXuZCDe7vouEfIcze5PFddsDgJ+0U8zhmSXN8LWHvdQtvgtjBI3+O4QY3OkjvuXuQNWfwtZT1cwORB8clJuJRQxxl8ps23me4cyuAjw18kxdHZsZdcN3ruDb+6i9NKXMUaI6yMOJM9Ietdm51hyGq3eFYNNNbq2brb5vdp/K6jBMCpNxsjD13edAeIsugYwAWAsOAVKw/ZbLVWvE1mFYNDStLhm+3bkdrb9goO6SdqjiFTaM/9NES2EfjPF/n7LuOmLazqYxQQOtJILzkasj/D5+3ioXur0qXBlbcnbKpfVFR2hQHUbJbc1VAQ0O62DjE92Q/SfuqaNmtrKWvaDC+0lu1E42e3y4jvC+awV709Q5jg5ji1wN2kOsQe4rqZFwTPqpFEOyPSk9m7FXDrGaCZo7bf1Dj46+KlPDcShqWCSCRsrDxadO48j4qadlMotGWiLBxrEW0tPNUP+GNhNuZ4DzNgukTWbU7X0uHgCQl8rhdsTPitzPIKONoulGpmaWU7RTNI7Tg7ff5G2XouIxLEZKmaSeV29I91yeXcO5YMjlBsvjBI9J53PJc4lzibkk3JK82qxXXyUSZtNnMMNXVQwAZOd2u5ozJ9AVO0GFtjmaQAA2OwAFgFwvQ1g5/rVbh+SM/N37fNSnuC5KnHg4yPdsjLLPE4ndiaSGN531cfReE9cKWMg5vLL58OQV+32KBkjGMI6zfyyvbnfut8yO9XUeBOrZqeRzw5l7zNAtYD66ea34nthbPPzrdkRsNjMKraMMqA90jJWXqKdx0/CW/nt9OS7HHdoYaWjfWON2ht2DQvcdG253Wa0ADkOAUGdKu032upNPG7+hC6xscnyaE+WnqsM3udm6EaVHH4lXyVE0k8rt6SR13FYxRWqsmHPsrDIeSuKsIQAK4K1XBcBeHLa4Djk9HKJIJCx3EfdeOThxC06qCug+sFHvTRiBjo4oQbGWW7u9rRf3LVIShvpuqt6qpofww383O/4hWPozwVsjgaLzevR5XkqzQAF6wxF72saLkmzRzJXmF2nRXgZqKwSuH9OHtHvf8AdH7+SIEvbN4YKWlhgb91gv3uOZPrdNqcTFJTOkv2rWZ+o5fytvE3PwUZdKeJb00cAOTRvO8Tp8r+qtxrdIrm6ic3jdayeaNkLSdN551ef8JPmpI2FgDQ8k5NaAfdRrgzAX6ealHZotZA57zuxtJc8nSwHsFtyuoMw4leRfhj9JW032OkO4bTzAthHFg4u8h87KAgt3tjj7q+rkmNxGDuwt/CwaeZ1WkXns9EoSrSqqiidBKtKEqtkBRVCKoQ6FTijigQ4fWKgDpPqxLitRY3EYaweIaL/MlT8Svl/EJjJLNITcvmc4nnclTkU4+zEeV5q96sUC4qeSn7o1wb7NQx3FnyDffzz0HpZQ1sbhP2uthiIuzevJ+gZn6ea+j4WZADRSRxl5cGsc4mwAuTyUB4zXGoqJZT955I7hw+SlTpLxkU9J1TTaSbIcw37x/bzUQU7eK1aePFmbPL0bSgdugAak2C3nSXj3UU0eHxGz3MHXWOjBw8z7Hmudo6tkTuufm2MXA/EeAXL4hWvnlfNIbve657u5NTLhI5p48tmOrShKoshrCoSr2MLiGgXJOSpJEWkhwsRqo2uieyW3dXBaFWyq1h5FUXTjTXZRVRChwtOqXVAqocPqbGarqaeeU/chc70aSvmV2g8Lqe+k+u6nC6nPN4DG99zn8rqA3mzWj8qnIrxrgx5CrQjzmqBQLCVOhSg/8AKnOnZY33P+1S5Bx7lxfRjQCDDIn/AHpSXu8zYfJoWw28xj7HQvsbSy9hnPPU+Q/ZTSukRbojPbrGftdZI4G8bDuR8rDj5m5Wla6wWMzmvOuqN1th8R07lutQRjpyZjYjU7xDRoDc95WESiosMpOTtmyMVFUEKLJw6SNrw+S5A+EAcVCTpWXYoKclFukbDCaMt7bhZx0vwCyamlbJYO4aEa+C8KnF2H4QfRYhxEk53tyCwOGVy3H1MdRoceJYrUomxxKJohNrdltm2+6Fzy2EuIAscyxNxlfgtctGnjJJ2eR/q5sWWcXi6oKhKoSrSVeeSXBERcBMPTnW2ipYAfieXuHgAB7lRTU62RFNkIdGKVVqIokz6YwWANipYGizY4Gbw8GjJRh0p4x19b1TTeOBu6ORec3H2HkiLRiXkU5Ojko1rKyTeeeQyCIp534kMK8jwJVERZTSWkqoKIgK3S6IgBcrHFEQHkXo05qiKIPW6qiID//Z'
			}, {
				'newname': 'CK Hutchison Holdings',
				'name': "Tzar Kuoi Li",
				"officer_role": "Group Co-MD & Deputy Chairman",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': '2 Dai Fu Street',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJwAxwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcBAAj/xAA8EAACAQMDAQYEBQEGBgMAAAABAgMABBEFITESBhMiQVFhBxQycUKBkaGxUhUjwdHh8DNDYnKCsggWNf/EABoBAAIDAQEAAAAAAAAAAAAAAAIDAQQFAAb/xAAlEQADAAICAgICAwEBAAAAAAAAAQIDERIhBDEFIhNBMlFhQiT/2gAMAwEAAhEDEQA/AH7ljmoLdXVxRWWLJploBXisdJI9i5VIgqW6uKmwSHavLCKcSPFTdJhKUh3OadhJDjFNohqZbQ5cZpFMCmkgrZyN0jAFOzynpNetYcLSp4vDiqja5Gc6XIFzSHJofcsSKJzR7moE6bGrMFzG0DG5pgg5rmq6jaabbtPcPkDhV3LGqLqXa67uJibMiCIbYxk/rWn4viZc3a9E5vMx4VqvZoiYEZLYAHrQTU5rdThp4gecdYqjx3E97IrXNxLInUA6k+RP+devrUQSBV6mznJ+xwd/1rSxfF8HuqMuvkv3MlmN9Zg475MgUtLm3f6Jkb7GqVuVBHK+HY8V1HdGXBOCMZJ/OrT8Kf0wV8nf7ReCu+BXMVW7HV5EmAlkJj69/ttVlR0lQPGwYHf8qp5cLxvRoYPJjOto4abYb04aRQLoczmNqQwp2m2G9SmQJxTTinqaajQLGyK9XTXqYLNEcZNMstSWG9Nuteaj0W0xgClKN67jelqtETsWnFTbUeIVFQVNth4xS79Ccj6DNsPD9qVMMivWw2FOTDaqDeqMxv7AiddzQTXLyOwspriQ46Fzv50fnGDmst+I+pB5EtYyXGM5B+k53BrV8DC82VT+i3+T8eN0Uy/vJbu5cySEguWxngE5xTKQNJsoySeMUT0DRLvWL4pBGSv4mxsBWg6T8PjhWkOWByPSvYc4x/VGHwvI+TKXomlPKGDJkOv9PBotqelTPAOlOrGcbeWP9/pWr6R2Wt7WAp3anPIxU06FblvFGMjjbilO6faGrHK6Zh0HZa6lbBgZTt5bHn/Smr7sjd2sfUUZhnyHFfQEem26coM8VFvrG2aNlMYIO2KF5bQaxY30fOTab3Tkgt1b4UkDNTtPuvlJgsvUqkdOM5A/Or12y7KhYzd2SDwnLIKz6bqHh6lB4x04z7HypipZZ0wEqwXtFm2ZAwIIPpSDzUPRpmeFomA8O4Of2qa31VmVPCmjdx5FkhUeptuacHFIbk1CCE021OeVNvRoFjderxrtMALfH2gtJWClsH3qX8/Aw+r96oNvp+oXYLxqSPepCQanEMPCWA9KqX8dC/jQmPMv/qS7JcRMdmFSU34NUSWHVkCvHbuPdak2Wr6jaOBdQuV8/Wk34Fa3FbGz5kvqlovCCplt9YoFpusW92oCtg+h5o9bEFxiszNFT/JB29rYathsKcmGxpNsNhTko2rNp/Yzm/sBdVIS1mYsFAUksfKsSVDe3rSdLSF2HdFt852BrZu0793pF2/ohrOOwdomo9oTKBmKzTqPh5Y7D+K9P8KuOOrIz7czBo3YzQINIslQIvesAXbHJq2QqqjAAobA4wMVMimFassXc66RPjxtXWx61HSTNKZiRtTtlfQmZgvFQZ36jipMqMajMnSerHFKrY+NA6fBypAIPlisv7b6GkU5ntlAD/UoHB9RWnTnct5ZqtdoYu9gJA8+fSkqnNdFjgrnTMr06dre9QPgeRBqysN/UetV7VoQZmkD4KkHBHO9HbdxLbxyDByPKi8lbSpBeDXFuBzyptuaeA8NNMN6qI0WIpD05SGG1GgRkivV016mbFs2m00CC3hCBR+ldn063gjZii7UcZhjnaq12j1EQwuoYZPpRXKSMuLqmV/UdZig64xGPaqu158w7u+Bk8YrtwTdTuxbIoa2FlIB4oJwLWxrzaY7dwg4khbpccFas/ZDVnnHc3Ozqcb1Vg4H1H96Xb3Xy86TISMHfFI8nxnkxtMdiyrembJanIFPTDahfZ27W7sIpVYHI5BorNxXkskubaYnJ1ZVu2gJ7P3uM7Jnaqd8MJRFBesAOtpFP35q89prf5rRr2H+qI1mPYC4EE7R7bnj1r0vxNf+el/pN/zk1q3kHFTYsvwKD2fVI4GcDFGFvbW2jy7gY56q1IByf0E7aEdO9PFFG1BoNetJ9opoyR5A5p5tQyhYH9DTuUlbhTYQlaNB4zgfeoFxeWqKfGCfvVP7Tajf3TfL6eelx9TZwFHqT5ChFjpskRH9ra8TI7YSFG3f2AIJP5CheTYajiXO5eCUZSRR7ZoXc2pdHQ/iH6UCSS2ViLG7t5879Ds2T9uo/wAUQkhkvYYo0mlt87t3bE4x+m1IfbLCbSM+7Q2b2t04dcgEn05prRJeqFot/Ccj7VZu2kUUVuhu2kLjYSqvVn2YUJ0Ls9ciOW7lkSKFwBF1ZzIft6Uy2niOxbnPtDh4pl+akyo0bFH+obGoz81Tk1GJpDUuktvRIEaNcrpG9epgJu1xKSCqniqJ2mmIdwzeWd6t5uIAo6pl5rOfiDqEMV2RFKrZTgU1fajJX1TYHtr2NVkPUM5qD36tI7MeeKAfOuA+G5pD3bcZNX1KRTdtlilmUkDNKHiDAnY1XI7ty+dzRGS6ICjg4qKhNEzkZpHw41JVD2bndDkZPlWgSEFcg1hXZ3VPkNXglLdKt4WrW7PVIpowVbIxmvHfL+I8efkvTNGE8sKkSLxBIjo3DDB/Osd0SFrPtQ1q4wVmI/etemmUjINZ12v02Wz1cavHGTbv0946n6DkZP6VY+IvjTh/sLJD4J/0Wq8vpbZOm26S++SeF+/60GvILd7fvda1SRFIyUU9OPfPpR22tQscsIwwXxDA5Hr/AL9Kgw6HJNdzy3kcdwk8ZTu5Pwg+lbK66BfYG0htAW4Y6dNO+CAWMxCn03Iq4adFJe3CWdtI8fXGZCWJbGDjFRNN7O6dpkRj+XU9RBbq8Rc+Wc0f0S3Cao86rgBFjUYI9870a02JbfErt1DcR3Ulo2EliOw4VweGH6YoJc9iVvo0kvLrF10kPLGM9e+fPjHArStY03v5RNH4ZIySrffyPtQr5WQA907owO4wP2OMiue5ZKaqQCOz1vNPA0wIEEaxhuCQBtk+dHYbGGJQ4R+lVwvVyx8z+wqXaoyNuhYj8R3pVwzKGc4O23lQ662FvfRWdWt06HSQBjjbIzULu2uLYSAjCHw49B5VP1GQu/VjGTTsEcMaTOiBY2QnpHAOKUy0npFQ1ZQt4wHoP4oa+xoprX/6lwPRsfsKFy80ley4vQikNSq41GQNMa9Xmr1GCaXd6Cp2DyBf+871X+0HZGCWEmFT3p/FnNXF7gyKM0iY5jI86lPj2ih76ZiNz2cure5aH3opbdkQ0PXcyFTV7ktFnvSzKNvWpclj8wRFEoGMZNMfkWwZ8fHL2yiXnZFkt1ltD1KvI9ar+oQhG8WzryK1nXbiHSNHfzcKSfeshubo3U7uwx1HNP8AHun7EeRML0NPIceE4NWvsjq9w39y7E9PBqqhciivZ5u41BT/AFDFL8yJvE00N8J1NrvpmrW8jSx8eVN3TjLRTxq8MgHhYA59aRpsmYh9qkzhZFAYDP4W9DXnvFpRl7NPJPsescMCC7IyN4XUDKj8wRRTuUZF/vJJCODsv/qBQmSMxOpB8Ei80Ys2UIAx3/mt2W/RnZEvZ6G0XPeEDI9s1Lsj0DbZVbJY+ZpTuAgVCATtx/hQDtL/AGqcWumP3ODnvO76sj05pnSEpcumWa6vocDrdQDtzQ8mEXSdzOHEnk29U+80zVb+JbW4DYZcM4PSD+n+FHNA0EaVJH/e5EYwFHAzQ8nQTiYXsMvLGvsRQrU71AhU7qdjjmit8tu0RJwGA2PnVXuIe+dsMejGcn+Ki9h4kn2D2BecFiTsaLWlssloskn/AA0JcqTjJHrQ5l6CW8zUHUdUurezmhhkARuD5jPP8Ukt65a0AbiYz3MsrHJdySajS80tOM0iWlT7Lg3SWrtcajQA23NerjV6jBNaEfgBFMztipZcJHyKDX94oJGRmuZRldjihevqonHd2dtah5WAc81XluhjNIW1N/OGbPSOKHehjnfshdpVk10mOBSsIO+POqNqNj8lIRIuBWuwWLkdxaxhnxgkDYVnfba2NprEUE7A9fkKsYXWxGX8bWt9ldiCMB0inrdu6vovvRD5FVhDJgbbVCgheTUokG+DnapqlSYyIccUaRozkwrn0FEJsgZqBo8RWJc+lT5+DXmLf3ejSv8Alo5BM7+AknG4Hl71NEzr0kGghkaNiVO/FFbSQSRqSRj2rY8XNznj+ypnxcft+gvpc6mXqlOTjz4og0yOCRgKOWJxQCTwqWiOGoXqUV9cIsc1+iQHyRN/5xmrqrRSeJWyx3Wt6TbyEPc5ZRwvn9qE33bCC2I6rCRQThQwOT+1R7SDTLK0MdxeXEnUBuQoZj98V251WzwfkYD3mw638TfkTXO/9CnEt+hmTU77VXJW2a1jUcucFvYCpAkMVqqdfUQvPrUUTS93l2LSuefSo88jDI/Pc0tvY5Sl0Iv7lY49zvioN7p3zfZLVNUlyot1UwEH8YYZ/nFQL2WS8vVhhUyHqCqoO7N5CrB8SrmPRexdhoiP1TXTDq8sqm7H7dRA/P2puHHyYjyM3CdIpen3XzcHUdpB9Q/xpcgPViq/DLLAOqFyjkY2p7TtZmuZXgmjyycmpyeHSbcDcPyENJX7Cx2NJauh0ceE7+Y9KS1Vmmnpl9NNbQhq9XGrlGCaPd3mxGaCOWlkJ5p5uuU4NSreAbDGaArDdtbNIQOBVh0uwaVlgtwS34m9K7o2myXzhYh0xj6nx/FW8R2mi2LOelFRd2NWMGB32/RU8jyFHS9g3V7yx7LaPJcSkdQXYebN7VgOsXVxq1/Jf3WO8c+FRwB5CrD2y7RTdotS6lY/LREiJfX3ofb2IBEk3PpWlwULSKE7p7YPjluukB8dPvRPs5FE123X/wAU7j7UuSJcbLtUB2kgmjlj2ZDkY/iqefEsmNzPTL2HM4tN9mn2kQSEHHlTc52NQ9E1mG/s13AcDcentUmVwVO9eSeOopzXs1pfJ8iBNyaVZ3Xy745U0iY81Fc+tWMVuKTRZ4K54ssRmWRMoaUsbuvTyD5VVxfCyQyyzdCD259qMaLrQmtYblo2CTZKeuASM/qK18V/kWzMz4/xVpE06N3x6thjb7U9Ho0CDDucjfam59ZC56cAUNudeaRSsS52xkUzSEt2wvKltChwy5WqXqeotPcSpGNs46val3N5dXGUVyAdj/pVj7Ndm47fou71D3o3WNvw+59/4+9HMcnpA3kWNdjvYzs98sVvrxSJyCUVtu7X1PuazHtv2gHaPtFNdxHNrEO5tveNSd/zJJ/MVoXxP7QHR9D+RtnK3uogpsd0iH1n7nOPzPpWOR7f5VoYcakzbt29slL5Dzp3TbcJczTEfUAKYi3I9TRWCPojpzBR1gQepDg05HMrHokGGrwFNypnHpVfJhm12izh8i8Xr0OOK9TKyldpAWHkRXKpPDaejTnycdLezRIYT1ZxzRbStPe9nEacA+I+lA9EvWv7OBkQ9b7AVp2g6ctjaKGAMhGWPrUYcLqtP9FbPnUz0S7S3hsLUIgCqorKfib2oe5n/sy0fC/80j09Ku3bvX49I0qQg4cjAxzmsLDy3dy88zEyStkmteJSRkvbZJsYsjqYccUSjbOzUxkQxquRUeW9WMZNLe7ZYlqEEZAp2Wh9yoGfCagPqE8u0eQKQ17NGMSNke9d+HR35SRaXsthcd5GdvxD1q12mppcwh0bY1R3nSUZU705pt41rcjIPQxwfas3zfCnJ9kuy/4fl8Hxr0X1n6lwOTUC9vYbZD1sOryWh1xq7LGUhzv+I80AuJmkfLMSTVPxvjXT3k9F3yPkZx/XH2xWq38l4zNIfAPpXyFXXRgf/peh6ov0W8k1vMf+kuTk1n0oLjFbL8NbCLUPh8LSZQY5JZgc+R6ua2Kwyo4yjEeanfOu2QJYoiePf70PuEVcLEowT5Dk+lDhd3mgaw+h6orO64Fuw37xScDHr6Ve9M0j5boubpeq5/An9H+tUvxVy0X3nhTyI2haGbcpc3ag3PKRk5EQ9T70ckkit4WmlbpijBZifIDkmuhelSXPr1E+dZ78We0DW9jHo9u+J7rxzgcrF5D2LEfoCKvY4UrRm5Mjt7Zn/aXW5e0eu3GoSHETHpgT+mMfSPv5n3JqEo3ApuEYGafQHjG5qyhZLtI8tk8USj+nFRIh0KFqUnFcSLG1cO4NeB3pLtjGM7nyFQ0cNuANjz9q9XS3kf3r1DoM074QNDqOmShwDNbSYzjyIyK0qd1hiJ42rJv/AI/DMetuSch4QP0atJ1uRltpiDuEOKbklfkehDb0tmQ/E7UzfamLZD/dxHLb+dVmxIx1+gpOszSTXNzJIcsZSCa5Dtb7VFddByOXEzM2F5qM8SgdVzJ0r6etcuJGjQleag26/MtmZmbfjNciH2yS16D4LOH/AMyKhvFcyHLnejqQxxphFApPV0AkAbe1SyEBVsLoeJdqlwgoQXwW4BqHqGo3JYr1gAHyFPQOWjjJ5KAmgaQa9EmWUk870yvUx8vvSSa8hIbArtHHn56RxWw/DfV7TR/hxJqF+xEFvPKSFGS2+wHvWOTHCEjmrbYztH8P9PGFdPn536HGVJxgEj2zUa7IZYIu29vf3cerX+ipcTWXU4VUHXaoRg4bzJyN+CQfvWgWN5ZavZi+06dJ4WP1Kc9J81PvWII5EUnTkIseTH1HpdR0YU78ZJO2D70iDVb3s4unajplw6z3rstyHJZZQDwR+f3pzwzraF8ns2DVr+302xnvbxgttap1n1Pp999h71886rqNxrOqXOo3RJknctj+keQHsBgflWn/ABfv527PaXGCEju3DyqowDgAgfbJ/YVlKDDYFJSDQ/GpCY86lQDDMxBwNt/WmAcKSOQM0u3Ym3jJPKgn86Ikmxkk5qQrnbFRo9kGKdBIXI5qSdjjyCMZc4PkAaiS3hOwFNTMWO5qOeag4caZicZ/evVFmdlG1drtHH//2Q=='
			},
			{
				'newname': 'CK Hutchison Holdings',
				'name': " George Colin MAGNUS",
				"officer_role": "Non-executive Directors",
				'country': 'Hong Kong',
				'img_src': 'http://www.huskyenergy.ca/images/people/George-Magnus.jpg',
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': '',
					'country': 'Hong kong'
				}
			},
			{
				'newname': 'CK Hutchison Holdings',
				'name': " LEUNG Siu Hon",
				"officer_role": "Non-executive Directors",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'https://cdn.southampton.ac.uk/assets/imported/transforms/site/student-testimonial/Photo/BEDF597A37974F3A9BAC631FBB07FB99/MR_Sh_Leung.jpg_SIA_JPG_fit_to_width_INLINE.jpg'
			},
			{
				'newname': 'CK Hutchison Holdings',
				'name': " LEE Yeh Kwong, Charles",
				"officer_role": "Non-executive Directors",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'https://www.ourhkfoundation.org.hk/sites/default/files/26/r-Lee%20Charles.JPG'
			},
			{
				'newname': 'CK Hutchison Holdings',
				'name': " CHOW WOO Mo Fong, Susan",
				"officer_role": "Non-executive Directors",
				'country': 'Hong Kong',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': '',
					'country': 'Hong kong'
				},
				'source_url': ' https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322',
				'img_src': 'https://crunchbase-production-res.cloudinary.com/image/upload/c_thumb,h_256,w_256,f_auto,g_faces,z_0.7,q_auto:eco/v1397752313/c83106e47e9b6ef3cd7099f57c408cf5.jpg'
			},
			{
				'newname': 'OCBC Bank',
				'name': "OOI SANG KUANG",
				"officer_role": "CHAIRMAN",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/ooisangkuang.jpg',
				'mdaas:RegisteredAddress': {
					'streetAddress': 'Kuala Lumpur',
					'city': 'Wilayah Persekutuan ',
					'country': 'Cayman Islands'
				},
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html'

			},
			{
				'newname': 'OCBC Bank',
				'name': 'CHUA KIM CHIU',
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/bod_chua-kim-chiu.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}
			}, {
				'newname': 'OCBC Bank',
				'name': "LAI TECK POH",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/laiteckpoh.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}
			}, {
				'newname': 'OCBC Bank',
				'name': "LEE TIH SHIH",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/drlee.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}

			}, {
				'newname': 'OCBC Bank',
				'name': "CHRISTINA ONG",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/ooisangkuang.jpg',
				'mdaas:RegisteredAddress': {
					'streetAddress': 'Kuala Lumpur',
					'city': 'Wilayah Persekutuan ',
					'country': 'Cayman Islands'
				},
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html'

			},
			{
				'newname': 'OCBC Bank',
				'name': ' QUAH WEE GHEE',
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/quahweeghee.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}
			}, {
				'newname': 'OCBC Bank',
				'name': "PRAMUKTI SURJAUDAJA",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/pramukti.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}
			}, {
				'newname': 'OCBC Bank',
				'name': "TAN NGIAP JOO",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/tanngiapjoo.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}

			}, {
				'newname': 'OCBC Bank',
				'name': "SAMUEL N. TSIEN",
				"officer_role": "Member of the Board of Directors",
				'country': 'Cayman Islands',
				'img_src': 'https://www.ocbc.com/assets/images/uploads/gcc_personnel/directors/2014/samueltsien.jpg',
				'source_url': 'https://www.ocbc.com/group/media/leaders-board-of-directors.html',
				'mdaas:RegisteredAddress': {
					'streetAddress': '',
					'city': 'George Town',
					'country': 'Cayman Islands'
				}

			}
		],
		CountriesofOperation: [{
				'newname': 'CK Hutchison Holdings',
				'Country': "Spain",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Poland",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Iraq",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Hong Kong",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Pakistan",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Tanzania",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Sweden",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Germany",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Argentina",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Vietnam",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Myanmar",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Mexico",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "The Bahamas",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Myanmar",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "Belgium",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "UK",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			},
			{
				'newname': 'CK Hutchison Holdings',
				'Country': "The Netherlands",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Egypt",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Saudi Arabia",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "UAE",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Oman",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Thailand",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Mainland China",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "South Korea",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Indonesia",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Australia",
				"Risk": "Low",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
				
			}, {
				'newname': 'CK Hutchison Holdings',
				'Country': "Panama",
				"Risk": "Medium",
				"source_url": "http://www.ckh.com.hk/en/businesses/global.php"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Macau",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Hong kong",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Austria",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Denmark",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Indonesia",
				"Risk": "Medium"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Ireland",
				"Risk": "low"
			}, , {
				'newname': '3 (telecommunications)',
				'Country': "Italy",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "Sweden",
				"Risk": "low"
			}, {
				'newname': '3 (telecommunications)',
				'Country': "United Kingdom",
				"Risk": "low"
			}, {
				'newname': 'OCBC Bank',
				'Country': "Malaysia",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
				
			}, {
				'newname': 'OCBC Bank',
				'Country': "Indonesia",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
				
			}, {
				'newname': 'OCBC Bank',
				'Country': "Hong Kong",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
				
			}, {
				'newname': 'OCBC Bank',
				'Country': "China",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
				
			}, {
				'newname': 'OCBC Bank',
				'Country': "Australia",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
				
			}, {
				'newname': 'OCBC Bank',
				'Country': "United Kingdom",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank"
			}, {
				'newname': 'OCBC Bank',
				'Country': "United States",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Malaysia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Indonesia",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Pakistan",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "Medium"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "China",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Australia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "United Kingdom",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "United States",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			},
			{
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Poland",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			},
			{
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Netherlands",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			},
			{
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Turkey",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				'Country': "Austria",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Lithuania",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Croatia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Saint Lucia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Zimbabwe",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "Medium"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Canada",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				'Country': "Finland",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Tunisia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Japan",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Trinidad and Tobago",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Georgia",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
				"Risk": "low"
			}, {
				'newname': 'BRITISH AIRWAYS PLC',
				'Country': "Morocco",
				"Risk": "low",
				"source_url": "https://en.wikipedia.org/wiki/British_Airways_destinations",
			}
		],
		corporateStructure: [{
			"name": "CK Hutchison Holdings",
			"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322",
			"subsidiaries": [{
					"percentage": "66.09%",
					"name": "Hutchison Telecommunications Hong Kong Holdings Ltd",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"
				},
				{
					"percentage": "87.87%",
					"name": "hutchison telecommunications (australia) limited",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"
				},
				{
					"percentage": "75.67%",
					"subsidiaries": [{
							"percentage": "38.01%",
							"name": "power assets holdings ltd",
							"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

						},
						{
							"percentage": "33.37%",
							"name": "hk electric investments ltd",
							"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

						}
					],
					"name": "ck infrastructure ltd"
				},
				{
					"percentage": "60.31%",
					"name": "Hutchison china meditech limited",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

				},
				{
					"percentage": "40.19%",
					"name": "Husky Energy inc",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

				},
				{
					"percentage": "30.07%",
					"name": "hutchison port holdings trust ",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

				},
				{
					"percentage": "36.73%",
					"name": "Tom group ltd",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

				},
				{
					"percentage": "45.32%",
					"name": "CK Life Sciences Intl., (Holdings) Inc.",
					"source_url": "https://www.bloomberg.com/research/stocks/private/people.asp?privcapId=411322"

				}
			]
		}, {
			"name": "OCBC Bank",
			"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",
			"subsidiaries": [{
					"percentage": "",
					"name": "Bank OCBC NISP",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",
					"name": "Great Eastern Life",
				},
				{
					"percentage": "",
					"name": "OCBC Wing Hang Bank",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"name": "Bank of Singapore",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"name": "Bank of Ningbo",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"name": "OCBC Al-Amin Bank Berhad",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				}, {
					"percentage": "",
					"name": "OCBC Securities",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"name": "Singapore Island Bank",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				},
				{
					"percentage": "",
					"name": "Great Eastern Holdings Ltd",
					"source_url": "https://en.wikipedia.org/wiki/OCBC_Bank",

				}
			],

		}, {
			"name": 'BRITISH AIRWAYS PLC',
			"source_url": "https://en.wikipedia.org/wiki/British_Airways",
			"subsidiaries": [{
					"percentage": "",
					"name": "British Airways",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"
				},
				{
					"percentage": "",
					"name": "Iberia",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "",
					"name": "Aer Lingus & Aer Lingus Regional",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "",
					"name": "Sun Air of Scandinavia",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "",
					"name": "Iberia Regional",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "100%",
					"name": "BA CityFlyer",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				}, {
					"percentage": "100%",
					"name": "OpenSkies",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "100%",
					"name": "Iberia Express",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "90.51%",
					"name": "Great Eastern Holdings Ltd",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "100%",
					"name": "LEVEL",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "18%",
					"name": "Comair",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "1%",
					"name": "Royal Air Maroc",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "2.3%",
					"name": "Air Mauritius",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				},
				{
					"percentage": "4.61%",
					"name": "Norwegian Air Shuttle ASA ",
					"source_url": "https://en.wikipedia.org/wiki/British_Airways"

				}
			],

		}],
		associateddocument: [{
				'newname': 'CK Hutchison Holdings',
				"url": "https://www.ocbc.com/assets/pdf/Credit%20Research/Corporates%20Reports/2017/OCBC%20Asia%20Credit%20-%20CK%20Hutchison%20Holdings%20Ltd%20Credit%20Update%20(28%20Aug).pdf",
				"meta_tags": [{
					"author": "trt2",
					"producer": "Microsoft® Word 2010",
					"title": "Credit Outlook –",
					"creator": "Microsoft® Word 2010",
					"moddate": "D:20170828113624+08'00'",
					"creationdate": "D:20170828113624+08'00'"
				}],
				"document_title": "CK Hutchison Holdings Ltd Credit Update",
				"mime": "application/pdf",
				"file_format": "PDF/Adobe Acrobat",
				"date": "2017-08-14"
			},
			{
				'newname': 'CK Hutchison Holdings',
				"url": "http://www.ckh.com.hk/upload/attachments/en/pr/e_CKHH_AR_2016_full_20170322.pdf",
				"meta_tags": [{
					"creationdate": "D:20170322095446+08'00'",
					"creator": "Adobe Acrobat 10.0.0",
					"author": "SC13823",
					"producer": "Adobe Acrobat 10.0.0",
					"moddate": "D:20170322115301+08'00'"
				}],
				"document_title": "CK Hutchison results for the year ended 31 December 2016",
				"mime": "application/pdf",
				"file_format": "PDF/Adobe Acrobat",
				"date": "2016-10-31"
			},
			{
				'newname': 'OCBC Bank',
				"url": "http://www.ocbcwhhk.com/webpages_cms/files/Media%20Release/2014/English/20140401_WHB_press%20release_EN2.pdf",
				"document_title": "OCBC Bank To Acquire Wing Hang Bank By Way of Voluntary ",
				"mime": "application/pdf",
				"file_format": "PDF/Adobe Acrobat",
				"date": "2017-03-23"
			}, {
				'newname': 'OCBC Bank',
				"url": "http://www.aozorabank.co.jp/about/newsrelease/2015/pdf/15082701_n.pdf",
				"document_title": "OCBC  Lion-OCBC Capital Aisa Fund I, L.P",
				"mime": "application/pdf",
				"file_format": "PDF/Adobe Acrobat",
				"date": "2017-04-09"
			}
		],
		ocbcStructre: {
			'name': 'OCBC Bank',
			'title': 'Company',
			'children': [{

					"title": "",
					"name": "Bank OCBC NISP"
				},
				{
					"title": "",
					"name": "Great Eastern Life"
				},
				{
					"title": "",
					"name": "OCBC Wing Hang Bank"
				},
				{
					"title": "",
					"name": "Bank of Singapore"
				},
				{
					"title": "",
					"name": "Bank of Ningbo"
				},
				{
					"title": "",
					"name": "OCBC Al-Amin Bank Berhad"
				}, {
					"title": "",
					"name": "OCBC Securities"
				},
				{
					"title": "",
					"name": "Singapore Island Bank"
				},
				{
					"title": "",
					"name": "Great Eastern Holdings Ltd"
				}
			]
		},
		britishAirwaysStructure: {
			'name': 'British Airways PLC',
			'title': 'Company',
			'children': [{

					"title": "",
					"name": "British Airways"
				},
				{
					"title": "",
					"name": "Iberia"
				},
				{
					"title": "",
					"name": "Aer Lingus & Aer Lingus Regional"
				},
				{
					"title": "",
					"name": "Sun Air of Scandinavia"
				},
				{
					"title": "",
					"name": "Iberia Regional"
				},
				{
					"title": "",
					"name": "BA CityFlyer"
				}, {
					"title": "",
					"name": "OpenSkies"
				},
				{
					"title": "",
					"name": "Iberia Express"
				},
				{
					"title": "",
					"name": "Vueling Airlines"
				},
				{
					"title": "",
					"name": "Comair "
				},
				{
					"title": "",
					"name": "Royal Air Maroc"
				},
				{
					"title": "",
					"name": "Air Mauritius"
				},
				{
					"title": "",
					"name": "Norwegian Air Shuttle ASA"
				}
			]
		},
		ckhutisonstructure: {
			'name': 'Ck Hutchison Holdings Limited',
			'title': '',
			'children': [{
					'name': 'Hutchison Telecommunications Hong Kong Holdings Ltd',
					'title': '',
				}, {
					"name": "Hutchison Telecommunications (Australia) limited",
					"title": ""
				},
				{
					"name": "Ck Infrastructue Holdings Limited",
					"title": "",
					'children': [{
						'name': 'Power Assets Holdings Ltd',
						'title': '',
						'children': [{
							'name': 'hk electric investments ltd',
							'title': ''
						}]
					}]
				},
				{
					'name': 'Hutchison China Meditech Limited',
					'title': '',
				},
				{
					"title": "",
					"name": "Husky Energy inc"
				},
				{
					"title": "",
					"name": "Hutchison Port Holdings Trust "
				},
				{
					"title": "",
					"name": "Tom Group Ltd"
				},
				{
					"title": "",
					"name": "CK Life Sciences Intl., (Holdings) Inc."
				}
			]
		},
		personOccupatinAndIndustry:{
		    "Profession": "Stock Brocker",
		    "business": [
		        {
		            "type": "Stock Exchange",
		            "corporation-name": "Nasdaq Stock Market",
		            "corporation-reg-no": null,
		            "corporation-address": "One Liberty Plaza 165 Broadway, New York City, New York, U.S.",
		            "position": "Non-executive chairman",
		            "salary": "35,200 USD",
		            "link-to-company": null
		        },
		        {
		            "type": "Investment Fund",
		            "corporation-name": "Madoff Investment Securities",
		            "corporation-reg-no": 748562,
		            "corporation-address": "885 Third Avenue, New York, USA",
		            "position": "Founder, Director",
		            "salary": null,
		            "link-to-company": "https://beta.companieshouse.gov.uk/company/01705962/officers"
		        }
		    ]
		},
		affliatedEntities:[{
			"type": "Company",
			"relation":"100% Shareholder",
			"name": "Bernard L. Madoff Investment Securities LLC ",
			"actityType": "Investment Services",
			"Director": "Bernard Lawrence Madoff",
			"tel": "1486253",
			"email": "info@madoffinvest.com",
			"web": "www.madoffinvest.com",
			"address": "1486253, info@madoffinvest.com, 885 Third Avenue, New York, USA"
		}, {
			"type": "Individual",
			"name": "Madoff, Peter B",
			"dob": "11/1/1946",
			"address": " p.madoff@gmail.com, +1 004 481 45 321, 34 PHEASANT RUN,USA ",
			"Passport": "A4854512",
			"email": "p.madoff@gmail.com",
			"tel": "+1 004 481 45 321",
			"relation": "Brother",
			"Profession": "Financist",
			"SocialSecurityNumber": "48561454147852",
			"JobDetails": {
				"title": "Director of National Stock Exchange| 25,000 USD",
				"company": "National Stock Exchange",
				"address": "Jersey City, New Jersey, United States"
			}
		}],

		relationAnalysisData:{
			"edges": [{
				"id": "#edge1",
				"labelE": "brother",
				"to": "#node2",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge2",
				"labelE": "Associate",
				"to": "#node3",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge3",
				"labelE": "Associate",
				"to": "#node4",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge4",
				"labelE": "Associate",
				"to": "#node5",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge5",
				"labelE": "Owner",
				"to": "#node6",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge6",
				"labelE": "Company",
				"to": "#node7",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge7",
				"labelE": "Company",
				"to": "#node8",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge8",
				"labelE": "Spouse",
				"to": "#node9",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge9",
				"labelE": "Son",
				"to": "#node10",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge10",
				"labelE": "Son",
				"to": "#node11",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge11",
				"labelE": "Niece",
				"to": "#node12",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge12",
				"labelE": "Company",
				"to": "#node13",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge13",
				"labelE": "Director,CEO",
				"to": "#node14",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge14",
				"labelE": "Secretary",
				"to": "#node15",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge15",
				"labelE": "Secretary",
				"to": "#node16",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge16",
				"labelE": "Board of Director",
				"to": "#node17",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge17",
				"labelE": "Board of Director",
				"to": "#node18",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge18",
				"labelE": "Board of Director",
				"to": "#node19",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge19",
				"labelE": "Board of Director",
				"to": "#node20",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge20",
				"labelE": "Person",
				"to": "#node21",
				"from": "#node6",
				"weight": 1
			},
			{
				"id": "#edge21",
				"labelE": "Mother",
				"to": "#node22",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge22",
				"labelE": "Father",
				"to": "#node23",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge23",
				"labelE": "Step Father",
				"to": "#node24",
				"from": "#node1",
				"weight": 1
			},
			{
				"id": "#edge24",
				"labelE": "Senior Managing Director",
				"to": "#node6",
				"from": "#node2",
				"weight": 1
			}
		],
		"vertices": [{
				"caseId": "",
				"name": "Bernard Madoff",
				"id": "#node1",
				"labelV": "Person",
				"start": "",
				"riskScore": 85,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Madoff Peter B",
				"id": "#node2",
				"labelV": "Person",
				"start": "",
				"riskScore": 75,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Fred Wilpon",
				"id": "#node3",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Michael lennox paton",
				"id": "#node4",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "George Louis Theodule",
				"id": "#node5",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": " Bernard L. Madoff Investment Securities LLC",
				"id": "#node6",
				"labelV": "Company",
				"start": "",
				"riskScore": 80,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Lion Global Investors Limited ",
				"id": "#node7",
				"labelV": "Company",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Accerent S.A.",
				"id": "#node8",
				"labelV": "Company",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Ruth Alpern Madoff",
				"id": "#node9",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Andrew Madoff",
				"id": "#node10",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Mark Madoff",
				"id": "#node11",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Shana Madoff",
				"id": "#node12",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "Nasdaq Stock Market",
				"id": "#node13",
				"labelV": "Company",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "RAVEN, Stephen Ernest John",
				"id": "#node14",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "ALLEN, Peter Robert",
				"id": "#node15",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "FLAX, Leon",
				"id": "#node16",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "BOND, Colin Edward",
				"id": "#node17",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "DALE, Christopher James",
				"id": "#node18",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "MARSHALL, Antony Duncan Trevit",
				"id": "#node19",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "PURCELL, John",
				"id": "#node20",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "STEVENSON, Malcolm",
				"id": "#node21",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "TOOP, Philip John",
				"id": "#node21",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "SYLVIA MUNTNER",
				"id": "#node22",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "RALPH MADOFF",
				"id": "#node23",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			},
			{
				"caseId": "",
				"name": "JACOB GOLDSTEIN",
				"id": "#node24",
				"labelV": "Person",
				"start": "",
				"riskScore": 0.001,
				"ir": 0,
				"dr": 0,
				"tr": 0,
				"properties": {}
			}
		]
	}
	})
	.constant('mockedCustomerConst', {
		identificationInfo: {
			'Customer Registration Number ': 'C145052',
			'Customer Registration Date': '3/1/2005',
			'Customer Name': 'Bernard Lawrence Madoff',
			'Date of Birth': 'April 29, 1938',
			'Gender': 'Male',
			'Home Phone': '+1 004 481 78 45',
			'Business Phone': '+1 004 481 78 45',
			'E-mail': 'Bernard Lawrence@hotmail.com',
			'Legal Address': '885 Third Avenue New York',
			'County': 'USA',
			'Citizenship': 'USA',
			'Nationality': 'USA',
			'Passport': 'A487485212',
			'Social Security Number': '486255185',
			'Profession': 'Stock Brocker'
		},
		sourceUrl: 'http://www.nndb.com/people/845/000179308/',
		relationshipInfo: 
			[
			    {
			        "mdaas:RegisteredAddress": {
			          "streetAddress": "885 Third Avenue",
			          "city": "New York City",
			          "zip": "10022",
			          "fullAddress": "885 Third Avenue New York City 10022"
			        },
			        "name": "Bernard Lawrence Madoff",
			        "sanction_bst:description": "",
			        "listId": "4563578",
			        "@source-id": "",
			        "high_risk_jurisdiction": "",
			        "sanction_url": "",
			        "sanction_source": "",
			        "pep_url": "",
			        "$$hashKey": ""
			      },
			      {
			      "mdaas:RegisteredAddress": {
			        "streetAddress": "885 Third Avenue",
			        "city": "New York City",
			        "zip": "10022",
			        "fullAddress": "885 Third Avenue New York City 10022"
			      },
			      "name": "Madoff, Peter B",
			      "sanction_bst:description": "",
			      "listId": "78562",
			      "@source-id": "",
			      "high_risk_jurisdiction": "",
			      "sanction_url": "",
			      "sanction_source": "",
			      "pep_url": "",
			      "$$hashKey": ""
			    },
			    {
			        "mdaas:RegisteredAddress": {
			            "streetAddress": "Palm Beach County",
			            "city": "Florida",
			            "zip": "",
			            "fullAddress": " Palm Beach County Florida"
			          },
			        "name": "George Louis Theodule",
			        "sanction_bst:description": "",
			        "listId": "4527852",
			        "@source-id": "",
			        "high_risk_jurisdiction": "",
			        "sanction_url": "",
			        "sanction_source": "",
			        "pep_url": "",
			        "$$hashKey": ""
			      },
			      {
			        "mdaas:RegisteredAddress": {
			            "streetAddress": "Marlborough Street",
			            "city": "Nassau",
			            "zip": "",
			            "fullAddress": " Marlborough Street Nassau New Providence Bahamas "
			          },
			        "name": "Michael Lennox",
			        "sanction_bst:description": "",
			        "listId": "58652",
			        "@source-id": "",
			        "high_risk_jurisdiction": "",
			        "sanction_url": "",
			        "sanction_source": "",
			        "pep_url": "",
			        "$$hashKey": ""
			      },
			      {
			        "mdaas:RegisteredAddress": {
			            "streetAddress": "Marlborough Street",
			            "city": "Nassau",
			            "zip": "",
			            "fullAddress": " Marlborough Street Nassau New Providence Bahamas "
			          },
			        "name": "Fred Wilpon",
			        "sanction_bst:description": "",
			        "listId": "1456285",
			        "@source-id": "",
			        "high_risk_jurisdiction": "",
			        "sanction_url": "",
			        "sanction_source": "",
			        "pep_url": "",
			        "$$hashKey": ""
				  },
				  
				  {
					"mdaas:RegisteredAddress": {
						"streetAddress": "One Liberty Plaza 165 Broadway",
						"city": "New York City",
						"zip": "",
						"fullAddress": " One Liberty Plaza 165 Broadway, New York City, New York, U.S."
					  },
					"name": "Nasdaq Stock Market",
					"sanction_bst:description": "",
					"listId": "",
					"@source-id": "",
					"high_risk_jurisdiction": "",
					"sanction_url": "",
					"pep_url": "",
					"$$hashKey": ""
				  },
				  {
					"mdaas:RegisteredAddress": {
						"streetAddress": "885 Third Avenue",
						"city": "New York City",
						"zip": "",
						"fullAddress": " 885 Third Avenue, New York, USA "
					  },
					"name": "Madoff Investment Securities",
					"sanction_bst:description": "",
					"listId": "",
					"@source-id": "",
					"high_risk_jurisdiction": "",
					"sanction_url": "",
					"pep_url": "",
					"$$hashKey": ""
				  },
				  {
					"mdaas:RegisteredAddress": {
						"streetAddress": "",
						"city": "",
						"zip": "",
						"fullAddress": " "
					  },
					"name": "Accerent S.A.",
					"sanction_bst:description": "",
					"listId": "",
					"@source-id": "",
					"high_risk_jurisdiction": "",
					"sanction_url": "",
					"pep_url": "",
					"$$hashKey": ""
				  }
			]
	}).constant('isicCodeConstants', {
			"A": "AGRICULTURE",
			"B": "MINING",
			"C": "MANUFACTURING",
			"D": "ELECTRICITY",
			"E": "WATER SUPPLY",
			"F": "CONSTRUCTION",
			"G": "WHOLESALE",
			"H": "TRANSPORTATION",
			"I": "ACCOMODATION",
			"J": "INFORMATION",
			"K": "FINANCIAL",
			"L": "REAL ESTATE",
			"M": "PROFESSIONAL",
			"N": "ADMINISTRATIVE",
			"O": "PUBLIC ADMINISTRATION",
			"P": "EDUCATION",
			"Q": "HUMAN HEALTH",
			"R": "ARTS",
			"S": "OTHER SERVICE",
			"T": "ACTIVITIES OF HOUSEHOLDS",
			"U": "ACTIVITIES OF EXTRATERRITORIAL",
			"V": "CHEMICALS",
			"W": "",
			"X": "",
			"Y": "",
			"Z": ""
	}).constant('CountryCodesConst', {
		 'CountryCodesArr': [{
		 		"total": 214
			 },
			 {
				"iso2Code":"WW",
				"name": "WORLD WIDE",	
			 }, {
		 		"id": "ABW",
		 		"iso2Code": "AW",
		 		"name": "Aruba",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Oranjestad",
		 		"longitude": "-70.0167",
		 		"latitude": "12.5167"
		 	},
		 	{
		 		"id": "AFG",
		 		"iso2Code": "AF",
		 		"name": "Afghanistan",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Kabul",
		 		"longitude": "69.1761",
		 		"latitude": "34.5228"
		 	},
		 	{
		 		"id": "AGO",
		 		"iso2Code": "AO",
		 		"name": "Angola",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Luanda",
		 		"longitude": "13.242",
		 		"latitude": "-8.81155"
		 	},
		 	{
		 		"id": "ALB",
		 		"iso2Code": "AL",
		 		"name": "Albania",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Tirane",
		 		"longitude": "19.8172",
		 		"latitude": "41.3317"
		 	},
		 	{
		 		"id": "AND",
		 		"iso2Code": "AD",
		 		"name": "Andorra",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Andorra la Vella",
		 		"longitude": "1.5218",
		 		"latitude": "42.5075"
		 	},
		 	{
		 		"id": "ARE",
		 		"iso2Code": "AE",
		 		"name": "United Arab Emirates",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Abu Dhabi",
		 		"longitude": "54.3705",
		 		"latitude": "24.4764"
		 	},
		 	{
		 		"id": "ARG",
		 		"iso2Code": "AR",
		 		"name": "Argentina",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Buenos Aires",
		 		"longitude": "-58.4173",
		 		"latitude": "-34.6118"
		 	},
		 	{
		 		"id": "ARM",
		 		"iso2Code": "AM",
		 		"name": "Armenia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Yerevan",
		 		"longitude": "44.509",
		 		"latitude": "40.1596"
		 	},
		 	{
		 		"id": "ASM",
		 		"iso2Code": "AS",
		 		"name": "American Samoa",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Pago Pago",
		 		"longitude": "-170.691",
		 		"latitude": "-14.2846"
		 	},
		 	{
		 		"id": "ATG",
		 		"iso2Code": "AG",
		 		"name": "Antigua and Barbuda",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Saint John's",
		 		"longitude": "-61.8456",
		 		"latitude": "17.1175"
		 	},
		 	{
		 		"id": "AUS",
		 		"iso2Code": "AU",
		 		"name": "Australia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Canberra",
		 		"longitude": "149.129",
		 		"latitude": "-35.282"
		 	},
		 	{
		 		"id": "AUT",
		 		"iso2Code": "AT",
		 		"name": "Austria",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Vienna",
		 		"longitude": "16.3798",
		 		"latitude": "48.2201"
		 	},
		 	{
		 		"id": "AZE",
		 		"iso2Code": "AZ",
		 		"name": "Azerbaijan",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Baku",
		 		"longitude": "49.8932",
		 		"latitude": "40.3834"
		 	},
		 	{
		 		"id": "BDI",
		 		"iso2Code": "BI",
		 		"name": "Burundi",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Bujumbura",
		 		"longitude": "29.3639",
		 		"latitude": "-3.3784"
		 	},
		 	{
		 		"id": "BEL",
		 		"iso2Code": "BE",
		 		"name": "Belgium",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Brussels",
		 		"longitude": "4.36761",
		 		"latitude": "50.8371"
		 	},
		 	{
		 		"id": "BEN",
		 		"iso2Code": "BJ",
		 		"name": "Benin",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Porto-Novo",
		 		"longitude": "2.6323",
		 		"latitude": "6.4779"
		 	},
		 	{
		 		"id": "BFA",
		 		"iso2Code": "BF",
		 		"name": "Burkina Faso",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Ouagadougou",
		 		"longitude": "-1.53395",
		 		"latitude": "12.3605"
		 	},
		 	{
		 		"id": "BGD",
		 		"iso2Code": "BD",
		 		"name": "Bangladesh",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Dhaka",
		 		"longitude": "90.4113",
		 		"latitude": "23.7055"
		 	},
		 	{
		 		"id": "BGR",
		 		"iso2Code": "BG",
		 		"name": "Bulgaria",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Sofia",
		 		"longitude": "23.3238",
		 		"latitude": "42.7105"
		 	},
		 	{
		 		"id": "BHR",
		 		"iso2Code": "BH",
		 		"name": "Bahrain",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Manama",
		 		"longitude": "50.5354",
		 		"latitude": "26.1921"
		 	},
		 	{
		 		"id": "BHS",
		 		"iso2Code": "BS",
		 		"name": "Bahamas, The",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Nassau",
		 		"longitude": "-77.339",
		 		"latitude": "25.0661"
		 	},
		 	{
		 		"id": "BIH",
		 		"iso2Code": "BA",
		 		"name": "Bosnia and Herzegovina",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Sarajevo",
		 		"longitude": "18.4214",
		 		"latitude": "43.8607"
		 	},
		 	{
		 		"id": "BLR",
		 		"iso2Code": "BY",
		 		"name": "Belarus",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Minsk",
		 		"longitude": "27.5766",
		 		"latitude": "53.9678"
		 	},
		 	{
		 		"id": "BLZ",
		 		"iso2Code": "BZ",
		 		"name": "Belize",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Belmopan",
		 		"longitude": "-88.7713",
		 		"latitude": "17.2534"
		 	},
		 	{
		 		"id": "BMU",
		 		"iso2Code": "BM",
		 		"name": "Bermuda",
		 		"region": {
		 			"id": "NAC",
		 			"value": "North America"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Hamilton",
		 		"longitude": "-64.706",
		 		"latitude": "32.3293"
		 	},
		 	{
		 		"id": "BOL",
		 		"iso2Code": "BO",
		 		"name": "Bolivia",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "La Paz",
		 		"longitude": "-66.1936",
		 		"latitude": "-13.9908"
		 	},
		 	{
		 		"id": "BRA",
		 		"iso2Code": "BR",
		 		"name": "Brazil",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Brasilia",
		 		"longitude": "-47.9292",
		 		"latitude": "-15.7801"
		 	},
		 	{
		 		"id": "BRB",
		 		"iso2Code": "BB",
		 		"name": "Barbados",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Bridgetown",
		 		"longitude": "-59.6105",
		 		"latitude": "13.0935"
		 	},
		 	{
		 		"id": "BRN",
		 		"iso2Code": "BN",
		 		"name": "Brunei Darussalam",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Bandar Seri Begawan",
		 		"longitude": "114.946",
		 		"latitude": "4.94199"
		 	},
		 	{
		 		"id": "BTN",
		 		"iso2Code": "BT",
		 		"name": "Bhutan",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Thimphu",
		 		"longitude": "89.6177",
		 		"latitude": "27.5768"
		 	},
		 	{
		 		"id": "BWA",
		 		"iso2Code": "BW",
		 		"name": "Botswana",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Gaborone",
		 		"longitude": "25.9201",
		 		"latitude": "-24.6544"
		 	},
		 	{
		 		"id": "CAF",
		 		"iso2Code": "CF",
		 		"name": "Central African Republic",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Bangui",
		 		"longitude": "21.6407",
		 		"latitude": "5.63056"
		 	},
		 	{
		 		"id": "CAN",
		 		"iso2Code": "CA",
		 		"name": "Canada",
		 		"region": {
		 			"id": "NAC",
		 			"value": "North America"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Ottawa",
		 		"longitude": "-75.6919",
		 		"latitude": "45.4215"
		 	},
		 	{
		 		"id": "CHE",
		 		"iso2Code": "CH",
		 		"name": "Switzerland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Bern",
		 		"longitude": "7.44821",
		 		"latitude": "46.948"
		 	},
		 	{
		 		"id": "CHI",
		 		"iso2Code": "JG",
		 		"name": "Channel Islands",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "",
		 		"longitude": "",
		 		"latitude": ""
		 	},
		 	{
		 		"id": "CHL",
		 		"iso2Code": "CL",
		 		"name": "Chile",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Santiago",
		 		"longitude": "-70.6475",
		 		"latitude": "-33.475"
		 	},
		 	{
		 		"id": "CHN",
		 		"iso2Code": "CN",
		 		"name": "China",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Beijing",
		 		"longitude": "116.286",
		 		"latitude": "40.0495"
		 	},
		 	{
		 		"id": "CIV",
		 		"iso2Code": "CI",
		 		"name": "Cote d'Ivoire",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Yamoussoukro",
		 		"longitude": "-4.0305",
		 		"latitude": "5.332"
		 	},
		 	{
		 		"id": "CMR",
		 		"iso2Code": "CM",
		 		"name": "Cameroon",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Yaounde",
		 		"longitude": "11.5174",
		 		"latitude": "3.8721"
		 	},
		 	{
		 		"id": "COD",
		 		"iso2Code": "CD",
		 		"name": "Congo, Dem. Rep.",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Kinshasa",
		 		"longitude": "15.3222",
		 		"latitude": "-4.325"
		 	},
		 	{
		 		"id": "COG",
		 		"iso2Code": "CG",
		 		"name": "Congo, Rep.",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Brazzaville",
		 		"longitude": "15.2662",
		 		"latitude": "-4.2767"
		 	},
		 	{
		 		"id": "COL",
		 		"iso2Code": "CO",
		 		"name": "Colombia",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Bogota",
		 		"longitude": "-74.082",
		 		"latitude": "4.60987"
		 	},
		 	{
		 		"id": "COM",
		 		"iso2Code": "KM",
		 		"name": "Comoros",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Moroni",
		 		"longitude": "43.2418",
		 		"latitude": "-11.6986"
		 	},
		 	{
		 		"id": "CPV",
		 		"iso2Code": "CV",
		 		"name": "Cape Verde",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Praia",
		 		"longitude": "-23.5087",
		 		"latitude": "14.9218"
		 	},
		 	{
		 		"id": "CRI",
		 		"iso2Code": "CR",
		 		"name": "Costa Rica",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "San Jose",
		 		"longitude": "-84.0089",
		 		"latitude": "9.63701"
		 	},
		 	{
		 		"id": "CUB",
		 		"iso2Code": "CU",
		 		"name": "Cuba",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Havana",
		 		"longitude": "-82.3667",
		 		"latitude": "23.1333"
		 	},
		 	{
		 		"id": "CUW",
		 		"iso2Code": "CW",
		 		"name": "Curacao",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Willemstad",
		 		"longitude": "",
		 		"latitude": ""
		 	},
		 	{
		 		"id": "CYM",
		 		"iso2Code": "KY",
		 		"name": "Cayman Islands",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "George Town",
		 		"longitude": "-81.3857",
		 		"latitude": "19.3022"
		 	},
		 	{
		 		"id": "CYP",
		 		"iso2Code": "CY",
		 		"name": "Cyprus",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Nicosia",
		 		"longitude": "33.3736",
		 		"latitude": "35.1676"
		 	},
		 	{
		 		"id": "CZE",
		 		"iso2Code": "CZ",
		 		"name": "Czech Republic",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Prague",
		 		"longitude": "14.4205",
		 		"latitude": "50.0878"
		 	},
		 	{
		 		"id": "DEU",
		 		"iso2Code": "DE",
		 		"name": "Germany",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Berlin",
		 		"longitude": "13.4115",
		 		"latitude": "52.5235"
		 	},
		 	{
		 		"id": "DJI",
		 		"iso2Code": "DJ",
		 		"name": "Djibouti",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Djibouti",
		 		"longitude": "43.1425",
		 		"latitude": "11.5806"
		 	},
		 	{
		 		"id": "DMA",
		 		"iso2Code": "DM",
		 		"name": "Dominica",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Roseau",
		 		"longitude": "-61.39",
		 		"latitude": "15.2976"
		 	},
		 	{
		 		"id": "DNK",
		 		"iso2Code": "DK",
		 		"name": "Denmark",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Copenhagen",
		 		"longitude": "12.5681",
		 		"latitude": "55.6763"
		 	},
		 	{
		 		"id": "DOM",
		 		"iso2Code": "DO",
		 		"name": "Dominican Republic",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Santo Domingo",
		 		"longitude": "-69.8908",
		 		"latitude": "18.479"
		 	},
		 	{
		 		"id": "DZA",
		 		"iso2Code": "DZ",
		 		"name": "Algeria",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Algiers",
		 		"longitude": "3.05097",
		 		"latitude": "36.7397"
		 	},
		 	{
		 		"id": "ECU",
		 		"iso2Code": "EC",
		 		"name": "Ecuador",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Quito",
		 		"longitude": "-78.5243",
		 		"latitude": "-0.229498"
		 	},
		 	{
		 		"id": "EGY",
		 		"iso2Code": "EG",
		 		"name": "Egypt, Arab Rep.",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Cairo",
		 		"longitude": "31.2461",
		 		"latitude": "30.0982"
		 	},
		 	{
		 		"id": "ERI",
		 		"iso2Code": "ER",
		 		"name": "Eritrea",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Asmara",
		 		"longitude": "38.9183",
		 		"latitude": "15.3315"
		 	},
		 	{
		 		"id": "ESP",
		 		"iso2Code": "ES",
		 		"name": "Spain",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Madrid",
		 		"longitude": "-3.70327",
		 		"latitude": "40.4167"
		 	},
		 	{
		 		"id": "EST",
		 		"iso2Code": "EE",
		 		"name": "Estonia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Tallinn",
		 		"longitude": "24.7586",
		 		"latitude": "59.4392"
		 	},
		 	{
		 		"id": "ETH",
		 		"iso2Code": "ET",
		 		"name": "Ethiopia",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Addis ababa",
		 		"longitude": "38.7468",
		 		"latitude": "9.02274"
		 	},
		 	{
		 		"id": "FIN",
		 		"iso2Code": "FI",
		 		"name": "Finland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Helsinki",
		 		"longitude": "24.9525",
		 		"latitude": "60.1608"
		 	},
		 	{
		 		"id": "FJI",
		 		"iso2Code": "FJ",
		 		"name": "Fiji",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Suva",
		 		"longitude": "178.399",
		 		"latitude": "-18.1149"
		 	},
		 	{
		 		"id": "FRA",
		 		"iso2Code": "FR",
		 		"name": "France",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Paris",
		 		"longitude": "2.35097",
		 		"latitude": "48.8566"
		 	},
		 	{
		 		"id": "FRO",
		 		"iso2Code": "FO",
		 		"name": "Faeroe Islands",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Torshavn",
		 		"longitude": "-6.91181",
		 		"latitude": "61.8926"
		 	},
		 	{
		 		"id": "FSM",
		 		"iso2Code": "FM",
		 		"name": "Micronesia, Fed. Sts.",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Palikir",
		 		"longitude": "158.185",
		 		"latitude": "6.91771"
		 	},
		 	{
		 		"id": "GAB",
		 		"iso2Code": "GA",
		 		"name": "Gabon",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Libreville",
		 		"longitude": "9.45162",
		 		"latitude": "0.38832"
		 	},
		 	{
		 		"id": "GBR",
		 		"iso2Code": "GB",
		 		"name": "United Kingdom",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "London",
		 		"longitude": "-0.126236",
		 		"latitude": "51.5002"
		 	},
		 	{
		 		"id": "GEO",
		 		"iso2Code": "GE",
		 		"name": "Georgia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Tbilisi",
		 		"longitude": "44.793",
		 		"latitude": "41.71"
		 	},
		 	{
		 		"id": "GHA",
		 		"iso2Code": "GH",
		 		"name": "Ghana",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Accra",
		 		"longitude": "-0.20795",
		 		"latitude": "5.57045"
		 	},
		 	{
		 		"id": "GIN",
		 		"iso2Code": "GN",
		 		"name": "Guinea",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Conakry",
		 		"longitude": "-13.7",
		 		"latitude": "9.51667"
		 	},
		 	{
		 		"id": "GMB",
		 		"iso2Code": "GM",
		 		"name": "Gambia, The",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Banjul",
		 		"longitude": "-16.5885",
		 		"latitude": "13.4495"
		 	},
		 	{
		 		"id": "GNB",
		 		"iso2Code": "GW",
		 		"name": "Guinea-Bissau",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Bissau",
		 		"longitude": "-15.1804",
		 		"latitude": "11.8037"
		 	},
		 	{
		 		"id": "GNQ",
		 		"iso2Code": "GQ",
		 		"name": "Equatorial Guinea",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Malabo",
		 		"longitude": "8.7741",
		 		"latitude": "3.7523"
		 	},
		 	{
		 		"id": "GRC",
		 		"iso2Code": "GR",
		 		"name": "Greece",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Athens",
		 		"longitude": "23.7166",
		 		"latitude": "37.9792"
		 	},
		 	{
		 		"id": "GRD",
		 		"iso2Code": "GD",
		 		"name": "Grenada",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Saint George's",
		 		"longitude": "-61.7449",
		 		"latitude": "12.0653"
		 	},
		 	{
		 		"id": "GRL",
		 		"iso2Code": "GL",
		 		"name": "Greenland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Nuuk",
		 		"longitude": "-51.7214",
		 		"latitude": "64.1836"
		 	},
		 	{
		 		"id": "GTM",
		 		"iso2Code": "GT",
		 		"name": "Guatemala",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Guatemala City",
		 		"longitude": "-90.5328",
		 		"latitude": "14.6248"
		 	},
		 	{
		 		"id": "GUM",
		 		"iso2Code": "GU",
		 		"name": "Guam",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Agana",
		 		"longitude": "144.794",
		 		"latitude": "13.4443"
		 	},
		 	{
		 		"id": "GUY",
		 		"iso2Code": "GY",
		 		"name": "Guyana",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Georgetown",
		 		"longitude": "-58.1548",
		 		"latitude": "6.80461"
		 	},
		 	{
		 		"id": "HKG",
		 		"iso2Code": "HK",
		 		"name": "Hong Kong SAR, China",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "",
		 		"longitude": "114.109",
		 		"latitude": "22.3964"
		 	},
		 	{
		 		"id": "HND",
		 		"iso2Code": "HN",
		 		"name": "Honduras",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Tegucigalpa",
		 		"longitude": "-87.4667",
		 		"latitude": "15.1333"
		 	},
		 	{
		 		"id": "HRV",
		 		"iso2Code": "HR",
		 		"name": "Croatia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Zagreb",
		 		"longitude": "15.9614",
		 		"latitude": "45.8069"
		 	},
		 	{
		 		"id": "HTI",
		 		"iso2Code": "HT",
		 		"name": "Haiti",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Port-au-Prince",
		 		"longitude": "-72.3288",
		 		"latitude": "18.5392"
		 	},
		 	{
		 		"id": "HUN",
		 		"iso2Code": "HU",
		 		"name": "Hungary",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Budapest",
		 		"longitude": "19.0408",
		 		"latitude": "47.4984"
		 	},
		 	{
		 		"id": "IDN",
		 		"iso2Code": "ID",
		 		"name": "Indonesia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Jakarta",
		 		"longitude": "106.83",
		 		"latitude": "-6.19752"
		 	},
		 	{
		 		"id": "IMN",
		 		"iso2Code": "IM",
		 		"name": "Isle of Man",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Douglas",
		 		"longitude": "-4.47928",
		 		"latitude": "54.1509"
		 	},
		 	{
		 		"id": "IND",
		 		"iso2Code": "IN",
		 		"name": "India",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "New Delhi",
		 		"longitude": "77.225",
		 		"latitude": "28.6353"
		 	},
		 	{
		 		"id": "IRL",
		 		"iso2Code": "IE",
		 		"name": "Ireland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Dublin",
		 		"longitude": "-6.26749",
		 		"latitude": "53.3441"
		 	},
		 	{
		 		"id": "IRN",
		 		"iso2Code": "IR",
		 		"name": "Iran, Islamic Rep.",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Tehran",
		 		"longitude": "51.4447",
		 		"latitude": "35.6878"
		 	},
		 	{
		 		"id": "IRQ",
		 		"iso2Code": "IQ",
		 		"name": "Iraq",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Baghdad",
		 		"longitude": "44.394",
		 		"latitude": "33.3302"
		 	},
		 	{
		 		"id": "ISL",
		 		"iso2Code": "IS",
		 		"name": "Iceland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Reykjavik",
		 		"longitude": "-21.8952",
		 		"latitude": "64.1353"
		 	},
		 	{
		 		"id": "ISR",
		 		"iso2Code": "IL",
		 		"name": "Israel",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "",
		 		"longitude": "35.2035",
		 		"latitude": "31.7717"
		 	},
		 	{
		 		"id": "ITA",
		 		"iso2Code": "IT",
		 		"name": "Italy",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Rome",
		 		"longitude": "12.4823",
		 		"latitude": "41.8955"
		 	},
		 	{
		 		"id": "JAM",
		 		"iso2Code": "JM",
		 		"name": "Jamaica",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Kingston",
		 		"longitude": "-76.792",
		 		"latitude": "17.9927"
		 	},
		 	{
		 		"id": "JOR",
		 		"iso2Code": "JO",
		 		"name": "Jordan",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Amman",
		 		"longitude": "35.9263",
		 		"latitude": "31.9497"
		 	},
		 	{
		 		"id": "JPN",
		 		"iso2Code": "JP",
		 		"name": "Japan",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Tokyo",
		 		"longitude": "139.77",
		 		"latitude": "35.67"
		 	},
		 	{
		 		"id": "KAZ",
		 		"iso2Code": "KZ",
		 		"name": "Kazakhstan",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Astana",
		 		"longitude": "71.4382",
		 		"latitude": "51.1879"
		 	},
		 	{
		 		"id": "KEN",
		 		"iso2Code": "KE",
		 		"name": "Kenya",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Nairobi",
		 		"longitude": "36.8126",
		 		"latitude": "-1.27975"
		 	},
		 	{
		 		"id": "KGZ",
		 		"iso2Code": "KG",
		 		"name": "Kyrgyz Republic",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Bishkek",
		 		"longitude": "74.6057",
		 		"latitude": "42.8851"
		 	},
		 	{
		 		"id": "KHM",
		 		"iso2Code": "KH",
		 		"name": "Cambodia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Phnom Penh",
		 		"longitude": "104.874",
		 		"latitude": "11.5556"
		 	},
		 	{
		 		"id": "KIR",
		 		"iso2Code": "KI",
		 		"name": "Kiribati",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Tarawa",
		 		"longitude": "172.979",
		 		"latitude": "1.32905"
		 	},
		 	{
		 		"id": "KNA",
		 		"iso2Code": "KN",
		 		"name": "St. Kitts and Nevis",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Basseterre",
		 		"longitude": "-62.7309",
		 		"latitude": "17.3"
		 	},
		 	{
		 		"id": "KOR",
		 		"iso2Code": "KR",
		 		"name": "Korea, Rep.",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Seoul",
		 		"longitude": "126.957",
		 		"latitude": "37.5323"
		 	},
		 	{
		 		"id": "KSV",
		 		"iso2Code": "KV",
		 		"name": "Kosovo",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Pristina",
		 		"longitude": "20.926",
		 		"latitude": "42.565"
		 	},
		 	{
		 		"id": "KWT",
		 		"iso2Code": "KW",
		 		"name": "Kuwait",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Kuwait City",
		 		"longitude": "47.9824",
		 		"latitude": "29.3721"
		 	},
		 	{
		 		"id": "LAO",
		 		"iso2Code": "LA",
		 		"name": "Lao PDR",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Vientiane",
		 		"longitude": "102.177",
		 		"latitude": "18.5826"
		 	},
		 	{
		 		"id": "LBN",
		 		"iso2Code": "LB",
		 		"name": "Lebanon",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Beirut",
		 		"longitude": "35.5134",
		 		"latitude": "33.8872"
		 	},
		 	{
		 		"id": "LBR",
		 		"iso2Code": "LR",
		 		"name": "Liberia",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Monrovia",
		 		"longitude": "-10.7957",
		 		"latitude": "6.30039"
		 	},
		 	{
		 		"id": "LBY",
		 		"iso2Code": "LY",
		 		"name": "Libya",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Tripoli",
		 		"longitude": "13.1072",
		 		"latitude": "32.8578"
		 	},
		 	{
		 		"id": "LCA",
		 		"iso2Code": "LC",
		 		"name": "St. Lucia",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Castries",
		 		"longitude": "-60.9832",
		 		"latitude": "14"
		 	},
		 	{
		 		"id": "LIE",
		 		"iso2Code": "LI",
		 		"name": "Liechtenstein",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Vaduz",
		 		"longitude": "9.52148",
		 		"latitude": "47.1411"
		 	},
		 	{
		 		"id": "LKA",
		 		"iso2Code": "LK",
		 		"name": "Sri Lanka",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Colombo",
		 		"longitude": "79.8528",
		 		"latitude": "6.92148"
		 	},
		 	{
		 		"id": "LSO",
		 		"iso2Code": "LS",
		 		"name": "Lesotho",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Maseru",
		 		"longitude": "27.7167",
		 		"latitude": "-29.5208"
		 	},
		 	{
		 		"id": "LTU",
		 		"iso2Code": "LT",
		 		"name": "Lithuania",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Vilnius",
		 		"longitude": "25.2799",
		 		"latitude": "54.6896"
		 	},
		 	{
		 		"id": "LUX",
		 		"iso2Code": "LU",
		 		"name": "Luxembourg",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Luxembourg",
		 		"longitude": "6.1296",
		 		"latitude": "49.61"
		 	},
		 	{
		 		"id": "LVA",
		 		"iso2Code": "LV",
		 		"name": "Latvia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Riga",
		 		"longitude": "24.1048",
		 		"latitude": "56.9465"
		 	},
		 	{
		 		"id": "MAC",
		 		"iso2Code": "MO",
		 		"name": "Macao SAR, China",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "",
		 		"longitude": "113.55",
		 		"latitude": "22.1667"
		 	},
		 	{
		 		"id": "MAF",
		 		"iso2Code": "MF",
		 		"name": "St. Martin (French part)",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Marigot",
		 		"longitude": "",
		 		"latitude": ""
		 	},
		 	{
		 		"id": "MAR",
		 		"iso2Code": "MA",
		 		"name": "Morocco",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Rabat",
		 		"longitude": "-6.8704",
		 		"latitude": "33.9905"
		 	},
		 	{
		 		"id": "MCO",
		 		"iso2Code": "MC",
		 		"name": "Monaco",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Monaco",
		 		"longitude": "7.41891",
		 		"latitude": "43.7325"
		 	},
		 	{
		 		"id": "MDA",
		 		"iso2Code": "MD",
		 		"name": "Moldova",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Chisinau",
		 		"longitude": "28.8497",
		 		"latitude": "47.0167"
		 	},
		 	{
		 		"id": "MDG",
		 		"iso2Code": "MG",
		 		"name": "Madagascar",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Antananarivo",
		 		"longitude": "45.7167",
		 		"latitude": "-20.4667"
		 	},
		 	{
		 		"id": "MDV",
		 		"iso2Code": "MV",
		 		"name": "Maldives",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Male",
		 		"longitude": "73.5109",
		 		"latitude": "4.1742"
		 	},
		 	{
		 		"id": "MEX",
		 		"iso2Code": "MX",
		 		"name": "Mexico",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Mexico City",
		 		"longitude": "-99.1276",
		 		"latitude": "19.427"
		 	},
		 	{
		 		"id": "MHL",
		 		"iso2Code": "MH",
		 		"name": "Marshall Islands",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Majuro",
		 		"longitude": "171.135",
		 		"latitude": "7.11046"
		 	},
		 	{
		 		"id": "MKD",
		 		"iso2Code": "MK",
		 		"name": "Macedonia, FYR",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Skopje",
		 		"longitude": "21.4361",
		 		"latitude": "42.0024"
		 	},
		 	{
		 		"id": "MLI",
		 		"iso2Code": "ML",
		 		"name": "Mali",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Bamako",
		 		"longitude": "-7.50034",
		 		"latitude": "13.5667"
		 	},
		 	{
		 		"id": "MLT",
		 		"iso2Code": "MT",
		 		"name": "Malta",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Valletta",
		 		"longitude": "14.5189",
		 		"latitude": "35.9042"
		 	},
		 	{
		 		"id": "MMR",
		 		"iso2Code": "MM",
		 		"name": "Myanmar",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Naypyidaw",
		 		"longitude": "95.9562",
		 		"latitude": "21.914"
		 	},
		 	{
		 		"id": "MNE",
		 		"iso2Code": "ME",
		 		"name": "Montenegro",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Podgorica",
		 		"longitude": "19.2595",
		 		"latitude": "42.4602"
		 	},
		 	{
		 		"id": "MNG",
		 		"iso2Code": "MN",
		 		"name": "Mongolia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Ulaanbaatar",
		 		"longitude": "106.937",
		 		"latitude": "47.9129"
		 	},
		 	{
		 		"id": "MNP",
		 		"iso2Code": "MP",
		 		"name": "Northern Mariana Islands",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Saipan",
		 		"longitude": "145.765",
		 		"latitude": "15.1935"
		 	},
		 	{
		 		"id": "MOZ",
		 		"iso2Code": "MZ",
		 		"name": "Mozambique",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Maputo",
		 		"longitude": "32.5713",
		 		"latitude": "-25.9664"
		 	},
		 	{
		 		"id": "MRT",
		 		"iso2Code": "MR",
		 		"name": "Mauritania",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Nouakchott",
		 		"longitude": "-15.9824",
		 		"latitude": "18.2367"
		 	},
		 	{
		 		"id": "MUS",
		 		"iso2Code": "MU",
		 		"name": "Mauritius",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Port Louis",
		 		"longitude": "57.4977",
		 		"latitude": "-20.1605"
		 	},
		 	{
		 		"id": "MWI",
		 		"iso2Code": "MW",
		 		"name": "Malawi",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Lilongwe",
		 		"longitude": "33.7703",
		 		"latitude": "-13.9899"
		 	},
		 	{
		 		"id": "MYS",
		 		"iso2Code": "MY",
		 		"name": "Malaysia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Kuala Lumpur",
		 		"longitude": "101.684",
		 		"latitude": "3.12433"
		 	},
		 	{
		 		"id": "NAM",
		 		"iso2Code": "NA",
		 		"name": "Namibia",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Windhoek",
		 		"longitude": "17.0931",
		 		"latitude": "-22.5648"
		 	},
		 	{
		 		"id": "NCL",
		 		"iso2Code": "NC",
		 		"name": "New Caledonia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Noum'ea",
		 		"longitude": "166.464",
		 		"latitude": "-22.2677"
		 	},
		 	{
		 		"id": "NER",
		 		"iso2Code": "NE",
		 		"name": "Niger",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Niamey",
		 		"longitude": "2.1073",
		 		"latitude": "13.514"
		 	},
		 	{
		 		"id": "NGA",
		 		"iso2Code": "NG",
		 		"name": "Nigeria",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Abuja",
		 		"longitude": "7.48906",
		 		"latitude": "9.05804"
		 	},
		 	{
		 		"id": "NIC",
		 		"iso2Code": "NI",
		 		"name": "Nicaragua",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Managua",
		 		"longitude": "-86.2734",
		 		"latitude": "12.1475"
		 	},
		 	{
		 		"id": "NLD",
		 		"iso2Code": "NL",
		 		"name": "Netherlands",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Amsterdam",
		 		"longitude": "4.89095",
		 		"latitude": "52.3738"
		 	},
		 	{
		 		"id": "NOR",
		 		"iso2Code": "NO",
		 		"name": "Norway",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Oslo",
		 		"longitude": "10.7387",
		 		"latitude": "59.9138"
		 	},
		 	{
		 		"id": "NPL",
		 		"iso2Code": "NP",
		 		"name": "Nepal",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Kathmandu",
		 		"longitude": "85.3157",
		 		"latitude": "27.6939"
		 	},
		 	{
		 		"id": "NZL",
		 		"iso2Code": "NZ",
		 		"name": "New Zealand",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Wellington",
		 		"longitude": "174.776",
		 		"latitude": "-41.2865"
		 	},
		 	{
		 		"id": "OMN",
		 		"iso2Code": "OM",
		 		"name": "Oman",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Muscat",
		 		"longitude": "58.5874",
		 		"latitude": "23.6105"
		 	},
		 	{
		 		"id": "PAK",
		 		"iso2Code": "PK",
		 		"name": "Pakistan",
		 		"region": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"adminregion": {
		 			"id": "SAS",
		 			"value": "South Asia"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Islamabad",
		 		"longitude": "72.8",
		 		"latitude": "30.5167"
		 	},
		 	{
		 		"id": "PAN",
		 		"iso2Code": "PA",
		 		"name": "Panama",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Panama City",
		 		"longitude": "-79.5188",
		 		"latitude": "8.99427"
		 	},
		 	{
		 		"id": "PER",
		 		"iso2Code": "PE",
		 		"name": "Peru",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Lima",
		 		"longitude": "-77.0465",
		 		"latitude": "-12.0931"
		 	},
		 	{
		 		"id": "PHL",
		 		"iso2Code": "PH",
		 		"name": "Philippines",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Manila",
		 		"longitude": "121.035",
		 		"latitude": "14.5515"
		 	},
		 	{
		 		"id": "PLW",
		 		"iso2Code": "PW",
		 		"name": "Palau",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Koror",
		 		"longitude": "134.479",
		 		"latitude": "7.34194"
		 	},
		 	{
		 		"id": "PNG",
		 		"iso2Code": "PG",
		 		"name": "Papua New Guinea",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Port Moresby",
		 		"longitude": "147.194",
		 		"latitude": "-9.47357"
		 	},
		 	{
		 		"id": "POL",
		 		"iso2Code": "PL",
		 		"name": "Poland",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Warsaw",
		 		"longitude": "21.02",
		 		"latitude": "52.26"
		 	},
		 	{
		 		"id": "PRI",
		 		"iso2Code": "PR",
		 		"name": "Puerto Rico",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "San Juan",
		 		"longitude": "-66",
		 		"latitude": "18.23"
		 	},
		 	{
		 		"id": "PRK",
		 		"iso2Code": "KP",
		 		"name": "Korea, Dem. Rep.",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Pyongyang",
		 		"longitude": "125.754",
		 		"latitude": "39.0319"
		 	},
		 	{
		 		"id": "PRT",
		 		"iso2Code": "PT",
		 		"name": "Portugal",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Lisbon",
		 		"longitude": "-9.13552",
		 		"latitude": "38.7072"
		 	},
		 	{
		 		"id": "PRY",
		 		"iso2Code": "PY",
		 		"name": "Paraguay",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Asuncion",
		 		"longitude": "-57.6362",
		 		"latitude": "-25.3005"
		 	},
		 	{
		 		"id": "PSE",
		 		"iso2Code": "PS",
		 		"name": "West Bank and Gaza",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "",
		 		"longitude": "",
		 		"latitude": ""
		 	},
		 	{
		 		"id": "PYF",
		 		"iso2Code": "PF",
		 		"name": "French Polynesia",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Papeete",
		 		"longitude": "-149.57",
		 		"latitude": "-17.535"
		 	},
		 	{
		 		"id": "QAT",
		 		"iso2Code": "QA",
		 		"name": "Qatar",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Doha",
		 		"longitude": "51.5082",
		 		"latitude": "25.2948"
		 	},
		 	{
		 		"id": "ROU",
		 		"iso2Code": "RO",
		 		"name": "Romania",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Bucharest",
		 		"longitude": "26.0979",
		 		"latitude": "44.4479"
		 	},
		 	{
		 		"id": "RUS",
		 		"iso2Code": "RU",
		 		"name": "Russian Federation",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Moscow",
		 		"longitude": "37.6176",
		 		"latitude": "55.7558"
		 	},
		 	{
		 		"id": "RWA",
		 		"iso2Code": "RW",
		 		"name": "Rwanda",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Kigali",
		 		"longitude": "30.0587",
		 		"latitude": "-1.95325"
		 	},
		 	{
		 		"id": "SAU",
		 		"iso2Code": "SA",
		 		"name": "Saudi Arabia",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Riyadh",
		 		"longitude": "46.6977",
		 		"latitude": "24.6748"
		 	},
		 	{
		 		"id": "SDN",
		 		"iso2Code": "SD",
		 		"name": "Sudan",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Khartoum",
		 		"longitude": "32.5363",
		 		"latitude": "15.5932"
		 	},
		 	{
		 		"id": "SEN",
		 		"iso2Code": "SN",
		 		"name": "Senegal",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Dakar",
		 		"longitude": "-17.4734",
		 		"latitude": "14.7247"
		 	},
		 	{
		 		"id": "SGP",
		 		"iso2Code": "SG",
		 		"name": "Singapore",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Singapore",
		 		"longitude": "103.85",
		 		"latitude": "1.28941"
		 	},
		 	{
		 		"id": "SLB",
		 		"iso2Code": "SB",
		 		"name": "Solomon Islands",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Honiara",
		 		"longitude": "159.949",
		 		"latitude": "-9.42676"
		 	},
		 	{
		 		"id": "SLE",
		 		"iso2Code": "SL",
		 		"name": "Sierra Leone",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Freetown",
		 		"longitude": "-13.2134",
		 		"latitude": "8.4821"
		 	},
		 	{
		 		"id": "SLV",
		 		"iso2Code": "SV",
		 		"name": "El Salvador",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "San Salvador",
		 		"longitude": "-89.2073",
		 		"latitude": "13.7034"
		 	},
		 	{
		 		"id": "SMR",
		 		"iso2Code": "SM",
		 		"name": "San Marino",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "San Marino",
		 		"longitude": "12.4486",
		 		"latitude": "43.9322"
		 	},
		 	{
		 		"id": "SOM",
		 		"iso2Code": "SO",
		 		"name": "Somalia",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Mogadishu",
		 		"longitude": "45.3254",
		 		"latitude": "2.07515"
		 	},
		 	{
		 		"id": "SRB",
		 		"iso2Code": "RS",
		 		"name": "Serbia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Belgrade",
		 		"longitude": "20.4656",
		 		"latitude": "44.8024"
		 	},
		 	{
		 		"id": "SSD",
		 		"iso2Code": "SS",
		 		"name": "South Sudan",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Juba",
		 		"longitude": "31.6",
		 		"latitude": "4.85"
		 	},
		 	{
		 		"id": "STP",
		 		"iso2Code": "ST",
		 		"name": "Sao Tome and Principe",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Sao Tome",
		 		"longitude": "6.6071",
		 		"latitude": "0.20618"
		 	},
		 	{
		 		"id": "SUR",
		 		"iso2Code": "SR",
		 		"name": "Suriname",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Paramaribo",
		 		"longitude": "-55.1679",
		 		"latitude": "5.8232"
		 	},
		 	{
		 		"id": "SVK",
		 		"iso2Code": "SK",
		 		"name": "Slovak Republic",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Bratislava",
		 		"longitude": "17.1073",
		 		"latitude": "48.1484"
		 	},
		 	{
		 		"id": "SVN",
		 		"iso2Code": "SI",
		 		"name": "Slovenia",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Ljubljana",
		 		"longitude": "14.5044",
		 		"latitude": "46.0546"
		 	},
		 	{
		 		"id": "SWE",
		 		"iso2Code": "SE",
		 		"name": "Sweden",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Stockholm",
		 		"longitude": "18.0645",
		 		"latitude": "59.3327"
		 	},
		 	{
		 		"id": "SWZ",
		 		"iso2Code": "SZ",
		 		"name": "Swaziland",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Mbabane",
		 		"longitude": "31.4659",
		 		"latitude": "-26.5225"
		 	},
		 	{
		 		"id": "SXM",
		 		"iso2Code": "SX",
		 		"name": "Sint Maarten (Dutch part)",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Philipsburg",
		 		"longitude": "",
		 		"latitude": ""
		 	},
		 	{
		 		"id": "SYC",
		 		"iso2Code": "SC",
		 		"name": "Seychelles",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Victoria",
		 		"longitude": "55.4466",
		 		"latitude": "-4.6309"
		 	},
		 	{
		 		"id": "SYR",
		 		"iso2Code": "SY",
		 		"name": "Syrian Arab Republic",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Damascus",
		 		"longitude": "36.3119",
		 		"latitude": "33.5146"
		 	},
		 	{
		 		"id": "TCA",
		 		"iso2Code": "TC",
		 		"name": "Turks and Caicos Islands",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Grand Turk",
		 		"longitude": "-71.141389",
		 		"latitude": "21.4602778"
		 	},
		 	{
		 		"id": "TCD",
		 		"iso2Code": "TD",
		 		"name": "Chad",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "N'Djamena",
		 		"longitude": "15.0445",
		 		"latitude": "12.1048"
		 	},
		 	{
		 		"id": "TGO",
		 		"iso2Code": "TG",
		 		"name": "Togo",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Lome",
		 		"longitude": "1.2255",
		 		"latitude": "6.1228"
		 	},
		 	{
		 		"id": "THA",
		 		"iso2Code": "TH",
		 		"name": "Thailand",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Bangkok",
		 		"longitude": "100.521",
		 		"latitude": "13.7308"
		 	},
		 	{
		 		"id": "TJK",
		 		"iso2Code": "TJ",
		 		"name": "Tajikistan",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Dushanbe",
		 		"longitude": "68.7864",
		 		"latitude": "38.5878"
		 	},
		 	{
		 		"id": "TKM",
		 		"iso2Code": "TM",
		 		"name": "Turkmenistan",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Ashgabat",
		 		"longitude": "58.3794",
		 		"latitude": "37.9509"
		 	},
		 	{
		 		"id": "TLS",
		 		"iso2Code": "TL",
		 		"name": "Timor-Leste",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Dili",
		 		"longitude": "125.567",
		 		"latitude": "-8.56667"
		 	},
		 	{
		 		"id": "TON",
		 		"iso2Code": "TO",
		 		"name": "Tonga",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Nuku'alofa",
		 		"longitude": "-175.216",
		 		"latitude": "-21.136"
		 	},
		 	{
		 		"id": "TTO",
		 		"iso2Code": "TT",
		 		"name": "Trinidad and Tobago",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Port-of-Spain",
		 		"longitude": "-61.4789",
		 		"latitude": "10.6596"
		 	},
		 	{
		 		"id": "TUN",
		 		"iso2Code": "TN",
		 		"name": "Tunisia",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Tunis",
		 		"longitude": "10.21",
		 		"latitude": "36.7899"
		 	},
		 	{
		 		"id": "TUR",
		 		"iso2Code": "TR",
		 		"name": "Turkey",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Ankara",
		 		"longitude": "32.3606",
		 		"latitude": "39.7153"
		 	},
		 	{
		 		"id": "TUV",
		 		"iso2Code": "TV",
		 		"name": "Tuvalu",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Funafuti",
		 		"longitude": "179.089567",
		 		"latitude": "-8.6314877"
		 	},
		 	{
		 		"id": "TZA",
		 		"iso2Code": "TZ",
		 		"name": "Tanzania",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Dodoma",
		 		"longitude": "35.7382",
		 		"latitude": "-6.17486"
		 	},
		 	{
		 		"id": "UGA",
		 		"iso2Code": "UG",
		 		"name": "Uganda",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Kampala",
		 		"longitude": "32.5729",
		 		"latitude": "0.314269"
		 	},
		 	{
		 		"id": "UKR",
		 		"iso2Code": "UA",
		 		"name": "Ukraine",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Kiev",
		 		"longitude": "30.5038",
		 		"latitude": "50.4536"
		 	},
		 	{
		 		"id": "URY",
		 		"iso2Code": "UY",
		 		"name": "Uruguay",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Montevideo",
		 		"longitude": "-56.0675",
		 		"latitude": "-34.8941"
		 	},
		 	{
		 		"id": "USA",
		 		"iso2Code": "US",
		 		"name": "United States",
		 		"region": {
		 			"id": "NAC",
		 			"value": "North America"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "OEC",
		 			"value": "High income: OECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Washington D.C.",
		 		"longitude": "-77.032",
		 		"latitude": "38.8895"
		 	},
		 	{
		 		"id": "UZB",
		 		"iso2Code": "UZ",
		 		"name": "Uzbekistan",
		 		"region": {
		 			"id": "ECS",
		 			"value": "Europe & Central Asia (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "ECA",
		 			"value": "Europe & Central Asia (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Tashkent",
		 		"longitude": "69.269",
		 		"latitude": "41.3052"
		 	},
		 	{
		 		"id": "VCT",
		 		"iso2Code": "VC",
		 		"name": "St. Vincent and the Grenadines",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Kingstown",
		 		"longitude": "-61.2653",
		 		"latitude": "13.2035"
		 	},
		 	{
		 		"id": "VEN",
		 		"iso2Code": "VE",
		 		"name": "Venezuela, RB",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "LAC",
		 			"value": "Latin America & Caribbean (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Caracas",
		 		"longitude": "-69.8371",
		 		"latitude": "9.08165"
		 	},
		 	{
		 		"id": "VIR",
		 		"iso2Code": "VI",
		 		"name": "Virgin Islands (U.S.)",
		 		"region": {
		 			"id": "LCN",
		 			"value": "Latin America & Caribbean (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "",
		 			"value": ""
		 		},
		 		"incomeLevel": {
		 			"id": "NOC",
		 			"value": "High income: nonOECD"
		 		},
		 		"lendingType": {
		 			"id": "LNX",
		 			"value": "Not classified"
		 		},
		 		"capitalCity": "Charlotte Amalie",
		 		"longitude": "-64.8963",
		 		"latitude": "18.3358"
		 	},
		 	{
		 		"id": "VNM",
		 		"iso2Code": "VN",
		 		"name": "Vietnam",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Hanoi",
		 		"longitude": "105.825",
		 		"latitude": "21.0069"
		 	},
		 	{
		 		"id": "VUT",
		 		"iso2Code": "VU",
		 		"name": "Vanuatu",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Port-Vila",
		 		"longitude": "168.321",
		 		"latitude": "-17.7404"
		 	},
		 	{
		 		"id": "WSM",
		 		"iso2Code": "WS",
		 		"name": "Samoa",
		 		"region": {
		 			"id": "EAS",
		 			"value": "East Asia & Pacific (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "EAP",
		 			"value": "East Asia & Pacific (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Apia",
		 		"longitude": "-171.752",
		 		"latitude": "-13.8314"
		 	},
		 	{
		 		"id": "YEM",
		 		"iso2Code": "YE",
		 		"name": "Yemen, Rep.",
		 		"region": {
		 			"id": "MEA",
		 			"value": "Middle East & North Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "MNA",
		 			"value": "Middle East & North Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Sana'a",
		 		"longitude": "44.2075",
		 		"latitude": "15.352"
		 	},
		 	{
		 		"id": "ZAF",
		 		"iso2Code": "ZA",
		 		"name": "South Africa",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "UMC",
		 			"value": "Upper middle income"
		 		},
		 		"lendingType": {
		 			"id": "IBD",
		 			"value": "IBRD"
		 		},
		 		"capitalCity": "Pretoria",
		 		"longitude": "28.1871",
		 		"latitude": "-25.746"
		 	},
		 	{
		 		"id": "ZMB",
		 		"iso2Code": "ZM",
		 		"name": "Zambia",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LMC",
		 			"value": "Lower middle income"
		 		},
		 		"lendingType": {
		 			"id": "IDX",
		 			"value": "IDA"
		 		},
		 		"capitalCity": "Lusaka",
		 		"longitude": "28.2937",
		 		"latitude": "-15.3982"
		 	},
		 	{
		 		"id": "ZWE",
		 		"iso2Code": "ZW",
		 		"name": "Zimbabwe",
		 		"region": {
		 			"id": "SSF",
		 			"value": "Sub-Saharan Africa (all income levels)"
		 		},
		 		"adminregion": {
		 			"id": "SSA",
		 			"value": "Sub-Saharan Africa (developing only)"
		 		},
		 		"incomeLevel": {
		 			"id": "LIC",
		 			"value": "Low income"
		 		},
		 		"lendingType": {
		 			"id": "IDB",
		 			"value": "Blend"
		 		},
		 		"capitalCity": "Harare",
		 		"longitude": "31.0672",
		 		"latitude": "-17.8312"
		 	}
		 ]
	}).constant("technologyConstant", {
		"email": "electronic communications",
		"website": "digital assets",
		"messaging": "communication platforms",
		"analytics": "data analysis",
		"database": "data storage"
	}).constant("handlePepofficers", [{
		"name": "KELLY A. AYOTTE",
		"peps": {
			"name":"KELLY A. AYOTTE",
			"type":"individual",
			"source":"Caterpillarâs Board of Directors",
			"nationalities":[
			{
			"country":"United States",
			"country_code":"US"
			}
			],
			"function":" Board of Director",
			"updated_at":"2018-12-13",
			"id":"Caterpillarâs Board of Directors.9810c6e7519afd253d77c27438b536f34484e00f",
			"url":"https://www.caterpillar.com/en/company/governance/board-of-directors.html",
			"timestamp":"2018-12-13T00:00:00.000000",
			"program":"United States"
			}
	}, {
		"name": "DENNIS A. MUILENBURG",
		"peps": {
			"name": "DENNIS A. MUILENBURG",
			"type": "individual",
			"source": "Caterpillarâs Board of Directors",
			"nationalities": [{
				"country": "United States",
				"country_code": "US"
			}],
			"function": " Board of Director",
			"updated_at": "2018-12-13",
			"id": "Caterpillarâs Board of Directors.9810c6e7519afd253d77c27438b536f34484e00f",
			"url": "https://www.caterpillar.com/en/company/governance/board-of-directors.html",
			"timestamp": "2018-12-13T00:00:00.000000",
			"program": "United States"
		}
	},{
		"name":"Andreas Ufer",
		"peps": {
		"name ": "Andreas Ufer ",
		"type ": "individual ",
		"source ": "Chief Risk Officer & Member of Management Board,KfW IPEX - Bank GmbH ",
		"nationalities ": [{
			"country ": "Germany ",
			"country_code ": "DE "
		}],
		"function ":"Chief Risk Officer & Member of Management Board ",
		"updated_at ":"2018 - 12 - 19 ",
		"id ":"Chief Risk Officer & Member of Management Board,KfW IPEX - Bank ",
		"url ":"https: //www.bloomberg.com/research/stocks/private/person.asp?personId=135027266&privcapId=22039397&previousCapId=22039397&previousTitle=KfW%20IPEX-Bank%20Gesellschaft%20mit%20beschr%C3%A4nkter%20Haftung",
		"timestamp":"2018-12-19T06:14:00.000000",
		"program":"Germany"
	}},
	{
		"name":"Andreas Ufer",
		"peps":{
			"name":"Andreas Ufer",
			"type":"individual",
			"source":"Chief Risk Officer & Member of Management Board, KfW IPEX-Bank GmbH",
			"nationalities":[
			{
			"country":"Germany",
			"country_code":"DE"
			}
			],
			"function":"Chief Risk Officer & Member of Management Board",
			"updated_at":"2018-12-19",
			"id":"Chief Risk Officer & Member of Management Board, KfW IPEX-Bank",
			"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=135027266&privcapId=22039397&previousCapId=22039397&previousTitle=KfW%20IPEX-Bank%20Gesellschaft%20mit%20beschr%C3%A4nkter%20Haftung",
			"timestamp":"2018-12-19T06:14:00.000000",
			"program":"Germany"
			}

	},
	{
		"name":"Klaus Reinhold Michalak",
		"peps":{
	"name":"Klaus Reinhold Michalak",
	"type":"individual",
	"source":"Chief Executive Officer of Kfw Ipex-Bank GmbH",
	"nationalities":[
	{
	"country":"Germany",
	"country_code":"DE"
	}
	],
	"function":"Chief Executive Officer",
	"updated_at":"2018-12-19",
	"id":"Chief Executive Officer of Kfw Ipex-Bank Gmbh",
	"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=23184766&privcapId=101462",
	"timestamp":"2018-12-19T06:06:00.000000",
	"program":"Germany"
	}
	},
	{	"name":"Markus Scheer",
		"peps":{
			"name":"Markus Scheer",
			"type":"individual",
			"source":"Managing Directors of Kfw Ipex-Bank GmbH",
			"nationalities":[
			{
			"country":"Germany",
			"country_code":"DE"
			}
			],
			"function":"Managing Director",
			"updated_at":"2018-12-19",
			"id":"Managing Directors of Kfw Ipex-Bank GmbH",
			"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=40224112&privcapId=101462",
			"timestamp":"2018-12-19T06:21:00.000000",
			"program":"Germany"
			}
	},{
		"name":"Norbert Gasten",
		"peps":{
			"name":"Norbert Gasten",
			"type":"individual",
			"source":"Employee Representative Member of Board of Supervisory Directors, KfW IPEX-Bank GmbH",
			"nationalities":[
			{
			"country":"Germany",
			"country_code":"DE"
			}
			],
			"function":"Employee Representative Member of Board of Supervisory Directors",
			"updated_at":"2018-12-19",
			"id":"Employee Representative Member of Board of Supervisory Directors, KfW IPEX-Bank GmbH",
			"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=577364197&privcapId=22039397&previousCapId=874832&previousTitle=RAG%20AG",
			"timestamp":"2018-12-19T06:33:00.000000",
			"program":"Germany"
			}
	},{
		"name":"Guido Knittel",
		"peps":{
			"name":"Guido Knittel",
			"type":"individual",
			"source":"Member of Board of Supervisory Directors, KfW IPEX-Bank GmbH",
			"nationalities":[
			{
			"country":"Germany",
			"country_code":"DE"
			}
			],
			"function":"Member of Board of Supervisory Directors",
			"updated_at":"2018-12-19",
			"id":"Member of Board of Supervisory Directors, KfW IPEX-Bank GmbH",
			"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=577071259&privcapId=22039397&previousCapId=874832&previousTitle=RAG%20AG",
			"timestamp":"2018-12-19T06:41:00.000000",
			"program":"Germany"
			}
	},{
		"name":"Dieter Koch",
		"peps":{
			"name":"Dieter Koch",
			"type":"individual",
			"source":"Member ofBoard of Supervisory Directors, KfW IPEX-Bank GmbH",
			"nationalities":[
			{
			"country":"Germany",
			"country_code":"DE"
			}
			],
			"function":"Member of Board of Supervisory Directors",
			"updated_at":"2018-12-19",
			"id":"Member of Board of Supervisory Directors, KfW IPEX-Bank GmbH",
			"url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=577065604&privcapId=22039397&previousCapId=101462&previousTitle=KfW",
			"timestamp":"2018-12-19T06:53:00.000000",
			"program":"Germany"
			}
	},{
		"name":"Kei Fong Chou",
		"peps":{
			"name":"Kei Fong Chou",
			"type":"individual",
			"source":"President and CEO of Novel Enterprises Limited",
			"nationalities":[
			{
			"country":"Hong Kong",
			"country_code":"HK"
			}
			],
			"function":"President and CEO",
			"updated_at":"2007-05-25",
			"id":"President and CEO of Novel Enterprises Limited",
			"url":"http://www.whartonhongkong07.com/bio-s-chou.html",
			"timestamp":"2007-05-25T00:00:00.000000",
			"program":"Hong Kong"
			}
	},{
		"name":"Pierre Aeby",
		"peps":{  
			   "name":"Pierre Aeby",
			   "type":"individual",
			   "source":"Chief Financial Officer at Triodos Bank",
			   "nationalities":[  
			      {  
			         "country":"Belgium",
			         "country_code":"BE"
			      }
			   ],
			   "function":"Chief Financial Officer",
			   "updated_at":"2018-12-19",
			   "id":"Chief Financial Officer at Triodos Bank",
			   "url":"https://managementscope.nl/manager/pierre-aeby",
			   "timestamp":"2018-12-19T06:06:00.000000",
			   "program":"Belgium"
			}
	},{
		"name":"Peter Blom",
		"peps":{  
			   "name":"Peter Blom",
			   "type":"individual",
			   "source":"CEO of Triodos Bank",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"CEO",
			   "updated_at":"2008-12-19",
			   "id":"CEO of Triodos Bank",
			   "url":"https://www.clubofrome.org/member/peter-blom/",
			   "timestamp":"2008-12-19T00:00:00.000000",
			   "program":"Netherlands"
			}
			
	},{  "name":"Jellie Banga",
		"peps":{  
			   "name":"Jellie Banga",
			   "type":"individual",
			   "source":"Chief Operational Officer at Triodos Bank",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"Chief Operational Officer",
			   "updated_at":"2012-11-28",
			   "id":"Chief Operational Officer at Triodos Bank",
			   "url":"https://managementscope.nl/manager/jellie-banga",
			   "timestamp":"2012-11-28T00:00:00.000000",
			   "program":"Netherlands"
			}
	},{
		"name":"Aart de Geus",
		"peps":{  
			   "name":"Aart de Geus",
			   "type":"individual",
			   "source":"Chairman of Triodos Bank Supervisory Board",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"Chairman of Supervisory Board",
			   "updated_at":"2014-05-01",
			   "id":"Chairman of Triodos Bank Supervisory Board",
			   "url":"https://www.parlement.com/id/vg9fgoprhbzu/a_j_aart_jan_de_geus",
			   "timestamp":"2014-05-01T00:00:00.000000",
			   "program":"Netherlands"
			}
	},{
		"name":"Udo Philipp",
		"peps":{
		"name":"Udo Philipp",
		"type":"individual",
		"source":"Member of Supervisory Board, Triodos Bank NV",
		"nationalities":[
		{
		"country":"Netherlands",
		"country_code":"NL"
		}
		],
		"function":"Member of Supervisory Board",
		"updated_at":"2019-01-30",
		"id":"Member of Supervisory Board, Triodos Bank NV",
		"url":"https://www.ethex.org.uk/triodos---governance-and-people_408.html",
		"timestamp":"2019-01-30T00:00:00.000000",
		"program":"Netherlands"
		}		
	},
	{
		 "name":"Ernst-Jan Boers",
		 "peps":{
 
			   "name":"Ernst-Jan Boers",
			   "type":"individual",
			   "source":"Commissioner, Triodos Bank",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"Commissioner",
			   "updated_at":"2017-12-19",
			   "id":"Commissioner, Triodos Bank",
			   "url":"https://managementscope.nl/manager/ernst-jan-boers",
			   "timestamp":"2017-12-19T00:00:00.000000",
			   "program":"Netherlands"
			}
	},

{ 
		"name":"Carla van der Weerdt",
		"peps":{  
			   "name":"Carla van der Weerdt",
			   "type":"individual",
			   "source":"Commissioner(vice-chairman) Triodos Bank",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"Commissioner(vice-chairman)",
			   "updated_at":"2017-12-19",
			   "id":"Commissioner(vice-chairman) Triodos Bank",
			   "url":"https://managementscope.nl/manager/carla-van-der-weerdt",
			   "timestamp":"2017-12-19T00:00:00.000000",
			   "program":"Netherlands"
			}
	},	
		{ "name":"Fieke van der Lecq",

		"peps" :{  
		   "name":"Fieke van der Lecq",
		   "type":"individual",
		   "source":"Member of Supervisory Board, Triodos Bank N.V.",
		   "nationalities":[  
		      {  
		         "country":"Netherlands",
		         "country_code":"NL"
		      }
		   ],
		   "function":"Member of Supervisory Board",
		   "updated_at":"2019-01-29",
		   "id":"Member of Supervisory Board, Triodos Bank N.V.",
		   "url":"https://www.bloomberg.com/research/stocks/private/person.asp?personId=557131990&privcapId=3760494&previousCapId=3760494&previousTitle=Triodos%20Bank%20N.V.",
		   "timestamp":"2019-01-29T00:37:00.000000",
		   "program":"Netherlands"
		}
		},
		{"name":"Dineke Oldenhof",
			"peps":{  
			   "name":"Dineke Oldenhof",
			   "type":"individual",
			   "source":"Member of Supervisory Board, Triodos Bank N.V.",
			   "nationalities":[  
			      {  
			         "country":"Netherlands",
			         "country_code":"NL"
			      }
			   ],
			   "function":"Member of Supervisory Board",
			   "updated_at":"2019-01-29",
			   "id":"Member of Supervisory Board, Triodos Bank N.V.",
			   "url":"https://www.bloomberg.com/research//stocks/private/person.asp?personId=570995663&privcapId=3760494&previousCapId=3113028&previousTitle=NatWest%20Markets%20N.V.",
			   "timestamp":"2019-01-29T08:17:00.000000",
			   "program":"Netherlands"
			}
			},
			{"name":"Gary Page",
				"peps":{  
					   "name":"Gary Page",
					   "type":"individual",
					   "source":"Member of Supervisory Board, Triodos Bank NV",
					   "nationalities":[  
					      {  
					         "country":"Netherlands",
					         "country_code":"NL"
					      }
					   ],
					   "function":"Member of Supervisory Board",
					   "updated_at":"2019-01-30",
					   "id":"Member of Supervisory Board, Triodos Bank NV",
					   "url":"https://www.ethex.org.uk/triodos---governance-and-people_408.html",
					   "timestamp":"2019-01-30T00:00:00.000000",
					   "program":"Netherlands"
					}
			},

			{
					"name":"Josephine de Zwaan",
					"peps":{  
						   "name":"Josephine de Zwaan",
						   "type":"individual",
						   "source":"Chair of the board of the Foundation for the Administration of Shares Triodos Bank N.V.",
						   "nationalities":[  
						      {  
						         "country":"Netherlands",
						         "country_code":"NL"
						      }
						   ],
						   "function":"Chair of the board of the Foundation for the Administration of Shares",
						   "updated_at":"2019-01-30",
						   "id":"Chair of the board of the Foundation for the Administration of Shares Triodos Bank N.V.",
						   "url":"https://www.linkedin.com/in/josephine-de-zwaan-6ab29321/?originalSubdomain=nl",
						   "timestamp":"2019-01-30T00:00:00.000000",
						   "program":"Netherlands"
						}
				},
				{
					 "name":"Mike Nawas",
					 "peps":{
			  
						   "name":"Mike Nawas",
						   "type":"individual",
						   "source":"Vice Chair of the Triodos Bank NV",
						   "nationalities":[  
						      {  
						         "country":"Netherlands",
						         "country_code":"NL"
						      }
						   ],
						   "function":"Vice Chair",
						   "updated_at":"2019-01-30",
						   "id":"Vice Chair of the Triodos Bank NV",
						   "url":"https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						   "timestamp":"2019-01-30T00:00:00.000000",
						   "program":"Netherlands"
						}
				},

			{
					"name":"Nikolai Keller",
					"peps":{  
						   "name":"Nikolai Keller",
						   "type":"individual",
						   "source":"Board member of SAAT Foundation for the Administration of Triodos Bank NV",
						   "nationalities":[  
						      {  
						         "country":"Netherlands",
						         "country_code":"NL"
						      }
						   ],
						   "function":"Board member of SAAT Foundation for the Administration",
						   "updated_at":"2019-01-30",
						   "id":"Board member of SAAT Foundation for the Administration of Triodos Bank NV",
						   "url":"https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						   "timestamp":"2019-01-30T00:00:00.000000",
						   "program":"Netherlands"
						}
				},{ 
					"name":"Willem Lageweg",
					"peps":{  
						   "name":"Willem Lageweg",
						   "type":"individual",
						   "source":"member of SAAT Board at Triodos Bank NV",
						   "nationalities":[  
						      {  
						         "country":"Netherlands",
						         "country_code":"NL"
						      }
						   ],
						   "function":"member of SAAT Board",
						   "updated_at":"2019-01-30",
						   "id":"member of SAAT Board at Triodos Bank NV",
						   "url":"https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						   "timestamp":"2019-01-30T00:00:00.000000",
						   "program":"Netherlands"
						}
				}, {
					"name": "mr. K.J.L. Schoors",
					"peps": {

						"name": "mr. K.J.L. Schoors",
						"type": "individual",
						"source": "member of SAAT Board at Triodos Bank NV",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "member of SAAT Board",
						"updated_at": "2019-01-30",
						"id": "member of SAAT Board at Triodos Bank NV",
						"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}


				, {
					"name": "mr. L.W. Lageweg ",
					"peps": {
						"name": "mr. L.W. Lageweg ",
						"type": "individual",
						"source": "member of SAAT Board at Triodos Bank NV",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "member of SAAT Board",
						"updated_at": "2019-01-30",
						"id": "member of SAAT Board at Triodos Bank NV",
						"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Peter Blom",
					"peps": {
						"name": "Peter Blom",
						"type": "individual",
						"source": "CEO of Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "CEO",
						"updated_at": "2008-12-19",
						"id": "CEO of Triodos Bank",
						"url": "https://www.clubofrome.org/member/peter-blom/",
						"timestamp": "2008-12-19T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Pierre Aeby",
					"peps": {
						"name": "Pierre Aeby",
						"type": "individual",
						"source": "Chief Financial Officer at Triodos Bank",
						"nationalities": [{
							"country": "Belgium",
							"country_code": "BE"
						}],
						"function": "Chief Financial Officer",
						"updated_at": "2018-12-19",
						"id": "Chief Financial Officer at Triodos Bank",
						"url": "https://managementscope.nl/manager/pierre-aeby",
						"timestamp": "2018-12-19T06:06:00.000000",
						"program": "Belgium"
					}
				}, {

					"name": "Pierre Aeby",
					"name": "Pierre Aeby",
					"type": "individual",
					"source": "Chief Financial Officer and Statutory Director at Triodos Bank NV",
					"nationalities": [{
						"country": "Belgium",
						"country_code": "BE"
					}],
					"function": "Chief Financial Officer and Statutory Director",
					"updated_at": "2019-01-30",
					"id": "Chief Financial Officer and Statutory Director at Triodos Bank NV",
					"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
					"timestamp": "2019-01-30T00:00:00.000000",
					"program": "Belgium"
				}, {

					"name": "Jellie Banga",
					"name": "Jellie Banga",
					"type": "individual",
					"source": "Chief Operational Officer at Triodos Bank",
					"nationalities": [{
						"country": "Netherlands",
						"country_code": "NL"
					}],
					"function": "Chief Operational Officer",
					"updated_at": "2012-11-28",
					"id": "Chief Operational Officer at Triodos Bank",
					"url": "https://managementscope.nl/manager/jellie-banga",
					"timestamp": "2012-11-28T00:00:00.000000",
					"program": "Netherlands"
				}, {

					"name": "Aart de Geus",
					"name": "Aart de Geus",
					"type": "individual",
					"source": "Chairman of Triodos Bank Supervisory Board",
					"nationalities": [{
						"country": "Netherlands",
						"country_code": "NL"
					}],
					"function": "Chairman of Supervisory Board",
					"updated_at": "2014-05-01",
					"id": "Chairman of Triodos Bank Supervisory Board",
					"url": "https://www.parlement.com/id/vg9fgoprhbzu/a_j_aart_jan_de_geus",
					"timestamp": "2014-05-01T00:00:00.000000",
					"program": "Netherlands"
				}, {
					"name": "Aart de Geus",
					"peps": {
						"name": "Aart de Geus",
						"type": "individual",
						"source": "chairman of the supervisory board of Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Chairman of Supervisory Board",
						"updated_at": "2014-05-01",
						"id": "chairman of the supervisory board of Triodos Bank",
						"url": "https://en.wikipedia.org/wiki/Aart_Jan_de_Geus",
						"timestamp": "2014-05-01T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Udo Philipp",
					"peps": {
						"name": "Udo Philipp",
						"type": "individual",
						"source": "Member of Supervisory Board, Triodos Bank NV",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Member of Supervisory Board",
						"updated_at": "2019-01-30",
						"id": "Member of Supervisory Board, Triodos Bank NV",
						"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Ernst-Jan Boers",
					"peps": {
						"name": "Ernst-Jan Boers",
						"type": "individual",
						"source": "Commissioner, Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Commissioner",
						"updated_at": "2017-12-19",
						"id": "Commissioner, Triodos Bank",
						"url": "https://managementscope.nl/manager/ernst-jan-boers",
						"timestamp": "2017-12-19T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Carla van der Weerdt",
					"peps": {
						"name": "Carla van der Weerdt",
						"type": "individual",
						"source": "Commissioner(vice-chairman) Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Commissioner(vice-chairman)",
						"updated_at": "2017-12-19",
						"id": "Commissioner(vice-chairman) Triodos Bank",
						"url": "https://managementscope.nl/manager/carla-van-der-weerdt",
						"timestamp": "2017-12-19T00:00:00.000000",
						"program": "Netherlands"
					}
				},

				{
					"name": "Fieke Van Der Lecq",
					"peps": {
						"name": "Fieke Van Der Lecq",
						"type": "individual",
						"source": "Director of Education, Risk Management for Financial Institutions, VU University Amsterdam",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Director of Education, Risk Management for Financial Institutions",
						"updated_at": "2018-12-01",
						"id": "Director of Education, Risk Management for Financial Institutions, VU University Amsterdam",
						"url": "https://managementscope.nl/manager/fieke-van-der-lecq",
						"timestamp": "2018-12-01T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Dineke H. Oldenhof",
					"peps": {
						"name": "Dineke H. Oldenhof",
						"type": "individual",
						"source": "Member of Supervisory Board, Triodos Bank N.V.",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Member of Supervisory Board",
						"updated_at": "2019-01-29",
						"id": "Member of Supervisory Board, Triodos Bank N.V.",
						"url": "https://www.bloomberg.com/research//stocks/private/person.asp?personId=570995663&privcapId=3760494&previousCapId=3113028&previousTitle=NatWest%20Markets%20N.V.",
						"timestamp": "2019-01-29T08:17:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "mr. L.W. Lageweg",
					"peps": {

						"name": "mr. L.W. Lageweg",
						"type": "individual",
						"source": "member of SAAT Board at Triodos Bank NV",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "member of SAAT Board",
						"updated_at": "2019-01-30",
						"id": "member of SAAT Board at Triodos Bank NV",
						"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Nikolai Keller ",
					"peps": {
						"name": "Nikolai Keller ",
						"type": "individual",
						"source": "Board member of SAAT Foundation for the Administration of Triodos Bank NV",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Board member of SAAT Foundation for the Administration",
						"updated_at": "2019-01-30",
						"id": "Board member of SAAT Foundation for the Administration of Triodos Bank NV",
						"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Mike Nawas",
					"peps": {
						"name": "Mike Nawas",
						"type": "individual",
						"source": "Director, Stichting Administratiekantoor Aandelen Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Director, Stichting Administratiekantoor",
						"updated_at": "2019-01-30",
						"id": "Director, Stichting Administratiekantoor Aandelen Triodos Bank",
						"url": "https://www.bloomberg.com/research/stocks/private/person.asp?personId=44734620&privcapId=133192770&previousCapId=133192770&previousTitle=Stichting%20Administratiekantoor%20Aandelen%20Triodos%20Bank",
						"timestamp": "2019-01-30T02:03:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Mike Nawas",
					"peps": {
						"name": "Mike Nawas",
						"type": "individual",
						"source": "Director, Stichting Administratiekantoor Aandelen Triodos Bank",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Director, Stichting Administratiekantoor",
						"updated_at": "2019-01-30",
						"id": "Director, Stichting Administratiekantoor Aandelen Triodos Bank",
						"url": "https://www.bloomberg.com/research/stocks/private/person.asp?personId=44734620&privcapId=133192770&previousCapId=133192770&previousTitle=Stichting%20Administratiekantoor%20Aandelen%20Triodos%20Bank",
						"timestamp": "2019-01-30T02:03:00.000000",
						"program": "Netherlands"
					}
				}, {
					"name": "Josephine de Zwaan",
					"peps": {
						"name": "Josephine de Zwaan",
						"type": "individual",
						"source": "Chair of the board of the Foundation for the Administration of Shares Triodos Bank N.V.",
						"nationalities": [{
							"country": "Netherlands",
							"country_code": "NL"
						}],
						"function": "Chair of the board of the Foundation for the Administration of Shares",
						"updated_at": "2019-01-30",
						"id": "Chair of the board of the Foundation for the Administration of Shares Triodos Bank N.V.",
						"url": "https://www.linkedin.com/in/josephine-de-zwaan-6ab29321/?originalSubdomain=nl",
						"timestamp": "2019-01-30T00:00:00.000000",
						"program": "Netherlands"
					}
				}, {
					
						"name": "Gary Page",
						"peps": {
							"name": "Gary Page",
							"type": "individual",
							"source": "Member of Supervisory Board, Triodos Bank NV",
							"nationalities": [{
								"country": "Netherlands",
								"country_code": "NL"
							}],
							"function": "Member of Supervisory Board",
							"updated_at": "2019-01-30",
							"id": "Member of Supervisory Board, Triodos Bank NV",
							"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
							"timestamp": "2019-01-30T00:00:00.000000",
							"program": "Netherlands"
						}
					},
					{
						"name": "Dineke Oldenhof",
						"peps": {
							"name": "Dineke Oldenhof",
							"type": "individual",
							"source": "member of Triodos Bank's Nomination and Compensation Committee",
							"nationalities": [{
								"country": "Netherlands",
								"country_code": "NL"
							}],
							"function": "member of Nomination and Compensation Committee",
							"updated_at": "2019-01-30",
							"id": "member of Triodos Bank's Nomination and Compensation Committee",
							"url": "https://www.ethex.org.uk/triodos---governance-and-people_408.html",
							"timestamp": "2019-01-30T00:00:00.000000",
							"program": "Netherlands"
						}
					},
					{
						"name": "Dineke Oldenhof",
						"peps": {
							"name": "Dineke Oldenhof",
							"type": "individual",
							"source": "member of Triodos Bank's Nomination and Compensation Committee",
							"nationalities": [{
								"country": "Netherlands",
								"country_code": "NL"
							}],
							"function": "member of Nomination and Compensation Committee",
							"updated_at": "2019-01-30",
							"id": "member of Triodos Bank's Nomination and Compensation Committee",
							"url": "https://nl.linkedin.com/in/dineke-oldenhof-839a096",
							"timestamp": "2019-01-30T00:00:00.000000",
							"program": "Netherlands"
						}
					}


])
.constant('customEntites',{
	editedEnties:[],
	adddeleteEntity:[],
	deleteEntityChart:[],
	screeningaddition:[],
	screeningEdit:[],
	screeningDelete:[]
})
.constant('dummyNews',[
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi hit with €800m in penalties over diesel emissions scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "00fb4312146b97cd563c2a39c02420b0",
		"source": "common-crawl",
		"article_path": "articles/00fb4312146b97cd563c2a39c02420b0.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/00fb4312146b97cd563c2a39c02420b0.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/00fb4312146b97cd563c2a39c02420b0.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Justice Department announces task force to deter fraud, avoid 'piling on'",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "04dcf8e9330ca7216a141f9684dc178b",
		"source": "common-crawl",
		"article_path": "articles/04dcf8e9330ca7216a141f9684dc178b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/04dcf8e9330ca7216a141f9684dc178b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/04dcf8e9330ca7216a141f9684dc178b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Owners of Tainted Volkswagen Diesel Cars File Lawsuit Seeking Compensation",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "054e08fee4d846ab9984febb39da3879",
		"source": "common-crawl",
		"article_path": "articles/054e08fee4d846ab9984febb39da3879.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/054e08fee4d846ab9984febb39da3879.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/054e08fee4d846ab9984febb39da3879.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Short selling and VW's sudden rise",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0955760ce1621ef09adb5379e5b0f932",
		"source": "common-crawl",
		"article_path": "articles/0955760ce1621ef09adb5379e5b0f932.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0955760ce1621ef09adb5379e5b0f932.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0955760ce1621ef09adb5379e5b0f932.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Mexico Issues $8.9 Million Fine Against Volkswagen Over Emissions",
		"event_id": "1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0a62752ccccdc9b2453dd44ee52a9338",
		"source": "common-crawl",
		"article_path": "articles/0a62752ccccdc9b2453dd44ee52a9338.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0a62752ccccdc9b2453dd44ee52a9338.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0a62752ccccdc9b2453dd44ee52a9338.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Faces Lawsuit in China Over Emissions",
		"event_id": "1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0beb36cc9f4494d0ebe9219f26376cad",
		"source": "common-crawl",
		"article_path": "articles/0beb36cc9f4494d0ebe9219f26376cad.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0beb36cc9f4494d0ebe9219f26376cad.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0beb36cc9f4494d0ebe9219f26376cad.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "New Mexico AG Sues Volkswagen Over Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0d0a3b19e737ae44d13da5d5351ded91",
		"source": "common-crawl",
		"article_path": "articles/0d0a3b19e737ae44d13da5d5351ded91.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0d0a3b19e737ae44d13da5d5351ded91.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0d0a3b19e737ae44d13da5d5351ded91.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Pleads Guilty to Criminal Charges in Emissions-Cheating Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0dbead9a4308ee3cf24047e339713453",
		"source": "common-crawl",
		"article_path": "articles/0dbead9a4308ee3cf24047e339713453.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0dbead9a4308ee3cf24047e339713453.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0dbead9a4308ee3cf24047e339713453.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Contends With New Legal Woes",
		"event_id": "1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "0dd32bb2a95c4da98663112de868c57f",
		"source": "common-crawl",
		"article_path": "articles/0dd32bb2a95c4da98663112de868c57f.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/0dd32bb2a95c4da98663112de868c57f.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/0dd32bb2a95c4da98663112de868c57f.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "South Korea Orders Recall After Finding Volkswagen Cheated on Emissions",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1066dc08cb46e076ed17ff341acd79c5",
		"source": "common-crawl",
		"article_path": "articles/1066dc08cb46e076ed17ff341acd79c5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1066dc08cb46e076ed17ff341acd79c5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1066dc08cb46e076ed17ff341acd79c5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Sales Fall in October Amid Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "12e096b2c984ee739ad1f5cb055e1ff6",
		"source": "common-crawl",
		"article_path": "articles/12e096b2c984ee739ad1f5cb055e1ff6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/12e096b2c984ee739ad1f5cb055e1ff6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/12e096b2c984ee739ad1f5cb055e1ff6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Engineer Sentenced for Role in Emissions Fraud",
		"event_id": "2",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "13047399b54ec008c9822c43cc4b85e8",
		"source": "common-crawl",
		"article_path": "articles/13047399b54ec008c9822c43cc4b85e8.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/13047399b54ec008c9822c43cc4b85e8.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/13047399b54ec008c9822c43cc4b85e8.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Fiat Chrysler agrees to $800 million U.S. diesel-emissions settlement",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "132fd9d3e067927316680d46ef7ec81c",
		"source": "common-crawl",
		"article_path": "articles/132fd9d3e067927316680d46ef7ec81c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/132fd9d3e067927316680d46ef7ec81c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/132fd9d3e067927316680d46ef7ec81c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "European Corporate Roundup for Monday",
		"event_id": "2",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1466764fda4cca1237869ca53b603580",
		"source": "common-crawl",
		"article_path": "articles/1466764fda4cca1237869ca53b603580.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1466764fda4cca1237869ca53b603580.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1466764fda4cca1237869ca53b603580.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Court Dismisses $1.3 Billion Damages Claim Against Porsche",
		"event_id": "2",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1473caa9874888a428bae4b795bbe8d9",
		"source": "common-crawl",
		"article_path": "articles/1473caa9874888a428bae4b795bbe8d9.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1473caa9874888a428bae4b795bbe8d9.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1473caa9874888a428bae4b795bbe8d9.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Fiat emissions probe centers on several models - letter",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "16368fb7dd96bb451052a99a789f328b",
		"source": "common-crawl",
		"article_path": "articles/16368fb7dd96bb451052a99a789f328b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/16368fb7dd96bb451052a99a789f328b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/16368fb7dd96bb451052a99a789f328b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Faces South Korea Sales Ban",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "16b0eb150d8b0e43660b8ce3011eb998",
		"source": "common-crawl",
		"article_path": "articles/16b0eb150d8b0e43660b8ce3011eb998.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/16b0eb150d8b0e43660b8ce3011eb998.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/16b0eb150d8b0e43660b8ce3011eb998.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen executive gets seven years for U.S. emissions fraud",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1870d103813f581d4e28e4d2fe218658",
		"source": "common-crawl",
		"article_path": "articles/1870d103813f581d4e28e4d2fe218658.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1870d103813f581d4e28e4d2fe218658.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1870d103813f581d4e28e4d2fe218658.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Dieselgate 2.0: Porsche And Audi Accused Of Using Sophisticated Defeat Devices",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1b1d7644dc1ab9bcdfb9bf128cd11159",
		"source": "common-crawl",
		"article_path": "articles/1b1d7644dc1ab9bcdfb9bf128cd11159.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1b1d7644dc1ab9bcdfb9bf128cd11159.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1b1d7644dc1ab9bcdfb9bf128cd11159.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Counted, Weighed, and Divided",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1e391182096eced84e89eb64432261c7",
		"source": "common-crawl",
		"article_path": "articles/1e391182096eced84e89eb64432261c7.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1e391182096eced84e89eb64432261c7.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1e391182096eced84e89eb64432261c7.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW bid to delay first U.S. diesel emissions trial rejected",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "1ec53b49a70f2f63cb5b6e52dd98f72a",
		"source": "common-crawl",
		"article_path": "articles/1ec53b49a70f2f63cb5b6e52dd98f72a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/1ec53b49a70f2f63cb5b6e52dd98f72a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/1ec53b49a70f2f63cb5b6e52dd98f72a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Used Cars to Avoid Buying",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2010e6e3b3ed7d534e75a60957b3abd4",
		"source": "common-crawl",
		"article_path": "articles/2010e6e3b3ed7d534e75a60957b3abd4.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2010e6e3b3ed7d534e75a60957b3abd4.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2010e6e3b3ed7d534e75a60957b3abd4.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "South Korean Regulator Considering Filing Criminal Charges Against Volkswagen Executives",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2088373cef2ded720479b4b02b6d7a02",
		"source": "common-crawl",
		"article_path": "articles/2088373cef2ded720479b4b02b6d7a02.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2088373cef2ded720479b4b02b6d7a02.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2088373cef2ded720479b4b02b6d7a02.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi names new leader after CEO arrested",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "20c215e7e862ac2cb64c2f71b5128761",
		"source": "common-crawl",
		"article_path": "articles/20c215e7e862ac2cb64c2f71b5128761.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/20c215e7e862ac2cb64c2f71b5128761.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/20c215e7e862ac2cb64c2f71b5128761.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Nordea Blacklists Volkswagen Stocks and Bonds",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "20dc7bc54666cf72409baa04d6756025",
		"source": "common-crawl",
		"article_path": "articles/20dc7bc54666cf72409baa04d6756025.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/20dc7bc54666cf72409baa04d6756025.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/20dc7bc54666cf72409baa04d6756025.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Pressured Dealers to Keep Prices at Its Suggested Levels",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "25646dfc8a110fb736fbebbb62abf544",
		"source": "common-crawl",
		"article_path": "articles/25646dfc8a110fb736fbebbb62abf544.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/25646dfc8a110fb736fbebbb62abf544.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/25646dfc8a110fb736fbebbb62abf544.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Authorities Investigating Missing VW Emissions Scandal File",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2586e2895bdacb6e4283ced16baf8843",
		"source": "common-crawl",
		"article_path": "articles/2586e2895bdacb6e4283ced16baf8843.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2586e2895bdacb6e4283ced16baf8843.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2586e2895bdacb6e4283ced16baf8843.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "European Investment Bank Chief Criticizes VW on Scandal Response",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "28e10d3335c66a2f50ba8652aca2401f",
		"source": "common-crawl",
		"article_path": "articles/28e10d3335c66a2f50ba8652aca2401f.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/28e10d3335c66a2f50ba8652aca2401f.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/28e10d3335c66a2f50ba8652aca2401f.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Supplier to Face Criminal Case in Emissions Fraud",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2a54d807f0dfe81d255b51c2c9730cad",
		"source": "common-crawl",
		"article_path": "articles/2a54d807f0dfe81d255b51c2c9730cad.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2a54d807f0dfe81d255b51c2c9730cad.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2a54d807f0dfe81d255b51c2c9730cad.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Emission Scandal: Latest News, Videos and Photos",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2accaabbe2c2141025e9d4b3c8c9f445",
		"source": "common-crawl",
		"article_path": "articles/2accaabbe2c2141025e9d4b3c8c9f445.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2accaabbe2c2141025e9d4b3c8c9f445.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2accaabbe2c2141025e9d4b3c8c9f445.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. FTC Alleges Volkswagen Erased or Lost 23 Mobile Phones of Key Employees",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2cec25dbde07eb54502e5790a699e409",
		"source": "common-crawl",
		"article_path": "articles/2cec25dbde07eb54502e5790a699e409.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2cec25dbde07eb54502e5790a699e409.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2cec25dbde07eb54502e5790a699e409.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW, Audi May Face Higher Costs to Resolve Emissions Issues",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2d038baefa6e54991ab8fee10a6b3735",
		"source": "common-crawl",
		"article_path": "articles/2d038baefa6e54991ab8fee10a6b3735.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2d038baefa6e54991ab8fee10a6b3735.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2d038baefa6e54991ab8fee10a6b3735.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Latest News on Manfred Durniok - Times of India",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2df5d484ecca5a84768afe84977a15b1",
		"source": "common-crawl",
		"article_path": "articles/2df5d484ecca5a84768afe84977a15b1.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2df5d484ecca5a84768afe84977a15b1.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2df5d484ecca5a84768afe84977a15b1.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Ex-Chairman’s Account Is Latest Plot Twist in Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "2ed32201153d3ee1367eb430daaa0d4b",
		"source": "common-crawl",
		"article_path": "articles/2ed32201153d3ee1367eb430daaa0d4b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/2ed32201153d3ee1367eb430daaa0d4b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/2ed32201153d3ee1367eb430daaa0d4b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "New Jersey Sues Volkswagen Over Emissions Cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "3074c57af1192109fb91dab1cb77ec94",
		"source": "common-crawl",
		"article_path": "articles/3074c57af1192109fb91dab1cb77ec94.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/3074c57af1192109fb91dab1cb77ec94.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/3074c57af1192109fb91dab1cb77ec94.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Faces Tax-Evasion Investigation in Germany",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "335b17803e6b74d136ff14511157a512",
		"source": "common-crawl",
		"article_path": "articles/335b17803e6b74d136ff14511157a512.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/335b17803e6b74d136ff14511157a512.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/335b17803e6b74d136ff14511157a512.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Engineer Pleads Guilty in Emissions-Cheating Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "33ec93e616388bd482b5653be70c8bf2",
		"source": "common-crawl",
		"article_path": "articles/33ec93e616388bd482b5653be70c8bf2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/33ec93e616388bd482b5653be70c8bf2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/33ec93e616388bd482b5653be70c8bf2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Bad News? What Bad News? Volkswagen Bullish Despite Emissions Costs",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "3511a9f0bda51d778e2324e8ccb5a241",
		"source": "common-crawl",
		"article_path": "articles/3511a9f0bda51d778e2324e8ccb5a241.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/3511a9f0bda51d778e2324e8ccb5a241.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/3511a9f0bda51d778e2324e8ccb5a241.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "BMW Denies VW Claims of Possible Coordination by German Car Makers",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "35517fc9fb101a3d927563b9bb9bea4c",
		"source": "common-crawl",
		"article_path": "articles/35517fc9fb101a3d927563b9bb9bea4c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/35517fc9fb101a3d927563b9bb9bea4c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/35517fc9fb101a3d927563b9bb9bea4c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi’s CEO, in Jail, Steps Down from Audi and Volkswagen Boards [Update]",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "3617213e378460c0c6408b94a8028acc",
		"source": "common-crawl",
		"article_path": "articles/3617213e378460c0c6408b94a8028acc.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/3617213e378460c0c6408b94a8028acc.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/3617213e378460c0c6408b94a8028acc.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Former VW Compliance Executive Pleads Guilty in Emissions Case",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "367e3e40038becbbb0d370c76c29f753",
		"source": "common-crawl",
		"article_path": "articles/367e3e40038becbbb0d370c76c29f753.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/367e3e40038becbbb0d370c76c29f753.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/367e3e40038becbbb0d370c76c29f753.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Corruption scandals show why leaders should highlight ethics",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "371790f90770aa2ae369a25c6799cae3",
		"source": "common-crawl",
		"article_path": "articles/371790f90770aa2ae369a25c6799cae3.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/371790f90770aa2ae369a25c6799cae3.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/371790f90770aa2ae369a25c6799cae3.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Widens Diesel-Engine Emissions Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "3a3ea76e34a4089b8f7f84e0df2c6f7c",
		"source": "common-crawl",
		"article_path": "articles/3a3ea76e34a4089b8f7f84e0df2c6f7c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/3a3ea76e34a4089b8f7f84e0df2c6f7c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/3a3ea76e34a4089b8f7f84e0df2c6f7c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "South Korea to File Criminal Complaints Against Volkswagen Over Emissions Claims",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "3e288835b82bb9e42da177376029aa36",
		"source": "common-crawl",
		"article_path": "articles/3e288835b82bb9e42da177376029aa36.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/3e288835b82bb9e42da177376029aa36.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/3e288835b82bb9e42da177376029aa36.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. judge rejects Wyoming's environmental lawsuit against Volkswagen",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "404927434b7bb0c00b8644755dbaddac",
		"source": "common-crawl",
		"article_path": "articles/404927434b7bb0c00b8644755dbaddac.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/404927434b7bb0c00b8644755dbaddac.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/404927434b7bb0c00b8644755dbaddac.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Car Cartel Faces Class Action Suit, 'Probably Not The Last One'",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "40afdb16ca70e173da95bd393738c111",
		"source": "common-crawl",
		"article_path": "articles/40afdb16ca70e173da95bd393738c111.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/40afdb16ca70e173da95bd393738c111.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/40afdb16ca70e173da95bd393738c111.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Dealerships Sue Auto Maker Over Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "40bf7350d701433b79edcd16d025b14a",
		"source": "common-crawl",
		"article_path": "articles/40bf7350d701433b79edcd16d025b14a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/40bf7350d701433b79edcd16d025b14a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/40bf7350d701433b79edcd16d025b14a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW files complaint over searches of its dieselgate law firm",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "470b860616e5803bff0e72a9fe97ca20",
		"source": "common-crawl",
		"article_path": "articles/470b860616e5803bff0e72a9fe97ca20.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/470b860616e5803bff0e72a9fe97ca20.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/470b860616e5803bff0e72a9fe97ca20.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Car Industry’s Diesel Woes Just Won’t Die",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "47b7df7568312d1416d6938facf61bb3",
		"source": "common-crawl",
		"article_path": "articles/47b7df7568312d1416d6938facf61bb3.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/47b7df7568312d1416d6938facf61bb3.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/47b7df7568312d1416d6938facf61bb3.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Did German carmakers hold back clean engine tech? The EU is investigating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "4c9615434464d9469f0792042f4e344e",
		"source": "common-crawl",
		"article_path": "articles/4c9615434464d9469f0792042f4e344e.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/4c9615434464d9469f0792042f4e344e.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/4c9615434464d9469f0792042f4e344e.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Ledger: U.S. Regulators Impose Lower Fines on Wall Street",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "4e29533b693a8b1b2f8df83b30323353",
		"source": "common-crawl",
		"article_path": "articles/4e29533b693a8b1b2f8df83b30323353.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/4e29533b693a8b1b2f8df83b30323353.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/4e29533b693a8b1b2f8df83b30323353.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Jail Time Urged in VW Takeover Case",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "4f24b15bbe1f481cf756eb341f5202d2",
		"source": "common-crawl",
		"article_path": "articles/4f24b15bbe1f481cf756eb341f5202d2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/4f24b15bbe1f481cf756eb341f5202d2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/4f24b15bbe1f481cf756eb341f5202d2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Diesel Scandal Coming To A TV Screen and Movie Theater Near You",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "4f714f397c683ffc2b134588fc74a622",
		"source": "common-crawl",
		"article_path": "articles/4f714f397c683ffc2b134588fc74a622.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/4f714f397c683ffc2b134588fc74a622.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/4f714f397c683ffc2b134588fc74a622.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW's Former CEO Martin Winterkorn Charged With Fraud And Conspiracy In Diesel-Cheating Case",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "515ce855b67159313c43da69f33a35f1",
		"source": "common-crawl",
		"article_path": "articles/515ce855b67159313c43da69f33a35f1.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/515ce855b67159313c43da69f33a35f1.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/515ce855b67159313c43da69f33a35f1.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi Settles Diesel Engine Probe for Almost $1 Billion",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "51933e25c43ec3bc201137d6ef73a992",
		"source": "common-crawl",
		"article_path": "articles/51933e25c43ec3bc201137d6ef73a992.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/51933e25c43ec3bc201137d6ef73a992.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/51933e25c43ec3bc201137d6ef73a992.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Germany fines Volkswagen $1.2 billion over diesel scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5502f662b762e61cc82673472a5970e9",
		"source": "common-crawl",
		"article_path": "articles/5502f662b762e61cc82673472a5970e9.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5502f662b762e61cc82673472a5970e9.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5502f662b762e61cc82673472a5970e9.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Suspends Quality-Control Chief",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "56f33a7c318a0757dd7ca8cc71256827",
		"source": "common-crawl",
		"article_path": "articles/56f33a7c318a0757dd7ca8cc71256827.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/56f33a7c318a0757dd7ca8cc71256827.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/56f33a7c318a0757dd7ca8cc71256827.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Latest News on Goliath car - Times of India",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "57c946de8a72a64143d418df08c15b55",
		"source": "common-crawl",
		"article_path": "articles/57c946de8a72a64143d418df08c15b55.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/57c946de8a72a64143d418df08c15b55.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/57c946de8a72a64143d418df08c15b55.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Risk Report: Companies Team Up to Promote Ethics in India",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "57e95eadc97cf1662b2b91d5d375cb13",
		"source": "common-crawl",
		"article_path": "articles/57e95eadc97cf1662b2b91d5d375cb13.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/57e95eadc97cf1662b2b91d5d375cb13.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/57e95eadc97cf1662b2b91d5d375cb13.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen’s Audi to Pay Millions to Update Engine Software in U.S. Model",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "596fdfe45768dc97c5a5ef32b7c77da1",
		"source": "common-crawl",
		"article_path": "articles/596fdfe45768dc97c5a5ef32b7c77da1.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/596fdfe45768dc97c5a5ef32b7c77da1.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/596fdfe45768dc97c5a5ef32b7c77da1.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Monkeys were used to test diesel fumes, German carmakers say",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5975d2cc87115db6f070c4972458afc6",
		"source": "common-crawl",
		"article_path": "articles/5975d2cc87115db6f070c4972458afc6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5975d2cc87115db6f070c4972458afc6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5975d2cc87115db6f070c4972458afc6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. Said to Uncover Evidence of Criminal Acts in VW Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "59cab34bb52078492249ce088d1f917c",
		"source": "common-crawl",
		"article_path": "articles/59cab34bb52078492249ce088d1f917c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/59cab34bb52078492249ce088d1f917c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/59cab34bb52078492249ce088d1f917c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "France Launches Volkswagen Investigation",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5a77be8d39aa42fc7a15421969b8f380",
		"source": "common-crawl",
		"article_path": "articles/5a77be8d39aa42fc7a15421969b8f380.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5a77be8d39aa42fc7a15421969b8f380.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5a77be8d39aa42fc7a15421969b8f380.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Prosecutors Raid VW Offices",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5a7b4d844aabe33143a9bcf1f6cd157f",
		"source": "common-crawl",
		"article_path": "articles/5a7b4d844aabe33143a9bcf1f6cd157f.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5a7b4d844aabe33143a9bcf1f6cd157f.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5a7b4d844aabe33143a9bcf1f6cd157f.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. regulators approve VW diesel fix for 84,000 vehicles",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5b1374521c2096a9e133cb82d612f7e9",
		"source": "common-crawl",
		"article_path": "articles/5b1374521c2096a9e133cb82d612f7e9.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5b1374521c2096a9e133cb82d612f7e9.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5b1374521c2096a9e133cb82d612f7e9.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen offers Germans mega bucks to scrap their diesel cars",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5d92598540b56a39138525c1bb01a449",
		"source": "common-crawl",
		"article_path": "articles/5d92598540b56a39138525c1bb01a449.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5d92598540b56a39138525c1bb01a449.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5d92598540b56a39138525c1bb01a449.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Scandal to Hurt Its Financing Arm",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5e776cc38ce81c065e93fcf90eaee19c",
		"source": "common-crawl",
		"article_path": "articles/5e776cc38ce81c065e93fcf90eaee19c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5e776cc38ce81c065e93fcf90eaee19c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5e776cc38ce81c065e93fcf90eaee19c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Documentary film-maker Alex Gibney on why corporate scandals are hot right now",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "5f42d81b1c4f49ba8ec164aeff6060c6",
		"source": "common-crawl",
		"article_path": "articles/5f42d81b1c4f49ba8ec164aeff6060c6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/5f42d81b1c4f49ba8ec164aeff6060c6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/5f42d81b1c4f49ba8ec164aeff6060c6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen’s European Customers May Miss Out on Emissions Settlement",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "61ac1bd19984c9d4bf7d26117aa2f42a",
		"source": "common-crawl",
		"article_path": "articles/61ac1bd19984c9d4bf7d26117aa2f42a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/61ac1bd19984c9d4bf7d26117aa2f42a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/61ac1bd19984c9d4bf7d26117aa2f42a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Dieselgate Product Of Vast VW-BMW-Daimler Car Cartel Conspiracy, Fresh Report Says",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "61feaa918d073295d5c441658a24f621",
		"source": "common-crawl",
		"article_path": "articles/61feaa918d073295d5c441658a24f621.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/61feaa918d073295d5c441658a24f621.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/61feaa918d073295d5c441658a24f621.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Dieselgate isn't over: Volkswagen's CEO is now under investigation",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "6490651d01c3e5713b5f84be666bdc68",
		"source": "common-crawl",
		"article_path": "articles/6490651d01c3e5713b5f84be666bdc68.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/6490651d01c3e5713b5f84be666bdc68.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/6490651d01c3e5713b5f84be666bdc68.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi CEO Targeted in Emissions-Cheating Investigation",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "653b1614c427573b373abd8d85f8b69c",
		"source": "common-crawl",
		"article_path": "articles/653b1614c427573b373abd8d85f8b69c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/653b1614c427573b373abd8d85f8b69c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/653b1614c427573b373abd8d85f8b69c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen’s Board to Discuss Giving Up Bonuses",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "65ce3851752c7dd497d8a8d71a740f87",
		"source": "common-crawl",
		"article_path": "articles/65ce3851752c7dd497d8a8d71a740f87.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/65ce3851752c7dd497d8a8d71a740f87.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/65ce3851752c7dd497d8a8d71a740f87.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen’s $14.7 Billion Buyback Deal on Emissions Scandal Approved",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "6daa1e5c30dfd825e68a7345719fb4fb",
		"source": "common-crawl",
		"article_path": "articles/6daa1e5c30dfd825e68a7345719fb4fb.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/6daa1e5c30dfd825e68a7345719fb4fb.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/6daa1e5c30dfd825e68a7345719fb4fb.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Lasting legacy of VW diesel scandal: EU gets serious about testing",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "6e07c168e3d084d2cdc52bd7ea0bcb19",
		"source": "common-crawl",
		"article_path": "articles/6e07c168e3d084d2cdc52bd7ea0bcb19.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/6e07c168e3d084d2cdc52bd7ea0bcb19.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/6e07c168e3d084d2cdc52bd7ea0bcb19.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Switzerland Considers Banning Volkswagen Diesel Car Sales",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "6e1089ee451b8cf85e81890d5e7a9a1b",
		"source": "common-crawl",
		"article_path": "articles/6e1089ee451b8cf85e81890d5e7a9a1b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/6e1089ee451b8cf85e81890d5e7a9a1b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/6e1089ee451b8cf85e81890d5e7a9a1b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. regulators approve fix for 38,000 VW 3.0-liter diesel SUVs",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "6f8a49d60d4e2a73ef6d0662ec4f4ee3",
		"source": "common-crawl",
		"article_path": "articles/6f8a49d60d4e2a73ef6d0662ec4f4ee3.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/6f8a49d60d4e2a73ef6d0662ec4f4ee3.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/6f8a49d60d4e2a73ef6d0662ec4f4ee3.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW executive convicted in U.S. may seek transfer to Germany - Welt am Sonntag",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "71142a299cf6651c7f825af1d9b89f45",
		"source": "common-crawl",
		"article_path": "articles/71142a299cf6651c7f825af1d9b89f45.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/71142a299cf6651c7f825af1d9b89f45.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/71142a299cf6651c7f825af1d9b89f45.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Fiat Chrysler to pay $515 mn in US 'dieselgate' settlements",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "733ddf2e9273ba4e4b6b406e456bd910",
		"source": "common-crawl",
		"article_path": "articles/733ddf2e9273ba4e4b6b406e456bd910.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/733ddf2e9273ba4e4b6b406e456bd910.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/733ddf2e9273ba4e4b6b406e456bd910.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "First The Cartel, Now The Rebellion: Labor Leaders And Managers Demand Heads At VW",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "7becf32b6803359c3d1fb97fbd0210cf",
		"source": "common-crawl",
		"article_path": "articles/7becf32b6803359c3d1fb97fbd0210cf.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/7becf32b6803359c3d1fb97fbd0210cf.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/7becf32b6803359c3d1fb97fbd0210cf.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Hyundai Offers Payment to Settle Fuel-Economy Suit",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "7d5c872435d8b998de94872ceed73bb5",
		"source": "common-crawl",
		"article_path": "articles/7d5c872435d8b998de94872ceed73bb5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/7d5c872435d8b998de94872ceed73bb5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/7d5c872435d8b998de94872ceed73bb5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW to Pay California $86 Million More in Penalties Over Emissions Cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "807b49a1c2b0751b859ecdc43eb1e5e5",
		"source": "common-crawl",
		"article_path": "articles/807b49a1c2b0751b859ecdc43eb1e5e5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/807b49a1c2b0751b859ecdc43eb1e5e5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/807b49a1c2b0751b859ecdc43eb1e5e5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Supplier to Plead Guilty to Conspiracy, Pay $35 Million Fine in Emissions-Cheating Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "84897d4628d94c1564d67ca25f915981",
		"source": "common-crawl",
		"article_path": "articles/84897d4628d94c1564d67ca25f915981.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/84897d4628d94c1564d67ca25f915981.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/84897d4628d94c1564d67ca25f915981.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Potentially Facing Much More 'Dieselgate' Pain In Germany",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "863583226446cbab2fe3a5938cc71c7d",
		"source": "common-crawl",
		"article_path": "articles/863583226446cbab2fe3a5938cc71c7d.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/863583226446cbab2fe3a5938cc71c7d.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/863583226446cbab2fe3a5938cc71c7d.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Business blog",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "882a1c4e9a0bb242f72e10cc57ff2046",
		"source": "common-crawl",
		"article_path": "articles/882a1c4e9a0bb242f72e10cc57ff2046.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/882a1c4e9a0bb242f72e10cc57ff2046.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/882a1c4e9a0bb242f72e10cc57ff2046.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Shares Slide After Audi Chief Arrested",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "889f869b8f55cfbdb026a68bf051e8b4",
		"source": "common-crawl",
		"article_path": "articles/889f869b8f55cfbdb026a68bf051e8b4.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/889f869b8f55cfbdb026a68bf051e8b4.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/889f869b8f55cfbdb026a68bf051e8b4.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Winterkorn to Resign as CEO of Porsche Holding Company Amid Volkswagen Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "88a586347f648750bab11bf14259c7a2",
		"source": "common-crawl",
		"article_path": "articles/88a586347f648750bab11bf14259c7a2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/88a586347f648750bab11bf14259c7a2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/88a586347f648750bab11bf14259c7a2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "News, Articles, Biography, Photos",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "89af515329251ce0ce47d6ed09143205",
		"source": "common-crawl",
		"article_path": "articles/89af515329251ce0ce47d6ed09143205.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/89af515329251ce0ce47d6ed09143205.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/89af515329251ce0ce47d6ed09143205.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen, Audi Halt Sales of 3.0-Liter Diesel 2013-2016 Models",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8aeba0004174e2dbbf98fdae5ddbe514",
		"source": "common-crawl",
		"article_path": "articles/8aeba0004174e2dbbf98fdae5ddbe514.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8aeba0004174e2dbbf98fdae5ddbe514.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8aeba0004174e2dbbf98fdae5ddbe514.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Prosecutors Seek Punitive Damages From Volkswagen",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8b628d3709cc366df57d5e640f06913d",
		"source": "common-crawl",
		"article_path": "articles/8b628d3709cc366df57d5e640f06913d.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8b628d3709cc366df57d5e640f06913d.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8b628d3709cc366df57d5e640f06913d.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Institutional Investors Sue Volkswagen Over Fall in Share Price",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8b839881ef6e0f94c25008f7a0ff64ea",
		"source": "common-crawl",
		"article_path": "articles/8b839881ef6e0f94c25008f7a0ff64ea.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8b839881ef6e0f94c25008f7a0ff64ea.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8b839881ef6e0f94c25008f7a0ff64ea.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Faces $9 Billion in Claims in Germany From Stock Drop",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8d944bba7e7db0b1940be1f36823a218",
		"source": "common-crawl",
		"article_path": "articles/8d944bba7e7db0b1940be1f36823a218.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8d944bba7e7db0b1940be1f36823a218.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8d944bba7e7db0b1940be1f36823a218.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen to Pay $1 Billion to Buy Back or Fix More Diesel Vehicles",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8e5c860c6540b11b622cbe5fa52a096e",
		"source": "common-crawl",
		"article_path": "articles/8e5c860c6540b11b622cbe5fa52a096e.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8e5c860c6540b11b622cbe5fa52a096e.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8e5c860c6540b11b622cbe5fa52a096e.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Halts Sales of Some New, Used Cars in U.S.",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "8ff6ff0ffa3ca3d063f176bd7e3af9f1",
		"source": "common-crawl",
		"article_path": "articles/8ff6ff0ffa3ca3d063f176bd7e3af9f1.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/8ff6ff0ffa3ca3d063f176bd7e3af9f1.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/8ff6ff0ffa3ca3d063f176bd7e3af9f1.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Cars film: Latest News, Videos and Photos of Cars film",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "90691a83810812122c0f771dcb0c58c2",
		"source": "common-crawl",
		"article_path": "articles/90691a83810812122c0f771dcb0c58c2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/90691a83810812122c0f771dcb0c58c2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/90691a83810812122c0f771dcb0c58c2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen’s Emissions-Testing Scandal Widens",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "9188e5bbf0f6a1874c7edf701c8520b2",
		"source": "common-crawl",
		"article_path": "articles/9188e5bbf0f6a1874c7edf701c8520b2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/9188e5bbf0f6a1874c7edf701c8520b2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/9188e5bbf0f6a1874c7edf701c8520b2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Suspends More Employees",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "934e8840087880d6d3e7540e2bca46cc",
		"source": "common-crawl",
		"article_path": "articles/934e8840087880d6d3e7540e2bca46cc.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/934e8840087880d6d3e7540e2bca46cc.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/934e8840087880d6d3e7540e2bca46cc.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Some Indicted VW Officials May Be Beyond U.S. Reach",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "936218f453aac7f0c41a619341bc4ce0",
		"source": "common-crawl",
		"article_path": "articles/936218f453aac7f0c41a619341bc4ce0.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/936218f453aac7f0c41a619341bc4ce0.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/936218f453aac7f0c41a619341bc4ce0.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Wins Approval for Diesel-Emissions Fixes",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "93dd9fe2e07b138833e5d9998312c372",
		"source": "common-crawl",
		"article_path": "articles/93dd9fe2e07b138833e5d9998312c372.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/93dd9fe2e07b138833e5d9998312c372.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/93dd9fe2e07b138833e5d9998312c372.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Senior Porsche Executive Arrested in Connection With Volkswagen Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "954fbd85cb5cd17b8fedb4f9d4b32239",
		"source": "common-crawl",
		"article_path": "articles/954fbd85cb5cd17b8fedb4f9d4b32239.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/954fbd85cb5cd17b8fedb4f9d4b32239.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/954fbd85cb5cd17b8fedb4f9d4b32239.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. Pursues Several Paths in Volkswagen Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "96a9adcd897d90894ba7c6623258f265",
		"source": "common-crawl",
		"article_path": "articles/96a9adcd897d90894ba7c6623258f265.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/96a9adcd897d90894ba7c6623258f265.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/96a9adcd897d90894ba7c6623258f265.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi on the Forbes World's Most Valuable Brands List",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "97d366e4a7e1d21cd9ece7dc14019155",
		"source": "common-crawl",
		"article_path": "articles/97d366e4a7e1d21cd9ece7dc14019155.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/97d366e4a7e1d21cd9ece7dc14019155.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/97d366e4a7e1d21cd9ece7dc14019155.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen diesel probe expanded to include CEO Matthias Mueller",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "97facd1c15272e5434e4b6fe4c2b9b2b",
		"source": "common-crawl",
		"article_path": "articles/97facd1c15272e5434e4b6fe4c2b9b2b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/97facd1c15272e5434e4b6fe4c2b9b2b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/97facd1c15272e5434e4b6fe4c2b9b2b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen agrees to $232 million 3.0-liter Canadian emissions settlement",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "99d46805284ab871c3403aa39dbe02de",
		"source": "common-crawl",
		"article_path": "articles/99d46805284ab871c3403aa39dbe02de.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/99d46805284ab871c3403aa39dbe02de.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/99d46805284ab871c3403aa39dbe02de.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Business blog",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "9a237cdb6fe0ba9a86dea18549d69905",
		"source": "common-crawl",
		"article_path": "articles/9a237cdb6fe0ba9a86dea18549d69905.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/9a237cdb6fe0ba9a86dea18549d69905.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/9a237cdb6fe0ba9a86dea18549d69905.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Suzuki Pays Damages to Volkswagen, Settling Dispute",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "9e8236e606687717f6398c998c2f86fc",
		"source": "common-crawl",
		"article_path": "articles/9e8236e606687717f6398c998c2f86fc.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/9e8236e606687717f6398c998c2f86fc.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/9e8236e606687717f6398c998c2f86fc.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi and Porsche Offices Raided as Volkswagen Emissions Scandal Drags On",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "a0894ce7611c38298a960cd8da2e6a50",
		"source": "common-crawl",
		"article_path": "articles/a0894ce7611c38298a960cd8da2e6a50.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/a0894ce7611c38298a960cd8da2e6a50.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/a0894ce7611c38298a960cd8da2e6a50.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi CEO arrested over alleged role in Dieselgate scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "a2476ecf324a5f1ef66e86d62b13e68e",
		"source": "common-crawl",
		"article_path": "articles/a2476ecf324a5f1ef66e86d62b13e68e.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/a2476ecf324a5f1ef66e86d62b13e68e.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/a2476ecf324a5f1ef66e86d62b13e68e.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Fiat Chrysler reaches settlement in emissions cheating cases",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "a5ac47e7471f81df7432f42e73b8f57d",
		"source": "common-crawl",
		"article_path": "articles/a5ac47e7471f81df7432f42e73b8f57d.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/a5ac47e7471f81df7432f42e73b8f57d.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/a5ac47e7471f81df7432f42e73b8f57d.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi Leadership Shenanigans Will Just Cause Short-Term Woe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "a92d828339aa4b73d90b6bf71056be39",
		"source": "common-crawl",
		"article_path": "articles/a92d828339aa4b73d90b6bf71056be39.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/a92d828339aa4b73d90b6bf71056be39.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/a92d828339aa4b73d90b6bf71056be39.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Merkel Says She Didn’t Know About Volkswagen Emissions Cheating Until U.S. Disclosure",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "aadd5810c9e93a638ac124d8f01b3f44",
		"source": "common-crawl",
		"article_path": "articles/aadd5810c9e93a638ac124d8f01b3f44.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/aadd5810c9e93a638ac124d8f01b3f44.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/aadd5810c9e93a638ac124d8f01b3f44.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "More Volkswagen Vehicles Have Emissions Software Issues",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "aae9fd42c842ec9ece8d12d954365d0a",
		"source": "common-crawl",
		"article_path": "articles/aae9fd42c842ec9ece8d12d954365d0a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/aae9fd42c842ec9ece8d12d954365d0a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/aae9fd42c842ec9ece8d12d954365d0a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen (VLKAY) Faces $1.2B Fine for Emission Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "af5552bf0b6c6d42a41b47561b7dcf32",
		"source": "common-crawl",
		"article_path": "articles/af5552bf0b6c6d42a41b47561b7dcf32.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/af5552bf0b6c6d42a41b47561b7dcf32.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/af5552bf0b6c6d42a41b47561b7dcf32.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Faces Recall of Audi Models in Korea",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b1a6c7c1d332de95e2b487378122e683",
		"source": "common-crawl",
		"article_path": "articles/b1a6c7c1d332de95e2b487378122e683.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b1a6c7c1d332de95e2b487378122e683.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b1a6c7c1d332de95e2b487378122e683.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Executive Oliver Schmidt Sentenced To 7 Years For Dieselgate",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b226bd4c53717973166b7a3defbb9918",
		"source": "common-crawl",
		"article_path": "articles/b226bd4c53717973166b7a3defbb9918.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b226bd4c53717973166b7a3defbb9918.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b226bd4c53717973166b7a3defbb9918.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen and Audi Recall 850,000 Vehicles in U.S. Due to Air-Bag Problems",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b2dddc93bd4a3301a32bd22fc5643f75",
		"source": "common-crawl",
		"article_path": "articles/b2dddc93bd4a3301a32bd22fc5643f75.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b2dddc93bd4a3301a32bd22fc5643f75.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b2dddc93bd4a3301a32bd22fc5643f75.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Confirms Audi Cars Also Had Emissions Cheating Software",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b3211d1185d4f473cac7fe084d48b9c2",
		"source": "common-crawl",
		"article_path": "articles/b3211d1185d4f473cac7fe084d48b9c2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b3211d1185d4f473cac7fe084d48b9c2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b3211d1185d4f473cac7fe084d48b9c2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen's 'dieselgate' spreads to Bosch",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b3da555484b4b2b88564f2b3e019ac7c",
		"source": "common-crawl",
		"article_path": "articles/b3da555484b4b2b88564f2b3e019ac7c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b3da555484b4b2b88564f2b3e019ac7c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b3da555484b4b2b88564f2b3e019ac7c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen's diesel scandal costs hit $30 billion",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b85e7e60695479e36aa04da2f8707e4a",
		"source": "common-crawl",
		"article_path": "articles/b85e7e60695479e36aa04da2f8707e4a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b85e7e60695479e36aa04da2f8707e4a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b85e7e60695479e36aa04da2f8707e4a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW's emission scandal goes beyond corporate lies",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "b8a67c04e39a05e5173508c6494b113b",
		"source": "common-crawl",
		"article_path": "articles/b8a67c04e39a05e5173508c6494b113b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/b8a67c04e39a05e5173508c6494b113b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/b8a67c04e39a05e5173508c6494b113b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "GM is latest automaker accused of diesel emissions cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ba7947907f764fe4f4aaf29503142929",
		"source": "common-crawl",
		"article_path": "articles/ba7947907f764fe4f4aaf29503142929.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ba7947907f764fe4f4aaf29503142929.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ba7947907f764fe4f4aaf29503142929.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "3,72,000 German drivers join legal action against Volkswagen",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bbbc5421e32abd09fbf612e9378f726c",
		"source": "common-crawl",
		"article_path": "articles/bbbc5421e32abd09fbf612e9378f726c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bbbc5421e32abd09fbf612e9378f726c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bbbc5421e32abd09fbf612e9378f726c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Lawsuit Accuses Volkswagen’s U.S. Unit of Deleting Emissions-Related Documents",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bbcadc06c008fca0c36047ff9567042b",
		"source": "common-crawl",
		"article_path": "articles/bbcadc06c008fca0c36047ff9567042b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bbcadc06c008fca0c36047ff9567042b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bbcadc06c008fca0c36047ff9567042b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "German Prosecutors Investigating Volkswagen Employee in Emissions Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bc4c4865d66e347804151e23ae18142c",
		"source": "common-crawl",
		"article_path": "articles/bc4c4865d66e347804151e23ae18142c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bc4c4865d66e347804151e23ae18142c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bc4c4865d66e347804151e23ae18142c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Role of Defeat Devices in Environmental Protection: Beyond The VW Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bdc471858e5f5dad2a7bc0864f831f43",
		"source": "common-crawl",
		"article_path": "articles/bdc471858e5f5dad2a7bc0864f831f43.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bdc471858e5f5dad2a7bc0864f831f43.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bdc471858e5f5dad2a7bc0864f831f43.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Must Face U.S. Investor Suit Over Emissions, Judge Rules",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "be89f329b6b4a327fcd2a3633e8cf023",
		"source": "common-crawl",
		"article_path": "articles/be89f329b6b4a327fcd2a3633e8cf023.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/be89f329b6b4a327fcd2a3633e8cf023.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/be89f329b6b4a327fcd2a3633e8cf023.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Ex-CEO Martin Winterkorn Indicted in Emissions Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bf9dc4a29c5babea3e9fa71e204b040e",
		"source": "common-crawl",
		"article_path": "articles/bf9dc4a29c5babea3e9fa71e204b040e.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bf9dc4a29c5babea3e9fa71e204b040e.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bf9dc4a29c5babea3e9fa71e204b040e.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "A Volkswagen-Scale Bill for Fiat Chrysler Would Be $3.5 Billion",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "bffa1bf3bc0ea12f5773f595ecc0adc4",
		"source": "common-crawl",
		"article_path": "articles/bffa1bf3bc0ea12f5773f595ecc0adc4.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/bffa1bf3bc0ea12f5773f595ecc0adc4.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/bffa1bf3bc0ea12f5773f595ecc0adc4.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Faces Class-Action Suit in Australia Over Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c1b1f5e3d8e4bc728434f39e2d42882f",
		"source": "common-crawl",
		"article_path": "articles/c1b1f5e3d8e4bc728434f39e2d42882f.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c1b1f5e3d8e4bc728434f39e2d42882f.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c1b1f5e3d8e4bc728434f39e2d42882f.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen fined 1 billion euros by German prosecutors over emissions cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c2715ce8a6d6c28ca9d53d1502fd3a80",
		"source": "common-crawl",
		"article_path": "articles/c2715ce8a6d6c28ca9d53d1502fd3a80.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c2715ce8a6d6c28ca9d53d1502fd3a80.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c2715ce8a6d6c28ca9d53d1502fd3a80.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Nears Deal to Add Another $1 Billion to Emissions Scandal Costs",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c3adc26cf1a5599d41448e8e25ccc62a",
		"source": "common-crawl",
		"article_path": "articles/c3adc26cf1a5599d41448e8e25ccc62a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c3adc26cf1a5599d41448e8e25ccc62a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c3adc26cf1a5599d41448e8e25ccc62a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Emissions Investigation Zeroes In on Two Engineers",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c45f83a710f96ff4b19da378f2e8e95b",
		"source": "common-crawl",
		"article_path": "articles/c45f83a710f96ff4b19da378f2e8e95b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c45f83a710f96ff4b19da378f2e8e95b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c45f83a710f96ff4b19da378f2e8e95b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "FCA Settles With U.S. Regulators Over Its Own \"Dieselgate\"",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c6bb6150e82fb717a151cb05203c32bb",
		"source": "common-crawl",
		"article_path": "articles/c6bb6150e82fb717a151cb05203c32bb.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c6bb6150e82fb717a151cb05203c32bb.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c6bb6150e82fb717a151cb05203c32bb.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Former VW Executive to Plead Guilty in Emissions-Cheating Case",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c7f864652c2240d1fcbea96a5ef76404",
		"source": "common-crawl",
		"article_path": "articles/c7f864652c2240d1fcbea96a5ef76404.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c7f864652c2240d1fcbea96a5ef76404.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c7f864652c2240d1fcbea96a5ef76404.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW says will pay 1 bn euro German fine over emissions cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "c902514c3afc9e9733ab278079a1b46a",
		"source": "common-crawl",
		"article_path": "articles/c902514c3afc9e9733ab278079a1b46a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/c902514c3afc9e9733ab278079a1b46a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/c902514c3afc9e9733ab278079a1b46a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen CEO Targeted in Emissions-Cheating Probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "cab91d7f0c235d61a3ac84b040f0e6bf",
		"source": "common-crawl",
		"article_path": "articles/cab91d7f0c235d61a3ac84b040f0e6bf.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/cab91d7f0c235d61a3ac84b040f0e6bf.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/cab91d7f0c235d61a3ac84b040f0e6bf.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Risk Report: Petrobras Highlights Class-Action Risks in Corruption Cases",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "cc32efc4c0ab1bdef7fd86c7daab39a6",
		"source": "common-crawl",
		"article_path": "articles/cc32efc4c0ab1bdef7fd86c7daab39a6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/cc32efc4c0ab1bdef7fd86c7daab39a6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/cc32efc4c0ab1bdef7fd86c7daab39a6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "White-Collar Crimes -- Motivations and Triggers",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "cc60341d47cdfc44b77c22fda82e4bf9",
		"source": "common-crawl",
		"article_path": "articles/cc60341d47cdfc44b77c22fda82e4bf9.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/cc60341d47cdfc44b77c22fda82e4bf9.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/cc60341d47cdfc44b77c22fda82e4bf9.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Germany hits Mercedes with mass diesel recall",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "cde49c8c80d600857c711e6956721912",
		"source": "common-crawl",
		"article_path": "articles/cde49c8c80d600857c711e6956721912.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/cde49c8c80d600857c711e6956721912.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/cde49c8c80d600857c711e6956721912.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Hedge Funds Deliver Evidence in Porsche Executives Trial",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ce4e9b63442908ca579762c25150192c",
		"source": "common-crawl",
		"article_path": "articles/ce4e9b63442908ca579762c25150192c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ce4e9b63442908ca579762c25150192c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ce4e9b63442908ca579762c25150192c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW Officials Meet Over Investigation as Crisis Hits Operations",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d2f202ae0e189b860dfbe825b08a9290",
		"source": "common-crawl",
		"article_path": "articles/d2f202ae0e189b860dfbe825b08a9290.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d2f202ae0e189b860dfbe825b08a9290.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d2f202ae0e189b860dfbe825b08a9290.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Ford sued by truck owners alleging diesel emissions cheating",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d33cb4bac0b4cdd9a5ec5b5c2e7dadb1",
		"source": "common-crawl",
		"article_path": "articles/d33cb4bac0b4cdd9a5ec5b5c2e7dadb1.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d33cb4bac0b4cdd9a5ec5b5c2e7dadb1.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d33cb4bac0b4cdd9a5ec5b5c2e7dadb1.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Wyoming Can’t Sue VW Over Diesel Emissions, Judge Says",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d41eefc6c39cc443983dbdbb12970b03",
		"source": "common-crawl",
		"article_path": "articles/d41eefc6c39cc443983dbdbb12970b03.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d41eefc6c39cc443983dbdbb12970b03.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d41eefc6c39cc443983dbdbb12970b03.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Report on Volkswagen’s Changes Since Emissions Scandal Cites Lack of Transparency",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d52a7f878dc62c68738fb8fdea9bec13",
		"source": "common-crawl",
		"article_path": "articles/d52a7f878dc62c68738fb8fdea9bec13.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d52a7f878dc62c68738fb8fdea9bec13.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d52a7f878dc62c68738fb8fdea9bec13.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Norway Oil Fund to Sue Volkswagen Over Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d661e91e0dba9c7d5187abef116d0fa5",
		"source": "common-crawl",
		"article_path": "articles/d661e91e0dba9c7d5187abef116d0fa5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d661e91e0dba9c7d5187abef116d0fa5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d661e91e0dba9c7d5187abef116d0fa5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Business Watch",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d7388fb511dbcc761e75b0c62866f35d",
		"source": "common-crawl",
		"article_path": "articles/d7388fb511dbcc761e75b0c62866f35d.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d7388fb511dbcc761e75b0c62866f35d.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d7388fb511dbcc761e75b0c62866f35d.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Risk Report: Rabobank to Pay Up Over Compliance End Run",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d7dec5b0b9118122da5148f292708923",
		"source": "common-crawl",
		"article_path": "articles/d7dec5b0b9118122da5148f292708923.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d7dec5b0b9118122da5148f292708923.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d7dec5b0b9118122da5148f292708923.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "What the Arrest of Audi's CEO Means for Volkswagen Stock",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d96380300313f0f356365be9dbe27021",
		"source": "common-crawl",
		"article_path": "articles/d96380300313f0f356365be9dbe27021.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d96380300313f0f356365be9dbe27021.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d96380300313f0f356365be9dbe27021.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen to Brits: We'll pay big bucks for your diesel clunker",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "d9fdf25477e564a75dc3a5b2b9146028",
		"source": "common-crawl",
		"article_path": "articles/d9fdf25477e564a75dc3a5b2b9146028.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/d9fdf25477e564a75dc3a5b2b9146028.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/d9fdf25477e564a75dc3a5b2b9146028.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Fiat Chrysler Automobiles Settles Its Own Diesel Scandal -- for Now",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "dc26c0b8e86e660aa767073c6f913475",
		"source": "common-crawl",
		"article_path": "articles/dc26c0b8e86e660aa767073c6f913475.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/dc26c0b8e86e660aa767073c6f913475.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/dc26c0b8e86e660aa767073c6f913475.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Risk Report: Quick Use of North Korea Sanctions Helps Banks",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "dc9cc529d60a64a598aeef61e03a162e",
		"source": "common-crawl",
		"article_path": "articles/dc9cc529d60a64a598aeef61e03a162e.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/dc9cc529d60a64a598aeef61e03a162e.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/dc9cc529d60a64a598aeef61e03a162e.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Reaches Antitrust Settlement With Canada Over Emissions Claims",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e09c3d57a761c80cf765cac5244392c7",
		"source": "common-crawl",
		"article_path": "articles/e09c3d57a761c80cf765cac5244392c7.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e09c3d57a761c80cf765cac5244392c7.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e09c3d57a761c80cf765cac5244392c7.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Fined $1.17 Billion in Germany in Emissions-Cheating Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e125876401b9e5d4de83532f5fa66e1c",
		"source": "common-crawl",
		"article_path": "articles/e125876401b9e5d4de83532f5fa66e1c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e125876401b9e5d4de83532f5fa66e1c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e125876401b9e5d4de83532f5fa66e1c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "U.S. grand jury indicts four Audi managers in VW emissions probe",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e214f29ede517d0fa5f1a92bbb20b306",
		"source": "common-crawl",
		"article_path": "articles/e214f29ede517d0fa5f1a92bbb20b306.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e214f29ede517d0fa5f1a92bbb20b306.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e214f29ede517d0fa5f1a92bbb20b306.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW's emission scandal goes beyond corporate lies",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e21f3bfdffa0404c8afc3360d1ee522a",
		"source": "common-crawl",
		"article_path": "articles/e21f3bfdffa0404c8afc3360d1ee522a.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e21f3bfdffa0404c8afc3360d1ee522a.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e21f3bfdffa0404c8afc3360d1ee522a.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Diesel emissions scandal: Volkswagen to pay $1.2 billion fine imposed by German prosecutors",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e38e4a9a21c16bdc99b1e070a5725ba5",
		"source": "common-crawl",
		"article_path": "articles/e38e4a9a21c16bdc99b1e070a5725ba5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e38e4a9a21c16bdc99b1e070a5725ba5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e38e4a9a21c16bdc99b1e070a5725ba5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Court Denies Former VW Compliance Executive’s Bid to Overturn Decision Keeping Him Imprisoned",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e39da8b1114177ef16f17d17378bec5b",
		"source": "common-crawl",
		"article_path": "articles/e39da8b1114177ef16f17d17378bec5b.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e39da8b1114177ef16f17d17378bec5b.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e39da8b1114177ef16f17d17378bec5b.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Agrees to $1.2 Billion Compensation for U.S. Dealers",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e4d832694d198c85e54c69c183283e99",
		"source": "common-crawl",
		"article_path": "articles/e4d832694d198c85e54c69c183283e99.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e4d832694d198c85e54c69c183283e99.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e4d832694d198c85e54c69c183283e99.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Korea Chief Apologizes Over Emissions Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e61d633f768094d500157950ac781cb2",
		"source": "common-crawl",
		"article_path": "articles/e61d633f768094d500157950ac781cb2.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e61d633f768094d500157950ac781cb2.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e61d633f768094d500157950ac781cb2.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Germany Arrests, U.S. Indicts Fired Audi Dieselgate Engineer",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e6d44c979d8d7922f92facc43f4cfeb7",
		"source": "common-crawl",
		"article_path": "articles/e6d44c979d8d7922f92facc43f4cfeb7.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e6d44c979d8d7922f92facc43f4cfeb7.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e6d44c979d8d7922f92facc43f4cfeb7.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "US judge gives final go-ahead to 3.0-litre Volkswagen diesel settlement",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e8c72dd18145bd314c9ff21a5258d9e6",
		"source": "common-crawl",
		"article_path": "articles/e8c72dd18145bd314c9ff21a5258d9e6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e8c72dd18145bd314c9ff21a5258d9e6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e8c72dd18145bd314c9ff21a5258d9e6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Says Diesel-Buyback Program Is Well Ahead of Schedule",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "e918dbc6eaa3b4749d2686988b22913d",
		"source": "common-crawl",
		"article_path": "articles/e918dbc6eaa3b4749d2686988b22913d.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/e918dbc6eaa3b4749d2686988b22913d.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/e918dbc6eaa3b4749d2686988b22913d.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Germany Set to Allow Collective Lawsuits Following VW Scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ea23dd887c2a81af879ed3ed54fd3ffd",
		"source": "common-crawl",
		"article_path": "articles/ea23dd887c2a81af879ed3ed54fd3ffd.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ea23dd887c2a81af879ed3ed54fd3ffd.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ea23dd887c2a81af879ed3ed54fd3ffd.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "How Volkswagen Rallied Its Employees After Its Emissions Scandal (At Least For Now)",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "eb04eb3c4cc98beca2faa9f685540c52",
		"source": "common-crawl",
		"article_path": "articles/eb04eb3c4cc98beca2faa9f685540c52.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/eb04eb3c4cc98beca2faa9f685540c52.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/eb04eb3c4cc98beca2faa9f685540c52.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "The Morning Risk Report: U.K. Regulator Makes Play to Retain State Company Listings",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "eb2b2154046eb787d2dd3eaba03d2be7",
		"source": "common-crawl",
		"article_path": "articles/eb2b2154046eb787d2dd3eaba03d2be7.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/eb2b2154046eb787d2dd3eaba03d2be7.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/eb2b2154046eb787d2dd3eaba03d2be7.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Assesses Emissions Scandal’s Impact on Its Finances",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ecc6d2ac88f16af21b3da4fcd4cab78c",
		"source": "common-crawl",
		"article_path": "articles/ecc6d2ac88f16af21b3da4fcd4cab78c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ecc6d2ac88f16af21b3da4fcd4cab78c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ecc6d2ac88f16af21b3da4fcd4cab78c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Says Carbon Deviations Much Smaller Than Suspected",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "efc37892108e32522e924cd273162e0f",
		"source": "common-crawl",
		"article_path": "articles/efc37892108e32522e924cd273162e0f.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/efc37892108e32522e924cd273162e0f.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/efc37892108e32522e924cd273162e0f.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Jailed Audi CEO Steps Aside While He Tries to Clear His Name",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "f358c127443e52bfbead2e11732e92c8",
		"source": "common-crawl",
		"article_path": "articles/f358c127443e52bfbead2e11732e92c8.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/f358c127443e52bfbead2e11732e92c8.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/f358c127443e52bfbead2e11732e92c8.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Head of VW's Audi arrested in Germany over diesel scandal",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "f723a4189eb13fc89cc7c66533380802",
		"source": "common-crawl",
		"article_path": "articles/f723a4189eb13fc89cc7c66533380802.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/f723a4189eb13fc89cc7c66533380802.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/f723a4189eb13fc89cc7c66533380802.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi Finds Illegal Emissions Software in Some Diesel Vehicles",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "f79d7fd4fdd512862bcc08f5995b089c",
		"source": "common-crawl",
		"article_path": "articles/f79d7fd4fdd512862bcc08f5995b089c.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/f79d7fd4fdd512862bcc08f5995b089c.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/f79d7fd4fdd512862bcc08f5995b089c.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi ordered to recall 127,000 vehicles over emissions: paper",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "f9c2542c2e7ed8b731ca698faed9fe15",
		"source": "common-crawl",
		"article_path": "articles/f9c2542c2e7ed8b731ca698faed9fe15.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/f9c2542c2e7ed8b731ca698faed9fe15.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/f9c2542c2e7ed8b731ca698faed9fe15.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Daimler Plans Emissions Modifications on More Than Three Million Vehicles",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "fb3f1aa7fc9c7c73f0d659afdf86b2e6",
		"source": "common-crawl",
		"article_path": "articles/fb3f1aa7fc9c7c73f0d659afdf86b2e6.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/fb3f1aa7fc9c7c73f0d659afdf86b2e6.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/fb3f1aa7fc9c7c73f0d659afdf86b2e6.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Audi CEO Arrested in Emissions-Cheating Investigation",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "fb87440be8ec7c779bda221a884f8481",
		"source": "common-crawl",
		"article_path": "articles/fb87440be8ec7c779bda221a884f8481.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/fb87440be8ec7c779bda221a884f8481.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/fb87440be8ec7c779bda221a884f8481.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Did GM Get a Pass from Federal Prosecutors?",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "fbdbd31f60af4eeff43d14338ed900c0",
		"source": "common-crawl",
		"article_path": "articles/fbdbd31f60af4eeff43d14338ed900c0.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/fbdbd31f60af4eeff43d14338ed900c0.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/fbdbd31f60af4eeff43d14338ed900c0.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "VW’s CEO Knows the Future Is Electric—His Company Isn’t So Sure",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "fcdd88603d25a814c5145f69691394f4",
		"source": "common-crawl",
		"article_path": "articles/fcdd88603d25a814c5145f69691394f4.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/fcdd88603d25a814c5145f69691394f4.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/fcdd88603d25a814c5145f69691394f4.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "Volkswagen Manager Sentenced to Prison Term in Emissions-Fraud Case",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "fef811c15e16c973f6deb322a173e1d5",
		"source": "common-crawl",
		"article_path": "articles/fef811c15e16c973f6deb322a173e1d5.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/fef811c15e16c973f6deb322a173e1d5.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/fef811c15e16c973f6deb322a173e1d5.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "dieselgate: Volkswagen fights investors as diesel-scandal cost could top $35 billion",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ff89872ba2f65daf9dbff7a0564e0ade",
		"source": "common-crawl",
		"article_path": "articles/ff89872ba2f65daf9dbff7a0564e0ade.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ff89872ba2f65daf9dbff7a0564e0ade.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ff89872ba2f65daf9dbff7a0564e0ade.json"
	},
	{
		"search_query": "volkswagen",
		"entity_id": "v1",
		"crawled": "N/A",
		"article_title": "EU raids Daimler and VW in widening cartel inquiry",
		"event_id": "-1",
		"query_id": "360d87027cf05c23880b353792f8c594",
		"uuid": "ffcf7e40e176e5adfb028d413057d4c8",
		"source": "common-crawl",
		"article_path": "articles/ffcf7e40e176e5adfb028d413057d4c8.json",
		"classification_1": "Financial Crime",
		"is_related": "False",
		"content_file_name": "360d87027cf05c23880b353792f8c594/classified/ffcf7e40e176e5adfb028d413057d4c8.json",
		"view_article_path": "https://yll0tnzxzi.execute-api.eu-west-1.amazonaws.com/Prod/v1/get-article?file_name=360d87027cf05c23880b353792f8c594/classified/ffcf7e40e176e5adfb028d413057d4c8.json"
	}
])
.constant('utilityConstant', {
	conplianceMapKeys : [
		{
			'key': 'hasActivityStatus',
			'value': 'Company Status',
			'class': 'fa-check-square',
			'texts': '',
			'edit': false,
			'title': 'Registered Company Status Sources'
		},
		{
			'key': 'bst:aka',
			'value': 'Alias Name',
			'class': ' fa-building-o',
			'texts': '',
			'edit': false,
			'title': 'Registered Alias Name Sources'
		},
		{
			'key': 'fullAddress',
			'value': 'Registered Address',
			'class': 'fa-map-marker',
			'texts': '',
			'edit': false,
			'title': 'Registered Address Sources'
		},
		{
			'key': 'streetAddress',
			'value': 'Street Address',
			'class': 'fa-road',
			'texts': '',
			'edit': false,
			'title': 'Registered street Address Sources'
		}
		, {
			'key': 'city',
			'value': 'City',
			'class': 'fa-city',
			'texts': '',
			'edit': false,
			'title': 'Registered City Sources'
		},
		{
			'key': 'zip',
			'value': 'Postal Code',
			'class': 'fa-map-signs',
			'texts': '',
			'edit': false,
			'title': 'Registered Postal Code Sources'
		},
		{
			'key': 'country',
			'value': 'Country',
			'class': 'fa-globe',
			'texts': '',
			'edit': false,
			'title': 'Registered Country Sources'
		},
		{
			'key': 'industryType',
			'value': 'Industry-Type',
			'class': 'fa-industry',
			'texts': '',
			'edit': false,
			'title': 'Registed Industry-Type Sources'
		},
		{
			'key': 'lei:legalForm',
			'value': 'Legal Type',
			'class': 'fa-briefcase',
			'texts': '',
			'edit': false,
			'title': 'Registed Legal Type Sources'
		},
		{
			'key': 'main_exchange',
			'value': 'Stock Exchange',
			'class': 'fa-area-chart',
			'texts': '',
			'edit': false,
			'title': 'Registered Stock Exchange Sources'
		},
		{
			'key': 'ticket_symbol',
			'value': 'Stock Ticker',
			'class': 'fa-tag',
			'texts': '',
			'edit': false,
			'title': 'Registered Stock Ticker In Sources'
		},
		{
			'key': 'isIncorporatedIn',
			'value': 'Date of Incorporation',
			'class': 'fa-calendar-o',
			'texts': '',
			'edit': false,
			'title': 'Registed Date of Incorporation Sources'
		},
		{
			'key': 'hasIPODate',
			'value': 'IPO Date',
			'class': 'fa-calendar-o',
			'texts': '',
			'edit': false,
			'title': 'Registered IPO Date Sources'
		},
		{
			'key': 'tr-org:hasRegisteredPhoneNumber',
			'value': 'Registered Phone Number',
			'class': 'fa-phone',
			'texts': '',
			'edit': false,
			'title': 'Registered Phone Number Sources'
		},
		{
			'key': 'hasRegisteredFaxNumber',
			'value': 'Registered Fax Number',
			'class': 'fa-fax',
			'texts': '',
			'edit': false,
			'title': 'Registered Fax Number Sources'
		},
		{
			'key': 'tr-org:hasHeadquartersPhoneNumber',
			'value': 'Headquater Phone Number',
			'class': 'fa-phone',
			'texts': '',
			'edit': false,
			'title': 'Registered Headquater Phone Number Sources'
		},
		{
			'key': 'tr-org:hasHeadquartersFaxNumber',
			'value': 'Headquarters Fax Number',
			'class': 'fa-fax',
			'texts': '',
			'edit': false,
			'title': 'Registered Headquarters Fax Number Sources'
		},
		{
			'key': 'isDomiciledIn',
			'value': 'Domiciled In',
			'class': 'fa-home',
			'texts': '',
			'edit': false,
			'title': 'Registered Domiciled In Sources'
		},

		{
			'key': 'hasURL',
			'value': 'Website',
			'class': 'fa-link',
			'texts': '',
			'edit': false,
			'title': 'Registered Website In Sources'
		},
		{
			'key': 'RegulationStatus',
			'value': 'Regulation Status',
			'class': 'fa-gavel',
			'texts': '',
			'edit': false,
			'title': 'Registered Regulation Status In Sources'
		},
		{
			'key': 'RegulationStatusEffectiveDate',
			'value': 'Regulation Status Effective Date',
			'class': 'fa-calendar',
			'texts': '',
			'edit': false,
			'title': 'Registered Regulation Status Effective Date In Sources'
		}

	],
	conplianceIdentifiers :[
		{
			'key': 'vat_tax_number',
			'value': 'VAT/TIN',
			'class': 'VAT',
			'texts': '',
			'edit': false,
			'title': 'Registered VAT/TIN Sources'
		},
		{
			'key': 'legal_entity_identifier',
			'value': 'LEI',
			'class': 'LEI',
			'texts': '',
			'edit': false,
			'title': 'Registered LEI Sources'
		},
		{
			'key': 'trade_register_number',
			'value': 'Trade/Commerce',
			'class': 'fa-shopping-cart',
			'texts': '',
			'edit': false,
			'title': 'Registered Trade/Commerce Sources'
		},
		{
			'key': 'international_securities_identifier',
			'value': 'International Securities Identification Number',
			'class': 'fa-barcode',
			'texts': '',
			'edit': false,
			'title': 'Registered International Securities Identification Number Sources'
		},
		{
			'key': 'swift_code',
			'value': 'BIC / Swift Codes',
			'class': 'fa-barcode',
			'texts': '',
			'edit': false,
			'title': 'Registered BIC / Swift Codes Sources'
		},
	],
	conplianceMapKeysReport : [{
		'key': 'vcard:organization-name',
		'value': 'Legal Name',
		'class': 'fa-check-square',
		'texts': '',
		'edit': false,
		'title': 'Registered Legal Name Sources',
		'showSource': false
	},
	{
		'key': 'bst:aka',
		'value': 'Alias Name',
		'class': ' fa-building-o',
		'texts': '',
		'edit': false,
		'title': 'Registered Alias Name Sources',
		'showSource': true
	},
	{
		'key': 'hasActivityStatus',
		'value': 'Company Status',
		'class': 'fa-check-square',
		'texts': '',
		'edit': false,
		'title': 'Registered Company Status Sources',
		'showSource': true
	},
	{
		'key': 'hasURL',
		'value': 'Company Website',
		'class': 'fa-link',
		'texts': '',
		'edit': false,
		'title': 'Registered Website In Sources',
		'showSource': true
	},
	{
		'key': 'fullAddress',
		'value': 'Registered Address',
		'class': 'fa-map-marker',
		'texts': '',
		'edit': false,
		'title': 'Registered Address Sources',
		'showSource': true
	},
	{
		'key': 'lei:legalForm',
		'value': 'Legal Form',
		'class': 'fa-briefcase',
		'texts': '',
		'edit': false,
		'title': 'Registed Legal Form Sources',
		'showSource': true
	},
	{
		'key': 'zip',
		'value': 'ZIP/Postal Code',
		'class': 'fa-map-signs',
		'texts': '',
		'edit': false,
		'title': 'Registered Postal Code Sources',
		'showSource': true
	},
	{
		'key': 'main_exchange',
		'value': 'Listed on Stock Exchange',
		'class': 'fa-area-chart',
		'texts': '',
		'edit': false,
		'title': 'Registered Stock Exchange Sources',
		'showSource': true
	},
	{
		'key': 'country',
		'value': 'Country',
		'class': 'fa-globe',
		'texts': '',
		'edit': false,
		'title': 'Registered Country Sources',
		'showSource': true
	},
	{
		'key': 'ticket_symbol',
		'value': 'Ticker Code',
		'class': 'fa-tag',
		'texts': '',
		'edit': false,
		'title': 'Registered  Ticker Code In Sources',
		'showSource': true
	},
	{
		'key': 'vat_tax_number',
		'value': 'VAT/TIN',
		'class': 'VAT',
		'texts': '',
		'edit': false,
		'title': 'Registered VAT/TIN Sources',
		'showSource': true
	},
	{
		'key': 'legal_entity_identifier',
		'value': 'LEI',
		'class': 'LEI',
		'texts': '',
		'edit': false,
		'title': 'Registered LEI Sources',
		'showSource': true
	},
	{
		'key': 'trade_register_number',
		'value': 'Trade Registration',
		'class': 'fa-shopping-cart',
		'texts': '',
		'edit': false,
		'title': 'Registered Trade Registration# Sources',
		'showSource': true
	},
	{
		'key': 'industryType',
		'value': 'Industry',
		'class': 'fa-industry',
		'texts': '',
		'edit': false,
		'title': 'Registed Industry-Type Sources',
		'showSource': true
	},
	{
		'key': 'isIncorporatedIn',
		'value': 'Date of Incorporation',
		'class': 'fa-calendar-o',
		'texts': '',
		'edit': false,
		'title': 'Registed Date of Incorporation Sources',
		'showSource': true
	},
	{
		'key': 'swift_code',
		'value': 'BIC / Swift Codes',
		'class': 'fa-barcode',
		'texts': '',
		'edit': false,
		'title': 'Registered BIC / Swift Codes Sources',
		'showSource': true
	},
	{
		'key': '',
		'value': '',
		'class': '',
		'texts': '',
		'edit': false,
		'title': '',
		'showSource': false
	},
	{
		'key': 'RegulationStatus',
		'value': 'Regulation Status',
		'class': 'fa-gavel',
		'texts': '',
		'edit': false,
		'title': 'Registered Regulation Status In Sources',
		'showSource': true
	},
	{
		'key': '',
		'value': '',
		'class': '',
		'texts': '',
		'edit': false,
		'title': '',
		'showSource': false
	},
	{
		'key': 'RegulationStatusEffectiveDate',
		'value': 'Regulation Status Effective Date',
		'class': 'fa-calendar',
		'texts': '',
		'edit': false,
		'title': 'Registered Regulation Status Effective Date In Sources',
		'showSource': true
	}],
	reportv2Data:{
		['CDD_Process_and_CDD_Results_Fields'] : [],
		['information_recevied_from_ing'] :[],
		['party_information'] :[],
		['Party_Address_Details'] :[],
		['Registration_and_TAX_Details'] :[],
		['outreach_required'] :'',
		['level_due_diligence'] :'',
		['mlro_management_approval'] :'',
		['sourceOfFundOutreach'] :'',
		['Associated_Parties_QuesAns'] :{},
		['main_priociples_data'] :[],
		['sourceOfFundOutreachInfo'] :'',
		['regulated_by'] :'',
		['stock_exchange'] :'',
		['ticker_code'] :'',
		['legal_ultimate_parent'] :'',
		['#UHRC'] :{
			selected: '',
			values: []
		},
		['#Party/Customer Product Information'] :[],
		['#Party/Customer Activity – High Risk'] :[],

		['#Party/Customer_Product_InformationQuestionAns'] :'No',
		['#Party/Customer_Activity_High_RiskQuestionAns'] :'No',
		['#UBO_Identified_in_ownership'] :'No',
		['#Main_principals_question'] :'No',
		['#Principals_question'] :'No',
		['#screeing_question'] :'No',
		['#RiskRating'] :'',
		['#CountryRisk'] :'',
		['#Trade_Registration_No'] :''
	}
	 
	
});