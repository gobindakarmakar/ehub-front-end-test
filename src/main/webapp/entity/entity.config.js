'use strict';

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', 'FlashProvider','FLASH_ALERT_DURATION','FLASH_ALERT_CLOSE',function($stateProvider, $provide, $urlRouterProvider, EHUB_FE_API, $locationProvider,FlashProvider,FLASH_ALERT_DURATION,FLASH_ALERT_CLOSE){
	  
	   $provide.factory('$stateProvider', function () {
			 return $stateProvider;
		 });
	 	$provide.factory('$urlRouterProvider', function () {
	        return $urlRouterProvider;
	    });
		
		
//		FlashProvider.setTimeout(FLASH_ALERT_DURATION);
		FlashProvider.setShowClose(FLASH_ALERT_CLOSE);
		
	  
}]);