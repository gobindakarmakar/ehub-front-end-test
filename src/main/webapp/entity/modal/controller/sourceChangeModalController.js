angular.module('ehubEntityApp')
		.controller('sourceChangeModalController', sourceChangeModalController);

sourceChangeModalController.$inject = [
	'$scope',
	'$uibModalInstance'
];

function sourceChangeModalController(
	$scope,
	$uibModalInstance){
	
	console.log("controller");
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.cancel = function(){
 		$uibModalInstance.close(false);
     };
 	$scope.closeWidgetCaptureModal = function(){
        $uibModalInstance.close(true);
     };
     
} 