angular.module('ehubEntityApp')
		.controller('addofficerModalController', addofficerModalController);

addofficerModalController.$inject = [
	'$scope',
	'$http',
	'$uibModalInstance'
];

function addofficerModalController(
	$scope,
	$http,
	$uibModalInstance){
	
	console.log("controller");
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
} 