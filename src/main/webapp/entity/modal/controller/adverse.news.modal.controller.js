"use strict";
angular.module('ehubEntityApp')
	.controller('adverseModalController', adverseModalController);
adverseModalController.$inject = [
	'$scope',
	'$http',
	'$timeout',
	'selectedEntity',
	'header',
	'$rootScope',
	'$uibModalInstance',
	'$uibModal',
	'selectedindex',
	'showMainName',
	'EntityApiService',
	'type',
	'summaryScreenig',
	'dummyNews',
	'filteredChart'
];

function adverseModalController(
	$scope,
	$http,
	$timeout,
	selectedEntity,
	header,
	$rootScope,
	$uibModalInstance,
	$uibModal,
	selectedindex,
	showMainName,
	EntityApiService,
	type,
	summaryScreenig,
	dummyNews,
	filteredChart) {
	/*Initializing scope variables*/
	$scope.articles = [];
	$scope.store_leftsideTilelist = [];
	$scope.leftsideTilelist = [];
	$scope.firstOpenedArticle = {
		isSignificant: false,
		significantTitle: 'Save news as significant',
		showMainName: showMainName,
		classificationFilter: [],
		filterEntites:'',
		entityDropdown:[],
		entitySelected:''
	};
	$scope.makeInsignificantComment = {
		isSignificant: false,
		typeScreening:"",
		screenedData:""
	};
	$scope.dummyNews = dummyNews;
	$scope.adverseNewsmodalObject = {
		clusteredIndex : -1
	};
	console.log(summaryScreenig);
	$scope.commentedListLength =0;
	$scope.commentedList =[];
	$scope.typeIs =type;
	var changeSignificance = [];
	$scope.sourceType = 'ALL';
	$scope.mySignificant = false;
	$scope.ErrorMessage = false;
	$scope.Selectedheadingarrow = 0;
	$scope.sentimentpopover= false;
	$scope.toHideShowQuestionIcon = false;
	$scope.actualSorted_Data = [];
	$scope.leftsideTilelist = [];
	$scope.hightLightrow ={};
	$scope.selectedEntityjurisdiction = {};
	$scope.showHideArticleQuestions = false;
	$scope.showSlider = {
		pepSlider : (type == 'pep') ? true : false,
		sactionSlider : (type == 'sanction') ? true : false
	};
	$scope.filtered = {
		pep : filteredChart.pep_slider_min,
		sanction: filteredChart.sanction_slider_min
	};
	var filter_type = type;
	var presentMArkEntites =[];
	var fixedMarkedEntites =[];
	$scope.totalScreeingresults = {
		financeCount:0,
		pepsCount :0,
		sanctionsCount:0,
		adverseCounts:0,
		jurisdictionCounts:0,

	};
	$scope.summaryFilterBadges ={
		adverse:'',
		finance:'',
		highRisk:'',
		pep:'',
		sanction:''
	};
	var leftSidePanelEntities= [];
	var entityDropdown =[];
	$scope.riskOverviewLoader = true;
	summaryScreenig.forEach(function(summary){
		leftSidePanelEntities.push({
			name:summary.name.toLowerCase(),
			key:summary.name,
			identifier:summary['@identifier'],
			financeCrime: (summary.finance_Crime_url && summary.finance_Crime_url.length > 0) ?'finance':'',
			adverseNews : (summary.adverseNews_url && summary.adverseNews_url.length > 0) ? 'adverse' : '',
			jurisdiction: (summary.high_risk_jurisdiction && summary.high_risk_jurisdiction.length !='LOW') ? 'highRisk' : '',
			pep_url:(summary.pep_url && summary.pep_url.length > 0) ? 'pep':'',
			sanction  : (summary['sanction_bst:description'] && summary['sanction_bst:description'].length > 0) ? 'sanction':'',
			adverse_article : (summary.adverseNews_url && summary.adverseNews_url.length > 0 &&  summary.adverseNews_url[0].article) ? summary.adverseNews_url[0].article : {},
			self:summary,
			finance_article : (summary.finance_Crime_url && summary.finance_Crime_url.length > 0 &&  summary.finance_Crime_url[0].article) ? summary.finance_Crime_url[0].article : {},
			values:[],
			financeValues : [],
			pep_url_values : (summary.pep_url && summary.pep_url.length > 0) ? summary.pep_url :[],
			sanction_url_values: (summary['sanction_bst:description'] && summary['sanction_bst:description'].length > 0) ? summary['sanction_bst:description'] : []
		});
		$scope.totalScreeingresults.adverseCounts += (summary.adverseNews_url && summary.adverseNews_url.length > 0 &&  summary.adverseNews_url[0].count) ? summary.adverseNews_url[0].count :0;
		$scope.totalScreeingresults.financeCount += (summary.finance_Crime_url && summary.finance_Crime_url.length > 0 &&  summary.finance_Crime_url[0].count) ? summary.finance_Crime_url[0].count :0;
		$scope.totalScreeingresults.jurisdictionCounts += (summary.high_risk_jurisdiction && summary.high_risk_jurisdiction.length !='LOW') ? 1: 0;
		$scope.totalScreeingresults.pepsCount += (summary.pep_url &&  summary.pep_url.length > 0) ? summary.pep_url.length : 0;
		$scope.totalScreeingresults.sanctionsCount+=summary['sanction_bst:description'].length > 0 ? summary['sanction_bst:description'].length :0;
		if(summary.Ubo_ibo){
			entityDropdown.push({name : summary.Ubo_ibo});
		}
		$scope.firstOpenedArticle.entityDropdown = _.uniqBy(entityDropdown, 'name');
	});
		$scope.totalScreeingresults.adverseCounts = Number($scope.totalScreeingresults.adverseCounts).formatAmt(0); 
		$scope.totalScreeingresults.financeCount  = Number($scope.totalScreeingresults.financeCount).formatAmt(0); 
		$scope.totalScreeingresults.jurisdictionCounts = Number($scope.totalScreeingresults.jurisdictionCounts).formatAmt(0);
		$scope.totalScreeingresults.pepsCount = Number($scope.totalScreeingresults.pepsCount).formatAmt(0); 
		$scope.totalScreeingresults.sanctionsCount =  Number($scope.totalScreeingresults.sanctionsCount).formatAmt(0);
	var ScreeningIndex = -1;
	if(selectedEntity['@identifier'] && selectedEntity['@identifier']){
		ScreeningIndex = _.findIndex(leftSidePanelEntities, {
				"identifier": selectedEntity['@identifier']
			});
	}
	 
	if(ScreeningIndex !== -1){
		$scope.selectedSummary = leftSidePanelEntities[ScreeningIndex];
	}else{
		$scope.selectedSummary = leftSidePanelEntities[0];
	}
	// $scope.leftsideTilelist = leftSidePanelEntities;
	$scope.leftsideTilelist = jQuery.extend(true, [], leftSidePanelEntities);
	$scope.firstOpenedArticle.filterEntites = !selectedindex ?  ($scope.selectedSummary && $scope.selectedSummary.name ?  $scope.selectedSummary.name  : ''):  ''; 
	$scope.summaryFilterBadges[type] = type;
	if (type === 'adverse' || type === 'finance') {
		getEntityArticles($scope.selectedSummary);
	} else if (type === 'pep') {
		$scope.pepDetails = $scope.selectedSummary.pep_url_values[0];
		$scope.pepDetails.name = $scope.selectedSummary.key;
		$scope.pepDetails.pep =$scope.pepDetails.pep?$scope.pepDetails.pep:false;
		$scope.pepDetails.identifier = ( !$scope.selectedSummary.self.Ubo_ibo ||  $scope.selectedSummary.self.Ubo_ibo !== 'officers') ? $scope.selectedSummary.identifier  : '';
		$scope.hightLightrow = $scope.pepDetails;
		var entries = jQuery.extend(true, [], $scope.pepDetails.entries);
		$scope.pepDetails.entries = [];
				angular.forEach(entries,function(d,key){
					if(typeof(d.attribute_name) === 'object'){
						angular.forEach(d.attribute_name,function(attrname,i1){
							$scope.pepDetails.entries.push({
								watchlist_id:d.watchlist_id,
								entry_id :  d.entry_id,
								attribute_name :[attrname]
							});
						});
					}
				});
	$timeout(function () {
			$('.scroll-body').mCustomScrollbar({
				axis: "y"
			});
		},500);
		
	} else if (type === "sanction") {
		if($scope.selectedSummary.sanction_url_values.length){
			$scope.pepDetails = $scope.selectedSummary.sanction_url_values[0];
			$scope.pepDetails.name = $scope.selectedSummary.key;
			$scope.pepDetails.sanction =$scope.pepDetails.sanction?$scope.pepDetails.sanction:false;
			$scope.pepDetails.identifier = ( !$scope.selectedSummary.self.Ubo_ibo ||  $scope.selectedSummary.self.Ubo_ibo !== 'officers') ? $scope.selectedSummary.identifier  : '';
			$scope.hightLightrow = $scope.pepDetails;
			var entries = jQuery.extend(true, [], $scope.pepDetails.entries);
				$scope.pepDetails.entries = [];
				angular.forEach(entries,function(d,key){
					if(typeof(d.attribute_name) === 'object'){
						angular.forEach(d.attribute_name,function(attrname,i1){
							$scope.pepDetails.entries.push({
								watchlist_id:d.watchlist_id,
								entry_id :  d.entry_id,
								attribute_name :[attrname]
							});
						});
					}
				});
			$timeout(function () {
				$('.scroll-body').mCustomScrollbar({
					axis: "y"
				});
			},500);
		}
	} else if (type === "highRisk") {
		$scope.selectedEntityjurisdiction = $scope.selectedSummary;
		$scope.hightLightrow = $scope.selectedEntityjurisdiction;
	}
	/* @purpose: on modal open calls the 2 API get article API and Article details
	 * @created: 25 feb  2019
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	//onPageLoad();

	function onPageLoad() {
		$scope.riskOverviewLoader = true;
		var article = {
			"articlePath": $scope.articles[0].article,
			"entityId":'',
			"entityName":'',
			"newsClass" :''
		};
		if($scope.articles && $scope.articles.length > 0){
			articleList(article,$scope.articles[0]);
		}
		
		// for (var i = 0; i < $scope.articles.length; i++) {
			
		// } //for loop
			var sorted_data = [];
		if($scope.articles.length === 1){
			sorted_data = $scope.articles;
		}else{
			sorted_data  =	$scope.articles.filter(function(d,i){
				if(d.article){
					if(i != 0){return d};
				}
			});
		}
		sorted_data.map(function (val) {
				val.key = val.name;
				val.showlist = true;
				val.entity_id = ((val.entity_id === "orgChartsubEntity" ) ? 'Subsidary' : (val.entity_id === "orgChartmainEntity" ? "Company":(val.entity_id === "orgChartParentEntity" ? "Shareholder" :'')) );
				if(val.Ubo_ibo){
					$scope.firstOpenedArticle.entityDropdown.push({name : val.Ubo_ibo});
					$scope.firstOpenedArticle.entityDropdown = _.uniqBy($scope.firstOpenedArticle.entityDropdown, 'name');
				}
				val.val.finance_Crime_url =  val.val.news ? ( val.val.news.length > 0 ? classtype( val.val.news,val.val,'Financial Crime') :[]  ): [],
				val.val.adverseNews_url = val.val.news ? ( val.val.news.length > 0 ? classtype( val.val.news,val.val,'Neutral') :[]  ): [],
				val.self = val.val;
				val.values = [];
			});
			for (var i = 0, j = sorted_data.length; i < j; i++) {
				var signficantarray = [];
				var primary = [];
				var secondary = [];
				var last = [];
				signficantarray = sorted_data[i].values.filter(function (v) {
					return (v.isSignificant === true);
				});
				last = sorted_data[i].values.filter(function (v) {
					return (v.isSignificant === false);
				});
				var farray = signficantarray.concat(primary).concat(last);
				sorted_data[i].values = farray;
			}
			$scope.actualSorted_Data =  $scope.actualSorted_Data.length > 0 ?  $scope.actualSorted_Data.concat(sorted_data) :sorted_data;
			$scope.leftsideTilelist = $scope.leftsideTilelist.length > 0 ?  $scope.leftsideTilelist.concat(sorted_data) :  sorted_data;
			var mainScreeningCounts = $scope.leftsideTilelist.map(function(d){ 
				return { 
					financeCount : (d.self.finance_Crime_url && d.self.finance_Crime_url.length > 0) ? parseInt(d.self.finance_Crime_url.map(function(c){ return c.count }).toString()) : 0,
					pepsCount : (d.self.pep_url && d.self.pep_url.length > 0) ? parseInt(d.self.pep_url.map(function(c){ return c.count }).toString()) : 0,
					sanctionsCount : (d.self["sanction_bst:description"] && d.self["sanction_bst:description"].length > 0) ? parseInt(d.self["sanction_bst:description"].map(function(c){ return c.count }).toString()) : 0,
					adverseCounts :  (d.self.adverseNews_url && d.self.adverseNews_url.length > 0) ? parseInt(d.self.adverseNews_url.map(function(c){ return c.count }).toString()) : 0,
					jurisdictionCounts : (d.self.high_risk_jurisdiction && (d.self.high_risk_jurisdiction.toLowerCase() == 'high' || d.self.high_risk_jurisdiction.toLowerCase() == 'medium')) ? 1 : 0,
				}
			});
			$scope.totalScreeingresults = {
				financeCount:0,
				pepsCount :0,
				sanctionsCount:0,
				adverseCounts:0,
				jurisdictionCounts:0,

			};
			angular.forEach(mainScreeningCounts,function(val,key){
				$scope.totalScreeingresults.financeCount = $scope.totalScreeingresults.financeCount + val.financeCount;
				$scope.totalScreeingresults.pepsCount = $scope.totalScreeingresults.pepsCount + val.pepsCount;
				$scope.totalScreeingresults.sanctionsCount = $scope.totalScreeingresults.sanctionsCount + val.sanctionsCount;
				$scope.totalScreeingresults.adverseCounts = $scope.totalScreeingresults.adverseCounts + val.adverseCounts;
				$scope.totalScreeingresults.jurisdictionCounts = $scope.totalScreeingresults.jurisdictionCounts + val.jurisdictionCounts;
			});
			console.log($scope.totalScreeingresults);
			
	} //on page loads the function Ends
	$scope.getListOFComments =  function(screenedTypedetails){
		$('.select-count-wrapper').mCustomScrollbar({
					axis: "y",
					theme : "minimal-dark"
				});
				var watchlistId =(screenedTypedetails.id?screenedTypedetails.id:screenedTypedetails.significantId)
			if(watchlistId){
					EntityApiService.getAPIListOfComments(watchlistId).then(function (response) {
						$scope.commentedListLength = response.data.length;
						$scope.commentedList =  response.data;
					},function(){
					
					});
			}
	}
	/* @purpose: on modal open calls the article List
	 * @created: 25 feb  2019
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function articleList(article,fromArticle) {
		$scope.ErrorMessage = false;
		$scope.riskOverviewLoader = true;
		EntityApiService.getListofArticles(article).then(function (response) {
			if (response && response.data && response.data.results && response.data.results.length > 0) {
				
			var nested_articles = d3.nest().key(function (d) {
				if (!d.isSignificant) {
					d.isSignificant = false;
				}
				return d['search_query'];
			}).entries(response.data.results);
			var sorted_data = _.sortBy(nested_articles, [function (o) {
				return o.key;
			}]);

			if(!fromArticle.values){
				sorted_data.map(function (val) {
					val.Ubo_ibo = fromArticle.Ubo_ibo ? fromArticle.Ubo_ibo : '';
					val.showlist = true;
					val.entity_id = ((fromArticle.entity_id === "orgChartsubEntity" ) ? 'Subsidary' : (fromArticle.entity_id === "orgChartmainEntity" ? "Company":(fromArticle.entity_id === "orgChartParentEntity" ? "Shareholder" :'')) );
					if(val.entity_id){
						$scope.firstOpenedArticle.entityDropdown.push({name : val.Ubo_ibo});
						$scope.firstOpenedArticle.entityDropdown = _.uniqBy($scope.firstOpenedArticle.entityDropdown, 'name');
					}
					fromArticle.val.finance_Crime_url =  fromArticle.val.news ? ( fromArticle.val.news.length > 0 ? classtype( fromArticle.val.news,fromArticle.val,'Financial Crime') :[]  ): [],
					fromArticle.val.adverseNews_url = fromArticle.val.news ? ( fromArticle.val.news.length > 0 ? classtype( fromArticle.val.news,fromArticle.val,'Neutral') :[]  ): [],
					val.self = fromArticle.val;
				});
				for (var i = 0, j = sorted_data.length; i < j; i++) {
					var signficantarray = [];
					var last = [];
					signficantarray = sorted_data[i].values.filter(function (v) {
						//return ((v.isSignificant === true) && (v.queue !== 'Low'));
						v.type = type ? type :'adversenews';
						return (v.isSignificant === true);
					});
					last = sorted_data[i].values.filter(function (v) {
						return (v.isSignificant === false);
					});
					var farray = signficantarray.concat(last);
					farray.forEach(function(val){
						val.type =type;
					});
					sorted_data[i].values = farray;
				}
				$scope.actualSorted_Data =  $scope.actualSorted_Data.length > 0 ?  $scope.actualSorted_Data.concat(sorted_data) :sorted_data;
				$scope.leftsideTilelist = $scope.leftsideTilelist.length > 0 ?  $scope.leftsideTilelist.concat(sorted_data) :  sorted_data;
				$scope.actualSorted_Data.reverse();
				$scope.leftsideTilelist.reverse();
			
				//	$scope.text = text;			
				$scope.riskOverviewLoader = true;
			
			}else{
				var leftsideTilelistIndex = $scope.leftsideTilelist.findIndex(function(d){return d.key === fromArticle.key;});
				$scope.leftsideTilelist[leftsideTilelistIndex].values = sorted_data[0].values;
			}
			var leftsideTilelistIndex1 = $scope.leftsideTilelist.findIndex(function(d){return d.key === fromArticle.key;});
			if(leftsideTilelistIndex1 !== -1){
				$scope.leftsideTilelist[leftsideTilelistIndex1].values.map(function(value){
					value.type =  type ? type : 'adverse';
				});
			}else{
				$scope.leftsideTilelist[0].values.map(function(value){
				value.type =  type ? type : 'adverse';
			});
			}
			
			if (response.data.results){
				$scope.getTheArticleDetails(response.data.results[0]);
			}else{
				 	$scope.ErrorMessage = true;
				}
			}
			},function(){
						$scope.ErrorMessage = true;
						$scope.riskOverviewLoader = false;
						$("#marketPulseText").empty();
					 });
	} //article list ends

	/* @purpose: on click of title calls the  Article details
	 * @created: 25 feb  2019
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	$scope.getTheArticleDetails = function (article, event, index,newsClusterID,fromCluster) {
		$scope.routeUrl ='';
		if (($scope.adverseNewsmodalObject.clusteredIndex !== index) && newsClusterID) {
			$scope.adverseNewsmodalObject.clusteredIndex = index;
		} else if(newsClusterID && fromCluster){
			$scope.adverseNewsmodalObject.clusteredIndex = -1;
			return;
		}
		$scope.riskOverviewLoader = true;
		$scope.showHideArticleQuestions = false;
		$scope.ErrorMessage = false;
		if(!newsClusterID && !fromCluster){//we are  highlithing the selected left side panel list
			$scope.hightLightrow = article;
		}
		$scope.commentedListLength = 0;
		$scope.commentedList =  [];
		if (article.view_article_path) {
			var articlepath = {
				articlePath: article.view_article_path
			};
			$scope.articlePath = article.view_article_path;
			EntityApiService.getArticlesDetails(articlepath).then(function (response) {
				$scope.riskOverviewLoader = false;
				var classGiven = article.classification_1 ? article.classification_1 : '';
				if(response.data.results&& !response.data.results.cluster ){
					response.data.results.cluster = [];
					if(article.cluster && article.cluster.length > 0){
						response.data.results.cluster = article.cluster;
					}else if(!newsClusterID){
						response.data.results.cluster.push(response.data.results);
					}
				}
				$scope.loadEntDetails(response.data.results, 0,classGiven,newsClusterID,fromCluster);
				if(response.data.results.significantId || response.data.results.id){
					$scope.getListOFComments(response.data.results);
				}
			},function(){
				$scope.riskOverviewLoader = false;
				$scope.ErrorMessage = true;
				$scope.publishedDate  = '';
				$scope.classification = '';
				$scope.sourceUrl = '';
				
				$("#marketPulseText").empty();
			});
		}else{
			$scope.riskOverviewLoader = false;
		}
		$('.question-box-wrapper  .list-group.custom-list-group').mCustomScrollbar({
			axis: "y"
		});
	};

	$scope.closeOverviewModal = function () {
		$uibModalInstance.close(changeSignificance);
	};
	$scope.showHideMenu = function (key) {
		var _this = $("#custom-check-text" + key);
		if ($(_this).parents(".pulseVisualizerList-wrapper-li").hasClass('active')) {
			$(_this).parents(".pulseVisualizerList-wrapper-li").removeClass('active');
			$(_this).parents('.toggle').next('.treeview-menu').slideUp();
		} else {
			$(".toggleBarMenu li").removeClass("active").find(".treeview-menu").slideUp();
			$(_this).parents(".pulseVisualizerList-wrapper-li").addClass('active');
			$(_this).parents('.toggle').next('.treeview-menu').slideDown();
		}
		setTimeout(function () {
			$('.pulseVisualizerList-wrapper .marketPulseList ul.toggleBarMenu .treeview-menu').mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 100);
	};
	/* @purpose:  on hover of star shows the tile mark/delte the significane news
	 * @created: 12 dec  2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	$scope.checksignificantTitle = function () {
		if (!$scope.firstOpenedArticle.isSignificant) {
			$scope.firstOpenedArticle.significantTitle = 'Save News as Significant';
		} else {
			$scope.firstOpenedArticle.significantTitle = 'Remove News as Significant';
		}
	};
	/* @purpose: Loads the main Entity/centre news and visualisation
	 * @created: 12 dec  2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	$scope.loadEntDetails = function (article, event,classgiven,newsClusterID,fromCluster) {
		$scope.currentArticle = article;
		if(!_.isEmpty($scope.currentArticle)){
			$scope.currentArticle.class = classgiven;
		}
		$scope.currentArticleExists = true;
		if (event) {
			$(event.target).parent().parent().find("li").removeClass("active");
			$(event.target).parent().addClass('active');
		}
		$scope.riskOverviewLoader = true;
		$scope.entities = [];
		$scope.entitiesDetailed = [];
		$scope.questionAndAnswers = [];
		var txt = article.text;
		$scope.text = article.text;
		$scope.textForModal = article.text;
		$scope.articelHeader = article.title;
		if(!newsClusterID && !fromCluster){
			$scope.mainArticelHeader= article.title;
		}
		$scope.publishedDate = article.published ? article.published : '';
		$scope.classification = article.classification_1 ? article.classification_1 : '';
		$scope.firstOpenedArticle.isSignificant = article.isSignificant ? article.isSignificant : false;
		$scope.firstOpenedArticle.cluster = article.cluster? article.cluster: [];
		if(article && article.facts){
			$scope.questionAndAnswers = article.facts ? article.facts.sort(function(a, b) { return a.start - b.start; }) : article.facts;
			$scope.toHideShowQuestionIcon = article.facts.length >0 ? true: false;
		}
		
		var data = {
			"id": article.name,
			"language": "en",
			"text": article.text + " " + article.name
		};
			$timeout(function () {
				$scope.riskOverviewLoader = false;
			}, 2000);
			$scope.firstOpenedArticle.sentimentEmotion = article.sentiment ? ( emotionalIcon(article.sentiment.toLowerCase())) : (article.sentiment && article.sentiment.data && article.sentiment.data.Sentiment)? ( emotionalIcon(article.sentiment.data.Sentiment.toLowerCase())) :'fa-meh-o text-yellow-dot';
			$scope.toolTipSentiment = article.sentiment ? ( emotionalIcon(article.sentiment.toLowerCase())) :  (article.sentiment && article.sentiment.data && article.sentiment.data.Sentiment) ?(article.sentiment && article.sentiment.data && article.sentiment.data.Sentiment) :'neutral';
		$scope.checksignificantTitle();
		
		$scope.routeUrl = article.url ? article.url : 'javascript:void(0);'; /* jshint ignore:line */
		var Url = '';
		if (article.url) {
			Url = article.url.replace(/^https?\:\/\/www\./i, "").split('/')[0];
		}
		if (article.url && Url === 'https:' || Url === 'http:') {
			Url = article.url.replace(/^https?\:\/\/\./i, "").split('/')[2];
		}
		$scope.sourceUrl = Url ? Url.split('.com').join('') : '';
		/* TransactionAnalysisApiService.loadMarketIntelligenceENT(txt).then(function(response){ */
	
		var entities = [];
		var entitiesUnique = [];
		if (article.entities && article.entities.length > 0) {
			angular.forEach(article.entities, function (v) {
				v.start =  v.BeginOffset;
				v.end = v.EndOffset;
				v.type = v.Type;
				v.text	= v.Text;
				if ($.inArray(v.Type.toLowerCase(), $scope.entities) == -1) {
					$scope.entities.push(v.Type.toLowerCase());
					entities[v.Type.toLowerCase()] = [];
					entitiesUnique[v.Type.toLowerCase()] = {};
				} //code not clear
				// var currenttxt = txt.slice(v.BeginOffset, v.EndOffset);
				 var currenttxt = v.Text;
				var currentobj = {
					currenttxt: v.Text,
					start: v.BeginOffset,
					end: v.EndOffset,
					type: v.Type.toLowerCase()
				};
				if (!entitiesUnique[v.Type.toLowerCase()][currenttxt]) {
					entitiesUnique[v.Type.toLowerCase()][currenttxt] = [];
				}
				entities[v.Type.toLowerCase()].push(currentobj);
				entitiesUnique[v.Type.toLowerCase()][currenttxt].push(currentobj);
			});
			presentMArkEntites = jQuery.extend(true, [], article.entities);
			fixedMarkedEntites = jQuery.extend(true, [], article.entities);

			var entitiesDetailed = [];
			$.each(d3.keys(entities), function (i, d) {
				entitiesDetailed.push({
					key: d,
					values: entitiesUnique[d]
				});
			});
		//sorting the object keys ascending order
		setTimeout(function () {
			$scope.loadEntity(txt, article.entities, $scope.entities, newsClusterID); //temporary close
		}, 2000);
	} else {
		setTimeout(function () {
			$scope.loadEntity(txt, [], [], newsClusterID);
		}, 2000);
	}
		$timeout(function () {
			$scope.entitiesDetailed = entitiesDetailed;
			$scope.$apply();
			$('.marketModal-pulse-modal-wrapper .modal-body .pulseVisualizerList-wrapper-li')
				.mCustomScrollbar({
					axis: "y"
				});
		}, 0);//this is temporary
	}; //function loadEntDetails ends 
	String.prototype.splice = function(idx, rem, str) {
		return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
	};

	/**Adding questions and answers in the content,on click of question mark */

	$scope.addQuestionAndAnswers = function(){
		var entityTypes = ['answer','question'];
		var entities =[];
		var questionsLength =0;
		presentMArkEntites = fixedMarkedEntites;
		$scope.showHideArticleQuestions = !$scope.showHideArticleQuestions;
		if($scope.questionAndAnswers !== undefined){
			$scope.questionAndAnswers.sort(function(a, b) { return a.start - b.start;});
		}
		var questioAnswers  =  jQuery.extend(true, [], $scope.questionAndAnswers);
		if($scope.showHideArticleQuestions){
			angular.forEach(questioAnswers,function(val,k){
				if(val.question && val.answer && val.end && val.start && ((val.end - val.start) === val.answer.length) && (val.end > val.start)){
					val.type="answer";
					var  questionPosition = $scope.text.indexOf(val.answer);
					$scope.text = $scope.text.splice(questionPosition,0,val.question);

					// $scope.text = $scope.text.splice(val.start+questionsLength,0,val.question);
					var questionPoints = {
						type:"question",
						start : questionPosition,
						end : questionPosition +val.question.length,
						text:val.question
					};
					questionsLength = questionsLength+val.question.length;
					val.start = questionPosition +val.question.length;
					val.end =val.start+val.answer.length;
					entities.push(questionPoints);
					entities.push(val);
				 	for(var i =0;i< presentMArkEntites.length;i++){
						 if( presentMArkEntites[i].start > questionPosition){
							presentMArkEntites[i].start = presentMArkEntites[i].start+val.question.length;
							presentMArkEntites[i].end = presentMArkEntites[i].end+val.question.length;
						 }
					 }
				}
			});	
			
			var showMArksWith_question =[];
			var totalEntitytype= $scope.entities.concat(entityTypes);
			showMArksWith_question =  presentMArkEntites.concat(entities);
			
			showMArksWith_question.sort(function (a, b) {
				return a.start - b.start;
			});
			$scope.loadEntity($scope.text, showMArksWith_question, totalEntitytype);
		}
		else{
			$("#marketPulseText").empty();
			$scope.text = $scope.textForModal;
			fixedMarkedEntites.forEach(function(val){
				val.start = val.BeginOffset;
				val.end = val.EndOffset;
			})
			fixedMarkedEntites.sort(function (a, b) {
				return a.start - b.start;
			});
			console.log('fixedMarkedEntites: ', fixedMarkedEntites);
			$scope.loadEntity($scope.text, fixedMarkedEntites, $scope.entities);
		}
	};

	/**
	 * Function to show entity visualization
	 */
	$scope.loadEntity = function (text, entities, entityTypes,newsClusterID) {
		$("#marketPulseText").empty();
		$("#newcluster0").empty();
		// Your API
		var api = '';
		// Init displaCY ENT
		var displacy = new displaCyENT(api, {
			container: '#newcluster0'
		});
		if (newsClusterID) {
			displacy = new displaCyENT(api, {
				container: newsClusterID
			});
		}
		var text = text;
		// Entities to visualise
		var ents = entityTypes;
		var spans = entities;
		// Render text
		displacy.render(text, spans, ents);
	};
	//---------------------------------------------------------------------------
	/**
	 * Function to filter entity visualizer
	 */
	$scope.filterEntities = function (entity, parent) {
		var allChildSelected = [];
		if (entity) {
			$(".childCheck_" + entity).prop("checked", $("#parentCheck_" + entity).prop("checked"));
		}
		var entityTypes = [];
		if (parent) {
			var childClass = $('.childCheck_' + parent);
			childClass.each(function () {
				allChildSelected.push($(this).prop('checked'));
			});
			var isEveryChildselected = allChildSelected.every(function (element) {
				return element === true;
			});
			var isEveryNOTChildselected = allChildSelected.every(function (element) {
				return element === false;
			});
			if (isEveryChildselected) { //check all are true
				$("#parentof" + parent).removeClass('determinant-checkbox'); //remove - determinate class
				$("#parentCheck_" + parent).prop("checked", true);
			} else if (isEveryNOTChildselected) { //check for all false
				$("#parentof" + parent).removeClass('determinant-checkbox');
				$("#parentCheck_" + parent).prop("checked", false);
			} else { //check atleast one is true or false
				$("#parentCheck_" + parent).prop("checked", true);
				$("#parentof" + parent).addClass('determinant-checkbox'); //add determinate 
				//note:determinant-checkbox is only added only if the checkbox has checked property
			}
		}
		$(".marketpulseparentCheck:checked").each(function () {
			//		    		   $.each($(this).siblings().find("li"),function(){
			//		    			   $(this).find("input:checkbox").prop("checked",true)
			//		    			   });
			entityTypes.push($(this).val());
		});
		var entities = [];
		$(".marketpulsechildrenCheck:checked").each(function () {
			var curnt = JSON.parse($(this).val());
			angular.forEach(curnt, function (val) {
				if ($.inArray(val.type, entityTypes) == -1) {
					entityTypes.push(val.type);
				}
				entities.push({
					start: val.start,
					type: val.type,
					end: val.end
				});
			});
		});
		entities.sort(function (a, b) {
			return a.start - b.start;
		});
		var clusterId = $scope.adverseNewsmodalObject.clusteredIndex !== -1 ? '#newcluster'+$scope.adverseNewsmodalObject.clusteredIndex : '#newcluster0';
		$scope.loadEntity($scope.text, entities, entityTypes,clusterId);
		//		    	     displacy.render(text, entities, entityTypes);
	};
	/* @purpose: filter the data as Primary or secondary
	 * 			 
	 * @created: 07 may 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	$scope.filterBySourcetype = function (type, issignificant) {
	$scope.ErrorMessage = false;
		var finalFilterData = [];
		for (var i = 0; i < $scope.actualSorted_Data.length; i++) {
			var filter = $scope.actualSorted_Data[i].values.filter(function (value) { /* jshint ignore:line */
				if (type === "ALL" && issignificant) {
					if (value.isSignificant === issignificant) {
						return value;
					}
				} else if (type === "ALL" && !issignificant) {
					return value;
				} else if (type && issignificant) {
					if (value.queue === type && value.isSignificant === issignificant) {
						return value;
					}
				} else {
					if (value.queue === type) {
						return value;
					}
				} //else ends
			});
			if (filter.length > 0) {
				var data = {
					key: $scope.actualSorted_Data[i].key,
					values: filter,
					showlist: $scope.actualSorted_Data[i].showlist
				};
				finalFilterData.push(data);
			}else {
				$scope.entitiesDetailed =[];
				$scope.ErrorMessage = true;
			}
		} //for loop ends
		$scope.leftsideTilelist = finalFilterData;
		$scope.currentArticle = $scope.leftsideTilelist.length ? $scope.leftsideTilelist[0].values[0] : {};
		if (JSON.stringify($scope.currentArticle) != "{}") {
			$scope.loadEntDetails($scope.currentArticle, '', '');
		}
		if ($scope.leftsideTilelist.length === 0) {
			$scope.currentArticleExists = false;
		}
		setTimeout(function () {
			$('.pulseVisualizerText-wrapper,.pulseVisualizerList-wrapper,.custom-list-group').mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		}, 10);
	}; //function ends
	$scope.pepSanctionSignificant = function (siginificance,pepDetails,isType) {
		if(isType=="pep"||isType=="sanction"){
			var isTypeSanction = isType=="sanction"? true:false;
		}
		var data;
		var requestType = siginificance == true?"DELETE":"POST";
		if(requestType =="POST"){
			 data = {
				"identifier": pepDetails.identifier,
				"mainEntityId": $rootScope.entityObj["@identifier"],
				"name": pepDetails.name,
				"entityName":$rootScope.entityObj.name,
				"sanction": isTypeSanction,
				"pep": !isTypeSanction,
				"value": pepDetails.value
			}
			EntityApiService.postSanctionPepsignificance(data,requestType).then(function(response){
				console.log('response: ', response.data);
				var index = _.findIndex($scope.leftsideTilelist, {//check for parent index in the screening Results
					"key":  pepDetails.name
				});
				var screening_index = _.findIndex($scope.$parent.screeningData, {//check for parent index in the screening Results
					"name":  pepDetails.name
				});
				var summary_index = _.findIndex($scope.$parent.summaryScreenig, {//check for parent index in the screening Results
										"name":  pepDetails.name
								});
				if(isType=="pep"){
					var valueIndex = _.findIndex($scope.leftsideTilelist[index].pep_url_values, {//check for parent index in the screening Results
						"value":   pepDetails.value
					});
					var screening_valueIndex = _.findIndex($scope.$parent.screeningData[screening_index].pep_url, {//check for parent index in the screening Results
						"value":   pepDetails.value
					});
					var summmary_valueIndex = _.findIndex($scope.$parent.summaryScreenig[summary_index].pep_url, {//check for parent index in the screening Results
											"value":   pepDetails.value
											});
					$scope.$parent.screeningData[screening_index].pep_url[screening_valueIndex].pep = response.data.pep;
					$scope.$parent.screeningData[screening_index].pep_url[screening_valueIndex].id = response.data.id;
					$scope.$parent.screeningData[screening_index].pep_url[screening_valueIndex].sanction = response.data.sanction;
					$scope.$parent.summaryScreenig[summary_index].pep_url[summmary_valueIndex].pep = response.data.pep;
				    $scope.$parent.summaryScreenig[summary_index].pep_url[summmary_valueIndex].id = response.data.id
				    $scope.$parent.summaryScreenig[summary_index].pep_url[summmary_valueIndex].sanction = response.data.sanction;
					
					$scope.leftsideTilelist[index].pep_url_values[valueIndex].pep = response.data.pep;
					$scope.leftsideTilelist[index].pep_url_values[valueIndex].id = response.data.id;
					$scope.leftsideTilelist[index].pep_url_values[valueIndex].userId = response.data.userId;
					$scope.leftsideTilelist[index].pep_url_values[valueIndex].sanction = response.data.sanction;
					var splicedPep= $scope.leftsideTilelist[index].pep_url_values.splice(valueIndex,1);
					$scope.leftsideTilelist[index].pep_url_values.unshift(splicedPep[0]);
				}else{
					
						var valueIndex = _.findIndex($scope.leftsideTilelist[index].sanction_url_values, {//check for parent index in the screening Results
							"value":   pepDetails.value
						});
						var summaryvalueIndex = _.findIndex($scope.$parent.summaryScreenig[summary_index].sanction_url_values, {//check for parent index in the screening Results
												"value":   pepDetails.value
											});
						var screening_valueIndex = _.findIndex($scope.$parent.screeningData[screening_index]['sanction_bst:description'], {//check for parent index in the screening Results
							"value":   pepDetails.value
						});
						var summary_valueIndex = _.findIndex($scope.$parent.summaryScreenig[summaryvalueIndex]['sanction_bst:description'], {//check for parent index in the screening Results
													"value":   pepDetails.value
													});
						$scope.$parent.screeningData[screening_index]['sanction_bst:description'][screening_valueIndex].pep = response.data.pep;
						$scope.$parent.screeningData[screening_index]['sanction_bst:description'][screening_valueIndex].id = response.data.id;
						$scope.$parent.screeningData[screening_index]['sanction_bst:description'][screening_valueIndex].sanction = response.data.sanction;
						$scope.$parent.summaryScreenig[summary_index]['sanction_bst:description'][summary_valueIndex].pep = response.data.pep;
						$scope.$parent.summaryScreenig[summary_index]['sanction_bst:description'][summary_valueIndex].id = response.data.id;
						$scope.$parent.summaryScreenig[summary_index]['sanction_bst:description'][summary_valueIndex].sanction = response.data.sanction;
						$scope.leftsideTilelist[index].sanction_url_values[valueIndex].pep = response.data.pep;
						$scope.leftsideTilelist[index].sanction_url_values[valueIndex].id = response.data.id;
						$scope.leftsideTilelist[index].sanction_url_values[valueIndex].userId = response.data.userId;
						$scope.leftsideTilelist[index].sanction_url_values[valueIndex].sanction = response.data.sanction;
						var splicedSanction= $scope.leftsideTilelist[index].sanction_url_values.splice(valueIndex,1);
					$scope.leftsideTilelist[index].sanction_url_values.unshift(splicedSanction[0]);
					
				}
				
			},function(){
					$scope.riskOverviewLoader = false;
			});
		} else{
			callMarkInsignificantPopUP(siginificance,isType,pepDetails);
		}
	
	}

	/* @purpose:  marked as significant
	 * 			 
	 * @created: 13 nov 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Badri */
	$scope.makeSignificant = function (siginificance,isType) {
		$scope.riskOverviewLoader = true;
		var type = siginificance === true ? 'delete' : 'save';
		if(type =='save'){
			var data = {
				"sentiment": $scope.currentArticle.sentiment ? $scope.currentArticle.sentiment : (($scope.currentArticle.sentiment && $scope.currentArticle.sentiment.data && $scope.currentArticle.sentiment.data.Sentiment) ?  $scope.currentArticle.sentiment.data.Sentiment : 'NEUTRAL'),
				"uuid":  $scope.currentArticle.uuid ? $scope.currentArticle.uuid :  ( ($scope.currentArticle.thread && $scope.currentArticle.thread.uuid ) ? $scope.currentArticle.thread.uuid : ''),
				"significantNews":!siginificance,
				"entityId":$rootScope.entityObj["@identifier"],
				"articleUrl":$scope.articlePath ? $scope.articlePath : '',
				"classification":$scope.classification ? $scope.classification : ''
				
			};
			APICallMarkUnmarAdverseFinanceSignificant(data,type)
		} else{
			callMarkInsignificantPopUP(siginificance,isType,$scope.currentArticle)
		}
		
	}; //function ends

	var modalInstanceInsignificant;
	function callMarkInsignificantPopUP(significane,isType,screenedDetails){
		$scope.makeInsignificantComment.isSignificant =significane;
		$scope.makeInsignificantComment.typeScreening=isType;
		$scope.makeInsignificantComment.screenedData =screenedDetails;
		
		modalInstanceInsignificant = $uibModal.open({
			templateUrl: 'markInsignificant.html',
			size: 'sm',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow  bst_modal z-99999999',
			scope: $scope
		});
	
		modalInstanceInsignificant.result.then(function (response) {
		}, function (reject) {
		});
	}
	$scope.markInsignificantComment = function(status,comment,screenedTypeDetails){
		console.log("tpes",status,comment,screenedTypeDetails);
		
		if(!status){
			modalInstanceInsignificant.dismiss('close');
			$scope.riskOverviewLoader = false
		} else{
		   if(screenedTypeDetails.typeScreening.toLowerCase() =='pep' || screenedTypeDetails.typeScreening.toLowerCase()  =='sanction' ){

			   var watchlishId =(screenedTypeDetails.screenedData.id?screenedTypeDetails.screenedData.id:screenedTypeDetails.screenedData.significantId)
				EntityApiService.deleteSanctionPepsignificance(screenedTypeDetails.typeScreening,watchlishId,comment).then(function(response){
									console.log('response: ', response.data);
									modalInstanceInsignificant.dismiss('close');
									
									var index = _.findIndex($scope.leftsideTilelist, {//check for parent index in the screening Results
										"key":  screenedTypeDetails.screenedData.name
									});
									var screening_index = _.findIndex($scope.$parent.screeningData, {//check for parent index in the screening Results
										"name":  screenedTypeDetails.screenedData.name
									});
									if(response.data.responseMessage=== "Significant watchList deleted successfully."){
									if(screenedTypeDetails.typeScreening =='pep'){
											var valueIndex = _.findIndex($scope.leftsideTilelist[index].pep_url_values, {//check for parent index in the screening Results
												"value":   screenedTypeDetails.screenedData.value
											});
											var screening_valueIndex = _.findIndex($scope.$parent.screeningData[screening_index].pep_url, {//check for parent index in the screening Results
												"value":   screenedTypeDetails.screenedData.value
											});
						
											$scope.$parent.screeningData[screening_index].pep_url[screening_valueIndex].pep = false;
											$scope.$parent.screeningData[screening_index].pep_url[screening_valueIndex].reason =comment;
											$scope.leftsideTilelist[index].pep_url_values[valueIndex].pep =false;
										}else{
											var valueIndex = _.findIndex($scope.leftsideTilelist[index].sanction_url_values, {//check for parent index in the screening Results
												"value":   screenedTypeDetails.screenedData.value
											});
											var screening_valueIndex = _.findIndex($scope.$parent.screeningData[screening_index]['sanction_bst:description'], {//check for parent index in the screening Results
												"value":   screenedTypeDetails.screenedData.value
											});
											$scope.leftsideTilelist[index].sanction_url_values[valueIndex].sanction = false;
											$scope.$parent.screeningData[screening_index]['sanction_bst:description'][screening_valueIndex].sanction = false;
											$scope.$parent.screeningData[screening_index]['sanction_bst:description'][screening_valueIndex].reason =comment;
						
										}
										$scope.getListOFComments(screenedTypeDetails.screenedData)
								}
								
								},function(){
									modalInstanceInsignificant.dismiss('close');
										$scope.riskOverviewLoader = false;
								});
			} else{
				var data = {
					"sentiment": $scope.currentArticle.sentiment ? $scope.currentArticle.sentiment : (($scope.currentArticle.sentiment && $scope.currentArticle.sentiment.data && $scope.currentArticle.sentiment.data.Sentiment) ?  $scope.currentArticle.sentiment.data.Sentiment : 'NEUTRAL'),
					"uuid":  $scope.currentArticle.uuid ? $scope.currentArticle.uuid :  ( ($scope.currentArticle.thread && $scope.currentArticle.thread.uuid ) ? $scope.currentArticle.thread.uuid : ''),
					"significantNews":!screenedTypeDetails.isSignificant,
					"entityId":$rootScope.entityObj["@identifier"],
					"articleUrl":$scope.articlePath ? $scope.articlePath : '',
					"classification":$scope.classification ? $scope.classification : '',
					"comment":comment
				};
				APICallMarkUnmarAdverseFinanceSignificant(data,'delete')
			}
		}
	}
 function APICallMarkUnmarAdverseFinanceSignificant(data,type){
	$scope.mySignificant = true;
	EntityApiService.markAsSignificant(data, type).then(function (response) {
		var newSignificance = {};
		if(response &&response.data && response.data.id){
			$scope.currentArticle.significantId = response.data.id;
		}
		var tempArr = jQuery.extend(true, [], changeSignificance);
		for (var i = 0, j = tempArr.length; i < j; i++) {
			if (tempArr[i].title === response.config.data.title) {
				changeSignificance.pop(tempArr[i]);
			}
		}
		
		if (response && response.data && response.data.significantNews==false ) {
			newSignificance = {
				"isSignificant": false,
				"title": response.config.data.title,
				"entityId": response.config.data.entityId,
				"entityName": response.config.data.entityName,
				"newsClass": response.config.data.newsClass,
				"publishedDate": response.config.data.publishedDate,
				"url": response.config.data.url
			};
			modalInstanceInsignificant.dismiss('close');
			$scope.getListOFComments($scope.makeInsignificantComment.screenedData)
		} else {
			newSignificance = {
				"isSignificant": true,
				"title": response.config.data.title,
				"entityId": response.config.data.entityId,
				"entityName": response.config.data.entityName,
				"newsClass": response.config.data.newsClass,
				"publishedDate": response.config.data.publishedDate,
				"url": response.config.data.url
			};
		}
		changeSignificance.push(newSignificance);
		var findLeftvalueIndex;
		var findLeftkeyIndex = _.findIndex($scope.leftsideTilelist, ['key', $scope.currentArticle.class]);
		if (findLeftkeyIndex != -1 && $scope.currentArticle.url) {
			findLeftvalueIndex =_.findIndex($scope.leftsideTilelist[findLeftkeyIndex].values, { 'title': $scope.currentArticle.title, 'url':$scope.currentArticle.url });//here im comparing with tile and url
		}else if(findLeftkeyIndex != -1 ){//here im checking only title as there is no source
			findLeftvalueIndex = _.findIndex($scope.leftsideTilelist[findLeftkeyIndex].values, ['title', $scope.currentArticle.title]);
		}
		if ((findLeftvalueIndex || findLeftvalueIndex === 0) && findLeftvalueIndex != -1) {
			var flag = $scope.leftsideTilelist[findLeftkeyIndex].values[findLeftvalueIndex].isSignificant === true ? false : true;
			$timeout(function () {
				$scope.leftsideTilelist[findLeftkeyIndex].values[findLeftvalueIndex].isSignificant = flag;
			}, 0);
		}
		var signinfnce = $scope.currentArticle.isSignificant === true ? false : true;
		var opened = $scope.firstOpenedArticle.isSignificant === true ? false : true;
		$scope.currentArticle.isSignificant = signinfnce;
		$scope.firstOpenedArticle.isSignificant = opened;
		$scope.checksignificantTitle();
		$scope.riskOverviewLoader = false;
	}, function () {
		$scope.mySignificant = false;
		$scope.riskOverviewLoader = false;
		modalInstanceInsignificant.dismiss('close');
	});
 } 

	/* @purpose:  filter Classification
	 * 			 
	 * @created: 13 nov 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Badri */
	$scope.classificationFilter = function (value) {
		var index = _.findIndex($scope.leftsideTilelist, ['key', value]);
		$scope.leftsideTilelist[index].showlist = !$scope.leftsideTilelist[index].showlist;
		if ($scope.currentArticle.class === value) {
			$scope.currentArticleExists = false;
			$scope.currentArticle = {};
		}
	}; //function ends
	/* @purpose:  changes the Direction of caret
	 * @created: 13 nov 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Badri */
	$scope.changeArrrowDirection = function (index,fromArticle) {
		$scope.selectedEntityjurisdiction = fromArticle;
		if ($scope.Selectedheadingarrow !== index) {
			$scope.Selectedheadingarrow = index;
		} else {
			$scope.Selectedheadingarrow = -1;
		}
		if($scope.summaryFilterBadges.finance === "finance" && fromArticle.financeValues.length === 0){
			getEntityArticles(fromArticle);
		}else if($scope.summaryFilterBadges.adverse === "adverse" && fromArticle.values.length === 0){
			getEntityArticles(fromArticle);
		}
	};
	/* @purpose:  mark the Sentiments
	 * @created: 13 nov 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Badri */
	$scope.sentimentSelection = function (sentiment) {
		$scope.riskOverviewLoader = true;
		$scope.sentimentpopover=false;
		var data = {
			"sentiment":sentiment,
			"uuid":  $scope.currentArticle.uuid ? $scope.currentArticle.uuid : '',
			"significantNews":  $scope.currentArticle.isSignificant ? $scope.currentArticle.isSignificant :'',
		};
		EntityApiService.sentimentUpdation(data).then(function (response) {
			var sentimentIndex = -1;
			if(response.data && response.data.url){
				sentimentIndex = _.findIndex($scope.articles, { 'url': response.data.url, 'title': response.data.title });
				changeSignificance.push({
					"isSignificant": response.data.significantNews,
					"title": response.data.title,
					"entityId": response.data.entityId,
					"entityName": response.data.entityName,
					"newsClass": response.data.newsClass,
					"publishedDate": response.data.publishedDate,
					"url": response.data.url,
					"sentiment":response.data.sentiment
				});
			}else{
				sentimentIndex = _.findIndex($scope.articles, {  'title': response.data.title });
				changeSignificance.push({
					"isSignificant":  response.data.significantNews,
					"title": response.data.title,
					"entityId": response.config.data.entityId,
					"entityName": response.config.data.entityName,
					"newsClass": response.config.data.newsClass,
					"publishedDate": response.config.data.publishedDate,
					"url": response.config.data.url,
					"sentiment":response.data.sentiment				
				});
			}
			if(response.data &&  response.data.sentiment){
				$scope.firstOpenedArticle.sentimentEmotion = emotionalIcon(response.data.sentiment.toLowerCase());
				$scope.toolTipSentiment = response.data.sentiment.toLowerCase();
			}
			if(sentimentIndex !==-1){
				$scope.articles[sentimentIndex].sentiment = sentiment.toLowerCase();
			}
			$scope.riskOverviewLoader = false;

		}, function () {
			$scope.riskOverviewLoader = false;
		});
	};

	/* @purpose:  check the emotion Icon need to shown
	 * @created: 23 jsn 2019
	 * @params: type(string)
	 * @returns: type of Emotion Icon
	 * 	 * @author: Ram  */
	function emotionalIcon(type){
		var icon ='';
		icon = type ==='negative' ? "fa-frown-o text-dark-red" :(type==='positive' ? "fa-smile-o text-green-dot" :"fa-meh-o text-yellow-dot");
		return icon;
	}
	function classtype(news,val,type){
		var data = _.find(news, { 'classification': type});
		var newsarray =[];
		if (data && data.count && data['_links'] && data['_links'].articles) {
			newsarray.push({	
				article: data['_links'].articles,
				name: val.name,
				count:data.count
			});
		}
		return newsarray;
	}
	$scope.filterSummaryscreeningBadges = function (type,onpageload,slider,fromFilterSummary) {
		$scope.ErrorMessage = false;
		$scope.mainArticelHeader = '';
		$scope.firstOpenedArticle.cluster = [];
		$scope.riskOverviewLoader = true;
		$scope.entitiesDetailed= [];
		$scope.showSlider = {
			pepSlider : false,
			sactionSlider : false
		};
		$scope.commentedList = [];
		if($scope.summaryFilterBadges[type] === type){
			$scope.summaryFilterBadges[type] = ''; //for multisource filter remove this line
		}else {
			$scope.summaryFilterBadges = {}; //for multisource filter remove this line
			$scope.summaryFilterBadges[type] = type;
		}
		if(type === "pep"){
			$scope.typeIs =type;
			$scope.showSlider.pepSlider = true;
			filter_type = type;
			if(slider) $scope.pepSanctionSliderChange('pep');
			if(fromFilterSummary) $scope.pepSanctionSliderChange('pep','','',fromFilterSummary);
			var pep  =  {};
			// pep =  $scope.firstOpenedArticle.filterEntites ? (_.find($scope.leftsideTilelist, { 'pep_url': 'pep','name': $scope.firstOpenedArticle.filterEntites})) : (_.find($scope.leftsideTilelist, { 'pep_url': 'pep'})) ;
			pep =  $scope.firstOpenedArticle.filterEntites ? (_.find($scope.leftsideTilelist, { 'pep_url': 'pep','name': $scope.firstOpenedArticle.filterEntites})) : (_.find($scope.leftsideTilelist, function(d){return d.pep_url_values.length > 0})) ;
			if(pep){
			$scope.pepDetails = pep.pep_url_values[0];
			$scope.pepDetails.name = pep.key;
			$scope.pepDetails.identifier =( pep.self.Ubo_ibo || pep.self.Ubo_ibo !== 'officers') ? pep.identifier  : '';
			$scope.hightLightrow = $scope.pepDetails;
			$scope.getListOFComments($scope.pepDetails);
			angular.forEach($scope.leftsideTilelist,function(val,index){
				
				var sig = val.pep_url_values.filter(function(d){ return d.pep})
					
				var unsig = val.pep_url_values.filter(function(d){ return !d.pep})
				
				val.pep_url_values = sig.concat(unsig);
			});
			var entries = jQuery.extend(true, [], $scope.pepDetails.entries);
				$scope.pepDetails.entries = [];
				angular.forEach(entries,function(d,key){
					if(typeof(d.attribute_name) === 'object'){
						angular.forEach(d.attribute_name,function(attrname,i1){
							$scope.pepDetails.entries.push({
								watchlist_id:d.watchlist_id,
								entry_id :  d.entry_id,
								attribute_name :[attrname]
							});
						});
					}
				});
			$('.scroll-body').mCustomScrollbar({
				axis: "y"
			});
			}else{
				$scope.pepDetails =[];
				$scope.hightLightrow ={};
				$scope.ErrorMessage = true;
			}
		 }else if(type === "sanction"){
			$scope.typeIs =type;
			$scope.showSlider.sactionSlider = true;
			filter_type = type;
			if(slider) $scope.pepSanctionSliderChange('sanction');
			if(fromFilterSummary) $scope.pepSanctionSliderChange('sanction','','',fromFilterSummary);
			var sancion  = {};
			// sancion = $scope.firstOpenedArticle.filterEntites ? _.find($scope.leftsideTilelist, { 'sanction': 'sanction','name': $scope.firstOpenedArticle.filterEntites}) :  _.find($scope.leftsideTilelist, { 'sanction': 'sanction'});
			sancion = $scope.firstOpenedArticle.filterEntites ? _.find($scope.leftsideTilelist, { 'sanction': 'sanction','name': $scope.firstOpenedArticle.filterEntites}) :  _.find($scope.leftsideTilelist, function(d){return d.sanction_url_values.length > 0});
			if(sancion){
				$scope.pepDetails = sancion.sanction_url_values[0];
				$scope.pepDetails.name = sancion.key;
			    $scope.pepDetails.identifier =( sancion.self.Ubo_ibo || sancion.self.Ubo_ibo !== 'officers') ? sancion.identifier  : '';
				$scope.hightLightrow = $scope.pepDetails;
				$scope.getListOFComments($scope.pepDetails)
				//$scope.leftsideTilelist[index].sanction_url_values = sig.concat(unsig);
				angular.forEach($scope.leftsideTilelist,function(val,index){
				
					var sig = val.sanction_url_values.filter(function(d){ return d.sanction})
				
					var unsig = val.sanction_url_values.filter(function(d){ return !d.sanction})
					
					val.sanction_url_values = sig.concat(unsig);
				});
				var entries = jQuery.extend(true, [], $scope.pepDetails.entries);
				$scope.pepDetails.entries = [];
				angular.forEach(entries,function(d,key){
					if(typeof(d.attribute_name) === 'object'){
						angular.forEach(d.attribute_name,function(attrname,i1){
							$scope.pepDetails.entries.push({
								watchlist_id:d.watchlist_id,
								entry_id :  d.entry_id,
								attribute_name :[attrname]
							});
						});
					}
				});
				$('.scroll-body').mCustomScrollbar({
					axis: "y"
				});
			}else{
				$scope.pepDetails =[];
				$scope.hightLightrow ={};
				$scope.ErrorMessage = true;

			}
		}
		else if(type === "highRisk"){
			var jurisdction   =  $scope.firstOpenedArticle.filterEntites ? _.find($scope.leftsideTilelist, { 'jurisdiction': "highRisk",'name': $scope.firstOpenedArticle.filterEntites}) :_.find($scope.leftsideTilelist, { 'jurisdiction': "highRisk"});
			if(jurisdction){
				$scope.selectedEntityjurisdiction  = jurisdction;
				$scope.hightLightrow = $scope.selectedEntityjurisdiction;
			}else{
				$scope.selectedEntityjurisdiction  = [];
				$scope.hightLightrow = {};
				$scope.ErrorMessage = true;
			}
		}
		else if (type === 'adverse') {
			$scope.typeIs =type;
			if ($scope.firstOpenedArticle.filterEntites) {
				$scope.leftsideTilelist = _.filter(leftSidePanelEntities, {
					'adverseNews': 'adverse',
					'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
				});
			} else {
				emptySearchedEntitesforadverse();
			}
			$scope.selectedSummary = $scope.leftsideTilelist.length > 0 ? $scope.leftsideTilelist[0] : {};
			var adverse = $scope.firstOpenedArticle.filterEntites ? _.find($scope.leftsideTilelist, {
				'adverseNews': 'adverse',
				'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
			}) : _.find($scope.leftsideTilelist, {
				'adverseNews': 'adverse'
			});
			var adverseIndex = $scope.firstOpenedArticle.filterEntites ? _.findIndex($scope.leftsideTilelist, {
				'adverseNews': 'adverse',
				'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
			}) : _.findIndex($scope.leftsideTilelist, {
				'adverseNews': 'adverse'
			});
			console.log('adverse: ', adverse);
			if (adverse && adverseIndex !== -1) {
				$scope.leftsideTilelist[adverseIndex].values = adverse.values;
				if (adverse.values.length === 0) {
					getEntityArticles(adverse);
				}else if($scope.leftsideTilelist[adverseIndex].values && $scope.leftsideTilelist[adverseIndex].values.length > 0 ){
					$scope.getTheArticleDetails($scope.leftsideTilelist[adverseIndex].values[0],'','','',false);
				}
			} else {
				$scope.text = '';
				$scope.articelHeader = '';
				$scope.publishedDate = '';
				$scope.classification = '';
				$scope.sourceUrl = '';
				$scope.ErrorMessage = true;
			}

		} else if (type === 'finance') {
			console.log('leftSidePanelEntities: ', leftSidePanelEntities);
			$scope.typeIs =type;
			if($scope.firstOpenedArticle.filterEntites){
				$scope.leftsideTilelist  = _.filter(leftSidePanelEntities, {
					'financeCrime': 'finance',
					'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
				});
			}else{
				emptySearchedEntitesOfFinance();
			}
			$scope.selectedSummary = $scope.leftsideTilelist.length > 0 ? $scope.leftsideTilelist[0] : {};
			var finance = $scope.firstOpenedArticle.filterEntites ? _.find($scope.leftsideTilelist, {
				'financeCrime': 'finance',
				'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
			}) : _.find($scope.leftsideTilelist, {
				'financeCrime': 'finance'
			});
			var financeIndex = $scope.firstOpenedArticle.filterEntites ? _.findIndex($scope.leftsideTilelist, {
				'financeCrime': 'finance',
				'name': $scope.firstOpenedArticle.filterEntites.toLowerCase()
			}) : _.findIndex($scope.leftsideTilelist, {
				'financeCrime': 'finance'
			});
			if (finance && financeIndex !== -1) {
				$scope.leftsideTilelist[financeIndex].financeValues = finance.financeValues;
				if (finance.financeValues.length === 0) {
					getEntityArticles(finance);
				}else if($scope.leftsideTilelist[financeIndex].financeValues && $scope.leftsideTilelist[financeIndex].financeValues.length > 0){
					$scope.getTheArticleDetails($scope.leftsideTilelist[financeIndex].financeValues[0],'','',false);
				}
			} else {
				$scope.text = '';
				$scope.articelHeader = '';
				$scope.publishedDate = '';
				$scope.classification = '';
				$scope.sourceUrl = '';
				$scope.ErrorMessage = true;
			}
		}
		$scope.riskOverviewLoader = false;
		};
	
	function getEntityArticles(fromArticle) {
		$scope.riskOverviewLoader = true;

		var article = {
			articlePath: fromArticle.adverse_article
		}; 
		if($scope.summaryFilterBadges.adverse === "adverse" &&  fromArticle.adverse_article){
			EntityApiService.getListofArticles(article).then(function (response) {
				if (response && response.data && response.data.results && response.data.results.length > 0) {
					var summaryIndex = _.findIndex($scope.leftsideTilelist, ['identifier', fromArticle.identifier]);
					if (summaryIndex !== -1) {
						response.data.results.forEach(function(val){val.type = 'adverse';});
						$scope.leftsideTilelist[summaryIndex].values = response.data.results;
						$scope.getTheArticleDetails(response.data.results[0],'','','',false);
					}
				}
			},function(){
				$scope.ErrorMessage =true;
				$scope.riskOverviewLoader = false;
				$scope.publishedDate  = '';
				$scope.classification = '';
				$scope.sourceUrl = '';
			});
		}else if($scope.summaryFilterBadges.finance === "finance" &&  fromArticle.finance_article){
			article.articlePath =fromArticle.finance_article;
			EntityApiService.getListofArticles(article).then(function (response) {
				if (response && response.data && response.data.results && response.data.results.length > 0) {
					var clustedData =[];
							var nested_data = d3.nest().key(function (d) {
								return d.event_id;
							}).entries(response.data.results);
							 console.log(nested_data);
							 nested_data.forEach(function(clusterData){
								if(clusterData['key'] === "-1"){
									clustedData = clustedData.concat(clusterData.values);
								}else{
									
									if(clusterData.values &&  clusterData.values.length > 0 ){
										clusterData.values.forEach(function(val){
											val.cluster = clusterData.values;
										});
										var clustedObject = clusterData.values[0];
										clustedData.push(clustedObject);
									}
								}
							 });
							 console.log(clustedData);
					var summaryIndex = _.findIndex($scope.leftsideTilelist, ['identifier', fromArticle.identifier]);
					if (summaryIndex !== -1) {
						clustedData.forEach(function(val){val.type = 'financeCrime';});
						$scope.leftsideTilelist[summaryIndex].financeValues = clustedData;
						$scope.adverseNewsmodalObject.clusteredIndex = 0;
						$scope.getTheArticleDetails(clustedData[0],'','','',false);
					}
				}
			},function(){
				$scope.ErrorMessage =true;
				$scope.riskOverviewLoader = false;
				$scope.publishedDate  = '';
				$scope.classification = '';
				$scope.sourceUrl = '';
			});
		}
	}
	$scope.screeenArticle = function(article,$index,value){
		$scope.pepDetails = article;
		$scope.pepDetails.name = value.key;
		$scope.pepDetails.identifier =( value.self.Ubo_ibo || value.self.Ubo_ibo !== 'officers') ? value.identifier  : '';
		$scope.hightLightrow = $scope.pepDetails;
		$scope.hightLightrow = article;
		$scope.commentedListLength =0;
			$scope.commentedList = [];
		if(article.significantId || article.id){
			$scope.getListOFComments(article);
		}
		var entries = jQuery.extend(true, [], $scope.pepDetails.entries);
		$scope.pepDetails.entries = [];
		angular.forEach(entries,function(d,key){
			if(typeof(d.attribute_name) === 'object'){
				angular.forEach(d.attribute_name,function(attrname,i1){
					$scope.pepDetails.entries.push({
						watchlist_id:d.watchlist_id,
						entry_id :  d.entry_id,
						attribute_name :[attrname]
					});
				});
			}
		});
		$('.scroll-body').mCustomScrollbar({
			axis: "y"
		});
	};

	$scope.store_leftsideTilelist = $scope.leftsideTilelist;
	$scope.pepSanctionSliderChange = function(type,slider,onpageload,fromFilterSummary){
		var slider_val = 0;
		var original_data ;
		var count = 0;
		// var adverseData = jQuery.extend(true,[],$scope.store_leftsideTilelist);
		 var adverseData = $scope.store_leftsideTilelist.map(function(val){
			 return val;
		 });
		if($scope.firstOpenedArticle.filterEntites){
			adverseData = adverseData.filter(function(d){
				return (d.name == $scope.firstOpenedArticle.filterEntites);
			});
		}
		angular.forEach(adverseData,function(value,key){
			
			if(type == 'pep'){
				slider_val = $scope.filtered.pep;
				original_data = value.self.pep_url;
				value.pep_url_values = original_data.filter(function(val){
					return ((val.confidence * 100) >= slider_val);
				});	
				count += value.pep_url_values.length;
				$scope.totalScreeingresults.pepsCount = Number(count).formatAmt(0);;
				if($scope.totalScreeingresults.pepsCount > 0){
					$scope.ErrorMessage = false;
				}
			}else if(type == 'sanction'){
				slider_val = $scope.filtered.sanction;
				original_data = value.self['sanction_bst:description'];
				value.sanction_url_values = original_data.filter(function(val){
					return ((val.confidence * 100) >= slider_val);
				});	
				count += value.sanction_url_values.length;
				$scope.totalScreeingresults.sanctionsCount = Number(count).formatAmt(0);
			
				if($scope.totalScreeingresults.sanctionsCount > 0){
					$scope.ErrorMessage = false;
				}
			}
		});

		if(filter_type == type){
			if(filter_type == 'pep'){
				$scope.leftsideTilelist = _.filter($scope.store_leftsideTilelist, function(d){return d.pep_url_values.length > 0});
			}else if(type == 'sanction'){
				$scope.leftsideTilelist = _.filter($scope.store_leftsideTilelist, function(d){return d.sanction_url_values.length > 0});
			}
			if(fromFilterSummary){
				return;
			}
			$scope.summaryFilterBadges = {};
			$scope.filterSummaryscreeningBadges(filter_type,onpageload,slider);
		}
	}
	// on page load default show list between 80 to 100 for pep and 70 to 100 for sanction
	$scope.pepSanctionSliderChange(filter_type,'','onpageload');
	if(filter_type == 'pep'){
		$scope.pepSanctionSliderChange('sanction'); // calling oposite arg of filter type as we alredy calling same filter type on the above line
	}else if(filter_type == 'sanction'){
		$scope.pepSanctionSliderChange('pep');
	}
    $scope.filterSearchInputOnChange = function(val){
	        if(!val){
	            var activeSummary = Object.keys($scope.summaryFilterBadges);
	            if(activeSummary.includes('pep')){
	                $scope.screeenArticle($scope.leftsideTilelist[0].pep_url_values[0],0,$scope.leftsideTilelist[0]);
						var count = 0;
						var slider_val = 0;
						var original_data ;
						angular.forEach($scope.leftsideTilelist,function(value,key){
								
							if(activeSummary.includes('pep')){
								slider_val = $scope.filtered.pep;
								original_data = value.self.pep_url;
								value.pep_url_values = original_data.filter(function(val){
									return ((val.confidence * 100) >= slider_val);
								});	
								count += value.pep_url_values.length;
								$scope.totalScreeingresults.pepsCount = Number(count).formatAmt(0);
								if($scope.totalScreeingresults.pepsCount > 0){
									$scope.ErrorMessage = false;
								}
							}else if(activeSummary.includes('sanction')){
								slider_val = $scope.filtered.sanction;
								original_data = value.self['sanction_bst:description'];
								value.sanction_url_values = original_data.filter(function(val){
									return ((val.confidence * 100) >= slider_val);
								});	
								count += value.sanction_url_values.length;
								$scope.totalScreeingresults.sanctionsCount = Number(count).formatAmt(0);
							
								if($scope.totalScreeingresults.sanctionsCount > 0){
									$scope.ErrorMessage = false;
								}
							}
						});
	                $scope.ErrorMessage = false;
				} else if (activeSummary.includes("finance")) {
					emptySearchedEntitesOfFinance();
					$scope.selectedSummary = $scope.leftsideTilelist.length > 0 ? $scope.leftsideTilelist[0] : {};
				}else if(activeSummary.includes("adverse")){
					emptySearchedEntitesforadverse();
					$scope.selectedSummary = $scope.leftsideTilelist.length > 0 ? $scope.leftsideTilelist[0] : {};
				}
	        }
	    };	

	$scope.scroll = function(){
		$('.select-count-wrapper').mCustomScrollbar({
					axis: "y",
					theme : "minimal-dark"
				});
			}
	$scope.routeSourceUrl =function(url){
		console.log('url: ', url);
		window.open(url,'_blank');
	}
	$scope.closeShareHolderModal = function(){
		$uibModalInstance.close(changeSignificance);
	}
	function emptySearchedEntitesOfFinance(){
		if (leftSidePanelEntities.length > 0) {
			var financeEntites = _.filter(leftSidePanelEntities, {	'financeCrime': 'finance'});
			$scope.leftsideTilelist = financeEntites.length > 0 ? financeEntites : [];
		}
	}
	function emptySearchedEntitesforadverse() {
		if (leftSidePanelEntities.length > 0) {
			var adverseEntities = _.filter(leftSidePanelEntities, { 'adverseNews': 'adverse' });
			$scope.leftsideTilelist = adverseEntities.length > 0 ? adverseEntities : [];
		}
	}
}	