angular.module('ehubEntityApp')
		.controller('LeadershipAdditionalModalController', LeadershipAdditionalModalController);

LeadershipAdditionalModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'keyStaff',
	'hunterData'
];

function LeadershipAdditionalModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		keyStaff,
		hunterData){

	$scope.entitySearchResult = {
		list: {
			keyStaff:keyStaff,
			hunterData: hunterData
		}
	};
	
	/*
 	*@purpose: close organizations modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}