angular.module('ehubEntityApp')
		.controller('CompanySourcedetailsController', companySourcedetailsController);

companySourcedetailsController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'data1',
	'EHUB_FE_API'
];

function companySourcedetailsController(
		$scope,
		$rootScope,
		$uibModalInstance,
		data1,
		EHUB_FE_API
		){
	
	$scope.complianceSources = data1;
 	console.log(data1);

	/*
 	*@purpose: close organizations modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	$scope.redirectTosourceManagement = function(){
		var url = EHUB_FE_API + 'sourceManagement/#!/sourceManagement';
		window.open(url, '_blank');
	}
 	$(document).ready(function() {
 		/*Custom Scroll Bar*/
 		$('.custom-list.list-scroll').mCustomScrollbar({
			axis: "y"
		});
 	});
 }