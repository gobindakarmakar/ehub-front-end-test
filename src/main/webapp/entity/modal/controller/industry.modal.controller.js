angular.module('ehubEntityApp')
		.controller('IndustryEntityModalController', industryEntityModalController);

industryEntityModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'industryModalData'
];

function industryEntityModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		industryModalData){
	
	$scope.industryModalData = industryModalData;
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}