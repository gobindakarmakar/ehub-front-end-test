angular.module('ehubEntityApp')
		.controller('DomainsCompanyModalController', domainsCompanyModalController);

domainsCompanyModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'domainsCompanyData'
];

function domainsCompanyModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		domainsCompanyData){
	
	$scope.domainsCompanyData = domainsCompanyData;
	/*
 	*@purpose: close domain company modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}