angular.module('ehubEntityApp')
		.controller('BalanceSheetModalController', balanceSheetModalController);

balanceSheetModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'yahooStockData',
	'stockModalData'
];

function balanceSheetModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		yahooStockData,
		stockModalData){
	$scope.yahooStockData = yahooStockData;
	$scope.stockModalData = stockModalData;
	
	console.log(yahooStockData,'yahooStockData', stockModalData, 'stockModalData');
	/*
 	*@purpose: close bad buyers modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}