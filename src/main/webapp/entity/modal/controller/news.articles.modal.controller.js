angular.module('ehubEntityApp')
		.controller('NewsArticlesEntityModalConteoller', newsArticlesEntityModalConteoller);

newsArticlesEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance'
];

function newsArticlesEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance){
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}