angular.module('ehubEntityApp')
		.controller('QuestionsPerAnswersStatusController', questionsPerAnswersStatusController);

	questionsPerAnswersStatusController.$inject = [
		'$scope',
		'$rootScope',
		'$uibModalInstance'
	];

	function questionsPerAnswersStatusController(
		$scope,
		$rootScope,
		$uibModalInstance){
			
		
		/*
	 	*@purpose: close modal
	 	*@created: 12th Jun 2018
	 	*@author: swathi
	 	*/
	 	$scope.closeModal = function(){
	 		$uibModalInstance.close('close');
	 	};
	 	$(document).ready(function(){

	 		if($scope.questionsList.length > 0){
				$('#questionsTable').mCustomScrollbar({
					axis : "y",
					theme : "minimal"
				});
				$('#questionsTable').css('height', '300px');
		 	}
	 	});
	}