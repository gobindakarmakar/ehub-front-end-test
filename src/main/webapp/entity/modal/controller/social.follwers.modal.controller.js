angular.module('ehubEntityApp')
		.controller('SocialFollwersModalController', socialFollwersModalController);

socialFollwersModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'entitySearchResult'
];

function socialFollwersModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		entitySearchResult){
	$scope.entitySearchResult = entitySearchResult;
	/*
 	*@purpose: close Overview Person Wikipedia  modal
 	*@created: 7 Dec 2017
 	*@params: none
 	*@returns: none
 	*@author: Ramsingh
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}