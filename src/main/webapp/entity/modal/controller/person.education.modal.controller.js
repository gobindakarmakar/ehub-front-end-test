angular.module('ehubEntityApp')
		.controller('PersonEducationModalController', personEducationModalController);

personEducationModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'personEducationData'
];

function personEducationModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		personEducationData){
	$scope.personEducation = personEducationData;
	
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}