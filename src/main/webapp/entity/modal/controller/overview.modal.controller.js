angular.module('ehubEntityApp')
		.controller('OverviewModalController', OverviewModalController);

OverviewModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'data1',
	'stockModalData'
];

function OverviewModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		data1,
		stockModalData){
	
	$scope.data = data1;
	console.log(data1);
	$scope.specialitiesList =data1.list['specialities'];
	$scope.connected_industries = data1.list.connected_industries;
	$scope.stockModalData = stockModalData;
	console.log($scope.connected_industries)
	/*
 	*@purpose: close organizations modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
 	$(document).ready(function() {
 		/*Custom Scroll Bar*/
 		$('.custom-modal.related-person-modal .modal-body .entity-result-item').mCustomScrollbar({
 			axis : "y"
 		});
 	});
}