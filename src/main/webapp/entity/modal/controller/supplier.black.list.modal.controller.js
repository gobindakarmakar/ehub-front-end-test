angular.module('ehubEntityApp')
		.controller('SupplierBlackListModalController', supplierBlackListModalController);

supplierBlackListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'supplierBlackData'
];

function supplierBlackListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		supplierBlackData){
	
	$scope.supplierBlackData = supplierBlackData;
	/*
 	*@purpose: close supplier black modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}