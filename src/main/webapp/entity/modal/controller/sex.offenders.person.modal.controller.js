angular.module('ehubEntityApp')
		.controller('SexOffendersPersonModalController', sexOffendersPersonModalController);

sexOffendersPersonModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'sexOffendersPersonData'
];

function sexOffendersPersonModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		sexOffendersPersonData){
	
	$scope.sexOffendersPersonData = sexOffendersPersonData;
	/*
 	*@purpose: close sex offenders modal
 	*@created: 22 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}