angular.module('ehubEntityApp')
		.controller('RealtedPersonEntityModalConteoller', realtedPersonEntityModalConteoller);

realtedPersonEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'personsData'
];

function realtedPersonEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		personsData){
	/*initializing variables*/
	$scope.relatedPersonTab = 'key Person';
	$scope.entityKeyPersonList = personsData.entityKeyPersonList;
	$scope.entityCompanyKeyExecutiveList = personsData.entityCompanyKeyExecutiveList;
	$scope.entityBoardmemberList = personsData.entityBoardmemberList;
	$scope.entityOtherMemberList = personsData.entityOtherMemberList;
	$scope.entityCompanyMemebrs = personsData.entityCompanyMemebrs;

	 /*
	  * @purpose: onClickRelatedPersonTab function to change tab
	  * @created: 22 aug 2017
	  * @params: tabName(string)
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickRelatedPersonTab = function(tabName){
		 $scope.relatedPersonTab = tabName;
	 };
	 /*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}