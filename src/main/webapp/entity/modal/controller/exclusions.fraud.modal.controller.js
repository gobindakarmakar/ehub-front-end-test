angular.module('ehubEntityApp')
		.controller('ExclusionsFraudModalController', exclusionsFraudModalController);

exclusionsFraudModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'exclusionsFraudData'
];

function exclusionsFraudModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		exclusionsFraudData){
	
	$scope.exclusionsFraudData = exclusionsFraudData;
	
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}