angular.module('ehubEntityApp')
		.controller('CrimestoppersukModalController', crimestoppersukModalController);

crimestoppersukModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'crimestoppersukData'
];

function crimestoppersukModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		crimestoppersukData){
	
	$scope.crimestoppersukData = crimestoppersukData;
	/*
 	*@purpose: close crimestoppersukData modal
 	*@created: 20 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}