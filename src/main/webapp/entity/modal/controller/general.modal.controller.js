angular.module('ehubEntityApp')
		.controller('GeneralEntityModalConteoller', generalEntityModalConteoller);

generalEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'generalData'
];

function generalEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		generalData){
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
 	$scope.generalEntityData = {};
 	$scope.disableSearchButton = true;
 	angular.forEach(generalData, function(value, key){
 		if(typeof value === 'string' && value !== '' && value){
 			var newKey = key.replace(/_/g,' ');
 			$scope.generalEntityData[newKey] = value;
 		}
 	});
	$scope.disableSearchButton = false;
}