angular.module('ehubEntityApp')
		.controller('RelatedOrganizationPersonModalController', relatedOrganizationPersonModalController);

relatedOrganizationPersonModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'relatedOrgPersonData'
];

function relatedOrganizationPersonModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		relatedOrgPersonData){
	/*initializing variables*/
	$scope.relatedOrgTab = 'Membership';
	$scope.entityMembershipList = relatedOrgPersonData.memberships;
	$scope.entityBoardMembershipList = relatedOrgPersonData.board_memberships;

	 /*
	  * @purpose: onClickRelatedOrgTab function to change tab
	  * @created: 22 aug 2017
	  * @params: tabName(string)
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickRelatedOrgTab = function(tabName){
		 $scope.relatedOrgTab = tabName;
	 };
	 /*
 	*@purpose: close related org modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}