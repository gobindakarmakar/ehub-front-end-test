angular.module('ehubEntityApp')
		.controller('RelatedPersonOrgController', relatedPersonOrgController);

relatedPersonOrgController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'personOrgData'
];

function relatedPersonOrgController(
		$scope,
		$rootScope,
		$uibModalInstance,
		personOrgData){
	$scope.personOrgData = personOrgData;
	 /*
 	*@purpose: close related information modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
 	 /*
 	*@purpose: click ok 
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.ok = function(){
 		$uibModalInstance.close('success');
 	};
}