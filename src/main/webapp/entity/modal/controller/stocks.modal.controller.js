angular.module('ehubEntityApp')
		.controller('StocksEntityModalConteoller', stocksEntityModalConteoller);

stocksEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'stockModalData',
	'yahooStockData'
];

function stocksEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		stockModalData,
		yahooStockData){
	$scope.stockModalData = stockModalData;
	$scope.yahooStockData = yahooStockData;
	console.log($scope.yahooStockData);
	console.log($scope.stockModalData);
	$scope.items = [];
	
	$scope.close  = function(){
		$uibModalInstance.dismiss('close');
	}
	if(stockModalData !== false){
	var balanceSheetData = [];
	
	angular.forEach(stockModalData.balance_sheet[Object.keys(stockModalData.balance_sheet)[0]], function(value, key){
		balanceSheetData.push({
			model: key
		});
	});
	angular.forEach(balanceSheetData, function(modelData, modelKey){
		angular.forEach(stockModalData.balance_sheet, function(sheetData, sheetKey){
			angular.forEach(stockModalData.balance_sheet[sheetKey], function(value, key){
				if(key === modelData.model){
					modelData[sheetKey] = value.replace('-','');
				}
			});	
		});
	});
	
	
	var cash_flow_statement = [];
	angular.forEach(stockModalData.cash_flow_statement[Object.keys(stockModalData.cash_flow_statement)[0]], function(value, key){
		cash_flow_statement.push({
			model: key
		});
	});
	angular.forEach(cash_flow_statement, function(modelData, modelKey){
		angular.forEach(stockModalData.cash_flow_statement, function(sheetData, sheetKey){
			angular.forEach(stockModalData.cash_flow_statement[sheetKey], function(value, key){
				if(key === modelData.model){
					modelData[sheetKey] = value.replace('-','');
				}
			});	
		});
	});
	
	

	var income_statement = [];
	angular.forEach(stockModalData.income_statement[Object.keys(stockModalData.income_statement)[0]], function(value, key){
		if(key!="eps"&&key!="eps_diluted")
			{
		income_statement.push({
			model: key
		});}
	});
	angular.forEach(income_statement, function(modelData, modelKey){
		angular.forEach(stockModalData.income_statement, function(sheetData, sheetKey){
			angular.forEach(stockModalData.income_statement[sheetKey], function(value, key){
				if(key === modelData.model){
					modelData[sheetKey] = value.replace('-','');
				}
			});	
		});
	});
	
//	setTimeout(function(){

		

		
//function tonums(a){
//
//			if(a=="0.00"){
//				return 0;
//			}	
//			
//		if(a.split("m").length>1){
//			
//			return (parseFloat(a.split("m")[0])*1000000)};
//		if(a.split("b").length>1){
//			
//			return (parseFloat(a.split("b")[0])*1000000000)}
//
//		}



//		
//		var options = {
//					container: ".stock_bar_chart-balance_sheet",
//					header: "BALANCE SHEET STOCK",
//					height: "300" ,
//					data:balanceSheetData}
//		var options2 = {
//		        container: ".stock_bar_chart-cash_flow_statement",
//		        header: "CASH FLOW STATEMENT STOCK",
//		        height: "300" ,
//		        data:cash_flow_statement}
//		var options3 = {
//        container: ".stock_bar_chart-income_statement",
//        header: "INCOME STATEMENT STOCK",
//        height: "300" ,
//        data:income_statement}
//		loadgroupedColumnChart(options);
//		loadgroupedColumnChart(options2);
//		loadgroupedColumnChart(options3);
//		  function loadgroupedColumnChart(options) {
//       
//            columnData = handleData(options.data, "model", "", "bar");
//            options.data = columnData;
//           
//             groupedColumChart(options);
//
//        
//    }
//		 
//		  
//		  
///*--------------------------------------------------------------------------------------------------------------*/
//    /**
//     *Function to handle data
//     */
//    function handleData(data, x, y, type) {
//        var str = JSON.stringify(data);
//      
//        str = str.replace(/model/g, 'x');
//        var object = JSON.parse(str);
//      return object;
//    }
//		
//    function groupedColumChart(barOptions) {
//        if (barOptions.container) {
//            $(barOptions.container).empty();
//        }
//        /*--------------------------Initialize Values-----------------------------*/
//        if (barOptions) {
//            this.container = barOptions.container ? barOptions.container : "body"
//            this.barColor = barOptions.barColor ? barOptions.barColor : "blue"
//            this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false
//            this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
//            this.data = (barOptions.data) ? barOptions.data : []
//            this.showTicks = barOptions.showTicks ? barOptions.showTicks : true;
//            this.ticks = barOptions.ticks ? barOptions.ticks : 'all';
//            this.showLegends = (barOptions.showLegends !== undefined) ? barOptions.showLegends : false;
//            this.xLabelRotation = barOptions.xLabelRotation ? barOptions.xLabelRotation : 0;
//            this.yLabelRotation = barOptions.yLabelRotation ? barOptions.yLabelRotation : 0;
//            this.margin = barOptions.margin ? {
//                top: barOptions.margin.top ? barOptions.margin.top : 20,
//                right: barOptions.margin.right ? barOptions.margin.right : 20,
//                bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
//                left: barOptions.margin.left ? barOptions.margin.left : 40
//            } : {top: 20, right: 20, bottom: 50, left: 50};
//
//            this.showAxisX = (barOptions.showAxisX !== undefined) ? barOptions.showAxisX : true;
//            this.showAxisY = (barOptions.showAxisY !== undefined) ? barOptions.showAxisY : true;
//            this.showXaxisTicks = barOptions.showXaxisTicks !== undefined ? barOptions.showXaxisTicks : true;
//            this.showYaxisTicks = barOptions.showYaxisTicks !== undefined ? barOptions.showYaxisTicks : true;
//            this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
//            this.randomIdString = Math.floor(Math.random() * 10000000000);
//            this.groupedColumnHeader = barOptions.header ? barOptions.header : "GROUPED STACKED CHART";
//            this.height = barOptions.height ? barOptions.height : 600;
//            /*this.ytext = barOptions.ytext ? barOptions.ytext : "Usability Rate";
//            this.xtext = barOptions.xtext ? barOptions.xtext : "Vehical Model";*/
//            this.legendColor = barOptions.legendColor ? barOptions.legendColor : false;
//            this.headerOptions = barOptions.headerOptions == false ? barOptions.headerOptions : true;
//
//
//        } else {
//            return false;
//        }
//        
//        var randomSubstring = this.randomIdString;
//        var actualOptions = jQuery.extend(true, {}, barOptions);
//        var h1 = this.height;
//        var h = parseInt(h1) + 150;
//        var header = this.groupedColumnHeader;
//        var _this = this;
//        var modalwidth = $(window).width() - 100;
//        var modal = ' <div id="modal_' + randomSubstring + '"class="modal fade " tabindex="-1" role="dialog"> <div class="modal-dialog modal-lg" style="width:' + modalwidth + 'px"> <form ><div class="modal-content"><div class="modal-body"  style="padding:0;background-color:#334d59" id="modal_chart_container' + randomSubstring + '"></div></div></form></div></div>';
//
//        var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 100%; margin: auto; margin-top: 0px; font-size: 14px;font-style: inherit;"> <div class="graphBox" style="height:' + h + 'px;max-height: ' + h + 'px;min-height: ' + h + 'px;margin: auto; width: 100%;left: 0px;top: 0px;position: relative;"> <div class="headerDiv" style="font-weight:bold;text-align: left;color: #239185; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + header + '</div><div id="groupedColumn_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'
//        /*var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 80%; margin: auto; margin-top: 30px; font-size: 14px;font-family: roboto-regular;"> <div class="graphBox" style="margin: auto; background-color: #374c59; width: 100%;left: 0px;top: 0px;overflow: hidden;position: relative;"> <div class="headerDiv" style="font-weight:bold;background-color: #425661;text-align: left;color: #239185; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + barOptions.header + '</div><div id="groupedColumn_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'*/
//        $(this.container).html(chartContainerdiv);
//        $(this.container).append(modal);
//        var chart_container = "#groupedColumn_chart_div" + randomSubstring;
//
//        this.width = barOptions.width ? barOptions.width : $(chart_container).width() - 10;
//        var groupedStacked = this.groupedStacked;
//        var margin = this.margin,
//                width = this.width - margin.left - margin.right,
//                height = this.height - margin.top - margin.bottom,
//                barColor = this.barColor;    
//        $(chart_container).parent().find(".headerDiv").append("<div style='float:right;font-size:14px;' class='groupedbarlegendContainerDiv legend-container' id='legendContainer-" + (randomSubstring) + "'></div>")
//
//        var svg = d3.select(chart_container)
//                .append("svg")
//                .attr('height', this.height)
//                .attr('width', this.width)
//                .attr('id', 'mainSvg-' + randomSubstring);
//
//        $(".grouped_bar_tooltip").remove();
//        var tool_tip = $('body').append('<div class="grouped_bar_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: show;"></div>');
//
//        
//        $(".grouped_bar_tooltip1").remove();
//        var tool_tip1 = $('body').append('<div class="grouped_bar_tooltip1" </div>');
//
//        var x0 = d3.scaleBand().rangeRound([0, width]).paddingInner(0.2),
//                y = d3.scaleLinear().rangeRound([height, 0]),
//                x1 = d3.scaleBand().padding(0.2);
//
//        var colorScale = d3.scaleOrdinal().range(["#3990CF", "#1DBF6A", "#E1C433", "#E61874", "#E58B19"]);
//
//        var g = svg.append("g")
//                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
//                .attr('id', 'mainGroup-' + randomSubstring);
//
//        var columnData = this.data;
//        if (columnData) {
//            if (columnData.length)
//                drawColumns(columnData, this.barColor);
//        } else {
//        }
//
///*        --------------------------------------------------------------------------*/
//        /**
//         * 
//         * @param {array} data
//         * @returns {undefined}Function to plot bars
//         */
//        function drawColumns(data) {
//
//            var clip = g.append("defs").append("svg:clipPath")
//                    .attr("id", "clip")
//                    .append("svg:rect")
//                    .attr("id", "clip-rect")
//                    .attr("x", "0")
//                    .attr("y", "0")
//                    .attr("width", width)
//                    .attr("height", height);
//            var keys = d3.keys(data[0]);
//            var stack = d3.stack()
//                    .keys(keys)
//                    /*.order(d3.stackOrder)*/
//                    .offset(d3.stackOffsetNone);
//
//            var layers = stack(data);
//            x0.domain(data.map(function (d) {
//                return d.x;
//            }));
//            
//            x1.domain(keys).rangeRound([0, x0.bandwidth()]);
//
//            var x_g = g.append("g")
//                    .attr("class", "axis axis--x")
//                    .attr("transform", "translate(0," + height + ")")
//                    .attr('id', 'xAxis-' + randomSubstring)
//                    .call(d3.axisBottom(x0).tickFormat (function (d){ 
//            			return capitalizeFirstLetter(d.replace(/_/g,' '));
//            			 }));
//            x_g.append("text")
//                    .attr("transform", "translate(" + (width / 2) + "," + 30 + ")")
//                    .attr("x", 8).attr("y", 10)
//                    .attr("dx", ".71em")
//                    .style("text-anchor", "middle")
//                    .style("fill", "#6c7e88")
//                    .text(_this.xtext).style("font-size", "14px");
//            x_g.selectAll("path").style("stroke", "#6c7e88")
//                    .style("shape-rendering", "crispEdges")
//                    .style("fill", "none");
//            x_g.selectAll("line").style("stroke", "#6c7e88")
//                    .style("shape-rendering", "crispEdges")
//                    .style("fill", "none");
//            x_g.selectAll("text").style("fill", "#6c7e88")
//                    .style("font-size", "10px")
//                    .style("stroke", "none")
//           
//            .attr("transform", "rotate(45)")
//            .style("text-anchor", "start");
//
//            y.domain([0, d3.max(data, function (d) {
//                    return d3.max(keys, function (key) {
//                    	if(key!="x")
//                    		{
//                    	d[key] =tonums(d[key])
//                        return d[key];
//                    		}
//                    });
//                })]);
//
//            var formatNumber = d3.format(".0f")
//                       var y_g = g.append("g")
//                    .attr("class", "axis axis--y")
//                    .attr('id', 'yAxis-' + randomSubstring)
//                    .call(d3.axisLeft(y).ticks(5)
//                    		.tickFormat (function (d) { 
//                    			return formatNumber(d / 1e9) + "B"; }));
//           
//            y_g.append("text")
//                    .attr("transform", "rotate(-90)")
//                    .attr("y", -margin.left)
//                    .attr("dy", "1.1em")
//                    .attr("dx", -20)
//                    .style("text-anchor", "end")
//                    .style("fill", "#6c7e88")
//                    .text(_this.ytext).style("font-size", "14px");
//
//            y_g.selectAll("path").style("stroke", "#6c7e88")
//                    .style("shape-rendering", "crispEdges")
//                    .style("fill", "none");
//            y_g.selectAll("line").style("stroke", "#6c7e88")
//                    .style("shape-rendering", "crispEdges")
//                    .style("fill", "none");
//            y_g.selectAll("text").style("fill", "#6c7e88")
//                    .style("font-size", "10px")
//                    .style("stroke", "none");
//            var groupedG = g.append("g")
//                    .selectAll("g")
//                    .data(data)
//                    .enter().append("g").attr("class", "groupg")
//                    .on("mouseover", function (d) {
//                    	
//                    	 var formatNumber = d3.format(".0f")
//                        $(this).find(".background_rect").css("fill", "#777");
//                        $(".grouped_bar_tooltip").empty();
//                        var keys = d3.keys(d)
//
//                        var text = '<span>' + capitalizeFirstLetter(d.x.replace(/_/g,' ')) + '</span><table style = "color: #7d94a5; border-collapse: collapse;font-size: 12px;min-width: 50px">';
//                        for (var i in keys) {
//                        	if(keys[i]!="x"){
//                            text += '<tr><td style="font-size: 12px; min-width: 50px;color: #7d94a5;">' + keys[i] + '</td><td>' + formatNumber(d[keys[i]]/ 1e9) + "B" + '</td></tr>';
//                        	}
//                    	}
//                        text += '</table>';
//                        $(".grouped_bar_tooltip").html('<div class="custom-tooltip right"><div class="tooltip-arrow"></div><div class="tooltip-inner"><p style="color: #7d94a5; !important">'+text+'</p></div></div>');
//                        return $(".grouped_bar_tooltip").css("visibility", "visible").css("background-color", "transparent");
//
//                    })
//                    .on("mousemove", function () {
//                        $(".grouped_bar_tooltip").css("top", (d3.event.pageY - 10) + "px")
//                        return  $(".grouped_bar_tooltip").css("left", (d3.event.pageX + 10) + "px");
//
//                    })
//                    .on("mouseout", function () {
//                        //hide tool-tip
//                        $(this).find(".background_rect").css("fill", "transparent");
//                        return $(".grouped_bar_tooltip").css("visibility", "hidden");
//                    })
//                    .attr("transform", function (d) {
//                        return "translate(" + x0(d.x) + ",0)";
//                    });
//            groupedG.append("rect").attr("class", "background_rect")
//                    .attr("x", 0).attr("y", 0).attr("width", x0.bandwidth()).attr("height", height).attr("fill", "transparent").attr("opacity", 1);
//            var prevVal = 0;
//            var rect = groupedG.selectAll(".rect_" + randomSubstring)
//                    .data(function (d) {
//                        prevVal = 0;
//                        return keys.map(function (key, i) {
//                            var curentObj = {key: key, value: d[key], value0: prevVal};
//                            prevVal = prevVal + d[key];
//                            return curentObj;
//                        });
//                    })
//
//                    .enter().append("rect").attr("class", function (d) {
//                return "rect_" + randomSubstring + " rect_" + randomSubstring + "_" + d.key
//            }).attr("clip-path", "url(#clip)")
//                    .attr("x", function (d) {
//                        return x1(d.key);
//                    })
//                    .attr("y", height)
//
//                    .attr("rx", (x1.bandwidth() / 4))
//                    .attr("ry", (x1.bandwidth() / 4))
//                    .attr("width", x1.bandwidth())
//
//                    .attr("height", function (d) {
//                        return height - y(d.value);
//                    })
//                    .attr("fill", function (d) {
//                        return colorScale(d.key);
//                    })
//                    .transition().duration(400).ease(d3.easeLinear)
//                    .attr("y", function (d) {
//                        return y(d.value);
//                    });
//            $('input[type=radio][name=mode]').change(function () {
//                if ($(this).val() == "grouped") {
//                    plotGroupedBars();
//                } else {
//                    plotStackedChart();
//                }
//            });
//
//            if (groupedStacked != "grouped") {
//                plotStackedChart();
//            }
//            function plotGroupedBars() {
//
//                y.domain([0, d3.max(data, function (d) {
//                        return d3.max(keys, function (key) {
//                            return d[key];
//                        });
//                    })]);
//                d3.selectAll('#yAxis-' + randomSubstring).call(d3.axisLeft(y).ticks(8, "s"));
//                var y_g = d3.selectAll('#yAxis-' + randomSubstring);
//                y_g.selectAll("path").style("stroke", "#6c7e88")
//                        .style("shape-rendering", "crispEdges")
//                        .style("fill", "none");
//                y_g.selectAll("line").style("stroke", "#6c7e88")
//                        .style("shape-rendering", "crispEdges")
//                        .style("fill", "none");
//                y_g.selectAll("text").style("fill", "#6c7e88")
//                        .style("font-size", "10px")
//                        .style("stroke", "none");
//                d3.selectAll(".rect_" + randomSubstring).transition()
//                        .duration(300)
//                        .delay(function (d, i) {
//                            return i * 10;
//                        })
//                        .attr("x", function (d) {
//                            return x1(d.key);
//                        })
//                        .attr("width", x1.bandwidth())
//                        .transition()
//                        .attr("y", function (d) {
//                            return y(d.value);
//                        })
//                        .attr("height", function (d) {
//                            return height - y(d.value);
//                        }).attr("rx", (x1.bandwidth() / 4))
//                        .attr("ry", (x1.bandwidth() / 4))
//
//            }
//            function plotStackedChart() {
//
//                y.domain([0, d3.max(layers[layers.length - 1], function (d) {
//
//                        return d[0] > d[1] ? d[0] : d[1];
//                    })]).nice();
//                d3.selectAll('#yAxis-' + randomSubstring).call(d3.axisLeft(y).ticks(8, "s"));
//                var y_g = d3.selectAll('#yAxis-' + randomSubstring);
//                y_g.selectAll("path").style("stroke", "#6c7e88")
//                        .style("shape-rendering", "crispEdges")
//                        .style("fill", "none");
//                y_g.selectAll("line").style("stroke", "#6c7e88")
//                        .style("shape-rendering", "crispEdges")
//                        .style("fill", "none");
//                y_g.selectAll("text")
//                        .style("fill", "#6c7e88")
//                        .style("font-size", "10px")
//                        .style("stroke", "none");
//                d3.selectAll(".rect_" + randomSubstring).transition()
//                        .duration(300)
//                        .delay(function (d, i) {
//                            return i * 10;
//                        })
//                        .attr("y", function (d) {
//                            return y(d.value + d.value0);
//                        })
//                        .attr("height", function (d) {
//                            return y(d.value0) - y(d.value + d.value0);
//                        })
//                        .transition()
//                        .attr("x", function (d, i) {
//                            if (x0.bandwidth() > 20) {
//                                return (x0.bandwidth()) / 4;
//                            } else {
//                                return 0;
//                            }
//                        })
//                        .attr("width", function () {
//                            if (x0.bandwidth() > 20) {
//                                return  x0.bandwidth() / 2;
//                            } else {
//                                return  x0.bandwidth();
//                            }
//                        })
//                        .attr("rx", 0)
//                        .attr("ry", 0);
//
//            }
//
//            renderLegend(keys, randomSubstring);
//        }
//
///*        ------------------------------------------------------------------------*/
//        function renderLegend(labelObject) {
//        	
//        	labelObject.pop()
//        		
//            var numberOfLegends = labelObject.length;
//            var legenddiv = d3.select('#legendContainer-' + randomSubstring)
//
//    /*holder for each legend*/
//            var legendHolder = legenddiv.selectAll('.legend-holder')
//                    .data(labelObject)
//                    .enter()
//                    .append('div')
//                    .attr('class', function (d) {
//                        return 'legend-holder legend_holder_div_' + randomSubstring + '_' + d;
//                    })
//                    .style('display', 'inline-block')
//                    .style('padding', '2px')
//                    .style('opacity', '1')
//                    .style('cursor', 'pointer')
//                    .on("click", function () {
//                        var current_div_class = $(this).attr("class");
//                        var rectClass = "rect_" + current_div_class.split(" ")[1].split("legend_holder_div_")[1];
//                        // Get  current div opacity
//                        var current_opacity = ($(this).css("opacity"));
//
//                        newOpacity = current_opacity == 1 ? 0.5 : 1;
//                        $(this).css("opacity", newOpacity);
//                        $("." + rectClass).css("opacity", newOpacity == 0.5 ? 0.2 : newOpacity);
//
//                    });
//    /*append legend circles*/
//            legendHolder
//                    .append('div')
//                    .style('background-color', function (d, i) {
//                        return colorScale(d)
//
//                    }).attr("class", "bar_legend_circles")
//                    .style("height", "10px")
//                    .style("width", "10px")
//                    .style("border-radius", "6px")
//                    .style("display", "inline-block");
//
//    /*append legend text*/
//            legendHolder.append("p").text(function (d, i) {
//                return d;
//            }).style('color', function (d, i) {
//                if (_this.legendColor) {
//                    return colorScale(d)
//                } else {
//                    return "rgb(108, 126, 136)";
//                }
//
//            }).style('display', 'inline-block').style('margin-left', '5px');
//
//        }
//   
//        function capitalizeFirstLetter(string) {
//            return string.charAt(0).toUpperCase() + (string.slice(1)).toLowerCase();
//        }
//
//
//    }
    
    
  
//	},0);
	}
	
}

