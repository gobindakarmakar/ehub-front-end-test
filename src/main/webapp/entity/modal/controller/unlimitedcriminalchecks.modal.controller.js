angular.module('ehubEntityApp')
		.controller('UnlimitedcriminalchecksModalController', unlimitedcriminalchecksModalController);

unlimitedcriminalchecksModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'unlimitedcriminalchecksData'
];

function unlimitedcriminalchecksModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		unlimitedcriminalchecksData){
	
	$scope.unlimitedcriminalchecksData = unlimitedcriminalchecksData;
	/*
 	*@purpose: close unlimitedcriminalchecksData modal
 	*@created: 20 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}