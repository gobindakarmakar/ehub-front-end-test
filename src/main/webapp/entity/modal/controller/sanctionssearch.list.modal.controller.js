angular.module('ehubEntityApp')
		.controller('SanctionssearchlistModalController', sanctionssearchlistModalController);

sanctionssearchlistModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'sanctionssearchData'
];

function sanctionssearchlistModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		sanctionssearchData){
	
	$scope.sanctionssearchData = sanctionssearchData;
	/*
 	*@purpose: close sanctionssearch modal
 	*@created: 22 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}