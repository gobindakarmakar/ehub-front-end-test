angular.module('ehubEntityApp')
		.controller('NewsEntityModalConteoller', newsEntityModalConteoller);

newsEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModal',
	'$uibModalInstance',
	'newsData'
];

function newsEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModal,
		$uibModalInstance,
		newsData){
	$scope.newsArticlesData = newsData;
	
	 /*
	  *@purpose: load entity visualiser modal
	  *@created: 18 aug 2017
	  *@params: details(string), title(string), url(string)
	  *@returns: none
	  *@author: swathi
	  */
	 $scope.showBstNamedEntity = function(details, title, url){
		 var openEntityVisualiserModal = $uibModal.open({
	            templateUrl: 'static/html/modal/entity.visualiser.modal.html',
	            controller: 'entityVisualiserController',
	            size: 'lg',
	            windowClass: 'entity-visualiser-modal update-entities-modal related-person-modal',
	            resolve:{
	            	getDetails: function(){
	            		return details;
	            	},
			 		getTitle: function(){
			    		return title;
			    	},
			    	getUrl: function(){
	            		return url;
	            	}
	            }
	        });

	 		openEntityVisualiserModal.result.then(function (response) {
	        }, function (reject) {
	        });
	 };
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}