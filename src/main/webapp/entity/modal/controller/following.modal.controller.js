angular.module('ehubEntityApp')
		.controller('FollowingEntityModalController', followingEntityModalController);

followingEntityModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'followingModalData'
];

function followingEntityModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		followingModalData){
	$scope.followingData = followingModalData;
	/*
 	*@purpose: close following modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}