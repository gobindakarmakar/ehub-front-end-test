angular.module('ehubEntityApp')
		.controller('RiskOffenceModalController', riskOffenceModalController);

riskOffenceModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'entitySearchResult'
];

function riskOffenceModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		entitySearchResult){
	$scope.entitySearchResult = entitySearchResult;
	/*
 	*@purpose: close Overview Person Wikipedia  modal
 	*@created: 7 Dec 2017
 	*@params: none
 	*@returns: none
 	*@author: Ramsingh
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}