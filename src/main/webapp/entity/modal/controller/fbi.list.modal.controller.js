angular.module('ehubEntityApp')
		.controller('FBIlistModalController', fBIlistModalController);

fBIlistModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'fbiData'
];

function fBIlistModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		fbiData){
	
	$scope.fbiData = fbiData;
	/*
 	*@purpose: close fbi modal
 	*@created: 22 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}