'use strict';
elementApp
	   .controller('EntityDataModalController', entityDataModalController);

       entityDataModalController.$inject = [
			'$scope',
			'$rootScope',
			'data',
			'$uibModalInstance',
			'$timeout',
			'EntityApiService',
			'EHUB_FE_API',
			'$filter',
			'TopPanelApiService',
			'EntityorgChartService'
        ];
        	/*
	         * @purpose:Set Entity data in modal
	         * @created: 21-February-2019
	         * @author:Amarjith
	        */
	    function entityDataModalController(
	    		$scope,
	    		$rootScope,
	    		data,
	    		$uibModalInstance,
	    		$timeout,
                EntityApiService,
				EHUB_FE_API,
				$filter,
				TopPanelApiService,
				EntityorgChartService
								){
			$scope.$parent.pdfLoader = false;
	    	$scope.oneAtATime = false;
			$scope.primarySource = [];
			$scope.secondarySource = [];
			$scope.userInputSource = null;
			$scope.userModifiedBy = '';
			$scope.userModifiedOn = '';
			$scope.userModifiedValue = '';
			$scope.textArea = {userModifiedValue : ''};
			$scope.overrideSource = '';
			$scope.overrideSrcValue = '';
			$scope.isUserdataSelected = false;
			$scope.modaldata =data;
			$scope.schema = data.schema;
			$scope.sourceUrlList = false;
			$scope.originalAns = data.source ? data.source.preValue ? ($scope.$parent.isJson(data.source.preValue) ? JSON.parse(data.source.preValue).toString() : data.source.preValue) : '' : '';
			$scope.dataPopObject = {
				overideSearchedInputSourceEdit:'',
				overideSearchedInputURLEdit:'',
				overideSearchedInputDateEdit:''
			};
			$scope.accordian ={
				primaryOpen : false,
				secondaryOpen : false
			};
			$scope.sourceList_forModal = [];
			$scope.sourceList_forModal = jQuery.extend(true, [], $scope.sourceList);
			$scope.title = data.title;
			$scope.disableInput = false;
			$scope.showAddbutton = false;
			$scope.isJson = function(str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			};
						
			var slectedDoc_DocId = '';
			function getKeyByValue(object, value) {
				
				return Object.keys(object).find(key => object[key] ===  value[value.indexOf(object[key])]);
			  }
			
			if(data.source.isUserData){			
				$scope.disableInput = false;	
				$scope.userModifiedBy = $scope.$parent.isJson(data.source.modifiedBy) ? JSON.parse(data.source.modifiedBy) : data.source.modifiedBy;
				$scope.userModifiedOn = data.source.modifiedOn;
				$scope.dataPopObject.overideSearchedInputSourceEdit=data.source.source?data.source.source:data.source.modifiedBy;
				$scope.overideSearchedInputSourceEditVal= data.source.source?data.source.source:data.source.modifiedBy;
				$scope.dataPopObject.overideSearchedInputURLEdit = data.source.sourceUrl?data.source.sourceUrl:"";
				var splittedvalue = data.source.publishedDate?data.source.publishedDate.split(' ') :"";
				$scope.dataPopObject.overideSearchedInputDateEdit  = splittedvalue?splittedvalue[0]:"";
				if(data.source.sourceType && data.source.sourceType !== ""){
					$scope.sourceTypeName = data.source.sourceType;
				}else if(data.source.userSource){
					$scope.sourceTypeName = data.source.userSource.split('.')[1];
				}

				if(!data.source.sourceUrl){
					$scope.dataPopObject.overideSearchedInputSourceEdit='';
					$scope.overideSearchedInputSourceEditVal= '';
					$scope.dataPopObject.overideSearchedInputURLEdit = '';
					$scope.dataPopObject.overideSearchedInputDateEdit  = '';
				}
				if(data.schema == "bst:aka"){					
					$scope.userModifiedValue =  data.source.value.toString();
					if(!data.source.preValue){
						data.source.preValue = '';
					}
					$scope.originalAns = $scope.isJson(data.source.preValue) ? JSON.parse(data.source.preValue).toString() : data.source.preValue.toString();
				}else if(data.schema == 'bst:stock_info'){
					$scope.userModifiedValue = data.source.value.main_exchange; //+value.main_exchange"_"+ data.source. ;
					if(!data.source.preValue){
						data.source.preValue = {};
						data.source.preValue.main_exchange = '';
					}
					$scope.originalAns = data.source.preValue.main_exchange;
				}else{
					if(data.source && data.source.value){
						$scope.userModifiedValue = data.source.value.replace(/(\r\n|\n|\r)/gm,",");
					}
					if(!data.source.preValue){
						data.source.preValue = '';
					}
					$scope.originalAns = data.source.preValue;
				}
				$scope.userModifiedValue = $scope.$parent.isJson($scope.userModifiedValue) ? JSON.parse($scope.userModifiedValue) : $scope.userModifiedValue;
				$scope.overrideValue = $scope.userModifiedValue ;
				$scope.textArea.userModifiedValue = $scope.userModifiedValue.toString();
				$scope.showAddbutton = true;
				$scope.showSavebtnwithclass = true;
			}else{	
				if(data.schema == "bst:aka"){					
					$scope.overrideValue = data.source.source //+"_"+data.source.value [0];
				}else if(data.schema == 'bst:stock_info'){
					$scope.overrideValue = data.source.source //+"_"+ data.source.value.main_exchange ;
				}
				else{
					$scope.overrideValue = data.source.source //+"_"+  data.source.value ;
				}
				// $scope.userModifiedBy = data.source.modifiedBy;
				$scope.userModifiedBy = $scope.$parent.isJson(data.source.modifiedBy) ? JSON.parse(data.source.modifiedBy) : data.source.modifiedBy;
				$scope.userModifiedOn = data.source.modifiedOn;
				$scope.userModifiedValue = 	data.source.userValue;
				$scope.overrideValue = $scope.$parent.isJson($scope.overrideValue) ? JSON.parse($scope.overrideValue) : $scope.overrideValue;
				$scope.showAddbutton = false;
				$scope.showSavebtnwithclass = false;
				$scope.userModifiedValue = $scope.$parent.isJson($scope.userModifiedValue) ? JSON.parse($scope.userModifiedValue) : $scope.userModifiedValue;
				if($scope.userModifiedValue && $scope.userModifiedValue.toString()){
					$scope.showAddbutton = true;
					$scope.showSavebtnwithclass = true;
					$scope.textArea.userModifiedValue = $scope.$parent.isJson(data.source.userValue) ?  JSON.parse(data.source.userValue) : data.source.userValue;
					$scope.dataPopObject.overideSearchedInputSourceEdit=data.source.userSource?data.source.userSource:'';
					$scope.overideSearchedInputSourceEditVal= data.source.userSource?data.source.userSource:'';
					$scope.dataPopObject.overideSearchedInputURLEdit = data.source.userSourceUrl?data.source.userSourceUrl:"";
					var splittedvalue = data.source.userPublishedDate?data.source.userPublishedDate.split(' ') :"";
					$scope.dataPopObject.overideSearchedInputDateEdit  = splittedvalue?splittedvalue[0]:"";
					$scope.disableInput = true;
				}
			}

			if(data.source.primarySource && data.source.primarySource.length > 0){
				data.source.primarySource = _.uniq(data.source.primarySource);

				data.source.primarySource.forEach(function (d) {	$scope.primarySource.push({
									source: d.source,
									sourceDisplayName: d.sourceDisplayName ? d.sourceDisplayName: '',
									sourceUrl: d.sourceUrl,
									value : $scope.isJson(d.value) ? JSON.parse(d.value) : d.value 
								});
				});

			}
			if(data.source.secondarySource && data.source.secondarySource.length > 0 && _.compact(data.source.secondarySource).length > 0){
				data.source.secondarySource = _.uniq(data.source.secondarySource);
				data.source.secondarySource.forEach(function (d) {
						$scope.secondarySource.push({
							source: d.source,
							sourceDisplayName: d.sourceDisplayName ? d.sourceDisplayName: '',
							sourceUrl: d.sourceUrl,
							value : $scope.isJson(d.value) ? JSON.parse(d.value) : d.value
						});

				});
			}

			if($scope.overrideValue){
				if(_.findIndex($scope.primarySource,function(d){ return d.source == $scope.overrideValue}) >= 0){
					$scope.accordian.primaryOpen = !$scope.accordian.primaryOpen;
				}else if( _.findIndex($scope.secondarySource,function(d){ return d.source == $scope.overrideValue}) >= 0){
					$scope.accordian.secondaryOpen = !$scope.accordian.secondaryOpen;
				}
			}

	    	if(data.name){
	    		$scope.header=data.name;
	    	}
	    	else if(data.secondary){
	    		$scope.header='Secondary Links';
	    	} else{
	    		$scope.header='Primary Links';
	    	}
	    	$scope.closeDataPopUp=function(){
	    		$uibModalInstance.close();
			};

			$scope.showSavebtnwithclass = false;
			$scope.overridesourceUrl = '';
			$scope.source_DisplayName = '';
			$scope.changeOverrideValue =  function(source, value, sourceUrl,source_DisplayName){	
				$scope.overrideSrcValue = value;
				if(source){
					$scope.overrideSource = source;
					$scope.isUserdataSelected = true;
					$scope.disableInput = true;
					$scope.showSavebtnwithclass = false;
					$scope.overridesourceUrl = sourceUrl;
					$scope.source_DisplayName = source_DisplayName ? source_DisplayName : '';
				}else{
					$scope.overrideSource = '';
					$scope.isUserdataSelected = false;
					$scope.disableInput = false;
					$scope.showSavebtnwithclass = true;
				}
			};
			
      $scope.overRide = function(){
				var saveEntityData = {};
				if($scope.overrideSource){
					saveEntityData = {
						"source": $scope.overrideSource,
						"sourceUrl": $scope.overridesourceUrl ? $scope.overridesourceUrl : '',
						"value": $scope.overrideSrcValue.toString(),
						"sourceSchema": data.schema,
						"entityId": $rootScope.entityObj['@identifier'],
						"isUserData":false,
						"sourceDisplayName": $scope.source_DisplayName,
						"sourceType":$scope.sourceTypeName ? $scope.sourceTypeName : 'link',
						"docId" : ''
					};
				}
				else{
					var sourceName = $scope.dataPopObject.overideSearchedInputSourceEdit;
					var prev_val = $scope.$parent.ceriSearchResultObject[data.schema].value;

					if(data.schema && data.schema == "bst:aka"){
						if(!$scope.isJson(prev_val)){
							prev_val = JSON.stringify(prev_val);
						}
					}
					// var prev_date = $scope.$parent.ceriSearchResultObject[data.schema].modifiedOn;
					// var prev_isuserdata = $scope.$parent.ceriSearchResultObject[data.schema].isUserData;
					var src_type = $scope.sourceTypeName;
					if($scope.showAddNew){ // $scope.showAddNew if custom source and sourceurl is added by the user
						src_type = "link";
					}
					saveEntityData = {
						"value": $scope.textArea.userModifiedValue ? $scope.textArea.userModifiedValue : data.source.userValue,
						"sourceSchema": data.schema,
						"entityId": $rootScope.entityObj['@identifier'],
						"source":sourceName ? sourceName : '',
						"sourceUrl":$scope.dataPopObject.overideSearchedInputURLEdit ? $scope.dataPopObject.overideSearchedInputURLEdit : '',
						"preValue":prev_val,
						"isUserData":true,
						"sourceType":src_type ? src_type : 'link',
						"publishedDate":$scope.dataPopObject.overideSearchedInputDateEdit ? $scope.dataPopObject.overideSearchedInputDateEdit : '', 
						"docId" : slectedDoc_DocId ? slectedDoc_DocId : ''
					};
				}
				// $scope.$parent.pageloader.companyDetailsReview = true;

				// if(saveEntityData.isUserData){
				// 	var sourceIndex = $scope.sourceList_forModal.findIndex(function(d){
				// 		return d.sourceName == $scope.dataPopObject.overideSearchedInputSourceEdit;
				// 	});
				// 	if(sourceIndex === -1){
				// 			$scope.addSource();
				// 			return;
				// 	}
				// }
				var currentdate = new Date(); 
				var datetime =  + currentdate.getFullYear()  + "-" 
						+ (currentdate.getMonth()+1) + "-"  
						+ currentdate.getDate() + " "							
						+ currentdate.getHours() + ":"  
						+ currentdate.getMinutes() + ":" 
						+ currentdate.getSeconds() + ":" 
						+ currentdate.getMilliseconds();
					
				$scope.$parent.pageloader.companyDetailsReview = true;

              EntityApiService.saveEntityAttributes(saveEntityData).then(function(response){
				
							if($scope.overrideSource){
								$scope.$parent.ceriSearchResultObject[data.schema].source = $scope.overrideSource;
								$scope.$parent.ceriSearchResultObject[data.schema].sourceUrl = $scope.overridesourceUrl;
							}
							if($scope.overrideSource){
								$scope.$parent.ceriSearchResultObject[data.schema].value = saveEntityData.value;
							}else{
								$scope.$parent.ceriSearchResultObject[data.schema].value = $scope.textArea.userModifiedValue ? $scope.textArea.userModifiedValue : data.source.userValue ;
							}
							
							if(!$scope.isUserdataSelected){						
								$scope.$parent.ceriSearchResultObject[data.schema].modifiedOn = datetime;
								$scope.$parent.ceriSearchResultObject[data.schema].isUserData = true;
								$scope.$parent.ceriSearchResultObject[data.schema].modifiedBy = $scope.$parent.ehubObject.fullName;
								$scope.$parent.ceriSearchResultObject[data.schema].source = saveEntityData.source;
								$scope.$parent.ceriSearchResultObject[data.schema].sourceUrl = saveEntityData.sourceUrl;
								$scope.$parent.ceriSearchResultObject[data.schema].publishedDate = saveEntityData.publishedDate;
								$scope.$parent.ceriSearchResultObject[data.schema].preValue = prev_val;
								$scope.$parent.ceriSearchResultObject[data.schema].userValue = saveEntityData.value ? saveEntityData.value : '';
								$scope.$parent.ceriSearchResultObject[data.schema].userSource = saveEntityData.source ? saveEntityData.source : '';
								$scope.$parent.ceriSearchResultObject[data.schema].userSourceUrl = saveEntityData.sourceUrl ? saveEntityData.sourceUrl : '';
								$scope.$parent.ceriSearchResultObject[data.schema].userPublishedDate = saveEntityData.publishedDate ? saveEntityData.publishedDate : '';
								$scope.$parent.ceriSearchResultObject[data.schema].docId = saveEntityData.docId ? saveEntityData.docId : '';
								
							}else{
								$scope.ceriSearchResultObject[data.schema].isUserData = false;
							}
							if(data.schema == 'bst:description'){
								$scope.$parent.companyInfo.texts = $scope.$parent.ceriSearchResultObject[data.schema].value;
							}
							$scope.$parent.ceriSearchResultObject[data.schema].sourceType = src_type ? src_type : '';
							$scope.$parent.pageloader.companyDetailsReview = false;
							$scope.$parent.getCaseByRiskDetails();
												$uibModalInstance.close();
						
              }).catch(function(error){
				$scope.$parent.pageloader.companyDetailsReview = false;
                  console.log('error: ', error);
              });
  
			};
			
			$scope.toggleAccordian = function(accordianType, event){
				if( $(event.target).parent().parent().parent().attr("role")	=== 'tab'){
					if(accordianType == 'primary'){
						$scope.accordian.primaryOpen = !!$scope.accordian.primaryOpen;
					}else if(accordianType == 'secondary'){
						$scope.accordian.secondaryOpen = !!$scope.accordian.secondaryOpen;
					}
				}else{
					return;
				}
			};          
			$scope.editInputSource=function(value,cancel){
					$scope.showEditMode=value;
					if(cancel){
						$scope.overideSearchedInputSourceEditVal= data.source.source?data.source.source:data.source.modifiedBy;
						$scope.dataPopObject.overideSearchedInputSourceEdit=data.source.source?data.source.source:data.source.modifiedBy;
						$scope.dataPopObject.overideSearchedInputURLEdit = data.source.sourceUrl?data.source.sourceUrl:"";
						var splittedvalue = data.source.publishedDate?data.source.publishedDate.split(' ') :"";
				    $scope.dataPopObject.overideSearchedInputDateEdit  = splittedvalue?splittedvalue[0]:"";
					}
			}
			$scope.addSourceOveride = function(){
				var url = EHUB_FE_API + 'sourceManagement/#!/sourceManagement';
	          window.open(url, '_blank');
				localStorage.setItem('toAdd', 'yes');
				$scope.showAddNew = false;
				$scope.dataPopObject.overideSearchedInputSourceEdit ="";
				$scope.overideSearchedInputSourceEditVal ="";
		//		modalInstanceAdd.dismiss('close');
				
			}
			$scope.sourceSearchInputOveride = function(value,type){
				$scope.fiteredSourceList =[];
				$scope.sourceUrlList = true;
				if($scope.dataPopObject.overideSearchedInputSourceEdit == ""){
					$scope.sourceUrlList = false;
				}
				if(value){
					$scope.showfiteredSourceList =true;
					angular.forEach($scope.sourceList_forModal,function(source){
						if(source.sourceName.toLowerCase().indexOf(value.toLowerCase())>=0){
							$scope.fiteredSourceList.push({sourceName : source.sourceName, source_type: source.source_type});
						} else{
							$scope.showAddNew =true;
						}
					});
				} else{
					$scope.showAddNew =false;
					$scope.dataPopObject.overideSearchedInputSourceEdit = '';
					$scope.dataPopObject.overideSearchedInputURLEdit = '';
					$scope.dataPopObject.overideSearchedInputDateEdit = '';
				}
						if(event.keyCode === 13){
							if($scope.fiteredSourceList && $scope.fiteredSourceList.length > 0){
								$scope.fillSourceSearchedInputOveride($scope.fiteredSourceList[0].sourceName,type);
								// $scope.updateCompanyDetails(output[0],obj,tabname);
							}else if( $scope.fiteredSourceList.length == 0 ){
								// $scope.saveIndustryOption(obj,compkey);
								$scope.showAddNew =false;
								// $scope.updateCompanyDetails(value,obj,tabname);
							}
						}
				// 	}
					setTimeout(function(){
						$(".custom-list.auto-complete-list.searchSource").mCustomScrollbar({
							axis: "y",
							theme: "minimal-dark"
						});
					},0);

			}
			$scope.applySourceEvidence = function(sourceData){
				var patt = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
				var result = patt.test($scope.dataPopObject.overideSearchedInputURLEdit);
				if(result && !$scope.sourceTypeName){
					$scope.sourceTypeName = "link";
				}
				if($scope.fiteredSourceList && $scope.fiteredSourceList.length == 0){
					$scope.sourceTypeName = "link";
					$scope.showAddNew = true;
				}
				if($scope.fiteredSourceList && $scope.fiteredSourceList.length > 0){
					$scope.showAddNew = false;
				}
				$scope.sourceUrlList = false;

			}
			$scope.closeSourceListPopUp = function (){
				if(data.source.isUserData){
				$scope.dataPopObject.overideSearchedInputSourceEdit=data.source.source?data.source.source:data.source.modifiedBy;
				$scope.overideSearchedInputSourceEditVal= data.source.source?data.source.source:data.source.modifiedBy;
				$scope.dataPopObject.overideSearchedInputURLEdit = data.source.sourceUrl?data.source.sourceUrl:"";
				var splittedvalue = data.source.publishedDate?data.source.publishedDate.split(' ') :"";
				$scope.dataPopObject.overideSearchedInputDateEdit  = splittedvalue?splittedvalue[0]:"";
			}else {
				$scope.dataPopObject.overideSearchedInputSourceEdit=data.source.userSource?data.source.userSource:'';
				$scope.overideSearchedInputSourceEditVal= data.source.userSource?data.source.userSource:'';
				$scope.dataPopObject.overideSearchedInputURLEdit = data.source.userSourceUrl?data.source.userSourceUrl:"";
				var splittedvalue = data.source.userPublishedDate?data.source.userPublishedDate.split(' ') :"";
				$scope.dataPopObject.overideSearchedInputDateEdit  = splittedvalue?splittedvalue[0]:"";
			}
				$scope.sourceUrlList = false;
			}
			$scope.fillSourceSearchedInputOveride = function(value,type){
				type = value.source_type;
				$scope.sourceTypeName = value.source_type;
				 if(value){
					var sourceIndex = $scope.sourceList_forModal.findIndex(function(d){
							return (d.sourceName == value.sourceName && (d.type === type || d.source_type === type || d.sourceType === type)) ;
						});
					// $scope.showfiteredSourceList =false;
					$scope.dataPopObject.overideSearchedInputSourceEdit= $scope.sourceList_forModal[sourceIndex].sourceName;
					$scope.overideSearchedInputSourceEditVal= $scope.sourceList_forModal[sourceIndex].sourceName;
					if(type === 'link'){
						$scope.dataPopObject.overideSearchedInputURLEdit = $scope.sourceList_forModal[sourceIndex].sourceUrl;
					}else if(type === 'doc' || type === 'docx' || type ==="png" || type === "pdf"){
						$scope.dataPopObject.overideSearchedInputURLEdit = $scope.sourceList_forModal[sourceIndex].sourceUrl;
						slectedDoc_DocId = $scope.sourceList_forModal[sourceIndex].docId ? $scope.sourceList_forModal[sourceIndex].docId : '';
					}
					$scope.dataPopObject.overideSearchedInputDateEdit  = $filter('date')(new Date(), 'yyyy-MM-dd');
				}
			};

					//adding source to the table
		$scope.addSource = function(){
		var sourcename = $scope.dataPopObject.overideSearchedInputSourceEdit;
		var sourceUrl = $scope.dataPopObject.overideSearchedInputURLEdit;
		// var sourceDate = $scope.dataPopObject.overideSearchedInputDateEdit;

			var reqData =  {
			"classifications":[{
				"classifcationName": "GENERAL",
				"classificationId": null,
				"hideStatusDto": null,
				"subClassifications":[]
			}],
			"sourceName": sourcename ? sourcename : "",
			"sourceUrl": sourceUrl ? sourceUrl : "",
			"sourceDisplayName": sourcename ? sourcename : "",
			"entityId": "",
			"sourceType": "",
			"sourceDomain":[],
			"sourceIndustry": [],
			"sourceJurisdiction": [],
			"sourceMedia": [],
		};		

		return;
		 EntityApiService.addNewSourceAPI(reqData).then(function (response) {
					var obj = {
						classifications: null,
						deleteMedia: false,
						entityId: null,
						sourceCreatedDate: null,
						sourceDisplayName: null,
						sourceDomain: null,
						sourceId: 0,
						sourceIndustry: null,
						sourceJurisdiction: null,
						sourceMedia: null,
						sourceName: reqData.sourceName,
						sourceType: null,
						sourceUrl: reqData.sourceUrl
					};
					$scope.sourceList_forModal.push(obj);
					$scope.overRide();
			}, function (err) {		
					console.log(err);
			});
			};

		$scope.dateError = false;
		$scope.dateValidate = function(){
			var dateString = $scope.dataPopObject.overideSearchedInputDateEdit;
			var regex = /^\d{4}(\-)(((0)[0-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])$/;
			//Check whether valid yyyy-MM-dd Date Format.
			if (regex.test(dateString)) {		
			 		$scope.dateError = false;
		} else {
				$scope.dateError = true;
		}
		};

		$scope.enabledSouceInputs = function(){
			$scope.showAddbutton = true;
			$scope.showSavebtnwithclass = true;
			$scope.disableInput = false;
		};

			/*
	* @purpose : Get all documents list 
	* @author : Amarjith Kumar
	* @Date : 30-March-2019
	*/
	$scope.documentListsData = [];
	// function getAllDocuments() {
	// 	var params = {
	// 		"token": $rootScope.ehubObject.token,
	// 		"pageNumber": 1,
	// 		"orderIn": 'desc',
	// 		"orderBy": 'uploadedOn',
	// 		"recordsPerPage": 9,
	// 		"entityId": $rootScope.entityObj["@identifier"]
	// 	};
	// 	$scope.documentListsData = [];
	// 	return new Promise(function (resolve, reject) {
	// 		TopPanelApiService.getAllDocuments(params).then(function (res) {
	// 			getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'docs');
	// 			getEvidenceListDocs(params);
	// 			resolve(true);
	// 		}).catch(function (err) {
	// 			reject(false);
	// 		});
	// 	});
	// }

	/*
	* @purpose : Get all evidence documents list 
	* @author : karnakar
	* @Date : 01-july-2019
	*/
	
	// function getEvidenceListDocs(paramFromApiFunc){
	// 	var params = paramFromApiFunc;
	// 	params.docFlag = 6;
	// 	$scope.documentListsData = [];
	// 	return new Promise(function(resolve, reject) {
	// 		TopPanelApiService.getAllEvidenceDocuments(params).then(function(res){
	// 			getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'evidence_docs');
	// 			resolve(true);
	// 		}).catch(function(err){
	// 			reject(false);
	// 		});
	// 	});
	// }

	// function getAllStickyNotes() {
	// 	var params = {
	// 		"token": $rootScope.ehubObject.token,
	// 		"docFlag": 5,
	// 		"pageNumber": 1,
	// 		"orderIn": 'desc',
	// 		"orderBy": 'uploadedOn',
	// 		"recordsPerPage": 9,
	// 		"entityId": $rootScope.entityObj["@identifier"]
	// 	};
	// 	$scope.documentListsData = [];
	// 	return new Promise(function (resolve, reject) {
	// 		TopPanelApiService.getAllDocuments(params).then(function (res) {
	// 			getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'sticky');
	// 			resolve(true);
	// 		}).catch(function (err) {
	// 			console.log(err);
	// 			reject(false)
	// 		});
	// 	});
	// }

	// function getSuccessReportDocuments(pagerecords, docOrSticky) {
	// 	var params = {
	// 		"token": $rootScope.ehubObject.token,
	// 		"pageNumber": 1,
	// 		"orderIn": 'desc',
	// 		"orderBy": 'uploadedOn',
	// 		"recordsPerPage": pagerecords,
	// 		"entityId": $rootScope.entityObj["@identifier"]
	// 	};
	// 	var params3 = {
	// 		"token": $rootScope.ehubObject.token,
	// 		"docFlag": 6,
	// 		"pageNumber": 1,
	// 		"orderIn": 'desc',
	// 		"orderBy": 'uploadedOn',
	// 		"recordsPerPage": pagerecords,
	// 		"entityId":$rootScope.entityObj["@identifier"]
	// 	};

	// 	if (docOrSticky === 'docs') {
	// 		return new Promise(function (resolve, reject) {
	// 			TopPanelApiService.getAllDocuments(params).then(function (res) {
	// 				if(res && res.data && res.data.result && res.data.result.length){
	// 					res.data.result.forEach(function(val,key){
	// 						val.source_type = val.type || 'doc';
	// 						val.sourceName = val.title || val.docName;
	// 						val.sourceUrl = val.docName;
	// 						val.docId = val.docId;
	// 						val.docFlag = val.docFlag;
	// 					});
	// 					$scope.documentListsData = res.data.result;
	// 					$scope.sourceList_forModal = $scope.sourceList_forModal.concat($scope.documentListsData);
	// 				}
	// 				resolve(true);
	// 			}).catch(function (err) {
	// 				console.log(err);
	// 				reject(false);
	// 			});
	// 		});
	// 	} 
	// 	else if(docOrSticky === "evidence_docs"){
	// 		return new Promise(function(resolve, reject) {
	// 			TopPanelApiService.getAllDocuments(params3).then(function(res){			
	// 				angular.forEach(res.data.result, function (val, key) {
	// 					val.source_type = val.type || 'png';
	// 					val.sourceName = val.title || val.docName;
	// 					val.sourceUrl = val.docName;
	// 					val.docId = val.docId;
	// 					val.docFlag = val.docFlag;
	// 					// var params = {
	// 					// 	"token": $rootScope.ehubObject.token,
	// 					// 	"docId": val.docId
	// 					// };
						
	// 					// return new Promise(function (resolve, reject) {
	// 					// 	TopPanelApiService.downloadDocument(params).then(function (docContent) {
	// 					// 		var blob = new Blob([docContent.data], {
	// 					//	// 		var imageUrl = urlCreator.createObjectURL(blob);
	// 					// 		resolve(true);
	// 					// 	}, function () {
	// 					// 		HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
	// 					// 		reject(false);
	// 					// 	});
	// 					// });
	// 				});				
	// 				$scope.evidenceDocumentsListsData = res.data.result;

	// 				$scope.sourceList_forModal = $scope.sourceList_forModal.concat($scope.evidenceDocumentsListsData);
	// 				resolve(true);
	// 			}).catch(function(err){
	// 				console.log(err);
	// 				reject(false);
	// 			});
	// 		});
	// 	}
	// } 			type: "application/text",
	// 					// 		});
	// 					// 		var urlCreator = window.URL || window.webkitURL;
	// 					// 		var imageUrl = urlCreator.createObjectURL(blob);
	// 					// 		resolve(true);
	// 					// 	}, function () {
	// 					// 		HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
	// 					// 		reject(false);
	// 					// 	});
	// 					// });
	// 				});				
	// 				$scope.evidenceDocumentsListsData = res.data.result;

	// 				$scope.sourceList_forModal = $scope.sourceList_forModal.concat($scope.evidenceDocumentsListsData);
	// 				resolve(true);
	// 			}).catch(function(err){
	// 				console.log(err);
	// 				reject(false);
	// 			});
	// 		});
	// 	}
	// }
			EntityorgChartService.getllSourceDocuments().then((response) => {
				if (response && response.length > 0) {
					if (response[0].documentListsData && response[0].documentListsData.length > 0) {
						// $scope.sourceList  = $scope.sourceList.concat(response[0].documentListsData);
						$scope.documentListsData = response[0].documentListsData;
						response[0].documentListsData.forEach((val) => {
							var sourceArray = $scope.sourceList_forModal.filter((searchedval) => searchedval.sourceName === val.sourceName);
							var sourceObject = sourceArray.filter((searchVal) => searchVal.type === val.type);
							if (sourceObject.length == 0) {
								$scope.sourceList_forModal.push(val);
							}
						});
					}
					if (response[1].evidenceDocumentsListsData && response[1].evidenceDocumentsListsData.length > 0) {
						// $scope.sourceList  = $scope.sourceList.concat(response[1].evidenceDocumentsListsData);
						$scope.evidenceDocumentsListsData = response[1].evidenceDocumentsListsData;
						response[1].evidenceDocumentsListsData.forEach((val) => {
							var sourceArray = $scope.sourceList_forModal.filter((searchedval) => searchedval.sourceName === val.sourceName);
							var sourceObject = sourceArray.filter((searchVal) => searchVal.type === val.type);
							if (sourceObject.length === 0) {
								$scope.sourceList_forModal = $scope.sourceList_forModal.push(val);
							}
						});

					}
				}
			});
	// getAllDocuments();
}
	    	