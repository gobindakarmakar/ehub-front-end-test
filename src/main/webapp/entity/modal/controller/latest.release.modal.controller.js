angular.module('ehubEntityApp')
		.controller('LatestReleaseListModalController', latestReleaseListModalController);

latestReleaseListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'latestReleaseData'
];

function latestReleaseListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		latestReleaseData){
	
	$scope.latestReleases = latestReleaseData;
	
	/*
 	*@purpose: close latestReleases modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}