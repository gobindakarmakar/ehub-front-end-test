angular.module('ehubEntityApp')
		.controller('ScamWebsiteListModalController', scamWebsiteListModalController);

scamWebsiteListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'scamWebsiteListData'
];

function scamWebsiteListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		scamWebsiteListData){
	
	$scope.scamWebsiteListData = scamWebsiteListData;
	/*
 	*@purpose: close scam website modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}