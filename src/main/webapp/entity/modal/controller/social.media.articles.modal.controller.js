angular.module('ehubEntityApp')
		.controller('SocialMediaArticlesController', socialMediaArticlesController);

socialMediaArticlesController.$inject = [
		'$scope',
		'$state',
		'$rootScope',
		'$stateParams',
		'EntityApiService',
		'$q',
		'EntityGraphService',
		'searchText',
		'$uibModalInstance',
		'searchType'
	];
	function socialMediaArticlesController(
			$scope, 
			$state, 
			$rootScope, 
			$stateParams,
			EntityApiService,
			$q,
			EntityGraphService,
			searchText,
			$uibModalInstance,
			searchType) {
	
	 /*initializing entity company related variables*/
	 $scope.disableSearchButton = true;
	 $scope.entityTwitterList = [];
	 $scope.entityLinkedinList = [];
	 $scope.entityInstagramList = [];
	 $scope.entityGooglePlusList = [];
	 $scope.entityFacebookList =  [];
	 $scope.socialMediaActiveCount = 0;
	 $scope.entitySearchResult = {
		 name: searchText
	 };
	 
	 var socialFetcherIds = ['55','56','13','21','57'];	 
	 /*
	  * @purpose: getting entity data by name
	  * @created: 19 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	  */
	 function getEntityCompanyDataByName(name) {
		 var deffered = $q.defer();
		 angular.forEach(socialFetcherIds, function(id, key) {
			 var ids = [];
			 ids.push(id);
			 var data = {
	            fetchers: ids, 
	            keyword: name,
	            searchType: searchType
			 };
			 if(ids.indexOf('13') !== -1)
				 data.lightWeight = true;
			 EntityApiService.getEntityDataByTextId(data).then(function(subResponse) {
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
			            if (subfetcherData.status) {
				           	if(subfetcherData.fetcher === 'plus.google.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
			            				if(edgeValue.relationshipName === 'recent_public_activity') {
									        $scope.entityGooglePlusList.push({
				            					name: subentity.properties.name,
				            					url: subentity.properties.url,
				            					title: edgeValue.entity.properties.title,
				            					coverPhoto: subentity.properties.cover_photo,
				            					lastUpdatedDate: new Date(edgeValue.entity.properties.last_updated_date),
				            					publishedDate: new Date(edgeValue.entity.properties.published_date),
				            					type: 'google'
				            				});
			            				}
			            			});
			            		});
			            	}
				           	if(subfetcherData.fetcher === 'instagram.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
								        if(edgeValue.relationshipName === 'media') {
				            				$scope.entityInstagramList.push({
				            					caption: edgeValue.entity.properties.caption,
				            					thumbnail: edgeValue.entity.properties.thumbnail,
				            					image: subentity.properties.image,
				            					name: subentity.properties.name,
				            					followingCount: subentity.properties.following_count,
				            					followersCount: subentity.properties.followers_count,
				            					type: 'instagram'
				            				});
			            				}
			            			});
			            		});
			            	}
			            	if(subfetcherData.fetcher === 'company.linkedin.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
				            			if(edgeValue.relationshipName === 'social_feed') {
					            			$scope.entityLinkedinList.push({
				            					name: edgeValue.entity.name,
				            					commentsCount: edgeValue.entity.properties.comments_count !== '' && edgeValue.entity.properties.comments_count ? edgeValue.entity.properties.comments_count : 0,
				            					likesCount: edgeValue.entity.properties.likes_count !== '' && edgeValue.entity.properties.likes_count ? edgeValue.entity.properties.likes_count : 0,
				            					postedOn: edgeValue.entity.properties.posted_at,
				            					text: edgeValue.entity.properties.text,
				            					image: subentity.properties.image,
				            					source: subentity.source
				            				});
				            			}
			            			});
			            		});
			            	}
			            	if(subfetcherData.fetcher === 'twitter.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
				            			if(edgeValue.relationshipName === 'tweets') {
				            				$scope.entityTwitterList.push({
			            					  title: subentity.properties.title,
				                              createdOn: new Date(edgeValue.entity.properties.created_at),
				                              image: subentity.properties.image,
				                              text: edgeValue.entity.properties.text,
				                              retweetCount: edgeValue.entity.properties.retweet_count,
				                              type: 'twitter'
				            				});
				            			}
			            			});
			            		});
			            	}
			            	if(subfetcherData.fetcher === 'pages.facebook.com') {
			            		var matchedName = '';
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			if(matchedName === '' || (matchedName !== '' && matchedName === subentity.name)){
			            				subentityKey === 0 ? matchedName = subentity.name : '';
			            				angular.forEach(subentity.edges, function(subedv, subedk){
			            					if(subedv.relationshipName === 'recent_posts'){
			            						angular.forEach(subedv.entity.edges, function(subsubedv, subsubedk){
					            					if(subsubedv.relationshipName === 'data'){
									            		$scope.entityFacebookList.push({
							            					text: subsubedv.entity.properties.message || subsubedv.entity.properties.story,
							            					created_time: new Date(subsubedv.entity.properties.created_time),
							            					website: subsubedv.entity.properties.website,
							            					risk_score: subsubedv.entity.properties.risk_score
							            				});
					            					}
			            						});
			            					}
			            				});
			            			}
			            		});
			            	}
			            }
				});
				 if(key === socialFetcherIds.length - 1){
					 deffered.resolve('completed'); 
				 }
			 }, function(error){
				deffered.reject(error);
			 });
		});
		
		return deffered.promise;
	 }
	 
	 getEntityCompanyDataByName(searchText).then(function(response){
		 $scope.disableSearchButton = false;
	 }, function(){
	 });
	 
	 /*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
};