angular.module('ehubEntityApp')
		.controller('CommentsEntityModalConteoller', commentsEntityModalConteoller);

commentsEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance'
];

function commentsEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance){
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}