'use strict';
angular.module('ehubEntityApp')
	.controller('FlowMaximizeModalController', flowMaximizeModalController);
flowMaximizeModalController.$inject = [
	'$scope',
	'$rootScope',
	'Id',
	'$uibModalInstance',
	'ChartOptions',
	'$timeout',
	'entitySearchResult',
	'allLevelData',
	'$uibModal',
	'EntityApiService',
	'summaryScreenig',
	'scope',
	'HostPathService',
	'EntityorgChartService',
	'customEntites'
];

function flowMaximizeModalController(
	$scope,
	$rootScope,
	Id,
	$uibModalInstance,
	ChartOptions,
	$timeout,
	entitySearchResult,
	allLevelData,
	$uibModal,
	EntityApiService,
	summaryScreenig,
	scope,
	HostPathService,
	EntityorgChartService,
	customEntites
) {
	$scope.modalOpen = true;
	$scope.chartLevel = 5;
	$scope.sliderMinValue = 10;
	$scope.sliderMaxValue = 100;
	$scope.entitySearchResult = entitySearchResult;
	$scope.filteredChart = {
		company_eachlevel: [10, 20, 30, 40, 50],
		numberofcomapnies: 20,
		numberOfLevel: [1, 5, 10, 15, 20, 30],
		selectedNumberofLevel: 5,
		risklevel: ["low", "high"],
		riskRatio: "low",
		applyButton: false
	}
	var filteredApplied = 0;
	var FlagTooltipData = {};
	var entityDataOnClikOfOdg, deleteSelectedEntity = {};
	var personEntitytype = ['person'];
	$scope.countryNames = scope.countryNames;
	$scope.popOverEditDataForOwnership = {};
	$scope.addEntityfrommodal = true;
	$scope.addEntityTypeList = scope.addEntityTypeList;
	//ChartOptions.id = Id;
	$scope.closeWidgetCaptureModal = function () {
		$("body").off("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup', handlepopup);
		$uibModalInstance.close(ChartOptions);
		$scope.modalOpen = false;
	};
	$scope.EditEntityTypeList =scope.EditEntityTypeList;
	console.log($scope.EditEntityTypeList)
	onload(ChartOptions, "#flowChartViewDiv");
	/* @purpose: Plot   Ownership data  Data
	 * @created: 07 may 2018
	 * @params: type(string)
	 * @returns: no
	 * @author: Ram Singh */
	function onload(ChartOptions, id) {
		$scope.widgetCapturePreloader = true;
		ChartOptions = ChartOptions.concat(customEntites.adddeleteEntity);
		setTimeout(function () {
			var currentOPts = jQuery.extend(true, {}, options);
			currentOPts.items = jQuery.extend(true, [], ChartOptions);
			var linesannotatios = [];
			angular.forEach(currentOPts.items,function(val,key){
				// var lineann = new primitives.famdiagram.HighlightPathAnnotationConfig();
				if(val.entity_type === 'person'){
					if(val.indirectChilds && val.indirectChilds.length){
						angular.forEach(val.indirectChilds,function(data,index){
							var lineann =  new primitives.orgdiagram.ConnectorAnnotationConfig();

							lineann.fromItem = data.id;
							lineann.toItem = val.id;
							lineann.label = "<div class='bp-badge' style='width:50px; height:17px;background-color:#388E3C; color: white;line-height:0.9;top:6px;position:relative;'>"+data.indirectPercentage.toFixed(2)+"%</div>";
							lineann.labelSize =  new primitives.common.Size(80, 30);
							lineann.offset = 0;
							lineann.connectorShapeType= primitives.common.ConnectorShapeType.OneWay;
							lineann.color= '#388E3C';
							lineann.lineWidth= 2;
							lineann.lineType= primitives.common.LineType.Dashed;
							lineann.selectItems= false;
							lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Offbeat;
							if(data.isDirect){
								lineann.lineWidth= 1;
								lineann.lineType= primitives.common.LineType.Solid;
								lineann.connectorPlacementType = primitives.common.ConnectorPlacementType.Straight;
								lineann.color= "rgb(88, 107, 113)";
							}
							linesannotatios.push(lineann);
						});
					}
				}
			});
		currentOPts.annotations = linesannotatios;
			$('#flowChartViewDiv').find('.orgdiagram .custom-scroll-wrapper').remove();
			setTimeout(function () {
				if (filteredApplied) {
					jQuery(id).famDiagram("update", /*Refresh: use fast refresh to update chart*/ primitives.orgdiagram.UpdateMode.Recreate);
					jQuery(id).children(".orgdiagram").addClass("custom-scroll-wrapper");
					$scope.widgetCapturePreloader = false;
				}
				//$(id).children().css("width", "100%");
				$(id).children().css("max-height", "100%");
				$(id).children().addClass('custom-scroll-wrapper');
				$(id).find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-university").remove();
				$(id).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
				$(id).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
				$(id).find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-university").remove();
				$(id).find(".orgdiagram").find(".orgChartParentEntity").parent().prepend('<i class="fa fa-university" style="color:#4c9d20"></i>');
				$($(id).find(".orgdiagram").find(".orgChartmainEntity").parent().prepend('<i class="fa fa-university"></i>')).css({
					'background-color': 'none'
				});
				$('.custom-scroll-wrapper').css('overflow', 'hidden').height();
				$('.custom-scroll-wrapper').css('overflow', 'auto');
				$(id).find(".orgdiagram").find(".orgChartsubEntity").parent().prepend('<i class="fa fa-university" style="color:#c7990c"></i>');
				//	$(id).find(".orgdiagram").find(".shareCubes").parent().prepend('<span class="c-pointer"><i class="fa fa-cubes" style="color:#4c9d20" id="faCubeId"></i></span>');
			}, 0)
			jQuery("#" + Id).famDiagram(currentOPts);
			$('.custom-scroll-wrapper').css('overflow', 'hidden').height();
			$('.custom-scroll-wrapper').css('overflow', 'auto');
			$scope.widgetCapturePreloader = false;
		}, 1000);
	}
	//-------------------------Implementation of hierarchy chart--------------------------------------------------
	var options = new primitives.famdiagram.Config();
	options.pageFitMode = 0;
	options.cursorItem = 2;
	options.linesWidth = 1;
	options.linesColor = "rgb(88, 107, 113)";
	options.hasSelectorCheckbox = primitives.common.Enabled.False;
	options.normalLevelShift = 20;
	options.dotLevelShift = 20;
	options.lineLevelShift = 20;
	options.normalItemsInterval = 10;
	options.dotItemsInterval = 10;
	options.lineItemsInterval = 10;
	options.defaultTemplateName = "info";
	options.templates = [getInfoTemplate()];
	options.onItemRender = onTemplateRender;
	options.onHighlightChanged = function (e, data) {
		var name = "";
		var subtitile = "";
		if(data.context){

			FlagTooltipData = (data && data.context) ? data.context : '';
		}
		//	entityDataOnClikOfOdg = (data && data.context) ? data.context : '';
		$("#flowChartViewDiv").find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-university").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-user").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-user").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-university").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-user").remove();
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartParentEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#4c9d20"></i>');
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartParentEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#4c9d20"></i>');
		$($("#flowChartViewDiv").find(".orgdiagram").find(".orgChartmainEntity")).parent().parent().css({
			'background-color': 'none'
		});
		$($("#flowChartViewDiv").find(".orgdiagram").find(".orgChartmainEntity").parent().prepend('<i class="fa fa-university"></i>')).css({
			'background-color': 'none'
		});
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartsubEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#c7990c"></i>');
		$("#flowChartViewDiv").find(".orgdiagram").find(".orgChartsubEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#c7990c"></i>');
	};
	var currentClickedData;
	options.onMouseClick = function (e, data) {
		if (data.context) {
			//	currentClickedData = data.context;
			entityDataOnClikOfOdg = (data && data.context) ? data.context : '';
			currentClickedData = _.find(allLevelData, {
				'@identifier': data.context.identifier
			});
			$("body").off("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
			$("body").on("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
			$timeout(function () {
				$("body").off("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
			}, 0);
		}
	}
	$("body").on("mouseover", ".orgdiagram .flag-pop-modal", function (e) {
		//below section handles the Data for flag tooltip
		var flagCountry = FlagTooltipData.basic && FlagTooltipData.basic['mdaas:RegisteredAddress'] && FlagTooltipData.basic['mdaas:RegisteredAddress'].fullAddress ? FlagTooltipData.basic['mdaas:RegisteredAddress'].fullAddress : '';
		var flag_class_code = FlagTooltipData.juridiction ? ('flag-icon-' + FlagTooltipData.juridiction.toLowerCase()) : ((FlagTooltipData.basic && FlagTooltipData.basic.isDomiciledIn) ? ('flag-icon-' + FlagTooltipData.basic.isDomiciledIn.toLowerCase()) : '');
		$(".percentage_tooltip").css("display", "block");
		if (FlagTooltipData.name) {
			$(".percentage_tooltip").html('<div class="top-heading d-flex">' +
				'<span class="f-18 flag-icon flag-icon-squared mar-r10 text-cream  ' + flag_class_code + '" style="align-self:baseline"></span><p class="f-12 text-cream">' + flagCountry + '</div>');
			var p = $('#maximizeAllchart')
			var position = p.offset();
			var windowWidth = window.innerWidth;
			var tooltipWidth = windowWidth - $('.percentage_tooltip').width() - 100;
			if ((e.pageX > tooltipWidth)) {
				$(".percentage_tooltip").css("left", ((e.pageX) - $('.percentage_tooltip').width() - 100) + "px");
			} else {
				$(".percentage_tooltip").css("left", (e.pageX) + 30 + "px");
			}
			//$(".percentage_tooltip").css("left", (e.pageX) + 30 + "px");
			$(".percentage_tooltip").css("z-index", "999999");
			$(".percentage_tooltip").css("display", "block");
			return $(".percentage_tooltip").css("top", e.pageY + "px");}
		 
		$(".entity_popover").html('');
		$(".entity_popover").css("display", "none");
	}).on("mouseout", '.orgdiagram .flag-pop-modal', function () {
		// data = {};
		$(".percentage_tooltip").html('');
		$(".percentage_tooltip").css("display", "none");
	});

	function handlepopup(event) {
		event.preventDefault();
		event.stopPropagation();
		var _this = $(this);
		$timeout(function () {
			var data = jQuery.extend(true, {}, currentClickedData);
			// currentClickedData = undefined;
			if (_this.attr("class").indexOf("newspaper-popup") > -1) {
				var index = (data.entity_id && data.entity_id === 'orgChartmainEntity') ? 0 : 1;
				$scope.openComanyBst(index, data);
				//adversenews
			} else if (_this.attr("class").indexOf("globe-popup") > -1) {
				$scope.openComanyBst(index, data, 'highRisk');
				//jurrisdiction
			} else if (_this.attr("class").indexOf("gavel-popup") > -1) {
				$scope.openComanyBst(0, data, "financeCrime");
				//finance_Crime_url
			} else if (_this.attr("class").indexOf("street-popup") > -1) {
				$scope.openComanyBst(index, data, 'pep');
				//pep_url	
			} else if (_this.attr("class").indexOf("user-popup") > -1) {
				var adversecontent = '';
				$scope.openComanyBst(index, data, 'sanction');
			}
		}, 0);
	}
	/*
	 * @purpose: open Adverse news pop directly on click newspaper icon
	 * @created: 29 Oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.openComanyBst = function (index, data, type) {
		var articles = (data.adverseNews_url && data.adverseNews_url.length > 0) ? data.adverseNews_url : (data.FalseNews ? data.FalseNews : []);
		if (type === "financeCrime") {
			articles = (data.finance_Crime_url && data.finance_Crime_url.length > 0) ? data.finance_Crime_url : [];
		}
		var showMainName = (index === 0) ? true : false;
		var articleVisualizerModalInstance = $uibModal.open({
			templateUrl: 'direct-adverse-news-modal.html',
			controller: 'adverseModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal full-analysis-wrapper adverse-full-modal  bst_modal ',
			resolve: {
				selectedEntity: function () {
					return data;
				},
				articles: function () {
					return articles;
				},
				header: function () {
					return articles.length > 0 ? articles[0] : {};
				},
				selectedindex: function () {
					return 0;
				},
				showMainName: function () {
					return showMainName;
				},
				type: function () {
					return type;
				},
				summaryScreenig: function () {
					return summaryScreenig;
				}
			}
		});
		articleVisualizerModalInstance.result.then(function (response) {}, function (error) {});
	};
	// jQuery("#flowChartViewDiv").famDiagram(options);
	function onTemplateRender(event, data) {
		data.renderingMode = filteredApplied;
		switch (data.renderingMode) {
			case primitives.common.RenderingMode.Create:
				/* Initialize widgets here */
				break;
			case primitives.common.RenderingMode.Update:
				/* Update widgets here */
				break;
		}
		var itemConfig = data.context;
		$(data.element[0]).attr('id', itemConfig.id);
		if (data.templateName == "info") {
			var indirectPercentage = itemConfig.indirectPercentage ? itemConfig.indirectPercentage.toFixed(2) : '';
			var int_percentage = indirectPercentage ? parseFloat(indirectPercentage) : '';
			var totalstringPercentage = itemConfig.totalPercentage ? itemConfig.totalPercentage.toFixed(2) : '';
			var totalPercentage = totalstringPercentage ? parseFloat(totalstringPercentage) : 0;
			if (!(data.element.find("[name=title]").hasClass("orgChartsubEntity") || data.element.find("[name=title]").hasClass("orgChartParentEntity") || data.element.find("[name=title]").hasClass("orgChartmainEntity"))) {
				data.element.find("[name=title]").text(itemConfig.title);
				data.element.find("[name=title]").addClass(itemConfig.entity_id);
				if ((itemConfig.entity_type && (personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) !== -1))) {
					data.element.find("[name=title]").addClass('person_icon');
				} else {
					data.element.find("[name=title]").addClass('comapny_icon');
				}
				if(itemConfig.isConflict){
					data.element.css('border','2px solid #ef5350');
				}
				if ((int_percentage || int_percentage === 0 || totalPercentage || totalPercentage === 0) && (itemConfig.entity_id !== "orgChartsubEntity")) {
					data.element.find("[name=ownershipPercentages]").append('<li class="progressbar-list-item pad-t0">' +
						'<div class="progress progress-gradient-grey" style="width:100%"> <span class="z-99 p-rel">' + int_percentage + '%</span>' +
						'<div class="progress-bar progress-gradient-blue z-9" role="progressbar" aria-valuenow="' + int_percentage + '"' +
						'aria-valuemin="0" aria-valuemax="100" style="width:' + int_percentage + '%">' +
						'<span class="sr-only ">' + int_percentage + '%</span>' +
						'</div>' +
						'</div>' +
						'</li>' +
						'<li class="progressbar-list-item">' +
						'<div class="progress progress-gradient-grey" style="width:100%"><span class="z-99 p-rel">' + totalPercentage + '</span>%' +
						' <div class="progress-bar z-9 progress-gradient-curious-blue" role="progressbar" aria-valuenow="' + totalPercentage + '"' +
						'aria-valuemin="0" aria-valuemax="100" style="width:' + totalPercentage + '%">' +
						'<span class="sr-only ">' + totalPercentage + '</span>' +
						'</div>' +
						'</div>' +
						'</li>');
				}
				//below Im checking entity as person 
				var ownershipdependency = '';
				if (itemConfig.entity_id === 'orgChartmainEntity') {
					ownershipdependency = '';
				} //below Im checkin low risk and percentage > 25                                                                                 				or  	below  we are checking high risk and percentage 10 																		
				// else if ((itemConfig.entity_type && (personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) !== -1)) && ((int_percentage && int_percentage > 25) || (int_percentage && int_percentage > 10 && itemConfig.high_risk_jurisdiction && itemConfig.high_risk_jurisdiction.toUpperCase() === 'HIGH'))) {
				// 	data.element.find("[name=description]").text('UBO ');
				// 	ownershipdependency = 'UBO';
				// } else if ((itemConfig.entity_type && (personEntitytype.indexOf(itemConfig.entity_type.toLowerCase()) === -1)) && ((int_percentage && int_percentage > 25) || (int_percentage && int_percentage > 10 && itemConfig.high_risk_jurisdiction && itemConfig.high_risk_jurisdiction.toUpperCase() === 'HIGH'))) {
				// 	data.element.find("[name=description]").text('IBO ');
				// 	ownershipdependency = 'IBO';
				// } else {
				// 	//	data.element.find("[name=description]").text(indirectPercentage + '%');
				// }
				else if(itemConfig.Ubo_ibo){
					var ubo_ibo_value  = (itemConfig.Ubo_ibo === 'UBO ' || itemConfig.Ubo_ibo === 'IBO ') ? itemConfig.Ubo_ibo :'';
					 data.element.find("[name=description]").text(ubo_ibo_value);
				 }
				data.element.find("[name=description]").addClass("indirectpercenOwnship");
				data.element.find("[name=description]").addClass("indirectpercenOwnship");
				if (!itemConfig.source_evidence && itemConfig.source_evidence_url) {
					data.element.find("[name=sourceevidence]").attr('href', itemConfig.source_evidence_url).text(itemConfig.source_evidence_url);
				} else if ((itemConfig.source_evidence && itemConfig.source_evidence_url) || (itemConfig.source_evidence && !itemConfig.source_evidence_url)) {
					data.element.find("[name=sourceevidence]").text(itemConfig.source_evidence);
				}
				if (itemConfig.source_evidence_url) {
					data.element.find("[name=source_evidence_url]").attr('href', itemConfig.source_evidence_url);
				} else if (!itemConfig.source_evidence_url) {
					data.element.find("[name=source_evidence_url]").css('display', 'none');
				}
			}
			if (itemConfig.addCubeIcon) {
				data.element.find("[name=title]").addClass("shareCubes");
			}
			if (itemConfig.showspinner) {
				data.element.find(".showspinner").removeClass("d-none");
			}else{
				data.element.find(".showspinner").addClass("d-none");

			}
			//Hide icon if result is false
			if (itemConfig.juridiction  || itemConfig.jurisdiction) {
				var flag = itemConfig.juridiction ? itemConfig.juridiction : (itemConfig.jurisdiction? itemConfig.jurisdiction:'')
				$(data.element[0]).find('span.flag-icon-squared').addClass("flag-icon-" + flag.toLowerCase()).css('display', 'block');
			}else if (itemConfig.basic && !_.isEmpty(itemConfig.basic) && itemConfig.basic.isDomiciledIn) {
				$(data.element[0]).find('span.flag-icon-squared').addClass("flag-icon-" + itemConfig.basic.isDomiciledIn.toLowerCase()).css('display', 'block');
			} else {
				$(data.element[0]).find('span.flag-icon-squared').css('display', 'none');
			}
			if (!itemConfig.adverseNews_url || itemConfig.adverseNews_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-newspaper-o').parent().css('display', 'none')
			} else if (itemConfig.adverseNews_url && itemConfig.adverseNews_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-newspaper-o').parent().css('display', 'block')
			}
			if (!itemConfig.finance_Crime_url || itemConfig.finance_Crime_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-gavel').parent().css('display', 'none')
			} else if (itemConfig.finance_Crime_url && itemConfig.finance_Crime_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-gavel').parent().css('display', 'block')
			}
			if (!itemConfig.pep_url || itemConfig.pep_url.length === 0) {
				$(data.element[0]).find('ul').find('.fa-street-view').parent().css('display', 'none')
			} else if (itemConfig.pep_url && itemConfig.pep_url.length > 0) {
				$(data.element[0]).find('ul').find('.fa-street-view').parent().css('display', 'block')
			}
			if (!itemConfig.high_risk_jurisdiction || itemConfig.high_risk_jurisdiction.toLowerCase() === 'low') {
				$(data.element[0]).find('ul').find('.fa-globe').parent().css('display', 'none');
			} else if (itemConfig.high_risk_jurisdiction && itemConfig.high_risk_jurisdiction.toLowerCase() !== 'low') {
				$(data.element[0]).find('ul').find('.fa-globe').parent().css('display', 'block')
			}
			if (!itemConfig.sanction_source || itemConfig.sanction_source.length === 0) {
				$(data.element[0]).find('ul').find('.fa-ban').parent().css('display', 'none')
			} else if (itemConfig.sanction_source && itemConfig.sanction_source.length > 0) {
				$(data.element[0]).find('ul').find('.fa-ban').parent().css('display', 'block')
			}
			if (itemConfig.entity_id === 'orgChartmainEntity') {
				$(data.element[0]).addClass("child-container additional-container");
			} else if (itemConfig['entity_id'] == 'orgChartsubEntity') { //yellow side border
				$(data.element[0]).addClass("child-container subsidery-container");
				$(data.element[0]).find(".percentageInfo").remove()
			}
			// else if (ownershipdependency == 'IBO') { //blue side border
			// 	$(data.element[0]).addClass("child-container");
			// }
			else if(itemConfig.Ubo_ibo === 'UBO ' && itemConfig.entity_id !== 'orgChartmainEntity' && (  itemConfig.entity_type && personEntitytype.indexOf( itemConfig.entity_type.toLowerCase()) !==-1) ){
				$(data.element[0]).addClass("");
			}
			else if (!itemConfig.Ubo_ibo || (itemConfig.Ubo_ibo !== 'UBO ' &&  itemConfig.entity_id !== 'orgChartmainEntity' && (  itemConfig.entity_type && personEntitytype.indexOf( itemConfig.entity_type.toLowerCase()) ===-1))) { //blue side border
				$(data.element[0]).addClass("child-container");
			}
			var listindirectsubsidary;
			var listindirectshareholder;
			if (itemConfig.indirectEntity == 'indirectsubsidary') { //dashed line in direct entity
				listindirectsubsidary = itemConfig.id;
				$(data.element[0]).addClass("border-red-thin-dashed-t");
			} else if (itemConfig.indirectEntity == 'indirectshareholder') {
				listindirectshareholder = itemConfig.id;
				$(data.element[0]).addClass("border-red-thin-dashed-b");
			}
			if (listindirectsubsidary) {
				$('#' + listindirectsubsidary).addClass('border-red-thin-dashed-t')
			}
			if (listindirectshareholder) {
				$('#' + listindirectshareholder).addClass('border-red-thin-dashed-b')
			}
		}
	}

	function getInfoTemplate() {
		var result = new primitives.orgdiagram.TemplateConfig();
		result.name = "info";
		result.itemSize = new primitives.common.Size(250, 61);
		result.minimizedItemSize = new primitives.common.Size(3, 3);
		result.highlightPadding = new primitives.common.Thickness(4, 4, 4, 4);
		var itemTemplate = jQuery('<div class="bp-corner-all org-chart-item2 bt-item-frame z-999">' +
			'<div name="title-wrapper"  class="width-100 top-heading">'
			/* +'<i  class="icon-wrapper pad-l5 pad-t10 f-16 text-dark-grey fa fa-building-o"  />'
			+ '</i>' */
			+
			'<span  class="flag-icon mar-x5 text-cream  flag-icon-squared flag-wrapper flag-pop-modal c-pointer f-9"></span>' +
			'<h3 name="title" class="bp-item text-overflow">' +
			'</h3>' 
			+'<span class="square-16 mar-l10 pad-t3 p-rel showspinner d-none" style="position: absolute;    right: 5%;    bottom: 1%;    transform: translate(-5%,-1%);"><span class="custom-spinner square-16 p-rel ">'
			+'<i class="fa fa-spinner fa-spin"></i>'
			+'</span></span>'
			+'<div class="bottom-details-wrapper" style="padding-left:55px;">' +
			'<span name= "description" class="text-cream orgTooltipclass description percentageInfo"></span>' +
			'<ul class="list-unstyled progressbar-list d-flex mar-b0"  name ="ownershipPercentages">' +
			'</ul>' +
			'</div>' +
			'<ul class="d-flex ai-c">' +
			'<li>' +
			'<i class= "fa text-cream fa-ban user-popup c-pointer">' +
			'</i>' +
			'</li>' +
			'<li>' +
			'<i class= "fa text-cream fa-street-view street-popup c-pointer ">' +
			'</i>' +
			'</li>' +
			'<li>' +
			'<i class= "fa text-cream fa-globe globe-popup c-pointer ">' +
			'</i>' +
			'</li>' +
			'<li>' +
			'<i class= "fa text-cream fa-gavel gavel-popup c-pointer ">' +
			'</i>' +
			'</li>' +
			'<li>' +
			'<i class= "fa text-cream fa-newspaper-o newspaper-popup c-pointer ">' +
			'</i>' +
			'</li>' +
			'</ul>' +
			'</div>' +
			'</div>'
		).css({
			width: result.itemSize.width + "%",
			height: result.itemSize.height + "px"
		}).addClass("bp-item bp-corner-all bt-item-frame");
		result.itemTemplate = itemTemplate.wrap('<div>').parent().html();
		return result;
	}
	$("body").on("click", ".org-chart-item2", function (e) {
		e.preventDefault();
		e.stopPropagation();
		//below section handles the Data for popover on click of entity
		if (entityDataOnClikOfOdg) {
			var flagCountry = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.country) ? entityDataOnClikOfOdg.country : '';
			var flag_class_code = entityDataOnClikOfOdg && entityDataOnClikOfOdg.juridiction ? ('flag-icon-' + entityDataOnClikOfOdg.juridiction.toLowerCase()) : ((entityDataOnClikOfOdg.basic && entityDataOnClikOfOdg.basic.isDomiciledIn) ? ('flag-icon-' + entityDataOnClikOfOdg.basic.isDomiciledIn.toLowerCase()) : '');
			var person_company_icon = entityDataOnClikOfOdg.entity_type ? (((personEntitytype.indexOf(entityDataOnClikOfOdg.entity_type.toLowerCase())) !== -1) ? 'fa-user' : 'fa-university') : 'fa-university'
			var evidenceUrl = entityDataOnClikOfOdg.source_evidence_url ? entityDataOnClikOfOdg.source_evidence_url : (entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : 'javascript:void(0);');
			var evidenceUrl_href = entityDataOnClikOfOdg.source_evidence_url ? entityDataOnClikOfOdg.source_evidence_url : 'javascript:void(0);';
			var evidenceSource = entityDataOnClikOfOdg.source_evidence ? entityDataOnClikOfOdg.source_evidence : '';
			var evidence_pointer = entityDataOnClikOfOdg.source_evidence_url ? 'c-pointer' : 'c-arrow';
			var evidence_pointer_link = entityDataOnClikOfOdg.source_evidence_url ? '' : 'hidden';
			var pop_indirectPercentage = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.indirectPercentage) ? entityDataOnClikOfOdg.indirectPercentage : 0;
			var pop_totalPercentage = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.totalPercentage) ? entityDataOnClikOfOdg.totalPercentage : 0;
			var fulladdress = entityDataOnClikOfOdg.basic && entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'] && entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'].fullAddress ? entityDataOnClikOfOdg.basic['mdaas:RegisteredAddress'].fullAddress : '';
			var entity_role = '';
			if (entityDataOnClikOfOdg.entity_id) {
				entity_role = entityDataOnClikOfOdg.entity_id === 'orgChartmainEntity' ? 'child-container additional-container' : (entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity' ? 'child-container' : '');
			}
			var hide_subsidaries = (entityDataOnClikOfOdg && entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity') ? 'd-none' : '';
			var pepclass = (entityDataOnClikOfOdg.pep_url && entityDataOnClikOfOdg.pep_url.length > 0) ? '' : 'd-none';
			var adverseclass = (entityDataOnClikOfOdg.adverseNews_url && entityDataOnClikOfOdg.adverseNews_url.length > 0) ? '' : 'd-none';
			var financeclass = (entityDataOnClikOfOdg.finance_Crime_url && entityDataOnClikOfOdg.finance_Crime_url.length > 0) ? '' : 'd-none';
			var sanctionclass = (entityDataOnClikOfOdg.sanction_source && entityDataOnClikOfOdg.sanction_source.length > 0) ? '' : 'd-none';
			var jurisdictionclass = (entityDataOnClikOfOdg.high_risk_jurisdiction && entityDataOnClikOfOdg.high_risk_jurisdiction.toLowerCase() !== 'low') ? '' : 'd-none';
			var numberOfShares = entityDataOnClikOfOdg.numberOfShares ? (!isNaN(entityDataOnClikOfOdg.numberOfShares) ? Number(entityDataOnClikOfOdg.numberOfShares).formatAmt(0) : entityDataOnClikOfOdg.numberOfShares) : 'N/A';
			var pepLength = (entityDataOnClikOfOdg.pep_url && entityDataOnClikOfOdg.pep_url.length > 0) ? entityDataOnClikOfOdg.pep_url.length : 0;
			var adversenewsLength = 0;
			if (entityDataOnClikOfOdg.adverseNews_url && entityDataOnClikOfOdg.adverseNews_url.length > 0) {
				entityDataOnClikOfOdg.adverseNews_url.map(function (val) {
					adversenewsLength = adversenewsLength + (val.count ? val.count : 0)
				});
			}
			var financenewsLength = 0;
			if (entityDataOnClikOfOdg.finance_Crime_url && entityDataOnClikOfOdg.finance_Crime_url.length > 0) {
				entityDataOnClikOfOdg.finance_Crime_url.map(function (val) {
					financenewsLength = financenewsLength + (val.count ? val.count : 0)
				});
			}
			var sanctionLength = (entityDataOnClikOfOdg.sanction_source && entityDataOnClikOfOdg.sanction_source.length > 0) ? entityDataOnClikOfOdg.sanction_source.length : 0;
			var jurisdictionLength = (entityDataOnClikOfOdg.high_risk_jurisdiction && entityDataOnClikOfOdg.high_risk_jurisdiction.toLowerCase() !== 'low') ? 1 : 0;
			var showScreeningList = (pepLength === 0 && adversenewsLength === 0 && financenewsLength === 0 && sanctionLength === 0 && jurisdictionLength === 0) ? 'd-none' : '';
			var uboClass = '';
			var Ubo_ibo = '';
			if (entityDataOnClikOfOdg.entity_id === 'orgChartmainEntity') {
				entity_role = 'child-container additional-container';
				uboClass = 'd-none';
			} else if (entityDataOnClikOfOdg.entity_id === 'orgChartsubEntity') {
				entity_role = 'child-container subsidery-container';
				uboClass = 'd-none';
			} else if (entityDataOnClikOfOdg.Ubo_ibo === "UBO ") {
				Ubo_ibo = 'UBO '; // +pop_totalPercentage;
				entity_role = '';
			} else if (entityDataOnClikOfOdg.Ubo_ibo === "IBO ") {
				Ubo_ibo = 'IBO '; //+pop_totalPercentage;
				entity_role = 'child-container';
			} else {
				Ubo_ibo = ''; //pop_totalPercentage;
				uboClass = 'd-none';
				entity_role = 'child-container';
			}
			var deletebtn = false;
			if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && !$scope.showSubsidaries) {
				deletebtn = false;
			} else if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length == 0) {
				deletebtn = true;
			} else if (entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && entityDataOnClikOfOdg.subsidiaries && entityDataOnClikOfOdg.subsidiaries.length > 0 && $scope.showSubsidaries) {
				deletebtn = false;
			} else if (entityDataOnClikOfOdg.entity_id !== "orgChartmainEntity" && entityDataOnClikOfOdg.parents && entityDataOnClikOfOdg.parents.length > 0 && (!entityDataOnClikOfOdg.subsidiaries || entityDataOnClikOfOdg.subsidiaries.length > 0) && $scope.showSubsidaries) {
				deletebtn = true;
			}
			$scope.popOverEditDataForOwnership = entityDataOnClikOfOdg;
			$timeout(function () {
				deleteSelectedEntity = entityDataOnClikOfOdg;
			}, 500);
			$(".entity_popover").css("display", "block");
			if (entityDataOnClikOfOdg.name) {
				$(".entity_popover").html('<div class="ownership-popover-wrapper right-pop ownership-container ' + entity_role + ' height-100 border-b0">' +
				'<div class="top-content-wrapper pad-b5 border-cream-thin-b">' +
				'<div class="bp-corner-all bt-item-frame bg-transparent">' +
				'<span class="f-16 c-pointer text-dark-cream f-12 p-abs t-5 r-10">✖</span>' +
				'<div class="width-100 detailed-pop-wrapper d-flex top-heading">' +
				'<div class="p-rel mar-r5 icon-flag-wrapper">' +
				'<span class="flag-icon mar-x5 text-cream  flag-icon-squared flag-wrapper popover-flag flag-pop-modal c-pointer f-9 ' + flag_class_code + '" ></span>' +
				'<i class="entity-icon fa ' + person_company_icon + '" style="color:#4c9d20"></i>' +
				'</div>' +
				'<div>' +
				'<div class= "d-flex ai-c jc-sb">' +
				'<h3 name="title" class="bp-item pad-t5 orgChartParentEntity shareCubes">' +
				entityDataOnClikOfOdg.name +
				'</h3>' +
				'<div class= "d-if ai-c pad-r25 mar-l10">' +
				'<button style="background: transparent;border:transparent;" class="as-fs" onclick="window.addEntityOwnershipModal()" ><i class="fa fa-plus-circle f-16 text-dark-blue c-pointer" style="position: relative;top: 3px;"></i></button>' +
				'<button style="background: transparent;border:transparent;" class="as-fs" onclick="window.showOwnerShipEntityEditModal()"><i class="fa fa-pencil f-14 text-dark-cream c-pointer" style="position: relative;top: 3px;"></i></button>' +
				(deletebtn ? ('<button style="background: transparent;border:transparent;" class="as-fs"><i class="fa fa-trash-o f-16 text-coral-red deleteScreeningfromModal" style="position: relative;top: 3px;"></i></button>') : '') +
				'</div>' +
				'</div>' +
				'<p class="mar-t0 mar-b10 text-cream" style="">' + fulladdress + '</p>' +
				'</div>' +
				'</div>' +
				'<div class="bottom-list-wrapper text-right ' + showScreeningList + '">' +
				'<ul class="d-if pad-x5 custom-list ai-c">' +
				'<li class="text-capitalize  street-popup ' + pepclass + ' bst_tooltip_wrapper" name="Pep alerts"><i class="fa text-cream fa fa-street-view c-pointer"></i><span>(' + pepLength + ')</span></li>' +
				'<li class="text-capitalize  user-popup ' + sanctionclass + '  bst_tooltip_wrapper" name="Sanction alerts"><i class="fa text-cream fa-exclamation-triangle c-pointer"></i><span>(' + sanctionLength + ')</span></li>' +
				'<li class="text-capitalize  globe-popup ' + jurisdictionclass + ' bst_tooltip_wrapper" name="Jurisdiction"><i class="fa text-cream fa-globe c-pointer"></i><span>(' + jurisdictionLength + ')</span></li>' +
				'<li class ="text-capitalize  gavel-popup ' + financeclass + '  bst_tooltip_wrapper " name="Finance news"><i class="fa text-cream fa-gavel c-pointer "></i><span>(' + financenewsLength + ')</span></li>' +
				'<li class="text-capitalize  newspaper-popup ' + adverseclass + ' bst_tooltip_wrapper" name="news"><i class="fa text-cream fa-newspaper-o c-pointer"></i><span>(' + adversenewsLength + ')</span></li>' +
				'</ul>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<div class="bottom-content-wrapper border-cream-thin-t">' +
				'<div class="' + hide_subsidaries + '">' +
				'<p class="text-cream d-flex mar-b5 mar-t10  ai-c"><span name="description" class="text-cream mar-r5 f-10 pad-x5 description percentageInfo ' + uboClass + '">' + Ubo_ibo + '</span><span>Number of shares : ' + numberOfShares + '</span></p>' +
				'<ul class="list-unstyled progressbar-list mar-b0">' +
				'<li class="progressbar-list-item mar-b5 pad-t0">' +
				'<div class="left-col f-12 text-cream" style="width: 120px;">Indirect Ownership </div>' +
				'<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">' +
				'<div class="progress-bar progress-gradient-blue" role="progressbar" aria-valuenow="' + parseInt(pop_indirectPercentage) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseInt(pop_indirectPercentage) + '%">' +
				'<span class="sr-only ">' + parseInt(pop_indirectPercentage).toFixed(2) + '</span></div>' +
				'</div>' +
				'<div class="right-col f-12 text-cream" style="width: 60px;">' + pop_indirectPercentage.toFixed(2) + '%</div>' +
				'</li>' +
				'<li class="progressbar-list-item mar-b5">' +
				'<div class="left-col f-12 text-cream" style="width:120px;">Direct Ownership</div>' +
				'<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">' +
				'<div class="progress-bar progress-gradient-curious-blue" role="progressbar" aria-valuenow="' + parseInt(pop_totalPercentage) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseInt(pop_totalPercentage) + '%">' +
				'<span class="sr-only ">' + pop_totalPercentage.toFixed(2) + '%' + '</span>' +
				'</div>' +
				'</div>' +
				'<div class="right-col f-12 text-cream" style="width: 60px;">' + pop_totalPercentage.toFixed(2) + '%</div>' +
				'</li>' +
				'</ul>' +
				'<a class="text-dark-cream roboto-regular f-12 ' + evidence_pointer + '" href="' + evidenceUrl_href + '" target="_blank">' + evidenceSource + '<span class= "fa mar-x5 fa-external-link ' + evidence_pointer + ' ' + evidence_pointer_link + '"> </span></a>' +
				'<p class="text-dark-cream mar-b0">Inner Source (fromSource)</p>'+
				'</div>' +
				'</div>');
			
				var p = $('#maximizeAllchart')
				var position = p.offset();
				var windowWidth = window.innerWidth;
				var tooltipWidth = windowWidth - $('.entity_popover').width() - 100;
				if ((e.pageX > tooltipWidth)) {
					$(".entity_popover").css("left", ((e.pageX) - $('.entity_popover').width() - 100) + "px");
				} else {
					$(".entity_popover").css("left", (e.pageX) + 100 + "px");
				}
				$(".entity_popover").css("z-index", "999999");
				$(".entity_popover").css("display", "block");
				return $(".entity_popover").css("top", e.pageY + "px");
		}
	}
	})
	$("body").on("click", function (e) {
		$(".entity_popover").html('');
		$(".entity_popover").css("display", "none");
	});
	$(".entity_popover").on("click", function (e) {
		$("body").off("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
		$("body").on("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
		$timeout(function () {
			$("body").off("click", '.orgdiagram .globe-popup,.orgdiagram .newspaper-popup,.orgdiagram .gavel-popup,.orgdiagram .street-popup,.orgdiagram .user-popup ,.entity_popover .globe-popup,.entity_popover .newspaper-popup,.entity_popover .gavel-popup,.entity_popover .street-popup,.entity_popover .user-popup', handlepopup);
		}, 0);
		$timeout(function () {
			$(".entity_popover").html('');
			$(".entity_popover").css("display", "none");
		}, 500)
	})
	/*
	 * @purpose: Apply the filter values and call API
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.changeChartLevels = function () {
		$scope.widgetCapturePreloader = true;
		filteredApplied = 1;
		console.log('ddddd', $scope.filteredChart);
		var param = {
			identifier: $scope.entitySearchResult["@identifier"],
			url: {
				"url": $scope.entitySearchResult.org_structure_link
			},
			numnberoflevel: $scope.filteredChart.selectedNumberofLevel,
			lowRange: $scope.sliderMinValue,
			highRange: $scope.sliderMaxValue
		};
		if ($scope.entitySearchResult.org_structure_link)
			EntityApiService.getOwnershipPath(param).then(function (response) {
				console.log('response: ', response);
				if (response && response.data && response.data.path) {
					$timeout(function () {
						// tag cloud variables initializations for person
						updateOwnershipRecurssive(response.data.path, chartTimeIntreval)
					}, 15000);
				}
			}, function (err) {
				console.log(err);
			});
	};
	/* @purpose: get the final Json of ownership structure
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function updateOwnershipRecurssive(path, chartTimeIntreval) {
		EntityApiService.getCorporateStructure(path).then(function (response) {
			console.log('response: ', response);
			if (response.data && response.data && response.data.status && response.data.status !== "completed") {
				chartTimeIntreval(path);
			} else {
				console.log('Api ended');
			}
			if (response.data && response.data.data && response.data.data.length > 0) {
				//	call final chart function here 
				$scope.widgetCapturePreloader = false;
				onload(response.data.data, "#flowChartViewDiv");
			}
		}, function (err) {
			console.log(err);
			chartTimeIntreval(path);
		});
	}
	/* @purpose: call back function hits API for every 15 sec untill APi status is completed
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function chartTimeIntreval(path) {
		console.log(path);
		$timeout(function () {
			updateOwnershipRecurssive(path, chartTimeIntreval)
		}, 15000);
	}
	/* @purpose: Initialize the value of Slider
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function sliderAvgExpen(min, max) {
		$scope.sliderMinValue = min;
		$scope.sliderMaxValue = max;
		$("#sliderAvgExpen").find(".price-range-min").html(min);
		$("#sliderAvgExpen").find(".price-range-max").html(max);
		$('#sliderAvgExpen').slider({
			range: true,
			min: 0,
			max: 100,
			values: [min, max],
			stop: function (event, ui) {
				percentageRange(ui.values);
				if (ui.values[0] == ui.values[1]) {
					$('#sliderAvgExpen .price-range-both i').css('display', 'none');
				} else {
					$('#sliderAvgExpen .price-range-both i').css('display', 'inline');
				}
				if (collision($('#sliderAvgExpen .price-range-min'), $('#sliderAvgExpen .price-range-max')) == true) {
					$('#sliderAvgExpen .price-range-min, #sliderAvgExpen .price-range-max').css('opacity', '0');
					$('#sliderAvgExpen .price-range-both').css('display', 'block');
				} else {
					$('#sliderAvgExpen .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sliderAvgExpen .price-range-both').css('display', 'none');
				}
			}
		});
		$('#sliderAvgExpen .ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#sliderAvgExpen').slider('values', 0) + '</span>');
		$('#sliderAvgExpen .ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#sliderAvgExpen').slider('values', 1) + '</span>');
	}
	$timeout(function () {
		sliderAvgExpen(10, 100);
	}, 1000);
	/* @purpose: Slider function hides when slideer key collide
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function collision($div1, $div2) {
		var x1 = $div1.offset().left;
		var w1 = 40;
		var r1 = x1 + w1;
		var x2 = $div2.offset().left;
		var w2 = 40;
		var r2 = x2 + w2;
		if (r1 < x2 || x1 > r2) return false;
		return true;
	}
	/* @purpose: get the values of slider and show on the slider move
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	function percentageRange(values) {
		console.log('values: ', values);
		$timeout(function () {
			$scope.sliderMinValue = values[0];
			$scope.sliderMaxValue = values[1];
		}, 0);
		$("#sliderAvgExpen").find(".price-range-min").html(values[0]);
		$("#sliderAvgExpen").find(".price-range-max").html(values[1]);
	}
	/* @purpose: onchange of risk value initalize the slider values
	 * @created: 3 oct 2018
	 * @returns: none 
	 * @author:Ram
	 */
	$scope.riskChange = function () {
		if ($scope.filteredChart.riskRatio === 'high') {
			$timeout(function () {
				sliderAvgExpen(25, 100);
				$scope.filteredChart.applyButton = true;
			}, 500);
		} else {
			$scope.filteredChart.applyButton = false;
			sliderAvgExpen(10, 100);
			//				$( "#sliderAvgExpen" ).slider( "destroy" );
		}
	}
	$("body").on("click", '.deleteScreeningfromModal', function () {
		var removedCompanyIndex = _.findIndex(ChartOptions, {
			'id': deleteSelectedEntity.id,
			'name': deleteSelectedEntity.name
		});
		var actualscreeningCompanyIndex  = _.findIndex(scope.actualScreeningData, { '@identifier': deleteSelectedEntity.identifier, 'name': deleteSelectedEntity.name });
		var screeningDatacompanyIndex  =  _.findIndex(scope.screeningData, { '@identifier': deleteSelectedEntity.identifier, 'name': deleteSelectedEntity.name });
		var customAddedscreeningIndex  =  _.findIndex(customEntites.adddeleteEntity, { '@identifier': deleteSelectedEntity.identifier, 'name': deleteSelectedEntity.name });
		var reportsubsidaryIndex  =  _.findIndex(scope.subsidiaries[0].subsidiaries, { 'id': deleteSelectedEntity.id, 'name': deleteSelectedEntity.name });
		if (removedCompanyIndex !== -1) {
			ChartOptions.splice(removedCompanyIndex, 1);
			filteredApplied = 1;
			onload(ChartOptions, "#flowChartViewDiv");
		}
		if(actualscreeningCompanyIndex !==-1){
			scope.actualScreeningData.splice(actualscreeningCompanyIndex, 1);
		}
		if(screeningDatacompanyIndex !==-1){
			scope.screeningData.splice(screeningDatacompanyIndex, 1);
		}
		if(reportsubsidaryIndex !==-1){
			scope.subsidiaries[0].subsidiaries.splice(reportsubsidaryIndex, 1);
		}
		if(customAddedscreeningIndex !== -1){
			customEntites.adddeleteEntity.splice(customAddedscreeningIndex,1)
		}
		EntityorgChartService.deleteEntityFomChart(deleteSelectedEntity);

	
	});
	var owenshipIndex;
	var modalInstanceAdd;
	window.addEntityOwnershipModal = function () {
		$scope.entitySearchResult.list.addEntitymodalChart = 'person';
		$scope.enterFirstname = false;
		reintializeEditScreening();
		modalInstanceAdd = $uibModal.open({
			templateUrl: 'addOwnership.html',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow  bst_modal add-ownership-modal add-new-officer',
			scope: $scope
		});
		owenshipIndex = ChartOptions.map(function (d) {
			return d['identifier'];
		}).indexOf($scope.popOverEditDataForOwnership.identifier)
		modalInstanceAdd.result.then(function (response) {}, function (reject) {});
		console.log(entityDataOnClikOfOdg, 'entityDataOnClikOfOdg')
	}
	$scope.addOwnershipPerson = {
		"firstName": "",
		"lastName": ""
	}
	reintializeEditScreening();

	function reintializeEditScreening() {
		$scope.addOwnershipCompany = {
			FalseNews: [],
			adverseNews_url: [],
			basic: {},
			country: "",
			entity_id: "",
			entity_ref_id: "",
			entity_type: "",
			finance_Crime_url: [],
			hasURL: "",
			id: "",
			identifier: "",
			indirectPercentage: '',
			name: "",
			jurisdiction: "",
			news: [],
			non_negativenews: [],
			officership: [],
			parentIds: [],
			parents: [],
			pep: [],
			pep_url: [],
			sanction_source: [],
			sanctions: [],
			screeningFlag: '',
			screeningUrl: "",
			source_evidence: "",
			subsidiaries: [],
			title: "",
			totalPercentage: '',
			'sanction_bst:description':[]
		};
		$scope.addOwnershipPerson.firstName = '';
		$scope.addOwnershipPerson.lastName = '';
		$scope.countryJurisdiction = {
			jurisdictionOriginalName: 'Select Country'
		};
	}
	$scope.addNewCompanyOwnershipFromModal = function (status, selectedCountry) {
		console.log('deleteSelectedEntity: ', deleteSelectedEntity);
		var name = $scope.entitySearchResult.list.addEntitymodalChart === 'person' ? ($scope.addOwnershipPerson.firstName + '' + ($scope.addOwnershipPerson.lastName ? ' '+$scope.addOwnershipPerson.lastName : '')) : $scope.addOwnershipCompany.name;
		if (!status) {
			modalInstanceAdd.dismiss('close');
		}
		if (($scope.addOwnershipPerson.firstName || $scope.addOwnershipCompany.name) && status && name.trim().length > 0) {
			modalInstanceAdd.dismiss('close');
			var idRandom = makeid();
			$scope.addOwnershipCompany.entity_type = $scope.entitySearchResult.list.addEntitymodalChart;
			$scope.addOwnershipCompany.entity_id = (entityDataOnClikOfOdg.entity_id === "orgChartsubEntity") ? "orgChartsubEntity" : "orgChartParentEntity";
			var addcompanyIndex = _.findIndex(ChartOptions, {
				identifier: deleteSelectedEntity.identifier
			});
			 ChartOptions[addcompanyIndex].parents.push(idRandom);
			 $scope.addOwnershipCompany.child = ChartOptions[addcompanyIndex].id;
			$scope.addOwnershipCompany.id = idRandom;
			$scope.addOwnershipCompany.name = name;
			$scope.addOwnershipCompany.title = $scope.addOwnershipCompany.name;
			$scope.addOwnershipCompany.jurisdiction = $scope.countryJurisdiction.jurisdictionName ? $scope.countryJurisdiction.jurisdictionName.toUpperCase() : '';
			$scope.addOwnershipCompany.juridiction = $scope.addOwnershipCompany.jurisdiction;
			$scope.addOwnershipCompany.country = $scope.countryJurisdiction.jurisdictionOriginalName;
			if ($scope.entitySearchResult.list.addEntitymodalChart === 'person') {
				$scope.addOwnershipCompany.Ubo_ibo = $scope.override === true ? "UBO " : ($scope.addOwnershipCompany.Ubo_ibo ? $scope.addOwnershipCompany.Ubo_ibo : '');
			}
			$scope.widgetCapturePreloader = true;
			// $scope.addOwnershipCompany.identifier = idRandom + 'dum';
			// ChartOptions.push($scope.addOwnershipCompany);
			// scope.screeningData.push($scope.addOwnershipCompany);
			// scope.actualScreeningData.push($scope.addOwnershipCompany);
			// scope.subsidiaries[0].subsidiaries.push($scope.addOwnershipCompany);
			getTheentityScreningDetails($scope.addOwnershipCompany);
			window.customerOutreachUpdateData(name,$scope.entitySearchResult.list.addEntitymodalChart	);
			// var filterDataChart = [];
			// filteredApplied = 1;
			// onload(ChartOptions, "#flowChartViewDiv");
			// reintializeEditScreening();
		} else if (!$scope.addOwnershipPerson.firstName) {
			$scope.enterFirstname = true;
		}

	};
	function getTheentityScreningDetails(addOwnershipCompany) {
		var queryCompany = {
			name: addOwnershipCompany.name,
			website: addOwnershipCompany.hasURL,
			country: addOwnershipCompany.jurisdiction ? addOwnershipCompany.jurisdiction.toUpperCase() : (addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction.toUpperCase() : addOwnershipCompany.country),
			entityType: $scope.entitySearchResult.list.addEntitymodalChart,
			dob: addOwnershipCompany.dateOfBirth,
			requestId: addOwnershipCompany.requestId,
			identifier: addOwnershipCompany.identifier
		};
		EntityApiService.getScreeningDetailsCompany(queryCompany).then(function (companyResponse) {
			if (companyResponse && companyResponse.data && !_.isEmpty(companyResponse.data) && companyResponse.data.name) {
				var mainscreeningIndex = checkAvailablityinChart(companyResponse);
				var actualScreeningIndex = checkAvailablityinScreening(companyResponse, scope.actualScreeningData);
				var ScreeningDataIndex = checkAvailablityinScreening(companyResponse, scope.screeningData);
				var customScreeningIndex =checkAvailablityinScreening(companyResponse, customEntites.screeningaddition);
				var financeNews = companyResponse.data.news ? (companyResponse.data.news.length > 0 ? classtype(companyResponse.data.news, companyResponse.data, 'Financial Crime') : []) : [];
				var adverseNews = companyResponse.data.news ? (companyResponse.data.news.length > 0 ? classtype(companyResponse.data.news, companyResponse.data, 'Neutral') : []) : [];
				var object = new EntityAddition(addOwnershipCompany, companyResponse, financeNews, adverseNews);
				var screening = new ScreeningObject(companyResponse, addOwnershipCompany, adverseNews, financeNews);
				if (companyResponse.data.status === "pending") {
					object.showspinner = true;
					if (mainscreeningIndex === -1) {
						ChartOptions.push(object);
						EntityorgChartService.addEntityToChart(object);
					} else {
						alreadyExistingComapny(financeNews, adverseNews, companyResponse, mainscreeningIndex);
					}
					if (ScreeningDataIndex !== -1) {
						addDataToScreenigTable(scope.actualScreeningData, actualScreeningIndex, screening);
					} else {
						scope.screeningData.push(screening);
					}
					if (actualScreeningIndex !== -1) {
						addDataToScreenigTable(scope.screeningData, ScreeningDataIndex, screening);
					} else {
						scope.actualScreeningData.push(screening);
					}
					if(customScreeningIndex === -1){
						EntityorgChartService.addscreeningTotable(screening);
					}else{
						customEntites.screeningaddition[customScreeningIndex] = screening;
					}
					/* Below function call same function Untill we get status 'done' for every 15 seconds */
					setTimeout(function () {
						getTheentityScreningDetails(object);
					}, 15000);
					filteredApplied = 1;
					onload(ChartOptions, "#flowChartViewDiv");
					reintializeEditScreening();
				} else if (companyResponse.data.status === "done") {
					object.showspinner = false;
					if (mainscreeningIndex === -1) {
						ChartOptions.push(object);
						EntityorgChartService.addEntityToChart(object);
					} else {
						alreadyExistingComapny(financeNews, adverseNews, companyResponse, mainscreeningIndex);
					}
					if (ScreeningDataIndex !== -1) {
						addDataToScreenigTable(scope.actualScreeningData, actualScreeningIndex, screening);
					} else {
						scope.actualScreeningData.push(screening);
					}
					if (actualScreeningIndex !== -1) {
						addDataToScreenigTable(scope.screeningData, ScreeningDataIndex, screening);
					} else {
						scope.screeningData.push(screening);
					}
					if(customScreeningIndex === -1){
						EntityorgChartService.addscreeningTotable(screening);
					}else{
						customEntites.screeningaddition[customScreeningIndex] = screening;
					}

				}
				// (addOwnershipCompany);
				scope.subsidiaries[0].subsidiaries.push(addOwnershipCompany);
				if ((financeNews.length > 0) || (adverseNews.length > 0) || (companyResponse.data.pep && companyResponse.data.pep.length > 0) || (companyResponse.data.sanctions && companyResponse.data.length > 0)) {
					$scope.identifiedRisk = $scope.identifiedRisk + 1;
					var summaryScreenigobject = new ScreeningObject(companyResponse, addOwnershipCompany, adverseNews, financeNews);
					summaryScreenigobject.showspinner = false;
					scope.summaryScreenig.push(summaryScreenigobject);
				}
				if (companyResponse.data.sanctions && companyResponse.data.sanctions.length > 0) {
					$scope.entitySearchResult.list.summaryCount.sanctionCount = $scope.entitySearchResult.list.summaryCount.sanctionCount + companyResponse.data.sanctions.length;
				}
				if (financeNews && financeNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.financeCount = $scope.entitySearchResult.list.summaryCount.financeCount + (financeNews[0].count ? financeNews[0].count : 0);
				}
				if (adverseNews && adverseNews.length > 0) {
					$scope.entitySearchResult.list.summaryCount.adverseCount = $scope.entitySearchResult.list.summaryCount.adverseCount + (adverseNews[0].count ? adverseNews[0].count : 0);
				}
				if (companyResponse.data.high_risk_jurisdiction && companyResponse.data.high_risk_jurisdiction.toLowerCase() !== 'low') {
					$scope.entitySearchResult.list.summaryCount.highRiskCount = $scope.entitySearchResult.list.summaryCount.highRiskCount + 1;
				}
				if (companyResponse.data.pep && companyResponse.data.pep.length > 0) {
					$scope.entitySearchResult.list.summaryCount.pepCount = $scope.entitySearchResult.list.summaryCount.pepCount + companyResponse.data.pep.length;
				}
				filteredApplied = 1;
				onload(ChartOptions, "#flowChartViewDiv");
				reintializeEditScreening();
				HostPathService.FlashSuccessMessage(companyResponse.data.name + 'Added successfully');

			} //name if ends
			$scope.widgetCapturePreloader = false;

		}, function () {
			$scope.widgetCapturePreloader = false;
			HostPathService.FlashErrorMessage('Something Went Wrong');
		});
	}
	var modalInstanceEdit;
	window.showOwnerShipEntityEditModal = function () {
		if (entityDataOnClikOfOdg && entityDataOnClikOfOdg.jurisdiction) {
			var countryEdit = _.find(scope.countryNames, {
				'jurisdictionName': entityDataOnClikOfOdg.jurisdiction.toLowerCase()
			});
			if (countryEdit) {
				$scope.countryJurisdiction = countryEdit;
				$scope.editCustomSelected = countryEdit.jurisdictionOriginalName;
				$scope.editSelectedFlagSource = countryEdit.flag;
			} else {
				$scope.countryJurisdiction = {
					jurisdictionOriginalName: 'Select Country'
				};
			}
		} else {
			$scope.countryJurisdiction = {
				jurisdictionOriginalName: 'Select Country'
			};
		}
		if(entityDataOnClikOfOdg.Ubo_ibo && entityDataOnClikOfOdg.Ubo_ibo === "UBO "){
			$scope.editUBOValue = true;
			 console.log(entityDataOnClikOfOdg.Ubo_ibo);
		}else{
			$scope.editUBOValue = false;
		}
		if(entityDataOnClikOfOdg.SelectedEntityType){
					$scope.entitySearchResult.list.recognized_entity.entityType = entityDataOnClikOfOdg.SelectedEntityType;
					}
		modalInstanceEdit = $uibModal.open({
			templateUrl: 'editOwnership.html',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow  bst_modal add-ownership-modal add-new-officer',
			scope: $scope
		});
		console.log(entityDataOnClikOfOdg, 'entityDataOnClikOfOdg')
	}
	$scope.saveTheValueOfOwnership = function (status,UBOstatus) {
		//   modalInstanceEdit.dismiss();
		modalInstanceEdit.close();
		if ($scope.popOverEditDataForOwnership.name && status) {
			console.log($scope.popOverEditDataForOwnership, 'popOverEditDataForOwnership')
			var index = ChartOptions.map(function (d) {
				return d['identifier'];
			}).indexOf($scope.popOverEditDataForOwnership.identifier);
			$scope.popOverEditDataForOwnership.title = $scope.popOverEditDataForOwnership.name;
			$scope.popOverEditDataForOwnership.country = $scope.countryJurisdiction.jurisdictionOriginalName !== "Select Country" ? $scope.countryJurisdiction.jurisdictionOriginalName : "";
			$scope.popOverEditDataForOwnership.jurisdiction = $scope.countryJurisdiction.jurisdictionName;
			$scope.popOverEditDataForOwnership.juridiction = $scope.countryJurisdiction.jurisdictionName;
			$scope.popOverEditDataForOwnership.Ubo_ibo =  (!$scope.popOverEditDataForOwnership.Ubo_ibo || $scope.popOverEditDataForOwnership.Ubo_ibo ==="UBO " ) ? (UBOstatus === true ? 'UBO ' : '') : $scope.popOverEditDataForOwnership.Ubo_ibo;
			$scope.popOverEditDataForOwnership.basic = {};
			ChartOptions[index] = $scope.popOverEditDataForOwnership;
			EntityorgChartService.editChartEntity($scope.popOverEditDataForOwnership);
			filteredApplied = 1;
			onload(ChartOptions, "#flowChartViewDiv");
		var mainscreeningIndex = _.findIndex(scope.actualScreeningData, {
				'@identifier': $scope.popOverEditDataForOwnership.identifier
			});
			var shownScreeninIndex = _.findIndex(scope.screeningData, {
				'@identifier': $scope.popOverEditDataForOwnership.identifier
			});
			var reportOwnership =   _.findIndex(scope.subsidiaries[0].subsidiaries, {
				'identifier': $scope.popOverEditDataForOwnership.identifier
			});
			if (shownScreeninIndex !== -1) {
				scope.screeningData[shownScreeninIndex] = $scope.popOverEditDataForOwnership;
			}
			if (mainscreeningIndex !== -1) {
				scope.actualScreeningData[mainscreeningIndex] = $scope.popOverEditDataForOwnership;
			}
			if (reportOwnership !== -1) {
				scope.subsidiaries[0].subsidiaries[reportOwnership] = $scope.popOverEditDataForOwnership;
			}
		}
	}

	function makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 5; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	$scope.countryJurisdiction = {
		jurisdictionOriginalName: 'Select Country'
	};
	$scope.countrySelected = function (country,JurisdictionObject,type) {
		$scope["SelectedFlagSource"+type] =JurisdictionObject.flag
		$scope.countryJurisdiction = JurisdictionObject;
	}
	$scope.addEntitytabChange = function (value) {
		$scope.entitySearchResult.list.addEntitymodalChart = value;
		reintializeEditScreening();
	}
	function classtype(news,val,type){
		var data = _.find(news, { 'classification': type});
		var news =[];
		if (data && data.count && data['_links'] && data['_links'].articles) {
			news.push({
				article: data['_links'].articles,
				name: val.name,
				entity_id:val.entity_id,
				val:val,
				Ubo_ibo : val.Ubo_ibo,
				count:data.count
			});
		}
		return news;
	}
	function EntityAddition(addOwnershipCompany, companyResponse, financeNews, adverseNews) {
		this.news = companyResponse.data.news,
		this['@identifier'] = companyResponse.data.identifier,
		this.identifier = companyResponse.data.identifier,
		this.level = 2,
		this.jurisdiction = addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction.toLowerCase() : '',
		this.juridiction = addOwnershipCompany.juridiction ? addOwnershipCompany.juridiction.toLowerCase() : '',
		this.parentIds = [],
		this.entity_id = addOwnershipCompany.entity_id,
		this.entity_ref_id = '',
		this.child = addOwnershipCompany.child,
		this.hasURL = addOwnershipCompany.hasURL,
		this.entity_type = addOwnershipCompany.entity_type && addOwnershipCompany.entity_type.entity_type ?  addOwnershipCompany.entity_type.entity_type : addOwnershipCompany.entity_type,
		this.finance_Crime_url = financeNews,
		this.adverseNews_url = adverseNews,
		this.screeningFlag = true,
		this.name = companyResponse.data.name,
		this.screeningUrl = companyResponse.data.screeningUrl,
		this.id = addOwnershipCompany.id,
		this.totalPercentage = addOwnershipCompany.totalPercentage,
		this.indirectPercentage = addOwnershipCompany.indirectPercentage,
		this.parents = [],
		this.status = "",
		this.title = addOwnershipCompany.name,
		this.Paridentifier = "",
		this.sanction_source = (companyResponse.data.sanctions && companyResponse.data.sanctions.length > 0) ? companyResponse.data.sanctions : [],
		this.pep_url = (companyResponse.data.pep && companyResponse.data.pep.length > 0) ? companyResponse.data.pep : [],
		this.Ubo_ibo = addOwnershipCompany.Ubo_ibo ? addOwnershipCompany.Ubo_ibo : '',
		this.country = addOwnershipCompany.country ? addOwnershipCompany.country : '',
		this.numberOfShares = addOwnershipCompany.numberOfShares ? addOwnershipCompany.numberOfShares : '',
		this.requestId = companyResponse.data.requestId,
		this.showspinner = true,
		this['sanction_bst:description'] = (companyResponse.data.sanctions && companyResponse.data.sanctions.length > 0) ? companyResponse.data.sanctions : []
	}
	function ScreeningObject(companyResponse,addOwnershipCompany,adverseNews,financeNews){
		this.news =  companyResponse.data.news,
		this.identifier = addOwnershipCompany.identifier ,
		this.level = 2,
		this.jurisdiction = addOwnershipCompany.jurisdiction ,
		this.parentIds = [],
		this.entity_id = addOwnershipCompany.entity_id,
		this.entity_ref_id = '',
		this.childs = [],
		this.adverseNews_url = adverseNews,
		this.pep_url = (companyResponse.data.pep && companyResponse.data.pep.length > 0) ? companyResponse.data.pep : [],
		this.finance_Crime_url = financeNews,
		this.hasURL = addOwnershipCompany.hasURL,
		this.entity_type = addOwnershipCompany.entity_type,
		this.screeningFlag = true,
		this.name =  companyResponse.data.name,
		this.screeningUrl =  companyResponse.data.screeningUrl,
		this.id = addOwnershipCompany.id,
		this.parents = [],
		this.status = "",
		this.title = addOwnershipCompany.name,
		this.Paridentifier = "",
		this.sanction_source = [],
		this.Ubo_ibo = addOwnershipCompany.Ubo_ibo ? addOwnershipCompany.Ubo_ibo :'',
		this.showdeleteIcon =true,
		this.showspinner = true,
		this.country = addOwnershipCompany.country ? addOwnershipCompany.country :''
	  }
	function checkAvailablityinChart(companyResponse){
		var mainscreeningIndex = 	_.findIndex(ChartOptions, {
			'name':companyResponse.data.name
		});
		return mainscreeningIndex;
	}	
	function alreadyExistingComapny(financeNews,adverseNews,companyResponse,mainscreeningIndex){
		ChartOptions[mainscreeningIndex].finance_Crime_url = financeNews;
		ChartOptions[mainscreeningIndex].adverseNews_url = adverseNews;
		ChartOptions[mainscreeningIndex].news =  companyResponse.data.news ?  companyResponse.data.news : [];
		ChartOptions[mainscreeningIndex].sanction_source = (companyResponse.data.sanctions && companyResponse.data.sanctions.length > 0) ? companyResponse.data.sanctions : [];
		ChartOptions[mainscreeningIndex].pep_url = (companyResponse.data.pep && companyResponse.data.pep.length > 0) ? companyResponse.data.pep : [];
	}
	function checkAvailablityinScreening(companyResponse,data){
		var actualscreeningIndex = 	_.findIndex(data, {
			'name':companyResponse.data.name
		});
		return actualscreeningIndex; 
	}
	function addDataToScreenigTable(screeningData,ScreeningDataIndex,object){
		screeningData[ScreeningDataIndex] = object;
	}

}