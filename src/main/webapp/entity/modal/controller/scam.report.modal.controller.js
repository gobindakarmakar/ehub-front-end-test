angular.module('ehubEntityApp')
		.controller('ScamReportModalController', scamReportModalController);

scamReportModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'scamReportData'
];

function scamReportModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		scamReportData){
	
	$scope.scamReportData = scamReportData;
	/*
 	*@purpose: close scam report modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
 	/*
	 * @purpose: onViewClickReplies open modal for scam replies
	 * @created: 18 sep 2017
	 * @params: index(number)
	 * @returns: no
	 * @author: sandeep
	*/
	 $scope.onViewClickReplies = function(index){
		 var repliesModalInstance = $uibModal.open({
			templateUrl: 'static/js/modal/views/replies-entity-modal.html',
			controller: 'RepliesEntityModalConteoller',
			windowClass: 'update-entities-modal related-person-modal',
			resolve: {
				repliesData: function(){
					return $scope.scamReportData[index];
				}
			}
		});
		
		 repliesModalInstance.result.then(function(response){
		}, function(error){
		});
	 };
}