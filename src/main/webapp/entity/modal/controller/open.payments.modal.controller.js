angular.module('ehubEntityApp')
		.controller('OpenPaymentsModalConteoller', openPaymentsModalConteoller);

openPaymentsModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'openPaymentsData'
];

function openPaymentsModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		openPaymentsData){
	
	$scope.openPaymentsData = openPaymentsData;
	
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}