angular.module('ehubEntityApp')
		.controller('IncomeSheetModalController', incomeSheetModalController);

incomeSheetModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'yahooStockData',
	'stockModalData'
];

function incomeSheetModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		yahooStockData,
		stockModalData){
	$scope.yahooStockData = yahooStockData;
	$scope.stockModalData = stockModalData;
	/*
 	*@purpose: close bad buyers modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}