angular.module('ehubEntityApp')
		.controller('BadbuyersPersonlistModalController', badBuyersPersonListModalController);

badBuyersPersonListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'badbuyerslistData'
];

function badBuyersPersonListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		badbuyerslistData){
	
	$scope.badbuyerslistData = badbuyerslistData;
	/*
 	*@purpose: close bad buyers modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}