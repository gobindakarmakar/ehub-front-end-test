angular.module('ehubEntityApp')
		.controller('TwitterFollowingFollowersController', TwitterFollowingFollowersController);

TwitterFollowingFollowersController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'followings',
	'followers' 
];

function TwitterFollowingFollowersController(
		$scope,
		$rootScope,
		$uibModalInstance,
		followings,
		followers){

	$scope.socialTabType = followings.length > 0 ? 'Followings': 'Followers';
	$scope.onClickSocialTypeTabs = function(type){
		 $scope.socialTabType = type; 
	};
	$scope.entitySearchResult = {
		list: {
			followings: followings,
			followers: followers
		}
	};
	
	/*
 	*@purpose: close organizations modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}