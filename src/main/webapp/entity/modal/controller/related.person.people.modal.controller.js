angular.module('ehubEntityApp')
		.controller('RealtedPersonPeopleModalConteoller', realtedPersonPeopleModalConteoller);

realtedPersonPeopleModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'personsData'
];

function realtedPersonPeopleModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		personsData){
	/*initializing variables*/
	personsData.entityKeyPersonList.length > 0 ? $scope.relatedPersonTab = 'key Person' :
		personsData.entityKeyExecutiveList.length > 0 ? $scope.relatedPersonTab = 'executives' : 
			personsData.entityBoardmemberList.length > 0 ? $scope.relatedPersonTab = 'boardmembers' : 'coworkers';
	
	$scope.entityKeyPersonList = personsData.entityKeyPersonList;
	$scope.entityKeyExecutiveList = personsData.entityKeyExecutiveList;
	$scope.entityBoardmemberList = personsData.entityBoardmemberList;
	$scope.entityCoworkersList = personsData.entityCoworkersList;

	 /*
	  * @purpose: onClickRelatedPersonTab function to change tab
	  * @created: 20 sep 2017
	  * @params: tabName(string)
	  * @return: no
	  * @auhtor: sandeep
	  */
	 $scope.onClickRelatedPersonTab = function(tabName){
		 $scope.relatedPersonTab = tabName;
	 };
	 /*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}