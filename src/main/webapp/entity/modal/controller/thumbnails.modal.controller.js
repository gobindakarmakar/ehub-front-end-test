angular.module('ehubEntityApp')
		.controller('ThumbnailsModalController', thumbnailsModalController);

thumbnailsModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'thumbnailsData'
];

function thumbnailsModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		thumbnailsData){
	
	$scope.thumbnailsData = thumbnailsData;
	
	/*
 	*@purpose: close thumbnails modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}