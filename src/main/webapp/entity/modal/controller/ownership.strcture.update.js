'use strict';
angular.module('ehubEntityApp')
    .controller('OwnershipstrcutureupdateModalController', ownershipstrcutureupdateModalController);

ownershipstrcutureupdateModalController.$inject = [
    '$scope',
    '$uibModalInstance',
    'updateData',
    'removeCustom'
];

function ownershipstrcutureupdateModalController(
    $scope,
    $uibModalInstance,
    updateData,
    removeCustom
) {
	$scope.removeCustom = removeCustom;
	
	
    $scope.closeWidgetCaptureModal = function () {
    	updateData.removeCustom = $scope.removeCustom;
        $uibModalInstance.close(updateData);
    };
    $scope.dismissModal = function(){
        $uibModalInstance.dismiss('close');
    }
}