'use strict';
angular.module('ehubEntityApp')
.controller('RedditCommentsModalController', redditCommentsModalController);

redditCommentsModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'redditCommentsData'
];

function redditCommentsModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		redditCommentsData){
	$scope.redditCommentsData = redditCommentsData;
	/*
 	*@purpose: close scam report modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
 	/*
	 * @purpose: onViewClickReplies open modal for scam replies
	 * @created: 18 sep 2017
	 * @params: index(number)
	 * @returns: no
	 * @author: sandeep
	*/
	 $scope.onViewClickReplies = function(index){
		 var repliesModalInstance = $uibModal.open({
			templateUrl: 'static/js/modal/views/replies-entity-modal.html',
			controller: 'RepliesEntityModalConteoller',
			windowClass: 'update-entities-modal related-person-modal',
			resolve: {
				repliesData: function(){
					return $scope.scamReportData[index];
				}
			}
		});
		
		 repliesModalInstance.result.then(function(response){
		}, function(error){
		});
	 };
}