angular.module('ehubEntityApp')
		.controller('GeneralInfoModalConteoller', generalInfoModalConteoller);

generalInfoModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'generalDetailList'
];

function generalInfoModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		generalDetailList){
	/*initializing variables*/
	$scope.generalDetailList = generalDetailList;
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
 	 	
 	
}