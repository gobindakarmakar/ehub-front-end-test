angular.module('ehubEntityApp')
		.controller('PersonInterestsModalController', personInterestsModalController);

personInterestsModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'personInterestsData'
];

function personInterestsModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		personInterestsData){
	$scope.personInterests = personInterestsData;
	
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}