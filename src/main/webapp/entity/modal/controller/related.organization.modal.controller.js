angular.module('ehubEntityApp')
		.controller('RelatedOrganizationEntityModalConteoller', relatedOrganizationEntityModalConteoller);

relatedOrganizationEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'relatedOrganizationData'
];

function relatedOrganizationEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		relatedOrganizationData){
	$scope.relatedOrganizationData = relatedOrganizationData;
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}