angular.module('ehubEntityApp')
		.controller('FollowersEntityModalController', followersEntityModalController);

followersEntityModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'followersModalData'
];

function followersEntityModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		followersModalData){
	$scope.followersData = followersModalData;
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}