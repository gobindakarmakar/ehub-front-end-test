angular.module('ehubEntityApp')
		.controller('KeyRelationshipsModalController', keyRelationshipsModalController);

keyRelationshipsModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'keyRelationshipsData'
];

function keyRelationshipsModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		keyRelationshipsData){
	
	$scope.keyRelationshipsData = keyRelationshipsData;
	/*
 	*@purpose: close key relationships modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}