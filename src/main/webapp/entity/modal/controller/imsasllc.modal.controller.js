angular.module('ehubEntityApp')
		.controller('ImsasllclistModalController', imsasllclistModalController);

imsasllclistModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'imsasllclistData'
];

function imsasllclistModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		imsasllclistData){
	
	$scope.imsasllclistData = imsasllclistData;
	/*
 	*@purpose: close imsasllc modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}