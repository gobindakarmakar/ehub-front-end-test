angular.module('ehubEntityApp')
		.controller('OrganizationsListModalController', organizationsListModalController);

organizationsListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'organizationsData'
];

function organizationsListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		organizationsData){
	
	$scope.organizationsData = organizationsData;
	
	/*
 	*@purpose: close organizations modal
 	*@created: 19 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}