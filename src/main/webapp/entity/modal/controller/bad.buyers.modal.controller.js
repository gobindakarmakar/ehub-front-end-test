angular.module('ehubEntityApp')
		.controller('BadBuyersListModalController', badBuyersListModalController);

badBuyersListModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'badBuyersListData'
];

function badBuyersListModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		badBuyersListData){
	
	$scope.badBuyersListData = badBuyersListData;
	/*
 	*@purpose: close bad buyers modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}