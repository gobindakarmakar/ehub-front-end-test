angular.module('ehubEntityApp')
		.controller('EmploymentHistoryModalController', employmentHistoryModalController);

employmentHistoryModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'employmentHistoryData'
];

function employmentHistoryModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		employmentHistoryData){
	$scope.employmentHistory = employmentHistoryData;
	
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}