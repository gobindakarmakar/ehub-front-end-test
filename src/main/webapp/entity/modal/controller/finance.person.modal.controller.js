angular.module('ehubEntityApp')
		.controller('FinancePersonModalController', financePersonModalController);

financePersonModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'entitySearchResult'
];

function financePersonModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		entitySearchResult){
	$scope.entitySearchResult = entitySearchResult;
	/*
 	*@purpose: close bad buyers modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}