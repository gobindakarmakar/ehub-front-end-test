angular.module('ehubEntityApp')
		.controller('FinanceModalController', financeModalController);

financeModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'yahooStockData',
	'stockModalData',
	'entitySearchResult'
];

function financeModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		yahooStockData,
		stockModalData,
		entitySearchResult){
	$scope.yahooStockData = yahooStockData;
	$scope.stockModalData = stockModalData;
	$scope.entitySearchResult = entitySearchResult;
	/*
 	*@purpose: close bad buyers modal
 	*@created: 21 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}