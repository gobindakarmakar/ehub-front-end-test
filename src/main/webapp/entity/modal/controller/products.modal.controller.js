angular.module('ehubEntityApp')
		.controller('ProductsEntityModalConteoller', productsEntityModalConteoller);

productsEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance'
];

function productsEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance){
	/*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
}