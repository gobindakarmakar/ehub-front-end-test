angular.module('ehubEntityApp')
		.controller('StaffAttendedSchoolsModalController', staffAttendedSchoolsModalController);

staffAttendedSchoolsModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'keyStaffAttendedSchoolsData'
];

function staffAttendedSchoolsModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		keyStaffAttendedSchoolsData){
	
	$scope.keyStaffAttendedSchoolsData = keyStaffAttendedSchoolsData;
	/*
 	*@purpose: close key staff attended schools modal
 	*@created: 18 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}