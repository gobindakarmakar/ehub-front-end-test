angular.module('ehubEntityApp')
		.controller('QuestionAnswersModalConteoller', questionAnswersModalConteoller);

questionAnswersModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'answerQuestionData'
];

function questionAnswersModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		answerQuestionData){
	$scope.entity = answerQuestionData;
	/*
 	*@purpose: close social media articles modal
 	*@created: 12 nov 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}