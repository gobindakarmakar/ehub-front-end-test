angular.module('ehubEntityApp')
		.controller('RepliesEntityModalConteoller', repliesEntityModalConteoller);

repliesEntityModalConteoller.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'repliesData'
];

function repliesEntityModalConteoller(
		$scope,
		$rootScope,
		$uibModalInstance,
		repliesData){
	$scope.entity = repliesData;
	/*
 	*@purpose: close social media articles modal
 	*@created: 14 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}