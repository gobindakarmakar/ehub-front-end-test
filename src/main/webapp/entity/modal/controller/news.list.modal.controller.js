angular.module('ehubEntityApp')
		.controller('NewslistModalController', newslistModalController);

newslistModalController.$inject = [
	'$scope',
	'$rootScope',
	'$uibModalInstance',
	'newsDeepWebData'
];

function newslistModalController(
		$scope,
		$rootScope,
		$uibModalInstance,
		newsDeepWebData){
	
	$scope.newsDeepWebData = newsDeepWebData;
	/*
 	*@purpose: close news modal
 	*@created: 22 sep 2017
 	*@params: none
 	*@returns: none
 	*@author: sandeep
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
	
}