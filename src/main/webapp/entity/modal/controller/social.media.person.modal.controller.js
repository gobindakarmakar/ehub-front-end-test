angular.module('ehubEntityApp')
		.controller('SocialMediaPersonController', socialMediaPersonController);

socialMediaPersonController.$inject = [
		'$scope',
		'$state',
		'$rootScope',
		'$stateParams',
		'EntityApiService',
		'$q',
		'EntityGraphService',
		'searchText',
		'$uibModalInstance',
		'searchType'
	];
	function socialMediaPersonController(
			$scope, 
			$state, 
			$rootScope, 
			$stateParams,
			EntityApiService,
			$q,
			EntityGraphService,
			searchText,
			$uibModalInstance,
			searchType) {
	
	 /*initializing entity company related variables*/
	 $scope.disableSearchButton = true;
	 $scope.entityTwitterList = [];
	 $scope.entityInstagramList = [];
	 $scope.entityGooglePlusList = [];
	 $scope.socialMediaActiveCount = 0;
	 $scope.entitySearchResult = {
		 name: searchText
	 };
	 
	 var socialFetcherIds = ['55','56','21'];	 
	 /*
	  * @purpose: getting entity data by name
	  * @created: 19 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	  */
	 function getEntityCompanyDataByName(name) {
		 var deffered = $q.defer();
		 angular.forEach(socialFetcherIds, function(id, key) {
			 var ids = [];
			 ids.push(id);
			 var data = {
	            fetchers: ids, 
	            keyword: name,
	            searchType: searchType
			 };
			 if(ids.indexOf('13') !== -1)
				 data.lightWeight = true;
			 EntityApiService.getEntityDataByTextId(data).then(function(subResponse) {
				angular.forEach(subResponse.data.results, function (subfetcherData, key) {
			            if (subfetcherData.status) {
				           	if(subfetcherData.fetcher === 'plus.google.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
			            				if(edgeValue.relationshipName === 'recent_public_activity') {
									        $scope.entityGooglePlusList.push({
				            					name: subentity.properties.name,
				            					url: subentity.properties.url,
				            					title: edgeValue.entity.properties.title,
				            					coverPhoto: subentity.properties.cover_photo,
				            					lastUpdatedDate: new Date(edgeValue.entity.properties.last_updated_date),
				            					publishedDate: new Date(edgeValue.entity.properties.published_date),
				            					type: 'google'
				            				});
			            				}
			            			});
			            		});
			            	}
				           	if(subfetcherData.fetcher === 'instagram.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
								        if(edgeValue.relationshipName === 'media') {
				            				$scope.entityInstagramList.push({
				            					caption: edgeValue.entity.properties.caption,
				            					thumbnail: edgeValue.entity.properties.thumbnail,
				            					image: subentity.properties.image,
				            					name: subentity.properties.name,
				            					followingCount: subentity.properties.following_count,
				            					followersCount: subentity.properties.followers_count,
				            					type: 'instagram'
				            				});
			            				}
			            			});
			            		});
			            	}
			            	if(subfetcherData.fetcher === 'twitter.com') {
			            		angular.forEach(subfetcherData.entities, function (subentity, subentityKey) {
			            			angular.forEach(subentity.edges, function (edgeValue, edgeKey) {
				            			if(edgeValue.relationshipName === 'tweets') {
				            				$scope.entityTwitterList.push({
			            					  title: subentity.properties.title,
				                              createdOn: new Date(edgeValue.entity.properties.created_at),
				                              image: subentity.properties.image,
				                              text: edgeValue.entity.properties.text,
				                              retweetCount: edgeValue.entity.properties.retweet_count,
				                              type: 'twitter'
				            				});
				            			}
			            			});
			            		});
			            	}
			            }
				});
				 if(key === socialFetcherIds.length - 1){
					 deffered.resolve('completed'); 
				 }
			 }, function(error){
				deffered.reject(error);
			 });
		});
		
		return deffered.promise;
	 }
	 
	 getEntityCompanyDataByName(searchText).then(function(response){
		 $scope.disableSearchButton = false;
	 }, function(){
	 });
	 
	 /*
 	*@purpose: close social media articles modal
 	*@created: 23 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeModal = function(){
 		$uibModalInstance.dismiss('close');
 	};
};