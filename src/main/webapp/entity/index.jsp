<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ehubEntityApp" class="bst_element">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../assets/images/logo-new.png" />
	<title style="text-transform: capitalize !important;" ng-bind="companyOrPersonName"></title>

	<!-------------------------------  Vendor Styles Starts  ------------------------------->
	<link href="./assets/css/entity-vendor.min.css" rel="stylesheet" />
	<!-------------------------------  Vendor Styles Ends  --------------------------------->

	<!-------------------------------  Custom Styles Starts  ------------------------------->
	<link href="./assets/css/entity-styles.min.css" rel="stylesheet" />
	<link href="../vendor/jquery/css/jquery.orgchart.css" rel="stylesheet" type="text/css" />
	<link href="../vendor/jquery/css/primitive.css" rel="stylesheet" type="text/css" />
	<link href="../vendor/jquery/css/flag-icon.min.css" rel="stylesheet">
	<!-------------------------------  Custom Styles Ends  --------------------------------->
	<style>
		.risk-score-panel .panel.custom-panel-wrapper.bg-transparent.autodesk-panel-wrapper .mTSWrapper.mTS_vertical {
    overflow: visible !important; 
    width:100%;  
} 
		.myOwnBg {
			color: white;
		}

		.defaultBg {
			color: blue;
		}

		#pulseVisualizerTextWrapper .popover .popover-inner .popover-content {
			white-space: no-wrap;
			min-width: 200px;
		}

		.adverse-full-modal .modal-content {
			height: calc(100vh - 30px) !important;
			min-height: 550px;
		}

		.adverse-full-modal .modal-dialog {
			margin: 15px auto !important;
		}

		.star {
			position: relative;
			display: inline-block;
			width: 0;
			height: 0;
			margin-left: 0.9em;
			margin-right: 0.9em;
			margin-bottom: 1.2em;
			border-right: 0.3em solid transparent;
			border-bottom: 0.7em solid #fc0;
			border-left: 0.3em solid transparent;
			/* Controlls the size of the stars. */
			font-size: 24px;
		}

		.star:before,
		.star:after {
			content: '';
			display: block;
			width: 0;
			height: 0;
			position: absolute;
			top: 0.6em;
			left: -1em;
			border-right: 1em solid transparent;
			border-bottom: 0.7em solid #fc0;
			border-left: 1em solid transparent;
			transform: rotate(-35deg);
		}

		.star:after {
			transform: rotate(35deg);
		}

		.modal-open .modal {
			z-index: 99999 !important;
		}

		.changed-content-wrapper .mCSB_container,
		.original-content-wrapper .mCSB_container {
			display: flex;
			padding: 0 10px 0 0;
		}

		.question-mark-for-adding {
			color: #3eb6ff !important;
		}
	</style>
</head>

<body ng-cloak class="custom-modal-body bst_element_body c-arrow custom-scroll-wrapper dark-grey-dashboard">
	<flash-message class="main-flash-wrapper"></flash-message>
	<div ng-show="topPanelPreview">
		<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
	</div>

	<div ui-view></div>

	<!--  Chat Panel Wrapper Starts  -->
	<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
	<!--  Chat Panel Wrapper Ends  -->

	<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs">
	</script>

	<!--  ==============================================================================
**********************  Local Path Starts *********************
==============================================================================*/
-->
	<!--------------------------------  Common Vendor Js Starts  ------------------------------->
	<script src='../vendor/jquery/js/jquery.min.js'></script>
	<script src='../vendor/jquery/js/jquery-ui.js'></script>
	<script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
	<script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
	<script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
	<script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
	<script src='../vendor/jquery/js/bootstrap.min.js'></script>
	<script src="../vendor/jquery/js/jquery.orgchart.js"></script>
	<script src='../vendor/jquery/js/displacy-entity.min.js'></script>
	<script src="../vendor/jquery/js/jQuery.highlight.js"></script>

	<script src='../vendor//angular/js/angular.min.js'></script>
	<script src='../vendor//angular/js/angular-ui-router.min.js'></script>
	<script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
	<script src='../vendor/angular/js/angular-sanitize.min.js'></script>
	<script src='../vendor//angular/js/html2canvas.js'></script>
	<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
	<script src='../vendor//angular/js/angular-flash.min.js'></script>
	<script src="../vendor/angular/js/FileSaver.min.js"></script>
	<script src="../vendor/jquery/js/angularjs-dropdown-multiselect.js"></script>

	<script src='../charts/d3.v3.min.js'></script>
	<script src='../charts/d3.v4.min.js'></script>
	<script src='../charts/d3v3.js'></script>
	<script src='../charts/d3js.js'></script>

	<script src='../scripts/VLA/js/cola.v3.min.js'></script>
	<script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
	<script src='../scripts/VLA/js/cytoscape-cola.js'></script>
	<script src='../scripts/VLA/js/jquery.qtip.js'></script>
	<script src='../scripts/VLA/js/cytoscape-qtip.js'></script>
	<!--------------------------------  Common Vendor Js Ends    ------------------------------->

	<!--------------------------------  Transaction Intelligence Vendor Js Starts  ------------------------------->
	<script src='../scripts/VLA/js/cytoscape-cose-bilkent.js'></script>
	<script src='../scripts/VLA/js/cytoscape-cxtmenu.js'></script>
	<script src='../scripts/VLA/js/cytoscape-markov-cluster.js'></script>
	<script src='../scripts/VLA/js/cytoscape-navigator.js'></script>
	<script src='../scripts/VLA/js/cytoscape-ngraph.forcelayout.js'></script>
	<script src='../scripts/VLA/js/cytoscape-panzoom.js'></script>
	<script src='../scripts/VLA/js/cytoscape-undo-redo.js'></script>
	<script src='../scripts/VLA/js/weaver.min.js'></script>
	<script src='../scripts/VLA/js/cytoscape-spread.js'></script>
	<script src='../scripts/VLA/js/random-color.js'></script>
	<script src='../scripts/VLA/js/spectrum.js'></script>
	<script src='../scripts/VLA/js/typeahead.bundle.js'></script>
	<script src='../scripts/VLA/js/jquery.selectric.min.js'></script>
	<script src='../scripts/VLA/js/custom_config.js'></script>
	<script src='../scripts/VLA/js/app.js'></script>

	<!-- 	<script src='../scripts/VLA/js/app_locale_ua.js'></script> 
				<script src='../scripts/VLA/js/app_locale_ar.js'></script>  -->

	<!--------------------------------  Common Vendor Js Ends    ------------------------------->
	<!--------------------------------  Entity Vendor Js Starts  ------------------------------->
	<script src='../vendor//jquery/js/moment.min.js'></script>
	<script src='../vendor/jquery/js/moment.min.js'></script>
	<script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
	<script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
	<script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
	<script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
	<script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
	<script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
	<script src="../vendor/jquery/js/moment-timezone.min.js"></script>
	<script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
	<script src="../vendor/jquery/js/dom-to-image.js"></script>
	<script src="../vendor/jquery/js/html2pdf.bundle.min.js"></script>
	<script src="../vendor/jquery/js/primitive.min.js"></script>
	<script src='../vendor//angular/js/ngStorage.js'></script>
	<script src='../vendor//angular/js/angular-moment.min.js'></script>
	<script src='../vendor//angular/js/socket.io.min.js'></script>
	<script src='../scripts/VLA/js/loadash.js'></script>
	<!--------------------------------  Entity Vendor Js Ends    ------------------------------->
	<!--------------------------------  Entity Vendor Js Starts  ------------------------------->
	<script src='../charts/groupedColumChart.js'></script>
	<script src='../charts/timescaleBubble.js'></script>
	<script src='../charts/timescaleBubble2.js'></script>
	<script src='../charts/bubbleChart.js'></script>
	<script src='../charts/pieChart.js'></script>
	<script src='../charts/leaflet/leaflet.js'></script>
	<script src='../charts/hotTopics.js'></script>
	<script src='../charts/topojson.min.js'></script>
	<script src='../charts/cloud.js'></script>
	<script src='../charts/ClusterBubble.js'></script>
	<script src='../charts/caseTimeline.js'></script>
	<script src='../charts/WorldMap/js/WorldChart.js'></script>
	<script src='../charts/brush.js'></script>
	<script src='../charts/collapse.js'></script>
	<script src='../charts/sank.min.js'></script>
	<script src='../charts/reusablePie.js'></script>
	<script src='../charts/networkChart.js'></script>
	<script src='../charts/networkChartEntity.js'></script>
	<script src='../charts/lineChart.js'></script>
	<script src='../charts/horizontalBarChart.js'></script>
	<script src='../charts/verticalBarChart.js'></script>
	<script src='../charts/cytoscape-cola.js'></script>
	<script src='../charts/datamaps.all.js'></script>
	<script src='../charts/brushMip.js'></script>
	<script src='../charts/timelineSimpleColumn.js'></script>
	<script src='../charts/bubbleEntity.js'></script>
	<script src='../charts/stackedtime.js'></script>
	<script src='../charts/temporal.js'></script>
	<script src='../charts/D3treeMap.js'></script>
	<script src='../charts/D3radarChart.js'></script>
	<script src='../charts/D3heatMap.js'></script>
	<script src='../charts/sankRiskScore.js'></script>
	<!--------------------------------  Entity Vendor Js Ends    ------------------------------->
	<!----------------------------  Initial Configuration JS Starts  ------------------------------->
	<script src="../scripts/app.js"></script>
	<script src="./entity.app.js"></script>
	<script src="./entity.config.js"></script>
	<script src="./entity.run.js"></script>
	<script src='./services/entity.common.tab.service.js'></script>
	<!----------------------------  Initial Configuration JS Ends  ------------------------------->
	<!--------------------------------  Common Scripts Js Starts  ------------------------------>
	<script src='../scripts/common/constants/app.constant.js'></script>
	<script src='../scripts/common/constants/common.constant.js'></script>
	<script src='../scripts/discover/constants/discover.constant.js'></script>
	<script src='../scripts/act/constants/act.constant.js'></script>

	<script src='../scripts/common/services/shared.service.js'></script>
	<script src='../scripts/common/services/top.panel.api.service.js'></script>
	<script src='../scripts/common/services/upload.file.service.js'></script>
	<script src='../scripts/discover/services/discover.api.service.js'></script>
	<script src='../scripts/act/services/act.api.service.js'></script>
	<script src='../scripts/act/services/act.graph.service.js'></script>
	<script src="../scripts/common/services/common.service.js"></script>
	<script src='../scripts/common/services/riskScore.service.js'></script>


	<script src='../scripts/common/js/submenu.controller.js'></script>
	<script src='../scripts/common/js/top.panel.controller.js'></script>
	<script src='../scripts/common/js/advanced.search.controller.js'></script>
	<script src='../scripts/common/js/user.events.controller.js'></script>
	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
	<script src='../scripts/common/js/my.entity.clipboard.controller.js'></script>
	<script src="../scripts/common/js/notification.controller.js"></script>
	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
	<script src="../scripts/common/js/chatbot.controller.js"></script>
	<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
	<script src="../scripts/common/modal/js/dataPopUp.modal.controller.js"></script>
	<script src="./modal/controller/entity.datapopup.modal.controller.js"></script>
	<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>



	<script src='../scripts/common/js/entity.visualiser.js'></script>
    
	<script src='../scripts/common/modal/js/onboarding.modal.controller.js'></script>
	<script src='../scripts/common/modal/js/uploadquestionaire.controller.js'></script>
	<script src='../scripts/common/modal/js/upload.documents.modal.controller.js'></script>
	<script src='../scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js'></script>
	<script src='../scripts/common/modal/js/idv.questionaire.controller.js'></script>
	<script src="../scripts/common/modal/js/create.event.controller.js"></script>
	<script src="../scripts/common/modal/js/participants.event.controller.js"></script>
	<script src="../scripts/common/modal/js/confirmDelSticky.controller.js"></script>
	<script src='../scripts/enrich/services/enrich.api.service.js'></script>
	<!--------------------------------  Common Scripts Js Ends   ------------------------------->

	<!--------------------------------  Common Scripts Js Starts  ------------------------------>
	<script src='../scripts/common/constants/enrich.search.constant.js'></script>
	<script src='../entity/constants/fetchers.list.constant.js'></script>
	<script src='../entity/constants/entityCompany.constant.js'></script>
	<script src='../scripts/enrich/constants/entity.search.constant.js'></script>

	
	<script src='../entity/services/entity.graph.services.js'></script>
	<script src='../scripts/enrich/services/entity.orgchart.service.js'></script>
	<script src='../scripts/enrich/services/enrich.graph.service.js'></script>
	<script src='../scripts/enrich/services/entity.api.services.js'></script>

	<script src='../scripts/common/filters/common.filters.js'></script>

	<script src='../entity/controllers/entity.company.new.controller.js'></script>
	<script src='../entity/controllers/entity.company.overview.controller.js'></script>
	<script src='../entity/controllers/entity.company.latestNews.controller.js'></script>
	<script src='../entity/controllers/entity.company.socialMedia.controller.js'></script>
	<script src='../entity/controllers/entity.company.financialGrowth.controller.js'></script>
	<script src='../entity/controllers/entity.company.compliance.controller.js'></script>
	<script src='../entity/controllers/entity.company.leaderships.contoller.js'></script>
	<script src='../entity/controllers/entity.company.medias.controller.js'></script>
	<script src='../entity/controllers/entity.company.threatsIntelligence.controller.js'></script>
	<script src='../entity/controllers/entity.company.riskAlerts.controller.js'></script>
	<script src='../entity/controllers/entity.company.formAnalysis.controller.js'></script>
	<script src='../entity/controllers/entity.person.new.controller.js'></script>
	<script src='../entity/controllers/entity.company.shareHolderEvidence.controller.js'></script>
	<script src="../entity/modal/controller/entityRIskModal.controller.js"></script>
	<script src='../entity/modal/controller/overview.modal.controller.js'></script>
	<script src='../entity/modal/controller/companySourceDetail.controller.js'></script>
	<script src='../entity/modal/controller/leadership.additional.modal.controller.js'></script>
	<script src='../entity/modal/controller/twitter.followers.following.controller.js'></script>
	<script src='../entity/modal/controller/bad.buyers.modal.controller.js'></script>
	<script src='../entity/modal/controller/bad.buyers.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/comments.modal.controller.js'></script>
	<script src='../entity/modal/controller/crimestoppers.uk.modal.controller.js'></script>
	<script src='../entity/modal/controller/domain.company.modal.controller.js'></script>
	<script src='../entity/modal/controller/employment.history.modal.controller.js'></script>
	<script src='../entity/modal/controller/exclusions.fraud.modal.controller.js'></script>
	<script src='../entity/modal/controller/fbi.list.modal.controller.js'></script>
	<script src='../entity/modal/controller/followers.modal.controller.js'></script>
	<script src='../entity/modal/controller/following.modal.controller.js'></script>
	<script src='../entity/modal/controller/general.info.modal.controller.js'></script>
	<script src='../entity/modal/controller/general.modal.controller.js'></script>
	<script src='../entity/modal/controller/imsasllc.modal.controller.js'></script>
	<script src='../entity/modal/controller/industry.modal.controller.js'></script>
	<script src='../entity/modal/controller/key.relationships.modal.controller.js'></script>
	<script src='../entity/modal/controller/key.staff.schools.modal.controller.js'></script>
	<script src='../entity/modal/controller/latest.release.modal.controller.js'></script>
	<script src='../entity/modal/controller/news.articles.modal.controller.js'></script>
	<script src='../entity/modal/controller/news.list.modal.controller.js'></script>
	<script src='../entity/modal/controller/adverse.news.modal.controller.js'></script>
	<script src='../entity/modal/controller/news.modal.controller.js'></script>
	<script src='../entity/modal/controller/open.payments.modal.controller.js'></script>
	<script src='../entity/modal/controller/organizations.modal.controller.js'></script>
	<script src='../entity/modal/controller/person.education.modal.controller.js'></script>
	<script src='../entity/modal/controller/person.interests.modal.controller.js'></script>
	<script src='../entity/modal/controller/previous.location.modal.controller.js'></script>
	<script src='../entity/modal/controller/products.modal.controller.js'></script>
	<script src='../entity/modal/controller/related.org.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/related.organization.modal.controller.js'></script>
	<script src='../entity/modal/controller/related.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/related.person.org.controller.js'></script>
	<script src='../entity/modal/controller/related.person.people.modal.controller.js'></script>
	<script src='../entity/modal/controller/replies.modal.controller.js'></script>
	<script src='../entity/modal/controller/sanctionssearch.list.modal.controller.js'></script>
	<script src='../entity/modal/controller/scam.report.modal.controller.js'></script>
	<script src='../entity/modal/controller/scam.website.modal.controller.js'></script>
	<script src='../entity/modal/controller/sex.offenders.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/social.media.articles.modal.controller.js'></script>
	<script src='../entity/modal/controller/social.media.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/stocks.modal.controller.js'></script>
	<script src='../entity/modal/controller/supplier.black.list.modal.controller.js'></script>
	<script src='../entity/modal/controller/thumbnails.modal.controller.js'></script>
	<script src='../entity/modal/controller/unlimitedcriminalchecks.modal.controller.js'></script>
	<script src='../entity/modal/controller/reddit.comments.modal.controller.js'></script>

	<script src='../entity/modal/controller/balance.sheet.modal.controller.js'></script>
	<script src='../entity/modal/controller/income.sheet.modal.controller.js'></script>
	<script src='../entity/modal/controller/cashflow.sheet.modal.controller.js'></script>
	<script src='../entity/modal/controller/finance.modal.controller.js'></script>

	<script src='../entity/modal/controller/question.answers.modal.controller.js'></script>
	<script src='../entity/modal/controller/finance.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/overview.person.modal.controller.js'></script>
	<script src='../entity/modal/controller/risk.personoffence.modal.controller.js'></script>
	<script src='../entity/modal/controller/social.follwers.modal.controller.js'></script>
	<script src='../entity/modal/controller/flowMaximizeModalController.js'></script>
	<script src='../entity/modal/controller/ownership.strcture.update.js'></script>
	<script src='../entity/modal/controller/QuestionsPerAnswersStatusController.js'></script>
	<script src='../entity/modal/controller/add.officer.modal.controller.js'></script>
	<script src='../entity/modal/controller/sourceChangeModalController.js'></script>


	<!--------------------------------  Common Scripts Js Ends   ------------------------------->

	<!--  ==============================================================================
**********************  Local Path Ends *********************
==============================================================================*/
-->


	<!--  ==============================================================================
**********************  Gulp Path Starts *********************
==============================================================================*/
-->
	<!--------------------------------  Common Vendor Js Starts  ------------------------------->
	<!-- 		<script src="../vendor/common-vendor.min.js"></script> -->
	<!--------------------------------  Common Vendor Js Ends    ------------------------------->

	<!--------------------------------  Entity Vendor Js Starts  ------------------------------->
	<!-- 		<script src="./assets/minifiedJs/entity-vendor.min.js"></script> -->
	<!--------------------------------  Entity Vendor Js Ends    ------------------------------->

	<!--------------------------------  Entity Vendor Js Starts  ------------------------------->
	<!-- 		<script src="./assets/minifiedJs/entity-charts.min.js"></script> -->
	<!--------------------------------  Entity Vendor Js Ends    ------------------------------->

	<!----------------------------  Initial Configuration JS Starts  ------------------------------->
	<!-- 		<script src="../scripts/app.js"></script> -->
	<!-- 		<script src="./entity.app.js"></script> -->
	<!-- 		<script src="./entity.config.js"></script> -->
	<!-- 		<script src="./entity.run.js"></script> -->
	<!----------------------------  Initial Configuration JS Ends  ------------------------------->


	<!--------------------------------  Common Scripts Js Starts  ------------------------------>
	<!-- 		<script src="../scripts/common-scripts.min.js"></script> -->
	<!--------------------------------  Common Scripts Js Ends   ------------------------------->

	<!--------------------------------  Common Scripts Js Starts  ------------------------------>
	<!-- 		<script src="./assets/minifiedJs/entity-scripts.min.js"></script> -->
	<!--------------------------------  Common Scripts Js Ends   ------------------------------->
	
	<!--new minified Js start -->
	<!-- <script src="../vendor/common.vendor.new.min.js"></script>
	<script src="./assets/minifiedJs/entity.vendor.new.min.js"></script>
	<script src="./assets/minifiedJs/entity.charts.new.min.js"></script>
	<script src="./assets/minifiedJs/entity.intialize.new.min.js"></script>
	<script src="../scripts/common.scripts.new.min.js"></script>
	<script src="./assets/minifiedJs/entity.scripts.new.min.js"></script> -->
	<!--new minified Js end -->

	<script type="text/ng-template" id="overviewModalId.html">
		<div class="modal-header pad-b0">
	<button type="button" class="close"  aria-hidden="true" ng-click="closeModal()"><span>&#x2716;</span></button>
	<h4 class="modal-title roboto-light f-18 lh-1_2 text-unset text-cream">More Information</h4>
</div>
<div class="modal-body">
			<!--  Entity Results Wrapper Starts  -->
			<div class="container-fulid pad-l0 entity-results-wrapper social-media-results-wrapper">
				<div class="row entity-contents-wrapper">
						<div class="col-md-12">
							<div class="entity-result-item">
							<div class="connected_industries-wrapper" ng-show="connected_industries && connected_industries.length>0">
									<h3> Connected Industries: </h3>
											 <span ng-repeat="indus in connected_industries" class=" text-uppercase">{{ indus.entity.name}}</span>
	</div>
								<div class="secialities-wrapper" ng-show="specialitiesList && specialitiesList.length>0">
									<h3> Specialities: </h3>
									<span class=" text-uppercase" style="margin-right:10px;margin-bottom:10px" ng-repeat="name in specialitiesList" class=" text-uppercase">{{ name }}</span>
								</div>
								<div class="secialities-wrapper">
									<h3> Company Profile</h3>
									<div class="row balance-data">
										<div class="col-sm-6" ng-repeat="(k,v) in stockModalData['stock_profile']['company_profile']" ng-if="k!='risk_score'">
											<p class="progress-text"><small class="text-uppercase">{{k | strReplace:'_':' '}} :</small>
												{{v}}
											</p>
										</div>
										<div class="col-sm-6">
											<p class="progress-text"><small>MARKET CAP:</small>
												{{stockModalData['company_profile']['data']['market_cap']}}
											</p>
										</div>
										<div class="col-sm-6">
											<p class="progress-text"><small>INVESTOR RELATIONS TELEPHONE:</small>
												{{stockModalData['company_profile']['data']['investor_relations_tel']}}
											</p>
										</div>
										<div class="col-sm-6" ng-repeat="(k,v) in stockModalData['company_profile']['shareholders']" ng-if="k!='risk_score'">
											<p class="progress-text"><small class="text-uppercase">{{k | strReplace:'_':' '}} :</small>
												{{v}}
											</p>
										</div>
									</div>
								</div>
								<div>
									<h3>Cyber Details</h3>
										<div class="row balance-data">
											<div class="col-sm-6" ng-repeat="(k,v) in stockModalData['stock_profile']['company_profile']" ng-if="k!='risk_score'">
												<p class="progress-text"><small class="text-uppercase">{{k | strReplace:'_':' '}} :</small>
													{{v}}
												</p>
											</div>
										</div>
								</div>
							</div>
					</div>
				</div>
			</div>
			<!--  Entity Results Wrapper Ends  -->


</div>

</script>
	<script type="text/ng-template" id="companySourceDetails.html">
		<div class="modal-header pad-b5">
<button type="button" class="close"  aria-hidden="true" ng-click="closeModal()"><span>&#x2716;</span></button>
<h4 class="modal-title roboto-light">Registered Number Sources<span popover-trigger="'mouseenter'"
popover-placement="top" uib-popover="Manage sources credibility" popover-class="f-12 text-cream roboto-light top-popover-wrapper"
popover-append-to-body="true" class="fa text-dark-blue c-pointer mar-l5 fa-link"  ng-if="ehubObject.adminUser" ng-click= "redirectTosourceManagement()"> </span></h4>
</div>
<div class="modal-body row bottom">
	<div class="bst_custom_accordian">
		<div class="col-xs-12">
	<uib-accordion>
			<div uib-accordion-group class="panel-default" is-open="true">
					<uib-accordion-heading>
					  I can have markup, too! <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
					</uib-accordion-heading>
					<div class="d-grid">
						<p>sdfs</p><span>adsfds</span> <div>asdfds</div> <span><i class="fa fa-external-link"></i></span>
					</div>
				  </div>
				  	<div uib-accordion-group class="panel-default" is-open="status.open">
						<uib-accordion-heading>
						  I can have markup, too! <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
						</uib-accordion-heading>
						<div class="d-grid">
							<p>sdfs</p><span>adsfds</span> <div>asdfds</div> <span><i class="fa fa-external-link"></i></span>
						</div>
					  </div>
			</uib-accordion>
		</div>
	</div>

<div class="col-sm-6 ">
	<div class="top-heading border-thin-dark-cream-b d-flex ai-c">
		<h4>Primary</h4><span class=" text-dark-pastal-green mar-r5 roboto-light"><i class="fa text-dark-pastal-green mar-r5 fa-link"></i> {{complianceSources.primarySource.length || 0}}</span>
	</div>
	<div class="bottom-content-wrapper">
		<ul class="custom-list pad-l0 item-1 mxh-200 list-scroll">
			<li class="c-arrow d-flex ai-c" ng-repeat = "source in complianceSources.primarySource track by $index"><span class="width-100 ws-normal" style="word-break: break-word;white-space:pre-wrap" >{{source | sourceurlbase}} <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11" ng-show ="$index === 0 && source == complianceSources.source"></i></span> 
				<a href= "{{source |sourceurlbase}}" target="_blank" ng-hide="(source === 'BST' )|| (source ===  'bst')"  popover-trigger="'mouseenter'"
				popover-placement="top" uib-popover="{{source}}" popover-class="f-12 text-cream roboto-light top-popover-wrapper word-ba"
				popover-append-to-body='true' ><span class="fa fa-external-link mar-autor f-16 c-pointer"></span></a></li>
			<li class="c-arrow" ng-show = "complianceSources.primarySource.length === 0 || !complianceSources.primarySource" > No Sources Available</li>
		</ul>
	</div>
</div>

<div class="col-sm-6 ">
	<div class="top-heading border-thin-dark-cream-b d-flex ai-c">
		<h4>Secondary</h4><span class="text-dark-cream mar-r5 roboto-light"><i class="fa text-dark-pastal-green mar-r5 fa-link"></i>  {{complianceSources.secondarySource.length || 0}}</span>
	</div>
	<div class="bottom-content-wrapper">
		<ul class="custom-list pad-l0 item-1 mxh-200 list-scroll">
			<li class=" c-arrow d-flex ai-c" ng-repeat = "source in complianceSources.secondarySource track by $index"><span class="width-100 ws-normal" style="word-break: break-word;white-space:pre-wrap" >{{source | sourceurlbase}} <i class="fa fa-check-circle-o mar-x5 text-dark-blue f-11" ng-show ="(source == complianceSources.source)"></i></span> 
				<a href= "{{source |sourceurlbase}}" target="_blank" ng-hide="(source === 'BST' )|| (source ===  'bst')"  popover-trigger="'mouseenter'"
				popover-placement="top-right" uib-popover="{{source}}" popover-class="f-12 text-cream roboto-light word-ba top-popover-wrapper"
				popover-append-to-body='true' ><span class="fa fa-external-link mar-autor f-16 c-pointer" ></span></a></li>
				<li class="c-arrow" ng-show = "complianceSources.secondarySource.length === 0 || !complianceSources.secondarySource" > No Sources Available</li>
			</ul>
	</div>
</div>
</div>

</script>
	<script type="text/ng-template" id="overview-person-modalId.html">
		<div class="modal-header ">
		<button type="button" class="close" aria-hidden="true" ng-click="closeModal()">
			<span>&#x2716;</span>
		</button>
		<h4 class="modal-title ">Additional Details</h4>
	</div>
	<div class="modal-body">

		<!--  Entity Company Starts  -->
		<div class="entity-company-wrapper">

			<!--  Entity Results Wrapper Starts  -->
			<div class="container-fulid entity-results-wrapper social-media-results-wrapper">
				<div class="row entity-contents-wrapper">
					<div class="entity-contents-inner-wrapper">
						<div class="col-md-12 entity-item">
							<div class="entity-result-item">
								<div class="secialities-wrapper">
									<h3 ng-if="entitySearchResult.person_more_info.early_life || entitySearchResult.person_more_info.early_life_and_education">Early life: </h3>
									<div class="row balance-data">
										<div class="col-sm-12">
											<p class="progress-text">
												{{entitySearchResult.person_more_info.early_life || entitySearchResult.person_more_info.early_life_and_education}}
											</p>
										</div>
									</div>
								</div>
								<div class="secialities-wrapper">
									<h3 ng-if="entitySearchResult.person_more_info.personal_life">Personal Life: </h3>
									<div class="row balance-data">
										<div class="col-sm-12">
											<p class="progress-text">
												{{entitySearchResult.person_more_info.personal_life}}
											</p>
										</div>
									</div>
								</div>
								<div class="secialities-wrapper">
									<h3 ng-if="entitySearchResult.person_more_info.political_views">Political Views: </h3>
									<div class="row balance-data">
										<div class="col-sm-12">
											<p class="progress-text">
												{{entitySearchResult.person_more_info.political_views}}
											</p>
										</div>
									</div>
								</div>
								<div class="secialities-wrapper">
									<h3 ng-if="entitySearchResult.person_more_info.opinions">Opinions: </h3>
									<div class="row balance-data">
										<div class="col-sm-12">
											<p class="progress-text">
												{{entitySearchResult.person_more_info.opinions}}
											</p>
										</div>
									</div>
								</div>
								<div class="secialities-wrapper">
									<h3 ng-if="entitySearchResult.person_more_info.subsidies">Subsidies: </h3>
									<div class="row balance-data">
										<div class="col-sm-12">
											<p class="progress-text">
												{{entitySearchResult.person_more_info.subsidies}}
											</p>
										</div>
									</div>
								</div>
								<div>
									<h3 ng-if="entitySearchResult.list['citizenship'].length > 0">Citizenship: </h3>
									<div class="row balance-data">
										<div class="col-sm-4" ng-repeat="citizenship in entitySearchResult.list['citizenship']">
											<p class="progress-text">
												{{citizenship}}
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  Entity Results Wrapper Ends  -->

		</div>
		<!--  Entity Company Ends  -->
	</div>
	<script>
		$(document).ready(
					function () {
						/*Custom Scroll Bar*/
						$('.custom-modal.related-person-modal .modal-body .entity-result-item').mCustomScrollbar({
							axis: "y"
						});
					}
				);
	</script>

	</script>
	<script type="text/ng-template" id="genralinfoId.html">
		<!--  Spinner Starts  -->
	<div class="custom-spinner" ng-if="disableSearchButton">
		<i class="fa fa-spinner fa-spin fa-3x"></i>
	</div>
	<!--  Spinner Ends  -->

	<div class="modal-header pad-b0">
		<button type="button" class="close" aria-hidden="true" ng-click="closeModal()">
			<span>&#x2716;</span>
		</button>
		<h4 class="modal-title roboto-light f-18 lh-1_2 text-unset text-cream">More information</h4>
	</div>
	<div class="modal-body">

	

			<!--  Entity Results Wrapper Starts  -->
			<div class="pad-l0 container-fulid entity-results-wrapper">
				<div class="entity-contents-wrapper">
					<div class="entity-result-item no-padding custom-mscroll-margin">
						<div class="main-content-description" ng-repeat="result in generalDetailList">
							<p>{{result.text}}</p>
							<p class="source-text">Source:
								<a href="{{result.source}}" target="_blank">{{result.source}}</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!--  Entity Results Wrapper Ends  -->

	</div>

	<script>
		/*--  Custom Scroll Bar  --*/
		setTimeout(function () {
			$('.entity-result-item').mCustomScrollbar({
				axis: "y"
			});
		}, 10);
	</script>

	</script>

	<script type="text/ng-template" id="ownershipModal.html">
		<style>
		.modal-body #flowChartViewDiv canvas {
			left: 0;
			height: 100%!important;
		}
		.modal-body.screencapture-modal-body{
			min-height: 0!important;			
			height: calc(100% - 43px)!important;
		}
		</style>
		<!-- Widget Capture Modal Starts  -->
<div class="capture-modal-wrapper width-100  ceri-wrapper" id="maximizeAllchart">
	<span ng-show="widgetCapturePreloader" class="custom-spinner case-dairy-spinner">
		<i class="fa fa-spinner fa-spin fa-2x"></i>
	</span>
	<div class="modal-header border-0">
		<button type="button" class="close text-dark-cream" ng-click="closeWidgetCaptureModal()"><span>&#x2716;</span></button>
		<p class="roboto-light f-18 text-cream border-cream-thin-b pad-b15 mar-b0">Ownership Structure</p>
	</div>
	<div class="screencapture-modal-body modal-body custom-scroll-wrapper overflow-a p-rel" id="scroll-to-centre">

		<div id="flowChartViewDiv" class="text-center border-thin-dark-cream-b  height-75 pad-b35 p-rel top-chart-content ownership-container orgchart-wrapper dashboard-wrapper"
		 style="width: 100%; filter: blur(0);">
			<ul class="list-inline pull-left width-95 custom-list p-abs l-0  circle-list" style=" z-index: 999;bottom:0;background: transparent;">
					<li>
							<a>
									<i class="fa fa-circle mar-5 text-yellow-dot"></i>
									<span class="text-dark-cream text-uppercase">Company<span> </span> </span>
							</a>
					</li>
					<li>
							<a>
									<i class="fa fa-circle mar-5 text-green-dot"></i>
									<span class="text-dark-cream text-uppercase">UBO<span> </span> </span>
							</a>
					</li>
					<li>
									<a>
											<i class="fa fa-circle mar-5 text-purple-dot"></i>
											<span class="text-dark-cream text-uppercase">Subsidary</span>
									</a>
							</li>  <li>
											<a>
													<i class="fa fa-circle mar-5 text-blue-dot"></i>
													<span class="text-dark-cream text-uppercase">Holding/Parent</span>
											</a>
									</li>
			</ul>
		</div>


		<div class="col-sm-12 bottom-chart-content height-25">
			<div class="row mar-x0 div-210">
				<div class="side-remain width-100">
					<p class="top-heading text-cream text-capitalize roboto-bold  f-18 mar-t10 mar-b15 "> Filters:</p>
					<div class="d-flex jc-sb">
						<div class=" text-cream roboto-light text-capitalize d-ib">
							<span class="lh-22 va-m">Max number of levels (depth):</span>
							<div class="w-55 input-group-mat border-r3 h-2_4 mar-l5 d-ib">
								<!-- <select class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12">
							 
									<option class="text-capitalize " value="open" selected="selected">5</option> 
									<option class="text-capitalize ng-binding ng-scope" value="close">10</option> 
									<option class="text-capitalize ng-binding ng-scope" value="close">15</option>
									<option class="text-capitalize ng-binding ng-scope" value="close">20</option>
									<option class="text-capitalize ng-binding ng-scope" value="close">30</option>
								</select> -->
								<select name="maxnumberlevel"  class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12" ng-model="filteredChart.selectedNumberofLevel" ng-options="chartLevel for chartLevel in filteredChart.numberOfLevel" >
											 
									</select>
							</div>
						</div>
						<div class=" text-cream roboto-light text-capitalize d-ib">
							<span class="lh-22 va-m">Risk Assesment:</span>
							<div class="w-55 input-group-mat border-r3 h-2_4 mar-l5 d-ib">
								<!-- <select class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12" >
									<option class="text-capitalize " value="close">high</option> 
									<option class="text-capitalize ng-binding ng-scope" value="open" selected="selected">low</option>
								</select> -->
								<select name="riskratio"  class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12" ng-model= "filteredChart.riskRatio" ng-options="risklevel for risklevel in filteredChart.risklevel" ng-change="riskChange()">
								</select>
								</div>
						</div>
						<div class="">
							<div class=" text-cream text-capitalize roboto-light mar mar-l15 mar-r5 d-ib h-2_4 ">
								<div class="custom-checkbox width-a">
									<label class="text-cream checkbox-inline">
										<input type="checkbox" class="chs evalInputForBanSymbol">
										<span>
											<i class="fa fa-check"></i>
										</span>
									</label>
									<span class="text-cream lh-22 va-m roboto-light ">Show only top</span>
								</div>
							</div>
							<div class=" text-cream roboto-light text-capitalize d-ib pad-r15">
								<div class="w-55 input-group-mat mar-r10 border-r3 h-2_4 mar-l5 d-ib">
										<select name="degreeinagrray"  class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12" ng-model="filteredChart.numberofcomapnies" ng-options="companyinlevel for companyinlevel in filteredChart.company_eachlevel" >
											 
											   </select>
									<!-- <select class="text-capitalize mar-0 width-100  border-0 h-2_4 custom-select-box f-12">
									 
										<option class="text-capitalize " value="close">10</option> 
										<option class="text-capitalize ng-binding ng-scope" value="open" selected="selected">20</option>
										<option class="text-capitalize ng-binding ng-scope" value="close">30</option>
										<option class="text-capitalize ng-binding ng-scope" value="close">40</option> 
									</select> -->
								</div>

								<span class="lh-22 va-m ">Subsidaries  from each level</span>
							</div>
						</div>
						<div class="side-details-wrapper">
							<div class="top-range-wrapper ">
								<p class="roboto-light f-12 lh-14 mar-b0 text-cream d-ib">Ownership control percentage</p>
								<div class="w-30 h-2_4 d-ib left mar-b15 input-group-mat border-r3">
									<input type="text" autocomplete="off" value="{{sliderMinValue}}" class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px"
									 id="">
								</div>
								<div class="w-30 h-2_4 d-ib input-group-mat border-r3">
									<input type="text" autocomplete="off" value="{{sliderMaxValue}}" class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px"
									 id="">
								</div>
							</div>
						<!-- 	<div class="color-input-wrapper" id="avg_expenditure">
								<div class="row">
									<div class="col-sm-12">
										<div id="slider" class="custom-range-slider slider-ownership-wrapper blue"></div>
									</div>
									<div class="col-sm-3 text-cream pull-left" style="width:10">10%</div>
								 
									<div class="col-sm-3 text-cream pull-right" style="width:100">100% </div>
								</div>
							</div> -->
							<div class="color-input-wrapper" id="avg_expenditure">
								<div class="row">
									<div class="col-sm-12">
										<div id="sliderAvgExpen" class="custom-range-slider blue round-thumb-slider customer-slider"  ng-model="chartLevel"></div>
									</div>
									<div class="col-sm-3 text-cream width-a mar-t10  pull-left" style="width:{{sliderMinValue}}">MIN 10
									</div>
									<div class="col-sm-3 text-cream width-a mar-t10  pull-right" style="width:{{sliderMaxValue}}">MAX 100
									</div>
									<button class="grad-button" ng-click="changeChartLevels()">apply</button></div>
								</div>
							</div>
						</div>
					
				</div>

			</div>

		</div>
	</div>
</div>
</div>


</script>
	<script type="text/ng-template" id="dataPopUp.modal.html">
		<div class="modal-header">
   <button type="button" class="close" ng-click="closeDataPopUp()" data-dismiss="modal" aria-hidden="true"><span>&#x2716;</span></button>
   <h4 class="modal-title">{{header}}</h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-sm-12 scrollText-wrapper mxh-450">
			<div id="mainData" class="text-left">
	
			</div>
		</div>
	</div>
</div>
<script>
		setTimeout(function(){
			$('.scrollText-wrapper').mCustomScrollbar({
				axis : "y",
				theme : "minimal-dark"
			});
		},10);
	</script>

	<script type="text/ng-template" id="direct-adverse-news-modal.html">
		<!--  Risk Overview Modal Wrapper Starts  -->
<div class="height-100  risk-overview-modal-wrapper" id="adversenewsModal">
		<div class="custom-spinner case-dairy-spinner" ng-show="riskOverviewLoader">
			<i class="fa fa-spinner fa-spin fa-3x"></i>
		</div>
		<div class="modal-header pad-b10 d-flex ai-c">
				<h4 class="modal-title mar-autor  text-overflow">
				 
						<span id="articleHeader">Screening <span></span></span> 
					   
				   </h4>
			<button type="button" class="f-24 bg-transparent border-0 text-cream" ng-click="closeShareHolderModal()" >&times;</button>
			
			
		</div>
		<div class="modal-body height-100 pad-t15">
			<div class="clearfix height-100">
				<div class="col-sm-3 height-95 pad-l0">
					<div class="d-flex ai-c h-35">
							<div class="bst_input_group p-rel height-100">
								<span class="input-group-prepend f-14 text-dark-cream t-14 fa fa-search">

								</span>
								<input type="text" autocomplete="off" placeholder="Search" class="custom-input border-0 height-100 pad-t10 pad-b0 f-12 pad-r5" ng-model= "firstOpenedArticle.filterEntites" ng-keyup="filterSearchInputOnChange(firstOpenedArticle.filterEntites)">
								<span class="label">Search</span>

								</div>
							<div class="bst_input_group mar-l10 p-rel height-100">
									<span class="input-group-append  f-14 text-dark-cream t-14 fa fa-caret-down">

										</span>
									<select class="custom-input border-0 height-100 pad-t10 pad-b0 pad-l5" ng-model="firstOpenedArticle.entitySelected" ng-options="color.name for color in firstOpenedArticle.entityDropdown">	<option value="">ALL</option></select>
								</div>
					</div>
						<div class="risk-tags-wrapper pad-y5 d-flex ai-c">
								<div class="risk-tag c-pointer d-if pad-x5 ai-c jc-c " ng-click="filterSummaryscreeningBadges('pep','','','fromFilterSummary')" popover-placement="top-left" popover-trigger="'mouseenter'" uib-popover="{{summaryFilterBadges.pep === 'pep'  ? 'Hide Peps' : 'Show Peps'}}" popover-class="top-popover-wrapper risk-popover-wrapper" ng-class ="summaryFilterBadges.pep === 'pep' ? 'coral-red-tag' :  'bordered-tag'">
													<i class="fa fa-street-view" ></i><span class="mar-l5">{{totalScreeingresults.pepsCount}}</span>
								</div>
								<div class="risk-tag c-pointer  d-if pad-x5 ai-c jc-c" ng-click="filterSummaryscreeningBadges('sanction','','','fromFilterSummary')" popover-placement="top-left" popover-trigger="'mouseenter'" uib-popover="{{summaryFilterBadges.sanction === 'sanction'  ? 'Hide Sanction' : 'Show Sanctions'}}" popover-class="top-popover-wrapper risk-popover-wrapper"  ng-class ="summaryFilterBadges.sanction === 'sanction' ? 'coral-red-tag' :  'bordered-tag'">
																<i class="fa fa-ban " ></i><span class="mar-l5">{{totalScreeingresults.sanctionsCount}}</span>
								</div>	
								<div class="risk-tag  c-pointer d-if pad-x5 ai-c jc-c "  ng-click="filterSummaryscreeningBadges('highRisk','','','fromFilterSummary')"  popover-placement="top-left" popover-trigger="'mouseenter'" uib-popover=" {{summaryFilterBadges.highRisk === 'highRisk'  ? 'Hide Jurisdictions' : 'Show Jurisdictions'}}" popover-class="top-popover-wrapper risk-popover-wrapper" ng-class ="summaryFilterBadges.highRisk === 'highRisk' ? 'coral-red-tag' :  'bordered-tag'">
																						<i class="fa fa-globe"  ></i><span class="mar-l5">{{totalScreeingresults.jurisdictionCounts}}</span>
								</div>
								<div class="risk-tag c-pointer  d-if pad-x5 ai-c jc-c"  ng-click="filterSummaryscreeningBadges('finance','','','fromFilterSummary')" popover-placement="top-left" popover-trigger="'mouseenter'" uib-popover="{{summaryFilterBadges.finance === 'finance'  ? 'Hide Finance News' : 'Show Finance News'}}" popover-class="top-popover-wrapper risk-popover-wrapper" ng-class ="summaryFilterBadges.finance === 'finance' ? 'coral-red-tag' :  'bordered-tag'">
										<i class="fa fa-gavel "></i><span class="mar-l5">{{totalScreeingresults.financeCount}}</span>
								</div>	
								<div class="risk-tag  c-pointer d-if pad-x5 ai-c jc-c"  popover-placement="top-left" popover-trigger="'mouseenter'" uib-popover="{{summaryFilterBadges.adverse === 'adverse'  ? 'Hide News' : 'Show News'}}"ng-click="filterSummaryscreeningBadges('adverse','','','fromFilterSummary')" popover-class="top-popover-wrapper risk-popover-wrapper" ng-class ="summaryFilterBadges.adverse === 'adverse' ? 'coral-red-tag' :  'bordered-tag'">
																			<i class="fa fa-newspaper-o"></i><span class="mar-l5">{{totalScreeingresults.adverseCounts}}</span>
								</div>
						</div>
					<div class="adverse-slider mar-y10 custom-range-wrapper width-100 entity-range-bar" ng-if="showSlider.pepSlider">
							<p class="mar-b0 mar-t5 roboto-light text-cream d-ib  width-70" >Pep Alerts Confidence level</p>
							<div class="bst_input_group pull-right h-30 width-15 ">	<input type="text" autocomplete="off" class="custom-input h-30 pad-5 pe-none" ng-value="filtered.pep + '%'" ></div>
							<input type="range" min="0" max="100" step="1" class="bg-transparent pad-y5 " ng-change="pepSanctionSliderChange('pep','slider')" ng-model="filtered.pep">
					</div>
					<div class="adverse-slider mar-y10 custom-range-wrapper width-100 entity-range-bar" ng-if="showSlider.sactionSlider">
							<p class="mar-b0 mar-t5 roboto-light text-cream d-ib width-70">Sanctions Confidence level</p>
							<div class="bst_input_group pull-right h-30 width-15 ">		<input type="text" autocomplete="off" class="custom-input h-30 pad-5 pe-none" ng-value="filtered.sanction + '%'" ></div>
							<input type="range" min="0" max="100" step="1" class="bg-transparent radius-5 pad-y5 " ng-change="pepSanctionSliderChange('sanction','slider')" ng-model="filtered.sanction">
					</div>

						<div style="min-height: 300px;max-height: calc(100% - 15.5rem);" class="panel-group overflow-h pad-t5 question-box-wrapper custom-scroll-wrapper custom-accordion left-accordian-wrapper center-row1" id="accordion" role="tablist" aria-multiselectable="true">
         						
         			<uib-accordion  close-others="true">							
         								 
         								<div uib-accordion-group class="right-panel panel-default"  ng-repeat="(key, article) in leftsideTilelist  | filter:{key : firstOpenedArticle.filterEntites,Ubo_ibo:firstOpenedArticle.entitySelected.name,adverseNews:summaryFilterBadges.adverse,financeCrime:summaryFilterBadges.finance,jurisdiction:summaryFilterBadges.highRisk,pep_url:summaryFilterBadges.pep,sanction:summaryFilterBadges.sanction}" ng-init="status.open = ($index === 0?true:false)" is-open="status.open" ng-click ="changeArrrowDirection( $index, article )" ><!-- ng-show ="article.showlist" -->
         										<uib-accordion-heading >
														 <div class="uib-header-wrapper d-flex">
         											
         											<div class="icon-wrapper" ng-class="((article.Ubo_ibo.toLowerCase() === 'ibo') ? ('child-container') : ((article.Ubo_ibo.toLowerCase() === 'subsidaries') ? 'child-container additional-container' : ''))">
         
																<i class="fa entity-icon p-rel" ng-class="((article.Ubo_ibo.toLowerCase() === 'ubo' || article.Ubo_ibo.toLowerCase() === 'officer') ? ('fa-user-o') : ('fa-building'))"></i>
																<span class="description" ng-class="((article.Ubo_ibo.toLowerCase() === 'ibo') ? ('child-container') : ((article.Ubo_ibo.toLowerCase() === 'subsidaries') ? 'child-container additional-container' : ''))">{{(article.pep_url_values.length || 0)+(article.sanction_url_values.length || 0)+((article.self.high_risk_jurisdiction === 'HIGH' ? 1 : 0) ||(article.self.high_risk_jurisdiction === 'MEDIUM' ? 1 : 0) || 0)+(article.self.finance_Crime_url.length > 0 ? article.self.finance_Crime_url[0].count : 0)+ (article.self.adverseNews_url.length > 0 ? article.self.adverseNews_url[0].count : 0)}}</span>
															</div>
         											<div class="heading-list-wrapper ">														
         												<span class="ws-normal">	{{article.key}}</span>
         												<ul class="list-inline custom-list item-5 text-center">
         													<li class=""><i class="fa fa-street-view"  ></i><span>{{article.pep_url_values.length || 0}}</span></li>
         													<li class=""><i class="fa fa-ban"></i><span>{{article.sanction_url_values.length}}</span></li>
         													<li class=""><i class="fa fa-globe"></i><span>{{((article.self.high_risk_jurisdiction === 'HIGH' ? 1 : 0) ||(article.self.high_risk_jurisdiction === 'MEDIUM' ? 1 : 0) || 0) }}</span></li>
         													<li class=""><i class="fa fa-gavel"></i><span>{{ article.self.finance_Crime_url.length > 0 ? article.self.finance_Crime_url[0].count : 0}}</span></li>
         													<li class=""><i class="fa fa-newspaper-o"></i><span>{{ article.self.adverseNews_url.length > 0 ? article.self.adverseNews_url[0].count : 0}}</span></li>
         												</ul>
         							
         											</div>
         											<%-- {{key+" "+status.open}} --%>
         												<i class="mar-autol fa f-18" ng-class="{'fa-caret-down': status.open, 'fa-caret-left': !status.open}"></i>
         											</div>
         										</uib-accordion-heading>
         									<ul class="list-group custom-list-group mxh-none">
         										<li class="list-group-item" ng-class="{'active': list === hightLightrow}" ng-repeat="list in  article.values"
         										ng-click="getTheArticleDetails(list,$event,$index,'',false)" ng-if ="(summaryFilterBadges.adverse)">
         										<p  class="mar-b0 roboto-light d-flex ai-t"><i class ="fa  mar-r10 f-16 text-cream" ng-class ="list.type === 'financeCrime' ? 'fa-gavel' : 'fa-newspaper-o'"></i>{{list.name || list.search_query}}<i  ng-show="list.isSignificant"class ="fa fa-star mar-autol f-16 text-dark-blue"></i></p>
         											<div class="d-flex roboto-light ai-c">{{list.title || list.article_title}}
         												<span class="pull-right" ng-show="list.isSignificant"><i class="fa fa-star text-dark-blue" aria-hidden="true"></i></span>
         												<!-- <span  class="pull-right pad-r3" ng-show="list.queue ==='Low'"><i class="fa fa-arrow-circle-o-down text-dark-green"></i></span>	 -->													
         											</div>
         										</li>
         										<li class="list-group-item" ng-class="{'active': list === hightLightrow}" ng-repeat="list in  article.financeValues"
         										ng-click="getTheArticleDetails(list,$event,$index,'',false)" ng-if =" (summaryFilterBadges.finance)">
         										<p  class="mar-b0 roboto-light d-flex ai-t"><i class ="fa  mar-r10 f-16 text-cream" ng-class ="list.type === 'financeCrime' ? 'fa-gavel' : 'fa-newspaper-o'"></i>{{list.name || list.search_query}}<i  ng-show="list.isSignificant"class ="fa fa-star mar-autol f-16 text-dark-blue"></i></p>
         											<div class="d-flex roboto-light ai-c">{{list.title || list.article_title}}
         												<span class="pull-right d-flex ai-c f-16" ng-show="list.cluster.length > 0"><i class="fa fa-link f-12 mar-r5 text-cream" aria-hidden="true"></i>{{list.cluster.length}}</span>
         												<span class="pull-right" ng-show="list.isSignificant"><i class="fa fa-star text-dark-blue" aria-hidden="true"></i></span>
         												<!-- <span  class="pull-right pad-r3" ng-show="list.queue ==='Low'"><i class="fa fa-arrow-circle-o-down text-dark-green"></i></span>	 -->													
         											</div>
         										</li>
         										<li class="list-group-item pad-y5" ng-class="{'active': list === hightLightrow}" ng-repeat="list in  article.pep_url_values | orderBy:'-confidence'"
         										ng-click="screeenArticle(list,$index,article)" ng-if =" (summaryFilterBadges.pep)">
         										<p  class="mar-b0 roboto-light d-flex ai-t">
         											
         												<p class="roboto-light mar-b0">{{list.value}}</p>
         												<p class="roboto-light mar-b0"> Confidence Level : {{list.confidence*100 | number : 2}}<span ng-if="list.confidence*100">%</span>
         												<span class="pull-right" ng-show="list.pep"><i class="fa fa-star text-dark-blue" aria-hidden="true"></i></span>
         												<!-- <span  class="pull-right pad-r3" ng-show="list.queue ==='Low'"><i class="fa fa-arrow-circle-o-down text-dark-green"></i></span>	 -->													
         											
         										</li>
         										<li class="list-group-item" ng-class="{'active': list === hightLightrow}" ng-repeat="list in  article.sanction_url_values | orderBy:'-confidence'"
         										ng-click="screeenArticle(list,$index,article)" ng-if ="summaryFilterBadges.sanction ">
         										<p  class="mar-b0 roboto-light d-flex ai-t">
         												<p class="roboto-light mar-b0">{{list.value}}</p>
         												<p class="roboto-light mar-b0"> Confidence Level : {{list.confidence*100 | number : 2}}<span ng-if="list.confidence*100">%</span>
         												<span class="pull-right" ng-show="list.sanction"><i class="fa fa-star text-dark-blue" aria-hidden="true"></i></span>
         												<!-- <span  class="pull-right pad-r3" ng-show="list.queue ==='Low'"><i class="fa fa-arrow-circle-o-down text-dark-green"></i></span>	 -->													
         										</li>
         										<li class="list-group-item" ng-class="{'active': list === hightLightrow}" ng-click="jurisdictionDetails(selectedEntityjurisdiction.self)" ng-if ="(summaryFilterBadges.highRisk)">
         										<p  class="mar-b0 roboto-light d-flex ai-t"><i class ="fa fa-globe mar-r10 f-16 text-cream" ></i> {{selectedEntityjurisdiction.self.country }} {{selectedEntityjurisdiction.self.high_risk_jurisdiction}} </p>
         											<div class="d-flex roboto-light ai-c">
         											</div>
         										</li>
         									</ul>
         								</div>										
         										
         			</uib-accordion>
         		</div>
					
				</div>
				<div class="height-95 radius-10 left-side-panel " ng-class = "(currentArticleExists && (summaryFilterBadges.adverse  || summaryFilterBadges.finance)) ? 'col-sm-6' : 'col-sm-9' ">
					<div ng-show="currentArticleExists && (summaryFilterBadges.adverse  || summaryFilterBadges.finance)" id="pulseVisualizerTextWrapper"
					class="pulseVisualizerText-wrapper height-100 pad-l15 welcome-page decision-score-pop bottom-pop">
						<h4 class="modal-title d-flex pad-r10 ai-c mar-r5text-overflow">
								<span class="pull-right mar-r5 d-flex ai-c f-16 mar-r5" ng-show="firstOpenedArticle.cluster.length > 1"><i class="fa fa-link f-12  text-cream" aria-hidden="true"></i>{{firstOpenedArticle.cluster.length}}</span>
							<span class="text-cream text-overflow">{{mainArticelHeader }}</span>
							<div class="header_icon_wrapper d-flex mar-autol jc-sb ai-c">
								<a href="javascript:void(0);" class="mar-l10" uib-popover="{{ firstOpenedArticle.significantTitle }}"
								popover-trigger="'mouseenter'" popover-placement="bottom" popover-class="bottom-pop-wrapper" popover-append-to-body='true'
								ng-class=" firstOpenedArticle.isSignificant ?  'text-dark-blue' : 'text-cream'  " ng-model="mySignificant" ng-click="makeSignificant(firstOpenedArticle.isSignificant,typeIs)">
									<i class="  fa fa-star f-18" ng-class=" firstOpenedArticle.isSignificant ?  'text-dark-blue' : 'text-cream'  "
									aria-hidden="true"></i>
								</a>
                                <span popover-trigger="outsideClick"  class="mar-l10 c-pointer" ng-if="commentedList.length > 0" uib-popover-template="'selectCount.html'" popover-placement="bottom-right" popover-class="bottom-pop-wrapper count-select-popover" popover-append-to-body='true' ng-click="scroll()">{{commentedListLength}}</span>
								<!-- <input type="text" autocomplete="off" ng-model="newOfficer.country" uib-typeahead="country as country.COUNTRY +'(<span class='+'flag-icon -gr'>' +country.ISO2_CODE + '</span>)'  for country in countryNames | filter:$viewValue | limitTo:8"
																						class="form-control"><span class="flag-icon"    ng-class="country.ISO2_CODE ? 'flag-icon-'+country.ISO2_COE : '' ">
																							{{'a'+country}}</span>
																					-->

								<button popover-trigger="'outsideClick'" popover-is-open="sentimentpopover" popover-placement="bottom"
								uib-popover-template="'selectSentiment.html'" popover-class="bottom-pop-wrapper sentiment-select-popover"
								popover-append-to-body='true' class="btn btn-primary d-flex jc-sb pad-y0 custom-input mar-l5 pad-x0 border-0 height-100 ai-c width-100 bg-transparent dropdown-toggle"
								id="menu1" type="button" data-toggle="dropdown">
									<a href="javascript:void(0);" class="mar-r0" uib-popover="Sentiment is {{toolTipSentiment +','}} click to change it"
									popover-trigger="'mouseenter'" popover-placement="top" popover-class="top-popover-wrapper" popover-append-to-body='true'>
										<i class="fa f-18" ng-class="firstOpenedArticle.sentimentEmotion ? firstOpenedArticle.sentimentEmotion  : '' "
										aria-hidden="true"></i>
									</a> </button>
							</div>
							<!-- <span ng-if="currentArticle.queue !='Low'"class="pull-right" style="font-size: 8px;color:#fff;background: #1E9CAC;font-size: 12px;text-transform: capitalize;border-radius: 7px;padding: 1px 3px;opacity: 1;z-index: 99;line-height: normal !important;"><i class="fa fa-check-circle-o "></i> Primary Source</span>
																	<span ng-iass="{'question-mark-for-adding' : showHideArticleQuestions == true}" class="fa fa-question-circle c-pointer" ng-click="addQuestionAndAnswers()"></i></span></div>
						<div ng-if="sourceUrl" class="lf="currentArticle.queue ==='Low'"class="pull-right" style="font-size: 8px;color:#fff;background: #1E9CAC;font-size: 12px;text-transform: capitalize;border-radius: 7px;padding: 1px 3px;opacity: 1;z-index: 99;line-height: normal !important;"><i class="fa fa-check-circle-o "></i> Secondary Source</span> -->
						</h4>
						<!-- <div ng-hide="firstOpenedArticle.cluster && firstOpenedArticle.cluster.length > 0">
							<div ng-if="publishedDate"><span class="text-cream">Date:</span><span class="text-dark-cream mar-l5">
								{{publishedDate}}</span></div>
						<div ng-if="classification" class="lh-22"><span class="text-cream">Classification:</span><span class="text-dark-cream mar-l5">
								{{classification}} <i ng-clh-22 pad-b15"><span class="text-cream mar-r5">Source:</span> <a href="{{routeUrl}}"
							target="_blank">{{sourceUrl}}</a></div>
							<div class="adverse-news-scroll pad-r15 panel-group overflow-h pad-t5 pad-r10 question-box-wrapper custom-scroll-wrapper custom-accordion center-row1" style="min-height: 380px;max-height: calc(100% - 20rem)">


								<p class="text-captalise " id="marketPulseText"></p>
							</div>
						</div> -->
							<div  class="adverse-news-scroll pad-r15 panel-group overflow-h pad-t5 pad-r10 question-box-wrapper custom-scroll-wrapper custom-accordion center-row1" style="min-height: 380px;max-height: calc(100% - 10rem)">
							<uib-accordion close-others="true" >
								<div uib-accordion-group class="right-panel panel-default" ng-repeat="clusterednews in firstOpenedArticle.cluster"
									ng-init="status.open = ($index === 0?true:false)" is-open="status.open"
									>
									<!-- ng-show ="article.showlist" -->
									<uib-accordion-heading >
										<div class="uib-header-wrapper d-flex" ng-click="getTheArticleDetails(clusterednews,$event,$index,'#newcluster'+$index,true)">
											<div class="heading-list-wrapper mar-l0 width-90">
											 	<div ng-if="sourceUrl" class="lh-22 lh-22 d-flex ai-c "><span class="text-cream mar-r5">Title:</span> <a href="{{routeUrl}}"
													target="_blank">{{clusterednews.article_title || clusterednews.title}}</a><span ng-if="routeUrl" ng-click ="routeSourceUrl(routeUrl)"> <a href="{{routeUrl}}" target="_blank" class="fa fa-external-link" ng-if="routeUrl && adverseNewsmodalObject.clusteredIndex === $index"></a></span> </div>
											</div>
											<i class="mar-autol fa f-18" ng-class="{'fa-caret-down': status.open, 'fa-caret-left': !status.open}"></i>
										</div>
									</uib-accordion-heading>
									<div ng-if="publishedDate" class="mar-b10"><span class="text-cream f-12">Date:</span><span class="text-dark-cream f-12 mar-l5">
										{{publishedDate}}</span>
									</div>
								<div ng-if="classification" class="lh-22 mar-b10 f-12"><span class="text-cream">Classification:</span><span class="text-dark-cream f-12 mar-l5">
										{{classification}} <i ng-if="toHideShowQuestionIcon" ng-class="{'question-mark-for-adding' : showHideArticleQuestions == true}" class="fa fa-question-circle c-pointer" ng-click="addQuestionAndAnswers()"></i></span></div>
									<h4 class="modal-title d-flex pad-r10  ai-c jc-sb text-overflow">
										<span class="text-cream roboto-bold lh-1_2 f-24  text-overflow">{{ articelHeader }}</span>
							
										<!-- <span ng-if="currentArticle.queue !='Low'"class="pull-right" style="font-size: 8px;color:#fff;background: #1E9CAC;font-size: 12px;text-transform: capitalize;border-radius: 7px;padding: 1px 3px;opacity: 1;z-index: 99;line-height: normal !important;"><i class="fa fa-check-circle-o "></i> Primary Source</span>
																					<span ng-if="currentArticle.queue ==='Low'"class="pull-right" style="font-size: 8px;color:#fff;background: #1E9CAC;font-size: 12px;text-transform: capitalize;border-radius: 7px;padding: 1px 3px;opacity: 1;z-index: 99;line-height: normal !important;"><i class="fa fa-check-circle-o "></i> Secondary Source</span> -->
									</h4>
									<p class="text-captalise " id="newcluster{{$index}}"></p>
							</uib-accordion>
						</div>
					</div></div>
<!-- makeSignificant(firstOpenedArticle.isSignificant,$index) -->
					<div ng-if="summaryFilterBadges.highRisk">
						<p ng-if="selectedEntityjurisdiction.length > 0 && selectedEntityjurisdiction.self && selectedEntityjurisdiction.self.country">Country : {{selectedEntityjurisdiction.self.country}}</p>
						<p  ng-if="selectedEntityjurisdiction.length > 0 && selectedEntityjurisdiction.self && selectedEntityjurisdiction.self.high_risk_jurisdiction"> Jurisdiction Risk: {{selectedEntityjurisdiction.self.high_risk_jurisdiction}}</p>
					</div>
					<div ng-if="summaryFilterBadges.pep ||summaryFilterBadges.sanction "> 
						<h4 class="modal-title bst_border_b mar-b15 pad-b5 b-0 d-flex pad-r10 ai-c text-cream jc-sb text-overflow">
							<span class="ws-normal mar-b5">{{ pepDetails.value }}</span>
							
							<a ng-if="summaryFilterBadges.pep" href="javascript:void(0);" class="mar-autol" uib-popover="{{  pepDetails.pep ? 'Remove Pep as Significant' : 'Save Pep as Significant' }}"
								popover-trigger="'mouseenter'" popover-placement="bottom" popover-class="bottom-pop-wrapper" popover-append-to-body='true'
								ng-class=" pepDetails.pep ?  'text-dark-blue' : 'text-cream'  " ng-model="mySignificant" ng-click="pepSanctionSignificant(pepDetails.pep,pepDetails,typeIs,$index)">
									<i class="  fa fa-star f-18" ng-class=" pepDetails.pep ?  'text-dark-blue' : 'text-cream'  "
									aria-hidden="true"></i>
								</a>

								<a ng-if="summaryFilterBadges.sanction" href="javascript:void(0);" class="mar-l10" uib-popover="{{ pepDetails.sanction ? 'Remove Sanction as Significant' : 'Save Sanction as Significant' }}"
								popover-trigger="'mouseenter'" popover-placement="bottom" popover-class="bottom-pop-wrapper" popover-append-to-body='true'
								ng-class=" pepDetails.sanction ?  'text-dark-blue' : 'text-cream'  " ng-model="mySignificant" ng-click="pepSanctionSignificant(pepDetails.sanction,pepDetails,typeIs,$index)">
									<i class="  fa fa-star f-18" ng-class=" pepDetails.sanction ?  'text-dark-blue' : 'text-cream'  "
									aria-hidden="true"></i>
								</a>
								<span popover-trigger="outsideClick"  class="mar-l10 c-pointer" ng-if="commentedList.length > 0" uib-popover-template="'selectCount.html'" popover-placement="bottom-right" popover-class="bottom-pop-wrapper count-select-popover" popover-append-to-body='true' ng-click="scroll()">{{commentedListLength}}</span>
						</h4>
						<div  class="d-block">
							<div ng-repeat="entry in pepDetails.entries track by $index" ng-if="$index==0" class="d-flex  fg-1">
								<p ng-if="entry.watchlist_id" class="roboto-light mar-b5 "><span class="mar-r10">Watchlist Id:</span><span>{{entry.watchlist_id}}</span></p>
								<p ng-if="pepDetails.confidence" class="roboto-light mar-b5 mar-autol">Confidence level: {{pepDetails.confidence*100 |number : 2}} <span>%</span></p>
							</div>
						<p ng-if="entry.entry_id" class="roboto-light"><span class="mar-r10">Entry ID:</span><span >{{entry.entry_id}}</span></p>

						</div>
						
						<div class="custom-data-table-wrapper">
							<table class="striped-table table table-scroll scroll-table five-col-news full-width">
								<thead>
									<tr>
										<%--<th class="text-cream roboto-light">Watchlist Id</th>
									 <th class="text-cream roboto-light">Entity ID</th>
										<th class="text-cream roboto-light">Attribute Name</th> --%>
										<th class="text-cream roboto-light">Attribute Name</th>
										<th class="text-cream roboto-light">Recorder Information</th>
										<th class="text-cream roboto-light"></th>
										<th class="text-cream roboto-light">Matched Information</th>
										<th class="text-cream roboto-light">Matched Level(%)</th>
									</tr>
								</thead>
								<tbody class="scroll-body mxh-400">
									<tr ng-repeat="entry in pepDetails.entries track by $index">
										<%--<td class="text-cream roboto-light"> {{entry.watchlist_id}} </td>
									 <td class="text-cream roboto-light"> {{entry.entry_id}} </td>
										<td class="text-cream roboto-light"> <span class="d-flex" ng-repeat ="att_name in entry.attribute_name">{{att_name}}<span> </td> --%>
										<td class="text-cream roboto-light"> <span class="d-flex" ng-repeat ="att_name in entry.attribute_name">{{att_name.attribute_name}}<span> </td>
										<td class="text-cream roboto-light"> {{pepDetails.name }} </td>
										<td class="text-cream roboto-light" class="d-flex jc-fe" ng-repeat ="att_name in entry.attribute_name">
										<span ng-if="att_name.confidence < 0.5" style="border-radius:8px;background-color:#ef5350;width:24px;min-width:24px;height:16px;display:inline-flex;align-items:center;justify-content:center;margin-right:5px" ><i class="fa fa-times f-12" style="line-height:1"></i></span>
										<span ng-if="att_name.confidence  >= 0.5 && att_name.confidence  < 0.9" style="border-radius:8px;min-width:24px;background-color:#e6ae20;width:24px;height:16px;display:inline-flex;align-item:center;justify-content:center;margin-right:5px"><img src="../assets/images/tilde.svg" ></span>
										<span ng-if="att_name.confidence  >= 0.9" style="border-radius:8px;background-color:#3eb6ff;width:24px;min-width:24px;height:16px;display:inline-flex;align-item:center;justify-content:center;margin-right:5px"><img src="../assets/images/newEqual.svg" ></span>
										 </td>
										 <td class="text-cream roboto-light">  <span class="d-flex"><span>{{pepDetails.value }}</td>
										 <td class="text-cream roboto-light">  <span class="d-flex" ng-repeat ="att_name in entry.attribute_name">{{att_name.confidence*100 |number : 2}}<span> </td>
									</tr>
								</tbody>
							</table>
						</div>
					
					</div>
</div>
				<div class="no-data-wrapper text-center" ><div ng-show="ErrorMessage">data not found </div></div>
<div class="col-sm-3  pad-lt-0" ng-if="currentArticleExists && (summaryFilterBadges.adverse  || summaryFilterBadges.finance)">
	<!-- <div class="custom-scroll-wrapper" style=" min-height: 480px; max-height: calc(100vh - 100px);overflow-y: auto;">
		<h3 class="pad-t0 mar-b5">Source Reliability</h3>
		<div class="pulseVisualizerList-wrapper">
			<div class="marketPulseList">
				<div class=" input-group-mat width-a height-a p-rel mar-b0">
					<select class="text-capitalize mar-0 pad-l5 text-cream custom-input width-100 border-0 h-2_4 custom-select-box f-12 "
					 id="sourceReadability" ng-model="sourceType" ng-change="filterBySourcetype(sourceType,'')">
						<option class="text-capitalize " value="ALL" selected="selected">ALL</option>
						<option class="text-capitalize ng-binding ng-scope" value="high">PRIMARY</option>
						<option class="text-capitalize ng-binding ng-scope" value="Low">SECONDARY</option>
					</select>
					<span class="fa fa-caret-down p-abs r-10 t-5"></span>
				</div>
			</div>
		</div>
		<div> -->
			<!-- <a href="javascript:void(0);" class="text-grey width-100 mar-y0" >
								<label class="switch custom-switch-wrapper mar-l5">
									<input type="checkbox"  ng-model = "issignificant" ng-change="filterBySourcetype(sourceType,issignificant)" class="custom-switch-checkbox ng-untouched ng-valid ng-dirty ng-valid-parse ng-empty">
									<span class="slider round">
										<span class="iso-codes pull-left left ng-hide" ng-show="showIso">YES</span>
										<span class="iso-codes pull-right right" ng-hide="showIso">NO</span>
									</span>
								</label>
								<p class="inline mar-b0 lh-27" style="width: auto;">Show only marked</p>
							</a> -->
			<!-- Default false -->
			<!-- <div style="font-size:14px" class="custom-control custom-checkbox border-dark-grey-thin-b pad-t10 pad-b15">
							<label class="custom-control-label" for="defaultUnchecked">Show only marked</label>
							<input type="checkbox" class="custom-control-input" id="defaultUnchecked" ng-model = "issignificant1" ng-change="filterBySourcetype(sourceType,issignificant)">
						</div> -->

					<h3 class="mar-b5">Keywords</h3>
					<div class="pulseVisualizerList-wrapper">
						<div class="marketPulseList">							
								
							<ul class="toggleBarMenu mxh-320 list-unstyled">
							<!-- 	<li class="pulseVisualizerList-wrapper-li mar-b10">		
										<div ng-if="sourceUrl" class="lh-22">Source: <a href="{{routeUrl}}" target="_blank">{{sourceUrl}}</a></div>
											<div ng-if="publishedDate">Date: {{publishedDate}}</div>
								</li> -->
								<li class="pulseVisualizerList-wrapper-li" ng-repeat="entity in entitiesDetailed">
									<div class="toggle text-uppercase">
										<div class= "custom-checkbox  d-flex width-100  id= "parentof{{entity.key}}"><!-- class="custom-checkbox" -->
											<label class="checkbox-inline"> <input type="checkbox" checked value="{{ entity.key }}" ng-click="filterEntities(entity.key,'')" class="marketpulseparentCheck" id="parentCheck_{{entity.key }}"> <span><i class="fa fa-check"></i></span>
											</label> <span class="custom-check-text" id="custom-check-text{{ entity.key }}" ng-click="showHideMenu(entity.key)">{{ entity.key }}</span>
										</div>
									</div>
									<ul class="treeview-menu pad-b10  mxh-300 pad-l15 bg-transparent text-cream list-unstyled">
										<li ng-repeat="item in entity.values | toArray | orderBy :'$key'  ">
											<div>
												<div  class= "custom-checkbox d-flex width-100"><!-- class="custom-checkbox" -->
													<label class="checkbox-inline"> 
													<input type="checkbox" value="{{item}}" ng-click="filterEntities('',entity.key)" checked class="marketpulsechildrenCheck childCheck_{{entity.key }}"> <span><i class="fa fa-check"></i></span>
													</label>
													<small class="check-text width-90 pull-right">{{item.$key}}</small>
												 
												</div>
											</div>
										</li>
									</ul>
								</li>
								<!-- <li> <a class="btn btn-blue text-uppercase">Show Marked</a></li> -->
							</ul>
						</div>
				</div>
				<h3 class="pad-t15 mar-b5 d-none">Classification Filter</h3>
				<div class="d-none pulseVisualizerList-wrapper">
					<div class="marketPulseList">							
							
						<ul class="toggleBarMenu list-unstyled classfication-filter-wrapper mxh-300">
						<!-- 	<li class="pulseVisualizerList-wrapper-li mar-b10">		
									<div ng-if="sourceUrl" class="lh-22">Source: <a href="{{routeUrl}}" target="_blank">{{sourceUrl}}</a></div>
										<div ng-if="publishedDate">Date: {{publishedDate}}</div>
							</li> -->
							<li class="pulseVisualizerList-wrapper-li" ng-repeat="checklist in leftsideTilelist">
								<div class="toggle text-uppercase">
									<div class="custom-checkbox d-flex width-100">
										<label class="checkbox-inline"> <input type="checkbox" checked value="{{ checklist.key }}" ng-click="classificationFilter(checklist.key)" class="marketpulseparentCheck"> <span><i class="fa fa-check"></i></span>
										</label> <span class="custom-check-text" ng-click="classificationFilter(checklist.key)">{{ checklist.key }}</span>
									</div>
								</div>
							</li>
							<!-- <li> <a class="btn btn-blue text-uppercase">Show Marked</a></li> -->
						</ul>
					</div>
</div>

<h3 class="pad-t15 mar-b5 d-none">Sentiments</h3>
					<div class="pulseVisualizerList-wrapper d-none">
						<div class="marketPulseList">							
								
							<ul class="toggleBarMenu mxh-320 list-unstyled">
							<!-- 	<li class="pulseVisualizerList-wrapper-li mar-b10">		
										<div ng-if="sourceUrl" class="lh-22">Source: <a href="{{routeUrl}}" target="_blank">{{sourceUrl}}</a></div>
											<div ng-if="publishedDate">Date: {{publishedDate}}</div>
								</li> -->
								<li class="pulseVisualizerList-wrapper-li">
										<div class="toggle text-uppercase">
											<div class="custom-checkbox d-flex width-100">
												<label class="checkbox-inline"> <input type="checkbox" checked value="Positive"  class="marketpulseparentCheck"> <span><i class="fa fa-check"></i></span>
												</label> <span class="custom-check-text" >Positive</span>
											</div>
										</div>
									</li>							<li class="pulseVisualizerList-wrapper-li">
											<div class="toggle text-uppercase">
												<div class="custom-checkbox d-flex width-100 ">
													<label class="checkbox-inline"> <input type="checkbox" checked value="Negative"  class="marketpulseparentCheck"> <span><i class="fa fa-check"></i></span>
													</label> <span class="custom-check-text" >Negative</span>
												</div>
											</div>
										</li>							<li class="pulseVisualizerList-wrapper-li">
												<div class="toggle text-uppercase">
													<div class="custom-checkbox">
														<label class="checkbox-inline"> <input type="checkbox" checked value="Neutral"  class="marketpulseparentCheck"> <span><i class="fa fa-check"></i></span>
														</label> <span class="custom-check-text" >Neutral</span>
													</div>
												</div>
											</li>
								<!-- <li> <a class="btn btn-blue text-uppercase">Show Marked</a></li> -->
							</ul>
						</div>
				</div>
			</div></div>
				
						</div>
			</div>
		</div>
	</div>
	<!--  Risk Overview Modal Wrapper Ends  -->
	
	<script>
		setTimeout(function(){
			$('.pulseVisualizerText-wrapper .adverse-news-scroll,.pulseVisualizerList-wrapper .marketPulseList ul.toggleBarMenu.classfication-filter-wrapper,.pulseVisualizerList-wrapper .marketPulseList ul.toggleBarMenu .treeview-menu,.custom-list-group,.panel-group.question-box-wrapper').mCustomScrollbar({
				axis : "y",
				theme : "minimal-dark"
			});
			 
      $(".risk-tags-wrapper").mThumbnailScroller({
          axis: "x"
      });
		},100);

	</script>
	<script type="text/ng-template" id="add-officer-modal.html">
		<!--  add officer Modal Wrapper Starts  -->
<div class="risk-overview-modal-wrapper">
<!-- 		<div class="custom-spinner case-dairy-spinner" > -->
<!-- 			<i class="fa fa-spinner fa-spin fa-3x"></i> -->
<!-- 		</div> -->
		<div class="modal-header pad-b5">
			<button type="button" class="closePulseModal width-5 text-cream  pad-x0 pull-right" ng-click="closeModal()" data-dismiss="modal">&times;</button>
			<h4 class="modal-title width-95 text-overflow">
				<span id="articleHeader">Add Person/Company for screening</span>
			</h4>
		</div>
		<div class="modal-body">

				<div class="bst-uib-tab-wrapper pad-0 source-tabs-wrapper">
						<uib-tabset active="active">
							<uib-tab index="1" heading="Person"  ng-click ="entityScreeningTabchange('person')">
									<form class="form pad-r0" name="personForm" autocomplete="off">
											<div class="d-ib p-rel first-name-icon mar-b20 bst_input_group  width-50 ">					
												<input type="text" autocomplete="off" class="custom-input text-cream pad-t15 pad-x15 f-12 height-100"  placeholder="First Name" ng-model = "newOfficer.firstName">
												<span class="label">First Name*</span>
												<p class=" width-100 d-ib text-center text-dark-red f-10 mar-t5 ng-scope" ng-if="!newOfficer.firstName">*Please Enter First Name</p>
											</div>
											<div class="d-ib mar-b10 bst_input_group  width-45 p-rel pull-right">						
												<input type="text" autocomplete="off" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" placeholder="Last Name"  ng-model ="newOfficer.lastName">
												<span class="label"> Last Name</span>
												<p class=" width-45 d-none mar-xauto d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="enterName">*Required Field</p>
											</div>
											<div class="width-100 flag-icon mar-b20 jurisdiction-screening bst_input_group p-rel d-block" ng-class="{'modal-pad-left': customSelectedScreeningPerson }" id="input_container">
																		<input type="text" autocomplete="off" ng-model="customSelectedScreeningPerson" placeholder="" uib-typeahead="state as state.jurisdictionOriginalName for state in countryNames | filter:{jurisdictionOriginalName:$viewValue}" typeahead-template-url="customTemplate.html" class="custom-input pad-b5 pad-t15 pad-x15 mar-t0 bg-transparent typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="countrySelected($label,$item,'ScreeningPerson')" id="input">
																		<img ng-if="customSelectedScreeningPerson" ng-src="{{SelectedFlagSourceScreeningPerson}}" id="input_img">
																<span class="label p-abs t-0 flag_select_label" style="color:#888">Country Name </span>

		
												
												<!-- <p class=" width-100  mar-xauto d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="!customSelectedScreeningPerson">*Required Field</p> -->

											</div>
												<div class="width-100 mar-b20 calender-icon bst_input_group  p-rel d-block">							
													<input type="text" autocomplete="off" placeholder="Dob" name="dateofbirth" ng-pattern="/^\d{4}-\d{2}-\d{2}$/" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-model ="newOfficer.dateOfBirth">
													<span class="label">Date Of Birth</span>
													<p class=" width-100  mar-xauto d-ib text-center text-dark-red f-10 mar-b0 ng-scope" ng-show="personForm.dateofbirth.$invalid">*Please Enter Date in YYYY-MM-DD format</p>
												</div>
												<div ng-dropdown-multiselect="" class="mar-b20 git bst_input_group" options="screeningModelClassification.screeningPersonListData"  selected-model="screeningModelClassification.screeningPersonModel" events ="screeningSelectedPerson" extra-settings="screeningPersonRoleSettings" translation-texts="example5customTexts"></div>
												<div class="dropdown bst_input_group mar-b20 height-100 p-rel">
													<select class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty" style="-webkit-appearance: menulist !important;"
														ng-model="screeningModelClassification.screeningPersonRoleModel" 
														ng-options="v.label for v in screeningModelClassification.screeningPersonRoleData">
													</select>
													<span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Role</span>
												</div>
												<button type="button" class="bg-transparent text-uppercase text-dodger-blue f-14 border-0 pull-right" ng-click="closeModal()">Cancel</button>
												<button type="submit" class="bg-transparent text-uppercase text-dodger-blue f-14 border-0 pull-right" ng-class ="(newOfficer.requiredPersonname || personForm.dateofbirth.$invalid) ? 'c-ban' :'' " ng-disabled ="disabling(newOfficer.firstName,'customSelectedScreeningPerson')" ng-click ="addOfficerScreening()">Save</button>
											
										</form>
							</uib-tab>
							<uib-tab index="2" heading="Company" ng-click ="entityScreeningTabchange('organization')">
								
									<form class="form pad-r0"  name ="form" autocomplete="off">
											<div class="d-ib p-rel company-icon mar-b20 bst_input_group width-100 ">					
												<input type="text" autocomplete="off" class="custom-input text-cream pad-t15 pad-x15 f-12 height-100" placeholder="Company Name *"   ng-model = "newOfficer.CompanyName">
												<span class="label">Company Name *</span>
												<p class=" width-100  d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="!newOfficer.CompanyName">*Please Enter Company name</p>

											</div>
											 
											<div class="width-100 flag-icon mar-b20 jurisdiction-screening bst_input_group p-rel d-block" ng-class="{'modal-pad-left': customSelectedScreeningCompany }" id="input_container">
												<input type="text" autocomplete="off" ng-model="customSelectedScreeningCompany" placeholder="" uib-typeahead="state as state.jurisdictionOriginalName for state in countryNames | filter:{jurisdictionOriginalName:$viewValue}" typeahead-template-url="customTemplate.html" class="custom-input pad-b5 pad-t15 pad-x15 mar-t0 bg-transparent typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="countrySelected($label,$item,'ScreeningCompany')" id="input">
												<img ng-if="customSelectedScreeningCompany" ng-src="{{SelectedFlagSourceScreeningCompany}}" id="input_img">
												<span class="label p-abs t-0 flag_select_label" style="color:#888">Country Name </span>
												<!-- <p class=" width-100  mar-xauto d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="!customSelectedScreeningCompany">*Required Field</p> -->
											</div>
											
												<div class="width-100 mar-b20 website-icon bst_input_group p-rel d-block">							
													<input type="text" autocomplete="off"  name ="website"placeholder="website Name" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-pattern="/(http(s)?:\\)?([\w-]+\.)+[\w-]+(\[\?%&=]*)?/" ng-model ="newOfficer.website">
													<span class="label">Website</span>
												</div>	<p class=" width-100 mar-xauto d-ib text-center text-dark-red f-10 mar-b0 ng-scope" ng-show="form.website.$invalid"  >*Please Enter Valid format</p>
												<div ng-dropdown-multiselect="" class="bst_input_group mar-b20" options="screeningModelClassification.screeningCompanyListData"  selected-model="screeningModelClassification.screeningCompanyModel" events ="screeningSelectedCompany" extra-settings="screeningComapanyRoleSettings" translation-texts="example5customTexts"></div>
												<button type="button" class="bg-transparent text-uppercase text-dodger-blue f-14 border-0 pull-right" ng-click="closeModal()">Cancel</button>
												<button type="submit" class="bg-transparent text-uppercase text-dodger-blue f-14 border-0 pull-right" ng-class ="(!newOfficer.CompanyName || form.website.$invalid ) ? 'c-ban' :'' "  ng-disabled= "disabling(newOfficer.CompanyName,'customSelectedScreeningCompany')" ng-click ="addOfficerScreening()">Save</button>
											
										</form>
									</uib-tab>
						</uib-tabset>					
							


			<!-- <div class="col-md-6 col-sm-6 col-lg-6">
				Person <input name="entity-type" type = "radio" ng-model="newOfficer.ScreeeningModal" value="Person"  ng-click= "changeScreeningModal('Person')">
			</div>
			<div class="col-md-6 col-sm-6 col-lg-6">
				Company <input name="entity-type" type = "radio"  ng-model="newOfficer.ScreeeningModal " value="Company" ng-click= "changeScreeningModal('Company')">
			</div>
			<div class="" ng-if= "newOfficer.ScreeeningModal === 'Person'">		
				
			</div>
			<div class="" ng-if=  "newOfficer.ScreeeningModal === 'Company'">		
			
			</div> -->
		</div>
	</div>
	<!--  add officer Modal Wrapper Ends  -->
</script>
	<!--  Evidence Source popover Wrapper Starts  -->
	<script type="text/ng-template" id="sourceCountForEntities.html">
            <div class="select-sentiment-wrapper height-100 border-b0" id="screen_shot">
              <div class="bst_input_group bst_input_group_r  height-a">
                                        <!-- <div class="dropdown height-100 p-rel">
                                                        <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
                                                        </select>
                                                        <span class="label p-abs t-0 flag_select_label" style="color:#888">Entity Type</span>
                                        </div> -->
                                
                                    <input 
                                    class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10"
                                    autocomplete="off"
																		placeholder="source"
																		ng-model="searchScreenShotSource">
                                    <span class="label f-12 text-dark-cream">Source</span>
                                    <span class="error" ng-if="false">Source is not in the directory</span></div>
								<div class="d-flex ai-c pad-5"><p class="mar-b0 text-cream"><span>{{getMultiScreenShotSelected.length}}</span> Entities Selected</p>
								<%-- <button class="bordered-button sm-btns mar-autol mar-r10" ng-click="selectAllEntities()">Select All</button>
								<button class="bordered-button sm-btns mar-autol mar-r10" ng-click="deselectEntities()">Unselect All</button> --%>
								<div class="mar-autol d-flex ai-c">
								  <button class="grad-button d-flex ai-c sm-btns" ng-click="multipleAddToPage();" ng-show="isFromAddToPage"><img class="mar-r5" src="..\assets\img\file-plus-white.svg"/>Add To Page</button>
								  <span class="c-pointer pad-l5" ng-click="myPopover.close()"><i class="fa fa-times"></i></span>
							  </div>
							    <%-- <button class="grad-button d-flex ai-c sm-btns" ng-click="getMultiEvidence(getMultiScreenShotSelected)" ng-show="!isFromAddToPage && getMultiScreenShotSelected.length >0">Get Evidence</button> --%>
								</div>
                                     <div class="custom-data-table-wrapper">       
                                         <table class="table custom-table table-scroll border-0 scroll-table table-striped-z three-col-source-selection full-width"><thead><tr><th class="text-unset">Sources</th><th class="text-unset">Source Screenshot</th><th></th></tr></thead><tbody class="table-scroll-evidence height-90">

																				
																					<tr ng-repeat="(key,value) in filteredBars = (sourceWithBSTRegistry | filter:searchScreenShotSource)" ng-class="{'c-ban':value.disable}">
																					<td class="bst-checkbox-wrapper" ng-class="{'pe-none':value.disable}">
																							<div class="mar-l5 checkbox-inline" ng-hide="value.row_spinner">
																									<input type="checkbox"  class="mar-l0 d-block z-99 c-pointer op-0 " ng-model="value.checked" ng-click="setMultiCheckScreenshot(value)">
																									<span class="bg-blue-checkbox mar-r0"> 
																										<i class="fa fa-check"></i>
																									</span>
																							</div>
																							<div class="mar-l5 checkbox-inline"  ng-show="value.row_spinner">
																									<input type="checkbox"  class="mar-l0 d-block z-99 c-pointer op-0 pe-none " >
																									<span class="fa fa-spinner fa-spin fa-2x mar-r0"> 
																											
																									</span>
																									 
																							</div>
																							<span class="mar-l10 c-arrow">{{value.sourceName}}</span>
																					</td>
																					<td ng-class="{'pe-none':value.disable}" ng-if="value.uploadedFileName">{{value.uploadedFileName}}</td>
																					<td ng-class="{'pe-none':value.disable}" ng-if="!value.uploadedFileName">{{value.splitSourceScreenshot || '' }}</td>
																					<td>
                                                                                            <ul class="custom-list pad-l0  d-flex ai-c">
												                                             <li><span ><i class="icons icon-cloud-upload" aria-hidden="true" ng-if="value.showUploadIcon"  ng-click ="sourceLinkdUploadFile(value,$index)"></i></span></li>
																							 <li><span ><i class="fa fa-camera" ng-click="callScreenShotAPI(value,$index,value.uploadedFileName)" ng-if="value.showHideScreenIcon"></i></span></li>
																							 <li><span ng-class="{'c-ban':value.disable}"><i class="icons icon-cloud-download"  ng-click="downloadEvidenceSourceScreenshot(value,$event)"></i></span></li>
																							 <!-- <li><span><i class="fa fa-trash tex-coral-red" ng-if="value.showHideScreenIcon" ng-click="deleteEvidenceSourceScreenshot($index,$event)"></i></span></li> -->
																							 <li><span ng-class="{'c-ban':value.disable}"><img src="..\assets\img\file-plus-grey.svg" ng-if="!value.showHideAddtoPage && !value.addtopageloader" ng-click="addToPage(value,$index)" ng-class="{'pe-none':value.disable}"></span></li>
																							 <li ng-if="value.showHideAddtoPage" ><i class="fa fa-check"></i></li>
																							 <li class="custom-spinner mar-l15 p-rel t-10 l-0" ng-if="value.addtopageloader" ><i class="fa f-16 fa-spinner fa-spin"></i></li>
																							 <li><span><a href="{{value.SourceValue['bst:registryURI']}}" target="_blank" ng-class="{'c-ban':value.disable}"><i class="fa fa-external-link" ng-class="{'pe-none':value.disable}" ng-disabled="value.disable && !value.SourceValue['bst:registryURI']"></i></a></span></li>
																							</ul>
																					</td>
																				</tr>
                                            </tbody></table></div> 
																						<p ng-hide="filteredBars.length">Nothing here!</p>
            </div>
        </script>
	<!--  Evidence Source popover Wrapper Ends  -->
	<script type="text/ng-template" id="markInsignificant.html">
	      <!--  Insignificant Modal Wrapper Starts  -->
	       <div class="risk-overview-modal-wrapper">
	                       <div class="modal-header pad-b5">
	                           <button type="button" class="closePulseModal width-5 text-cream  pad-x0 pull-right" ng-click="markInsignificantComment(false)" data-dismiss="modal">&times;</button>
	                           <h4 class="modal-title width-95 text-overflow">
	                                    <i class="fa fa-star f-18 text-dark-blue" ng-class="makeInsignificantComment.isSignificant ? 'text-dark-blue' : 'text-cream'  " aria-hidden="true"></i>
	                                <span id="articleHeader">Marked as Significant</span>
	                           </h4>
	                       </div>
	                       <div class="modal-body">
							<span class="fa f-40 mar-r10 text-center d-block text-dark-cream fa-commenting"></span>
							<p class="d-flex text-dark-cream jc-c">please Add a Note(required)</p>
							<div class="bst_panel_wrapper custom-panel">
	                               <form class="form pad-r0" autocomplete="off" name="personForm">
	                                      <div class="d-flex mar-b20 ai-c">
	                                          <div class=" p-rel pad-t20 mar-r5 bst_input_group markInsignificant">
	                                               <textarea name="ReasonforInsignificant" ng-model="insignificantComment"
														   required="true" class="custom-input height-100"></textarea>
													<span class="label  p-abs text-dark-blue f-10 t-5 l-0">Reason for Insignificant*</span>
	                                               <p class=" width-100  text-center text-coral-red f-11 mar-t5 ng-scope" ng-if="enterName" >*Please Enter First Name</p>
	                                           </div>  
	                                      </div>
	                                          <div class="d-flex jc-fe">
	                                              <button type="button" class="bordered-button lh-18 f-14 height-a pull-right"  ng-click="markInsignificantComment(false)">Cancel</button>
	                                              <button type="submit" class="grad-button mar-l10 f-14 lh-20 height-a pull-right" ng-disabled="!insignificantComment" ng-class="{'c-ban op-85':!insignificantComment,'op-1':'insignificantComment'}" ng-click ="markInsignificantComment(true,insignificantComment,makeInsignificantComment)">Comment</button>
	                                          </div>
	                                         
	                                   </form>				
                                    </div>      
	               
	              
	                         
	                       </div>
	                   </div>
	                   <!--  Insignificant Modal Wrapper Ends  -->
	       </script>


	<!--  ==============================================================================
**********************  Gulp Path Ends *********************
==============================================================================*/
-->
	<script type="text/ng-template" id="selectCount.html">
		<div class="select-count-wrapper height-100 border-b0">
		
			<ul class="bg-transparent custom-list pad-l0 item-1 pad-y0 height-100" >
					<li class="c-pointer pad-y5 light-hover p-rel"  ng-repeat="commentItem in commentedList track by $index">
							<div class="d-flex ai-c">
						 <div class="ai-c jc-c d-flex media-icon">
							 <i class="fa fa-user text-dark-cream"></i>
						 </div>
						 <a href="javascript:void(0);" class="mar-r0 pad-l5 pad-r0 d-flex text-cream ai-c jc-sb" >{{commentItem.userName}}
							 <span class="d-flex ai-c" style="margin-right:40px">	<i  class=" fa f-14 fa-star mar-r40 text-dark-cream mar-r5" aria-hidden="true" ></i></span>
						 </a><span class="mar-autol">{{commentItem.createdDate | date:"MM/dd/yyyy 'at' h:mma"}}</span></div>
						 <div class="bottom-details-wrapper"><p class="word-ba pad-y5 f-12 text-cream">{{commentItem.comment}}</p></div>
					 </li>		
			</ul>
	</div>
</script>


	<script type="text/ng-template" id="selectCount.html">
		<div class="select-count-wrapper height-100 border-b0">
		
			<ul class="bg-transparent custom-list pad-l0 item-1 pad-y0 height-100" >
					<li class="c-pointer pad-y5 light-hover p-rel"  ng-repeat="commentItem in commentedList track by $index">
							<div class="d-flex ai-c">
						 <div class="ai-c jc-c d-flex media-icon">
							 <i class="fa fa-user text-dark-cream"></i>
						 </div>
						 <a href="javascript:void(0);" class="mar-r0 pad-l5 pad-r0 d-flex text-cream ai-c jc-sb" >{{commentItem.userName}}
							 <span class="d-flex ai-c" style="margin-right:40px">	<i  class=" fa f-14 fa-star mar-r40 text-dark-cream mar-r5" aria-hidden="true" ></i></span>
						 </a><span class="mar-autol">{{commentItem.createdDate | date:"MM/dd/yyyy 'at' h:mma"}}</span></div>
						 <div class="bottom-details-wrapper"><p class="word-ba pad-y5 f-12 text-cream">{{commentItem.comment}}</p></div>
					 </li>		
			</ul>
	</div>
</script>
	<!--  ==============================================================================
**********************  Sources List Popover Path Ends *********************
==============================================================================*/
-->


	<script type="text/ng-template" id="sourceDescription.html">
		<div class="source-description-wrapper height-100 border-b0 ">			
			<div class="popover-body">
			<div class="col-sm-6 pad-l0">
				<div class="top-heading border-thin-dark-cream-b d-flex ai-c">
					<h4>Primary</h4>
					<span class="fa text-dark-pastal-green mar-r5 fa-link">3</span>
				</div>
				<div class="bottom-content-wrapper">
					<ul class="custom-list pad-l0 item-1">
						<li class="d-flex ai-c">
							<span>EuroPages</span><i class="fa fa-check-circle "></i>
						</li><li class="d-flex ai-c">
							<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
						</li><li class="d-flex ai-c">
							<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
						</li><li class="d-flex ai-c">
							<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-6 pad-r0">
				<div class="top-heading border-thin-dark-cream-b d-flex ai-c">
					<h4>Primary</h4>
					<span class="fa text-dark-cream mar-r5 fa-link">3</span>
				</div>
				<div class="bottom-content-wrapper">
						<ul class="custom-list pad-l0 item-1">
								<li class="d-flex ai-c">
									<span class="width-100 ws-normal" style="word-break: break-word;white-space:pre-wrap">EuroPages</span><i class="fa fa-check-circle "></i>
								</li><li class="d-flex ai-c">
									<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
								</li><li class="d-flex ai-c">
									<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
								</li><li class="d-flex ai-c">
									<span>EuroPages</span><i class="fa fa-check-circle d-none"></i>
								</li>
							</ul>
				</div>
			</div>
			</div>
		</div>
	</script>
	<script type="text/ng-template" id="ownership-popover.html">
		<div class="ownership-popover-wrapper ownership-container height-100 border-b0">
		<div class=top-content-wrapper>
				<div class="bp-corner-all bt-item-frame bp-item">
					<div class="width-100 top-heading">
						<span class="flag-icon mar-x5 text-cream  flag-icon-squared flag-wrapper flag-pop c-pointer f-9"></span>
						<span name="description" class="text-cream orgTooltipclass description percentageInfo indirectpercenOwnship">10.06%</span>
						<h3 name="title" class="bp-item text-overflow orgChartParentEntity shareCubes">
							<i class="fa fa-university" style="color:#4c9d20"></i>
							Capital Group International
						</h3>
					</div><div class="bottom-list-wrapper text-right d-none">
							<ul class=""> 
							<li><i class="fa text-cream fa-user-alt-slash user-pop c-pointer"></i></li>
							<li><i class="fa text-cream fa-street-view street-pop c-pointer "></i></li>
							<li><i class="fa text-cream fa-globe globe-pop c-pointer "></i></li>
							<li><i class="fa text-cream fa-gavel gavel-pop c-pointer "></i></li>
							<li ><i class="fa text-cream fa-newspaper-o newspaper-pop c-pointer "></i></li>
						</ul>
					</div>
				</div>
		    </div>
			<div class=bottom-content-wrapper></div>
			<div class="">
				<ul class="list-unstyled progressbar-list mar-b0">
					<div class="top-heading ">
						<h3 class="f-14 pad-b5 text-yellow mar-b10 border-cream-thin-b">AGENCE DES PARTICIPATIONS DE L'ETAT</h3>
						<div> 
							<li class="progressbar-list-item mar-b10 pad-t0">
								<div class="left-col f-12 text-cream" style="width: 120px;">Indirect Ownership </div>
								<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">
									<div class="progress-bar progress-gradient-blue" role="progressbar" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100" >
										<span class="sr-only ">11</span></div></div><div class="right-col f-12 text-cream" style="width: 60px;">11.11%</div>
							</li>
									<li class="progressbar-list-item">
										<div class="left-col f-12 text-cream" style="width:120px;">Direct Ownership</div>
										<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);"> 
											<div class="progress-bar progress-gradient-curious-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
												<span class="sr-only ">100</span>
											</div>
										</div>
											<div class="right-col f-12 text-cream" style="width: 60px;">100.00%</div>
									</li>
								</ul>
							</div>	
			</div>
	</script>

	<script type="text/ng-template" id="approval.html">
		<div class="approval-popover-wrapper height-100 border-b0">
			<div class=top-content-wrapper>
				<h3 class="roboto-light mar-b0 f-14 text-dark-cream"><span >{{entitySearchResult.complianceWidgets.popoverMsg}}</span> </h3>
		    </div>
			<div class=bottom-content-wrapper>
				<p class="f-12 mar-b0 roboto-light text-cream"><span >{{entitySearchResult.complianceWidgets.popoverDate | date :  "short"}}<span></p>
			</div>
		</div>
	</script>
	<!-- <script type="text/ng-template" id="selectedEntity.html">
		<ul class="d-flex ai-c selected-entity-wrapper custom-list pad-l0"><li><span>2</span>: Entities</li><li class="d-flex ai-c">Add: <span class="entity-icon-wrapper d-flex ai-c jc-c mar-x5"  popover-placement="bottom"
                    uib-popover-template="'selectedEntityDropdown.html'"
                    popover-class="bottom-pop-wrapper selected-entity" popover-append-to-body='true'><i class="fa fa-building"></i></span><span class="entity-icon-wrapper d-flex ai-c jc-c"><i class="fa fa-user"></i></span></li><li><i class="fa fa-long-arrow-up"></i></li><li class="d-flex ai-c"><i class="fa fa-info-circle"></i><i class="fa fa-pencil mar-x5"></i><i class="fa fa-trash text-coral-red"></i></li></ul>
	</script> -->
	<script type="text/ng-template" id="selectedEntityDropdown.html">
		<ul class="d-flex ai-c fd-c selected-entity-dropdown-wrapper custom-list pad-l0"><li><span><i class="fa fa-building mar-r10 f-16 text-dark-green"></i>sdffsdf</span></li><li><span><i class="fa fa-building  mar-r10 f-16  text-dark-blue"></i>sdffsdf</span></li></ul>
</script>
	<!-- <div class="ownership-popover-wrapper right-pop ownership-container height-100 border-b0">
			<div class=top-content-wrapper>
					<div class="bp-corner-all bt-item-frame bg-transparent">
						<div class="width-100 top-heading">
							<span class="flag-icon mar-x5 text-cream  flag-icon-squared flag-wrapper flag-pop c-pointer f-9"></span>
							<span name="description" class="text-cream orgTooltipclass description percentageInfo indirectpercenOwnship">10.06%</span>
							<h3 name="title" class="bp-item text-overflow orgChartParentEntity shareCubes">
								<i class="fa fa-university" style="color:#4c9d20"></i>
								Capital Group International
							</h3>
							<p class="mar-b0 text-cream">Lorem ipsum dolor sit fugit ad nem</p>
						</div><div class="bottom-list-wrapper text-right">
								<ul class="">
								<li><i class="fa text-cream fa-user-alt-slash user-pop c-pointer"></i></li>
								
								<li><i class="fa text-cream fa-gavel gavel-pop c-pointer "></i></li>
								<li ><i class="fa text-cream fa-newspaper-o newspaper-pop c-pointer "></i></li>
							</ul>
						</div>
					</div>
				</div>
				<div class=bottom-content-wrapper></div>
				<div class="">
					<ul class="list-unstyled progressbar-list mar-b0">
						<div class="top-heading ">
							<h3 class="f-14 pad-b5 text-yellow mar-b10 border-cream-thin-b">AGENCE DES PARTICIPATIONS DE L'ETAT</h3>
							<div> 
								<li class="progressbar-list-item mar-b10 pad-t0">
									<div class="left-col f-12 text-cream" style="width: 120px;">Indirect Ownership </div>
									<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);">
										<div class="progress-bar progress-gradient-blue" role="progressbar" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100" >
											<span class="sr-only ">11</span></div></div><div class="right-col f-12 text-cream" style="width: 60px;">11.11%</div>
								</li>
										<li class="progressbar-list-item">
											<div class="left-col f-12 text-cream" style="width:120px;">Direct Ownership</div>
											<div class="progress progress-gradient-grey" style="width: calc(100% - 180px);"> 
												<div class="progress-bar progress-gradient-curious-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
													<span class="sr-only ">100</span>
												</div>
											</div>
												<div class="right-col f-12 text-cream" style="width: 60px;">100.00%</div>
										</li>
									</ul>
								</div>	
				</div> -->

	<script type="text/ng-template" id="entityComplianceModal.html">
		<!--  Entity Compliance Details/Identifiers modal  -->
					<div class="modal-header pad-y10 d-flex ai-c">
							<h4 class="modal-title mar-autor"><span> {{title}} </span></h4>
							<button type="button" class="f-24 bg-transparent border-0 mar-l10 text-cream " ng-click="closeDataPopUp()"
									data-dismiss="modal" aria-hidden="true"><span>&#x2716;</span></button>
					
					</div>
					<div class="modal-body clearfix">
					
							<div class="entity-overRiding-modal" style="overflow-y: auto;height: auto;min-height: 150px;max-height: 450px;">
									<div class="col-xs-12 pad-x0 bst_custom_accordian company_details">
											<uib-accordion close-others="oneAtATime">
													<div uib-accordion-group class="panel-default" ng-click="toggleAccordian('primary', $event)" is-open="accordian.primaryOpen">
															<uib-accordion-heading>
																	Primary <span class="text-dark-pastal-green mar-l5" ng-style="myStyle2" style="cursor: pointer;">
																			<i class="fa f-10 fa-link"></i> {{primarySource.length || 0}}</span>
																	<i class=" fa" ng-class="{'fa-caret-down': accordian.primaryOpen, 'fa-caret-left': !accordian.primaryOpen}"></i>
					
															</uib-accordion-heading>
															<div class="" ng-if="primarySource.length == 0">
																			<p style="word-break: break-word;white-space:pre-wrap">No Primary source available</p>
															</div>
															<div ng-if="primarySource && primarySource.length > 0" class="sources-list-details " ng-repeat="primary in primarySource">
																	<p style="word-break: break-word;white-space:pre-wrap" ng-bind-html="(primary.sourceDisplayName || primary.source) | sourceurlbase"></p>
																	<span  class="overflow-a custom-scroll-wrapper original-content-wrapper mxh-130">{{primary.value.toString()}}</span>
																	<label class="switch mar-l25 custom-switch-wrapper override-switch bst-switch-wrapper">
																			<input style="" type="radio" name="override" ng-value="primary.source"
																					ng-model="overrideValue" ng-click="changeOverrideValue(primary.source, primary.value, primary.sourceUrl, primary.sourceDisplayName)" /></span>
																			<span class="slider round">
																			</span>
																	</label>
																	<span> <a href="{{makeUrlSecureToWork(primary.sourceUrl)}}" target="_blank" class="fa fa-external-link" ng-style="primary.source.toLowerCase() === 'bst' ? {'visibility': 'hidden' } :   {'visibility': 'visible' }"></a></span>
															</div>
													</div>
													<div uib-accordion-group class="panel-default" ng-click="toggleAccordian('secondary', $event)" is-open="accordian.secondaryOpen">
															<uib-accordion-heading>
																	Secondary <span class="text-dark-cream mar-l5" ng-style="myStyle2" style="cursor: pointer;"><i class="fa f-10 fa-link"></i>
																			{{secondarySource.length || 0}}</span>
																	<i class=" fa" ng-class="{'fa-caret-down': accordian.secondaryOpen, 'fa-caret-left': !accordian.secondaryOpen}"></i>
					
															</uib-accordion-heading>
															<div class="" ng-if="secondarySource.length == 0">
																	<p style="word-break: break-word;white-space:pre-wrap">No Secondary source available</p>
															</div>
															<div ng-if="secondarySource && secondarySource.length > 0" ng-repeat="secondary in secondarySource">
																	<div class="sources-list-details pad-y5" ng-if="schema !== 'bst:stock_info' && schema !== 'lei:legalForm'">
																			<p style="word-break: break-word;white-space:pre-wrap" ng-bind-html="(secondary.sourceDisplayName || secondary.source) | sourceurlbase"></p>
																			<span  class="overflow-a custom-scroll-wrapper original-content-wrapper mxh-130">{{secondary.value.toString()}}</span>
																			<label class="switch mar-l25 custom-switch-wrapper override-switch bst-switch-wrapper">
																					<input style="" type="radio" name="override" ng-value="secondary.source"
																							ng-model="overrideValue" ng-click="changeOverrideValue(secondary.source, secondary.value,secondary.sourceUrl, secondary.sourceDisplayName)" />
																					<span class="slider round">
																					</span>
																			</label>
																			<span><a href="{{makeUrlSecureToWork(secondary.sourceUrl)}}" target="_blank" class="fa fa-external-link" ng-style="secondary.source.toLowerCase() === 'bst' ? {'visibility': 'hidden' } :   {'visibility': 'visible' }"></a></span>
																	</div>
					
																	<div class="sources-list-details pad-y5" ng-if="schema === 'bst:stock_info'">
																			<p style="word-break: break-word;white-space:pre-wrap">{{(secondary.sourceDisplayName || secondary.source) | sourceurlbase}}</p>
																		<div class="overflow-a custom-scroll-wrapper original-content-wrapper mxh-130">	<span class="word-wb">{{secondary.value.main_exchange}}</span></div>
																			<label class="switch mar-l25 custom-switch-wrapper override-switch bst-switch-wrapper">
																					<input style="" type="radio" name="override" ng-value="secondary.source"
																							ng-model="overrideValue" ng-click="changeOverrideValue(secondary.source, secondary.value.main_exchange,secondary.sourceUrl, secondary.sourceDisplayName)" />
																					<span class="slider round">
																					</span>
																			</label>
																			<span><a href="{{makeUrlSecureToWork(secondary.sourceUrl)}}" target="_blank" class="fa fa-external-link" ng-style="secondary.source.toLowerCase() === 'bst' ? {'visibility': 'hidden' } :   {'visibility': 'visible' }"></a></span>
																	</div>
					
																	<div class="sources-list-details pad-y5 " ng-if="schema === 'lei:legalForm'">
																			<p style="word-break: break-word;white-space:pre-wrap">{{(secondary.sourceDisplayName || secondary.source) | sourceurlbase}}</p>
																		<div class="overflow-a custom-scroll-wrapper original-content-wrapper mxh-130">	<span class="word-wb">{{secondary.value.label}}</span></div>
																			<label class="switch mar-l25 custom-switch-wrapper override-switch bst-switch-wrapper">
																					<input style="" type="radio" name="override" ng-value="secondary.source"
																							ng-model="overrideValue" ng-click="changeOverrideValue(secondary.source, secondary.value.label,secondary.sourceUrl, secondary.sourceDisplayName)" />
																					<span class="slider round">
																					</span>
																			</label>
																			<span><a href="{{makeUrlSecureToWork(secondary.sourceUrl)}}" target="_blank" class="fa fa-external-link" ng-style="secondary.source.toLowerCase() === 'bst' ? {'visibility': 'hidden' } :   {'visibility': 'visible' }"></a></span>
																	</div>
					
																	
															</div>
													</div>
											</uib-accordion>
											<div class="user-input-wrapper pad-y10  pad-x25" >
													<div class="top-heading border-thin-dark-cream-b d-flex ai-c">
															<h4 class="">User Input</h4>
													</div>
													<div class="bottom-content-wrapper">
															<button ng-if="!showAddbutton" type="button" class="btn ai-c btn-block btn-sm d-flex mar-t5 text-dark-cream" ng-click="enabledSouceInputs()"><span class= "fa fa-plus text-cream mar-r10"></span> Add New</button>
															<div ng-if="userModifiedValue">
															<div class="d-flex f-14 pad-y5 mar-y5 text-dark-cream  roboto-light">
																<div class="width-25 d-flex pad-r10">
																	<i class="fa mar-r10 text-cream fa-user-o"></i><span style="flex: 1 0 0%;" class="text-overflow">{{userModifiedBy}}</span></div>
																<div class=" width-75 ws-normal changed-content-wrapper overflow-a mxh-130">	<span class="word-wb" style="flex: 1 0 50%;">{{userModifiedValue.toString()}}</span></div>
																
																<span style="flex:.3">
																				<label class="switch mar-l25 custom-switch-wrapper override-switch bst-switch-wrapper">
																						<input type="radio" ng-value="userModifiedValue" name="override" ng-model="overrideValue"
																								ng-click="changeOverrideValue('', userModifiedValue)" />
																						<span class="slider round"></span>
																				</label></span>
																</div>
															<div class="d-flex f-14 pad-y5  text-dark-cream roboto-light" ng-if="originalAns"><div  class="d-flex width-25 pad-r10"><i class="fa text-cream mar-r10  fa-history"></i><span style="flex: 1 0 0%;" class="pad-r10">Original Answer </span></div><div class="original-content-wrapper overflow-a mxh-130" style="flex: 1 0 75%;"><span class="word-wb">{{originalAns}}</span></div></div>

														
															<div class="d-flex f-14 pad-y5 text-dark-cream roboto-light"><div  class="d-flex width-25 pad-r10"><i class="fa text-cream mar-r10 fa-calendar"></i><span style="flex: 1 0 0%;" class="pad-r10">Changed Date </span></div><span style="flex: 1 0 50%;">{{userModifiedOn}}</span></div>
														</div>
													</div>
											</div>
									


									</div>
					
							</div>
							
							<div class="d-flex pad-b20 mar-x25 mar-t10 pad-t10 d-none ai-c bst_border_t" ng-if="userModifiedValue || showAddbutton">
								<form class="form jc-sb pad-x0 d-flex" ng-submit="applySourceEvidence(form)" name="form">
									<div class="width-70 source-icon p-rel d-block" ng-show = "popOverEditDataForOwnership.entity_type !== 'person' || !popOverEditDataForOwnership.entity_type">
										<div class="bst_input_group bst_input_group_r  height-a">
									<!-- <div class="dropdown height-100 p-rel">
													<select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
													</select>
													<span class="label p-abs t-0 flag_select_label" style="color:#888">Entity Type</span>
									</div>  ng-focus="getNewSources()"-->
							
								<input 
								class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10"
								autocomplete="no-place"
								placeholder="source"								
								ng-keyup="sourceSearchInputOveride(dataPopObject.overideSearchedInputSourceEdit)"
								ng-value="overideSearchedInputSourceEditVal"
								ng-model="dataPopObject.overideSearchedInputSourceEdit"
								ng-disabled="disableInput"
							required />
								<span class="label f-12 text-dark-cream">Source</span>
								<!-- <span class="error" ng-if="(showAddNew && fiteredSourceList && fiteredSourceList.length == 0)">Source is not in the directory</span> -->
								<ul class="custom-list searchSource z-99 mxh-140 l-0 pad-y10  item-1"
										ng-if="showfiteredSourceList && dataPopObject.overideSearchedInputSourceEdit && fiteredSourceList && sourceUrlList ">
											<div class="width-95 mar-b0 mar-x10   p-rel d-block">	
												<div class="bst_input_group bst_input_group_r  height-a">						
													<input type="text" ng-disabled="disableInput" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" http-prefix placeholder="sourceURL"  ng-value="dataPopObject.overideSearchedInputURLEdit" ng-model="dataPopObject.overideSearchedInputURLEdit" name="sourceURL" class="custom-input text-cream mar-b0 pad-x15 pad-t15 f-12 height-100" required />
													<span class="label">Source URL *</span>
												</div>
											</div>
											<!-- <li class="pad-x10 f-14 h-30 d-flex ai-c" ng-show="form.sourceURL.$dirty && form.sourceURL.$error.required"><span>*Source URL Required</span></li>
											<li class="pad-x10 f-14 h-30 d-flex ai-c" ng-show="form.sourceURL.$dirty && form.sourceURL.$error.pattern"><span>*Source URL Pattern Is Invalid</span></li> -->
								</ul>
								<ul style="top:95px;" class="custom-list searchSource z-99 mxh-140 l-0 pad-y10  item-1"
										ng-if="showfiteredSourceList && dataPopObject.overideSearchedInputSourceEdit && (fiteredSourceList && fiteredSourceList.length > 0) && sourceUrlList">
									
										<li class="pad-x10 f-14 h-30 d-flex ai-c" ng-repeat="sourceNameSearched in fiteredSourceList track by $index"
												ng-click="fillSourceSearchedInputOveride(sourceNameSearched)">
												<span>
												<i class="fa pad-r10 c-pointer fa-link" ng-if="(sourceNameSearched.source_type == 'link')"></i>
												<i class="fa pad-r10 c-pointer fa-file-word-o" ng-if="(sourceNameSearched.source_type == 'doc' || sourceNameSearched.source_type == 'docx')"></i>
												<i class="fa pad-r10 c-pointer fa-file-pdf-o" ng-if="(sourceNameSearched.source_type == 'pdf')"></i>
												<i class="fa pad-r10 c-pointer fa-file-image-o" ng-if="(sourceNameSearched.source_type == 'png')"></i>
												</span>
												{{sourceNameSearched.sourceName}}
							</li>
								</ul>
								<ul class="custom-list searchSource z-99 mxh-140 l-0 pad-y10  item-1"
										ng-if="showfiteredSourceList && dataPopObject.overideSearchedInputSourceEdit && fiteredSourceList && sourceUrlList ">
										<div class="pad-x10 d-flex ai-c">
											<button class="btn mar-autol sm-btns mar-r10 bordered-button" ng-click="closeSourceListPopUp()">Cancel</button>
											<button type="submit" value="Submit" class="btn grad-button sm-btns ng-scope"  ng-disabled="!dataPopObject.overideSearchedInputSourceEdit || !dataPopObject.overideSearchedInputURLEdit">Apply</button>
										</div>
								</ul>
								<%-- <ul class="custom-list z-99 mxh-140 l-0 pad-y10  item-1"
										ng-if="showAddNew && (fiteredSourceList && fiteredSourceList.length == 0)">
											<div class="width-95 mar-b0 mar-x10   p-rel d-block">	
												<div class="bst_input_group bst_input_group_r  height-a">						
													<p type="text" 	  ng-disabled="disableInput" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" http-prefix placeholder="sourceURL"  ng-value="overideSearchedInputURLEdit" ng-model="dataPopObject.overideSearchedInputURLEdit" name="sourceURL" class="custom-input text-cream mar-b0 pad-x15 pad-t15 f-12 height-100" >
													<span class="label">Source URL *</span>
												</div>
											</div>
										<li class="pad-x10 d-flex ai-c" ng-click="addSourceOveride()">Add new
										</li>
								</ul> --%>
									</div>
								</div>
								
									<div class="width-25 mar-b0  p-rel d-block">							
													<!-- <div class="input-group">
													<input type="text" class="pad-y10 pad-l10 pad-r0 custom-input" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
													<span class="input-group-btn">
															<button type="button" class="btn bg-transparent  border-0 text-cream btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
													</span>
													</div> -->
													<div class="bst_input_group bst_input_group_r  height-a">		
													<span class="input-group-prepend f-14 text-dark-cream fa fa-calendar"></span>
													<input type="text" 	autocomplete="off" ng-disabled="disableInput" placeholder="yyyy-mm-dd" ng-value="overideSearchedInputDateEdit" ng-model="dataPopObject.overideSearchedInputDateEdit" ng-change="dateValidate()" name="dateofbirth" class="custom-input text-cream f-12  pad-b10 pad-t15 " >

													<span class="label l-3 pad-x0 f-12 text-dark-cream">Published Date</span></div>
												  <span class="error" ng-if="dateError">Invalid format (yyyy-MM-dd)</span>													

									</div>
									<%-- <div class="width-10 jc-c d-flex ai-c"> --%>
										<%-- <i class = "fa text-dark-cream c-pointer fa-pencil" ng-if="!showEditMode" ng-click="editInputSource(true)"></i>
											<i class = "fa fa-check mar-r5 c-pointer text-dark-green" ng-if="showEditMode" ng-click="editInputSource(false)"></i>
											<i class = "fa fa-times c-pointer text-coral-red" ng-if="showEditMode" ng-click="editInputSource(false,'cancel')"></i> --%>

								<%-- </div> --%>
							</div>
							<div ng-if="userModifiedValue || showAddbutton" class="d-flex pad-b20 mar-x25 mar-t10 pad-t10 d-none ai-c bst_border_t" >
								<div class="bst_input_group bst_input_group_r pad-x15 pad-t15 height-a">						

									<textarea type="text" ng-disabled="disableInput" autocomplete="off" placeholder="Value" ng-model="textArea.userModifiedValue"  class="custom-scroll-wrapper width-100 custom-input text-cream pad-x15 pad-t15 f-12 height-100" ></textarea>

									<span class="label">value</span>
								</div>
							</div>
							</form>
					</div>
					
					
					<div class="modal-footer d-flex ai-c border-0">
					<%-- ng-disabled="dateError || !dataPopObject.overideSearchedInputSourceEdit || !overideSearchedInputSourceEditVal || !dataPopObject.overideSearchedInputURLEdit || !overideSearchedInputURLEdit"  --%>
							<button class="btn mar-autol bordered-button" ng-click="closeDataPopUp()">Cancel</button>
							<button class="btn grad-button" ng-if="!showSavebtnwithclass" ng-disabled="sourceUrlList" ng-click="overRide()" >Save</button>
							<button class="btn grad-button" ng-if="showSavebtnwithclass" ng-disabled="sourceUrlList" ng-click="overRide()" >Save</button>
					</div>
				
	</script>
	<script type="text/ng-template" id="confirmModal.html">
		<div class="modal-header border-0 pad-b0">
					<h3 ng-if="!removeCustom"  class="modal-title f-14 text-cream" id="modal-title">Deleting the entity may remove related entities as well from Screening Results, do you wish to continue?</h3>
					<h3 ng-if="removeCustom" class="modal-title f-14 text-cream" id="modal-title">Removing the customized view, will remove all related entities from Screening Results, do you wish to continue?</h3>
			</div>
			<div class="modal-footer border-0 pad-b0">
					<button class="bordered-button lh-18 f-14 height-a" type="button" ng-click= "dismissModal()">Cancel</button>
					<button class="grad-button mar-l10 f-14 lh-20 height-a" type="button"  ng-click= "closeWidgetCaptureModal()">OK</button>
			</div>
	</script>
	<script type="text/ng-template" id="changeSourceconfirmModal.html">
		<div class="modal-header border-0 pad-b0">
					<h3  class="modal-title f-14 text-cream" id="modal-title">Changing the source will populate the Screening Results from new source information, do you wish to continue?</h3>
			</div>
			<div class="modal-footer border-0 pad-b0">
					<button class="bordered-button lh-18 f-14 height-a" type="button" ng-click= "cancel()">Cancel</button>
					<button class="grad-button mar-l10 f-14 lh-20 height-a" type="button"  ng-click= "closeWidgetCaptureModal()">OK</button>
			</div>
	</script>

	<%-- // share holder evidence modal --%>
	<script type="text/ng-template" id="shareHolderEvidenceModal.html">

		<div class="modal-header pad-b10 mar-l0 d-flex ai-c">
				<h4 class="modal-title mar-autor  text-overflow">
				 
						<span>Shareholder Evidence </span> 
					   
				   </h4>
			<button type="button" class="f-24 bg-transparent border-0 text-cream" ng-click="closeShareOverviewModal()">×</button>
			
			
		</div>
		
			<div class="modal-body height-100 pad-t15">
			<div class="clearfix height-100">
				<div class="col-sm-3 right-panel height-100 pad-l0">
				<ul class="list-group custom-list-group height-95 left-side-lists">
					<li class="list-group-item evidence-list" ng-repeat = "(index,companies) in entityShareHolderListsData track by $index" ng-class="{'active': (index == defaultActiveIndex), 'pe-none' : (shareHOlderLoader)}" ng-click="getMultiSource(index,companies)">
					<p class="mar-b0 roboto-light d-flex ai-t">{{companies.name}}</p>
						
					</li>
				</ul>
				</div>
				<div class="height-95 pad-x0 col-sm-9">

						<div ng-hide="shareHOlderLoader" class="no-data-wrapper text-center" ng-if="multisourceDiscData.length == 0" >
								<p style="text-align:center;color:red">No Data Found</p>
						</div>
						<span  class="custom-spinner height-200px" ng-if="shareHOlderLoader">
								<i class="fa fa-spinner fa-spin fa-2x"></i>
						</span>
					<div class="panel-group overflow-a height-100 pad-r10 custom-scroll-wrapper bst_custom_accordian  evidence-accordian-wrapper" id="accordion3" role="tablist" aria-multiselectable="true">
						<uib-accordion  close-others="true">							
											
											<div uib-accordion-group class="right-panel panel-default" ng-hide="shareHOlderLoader" ng-if="multisourceDiscData.length > 0" ng-repeat = "value in multisourceDiscData track by $index">
													<uib-accordion-heading >
															<div class="uib-header-wrapper d-flex">
																 <div class="pad-l0 custom-media-wrapper">
																	<div class="media">
																		<div class="media-left c-pointer ">
																			<div class="entity-company-image">
																				<img class="img-responsive" ng-src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Walmart_logo.svg/2000px-Walmart_logo.svg.png" ng-show="entitySearchResult.list.topHeaderObject.logo_url || entitySearchResult.comapnyLogo " alt="fetcher_logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Walmart_logo.svg/2000px-Walmart_logo.svg.png">
																				<i class="fa fa-building-o ng-hide" ng-hide="entitySearchResult.list.topHeaderObject.logo_url ||  entitySearchResult.comapnyLogo"></i>
																			</div>
																		</div>
																		<div class="media-body">
																		    <div class="media-header-wrapper">
																			<%-- <i class="fa fa-star"> --%>
																			<h4 class="media-heading text-unset text-left width-30" style="text-align:left;">{{value['overviewSrc']['vcard:organization-name']}}</h4>
																			<p ng-if="value.isScreenShot" class="width-40 mar-b0 word-ba text-unset" style="text-align:left;">{{value.screenShot_sourceName}}</p>
																			
																			 <ul class="custom-list pad-l10 d-flex ai-c mar-autol" >
																					
																					<li ng-if="value.isScreenShot"><i class="icons f-16 icon-cloud-download" ng-click="downloadEvidence(value,$event)"></i></li>
																					<li ng-if="value.isScreenShot && !value.addedtopage && !value.addtopageloader" class="mar-x5 square-20"><img src="..\assets\img\file-plus-grey.svg" ng-click="addtoEvidencePage(value,$event)"></li>																					<%-- <li style="color:red"><i class="fa fa-trash " aria-hidden="true"></i></li> --%>
																					<li ng-if="value.isScreenShot && value.addedtopage" class="mar-l5"><i class="fa fa-check"></i></li>
																					<li class="custom-spinner mar-l15 p-rel t-10 l-0" ng-show="value.addtopageloader "><i class="fa f-16 fa-spinner fa-spin"></i></li>
																					<li ng-if="!value.loading && !value.isScreenShot" class="mar-x5" ng-click="openEditListShareHolder(value['overviewSrc'], value['select_entity'], $event)"><i class="fa f-16 fa-camera"></i>
																					</li>
																				
																																									
																					<li class="custom-spinner p-rel t-10 l-0" ng-show="value.loading "><i class="fa f-16 fa-spinner fa-spin"></i></li>
																				
																				<li id="link" ng-click="openWindowRegistryURI(value['overviewSrc']['bst:registryURI'], $event)">
																					<a target="_blank" class="mar-r5" href="{{value['overviewSrc']['bst:registryURI']}}">
																						<span><i class="fa f-16 fa-external-link "></i></span>
																					</a>
																				</li>
																			</ul>
																		</div>
																		<div class="d-flex width-100">
																			<p class="roboto-light text-unset mar-b0">{{value['overviewSrc']['bst:registryURI'] | sourceurlbase}}</p>
																		</div>
																		<div class="d-flex width-100">
																			<p class="roboto-light text-unset mar-b0">{{value['overviewSrc']['mdaas:RegisteredAddress'] ? (value['overviewSrc']['mdaas:RegisteredAddress'].fullAddress) : ''}}</p>
																		</div>
																	</div>
																	</div>
																</div>
														</div>
													</uib-accordion-heading>
													<div class="roboto-light" style="min-height:30px;">
														<p class="roboto-light mar-b0" >{{value['overviewSrc']['bst:description'] ? value['overviewSrc']['bst:description'] : ''}}</p>
													</div>
											</div>	
																				
													
						</uib-accordion>
					</div>
    			</div>
	</script>
	<%-- // complex Ownership modal --%>
	<%-- <div class="custom-spinner full-page-spinner height-200px" ng-if="complexStructureLoader">
		<i class="fa fa-spinner fa-spin fa-3x"></i>
	</div> --%>
	<script type="text/ng-template" id="complexOwnershipModal.html">
		<span ng-if="complexOwnershipToggle" class="modal-title f-14 text-cream">Complex Structure requires to get existence evidence for each Intermediate / Ultimate Legal Parents by showing search results for each entity, would you like to continue?</span>
		<span ng-if="!complexOwnershipToggle">Successfully marked as Non-Complex Structure</span>
		<div class="modal-footer border-0 pad-b0">
				<button ng-if="complexOwnershipToggle" class="bordered-button lh-18 f-14 height-a" ng-click="ownershipToggleTOIntial()" type="button" >Cancel</button>
				<button class="grad-button mar-l10 f-14 lh-20 height-a" type="button" ng-click="dataOverOwnerShipModal()">OK</button>
		</div>
	</script>

	<script type="text/ng-template" id="EntityShareHolderNoData.html">
		<span class="modal-title f-14 text-cream">No  Intermediate / Ultimate Legal Parents identified</span>
		<div class="modal-footer border-0 pad-b0">
			<button class="grad-button mar-l10 f-14 lh-20 height-a" type="button" ng-click="closeShareOverviewModal()" >OK</button>
		</div>
	</script>


	<script>
		$(document).ready(function () {
			/*--  Custom Scroll Bar  --*/
			$('.evidence-accordian-wrapper , .links-list-wrapper ul,.chat-bot-wrapper,.panel-scroll,.changed-content-wrapper, #flowChartViewDiv .orgdiagram.custom-scroll-wrapper')
				.mCustomScrollbar({
					axis: "y"
				});
		});
	</script>
	
	<script type="text/ng-template" id="customTemplate.html">
		<a>
			<img ng-src="{{match.model.flag}}" width="16">
			<span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
		</a>
	</script>

	<script type="text/ng-template" id="stickyLinks.html">
		<div class="width-100 p-rel d-block">
			<form class="form jc-sb pad-x0" ng-submit="applySourceEvidenceEntityClipBoard(form)" name="form">
				<div class="bst_input_group p-rel bst_input_group_r  height-a">
					<input class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10" ng-model="dataPopObjectEntityClipBoard.selectedSourceTitle"/>
					<span class="label f-12 text-dark-cream">Title</span>
				</div>
				<div class="bst_input_group bst_input_group_r mar-t10 p-rel height-a">
					<input 
						class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10"
						autocomplete="no-place"
						placeholder="source"
						ng-keyup="sourceSearchInputOverideEntityClipBoard(dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit)"
						ng-value="overideSearchedInputSourceEditValEntityClipBoard"
						ng-model="dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit"
						ng-disabled="disableInput"
						/>
					<span class="label f-12 text-dark-cream">Source</span>
					<ul class="custom-list searchSource z-99 mxh-140 l-0 pad-b35 pad-y10  item-1"
							ng-if="showfiteredSourceListEntityClipBoard && dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit && (fiteredSourceListEntityClipBoard && fiteredSourceListEntityClipBoard.length > 0) && sourceUrlListEntityClipBoard">
							<div class="width-95 mar-b0 mar-x10   p-rel d-block">	
								<div class="bst_input_group bst_input_group_r  height-a">						
									<input type="text" ng-disabled="disableInput" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" http-prefix placeholder="sourceURL"  ng-value="dataPopObjectEntityClipBoard.overideSearchedInputURLEdit" ng-model="dataPopObjectEntityClipBoard.overideSearchedInputURLEdit" name="sourceURL" class="custom-input text-cream mar-b0 pad-x15 pad-t15 f-12 height-100"/>
									<span class="label">Source URL *</span>
								</div>
							</div>
							
							<li class="pad-x10 f-14 h-30 d-flex ai-c" ng-repeat="sourceNameSearched in fiteredSourceListEntityClipBoard track by $index"
									ng-click="fillSourceSearchedInputOverideEntityClipBoard(sourceNameSearched)">
									<span>
									<i class="fa pad-r10 c-pointer fa-link" ng-if="(sourceNameSearched.source_type == 'link')"></i>
									<i class="fa pad-r10 c-pointer fa-file-word-o" ng-if="(sourceNameSearched.source_type == 'doc' || sourceNameSearched.source_type == 'docx')"></i>
									<i class="fa pad-r10 c-pointer fa-file-pdf-o" ng-if="(sourceNameSearched.source_type == 'pdf')"></i>
									<i class="fa pad-r10 c-pointer fa-file-image-o" ng-if="(sourceNameSearched.source_type == 'png')"></i>
									</span>
									{{sourceNameSearched.sourceName}}
							</li>
							<div class="pad-x10 width-100 d-flex p-abs bg-process-background b-0 ai-c pad-y5">
								<button class="btn mar-autol sm-btns mar-r10 bordered-button" ng-click="closeSourceListPopUp()">Cancel</button>
								<button type="submit" value="Submit" class="btn grad-button sm-btns ng-scope">Apply</button>
							</div>
					</ul>
				</div>
		</form>
	</div>
	</script>
</body>
<script>
	// angular.element('.editableDiv').triggerHandler('click',function(){
	// 	alert("working!");
	// });
// $(document).ready(function(){

// });
  </script>

		</html>
