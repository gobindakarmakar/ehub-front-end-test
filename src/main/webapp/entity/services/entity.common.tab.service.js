'use strict';
elementApp
    .factory('EntityCommonTabService', entityCommonTabService);
entityCommonTabService.$inject = [
    '$timeout',
    'worldCountryDetailList',
    'EntityGraphService',
    'EntityApiService',
    'chartsConst'
];
function entityCommonTabService($timeout,
    worldCountryDetailList,
    EntityGraphService,
    EntityApiService,
    chartsConst) {
    var commontabObject = {
        checkFetcherStatus: checkFetcherStatus,
        socialBubbleChart: socialBubbleChart,
        World: World,
        loadDataAndPlotGraphForEntity: loadDataAndPlotGraphForEntity,
        InitializeandPlotPie: InitializeandPlotPie,
        plotLineChart: plotLineChart,
        loadoverviewVLA: loadoverviewVLA,
        calculateCountryRisk: calculateCountryRisk,
        toTitleCase: toTitleCase,
        loadlineData:loadlineData,
        makeUrlSecureToWork:makeUrlSecureToWork,
        getSourceInfo:getSourceInfo,
        isJson:isJson,
        tonums:tonums,
        loadRiskpieChart:loadRiskpieChart,
        AppendOrgGraphIcon:AppendOrgGraphIcon,
        'lazyLoadingentity':{
            fetecherAPimade: [],
            threatrstabData: {},
            threatsTabFirsttime: true,
            leadershipTabFirsttime: true,
            financeTabFirsttime: true,
            riskTabFirsttime: true,
            newsTabFirsttime: true,
            socialMediaTabFirsttime: true,
            compliancetabFirsttime: true,
            fetcher_data: '',
            fetcherApiFirstTime: true,
            overviewTabFirsttime: true,
            mediaTabFirsttime: true,
            overviewFetchers: ['1016', '1013', '1006', '1008', '1021', '2001', '21', '1017', '1012'],
            complianceFetchers: ['1016'],
            leadershipFetchers: ['1016', '1006', '1008', '3', '1005', '26', '1012', '1021', '1009', '2001', '1004', '3001', '3002', '3003', '4001', '3008'],
            riskFetchers: ['1016', '20', '17', '9', '12', '10', '1001', '6', '14', '2003', '16', '11', '15', '1007', '2005', '2001', '1012'],
            newsTabFetchers: ['1016', '2001', '1011', '4001', '1003', '2004'],
            socialMediaFetchers: ['1016', '2001', '23', '3', '1008', '1017', '21'],
            mediaFetchers: ['1016', '1023']
        },
        'basicsharedObject': {
            companyName: '',
            totalOfficers_link: [],
            addModelClassification : {
                companyModel: [],
                personModel: [],
                companyListData: [],
                personListData: [],
                personRoleData: [],
                personRoleModel: [],
            },
            editModelClassification : {
                editPersonListData: [],
                editPersonModel: [],
                editCompanyModel: [],
                editCompanyListData: [],
                editRoleModel: [],
                editRoleData: []
            },
            screeningModelClassification :{
                screeningPersonRoleModel: [],
                screeningPersonRoleData: [],
                screeningPersonListData: [],
                screeningPersonModel: [],
                screeningCompanyListData: [],
                screeningCompanyModel: [],
            },
            subsidiaries :[{
                "name": '',
                "source_url": '',
                "subsidiaries": [],
                "natures_of_control": [],
                "parent_companies": []
            }],
            stockModalData : {
                top_owners: [],
                locations: [],
                top_mutual_holding: [],
                recent_institutional_activity: [],
                balance_sheet: {},
                total_assets: '',
                total_liabilities: '',
                long_term_debt: '',
                net_income: '',
                net_revenues: '',
                'net_cash_flow_-_operating_activities': '',
                'net_cash_flow_investing': '',
                'net_cash_flow_-_financing': '',
                income_statement: {},
                cash_flow_statement: {},
                stock_profile: {
                    growth_and_valuation: {},
                    today_trading: {},
                    company_profile: {},
                    competitors: [],
                    other_values: [],
                    financials: {}
                },
                company_profile: {
                    data: {},
                    shareholders: {}
                },
                'press_releases': [],
                'adverse_news': [],
                source: 'stocks.money.cnn.com'
            },
            yahooStockData : {
                company_profile: {
                    data: {},
                    keyStaff: []
                },
                stock_holders: {
                    direct_holders_list: []
                },
                financial_statements: {
                    financevalues: [],
                    financedates: []
                },
                stock_graphs: {
                    last_5year_stock_history: [],
                    last_6month_stock_history: [],
                    last_day_stock_history: [],
                    last_year_stock_history: [],
                    last_5d_stock_history: [],
                    last_month_stock_history: []
                },
                stock_statistics: {
                    dividends_Splits: {},
                    stock_price_history: {},
                    fiscal_year: {},
                    profitability: {},
                    balance_sheet: {},
                    management_effectiveness: {},
                    other_stock_values: {},
                    cash_flow_statement: {},
                    income_statement: {}
                },
                source: 'finance.yahoo.com'
            },
            tagCloudPopularOptions :{
                header: "MY ENTITIES",
                container: "#tagCloudPopularTags",
                height: 220,
                width: 300,
                data: [],
                margin: {
                    bottom: 10,
                    top: 10,
                    right: 10,
                    left: 10
                }
            },
            bloombergStockLineChartData :{
                'five year stock prices': [],
                'one year stock prices': [],
                'one month stock prices': [],
                'current day stock prices': []
            },
            list: {
                key_staff: [],
                'sex-offenders': [],
                'fbi_data': [],
                similar_companies: [],
                resolve_related_similar_companies: [],
                search_related_similar_companies: [],
                stocks_bloomberg_similar_companies: [],
                bloomberg_similar_companies: [],
                specialities: [],
                people_also_viewd: [],
                company_boardmembers: [],
                stock_company_boardmembers: [],
                company_key_executives: [],
                mixed_company_key_executives: [],
                stock_company_key_executives: [],
                other_board_members: [],
                corporate_governance_committee: [],
                nominating_committee: [],
                compensation_committee: [],
                audit_committee: [],
                industry: [],
                products: [],
                five_year_stock_prices: [],
                one_month_stock_prices: [],
                one_year_stock_prices: [],
                current_day_stock_prices: [],
                key_persons: [],
                stock_holders: [],
                recent_user_comments: [],
                stock_graphs: [],
                stock_statistics: [],
                financial_statements: [],
                news_articles: [],
                all_news_articles: [],
                following: [],
                followers: [],
                near_by_companies: [],
                has_returns: {},
                has_accounts: {},
                has_company_members: [],
                risidata: [],
                key_staff_attended_schools: [],
                key_relationships: [],
                supplierblacklist: {},
                badbbuyer_initial: true,
                scam: [],
                tradedAs: '',
                founders: '',
                services: '',
                key_people: '',
                exclusion_fraud: [],
                crimeList: {
                    interpol: []
                },
                badbuyerslist: {},
                hunterData: {},
                blackhatworld: [],
                latestReleases: [],
                xforceibmcloud: [],
                domain_servers: [],
                locations: [],
                cypherpunk: [],
                reddit: [],
                ratings_trends: [],
                current_job_openings: [],
                sanctionssearch: [],
                imsasllc: [],
                unlimitedcriminalchecks: [],
                truecaller: [],
                meetupData: [],
                highcountryRisk: 0,
                'url_analysis': [],
                'fan_counts_numbric': 0,
                'fan_counts': 0,
                'followers_numberic': 0,
                'twitter_following_numberic': 0,
                'social_media_count':0,
                'follower_following_count': [],
                'news_deep_web': [],
                'inc-us-eu': [],
                'companies-corporationwiki': [],
                'company_locations': [],
                'top_holders': [],
                'associated_companies': {
                    'memberships': [],
                    'owners': [],
                    'leadership & staff': [],
                    'services transactions': [],
                    'political_financial_supports': [],
                    'politicians': [],
                    'political_organizations': [],
                    'transaction': [],
                    'child_organizations': [],
                    'holdings': [],
                    'donation_grant_recipients': []
                },
                'associated_persons': {
                    'transaction': [],
                    'lobbying': [],
                    'hierarchy': [],
                    'donation': [],
                    'membership': [],
                    'position': [],
                    'ownership': [],
                },
                'connected_industries': [],
                'press_releases': [],
                'sanctionList': [],
                'adverse_news': [],
                'pep': [],
                'denied_personlist': [],
                'crimestoppers_uk': [],
                'answerstedhctbek': [],
                'related_persons_tags': true,
                'related_company_tags': true,
                'twitter_socket_network_chart': true,
                'social_followers_bubble_chart': true,
                'is_popular_tag_chart': true,
                'is_traffic_by_countries': true,
                'is_search_keywords': true,
                'is_traffic_by_domain': true,
                'is_badbuyerslist': true,
                tagCloudOrganizationNameList: [],
                'traffic_by_countries': [],
                'search_keywords': [],
                'local_ranking': [],
                'traffic_by_domain': [],
                videos_list: [],
                phoneno: [],
                topHeaderObject: {
                    FoundedDate: '',
                    industry: "",
                    hasURL: ""
                },
                isSignificant: false,//siginifiance false by default
                significantTitle: '',
                summaryCount: {
                    highRiskCount: 0,
                    financeCount: 0,
                    adverseCount: 0,
                    pepCount: 0,
                    sanctionCount: 0
                },
                addEntitymodalChart: 'person',
                addActiveTab: 0,
                recognized_entity: {
                    entityType: ""
                },
                graphviewIndex: 1,
                currentVLArendered: 'vlaContainer',
                orgChartSources: [],
                graphSourceSelected: '',
                intialSourceSelected: '',
                previousSourceSelected: '',
                sourceFirsttimeSelected: true,
                sourceFilterChart_not_Started: true,
                searchedSourceURLCompany: '',
                searchedSourceURLValuePerson: '',
                selectedScreeningRow: {},
                keymangerSourceselected: '',
                keymangerapplybutton: true
            },
            is_data_not_found: {
                is_data_leadership: true,
                is_stock_performance: true,
                is_traffic_by_country: true,
                is_traffic_key_word: true,
                is_traffic_bydomain: true,
                associated_company: true,
                board_of_director: true,
                is_committe: true,
                is_people_visited: true,
                is_nearby_company: true,
                is_company_member: true,
                is_topholders: true,
                is_balancesheet: true,
                is_incomeStatement: true,
                is_financialStatement: true,
                is_stockHistory: true,
                is_cyberalert: true,
                is_sanctionedlist: true,
                is_adversenews: true,
                is_indus_security_inc: true,
                is_latestnews: true,
                is_top_influencers: true,
                is_press_releases: true,
                is_recent_events: true,
                is_googlepost: true,
                is_linkedin_post: true,
                is_recentTweets: true,
                is_instagramlist: true,
                is_ratings_trends: true,
                is_facebook_list: true,
                is_twitter_list: true,
                is_carrer_opportuny: true,
                is_risk_ratio: true,
                is_politcal_donation: true,
                is_populartags: true,
                is_keyExecutive: true,
                is_networkgraph: true,
                is_interactionration: true,
                is_committe_audit: true,
                is_committe_compensation: true,
                is_committe_nominating: true,
                is_committe_corporate: true,
                is_shareholder: true,
                is_supplierlist: true,
                is_crimelist: true,
                is_sexoffender: true,
                is_deninedperson: true,
                is_exclusions: true,
                is_fbidata: true,
                is_investment: true,
                is_interlockdata: true,
                shareholder_div: false,
                stockprice_div: false,
                is_data_industry: true,
                is_bySource_pie: true,
                is_autodesk_Vla: true,
                is_autodesk_chart: false,
                is_rightScreening_data: true,
                is_rightadverseNews: true,
                is_complianceDetails: false
            },
            pageloader: {
                comapnyinformation: true,
                screeningLoader: true,
                directorsloader: true,
                coropoateloader: true,
                countriesloader: true,
                associatedDocumentloader: true,
                treeGraphloader: true,
                companyDetailsloader: true,
                loadingText: false,//loading flag is off by default
                disableFilters: false,//inital filters are clickable
                companyInfoReview: false,
                companyDocumentsReview: false,
                companyOperationReview: false,
                companyStructureReview: false,
                companyResultsReview: false,
                companyDetailsReview: false,
                chartFailureMessage: ''
            },
            bloombergChartDetailsOptions : {
                container: "#bloomberg-line-chart-details",
                height: '350',
                width: $("#bloomberg-line-chart-details").width(),
                uri: "../vendor/data/bloombergdata.json",
                data: {}
            },
            filteredChart : {
                company_eachlevel: [5, 10, 20, 30, 40, 50],
                numberofcomapnies: 5,
                numberOfLevel: [1, 5, 10, 15, 20, 30],
                selectedNumberofLevel: 5,
                risklevel: ["low", "high"],
                riskRatio: 1,
                applyButton: true,
                yearliderMinValue: 0,
                yearsliderMaxValue: 2,
                fromyear: ((new Date().getFullYear()) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + ("0" + (new Date().getDate())).slice(-2)),
                toyear: ((new Date().getFullYear() - 2) + '-' + (("0" + (new Date().getMonth() + 1)).slice(-2)) + '-' + ("0" + (new Date().getDate())).slice(-2)),
                startYear: 0,
                endYear: 2,
                show_subsidaries_filter: false,
                pep_slider_min: 85,
                pep_slider_max: 100,
                sanction_slider_min: 70,
                sanction_slider_max: 100,
                not_screening: true
            },
            newOfficer : {
                firstName: '',
                lastName: '',
                country: '',
                dob: '',
                ScreeeningModal: 'person',
                CompanyName: '',
                website: '',
                requiredComapnyname: false,
                requiredPersonname: false,
                dateOfBirth: ''
            },
              tagCloudPersonOptions : {
                header: "MY ENTITIES",
                container: "#tagcloudCompanyPerson",
                height: 200,
                width: 300,
                data: [],
                margin: {
                    bottom: 10,
                    top: 10,
                    right: 10,
                    left: 10
                },
                domain: {
                    x: 1,
                    y: 10000
                },
            },
            'riskScoreData' :[],
            entites_renderedIn_chart:[]
        },
        'entityChartSharedObject': {
            bubbleSocialOptions: {
                "container": "#socialBubbleChart",
                "height": 310,
                data: {
                    name: '',
                    children: []
                }
            },
            twitter_tag_words: [],
            tagCloudTwitterTagsOptions: {
                header: "MY ENTITIES",
                container: "#tagcloudCompanyTwitterTags",
                height: 200,
                width: ($("#tagcloudCompanyTwitterTags").width() ? $("#tagcloudCompanyTwitterTags").width() : 300),
                data: [],
                margin: {
                    bottom: 10,
                    top: 10,
                    right: 10,
                    left: 10
                }
            },
            'financeData': {},
            'officerData': {},
            'vlaDataArr': {},
            'dataWithDate': {},
            'tagCloudNameList': [],
            'comapnyInfo': {},
            'social_activity_locations':[],
            'locationList':[]
        },
        'tabsWorldMap': {
            worldChartTwitterLocationsOptions: {
                container: "#twitter-locations-chart",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                height: 200,
                width: 300,
                markers: {}
            },
            worldComplianceLocationsOptions: {
                container: "#companyexample5",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                // "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            },
            worldComplianceLocationsOptionsForReport: {
                container: "#companyexampleReport",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                // "proxy/api/cases/all"
                height: 700,
                markers: {}
            },
            worldComplianceLocationsOptionsAtBottom: {
                container: "#companyComplianceWorldMap",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                height: 300,
                width: 500,
                markers: {}
            },
            worldChartOptions1: {
                container: "#companyexample4",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                dontShowAmount: true,										// "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            },
            worldChartOptions2: {
                container: "#companyexample5",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                dontShowAmount: true,																// "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            },
            worldChartNewsLocationsOptions: {
                container: "#worldMapNewsSources",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                // "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            },
            worldChartTwitterLocationsOptions: {
                container: "#twitter-locations-chart",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                // "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            },
            digitalFootPrintChart: {
                container: "#digitalFootprint",
                uri1: "../vendor/data/worldCountries.json",// Url of data
                uri2: "../vendor/data/worldMapData.json",// Url of data
                dontShowAmount: true,																// "proxy/api/cases/all"
                height: 200,
                width: 300,
                markers: {}
            }
        }
    }
    return commontabObject;
    function checkFetcherStatus(fetchers) {
        var fetch = [];
        for (var i = 0; i < fetchers.length; i++) {
            var index = commontabObject.lazyLoadingentity.fetecherAPimade.indexOf(fetchers[i]);
            if (index === -1) {
                commontabObject.lazyLoadingentity.fetecherAPimade.push(fetchers[i]);
                fetch.push(fetchers[i]);
            }
        }
        return fetch;
    }
    function socialBubbleChart(bubbleOptions) {
        $timeout(function () {
            var exampleChart = new BubbleHierarchyChart(bubbleOptions);
        }, 10000);
    }
    function InitializeandPlotPie(data, id, colorsObj, colorsArr, isQuessionairePie, height, width) {
        data.map(function (d) {
            return d.value = d.doc_count;
        });
        var newData = [];
        var keys = [];
        angular.forEach(data, function (d, i) {
            if (d.value && (d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(), keys) == -1)) {
                newData.push(d);
                keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
            }
        });
        var maxval = d3.max(data, function (d) {
            return d.value;
        });
        var sum = d3.sum(data, function (d) {
            return d.value;
        });
        var colors;
        if (colorsArr) {
            colors = colorsArr;
        } else {
            colors = ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d", "#669900", "#334433"]
        }
        if (isQuessionairePie) {
            newData.sort(function (a, b) { return b.value - a.value; });
            var unevalVals = newData.filter(function (arr) {
                return arr.key == "unevaluated";
            });
            var evalCount;
            if (unevalVals && unevalVals[0] && unevalVals[0].value) {
                evalCount = sum - unevalVals[0].value;
            } else {
                evalCount = sum;
            }
            var options = {
                container: "#" + id,
                data: newData,
                height: height ? height : 160,
                width: width ? width : undefined,
                colors: colors,
                colorsObj: colorsObj,
                islegends: false,
                islegendleft: false,
                istxt: sum == 0 ? 0 : parseInt((evalCount / sum) * 100) + "%",
                // istxt: $scope.noOfQuestionsPerAnsPercentage+"%",
                txtColor: colorsObj[newData[0].key]
            };
        } else {
            var options = {
                container: "#" + id,
                data: newData,
                height: 160,
                colors: colors,
                colorsObj: colorsObj,
                islegends: true,
                islegendleft: true,
                legendwidth: 30,
                format: true,
                istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
                legendmargintop: 30
            };
        }
        setTimeout(function () {
            new reusablePie(options);
        });
    };
    function World(options, CountryOperations) {
        options.data = [worldCountryDetailList, []];
        options.countryOperations = CountryOperations ? CountryOperations : [];
        EntityGraphService.plotWorldLocationChart(options);
    }
    function loadDataAndPlotGraphForEntity(options, layout) {
        $('#' + options.id).html("");
        handleGraphData(options.data, options, layout)
        function handleGraphData(data, options, layout) {
            var finalData = {};
            var edges = [];
            var nodeIDS = [];
            var nodes = [];
            angular.forEach(data.vertices, function (val, i) {
                nodeIDS.push(val.id)
                val.weight = 10;
                nodes.push({
                    data: val
                });
            });
            angular.forEach(data.edges, function (val, i) {
                if ($.inArray(val.from, nodeIDS) != -1 && $.inArray(val.to, nodeIDS) != -1)
                    edges.push({
                        data: {
                            source: val.from,
                            target: val.to,
                            labelE: val.labelE,
                            id: val.id
                        }
                    });
            });
            finalData.nodes = nodes;
            finalData.edges = edges;
            loadEntityNetworkChart(finalData, options, commontabObject.basicsharedObject.companyName, layout);
        }
    }
    function plotLineChart(lineChartData, id, lineWidth, height, stockCurrency) {
        var lineData = [];
        if (id == '#lineChartCashFlow') {
            angular.forEach(lineChartData, function (v, k) {
                v.values.map(function (d) {
                    d.x = new Date(d.date);
                    d.value = d.sum;
                    return d.time = d.date;
                });
            });
            lineData = lineChartData;
        } else {
            lineChartData.map(function (d) {
                d.x = new Date(d.date);
                d.value = d.sum;
                return d.time = d.date;
            });
            lineData = [{
                "key": "",
                "values": lineChartData
            }];
        }
        var options = {
            container: id,
            height: height != undefined ? height : '',
            width: (lineWidth != undefined && lineWidth !== "") ? lineWidth : 270,
            axisX: true,
            axisY: true,
            gridy: true,
            tickformat: "year",
            data: lineData,
            marginRight: 20,
            gridcolor: "#2e424b",
            actualData: lineData[0].values,
            ystartsFromZero: true,
            currency: stockCurrency
        };
        new InitializeandPlotLine(options);
    }
    function loadoverviewVLA(id, data, vlaDataArr) {
        if ($("#" + id).find("canvas").length == 0) {
            $('#' + id)[0].style.display = "none";
            $('#' + id).siblings()[0].style.display = "";
            $('#' + id).siblings()[1].style.display = "none";
            $('#' + id).parent().siblings('a')[0].style.display = "none";
            var data = data;
            EntityApiService.getGraphData(data).then(function (response) {
                $('#' + id).html("");
                if (response !== void 0) {
                    // if (data.alias === "companyOverview") {
                    //     overviewCaseId = response.data.caseId;
                    // }
                    var caseId = response.data.caseId;
                    EntityApiService.getVLAdata(caseId).then(function (resp) {
                        if (resp != void 0) {
                            vlaDataArr[id] = resp.data;
                            var options = {
                                data: resp.data,
                                id: id
                            }
                            if (resp.data.vertices.length > 0) {
                                $('#' + id)[0].style.display = "";
                                $('#' + id).siblings()[0].style.display = "none";
                                $('#' + id).parent().siblings('a')[0].style.display = "";
                                loadDataAndPlotGraphForEntity(options);
                            } else {
                                $('#' + id).siblings()[1].style.display = "";
                                $('#' + id).siblings()[0].style.display = "none";
                            }
                        }
                    }, function () {
                        $('#' + id).siblings()[1].style.display = "";
                        $('#' + id).siblings()[0].style.display = "none";
                    });
                }
            }, function (error) {
                $('#' + id).siblings()[1].style.display = "";
                $('#' + id).siblings()[0].style.display = "none";
            });
        }
    }
    function calculateCountryRisk(country) {
        var captialCaseCountry = toTitleCase(country);
        var risk;
        if (captialCaseCountry) {
            //	d3.json("./constants/countryrisk.json", function (error, root) {
            risk = _.find(chartsConst.countryRisk, ['COUNTRY', captialCaseCountry.toUpperCase()]);
            if (risk) {
                return risk["FEC/ESR Risk Level"];
            } else {
                return '';
            }
        }
    }
    function toTitleCase(str) {
        if (str) {
            return str.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
    }
    function loadlineData(data) {
		var current_options = data;
		if (current_options && current_options.data && current_options.data.length > 0) {
			EntityGraphService.bloombergLineChart(current_options);
		}
    }
    /*
	 * @purpose:Show more data in model @created: 14 feb 2019 @params:
	 * data(url without http) @return: url @author: varsha
	 */
    function makeUrlSecureToWork(val) {
		if (val && val.split(':')[0] != 'https' && val.split(':')[0] != 'http') {
			val = 'https://' + val;
		}
		return val;
	}
   function isJson(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
    }
    function tonums(a) {
		if (typeof a == "string") {
			if (a == "0.00") return 0;
			if (a.split("m").length > 1) {
				if (is_numeric(parseFloat(a.split("m")[0]) * 1000000))
					return (parseFloat(a.split("m")[0]) * 1000000);
			}
			if (a.split("b").length > 1) {
				if (is_numeric(parseFloat(a.split("b")[0]) * 1000000000))
					return (parseFloat(a.split("b")[0]) * 1000000000)
			}
			if (is_numeric(parseFloat(a)))
				return parseFloat(a);
		}
		if (typeof a == "number") return a;
    }
    function getSourceInfo(obj) {
		var srcobj = {
			value: (obj && obj.value) ? obj.value : '',
			source: (obj && obj.source) ? obj.source : '',
			sourceUrl: (obj && obj.sourceUrl) ? obj.sourceUrl : '',
			sourceType: (obj && obj.sourceType) ? obj.sourceType : 'link',
			docId: (obj && obj.docId) ? obj.docId : ''
		}
		return srcobj;
    }
     function loadRiskpieChart(setTitle, options,cumulativeRisk) {
		var data = [
			{ name: "Risk Score", value: cumulativeRisk },
			{ name: " ", value: 100 - cumulativeRisk },
		];
		var text = Math.round(cumulativeRisk);
		var container = "#finalDiv";
		var width = parseInt($('.lastRect').attr('height')) ? $('.lastRect').attr('height') : 100;
		var height = parseInt($('.lastRect').attr('height')) ? $('.lastRect').attr('height') : 100;
		var ht;
		var thickness = 20;
		var duration = 750;
		var labeltxtY = 115;
		if (options.edge_labels.length > 4) {
			labeltxtY = 95;
			width = width * 1.5;
			height = height * 1.5;
			ht = $('.lastRect').attr('height') ? $('.lastRect').attr('height') / 2 : 15;
		}
		var radius = Math.min(width, height) / 2;
		var color = d3.scaleOrdinal(d3.schemeCategory10);
		var colorObj = { " ": "#566a71", "Risk Score": "rgb(66, 148, 217)" };
		if (options.isReport) {
			$('#finalDiv').attr('width', 100);
			$('#finalDiv').attr('height', 120);
		} else {
			$('#finalDiv').attr('width', width);
			$('#finalDiv').attr('height', height);
		}
		$('#finalDiv').empty();
		var svg = d3.select('#finalDiv').attr('height', 120).attr('width', 120)
			.append('svg')
			.attr("class", "sankPie")
			.attr('width', width)
			.attr('height', parseFloat(height) + 50);
		//    var temp_width = $('.lastRect').attr('height') ? $('.lastRect').attr('height')/2 : 20;
		//    $('#finalDiv').attr('transform', 'translate(' + (width / 2) + ',' + (-parseInt(temp_width)) + ')');
		$('#finalDiv').attr('transform', 'translate(' + (20) + ',' + (0) + ')');
		var g = svg.append('g')
			.attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');
		var arc = d3.arc()
			.innerRadius(radius - thickness)
			.outerRadius(radius);
		var pie = d3.pie()
			.value(function (d) { return d.value; })
			.sort(null);
		var path = g.selectAll('path')
			.data(pie(data))
			.enter()
			.append("g")
			.append('path')
			.attr('d', arc)
			.attr('fill', function (d, i) {
				return colorObj[d.data.name]
			})
			.each(function (d, i) { this._current = i; });
		g.append('text')
			.attr('text-anchor', 'middle')
			.attr('dy', '.35em')
			.attr('font-size', '14px')
			.attr("fill", "#ccc")
			.text(text);
		//    if(setTitle){
		svg.append("text")
			.attr('x', 40)
			.attr('y', labeltxtY)
			.attr('width', 50)
			.attr('height', 80)
			.attr("text-anchor", "middle")
			.attr("stroke", 'none')
			.attr("font-weight", 'normal')
			.attr("fill", '#9B9FA2')
			.style("font-size", '12px')
			.text('RISK SCORE');
		//    }
		if (options.isReport) {
			setTimeout(() => {
				var mainSvgwidth = parseInt($("#reportchartContentDiv").find("svg.mainSvg").attr("width")) + 100;
				$("#reportchartContentDiv").find("svg.mainSvg").attr("width", mainSvgwidth);
			}, 100);
		}
    }
    function AppendOrgGraphIcon(divId){
        $("#" + divId).find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-university").remove();
        $("#" + divId).find(".orgdiagram").find('.orgChartParentEntity').parent().find("i.fa-user").remove();
        $("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-university").remove();
        $("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().find("i.fa-user").remove();
        $("#" + divId).find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-university").remove();
        $("#" + divId).find(".orgdiagram").find(".orgChartsubEntity").parent().find("i.fa-user").remove();
        $("#" + divId).find(".orgdiagram").find(".orgChartParentEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#4c9d20"></i>');
        $("#" + divId).find(".orgdiagram").find(".orgChartParentEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#4c9d20"></i>');
        $("#" + divId).find(".orgdiagram").find(".orgChartmainEntity").parent().parent().css({
        'background-color': 'none'
        });
        $($("#" + divId).find(".orgdiagram").find(".orgChartmainEntity.comapny_icon").parent().prepend('<i class="fa fa-university"></i>')).css({
        'background-color': 'none'
        });
        $($("#" + divId).find(".orgdiagram").find(".orgChartmainEntity.person_icon").parent().prepend('<i class="fa fa-user"></i>')).css({
        'background-color': 'none'
        });
        $("#" + divId).find(".orgdiagram").find(".orgChartsubEntity.comapny_icon").parent().prepend('<i class="fa fa-university" style="color:#c7990c"></i>');
        $("#" + divId).find(".orgdiagram").find(".orgChartsubEntity.person_icon").parent().prepend('<i class="fa fa-user" style="color:#c7990c"></i>');
        $("#" + divId).find(".orgdiagram").find(".orgChartsubEntity").prepend('<i class="fa fa-user" style="color:#c7990c"></i>');
    }
}