'use strict';
elementApp
.service('EntityGraphService', entityGraphService);

entityGraphService.$inject = [
	'$q',
	'$timeout',
	'$state'
];

function entityGraphService(
		$q,
		$timeout,
		$state){
	
	this.bloombergLineChart  = function(options) {
		     if(options) {
		     options.container=options.container?options.container:"body"
		     options.width=options.width?options.width:$(options.container).width()-10
		     options.height=options.height?options.height:300
		     options.marginTop=options.marginTop?options.marginTop:15
		     options.marginBottom=options.marginBottom?options.marginBottom:30
		     options.marginRight=options.marginRight?options.marginRight:70
		     options.marginLeft=options.marginLeft?options.marginLeft:15
		     options.xParam=options.xParam?options.xParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
		     options.yParam=options.yParam?options.yParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
		     options.gridx=options.gridx?options.gridx:false
		     options.gridy=options.gridy?options.gridy:false
		     options.axisX=options.axisX?options.axisX:false
		     options.axisY=options.axisY?options.axisY:false
    		 options.y_axis_title = options.y_axis_title;
		     options.ChangeRadius=options.ChangeRadius?options.ChangeRadius:'false'
		  }   
		     
		  if(options.dateFormat){
			  options.rectWidthValue = '100px';
		  } else {
			  options.dateFormat ='DD/MM/YYYY';
			  options.rectWidthValue = '135px';
		  }



		 var data = [];

	//	 var tool_tip = $('body').append('<div class="line_Chart_tooltip" style="position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;background-color:#FB8B1E;padding: 10px;border-radius: 5px;border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		 var div = d3.select("body").append("div").attr("class", "line_Chart_tooltip").style("position", "absolute").style("z-index", 1000).style('visibility','visible').style("background","rgb(27, 39, 53)").style("padding","5px 10px").style("border-radius","10px").style("font-size","10px").style("display","none");
			
/*		 // basic SVG setup
*/		 var margin = {top:  options.marginTop, right:  options.marginRight, bottom: options.marginBottom, left:  options.marginLeft},
		     width =   options.width - margin.left - margin.right,
		     height =  options.height - margin.top - margin.bottom;


		 var svg = d3.select(options.container).append("svg")
		         .attr("width", width + margin.left + margin.right)
		         .attr("height", height + margin.top + margin.bottom)
		         .append("g")
		         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



/*		 // setup scales - the domain is specified inside of the function called when we load the data
*/		 var xScale = d3.scaleTime().rangeRound([0, width]);
		 var yScale = d3.scaleLinear().rangeRound([height, 0]);

		

/*		 // set the line attributes
*/		 var line = d3.line()
		         .x(function (d) {
		             return xScale(d.year); })  
		         .y(function (d) {
		             return yScale(d.value); }); 

		 var area = d3.area()
		         .x(function (d) {
		             return xScale(d.year); })
		         .y1(function (d) {
		             return yScale(d.value); }); 

		 var lineSvg = svg.append("g");                             
		     


/*		 // gridlines function in y axis function
*/		 function make_y_gridlines() {		
		     return d3.axisLeft(yScale)
		         .ticks(5)
		 };
		 function make_x_gridlines() {   
		     return d3.axisBottom(yScale)
		         .ticks(5)
		 };

		var bisectDate = d3.bisector(function(d) { return d.year; }).left ;
		function sortByDateAscending(a, b) {
		    return a.year - b.year;
		}

//		$.each(options.data,function(i,d){
			options.data.sort(sortByDateAscending);
//		})
		
		data = options.data;

		     data.forEach(function(d) {
		    	 d.year = (d.year);
		       d.value = +d.value;
		   });


/*		     // adding domain range to the x-axis 
*/		     xScale.domain(d3.extent(data, function(d) { return d.year; }));
/*		     // add domain ranges to the x and y scales
*/		     yScale.domain([
		         d3.min(data, function (c) {
		             return c.value ; }),        
		         d3.max(data, function (c) {
		             return c.value ; })
		         ]);
		         area.y0(height);


		    
/*		       // add the Y gridlines
*/		   svg.append("g")     
		       .attr("class", "bs_linegrid")
		       .attr("fill","white")
		       .call(make_y_gridlines()
		           .tickSize(-width)
		           .tickFormat("")
		       )

svg.select(".bs_linegrid").select("path").style("display","none");
/*		   // add the x axis
*/		    var x_g =  svg.append("g")
		             .attr("class", "x axis x_axis")
		             .attr("transform", "translate(0," + height + ")")
		             .call(d3.axisBottom(xScale).ticks(5)).style("stroke","#7B7D80").style("font-family","Roboto Light");
x_g.select("path").style("stroke","#7b7d80");
/*		     // add the y axis
*/		     var y_svg = svg.append("g")
		             .attr("class", "y axis y_axis")
		             .attr("transform", "translate("+width+",0)")
		             .call(d3.axisRight(yScale).ticks(5)).style("stroke","#7B7D80").style("font-family","Roboto Light");
			y_svg.select("path").style("stroke","#7b7d80");
			y_svg.append("text")
	           .attr("transform", "rotate(-90)")
	         .attr("y", 0 + (margin.left+25))
	          .attr("x",0 - (height / 2))
	            .attr("dx", ".71em")
	            .style("text-anchor", "middle")
	            .style("fill", "#6c7e88")
	            .text(options.y_axis_title).style("font-size", "14px").style("font-family","Roboto Light");;
/*		  // add the area
*/		     svg.append("path")
		        .attr("fill",'#356d96')
		        .attr("class", "area")
		        .attr("d", area(data));

/*		   // add the valueline path.
*/		     svg.append("path")
		       .data([data])
		       .attr("class", "line")
		       .attr("d", line).attr("fill",'none').attr('stroke','#356d96').attr('stroke-width','3px');
		      
		    


		      var focus = svg.append("g")                  
		     .style("display", "none");

 		     var rect= svg.append("rect").attr('class','tool')                                    
		         .attr("width", width)                              
		         .attr("height", height)                            
		         .style("fill", "none")                           
		         .style("pointer-events", "all")                    
		         .on("mouseover", function() { focus.style("display",'block'); })
		         .on("mouseout", function() { 
		        	 div.style("display","none")
		        	 focus.style("display", "none"); })
		         .on("mousemove", mousemove);                       

 		    function mousemove() {
 		    	var p=$(options.container)
 	        	var position=p.offset();
 	         	var elmen=document.getElementsByClassName('line_Chart_tooltip')
 	         	for(i=0;i<elmen.length;i++){
 	            var tooltipWidth=elmen[i].clientWidth
 	         	}
 	            
 	        	var cursor=d3.event.x;
 	         	if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)&&cursor>150){
 	         		for(i=0;i<elmen.length;i++){
 	         			elmen[i].classList.remove("tooltip-left");
 	            		elmen[i].classList.add("tooltip-right");
 	         		}
 	        		if(tooltipWidth>0){
 	        			 div.style("left", d3.event.pageX -tooltipWidth + "px");
 	        		}else{
 	        			 div.style("left", d3.event.pageX -(width/2)+ 100 + "px");
 	        		}
 			   	    div.style("top", d3.event.pageY - 25 + "px");  
 	         	}else{
 	          		for(i=0;i<elmen.length;i++){
 	         		elmen[i].classList.remove("tooltip-right");
 	         		elmen[i].classList.add("tooltip-left");
 	         		}
 	      		  div.style("left", d3.event.pageX + 10 + "px");
 	              div.style("top", d3.event.pageY - 25 + "px");
 	      
 	        	}
 	         var x0 = xScale.invert(d3.mouse(this)[0]),              
		             i = bisectDate(data, x0, 1),                   
		             d0 = data[i - 1],                             
		             d1 = data[i];     
 		      if(d0 && d1){
 		    	  div.style("display","inline-block")
 		          var   d = x0 - d0.year > d1.year - x0 ? d1 : d0;   
				  var myDate = new Date(d.year);
				  if(d.currency){
					div.html('<span>Date:</span>'+d.year+'  '+' <br> '+ '<span>Value:</span>'+d.value+' <br> '+ '<span>currency:</span>'+d.currency);
				  }else
 		          div.html('<span>Date:</span>'+moment(d.year).format(options.dateFormat)+'  '+' <br> '+ '<span>Value:</span>'+d.value);
 		          focus.attr("transform", "translate(" +  xScale(d.year) + "," +  yScale(d.value) + ")");
 	//focus.select("text").text(function() {return
 	//(d.risk?d.risk:d.values[0].risk)+" : "+d.value; });
	 		      focus.select(".x-hover-line").attr("y2", height - yScale(d.value));
	 		      focus.select(".y-hover-line").attr("x2", width + width);
 		      }
 		    }
//		     
//		 function mousemove() {
//		         var x0 = xScale.invert(d3.mouse(this)[0]),              
//		             i = bisectDate(data, x0, 1),                   
//		             d0 = data[i - 1],                             
//		             d1 = data[i];     
//		         console.log(data)
//		           if(data.length>0){
//		          var   d = x0 - d0.year > d1.year - x0 ? d1 : d0;   
//		              var myDate = new Date(d.year);
//		           }
//		       focus.selectAll(".x").remove() ;       
//		       focus.append("line")
//		         .attr("class", "x")
//		         .style("stroke", "#5b95bf")
//		         .style("opacity", 1)
//		         .attr("y1", 0)
//		         .attr("y2", height); 
//		     
//
//		    focus.selectAll(".y0").remove()   
//		     focus.append("circle")
//		         .attr("class", "y0")
//		         .style("fill", "#5b95bf")
//		         .style("stroke", "#5b95bf")
//		         .attr("r", 6);
//
//		   focus.selectAll(".y1").remove()   
//		   focus.append("rect")
//		    .attr("class", "y1")
//		    .attr("width", options.rectWidthValue).style('fill','#356d96')
//		    .attr("height", "18px") .attr("transform",
//		             "translate(" + (xScale(d.year)-50) + "," +
//		                            (height+5)+ ")");;
//		   focus.append("text")
//		     .attr("class", "y1").style("font-weight","normal").style("font-family","Roboto Regular")
//		     .style("font-size","12px")
//		     .style("stroke", "#b1c6cf")
//		     .attr("dx","12px")
//		     .attr("dy", "10px");
//
//		 focus.select("circle.y0")
//		       .attr("transform",
//		             "translate(" + xScale(d.year) + "," +
//		                            yScale(d.value) + ")");
//		                            
//		   
//		 focus.select("text.y1").style("font-weight","normal")
//		   .attr("transform",
//		             "translate(" + (xScale(d.year)-55) + "," +
//		                            (height+7) + ")")
//		                           .text(moment(d.year).format(options.dateFormat)+'  '+' | '+ '  '+d.value);
//
//		 focus.select(".x")
//		   .attr("transform",
//		               "translate(" + xScale(d.year) + ",0)")                       
//		                  .attr("y2", height);
//
//		           }   
	};
     /*
	  * @purpose: getting entity data by name
	  * @created: 09 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	 */
	function getLatLngByName(name){
		var deferred = $q.defer();
		var geocoder = new google.maps.Geocoder();
		var address = name, latitude, longitude;

		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			    latitude = results[0].geometry.location.lat();
			    longitude = results[0].geometry.location.lng();
				deferred.resolve({lat: latitude, lng: longitude});
		    } else {
		    	deferred.reject('error');
		    }
		}); 
		
		return deferred.promise;
	}
	/*
	  * @purpose: getting entity data by name
	  * @created: 09 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	  */
	this.plotWorldLocationChart = function(barOptions){
		 //code for number formatting
	    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
	        var n = this,
	            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
	            decSeparator = decSeparator == undefined ? "." : decSeparator,
	            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
	            sign = n < 0 ? "-" : "",
	            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
	            j = (j = i.length) > 3 ? j % 3 : 0;
	        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
	    };
		var default_map_fill_color="#4E74DF";
		    if (barOptions.container) {
		        $(barOptions.container).empty();
		    }
/*		    //--------------------------Initialize Values-----------------------------
*/		    if (barOptions) {
		        this.container = barOptions.container ? barOptions.container : "body"
		        this.barColor = barOptions.barColor ? barOptions.barColor : "blue"
		        this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false
		        this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
		        this.data = (barOptions.data) ? barOptions.data : []
		        this.showTicks = barOptions.showTicks ? barOptions.showTicks : true;
		        this.ticks = barOptions.ticks ? barOptions.ticks : 'all';
		        this.showLegends = (barOptions.showLegends !== undefined) ? barOptions.showLegends : false;
		        this.xLabelRotation = barOptions.xLabelRotation ? barOptions.xLabelRotation : 0;
		        this.yLabelRotation = barOptions.yLabelRotation ? barOptions.yLabelRotation : 0;
		        this.margin = barOptions.margin ? {
		            top: barOptions.margin.top ? barOptions.margin.top : 20,
		            right: barOptions.margin.right ? barOptions.margin.right : 20,
		            bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
		            left: barOptions.margin.left ? barOptions.margin.left : 40
		        } : {top: 0, right: 5, bottom: 0, left: 5};
		        this.height = barOptions.height ? barOptions.height : 600;
		        this.width = barOptions.width ? barOptions.width : $(this.container).width() - 10;
		        this.showAxisX = (barOptions.showAxisX !== undefined) ? barOptions.showAxisX : true;
		        this.showAxisY = (barOptions.showAxisY !== undefined) ? barOptions.showAxisY : true;
		        this.showXaxisTicks = barOptions.showXaxisTicks !== undefined ? barOptions.showXaxisTicks : true;
		        this.showYaxisTicks = barOptions.showYaxisTicks !== undefined ? barOptions.showYaxisTicks : true;
		        this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
		        this.randomIdString = Math.floor(Math.random() * 10000000000);
				barOptions.ChangeRadius=barOptions.ChangeRadius?barOptions.ChangeRadius:'false'



			} else {
		        return false;
		    }
		    var randomSubstring = this.randomIdString;

		    var margin = this.margin,
		            width = this.width - margin.left - margin.right,
		            height = this.height - margin.top - margin.bottom,
		            barColor = this.barColor;
		    if(height>width){
		        height = width;
		        this.height = this.width;
		    }
/*		    //define tool tip

*/		    $(".Bubble_Chart_tooltip").remove();
		   // var tool_tip = $('body').append('<div class="world_map_tooltip1" style="padding:10px;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none; z-index: 1000"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		    var tool_tip = $('body').append('<div class="Bubble_Chart_tooltip" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

		    var colorScale = d3.scaleThreshold()
		            .domain([10000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000, 500000000, 1500000000])
		            .range(["rgb(247,251,255)", "rgb(222,235,247)", "rgb(198,219,239)", "rgb(158,202,225)", "rgb(107,174,214)", "rgb(66,146,198)", "rgb(33,113,181)", "rgb(8,81,156)", "rgb(8,48,107)", "rgb(3,19,43)"]);
			
			var colorScaleForReport = d3.scaleOrdinal()
		            .domain(['UHRC','High','Medium','Low'])
					.range(["#ef5350","#ef5350","#f7a248","#b3b2d1"]);
					
			var colorScaleForCompliance = d3.scaleOrdinal()
					.domain(['UHRC','High','Medium','Low'])
					.range(["#a84778","#a84778","#f8ce00","#839cab"]);


		    var centered;
            var chart_container=this.container;
		    var worlddata = this.data[0];
		    var populationData = this.data[1];
		    var path = d3.geoPath();
		    var svg = d3.select(this.container)
		            .append("svg")
		            .attr('height', this.height)
		            .attr('width', this.width)
		            .attr('id', 'mainSvg-' + randomSubstring)
		            .append('g')
		            .attr('class', 'map');
		    var mapScale=Math.floor(height/4);
			if(barOptions.container == '#companyexampleReport'){
				mapScale=Math.floor(height/5.9);
			}
			if(barOptions.container == '#companyComplianceWorldMap'){
				mapScale=Math.floor(height/3.6);
			}
			var projection = d3.geoEquirectangular().scale(mapScale).translate([width / 2, height / 2]);
		    var path = d3.geoPath().projection(projection);

		    var WorldData = this.data;
		    if (WorldData) {
		        if (WorldData.length)
		            drawWorld(worlddata, populationData);
		    } else {
		    	
		    }
		    function drawWorld(data, population) {
		    	
		        var populationById = {};

		        population.forEach(function (d) {
		            populationById[d.id] = +d.population;
		        });

/*		        //TODO: Need to add Pins
		        //TODO: Need to add onClick behavior for pins to show Information
*/		        var svg_g = svg.append("g")
						.attr("class", "countries");
						
		                svg_g.selectAll("path")
		                .data(data.features)
		                .enter()
		                .append("path")
						.attr("d", path)
						.style("fill", function (d) {
		                    if(populationById[d.id]){
		                        return "#435DBE";
		                    }else{
								if(barOptions.container == '#companyexample5' || barOptions.container == '#companyexampleReport' || barOptions.container == '#companyComplianceWorldMap'){
									var country_index = findIndexForEachCountryColor(d, barOptions.countryOperations);
									if(country_index >= 0){
										if(barOptions.container == '#companyexampleReport'){
											return colorScaleForReport(barOptions.countryOperations[country_index].Risk || barOptions.countryOperations[country_index].CountryRisk);
										}
										else{
											return colorScaleForCompliance(barOptions.countryOperations[country_index].Risk || barOptions.countryOperations[country_index].CountryRisk);
										}
									}else{
										if(barOptions.container == '#companyComplianceWorldMap'){
											return '#444444';
										}
										else{
											return '#ced5dd';
										}
									}
								} else{
									return "#44555D";
								}								
		                    }
		                    return colorScale(populationById[d.id]);
		                })
		                .style('stroke', 'none')
		                .style('stroke-width', 0.5)
						.style("opacity", 0.8);  
					
					if(barOptions.container == '#companyexampleReport' || barOptions.container == '#companyComplianceWorldMap'){
						svg_g.selectAll(".countryNames").data(barOptions.countryOperations)
								.enter().append("text")
							.attr("transform", function(v) {
								var country_index = findIndexForEachCountryName(v, data.features);
								if(country_index >= 0){
									var proj = projection([
										data.features[country_index].properties.longitude,
										data.features[country_index].properties.latitude
									]);
									return "translate(" + proj + ")";
								}
								})
							.style("fill", function(){
								if(barOptions.container == '#companyexampleReport'){return "#2d2d2d";}
								else{return "#fff";}
							})
							.style("font-family",function(){
								if(barOptions.container == '#companyexampleReport'){return "opensans-regular";}
								else{return "'Roboto Regular',sans-serif";}
							})
							.style("font-size","8px")
							.attr("text-anchor", "middle")
							.text(function(v,i) {
								var country_index = findIndexForEachCountryName(v, data.features);
								if(country_index >= 0){
									return (v.jurisdiction ? v.jurisdiction : data.features[country_index].id.slice(0,2));
								}
						});  
					}
				
            		svg_g.selectAll(".mark")
				    	.data(barOptions.markers);
					var geocoder = new google.maps.Geocoder();
            	angular.forEach(barOptions.markers, function(markerV, markerK){  
            		if(markerV.lat !== '' && markerV.lat && markerV.long !== '' && markerV.long){
            			 addLocation(markerV);
            		}else{
            		 setTimeout(function(){
            			 addLocation(markerV);
            		 },1000*markerK);
            		}
		    	});
            	function addLocation(markerV){
            		if(markerV.lat !== '' && markerV.lat && markerV.long !== '' && markerV.long){
            			addCircle(markerV,[markerV.long,markerV.lat]);
            		}else{
	            		var  latitude, longitude;
	            		geocoder.geocode( { 'address': markerV.name}, function(results, status) {
	            			if (status == google.maps.GeocoderStatus.OK) {
	            				markerV.lat  = results[0].geometry.location.lat();
	            				markerV.long = results[0].geometry.location.lng();            				
	            		    
				    	
				    		var projectedValue = projection([markerV.long,markerV.lat])
				    		
			        		addCircle(markerV,projectedValue);
	            		}
						});
            		}
            		function addCircle(markerV,projectedValue){
            			  var domainX = barOptions['domain'] ? barOptions['domain']['x'] : 10;
            			  var domainY = barOptions['domain'] ? barOptions['domain']['y'] : 5000;
            			 var sizeScale = d3.scaleLinear().domain([domainX,domainY]).range([5,15]);
            	            
					if(barOptions.container != '#companyexampleReport'){
            		var circle=	svg_g.append("circle")
//					    .attr('class','mark')
        				 .attr('class', function(d){
        					 if(markerV.isIncident == true){
        						 return 'mark c-pointer';
        					 }else{
        						 return 'mark';
        					 }
        				 })

					     .on("contextmenu",function(){
			    	        	if(location.hash =="#!/alertDashboard"){
			    	        		alert();
			    	                 window.transSelectedFilter.data = markerV
			    	           		 window.transSelectedFilter.id = chart_container;
			    	           		 window.transSelectedFilter.chartType ="world";
			    	           	 }
			    		     })
					      .on("mouseover", function (d) {
					    	 
//		                    $(this).css("opacity", 1);
		                    if(markerV.txt){
				    			 $(".Bubble_Chart_tooltip").html('<span>' + markerV.txt + '</span> ' );	
				    		}
		                    else if(markerV.isIncident == true){
		                    	$(".Bubble_Chart_tooltip").html('<span>' + markerV.name + '</span> '+'<br>'+'<span>Number of incidents : ' + nFormatter(markerV.incidentsCount,2) + '</span>');
							}
							else if(barOptions.dontShowAmount){
		                    	$(".Bubble_Chart_tooltip").html('<span>Location : ' + markerV.name + '</span>');
							}
		                    else if(barOptions.type="byamount"){
		                    	$(".Bubble_Chart_tooltip").html('<span>Name : ' + markerV.name + '</span> '+'<br>'+'<span>Amount : $' + Number(markerV.amount).formatAmt() + '</span>');
					    		
		                    }
		                    else{
				    		   $(".Bubble_Chart_tooltip").html('<span>Name : ' + markerV.name + '</span> '+'<br>'+'<span>Amount : ' + markerV.amount + '</span>');
				    		}
		                     return $(".Bubble_Chart_tooltip").css("display", "block");
				         })
				         .on('click', function(){
				        	 if(chart_container === "#threatWorldMap"){
				        		 window.pushFilters(markerV.locationCode, true, 'location', markerV.name, 'locationMultiple', 'locationMultipleEnd');
				        		 $(".Bubble_Chart_tooltip").css("display", "none");
				        	 }
				         })
		                .on("mousemove", function () {
		                	var p=$(chart_container)
		                	var position=p.offset();
		                	var windowWidth=window.innerWidth;
		                	var tooltipWidth=$(".Bubble_Chart_tooltip").width()+50
		                	var cursor=d3.event.x;
		                 	if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)){
		                		var element = document.getElementsByClassName("Bubble_Chart_tooltip");
		                		element[0].classList.remove("tooltip-left");
		            			element[0].classList.add("tooltip-right");
			               		$(".Bubble_Chart_tooltip").css("left",(d3.event.pageX - 15-$(".Bubble_Chart_tooltip").width()) + "px");
			             	}else{
								var element =document.getElementsByClassName("Bubble_Chart_tooltip");
								if (element && element.length > 0) {
									element[0].classList.remove("tooltip-right");
									element[0].classList.add("tooltip-left");
									$(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
								}
		                	}
		                    return $(".Bubble_Chart_tooltip").css("top",d3.event.pageY + "px")
		   	
		                	
		                  //   $(".Bubble_Chart_tooltip").css("top", (d3.event.pageY - 10) + "px")
		                    //return  $(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
		                })
		                .on("mouseout", function () {
//		                    $(this).css("opacity", 0.4);
		                    //hide tool-tip
		                    return $(".Bubble_Chart_tooltip").css("display", "none");
		                })
		                

					   .attr("fill",function(d){
						   if(markerV.fillColor){
							   return markerV.fillColor;
						   }
						   if(barOptions.fillColor){
							   return barOptions.fillColor;
						   }else{
							   return default_map_fill_color;
						   }
					   })
					   .style("opacity",barOptions.opacity?barOptions.opacity:1)
					   .attr("transform", function(d) {return "translate(" + projection([markerV.long,markerV.lat]) + ")";});
            		      		if(barOptions.ChangeRadius=='true'){
            	         			circle.attr('r', function(d){
			         					  return sizeScale(parseInt(markerV.amount))
										})
			            		}else{
			            			circle.attr('r',4)
			            		}
						}
            		}
            	}
		        svg_g.append("path")
		                .datum(topojson.mesh(data.features, function (a, b) {
		                    return a.id !== b.id;
		                }))
		                .attr("class", "names")
		                .attr("d", path);
		        
		var zoom = d3.zoom().on("zoom",function() {
			svg_g.attr("transform",d3.event.transform)
			svg_g.selectAll("path")  
		            .attr("d", path.projection(projection)); 
		});
		svg_g.call(zoom);
	    }
	};
}

/**
 * Finding index for each country color for report world map
 */
function findIndexForEachCountryColor(d, options) {
	var index = _.findIndex(options, function (v) {
		var county_Name = v.country_name;
		if ((county_Name && (county_Name.toLowerCase() === "russian federation"))){
			county_Name = "RUSSIA";
		}
		if ((d.properties && d.properties.name && county_Name && (d.properties.name.toLowerCase() == county_Name.toLowerCase()))) {
			return (county_Name.toLowerCase() == d.properties.name.toLowerCase());
		}
		else if ((d.properties && d.properties.name && v.Country && (d.properties.name.toLowerCase() == v.Country.toLowerCase()))) {
			return (d.properties.name.toLowerCase() == v.Country.toLowerCase());
		}
	});
	return index;
}

/**
 * Finding index for each country name for report world map
 */
function findIndexForEachCountryName(v, options) {
	var index = _.findIndex(options, function (d) {
		var county_Name = v.country_name;
		if ((county_Name && (county_Name.toLowerCase() === "russian federation"))){
			county_Name = "RUSSIA";
		}
		if ((d.properties && d.properties.name && county_Name && (d.properties.name.toLowerCase() == county_Name.toLowerCase()))) {
			return (county_Name.toLowerCase() == d.properties.name.toLowerCase());
		}
		else if ((d.properties && d.properties.name && v.Country && (d.properties.name.toLowerCase() == v.Country.toLowerCase()))) {
			return (d.properties.name.toLowerCase() == v.Country.toLowerCase());
		}
	});
	return index;
}


function nFormatter(num, digits) {
	var si = [
	  { value: 1E18, symbol: "E" },
	  { value: 1E15, symbol: "P" },
	  { value: 1E12, symbol: "T" },
	  { value: 1E9,  symbol: "G" },
	  { value: 1E6,  symbol: "M" },
	  { value: 1E3,  symbol: "k" }
	], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
	for (i = 0; i < si.length; i++) {
	  if (num >= si[i].value) {
		return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
	  }
	}
	return num.toFixed(digits).replace(rx, "$1");
}