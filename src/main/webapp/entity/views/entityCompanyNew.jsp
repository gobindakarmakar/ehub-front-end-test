<!-- entityCompany.jsp -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->
<style>
    
</style>


<!--  Spinner Starts  -->
<div class="custom-spinner full-page-spinner" ng-if="disableSearchButton">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<div class="custom-spinner full-page-spinner" ng-if="pdfLoader">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>

<!--  Spinner Ends  -->

<!--  Entity Company Starts  -->
<div id="entity-company" class="entity-company-wrapper">

    <!--  Entity Company Content Starts  -->
    <div id="entity-company-content" class="entity-page" ng-show="!disableSearchButton && !noDataFoundText">
        <div class="entity-company-content-inner-wrapper entityCompanyNew-inner-wrapper"
            ng-style="{'padding-right': mainInfoTabType === 'Duediligence'?'0px':'325px'}">

            <!--  Entity Company Content Left Starts  -->
            <div class="entity-company-content-left">

                <!--  Entity Results Wrapper Starts  -->
                <div class="container-fulid entity-results-wrapper">
                    <div class="custom-row bst_row_wrapper mar-0">

                        <!--  Entity Company Intro Starts  -->
                        <div
                            class="custom-col d-flex bst_panel_wrapper custom-panel-wrapper entity-company-intro entity-company-new-intro">
                            <div class="entity-company-image pe-none">
                                <a href="javascript:void(0);" target="_blank">
                                    <img class="img-responsive"
                                        ng-src="{{ entitySearchResult.list.topHeaderObject.logo_url	||entitySearchResult.comapnyLogo|| '../assets/images/dummy-logo.png'}}"
                                        ng-show="entitySearchResult.list.topHeaderObject.logo_url || entitySearchResult.comapnyLogo "
                                        alt="fetcher_logo"></img>
                                    <i class="fa fa-building-o"
                                        ng-hide="entitySearchResult.list.topHeaderObject.logo_url ||  entitySearchResult.comapnyLogo"></i>
                                </a>
                            </div>
                            <div class="entity-company-description p-rel clearfix">
                                <h2 class="company-title bottom-pop">{{entitySearchResult.name}}<a
                                        href="javascript:void(0);" class="va-m h-1_8 mar-l5 mar-r25 f-15"
                                        uib-popover="{{entitySearchResult.list.significantTitle}}"
                                        popover-trigger="'mouseenter'" popover-placement="bottom"
                                        ng-class=" entitySearchResult.list.isSignificant ?  'text-dark-blue' : 'text-white'  "
                                        ng-model="mySignificant"
                                        ng-click="makeSignificant(entitySearchResult.list.isSignificant)">
                                        <i class="fa fa-star" aria-hidden="true"
                                            ng-mouseover="checksignificantTitle()"></i>
                                    </a> </h2>

                                <!-- <p>{{entitySearchResult.title}}</p> -->
                                <div class="main-content-description main-information-content p-rel">
                                    <div class="highlighting-info-wrapper">
                                        <a href="" class="read-more" ng-click="onViewClick('overview')">Read More</a>
                                        <ul class="list-inline clearfix">
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key">Industry: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.list.topHeaderObject.industry}}">
                                                            {{ceriSearchResultObject['industryType'].value ||
                                                            entitySearchResult.list.topHeaderObject.industry || 'N/A'}}</span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Company: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject['lei:legalForm'].value || entitySearchResult.list.topHeaderObject['lei:legalForm'].label}}">
                                                            {{ ceriSearchResultObject['lei:legalForm'].value
                                                            ||entitySearchResult.list.topHeaderObject['lei:legalForm'].label
                                                            || 'N/A'}}</span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Incorporation Date: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject['isIncorporatedIn'].value  ||entitySearchResult.list.topHeaderObject.hasLatestOrganizationFoundedDate}}">
                                                            {{ceriSearchResultObject['isIncorporatedIn'].value
                                                            ||entitySearchResult.list.topHeaderObject.hasLatestOrganizationFoundedDate||
                                                            'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Country Of Origin: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject.isDomiciledIn.value ||  entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'][0].country || entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'].country}}">
                                                            {{ceriSearchResultObject.isDomiciledIn.value ||
                                                            entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'][0].country
                                                            ||
                                                            entitySearchResult.list.topHeaderObject['mdaas:RegisteredAddress'].country
                                                            ||
                                                            'N/A'}}
                                                        </span>
                                                    </p>
                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key">Founder: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.founder|| entitySearchResult.list.founders[0].name}}">
                                                            {{entitySearchResult.founder||
                                                            entitySearchResult.list.founders[0].name || 'N/A'}}</span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Founded: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject['hasLatestOrganizationFoundedDate'].value ||  entitySearchResult.list.topHeaderObject.hasLatestOrganizationFoundedDate|| entitySearchResult.list.topHeaderObject.FoundedDate}}">
                                                            {{ceriSearchResultObject['hasLatestOrganizationFoundedDate'].value
                                                            ||
                                                            entitySearchResult.list.topHeaderObject.hasLatestOrganizationFoundedDate||
                                                            entitySearchResult.list.topHeaderObject.FoundedDate
                                                            || 'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Nature Of Business: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.advanceCompanyDetails.nature_of_business || entitySearchResult.list.topHeaderObject.bizType}}">
                                                            {{entitySearchResult.advanceCompanyDetails.nature_of_business
                                                            || entitySearchResult.list.topHeaderObject.bizType
                                                            ||'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Revenue: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.list.topHeaderObject.revenue || entitySearchResult['revenue']}}">
                                                            {{ entitySearchResult.list.topHeaderObject.revenue ||
                                                            entitySearchResult['revenue']|| 'N/A'}}</span>
                                                    </p>

                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p ng-if="!hideTabs">
                                                        <span class="key">Followers: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.followers_count|| entitySearchResult.list.social_media_count  }}">
                                                            {{entitySearchResult.followers_count|| entitySearchResult.list.social_media_count  || 'N/A'}}</span>
                                                    </p>
                                                    <p ng-if="!hideTabs">
                                                        <span class="key">Strength: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.list.topHeaderObject.company_size || entitySearchResult.number_of_employees||
                                                            entitySearchResult.company_size ||  entitySearchResult.volume }}">
                                                            {{entitySearchResult.list.topHeaderObject.company_size ||
                                                            entitySearchResult.number_of_employees||
                                                            entitySearchResult.company_size ||
                                                            entitySearchResult.volume || 'N/A'}}</span>
                                                    </p>
                                                    <p ng-if="hideTabs">
                                                        <span class="key">Email: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.list['truecaller'][0]['email']||entitySearchResult.email || entitySearchResult.list.topHeaderObject.email }}">
                                                            {{entitySearchResult.list['truecaller'][0]['email']||
                                                            entitySearchResult.email ||
                                                            entitySearchResult.list.topHeaderObject.email || 'N/A'}}</span>
                                                    </p>
                                                    <p ng-if="hideTabs">
                                                        <span class="key">Fax Number: </span>
                                                        <span class="value"
                                                            title="{{(ceriSearchResultObject['tr-org:hasHeadquartersFaxNumber'].value)|| (ceriSearchResultObject['hasRegisteredFaxNumber'].value)}}">
                                                            {{(ceriSearchResultObject['tr-org:hasHeadquartersFaxNumber'].value)||
                                                            (ceriSearchResultObject['hasRegisteredFaxNumber'].value) ||
                                                            "N/A"}}
                                                        </span>
                                                    </p>
                                                    <p
                                                        ng-show="ceriSearchResultObject.hasActivityStatus.value || entitySearchResult.list.topHeaderObject.hasActivityStatus">
                                                        <span class="key">Company Status: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject.hasActivityStatus.value || entitySearchResult.list.topHeaderObject.hasActivityStatus}}">
                                                            {{ ceriSearchResultObject.hasActivityStatus.value ||
                                                            entitySearchResult.list.topHeaderObject.hasActivityStatus||
                                                            'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Dissolution Date: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.advanceCompanyDetails.dissolution_date|| 'N/A'}}">
                                                            {{entitySearchResult.advanceCompanyDetails.dissolution_date||
                                                            'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Business Type: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.business_type||   entitySearchResult.list.topHeaderObject.nature_of_business}}">
                                                            {{entitySearchResult.business_type||
                                                            entitySearchResult.list.topHeaderObject.nature_of_business
                                                            ||
                                                            'N/A'}}</span>
                                                    </p>
                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p ng-if="!hideTabs">
                                                        <span class="key">Email: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.list['truecaller'][0]['email']||
                                                        entitySearchResult.email ||  entitySearchResult.list.topHeaderObject.email}}">
                                                            {{entitySearchResult.list['truecaller'][0]['email']||
                                                            entitySearchResult.email ||
                                                            entitySearchResult.list.topHeaderObject.email || 'N/A'}}</span>

                                                    </p>
                                                    <p ng-if="!hideTabs">
                                                        <span class="key">Fax Number: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject['tr-org:hasHeadquartersFaxNumber'].value || ceriSearchResultObject['hasRegisteredFaxNumber'].value ||  entitySearchResult.list.topHeaderObject['tr-org:hasHeadquartersFaxNumber'] ||entitySearchResult.list.topHeaderObject['hasRegisteredFaxNumber']}}">
                                                            {{
                                                            ceriSearchResultObject['tr-org:hasHeadquartersFaxNumber'].value
                                                            || ceriSearchResultObject['hasRegisteredFaxNumber'].value
                                                            ||
                                                            entitySearchResult.list.topHeaderObject['tr-org:hasHeadquartersFaxNumber']
                                                            ||entitySearchResult.list.topHeaderObject['hasRegisteredFaxNumber']
                                                            || "N/A"}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Website: </span>
                                                        <a ng-show="entitySearchResult.list.topHeaderObject.hasURL || entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website"
                                                            target="_blank"
                                                            href="{{entitySearchResult.list.topHeaderObject.hasURL|| entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website}}"
                                                            class="value"
                                                            title="{{ entitySearchResult.list.topHeaderObject.hasURL || entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website}}">
                                                            {{entitySearchResult.list.topHeaderObject.hasURL ||
                                                            entitySearchResult.website||entitySearchResult.company_url
                                                            || entitySearchResult.company_website || 'N/A'}}</a>
                                                        <a ng-hide="entitySearchResult.list.topHeaderObject.hasURL || entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website"
                                                            target="_blank"
                                                            href="{{entitySearchResult.list.topHeaderObject.hasURL|| entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website}}"
                                                            class="value"
                                                            title="{{ entitySearchResult.list.topHeaderObject.hasURL || entitySearchResult.website|| entitySearchResult.company_url  || entitySearchResult.company_website}}">
                                                            N/A</a>
                                                    </p>
                                                    <p>
                                                        <span class="key">in liquidation: </span>
                                                        <span class="value"
                                                            title="{{entitySearchResult.advanceCompanyDetails.in_liquidation}}">
                                                            {{entitySearchResult.advanceCompanyDetails.in_liquidation||

                                                            'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p ng-show="entitySearchResult.list.topHeaderObject.isDomiciledIn">
                                                        <span class="key">has uk establishment: </span>
                                                        <span class="value" title="yes"
                                                            ng-show="entitySearchResult.list.topHeaderObject.isDomiciledIn === 'GB'">
                                                            YES</span>
                                                        <span class="value" title="NO"
                                                            ng-hide="entitySearchResult.list.topHeaderObject.isDomiciledIn === 'GB'">
                                                            NO</span>
                                                    </p>
                                                    <p>
                                                        <span class="key">Company Size: </span>
                                                        <span class="value"
                                                            title="{{ceriSearchResultObject.size.value ||  entitySearchResult.company_size|| entitySearchResult.list.topHeaderObject.company_size}}">
                                                            {{ceriSearchResultObject.size.value ||
                                                            entitySearchResult.company_size||
                                                            entitySearchResult.list.topHeaderObject.company_size ||
                                                            'N/A'}}</span>

                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="entity-company-risk d-if clearfix" ng-if="mainInfoTabType === 'Duediligence'">
                                <div class="header-content-entry person-bar-graph progress-bar-wrapper">
                                    <div class="header-content-entry person-circle-graph  custom-person-circle-graph clearfix">
                                        <div class="c100 mar-t10 p{{cumulativeRisk| number: 0}} orange radial" ng-click="openRiskScoreModal();">
                                            <span class="inner-number riskScoreTooltip" ng-if="isDefined(cumulativeRisk) && cumulativeRisk != null ">{{cumulativeRisk | number : 0}}%
                                            </span>
                                            <span class="inner-number" ng-if="!isDefined(cumulativeRisk) || cumulativeRisk == null ">
                                                N/A
                                            </span>
                                            <span class="inner-text"> Risk Score</span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="highlighting-list">
                                        <ul class="highlighting-info  list-inline custom-list item-1 pad-l0 pad-t10 screening-list">
                                               <!--  <li  class="d-flex  c-pointer mar-b5 ai-c border-0" ng-show="entitySearchResult.list.summaryCount.pepCount === 0">                                                  
                                                        <i class="fa fa-street-view mar-r10  showTooltipforSummary  f-21" ng-class="(entitySearchResult.list.summaryCount.pepCount > 0) ? 'text-light-orange c-pointer' : 'text-dark-green2 pe-none op-1'" ng-click="openAdverseDirectly(0,'','pep','true')"
                                                        ng-mouseover="tooltipdtata('Total Pep Count',(entitySearchResult.list.summaryCount.pepCount || 0))"></i>
                                                    <span class=" text-dark-cream mar-r10">PEP Alerts</span>
                                                    <span class="mar-autol f-16">
                                                        <span ng-class ="entitySearchResult.list.summaryCount.pepCount > 0 ? 'c-pointer' :'pe-none op-1'" ng-disabled ="entitySearchResult.list.summaryCount.pepCount > 0 ? 'false' : 'true'" ng-click="openAdverseDirectly(0,'','pep','true')">{{entitySearchResult.list.summaryCount.pepCount || 0}}</span>
                                                        
                                                    </span>
                                                 
                                                </li> -->
                                                <li  class="d-flex  c-pointer mar-b5 ai-c border-0 ">
                                                        <i class="fa fa-street-view mar-r10  showTooltipforSummary f-21" ng-class="(entitySearchResult.list.summaryCount.pepCount > 0) ? 'text-light-orange c-pointer' : 'text-dark-green2 pe-none op-1'" ng-click="openAdverseDirectly(0,'','pep','true')"
                                                        ng-mouseover="tooltipSummaryCountFunction('Total Pep Count',(entitySearchResult.list.summaryCount.pepCount || 0))"></i>
                                                    <span class=" text-dark-cream mar-r10">PEP Alerts</span>
                                                    <span class="mar-autol f-16">
                                                        <span ng-class ="entitySearchResult.list.summaryCount.pepCount > 0 ? 'c-pointer' :'pe-none op-1'" ng-disabled ="entitySearchResult.list.summaryCount.pepCount > 0 ? 'false' : 'true'" ng-click="openAdverseDirectly(0,'','pep','true')">{{entitySearchResult.list.summaryCount.pepCount || 0 }}</span>

                                        </li>
                                      
                                        <%-- <li class="d-flex mar-b5 ai-c border-0" ng-hide="actualScreeningData[0].pep_url">
                                            <i class="fa mar-r10 fa-street-view  f-21" ng-class="(actualScreeningData[0].pep_url && actualScreeningData[0].pep_url.length > 0) ? 'text-light-orange' : 'text-dark-green2'"
                                                ng-click="openmodal(actualScreeningData[0], 'pepalert', $index)"
                                                ng-mouseover="tooltipdtata(actualScreeningData[0])"></i>
                                            <span class=" text-dark-cream mar-r10">PEP Alerts</span>
                                            <span class="mar-autol f-16">
                                                <span>{{(actualScreeningData[0].pep_url &&
                                                    actualScreeningData[0].pep_url.length > 0) ?
                                                    actualScreeningData[0].pep_url.length : 0 }}</span> --%>

                                        <!-- <span class="text-blue1 mar-r5  companyInfoTooltip" ng-if="actualScreeningData[0].pep_url === ''" ng-mouseover="mouseover(actualScreeningData[0].pep_url, 'companyInfoTooltip');"
                                                            ng-style="myStyle2" ng-click="corporatePopup(actualScreeningData[0].pep_url)"
                                                            style="cursor: pointer;">[0P]</span>
                                                        <span class="text-blue1 mar-r5  companyInfoTooltip" ng-if="actualScreeningData[0].pep_url !== ''" ng-mouseover="mouseover(actualScreeningData[0].pep_url, 'companyInfoTooltip');"
                                                            ng-style="myStyle2" ng-click="corporatePopup(actualScreeningData[0].pep_url)"
                                                            style="cursor: pointer;">[1P]</span>
                                                        <span class="text-pink">[0S]</span> -->
                                        <%-- </span>
                                                  
                                                </li> --%>
                                        <li class="d-flex  c-pointer mar-b5 ai-c border-0"
                                            ng-show="entitySearchResult.list.summaryCount.sanctionCount.length === 0">

                                            <i class="fa fa-ban mar-r10 showTooltipforSummary  f-21"
                                                ng-class="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1'"
                                                ng-click="openAdverseDirectly(0,'','sanction','true')"
                                                ng-mouseover="tooltipSummaryCountFunction('Total Sanction Count',(entitySearchResult.list.summaryCount.sanctionCount || 0))"></i>
                                            <span class=" text-dark-cream mar-r10">Sanctions</span>
                                            <span class="mar-autol f-16">
                                                <span
                                                    ng-class="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'c-pointer' :'pe-none op-1'"
                                                    ng-disabled="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'false' : 'true'"
                                                    ng-click="openAdverseDirectly(0,'','sanction','true')">{{entitySearchResult.list.summaryCount.sanctionCount || 0}}</span>
                                            </span>

                                        </li>
                                        <li class="d-flex  c-pointer mar-b5 ai-c border-0"
                                            ng-hide="entitySearchResult.list.summaryCount.sanctionCount.length === 0">

                                            <i class="fa fa-ban mar-r10  showTooltipforSummary  f-21"
                                                ng-class="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1 '"
                                                ng-click="openAdverseDirectly(0,'','sanction','true')"
                                                ng-mouseover="tooltipSummaryCountFunction('Total Sanction Count',(entitySearchResult.list.summaryCount.sanctionCount  ||  0))"></i>
                                            <span class=" text-dark-cream mar-r10">Sanctions</span>
                                            <span class="mar-autol f-16">
                                                <span
                                                    ng-class="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'c-pointer' :'pe-none op-1'"
                                                    ng-disabled="entitySearchResult.list.summaryCount.sanctionCount > 0 ? 'false' : 'true'"
                                                    ng-click="openAdverseDirectly(0,'','sanction','true')">{{entitySearchResult.list.summaryCount.sanctionCount  ||  0}}</span>
                                            </span>

                                        </li>
                                        <li class="d-flex mar-b5 ai-c border-0">
                                            <i class="fa fa-globe mar-r10 showTooltipforSummary f-21"
                                                ng-class="entitySearchResult.list.summaryCount.highRiskCount > 0 ?  'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1'  "
                                                ng-mouseover="tooltipSummaryCountFunction('Total Jurisdiction Count',(entitySearchResult.list.summaryCount.highRiskCount || 0))"
                                                ng-click="openAdverseDirectly(0,'','highRisk','true')"></i>
                                            <span class=" text-dark-cream mar-r10">Jurisdiction</span>
                                            <span class="mar-autol f-16">
                                                <span class=""
                                                    ng-class="entitySearchResult.list.summaryCount.highRiskCount > 0 ? 'c-pointer' :'pe-none op-1'"
                                                    ng-disabled="entitySearchResult.list.summaryCount.highRiskCount > 0 ? 'false' : 'true'"
                                                    ng-click="openAdverseDirectly(0,'','highRisk','true')">{{entitySearchResult.list.summaryCount.highRiskCount || 0}}</span>
                                            </span>

                                        </li>
                                        <!--   <li  class="d-flex  c-pointer mar-b5 ai-c border-0" ng-hide="(actualScreeningData[0].high_risk_jurisdiction === 'high') || (actualScreeningData[0].high_risk_jurisdiction === 'medium')">
                                                        <i class="fa mar-r10 fa-globe showTooltipforSummary f-21" ng-class="(actualScreeningData[0].high_risk_jurisdiction && (actualScreeningData[0].high_risk_jurisdiction === 'high' || actualScreeningData[0].high_risk_jurisdiction === 'medium')) ? 'text-light-orange' : 'text-dark-green2 pe-none op-1'" ng-click="openmodal(actualScreeningData[0], 'high_risk_jurisdiction')"
                                                        ng-mouseover="tooltipSummaryCountFunction(actualScreeningData[0])"></i>
                                                    <span class=" text-dark-cream mar-r10">Jurisdiction</span>
                                                    <span class="mar-autol f-16">
                                                            <span>{{(actualScreeningData[0].high_risk_jurisdiction && (actualScreeningData[0].high_risk_jurisdiction === 'HIGH' || actualScreeningData[0].high_risk_jurisdiction === 'MEDIUM')) ? 1 : 0 }}</span>
                                                    </span>
                                                  
                                                </li>
                                                <li  class="d-flex  c-pointer mar-b5 ai-c border-0" ng-show="actualScreeningData[0].high_risk_jurisdiction === 'medium'">
                                                        <i class="fa mar-r10 fa-globe showTooltipforSummary f-21" ng-class="(actualScreeningData[0].high_risk_jurisdiction && (actualScreeningData[0].high_risk_jurisdiction === 'high' || actualScreeningData[0].high_risk_jurisdiction === 'medium')) ? 'text-light-orange' : 'text-dark-green2 pe-none op-1'" ng-click="openmodal(actualScreeningData[0], 'high_risk_jurisdiction')"
                                                        ng-mouseover="tooltipSummaryCountFunction(actualScreeningData[0])"></i>
                                                    <span class=" text-dark-cream mar-r10">Jurisdiction</span>
                                                    <span class="mar-autol f-16">
                                                            <span>{{(actualScreeningData[0].high_risk_jurisdiction && (actualScreeningData[0].high_risk_jurisdiction === 'high' || actualScreeningData[0].high_risk_jurisdiction === 'medium')) ? 1 : 0 }}</span>
                                                    </span>
                                                   
                                                </li> -->
                                        <!-- 
                                                <li  class="d-flex mar-b5 ai-c border-0" ng-hide="entitySearchResult.list.summaryCount.financeCount === 0">
                                                        <i class="fa fa-gavel mar-r10  showTooltipforSummary  f-21" ng-class="((summaryScreenig[0].finance_Crime_url && (summaryScreenig[0].finance_Crime_url.length > 0)) ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1')" ng-click="openAdverseDirectly(0,'','finance','true')" ng-mouseover="tooltipSummaryCountFunction('Total Finance News Count',(entitySearchResult.list.summaryCount.financeCount|| 0))"
                                                       ></i>
                                                    <span class=" text-dark-cream mar-r10">Finance Crime</span>
                                                    <span class="mar-autol f-16">
                                                        <span  class="" ng-class ="entitySearchResult.list.summaryCount.financeCount > 0 ? 'c-pointer' :'pe-none op-1'" ng-disabled ="entitySearchResult.list.summaryCount.financeCount > 0 ? false : true" ng-click="openAdverseDirectly(0,'','finance','true')">{{entitySearchResult.list.summaryCount.financeCount|| 0 }}</span>
                                                    </span>
                                                </li> -->
                                        <li class="d-flex mar-b5 ai-c border-0">
                                            <i class="fa fa-gavel   mar-r10 showTooltipforSummary  f-21"
                                                ng-class="(entitySearchResult.list.summaryCount.financeCount > 0) ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1'"
                                                ng-click="openAdverseDirectly(0,'','finance','true')"
                                                ng-mouseover="tooltipSummaryCountFunction('Total Finance News Count',(entitySearchResult.list.summaryCount.financeCount|| 0))"></i>
                                            <span class=" text-dark-cream mar-r10">Finance Crime</span>
                                            <span class="mar-autol f-16">
                                                <span class=""
                                                    ng-class="entitySearchResult.list.summaryCount.financeCount > 0 ? 'c-pointer' :'pe-none op-1'"
                                                    ng-disabled="entitySearchResult.list.summaryCount.financeCount > 0 ? false : true"
                                                    ng-click="openAdverseDirectly(0,'','finance','true')">{{entitySearchResult.list.summaryCount.financeCount || 0 }}</span>
                                            </span>
                                        </li>
                                        <!--   <li  class="d-flex mar-b5 ai-c border-0" ng-hide="entitySearchResult.list.summaryCount.adverseCount === 0">
                                                        <i class="fa fa-newspaper-o mar-r10  showTooltipforSummary f-21" ng-class="(entitySearchResult.list.summaryCount.adverseCount > 0) ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1'" ng-click="openAdverseDirectly(0,'','adverse','true')" ng-mouseover="tooltipSummaryCountFunction('Total Adverse News Count',(entitySearchResult.list.summaryCount.adverseCount || 0))"
                                                       ></i>
                                                    <span class=" text-dark-cream mar-r10">Adverse News</span>
                                                    <span class="mar-autol f-16">
                                                        <span  class="" ng-class ="entitySearchResult.list.summaryCount.adverseCount > 0 ? 'c-pointer' :'pe-none op-1'" ng-disabled ="entitySearchResult.list.summaryCount.adverseCount > 0 ? false : true"  ng-click="openAdverseDirectly(0,'','adverse','true')">{{ entitySearchResult.list.summaryCount.adverseCount || 0}}</span>
                                                    </span>
                                                </li> -->
                                                <li  class="d-flex mar-b5 ai-c border-0" >
                                                    <i class="fa fa-newspaper-o mar-r10  showTooltipforSummary  f-21" ng-class="(entitySearchResult.list.summaryCount.adverseCount> 0) ? 'text-light-orange  c-pointer' : 'text-dark-green2 pe-none op-1'" ng-click="openAdverseDirectly(0,'','adverse','true')" ng-mouseover="tooltipSummaryCountFunction('Total News Count',(entitySearchResult.list.summaryCount.adverseCount || 0))"></i>
                                                    <i class="fa fa-newspaper-o mar-r10  text-dark-grey  f-21" ng-show=" entitySearchResult.list.summaryCount.adverseCount === 0  && summaryScreenig[0].non_negativenews.length > 0"
                                                        ng-click="openAdverseDirectly(0,'','adverse','true')" ng-mouseover="tooltipSummaryCountFunction('Total News Count',(entitySearchResult.list.summaryCount.adverseCount || 0))"></i>
                                                    <span class=" text-dark-cream mar-r10">News</span>
                                                    <span class="mar-autol f-16">
                                                            <span class="" ng-class ="entitySearchResult.list.summaryCount.adverseCount > 0 ? 'c-pointer' :'pe-none op-1'" ng-disabled ="entitySearchResult.list.summaryCount.adverseCount > 0 ? false : true"  ng-click="openAdverseDirectly(0,'','adverse','true')">{{entitySearchResult.list.summaryCount.adverseCount || 0 }}</span>
                                                        </span>
                                                </li>
                                            </ul>
                                </div>
                            </div>
                        </div>
                        <!--  Entity Company Intro Ends  -->

                        <!--  Entity Contents Wrapper Starts  -->
                        <div class="custom-col pad-0 entity-company-new-desc">
                            <div class="innerSidebarToggle">
                                <!--  Custom TABS Section Wrapper Starts  -->
                                <div class="entity-tabs-wrapper">
                                    <div class="tabs-section">
                                        <ul class="nav nav-tabs">
                                            <li ng-class="{'active': mainInfoTabType === 'Overview'}">
                                                <a href ng-click="onClickInfoTabs('Overview')">
                                                    <i class="fa fa-globe"></i>
                                                    <span class="main-nav-texts">Overview</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Duediligence'}">
                                                <a href ng-click="onClickInfoTabs('Duediligence')">
                                                    <i class="fa fa-search"></i>
                                                    <span class="main-nav-texts">Compliance</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Leaderships'}">
                                                <a href ng-click="onClickInfoTabs('Leaderships')">
                                                    <i class="fa fa-users"></i>
                                                    <span class="main-nav-texts">Leadership</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Financial Growth'}">
                                                <a href ng-click="onClickInfoTabs('Financial Growth')">
                                                    <i class="fa fa-signal"></i>
                                                    <span class="main-nav-texts">Financial</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Risk Alerts'}">
                                                <a href ng-click="onClickInfoTabs('Risk Alerts')">
                                                    <i class="fa fa-exclamation-triangle"></i>
                                                    <span class="main-nav-texts">Risk Alerts</span>
                                                </a>
                                            </li>
                                            <li ng-if="!hideTabs" ng-class="{'active': mainInfoTabType === 'Autodesk'}">
                                                <a href ng-click="onClickInfoTabs('Autodesk')">
                                                    <i class="fa fa-user-secret"></i>
                                                    <span class="main-nav-texts">Threats Intelligence</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Latest News'}">
                                                <a href ng-click="onClickInfoTabs('Latest News')">
                                                    <i class="fa fa-newspaper-o"></i>
                                                    <span class="main-nav-texts">Latest News</span>
                                                </a>
                                            </li>
                                            <li ng-if="!hideTabs" ng-class="{'active': mainInfoTabType === 'Social Media'}">
                                                <a href ng-click="onClickInfoTabs('Social Media')">
                                                    <i class="fa fa-share-alt-square"></i>
                                                    <span class="main-nav-texts">Social Media</span>
                                                </a>
                                            </li>
                                            <li ng-if="!hideTabs" ng-class="{'active': mainInfoTabType === 'Medias'}">
                                                <a href ng-click="onClickInfoTabs('Medias')">
                                                    <i class="fa fa-upload"></i>
                                                    <span class="main-nav-texts">Media</span>
                                                </a>
                                            </li>
                                            <li ng-if="isQuessionaire"
                                                ng-class="{'active': mainInfoTabType === 'Upload'}">
                                                <a href ng-click="onClickInfoTabs('Upload')">
                                                    <i class="fa fa-tasks"></i>
                                                    <span class="main-nav-texts">Form Analysis</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--  Custom TABS Section Wrapper Ends  -->
                                <div class="entity-contents-wrapper">
                                    <!--   Overview Tab Starts   -->
                                    <div class="overview-tab-wrapper" ng-show="mainInfoTabType === 'Overview'">
                                        <div ng-include="'./views/entityCompOverview.html'" ng-controller ="EntityCompanyOverviewNewController"></div>
                                    </div>
                                    <!--   Overview Tab Ends   -->

                                    <!--   Deligence Tab Starts   -->
                                    <div class="due-tab-wrapper" ng-show="mainInfoTabType === 'Duediligence'">
                                        <div ng-include="'./views/entityCompCompliance.html'" ng-controller ="EntityCompanyDueDiligenceNewController" ></div>
                                    </div>
                                    <!--   Deligence Tab Ends   -->

                                    <!--   Autodesk Tab Starts   -->
                                    <div ng-if="!hideTabs" class="autodesk-tab-wrapper"
                                        ng-show="mainInfoTabType === 'Autodesk'">
                                        <div class="pad-t8" ng-include="'./views/entityCompanyThreat.html'" ng-controller ="EntityCompanyAutodeskNewController"></div>
                                    </div>
                                    <!--   Autodesk Tab Ends   -->

                                    <!--  Leadership Tab Starts  -->
                                    <div class="leadership-tab-wrapper" ng-show="mainInfoTabType === 'Leaderships'">
                                        <div ng-include="'./views/entityCompLeadership.html'"  ng-controller ="EntityCompanyLeadershipsNewController"></div>
                                    </div>
                                    <!--  Leadership Tab Ends  -->

                                    <!--  Financial Growth Tab Starts  -->
                                    <div class="financial-growth-tab-wrapper"
                                        ng-show="mainInfoTabType === 'Financial Growth'">
                                        <div ng-include="'./views/entityCompFinancial.html'" ng-controller="EntityCompanyFinancialGrowthNewController"></div>
                                    </div>
                                    <!--  Financial Growth Tab Ends  -->


                                    <!--  Risk Alerts Tab Starts  -->
                                    <div class="risk-alerts-tab-wrapper" ng-show="mainInfoTabType === 'Risk Alerts'">
                                        <div ng-include="'./views/entityCompRiskAlerts.html'"ng-controller="EntityCompanyRiskAlertsNewController"></div>
                                    </div>
                                    <!--  Risk Alerts Tab Ends  -->

                                    <!--  Latest News Tab Starts  -->
                                    <div class="latest-news-tab-wrapper" ng-show="mainInfoTabType === 'Latest News'">
                                        <div ng-include="'./views/entityCompLatestNews.html'" ng-controller="EntityCompanyLatestNewsController"></div>
                                    </div>
                                    <!--  Latest News Tab Ends  -->

                                    <!--  Social Media Tab Starts  -->
                                    <div ng-if="!hideTabs" class="social-media-tab-wrapper mnh-450"
                                        ng-show="mainInfoTabType === 'Social Media'">
                                        <div ng-include="'./views/entityCompSocialMedia.html'"  ng-controller="EntityCompanySocialMediaNewController"></div>
                                    </div>
                                    <!--  Social Media Tab Ends  -->

                                    <!--  Medias Tab Starts  -->
                                    <div ng-if="!hideTabs" class="medias-tab-wrapper mnh-450"
                                        ng-show="mainInfoTabType === 'Medias'">
                                        <div ng-include="'./views/entityCompMedia.html'"  ng-controller="EntityCompanyMediasNewController"></div>
                                    </div>
                                    <!--  Medias Tab Ends  -->

                                    <!--Upload Questionaire Starts -->
                                    <div class="medias-tab-wrapper pad-0 upload-questionnaire"
                                        ng-show="mainInfoTabType === 'Upload'">
                                        <div ng-include="'./views/entityCompFormAnalysis.html'" ></div>
                                    </div>
                                    <!--Upload Questionaire Ends -->

                                </div>
                            </div>
                        </div>
                        <!--   Entity Contents Wrapper Ends  -->

                    </div>

                </div>
                <!--  Entity Results Wrapper Ends  -->

            </div>
            <!--  Entity Company Content Left Ends  -->

            <!--  Entity Company Content Right Starts  -->
            <div class="entity-company-content-right entity-company-new-right"
                ng-if="mainInfoTabType !== 'Duediligence'&& mainInfoTabType !== 'Upload' ">
                <!--  Risk Store Panel Starts  -->
                <div class="risk-score-panel right-overview-wrapper" ng-show="mainInfoTabType === 'Overview'">
                    <div ng-include="'./views/rightPanel/entityCompOverviewRightPanel.html'"></div>
                </div>
                <!--  Risk Store Panel Ends  -->

                <!-- Duediligence rightside tab strats -->
                <div class="risk-score-panel height-100 due-tab-wrapper adverse-news-panel entity-company-new-right right-leadership-wrapper"
                    ng-show="mainInfoTabType === 'Duediligence'">
                    <div ng-include="'./views/rightPanel/entityCompComplianceRightPanel.html'"></div>
                </div>
                <!-- Duediligence rightside tab ends -->

                <!-- Autodesk rightside tab strats -->
                <div class="risk-score-panel height-100 autodesk-tab-wrapper right-leadership-wrapper"
                    ng-show="mainInfoTabType === 'Autodesk'">
                    <div ng-include="'./views/rightPanel/entityCompThreatRightPanel.html'"></div>
                </div>
                <!-- Autodesk rightside tab ends -->


                <div class="risk-score-panel right-leadership-wrapper" ng-show="mainInfoTabType === 'Leaderships'">
                    <div ng-include="'./views/rightPanel/entityCompLeadershipRightPanel.html'"></div>
                </div>

                <div class="risk-score-panel right-financial-wrapper" ng-show="mainInfoTabType === 'Financial Growth'">
                    <div ng-include="'./views/rightPanel/entityCompFinancialRightPanel.html'"></div>
                </div>

                <div class="risk-score-panel right-riskalerts-wrapper" ng-show="mainInfoTabType === 'Risk Alerts'">
                    <div ng-include="'./views/rightPanel/entityCompRiskAlertsRightPanel.html'"></div>
                </div>

                <div class="risk-score-panel right-latestnews-wrapper" ng-show="mainInfoTabType === 'Latest News'">
                    <div ng-include="'./views/rightPanel/entityCompLatestNewsRightPanel.html'"></div>
                </div>

                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Social Media'">
                    <div ng-include="'./views/rightPanel/entityCompSocialMediaRightPanel.html'"></div>
                </div>

                <div class="risk-score-panel right-medias-wrapper" ng-show="mainInfoTabType === 'Medias'">
                    <div ng-include="'./views/rightPanel/entityCompMediaRightPanel.html'"></div>
                </div>
                <!--  Entity Company Content Right Ends  -->
            </div>
        </div>
        <!--  Entity Company Content Ends  -->
        <div class="alert text-center" ng-show="noDataFoundText">
            <span>{{noDataFoundText}}</span>
        </div>


    </div>
</div>
<!--  Entity Company Ends  -->
<script type="text/ng-template" id="editOwnership.html">
    <div class="risk-overview-modal-wrapper">
            <!-- 		<div class="custom-spinner case-dairy-spinner" > -->
            <!-- 			<i class="fa fa-spinner fa-spin fa-3x"></i> -->
            <!-- 		</div> -->
                    <div class="modal-header pad-b5">
                        <button type="button" class="closePulseModal width-5 text-cream  pad-x0 pull-right" ng-click="saveTheValueOfOwnership(false)" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title width-95 text-overflow">
                            <span id="articleHeader">Edit Entity For Screening</span>
                        </h4>
                    </div>
    <div class="modal-body">
    <form class="form pad-r0" autocomplete="no-display" name="personForm">
        <div class="d-flex mar-b20 ai-c">
            <div class=" p-rel  mar-b0 bst_input_group  width-45 " ng-class ="popOverEditDataForOwnership.entity_type === 'person' ? 'first-name-icon':'company-icon' ">					
                <input type="text" autocomplete="no-display" name="companyName" class="custom-input text-cream pad-t15 pad-x15 f-12 height-100" ng-model ="popOverEditDataForOwnership.name"  placeholder="First Name"  ng-disabled="popOverEditDataForOwnership.entity_id ==='orgChartmainEntity'" ng-class="popOverEditDataForOwnership.entity_id ==='orgChartmainEntity' ? 'c-ban':''" >
                <span class="label" ng-hide = "popOverEditDataForOwnership.entity_type && popOverEditDataForOwnership.entity_type === 'person'">Company Name</span>
                <span class="label" ng-show = "popOverEditDataForOwnership.entity_type && popOverEditDataForOwnership.entity_type === 'person'">Person Name</span>
                <p class=" width-100  text-center text-coral-red f-10 mar-b0 " ng-if="personForm.companyName.$touched && personForm.companyName.$invalid">*Please Enter Company Name</p>
            </div>
          
                <div class=" mar-b0 mar-x15 bst_input_group  width-20 p-rel ">	
                        <input type="text" autocomplete="no-display"  ng-model="popOverEditDataForOwnership.numberOfShares" maxlength="7" placeholder  class="custom-input text-cream pad-x15 pad-t15 f-12 height-100">
					    <span class="label"> Number Of Shares</span>
                        <p class=" width-100 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0" >*Required Field</p>
                </div>
                <div class="custom-range-wrapper mar-t0 width-20 entity-range-bar" ng-show="editshowDirectIndirectPercentagePerson && popOverEditDataForOwnership.Ubo_ibo !== 'officers'"> 
                        <h3 class="roboto-light text-capitalize mar-b5 mar-t0 text-dark-cream f-12 c-arrow">Direct ownership 
                        </h3>
                       
                        <input type="range" min="1" max= "100"  step="any"  ng-model="popOverEditDataForOwnership.totalPercentage"  class="">
                    </div>
                    <div class="w-30 mar-l5 h-2_4 bst_input_group d-ib mar-b0 border-r3" ng-show="editshowDirectIndirectPercentagePerson && popOverEditDataForOwnership.Ubo_ibo !== 'officers'">
                            <input type="number"  max= "100" step="any" ng-model="popOverEditDataForOwnership.totalPercentage" class="custom-input border-blue-thin-b f-12 h-2_4 mar-0 pad-5"
                                id="">
                        </div>
            </div>
            <div class="d-flex mar-b20 ai-c"> 
                    <div class="width-45 flag-icon jurisdiction-screening bst_input_group p-rel d-block" ng-class="{'modal-pad-left': editCustomSelected.country }" id="input_container">
                        <input type="text" autocomplete="off" ng-model="editCustomSelected.country" placeholder="" uib-typeahead="state as state.jurisdictionOriginalName for state in countryNames | filter:{jurisdictionOriginalName:$viewValue}" typeahead-template-url="customTemplate.html" class="custom-input pad-b5 pad-t15 pad-x15 mar-t0 bg-transparent typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="countrySelected($label,$item)" id="input">
                        <img ng-if="editCustomSelected.country" ng-src="{{editSelectedFlagSource}}" id="input_img">
                        <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Country Name </span>
                            <p class=" width-45 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="!editCustomSelected.country">*Required Field</p>

                    </div>    
                        
                    <div class="width-20 mar-x15 bst_input_group p-rel d-block" ng-if="popOverEditDataForOwnership.entity_type !== 'person'">
                            <div class="select-box-wrapper  height-100" >
                                <!-- <div class="dropdown height-100 p-rel">
                                        <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
                                 	   </select>
                                        <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Entity Type</span>
                                </div> -->
                                <button class="btn btn-primary d-flex jc-sb pad-t15 custom-input pad-x15 border-0 height-100 ai-c width-100 bg-transparent dropdown-toggle" id="menu1" type="button" data-toggle="dropdown" ng-click="countrytoggleDropDown()">
                                        <span class="width-75 f-12 text-left " ><a class="f-12 text-cream"><span></span>{{entitySearchResult.list.recognized_entity.entityType}}</a></span>
                                        <span class="caret"></span></button>
                                        <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)"> Entity Type </span>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="z-index:9999;">
                                        <li class="c-pointer" ng-click="entitySelected(item)" ng-repeat=" item in EditEntityTypeList track by $index"><a>{{item.entityType}}</a></li>
                                    </ul>
                            </div>
                            <p class=" width-25 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="enterName">*Required Field</p>

                        </div>
                        <div class="dropdown bst_input_group width-20 mar-x15 height-100 p-rel" ng-if= "popOverEditDataForOwnership.entity_type == 'person'">
                            <!-- <select class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty" style="-webkit-appearance: menulist !important;"
                                ng-model="editModelClassification.editRoleModel" 
                                role="role" 
                                ng-options="v.label for v in editModelClassification.editRoleData" ng-value="v.label">
                            </select> -->
                            <select ng-model="editModelClassification.editRoleModel" class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty" id="entityOfficeRole" style="-webkit-appearance: menulist !important;">
                                    <option ng-repeat="v in editModelClassification.editRoleData" ng-selected="editModelClassification.editRoleModel == v.label" value="{{v.label}}">{{v.label}}</option>
                            </select>
                            <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Role</span>

                        </div>
                        <div class="custom-range-wrapper mar-t0  width-20 entity-range-bar" ng-if="editshowDirectIndirectPercentagePerson && popOverEditDataForOwnership.Ubo_ibo !== 'officers' ">
                                <h3 class="roboto-light text-capitalize mar-b5 mar-t0 text-dark-cream f-12 c-arrow">Indirect Ownership
                                </h3>
                               
                                <input type="range" min="0" max="100" step="any" ng-model="popOverEditDataForOwnership.indirectPercentage"  class="">
                        </div>
                         <div class="w-30 mar-l5 h-2_4 bst_input_group d-ib mar-b0  border-r3" ng-if="editshowDirectIndirectPercentagePerson && popOverEditDataForOwnership.Ubo_ibo !== 'officers' ">
                                    <input type="number" max= "100" step="any" ng-model="popOverEditDataForOwnership.indirectPercentage" class="custom-input border-blue-thin-b f-12 h-2_4 pad-0"
                                        id="">
                        </div>
                    </div>
            <div class="d-flex mar-b20 ai-c">
                <div class="width-45 mar-b0 mar-r10 website-icon bst_input_group  p-rel d-block" ng-hide = "!popOverEditDataForOwnership.entity_type || popOverEditDataForOwnership.entity_type === 'person'">							
                    <input type="text" autocomplete="no-display" placeholder="website" name="dateofbirth" ng-pattern="/(http(s)?:\\)?([\w-]+\.)+[\w-]+(\[\?%&=]*)?/" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-model ="popOverEditDataForOwnership.hasURL">
                    <span class="label">Website</span>
                    <p class=" width-45  mar-xauto d-ib text-center text-coral-red f-10 mar-b0 ng-scope" ng-show="popOverEditDataForOwnership.hasURL.$invalid">*Please Enter Valid format</p>
                </div>
                <div class="width-45 mar-b0 calender-icon bst_input_group  p-rel d-block" ng-show = "popOverEditDataForOwnership.entity_type === 'person'">							
                        <input type="text" autocomplete="no-display" placeholder="Dob" name="dateofbirth" ng-pattern="/^\d{4}-\d{2}-\d{2}$/" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-model ="popOverEditDataForOwnership.date_of_birth">
                        <span class="label">Date Of Birth</span>
                        <p class=" width-45  mar-xauto d-ib text-center text-coral-red f-10 mar-b0 ng-scope" ng-show="personForm.dateofbirth.$invalid">*Please Enter Date in YYYY-MM-DD format</p>
                    </div>
                   
                    <div ng-show = "popOverEditDataForOwnership.entity_type === 'person'" ng-dropdown-multiselect="" class="mar-x15 bst_input_group  width-20 editClassification" options="editModelClassification.editPersonListData" events="editSelectPersonListData" selected-model="editModelClassification.editPersonModel" extra-settings="personRoleSettings" translation-texts="example5customTexts"></div>
                    <div ng-show = "popOverEditDataForOwnership.entity_type !== 'person'" ng-dropdown-multiselect="" class="bst_input_group mar-x5 width-20 editClassification" options="editModelClassification.editCompanyListData" events="editSelectCompanyListData" selected-model="editModelClassification.editCompanyModel" extra-settings="companyRoleSettings" translation-texts="example5customTexts"></div>
                    <div class="width-20 mar-x5 d-flex ai-c" ng-show = "popOverEditDataForOwnership.entity_type === 'person'">
                        <span class="text-cream f-14">Mark as UBO</span>
                        <label class="switch mar-autol custom-switch-wrapper override-switch bst-switch-wrapper">
                                <input style="" type="checkbox" name="override"
                                        ng-model="editUBOValue" /></span>
                                <span class="slider round">
                                </span>
                        </label>
                    </div> 
                </div>
            <div class="d-flex mar-b20 ai-c bst_border_t">
          
               
                  
                <div class="width-40 source-icon bst_input_group p-rel d-block">
                    <div class="select-box-wrapper  height-100">
                        <!-- <div class="dropdown height-100 p-rel">
                                <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
                                </select>
                                <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Entity Type</span>
                        </div> -->
                        <input 
                        class="custom-input pad-x10 pad-t15 lh-18 height-a mar-r10"
                        autocomplete="no-display" placeholder="source" ng-model="popOverEditDataForOwnership.source_evidence"
						ng-focus="getNewSources()" ng-change= "toggleSourcedropDown(showfiteredSourceList)" ng-click= "toggleSourcedropDown(showfiteredSourceList,'click')">
                        <span class="label">Source</span>
                    <ul class="custom-list searchSource z-99 mxh-140 l-0 pad-10px p-abs item-1 "  ng-show="popOverEditDataForOwnership.source_evidence && showfiteredSourceList">
                        <li class="" ng-repeat="(key, value) in  sourceList | filter:{ sourceName: popOverEditDataForOwnership.source_evidence }" ng-if="value && value.sourceName" ng-click="fillSourceSearchedInput(value,'edit')"  >
                            <span>
                                <i class="fa pad-r10 c-pointer fa-link" ng-if="(value.source_type == 'link')"></i>
                                <i class="fa pad-r10 c-pointer fa-file-word-o" ng-if="(value.source_type == 'doc' || value.source_type == 'docx')"></i>
                                <i class="fa pad-r10 c-pointer fa-file-pdf-o" ng-if="(value.source_type == 'pdf')"></i>
                                <i class="fa pad-r10 c-pointer fa-file-image-o" ng-if="(value.source_type == 'png')"></i>
                                </span> {{value.sourceName}}</li>
                        <%-- <li  ng-click="addSource()">Add new</li> --%>
                    </ul>
                    <p class=" width-100 text-center text-coral-red f-10 mar-t5 " ng-if="popOverEditDataForOwnership.source_evidence === ''">*Please Enter Source </p>
                  <!--   <ul class="custom-list z-99 mxh-140 l-0 pad-10px p-abs item-1"
                        ng-if="showAddNew && (sourceList && sourceList.length == 0)">
                        <li class="" ng-click="saveIndustryOption(compkeys,compkeys.key)">Add new
                        </li>
                    </ul> -->
                </div>
            </div>
                <div class="width-40 mar-b0 mar-x15  bst_input_group  p-rel d-block">							
                    <input type="text" autocomplete="no-display" placeholder="sourceURL"  ng-value="popOverEditDataForOwnership.sourceUrl" ng-model="popOverEditDataForOwnership.sourceUrl" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" http-prefix name="sourceURL" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" >
                    <span class="label">Source URL</span>
                    <span ng-if="personForm.sourceURL.$error.pattern" class="error f-10">Please enter valid url</span> 
                </div>
                <div class="width-20 mar-b0 bst_input_group bst_input_group_r  p-rel d-block">							
                        <!-- <div class="input-group">
                        <input type="text" class="pad-y10 pad-l10 pad-r0 custom-input" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                        <span class="input-group-btn">
                            <button type="button" class="btn bg-transparent  border-0 text-cream btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                        </div> -->
                        <span class="input-group-prepend f-14 text-dark-cream fa fa-calendar"></span>
                          <input type="text" autocomplete="no-display" placeholder="website" ng-value="searchedSourceInputDateValue" ng-model="popOverEditDataForOwnership.from" ng-change="dateFormatValidate(popOverEditDataForOwnership.from)" name="dateofbirth" class="custom-input text-cream pad-b10 pad-t15 f-12 height-100" >
                    <span class="label pad-l15">Published Date</span>
                    <span class="error f-10" ng-if="dateFormatError">Invalid format (yyyy-MM-dd)</span>
                                                            
                </div>
            </div>

            
            <div class="d-flex jc-fe">
                <button type="button" class="bordered-button lh-18 f-14 height-a pull-right" ng-click="saveTheValueOfOwnership(false,editUBOValue)">Cancel</button>
                <button type="button" class="grad-button mar-l10 f-14 lh-20 height-a pull-right" ng-disabled="popOverEditDataForOwnership.name === '' "  ng-class ="popOverEditDataForOwnership.name === '' ? 'c-ban' : ''"  ng-click="saveTheValueOfOwnership(true,editUBOValue)">Update</button>
            </div>
            
        </form></div></div>







    
    <!-- <div >
    <div class="overlay-content p-rel mar-auto clearfix  fixed-message-box">
			<div class="ownership-popover-wrapper right-pop ownership-container  height-100 border-b0">
				<div class="top-content-wrapper pad-b5 border-cream-thin-b">
					<div class="bp-corner-all bt-item-frame bg-transparent">
						<div class="width-100 d-flex detailed-pop-wrapper top-heading">
							<div class="p-rel mar-r5 icon-flag-wrapper">
								<span class="flag-icon mar-x5 text-cream popover-flag flag-icon-squared flag-wrapper flag-pop c-pointer f-9 flag-icon-us"></span>
								<i class="entity-icon fa {{popOverEditDataForOwnership.person_company_icon}}" style="color:#4c9d20"></i>
							</div>
							<div><h3 name="title" class="bp-item orgChartParentEntity shareCubes" style="display:inline-block;">{{popOverEditDataForOwnership.name}}</h3>
								<span class="f-16 c-pointer p-abs t-0 r-0 pull-right text-dark-cream mar-l10"  ng-click="saveTheValueOfOwnership(false)">✖</span>
								<p class="mar-t0 mar-b10 text-cream" style="line-height:14px!important">{{popOverEditDataForOwnership.fulladdress}}</p>
							</div>
						</div>
						<div class="bottom-list-wrapper text-right d-none">
							<ul class="d-flex pad-x5 custom-list ai-c">
								<li class="text-capitalize  street-pop d-none"><i class="fa text-cream fa fa-street-view  c-pointer"></i>sanction alerts<span>(0)</span></li>
								<li class="text-capitalize  user-pop d-none"><i class="fa text-cream fa-exclamation-triangle  c-pointer"></i>sanction alerts<span>(0)</span></li>
								<li class="text-capitalize  globe-pop d-none"><i class="fa text-cream fa-globe  c-pointer"></i>jurisdiction<span>(0)</span></li>
								<li class="text-capitalize  gavel-pop d-none"><i class="fa text-cream fa-gavel  c-pointer "></i>finance news <span>(0)</span></li>
								<li class="text-capitalize  newspaper-pop d-none "><i class="fa text-cream fa-newspaper-o c-pointer"></i>adverse news<span>(0)</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="bottom-content-wrapper border-cream-thin-t">
					<div class="">
				<div style="display: flex;">
					<p class="text-cream d-flex mar-b5 mar-t10  ai-c width-100">
						<span style="max-width: 100%;" class="width-100">Number of shares</span>
						   <input type="number"  ng-model="popOverEditDataForOwnership.numberOfShares"  class="custom-input color-input" style="max-width:100%;background: darkgray;color: black;border-top-left-radius: 5px;border-top-right-radius: 5px;border-bottom-width: 2px;">
					</p>
					<p class="text-cream d-flex mar-b5 mar-t10 width-100 ai-c" style="text-align: center;" ng-if="popOverEditDataForOwnership.entity_type !='person'">
						<span style="max-width: 100%;" class="width-100">Type of entity</span>
						 <select class="selectBoxWidget" ng-model="popOverEditDataForOwnership.entityType" style="-webkit-appearance: none;  appearance: none;"
                                    ng-options="v for v in dropDownDataAttribute">
                                </select>
					</p>
					<p  class="text-cream d-flex mar-b5 mar-t10 width-100 ai-c" style="text-align: center;" ng-if="popOverEditDataForOwnership.entity_type=='person'">
						<span style="max-width: 100%;" class="width-100">UBO</span>
						<label class="switch">
 						 <input type="checkbox" checked>
  						<span class="slider round"></span>
						</label>
					</p>
				</div>
					<ul class="list-unstyled progressbar-list mar-b0" style="display: flex;">
						<li class="progressbar-list-item mar-b5 pad-t0" style="margin-right: 20px;">
						 <div class="custom-range-wrapper no-thumb-border" style="text-align: center;">
								<span style="margin-bottom: 10px; font-size: 14px;color: #c2d0c2;">Direct ownership</span>
                                <input type="range" min="1" max= "100"  step="1"  ng-model="popOverEditDataForOwnership.pop_indirectPercentage"  class="bg-transparent">
                            </div>
						</li>
						<li class="progressbar-list-item mar-b5">
							 <div class="custom-range-wrapper no-thumb-border" style="text-align: center;">
								<span style="margin-bottom: 10px; font-size: 14px;color: #c2d0c2;">Indirect ownership</span>
                                <input type="range" min="0" max="100" step="1" ng-model="popOverEditDataForOwnership.pop_totalPercentage"  class="bg-transparent">
                            </div>
						</li>
					</ul>
				</div>
 				<div class="btn-group mar-autox ">
                <div class="btn btn-blue text-uppercase pad-x10 pad-y5 mar-r5" ng-click="saveTheValueOfOwnership(true)">Yes</div>
                <div class="btn btn-blue pad-x10 pad-y5 border-grey-thin bg-dark-grey radius-0 text-uppercase" ng-click="saveTheValueOfOwnership(false)">Cancel</div>
            </div>
			</div>
       
        </div>
    </div> -->
	</script>

<script type="text/ng-template" id="addOwnership.html">
    <!--  add officer Modal Wrapper Starts  -->
    <div class="risk-overview-modal-wrapper">
                    <div class="modal-header pad-b5">
                        <button type="button" class="closePulseModal width-5 text-cream  pad-x0 pull-right" ng-click="addNewCompanyOwnership(false)" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title width-95 text-overflow">
                            <span id="articleHeader">Add Person/Company</span>
                        </h4>
                    </div>
                    <div class="modal-body">
            
                            <div class="bst-uib-tab-wrapper pad-0 source-ownership-wrapper source-tabs-wrapper">
                                    <uib-tabset  active="entitySearchResult.list.addActiveTab">
                                        <uib-tab index="0" heading="ADD PERSON" ng-click="addEntitytabChange('person')">
                                                <form class="form pad-r0" autocomplete="no-display" name="personInfoForm">
                                                    <div class="d-flex mar-b20 ai-c">
                                                        <div class=" p-rel first-name-icon mar-b0 mar-r5 bst_input_group  width-30">					
                                                            <input type="text" autocomplete="no-display" class="custom-input text-cream pad-t15 pad-x15 f-12 height-100" name ="name" placeholder="First Name" ng-model = "addOwnershipPerson.firstName">
                                                            <span class="label">First Name*</span>
                                                            <p class=" width-100  text-center text-coral-red f-11 mar-t5 ng-scope" ng-if="!addOwnershipPerson.firstName" >*Please Enter First Name</p>
                                                        </div>
                                                        <div class=" mar-b0 bst_input_group  width-20 p-rel ">						
                                                                <input type="text" autocomplete="no-display" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" placeholder="Last Name"  ng-model ="addOwnershipPerson.lastName">
                                                                <span class="label"> Last Name</span>
                                                                <p class=" width-100 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="enterName">*Required Field</p>
                                                            </div> 
                                                            <div class=" mar-b0 mar-x15 bst_input_group  width-20 p-rel ">						
                                                                    <input type="text" autocomplete="no-display"  placeholder class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" maxlength="7" ng-model ="addOwnershipCompany.numberOfShares">
                                                                    <span class="label"> Number Of Shares</span>
                                                                    <p class=" width-100 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="enterName">*Required Field</p>
                                                            </div>
                                                            <div class="custom-range-wrapper mar-t0 width-20 entity-range-bar" ng-if="showDirectIndirectPercentagePerson">
                                                                    <h3 class="roboto-light text-capitalize mar-b10 mar-t0 text-dark-cream f-12 c-arrow">Direct Ownership
                                                                    </h3>
                                                                   
                                                                    <input type="range" min="0" max="100" step="any" ng-model="addOwnershipCompany.totalPercentage" >
                                                            </div>
																 <div class="w-30 mar-l5 mar-t15 h-2_4 bst_input_group d-ib mar-b0 border-r3" ng-if="showDirectIndirectPercentagePerson">
                                                                                    <input type="number" max= "100"  step="any" ng-model="addOwnershipCompany.totalPercentage" class="custom-input f-12 h-2_4 pad-5"
                                                                                        id="">
                                                                                </div>

                                                        </div>
                                                        <div class="d-flex mar-b20 ai-c"> 
                                                            <div class="width-45 flag-icon jurisdiction-screening bst_input_group p-rel d-block" ng-class="{'modal-pad-left': customSelectedAddPerson }" id="input_container">
                                                                <input type="text" autocomplete="off" ng-model="customSelectedAddPerson" placeholder="" uib-typeahead="state as state.jurisdictionOriginalName for state in countryNames | filter:{jurisdictionOriginalName:$viewValue}" typeahead-template-url="customTemplate.html" class="custom-input pad-b5 pad-t15 pad-x15 mar-t0 bg-transparent typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="countrySelected($label,$item,'AddPerson')" id="input">
                                                                <img ng-if="customSelectedAddPerson" ng-src="{{SelectedFlagSourceAddPerson}}" id="input_img">
                                                                <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Country Name </span>
                                                                    <!-- <p class=" width-45 mar-xauto d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="!customSelectedAddPerson">*Required Field</p> -->
                    
                                                                </div>    
                                                                    
                                                                  <!--   <div class="width-20 mar-x15 bst_input_group p-rel d-block">
                                                                        <div class="select-box-wrapper  height-100">
                                                                            <div class="dropdown height-100 p-rel">
                                                                                <select class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty"
                                                                                    ng-model="addOwnershipCompany.entityType" style="-webkit-appearance: none;  appearance: none;"
                                                                                    ng-options="v for v in dropDownDataAttribute">
                                                                                </select>
                                                                                <span class="label flag_select_label p-abs t-0 "> Entity Type</span>

                                                                            </div>




                                                                        </div>
                                                                        <p class=" width-25 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="enterName">*Required Field</p>
                        
                                                                    </div> -->
                                                                    <!-- <div ng-dropdown-multiselect="" class="bst_input_group mar-x15 width-30" options="personRoleData" selected-model="personRoleModel" extra-settings="personRoleSettings"></div> -->
                                                                    <div class="dropdown bst_input_group mar-x15 width-25 height-100 p-rel">
                                                                        <!-- <select class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty" style="-webkit-appearance: menulist !important;"
                                                                            ng-model="addModelClassification.personRoleModel" 
                                                                            ng-options="v.label for v in addModelClassification.personRoleData">
                                                                        </select> -->
                                                                        <select ng-model="addModelClassification.personRoleModel" class="selectBoxWidget custom-input custom-input_select custom-select height-100 pad-t15 pad-b5 pad-x15 text-cream bg-transparent border-0 ng-pristine ng-untouched ng-valid ng-empty" id="addPersonRole" style="-webkit-appearance: menulist !important;">
                                                                                <option ng-repeat="v in addModelClassification.personRoleData" ng-selected="editModelClassification.editRoleModel == v.label" value="{{v.label}}">{{v.label}}</option>
                                                                        </select>
                                                                        <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Role</span>
                                                                    </div>

                                                                    <div class="custom-range-wrapper mar-t0 width-20 entity-range-bar" ng-if="showDirectIndirectPercentagePerson">
                                                                            <h3 class="roboto-light text-capitalize mar-b5 mar-t0 text-dark-cream f-12 c-arrow">InDirect Ownership
                                                                            </h3>
                                                                           
                                                                            <input type="range" min="0" max="100" step="any" class="bg-transparent pad-y5"
                                                                                ng-model="addOwnershipCompany.indirectPercentage" >
                                                                        </div>
                                                                       
																			 <div class="w-30 mar-l5 mar-t15 h-2_4 bst_input_group mar-l24perc d-ib mar-b0 border-r3" ng-if="showDirectIndirectPercentagePerson">
                                                                                    <input type="number" max= "100" step="any" ng-model="addOwnershipCompany.indirectPercentage" class="custom-input f-12 h-2_4 pad-5"
                                                                                        id="">
                                                                                </div>

                                                                </div>
                                                                    <div class="d-flex mar-b20 ai-c">
                                                            <div class="width-45 mar-b0 calender-icon bst_input_group  p-rel d-block">							
                                                                <input type="text" autocomplete="no-display" placeholder="Dob" name="dateofbirth" ng-pattern="/^\d{4}-\d{2}-\d{2}$/" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-model ="addOwnershipCompany.date_of_birth">
                                                                <span class="label">Date Of Birth</span>
                                                                <p class=" width-45  mar-xauto d-ib text-center text-coral-red f-10 mar-b0 ng-scope" ng-show="personInfoForm.dateofbirth.$invalid">*Please Enter Date in YYYY-MM-DD format</p>
                                                            </div>
                                                            <div ng-dropdown-multiselect="" class="bst_input_group width-25 mar-l20 mar-r15 addPerson_CompanyClassification" options="addModelClassification.personListData" selected-model="addModelClassification.personModel" events= "selectPersonListData" extra-settings="personSettings" translation-texts="example5customTexts"></div>
                                                            <div class="width-20 mar-l20 d-flex ai-c">
                                                                <span class="text-cream f-14">Mark as UBO</span>
                                                                <label class="switch mar-autol custom-switch-wrapper override-switch bst-switch-wrapper">
                                                                        <input style="" type="checkbox" name="override" ng-value=""
                                                                                ng-model="overrideValue" ng-click="setUBOvalue(overrideValue)"/></span>
                                                                        <span class="slider round">
                                                                        </span>
                                                                </label>
                                                            </div>
                                                           
                                                        </div>
                                                        <div class="d-flex mar-b20 ai-c bst_border_t">
          
               
                  
                                                                <div class="width-45 source-icon bst_input_group p-rel d-block" >
                                                                    <div class="select-box-wrapper  height-100">
                                                                        <!-- <div class="dropdown height-100 p-rel">
                                                                                <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
                                                                                </select>
                                                                                <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Entity Type</span>
                                                                        </div> -->
                                                                        <input 
                                                                        class="custom-input pad-x10 pad-t15 lh-18 height-a mar-r10"
                                                                        autocomplete="no-display" placeholder="addSource"
                                                                        ng-keyup="sourceSearchInput(addsourceInput.searchedSourceInputPerson,'Person')"
                                                                        ng-model="addsourceInput.searchedSourceInputPerson"
																		ng-focus="getNewSources()"
                                                                        ng-value="searchedSourceInputValPerson">
                                                                        <span class="label">Source</span>
                                                                    <ul class="custom-list searchSource z-99 mxh-140 l-0 pad-10px p-abs item-1"
                                                                        ng-if="showfiteredSourceList && addsourceInput.searchedSourceInputPerson && (fiteredSourceList && fiteredSourceList.length > 0)">
                                                                        <li class="" ng-repeat="sourceNameSearched in fiteredSourceList track by $index"
                                                                            ng-click="fillSourceSearchedInput(sourceNameSearched,'Person')">
                                                                            {{sourceNameSearched}}</li>
                                                                    </ul>
                                                                    <%-- <ul class="custom-list z-99 mxh-140 l-0 pad-10px p-abs item-1"
                                                                        ng-if="showAddNew && (fiteredSourceList && fiteredSourceList.length == 0)">
                                                                        <li class="" ng-click="addSource()">Add new
                                                                        </li>
                                                                    </ul> --%>
                                                                <p class=" width-100  text-center text-coral-red f-10 mar-t5 " ng-if="addsourceInput.searchedSourceInputCompany === '' && addsourceInput.searchedSourceInputPerson === ''">*Please Enter Source </p>

                                                                </div>
                                                            </div>
                                                                <div class="width-30 mar-b0 mar-x15  bst_input_group  p-rel d-block">							
                                                                    <input type="text" autocomplete="no-display" placeholder="sourceURL"  ng-value="searchedSourceURLValuePerson" ng-model="entitySearchResult.list.searchedSourceURLPerson" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/"  name="sourceURL" http-prefix  class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" >
                                                                    <span class="label">Source URL</span>
                                                                    <span ng-if="personInfoForm.sourceURL.$error.pattern" class="error f-10">Please enter valid url</span> 
                                                                </div>
                                                                <div class="width-25 mar-b0  bst_input_group bst_input_group_r  p-rel d-block">							
                                                                        <!-- <div class="input-group">
                                                                        <input type="text" class="pad-y10 pad-l10 pad-r0 custom-input" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                                                                        <span class="input-group-btn">
                                                                            <button type="button" class="btn bg-transparent  border-0 text-cream btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                                                        </span>
                                                                        </div> -->
                                                                        <span class="input-group-prepend f-14 text-dark-cream fa fa-calendar"></span>
                                                                          <input type="text" autocomplete="no-display" placeholder="website"  ng-model="addOwnershipCompany.from" ng-change="dateFormatValidate(addOwnershipCompany.from)" name="dateofbirth" class="custom-input  pad-b10 pad-t15 text-cream f-12 height-100" >
                                                                    <span class="label">Published Date</span>
                                                                    <span class="error f-10" ng-if="dateFormatError">Invalid format (yyyy-MM-dd)</span>                              
                                                                </div>
                                                            </div>
                                                        <div class="d-flex jc-fe">
                                                            <button type="button" class="bordered-button lh-18 f-14 height-a pull-right" ng-hide ="addEntityfrommodal" ng-click="addNewCompanyOwnership(false)">Cancel</button>
                                                            <button type="button" class="bordered-button lh-18 f-14 height-a pull-right" ng-show ="addEntityfrommodal"  ng-click="addNewCompanyOwnershipFromModal(false)">Cancel</button>
                                                            <button type="button" class="grad-button mar-l10 f-14 lh-20 height-a pull-right"  ng-hide ="addEntityfrommodal" ng-disabled="disabling(addOwnershipPerson.firstName,'customSelectedAddPerson')" ng-click ="addNewCompanyOwnership(true,countryJurisdiction.jurisdictionOriginalName,searchedSourceInputValPerson)">Add</button>
                                                            <button type="button" class="grad-button mar-l10 f-14 lh-20 height-a pull-right"  ng-show ="addEntityfrommodal" ng-click ="addNewCompanyOwnershipFromModal(true,countryJurisdiction.jurisdictionOriginalName,searchedSourceInputValPerson)">Add</button>
                                                        </div>
                                                        
                                                    </form>
                                        </uib-tab>
                                        <uib-tab index="1" heading="ADD COMPANY" ng-click="addEntitytabChange('organization')">
                                                <form class="form pad-r0" autocomplete="no-display" name="personForm">
                                                        <div class="d-flex mar-b20 ai-c">
                                                            <div class=" p-rel company-icon mar-b0 bst_input_group  width-45 ">					
                                                                <input type="text" autocomplete="no-display" class="custom-input text-cream pad-t15 pad-x15 f-12 height-100" ng-model="addOwnershipCompany.name"  placeholder=" ">
                                                                <span class="label">Company Name</span>
                                                                <p class=" width-100  text-center text-coral-red f-10 mar-t5 " ng-if="addOwnershipCompany.name === '' ">*Please Enter Company Name</p>
                                                            </div>
                                                          
                                                                <div class=" mar-b0 mar-x15 bst_input_group  width-25 p-rel ">						
                                                                        <input type="text" autocomplete="no-display" placeholder class="custom-input text-cream pad-x15 pad-t15 f-12 height-100"  maxlength="7" ng-model="addOwnershipCompany.numberOfShares" placeholder=" " >
                                                                        <span class="label"> Number Of Shares</span>
                                                                        <p class=" width-100 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0" >*Required Field</p>
                                                                </div>
                                                                <div class="custom-range-wrapper mar-t0 width-20 entity-range-bar" ng-if="showDirectIndirectPercentageCompany">
                                                                        <h3 class="roboto-light text-capitalize mar-b5 mar-t0 text-dark-cream f-12 c-arrow">indirect ownership
                                                                        </h3>
                                                                        <input type="range" min="0" max="100" step="any" ng-model="addOwnershipCompany.indirectPercentage"  class="bg-transparent pad-y5">
                                                                    </div>
                                                                   <div class="w-30 mar-l5 mar-t15 h-2_4 bst_input_group d-ib mar-b0 border-r3" ng-if="showDirectIndirectPercentageCompany">
                                                                                    <input type="number" max= "100" step="any" ng-model="addOwnershipCompany.indirectPercentage" class="custom-input f-12 h-2_4 pad-5"
                                                                                        id="">
                                                                 </div>
                                                        </div>
                                                            <div class="d-flex mar-b20 ai-c"> 
                                                                <div class="width-45 flag-icon jurisdiction-screening bst_input_group p-rel d-block" ng-class="{'modal-pad-left': customSelectedAddCompany }" id="input_container">
                                                                    <input type="text" autocomplete="off" ng-model="customSelectedAddCompany" placeholder="" uib-typeahead="state as state.jurisdictionOriginalName for state in countryNames | filter:{jurisdictionOriginalName:$viewValue}" typeahead-template-url="customTemplate.html" class="custom-input pad-b5 pad-t15 pad-x15 mar-t0 bg-transparent typehead-dropdown" typeahead-show-hint="true" typeahead-min-length="0" typeahead-on-select="countrySelected($label,$item,'AddCompany')" id="input">
                                                                    <img ng-if="customSelectedAddCompany" ng-src="{{SelectedFlagSourceAddCompany}}" id="input_img">
                                                                    <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Country Name </span>
                                                                        <!-- <p class=" width-45  mar-xauto d-ib text-center text-dark-red f-11 mar-t5 ng-scope" ng-if="!customSelectedAddCompany">*Required Field</p> -->
                        
                                                                </div>    
                                                                        
                                                                        <div class="width-25 mar-x15 bst_input_group p-rel d-block">
                                                                            <div class="select-box-wrapper  height-100">
                                                                                <div class="dropdown height-100 p-rel">
                                                                                    <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" id="addCompanyEntityType"
                                                                                        ng-model="addOwnershipCompany.entityType" ng-options="v.entityType for v in addEntityTypeList">
                                                                                    </select>
                                                                                    <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Entity Type</span>
                                                                                </div>
                                    
                                                                            </div>
                                                                            <p class=" width-25 d-none mar-xauto d-ib text-center text-dark-cream f-10 mar-b0 ng-scope" ng-if="enterName">*Required Field</p>
                            
                                                                        </div>
                                                                        <div class="custom-range-wrapper mar-t0 width-20 entity-range-bar" ng-if="showDirectIndirectPercentageCompany">
                                                                                <h3 class="roboto-light text-capitalize mar-b5 mar-t0 text-dark-cream f-12 c-arrow">Direct Ownership
                                                                                </h3>
                                                                               
                                                                                <input type="range" min="0" max="100" step="any" ng-model="addOwnershipCompany.totalPercentage" class="bg-transparent pad-y5"
                                                                                    >
                                                                            </div>
                                                                            <div class="w-30 mar-l5 mar-t15 h-2_4 bst_input_group d-ib mar-b0 border-r3" ng-if="showDirectIndirectPercentageCompany"> 
                                                                                    <input type="number" max= "100" step="any" ng-model="addOwnershipCompany.totalPercentage" class="custom-input f-12 h-2_4 pad-5"
                                                                                        id="">
                                                                                </div>
                                                            </div>
                                                            <div class="d-flex mar-b20 ai-c">
                                                                <div class="width-45 mar-b0 mar-r10 website-icon bst_input_group  p-rel d-block">							
                                                                    <input type="text" autocomplete="no-display" placeholder="Website" name="website" ng-pattern="/(http(s)?:\\)?([\w-]+\.)+[\w-]+(\[\?%&=]*)?/" class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" ng-model ="addOwnershipCompany.hasURL">
                                                                    <span class="label">Website</span>
                                                                    <p class=" width-45  mar-xauto d-ib text-center text-coral-red f-10 mar-b0 ng-scope" ng-show="addOwnershipCompany.hasURL.$invalid">*Please Enter Date in Valid format</p>
                                                                </div>
                                                                <div ng-dropdown-multiselect="" class="bst_input_group mar-x5 width-25 addPerson_CompanyClassification" options="addModelClassification.companyListData" selected-model="addModelClassification.companyModel" events="selectCompanyListData" extra-settings="companySettings" translation-texts="example5customTexts"></div>
                                                            </div>
                                                            <div class="d-flex mar-b20 ai-c bst_border_t">
          
               
                  
                                                                    <div class="width-45 source-icon bst_input_group p-rel d-block">
                                                                        <div class="select-box-wrapper  height-100">
                                                                            <!-- <div class="dropdown height-100 p-rel">
                                                                                    <select class="selectBoxWidget height-100 pad-x15 bg-transparent border-0 custom-input text-cream custom-select pad-t15 pad-b5" ng-model="entitySearchResult.list.recognized_entity" ng-options="item.entityType for item in EditEntityTypeList">
                                                                                    </select>
                                                                                    <span class="label p-abs t-0 flag_select_label" style="color:rgba(251,251,251,.6)">Entity Type</span>
                                                                            </div> -->
                                                                            <input 
                                                                            class="custom-input pad-x10 pad-t15 lh-18 height-a mar-r10"
                                                                            autocomplete="no-display" placeholder="addSource"
                                                                            ng-keyup="sourceSearchInput(addsourceInput.searchedSourceInputCompany,'Company')"
                                                                            ng-model="addsourceInput.searchedSourceInputCompany"
																			ng-focus="getNewSources()"
                                                                            ng-value="searchedSourceInputValCompany">
                                                                            <span class="label">Source</span>
                                                                        <ul class="custom-list searchSource z-99 mxh-140 l-0 pad-10px p-abs item-1"
                                                                            ng-if="showfiteredSourceList && addsourceInput.searchedSourceInputCompany && (fiteredSourceList && fiteredSourceList.length > 0)">
                                                                            <li class="" ng-repeat="sourceNameSearched in fiteredSourceList track by $index"
                                                                                ng-click="fillSourceSearchedInput(sourceNameSearched,'Company')">
                                                                                {{sourceNameSearched}}</li>
                                                                        </ul>
                                                                        <%-- <ul class="custom-list z-99 mxh-140 l-0 pad-10px p-abs item-1"
                                                                            ng-if="showAddNew && (fiteredSourceList && fiteredSourceList.length == 0)">
                                                                            <li class=""  ng-click="addSource()">Add new
                                                                            </li>
                                                                        </ul> --%>
                                                                <p class=" width-100  text-center text-coral-red f-10 mar-t5 " ng-if="addsourceInput.searchedSourceInputCompany === '' && addsourceInput.searchedSourceInputPerson === ''">*Please Enter Source </p>
                                                                    </div>
                                                                </div>
                                                                    <div class="width-25 mar-b0 mar-x15  bst_input_group  p-rel d-block">							
                                                                        <input type="text" autocomplete="no-display" placeholder="sourceURL"  ng-value="searchedSourceURLValueCompany" ng-model="entitySearchResult.list.searchedSourceURLCompany" name="sourceURL" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" http-prefix class="custom-input text-cream pad-x15 pad-t15 f-12 height-100" >
                                                                        <span class="label">Source URL</span>
                                                                         <span ng-if="personForm.sourceURL.$error.pattern" class="error f-10">Please enter valid url</span> 
                                                                    </div>
                                                                    <div class="width-25 mar-b0  bst_input_group bst_input_group_r  p-rel d-block">							
                                                                            <!-- <div class="input-group">
                                                                            <input type="text" class="pad-y10 pad-l10 pad-r0 custom-input" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                                                                            <span class="input-group-btn">
                                                                                <button type="button" class="btn bg-transparent  border-0 text-cream btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                                                            </span>
                                                                            </div> -->
                                                                            <span class="input-group-prepend f-14 text-dark-cream fa fa-calendar"></span>
                                                                              <input type="text" autocomplete="no-display" placeholder="website" ng-model="addOwnershipCompany.from" ng-change="dateFormatValidate(addOwnershipCompany.from)" name="dateofbirth" class="custom-input text-cream  pad-b10 pad-t15  f-12 height-100"  >
                                                                        <span class="label">Published Date</span>
                                                                        <span class="error f-10" ng-if="dateFormatError">Invalid format (yyyy-MM-dd)</span>                                   
                                                                    </div>
                                                                </div>
                                                            <div class="d-flex jc-fe">
                                                                <button type="button" class="bordered-button lh-18 f-14 height-a pull-right" ng-hide ="addEntityfrommodal" ng-click="addNewCompanyOwnership(false)">Cancel</button>
                                                                <button type="button" class="bordered-button lh-18 f-14 height-a pull-right" ng-show ="addEntityfrommodal" ng-click="addNewCompanyOwnershipFromModal(false)">Cancel</button>
                                                                <button type="submit" class="grad-button mar-l10 f-14 lh-20 height-a pull-right" ng-hide ="addEntityfrommodal" ng-disabled="disabling(addOwnershipCompany.name,'customSelectedAddCompany')" ng-click="addNewCompanyOwnership(true,countryJurisdiction.jurisdictionOriginalName,searchedSourceInputValCompany)">Add</button>
                                                                <button type="submit" class="grad-button mar-l10 f-14 lh-20 height-a pull-right" ng-show ="addEntityfrommodal" ng-disabled="disabling()" ng-click="addNewCompanyOwnershipFromModal(true,countryJurisdiction.jurisdictionOriginalName,searchedSourceInputValCompany)">Add</button>
                                                            </div>
                                                            
                                                        </form>


                                                </uib-tab>
                                    </uib-tabset>					
                                        
            
            
                        <!-- <div class="col-md-6 col-sm-6 col-lg-6">
                            Person <input name="entity-type" type = "radio" ng-model="newOfficer.ScreeeningModal" value="Person"  ng-click= "changeScreeningModal('Person')">
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                            Company <input name="entity-type" type = "radio"  ng-model="newOfficer.ScreeeningModal " value="Company" ng-click= "changeScreeningModal('Company')">
                        </div>
                        <div class="" ng-if= "newOfficer.ScreeeningModal === 'Person'">		
                            
                        </div>
                        <div class="" ng-if=  "newOfficer.ScreeeningModal === 'Company'">		
                        
                        </div> -->
                    </div>
                </div>
                <!--  add officer Modal Wrapper Ends  -->
	</script>

<script>
    $(document).ready(function () {
        /*Custom Scroll Bar*/
        $('.widget-content').mCustomScrollbar({
            axis: "y",
            theme: "minimal-dark"
        });
        $('.main-content-descriptionScrollCls').mCustomScrollbar({
            axis: "y",
            theme: "minimal-dark"
        });

    });
    $(document).ready(function () {
        $(".db-util-process-tab-discover").mThumbnailScroller({
            axis: "x"
        });
        $(".item-submmenu-list, .custom-commitee-tabs > .nav-tabs , .custom-fraud-tabs > .nav-tabs")
            .mThumbnailScroller({
                axis: "x"
            });
    });
    $('.entity-result-item').mCustomScrollbar({
        axis: "y"
    });
    $('.ratios-ratio-chart-wrapper').mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    setTimeout(function () {
        $('#politicalOrgsWrapper').mCustomScrollbar({
            axis: "y"
        });
        $('.media-custom-scroll').mCustomScrollbar({
            axis: "y"
        });
    }, 1000);
    $(
        '.entityCompanyNew-inner-wrapper .entity-company-content-right, .latest-news-section-item, .employee-academics-chart-wrapper'
    ).mCustomScrollbar({

        axis: "y",
        theme: "minimal-dark"
    });
    $('.bloomberg-stock-inner-wrapper').mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $('.holders-table-wrapper').mCustomScrollbar({
        axis: "y"
    });
    $(".panel .panel-scroll").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $(".panel .panel-h-scroll").mCustomScrollbar({
        axis: "xy",
        theme: "minimal-dark"
    });

    $(".panel .panel-accordian").mCustomScrollbar({
        axis: "y",
        theme: "minimal",
        callbacks: {
            onScrollStart: function () {
                angular.element('.panel .panel-accordian').scope().screeningLoadmore();
            }
        }

    });
</script>
<script type="text/ng-template" id="cyberNewsTemplate.html">
    <div class="panel bg-transparent  custom-panel-wrapper" id="popOver" ng-if="cyberNews.length> 0">

                <div class="panel-body pad-x15 pad-b15" ng-repeat="news in cyberNews">
                    <a href="{{news.url}}" target="_blank">
                        <p class="content-heading text-uppercase text-dark-blue">{{ news.title }}</p>

                    </a>
                    <p ng-if="news.technologies[0].type">Technology: {{news.technologies[0].type}}</p>
                    <p>{{news.text}}</p>
                    <p>{{news.published}}</p>
                    <p>{{news.location.name}}</p>
                </div>
                <span ng-show="AdverseNewsPreloader" class="custom-spinner">
                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                </span>
            </div>
        </script>
<script type="text/ng-template" id="idv.questionaire.modal.html">
    <div id="onboardinghere" ng-if="showOnBoardingQuestionary">
	<div class="modal-header text-center">
	   <h3 class="modal-title" ng-if="!openCheckList">Onboarding Questionnaire</h3>
	    <h3 class="modal-title" ng-if="openCheckList">Compliance Checklist</h3>
	   <button type="button" class="close" ng-click="idvQuestionaire.closeOnBoardingQuestionnaireModal()" style="position: absolute; right:20px; top: 20px;">&times;</button>
	</div>
	<div class="iframe-wrapper">
   		<iframe class="iframe1" ng-src="{{idvQuestionaire.filterUrl()}}" />
	</div>
</div>
<div class="intro-onboarding" ng-if="showOnBoardingIntro">
   <div class="intro-onboarding-inner-wrapper">
      <h3>KNOW YOUR CUSTOMER</h3>
      <h4>Please confirm your consent for participation</h4>
      <h5 style="">
         Terms & Condition
      </h5>
      <p class="onboarding-terms">
         Welcome to our Questionnaire. The questionnaire on this website, was designed by BST in accordance 
         with the company policy. By using this questionnaire you are agreeing to comply with and be bound 
         by the following terms and conditions of use, which together with our privacy policy govern 
         Blackswan-techologies\' relationship with you in relation to this website. If you disagree with 
         any part of these terms and conditions, please do not use our website. The term [Customer\'s name] 
         refers to the owner of the data whose registered office is [address]. Our company registration 
         number is [company registration number and place of registration]. The term \'you\' refers to 
         the user or viewer of our website. The use of this website is subject to the following terms 
         of use: The content of the pages of this website is for your general information and use only.
         It is subject to change without notice. This website uses cookies to monitor browsing preferences. 
         If you do allow cookies to be used, the following personal information may be stored by us for use
         by third parties: [insert list of information]. Neither we nor any third parties provide any warranty
         or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information 
         and materials found or offered on this website for any particular purpose. You acknowledge that such
         information and materials may contain inaccuracies or errors and we expressly exclude liability for 
         any such inaccuracies or errors to the fullest extent permitted by law. Your use of any information
         or materials on this website is entirely at your own risk, for which we shall not be liable. It shall
         be your own responsibility to ensure that any products, services or information available through
         this website meet your specific requirements. This website contains material which is owned by or
         licensed to us. This material includes, but is not limited to, the design, layout, look, appearance
         and graphics. Reproduction is prohibited other than in accordance with the copyright notice, 
         which forms part of these terms and conditions. All trade marks reproduced in this website which are
         not the property of, or licensed to, the operator are acknowledged on the website. Unauthorised 
         use of this website may give rise to a claim for damages and/or be a criminal offense. From time 
         to time this website may also include links to other websites. These links are provided for your 
         convenience to provide further information. They do not signify that we endorse the website(s).
         We have no responsibility for the content of the linked website(s). Your use of this website and 
         any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland,
         Scotland and Wales.
      </p>
      <div class="intro-onboarding-footer">
         <a href="javascript:void(0);" ng-click="idvQuestionaire.closeidvQuestionaireModal()" id="closemodal">Cancel</a>
         <a href="javascript:void(0);" ng-click="idvQuestionaire.onClickAgree()" id="agree"><i class="fa fa-check-circle"></i> Agree</a>
      </div>
   </div>
</div>

<script>
	$('.onboarding-terms').mCustomScrollbar({
		axis : "y"
	});
	$('.iframe-wrapper').mCustomScrollbar({
		axis : "y",
		theme : "minimal-dark"
	});
</script>
</script>

<script type="text/ng-template" id="digitalAssestsTemplate.html">
    <div class="panel tab-wrapper custom-uib-tab-wrapper bg-transparent  custom-panel-wrapper" ng-if="digiHeaders.length>0">
                <uib-tabset active="active">
                    <uib-tab ng-repeat="tab in digiHeaders" heading="{{tab}}">
                        <div class="panel-body pad-10px" id="DApopOver">
                            <p ng-repeat="source in digi_obj[tab] track by $index">
                                <a href="{{source}}" class=" text-dark-blue" target="_blank">{{source}}</a>
                            </p>
                        </div>
                    </uib-tab>
                </uib-tabset>
            </div>
        </script>
<script type="text/ng-template" id="overrideAnswerTemplate.html">
    <div class="col-sm-12 pad-x0">
                <div class="primary-section">
                    <p style="">
                        <b>Form Data:</b> {{currentAns}}</p>
                    <p style="" class="mar-b5">
                        <b>OSINT Data:</b> {{osnitAns}}</p>
                    <p style="" ng-if="fromCompanyHouse">
                        <a class="width-100 text-overflow" href="https://beta.companieshouse.gov.uk/company/{{ currentCompanyId }}" target="_blank">https://beta.companieshouse.gov.uk/company/{{ currentCompanyId }}</a>
                    </p>
                    <span>
                        <a ng-click="overrideAnswer()">Override Answer</a>
                    </span>
                </div>
            </div>
        </script>
<script type="text/ng-template" id="overrideAnsGreenGlobe.html">
    <div class="col-sm-12 pad-x0">
                <div class="primary-section" ng-if="!showTopEvents">
                    <p style="">
                        <b>Form Data:</b> {{ greenCurrentAns }}</p>
                    <p style="" class="mar-b5">
                        <b>OSINT Data:</b> {{ greenOsnit }}</p>
                    <p ng-if="fromCompanyHouseverified" style="">
                        <a class="width-100 text-overflow" href="https://beta.companieshouse.gov.uk/company/{{ currentCompanyId }}" target="_blank">https://beta.companieshouse.gov.uk/company/{{ currentCompanyId }}</a>
                    </p>
                </div>
                <div class="primary-section mxh-300" ng-if="showTopEvents">
                    <div class="panel-body pad-x15 pad-b15 " ng-repeat="news in currentEvents">
                        <a href="{{news.url}}" target="_blank">
                            <p class="content-heading mar-b5 text-uppercase text-dark-blue">{{ news.title }}</p>

                        </a>

                        <p class="mar-b5">{{news.published | date : 'dd/MM/yyyy'}}</p>
                        <p class="details-wrapper">{{news.text}}</p>
                    </div>
                </div>
            </div>
        </script>
<script type="text/ng-template" id="incidentsByIndustryTemplate.html">
    <div class="col-sm-12 pad-x0">
                <div class="primary-section">
                    <p class="text-uppercase mar-b5">
                        {{ industriesName }}</p>
                    <p class="text-uppercase mar-b5">
                        <b>Number of incidents: </b> {{ incidentsByIndustryCount }}</p>
                    </p>
                </div>
            </div>
        </script>
<script type="text/ng-template" id="topCountriesTemplate.html">
    <div class="col-sm-12 pad-x0">
                <div class="primary-section">
                    <p style="text-transform: uppercase;">
                        {{ topCountriesName }}</p>
                    <p style="text-transform: uppercase;">
                        <b>Number of incidents: </b> {{ incidentsByTopCountriesNameCount }}</p>
                    </p>
                </div>
            </div>
        </script>
<script type="text/ng-template" id="refreshQuestionnaireTemplate.html">
    <div class="comments-modal-wrapper">
                <div class="modal-body pad-b5">
                    Due to questions remapping, some of the evaluations might be cleared. Are you sure you wish to proceed?
                </div>
                <div class="modal-footer pad-y0 border-0">
                    <button type="submit" class="btn btn-primary btn-cancel" ng-click="closePopover('ok')">Ok</button>
                    <button type="button" class="btn btn-default btn-cancel" ng-click="closePopover('close')">Cancel</button>
                </div>
            </div>
        </script>
        <script type="text/ng-template" id="activateScreening.html">
         <div class="panel bg-transparent border-0 mar-0 custom-panel-wrapper" id="">
                <div class="panel-body d-flex ai-c jc-sb pad-x0 pad-y0" >
                <div class="description-wrapper"><ul class="custom-list pad-l0 entity-selection-list d-flex ai-c"><li class="border-b0 pad-l0"><span class="roboto-light text-cream f-14"><span class="mar-r5">3</span>Entities Selected </span></li><li clas="as-fe"><a class="text-dark-blue text-uppercase">Select all</a></li><li ><a class="text-dark-blue text-uppercase">Deselect all</a></li></ul>
                </div>
               <div class="screening-button-wrapper d-flex ai-c mar-autol">   <button class="grad-button mar-t0 mar-r10" ><i class="fa fa-play mar-r5"></i>Activate</button><button class="grad-button mar-t0"> <i class="fa fa-trash mar-r5"></i>Remove</button></div></div>
    
                                
                <span ng-show="AdverseNewsPreloader" class="custom-spinner">
                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                </span>
            </div>
        </script>
        <script type="text/ng-template" id="ownership.html">
            <div class="panel bg-transparent border-0 mar-0 custom-panel-wrapper" id="">
                <div class="panel-body pad-x0 pad-y0" >
                        <ul class="custom-list pad-l0 item-3 d-none">
                                <li class="text-cream">	<div class="bst-checkbox-wrapper d-ib width-a" ng-click = "checkallroles(Rolechecklist.all_checkedRole)">
                                        <label class="text-cream checkbox-inline" >
                                            <input type="checkbox" ng-model ="Rolechecklist.all_checkedRole"  ng-checked = "Rolechecklist.all_checkedRole" >
                                            <span>
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </label>
                                        <span class="text-cream lh-22 va-m roboto-light ">All</span>
                                    </div><span class="c-pointer pull-right" ng-click = "clearallrole()">Clear All </span>
                                </li></ul>
                    <ul class="custom-list mxh-140 scroll-list pad-r15 pad-l0 item-1"  >                       
                        <li ng-repeat = "officername in unique_officer_roles_array | orderBy : 'name'" class="border-0 text-cream" ng-click = "singleroleselection(officername.name,officername,$event)">
                            <div class="custom-checkbox d-ib width-a" >
                                <label class="text-cream checkbox-inline" >
                                    <input type="checkbox" ng-checked="officername.value">
                                    <span>
                                        <i class="fa fa-check" ng-show= "officername.value"></i>
                                    </span>
                                </label>
                                <span class="text-cream lh-22 va-m roboto-light ">{{officername.name}}</span>
                            </div>
                        </li>

                    </ul><div><button class="grad-button mar-r10" ng-click = "roleselection()">Apply</button><button class="bordered-button" ng-click = "screeningtoggle()">cancel</button></div>

                                 </div>
                <span ng-show="AdverseNewsPreloader" class="custom-spinner">
                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                </span>
            </div>
        </script>
<script type="text/ng-template" id="ownershipSources.html">
            <div class="panel bg-transparent border-0 mar-0 custom-panel-wrapper" id="">
            <div class="panel-heading d-flex bg-transparent pad-x0">Sources <span class="mar-autol text-dark-cream"><i class="fa mar-r5 fa-link"></i>{{entitySearchResult.list.selectedScreeningRow.sources.length}}</span></div>
                <div class="panel-body pad-x0 pad-y0" >
                    <ul class="custom-list mxh-140 scroll-list pad-r15 pad-l0 item-1"  >                       
                        <li ng-repeat = "source in entitySearchResult.list.selectedScreeningRow.sources" class="border-0 pad-t0 text-cream">
                        <div class="custom-radio mar-y0 white d-ib d-flex ai-c  width-a" ng-click = "setTheSourceManager(source)">
                                <label class=" clearfix square-18 mar-r10 pad-0 mar-0 d-flex ai-c blue" >
                                    <input type="radio" ng-checked ="entitySearchResult.list.selectedScreeningRow.source_evidence === source" name ="sources" >
                                    <span class="pad-0 mar-0 square-18">
                                        <i class="fa  fa-circle"></i>
                                    </span>
                                </label>
                                <span class="text-cream c-arrow f-16 lh-22 va-m roboto-light">{{source}}</span>
                            </div></li>

                    </ul><div><button class="grad-button mar-r10" ng-class =" entitySearchResult.list.keymangerapplybutton ?'c-ban' : ''" ng-click = "selectedkeymanagerSourceApply()" ng-disabled = "entitySearchResult.list.keymangerapplybutton">Apply</button><button class="bordered-button" ng-click = "screeningtoggle()">cancel</button></div>

                                 </div>
                <span ng-show="AdverseNewsPreloader" class="custom-spinner">
                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                </span>
            </div>
        </script>
<script>

    $(" .analyze-panel-wrapper .action-icon-list li+.popover .primary-section, .scroll-list").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });

    $(document).ready(function () {
        setTimeout(function () {
            $("#scroll1").mThumbnailScroller({
                axis: "x"
            });
        }, 500);
    });

    $(document).ready(function () {
        $("body").on("click", ".customScrollForSelect", function () {
            $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect").addClass(
                "wrapperScroll");
            $(
                ".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul.dropdown-menu"
            ).addClass("scrollbar style-1");
        });
        $("body").on("click",
            ".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul li:nth-child(1)",
            function () {
                console.log("working");
                $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect").removeClass(
                    "open");
                $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect button")
                    .removeAttr(
                        "disabled");
                $(
                    ".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul.dropdown-menu"
                ).css("display", "none");
                $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul li").css(
                    "display", "none");
            });
        $("body").on("click",
            ".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul li:nth-child(2)",
            function () {
                $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect").removeClass(
                    "open");
                $(
                    ".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul.dropdown-menu"
                ).css("display", "none");
                $(".customScrollForSelect .multiselect-parent.btn-group.dropdown-multiselect ul li").css(
                    "display", "none");
            });
    });
</script>
