<!-- entityPerson.jsp -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Spinner Starts  -->
<div class="custom-spinner full-page-spinner" ng-if="disableSearchButton">
    <i class="fa fa-spinner fa-spin fa-3x"></i>
</div>
<!--  Spinner Ends  -->

<!--  Entity Person Starts  -->
<div id="entity-company" class="entity-person-wrapper">

    <!--  Entity Person Content Starts  -->
    <div id="entity-company-content" class="entity-page">
        <div class="entity-person-content-inner-wrapper entityPersonNew-inner-wrapper">

            <!--  Entity Person Content Left Starts  -->
            <div class="entity-person-content-left">

                <!--  Entity Results Wrapper Starts  -->
                <div class="container-fulid entity-results-wrapper">
                    <div class="custom-row">

                        <!--  Entity Person Intro Starts  -->
                        <div class="custom-col entity-person-intro entity-person-new-intro">
                            <div class="entity-person-image static-logo-img">
                                <a href="javascript:void(0);" target="_blank">
                                    <img class="img-responsive" ng-src="{{ entitySearchResult.image || entitySearchResult.list['truecaller'][0].image ||entitySearchResult.list.forbes.details[0].image || '../assets/images/dummy-logo.png'}}"></img>
                                </a>
                            </div>
                            <div class="entity-person-description clearfix">
                                <a class="read-more mar-r10" ng-click="readmoreModal.overviewPersonModal();">READ MORE</a>
                                <h2 class="person-title">{{entitySearchResult.name}}</h2>
                                <div class="main-content-description main-information-content">
                                    <div class="highlighting-info-wrapper">
                                        <ul class="list-inline clearfix">
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.list.forbes.details[0].current_position|| entitySearchResult.works_for || entitySearchResult.list['previous_work_places'][0]['title'] || entitySearchResult.current_company || entitySearchResult.job || entitySearchResult.job_title || entitySearchResult.occupation || entitySearchResult.list['corporationwiki'].current_job}}">{{entitySearchResult.list.forbes.details[0].current_position|| entitySearchResult.works_for || entitySearchResult.list["previous_work_places"][0]['title'] || entitySearchResult.current_company || entitySearchResult.job || entitySearchResult.job_title || entitySearchResult.occupation || entitySearchResult.list['corporationwiki'].current_job || 'N/A'}}
                                                        </span>

                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.list['previous_work_places'][0]['company']|| entitySearchResult.current_current_work_place || entitySearchResult.work_location || entitySearchResult.business_type || entitySearchResult.list['corporationwiki'].location || entitySearchResult.list.forbes.details[0].residence }}"> {{entitySearchResult.list["previous_work_places"][0]['company']|| entitySearchResult.current_current_work_place || entitySearchResult.work_location || entitySearchResult.business_type || entitySearchResult.list['corporationwiki'].location|| entitySearchResult.list.forbes.details[0].residence ||'N/A'}}</span>
                                                    </p>
                                                    <p  title="Birth Place: {{entitySearchResult.list.geni.timeline[0].birthdate + ' ' + entitySearchResult.list.geni.timeline[0].birthplace|| entitySearchResult.list['born_place']}}">
                                                        <span class="key">Birth Place:</span>
                                                        <span class="value">{{entitySearchResult.list.geni.timeline[0].birthdate + ' ' + entitySearchResult.list.geni.timeline[0].birthplace||entitySearchResult.list['born_place'] || 'N/A'}}</span>
                                                    </p>
                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.list['has_education'][0].school_name}}">{{entitySearchResult.list["has_education"][0].school_name|| 'N/A'}}</span>
                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.address|| entitySearchResult.address_city || entitySearchResult.location || entitySearchResult.place_of_birth ||
																	entitySearchResult.residence || entitySearchResult.list['people_details'][0].address_city || entitySearchResult.list['truecaller'][0].location}}"> {{entitySearchResult.address|| entitySearchResult.address_city || entitySearchResult.location || entitySearchResult.place_of_birth ||
																	entitySearchResult.residence || entitySearchResult.list['people_details'][0].address_city || entitySearchResult.list['truecaller'][0].location || 'N/A'}}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.list['aliases']}}">Aliases</span>
                                                        <span class="value">{{entitySearchResult.list['aliases']|| 'N/A'}}</span>
                                                    </p>
                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.followers_count|| '100'}}">Followers: {{entitySearchResult.followers_count|| '100'}} </span>
                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.following_count|| '12'}}">Following: {{entitySearchResult.following_count|| '12'}} </span>
                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.person_more_info.net_worth}}"> Net Worth: </span>
                                                        <span  class="value" title="{{entitySearchResult.person_more_info.net_worth}}"> {{entitySearchResult.person_more_info.net_worth|| 'N/A'}} </span>
                                                    </p>
                                                </div>
                                            </li>
                                            <li class="highlighting-list">
                                                <div class="highlighting-info">
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.list['employment_history'][0].email|| entitySearchResult.list['hunterData'].email || entitySearchResult.list['badbuyerslist'][0].email || entitySearchResult.list['truecaller'][0].email}}">Email:</span>
                                                        <span title="{{entitySearchResult.list['employment_history'][0].email|| entitySearchResult.list['hunterData'].email || entitySearchResult.list['badbuyerslist'][0].email || entitySearchResult.list['truecaller'][0].email}}"> {{entitySearchResult.list["employment_history"][0].email|| entitySearchResult.list["hunterData"].email || entitySearchResult.list['badbuyerslist'][0].email || entitySearchResult.list['truecaller'][0].email || 'N/A'}}</span>
                                                    </p>
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.person_more_info.website|| entitySearchResult.website || entitySearchResult.list['similar_profiles_list'][0].website}}">Website:</span>
                                                        <a class="value" target="_blank" href="{{entitySearchResult.person_more_info.website|| entitySearchResult.website || entitySearchResult.list['similar_profiles_list'][0].website}}" title="{{entitySearchResult.person_more_info.website|| entitySearchResult.website || entitySearchResult.list['similar_profiles_list'][0].website}}"> {{entitySearchResult.person_more_info.website|| entitySearchResult.website || entitySearchResult.list['similar_profiles_list'][0].website || 'N/A'}}</a>
                                                    </p>
                                                    <!-- <p>
                                                            <span class="key" title="{{entitySearchResult.work_telephone || entitySearchResult.list['employment_history'][0].phone || entitySearchResult.list['badbuyerslist'][0].phone_number || entitySearchResult.list['similar_profiles_list'][0].phone || entitySearchResult.list['people_details'][0].telephone}}" > Phone: </span>
                                                            <span  title="{{entitySearchResult.work_telephone || entitySearchResult.list['employment_history'][0].phone || entitySearchResult.list['badbuyerslist'][0].phone_number || entitySearchResult.list['similar_profiles_list'][0].phone || entitySearchResult.list['people_details'][0].telephone}}" class="value"> {{entitySearchResult.work_telephone || entitySearchResult.list['employment_history'][0].phone || entitySearchResult.list['badbuyerslist'][0].phone_number || entitySearchResult.list['similar_profiles_list'][0].phone || entitySearchResult.list['people_details'][0].telephone || 'N/A'}} </span>
                                                    </p> -->
                                                    <p>
                                                        <span class="key" title="{{entitySearchResult.fax}}"> Fax Number: </span>
                                                        <span  class="value" title="{{entitySearchResult.fax}}"> {{entitySearchResult.fax|| 'N/A'}} </span>
                                                    </p>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  Entity Person Intro Ends  -->

                        <!--  Entity Contents Wrapper Starts  -->
                        <div class="custom-col entity-person-new-desc">
                            <div class="innerSidebarToggle">
                                <!--  Custom TABS Section Wrapper Starts  -->
                                <div class="entity-tabs-wrapper">
                                    <div class="tabs-section">
                                        <ul class="nav nav-tabs">
                                            <li ng-class="{'active': mainInfoTabType === 'Overview'}">
                                                <a href ng-click="onClickInfoTabs('Overview')">
                                                    <i class="fa fa-globe"></i>
                                                    <span class="main-nav-texts">Overview</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Duediligence'}">
                                                <a href ng-click="onClickInfoTabs('Duediligence')">
                                                    <i class="fa fa-search"></i>
                                                    <span class="main-nav-texts">Customer due diligence</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Career'}">
                                                <a href ng-click="onClickInfoTabs('Career')">
                                                    <i class="fa fa-bank"></i>
                                                    <span class="main-nav-texts">Career</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Academics'}">
                                                <a href ng-click="onClickInfoTabs('Academics')">
                                                    <i class="fa fa-graduation-cap"></i>
                                                    <span class="main-nav-texts">Academics</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Finance'}">
                                                <a href ng-click="onClickInfoTabs('Finance')">
                                                    <i class="fa fa-usd"></i>
                                                    <span class="main-nav-texts">Finance</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Risks'}">
                                                <a href ng-click="onClickInfoTabs('Risks')">
                                                    <i class="fa fa-exclamation-triangle"></i>
                                                    <span class="main-nav-texts">Risks</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Social Media'}">
                                                <a href ng-click="onClickInfoTabs('Social Media')">
                                                    <i class="fa fa-share-alt"></i>
                                                    <span class="main-nav-texts">Social Media</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Transactions'}">
                                                <a href ng-click="onClickInfoTabs('Transactions')">
                                                    <i class="fa fa-money"></i>
                                                    <span class="main-nav-texts">Transactions</span>
                                                </a>
                                            </li>
                                            <li ng-class="{'active': mainInfoTabType === 'Documents'}">
                                                <a href ng-click="onClickInfoTabs('Documents')">
                                                    <i class="fa fa-upload"></i>
                                                    <span class="main-nav-texts">Documents</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <!--  Custom TABS Section Wrapper Ends  -->

                                <div class="entity-contents-wrapper">

                                    <!--   Overview Tab Starts   -->
                                    <div class="overview-tab-wrapper person-wrapper" ng-show="mainInfoTabType === 'Overview'">
                                        <div class="row custom-row">

                                            <!--  General Info Wrapper Starts  -->
                                            <div class="col-md-6 entity-item who-am-i-wrapper pad-left-tab">
                                                <h3>Who Am I</h3>
                                                <!-- <a class="read-more" ng-click="onViewClick('general-info')">Read More</a> -->
                                                <div class="whoweare-result-item">
                                                    <p>{{entitySearchResult.person_more_info.summary|| entitySearchResult.profile_summary +' '+ entitySearchResult.scars_and_marks  + ' ' + ' ' + entitySearchResult.list['truecaller'][0].details || entitySearchResult.summary}}
                                                    </p>
                                                </div>
                                            </div>

                                            <!--  Career Person Starts  -->
                                            <div class="col-md-6 entity-item career-details-wrapper">
                                                <h3>Career Details
                                                    <!-- <a href="javescript:void(0);" class="more-positions"> 3 More Positions</a> -->
                                                </h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['previous_work_places'].length > 0 || entitySearchResult.list['personal_profiles_bloomberg']['career_history'].length > 0 || entitySearchResult.list['employment_history'].length > 0 || entitySearchResult.list['government_positions']['previous_roles'].length > 0 || entitySearchResult.list['associated_companies']['business_positions'].length > 0 || entitySearchResult.list['corporationwiki']['previous_job'].length > 0">
                                                    <ul class="media-list career-details-lists">
                                                        <li class="media" ng-repeat="prework in entitySearchResult.list['previous_work_places']">
                                                            <div class="media-left">
                                                                <a href="{{prework.link}}" target="_blank">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">{{prework.title}}</h4>
                                                                <p>
                                                                    <span class="value">{{prework.company}}</span>
                                                                    <span class="value">{{prework.employed_dates}}</span>
                                                                </p>
                                                                <p>{{prework.employed_location}}</p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['personal_profiles_bloomberg']['career_history']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.job_title}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.company}} | </span>
                                                                    <span class="value">
                                                                        {{entity.time_period}}
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['employment_history']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.job_rank}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.company}} | </span>
                                                                    <span class="key">Email: {{entity.email}}</span>
                                                                </p>
                                                                <p>
                                                                    {{entity.street}} {{entity.city}} {{entity.state}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['government_positions']['previous_roles']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.title}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.dates}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['associated_companies']['business_positions']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.relationship}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.entity_name}} | </span>
                                                                    <span class="key">{{entity.summary}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['corporationwiki']['previous_job']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.companyName}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.location}} | </span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-show="entitySearchResult.list['previous_work_places'].length === 0 && (!entitySearchResult.list['personal_profiles_bloomberg']['career_history'] || entitySearchResult.list['personal_profiles_bloomberg']['career_history'].length === 0) && entitySearchResult.list['employment_history'].length === 0 && (!entitySearchResult.list['government_positions']['previous_roles'] || entitySearchResult.list['government_positions']['previous_roles'].length === 0) && entitySearchResult.list['associated_companies']['business_positions'].length === 0 && entitySearchResult.list['corporationwiki']['previous_job'].length === 0">
                                                    <div class="alert-message loading-message">Loading...</div>
                                                </div>
                                            </div>
                                            <!--  Career Person Ends  -->

                                            <!--  Academic Information Wrapper Starts  -->
                                            <div class="col-sm-6 entity-item academic-info pad-left-tab">
                                                <div class="row custom-row">
                                                    <div class="col-md-12">
                                                        <h3>Academic Information
                                                            <!-- <a href="javescript:void(0);" class="more-positions"> 3 More</a> -->
                                                        </h3>
                                                        <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['has_education'].length > 0">
                                                            <ul class="media-list academic-info-lists">
                                                                <li class="media" ng-repeat="Edu in entitySearchResult.list['has_education']" ng-if="Edu.school_name !== ''">
                                                                    <div class="media-left">
                                                                        <a href="{{Edu.url}}" target="_blank">
                                                                            <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <h4 class="media-heading">{{Edu.school_name}}
                                                                        </h4>
                                                                        <p>
                                                                            <span class="key"> <span ng-if="Edu.start_time !== ''">{{Edu.start_time}}</span> <span ng-if="Edu.end_time !== ''">  -   {{Edu.end_time}}</span> </span>
                                                                            <!-- <span class="value">San Francisco bay Area</span> -->
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['has_education'].length > 0">
                                                            <div class="alert-message loading-message"  ng-if="entitySearchResult.data_not_found.academicinfo">Loading...</div>
                                                            <div class="alert-message"   ng-if="!entitySearchResult.data_not_found.academicinfo">Data Not Found</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Academic Information Wrapper Ends  -->

                                            <!--  Relationship Analysis Wrapper Starts  -->
                                            <div class="col-sm-6 entity-item relationship-analysis">
                                                <h3>Relationship Analysis</h3>
                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphOverview')" class="read-more">Expand</a>
                                                <div class="networkGraphMipMain">
                                                    <div id="networkGraphOverview"></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>
                                                </div>
                                            </div>
                                            <!--  Relationship Analysis Wrapper Ends  -->

                                            <!--  Instagram Update Wrapper Starts  -->
                                            <div class="col-md-6 entity-item instagram-update-wrapper">
                                                <h3>Instagram Update</h3>
                                                <div class="row">
                                                    <div class="col-md-6" ng-show="entitySearchResult.list.instagramList.length > 0">
                                                        <img class="img-responsive" ng-src="{{entitySearchResult.list['instagramList'][0].image|| '../assets/images/enrich/media-thumbs/person/insta-update.PNG'}}" ng-show="entitySearchResult.list['instagramList'].length > 0" alt="img">
                                                    </div>
                                                    <div class="col-md-6" ng-show="entitySearchResult.list.instagramList.length > 0">
                                                        <p class="custom-instagram-update">{{entitySearchResult.list['instagramList'][0].caption}}
                                                        </p>
                                                        <ul class="list-inline insta-score" >
                                                            <li>
                                                                {{entitySearchResult.list['instagramList'][0].followingCount}} Followings
                                                            </li>
                                                            <li>
                                                                {{entitySearchResult.list['instagramList'][0].followersCount}} Followers
                                                            </li>
                                                            <li>
                                                                <i class="fa fa-heart-o"></i> {{entitySearchResult.list['instagramList'][0].likes_count}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="alert-message-wrapper" ng-hide="entitySearchResult.list.instagramList.length > 0">
                                                        <div class="alert-message loading-message"  ng-if="entitySearchResult.data_not_found.instagram">Loading</div>
                                                        <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.instagram">Data Not Found</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Instagram Update Wrapper Ends  -->

                                            <!--  Family Members Wrapper Starts  -->
                                            <div class="col-md-6 entity-item family-wrapper">
                                                <h3>Family Members</h3>
                                                <div class="entity-item-submmenu"  ng-show="entitySearchResult.list['parents'].length > 0 || entitySearchResult.list['associated_companies']['family'].length > 0 || entitySearchResult.list['associated_companies']['friends'].length > 0 || entitySearchResult.list['relatives'].length > 0 || entitySearchResult.list.geni.family.length > 0 || entitySearchResult.person_more_info['spouse(s)']">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="item-submmenu-list list-inline">
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Total Members
                                                                        <strong>{{entitySearchResult.list['parents'].length + entitySearchResult.list['associated_companies']['family'].length + entitySearchResult.list['associated_companies']['friends'].length + entitySearchResult.list['relatives'].length + spousecount + entitySearchResult.list.geni.family.length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="entity-result-item" ng-show="entitySearchResult.list['parents'].length > 0 || entitySearchResult.list['associated_companies']['family'].length > 0 || entitySearchResult.list['associated_companies']['friends'].length > 0 || entitySearchResult.list['relatives'].length > 0 || entitySearchResult.list.geni.family.length > 0 || entitySearchResult.person_more_info['spouse(s)']">
                                                    <div class="person-table-wrapper">
                                                        <table class="table associated-table">
                                                            <tr ng-repeat="entity in entitySearchResult.list['parents']">
                                                                <td>{{entity}}</td>
                                                                <td>Parents</td>
                                                                <td>Family</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['relatives']">
                                                                <td>{{entity}}</td>
                                                                <td>Relatives</td>
                                                                <td>Family</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['associated_companies']['family']">
                                                                <td>{{entity.name}}</td>
                                                                <td>{{entity.relationship}}</td>
                                                                <td>{{entity.summary|| 'Family'}}</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['associated_companies']['friends']">
                                                                <td>{{entity.name}}</td>
                                                                <td>{{entity.relationship}}</td>
                                                                <td>{{entity.summary|| 'Family'}}</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list.geni.family">
                                                                <td>{{entity.name}}({{entity.relationship}})</td>
                                                                <td>Relatives</td>
                                                                <td>{{entity.entityType|| 'Family'}}</td>
                                                            </tr>
                                                            <tr ng-show="entitySearchResult.person_more_info['spouse(s)']">
                                                                <td>{{entitySearchResult.person_more_info['spouse(s)']}}</td>
                                                                <td>Spouse</td>
                                                                <td>Family</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-show="entitySearchResult.list['parents'].length === 0
                                                            && entitySearchResult.list['associated_companies']['family'].length === 0
                                                            && entitySearchResult.list['associated_companies']['friends'].length === 0
                                                            && entitySearchResult.list['relatives'].length === 0 && entitySearchResult.list.geni.family.length === 0 && spousecount === 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.familydata">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.familydata">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Family Members Wrapper Ends  -->
                                        </div>
                                    </div>
                                    <!--   Overview Tab Ends   -->
                                    <div class="due-tab-wrapper" ng-show="mainInfoTabType === 'Duediligence'">

                                        <!--  First Row Starts   -->
                                        <div class="row top-row custom-row">
                                            <div class="col-sm-6 custom-col">
                                                <div class="panel custom-panel-wrapper details-panel-wrapper first-tab-panel">
                                                    <div class="panel-heading ">
                                                        <h4 class="top-details-heading">Identification Information
                                                            <!-- 																	<span class="pull-right"> -->
                                                            <!-- 																		<span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover(Source_url_tuna, 'companyInfoTooltip');" ng-style="myStyle2" -->
                                                            <!-- 																			  ng-click="corporatePopup(Source_url_tuna)">[1P]</span> -->
                                                            <!-- 																		<span class="text-pink">[0S]</span> -->
                                                            <!-- 																		  <span class="text-pink">[ 2S ]</span> -->
                                                            <!-- 																	</span> -->
                                                        </h4>
                                                    </div>
                                                    <div class="panel-body panel-scroll alert-message-wrapper">
                                                        <!--  Progressbar List Starts -->
                                                        <div class="company-details" id="identificationInfo" ng-if="identificationInfoLength > 0">
                                                            <!-- 																	<p style="text-align: left" class="text-dark-grey2"> -->
                                                            <!-- 																		{{ceriSearchResultObject.description}} -->
                                                            <!-- 																		{{entitySearchResult.description}} -->
                                                            <!-- 																	</p> -->
                                                            <div class="company-details-wrapper" ng-repeat="(key, value) in identificationInfo">
                                                                <div class="left">
                                                                    <p class="clearfix"> {{key}}
                                                                        <span class="pull-right">: </span>
                                                                    </p>
                                                                </div>
                                                                <div class="center">
                                                                    <span>{{value}}</span>
                                                                    <span class="text-blue1 "  ng-if="key !== 'Customer Name' && key !== 'Passport'">[0P]</span>
                                                                   <span class="text-blue1 companyTooltip" ng-if="key == 'Customer Name' || key == 'Passport'" ng-mouseover="mouseover('Generated From Scanned Source', 'companyTooltip', 'Primary');" ng-style="myStyle2" ng-click="corporatePopup(sourceUrl, $index, 'dummy')" style="cursor: pointer;">[1P]</span>
                                                                    
                                                                    <span class="text-pink" ng-if="key == 'Customer Name' || key == 'Passport'">[0S]</span>
                                                                    <span class="text-pink companyTooltip" ng-if="key !== 'Customer Name' && key !== 'Passport'" ng-mouseover="mouseover(sourceUrl, 'companyTooltip','Secondary');" ng-style="myStyle2" ng-click="corporatePopup(sourceUrl, $index)" style="cursor: pointer;">[1S]</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="alert-message alert-message-wrapper" ng-if="identificationInfoLength == 0">Data Not Found</div>
                                                        <!--  Progressbar List Ends -->
                                                    </div>                                              
                                                </div>
                                            </div>

                                            <div class="col-sm-6 custom-col industry-col entity-item career-details-wrapper border-0 height-100 pad-0">
                                                <div class="panel custom-panel-wrapper details-panel-wrapper">
                                                    <div class="panel-heading pad-b0">
                                                        <h4 class="top-details-heading">Occupation and Industry</h4>
                                                    </div>

                                                    <!-- 															<span ng-show="ErrorHandling.companyInformation" > -->
                                                    <!-- 																<span class="text-dark-red no-data">No Data Found</span> -->
                                                    <!-- 															</span> -->
                                                    <div class="panel-body mar-t0 alert-message-wrapper">
                                                        <div class="company-details" ng-if="personOccupatinAndIndustry">
                                                            <div class="company-details-wrapper">
                                                                <h4 class="media-heading text-dark-grey2"> Profession
                                                                    <span>: </span>
                                                                    <span>{{ personOccupatinAndIndustry.Profession}}</span>
                                                                </h4>
                                                            </div>
                                                            <div class="company-details-wrapper mar-0 pad-0 panel-scroll entity-result-item">
                                                                <ul class="media-list career-details-lists">
                                                                    <li class="media" ng-repeat="buss in personOccupatinAndIndustry.business">
                                                                        <div class="media-left">
                                                                            <a href="javascript:void(0);">
                                                                                <img class="img-responsive mCS_img_loaded" src="../assets/images/menu_icons/co.png" alt="img">
                                                                            </a>
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <h4 class="media-heading text-dark-grey2">
                                                                                {{ buss.position}}<span ng-if="buss.salary"> - {{ buss.salary}}</span>
                                                                            </h4>
                                                                            <p>
                                                                                <a ui-sref="entityCompanyNew({query:buss['corporation-name']})" target="_blank">
                                                                                    <span>{{ buss['corporation-name']}}  </span>
                                                                                    <span ng-if="buss['corporation-reg-no']">({{ buss['corporation-reg-no']}})</span>
                                                                                    <span> | {{buss.type}} </span>
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <span>{{ buss['corporation-address']}}</span>

                                                                            </p>
                                                                            <p><a href="{{ buss['link-to-company']}}" target="_blank">{{ buss['link-to-company']}}</a></p>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>	

                                                        </div>
                                                        <div class="alert-message alert-message-wrapper" ng-if=" !personOccupatinAndIndustry">Data Not Found</div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--  First Row Ends   -->

                                        <!--  Second Row Starts   -->
                                        <div class="row custom-row">
                                            <div class="col-sm-6 custom-col entity-item academic-info">
                                                <div class="panel height-100 mar-b0 custom-panel-wrapper details-panel-wrapper first-tab-panel">
                                                    <div class="panel-heading panel-heading-right">
                                                        <h4 class="top-details-heading">Relationships Information</h4>
                                                    </div>
                                                    <div class="panel-body right-panel-body alert-message-wrapper">
                                                        <div class="company-details" ng-if="screeningData.length > 0">
                                                            <ul class="list-inline custom-list count-list item-2">
                                                                <li>
                                                                    Total
                                                                    <span class="help-block text-dark-grey2">Entities</span>
                                                                    <span class="count">{{screeningData.length|| 0}}</span>
                                                                </li>
                                                                <li>
                                                                    Identified
                                                                    <span class="help-block text-dark-grey2">Risks</span>
                                                                    <span class="count">{{identifiedRisk}}</span>
                                                                </li>                                                           
                                                            </ul>
                                                        </div>
                                                        <!--  Custom Accordion Starts  -->
                                                        <div class="panel-group custom-accordion center-row1 " id="accordion" role="tablist" aria-multiselectable="true" ng-if="screeningData.length > 0">

                                                            <span ng-show="pageloader.screeningLoader" class="custom-spinner height-200px">
                                                                <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                            </span>
                                                            <div class="panel panel-default" ng-repeat="screening in screeningData">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed clearfix">
                                                                            <!-- ng-click="selectedPanel('#collapse'+$index,$index)" -->
                                                                            <span class="pull-left width-50 mar-t10">{{screening.name}}</span>
                                                                            <ul class="list-inline pull-left width-50 custom-list action-icon-list item-4 text-right">
                                                                                <li ng-show="screening.sanction_url || screening.sanction_available">
                                                                                    <!--                                                                                   <span class="help-block">
                                                                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screening.sanction_url === ''" ng-mouseover="mouseover(screening.sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.sanction_url)" style="cursor: pointer;">[0P]</span>
                                                                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screening.sanction_url !== ''" ng-mouseover="mouseover(screening.sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.sanction_url)" style="cursor: pointer;">[1P]</span>
                                                                                                                <span class="text-pink">[0S]</span>
                                                                                                            </span>-->
                                                                                    <i class="fa fa-ban text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-ban  text-light-orange showTooltip" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-hide="screening.sanction_url || screening.sanction_available">
                                                                                    <!--                                                                                    <span class="help-block">
                                                                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip"  ng-if="screening.sanction_url === ''" ng-mouseover="mouseover(screening.sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.sanction_url)" style="cursor: pointer;">[0P]</span>
                                                                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip"  ng-if="screening.sanction_url !== ''" ng-mouseover="mouseover(screening.sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.sanction_url)" style="cursor: pointer;">[1P]</span>
                                                                                                                <span class="text-pink">[0S]</span>
                                                                                                            </span>-->
                                                                                    <i class="fa fa-ban  text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-ban text-dark-green2 showTooltip" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-hide="screening.pep_url || screening.pep_available">
                                                                                    <!--                                                                                    <span class="help-block">
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip"  ng-if= "screening.pep_url === ''" ng-mouseover="mouseover(screening.pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.pep_url)" style="cursor: pointer;">[0P]</span>
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if= "screening.pep_url !== ''" ng-mouseover="mouseover(screening.pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.pep_url)" style="cursor: pointer;">[1P]</span>
                                                                                                                                                                            <span class="text-pink">[0S]</span>
                                                                                                                                                                        </span>-->
                                                                                    <i class="fa fa-street-view text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-street-view text-dark-green2 showTooltip " ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-show="screening.pep_url || screening.pep_available">
                                                                                    <!--                                                                                    <span class="help-block">
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if= "screening.pep_url === ''" ng-mouseover="mouseover(screening.pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.pep_url)" style="cursor: pointer;">[0P]</span>
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if= "screening.pep_url !== ''"  ng-mouseover="mouseover(screening.pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screening.pep_url)" style="cursor: pointer;">[1P]</span>
                                                                                    
                                                                                                                                                                            <span class="text-pink">[0S]</span>
                                                                                                                                                                        </span>-->
                                                                                    <i class="fa fa-street-view text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-street-view text-light-orange showTooltip " ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>    
                                                                                <li ng-hide="(screening.high_risk_jurisdiction === 'high') || (screening.high_risk_jurisdiction === 'medium')">
                                                                                    <!--                                                                                    <span class="help-block">
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                                                                                                                                            <span class="text-pink">[0S]</span>
                                                                                                                                                                        </span>-->
                                                                                    <i class="fa fa-globe text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-globe text-dark-green2 showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-show="screening.high_risk_jurisdiction === 'medium'">
                                                                                    <!--                                                                                    <span class="help-block">
                                                                                                                                                                            <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                                                                                                                                            <span class="text-pink">[0S]</span>
                                                                                                                                                                        </span>-->
                                                                                    <i class="fa fa-globe text-dark-yellow2 showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-globe text-yellow2 showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-show="screening.high_risk_jurisdiction === 'high'">
                                                                                    <!--                                                                                     <span class="help-block">
                                                                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                                                                                <span class="text-pink">[0S]</span>
                                                                                                            </span>-->
                                                                                    <i class="fa fa-globe text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                    <!--<i class="fa fa-globe text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-show="screening.finance_availble">
                                                                                        <i class="fa fa-gavel text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'financeCrime')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                        <!--<i class="fa fa-gavel text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>
                                                                                <li ng-hide="screening.finance_availble">
                                                                                        <i class="fa fa-gavel text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screening, 'financeCrime')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                        <!--<i class="fa fa-gavel text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                                                </li>  
                                                                            </ul>
                                                                        </a>
                                                                    </h4>
                                                                </div>                                                                
                                                            </div> 
                                                        </div>
                                                        <!--  Custom Accordion Ends  -->  
                                                        <div class="alert-message alert-message-wrapper" ng-if="screeningData.length === 0">Data Not Found</div>
                                                    </div>
                                                    
                                                    <span ng-show="pageloader.directorsloader" class="custom-spinner">
                                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                    </span>

                                                </div>
                                            </div>
                                            <div class="col-sm-6 custom-col entity-item overview-network-wrapper">
                                                <h3>Relationships Analysis</h3>
                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphCdd')" class="read-more">Expand</a>
                                                <div class="networkGraphMipMain">
                                                    <div id="networkGraphCdd" style="width: 100%;height: 280px;position: absolute;top: 0;left: 0;"></div>

                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row top-row custom-row">
                                            <div class="col-sm-6 custom-col industry-col entity-item career-details-wrapper height-100 pad-0">
                                                <div class="panel custom-panel-wrapper details-panel-wrapper first-tab-panel height-100 mar-0">
                                                    <div class="panel-heading pad-b0">
                                                        <h4 class="top-details-heading">Affiliated Entities</h4>
                                                    </div>
                                                    <div class="panel-body mar-t0 alert-message-wrapper panel-scroll entity-result-item entity-result-item-lg">
                                                        <div class="company-details" ng-if="affliatedEntities">                                                            
                                                            <div class="company-details-wrapper mar-0 pad-0">
                                                                <ul class="media-list career-details-lists">
                                                                    <li class="media" ng-repeat="entity in affliatedEntities">
                                                                        <div class="media-left" ng-if="entity.type == 'Company'">
                                                                            <a href="javascript:void(0);">
                                                                                <img class="img-responsive mCS_img_loaded" src="../assets/images/menu_icons/co.png" alt="img">
                                                                            </a>
                                                                        </div>
                                                                        <div class="media-left" ng-if="entity.type == 'Individual'">
                                                                            <a href="javascript:void(0);">
                                                                                <img class="img-responsive mCS_img_loaded" src="../assets/images/menu_icons/icon_profile_settings.png" alt="img">
                                                                            </a>
                                                                        </div>
                                                                        <div class="media-body" ng-if="entity.type == 'Individual'">
                                                                            <h4 class="media-heading text-dark-grey2">
                                                                                {{ entity.name}} ({{entity.dob}}) <span>  - {{ entity.Profession}} | {{ entity.relation}}</span>
                                                                            </h4>

                                                                            <p>
                                                                                <span>{{ entity['address']}}</span>

                                                                            </p>
                                                                            <p>
                                                                                <span>Passport: {{ entity['Passport']}}</span>
                                                                                <span>Social Security: {{ entity['SocialSecurityNumber']}}</span>

                                                                            </p>
                                                                            <p>
                                                                                <a ui-sref="entityCompanyNew({query:entity.JobDetails['company']})" target="_blank">
                                                                                    <span>{{ entity.JobDetails.title}}  </span>                                                                                   
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <a ui-sref="entityCompanyNew({query:entity.JobDetails['company']})" target="_blank">
                                                                                    <span>{{ entity.JobDetails.address}}  </span>                                                                                   
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                        <div class="media-body" ng-if="entity.type == 'Company'">
                                                                            <h4 class="media-heading text-dark-grey2">
                                                                                {{ entity.name}}  
                                                                            </h4>
                                                                            <p><span>  {{ entity.actityType}} | {{ entity.relation}} </span></p>
                                                                            <p><span>Director : {{ entity.Director}}</span></p>
                                                                            <p><span>{{ entity.address}}</span></p>
                                                                            <p><span>{{ entity.web}}</span></p>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>	

                                                        </div>
                                                        <div class="alert-message alert-message-wrapper" ng-if="!affliatedEntities">Data Not Found</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Second Row Ends   -->

                                        <!-- 												 Third Row Starts   -->
                                        <!-- 												<div class="row top-row custom-row">  -->
                                        <!-- 													<div class="col-sm-6 custom-col "> -->
                                        <!-- 														<div class="panel  mar-b15 height-100 custom-panel-wrapper details-panel-wrapper"> -->
                                        <!-- 															<div class="panel-heading panel-heading-right"> -->
                                        <!-- 																<h4 class="top-details-heading">Ownership structure -->
                                        <!-- 																</h4> -->
                                        <!-- 															</div>                                        -->
                                        <!-- 															<div class="panel-body right-panel-body "> -->
                                        <!-- 																<div class="company-details"> -->
                                        <!-- 																	<ul class="list-inline custom-list count-list item-2"> -->
                                        <!-- 																		<li ng-if="subsidiaries[0].name"> -->
                                        <!-- 																			Parent -->
                                        <!-- 																			<span class="help-block text-dark-grey2">Companies</span> -->
                                        <!-- 																			<span class="count">01</span> -->
                                        <!-- 																		</li> -->
                                        <!-- 																		<li ng-if="subsidiaries[0].name"> -->
                                        <!-- 																			Subsidary -->
                                        <!-- 																			<span class="help-block text-dark-grey2">Companies</span> -->
                                        <!-- 																			<span class="count">{{subsidiaries[0].subsidiaries.length}}</span> -->

                                        <!-- 																		</li> -->
                                        <!-- 																	</ul> -->
                                        <!-- 																	<div class="company-details-body  custom-media height-200px"> -->
                                        <!-- 																		<ul class="row pad-x15 media-list panel-scroll"> -->
                                        <!-- 																			<li class="media"> -->
                                        <!-- 																				<div class="media-left" ng-if="subsidiaries[0].name"> -->
                                        <!-- 																					<a href=""> -->
                                        <!-- 																						<img class="company-icon image-object" ng-src="{{('../assets/images/menu_icons/co.png')}}"> -->
                                        <!-- 																					</a> -->
                                        <!-- 																				</div> -->
                                        <!-- 																				<div class="media-body"> -->
                                        <!-- 																					<h3 class="media-heading"> -->
                                        <!-- 																						<a href="javascript:void(0);" ng-if="subsidiaries[0].name" ng-click="ceriToEntityCompany(subsidiaries[0].name)" ng-mouseover="mouseover()" -->
                                        <!-- 																						   ng-style="myStyle2"> -->
                                        <!-- 																							{{subsidiaries[0].name}} -->
                                        <!-- 																						</a> -->
                                        <!-- 																					</h3> -->
                                        <!-- 																					<p class="corporate-details-wrapper progress-text mar-b10 width-100" ng-if="subsidiaries[0].name"> -->
                                        <!-- 																						<span class="help-block text-dark-green2">Parent company</span> -->
                                        <!-- 																						<span class="text-blue1 corporateTooltip" ng-mouseover="mouseover(subsidiaries[0].source_url, 'corporateTooltip')" -->
                                        <!-- 																							  ng-style="myStyle2" ng-click="corporatePopup(subsidiaries[0].source_url)">[1P]</span> -->
                                        <!-- 																						<span class="text-pink">[0S]</span> -->
                                        <!-- 																					</p> -->
                                        <!-- 																				</div> -->
                                        <!-- 																			</li> -->
                                        <!-- 																			<li class="media" ng-repeat="sub in subsidiaries[0].subsidiaries"> -->
                                        <!-- 																				<div class="media-left"> -->
                                        <!-- 																					<a href=""> -->
                                        <!-- 																						<img class="company-icon image-object" ng-src="{{('../assets/images/menu_icons/co.png')}}"> -->
                                        <!-- 																					</a> -->
                                        <!-- 																				</div> -->
                                        <!-- 																				<div class="media-body"> -->
                                        <!-- 																					<h3 class="media-heading"> -->
                                        <!-- 																						<a href="javascript:void(0);" ng-click="ceriToEntityCompany(sub.name)" ng-mouseover="mouseover()" ng-style="myStyle2"> -->
                                        <!-- 																							{{sub.name}} -->
                                        <!-- 																						</a> -->
                                        <!-- 																					</h3> -->
                                        <!-- 																					<p class="corporate-details-wrapper progress-text mar-b10 width-100"> -->
                                        <!-- 																						<span class="help-bloc text-yellow2">{{sub.percentage}} Subsidiary Company</span> -->
                                        <!-- 																						<span class="text-blue1 corporateTooltip" ng-mouseover="mouseover(sub.source_url, 'corporateTooltip')" -->
                                        <!-- 																							  ng-style="myStyle2" ng-click="corporatePopup(sub.source_url)">[1P]</span> -->
                                        <!-- 																						<span class="text-pink">[0S]</span> -->
                                        <!-- 																					</p> -->
                                        <!-- 																				</div> -->
                                        <!-- 																			</li> -->
                                        <!-- 																		</ul> -->
                                        <!-- 																		<span ng-show="((subsidiaries.length === 0)||(subsidiaries[0].subsidiaries.length === 0)) && !pageloader.coropoateloader" class="text-dark-red no-data">No Data Found</span> -->
                                        <!-- 																	</div> -->
                                        <!-- 																</div> -->
                                        <!-- 															</div> -->
                                        <!-- 															<span ng-show="pageloader.coropoateloader" class="custom-spinner"> -->
                                        <!-- 																<i class="fa fa-spinner fa-spin fa-2x"></i> -->
                                        <!-- 															</span> -->
                                        <!-- 														</div> -->
                                        <!-- 													</div>                                             -->
                                        <!-- 													<div class="col-sm-6 height-100 custom-col"> -->
                                        <!-- 														<div class="panel custom-panel-wrapper details-panel-wrapper" id="flowChartViewDiv"> -->
                                        <!-- 															<div class="panel-heading "> -->
                                        <!-- 																<h4 class="top-details-heading mar-t5 clearfix"> -->
                                        <!-- 																	<ul class="list-inline pull-left custom-list circle-list"> -->
                                        <!-- 																		<li> -->
                                        <!-- 																			<a> -->
                                        <!-- 																				<i class="fa mar-x5 fa-circle text-green"></i> -->
                                        <!-- 																				<span class="text-grey">Ownership structure -->
                                        <!-- 																					<span> </span> -->
                                        <!-- 																				</span> -->
                                        <!-- 																			</a> -->
                                        <!-- 																		</li> -->
                                        <!-- 																	</ul> -->
                                        <!-- 																	<ul class="list-inline pull-right custom-list circle-list"> -->
                                        <!-- 																		<li> -->
                                        <!-- 																			<a ng-click="openMaximizeFlowModal('flowChartViewDiv')"> -->
                                        <!-- 																				<span class="text-dark-grey">View Full Screen</span> -->
                                        <!-- 																				<i class="fa fa-arrows-alt text-dark-grey"></i> -->
                                        <!-- 																			</a> -->
                                        <!-- 																		</li> -->
                                        <!-- 																	</ul> -->
                                        <!-- 																</h4> -->
                                        <!-- 															</div> -->
                                        <!-- 															<div class="panel-body panel-h-scroll height-280px"> -->
                                        <!-- 																<div id="chart-container" class="orgchart-wrapper"></div> -->
                                        <!-- 															</div> -->
                                        <!-- 															<span ng-show="pageloader.treeGraphloader" class="custom-spinner"> -->
                                        <!-- 																<i class="fa fa-spinner fa-spin fa-2x"></i> -->
                                        <!-- 															</span> -->
                                        <!-- 															<span ng-show="ErrorHandling.flowchart" class="text-dark-red no-data"> Data Not Found</span> -->

                                        <!-- 														</div> -->
                                        <!-- 													</div> -->
                                        <!-- 												</div> -->
                                        <!-- 												 Third Row Ends   -->

                                        <!-- 												 Fourth Row Starts   -->
                                        <!-- 												<div class="row bottom-row custom-row"> -->
                                        <!-- 													<div class="col-sm-4 custom-col"> -->
                                        <!-- 														<div class="panel custom-panel-wrapper details-panel-wrapper"> -->
                                        <!-- 															<div class="panel-heading "> -->
                                        <!-- 																<h4 class="top-details-heading">Countries of operation</h4> -->
                                        <!-- 															</div> -->
                                        <!-- 															<span ng-show="pageloader.countriesloader" class="custom-spinner"> -->
                                        <!-- 																<i class="fa fa-spinner fa-spin fa-2x"></i> -->
                                        <!-- 															</span> -->
                                        <!-- 															<div class="panel-body"> -->
                                        <!-- 																<div class="company-details"> -->
                                        <!-- 																	<ul class="list-inline custom-list count-list item-2"> -->
                                        <!-- 																		<li> -->
                                        <!-- 																			Total -->
                                        <!-- 																			<span class="help-block text-dark-grey2">Countries</span> -->
                                        <!-- 																			<span class="count">{{CountryOperations.length}}</span> -->
                                        <!-- 																		</li> -->
                                        <!-- 																		<li> -->
                                        <!-- 																			High -->
                                        <!-- 																			<span class="help-block text-dark-grey2">Risk</span> -->
                                        <!-- 																			<span class="count">{{highriskLength.length || 0}}</span> -->
                                        <!-- 																		</li> -->
                                        <!-- 																	</ul> -->
                                        <!-- 																</div> -->
                                        <!-- 																<div class="custom-data-table-wrapper height-200px"> -->
                                        <!-- 																	<table class="table border-b0 data-table table-scroll three-col-equal " role="grid" id="countiesOperation"> -->
                                        <!-- 																		<thead id=""> -->
                                        <!-- 																			<tr id="" role="row" class="odd"> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Country -->
                                        <!-- 																				</th> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Risk -->
                                        <!-- 																				</th> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Source -->
                                        <!-- 																				</th> -->
                                        <!-- 																			</tr> -->
                                        <!-- 																		</thead> -->
                                        <!-- 																		<tbody id="" class="panel-scroll">   -->
                                        <!-- 																			<tr ng-repeat="country in CountryOperations"> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					{{country.Country}} -->
                                        <!-- 																				</td> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					{{country.Risk}} -->
                                        <!-- 																				</td> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					<strong class="text-blue1 mar-r5 countryTooltip" ng-mouseover="mouseover(country.source_url, 'countryTooltip')" -->
                                        <!-- 																							ng-style="myStyle2" ng-click="corporatePopup(country.source_url)">[1P]</strong> -->
                                        <!-- 																					<strong class="text-pink">[0S]</strong> -->
                                        <!-- 																				</td> -->
                                        <!-- 																			</tr> -->
                                        <!-- 																		</tbody> -->
                                        <!-- 																	</table> -->
                                        <!-- 																	<span ng-show="CountryOperations.length === 0" class="text-dark-red no-data">Data Not Found</span> -->
                                        <!-- 																</div> -->
                                        <!-- 															</div> -->
                                        <!-- 														</div> -->
                                        <!-- 													</div> -->
                                        <!-- 													<div class="col-sm-8 custom-col"> -->
                                        <!-- 														<div class="panel custom-panel-wrapper details-panel-wrapper"> -->
                                        <!-- 															<div class="panel-heading "> -->
                                        <!-- 																<h4 class="top-details-heading">Associated Documents</h4> -->
                                        <!-- 															</div> -->
                                        <!-- 															<span ng-show="pageloader.associatedDocumentloader" class="custom-spinner"> -->
                                        <!-- 																<i class="fa fa-spinner fa-spin fa-2x"></i> -->
                                        <!-- 															</span> -->
                                        <!-- 															<div class="panel-body"> -->
                                        <!-- 																<div class="company-details"> -->
                                        <!-- 																	<ul class="list-inline custom-list count-list item-3"> -->
                                        <!-- 																		<li> -->
                                        <!-- 																			Total -->
                                        <!-- 																			<span class="help-block text-dark-grey2">Documents</span> -->
                                        <!-- 																			<span class="count">{{companyAssociatedDocDetailsInfo.length}}</span> -->
                                        <!-- 																		</li> -->
                                        <!-- 																	</ul> -->
                                        <!-- 																</div> -->
                                        <!-- 																<div class="custom-data-table-wrapper height-200px"> -->
                                        <!-- 																	<table class="table border-b0 data-table table-scroll three-col " role="grid"> -->
                                        <!-- 																		<thead id=""> -->
                                        <!-- 																			<tr id="" role="row" class="odd"> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Document Name -->
                                        <!-- 																				</th> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Last Updated -->
                                        <!-- 																				</th> -->
                                        <!-- 																				<th> -->
                                        <!-- 																					Source -->
                                        <!-- 																				</th> -->
                                        <!-- 																			</tr> -->
                                        <!-- 																		</thead> -->
                                        <!-- 																		<tbody id="custlist" class="panel-scroll"> -->
                                        <!-- 																			<tr ng-repeat="documentsInfo in companyAssociatedDocDetailsInfo" ng-if="documentsInfo.description"> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					<p class="corporate-details-wrapper mar-b0 text-overflow"> -->
                                        <!-- 																						<span class="fa fa-file-pdf-o text-dark-red pull-left"></span> -->
                                        <!-- 																						{{documentsInfo.description}} -->
                                        <!-- 																					</p> -->
                                        <!-- 																				</td> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					<span>{{documentsInfo.date}}</span> -->
                                        <!-- 																				</td> -->
                                        <!-- 																				<td> -->
                                        <!-- 																					<strong class="text-blue1 mar-r5 associatedDocTooltip" ng-mouseover="mouseover(documentsInfo.links.self || documentsInfo.url, 'associatedDocTooltip')" -->
                                        <!-- 																							ng-style="myStyle2" ng-click="corporatePopup((documentsInfo.links.self || documentsInfo.url), 'associateddocument')">[1P]</strong> -->
                                        <!-- 																					<strong class="text-pink" ng-mouseover="mouseover()" ng-style="myStyle2">[0S]</strong> -->
                                        <!-- 																				</td> -->
                                        <!-- 																			</tr> -->
                                        <!-- 																	</table> -->
                                        <!-- 																	<span ng-show="(companyAssociatedDocDetailsInfo.length === 0) &&!(pageloader.associatedDocumentloader)" class="text-dark-red no-data">Data Not Found</span> -->
                                        <!-- 																</div> -->
                                        <!-- 																 Third Row Ends  -->
                                        <!-- 															</div> -->
                                        <!-- 														</div> -->
                                        <!-- 													</div> -->
                                        <!-- 												</div> -->
                                        <!-- 												 Fourth Row Ends   -->

                                    </div>
                                    <!-- Due Deligence tab starts 	 -->

                                    <!--   Career Tab Starts   -->
                                    <div class="career-tab-wrapper person-wrapper" ng-show="mainInfoTabType === 'Career'">
                                        <div class="row custom-row">

                                            <!--  Current Role Wrapper Starts  -->
                                            <div class="col-md-4 entity-item current-role pad-left-tab">
                                                <h3>Current Role</h3>
                                                <div class="entity-result-item custom-result-item" ng-hide="entitySearchResult.job === '' || entitySearchResult.job_title === ''">
                                                    <ul class="media-list current-role-lists">
                                                        <li class="media">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entitySearchResult.job|| entitySearchResult.job_title || entitySearchResult.list.forbes.details[0].current_position}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entitySearchResult.current_current_work_place}} </span>
                                                                    <span class="value">{{entitySearchResult.company_website}}</span>
                                                                    <span class="value">{{entitySearchResult.years_active}}</span>
                                                                </p>
                                                                <p>
                                                                    {{entitySearchResult.company_address}}
                                                                </p>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                    <p>
                                                        {{entitySearchResult.company_description}}
                                                    </p>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper"  ng-show="entitySearchResult.job === '' || entitySearchResult.job_title === ''">
                                                    <div class="alert-message alert-message-wrapper" ng-if="entitySearchResult.job === '' && entitySearchResult.job_title === ''">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Current Role Wrapper Ends  -->

                                            <!--  Previous Roles Wrapper Starts  -->
                                            <div class="col-md-4 entity-item previous-roles-wrapper">
                                                <h3>Previous Roles
                                                    <!-- <a href="javescript:void(0);" class="more-positions"> 3 More Positions</a> -->
                                                </h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['employment_history'].length > 0 || entitySearchResult.list['previous_work_places'].length > 0 || entitySearchResult.list['government_positions']['previous_roles'].length > 0 || entitySearchResult.list['associated_companies']['business_positions'].length > 0 || entitySearchResult.list['corporationwiki']['previous_job'].length > 0">
                                                    <ul class="media-list previous-roles">
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['employment_history']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.job_rank}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.company}} | </span>
                                                                    <span class="key">Email: {{entity.email}}</span>
                                                                </p>
                                                                <p>
                                                                    {{entity.street}} {{entity.city}} {{entity.state}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['previous_work_places']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.title}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.company}} | </span>
                                                                    <span class="key">{{entity.employed_dates}}</span>
                                                                </p>
                                                                <p ng-if="entity.employed_location !== ''">
                                                                    {{entity.employed_location}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['government_positions']['previous_roles']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.title}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.dates}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['associated_companies']['business_positions']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.relationship}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.entity_name}} | </span>
                                                                    <span class="key">{{entity.summary}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['corporationwiki']['previous_job']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.companyName}}
                                                                </h4>
                                                                <p>
                                                                    <span class="key">{{entity.location}} | </span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-show="entitySearchResult.list['employment_history'].length === 0 && entitySearchResult.list['previous_work_places'].length === 0 && (!entitySearchResult.list['government_positions']['previous_roles'] || entitySearchResult.list['government_positions']['previous_roles'].length === 0) && entitySearchResult.list['associated_companies']['business_positions'].length === 0 && entitySearchResult.list['corporationwiki']['previous_job'].length === 0">
                                                    <div class="alert-message loading-message">Loading...</div>
                                                </div>

                                            </div>
                                            <!--  Previous Roles Wrapper Ends  -->


                                            <!--  Previous Roles Wrapper Starts  -->
                                            <div class="col-md-4 entity-item previous-roles-wrapper">
                                                <h3>Co-Workers
                                                    <!-- <a href="javescript:void(0);" class="more-positions"> 3 More Positions</a> -->
                                                </h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['coworkers'].length > 0">
                                                    <ul class="media-list previous roles">
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['coworkers']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/icon_profile_settings.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.name}}
                                                                </h4>
                                                                <p>
                                                                    <!-- <span class="key">{{entity.name}}</span> -->
                                                                    <!-- 	<span class="key">Email: {{entity.email}}</span> -->
                                                                </p>
                                                                <p>
                                                                    <!-- {{entity.street}} {{entity.city}} {{entity.state}} -->
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['coworkers'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.coworker_data">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.coworker_data">Data Not Found</div>
                                                </div>

                                            </div>
                                            <!--  Previous Roles Wrapper Ends  -->



                                            <!--  Board Memberships Wrapper Starts  -->
                                            <div class="col-md-6 pad-left-tab entity-item board-memberships-person">
                                                <h3>Board Memberships in other companies</h3>
                                                <div class="entity-item-submmenu" ng-show="(entitySearchResult.list['board_member_of'].length > 0 || entitySearchResult.list['personal_profiles_bloomberg']['board_memberships'].length > 0 || entitySearchResult.list['boardmemberList'].length > 0) && entitySearchResult.list['board_membership_counts_data'].length > 0">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="boardMembers-item-submmenu-list keyValueList list-inline">
                                                            <li ng-repeat="entity in entitySearchResult.list['board_membership_counts_data']">
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        {{entity.position}}
                                                                        <strong>{{entity.count}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['board_member_of'].length > 0 || entitySearchResult.list['personal_profiles_bloomberg']['board_memberships'].length > 0 || entitySearchResult.list['boardmemberList'].length > 0">
                                                    <div class="person-table-wrapper">
                                                        <table class="table associated-table">
                                                            <tr ng-repeat="entity in entitySearchResult.list['personal_profiles_bloomberg']['board_memberships']">
                                                                <td>{{entity.entity_name}}</td>
                                                                <td>{{entity.position_holding}}</td>
                                                                <td>{{entity.time_period}}</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['boardmemberList']">
                                                                <td>{{entity.name}}</td>
                                                                <td>{{entity.designation}}</td>
                                                                <td>{{entity.website}}</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['board_member_of']">
                                                                <td>{{entity}}</td>
                                                                <!-- 	<td>Board Member</td> -->
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-show="!entitySearchResult.list['personal_profiles_bloomberg']['board_memberships'] && entitySearchResult.list['boardmemberList'].length === 0">
                                                    <div class="alert-message alert-message-wrapper ">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Board Memberships Wrapper Ends  -->


                                            <!--  Professional Connections Wrapper Starts  -->
                                            <div class="col-sm-6 entity-item professional-connections">
                                                <h3>Professional Connections</h3>
                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphCareers')" class="read-more">Expand</a>
                                                <div class="networkGraphMipMain">
                                                    <div id="networkGraphCareers"></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>
                                                </div>
                                            </div>
                                            <!--  Professional Connections Wrapper Ends  -->

                                            <!--  Government Positions Wrapper Starts  -->
                                            <div class="col-md-6 entity-item government-positions">
                                                <h3>Government positions</h3>

                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['government_positions_list'].length > 0">
                                                    <div class="person-table-wrapper">
                                                        <table class="table associated-table">
                                                            <tr ng-repeat="entity in entitySearchResult.list['government_positions_list']" ng-if="entity.type_ !== '' && entity.type_ && entity.name !== '' && entity.name">
                                                                <td>{{entity.name}}</td>
                                                                <td>{{entity.type_}}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['government_positions_list'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.gov_people">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.gov_people">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Government positions Wrapper Ends  -->

                                            <!--  Other Memberships Wrapper Starts  -->
                                            <div class="col-md-6 entity-item other-memeberships">
                                                <h3>Other Memberships</h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['memberships'].length > 0 || entitySearchResult.list['corporationwiki']['connected_companies'].length > 0">
                                                    <div class="person-table-wrapper">
                                                        <table class="table associated-table">
                                                            <tr ng-repeat="entity in entitySearchResult.list['memberships']">
                                                                <td>{{entity.name}}</td>
                                                                <td>{{entity.position}}</td>
                                                                <td>{{entity.risk_score}}</td>
                                                            </tr>
                                                            <tr ng-repeat="entity in entitySearchResult.list['corporationwiki']['connected_companies']">
                                                                <td>{{entity.company_name}}</td>
                                                                <td>{{entity.role}}<p ng-if="entity.company_status !== '' && entity.company_status">({{entity.company_status}})</p></td>
                                                                <td>{{entity.risk_score}}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper"  ng-hide="entitySearchResult.list['memberships'].length > 0 || entitySearchResult.list['corporationwiki']['connected_companies'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.other_membership">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper"  ng-if="entitySearchResult.list['memberships'].length === 0 && entitySearchResult.list['corporationwiki']['connected_companies'].length === 0">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Other Memberships Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--   Career Tab Ends   -->


                                    <!--  Academics Tab Starts  -->
                                    <div class="academics-tab-wrapper" ng-show="mainInfoTabType === 'Academics'">
                                        <div class="row custom-row">

                                            <!--  Academics Counter Info Starts  -->
                                            <div class="col-md-12 entity-item academics-counter-wrapper pad-left-tab" ng-show="entitySearchResult.list['has_degree'].length > 0">
                                                <div class="entity-item-submmenu">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="item-submmenu-list list-inline academics-counter">
                                                            <li ng-repeat="degrees in entitySearchResult.list['has_degree']">
                                                                <div class="submmenu-list" >
                                                                    <h5 class="text-uppercase">
                                                                        <!-- Masters Degree -->{{degrees.degree}}
                                                                        <strong>{{degrees.degreecount}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Academics Counter Info Ends  -->

                                            <!--  College Degree Wrapper Starts  -->
                                            <div class="col-sm-12 entity-item  college-degree-wrapper pad-left-tab">
                                                <h3>College degree</h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['personal_profiles_bloomberg']['honorary_digrees'].length > 0 || entitySearchResult.list['has_education'].length > 0">
                                                    <ul class="media-list">
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['has_education']" ng-if="entity.school_name !== ''">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" ng-src="{{entity.logo|| '../assets/images/menu_icons/co.png'}}" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.school_name}}
                                                                </h4>
                                                                <h5>
                                                                    {{entity.type}}
                                                                </h5>
                                                                <p >
                                                                    <span ng-if="entity.start_time !== ''">{{entity.start_time}}</span>	   <span ng-if="entity.end_time !== ''">  -  {{entity.end_time}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['personal_profiles_bloomberg']['honorary_digrees']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.title}}
                                                                </h4>
                                                                <h5>
                                                                    {{entity.description}}
                                                                </h5>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto" ng-show="entitySearchResult.list['has_education'].length === 0 && (!entitySearchResult.list['personal_profiles_bloomberg']['honorary_digrees'] || entitySearchResult.list['personal_profiles_bloomberg']['honorary_digrees'].length === 0)">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.honoray_data && entitySearchResult.data_not_found.academicinfo">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.honoray_data && !entitySearchResult.data_not_found.academicinfo">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  College Degree Wrapper Ends  -->

                                            <!--  Certificates Wrapper Starts  -->
                                            <div class="col-sm-5 entity-item certificate-wrapper pad-left-tab">
                                                <h3>Certificate if any</h3>
                                                <div class="entity-result-item v-scroll custom-result-item"  ng-show="entitySearchResult.list['personal_profiles_bloomberg'].awards.length > 0 || entitySearchResult.list['has_certificates'].length > 0">
                                                    <ul class="media-list">
                                                        <li class="media" ng-repeat="award in entitySearchResult.list['personal_profiles_bloomberg'].awards">
                                                            <!-- <div class="media-left">
                                                                    <a href="javascript:void(0);">
                                                                            <img class="img-responsive" src="../assets/images/enrich/media-thumbs/person/bcg.png" alt="img">
                                                                    </a>
                                                            </div> -->
                                                            <div class="media-body-certificates">
                                                                <h4>
                                                                    {{award.title}}
                                                                </h4>
                                                                <p>
                                                                    {{award.year}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="certificate in entitySearchResult.list['has_certificates']">
                                                            <div class="media-body-certificates">
                                                                <h4>
                                                                    {{certificate.certificate}}
                                                                </h4>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="entity-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['personal_profiles_bloomberg'].awards.length > 0 || entitySearchResult.list['has_certificates'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.awards_data && entitySearchResult.data_not_found.hasCertificates">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.awards_data && !entitySearchResult.data_not_found.hasCertificates">Data Not Found</div>
                                                </div>
                                            </div>

                                            <div class="col-sm-7 entity-item">
                                                <h3>Academic Connections</h3>
                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphAcademics')" class="read-more">Expand</a>
                                                <div class="networkGraphMipMain" style="height: 310px">
                                                    <div id="networkGraphAcademics" style="height: 310px"></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>
                                                </div>

                                            </div>
                                            <!--  Certificates Wrapper Ends  -->

                                            <!--  Schooling Wrapper Starts  -->
                                            <div class="col-sm-5 entity-item schooling-wrapper">
                                                <h3>Schooling</h3>
                                                <div class="entity-result-item v-scroll custom-result-item" ng-show="entitySearchResult.list['has_education'].length > 0 || entitySearchResult.list['personal_profiles_bloomberg']['education'].length > 0">
                                                    <ul class="media-list">
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['has_education']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="{{entity.logo|| '../assets/images/menu_icons/co.png'}}" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.school_name}}
                                                                </h4>
                                                                <p>
                                                                    {{entity.type}}
                                                                </p>
                                                                <p>
                                                                    <span ng-if="entity.start_time !== ''"> {{entity.start_time}} </span> <span ng-if="entity.end_time !== ''">   -   {{entity.end_time}}</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['personal_profiles_bloomberg']['education']">
                                                            <div class="media-left">
                                                                <a href="javascript:void(0);">
                                                                    <img class="img-responsive" src="../assets/images/menu_icons/co.png" alt="img">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h4 class="media-heading">
                                                                    {{entity.title}}
                                                                </h4>
                                                                <p>
                                                                    {{entity.time}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto" ng-hide="entitySearchResult.list['has_education'].length > 0 || entitySearchResult.list['personal_profiles_bloomberg']['education'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.schooling && entitySearchResult.data_not_found.academicinfo">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.schooling && !entitySearchResult.data_not_found.academicinfo">Data Not Found</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 entity-item schooling-wrapper">

                                            </div>
                                            <!--  Schooling Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--  Academics Tab Ends  -->

                                    <!--  Finance Tab Starts  -->
                                    <div class="finance-person-tab-wrapper" ng-show="mainInfoTabType === 'Finance'">
                                        <div class="row custom-row">

                                            <!--  General Info Wrapper Starts  -->
                                            <div class="col-md-6 entity-item current-role pad-left-tab calculated-compentation">
                                                <h3>
                                                    <span>Total Calculated Compensation</span>
                                                    <!-- <small>as of Fiscal Year 2016 </small> -->
                                                    <strong ng-if="entitySearchResult.list['financial_data']['total_calculated_compensation']"> {{entitySearchResult.list['financial_data']['total_calculated_compensation']}}</strong><strong ng-if="!entitySearchResult.list['financial_data']['total_calculated_compensation']">NA</strong>
                                                </h3>
                                                <div class="entity-result-item custom-result-item">
                                                    <div class="salary-compensation-wrapper">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Salary</span>
                                                                    <strong ng-if="entitySearchResult.list['financial_data']['total_anual_sallery'] !== '' && entitySearchResult.list['financial_data']['total_anual_sallery']">{{entitySearchResult.list['financial_data']['total_anual_sallery']}}</strong><strong ng-if="entitySearchResult.list['financial_data']['total_anual_sallery'] == '' || !entitySearchResult.list['financial_data']['total_anual_sallery']">NA</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Bonus</span>
                                                                    <strong>NA</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Total Calculated Compensation</span>
                                                                    <strong ng-if="entitySearchResult.list['financial_data']['total_calculated_compensation']"> {{entitySearchResult.list['financial_data']['total_calculated_compensation']}}</strong><strong ng-if="!entitySearchResult.list['financial_data']['total_calculated_compensation']">NA</strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="total-compensation-wrapper">
                                                        <h3>Total Compensation</h3>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Total Annual Cash Compensation</span>
                                                                    <strong ng-if="entitySearchResult.list['financial_data']['total_annual_cash_compensation']"> {{entitySearchResult.list['financial_data']['total_annual_cash_compensation']}}</strong><strong ng-if="!entitySearchResult.list['financial_data']['total_annual_cash_compensation']">NA</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Total Short term Compensation</span>
                                                                    <strong ng-if="entitySearchResult.list['financial_data']['total_short_term_compensation']"> {{entitySearchResult.list['financial_data']['total_short_term_compensation']}}</strong><strong ng-if="!entitySearchResult.list['financial_data']['total_short_term_compensation']">NA</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p>
                                                                    <span>Other Long Term Compensation</span>
                                                                    <strong ng-if="entitySearchResult.list['financial_data']['other_long_term_compensation']"> {{entitySearchResult.list['financial_data']['other_long_term_compensation']}}</strong><strong ng-if="!entitySearchResult.list['financial_data']['other_long_term_compensation']">NA</strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="bloombeerg-compensation-wrapper">
                                                            <div class="bloomberg-stock-inner-wrapper">
                                                    <div class="row range-wrapper" ng-repeat="(k,v) in entitySearchResult['stocks']['bloomberg_prop']" ng-if="k != 'company_website' && k != 'company_address' && k != 'risk_score' && k!= 'company_description' && k!='name'">
                                                     <span class="col-sm-4 text-capitalize">{{k | strReplace:'_':' '}} </span>
                                                     <span class="col-sm-8">: {{v ? v : 'N/A'}}</span>
                                                   </div>
                                           </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <!--  General Info Wrapper Starts  -->

                                            <!--  Stock Option Wrapper Starts  -->
                                            <div class="col-md-6 entity-item stock-option-wrapper">
                                                <a class="read-more mar-r10" ng-click="readmoreModal.financePersonModal();">READ MORE</a>
                                                <h3> Stock Option</h3>
                                                <div class="entity-result-item v-scroll custom-result-item">
                                                    <table class="table associated-table">
                                                        <tr>
                                                            <td>Restricted Stock Awards</td>
                                                            <td>{{entitySearchResult.list['stock_options']['restricted_stock_awards']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>All Other Compensation</td>
                                                            <td>{{entitySearchResult.list['stock_options']['all_other_compensation']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Exercised options </td>
                                                            <td>{{entitySearchResult.list['stock_options']['exercised_options']|| 'NA'}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Exercised options Value</td>
                                                            <td>{{entitySearchResult.list['stock_options']['exercised_options_value']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Exerrcisable Options </td>
                                                            <td>{{entitySearchResult.list['stock_options']['exercisable_options']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Exercisable Options Value</td>
                                                            <td>{{entitySearchResult.list['stock_options']['exercisable_options_value']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Unexercisable Options</td>
                                                            <td>{{entitySearchResult.list['stock_options']['unexercisable_options']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Unexercisable Options Value</td>
                                                            <td>{{entitySearchResult.list['stock_options']['unexercisable_options_value']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Value of options</td>
                                                            <td>{{entitySearchResult.list['stock_options']['total_value_of_options']|| 'NA'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Number of options</td>
                                                            <td>{{entitySearchResult.list['stock_options']['total_number_of_options']|| 'NA'}}</td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                            <!--  Stock Option Wrapper Ends  -->

                                            <!--  bloomberg-timeline Info Starts  -->
                                            <div class="col-sm-12 entity-item bloomberg-line-chart-wrapper pad-left-tab">
                                                <div class="row custom-row">
                                                    <div class="col-md-6 bloomberg-stock-detail-wrapper">
                                                        <div class="bloomberg-stock-inner-wrapper">
                                                            <div class="row">
                                                                <div class="col-sm-4 range-wrapper">
                                                                    <p class="row">
                                                                        <span class="col-sm-4">STOCK CODE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].stock_id ? entitySearchResult['stocks']['bloomberg_prop'].stock_id : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">EXCHANGE LOCATION: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].exchange ? entitySearchResult['stocks']['bloomberg_prop'].exchange : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">CURRENT STOCK STATUS: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].current_stock_status ? entitySearchResult['stocks']['bloomberg_prop'].current_stock_status : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">EXCHANGE CURRENCY: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].exchange_currency ? entitySearchResult['stocks']['bloomberg_prop'].exchange_currency : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">EXPENSE RATIO: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].expense_ratio ? entitySearchResult['stocks']['bloomberg_prop'].expense_ratio : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">FUND Managers: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].fund_managers ? entitySearchResult['stocks']['bloomberg_prop'].fund_managers : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">FUND TYPE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['fund_type'] ? entitySearchResult['stocks']['bloomberg_prop']['fund_type'] : 'N/A'}}</span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-sm-4 open-wrapper">
                                                                    <p class="row">
                                                                        <span class="col-sm-4">CURRENT PRICE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].current_price ? entitySearchResult['stocks']['bloomberg_prop'].current_price : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">CURRENT MGMT FEE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].current_mgmt_fee ? entitySearchResult['stocks']['bloomberg_prop'].current_mgmt_fee : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">PREVIOUS CLOSE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].previous_close ? entitySearchResult['stocks']['bloomberg_prop'].previous_close : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">DIVIDEND INDICATED GROSS YIELD: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].dividend_indicated_gross_yield ? entitySearchResult['stocks']['bloomberg_prop'].dividend_indicated_gross_yield : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">52WK PRICE RANGE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['52wk_range'] ? entitySearchResult['stocks']['bloomberg_prop']['52wk_range'] : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">REDEMPTION FEE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['redemption_fee'] ? entitySearchResult['stocks']['bloomberg_prop']['redemption_fee'] : 'N/A'}}</span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-sm-4 volume-wrapper">
                                                                    <p class="row">
                                                                        <span class="col-sm-4">1 YR RETURN: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['1_yr_return'] ? entitySearchResult['stocks']['bloomberg_prop']['1_yr_return'] : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">PRICE TAKEN AT: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop'].price_taken_at ? entitySearchResult['stocks']['bloomberg_prop'].price_taken_at : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">FRONT LOAD FEE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['front_load_fee'] ? entitySearchResult['stocks']['bloomberg_prop']['front_load_fee'] : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">GEOGRAPHIC FOCUS: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['geographic_focus'] ? entitySearchResult['stocks']['bloomberg_prop']['geographic_focus'] : 'N/A'}}</span>
                                                                    </p>
                                                                    <p class="row">
                                                                        <span class="col-sm-4">INCEPTION DATE: </span>
                                                                        <span class="col-sm-8">{{entitySearchResult['stocks']['bloomberg_prop']['inception_date'] ? entitySearchResult['stocks']['bloomberg_prop']['inception_date'] : 'N/A'}}</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 stock-prices-wrapper">
                                                        <div class="row">
                                                            <!-- 	<div>
                                                                                    <button class="btn btn-primary" ng-click="readmoreModal.financeModal();">READ MORE</button>						             								</button>
                                                                            </div> -->
                                                            <div class="col-sm-6"><h3>Stocks Prices</h3></div>
                                                            <div class="col-sm-6 text-right" ng-show="bloombergStockLineChartData['five year stock prices'].length > 0">
                                                                <div class="bloomberg-event-checkbox">
                                                                    <span ng-class="{'active': bloombergSelectedTab === 'current day stock prices'}" ng-click="onChangeBloombergDetailsKey('current day stock prices')" ng-if="bloombergStockLineChartData['current day stock prices']">Today</span>
                                                                    <span ng-class="{'active': bloombergSelectedTab === 'one month stock prices'}" ng-click="onChangeBloombergDetailsKey('one month stock prices')" ng-if="bloombergStockLineChartData['one month stock prices']">1M</span>
                                                                    <span ng-class="{'active': bloombergSelectedTab === 'one year stock prices'}" ng-click="onChangeBloombergDetailsKey('one year stock prices')" ng-if="bloombergStockLineChartData['one year stock prices']">1Y</span>
                                                                    <span ng-class="{'active': bloombergSelectedTab === 'five year stock prices'}" ng-click="onChangeBloombergDetailsKey('five year stock prices')" ng-if="bloombergStockLineChartData['five year stock prices']">5Y</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row balance-data">
                                                            <div class="col-sm-4" ng-if="bloombergSelectedTab === 'current day stock prices' || bloombergSelectedTab === 'one year stock prices' || bloombergSelectedTab === 'five year stock prices' || bloombergSelectedTab === 'one month stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['lastPrice']"><small>LAST PRICE:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['lastPrice']}}
                                                                </h5>
                                                            </div>
                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices' || bloombergSelectedTab === 'one year stock prices' || bloombergSelectedTab === 'five year stock prices' || bloombergSelectedTab === 'one month stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['lastUpdateUTC']"><small>LAST UPDATED DATE:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['lastUpdateUTC] | date}}
                                                                </h5>
                                                            </div>
                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices' || bloombergSelectedTab === 'one year stock prices' || bloombergSelectedTab === 'five year stock prices' || bloombergSelectedTab === 'one month stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['nyTradeStartTime']"><small>TRADING START TIME:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['nyTradeStartTime']| limitTo:'5'}}
                                                                </h5>
                                                            </div>
                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices' || bloombergSelectedTab === 'one year stock prices' || bloombergSelectedTab === 'five year stock prices' || bloombergSelectedTab === 'one month stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['nyTradeEndTime']"><small>TRADING END TIME:</small>
                                                                    {{ entitySearchResult['stocks']['current_day_stock_prices']['nyTradeEndTime']| limitTo:'5'}}
                                                                </h5>
                                                            </div>

                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['previousClosingPriceOneTradingDayAgo']"><small>PREVIOUS CLOSE PRICE:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['previousClosingPriceOneTradingDayAgo']}}
                                                                </h5>
                                                            </div>
                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices' || bloombergSelectedTab === 'one year stock prices' || bloombergSelectedTab === 'five year stock prices' || bloombergSelectedTab === 'one month stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['timeZoneOffset']" ><small>TIME ZONE OFFSET:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['timeZoneOffset']}}
                                                                </h5>
                                                            </div>
                                                            <div class="col-sm-4"  ng-if="bloombergSelectedTab === 'current day stock prices'">
                                                                <h5 ng-if="entitySearchResult['stocks']['current_day_stock_prices']['tradingDayCloseUTC']"><small>TRADING DAY CLOSE:</small>
                                                                    {{entitySearchResult['stocks']['current_day_stock_prices']['tradingDayCloseUTC']| date}}
                                                                </h5>
                                                            </div>
                                                        </div>
                                                        <div id="bloomberg-line-chart-details"  ng-show="bloombergStockLineChartData"></div>
                                                        <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper financialline-chart-ditto" ng-hide="bloombergStockLineChartData['five year stock prices'].length > 0">
                                                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.stocks_prices">Loading...</div>
                                                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.stocks_prices">Data Not Found</div>
                                                        </div>
                                                        <!-- <a class="read-more mar-r10" ng-click="readmoreModal.financeModal();" ng-hide="bloombergStockLineChartData['five year stock prices'].length > 0">READ MORE</a> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  bloomberg-timeline Info Ends  -->


                                            <!--  Board Memberships Wrapper Starts  -->
                                            <!-- <div class="col-md-6 entity-item board-memberships-wrapper">
                                                    <h3>Board Memberships in other companies</h3>
                                                    <div class="entity-item-submmenu" ng-show="entitySearchResult.list['board_member_of'].length > 0 || entitySearchResult.list['boardmemberList'].length > 0">
                                                            <div class="item-submmenu-list-wrapper">
                                                                    <ul class="item-submmenu-list list-inline">
                                                                            <li>
                                                                                    <div class="submmenu-list">
                                                                                            <h5 class="text-uppercase">
                                                                                                    Board of Directors
                                                                                                    <strong>{{entitySearchResult.list['boardmemberList'].length}}</strong>
                                                                                            </h5>
                                                                                    </div>
                                                                            </li><li>
                                                                                    <div class="submmenu-list">
                                                                                            <h5 class="text-uppercase">
                                                                                                    Board Members
                                                                                                    <strong>{{entitySearchResult.list['board_member_of'].length}}</strong>
                                                                                            </h5>
                                                                                    </div>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                                    <div class="entity-result-item v-scroll" ng-show="entitySearchResult.list['board_member_of'].length > 0 || entitySearchResult.list['boardmemberList'].length > 0">
                                                            <div class="person-table-wrapper">
                                                                    <table class="table associated-table">
                                                                            <tr ng-repeat="entity in entitySearchResult.list['boardmemberList']">
                                                                                    <td>{{entity.name}}</td>
                                                                                    <td>{{entity.designation}}</td>
                                                                                    <td>{{entity.website}}</td>
                                                                            </tr>
                                                                            <tr ng-repeat="entity in entitySearchResult.list['board_member_of']">
                                                                                    <td>{{entity}}</td>
                                                                                    <td>Board Members</td>
                                                                            </tr>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div class="custom-result-item alert-message-wrapper" ng-show="entitySearchResult.list['board_member_of'].length === 0 && entitySearchResult.list['boardmemberList'].length === 0">
                                                            <div class="alert-message loading-message">Loading...</div>
                                                    </div>
                                            </div> -->
                                            <!--  Board Memberships Wrapper Ends  -->

                                            <!--  Holding Wrapper Starts  -->
                                            <div class="col-sm-6 entity-item holdings-wrapper">
                                                <h3>Holdings</h3>
                                                <div class="entity-result-item v-scroll" ng-show="entitySearchResult.list['associated_companies']['holdings'].length > 0">
                                                    <table class="table associated-table">
                                                        <tr ng-repeat="entity in entitySearchResult.list['associated_companies']['holdings']">
                                                            <td>{{entity.name}}</td>
                                                            <td>{{entity.relationship}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['associated_companies']['holdings'].length > 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.holdings">Loading...</div>
                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.holdings">Data Not Found</div>
                                                </div>
                                            </div>
                                            <!--  Holding Wrapper Endss  -->


                                            <!--  Dividents Wrapper Starts  -->
                                            <div class="col-md-6 entity-item government-positions relative">
                                                <h3>Dividents</h3>
                                                <div class="entity-result-item v-scroll" ng-show="entitySearchResult.list['dividents'].length > 0">
                                                    <div class="recent-linkedin" ng-repeat="entity in entitySearchResult.list['dividents']">
                                                        <h4 class="media-heading">
                                                            {{entity.name}}
                                                        </h4>
                                                        <div class="linkedin-content">
                                                            <p class="progress-text">
                                                                <span class="key">All Other Compensation</span>
                                                                <span class="value"> : {{entity.all_other_compensation|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Exercisable Options</span>
                                                                <span class="value"> : {{entity.exercisable_options|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Exercisable Options Value</span>
                                                                <span class="value"> : {{entity.exercisable_options_value|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Exercised Options</span>
                                                                <span class="value"> : {{entity.exercised_options|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Exercised Options Value</span>
                                                                <span class="value"> : {{entity.exercised_options_value|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Total Number of Options</span>
                                                                <span class="value"> : {{entity.total_number_of_options|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Total Value of Options</span>
                                                                <span class="value"> : {{entity.total_value_of_options|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Restricted Stock Awards</span>
                                                                <span class="value"> : {{entity.restricted_stock_awards|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Unexercisable Options</span>
                                                                <span class="value"> : {{entity.unexercisable_options|| 'NA'}} </span>
                                                            </p>
                                                            <p class="progress-text">
                                                                <span class="key">Unexercisable Options Value</span>
                                                                <span class="value"> : {{entity.unexercisable_options_value|| 'NA'}} </span>
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="custom-result-item alert-message-wrapper" ng-show="entitySearchResult.list['dividents'].length === 0">
                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.dividents_data">Loading...</div>
                                                    <div class="alert-message" ng-if="!entitySearchResult.data_not_found.dividents_data">Data Not Found</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 entity-item">
                                                <h3>Finance Connections</h3>
                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphFinance')" class="read-more">Expand</a>
                                                <div class="networkGraphMipMain">
                                                    <div id="networkGraphFinance"></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>
                                                </div>
                                            </div>
                                            <!--  Dividents Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--   Finance Tab Ends   -->


                                    <!--  Risks Tab Starts  -->
                                    <div class="risks-tab-wrapper" ng-show="mainInfoTabType === 'Risks'">
                                        <div class="row custom-row">

                                            <!--  Risks Counter Info Starts  -->
                                            <div class="col-md-12 entity-item risks-counter-wrapper pad-left-tab">
                                                <div class="entity-item-submmenu">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="item-submmenu-list list-inline">
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Pep Risk
                                                                        <strong>{{entitySearchResult.list['pep'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Fraud Risk
                                                                        <strong>{{entitySearchResult.list['scam'].length + entitySearchResult.list['imsasllc'].length + entitySearchResult.list['sex-offenders'].length + entitySearchResult.list['denied_personlist'].length + entitySearchResult.list['fbi_data'].length + entitySearchResult.list['badbuyerslist'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Criminal Offences
                                                                        <strong>{{entitySearchResult.list['imsasllc_offenses'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        News
                                                                        <strong>{{entitySearchResult.list['adverse_news'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Dark Web Articles
                                                                        <strong>{{entitySearchResult.list.news_deep_web.length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Blacklist / Sanctions
                                                                        <strong>{{entitySearchResult.list['supplierblacklist'].Source ? (entitySearchResult.list['sanctionList'].length + 1) : entitySearchResult.list['sanctionList'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Risks Counter Info Ends  -->

                                            <!--  alert-timeline Info Starts  -->
                                            <div class="col-md-12 entity-item overall-sentiment-wrapper alert-timeline-wrapper pad-left-tab">
                                                <h3>
                                                    Alert Timeline
                                                    <ul class="custom-sentiment-feed list-inline">
                                                        <li><a class="refresh" href>PEP</a></li>
                                                        <li><a class="sanctions" href>BLACK LIST</a></li>
                                                        <li><a class="heart" href>FRAUD ALERTS</a></li>
                                                        <li><a style="color: #e81f0f;" href>DARK WEB</a></li>
                                                        <li><a style="color: #69ca6b;" href>NEWS</a></li>
                                                        <li><a style="color:#dd9d22" href>CRIMINAL OFFENCES</a></li>
                                                    </ul>
                                                </h3>
                                                <div class="alertTimeLineChart-wrapper" id="riskyTimelineChart">

                                                </div>
                                            </div>

                                            <!--  Alert Timeline Info Ends  -->

                                            <!--  Pep Alerts Wrapper Starts  -->
                                            <div class="col-md-12 risks-pep-wrapper pad-left-tab">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Pep Alerts</h3>
                                                                <div class="entity-result-item v-scroll" ng-show="entitySearchResult.list['pep'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="pep in entitySearchResult.list['pep']">
                                                                            <div class="recent-linkedin">
                                                                                <p class="mar-b5">{{pep.list_name}}</p>
                                                                                <div class="linkedin-content">
                                                                                    <p class="mar-b5">
                                                                                        {{pep.summary}}
                                                                                    </p>
                                                                                </div>
                                                                                <a href="{{pep.link}}" target="_blank">
                                                                                    {{pep.link}}
                                                                                </a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper" ng-show="entitySearchResult.list['pep'].length === 0">
                                                                    <div class="alert-message text-uppercase">No PEP Alerts</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Criminal offences</h3>
                                                                <div class="entity-result-item relative" ng-show="entitySearchResult.list['imsasllc_offenses'].length > 0">
                                                                    <li class="media" ng-repeat="entity in entitySearchResult.list['imsasllc_offenses']">
                                                                        <div class="recent-linkedin">
                                                                            <div class="linkedin-content">
                                                                                <p ng-if="entity.Code !== '' && entity.Code">Code: {{entity.Code}}</p>
                                                                                <p>{{entity.Description}}<p ng-if="entity.CaseNumber !== '' && entity.CaseNumber">({{entity.CaseNumber}})</p>
                                                                                <p ng-if="entity.DateOfCrime !== '' && entity.DateOfCrime">Date of crime : {{entity.DateOfCrime}}</p>
                                                                                <p ng-if="entity.Disposition !== '' && entity.Disposition">Disposition : {{entity.Disposition}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper" ng-show="entitySearchResult.list['imsasllc_offenses'].length === 0">
                                                                    <div class="alert-message text-uppercase">No Crimnal offences</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Fraud Alerts</h3>
                                                                <div class="entity-result-item v-scroll recent-pep" ng-show="entitySearchResult.list['scam'].length > 0 || entitySearchResult.list['badbuyerslist'].length > 0 || entitySearchResult.list['imsasllc'].length > 0 || entitySearchResult.list['sex-offenders'].length > 0 || entitySearchResult.list['denied_personlist'].length > 0 || entitySearchResult.list['exclusions']['person'] > 0 || entitySearchResult.list['fbi_data'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="scam in entitySearchResult.list['scam']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Scam List
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{scam.root.title}}</p>
                                                                                    <p ng-if="scam.root.reported_by && scam.root.reported_by !== ''">
                                                                                        Reported By : {{scam.root.reported_by}}
                                                                                    </p>
                                                                                    <p ng-if="scam.root.report_time && scam.root.report_time !== ''">
                                                                                        at {{scam.root.report_time}}
                                                                                    </p>
                                                                                    <p>{{scam.root.body}}</p>
                                                                                    <a href="{{scam.root.url}}" target="_blank">
                                                                                        {{scam.root.url}}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['badbuyerslist']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Bad Buyer's List
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{entity.name}} <p ng-if="entity.email && entity.email !== ''">({{entity.email}})</p></p>
                                                                                    <p>Buyers Address : {{entity.buyers_address}} {{entity.buyers_country}}</p>
                                                                                    <p>{{entity.details}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['imsasllc']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Crime List(Imsaslic)
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{entity.properties.display_title|| entity.properties.FullName}}({{entity.properties.date_of_birth|| entity.properties.DOB}})</p>
                                                                                    <p>{{entity.properties.language_spoken|| entity.properties.SourceorJurisdiction}}</p>
                                                                                    <p>{{entity.properties.nationality|| entity.properties.Score}}</p>
                                                                                    <p>{{entity.properties.Category}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['sex-offenders']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Sex Offenders List
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p class="sex-offender">{{entity.name}}</p>
                                                                                    <p class="progress-text" ng-repeat="(key, value) in entity" ng-if="key !== name">
                                                                                        <span class="key">{{key}}</span>
                                                                                        <span class="value"> : {{value}} </span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['denied_personlist']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Denied Person List
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{entity.name}}</p>
                                                                                    <p class="progress-text">
                                                                                        <span class="date" ng-if="entity.effective_date !== '' && entity.effective_date">Effective from {{entity.effective_date| date : 'dd/MM/yyyy'}}</span>
                                                                                        <span class="date" ng-if="entity.expiration_date !== '' && entity.expiration_date">Expire on {{entity.expiration_date| date : 'dd/MM/yyyy'}}</span>
                                                                                        <span ng-if="entity.fr_citation !== '' && entity.fr_citation">FR Citation: {{entity.fr_citation}}</span>
                                                                                        <span>Risk Score : {{entity.risk_score}}</span>
                                                                                    </p>
                                                                                    <p class="description" ng-if="entity.action !== '' && entity.action">
                                                                                        Action : {{entity.action}}
                                                                                    </p>
                                                                                    <p class="description" ng-if="entity.address !== '' && entity.address">
                                                                                        Address : {{entity.address}}
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-show="entitySearchResult.list['exclusions']['person']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                Exclusions Details
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{entitySearchResult.list['exclusions']['person'].first_name}} {{entitySearchResult.list['exclusions']['person'].middle_name}} {{entitySearchResult.list['exclusions']['person'].last_name}}</p>
                                                                                    <p class="progress-text">
                                                                                    <h3>Speciality : {{entitySearchResult.list['exclusions']['person'].specialty}}</h3>
                                                                                    <span ng-if="entitySearchResult.list['exclusions']['person'].excl_type !== '' && entitySearchResult.list['exclusions']['person'].excl_type">Exclusion Type : {{entitySearchResult.list['exclusions']['person'].excl_type}}</span>
                                                                                    <span ng-if="entitySearchResult.list['exclusions']['person'].excl_date !== '' && entitySearchResult.list['exclusions']['person'].excl_date">Dated on : {{entitySearchResult.list['exclusions']['person'].excl_date}}</span>
                                                                                    <span ng-if="entitySearchResult.list['exclusions']['person'].address1 !== '' && entitySearchResult.list['exclusions']['person'].address1">Address : {{entitySearchResult.list['exclusions']['person'].address1}} {{entitySearchResult.list['exclusions']['person'].address2}}</span>
                                                                                    <span ng-if="entitySearchResult.list['exclusions']['person'].general !== '' && entitySearchResult.list['exclusions']['person'].general">General : {{entitySearchResult.list['exclusions']['person'].general}}</span>
                                                                                    <span>Risk Score : {{entitySearchResult.list['exclusions']['person'].risk_score}}</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['fbi_data']">
                                                                            <h3 class="media-heading" ng-if="$index === 0">
                                                                                FBI Data Details
                                                                            </h3>
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p>{{entity.properties.title}}</p>
                                                                                    <p class="progress-text">
                                                                                        <span ng-if="entity.properties.eyes !== '' && entity.properties.eyes">Eyes : {{entity.properties.eyes}}</span>
                                                                                        <span ng-if="entity.properties.weight !== '' && entity.properties.weight">Weight : {{entity.properties.weight}}</span>
                                                                                        <span ng-if="entity.properties.height !== '' && entity.properties.height">Height : {{entity.properties.height}}</span>
                                                                                        <span ng-if="entity.properties.hair !== '' && entity.properties.hair">Hair : {{entity.properties.hair}}</span>
                                                                                        <span ng-if="entity.properties.scars_and_marks !== '' && entity.properties.scars_and_marks">Scars and marks : {{entity.properties.scars_and_marks}}</span>
                                                                                        <span ng-if="entity.properties.place_of_birth !== '' && entity.properties.place_of_birth">Place of birth : {{entity.properties.place_of_birth}}</span>
                                                                                        <span ng-if="entity.properties.remarks !== '' && entity.properties.remarks">Remarks : {{entity.properties.remarks}}</span>
                                                                                        <span ng-if="entity.properties.sex !== '' && entity.properties.sex">Sex : {{entity.properties.sex}}</span>
                                                                                        <span ng-if="entity.properties.dates_of_birth_used !== '' && entity.properties.dates_of_birth_used">Dates of birth used : {{entity.properties.dates_of_birth_used}}</span>
                                                                                    </p>
                                                                                    <p ng-if="entity.properties.description !== '' && entity.properties.description">
                                                                                        Description : {{entity.properties.description}}
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['scam'].length > 0 && entitySearchResult.list['badbuyerslist'].length > 0 && entitySearchResult.list['imsasllc'].length > 0 && entitySearchResult.list['sex-offenders'].length > 0 && entitySearchResult.list['denied_personlist'].length > 0 && entitySearchResult.list['exclusions']['person'] && entitySearchResult.list['fbi_data'].length > 0">
                                                                    <div class="alert-message text-uppercase">No Fraud Alerts</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Pep Alerts Wrapper Starts  -->

                                            <!--  Adverse News Wrapper Starts  -->
                                            <div class="col-md-12 social-media-trending person-wrapper">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3 class="text-dark-grey2">News</h3>
                                                                <div class="entity-result-item v-scroll" ng-show="entitySearchResult.list['adverse_news'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="news in entitySearchResult.list['adverse_news']">
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content">
                                                                                    <p class="mar-b5"><a target="_blank" href="{{news.url}}">{{news.title}}</a></p>
                                                                                    <p class="progress-text mar-b5">
                                                                                        <span class="date" ng-if="news.published !== '' && news.published">Published on {{news.published| date : 'dd/MM/yyyy'}}</span>
                                                                                    </p>
                                                                                    <p class="mar-b5" ng-if="news.text !== '' && news.text">
                                                                                        Description : {{news.text}}
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['adverse_news'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.data_adverse_news">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.data_adverse_news" >Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 entity-item">
                                                            <div class="relationship-news-wrapper">
                                                                <h3>Donation Link Analysis</h3>
                                                                <a href="" ng-click="loadLinkAnalysis('', 'networkGraphRisks')" class="read-more">Expand</a>
                                                                <div class="networkGraphMipMain" style="height: 310px">
                                                                    <div id="networkGraphRisks" style="height: 310px"></div>
                                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message loading-message">Loading...</div></div>
                                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto"><div class="alert-message">Data Not Found</div></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Adverse News Wrapper Ends  -->

                                            <!--  AML Alerts Wrapper Starts  -->
                                            <div class="col-md-12 social-media-trending person-wrapper">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-linkedin-wrapper">
                                                                <h3>Dark Web</h3>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list['news_deep_web'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['news_deep_web']">
                                                                            <h3 class="media-heading">
                                                                                <a class="custom-school" title="{{entity.author}}" href="{{entity.author_url}}" target="_blank">
                                                                                    <i class="fa fa-user"></i> {{entity.name}}
                                                                                </a>
                                                                                <span class="custom-school" title="{{entity.title}}">
                                                                                    {{entity.title}}
                                                                                </span>
                                                                            </h3>
                                                                            <div class="media-body">
                                                                                <p class="progress-text" title="Dated: {{entity.dated}}" ng-if="entity.postedOn !== '' && entity.postedOn">
                                                                                    Posted on: {{entity.postedOn| date : 'dd-MM-yyyy'}}
                                                                                </p>
                                                                                <p class="progress-text" title="Details: {{entity.details}}" ng-if="entity.details !== '' && entity.details">
                                                                                    Details: {{entity.details}}
                                                                                </p>
                                                                                <p class="progress-text" title="Risk Score: {{entity.risk_score}}" ng-if="entity.risk_score !== '' && entity.risk_score">
                                                                                    Risk Score: {{entity.risk_score}}
                                                                                    <!-- <span>Source: {{entity.source}}</span> -->
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['news_deep_web'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.deepweb">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.deepweb" >Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="relationship-news-wrapper">
                                                                <h3>Blacklists</h3>
                                                                <div class="entity-result-item relative" ng-show="entitySearchResult.list['sanctionList'].length > 0">
                                                                    <ul class="media-list" >
                                                                        <!--   <li class="media" ng-repeat="(key, value) in entitySearchResult.list['supplierblacklist']" ng-if="value !== '' && value">
                                                                              <div class="media-body">
                                                                                  <p class="row progress-text">
                                                                                      <span class="col-sm-5 pad-lt-0 key text-capitalize">{{key}}</span>
                                                                                      <span class="col-sm-7 pad-rt-0 pad-lt-0 value  text-capitalize"> : {{value}} </span>
                                                                                  </p>
                                                                              </div>
                                                                          </li> -->
                                                                        <li class="media" ng-repeat="sanctionList in entitySearchResult.list['sanctionList']">
                                                                            <div class="media-left">
                                                                                <a href="javascript:void(0);">
                                                                                    <img class="img-responsive mCS_img_loaded" src="../assets/images/menu_icons/icon_profile_settings.png" alt="img">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p>{{sanctionList.list_name}}</p>
                                                                                <p>{{sanctionList.aliases[0].name}}</p>
                                                                                <!-- <p>{{sanctionList.list_type}}</p> -->
                                                                                <p>
                                                                                    {{sanctionList.summary}}
                                                                                </p>
                                                                                <p>{{sanctionList.place[0].place}}</p> 
                                                                                <div ng-repeat="birth in  sanctionList.birth_dates"> 
                                                                                    <p ng-if="birth.quality === 'strong'">DOB: {{birth.date}}</p>
                                                                                </div>
                                                                                <p ng-if="sanctionList.source">Source:{{sanctionList.source}}</p>
                                                                                <!-- <p>{{sanctionList.updated_at}}</p>             -->                                                          
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['sanctionList'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.blacklist">Loading...</div>
                                                                    <div class="alert-message" ng-if="!entitySearchResult.data_not_found.blacklist">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-linkedin-wrapper">
                                                                <h3>Criminal Records</h3>
                                                                <a class="read-more mar-r10" ng-click="readmoreModal.riskOffenceModal();" ng-show="entitySearchResult.list['crimeList']['interpol'].length > 0 || entitySearchResult.list['crimestoppers_uk'].length > 0 || entitySearchResult.list['unlimitedcriminalchecks'].length > 0 || entitySearchResult.list['sci_ccc_nashville_gov'].length > 0">READ MORE</a>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list['crimeList']['interpol'].length > 0 || entitySearchResult.list['crimestoppers_uk'].length > 0 || entitySearchResult.list['unlimitedcriminalchecks'].length > 0 || entitySearchResult.list['sci_ccc_nashville_gov'].length > 0">
                                                                    <ul class="media-list" >
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['crimeList']['interpol']">
                                                                            <div class="media-left criminal-records-wrapper">
                                                                                <a href="javascript:void(0);">
                                                                                    <img class="img-responsive" ng-src="{{entity.properties.image}}" ng-if="entity.properties.image && entity.properties.image !== ''"/>
                                                                                    <i class="fa fa-user" ng-if="entity.properties.image === '' && !entity.properties.image"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h3 class="media-heading">
                                                                                    {{entity.properties.display_title}}
                                                                                </h3>
                                                                                <p class="progress-text" ng-if="entity.properties.nationality !== '' && entity.properties.nationality">
                                                                                    <span class="key">Nationality</span><span class="value">: {{entity.properties.nationality}} </span>
                                                                                </p>
                                                                                <p class="progress-text" ng-if="entity.properties.date_of_birth !== '' && entity.properties.date_of_birth">
                                                                                    <span class="key">Age</span><span class="value">: {{entity.properties.date_of_birth}}</span>
                                                                                </p>
                                                                                <p class="progress-text" ng-if="entity.properties.colour_of_eyes !== '' && entity.properties.colour_of_eyes">
                                                                                    <span class="key">Eyes Color</span><span class="value">: {{entity.properties.colour_of_eyes}}</span>
                                                                                </p>

                                                                                <p class="description" ng-if="entity.properties.colour_of_hair !== '' && entity.properties.colour_of_hair">
                                                                                    <span class="key">Hair Color</span><span class="value">: {{entity.properties.colour_of_hair}}</span>
                                                                                </p>
                                                                                <p class="description">
                                                                                    <span ng-if="entity.properties.weight !== '' && entity.properties.weight">
                                                                                        <span class="key">Weight</span><span class="value">: {{entity.properties.weight}}</span>
                                                                                    </span>
                                                                                    <span ng-if="entity.properties.height !== '' && entity.properties.height">
                                                                                        <span class="key">Height</span><span class="value">: {{entity.properties.height}}</span>
                                                                                    </span>
                                                                                </p>
                                                                                <p class="description" ng-if="entity.properties.language_spoken !== '' && entity.properties.language_spoken">
                                                                                    <span class="key">Language spoken</span><span class="value">: {{entity.properties.language_spoken}}</span>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="description" ng-if="entity.properties.sex !== '' && entity.properties.sex">
                                                                                        <span class="key">Sex</span><span class="value">: {{entity.properties.sex}}</span>
                                                                                    </span>
                                                                                    <span ng-if="entity.properties.risk_score !== '' && entity.properties.risk_score">
                                                                                        <span class="key"> Risk Score</span><span class="value">: {{entity.properties.risk_score}} </span>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                            <div class="" ng-if="entity.properties.charges.length > 0">
                                                                                <div class="key"><p>Charges:</p></div>
                                                                                <div class="value">
                                                                                    <p ng-repeat="charge in entity.properties.charges">
                                                                                    <h3>{{charge.name}}</h3>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['crimestoppers_uk']">
                                                                            <div class="media-left criminal-records-wrapper">
                                                                                <a href="{{entity.image}}" target="_black" ng-if="entity.image !== '' && entity.image">
                                                                                    <img class="image-object img-responsive" ng-src="{{entity.image}}">
                                                                                </a>
                                                                                <a href="javascript:void(0);" ng-if="entity.image === '' || !entity.image">
                                                                                    <i class="fa fa-user"></i>
                                                                                </a>
                                                                            </div>

                                                                            <div class="media-body">
                                                                                <h3 class="media-heading">
                                                                                    {{entity.name}} <span ng-if="entity.cs_reference !== '' && entity.cs_reference">({{entity.cs_reference}})</span>
                                                                                </h3>
                                                                                <p class="progress-text">
                                                                                    <span class="text-capitalize" ng-if="entity.crime_type !== '' && entity.crime_type">Crime Type: {{entity.crime_type}}</span>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="text-capitalize" ng-if="entity.police_force !== '' && entity.police_force">Police Force: {{entity.police_force}}</span>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="text-capitalize" ng-if="entity.age_range !== '' && entity.age_range">Age: {{entity.age_range}}</span>
                                                                                    <span class="text-capitalize" ng-if="entity.crime_location !== '' && entity.crime_location">   Location: {{entity.crime_location}}</span>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="text-capitalize" ng-if="entity.sex !== '' && entity.sex">Sex: {{entity.sex}}</span>
                                                                                    <span class="description" ng-if="entity.risk_score !== '' && entity.risk_score">
                                                                                        Risk Score: {{entity.risk_score}} <!-- Source: {{entity.source}} -->
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media" ng-repeat="case in entitySearchResult.list['sci_ccc_nashville_gov']">
                                                                            <div class="media-left criminal-records-wrapper">
                                                                                <a href="javascript:void(0);">
                                                                                    <i class="fa fa-user"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h3 class="media-heading">{{case.name}}</h3>
                                                                                <p class="description">
                                                                                    <span> Offense:  </span>
                                                                                    {{case.charged_offense}}</p>
                                                                                <p class="description">
                                                                                    <span> Offense Date :  </span>
                                                                                    {{case.offense_date}}</p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <ul class="media-list" ng-repeat="entity in entitySearchResult.list['unlimitedcriminalchecks']">
                                                                        <li class="media" ng-repeat="(key, value) in entity">
                                                                            <h3 class="media-heading" ng-if="key === 'name'">
                                                                                <a class="text-uppercase" href="{{entity['root'].url}}" target="_black">
                                                                                    {{value}}
                                                                                </a>
                                                                            </h3>
                                                                            <div class="media-body" ng-if="key !== 'name'">
                                                                                <p class="row progress-text  custom-colon-section" ng-if="value !== '' && value">
                                                                                    <span class="col-sm-4 text-capitalize">{{key}}</span>
                                                                                    <span class="col-sm-8 text-capitalize"><small class="custom-colon">:</small>{{value}} </span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper" ng-show="entitySearchResult.list['crimeList']['interpol'].length === 0 && entitySearchResult.list['crimestoppers_uk'].length === 0 && entitySearchResult.list['unlimitedcriminalchecks'].length === 0 && entitySearchResult.list['sci_ccc_nashville_gov'].length === 0">
                                                                    <div class="alert-message alert-message-wrapper">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  AML Alerts Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--  Risks Tab Ends  -->

                                    <!--  Social Media Tab Starts  -->
                                    <div class="social-media-tab-wrapper person-social" ng-show="mainInfoTabType === 'Social Media'">
                                        <div class="row custom-row">

                                            <!--  Social Media Counter Info Starts  -->
                                            <div class="col-md-12 entity-item twitter-counter-wrapper pad-left-tab">
                                                <div class="entity-item-submmenu">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="item-submmenu-list list-inline">
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        <i class="fa fa-twitter"></i> Twitter Followers
                                                                        <strong>{{entitySearchResult.list['follower_following_count'][0].twitter_followers_number|| 0}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        <i class="fa fa-twitter"></i> Twitter Retweets
                                                                        <strong>{{entitySearchResult['twitter_retweets_count']|| 0}}
                                                                        </strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <!-- <li>
                                                                    <div class="submmenu-list">
                                                                            <h5 class="text-uppercase">
                                                                                    <i class="fa fa-linkedin"></i> Linkedin Followers
                                                                                    <strong>190K</strong>
                                                                            </h5>
                                                                    </div>
                                                            </li>
                                                            <li>
                                                                    <div class="submmenu-list">
                                                                            <h5 class="text-uppercase">
                                                                                    <i class="fa fa-linkedin"></i> Linkedin Connections
                                                                                    <strong>230K</strong>
                                                                            </h5>
                                                                    </div>
                                                            </li> -->
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        <i class="fa fa-google-plus"></i> Google Plus Connections
                                                                        <strong>{{entitySearchResult.list.googlePlusList.length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        <i class="fa fa-linkedin"></i> Instagram Followers
                                                                        <strong>{{entitySearchResult.list['instagramfollowersCountNumberic']}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        <i class="fa fa-linkedin"></i> Instagram Connections
                                                                        <strong>{{entitySearchResult.list['instagramListConnectionNumberic']}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Social Media Counter Info Ends  -->

                                            <!--  Activity Feed Wrapper Starts  -->
                                            <div class="col-md-12 entity-item activity-feed-wrapper pad-left-tab">
                                                <span ng-show="sentimentsLoader" class="custom-spinner">
                                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                </span>
                                                <h3>
                                                    Activity Feed
                                                    <ul class="custom-activity-feed list-inline">
                                                        <li><a class="refresh" href>NEGATIVE</a></li>
                                                        <li><a class="" href>NEUTRAL</i></a></li>
                                                        <li><a class="heart" href>POSITIVE</a></li>
                                                    </ul>
                                                </h3>
                                                <div class="overallSentimentChart-wrapper">

                                                </div>
                                            </div>
                                            <!--  Activity Feed Wrapper Ends  -->

                                            <!--  Recent Tweets Wrapper Starts  -->
                                            <div class="col-md-12 twitter-feed-wrapper pad-left-tab">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-tweets-wrapper">
                                                                <h3>Recent Tweets</h3>
                                                                <div class="entity-result-item"  ng-show="entitySearchResult.list['twitterList'].length > 0">
                                                                    <ul class="media-list" ng-repeat="tweets in entitySearchResult.list['twitterList']">
                                                                        <li class="media">
                                                                            <div class="recent-tweet">
                                                                                <div class="tweet-bio">
                                                                                    <div class="tweet-bio-dp">
                                                                                        <img class="image-object" ng-src="{{ tweets.image || '../assets/images/twitter/andrew.jpg'}}" />
                                                                                    </div>
                                                                                    <div class="tweet-bio-details">
                                                                                        <h5>{{tweets.name}}</h5>
                                                                                        <p>@{{tweets.name}}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tweet-content">
                                                                                    <span class="date ng-binding">
                                                                                        Posted on {{tweets.createdOn| date : 'dd/MM/yyyy'}}
                                                                                    </span>
                                                                                    <p>{{tweets.text}}<!-- @MunichRe_US in panel discussion about systems issues with #cyberrisk. #cyberNYC --></p>
                                                                                </div>
                                                                                <div class="tweet-count">
                                                                                    <ul class="list-inline">
                                                                                        <li>
                                                                                            <span class="refresh" href>
                                                                                                <i class="fa fa-retweet"></i> {{tweets.retweetCount|| 0}}</span>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['twitterList'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.recent_tweets">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.recent_tweets">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-tweets-wrapper">
                                                                <h3>Instagram Posts</h3>
                                                                <div class="entity-result-item latest-social-news" ng-show="entitySearchResult.list.instagramList.length > 0">
                                                                    <ul class="media-list instagram-posts">
                                                                        <li class="media" ng-repeat="instagramData in entitySearchResult.list.instagramList" >
                                                                            <div class="recent-linkedin">
                                                                                <div class="linkedin-content-left">
                                                                                    <span class="img-section-wrapper">
                                                                                        <img class="image-object" ng-src="{{instagramData.image|| 'https://tctechcrunch2011.files.wordpress.com/2017/08/gettyimages-692915429.jpg?w=150'}}" />
                                                                                    </span>
                                                                                </div>
                                                                                <div class="linkedin-content">
                                                                                    <p class="text-capitalize">
                                                                                        {{instagramData.name}}
                                                                                    </p>
                                                                                    <p class="post">
                                                                                        <small class="icon">Following</small>
                                                                                        <span class="badge count">{{instagramData.followingCount}}</span>
                                                                                        <small class="icon">Followers</small>
                                                                                        <span class="badge count">{{instagramData.followersCount}}</span>
                                                                                    </p>
                                                                                    <p>{{instagramData.caption}}</p>
                                                                                    <p class="posted-img">
                                                                                        <img class="img-responsive" ng-src="{{instagramData.thumbnail|| 'https://tctechcrunch2011.files.wordpress.com/2017/08/gettyimages-692915429.jpg?w=150'}}" />
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper"  ng-hide="entitySearchResult.list.instagramList.length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.instagram">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.instagram">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-linkedin-wrapper">
                                                                <h3>Google Plus Post</h3>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list.googlePlusList.length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="googlePlusData in entitySearchResult.list.googlePlusList">
                                                                            <div class="media-left">
                                                                                <a href="{{googlePlusData.url}}" target="_blank">
                                                                                    <img class="image-object" ng-src="{{googlePlusData.coverPhoto|| 'https://tctechcrunch2011.files.wordpress.com/2017/08/gettyimages-692915429.jpg?w=150'}}" />
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading text-capitalize">
                                                                                    {{googlePlusData.name}}
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date"><a class="" href="javascript:void(0);">Published on</a> {{googlePlusData.publishedDate| date : 'dd/MM/yyyy'}}</span>
                                                                                    <span class="date"><a class="" href="javascript:void(0);">Last Updated on</a> {{googlePlusData.lastUpdatedDate| date : 'dd/MM/yyyy'}}</span>
                                                                                </p>
                                                                                <p class="description">
                                                                                    {{googlePlusData.title}}
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list.googlePlusList.length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.google_plus">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.google_plus">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Recent Tweets Wrapper Ends  -->

                                            <!--  Recent Followers Wrapper Starts  -->
                                            <div class="col-md-12 linkedin-feed-wrapper">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Recent Followers</h3>
                                                                <a class="read-more mar-r10" ng-click="readmoreModal.socialFollwersModal();" ng-show="entitySearchResult.list['followers'].length > 0">READ MORE</a>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list['followers'].length > 0">
                                                                    <ul class="media-list linkedIn-media-list">
                                                                        <li class="media" ng-repeat="follower in entitySearchResult.list['followers']">
                                                                            <div class="media-left">
                                                                                <a href="javascript:void(0);" target="_blank">
                                                                                    <img class="image-object" ng-src="{{ follower.image ||'https://lh3.googleusercontent.com/rh85SdqUwnzVYpbZ0LL9pk-ncZ7Bfmj17biw7MLAhoA1DzmT9XlS9a819CxmI6aju_kSDm61jWHe=s630-fcrop64=1,010f0000ffffffff'}}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading text-capitalize">
                                                                                    {{follower.name}}
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date ng-binding">
                                                                                        <a href="javascript:void(0);">Profile Created</a> {{follower.createdOn| date : 'dd/MM/yyyy'}}
                                                                                    </span>
                                                                                    <!-- 	<span class="date ng-binding">
                                                                                                    <a href="javascript:void(0);">Last Updated on</a> 25/10/2017
                                                                                            </span> -->
                                                                                </p>
                                                                                <!-- <p class="description ng-binding">
                                                                                        EQuIP offers clear, comprehensive non-damage business interruption cover, designed especially for the...
                                                                                </p> -->
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['followers'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.recent_follower">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.recent_follower">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 entity-item">
                                                            <div class="social-network-wrapper">
                                                                <h3>Live Feed</h3>
                                                                <div class="">
                                                                    <div  class="social-network-chart-wrapper">
                                                                        <div id="twitter-socket-network-chart"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto" ng-show="entitySearchResult.list['twitter_socket_network_chart']">
                                                                    <div class="alert-message loading-message">Loading...</div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="text-right"> -->
                                                            <a class="read-more" ng-click="onClickLoadLiveFeed()"><!-- btn btn-more-reviews btn-live-feed right-10 -->
                                                                <!-- <i class="fa fa-feed"></i> -->Live Feed
                                                            </a>
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Recent Followers Wrapper Starts  -->
                                            <div class="col-md-12 linkedin-feed-wrapper traffic-by-country-wrapper">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="twitter-tag-wrapper">
                                                                <h3>Traffic By Countries</h3>
                                                                <div class="entity-result-item twitter-tag-chart-wrapper" ng-show="!entitySearchResult.list['is_traffic_by_countries']">
                                                                    <div id="traffic_by_countries" ></div>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_traffic_by_countries']">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.tra_by_countries">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.tra_by_countries">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="twitter-tag-wrapper">
                                                                <h3>Traffic By Search Key Words</h3>
                                                                <div class="entity-result-item twitter-tag-chart-wrapper" ng-show="!entitySearchResult.list['is_search_keywords']">
                                                                    <div id=search_keywords ></div>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_search_keywords']">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.sear_keywords">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.sear_keywords">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="twitter-tag-wrapper">
                                                                <h3>Traffic By Domain</h3>
                                                                <div class="entity-result-item twitter-tag-chart-wrapper" ng-show="!entitySearchResult.list['is_traffic_by_domain']">
                                                                    <div id=traffic_by_domain></div>
                                                                </div>
                                                                <div class="entity-result-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_traffic_by_domain']">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.tra_by_domain">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.tra_by_domain">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Recent Followers Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--  Social Media Tab Ends  -->


                                    <!--  Transactions Tab Starts  -->
                                    <div class="transaction-tab-wrapper" ng-show="mainInfoTabType === 'Transactions'">
                                        <div class="row custom-row">

                                            <!--  Transactions Top Section Starts  -->
                                            <div class="col-md-12 transaction-top-wrapper pad-left-tab">
                                                <div class="tranx-search-list-wrapper">
                                                    <form class="form-inline">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" placeholder="Client Name / PP Number" ng-model="searchTranscation">
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="period-search-wrapper">
                                                                    <div class="form-group period-label">
                                                                        <label for="from">On Period</label>
                                                                    </div>
                                                                    <div class="form-group date-picker-group">
                                                                        <div class="date-wrapper">
                                                                            <input placeholder="From" type="text" class="form-control text-center" id="transSearchFrom" ng-model="dateobj.searchFrom" ng-change="dateInitialize()">
                                                                            <i class="fa fa-chevron-down"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group date-picker-group">
                                                                        <div class="date-wrapper">
                                                                            <input placeholder="To" type="text" class="form-control text-center" id="transSearchTo" ng-model="dateobj.searchTo" >
                                                                            <i class="fa fa-chevron-down"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tranx-submit-buttons">
                                                                        <button type="submit" class="btn btn-default" ng-disabled="dateobj.searchFrom == undefined" ng-click ="searchPersonTranx(dateobj)">Search </button>
                                                                        <button type="submit" class="btn btn-default" ng-click ="resetDate()">Clear</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!--  Transactions Top Section Ends  -->


                                            <!--  Recent Uploads Wrapper Starts  -->
                                            <div class="col-md-12 entity-item board-memberships pad-left-tab">
                                                <h3>Person/Organization Transactions</h3>
                                                <div class="entity-result-item">
                                                    <div class="person-table-wrapper">
                                                        <table class="table associated-table table-striped">
                                                            <tr>
                                                                <th>Description</th>
                                                                <th>Date</th>
                                                                <th>Amount</th>
                                                                <th>Type</th>
                                                                <th>Recipient Detail</th>
                                                                <th>Recipit Code</th>
                                                            </tr>
                                                            <tr ng-repeat="trans in entitySearchResult.list['person_transaction_list']| filter: searchTranscation " ng-hide="transcationSearchon">
                                                                <td>
                                                                    <p ng-if="trans.tranx_type === 'Person'">{{trans.recipient_name}}</p>
                                                                    <p ng-if="trans.tranx_type === 'Organization'">{{trans.org_name}}</p>
                                                                </td>
                                                                <td>{{trans.date}}</td>
                                                                <td>{{trans.amount}}</td>
                                                                <td>{{trans.tranx_type}}</td>
                                                                <td>{{trans.recipient_detail|| 'NA'}}</td>
                                                                <td>{{trans.recipcode|| 'NA'}}</td>
                                                            </tr>
                                                            <tr ng-repeat="trans in newdata| filter: searchTranscation " ng-show="newdata.length > 0">
                                                                <td>
                                                                    <p ng-if="trans.tranx_type === 'Person'">{{trans.recipient_name|| 'NA'}}</p>
                                                                    <p ng-if="trans.tranx_type === 'Organization'">{{trans.org_name|| 'NA'}}</p>
                                                                </td>
                                                                <td>{{trans.date}}</td>
                                                                <td>{{trans.amount}}</td>
                                                                <td>{{trans.tranx_type}}</td>
                                                                <td>{{trans.recipient_detail|| 'NA'}}</td>
                                                                <td>{{trans.recipcode|| 'NA'}}</td>
                                                            </tr>
                                                            <tr ng-if="entitySearchResult.list['person_transaction_list'].length === 0">
                                                                <td colspan="6" style="text-align: center;">No Transaction Data Found.</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Recent Uploads Wrapper Ends  -->

                                        </div>
                                    </div>
                                    <!--  Transactions Tab Ends  -->


                                    <!--  Documents Tab Starts  -->
                                    <div class="document-tab-wrapper" ng-show="mainInfoTabType === 'Documents'">
                                        <div class="row custom-row">
                                            <!--  Documents Counter Info Starts  -->
                                            <div class="col-md-12 document-counter-wrapper entity-item pad-left-tab">
                                                <div class="entity-item-submmenu">
                                                    <div class="item-submmenu-list-wrapper">
                                                        <ul class="item-submmenu-list list-inline">
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Total Assets
                                                                        <strong>{{entitySearchResult.list['document_list'].length + entitySearchResult.list['videos_list'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Videos
                                                                        <strong>{{entitySearchResult.list['videos_list'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Audios \ Podcast
                                                                        <strong>319</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="submmenu-list">
                                                                    <h5 class="text-uppercase">
                                                                        Documents
                                                                        <strong>{{entitySearchResult.list['document_list'].length}}</strong>
                                                                    </h5>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <form class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="documents">Upload Documents</label>
                                                                        <div class="custom-upload-wrapper">
                                                                            <div class="custom-file-upload">
                                                                                <!-- <input type="file" name="uploadDoc" id="uploadDoc"> -->
                                                                                <label for="uploadDoc" class="file-inline" >
                                                                                    <span class="custom-input">
                                                                                        <strong class="text-uppercase uploaded-name"> </strong>
                                                                                        <small class="text-uppercase">Browse</small>
                                                                                    </span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- 																			<button type="submit" class="btn btn-default btn-browse">Browse</button> -->
                                                                    <button type="submit" class="btn btn-default btn-upload" ng-disabled="true">Upload</button>
                                                                </form>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Documents Counter Info Ends  -->

                                            <!--  Recent Uploads Wrapper Starts  -->
                                            <div class="col-md-12 recent-uploads-wrapper pad-left-tab">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-sm-5 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Recent Uploads</h3>
                                                                <div class="entity-result-item" ng-show="false">
                                                                    <table class="table associated-table table-striped">
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Autodesk press release</td>
                                                                            <td>Yesterday</td>
                                                                            <td>1.9MB</td>
                                                                            <td>PDF</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper"  ng-hide="false">
                                                                    <div class="alert-message loading-message" ng-if="false">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="true">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-7 entity-item">
                                                            <div class="recent-pep-wrapper">
                                                                <h3>Asset Trends
                                                                    <ul class="list-inline pull-right">
                                                                        <li>Videos</li>
                                                                        <li>Audios</li>
                                                                        <li>Documents</li>
                                                                    </ul>
                                                                </h3>
                                                                <div class="" id="assets-trend-line-chart">
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper assets-trends" ng-show="entitySearchResult.list['is_assets_trend_line_chart']">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.asset_data">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.asset_data">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Recent Uploads Wrapper Ends  -->

                                            <!--  Media Wrapper Starts  -->
                                            <div class="col-md-12 uploaded-content-wrapper social-media-trending">
                                                <div class="container-fluid">
                                                    <div class="row">

                                                        <!--  Videos Wrapper Starts  -->
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="all-documents-wrapper videos-list-wrapper">
                                                                <h3>Videos</h3>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list['videos_list'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['videos_list']">
                                                                            <div class="media-left">
                                                                                <a href="{{entity.watch_url}}" target="_blank">
                                                                                    <img class="image-object" ng-src="{{entity.thumbnail|| '../assets/images/enrich/media-thumbs/person/document/video.png'}}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="{{entity.watch_url}}" target="_blank">
                                                                                        {{entity.title}}
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">{{entity.pub_date| date : 'dd-MM-yyyy'}}</span>
                                                                                    <span class="date">{{entity.description}}</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper" ng-hide="entitySearchResult.list['videos_list'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.video_data">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.video_data">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--  Videos Wrapper Ends  -->

                                                        <!--  Podcasts Wrapper Starts  -->
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="all-documents-wrapper">
                                                                <h3> Podcasts</h3>
                                                                <div class="entity-result-item" ng-show="false">
                                                                    <ul class="media-list">
                                                                        <li class="media">
                                                                            <div class="media-left">
                                                                                <a href="#" target="_blank">
                                                                                    <img class="image-object" src="../assets/images/enrich/media-thumbs/person/document/podcasts.png">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="javascript:void(0);" target="_blank">
                                                                                        5 Ys of investing
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">By Sam D Aliah</span>
                                                                                    <span class="date">19imns</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media">
                                                                            <div class="media-left">
                                                                                <a href="#" target="_blank">
                                                                                    <img class="image-object" src="../assets/images/enrich/media-thumbs/person/document/podcasts.png">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="javascript:void(0);" target="_blank">
                                                                                        5 Ys of investing
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">By Sam D Aliah</span>
                                                                                    <span class="date">19imns</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media">
                                                                            <div class="media-left">
                                                                                <a href="#" target="_blank">
                                                                                    <img class="image-object" src="../assets/images/enrich/media-thumbs/person/document/podcasts.png">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="javascript:void(0);" target="_blank">
                                                                                        5 Ys of investing
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">By Sam D Aliah</span>
                                                                                    <span class="date">19imns</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                        <li class="media">
                                                                            <div class="media-left">
                                                                                <a href="#" target="_blank">
                                                                                    <img class="image-object" src="../assets/images/enrich/media-thumbs/person/document/podcasts.png">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="javascript:void(0);" target="_blank">
                                                                                        5 Ys of investing
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">By Sam D Aliah</span>
                                                                                    <span class="date">19imns</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                                <div  class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="false">
                                                                    <div class="alert-message loading-message" ng-if="false">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="true">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--  Podcasts Wrapper Ends  -->

                                                        <!--  Documents Wrapper Starts  -->
                                                        <div class="col-sm-4 entity-item">
                                                            <div class="all-documents-wrapper">
                                                                <h3> Documents</h3>
                                                                <div class="entity-result-item" ng-show="entitySearchResult.list['document_list'].length > 0">
                                                                    <ul class="media-list">
                                                                        <li class="media" ng-repeat="entity in entitySearchResult.list['document_list']">
                                                                            <div class="media-left">
                                                                                <a href="{{entity.url}}" target="_blank">
                                                                                    <img class="image-object" src="../assets/images/enrich/media-thumbs/person/document/documents.png">
                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <p class="media-heading">
                                                                                    <a href="{{entity.url}}" target="_blank">
                                                                                        {{entity.document_title}}
                                                                                    </a>
                                                                                </p>
                                                                                <p class="progress-text">
                                                                                    <span class="date">{{entity.mime}}</span>
                                                                                    <span class="date">{{entity.file_format}}</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper" ng-hide="entitySearchResult.list['document_list'].length > 0">
                                                                    <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.documents">Loading...</div>
                                                                    <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.documents">Data Not Found</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--  Documents Wrapper Ends  -->

                                                    </div>
                                                </div>
                                            </div>
                                            <!--  Media Wrapper Ends  -->

                                            <!--  Relationship Chart Wrapper Starts  -->
                                            <!-- 	<div class="col-md-12 document-chart-wrapper social-media-trending">
                                                            <div class="container-fluid">
                                                                    <div class="row">
                                                                            <div class="col-sm-6 entity-item">
                                                                                    <div class="recent-pep-wrapper">
                                                                                            <h3>Relationship Chart
                                                                                                    <a href="javescript:void(0);" class="more-positions"> View Full Screen
                                                                                                            <i class="fa fa-arrows-alt"></i>
                                                                                                    </a>
                                                                                            </h3>
                                                                                            <div class="networkGraphMipMain">
                                                                                                    <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper vla-chart-ditto">
                                                                                                            <div class="alert-message loading-message">Loading...</div>
                                                                                                    </div>
                                                                                            </div>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                    </div> -->
                                            <!--  Relationship Chart Section Ends  -->

                                        </div>
                                    </div>
                                    <!--  Documents Tab Ends  -->

                                </div>
                            </div>
                        </div>
                        <!--   Entity Contents Wrapper Ends  -->

                    </div>
                </div>
                <!--  Entity Results Wrapper Ends  -->

            </div>
            <!--  Entity Person Content Left Ends  -->

            <!--  Entity Person Content Right Starts  -->
            <div class="entity-company-content-right entity-person-new-right">

                <!--  Overview Right Section Panel Starts  -->
                <div class="risk-score-panel right-overview-wrapper" ng-show="mainInfoTabType === 'Overview'">
                    <!-- Person Address Starts -->
                    <div class="entity-item contact-address-wrapper">
                        <div class="contact-address head-quarters">
                            <h3>Contact Address</h3>
                            <h5 class="text-uppercase">
                                <i class="fa fa-university"></i>
                                <span>Address</span>
                            </h5>
                            <p class="text-uppercase" ng-if="entitySearchResult.work_location || entitySearchResult.address || entitySearchResult.place_of_birth || entitySearchResult.company_address || entitySearchResult.address_city || entitySearchResult.location || entitySearchResult.residence || entitySearchResult.list['people_details'].length > 0">
                                {{entitySearchResult.work_location|| entitySearchResult.address || entitySearchResult.place_of_birth || entitySearchResult.company_address || entitySearchResult.address_city || entitySearchResult.location || entitySearchResult.residence || entitySearchResult.list['people_details'][0].address_city || 'N/A'}}
                            </p>
                            <p ng-if="entitySearchResult.list['employment_history'].length > 0 && !entitySearchResult.address && !entitySearchResult.work_location && !entitySearchResult.place_of_birth && !entitySearchResult.company_address && !entitySearchResult.address_city && !entitySearchResult.location && !entitySearchResult.residence && !entitySearchResult.list['people_details'][0].address_city">
                                {{entitySearchResult.list["employment_history"][0].street + ' ' + entitySearchResult.list["employment_history"][0].city + ' ' + entitySearchResult.list["employment_history"][0].state + ' ' + entitySearchResult.list["employment_history"][0].zip|| 'N/A'}}
                            </p>
                        </div>
                        <div class="contact-address head-quarters">
                            <h5 class="text-uppercase">
                                <i class="fa fa-phone-square"></i>
                                <span>Phone</span>
                            </h5>
                            <p class="text-uppercase">
                                {{entitySearchResult.work_telephone|| entitySearchResult.list['employment_history'][0].phone || entitySearchResult.list['badbuyerslist'][0].phone_number || entitySearchResult.list['similar_profiles_list'][0].phone || entitySearchResult.list['people_details'][0].telephone || 'N/A'}}
                            </p>
                        </div>
                    </div>
                    <!-- Person Address Ends -->

                    <div class="risk-rating-wrapper">
                        <h3>Risk Ratings </h3>
                        <div class="riskscoreprogress" ng-cloak>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">Back list</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ (entitySearchResult.list['sanctionList'].length > 0 || entitySearchResult.list.supplierblacklist.Source) ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100" style="width: {{(entitySearchResult.list['sanctionList'].length > 0|| entitySearchResult.list.supplierblacklist.Source ) ? 100 : 0}}%">
                                            <span class="sr-only ng-binding">{{(entitySearchResult.list['sanctionList'].length > 0|| entitySearchResult.list.supplierblacklist.Source ) ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">PEPs</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{entitySearchResult.list.pep.length > 0 ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100" style="width: {{entitySearchResult.list.pep.length > 0 ? 100 : 0}}%">
                                            <span class="sr-only ng-binding">{{entitySearchResult.list['pep'].length > 0 ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">Fraud</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{ (entitySearchResult.list['scam'].length > 0 || entitySearchResult.list['badbuyerslist'].length > 0 || entitySearchResult.list['imsasllc'].length > 0 || entitySearchResult.list['sex-offenders'].length > 0 || entitySearchResult.list['denied_personlist'].length > 0 || entitySearchResult.list['exclusions'].length > 0 || entitySearchResult.list['fbi_data'].length > 0) ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100" style="width: {{(entitySearchResult.list['scam'].length > 0|| entitySearchResult.list['badbuyerslist'].length > 0 || entitySearchResult.list['imsasllc'].length > 0 || entitySearchResult.list['sex-offenders'].length > 0 || entitySearchResult.list['denied_personlist'].length > 0 || entitySearchResult.list['exclusions'].length > 0 || entitySearchResult.list['fbi_data'].length > 0 ) ? 100 : 0}}%">
                                            <span class="sr-only ng-binding">{{(entitySearchResult.list['scam'].length > 0|| entitySearchResult.list['badbuyerslist'].length > 0 || entitySearchResult.list['imsasllc'].length > 0 || entitySearchResult.list['sex-offenders'].length > 0 || entitySearchResult.list['denied_personlist'].length > 0 || entitySearchResult.list['exclusions'].length > 0 || entitySearchResult.list['fbi_data'].length > 0 ) ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">Criminal Offences</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{entitySearchResult.list.imsasllc_offenses.length > 0 ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100" style="width: {{entitySearchResult.list.imsasllc_offenses.length > 0 ? 100 : 0}}%">
                                            <span class="sr-only ng-binding">{{entitySearchResult.list['imsasllc_offenses'].length > 0 ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">Criminal Records</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{(entitySearchResult.list['crimeList']['interpol'].length > 0|| entitySearchResult.list['crimestoppers_uk'].length > 0 || entitySearchResult.list['unlimitedcriminalchecks'].length > 0) ? 100 : 0}}" aria-valuemin="100" aria-valuemax="100" style="width: {{(entitySearchResult.list['crimeList']['interpol'].length > 0|| entitySearchResult.list['crimestoppers_uk'].length > 0 || entitySearchResult.list['unlimitedcriminalchecks'].length > 0) ? 100 : 0 }}%">
                                            <span class="sr-only ng-binding">{{(entitySearchResult.list['crimeList']['interpol'].length > 0|| entitySearchResult.list['crimestoppers_uk'].length > 0 || entitySearchResult.list['unlimitedcriminalchecks'].length > 0) ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="risk-progressbar-holder">
                                <div class="progress-wrapper">
                                    <div class="inline">News</div>
                                    <div class="progress">
                                        <div class="progress-bar progressdr" role="progressbar" aria-valuenow="{{entitySearchResult.list.adverse_news.length > 0 ? 100 : 0}}" aria-valuemin="0" aria-valuemax="100" style="width: {{entitySearchResult.list.adverse_news.length > 0 ? 100 : 0}}%">
                                            <span class="sr-only ng-binding">{{entitySearchResult.list['adverse_news'].length > 0 ? 100 : 0}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--  Associated Locations Wrapper Starts  -->
                    <div class="entity-item associated-locations-wrapper">
                        <h3>ASSOCIATED LOCATIONS</h3>
                        <div class="WorldgraphBox">
                            <div id="person-location-chart" class="chartContentDiv"></div>
                            <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper associated-locations" ng-show="entitySearchResult.list['is_overview_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!--  Associated Locations Wrapper Ends  -->

                    <!--  Related Entities Starts  -->
                    <div class="entity-item related-entities-wrapper">
                        <h3>Related Entities</h3>
                        <div class="customEntities-result-item">
                            <div ng-hide="entitySearchResult.list['is_overview_entities_tag_chart']">
                                <div id="tagcloudCompanyOrganization"></div>
                            </div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper related-entries" ng-show="entitySearchResult.list['is_overview_entities_tag_chart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.relatedentity">Loading...</div>
                                <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.relatedentity">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!--  Related Entities Starts  -->


                    <!--  News Wrapper Starts  -->
                    <div class="entity-item social-media-followers">
                        <h3>Social Media Followers</h3>
                        <div class="socialBubbleChart-wrapper" ng-hide="entitySearchResult.list['is_overview_social_bubble_chart']">
                            <div id="socialBubbleChart"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper social-media-updates" ng-show="entitySearchResult.list['is_overview_social_bubble_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.socialmedia_follower">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.socialmedia_follower">Data Not Found</div>
                        </div>
                    </div>
                    <!-- </div> -->
                </div>
                <!--  Overview Right Section Ends  -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Duediligence'">
                    <!-- Screening Data -->
                    <div class="due-tab-wrapper mar-b10">
                        <div class="row custom-row">
                            <div class="custom-col">
                                <div class="panel custom-panel-wrapper details-panel-wrapper height-200px relative">
                                    <div class="panel-heading ">
                                        <h4 class="top-details-heading f-12">Screening Results</h4>
                                    </div>
                                    <div class="panel-body alert-message-wrapper">
                                        <div class="company-details" ng-if="screeningData.length > 0">
                                            <ul class="list-inline  custom-list count-list item-3">
                                                <!--                                                <li class="pad-x15">
                                                                                                    Total
                                                                                                    <span class="help-block">Entities</span>
                                                                                                    <span class="count">{{screeningData.length|| 0}}</span>
                                                                                                </li>-->
                                                <li class="pad-x15">
                                                    Identified
                                                    <span class="help-block text-dark-grey2">Risks</span>
                                                    <!--                                                     <span class="count">{{identifiedRisk}}</span> -->
                                                    <span class="count">{{identifiedRisk || 0}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="company-details-body  custom-media" ng-if="screeningData.length > 0">
                                            <ul class="list-inline custom-list item-3 text-center screening-list">
                                                <li ng-show="screeningData[0].pep_url ||  screening.pep_available">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].pep_url === ''" ng-mouseover="mouseover(screeningData[0].pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].pep_url)" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].pep_url !== ''" ng-mouseover="mouseover(screeningData[0].pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].pep_url)" style="cursor: pointer;">[1P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-street-view text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'pepalert', $index)" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">PEP Alerts</span>                                                                    
                                                </li>                                                                    
                                                <li ng-hide="screeningData[0].pep_url ||  screeningData[0].pep_available">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip"  ng-if="screeningData[0].pep_url === ''"   ng-mouseover="mouseover(screeningData[0].pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].pep_url)" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip"  ng-if="screeningData[0].pep_url !== ''"   ng-mouseover="mouseover(screeningData[0].pep_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].pep_url)" style="cursor: pointer;">[1P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-street-view text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'pepalert', $index)" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">PEP Alerts</span>                                                                    
                                                </li>                                                                    
                                                <li ng-show="screeningData[0].sanction_url || screeningData[0].sanction_available">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].sanction_url === ''" ng-mouseover="mouseover(screeningData[0].sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].sanction_url)" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].sanction_url !== ''" ng-mouseover="mouseover(screeningData[0].sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].sanction_url)" style="cursor: pointer;">[1P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-ban text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'sanction', $index)" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">Sanctions</span>                                                                   
                                                </li>                                                                    
                                                <li ng-hide="screeningData[0].sanction_url || screeningData[0].sanction_available">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].sanction_url === ''" ng-mouseover="mouseover(screeningData[0].sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].sanction_url)" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].sanction_url !== ''"  ng-mouseover="mouseover(screeningData[0].sanction_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].sanction_url)" style="cursor: pointer;">[1P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-ban  text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'sanction', $index)" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">Sanctions</span>                                                                   
                                                </li>                                                                    
                                                <li ng-show="screeningData[0].high_risk_jurisdiction === 'high'">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-globe text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">Jurisdiction</span>                                                                    
                                                </li>                                                                    
                                                <li ng-hide="(screeningData[0].high_risk_jurisdiction === 'high') || (screeningData[0].high_risk_jurisdiction === 'medium')">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-globe text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">Jurisdiction</span>                                                                    
                                                </li>
                                                <li ng-show="screeningData[0].high_risk_jurisdiction === 'medium'">
                                                    <span class="help-block">
                                                        <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                        <span class="text-pink">[0S]</span>
                                                    </span>
                                                    <i class="fa fa-globe text-light-yellow2 showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                    <span class="help-block text-dark-grey2">Jurisdiction</span>                                                                    
                                                </li>     
                                                <li ng-hide="screeningData[0].finance_Crime_url === ''">
                                                        <span class="help-block">
                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].finance_Crime_url ===''" ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].finance_Crime_url !== ''" ng-mouseover="mouseover(screeningData[0].finance_Crime_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].finance_Crime_url)" style="cursor: pointer;">[1P]</span>
                                                                <span class="text-pink">[0S]</span>
                                                            </span>
                                                        <i class="fa fa-gavel text-light-orange showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'financeCrime')" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                        <span class="help-block text-dark-grey2">Finance Crime</span> 
                                                        <!--<i class="fa fa-gavel text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                </li>
                                                <li ng-show="screeningData[0].finance_Crime_url === ''">
                                                        <span class="help-block">
                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].finance_Crime_url === '' "  ng-mouseover="mouseover('', 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup('')" style="cursor: pointer;">[0P]</span>
                                                                <span class="text-blue1 mar-r5 mar-r5 companyInfoTooltip" ng-if="screeningData[0].finance_Crime_url !==''" ng-mouseover="mouseover(screeningData[0].finance_Crime_url, 'companyInfoTooltip');" ng-style="myStyle2" ng-click="corporatePopup(screeningData[0].finance_Crime_url)" style="cursor: pointer;">[1P]</span>
                                                                <span class="text-pink">[0S]</span>
                                                            </span>
                                                        <i class="fa fa-gavel text-dark-green2 showTooltip f-21 mar-y10" ng-click="openmodal(screeningData[0], 'financeCrime')" ng-mouseover="tooltipdtata(screeningData[0])"></i>
                                                        <span class="help-block text-dark-grey2">Finance Crime</span> 
                                                        <!--<i class="fa fa-gavel text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>-->
                                                </li>                                                                     
                                            </ul>
                                        </div>
                                        <!--  Custom Accordion Starts  -->
                                        <!--                                        <div class="panel-group custom-accordion center-row1 " id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                                                                    <span ng-show="pageloader.screeningLoader" class="custom-spinner height-200px">
                                                                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                                                                    </span>
                                                                                    <div class="panel panel-default" ng-repeat="screening in screeningData">
                                                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                                                            <h4 class="panel-title">
                                                                                                <a class="collapsed clearfix">
                                                                                                     ng-click="selectedPanel('#collapse'+$index,$index)" 
                                                                                                    <span class="pull-left width-50">{{screening.name}}</span>
                                                                                                    <ul class="list-inline pull-left width-50 custom-list action-icon-list item-5 text-right">
                                                                                                        <li class="pad-0" ng-show="(screening['sanction_bst:description']) || (screening.listId) || (screening.sanction_url) || (screening.sanction_source)">
                                                                                                            <i class="fa fa-ban  text-light-orange showTooltip" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-hide="(screening['sanction_bst:description']) || (screening.listId) || (screening.sanction_url) || (screening.sanction_source)">
                                                                                                            <i class="fa fa-ban text-dark-green2 showTooltip" ng-click="openmodal(screening, 'sanction', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-hide="screening.pep_url">
                                                                                                            <i class="fa fa-street-view text-dark-green2 showTooltip " ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-show="screening.pep_url">
                                                                                                            <i class="fa fa-street-view text-light-orange showTooltip " ng-click="openmodal(screening, 'pepalert', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                           <li class="pad-0">
                                                                                                              <i class="fa fa-gavel text-dark-green2 showTooltip" ng-click="openmodal(screening, 'database', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                          </li> 
                                                                                                        <li class="pad-0">
                                                                                                            <i class="fa fa-database text-dark-green2 showTooltip" ng-click="openmodal(screening, 'database', $index)" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-hide="(screening.high_risk_jurisdiction === 'high') || (screening.high_risk_jurisdiction === 'medium')">
                                                                                                            <i class="fa fa-globe text-dark-green2 showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-show="screening.high_risk_jurisdiction === 'medium'">
                                                                                                            <i class="fa fa-globe text-yellow2 showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                        <li class="pad-0" ng-show="screening.high_risk_jurisdiction === 'high'">
                                                                                                            <i class="fa fa-globe text-light-red showTooltip" ng-click="openmodal(screening, 'high_risk_jurisdiction')" ng-mouseover="tooltipdtata(screening)"></i>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </a>
                                                                                            </h4>
                                                                                        </div>
                                                                                        <div id="{{'collapse' + $index}}" class="panel-collapse collapse" ng-class="{in: selectedPanelToCollapse[$index]}">
                                                                                            <div class="panel-body">
                                                                                                <ul class="list-inline custom-list item-3">
                                                                                                    <li>Identified Risk :
                                                                                                        <span class="text-light-orange">2</span>
                                                                                                    </li>
                                                                                                    <li>Ownership :
                                                                                                        <span class="text-grey">15%</span>
                                                                                                    </li>
                                                                                                    <li>CEO :
                                                                                                        <span class="text-grey">Kate Collings</span>
                                                                                                    </li>
                                                                                                </ul>
                                                                                                <ul class="list-unstyled width-90 custom-list left-icon-list">
                                                                                                    <li>
                                                                                                        <a class="fa fa-ban left-icon"></a>
                                                                                                        <span class="help-block text-light-orange">Sanctions</span>
                                                                                                        <a class="help-block text-blue1">
                                                                                                            <span>Source :</span> {{screening.source}}</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                        
                                                                                    </div>
                                                                                </div>-->
                                        <span ng-show="pageloader.screeningLoader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                        </span>
                                        <div class="alert-message alert-message-wrapper" ng-if="screeningData.length === 0">Data Not Found</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Screening Resuls Ends -->

                    <!-- Adverse news Section -->
                    <div class="entity-item">
                        <div class="recent-pep-wrapper">
                            <h3 class="text-dark-grey2 mar-b0">News</h3>
                            <div class="entity-result-item entity-result-item-lg  v-scroll" ng-show="entitySearchResult.list['adverse_news'].length > 0">
                                <ul class="media-list">
                                    <li class="media mar-t10" ng-repeat="news in entitySearchResult.list['adverse_news']">
                                        <div class="recent-linkedin">
                                            <div class="linkedin-content">
                                                <p class="mar-b0"><a target="_blank" href="{{news.url}}">{{news.title}}</a></p>
                                                <!--                                                <p class="progress-text">
                                                                                                    <span class="date" ng-if="news.published !== '' && news.published">Published on {{news.published| date : 'dd/MM/yyyy'}}</span>
                                                                                                </p>
                                                                                                <p ng-if="news.text !== '' && news.text">
                                                                                                    Description : {{news.text}}
                                                                                                </p>-->
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="entity-result-item custom-result-item alert-message-wrapper" ng-hide="entitySearchResult.list['adverse_news'].length > 0">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.data_adverse_news">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.data_adverse_news" >Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!-- Adverse news section Ends -->
                </div>
                <!-- Career Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Career'">
                    <!-- Activity Locations Starts -->
                    <div class="entity-item activity-location-wrapper">
                        <h3>Activity Locations</h3>
                        <div class="activity-location-chart-wrapper">
                            <div id="person-career-location-chart"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper associated-locations" ng-show="entitySearchResult.list['is_career_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!-- Activity Locations Ends -->

                    <!-- Activity Locations Starts -->
                    <div class="entity-item career-pregression-wrapper">
                        <h3>Career Progression</h3>
                        <div class="career-progression-chart-wrapper">
                            <div  ng-hide="entitySearchResult.list['is_career_timelinechart']">
                                <div id="Career-timeline"></div>
                            </div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper career-progression" ng-show="entitySearchResult.list['is_career_timelinechart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.carrerProgression">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.carrerProgression">Data Not Found</div>

                            </div>
                        </div>
                    </div>
                    <!-- Activity Locations Ends -->

                    <!-- Role Ratio Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Role Ratio</h3>
                        <div class="role-ration-chart-wrapper">
                            <div ng-hide="entitySearchResult.list['is_role_based_pie_chart']">
                                <div id="role-based-pie-chart"></div>
                            </div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-show="entitySearchResult.list['is_role_based_pie_chart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.role_ratio">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.role_ratio">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!-- Role Ratio Ends -->

                    <!-- Professional Connections Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Professional Connections</h3>
                        <div class="professional-connections-chart-wrapper">
                            <div ng-hide="entitySearchResult.list['is_connection_role_based_pie_chart']">
                                <div id="connection-role-based-pie-chart"></div>
                            </div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper professional-connections" ng-show="entitySearchResult.list['is_connection_role_based_pie_chart']">
                                <div class="alert-message loading-message" ng-if ="entitySearchResult.data_not_found.professional_connection">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if ="!entitySearchResult.data_not_found.professional_connection">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!-- Professional Connections Ends -->


                    <!-- Similar Profiles Starts -->
                    <div class="top-contributors-wrapper">
                        <h3>Similar Profiles</h3>
                        <div class="entity-result-item" ng-show=" entitySearchResult.list['similar_profiles_list'].length > 0">
                            <ul class="media-list">
                                <li class="media" ng-repeat="entity in entitySearchResult.list['similar_profiles_list']">
                                    <!-- ng-repeat here -->
                                    <div class="total-contributors">
                                        <div class="contributors-bio">
                                            <div class="contributors-bio-dp">
                                                <img class="image-object" src="../assets/images/menu_icons/icon_profile_settings.png" />
                                            </div>
                                            <div class="contributors-bio-details">
                                                <h5>{{entity.name}}</h5>
                                                <p ng-if="entity.job_title !== '' && entity.job_title">{{entity.job_title}}, </p>
                                                <p ng-if="entity.current_company !== '' && entity.current_company"> {{entity.current_company}}</p>
                                                <p>{{entity.phone}}</p>
                                                <p>{{entity.address}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="right-section-item alert-message-wrapper" ng-hide=" entitySearchResult.list['similar_profiles_list'].length > 0">
                            <div class="alert-message loading-message" ng-if ="entitySearchResult.data_not_found.Similarprofile">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if ="!entitySearchResult.data_not_found.Similarprofile">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Similar Profiles Ends -->
                </div>
                <!-- Career Right Section Ends -->

                <!-- Academics Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Academics'">

                    <!-- Associated Locations Starts -->
                    <div class="entity-item activity-location-wrapper">
                        <h3>Associated Locations</h3>
                        <div class="activity-location-chart-wrapper">
                            <div id="person-academic-location-chart"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper associated-locations" ng-show="entitySearchResult.list['is_person_academic_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!-- Associated Locations Ends -->

                    <!-- Activity Locations Starts -->
                    <div class="entity-item career-pregression-wrapper">
                        <h3>Academic Timeline</h3>
                        <div ng-hide="entitySearchResult.list['is_academic_timelinechart']">
                            <div id="academic-timeline"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper career-progression" ng-show="entitySearchResult.list['is_academic_timelinechart']">
                            <div class="alert-message loading-message"  ng-if="entitySearchResult.data_not_found.academic_timeline">Loading...</div>
                            <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.academic_timeline">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Activity Locations Ends -->

                    <!-- Role Ratio Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Academic Background </h3>
                        <div class="role-ration-chart-wrapper">
                            <div ng-hide="entitySearchResult.list['academic-based-pie-chart']">
                                <div id="academic-based-pie-chart"></div>
                            </div>
                            <!-- <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-hide="false">
                                    <div class="alert-message loading-message">Loading...</div>
                            </1div> -->
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-show="entitySearchResult.list['academic-based-pie-chart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.academic_background">Loading...</div>
                                <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.academic_background">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!-- Role Ratio Ends -->

                    <!--  Related Entities Starts  -->
                    <div class="entity-item related-entities-wrapper">
                        <h3>Skills</h3>
                        <div class="customEntities-result-item">
                            <div ng-hide="entitySearchResult.list['is_skills_tag_cloud_chart']">
                                <div id="skills-tag-cloud-chart"></div>
                            </div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper related-entries" ng-show="entitySearchResult.list['is_skills_tag_cloud_chart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.skill_data">Loading...</div>
                                <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.skill_data">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!--  Related Entities Starts  -->

                </div>
                <!-- Academics Right Section Ends -->

                <!-- Finance Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Finance'">

                    <!-- Role Ratio Starts -->
                    <div class="entity-item competitor-compensation-wrapper">
                        <h3>Competitor Compensation</h3>
                        <div class="competitor-compensation-chart-wrapper" ng-show="entitySearchResult.list['competitor_compensation'].length > 0">
                            <!-- <div id="bubbleCompittitorsFinanceChart"></div> -->
                            <table class="table associated-table" ng-show="entitySearchResult.list['competitor_compensation'].length > 0">
                                <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>COMPANY</th>
                                        <th>COMPENSATION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="compen in entitySearchResult.list['competitor_compensation']">
                                        <td>{{compen.name}}</td>
                                        <td>{{compen.company}}</td>
                                        <td>{{compen.compensation}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper " ng-hide="entitySearchResult.list['competitor_compensation'].length > 0">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.competitor_compensation_data">Loading...</div>
                            <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.competitor_compensation_data">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Role Ratio Ends -->

                    <!-- Role Ratio Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Earnings Ratio</h3>
                        <div class="role-ration-chart-wrapper" ng-show="!entitySearchResult.list['is_earning_ratio_pie_chart']">
                            <div id="earning-ratio-pie-chart"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-show="entitySearchResult.list['is_earning_ratio_pie_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.earning_ratio">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.earning_ratio">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Role Ratio Ends -->

                    <!-- Professional Connections Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Holding Ratio</h3>
                        <div class="professional-connections-chart-wrapper">
                            <div id="holding-pie-chart"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper professional-connections" ng-show="entitySearchResult.list['is_holding_pie_chart']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.holding_ratio">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.holding_ratio">Data Not Found</div>
                            </div>
                        </div>
                    </div>
                    <!-- Professional Connections Ends -->



                </div>
                <!-- Finance Right Section Ends -->


                <!-- Academics Right Section Starts -->
                <div class="risk-score-panel right-risk-wrapper" ng-show="mainInfoTabType === 'Risks'">

                    <!-- Ratios Ratio Starts -->
                    <div class="entity-item ratios-ratio-wrapper">
                        <h3>Risk Ratio</h3>
                        <div class="ratios-ratio-chart-wrapper" ng-hide="entitySearchResult.list['is_risk_alters_pie_chart']">
                            <div id="pieChartRiskRatio"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_risk_alters_pie_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.risk_ratio_data">Loading...</div>
                            <div class="alert-message alert-message-wrapper"  ng-if="!entitySearchResult.data_not_found.risk_ratio_data">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Ratios Ratio Ends -->
                    <!-- Political And Organisation Support Starts -->
                    <div class="entity-item political-donations-wrapper">
                        <h3>Political Donations Trend</h3>
                        <div class="political-donations-chart-wrapper">
                            <div id="politicaldonations"></div>
                        </div>
                    </div>
                    <!-- Political And Organisation Support Ends -->

                    <!-- Political Ratio Starts -->
                    <div class="entity-item political-donations-wrapper">
                        <h3>Political Ratio</h3>
                        <div id="politicalOrgsWrapper">
                            <div id="politicalOrgs"></div>
                        </div>

                    </div>
                    <!-- Political Ratio Ends -->

                    <!-- Donation to Politicians Starts -->
                    <div class="entity-item ratios-ratio-wrapper">
                        <h3>Donation to Politicians </h3>
                        <div  class="ratios-ratio-chart-wrapper" ng-hide="entitySearchResult.list['is_donation_ratio']">
                            <div id="pieDonationRatio"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_donation_ratio']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.donation_politician_data">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.donation_politician_data">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Donation to Politicians Ends -->

                    <!-- Donation to Organisations Starts -->
                    <div class="entity-item donation-table">
                        <h3>Donation to Organisations </h3>
                        <div class="ratios-ratio-chart-wrapper" ng-hide="entitySearchResult.list['is_org_donation_ratio']">
                            <div id="organDonationRatio"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_org_donation_ratio']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.orgdination_data">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.orgdination_data">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Donation to Organisations Ends -->

                </div>
                <!-- Academics Right Section Ends -->


                <!-- Document Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Social Media'">

                    <!-- Twitter Tags Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Twitter Tags</h3>
                        <div class="role-ration-chart-wrapper" ng-show="!entitySearchResult.list['is_twitter_tag_chart']">
                            <div id="tagPersonTwitterTags"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper popular-tags" ng-show="entitySearchResult.list['is_twitter_tag_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.twitter_tags">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.twitter_tags">Data Not Found</div>
                        </div>

                    </div>
                    <!-- Twitter Tags Ends -->
                    <!-- Interest Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Interest</h3>
                        <div class="professional-connections-chart-wrapper">
                            <div ng-hide="entitySearchResult.list['is_interest_ratio']"><div id="interest_ratio"></div></div>
                            <div id="interest_ratio"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper" ng-show="entitySearchResult.list['is_interest_ratio']">
                                <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.interest_data">Loading...</div>
                                <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.interest_data">Data Not Found</div>
                            </div>
                        </div>
                    </div>

                    <div class="entity-item professional-connections-wrapper">
                        <h3>Follower Location</h3>
                        <div class="person-followers-location-wrapper">
                            <div id="person-social-location-chart"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper" ng-show="entitySearchResult.list['is_person_social_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!-- Interest Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Sentiments</h3>
                        <div class="general-sentiments-chart-wrapper" id="pieChartGeneralSentiment">
                        </div>
                    </div>
                    <!-- Sentiments Ends -->
                    <!-- Topic Explorer Starts -->
                    <!-- <div class="entity-item activity-location-wrapper">
                            <h3>Topic Explorer</h3>
                            <div class="activity-location-chart-wrapper">
                                    <div id=""></div>
                                    <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper competitor-compensation">
                                            <div class="alert-message loading-message">Loading...</div>
                                    </div>
                            </div>
                    </div> -->
                    <!-- Locations Ends -->
                    <div class="interaction-ratio-wrapper professional-connections-wrapper">
                        <h3>Twitter Interaction Ratio</h3>
                        <div class="interaction-ratio-chart-wrapper" ng-show="!entitySearchResult.list['is_twitter_interaction_ratio']">
                            <div class="professional-connections-chart-wrapper" id="interaction-ratio"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper business-outlook-right" ng-show="entitySearchResult.list['is_twitter_interaction_ratio']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.twitter_inter_ratio">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.twitter_inter_ratio">Data Not Found</div>
                        </div>
                    </div>

                    <div class="interaction-ratio-wrapper professional-connections-wrapper">
                        <h3>Twitter followers Trends</h3>
                        <div class="professional-connections-chart-wrapper" ng-show="!entitySearchResult.list['is_twitter_follower_trends']">
                            <div class="followers-trend-chart-wrapper" id="lineChartFollowersTrend">
                            </div>
                        </div>
                        <div class="right-section-item alert-message-wrapper" ng-show="entitySearchResult.list['is_twitter_follower_trends']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.twitter_fol_trend">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.twitter_fol_trend">Data Not Found</div>
                        </div>
                    </div>
                </div>
                <!-- Document Right Section Ends -->


                <!-- Transactions Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Transactions'">
                    <!-- <h3>Alert Summary</h3> -->

                    <!-- Professional Connections Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Transaction Types</h3>
                        <div class="professional-connections-chart-wrapper" ng-show="!entitySearchResult.list['is_transaction_type_pie_chart']">
                            <div id="transaction-type-pie-chart"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper professional-connections" ng-show="entitySearchResult.list['is_transaction_type_pie_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.transaction_types">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.transaction_types">Data Not Found</div>
                        </div>

                    </div>
                    <!-- Professional Connections Ends -->

                    <!-- Role Ratio Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Amount Ratio</h3>
                        <div class="role-ration-chart-wrapper" ng-show="!entitySearchResult.list['is_transaction_amount_type_pie_chart']">
                            <div id="transaction-amount-type-pie-chart"></div>
                        </div>
                        <!-- <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-show="entitySearchResult.list['is_transaction_amount_type_pie_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                        </div> -->
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper role-ratio" ng-show="entitySearchResult.list['is_transaction_amount_type_pie_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.amount_ratio">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.amount_ratio">Data Not Found</div>
                        </div>

                    </div>
                    <!-- Role Ratio Ends -->


                    <!-- Activity Locations Starts -->
                    <div class="entity-item activity-location-wrapper" ng-show="mainInfoTabType === 'Transactions'">
                        <h3>Locations</h3>
                        <div class="activity-location-chart-wrapper">
                            <div id="transactions-locations-chart" class="chartContentDiv" ng-show="!entitySearchResult.list['is_transaction_location_chart']"></div>
                            <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper associated-locations" ng-show="entitySearchResult.list['is_transaction_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!-- Activity Locations Ends -->
                </div>
                <!-- Transactions Right Section Ends -->

                <!-- Document Right Section Starts -->
                <div class="risk-score-panel right-socialmedia-wrapper" ng-show="mainInfoTabType === 'Documents'">

                    <!-- Assets Ratio Starts -->
                    <div class="entity-item professional-connections-wrapper">
                        <h3>Assets Ratio</h3>
                        <div class="professional-connections-chart-wrapper" ng-show="!entitySearchResult.list['is_assets_ration_pie_chart']">
                            <div id="pieChartAssetsRatio"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper assets-ratio" ng-show="entitySearchResult.list['is_assets_ration_pie_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.asserts_ratio">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.asserts_ratio">Data Not Found</div>
                        </div>

                    </div>
                    <!-- Assets Ratio Ends -->

                    <!-- Popular Tags Starts -->
                    <div class="entity-item role-ratio-wrapper">
                        <h3>Popular Tags</h3>
                        <div class="role-ration-chart-wrapper" ng-show="!entitySearchResult.list['is_person_popular_chart']">
                            <div id="personPopularTags"></div>
                        </div>
                        <div class="right-section-item alert-message-wrapper dittoBlurChart-wrapper popular-tags" ng-show="entitySearchResult.list['is_person_popular_chart']">
                            <div class="alert-message loading-message" ng-if="entitySearchResult.data_not_found.popular_tags">Loading...</div>
                            <div class="alert-message alert-message-wrapper" ng-if="!entitySearchResult.data_not_found.popular_tags">Data Not Found</div>
                        </div>
                    </div>
                    <!-- Popular Tags Ends -->


                    <!-- Locations Starts -->
                    <div class="entity-item activity-location-wrapper" ng-show="mainInfoTabType === 'Documents'">
                        <h3>Locations</h3>
                        <div class="activity-location-chart-wrapper">
                            <div id="documents-locations-chart" class="chartContentDiv" ng-show="!entitySearchResult.list['is_document_location_chart']"></div>
                            <div class="entity-result-item custom-result-item alert-message-wrapper dittoBlurChart-wrapper associated-locations" ng-show="entitySearchResult.list['is_document_location_chart']">
                                <div class="alert-message loading-message">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <!-- Locations Ends -->
                </div>
                <!-- Document Right Section Ends -->


            </div>

        </div>
        <!--  Entity Person Content Right Ends  -->
    </div>
</div>
<!--  Entity Person Content Ends  -->

<div ng-show="noDataFoundText">
    <span>{{noDataFoundText}}</span>
</div>
<!--  Entity Person Ends  -->

<!-- <script src="http://d3js.org/d3.v4.min.js"></script> -->
<script>
    $(document).ready(function () {
    /*Custom Scroll Bar*/
    $('.widget-content').mCustomScrollbar({
    axis: "y",
            theme: "minimal-dark"
    });
    });
    $('.entity-result-item').mCustomScrollbar({
    axis : "y"
    });
    $(document).ready(function () {
    $(".db-util-process-tab-discover").mThumbnailScroller({
    axis: "x"
    });
    $(".item-submmenu-list").mThumbnailScroller({
    axis : "x"
    });
    });
    $('.v-scroll').mCustomScrollbar({
    axis: "y"
    }); ''

            $('.bloomberg-stock-detail-wrapper').mCustomScrollbar({
    axis : "y",
            theme : "minimal-dark"
    });
    $('.role-ration-chart-wrapper, .professional-connections-chart-wrapper, #politicalOrgsWrapper, .ratios-ratio-chart-wrapper').mCustomScrollbar({
    axis : "y",
            theme : "minimal-dark"
    });
    $('.whoweare-result-item').mCustomScrollbar({
    axis : "y",
            theme : "minimal-dark"
    });
    $('.whoweare-result-item').find('p').css("overflow", "visible").css("height", "auto");
    $('.whoweare-result-item').css("height", "214px")
            // $('.associated-table-wrapper').mCustomScrollbar({
            // 	axis: "y"
            // });
            // $('.holders-table-wrapper').mCustomScrollbar({
            // 	axis: "y"
            // });
            $('.entityPersonNew-inner-wrapper .entity-company-content-right, .competitor-compensation-chart-wrapper').mCustomScrollbar({
    axis: "y",
            theme: "minimal-dark"
    });
    //initialize date pickers
    $('#transSearchFrom').datepicker();
    $('#transSearchTo').datepicker();
    $("#transSearchTo").datepicker({ minDate: new Date()});</script>
            <script>
                $(document).ready(function () {
                    /*Custom Scroll Bar*/
                    $('.widget-content').mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                    $('.main-content-descriptionScrollCls').mCustomScrollbar({
                        axis: "y",
                        theme: "minimal-dark"
                    });
                    
                });
                $(document).ready(function () {
                    $(".db-util-process-tab-discover").mThumbnailScroller({
                        axis: "x"
                    });
                    $(".item-submmenu-list, .custom-commitee-tabs > .nav-tabs , .custom-fraud-tabs > .nav-tabs").mThumbnailScroller({
                        axis: "x"
                    });
                });
                $('.entity-result-item').mCustomScrollbar({
                    axis: "y"
                });
                $('.ratios-ratio-chart-wrapper').mCustomScrollbar({
                    axis: "y",
                    theme: "minimal-dark"
                });
                setTimeout(function () {
                    $('#politicalOrgsWrapper').mCustomScrollbar({
                        axis: "y"
                    });
                    $('.media-custom-scroll').mCustomScrollbar({
                        axis: "y"
                    });
                }, 1000);
                $(
                    '.entityCompanyNew-inner-wrapper .entity-company-content-right, .latest-news-section-item, .employee-academics-chart-wrapper'
                ).mCustomScrollbar({
    
                    axis: "y",
                    theme: "minimal-dark"
                });
                $('.bloomberg-stock-inner-wrapper').mCustomScrollbar({
                    axis: "y",
                    theme: "minimal-dark"
                });
                $('.holders-table-wrapper').mCustomScrollbar({
                    axis: "y"
                });
                $(".panel .panel-scroll").mCustomScrollbar({
                    axis: "y",
                    theme: "minimal-dark"
                });
                $(".panel .panel-h-scroll").mCustomScrollbar({
                    axis: "xy",
                    theme: "minimal-dark"
                });
                $("#accordion").mCustomScrollbar({
                    axis: "y",
                    theme: "minimal"
                });
             
            </script>