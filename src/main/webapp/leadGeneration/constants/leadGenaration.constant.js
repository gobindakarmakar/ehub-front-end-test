'use strict';
angular.module('leadGenerationApp').constant('pieChartsConst',{
	cardDurationPie : [ //Pie data
		{
			'key': "0-5(Duration)",
			'value': 6
		}, {

			'key': "5-10(Duration)",
			'value': 5
		}, {

			'key': "10-15(Duration)",
			'value': 7
		}
		, {

			'key': ">15(Duration)",
			'value': 2
		}
	],
	cardTypePie : [ //Pie data
		{
			'key': "Bank",
			'value': 6
		}, {

			'key': "Credit Card Co",
			'value': 5
		}, {

			'key': "Club",
			'value': 7
		}
		, {

			'key': "others",
			'value': 2
		}
	],
	genderPie : [
		{
			'key': "Male",
			'value': 6
		}, {
			'key': "Female",
			'value': 3
		}, {
			'key': "Other",
			'value': 1
		}
	],
	
	issuedByPie : [ //Pie data
		{
			'key': "ISRA",
			'value': 10
		}, {

			'key': "VISA",
			'value': 8
		}, {

			'key': "MASTER CARD",
			'value': 7
		}
		, {

			'key': "DINERS",
			'value': 5
		}
	],
	usagePie  :  [ //Pie data
		{
			'key': "LOCAL",
			'value': 10
		}, {

			'key': "ABROAD",
			'value': 7
		}

	],
	formPie  : [ //Pie data
		{
			'key': "POS",
			'value': 6
		}, {

			'key': "ATM",
			'value': 5
		}
		, {

			'key': "ONLINE",
			'value': 2
		}
	],
	typePie : [ //Pie data
		{
			'key': "STANDARD",
			'value': 6
		}, {

			'key': "EMI",
			'value': 5
		}, {

			'key': "CASHBACK",
			'value': 6
		}
		, {

			'key': "GIFT",
			'value': 2
		}
	],  
	spendingGenrePie : [ //Pie data
        {
            'key': "HealthCare",
            'value': 600
        }, {

            'key': "Fashion",
            'value': 500
        }, {

            'key': "Electronics",
            'value': 300
        }, {

            'key': "FMCG",
            'value': 250
        }, {
            'key': "Food And Dining",
            'value': 200
        }, {

            'key': "Invesments",
            'value': 150
        }, {

            'key': "Entertainment",
            'value': 100
        }

    ],
   topMerchantPie : [ //Pie data
        {
            'key': "Walmart",
            'value': 600
        }, {

            'key': "Google Movies",
            'value': 600
        }, {

            'key': "KFC",
            'value': 300
        }, {

            'key': "Subway",
            'value': 250
        }, {
            'key': "Amazon",
            'value': 200
        }, {

            'key': "Ebay",
            'value': 150
        }, {

            'key': "Apple Store",
            'value': 100
        }

    ]
		  
	   	})
	   	.constant('worldMapConst',{
	   		markersTrend : [{
	   			'lat': '37.5364134', //worldMap Data
	   			'long': '-122.24553639999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Redwood Shores, CA",
	   			'source': "company.linkedin.com",
	   			'title': "Oracle",
	   			"amount":"$8000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '55.378051',
	   			'long': '-3.43597299999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "united kingdom",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$10000",
	   			'fillColor': '#2c72ae'
	   		}, {
	   			'lat': '87.627815', //worldMap Data
	   			'long': '41.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicago",
	   			'source': "company.linkedin.com",
	   			'title': "Chicago",
	   			"amount":"$12500",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '80.627815', //worldMap Data
	   			'long': '31.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicagocity",
	   			'source': "company.Microsoft.com",
	   			'title': "Chicagocity",
	   			"amount":"$11000",
	   			'fillColor': '#2c72ae'
	   		},{
	   			'lat': '57', //worldMap Data
	   			'long': '2',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Abeerden,Scotland",
	   			'source': "company.facebook.com",
	   			'title': "TATA",
	   			"amount":"$8000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '34',
	   			'long': '138',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Adelaide, Australia",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$10000",
	   			'fillColor': '#2c72ae'
	   		}, {
	   			'lat': '52', //worldMap Data
	   			'long': '4.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Amsterdam, Netherlands",
	   			'source': "company.linkedin.com",
	   			'title': "Chicago",
	   			"amount":"$12500",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '39.627815', //worldMap Data
	   			'long': '32.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Ankara, Turkey",
	   			'source': "company.Microsoft.com",
	   			'title': "Chicagocity",
	   			"amount":"$11000",
	   			'fillColor': '#2c72ae'
	   		}
	   		],
	   		markers : [{
	   			'lat': '37.5364134', //worldMap Data
	   			'long': '-122.24553639999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Redwood Shores, CA",
	   			'source': "company.linkedin.com",
	   			'title': "Oracle",
	   			"amount":"$12000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '55.378051',
	   			'long': '-3.43597299999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "united kingdom",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$12000",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '87.627815', //worldMap Data
	   			'long': '41.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicago",
	   			'source': "company.linkedin.com",
	   			"amount":"$2000",
	   			'title': "Chicago",
	   			'fillColor': '#E94488'
	   		}
	   		],
	   		markers1 : [{
	   			'lat': '37.5364134', //worldMap Data
	   			'long': '-122.24553639999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Redwood Shores, CA",
	   			'source': "company.linkedin.com",
	   			'title': "Oracle",
	   			"amount":"$8000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '55.378051',
	   			'long': '-3.43597299999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "united kingdom",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$10000",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '87.627815', //worldMap Data
	   			'long': '41.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicago",
	   			'source': "company.linkedin.com",
	   			'title': "Chicago",
	   			"amount":"$12500",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '80.627815', //worldMap Data
	   			'long': '31.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicagocity",
	   			'source': "company.Microsoft.com",
	   			'title': "Chicagocity",
	   			"amount":"$11000",
	   			'fillColor': '#E94488'
	   		},{
	   			'lat': '57', //worldMap Data
	   			'long': '2',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Abeerden,Scotland",
	   			'source': "company.facebook.com",
	   			'title': "TATA",
	   			"amount":"$8000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '34',
	   			'long': '138',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Adelaide, Australia",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$10000",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '52', //worldMap Data
	   			'long': '4.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Amsterdam, Netherlands",
	   			'source': "company.linkedin.com",
	   			'title': "Chicago",
	   			"amount":"$12500",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '39.627815', //worldMap Data
	   			'long': '32.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Ankara, Turkey",
	   			'source': "company.Microsoft.com",
	   			'title': "Chicagocity",
	   			"amount":"$11000",
	   			'fillColor': '#E94488'
	   		}
	   		],
	   		markers2 : [{
	   			'lat': '37.5364134', //worldMap Data
	   			'long': '-122.24553639999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Redwood Shores, CA",
	   			'source': "company.linkedin.com",
	   			'title': "Oracle",
	   			"amount":"$8000",
	   			'fillColor': '#E94488'
	   		},
	   		{
	   			'lat': '55.378051',
	   			'long': '-3.43597299999999',
	   			'mark': "assets/images/redpin.png",
	   			'name': "united kingdom",
	   			'source': "companieslist.co.uk",
	   			'title': "companieslist.co.uk",
	   			"amount":"$10000",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '87.627815', //worldMap Data
	   			'long': '41.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicago",
	   			'source': "company.linkedin.com",
	   			'title': "Chicago",
	   			"amount":"$12500",
	   			'fillColor': '#E94488'
	   		}, {
	   			'lat': '80.627815', //worldMap Data
	   			'long': '31.88205',
	   			'mark': "assets/images/redpin.png",
	   			'name': "Chicagocity",
	   			'source': "company.Microsoft.com",
	   			'title': "Chicagocity",
	   			"amount":"$11000",
	   			'fillColor': '#E94488'
	   		}]
	   		
	   	})
	   	.constant('areaLineChartConst',{
	   		
	 areaLineChart : [{
        date: "3-Apr-09",
        close: "500.32"
    }, {
        date: "4-Apr-10",
        close: "1000"
    }, {
        date: "1-May-11",
        close: "600"
    }, {
        date: "30-Apr-12",
        close: "1300"
    }, {
        date: "27-Apr-13",
        close: "900"
    }, {
        date: "26-Apr-14",
        close: "1400"
    }, {
        date: "25-Apr-15",
        close: "1100"
    }, {
        date: "5-May-16",
        close: "2200"
    }, {
        date: "11-Apr-17",
        close: "1500"
    }, {
        date: "9-Apr-18",
        close: "3000"
    }, ],
    areaLineChart1 : [{
        date: "3-Apr-09",
        close: "500.32"
    }, {
        date: "4-Apr-10",
        close: "1000"
    }, {
        date: "1-May-11",
        close: "600"
    }, {
        date: "30-Apr-12",
        close: "1300"
    }, {
        date: "27-Apr-13",
        close: "900"
    }, {
        date: "26-Apr-14",
        close: "1400"
    }, {
        date: "25-Apr-15",
        close: "1100"
    }, {
        date: "5-May-16",
        close: "2200"
    }, {
        date: "11-Apr-17",
        close: "1500"
    }, {
        date: "9-Apr-18",
        close: "3000"
    }, ],
    LineChart : [{
        time: "2017-01",
        value: 289804026
    }, {
    	time: "2017-02",
        value: 48911841
    }, {
    	time: "2017-03",
        value: 76309491
    }, {
    	time: "2017-04",
        value: 135617558
    }, {
    	time: "2017-05",
        value: 23725884
    }, {
    	time: "2017-06",
        value: 157356192
    }, {
    	time: "2017-07",
        value: 203958261
    }, {
    	time: "2017-08",
    	value: 402202155
    }, {
    	time: "2017-09",
    	value: 1113778931
    }, ],
		})
		.constant('israDiamondnameConst',['DIAMOND','Diamond'])   	
