'use strict';
angular.module('leadGenerationApp')
	   .controller('LeadGenerationCustomerPageController', leadGenerationCustomerPageController);

  leadGenerationCustomerPageController.$inject = [
			'$scope',
			'$state',
			'$stateParams',
			'EntityGraphService',
			'$rootScope',
			'$http',
			'EHUB_API',
			'$timeout',
            'LeadGenerationApiService',
            '$uibModal'
		];
		
		function leadGenerationCustomerPageController (
				$scope,
				$state,
				$stateParams,
				EntityGraphService,
				$rootScope,
				$http,
				EHUB_API,
				$timeout,
                LeadGenerationApiService,
                $uibModal
				) {

            var params = {
                token: $rootScope.ehubObject.token
            };
            var StoryId = sessionStorage.getItem("storyId");
            if(StoryId =='undefined'){
        		 $scope.hideStories = false;
        	 }else{
        		 $scope.hideStories = true;
        	 }
            var filterData = {
            		edges:[],
            		vertices:[]
            };
            var dataNew = {
            };
            $scope.StoryName = sessionStorage.getItem("StoryName") ;
            var clusterId = sessionStorage.getItem("clusterId");
           
			$scope.customer_Id = $stateParams.cId;
            if($stateParams.story == 'false'){
            	$rootScope.fromEod = 'false';
            	$('#hideOnEarlyAdopter').css('display','none');
            }else{
            	$rootScope.fromEod = 'true';
            	$('#hideOnEarlyAdopter').css('display','block');
            }
			var vla_inst;
			window.imgPath ="../scripts/VLA/";
			 //code for number formatting
			
		    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
		        var n = this,
		            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
		            decSeparator = decSeparator == undefined ? "." : decSeparator,
		            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
		            sign = n < 0 ? "-" : "",
		            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
		            j = (j = i.length) > 3 ? j % 3 : 0;
		        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
		    };
		    
		    $scope.formatNummerWithComma = function(amount){
		    	var formatAmount;
		    	formatAmount = Number(Math.round(amount)).formatAmt();
		    	return formatAmount;
		    };
		    
		    /*
		     * @purpose: Plot VLA
		     * @created: 26 Mar 2018
		     * @return: date
		     * @author:Amritesh
		     */
			/*Initializing global variables*/ 
    		window.global = {
    	            page:"customerPage",
        			dimensions:{},
        			openArticleModal:function(entityID){
        				var text = entityID.data();
        				var openRiskOverviewModal = $uibModal.open({
        		            templateUrl: 'scripts/act/modal/views/riskoverview.modal.html',
        		            resolve: {
        		            	text: function () {
        		                  return text;
        		                }
        		              },
        		            controller: 'RiskOverViewController',
        		            size: 'lg',
        		            backdrop: 'static',
        		            windowClass: 'marketModal-pulse-modal-wrapper',
        		        });

        			 openRiskOverviewModal.result.then(function () {
        		 			
        		        }, function () {
        		        });
        			}
        		 };
             window.plotCountriesInlinkAnalysis =function(){      
    			 
    		};
//-------------------------------Api call start--------------------------------------------------------------------  
             
             /*
              * @purpose: Get customer details
              * @created: 11th july 2018
              * @params: params(object)
              * @return: success, error functions
              * @author: varsha
              */


            function fetchCustomerDetails() {
                LeadGenerationApiService.getCustomerDetailById($scope.customer_Id,{}).then(function(response) {
                    $scope.customerDetails = response.data.data;
                    if($stateParams.story != 'false'){
                    	getClusterDetail();
                    }
                });
            }

            fetchCustomerDetails();
            
            /*
             * @purpose: Get cluster details
             * @created: 11th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            var originalClusterList = [];
            function getClusterDetail(){
            	 LeadGenerationApiService.getClusterDetail(clusterId,{}).then(function(response) {
            		 $scope.clusterDetail = response.data.data;
            		 originalClusterList = jQuery.extend(true, {}, response.data.data);
            		 setBarValue();
            		 if(StoryId =='undefined'){
                		return false;
                	 }else{
                		 customer__stories_clusters(clusterId);
                	 }
                 });
            }
            
            
            /* @purpose: post cluster rename name
     	     * @created: 16th august 2018
     	     * @author: varsha
     	     */
            
            function postsetClusterName(stroryId,clusterId,data) {
                 LeadGenerationApiService.postsetClusterName(stroryId,clusterId,data).then(function() {
                 }, function() {
                 });
             }
            
            /* @purpose: On key press while renaming
     	     * @created: 16th august 2018
     	     * @author: varsha
     	     */
             
            $scope.renameClusterNameKeyUp = function(event){
            	if(event!=undefined){
            		if(event.which == 27){
            			$scope.renameClusterName('discard');
                		return false;
                	}else if(event.which == 13){
                		$scope.renameClusterName('save');
                		return false;
                	}else{
                		return true;
                	}
            	}
            };
            
            /* @purpose: save edited name for cluster
     	     * @created: 16th august 2018
     	     * @author: varsha
     	     */
            
            $scope.renameClusterName = function(name){
            	if(name == 'discard'){
            		$scope.clusterDetail.cluster_name = originalClusterList.cluster_name;
            		return false;
        	 	}
             	if(StoryId == undefined){
             		StoryId = 'UC3';
             	}
             	if($scope.clusterDetail.cluster_name == originalClusterList.cluster_name){
             		return false;
             	}else if($scope.clusterDetail.cluster_name.length > 0 &&  $scope.clusterDetail.cluster_name != originalClusterList.cluster_name){
             		originalClusterList.cluster_name = $scope.clusterDetail.cluster_name;
             		postsetClusterName(StoryId,clusterId,{'name':$scope.clusterDetail.cluster_name});
             	}else {
             		$scope.clusterDetail.cluster_name= originalClusterList.cluster_name;
             	}
             };	
          
            /*
             * @purpose: Get customer stories details
             * @created: 11th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            function customer__stories_clusters(clusterId){
           	 LeadGenerationApiService.customer__stories_clusters(clusterId,{}).then(function(response) {
           		 $scope.customer_story = response.data.data;
           	 });
           }
           
            /*
             * @purpose: Get customer graph details
             * @created: 11th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            var vlaData =[];
            $scope.NoMerchantData = false;
            function getGraphCustomerById() {
                LeadGenerationApiService.getGraphCustomerById($scope.customer_Id, params).then(function(response) {
                    if(response.data.data.vertices <=0){
                    	$scope.NoMerchantData = true;
                    	return false;
                    }
                    var expenditureDomain = d3.extent(response.data.data.vertices,function(d){if(d.avgExp){return parseFloat(d.avgExp);}});
                     var colorScale = d3.scaleLinear().domain(expenditureDomain).range(["#768A93","#e80202"]);
                    response.data.data.vertices.map(function(d){
                    	if(d.avgExp){
                    	d.evt_color  =  colorScale(parseFloat(d.avgExp));
                    	}
                    });
                    vlaData = jQuery.extend(true, [], response.data.data);
                	window.current_data = response.data;
                	var data = jQuery.extend(true, [], response.data.data);
                    data.vertices.sort(function(x, y) {
                        return d3.ascending(x.avgExp,y.avgExp);
                    });
                    $scope.minAvgExp = data.vertices[0].avgExp;
                    $scope.maxAvgExp = data.vertices[data.vertices.length-1].avgExp;
                    var avgTwentyFifth = data.vertices[data.vertices.length-(data.vertices.length-1 <25?data.vertices.length-1:25)].avgExp;
                    var sliderDiffVallue = $scope.maxAvgExp/100;
                    	$("#avgExpen").find(".price-range-min").html("<span class='fa fa-ils f-10'>" + parseInt( avgTwentyFifth))+"</span> ";
                        $("#avgExpen").find(".price-range-max").html("<span class='fa fa-ils f-10'> " + parseInt( $scope.maxAvgExp))+"</span>";
                    
                    flterVla(parseInt(avgTwentyFifth),parseInt($scope.maxAvgExp));
                    initializeSlider(parseInt(avgTwentyFifth/sliderDiffVallue));
                }, function() {
                   	$('#vla').html("<div class='leadGenErrorDiv'> <span>Failed To Get Data From server</span></div> ");
		        });
            }
            getGraphCustomerById();
  
     	    
            
   //------------------------End of api call-------------------------------------------------
            /*
             * @purpose: plot vla
             * @created: 11th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            function plotVla(){
            	window.global.queryString = 'mirx';
        		global.dimensions.width = $(window).width() - 330;
        	    global.dimensions.height = document.documentElement.clientHeight - 160;
        	    global.dimensions.left = "320px";
        	    global.dimensions.top = "152px";
          	    var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "concentric", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
                 if (typeof custom_config != 'undefined'){
                     vla_options = $.extend({}, custom_config, vla_options);}
                 vla_options.data = window.current_data;
    			 vla_options.adverseNews = "adversenews";
                 vla_inst =  PlotVLA(vla_options);
                 var customerLevelsforLeadGen={
                		 	"Customer":400,
                			"Card":300,
                			"Merchant":1,
                			"MerchantType":200
                	};
                 setTimeout(function(){
                	 if(_ALLVLA.length > 0){
         				for(var i=0; i<_ALLVLA.length; i++){
         					_cy_ = _ALLVLA[i];
         					_cy_.layout({
         				        name: 'concentric',
         				        nodeSpacing: 50,
         				        edgeLengthVal: 45,
         				        animate: true,
         				        randomize: false,
         				        maxSimulationTime: 1500,
        				        concentric:function(node){        				        
        	                    		return customerLevelsforLeadGen[node.data().labelV]?customerLevelsforLeadGen[node.data().labelV]:1;
        	                    	
        				        }
         				    });
         				}
         			}
                 },1000);
            }
          
            /*
             * @purpose: Plot vla when slider stops
             * @created: 12th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            var avg_expendi_start;
            var avg_expendi_end;
            function filterDataForExpenditure() {
                flterVla(avg_expendi_start,avg_expendi_end);
            }
            
            /*
             * @purpose: set max amd min value for avg expenditure
             * @created: 12th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            function setSliderValue(values){
            	 var minMaxExp = [];
                 minMaxExp[0] = $scope.minAvgExp;
                 minMaxExp[1] = $scope.maxAvgExp; 
                 var  expScale = d3.scaleLinear().domain([0, 100]).range(minMaxExp);
                  avg_expendi_start = expScale(values[0]);
                  avg_expendi_end = expScale(values[1]);
                  $("#avgExpen").find(".price-range-min").html("<span class='fa fa-ils f-10'></span> " + parseInt(avg_expendi_start));
                  $("#avgExpen").find(".price-range-max").html("<span class='fa fa-ils f-10'></span> " + parseInt(avg_expendi_end));
            }
           
          
            
            /*
             * @purpose: set max amd min value for avg expenditure
             * @created: 12th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            function flterVla(start,end){
            	 var arrayId = [];
            	 filterData = {
                 		edges:[],
                 		vertices:[]
                 };
            	vlaData.vertices.map(function(d){
            		if(parseInt(d.avgExp) >= start && parseInt(d.avgExp) <= end){
            			filterData.vertices.push(d);
            		}else if(d.labelV =="Customer"){
            			filterData.vertices.push(d);
            		}else{
            			arrayId.push(d.id);
            		}
            	});
            	var edgeData = [];
            	vlaData.edges.map(function(d){
            		if($.inArray(d.from,arrayId) == -1 && $.inArray(d.to,arrayId) == -1 ){
            			filterData.edges.push(d);
            			edgeData.push(d.from);
            			edgeData.push(d.to);
            		}
            	});
            	/*var edgeFilterArray = [];
            	edgeData.map(function(d){
            		if(edgeFilterArray.indexOf(d)==-1){
            			edgeFilterArray.push(d)
            		}
            	})
            	var verticesList = [];
            	console.log(filterData,'filterDatafilterData',arrayId)
            	filterData.vertices.map(function(d,k){
            		if(d.labelV != 'Customer'){
            			if($.inArray(d.id,edgeFilterArray) == -1){
                			console.log(d.id)
                		}else{
                			verticesList.push(d)
                		}
            		}else{
            			verticesList.push(d)
            		}
            	})
            	filterData.vertices = verticesList
            	console.log(filterData,'filterDatafilterData',arrayId)*/
            	dataNew.data = filterData;
            	window.current_data = dataNew;
            	
            	$('#vla').empty();
                 plotVla();
            }
            
            
            /*
             * @purpose:On slider change
             * @created: 12th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
            initializeSlider(0,true);
           function initializeSlider(slideVlue,status){
        	   $( "#slider-range" ).slider({
                   range: true,
                   min: 0,
                   max: 100,
                   values: [slideVlue,100],
                   slide:function(event,ui){
                 	  setSliderValue(ui.values);
                   },
                   stop: function( event, ui ) {
                 	  filterDataForExpenditure(ui.values);
                   }
                 });
        	   if(status){
        		   $('#slider-range .ui-slider-handle:eq(0)').append('<span style ="position:absolute;bottom:90%;left:2%;font-size:10px;" class="price-range-min w-30 value1">' + $('#slider-range').slider('values', 0) + '</span>');
                   $('#slider-range .ui-slider-handle:eq(1)').append('<span style ="position:absolute;bottom:90%;left:2%;font-size:10px;" class="price-range-max w-30 value1">' + $('#slider-range').slider('values', 1) + '</span>');
                
        	   }
                
           } 
                    
            
           

            /*
             * @purpose: Set avg expenditure
             * @created: 12th july 2018
             * @params: params(object)
             * @return: success, error functions
             * @author: varsha
             */
            
				function setBarValue(){
					  $scope.generalExp ={
				        		 'key':'General exp',
				        		 'value':$scope.clusterDetail.general_avg_expenditure,
				         };
				         
				         $scope.avgExpen ={
				        		 'key':'Avg exp',
				        		 'value':$scope.customerDetails.total_amount
				         };
				         
				         if($scope.customerDetails.total_amount > $scope.clusterDetail.general_avg_expenditure){
				        	 $scope.generalExp.percent = (($scope.clusterDetail.general_avg_expenditure / $scope.customerDetails.total_amount) * 100);
				        	 $scope.avgExpen.percent = 100;
				         }else{
				        	 $scope.generalExp.percent = 100;
				        	 $scope.avgExpen.percent = (($scope.customerDetails.total_amount/ $scope.clusterDetail.general_avg_expenditure) * 100);
				         }
					  
				}
       
		}