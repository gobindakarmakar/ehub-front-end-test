'use strict';
/*exported BubbleHierarchyChart */
angular.module('leadGenerationApp')
	.controller('LeadGenerationUC2ClusterController',
		leadGenerationClusterController);

leadGenerationClusterController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'LeadGenerationApiService',
	'$q',
	'$timeout'
];

function leadGenerationClusterController(
	$scope,
	$state,
	$rootScope,
	LeadGenerationApiService,
	$q,
	$timeout
) {
	    $rootScope.showDetailPage = false;
	    $rootScope.showComparisonPageBreadCrumbs = false;
	    $rootScope.showDetailPageActive = false;
		$rootScope.showComparisonPageBreadCrumbsActive=false;
		$scope.legendHideShow = true;
	var currentData = [];
	var verticalBarChartDataOriginal = [];
	var minMaxValues_exp, minMaxValues_count, actualColorscale;
	var cardHolderType = window.localStorage.getItem("cardHolderType");
	var params = {
		token: $rootScope.ehubObject.token
	};
	var format = d3.format(",d");
	var bubbledata = { //BubbleEntity Data
		name: '',
		children: []
	};
	var avgExpCustomerCountData = [];
	var colors = ['#328FD7', '#E4006E'];
	var plotBubbleChartResize = {};

	//	    condition to check url call for UC1(general cluster)
	if (cardHolderType === 'General Card Holder') {
		var urlCalls = [
			LeadGenerationApiService.getclustersInfo(params,
				'leadGeneration/uc1/1_2/cluster_stats'),
			LeadGenerationApiService.getOverAllStats(params,
				'leadGeneration/uc1/1_2/overall_stats')
		];
	} else if (cardHolderType === 'Dual Card Holders') {
		var clustersUrl = 'leadGeneration/uc2/2_2/cluster_stats';
		var OverAllStatsUrl = 'leadGeneration/uc2/2_2/overall_stats';
		var urlCalls = [
			LeadGenerationApiService.getclustersInfo(params, clustersUrl),
			LeadGenerationApiService.getOverAllStats(params, OverAllStatsUrl)
		];
	} else if (cardHolderType === 'Predicting Dual Card Holders') {
		var clustersUrl = 'leadGeneration/uc3/3_2/cluster_stats';
		var OverAllStatsUrl = 'leadGeneration/uc3/3_2/overall_stats';
		var urlCalls = [
			LeadGenerationApiService.getclustersInfo(params, clustersUrl),
			LeadGenerationApiService.getOverAllStats(params, OverAllStatsUrl)
		];
	} else {
		var clustersUrl = 'leadGeneration/uc3/3_2/cluster_stats';
		var OverAllStatsUrl = 'leadGeneration/uc3/3_2/overall_stats';
		var urlCalls = [
			LeadGenerationApiService.getclustersInfo(params, clustersUrl),
			LeadGenerationApiService.getOverAllStats(params, OverAllStatsUrl)
		];
	}

	 //code for number formatting
    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };


    $scope.formatNummerWithComma = function(amount) {
    	 var formatAmount;
         formatAmount = Number(Math.round(amount)).formatAmt();
         return formatAmount.split('.')[0];
    };

	//Updating values for resize bubbble chart
	function updateResizeBubbleChart(colors,currentData,colorScale){
		plotBubbleChartResize = {
			"colorsForResize" : colors,
			"currentDataForResize" : currentData,
			"colorScaleForResize"  :colorScale
		};
	};
    
	loadCluster(urlCalls);
	var colorScale;
	var filterParams = {};
	$scope.minMaxValues_exp= [];
	$scope.clusterPreloader = true;
	var getOverallExpenditureAvg;
	var israCardPopulation;

	function loadCluster(urlCalls) {
		plotBubbleChartResize = {};
		$q.all(urlCalls).then(function(values) {
			$scope.clusterPreloader = false;
			var getClusterSize = values[0].data.data;
			getOverallExpenditureAvg = values[1].data.data.average_expenditure;
			israCardPopulation = values[1].data.data.total_customers;
			getClusterSize.map(function(v) {
				bubbledata.children.push({
					"name": v.name,
					"clusterID": v.cluster_id,
					"size": v.customer_count,
					"avgExp": (v.avg_expenditure).toFixed(2),
					"avgExpPerCustomerAmongGP": (getOverallExpenditureAvg /
						israCardPopulation).toFixed(2),
					"customersOutOfIsracardPopulation": ((v.customer_count /
						israCardPopulation) * 100).toFixed(2),
					"totalAmount": (v.total_expenditure).toFixed(2),
					"progress1": v.avg_expenditure > (getOverallExpenditureAvg /
						israCardPopulation) ? 100 : (v.avg_expenditure / (
						getOverallExpenditureAvg / israCardPopulation)) * 100,
					"progress2": (getOverallExpenditureAvg / israCardPopulation) > v.avg_expenditure ?
						100 : ((getOverallExpenditureAvg / israCardPopulation) / v.avg_expenditure) *
						100,
					"cluser_attributes": v.cluser_attributes
				});
				avgExpCustomerCountData.push({
					"size": v.customer_count,
					"avgExp": v.avg_expenditure
				});
			});
			
			$scope.clusterInfoToShow = {
				bubblesNum: bubbledata.children.length,
				totalIsraCustomers: format(values[1].data.data.total_customers),
				totalExp: (values[1].data.data.total_expenditure).toFixed(2)
			};
			minMaxValues_count = d3.extent(avgExpCustomerCountData, function(d) {
				return d.size;
			});
			minMaxValues_exp = d3.extent(avgExpCustomerCountData, function(d) {
				return parseFloat(d.avgExp);
			});
			$scope.minMaxValues_exp = minMaxValues_exp;
			$scope.rangeMinAvgExp = minMaxValues_exp[0];
			$scope.rangeMaxAvgExp = minMaxValues_exp[1];

			$scope.rangeMinCluster = minMaxValues_count[0];
			$scope.rangeMaxCluster = minMaxValues_count[1];

			$("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " +Number(parseInt($scope.rangeMinAvgExp)).formatAmt().split('.')[0]);
		    $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " +Number(parseInt($scope.rangeMaxAvgExp)).formatAmt().split('.')[0]);
		    $("#cluster_range").find(".price-range-min").html(Number(parseInt($scope.rangeMinCluster)).formatAmt().split('.')[0]);
		    $("#cluster_range").find(".price-range-max").html(Number(parseInt($scope.rangeMaxCluster)).formatAmt().split('.')[0]);

			colorScale = d3.scaleLinear().domain(minMaxValues_exp).range(colors);
			actualColorscale = colorScale;
			loadLegends(minMaxValues_exp, colorScale);
			currentData = bubbledata;
			updateResizeBubbleChart(colors,currentData,colorScale);
			if ($scope.showView) {
				plotBubbleChart('#israClusterBubbleChart', colors, currentData,
					colorScale);
			} else {
				plotVerticalBarChart(currentData);
			}
		}, function() {
			$scope.clusterInfoToShow = {
				bubblesNum: 0,
				totalIsraCustomers: 0,
				totalExp: 0
			};
			$scope.clusterPreloader = false;
			$('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed To Get Data From server</span></div> ");
		});
	}
	/* 
	 * purpose: load legends  using api data
	 * created: 20th mar 2018
	 * params: data(array of min max avg exp),colorScale(scale)
	 * return: no
	 * author: swathi
	 */
	function loadLegends(data, colorScale) {
		var data0 = parseInt(data[0]);
		var data1 = parseInt(data[1]);
		$scope.bubbleChartLegend = [];
		var diffValue = ((parseInt(data1) - parseInt(data0)) / 5); //5 buckets 
		for (var i = 1; i <= 5; i++) {
			$scope.bubbleChartLegend.push({
				'name': Number(Math.round((parseInt(data0) + parseInt(diffValue * (i - 1))))).formatAmt() + ' NIS' + ' - ' +
				Number(Math.round((parseInt(data0) + parseInt(diffValue * i)))).formatAmt() + ' NIS',
				'color': colorScale((parseInt(data0) + parseInt(diffValue * i)))
			});
		}
	}

	var bubbleSocialOptions = {};
	bubbleSocialOptions = { //BubbleEntity option
		"height": 600,
		"data": bubbledata,
		"opacity": 0.5,
		"detailPage": cardHolderType
	};
	
	$scope.formatTotalAmount = function(amount) {
        if (amount != undefined && amount > 0) {
            var amountVal = amount.toString().split('.');
            var finalAmount;
            if (amountVal[0].length <= 6) {
                finalAmount = $scope.formatNummerWithComma(amount);
            }else if (amountVal[0].length <= 8 && amountVal[0].length > 6) {
                finalAmount = (amount / 1000000).toFixed(2) + 'M';
            } else if (amountVal[0].length >= 9) {
                finalAmount = (amount / 1000000000).toFixed(2) + 'B';
            }
            return finalAmount;
        }
    };
	
	 $scope.formatNummerWithComma = function(amount) {
	        var formatAmount;
	        formatAmount = Number(Math.round(amount)).formatAmt();
	        return formatAmount;
	    };
	
	/*
	 * @purpose: call bubble chart on cluster page
	 * @created: 6 march 2018
	 * @return: no
	 * @author: Amritesh
	 */
	function plotBubbleChart(id, color, data, colorScale) {
		if(data && data.children && data.children.length >0){
			getMinMaxForSDchart(data);
			$timeout(function() {
				$scope.clusterInfoToShow.bubblesNum = data.children.length;
			}, 0);
			$scope.clusterInfoToShow.totalIsraCustomers = format(d3.sum(data.children,
				function(g) {
					return g.size;
				}));
			$scope.clusterInfoToShow.totalExp = d3.sum(data.children, function(g) {
				return g.totalAmount;
			});
			$scope.clusterInfoToShow.totalExp = parseInt($scope.clusterInfoToShow.totalExp);
			var bubbleSocialOptions1 = jQuery.extend(true, {}, bubbleSocialOptions);
			bubbleSocialOptions1.container = id;
			bubbleSocialOptions1.bubbleColor = color;
			bubbleSocialOptions1.domainArray = [
				[primaryminStandardDEviationValue, primarymaxStandardDEviationValue],
				[secondaryminStandardDEviationValue, secondarymaxStandardDEviationValue]
			];
			bubbleSocialOptions1.bubbleColorScale = colorScale;
			bubbleSocialOptions1.bubbleSizeScale = true;
			bubbleSocialOptions1.data = data;
			bubbleSocialOptions1.specialToolTip = true;
			BubbleHierarchyChart(bubbleSocialOptions1);
			$scope.legendHideShow = true;
		}else{
			$scope.clusterInfoToShow = {
				bubblesNum: 0,
				totalIsraCustomers: 0,
				totalExp: 0
			};
			$scope.legendHideShow = false;
			$('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Data not found</span></div> ");
		}
	}

	/*
	 * @purpose:  switch to details page as per UC selected on landing page
	 * @created: 6 march 2018
	 * @return: no
	 * @author: Amritesh
	 */
	window.detailsPage = function(detailsPageToShow, id, totalCust) {
		if (detailsPageToShow == 'General Card Holder') {
			$state.go('generalCardHolderDetails', {
				clusterId: id
			}, {
				totalCustomers: totalCust
			});
		} else if (detailsPageToShow == 'Dual Card Holders') {
			$state.go('dualCardHolderDetails', {
				clusterId: id
			}, {
				totalCustomers: totalCust
			});
		} else if (detailsPageToShow == 'Predicting Dual Card Holders') {
			$state.go('predictingDualCardHolderDetails');
		}
	};
	$scope.detailsPage = function(detailsPageToShow, id, totalCust) {
		if (detailsPageToShow == 'General Card Holder') {
			$state.go('generalCardHolderDetails', {
				clusterId: id
			}, {
				totalCustomers: totalCust
			});
		} else if (detailsPageToShow == 'Dual Card Holders') {
			$state.go('dualCardHolderDetails', {
				clusterId: id
			}, {
				totalCustomers: totalCust
			});
		} else if (detailsPageToShow == 'Predicting Dual Card Holders') {
			$state.go('predictingDualCardHolderDetails');
		}
	};
	/*
	 * @purpose: Initialize slider
	 * @created: 7 march 2018
	 * @return: no
	 * @author: Amritesh
	 */
	// // slider call
	function slider1() {
		$('#slider').slider({
			range: true,
			min: 0,
			max: 100,
			values: [0, 100],
			slide: function(event,ui){   
				avgExpenditurefilterData(ui.values[0], ui.values[1], "onChange");   
        	},
			stop: function(event, ui) {
				avgExpenditurefilterData(ui.values[0], ui.values[1]);
				if (ui.values[0] == ui.values[1]) {
					$('.price-range-both i').css('display', 'none');
				} else {
					$('.price-range-both i').css('display', 'inline');
				}

				if (collision($('#slider .price-range-min'), $(
						'#slider .price-range-max')) == true) {
					$('#slider .price-range-min, #slider .price-range-max').css('opacity',
						'0');
					$('#slider .price-range-both').css('display', 'block');
				} else {
					$('#slider .price-range-min, #slider .price-range-max').css('opacity',
						'1');
					$('#slider .price-range-both').css('display', 'none');
				}
			}
		});

		$('.ui-slider-range').append('<span class="price-range-both value1"><i>' + $(
			'#slider').slider('values', 0) + ' - </i>' + $('#slider').slider('values',
			1) + '</span>');
		$('.ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' +
			$('#slider').slider('values', 0) + '</span>');
		$('.ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' +
			$('#slider').slider('values', 1) + '</span>');
	}

	function slider2() {
		$('#slider2').slider({
			range: true,
			min: 0,
			max: 100,
			values: [0, 100],
			slide: function(event,ui){   
				clusterRangefilterData(ui.values[0], ui.values[1], "onChange");   
        	},
			stop: function(event, ui) {
				clusterRangefilterData(ui.values[0], ui.values[1]);
				if (ui.values[0] == ui.values[1]) {
					$('#slider2 .price-range-both i').css('display', 'none');
				} else {
					$('#slider2 .price-range-both i').css('display', 'inline');
				}

				if (collision($('#slider2 .price-range-min'), $(
						'#slider2 .price-range-max')) == true) {
					$('#slider2 .price-range-min, #slider2 .price-range-max').css('opacity',
						'0');
					$('#slider2 .price-range-both').css('display', 'block');
				} else {
					$('#slider2 .price-range-min,#slider2 .price-range-max').css('opacity',
						'1');
					$('#slider2 .price-range-both').css('display', 'none');
				}
			}
		});

		$('#slider2 .ui-slider-range').append(
			'<span class="price-range-both value1"><i>' + $('#slider2').slider('values',
				0) + ' - </i>' + $('#slider2').slider('values', 1) + '</span>');
		$('#slider2 .ui-slider-handle:eq(0)').append(
			'<span class="price-range-min value1">' + $('#slider2').slider('values', 0) +
			'</span>');
		$('#slider2 .ui-slider-handle:eq(1)').append(
			'<span class="price-range-max value1">' + $('#slider2').slider('values', 1) +
			'</span>');
	}

	slider1();
	slider2();

	/*
	 * @purpose: filterData according to avg expenditure range
	 * @created: 9 march 2018
	 * @return: no
	 * @author: Amritesh
	 */
	function avgExpenditurefilterData(min, max, type) {
		var expScale = d3.scaleLinear().domain([0, 100]).range(minMaxValues_exp);
		$scope.rangeMinAvgExp = expScale(min);
		$scope.rangeMaxAvgExp = expScale(max);
		$("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMinAvgExp)).formatAmt().split('.')[0]);
	    $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMaxAvgExp)).formatAmt().split('.')[0]);
	    
	    if(type != "onChange"){
	    	plotBubbleChartFilter();
	    }
	}

	/*
	 * @purpose: filterData according to cluster range
	 * @created: 9 march 2018
	 * @return: no
	 * @author: Amritesh
	 */

	function clusterRangefilterData(min, max, type) {
		var expScale = d3.scaleLinear().domain([0, 100]).range(minMaxValues_count);
		$scope.rangeMinCluster = expScale(min);
		$scope.rangeMaxCluster = expScale(max);
		$("#cluster_range").find(".price-range-min").html(Number(parseInt($scope.rangeMinCluster)).formatAmt().split('.')[0]);
		$("#cluster_range").find(".price-range-max").html(Number(parseInt($scope.rangeMaxCluster)).formatAmt().split('.')[0]);

		if(type != "onChange"){
			plotBubbleChartFilter();
		}
	}

	function plotBubbleChartFilter() {
		filterParams = {
			"customer_count_start": $scope.rangeMinCluster,
			"customer_count_end": $scope.rangeMaxCluster,
			"avg_expendi_start": $scope.rangeMinAvgExp,
			"avg_expendi_end": $scope.rangeMaxAvgExp
		};
		if (cardHolderType === 'General Card Holder') {
			var filterUrl = 'leadGeneration/uc1/1_2/cluster_stats';
			LeadGenerationApiService.getClusterFilteredData(params, filterParams,
				filterUrl).then(function(response) {
				setFilteraData(response);
				$scope.clusterPreloader = false;

			}, function() {
				$scope.clusterPreloader = false;
			});
		} else if (cardHolderType === 'Dual Card Holders') {
			$scope.clusterPreloader = true;
			var filterUrl = 'leadGeneration/uc2/2_2/cluster_stats';
			LeadGenerationApiService.getClusterFilteredData(params, filterParams, filterUrl).then(function(response) {
				setFilteraData(response);
				$scope.clusterPreloader = false;
			}, function() {
				$scope.clusterPreloader = false;
			});
		} else if (cardHolderType === 'Predicting Dual Card Holders') {
			var filterUrl = 'leadGeneration/uc3/3_2/cluster_stats';
			LeadGenerationApiService.getClusterFilteredData(params, filterParams,
				filterUrl).then(function(response) {
				setFilteraData(response);
				$scope.clusterPreloader = false;
			}, function() {
				$scope.clusterPreloader = false;
			});
		}

	}

	/*
	 * @purpose:  to showTooltip on slider
	 * @created: 6 Apr 2018
	 * @return: no
	 * @author: Amritesh
	 */
	$("#slider").on("mouseover", function() {
		$("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMinAvgExp)).formatAmt().split('.')[0]);
	    $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMaxAvgExp)).formatAmt().split('.')[0]);
	}).on("mousemove", function() {
		$("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMinAvgExp)).formatAmt().split('.')[0]);
	    $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.rangeMaxAvgExp)).formatAmt().split('.')[0]);
	}).on("mouseout", function() {

	});
	$("#slider2").on("mouseover", function() {
		 $("#cluster_range").find(".price-range-min").html(Number(parseInt($scope.rangeMinCluster)).formatAmt().split('.')[0]);
		 $("#cluster_range").find(".price-range-max").html(Number(parseInt($scope.rangeMaxCluster)).formatAmt().split('.')[0]);
	}).on("mousemove", function() {
		 $("#cluster_range").find(".price-range-min").html(Number(parseInt($scope.rangeMinCluster)).formatAmt().split('.')[0]);
		 $("#cluster_range").find(".price-range-max").html(Number(parseInt($scope.rangeMaxCluster)).formatAmt().split('.')[0]);
	}).on("mouseout", function() {

	});
	/*
	 * @purpose: cluster Filters calls 
	 * @created: 6 Apr 2018
	 * @return: no
	 * @author: Amritesh
	 */

	function setFilteraData(response) {
		plotBubbleChartResize = {};
		$scope.clusterPreloader = true;
		var filterdata = { //BubbleEntity Data
			name: '',
			children: []
		};
		var getFilteredClusterSize = response.data.data;
		getFilteredClusterSize.map(function(v) {
			filterdata.children.push({
				"name": v.name,
				"clusterID": v.cluster_id,
				"size": v.customer_count,
				"avgExp": (v.avg_expenditure).toFixed(2),
				"avgExpPerCustomerAmongGP": (getOverallExpenditureAvg /
					israCardPopulation).toFixed(2),
				"customersOutOfIsracardPopulation": ((v.customer_count /
					israCardPopulation) * 100).toFixed(2),
				"totalAmount": (v.total_expenditure).toFixed(2),
				"progress1": v.avg_expenditure > (getOverallExpenditureAvg /
					israCardPopulation) ? 100 : (v.avg_expenditure / (
					getOverallExpenditureAvg / israCardPopulation)) * 100,
				"progress2": (getOverallExpenditureAvg / israCardPopulation) > v.avg_expenditure ?
					100 : ((getOverallExpenditureAvg / israCardPopulation) / v.avg_expenditure) *
					100,
				"cluser_attributes": v.cluser_attributes
			});
		});

		currentData = filterdata;
		if (filterdata.children.length > 0) {
			var colorScale = d3.scaleLinear().domain([$scope.rangeMinAvgExp, $scope.rangeMaxAvgExp])
				.range([actualColorscale($scope.rangeMinAvgExp), actualColorscale($scope.rangeMaxAvgExp)]);
			currentData = filterdata;

			updateResizeBubbleChart(colors,currentData,colorScale);

			if ($scope.showView) {
				plotBubbleChart('#israClusterBubbleChart', colors, currentData, colorScale);
			} else {
				$('#barchartProgress').html(' ');
				$scope.verticalBarChartData = filterdata.children;
				verticalBarChartDataOriginal = jQuery.extend(true, [], currentData.children);
				setTimeout(function() {
					plotVerticalBarChart(currentData);
				}, 1000);
			}

		}else{
			$scope.clusterInfoToShow = {
				bubblesNum: 0,
				totalIsraCustomers: 0,
				totalExp: 0
			};
			if ($scope.showView) {
				$scope.legendHideShow = false;
				$('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'style='position: relative'> <span class='no-data-wrapper'style='position: absolute'>Data not found</span></div> ");
			}else{
				$scope.verticalBarChartData = [];
				$scope.legendHideShow = false;
				//$('#barchartProgress').html("<div class='leadGenErrorDiv text-center mnh-300' style='position: relative'> <span class='no-data-wrapper' style='position: absolute;padding-left: 200px'>Data not found</span></div> ");
			}
		}
	}

	/*
	 * @purpose: reset filters
	 * @created: 16 march 2018
	 * @return: no
	 * @author: swathi
	 */
	$scope.resetFilters = function(value) {
		if (value == 'avgExpenditure') {
			slider1();
			$scope.rangeMinAvgExp = minMaxValues_exp[0];
			$scope.rangeMaxAvgExp = minMaxValues_exp[1];
			plotBubbleChartFilter();
		} else {
			slider2();
			$scope.rangeMinCluster = minMaxValues_count[0];
			$scope.rangeMaxCluster = minMaxValues_count[1];
			plotBubbleChartFilter();
		}
	};

	//----------------------New Implementation in uc2-----------------------------------

	var uc2Baroptions = {
		axisX: false,
		axisY: false,
		barText: true,
		color: ["#5d96c8", "#b753cd", "#69ca6b", "#af3251", "#c1bd4f", "#db3f8d",
			"#669900", "#334433", "#fdcb6e", "#b2bec3", "#fab1a0", "#e17055"
		],
		gridx: false,
		gridy: false,
		height: 200,
		marginBottom: 20,
		marginLeft: 30,
		marginRight:1,
		marginTop: 30,
		labelSize: "8px",
		showxYaxis: false,
		gridColor: "#657F8B",
		label: false,
		movinglineStroke: "#657F8B",
		movingtextColor: "#FFFFFF",
		container: "#uc2Bar"
	};

	/*
	 * @purpose:  get min and max value for standard deviation
	 * @created: 24th July 2018
	 * @return: no
	 * @author: varsha
	 */
	var attrListWithColor = [];
	var primaryminStandardDEviationValue, primarymaxStandardDEviationValue,
		secondaryminStandardDEviationValue, secondarymaxStandardDEviationValue;

	function getMinMaxForSDchart(data) {
		var PrimaryallValueList = [];
		var SecondaryallValueList = [];
		var allAttrList = [];
		attrListWithColor = [];
		var colorList2 = ['#ce3939', '#ce6839', '#ce9c39', '#bace39', '#45ce39',
			'#39ce79', '#39cece', '#3997ce', '#3965ce', '#6539ce', '#ce39bf', '#ce3979',
			'#bc7676', '#63552c', '#39632c', '#632c49', '#66525c', '#e0c7d4', '#9e9398',
			'#c1608c', '#436a84'
		];

		angular.forEach(data.children, function(d) {
			for (var i = 0; i < d.cluser_attributes.primary_attributes.length; i++) {
				if (allAttrList.indexOf(d.cluser_attributes.primary_attributes[i].attribute_name)<0) {
					allAttrList.push(d.cluser_attributes.primary_attributes[i].attribute_name);
					attrListWithColor.push({
						name: d.cluser_attributes.primary_attributes[i].attribute_name,
						color: colorList2[i]
					});
				}
			}
			PrimaryallValueList.push(d3.min(d.cluser_attributes.primary_attributes, function(d) {
				return d.attribute_sd_value;
			}));
			PrimaryallValueList.push(d3.max(d.cluser_attributes.primary_attributes, function(d) {
				return d.attribute_sd_value;
			}));
			PrimaryallValueList.push(d3.min(d.cluser_attributes.secondary_attributes, function(d) {
				return d.attribute_sd_value;
			}));
			PrimaryallValueList.push(d3.max(d.cluser_attributes.secondary_attributes, function(d) {
				return d.attribute_sd_value;
			}));

		});
		PrimaryallValueList.sort(function(x, y) {
			return d3.ascending(x, y);
		});
		SecondaryallValueList.sort(function(x, y) {
			return d3.ascending(x, y);
		});
		primaryminStandardDEviationValue = PrimaryallValueList[0];
		primarymaxStandardDEviationValue = PrimaryallValueList[PrimaryallValueList.length -
			1];
		secondaryminStandardDEviationValue = PrimaryallValueList[0];
		secondarymaxStandardDEviationValue = PrimaryallValueList[PrimaryallValueList.length -
			1];
		sessionStorage.setItem("primaryminStandardDEviationValue",
			primaryminStandardDEviationValue);
		sessionStorage.setItem("primarymaxStandardDEviationValue",
			primarymaxStandardDEviationValue);
		sessionStorage.setItem("secondaryminStandardDEviationValue",
			primaryminStandardDEviationValue);
		sessionStorage.setItem("secondarymaxStandardDEviationValue",
			primarymaxStandardDEviationValue);
	}

	/*
	 * @purpose: Plot vertical chart in science view.
	 * @created: 24th July 2018
	 * @return: no
	 * @author: varsha
	 */

	var allChartOptions = [];
	function plotVerticalBarChart(data) {
		if(data && data.children && data.children.length > 0){
			$timeout(function() {
				$scope.clusterInfoToShow.bubblesNum = data.children.length;
			}, 0);
			$scope.clusterInfoToShow.totalIsraCustomers = format(d3.sum(data.children,
				function(g) {
					return g.size;
				}));
			$scope.clusterInfoToShow.totalExp = d3.sum(data.children, function(g) {
				return g.totalAmount;
			});
			$scope.clusterInfoToShow.totalExp = parseInt($scope.clusterInfoToShow.totalExp);
			$('#barchartProgress').html(' ');
			allChartOptions = [];
			getMinMaxForSDchart(data);
			angular.forEach(data.children, function(d) {
				var data1 = [];
				var data2 = [];
				angular.forEach(d.cluser_attributes.primary_attributes, function(d1) {
					data1.push({
						'name': d1.attribute_name,
						'value': d1.attribute_sd_value
					});
				});
				angular.forEach(d.cluser_attributes.secondary_attributes, function(
					d1) {
					data2.push({
						'name': d1.attribute_name,
						'value': d1.attribute_sd_value
					});
				});
				uc2Baroptions.data = [data1, data2];
				uc2Baroptions.container = '#barChart' + d.clusterID;
				uc2Baroptions.container2 = 'mainsvg' + d.clusterID;
				uc2Baroptions.fixedColorWithKey = attrListWithColor;
				$scope.$apply(function() {
					$scope.scienceViewLegendData = attrListWithColor;
				});

				uc2Baroptions.domainArray = [
					[primaryminStandardDEviationValue, primarymaxStandardDEviationValue],
					[secondaryminStandardDEviationValue, secondarymaxStandardDEviationValue]
				];
				var clusterOption = jQuery.extend(true,{}, uc2Baroptions);
	            allChartOptions.push(clusterOption);
	            uc2Baroptions.width = $('#barChart' + d.clusterID).width();
				NegativeBarWithTooltipChart(uc2Baroptions);
				$scope.legendHideShow = true;
			});
		}else{
			$scope.verticalBarChartData = [];
			$scope.clusterInfoToShow = {
				bubblesNum: 0,
				totalIsraCustomers: 0,
				totalExp: 0
			};
			$scope.legendHideShow = false;
			//$('#barchartProgress').html("<div class='leadGenErrorDiv text-center mnh-300'style='position: relative;padding-left: 200px'> <span class='no-data-wrapper'style='position: absolute'>Data not found</span></div> ");
		}
		
	}

	 $(window).resize(function () {
	    	if(location.hash.indexOf('dualCardClusterPage') >= 0){
	    		plotVerticalBarChart(currentData);
	    	}
	    });
	
	 /*
     * @purpose: Functionality of enable and disable the chart.
     * @created: 24th August 2018
     * @return: no
     * @author: varsha
     */
	
	 var disableClusterCount = 0;
	     $scope.updateClusterData = function(status,value){
	    	 var index  = allChartOptions.map(function(d){
	    		return d.container;
	    	}).indexOf('#barChart'+value.clusterID);
	    	 
	    	disableClusterCount = 0;
	    	
	    	if(status == 'enable'){
	    		allChartOptions[index].allowDisable = false;
	    	}else if(status =='disable'){
	    		allChartOptions[index].allowDisable = true;
	    	}
	    	 allChartOptions.map(function(d){
	 			if(d.allowDisable){
	 				disableClusterCount = disableClusterCount+1;
	 			}
	 		});
	 		
	    	if(disableClusterCount == allChartOptions.length){
	    		 return false;
	    	}else{
		    	if(status == 'enable'){
		    		$scope.verticalBarChartData[index].activeCluster = false;
		    		allChartOptions[index].DisableCluster = false;
		    		reCalculateDomainArray(allChartOptions);
		    		return false;
		    	}else if(status =='disable'){
		    		$scope.verticalBarChartData[index].activeCluster = true;
		    		allChartOptions[index].DisableCluster = true;
		    		reCalculateDomainArray(allChartOptions);
		    		return true;
		    	}
	    	}
	    };
	   
	     
	     
	     /*
	      * @purpose: Recalculate the domain array 
	      * @created: 24th August 2018
	      * @return: no
	      * @author: varsha
	      */
	    
	    function reCalculateDomainArray(allChartOptions){
	    var minMaxList = [];
	    	allChartOptions.map(function(d){
	    		if(!d.DisableCluster){
	    			minMaxList.push(d3.min(d.data[0], function(d) {
	    				return d.value;
	    			}));
	    			minMaxList.push(d3.max(d.data[0], function(d) {
	    				return d.value;
	    			}));
	    			minMaxList.push(d3.min(d.data[1], function(d) {
	    				return d.value;
	    			}));
	    			minMaxList.push(d3.max(d.data[1], function(d) {
	    				return d.value;
	    			}));
	    		}
	    	});
	    	minMaxList.sort(function(x, y) {
	            return d3.ascending(x, y);
	        });
		    allChartOptions.map(function(d){
				if(!d.DisableCluster){
					d.domainArray[0]=[minMaxList[0],minMaxList[minMaxList.length-1]];
					d.domainArray[1]=[minMaxList[0],minMaxList[minMaxList.length-1]];
					NegativeBarWithTooltipChart(d);
				}
			});
	    }
	
	
	
	/*
	 * @purpose: Set legend data
	 * @created: 24th July 2018
	 * @return: no
	 * @author: varsha
	 */

	window.VerticalNegativeBarChartlegendData = function(data, container) {
		if (container == "#uc2Bar") {
			var html = "";
			angular.forEach(data, function(val) {
				html = html +
					'<div class="d-ib mar-x5" ><span style="width:10px;height:10px;font-size:8px;border-radius:50%;margin-right:5px;background-color:' +
					val.color + '"></span><span style="font-size:10px;">' + val.name +
					'</span></div>';
			});
			$(container).siblings("#legendsDiv").html(html);
		}

	};

	/* @purpose: Toggle for science view and business view
	 * @created: 16th august 2018
	 * @author: varsha
	 */

	$scope.showView = true;
	$scope.checkedValue = 'business';
	$scope.changeView = function(checkedValue) {
		if (checkedValue == 'business') {
			$scope.showView = true;

			updateResizeBubbleChart(colors,currentData,colorScale);

			setTimeout(function() {
				plotBubbleChart('#israClusterBubbleChart', colors, currentData,
					colorScale);
			}, 0);
		} else {
			$scope.showView = false;
			$scope.verticalBarChartData = currentData.children;
			$scope.verticalBarChartData.map(function(d){
				d.activeCluster = false;
			});
			verticalBarChartDataOriginal = jQuery.extend(true, [], currentData.children);
			setTimeout(function() {
				plotVerticalBarChart(currentData);
			}, 1000);
		}
	};

	function postsetClusterName(stroryId, clusterId, data) {
		LeadGenerationApiService.postsetClusterName(stroryId, clusterId, data).then(
			function() {
//				console.log(response)
			},
			function() {});
	}

	/* @purpose: Initialize data when click on edit 
	 * @created: 16th august 2018
	 * @author: varsha
	 */

	var renameActiveIndex;
	var renameStoryId;
	var renameCluster;
	$scope.initializeRenameData = function(activeIndex, Cluster, StoryId) {
		renameStoryId = StoryId;
		renameCluster = Cluster;
		renameActiveIndex = activeIndex;
	};

	/* @purpose: on esc and enter key press while renaming
	 * @created: 16th august 2018
	 * @author: varsha
	 */
	$scope.renameClusterNameKeyUp = function(event) {
		if (event != undefined) {
			if (event.which == 27) {
				$scope.renameClusterName(renameActiveIndex, 'discard');
				return false;
			} else if (event.which == 13) {
				$scope.renameClusterName(renameActiveIndex, renameCluster, renameStoryId);
				return false;
			} else {
				return true;
			}
		}
	};

	/* @purpose: save edited name for cluster
	 * @created: 16th august 2018
	 * @author: varsha
	 */
	$scope.renameClusterName = function(activeIndex, clusterDetail, story_id) {
		if (clusterDetail == 'discard') {
			$scope.verticalBarChartData[activeIndex].name =
				verticalBarChartDataOriginal[activeIndex].name;
			return false;
		}
		if ($scope.verticalBarChartData[activeIndex].name ==
			verticalBarChartDataOriginal[activeIndex].name) {
			return false;
		} else if ($scope.verticalBarChartData[activeIndex].name.length > 0 &&
			$scope.verticalBarChartData[activeIndex].name !=
			verticalBarChartDataOriginal[activeIndex].name) {
			verticalBarChartDataOriginal[activeIndex].name = $scope.verticalBarChartData[
				activeIndex].name;
			postsetClusterName(story_id, $scope.verticalBarChartData[activeIndex].clusterID, {
				'name': $scope.verticalBarChartData[activeIndex].name
			});
		} else {
			$scope.verticalBarChartData[activeIndex].name=
				verticalBarChartDataOriginal[activeIndex].name;
		}
	};

	$(window).resize(function(){
		plotBubbleChart('#israClusterBubbleChart', plotBubbleChartResize.colorsForResize, plotBubbleChartResize.currentDataForResize,plotBubbleChartResize.colorScaleForResize);
	});
//
//	function removeDuplicates(arr, prop) {
//		var obj = {};
//		for (var i = 0, len = arr.length; i < len; i++) {
//			if (!obj[arr[i][prop]]){ obj[arr[i][prop]] = arr[i];}
//		}
//		var newArr = [];
//		for (var key in obj) {
//			newArr.push(obj[key]);
//		}
//		return newArr;
//	}

}