'use strict';
angular.module('leadGenerationApp')
	.controller('LeadGenerationDualCardHolderDetailsController',
		leadGenerationDualCardHolderDetailsController);
leadGenerationDualCardHolderDetailsController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'EntityGraphService',
	'EHUB_API',
	'$http',
	'LeadGenerationGraphService',
	'LeadGenerationApiService',
	'$stateParams',
	'$q',
	'israDiamondnameConst',
	'$timeout'
];

function leadGenerationDualCardHolderDetailsController(
	$scope,
	$state,
	$rootScope,
	EntityGraphService,
	EHUB_API,
	$http,
	LeadGenerationGraphService,
	LeadGenerationApiService,
	$stateParams,
	$q,
	israDiamondnameConst,
	$timeout
) {
	$scope.returnToClusterPage = function () {
		$state.go("cluster");
	};

	$scope.chordTableFirstColTooltipScope = "";
	$scope.chord_chartFlag = false;
	$scope.chordchart = function () {
		$scope.chord_chartFlag = !$scope.chord_chartFlag;
	};
	var params = {
		token: $rootScope.ehubObject.token
	};
	$scope.diamond_name = israDiamondnameConst[1];
	$scope.clusterId = $stateParams.clusterId;
	sessionStorage.setItem("clusterId", $stateParams.clusterId);

	var clubdetails = [];
	var graph1_data = {
		id: '#vlaCompare1',
		graphtype: 'MerchantType'
	};
	var graph2_data = {
		id: '#vlaCompare2',
		graphtype: 'MerchantType'
	};
	var single_clubdetails;
	var arc_tooltip_details;
	var average_expenditure_of_all_customers_in_cluster;
	var chorddetails = {
		segementarray: [],
		arc_connectionmatrix: []
	};
	$scope.dualcardObject = {
		segmenttooltipspinner: false,
		arctooltipspinner: false,
		chord_chart_spinner: false,
		firstvlachartspinnner: false,
		secondvlachartspinnner: false,
		vlaClubnames: {}
	};
	var clusterFilteredData = {};
	var initialCustomerCount, initialTotExp, initialAvgExp;

	var colorList2 = ['#FF3143', '#F2626B', '#FFAFCD', '#EEC2EB', '#FF71FD', '#BA8BDB', '#9236C6', '#5370FD', '#5CAAF9', '#00B9C9', '#37B761', '#FFA238', '#FEE14D', '#FCFFA8', '#C9B7B1', '#A17F74', '#5d4037', '#775045', '#a58074', '#757575', '#546e7a', '#b0bec5', '#a8a771', '#968800', '#6b6b3c', '#9b5f0e', '#86490c', '#ff7400', '#f7a248', '#fdbd39', '#fdd758', '#fff6a3', '#eeff41', '#c9d200', '#84a100', '#346500', '#00711f', '#5dab60', '#75b788', '#a5d6a7', '#80cbc4', '#4db6ac', '#00cab1', '#00897b', '#00695c', '#006064', '#008297', '#00baca', '#80deea', '#3eb6ff', '#488dfa', '#4654a6', '#1f3397', '#5e35b1', '#8661c6', '#b89be0', '#d8a8e0', '#ff93ff', '#9c27b0', '#9c2162', '#c64892', '#ffb8d1', '#e57373', '#ef5350', '#c40100'];
	$scope.showProgressBar = false;
	var uc2Baroptions = {
		axisX: false,
		axisY: false,
		barText: true,
		color: colorList2,
		gridx: false,
		gridy: false,
		height: 300,
		marginBottom: 20,
		marginLeft: 23.5,
		marginRight: 4.5,
		marginTop: 30,
		labelSize: "8px",
		showxYaxis: false,
		movinglineStroke: "#657F8B",
		movingtextColor: "#FFFFFF",
		gridColor: "#657F8B",
		label: false,
		container: "#uc2Bar"
	};

	//code for number formatting
	Number.prototype.formatAmt = function (decPlaces, thouSeparator, decSeparator) {
		var n = this,
			decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
			decSeparator = decSeparator == undefined ? "." : decSeparator,
			thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
			sign = n < 0 ? "-" : "",
			i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
			j = (j = i.length) > 3 ? j % 3 : 0;
		return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
	};

	jQuery.fn.d3Click = function () {
		this.each(function (i, e) {
			var evt = new MouseEvent("click");
			e.dispatchEvent(evt);
		});
	};


	$rootScope.showDetailPage = true;
	$rootScope.showComparisonPageBreadCrumbs = false;
	$rootScope.showDetailPageActive = true;
	$rootScope.showComparisonPageBreadCrumbsActive = false;
	$scope.formatTotalAmount = function (amount) {
		if (amount != undefined) {
			var amountVal = amount.toString().split('.');
			var finalAmount;
			if (amountVal[0].length <= 6) {
				finalAmount = formatNummerWithComma(amount);
			} else if (amountVal[0].length <= 8 && amountVal[0].length > 6) {
				finalAmount = (amount / 1000000).toFixed(2) + 'M';
			} else if (amountVal[0].length >= 9) {
				finalAmount = (amount / 1000000000).toFixed(2) + 'B';
			}
			return finalAmount;
		}
	};

	function formatNummerWithComma(amount) {
		var formatAmount;
		formatAmount = Number(Math.round(parseFloat(amount))).formatAmt();
		return formatAmount.split('.')[0];
	}



	//-------------------------Declaration Ends -------------------------------------------------
	//-----------------------Api Functions -----------------------------------------------------
	/* @purpose: Call api for posting cluster name
	 * @created: 16th august 2018
	 * @author: varsha
	 */

	function postsetClusterName(stroryId, clusterId, data) {
		LeadGenerationApiService.postsetClusterName(stroryId, clusterId, data).then(
			function () {},
			function () {});
	}

	//-----------------------Api Function Ends-----------------------------------------------------
	/* @purpose: Plot cluster chart
	 * @created: 16th august 2018
	 * @author: varsha
	 */
	var originalclusterDetail = [];
	function getClusterInformation(clusterFilteredData){
		originalclusterDetail = [];
		LeadGenerationApiService.getclustersInfoByClusterId($scope.clusterId, params,
			'leadGeneration/uc2/2_2/cluster_stats/', clusterFilteredData).then(function (response) {
			$scope.clusterDetail = response.data.data;
			var isFilterExist = false;
			if(Object.keys(clusterFilteredData).length > 0 && clusterFilteredData["distribution_filter"]){
				angular.forEach(clusterFilteredData["distribution_filter"], function(v, k){
					if(Object.keys(v).length > 0){
						isFilterExist = true;
					}
				});
			}
			if(!isFilterExist){
				$scope.clusterDetail.customer_percentage = 100;
				$scope.clusterDetail.total_expenditure_percentage = 100;
				$scope.clusterDetail.avg_expenditure_percentage = 100;
				initialCustomerCount =  $scope.clusterDetail.customer_count;
				initialTotExp = $scope.clusterDetail.total_expenditure;
				initialAvgExp = $scope.clusterDetail.avg_expenditure;
			}else{
				$scope.clusterDetail.customer_percentage = ($scope.clusterDetail.customer_count / initialCustomerCount)*100;
				$scope.clusterDetail.total_expenditure_percentage = ($scope.clusterDetail.total_expenditure / initialTotExp)*100;
				$scope.clusterDetail.avg_expenditure_percentage = ($scope.clusterDetail.avg_expenditure / initialAvgExp)*100;
			}
			average_expenditure_of_all_customers_in_cluster = response.data.data.avg_expenditure;
			originalclusterDetail = jQuery.extend(true, {}, response.data.data);
			uc2Baroptions.container = "#uc2Bar";
			if (response.data.data) {
				var data1 = [];
				response.data.data.cluser_attributes.primary_attributes.forEach(
					function (d) {
						data1.push({
							'name': d.attribute_name,
							'value': d.attribute_sd_value
						});
					});
				var data2 = [];
				response.data.data.cluser_attributes.secondary_attributes.forEach(
					function (d) {
						data2.push({
							'name': d.attribute_name,
							'value': d.attribute_sd_value
						});
					});
				uc2Baroptions.data = [data1, data2];
				var min1 = d3.min(response.data.data.cluser_attributes.primary_attributes, function (d) {
					return d.attribute_sd_value;
				});
				var max1 = d3.max(response.data.data.cluser_attributes.primary_attributes, function (d) {
					return d.attribute_sd_value;
				});
				var min2 = d3.min(response.data.data.cluser_attributes.secondary_attributes, function (d) {
					return d.attribute_sd_value;
				});
				var max2 = d3.max(response.data.data.cluser_attributes.secondary_attributes, function (d) {
					return d.attribute_sd_value;
				});
				var datasort = [min1, max1, min2, max2];
				datasort.sort(function (x, y) {
					return d3.ascending(x, y);
				});
				uc2Baroptions.domainArray = [
					[datasort[0], datasort[datasort.length - 1]],
					[datasort[0], datasort[datasort.length - 1]]
				];
				uc2Baroptions.container2 = 'mainsvg' + response.data.data.cluster_id;
				NegativeBarWithTooltipChart(uc2Baroptions);
			}
		}, function () {
			$('#uc2Bar').html(
				"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Failed To Get Data From server</span></div> "
			);
		});
	}

	$(window).resize(function () {
		if (!$rootScope.showDetailPageActive && location.hash.indexOf('dualCardHolderDetails') >= 0) {
			comparisonOptions.width = $('#compareChart').width() / 2.2;
			comparisonProgressBar(comparisonOptions);
		} else if ($rootScope.showDetailPageActive && location.hash.indexOf('dualCardHolderDetails') >= 0) {
			uc2Baroptions.width = $(uc2Baroptions.container).width();
			NegativeBarWithTooltipChart(uc2Baroptions);
			var validForChordDiagram = false;
			for (var i = 0; i < resizeArc_connectionmatrix.length; i++) {
				for (var j = 0; j < resizeArc_connectionmatrix[i].length; j++) {
					if (resizeArc_connectionmatrix[i][j] > 0) {
						validForChordDiagram = true;
					}

				}
			}
			if (validForChordDiagram) {
				plotchordchart(resizeSegmentarray, resizeArc_connectionmatrix,
					resizePrimaryPercentage, resizeExpenditureList);
			}
			if (clickedGroupRibbon != 'none') {
				$(clickedGroupRibbon).d3Click();
			}
		}

	});

	/* @purpose: Set legend data for  tooltip for bubble chart
	 * @created: 16th august 2018
	 * @author: varsha
	 */

	window.VerticalNegativeBarChartlegendData = function (data, container) {
		if (container == "#uc2Bar") {
			var html = "";
			angular.forEach(data, function (val) {
				html = html +
					'<div class="d-ib mar-x5" ><span style="width:10px;height:10px;font-size:8px;border-radius:50%;margin-right:5px;background-color:' +
					val.color + '"></span><span style="font-size:10px;">' + val.name +
					'</span></div>';
			});

			$(container).siblings("#legendsDiv").append(html);
		}
		$scope.clusterLegend = data;
	};



	/* @purpose: post cluster rename name on key actions
	 * @created: 16th august 2018
	 * @author: varsha
	 */

	$scope.renameClusterNameKeyUp = function (event) {
		if (event != undefined) {
			if (event.which == 27) {
				$scope.renameClusterName('discard');
				return false;
			} else if (event.which == 13) {
				$scope.renameClusterName('save');
				return false;
			} else {
				return true;
			}
		}
	};

	/* @purpose: post cluster rename cluster name
	 * @created: 16th august 2018
	 * @author: varsha
	 */
	$scope.renameClusterName = function (name) {
		if (name == 'discard') {
			$scope.clusterDetail.name = originalclusterDetail.name;
			return false;
		}
		var StoryId = 'UC2';
		if ($scope.clusterDetail.name == originalclusterDetail.name) {
			return false;
		} else if ($scope.clusterDetail.name.length > 0 && $scope.clusterDetail.name != originalclusterDetail.name) {
			originalclusterDetail.name = $scope.clusterDetail.name;
			postsetClusterName(StoryId, $scope.clusterDetail.cluster_id, {
				'name': $scope.clusterDetail.name
			});
		} else {
			$scope.clusterDetail.name = originalclusterDetail.cluster_name;
		}
	};


	window.filterChordChartWithFilter = function (filterUC2Object) {
		getClubDetailsbyclusterId(filterUC2Object);
		clubConnectionsDetails(filterUC2Object);
		getClusterInformation(filterUC2Object);
		urlCalls = [
			LeadGenerationApiService.getclustersInfoByClusterId($scope.clusterId, params,
				'leadGeneration/uc2/2_2/cluster_stats/', filterUC2Object),
			LeadGenerationApiService.getOverAllStats(params,
				'leadGeneration/uc2/2_2/overall_stats')
		];
		getUrlCallsData(urlCalls);
	};

	window.addEventListener("hashchange", function () {
		$('#chordDaigram_tooltip').remove();
	});

	/*
	 * purpose: get clusters Info By ClusterId 
	 * created: 5 april 2018 
	 * params(object) author: Ram singh
	 */
	var urlCalls = [
		LeadGenerationApiService.getclustersInfoByClusterId($scope.clusterId, params,
			'leadGeneration/uc2/2_2/cluster_stats/', clusterFilteredData),
		LeadGenerationApiService.getOverAllStats(params,
			'leadGeneration/uc2/2_2/overall_stats')
	];
	$scope.clusterInfoPreloader = true;
	function getUrlCallsData(urlCalls){
		$q.all(urlCalls).then(function (values) {
			var clusterData = values[0].data.data;
			$scope.clusterName = clusterData.name;
			$scope.totalCustomers = clusterData.customer_count;
			$scope.customerPercentage = parseFloat(clusterData.customer_primary_percentage).toFixed(2);
			$scope.avgExpend = (clusterData.avg_expenditure).toFixed(2);
			$scope.GpAvgExpend = (values[1].data.data.average_expenditure).toFixed(2);
			$scope.progress1 = $scope.avgExpend > $scope.GpAvgExpend ? 100 : ($scope.avgExpend /
				$scope.GpAvgExpend) * 100;
			$scope.progress2 = $scope.GpAvgExpend > $scope.avgExpend ? 100 : ($scope.GpAvgExpend /
				$scope.avgExpend) * 100;
		}, function () {
	
		});
	}

	/*
	 * @purpose: Getting the club Details 
	 * @created: 11 april 2018 
	 * @return: no
	 * @author:Ram
	 */
	var primaryPercentage = [];
	var expenditureList = [];
	var clubChordStatsData = [];
	var colorScale;
	var chorddetailsSegmentarrayFilter = [];
	var primaryPercentageFilter = [];
	var expenditureListFilter = [];
	var chorddetailsArc_connectionmatrixFilter;
	var sumOfCustomerNumber;

	function getClubDetailsbyclusterId(data) {
		LeadGenerationApiService.getClubDataByClusterId($scope.clusterId, params, data)
			.then(function (response) {
				clubdetails = response.data.data.club_stats;
				primaryPercentage = [];
				expenditureList = [];
				clubChordStatsData = [];
				response.data.data.club_stats.sort(function (x, y) {
					return d3.ascending(x.average_expenditure, y.average_expenditure);
				});
				clubChordStatsData = jQuery.extend(true, [], response.data.data.club_stats);
				filterDataByCustomerCount = clubChordStatsData;
				chorddetails.segmentarray = response.data.data.club_stats.map(function (
					item) {
					primaryPercentage.push(item.customer_primary_percentage);
					expenditureList.push({
						'avgExp': item.average_expenditure,
						'customerCount': item.customer_count,
						'chordName': item.club_name
					});
					return item.club_name;
				});
				sumOfCustomerNumber = d3.sum(expenditureList, function (d) {
					return d.customerCount;
				});
				chorddetailsSegmentarrayFilter = chorddetails.segmentarray;
				colorScale = d3.scaleLinear().domain([d3.min(expenditureList, function (
					d) {
					return d.avgExp;
				}), d3.max(expenditureList, function (d) {
					return d.avgExp;
				})]).range(["#cfa9e5", "#9341C3"]);
				var maxEpenditure = d3.max(expenditureList, function (d) {
					return d.avgExp;
				});
				expenditureList.map(function (d) {
					d.name = d.chordName;
					d.evt_color = colorScale(parseFloat(d.avgExp));
					d.offset = (d.avgExp / maxEpenditure) * 100 + '%';
				});
				primaryPercentageFilter = primaryPercentage;
				expenditureListFilter = expenditureList;
				$('#noData').hide();
			}, function () {
				$scope.dualcardObject.chord_chart_spinner = false;
				$('#noData').show();
				$('#customerUsageChart').empty();
				$('#noData').html(
					"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Failed To Get Data From server</span></div> "
				);
			});

	}
	getClubDetailsbyclusterId({});

	/*
	 * @purpose: Intilaizing the final Matrix to Zero 
	 * @created: 11 april 2018
	 * @return: no @author:Ram
	 */
	var resizeSegmentarray = [];
	var resizeArc_connectionmatrix = [];
	var resizePrimaryPercentage = [];
	var resizeExpenditureList = [];

	function intialize_matrix(segmentarray, connections) {
		resizeSegmentarray = [];
		resizeArc_connectionmatrix = [];
		resizePrimaryPercentage = [];
		resizeExpenditureList = [];
		chorddetails.arc_connectionmatrix = new Array(segmentarray.length);
		for (var i = 0; i < segmentarray.length; i++) {
			chorddetails.arc_connectionmatrix[i] = new Array();
			for (var j = 0; j < segmentarray.length; j++) {
				chorddetails.arc_connectionmatrix[i][j] = 0;
			}
		}
		for (var i = 0; i < connections.length; i++) {
			var a_i = chorddetails.segmentarray.indexOf(connections[i].club_one_name);
			var a_j = chorddetails.segmentarray.indexOf(connections[i].club_two_name);
			if(a_i!=-1 && a_j!=-1){
				chorddetails.arc_connectionmatrix[a_i][a_j] = connections[i].customer_count;
			}

		}
		chorddetailsArc_connectionmatrixFilter = chorddetails.arc_connectionmatrix;
		setMinMaxData();
		
		resizeSegmentarray = chorddetails.segmentarray;
		resizeArc_connectionmatrix = chorddetails.arc_connectionmatrix;
		resizePrimaryPercentage = primaryPercentage;
		resizeExpenditureList = expenditureList;
		var valid = validToPlot(chorddetailsArc_connectionmatrixFilter)
		if(valid){
			plotchordchart(chorddetails.segmentarray, chorddetails.arc_connectionmatrix,
				primaryPercentage, expenditureList);
		}
		setChordTableData(chorddetailsSegmentarrayFilter, clubdetails, chorddetailsArc_connectionmatrixFilter);
		$scope.dualcardObject.chord_chart_spinner = false;
		getMaxLinkFunction(chorddetails.arc_connectionmatrix);
	}

	function SumOfArray(arr) {
		var sum = 0;
		for (var i = 0; i < arr.length; i++) {
			if (arr[i] != 0) {
				sum = sum + 1;
			}
		}
		return sum;
	}

	function getMaxLinkFunction(data) {
		$('#sideBarForChordClick').css('display', 'none');
		$('#ribbonInfo').css('display', 'none');
		var getMaxLinks = [];
		for (var i = 0; i < data.length; i++) {
			getMaxLinks.push(SumOfArray(data[i]));
		}
		var max = d3.max(getMaxLinks, function (d) {
			return d;
		});
		$scope.maxFilterValue = max;
		$scope.maxSegmentValue = max;
		$scope.currentValueConnection = max;
		$('#no_connection').attr('max', max);
		if(slider){
			noOfConnection = max;
			$('#currentValue').text(parseInt($('#no_connection').val()));
		}else{
			$('#currentValue').text(parseInt($('#no_connection').val()));
			$('#no_connection').val(max);
			noOfConnection = max;
		}
		
		return max;
	}
	var noOfConnection;
	$scope.setFilterForChord = function (values) {
		$('#sideBarForChordClick').css('display', 'none');
		$('#ribbonInfo').css('display', 'none');
		noOfConnection = $('#no_connection').val();
		$('#currentValue').text(values);

		setDataForChordDiagram();
		setDataForChord(values);
	};
	
	var valueToShow;
	var slider = false;
	$("#no_connection" ).change(function(event) {
		slider = true;
		$('#sideBarForChordClick').css('display', 'none');
		$('#ribbonInfo').css('display', 'none');
		noOfConnection = parseInt(event.target.value);
		$('#currentValue').text(parseInt(event.target.value));
		valueToShow= parseInt(event.target.value);
		$('#no_connection').val(parseInt(event.target.value));
		setDataForChordDiagram();
		setDataForChord(parseInt(event.target.value));
	});

	var chordFilterData = [];
	var chordMultiFilterData = [];

	function setDataForChord(value) {
		if (chorddetailsSegmentarrayFilter && chorddetailsSegmentarrayFilter.length > 0) {
			chordMultiFilterData = jQuery.extend(true, [], chorddetailsArc_connectionmatrixFilter);
			for (var i = 0; i < chordMultiFilterData.length - 1; i++) {
				var count = 0;
				for (var j = 0; j < chordMultiFilterData[i].length; j++) {
					if (chordMultiFilterData[i][j] != 0 && count < value) {
						count++;
						chordMultiFilterData[i][j] = chordMultiFilterData[i][j];
					} else {
						chordMultiFilterData[i][j] = 0;
					}
				}
			}
			setChordTableData(chorddetailsSegmentarrayFilter, filterDataByCustomerCount, chordMultiFilterData);
			resizeSegmentarray = chorddetailsSegmentarrayFilter;
			resizeArc_connectionmatrix = chordMultiFilterData;
			resizePrimaryPercentage = primaryPercentageFilter;
			resizeExpenditureList = expenditureListFilter;
			var valid = validToPlot(chordMultiFilterData)
			if(valid){
			plotchordchart(chorddetailsSegmentarrayFilter, chordMultiFilterData,
				primaryPercentageFilter, expenditureListFilter);
			$('#noData').hide();
			}
		} else {
			chordFilterData = jQuery.extend(true, [], chorddetailsArc_connectionmatrixFilter);
			for (var i = 0; i < chordFilterData.length - 1; i++) {
				var count = 0;
				for (var j = 0; j < chordFilterData[i].length; j++) {
					if (chordFilterData[i][j] != 0 && count < value) {
						count++;
					} else {
						chordFilterData[i][j] = 0;
					}
				}
			}
			resizeSegmentarray = chorddetails.segmentarray;
			resizeArc_connectionmatrix = chordFilterData;
			resizePrimaryPercentage = primaryPercentage;
			resizeExpenditureList = expenditureList;
			var valid = validToPlot(chordFilterData)
			if(valid){
			plotchordchart(chorddetails.segmentarray, chordFilterData,
				primaryPercentage, expenditureList);
			$('#noData').hide();
			}
		}

	}

	/*
	 * purpose: get chord matrix data 
	 * created: 5 april 2018 
	 * params(object) author: Ram singh
	 */

	var connectionChordData = [];

	function clubConnectionsDetails(data) {
		$scope.dualcardObject.chord_chart_spinner = true;
		LeadGenerationApiService.getClubconnectionDetails($scope.clusterId, params,
			data).then(function (response) {
			connectionChordData = [];
			if (response && response.data && response.data.data && response.data.data.length > 0) {
				connectionChordData = jQuery.extend(true, [], response.data.data);
				intialize_matrix(chorddetails.segmentarray, response.data.data);
				$('#noData').hide();
			} else {
				resizeSegmentarray = [];
				resizeArc_connectionmatrix = [];
				resizePrimaryPercentage = [];
				resizeExpenditureList = [];
				$scope.dualcardObject.chord_chart_spinner = false;
				$('#noData').show();
				$('#customerUsageChart').empty();
				$('#noData').html(
					"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data not Found</span></div> "
				);
			}
		}, function () {
			resizeSegmentarray = [];
			resizeArc_connectionmatrix = [];
			resizePrimaryPercentage = [];
			resizeExpenditureList = [];
			$scope.dualcardObject.chord_chart_spinner = false;
			$('#noData').show();
			$('#customerUsageChart').empty();
			$('#noData').html(
				"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Failed To Get Data From server</span></div> "
			);
		});
	}

	clubConnectionsDetails({});

	/*
	 * purpose: Plot Chord diagram
	 * created: 5 april 2018 params:
	 * params(object) author: Ram singh
	 */
	$('#ribbonInfo').css('display', 'none');
	var ribbons;
	var old_this_onClick;
	var ribbinOldClick;
	var oldClickedSegmentColor;
	var ribbonoldClickedSegmentColor;
	var clickedGroupRibbon;
	var storedClickedRibbon = {};

	function plotchordchart(segment_array, final_matrix, primaryPercentage,
		expenditureList) {
		$('#customerUsageChart').empty();
		$('#chordDaigram_tooltip').remove();
		var margin = {
			left: 10,
			top: 90,
			right: 200,
			bottom: 90
		};


		// Initialize canvas and inner/outer radii of the chords
		var svg = d3.select("#customerUsageChart").append("svg")
			.attr("width", $('#chordDiagram').width() + margin.left + margin.right)
			.attr("height", $('#chordDiagram').height() - 20 + margin.top + margin.bottom);

		var width = $('#chordDiagram').width() - 50 - margin.left - margin.right, // more flexibility:
			height = $('#chordDiagram').height() - 20 - margin.top - margin.bottom, // same:
			innerRadius = Math.min(width, height) * 0.39,
			outerRadius = innerRadius * 1.1;

		//var defs = svg.append("defs");

		//Function to create the id for each chord gradient
		function getGradID(d) {
			return "linkGrad-" + d.source.index + "-" + d.target.index;
		}



		// Set number format to show $M for millions with thousands separator (i.e.
		// $1,000M).
		//		var formatValue = d3.formatPrefix("$,.0", 1e3);
		var chordChartdiv = d3.select(".details-collection-wrapper").append("div").attr("id",
			"chordDaigram_tooltip").attr("class", "card_segment_tooltip").style(
			"z-index", 10).style("background", "rgb(27, 39, 53)").style(
			"border-radius", "10px").style("font-size", "10px").style("display", "none");

		// Initialize chord diagram

		var chord = d3.chord()
			.padAngle(0.05)
			.sortSubgroups(d3.descending);

		// Set Arc Raddii

		var arc = d3.arc()
			.innerRadius(innerRadius)
			.outerRadius(outerRadius);

		// Set Ribbbons

		var ribbon = d3.ribbon()
			.radius(innerRadius);

		//Create the gradients definitions for each chord
		var grads = svg.append("defs").selectAll("linearGradient")
			.data(chord(final_matrix))
			.enter().append("linearGradient")
			.attr("id", getGradID)
			.attr("gradientUnits", "userSpaceOnUse")
			.attr("x1", function (d) {
				return innerRadius * Math.cos((d.source.endAngle - d.source.startAngle) / 2 + d.source.startAngle - Math.PI / 2);
			})
			.attr("y1", function (d) {
				return innerRadius * Math.sin((d.source.endAngle - d.source.startAngle) / 2 + d.source.startAngle - Math.PI / 2);
			})
			.attr("x2", function (d) {
				return innerRadius * Math.cos((d.target.endAngle - d.target.startAngle) / 2 + d.target.startAngle - Math.PI / 2);
			})
			.attr("y2", function (d) {
				return innerRadius * Math.sin((d.target.endAngle - d.target.startAngle) / 2 + d.target.startAngle - Math.PI / 2);
			});

		//Set the starting color (at 0%)
		grads.append("stop")
			.attr("offset", "0%")
			.attr("stop-color", function (d) {
				return expenditureList[d.source.index].evt_color;
			});

		//Set the ending color (at 100%)
		grads.append("stop")
			.attr("offset", "100%")
			.attr("stop-color", function (d) {
				return expenditureList[d.target.index].evt_color;
			});
		var translateValWidth;
		var translateValHeight;
		if ($('#chordDiagram').width() < 600 && $('#chordDiagram').width() > 550) {
			translateValWidth = 1;
			translateValHeight = 1.65;
		} else if ($('#chordDiagram').width() < 700 && $('#chordDiagram').width() >= 600) {
			translateValWidth = 1.1;
			translateValHeight = 1.6;
		} else if ($('#chordDiagram').width() < 800 && $('#chordDiagram').width() >= 700) {
			translateValWidth = 1.2;
			translateValHeight = 1.5;
		} else if ($('#chordDiagram').width() < 900 && $('#chordDiagram').width() >= 800) {
			translateValWidth = 1.3;
			translateValHeight = 1.4;
		} else if ($('#chordDiagram').width() < 1000 && $('#chordDiagram').width() >= 900) {
			translateValWidth = 1.4;
			translateValHeight = 1.4;
		} else {
			translateValWidth = 1.5;
			translateValHeight = 1.4;
		}

		var g = svg.append("g")
			.attr("transform", "translate(" + width / translateValWidth + "," + height / translateValHeight + ")")
			.datum(chord(final_matrix));


		// Defines each "group" in the chord diagram
		var group = g.append("g")
			.attr("class", "groups")
			.selectAll("g")
			.data(function (chords) {
				return chords.groups;
			})
			.enter().append("g");
		var extent = d3.extent(expenditureList, function (data) {
			return data.avgExp;
		});

		// Draw the radial arcs for each group
	var groupPath =	group.append("path")
			.attr("id", function (d) {
				return "group" + d.index;
			})
			.style("fill", function (d, i) {
				return expenditureList[i].evt_color;
			})
			.style("stroke", function (d, i) {
				return expenditureList[i].evt_color;
			})
			.attr('stroke-width', '2px')
			.attr("d", arc)
			.attr('opacity', function (d, i) {
				if (extent[0] == extent[1]) {
					return extent[0];
				} else {
					var expScale = d3.scale.linear().domain(extent).range([0.1, 1]);
					return expScale(expenditureList[i].avgExp);
				}
			})
			.on("mouseover", function (d) {
				$("#group" + d.index).css("cursor", "help");
			})
			.on("click", fade(0.1, "visible"))
			.on("mousemove", function (d, i) {
				var p = $("#customerUsageChart");
				var position = p.offset();
				var tooltipWidth = $(".card_segment_tooltip").width() + 50;
				var cursor = d3.event.x;
				if ((position.left < d3.event.pageX) && (cursor > tooltipWidth)) {
					var element = document.getElementsByClassName("card_segment_tooltip");
					for (var i = 0; i < element.length; i++) {
						element[i].classList.remove("tooltip-left");
						element[i].classList.add("tooltip-right");
					}
					$(".card_segment_tooltip").css("left", (d3.event.pageX - 40 - $(
						".card_segment_tooltip").width()) + "px");
				} else {
					var element = document.getElementsByClassName("card_segment_tooltip");
					for (i = 0; i < element.length; i++) {
						element[i].classList.remove("tooltip-right");
						element[i].classList.add("tooltip-left");
					}
					$(".card_segment_tooltip").css("left", (d3.event.pageX + 50) + "px");
				}
			});
		
	
	var groupText = group.append("text")
	.attr("x", 6)
	.attr("dy", 8);
	
//	 subtract 48 because the sides of the anchor are 24 pixels each (the difference in the radii). 
//	 divide by four because the path doubles back on itself. 
	
	groupText.append("textPath")
	.attr("xlink:href", function(d, i) { return "#group" + i; })
	.text(function(d, i) { 
    		return 'P :' + parseInt(primaryPercentage[i]).toFixed(0) + '%';
		})
	.attr("startOffset",function(d,i) { return (groupPath._groups[0][i].getTotalLength() - 48) / 4; })
	.style("fill", "white")
	.style("text-anchor","middle");
	
	// Remove the labels that don't fit. :(
	groupText.filter(function(d, i) { return groupPath._groups[0][i].getTotalLength() / 3-18 < this.getComputedTextLength(); })
	.remove();
		
		var count = 0;
		group.append("text").each(function (d) {
				d.angle = (d.startAngle + d.endAngle) / 2;
			})
			.attr('class', 'node-text-custom')
			.attr("dy", ".35em")
			.attr("transform", function (d) {
				count = count + 1;
				if (count > group._groups[0].length / 2) {
					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
						"translate(" + (innerRadius + 40) + ")" +
						(d.angle > Math.PI ? "rotate(180)" : "");

				} else {
					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
						"translate(" + (innerRadius + 40) + ")" +
						(d.angle > Math.PI ? "rotate(180)" : "");
				}

			})
			.attr("id", function (d) {
				return 'text-index-' + d.index;
			})
			.style('text-transform', 'capitalize')
			.style("fill", "#f5f5f5")
			.style("font-size", function (d) {
				if (d.value >= 2) {
					return 12 + 'px';
				} else {
					return 8 + 'px';
				}
			})
			.style("text-anchor", function (d) {
				return d.angle > Math.PI ? "end" : null;
			})
			.attr("href", function (d) {
				return "#group" + d.index;
			})
			.text(function (d, i) {
				return segment_array[i];
			});

//		var count2 = 0;
//		group.append("text").each(function (d) {
//				d.angle = (d.startAngle + d.endAngle) / 2;
//			})
//			.attr('class', 'node-text-custom')
//			.attr("dy", ".35em")
//			.attr("transform", function (d) {
//				count2 = count2 + 1;
//				if (count2 > group._groups[0].length / 2) {
//					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
//						"translate(" + (innerRadius + 40) + ")" +
//						(d.angle > Math.PI ? "rotate(180)" : "");
//
//				} else {
//					return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
//						"translate(" + (innerRadius + 40) + ",20)" +
//						(d.angle > Math.PI ? "rotate(180)" : "");
//				}
//			})
//			.attr("id", function (d) {
//				return 'text-index-' + d.index;
//			})
//			.style('text-transform', 'capitalize')
//			.style("fill", "#999C9F")
//			.style("font-size", 12 + 'px')
//			.style("text-anchor", function (d) {
//				return d.angle > Math.PI ? "end" : null;
//			})
//			.attr("href", function (d) {
//				return "#group" + d.index;
//			})
//			.text(function (d, i) {
//				
//				if (d.value >= 2) {
//					return '(P :' + parseInt(primaryPercentage[i]).toFixed(0) + '%)';
//				}
//			});

		// Draw the ribbons that go from group to group
		ribbons = g.append("g")
			.attr("class", "ribbons")
			.selectAll("path")
			.data(function (chords) {
				return chords;
			})
			.enter().append("path")
			.attr("d", ribbon)
			.attr("id", function (d, i) {
				return "ribbon" + i;
			})
			.attr('stroke-width', '2px')
			/*	.style("fill", function(d) {
					return expenditureList[d.target.index]['evt_color'];
				})*/
			.style("fill", function (d) {
				return "url(#" + getGradID(d) + ")";
			})
			.style("stroke", function (d) {
				return expenditureList[d.target.index].evt_color;
			})
			.style("fill-opacity", 0.6)
			.on("mouseover", function () {
				ribbons
					.transition()
					.style("cursor", "help");
			})
			.on("click", function (d) {
				$('#sideBarForChordClick').css('display', 'none');
				if (old_this_onClick) {
					$(old_this_onClick).css('stroke', oldClickedSegmentColor);
				}
				if (ribbinOldClick && ribbinOldClick != this) {
					$(ribbinOldClick).css('stroke', ribbonoldClickedSegmentColor);
				}
				if (!old_this_onClick) {
					old_this_onClick = this;
				}
				if ($('#' + $(this).attr('id')).css('stroke') == 'rgb(63, 140, 145)') {
					ribbons
						.transition()
						.style("opacity", 1).style("cursor", "help");
					$('#ribbonInfo').css('display', 'none');
					clickedGroupRibbon = 'none';
					$(this).css('stroke', expenditureList[d.target.index].evt_color);
				} else {
					ribbons
						.transition()
						.style("opacity", 0.1)
						.style("cursor", "help");
					tooltipdispalyArc(d.source.index, d.target.index, function () {});


					ribbons
						.filter(function (d1) {
							return d1.source.index == d.source.index && d1.target.index == d.target.index;
						})
						.transition()
						.style("opacity", 1);
					clickedGroupRibbon = '#' + $(this).attr('id');
					$(this).css('stroke', '#3f8c91');

				}
				ribbonoldClickedSegmentColor = expenditureList[d.target.index].evt_color;
				ribbinOldClick = this;
				$scope.$apply(function () {
					//					console.log(d,'sssssssssss')
					storedClickedRibbon = d;
				});
			});
		$('#customerUsageChart svg').click(function (event) {
			if ($(event.target).attr('id') == undefined) {
//				clickedGroupRibbon = 'none';
				if (old_this_onClick) {
					ribbons
						.transition()
						.style("opacity", 1);
					$('#chordDaigram_tooltip').remove();
					$('#ribbonInfo').css('display', 'none');
					$('#sideBarForChordClick').css('display', 'none');
					$(old_this_onClick).css('stroke', oldClickedSegmentColor);
					$(ribbinOldClick).css('stroke', ribbonoldClickedSegmentColor);
					chordChartdiv = d3.select(".details-collection-wrapper").append("div").attr("id",
						"chordDaigram_tooltip").attr("class", "card_segment_tooltip").style(
						"z-index", 10).style("background", "rgb(27, 39, 53)").style(
						"border-radius", "10px").style("font-size", "10px").style("display", "none");
				}
			}
		});
	}

	/*
	 * @purpose: plot bar chart for connection(total, avg expenditures)
	 * @created: 29th oct 2018 
	 * @params: data, id
	 * @author: Swathi
	 */
	$('#sideBarForChordClick').css('display', 'none');
	var colors = ["#4488FA", "#53D3E2"];

	function plotConnectionBars(data, id, type) {
		if (!data || data.length == 0) {
			return false;
		} else {
			if (type == "avg") {
				var plotBarsData = [{
						"letter": data.club_one_name,
						"frequency": data.club_one_avg_expenditure,
						"count": data.club_one_avg_expenditure
					},
					{
						"letter": data.club_two_name,
						"frequency": data.club_two_avg_expenditure,
						"count": data.club_two_avg_expenditure
					}
				];
			}
			if (type == "total") {
				var plotBarsData = [{
						"letter": data.club_one_name,
						"frequency": data.club_one_sum_expenditure,
						"count": data.club_one_sum_expenditure
					},
					{
						"letter": data.club_two_name,
						"frequency": data.club_two_sum_expenditure,
						"count": data.club_two_sum_expenditure
					}
				];
			}
			var barOptions1 = {
				container: id,
				height: 180,
				width: $(id).width() - 50,
				marginLeft: 60,
				marginTop: 0,
				marginBottom: 20,
				axisY: true,
				axisX: false,
				colors: colors,
				data: plotBarsData,
				avgExpCluster: ($scope.clusterDetail.avg_expenditure).toFixed(2),
				totExpCluster: ($scope.clusterDetail.total_expenditure).toFixed(2),
				avgExpClusterFormattted: $scope.formatTotalAmount($scope.clusterDetail.avg_expenditure),
				totExpClusterFormattted: $scope.formatTotalAmount($scope.clusterDetail.total_expenditure)
			};
			simpleVerticalBarChart(barOptions1);
			$(id).find('.grid').find('.domain').css('display', 'none');
			$(id).find('.grid').find('text').css('stroke', '#797979');
		}
	}
	/*
	 * @purpose: plot pie chart for connection(total customers)
	 * @created: 29th oct 2018 
	 * @params: data, id
	 * @author: Swathi
	 */
	//	 var colors = ['#1C6EC1', '#72BE74', '#ADE3AC', '#55D2E1', '#F36030', '#A072FC', '#CB3E4B', '#CB8B3D', '#2B2F26', '#E66C40', '#00C4AB', '#CBA03D', '#76CB3D', '#CB493D', '#70685F']
	function plotConnectionPie(data, id) {
		var data = [{
				key: data.club_one_name,
				value: 300,
				totCustomers: 500
			},
			{
				key: data.club_two_name,
				value: 100,
				totCustomers: 400
			}
		];
		if (data.length == 0) {
			$(id).html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
			return false;
		} else {
			data.map(function (d) {
				d.percentage = ((d.value / d.totCustomers) * 100).toFixed(2);
			});
			$scope.connectionPieData = data;
			var options = { //Piechart option
				container: id,
				colors: colors,
				data: data,
				height: 80,
				width: 60,
				txtSize: '10px',
				legendwidth: 60,
				islegends: true,
				txtColor: '4D6772',
				legendmargintop: 20,
				format: true
			};
			setTimeout(function () {
				new reusablePie(options); //load pie chart
			});
		}
	}
	/*
	 * @purpose: tooltip display finds the hover club details
	 *  @created: 11 april
	 * 2018 @return: no @author:Ram
	 */

	function tooltipdispaly(startindex, finalindex, segmentcallback) {
		single_clubdetails = _.find(clubdetails, ['club_name', resizeSegmentarray[
			finalindex]]);
		segmentcallback(single_clubdetails);
	}

	/*
	 * @purpose: tooltip display finds the hover ARC details
	 * @created: 11 april
	 * 2018 @return: no @author:Ram
	 */

	function tooltipdispalyArc(startindex, finalindex, arcdetails) {
		var club = {};
		club.club_one_name = chorddetails.segmentarray[startindex];
		club.club_two_name = chorddetails.segmentarray[finalindex];
		var data = {};
		LeadGenerationApiService.getconnectedclubdetails($scope.clusterId, params,
			data, club).then(function (response) {
			arc_tooltip_details = response.data.data;
			arcdetails(arc_tooltip_details);
			$('#ribbonInfo').css('display', 'block');
			$scope.connectionDetails = response.data.data;

			$scope.connectionDetails.connection_cust_percentage = (($scope.connectionDetails.common_customer_count / $scope.clusterDetail.customer_count) * 100).toFixed(2);
			$scope.connectionDetails.total_expenditure_percentage = (($scope.connectionDetails.total_expenditure / $scope.clusterDetail.total_expenditure) * 100).toFixed(2);
			$scope.connectionDetails.avg_expenditure_percentage = (($scope.connectionDetails.avg_expenditure / $scope.clusterDetail.avg_expenditure) * 100).toFixed(2);

			$scope.connectionDetails.common_customer_count = $scope.formatTotalAmount($scope.connectionDetails.common_customer_count);
			plotConnectionPie($scope.connectionDetails, "#connection_tot_customers_Pie");
			plotConnectionBars($scope.connectionDetails, "#connection_avg_bar", "avg");
			plotConnectionBars($scope.connectionDetails, "#connection_total_bar", "total");
		}, function () {});
	}

	var selectedChordName, clikedChordRibbons = [];

	function fade(opacity, visibility) {
		return function (d, i) {
			//var chordDetail = d;
			var chordIndex = i;
			setTableForChord(d, i);
			if (ribbinOldClick) {
				$(ribbinOldClick).css('stroke', ribbonoldClickedSegmentColor);
			}
			if (old_this_onClick && old_this_onClick != this) {
				ribbons
					.transition()
					.style("opacity", 1);
				$('#sideBarForChordClick').css('display', 'block');
				$(old_this_onClick).css('stroke', oldClickedSegmentColor);
			}
			if ($('#' + $(this).attr('id')).css('stroke') == 'rgb(63, 140, 145)') {
				clickedGroupRibbon = 'none';
				$("#group" + d.index).css("cursor", "help");
				$('#ribbonInfo').css('display', 'none');
				ribbons
					.transition()
					.style("opacity", 1);
				$('#sideBarForChordClick').css('display', 'none');
				$(this).css('stroke', expenditureList[i].evt_color);
			} else {
				clikedChordRibbons = [];
				clickedGroupRibbon = "#group" + d.index;
				$("#customerUsageChart svg").trigger("click");
				$("#group" + d.index).css("cursor", "help");
				selectedChordName = expenditureList[i].chordName;
				$('#ribbonInfo').css('display', 'none');
				ribbons
					.filter(function (d) {
						if(d.source.index == chordIndex || d.target.index == chordIndex){
							clikedChordRibbons.push({
								"startName": resizeSegmentarray[d.source.index],
								"targetName": resizeSegmentarray[d.target.index]
							});
						}
						//console.log("in....",ribbons, d, i, d.source.index, d.target.index, chordIndex);
						return d.source.index != chordIndex && d.target.index != chordIndex;
					})
					.transition()
					.style("opacity",0.1);
				$('#sideBarForChordClick').css('display', 'block');
				$(this).css('stroke', '#3f8c91');
			}
			oldClickedSegmentColor = expenditureList[i].evt_color;
			old_this_onClick = this;
			if (visibility == "visible") {
				$(".card_segment_tooltip").html(
					'<div class="p-rel"><span  class="custom-spinner "><i class="fa fa-spinner fa-spin fa-2x"></i></span><ul class="custom-list item-1 pad-10px card-tool-tip-right"><li><p>Total Customer -</p></li><li><p>% Out of customers</p></li><li><p>Average expenditure per customer in the segment</p></li><li class="border-r0"><p>Average Expenditure Among All Customers In The Cluster</p></li></ul></div>'
				);
				$("#chordDaigram_tooltip").removeClass("arc_segment_tooltip");
				$("#chordDaigram_tooltip").addClass("card_segment_tooltip");
				$("#chordDaigram_tooltip").addClass("tooltip-top");
				tooltipdispaly(d, i, function (segmentdetails) {
					var params = {
						"clusterId": $scope.clusterId,
						"clubName": segmentdetails.club_name
					};
					$scope.clubTablePreloader = true;
					LeadGenerationApiService.getClubConnections(params).then(function (response) {
						$scope.chordTableData = [];
						angular.forEach(response.data.data, function (d) {
//							if (d.common_customer_count <= maxCustomerCount && d.common_customer_count >= minCustomerCount && d.avg_expenditure <= maxAvgExpenditure && d.avg_expenditure >= minAvgExpenditure) {
//								clikedChordRibbons.map(function(v, k){
//									if((d.club_two_name == v.targetName && d.club_one_name == v.startName) || (d.club_two_name == v.startName && d.club_one_name == v.targetName)){
										$scope.chordTableData.push({
											"club_name": d.club_two_name,
											"club_one_name": d.club_one_name,
											"club_two_name": d.club_two_name,
											"customer_count": d.common_customer_count,
											"average_expenditure": d.avg_expenditure,
											"total_expenditure": d.total_expenditure,
											"avg_expenditure_percentage": parseFloat(((d.avg_expenditure / segmentdetails.average_expenditure) * 100).toFixed(2)),
											"total_expenditure_percentage": ((d.total_expenditure / segmentdetails.total_expenditure) * 100).toFixed(2),
											"customer_primary_percentage": parseInt((d.common_customer_count / segmentdetails.customer_count) * 100),
											"customer_per": parseInt((d.common_customer_count / segmentdetails.customer_count) * 100)
										});
//									}
//								});
//							}
						});
						$scope.gridOptions.data = $scope.chordTableData;
						$scope.clubTablePreloader = false;
					}, function () {
						$scope.clubTablePreloader = false;
					});

					$(".card_segment_tooltip").html(
						'<div class="p-rel"><ul class="custom-list item-1 pad-y10 pad-x5 card-tool-tip-right"><li class="c-arrow"><h3 class="text-capitalize mar-b0">' + segmentdetails.club_name.toLowerCase() + '</h3></li><li class="c-arrow"><p class=" ws-normal d-ib" style="white-space: normal;">Total Customers in the club <span class="fa fa-info-circle text-info-blue value_for_tooltip" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span><strong style="white-space: normal;"></p>' +
						$scope.formatTotalAmount(segmentdetails.customer_count) + '<span class="d-inline  value_for_tooltip" > (' + ((segmentdetails.customer_count / $scope.clusterDetail.customer_count) * 100).toFixed(2) + '%)' +
						'</span></strong></li><li class="border-r0 c-arrow"><p class="d-ib  ws-normal" style="white-space: normal;">Primary Usage <span class="fa fa-info-circle text-info-blue primary_value_tooltip" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p><strong class="pink" style="white-space: normal;">' +
						'<span class="d-inline primary_value_tooltip">' + primaryPercentage[i].toFixed(0) + '%</span>' +
						'</strong></li><li class="c-arrow"><p class="d-ib  ws-normal" style="white-space: normal;">Total expenditure in the club <span class="fa fa-info-circle text-info-blue total_exp_for_tooltip" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p><strong style="white-space: normal;">' +
						'<span class="fa fa-ils f-16 mar-r5 d-inline "></span>' + $scope.formatTotalAmount(segmentdetails.total_expenditure) + '<span class="d-inline total_exp_for_tooltip"> (' + (segmentdetails.total_expenditure_percentage).toFixed(2) + '%)' +
						'</span></strong></li><li class="c-arrow"><p class=" ws-normal" style="white-space: normal;">Average expenditure per customer in the club</p><strong  class="pink" style="white-space: normal;">' +
						'<span class="fa fa-ils f-16 mar-r5 d-inline"></span>' + $scope.formatTotalAmount(segmentdetails.average_expenditure) +
						'</strong></li><li class="border-r0 c-arrow"><p class=" ws-normal" style="white-space: normal;">Average expenditure among all customers in the cluster</p><strong class="pink" style="white-space: normal;">' +
						'<span class="fa fa-ils f-16 mar-r5 d-inline"></span>' + $scope.formatTotalAmount(average_expenditure_of_all_customers_in_cluster) +
						'</strong></li></ul>' +
						'<span class="right-col hidden" style="text-transform:unset !important">' +
						((segmentdetails.customer_count / $scope.clusterDetail.customer_count) * 100).toFixed(2) + '%' + ' of Cluster Customers</span>' +
						'<span class="right-col-percentage hidden" style="text-transform:unset !important"> Percentage of customer which uses the card as primary ' +
						primaryPercentage[i].toFixed(0) + '%</span>' +
						'<span class="right-col-total-expenditure hidden" style="text-transform:unset !important"> Percentage from the total expenditure in Cluster ' +
						(segmentdetails.total_expenditure_percentage).toFixed(2) + '%</span></div>');
				});
				$(".card_segment_tooltip").css("display", "block");
			} else {
				//$scope.dualcardObject.segmenttooltipspinner = true;
				$(".card_segment_tooltip").css("display", "none");
				$(".card_segment_tooltip").html();
			}
		};
	}
	var clubnames = {};
	var chartOption1 = {
		container: "#vlaCompare1",
		containerName: 'vlaCompare1',
		height: 300,
		width: 500,
		marginTop: 50
	};
	var chartOption2 = {
		container: "#vlaCompare2",
		containerName: 'vlaCompare2',
		height: 300,
		width: 500,
		marginTop: 50
	};
	var avoidClubKeys = [];
	$scope.avoidClubKeys = [];

	function getvlaclubnames(clubdata, clubConnectionInfo) {
		var club = {};
		if (!clubdata.source || clubdata.source.index == undefined) {
			club.club_one_name = $scope.avoidClubKeys[0];
			club.club_two_name = $scope.avoidClubKeys[1];
		} else {
			if (clubConnectionInfo != "comparisonRibbonInfo") {
				clubnames.club_one_name = clubConnectionInfo.club_one_name;
				clubnames.club_two_name = clubConnectionInfo.club_two_name;
			} else {
				clubnames.club_one_name = $scope.connectionDetails.club_one_name;
				clubnames.club_two_name = $scope.connectionDetails.club_two_name;
			}
			club.club_one_name = clubnames.club_one_name;
			club.club_two_name = clubnames.club_two_name;
			var path1 = EHUB_API + 'leadGeneration/uc2/2_4/graph/' + $scope.clusterId +
				'/' + clubnames.club_one_name + '/' + graph1_data.graphtype + '?token=' +
				$rootScope.ehubObject.token;
			var path2 = EHUB_API + 'leadGeneration/uc2/2_4/graph/' + $scope.clusterId +
				'/' + clubnames.club_two_name + '/' + graph2_data.graphtype + '?token=' +
				$rootScope.ehubObject.token;
			avoidClubKeys[0] = clubnames.club_one_name;
			avoidClubKeys[1] = clubnames.club_two_name;
			$scope.avoidClubKeys = avoidClubKeys;
			getComparisonData(path1, 'vlaCompare1');
			getComparisonData(path2, 'vlaCompare2');
		}
		if (clubConnectionInfo != "comparisonRibbonInfo") {
			$scope.connectionDetails = {};
			$scope.connectionDetails.connection_cust_percentage = ((clubConnectionInfo.customer_count / $scope.clusterDetail.customer_count) * 100).toFixed(2);
			$scope.connectionDetails.total_expenditure_percentage = ((clubConnectionInfo.total_expenditure / $scope.clusterDetail.total_expenditure) * 100).toFixed(2);
			$scope.connectionDetails.avg_expenditure_percentage = ((clubConnectionInfo.average_expenditure / $scope.clusterDetail.avg_expenditure) * 100).toFixed(2);
			$scope.connectionDetails.common_customer_count = $scope.formatTotalAmount(clubConnectionInfo.customer_count);
			$scope.connectionDetails.total_expenditure = clubConnectionInfo.total_expenditure;
			$scope.connectionDetails.avg_expenditure = clubConnectionInfo.average_expenditure;
		}

		//vla(clubnames, arc_tooltip_details, graph1_data, graph2_data);
	}

	var compareGraphData1 = [];
	var compareGraphData2 = [];
	var allKeys = [];

	function getComparisonData(path, id) {
		compareGraphData1 = [];
		compareGraphData2 = [];
		LeadGenerationApiService.comparisonChartPlot(path).then(function (response) {
			if (id == 'vlaCompare1') {
				compareGraphData1 = response;
				chartOption1.data = response.data;
				response.data.data.vertices.map(function (d, k) {
					if (k != 0) {
						if (allKeys.indexOf(d.name) < 0) {
							allKeys.push(d.name);
						}
					}
				});
			} else {
				compareGraphData2 = response;
				chartOption2.data = response.data;
				response.data.data.vertices.map(function (d, k) {
					if (k != 0) {
						if (allKeys.indexOf(d.name) < 0) {
							allKeys.push(d.name);
						}
					}
				});
				setTimeout(function () {
					plotComparisonChart(compareGraphData1, compareGraphData2);
				}, 1000);
			}
		}, function () {

		});
	}


	var comparisonOptions = {};

	function plotComparisonChart(data1, data2) {
		if (data1.data && data1.data.data && data1.data.data.vertices && data1.data.data.vertices.length > 0 && data2.data && data2.data.data && data2.data.data.vertices && data2.data.data.vertices.length > 0) {
			var colorArrayProgressBar = [{
					'color': ' ',
					'textColor': ''
				},
				{
					'color': '#F2626B',
					'textColor': '#ffffff'
				},
				{
					'color': '#FF3143',
					'textColor': '#ffffff'
				},
				{
					'color': '#FFAFCD',
					'textColor': '#ffffff'
				},
				{
					'color': '#EEC2EB',
					'textColor': '#ffffff'
				},
				{
					'color': '#FF71FD',
					'textColor': '#ffffff'
				},
				{
					'color': '#BA8BDB',
					'textColor': '#ffffff'
				},
				{
					'color': '#9236C6',
					'textColor': '#ffffff'
				},
				{
					'color': '#5370FD',
					'textColor': '#ffffff'
				},
				{
					'color': '#5CAAF9',
					'textColor': '#ffffff'
				},
				{
					'color': '#00B9C9',
					'textColor': '#ffffff'
				},
				{
					'color': '#37B761',
					'textColor': '#ffffff'
				},
				{
					'color': '#FFA238',
					'textColor': '#ffffff'
				},
				{
					'color': '#FEE14D',
					'textColor': 'lightslategray'
				},
				{
					'color': '#FCFFA8',
					'textColor': 'lightslategray'
				},
				{
					'color': '#C9B7B1',
					'textColor': '#ffffff'
				},
				{
					'color': '#A17F74',
					'textColor': '#ffffff'
				}
			];
			//var colorArrayProgressBar = [' ','#FF3143','#F2626B','#FFAFCD','#EEC2EB','#FF71FD','#BA8BDB','#9236C6','#5370FD','#5CAAF9','#00B9C9','#37B761','#FFA238','#FEE14D','#FCFFA8','#C9B7B1','#A17F74'];
			var getNoOfAttr = [];
			data1.data.data.vertices.map(function (d) {
				if (getNoOfAttr.indexOf(d.name) < 0) {
					getNoOfAttr.push(d.name);
				}
			});

			data2.data.data.vertices.map(function (d) {
				if (getNoOfAttr.indexOf(d.name) < 0) {
					getNoOfAttr.push(d.name);
				}
			});

			var colorObj = [];
			getNoOfAttr.map(function (d, k) {
				if (d != avoidClubKeys[0] && d != avoidClubKeys[1]) {
					colorObj.push({
						'name': d,
						'color': colorArrayProgressBar[k].color,
						'textColor': colorArrayProgressBar[k].textColor
					});
				}
			});
			var createFinalObj = [];
			var chordNameWithData = [];
			getNoOfAttr.map(function (d) {
				if (d != avoidClubKeys[0] && d != avoidClubKeys[1]) {
					var index1 = data1.data.data.vertices.map(function (d1) {
						return d1.name;
					}).indexOf(d);
					var index2 = data2.data.data.vertices.map(function (d2) {
						return d2.name;
					}).indexOf(d);
					if (data1.data.data.vertices[index1] && data1.data.data.vertices[index1].avgExp) {
						var chord1Value = data1.data.data.vertices[index1].avgExp;
					} else {
						var chord1Value = 0;
					}
					if (data2.data.data.vertices[index2] && data2.data.data.vertices[index2].avgExp) {
						var chord2Value = data2.data.data.vertices[index2].avgExp;
					} else {
						var chord2Value = 0;
					}
					createFinalObj.push({
						'name': d,
						'chord1': chord1Value,
						'chord2': chord2Value
					});
				}
			});
			var indexOfChord1 = data1.data.data.vertices.map(function (d1) {
				return d1.name;
			}).indexOf(avoidClubKeys[0]);
			var indexOfChord2 = data2.data.data.vertices.map(function (d1) {
				return d1.name;
			}).indexOf(avoidClubKeys[1]);
			chordNameWithData.push(data1.data.data.vertices[indexOfChord1]);
			chordNameWithData.push(data2.data.data.vertices[indexOfChord2]);
			$scope.clubDetails = [];
			$scope.clubDetails = chordNameWithData;
			

			$scope.comparisonProgressBarLegendData = [];
			$scope.$apply(function () {
				$scope.comparisonProgressBarLegendData = colorObj;
				$scope.addMediaPreloader = false;
			});
			
			comparisonOptions = {
				container: "#vlaCompare1",
				colorArray: colorArrayProgressBar,
				width: $('#compareChart').width() / 2.2,
				height: 400,
				colorObj: colorObj,
				data: createFinalObj,
				avoidKeys: avoidClubKeys
			};

			comparisonProgressBar(comparisonOptions);
		} else {
			$scope.$apply(function () {
				$scope.addMediaPreloader = false;
			});
			$('#vlaCompare1').html(
				"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data not Found</span></div> "
			);
		}
	}
	function sliderAvgExpen() {
		$('#sliderAvgExpen').slider({
			range: true,
			min: 0,
			max: 100,
			values: [0, 100],
			slide: function(event,ui){
				slider = false;
        	   filterByAvgExpenditure(ui.values, "onChange");
            },
			stop: function (event, ui) {
				$(".Bubble_Chart_tooltip").remove();
				filterByAvgExpenditure(ui.values);
				if (ui.values[0] == ui.values[1]) {
					$('#sliderAvgExpen .price-range-both i').css('display', 'none');
				} else {
					$('#sliderAvgExpen .price-range-both i').css('display', 'inline');
				}

				if (collision($('#sliderAvgExpen .price-range-min'), $('#sliderAvgExpen .price-range-max')) == true) {
					$('#sliderAvgExpen .price-range-min, #sliderAvgExpen .price-range-max').css('opacity', '0');
					$('#sliderAvgExpen .price-range-both').css('display', 'block');
				} else {
					$('#sliderAvgExpen .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sliderAvgExpen .price-range-both').css('display', 'none');
				}
			}
		});

		$('#sliderAvgExpen .ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#sliderAvgExpen').slider('values', 0) + '</span>');
		$('#sliderAvgExpen .ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#sliderAvgExpen').slider('values', 1) + '</span>');
	}

	function sliderCustomerCount() {
		$('#sliderCustomerCount').slider({
			range: true,
			min: 0,
			max: 100,
			values: [0, 100],
			slide: function(event,ui){
				slider = false;
				filterByCustomerCount(ui.values, "onChange");
            },
			stop: function (event, ui) {
				$(".Bubble_Chart_tooltip").remove();
				filterByCustomerCount(ui.values);
				if (ui.values[0] == ui.values[1]) {
					$('#sliderCustomerCount .price-range-both i').css('display', 'none');
				} else {
					$('#sliderCustomerCount .price-range-both i').css('display', 'inline');
				}

				if (collision($('#sliderCustomerCount .price-range-min'), $('#sliderCustomerCount .price-range-max')) == true) {
					$('#sliderCustomerCount .price-range-min, #sliderCustomerCount .price-range-max').css('opacity', '0');
					$('#sliderCustomerCount .price-range-both').css('display', 'block');
				} else {
					$('#sliderCustomerCount .price-range-min,#slider2 .price-range-max').css('opacity', '1');
					$('#sliderCustomerCount .price-range-both').css('display', 'none');
				}
			}
		});

		$('#sliderCustomerCount .ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#sliderCustomerCount').slider('values', 0) + '</span>');
		$('#sliderCustomerCount .ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#sliderCustomerCount').slider('values', 1) + '</span>');
	}


	sliderAvgExpen();
	sliderCustomerCount();
	var filterDataByCustomerCount = [];
	var customerCountScale;
	var expScale;
	var minCustomerCount;
	var maxCustomerCount;
	var minAvgExpenditure;
	var maxAvgExpenditure;

	/* @purpose: Set minimum and maximum for sliders
	 * @created: 24th Oct 2018
	 * @author: varsha
	 */

	function setMinMaxData() {
		customerCountScale = d3.scaleLinear().domain([0, 100]).range([d3.min(clubChordStatsData, function (d) {
			return d.customer_count;
		}), d3.max(clubChordStatsData, function (d) {
			return d.customer_count;
		})]);
		expScale = d3.scaleLinear().domain([0, 100]).range([d3.min(clubChordStatsData, function (d) {
			return d.average_expenditure;
		}), d3.max(clubChordStatsData, function (d) {
			return d.average_expenditure;
		})]);
		minCustomerCount = customerCountScale(0);
		maxCustomerCount = customerCountScale(100);
		minAvgExpenditure = expScale(0);
		maxAvgExpenditure = expScale(100);
		$scope.minimumExpenditure = parseInt(minAvgExpenditure);
		$scope.maximumExpenditure = parseInt(maxAvgExpenditure);
		$('#minCus').text($scope.formatTotalAmount(minCustomerCount));
		$('#maxCus').text($scope.formatTotalAmount(maxCustomerCount));
		$("#customerCount").find(".price-range-min").html(formatNummerWithComma(minCustomerCount));
		$("#customerCount").find(".price-range-max").html(formatNummerWithComma(maxCustomerCount));
		$("#avgExpenditure").find(".price-range-min").html("<span class='fa fa-ils f-10'></span> " + formatNummerWithComma(minAvgExpenditure)) ;
		$("#avgExpenditure").find(".price-range-max").html("<span class='fa fa-ils f-10'></span> " + formatNummerWithComma(maxAvgExpenditure)) ;
	}



	/* @purpose: Filter By Customer count
	 * @created: 24th Oct 2018
	 * @author: varsha
	 */

	function filterByCustomerCount(values, type) {
		filterDataByCustomerCount = [];
		minCustomerCount = customerCountScale(values[0]);
		maxCustomerCount = customerCountScale(values[1]);
		if(type != "onChange"){
			clubChordStatsData.map(function (d) {
				if (d.customer_count <= maxCustomerCount && d.customer_count >= minCustomerCount && d.average_expenditure <= maxAvgExpenditure && d.average_expenditure >= minAvgExpenditure) {
					filterDataByCustomerCount.push(d);
				}
			});
			setDataForChordDiagram();
		}
		$scope.minimumCustomerCount = minCustomerCount;
		$scope.maximumCustomerCount = maxCustomerCount;
		$('#minCus').text($scope.formatTotalAmount(minCustomerCount));
		$('#maxCus').text($scope.formatTotalAmount(maxCustomerCount));
		$("#customerCount").find(".price-range-min").html(formatNummerWithComma(minCustomerCount));
		$("#customerCount").find(".price-range-max").html(formatNummerWithComma(maxCustomerCount));
	}

	/* @purpose: Filter By average expenditure
	 * @created: 24th Oct 2018
	 * @author: varsha
	 */

	var minAvgExpenditure;
	var maxAvgExpenditure;

	function filterByAvgExpenditure(values, type) {
		filterDataByCustomerCount = [];
		minAvgExpenditure = expScale(values[0]);
		maxAvgExpenditure = expScale(values[1]);
		if(type != "onChange"){
			clubChordStatsData.map(function (d) {
				if (d.customer_count <= maxCustomerCount && d.customer_count >= minCustomerCount && d.average_expenditure <= maxAvgExpenditure && d.average_expenditure >= minAvgExpenditure) {
					filterDataByCustomerCount.push(d);
				}
			});
			setDataForChordDiagram();
		}
		$scope.minimumExpenditure = minAvgExpenditure;
		$scope.maximumExpenditure = maxAvgExpenditure;
		$('#minexp').text($scope.formatTotalAmount(minAvgExpenditure));
		$('#maxexp').text($scope.formatTotalAmount(maxAvgExpenditure));
		$("#avgExpenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + formatNummerWithComma(minAvgExpenditure));
		$("#avgExpenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + formatNummerWithComma(maxAvgExpenditure));
	}


	/* @purpose: REplot chord diagram after filter
	 * @created: 24th Oct 2018
	 * @author: varsha
	 */

	function setDataForChordDiagram() {
		chorddetailsSegmentarrayFilter = [];
		primaryPercentageFilter = [];
		expenditureListFilter = [];
		chorddetailsSegmentarrayFilter = filterDataByCustomerCount.map(function (
			item) {
			primaryPercentageFilter.push(item.customer_primary_percentage);
			expenditureListFilter.push({
				'avgExp': item.average_expenditure,
				'customerCount': item.customer_count
			});
			return item.club_name;
		});
		expenditureListFilter.map(function (d) {
			d.evt_color = colorScale(parseFloat(d.avgExp));
		});


		chorddetailsArc_connectionmatrixFilter = new Array(chorddetailsSegmentarrayFilter.length);
		for (var i = 0; i < chorddetailsSegmentarrayFilter.length; i++) {
			chorddetailsArc_connectionmatrixFilter[i] = new Array();
			for (var j = 0; j < chorddetailsSegmentarrayFilter.length; j++) {
				chorddetailsArc_connectionmatrixFilter[i][j] = 0;
			}
		}
		for (var i = 0; i < connectionChordData.length; i++) {
			var a_i = chorddetailsSegmentarrayFilter.indexOf(connectionChordData[i].club_one_name);
			var a_j = chorddetailsSegmentarrayFilter.indexOf(connectionChordData[i].club_two_name);
			if (a_j != -1 && a_i != -1) {
				chorddetailsArc_connectionmatrixFilter[a_i][a_j] = connectionChordData[i].customer_count;
			}


		}

		var validForChordDiagram = false;
		for (var i = 0; i < chorddetailsArc_connectionmatrixFilter.length; i++) {
			for (var j = 0; j < chorddetailsArc_connectionmatrixFilter[i].length; j++) {
				if (chorddetailsArc_connectionmatrixFilter[i][j] > 0) {
					validForChordDiagram = true;
				}

			}
		}
		if (validForChordDiagram) {
			setChordTableData(chorddetailsSegmentarrayFilter, filterDataByCustomerCount, chorddetailsArc_connectionmatrixFilter);
			getMaxLinkFunction(chorddetailsArc_connectionmatrixFilter);
			for (var i = 0; i < chorddetailsArc_connectionmatrixFilter.length - 1; i++) {
				var count = 0;
				for (var j = 0; j < chorddetailsArc_connectionmatrixFilter[i].length; j++) {
					if (chorddetailsArc_connectionmatrixFilter[i][j] >= 1 && count < noOfConnection) {
						count++;
					} else {
						chorddetailsArc_connectionmatrixFilter[i][j] = 0;
					}
				}
			}
			getMaxLinkFunction(chorddetailsArc_connectionmatrixFilter);

			resizeSegmentarray = chorddetailsSegmentarrayFilter;
			resizeArc_connectionmatrix = chorddetailsArc_connectionmatrixFilter;
			resizePrimaryPercentage = primaryPercentageFilter;
			resizeExpenditureList = expenditureListFilter;
			var valid = validToPlot(chorddetailsArc_connectionmatrixFilter)
			if(valid){
			plotchordchart(chorddetailsSegmentarrayFilter, chorddetailsArc_connectionmatrixFilter,
				primaryPercentageFilter, expenditureListFilter);
			$('#noData').hide();
			}
			
		} else {
			resizeSegmentarray = chorddetailsSegmentarrayFilter;
			resizeArc_connectionmatrix = chorddetailsArc_connectionmatrixFilter;
			resizePrimaryPercentage = primaryPercentageFilter;
			resizeExpenditureList = expenditureListFilter;
			$('#noData').show();
			$('#customerUsageChart').empty();
			$('#noData').html(
				"<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data not Found</span></div> "
			);
			$('#ribbonInfo').css('display','none');
			$('#sideBarForChordClick').css('display','none');
		}


	}
	
	
	function validToPlot (chorddetailsArc_connectionmatrixFilter){
		var validForChordDiagram = false;
		for (var i = 0; i < chorddetailsArc_connectionmatrixFilter.length; i++) {
			for (var j = 0; j < chorddetailsArc_connectionmatrixFilter[i].length; j++) {
				if (chorddetailsArc_connectionmatrixFilter[i][j] > 0) {
					validForChordDiagram = true;
				}

			}
		}
		return validForChordDiagram;
	}

	/* @purpose: On click of chord get linked ribbon
	 * @created: 25th Oct 2018
	 * @author: varsha
	 */

	var setChordDataObj = [];

	function setChordTableData(chorddetailsSegmentarrayFilter, filterDataByCustomerCount, chorddetailsArc_connectionmatrixFilter) {
		setChordDataObj = [];
		chorddetailsSegmentarrayFilter.map(function (d) {
			var localObj = {
				'name': d,
				'connectionBy': []
			};
			connectionChordData.map(function (d1) {
				var a_i = chorddetailsSegmentarrayFilter.indexOf(d1.club_one_name);
				var a_j = chorddetailsSegmentarrayFilter.indexOf(d1.club_two_name);
				if (d1.club_one_name == d && chorddetailsArc_connectionmatrixFilter[a_i][a_j] != 0) {
					var index = filterDataByCustomerCount.map(function (d) {
						return d.club_name;
					}).indexOf(d1.club_two_name);
					localObj.connectionBy.push(filterDataByCustomerCount[index]);
				}
			});
			setChordDataObj.push(localObj);
		});
		//		console.log(setChordDataObj)
	}

	$scope.gridOptions = {
		enableRowHeaderSelection: false,
		enableColumnMenus: false,
		columnDefs: [{
				name: 'club_name',
				displayName: '',
				width: '35%',
				rowGroupIndex: 0,
				icons: {
					sortAscending: '<i class="fa fa-sort-alpha-asc"/>',
					sortDescending: '<i class="fa fa-sort-alpha-desc"/>'
				}
			},
			{
				name: 'customer_count',
				displayName: '',
				width: '20%',
				type: 'number'
			},
			{
				name: 'average_expenditure',
				displayName: '',
				width: '25%',
				type: 'number'
			},
			{
				name: 'customer_primary_percentage',
				displayName: '%',
				width: '20%',
				type: 'number'
			}
		],
		//	headerTemplate: 'views/NewFile.html';
	};





	function setTableForChord(d) {
		$scope.chordTableData = [];
		$timeout(function () {
			$scope.chordClickName = setChordDataObj[d.index].name;
		}, 0);
	}

	/* @purpose: Show chord page
	 * @created: 25th Oct 2018
	 * @author: varsha
	 */

	$rootScope.backToChord = function () {
		$('#vlaCompare1').empty();
		$rootScope.showComparisonPageBreadCrumbs = false;
		$rootScope.showDetailPage = true;
		$rootScope.showDetailPageActive = true;
		$scope.showProgressBar = false;
		
		setTimeout(function () {
          $(".filters-subhead-wrapper,.mxh-dashboard, .scroll-table-chord").mCustomScrollbar({
        	  axis: "y",
        	  theme: "minimal-dark"
          });
		}, 0);
	};

	$scope.chordTableFirstColTooltip = function (name) {
		$scope.chordTableFirstColTooltipScope = name;
	};
	/* @purpose: Show comparison page
	 * @created: 25th Oct 2018
	 * @author: varsha
	 */

	$scope.showComparisonPage = function (clubConnectionInfo) {
		$(".filters-subhead-wrapper,.mxh-dashboard, .scroll-table-chord").mCustomScrollbar('destroy');
		$scope.showProgressBar = true;
		$scope.addMediaPreloader = true;
		$rootScope.showDetailPage = true;
		$rootScope.showComparisonPageBreadCrumbs = true;
		$rootScope.showDetailPageActive = false;
		$rootScope.showComparisonPageBreadCrumbsActive = true;
		getvlaclubnames(storedClickedRibbon, clubConnectionInfo);
	};

	/* @purpose: Shortcut to go comparison page from chord side bar
	 * @created: 1st Nov 2018
	 * @author: varsha
	 */

	$scope.shortcutToConnetion = function (club_name, clubConnectionInfo) {
		var path1 = EHUB_API + 'leadGeneration/uc2/2_4/graph/' + $scope.clusterId +
			'/' + selectedChordName + '/' + graph1_data.graphtype + '?token=' +
			$rootScope.ehubObject.token;
		var path2 = EHUB_API + 'leadGeneration/uc2/2_4/graph/' + $scope.clusterId +
			'/' + club_name + '/' + graph2_data.graphtype + '?token=' +
			$rootScope.ehubObject.token;
		avoidClubKeys = [];
		avoidClubKeys[0] = selectedChordName;
		avoidClubKeys[1] = club_name;
		$scope.avoidClubKeys = [];
		$scope.avoidClubKeys = avoidClubKeys;
		getComparisonData(path1, 'vlaCompare1');
		getComparisonData(path2, 'vlaCompare2');
		$scope.showComparisonPage(clubConnectionInfo);
	};
	


}