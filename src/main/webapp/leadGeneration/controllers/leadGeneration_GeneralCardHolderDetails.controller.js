'use strict';
angular.module('leadGenerationApp')
    .controller('LeadGenerationGeneralCardHolderDetailsController', leadGenerationGeneralCardHolderDetailsController);

leadGenerationGeneralCardHolderDetailsController.$inject = [
    '$scope',
    '$state',
    'LeadGenerationGraphService',
    'LeadGenerationApiService',
    '$rootScope',
    '$stateParams',
    '$q',
];

function leadGenerationGeneralCardHolderDetailsController(
    $scope,
    $state,
    LeadGenerationGraphService,
    LeadGenerationApiService,
    $rootScope,
    $stateParams
) {

    $scope.hidingOnfilter = true;
    $scope.israCardClusterViewVariable = {
        plotAgeData: [],
        bs_Score: [],
        as_Score: [],
        standardDeviationValue: 1.5,
        customerData: [],
        clusterDetail: [],
        StoryName: '',
        avgExpenBar: [],
        generalExpenBar: [],
        cardDurationLegend: [],
        genderLegend: [],
        issuedByLegend: [],
        clusterLegend: []
    };

    $scope.israCardClusterViewFunction = {
        deviationChange: deviationChange,
        progressBarFilter: progressBarFilter,
        removeTab: removeTab,
        renameClusterName: renameClusterName,
        renameClusterNameKeyUp: renameClusterNameKeyUp,
        changeTablePageNo: changeTablePageNo,
        changeCustomerId:changeCustomerId
    };


    var filterParam = {
        string_filters: [],
        integer_filters: [],
        combine_filters: [],
    };
    var originalCustomerData=[];

    //code for number formatting
    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };
    
    $scope.formatTotalAmount = function (amount) {
		if (amount != undefined) {
			var amountVal = amount.toString().split('.');
			var finalAmount;
			if (amountVal[0].length <= 6) {
				finalAmount = formatComma(amount);
			} else if (amountVal[0].length <= 8 && amountVal[0].length > 6) {
				finalAmount = (amount / 1000000).toFixed(2) + 'M';
			} else if (amountVal[0].length >= 9) {
				finalAmount = (amount / 1000000000).toFixed(2) + 'B';
			}
			return finalAmount;
		}
	};
	function formatComma(amount){
		var formatAmount;
		formatAmount = Number(Math.round(parseFloat(amount))).formatAmt();
		return formatAmount.split('.')[0];
	}

    $scope.formatNummerWithComma = function(amount) {
    	 var formatAmount;
         formatAmount = Number(Math.round(amount)).formatAmt();
         return formatAmount;
    };
    var colors = ['#1C6EC1', '#72BE74', '#ADE3AC', '#55D2E1', '#F36030', '#A072FC', '#CB3E4B', '#CB8B3D', '#2B2F26', '#E66C40', '#00C4AB', '#CBA03D', '#76CB3D', '#CB493D', '#70685F'];

    var StoryId = sessionStorage.getItem("storyId");
    if (StoryId == 'undefined') {
        $scope.hideStories = false;
    } else {
        $scope.hideStories = true;
    }

    var dayOfWeekKeysUImodification = [{
        key: 'day_1_4',
        UIname: 'Sun - Wed'
    }, {
        key: 'day_5',
        UIname: 'Thu'
    }, {
        key: 'day_6',
        UIname: 'Fri'
    }, {
        key: 'day_7',
        UIname: 'Sat'
    }];
    var dayPartKeysUImodification = [{
        key: 'DP1M_23_04',
        UIname: '23:00 - 4:00'
    }, {
        key: 'DP2M_04_06',
        UIname: '4:00 - 06:00'
    }, {
        key: 'DP3M_06_12',
        UIname: '6:00 - 12:00'
    }, {
        key: 'DP4M_12_1630',
        UIname: '12:00 - 16:30'
    }, {
        key: 'DP5M_1630_23',
        UIname: '16:30 - 23:00'
    }];

    var commonLocalizationTable = [{
        key: 'DP1M_23_04',
        UIname: '23:00 - 4:00'
    }, {
        key: 'DP2M_04_06',
        UIname: '4:00 - 06:00'
    }, {
        key: 'DP3M_06_12',
        UIname: '6:00 - 12:00'
    }, {
        key: 'DP4M_12_1630',
        UIname: '12:00 - 16:30'
    }, {
        key: 'DP5M_1630_23',
        UIname: '16:30 - 23:00'
    }, {
        key: 'DAY_1_4',
        UIname: 'Sun - Wed'
    }, {
        key: 'DAY_5',
        UIname: 'Thu'
    }, {
        key: 'DAY_6',
        UIname: 'Fri'
    }, {
        key: 'DAY_7',
        UIname: 'Sat'
    }];
    
    /* @purpose: call rename cluster api
     * @created: 14th August 2018
     * @params: params(object)
     * @author: varsha
     */

    function postsetClusterName(stroryId, clusterId, data) {
        LeadGenerationApiService.postsetClusterName(stroryId, clusterId, data).then(function() {
        }, function() {});
    }
    
    
    /* @purpose: Get search customer list
     * @created: 3rd December 2018
     * @params: customerId
     * @author: varsha
     */

    function getsearchCustomer(customerId) {
        LeadGenerationApiService.getsearchCustomer(customerId,clusterId).then(function(response) {
        	$scope.customerListPreloader = false;
        	 if(response.data && response.data.data && response.data.data.customers && response.data.data.customers.length > 0){
            	$scope.israCardClusterViewVariable.customerData = response.data.data.customers;
            }else{
            	$scope.israCardClusterViewVariable.customerData = [];
            	  $scope.custErrorMsg = "Data Not Found";
            }
        }, function() {
        	 $scope.customerListPreloader = false;
             $scope.custErrorMsg = "Failed To Get Data From server";
        });
    }
    
   
    

    /* @purpose: on esc and enter key press while renaming
     * @created: 14th August 2018
     * @params: params(object)
     * @author: varsha
     */

    function renameClusterNameKeyUp(event) {
        if (event != undefined) {
            if (event.which == 27) {
                renameClusterName('discard');
                return false;
            } else if (event.which == 13) {
                renameClusterName('save');
                return false;
            } else {
                return true;
            }
        }
    }


    /* @purpose: save edited name for cluster
     * @created: 14th august 2018
     * @author: varsha
     */

    function renameClusterName(name) {
        if (name == 'discard') {
            $scope.israCardClusterViewVariable.clusterDetail.cluster_name = originalclusterDetail.cluster_name;
            return false;
        }
        if (StoryId == undefined) {
            StoryId = 'UC3';
        }
        if ($scope.israCardClusterViewVariable.clusterDetail.cluster_name == originalclusterDetail.cluster_name) {
            return false;
        } else if ($scope.israCardClusterViewVariable.clusterDetail.cluster_name.length > 0 && $scope.israCardClusterViewVariable.clusterDetail.cluster_name != originalclusterDetail.cluster_name) {
            originalclusterDetail.cluster_name = $scope.israCardClusterViewVariable.clusterDetail.cluster_name;
            postsetClusterName(StoryId, clusterId, {
                'name': $scope.israCardClusterViewVariable.clusterDetail.cluster_name
            });
        } else {
            $scope.israCardClusterViewVariable.clusterDetail.cluster_name= originalclusterDetail.cluster_name;
        }
    }

    //----------------------------Api functions-----------------------------------------------

    /* @purpose: get distribution
     * @created: 28th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var genderData = [];
    var ageData = [];
    var bs_scoreData = [];
    var as_scoreData = [];
    var bankingData = [];
    var dayPartData = [];
    var dayWeekData = [];
    var merchantData = [];
    
    
   
   
    function getDistribution(clusterId, attrName, attrType, distrubtionCount, params,day) {
        LeadGenerationApiService.getDistribution(clusterId, attrName, attrType, distrubtionCount, params).then(function(response) {
        	$scope.fullpageloader = false;
            if (attrName == 'gender') {
            	genderData = jQuery.extend(true,[], response.data.data);
            	if(storeDataColorObj){
            		setColorObjPies('gender',response.data.data)
            	}
                plotPiechart(response.data.data, '#gender_Pie');
            } else if (attrName == 'age') {
            	ageData = jQuery.extend(true,[], response.data.data);
                plotBarChart('age', response.data.data);
            } else if (attrName == 'bs_Score') {
            	bs_scoreData = jQuery.extend(true,[], response.data.data);
                plotFixedAsBsScore(response.data.data, '#bs_score', 130);
            } else if (attrName == 'as_Score') {
            	as_scoreData = jQuery.extend(true,[], response.data.data);
                plotFixedAsBsScore(response.data.data, '#as_score', 130);
            } else if (attrName == 'banking_card') {
            	bankingData = jQuery.extend(true,[], response.data.data);
            	if(storeDataColorObj){
            		setColorObjPies('banking_card',response.data.data)
            	}
                plotBankingCard(response.data.data);
            } else if (day == 'dayPart') {
            	dayPartData = jQuery.extend(true,{}, response.data.data);
            	if(storeDataColorObj){
            		setColorObjPies('dayPart',response.data.data)
            	}
            	plotDataPartCombine(response.data.data, '#dataPart', 130, dayPartKeysUImodification);
            } else if (day == 'dayOfTheWeek') {
            	dayWeekData = jQuery.extend(true,{}, response.data.data);
            	if(storeDataColorObj){
            		setColorObjPies('dayOfTheWeek',response.data.data)
            	}
                plotDataPartCombine(response.data.data, '#dayWeek', 130, dayOfWeekKeysUImodification);
            } else if (attrName == 'CultureandLeasure:Communication:Insurance:Fuel:Apparel:Furniture:Electronics:Food:InfraandGovernmental:MedicalandCosmetics:Pharma:Transportation:Tourism:MarketPlaceandDutyfree:GiftsandJewleries:Others') {
            	merchantData = jQuery.extend(true,{}, response.data.data);
            	plotMerchantType(response.data.data);
            }
        }, function() {
        	$scope.fullpageloader = false;
        });
    }

    $scope.israCardClusterViewVariable.StoryName = sessionStorage.getItem("StoryName");
    var StoryId = sessionStorage.getItem("storyId");
    var clusterId = $stateParams.clusterId;
    sessionStorage.setItem("clusterId", $stateParams.clusterId);
    $scope.fullpageloader = true;
    
    
    var genderColorObj = [];
    var bankingColorObj = [];
    var dayPartColorObj = [];
    var dayOfWeekColorObj = [];
   
    function setColorObjPies(type,data){
    	 if (type == 'gender') {
    		 data.map(function(d,k) {
    			 genderColorObj.push({
    				 'key':d.value,
    				 'color':colors[k]
    			 })
             });
    		
    	 }else if (type == 'banking_card') {
    		 data.map(function(d) {
                 if (d.value == 'true') {
                     d.key = 'Banking';
                     d.color = colors[0];
                 } else {
                     d.key = 'Non Banking';
                     d.color = colors[1]
                 }
             });
    		 
    		 bankingColorObj = data
    		
    	 }else if (type == 'dayPart') {
    		 var newData = [];
    		 var count = 0;
             angular.forEach(data, function(k, v) {
            	 count++;
                 var index = dayPartKeysUImodification.map(function(d) {
                     return d.key;
                 }).indexOf(v);
                 if (index != -1) {
                     newData.push({
                         key: dayPartKeysUImodification[index].UIname,
                         color: colors[count]
                     });
                 }
             });
    		 
             dayPartColorObj = newData
    		
    	 }else if (type == 'dayOfTheWeek') {
    		 var newData2 = [];
    		 var count2 = 0;
             angular.forEach(data, function(k, v) {
            	 count2++;
                 var index = dayOfWeekKeysUImodification.map(function(d) {
                     return d.key;
                 }).indexOf(v);
                 if (index != -1) {
                	 newData2.push({
                         key: dayOfWeekKeysUImodification[index].UIname,
                         color: colors[count2]
                     });
                 }
             });
    		 
             dayOfWeekColorObj = newData2
    		
    	 }
    	 
    	
    }
   

    /*
     * @purpose: change attribute name to local table
     * @created: 20th August 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function localizeTableAttributeNameChange(name) {
        var index = commonLocalizationTable.map(function(d) {
            return d.key;
        }).indexOf(name.toUpperCase());
        if (index == -1) {
            return name;
        } else if (index != -1) {
            return commonLocalizationTable[index].UIname;
        }
    }
    var color = ['#ce3939', '#ce6839', '#ce9c39', '#bace39', '#45ce39', '#39ce79', '#39cece', '#3997ce', '#3965ce', '#6539ce', '#ce39bf', '#ce3979', '#bc7676', '#63552c', '#39632c', '#632c49', '#66525c', '#e0c7d4', '#9e9398', '#c1608c', '#436a84'];
    var baroptions = {
        container: '#barChart',
        height: 180,
        marginRight: 5,
        marginLeft: 40,
        marginBottom: 1,
        marginTop: 30,
        color: color,
        gridColor: '#50707E',
    };


    var originalclusterDetail = [];
    var totalCustomerValue = 50;
    function getCluster(clusterId, emptyObjForGetCluster) {
        LeadGenerationApiService.getClusterDetail(clusterId, emptyObjForGetCluster).then(function(response) {
            if (response && response.data && response.data.data && response.data.data.sd_attributes.length == 0) {
                $('#clusterChart').html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
                return false;
            } else {
                $scope.israCardClusterViewVariable.clusterDetail = response.data.data;
                var customerfilterParam = jQuery.extend(true,{},filterParam);
                customerfilterParam.total_pages = Math.round($scope.israCardClusterViewVariable.clusterDetail.cluster_user_count / totalCustomerValue);
                customerfilterParam.page_number_required = 0;
                getCustomerDetail(clusterId, customerfilterParam);
                originalclusterDetail = jQuery.extend(true, {}, response.data.data);
                sessionStorage.setItem("clusterId", clusterId);
                var verticalBarChartData = [];
                angular.forEach($scope.israCardClusterViewVariable.clusterDetail.sd_attributes, function(d1) {
                    if (d1.is_found) {
                        var changedName = localizeTableAttributeNameChange(d1.attribute_name);
                        verticalBarChartData.push({
                            'name': changedName,
                            'value': d1.attribute_sd_value
                        });
                    }
                });
                baroptions.container = '#clusterChart';
                baroptions.domainArray = [d3.min($scope.israCardClusterViewVariable.clusterDetail.sd_attributes, function(d) {
                    if (d.is_found) {
                        return d.attribute_sd_value;
                    }
                }), d3.max($scope.israCardClusterViewVariable.clusterDetail.sd_attributes, function(d) {
                    if (d.is_found) {
                        return d.attribute_sd_value;
                    }
                })];
                //baroptions.domainArray =[minStandardDEviationValue,maxStandardDEviationValue]
                baroptions.data = verticalBarChartData;
                baroptions.data.sort(function(x, y) {
                    return d3.ascending(x.name, y.name);
                });
                verticalBarChart(baroptions);
                $scope.fullpageloader = false;
                setBarValue();
            }
        });

    }


  

    window.VerticalNegativeBarChartlegendData = function(data) {
        //$scope.$apply(function() {
        $scope.israCardClusterViewVariable.clusterLegend = data;
        //});
    };


    /* @purpose: get customer detail
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    function getCustomerDetail(clusterId, params) {
    	$scope.customerListPreloader = true;
        LeadGenerationApiService.getCustomerDetail(clusterId, params).then(function(response) {
    		$scope.customerListPreloader = false;
            if(response.data && response.data.data && response.data.data.customers && response.data.data.customers.length > 0){
            	 originalCustomerData = jQuery.extend(true,[],response.data.data.customers);
            	$scope.israCardClusterViewVariable.customerData = response.data.data.customers;
            }
        	else{
                  $scope.custErrorMsg = "Data Not Found";
        	}
        }, function() {
        	 $scope.customerListPreloader = false;
             $scope.custErrorMsg = "Failed To Get Data From server";
        });
   }
    //-------------------------------End of api functions----------------------------------   

    /*
     * @purpose: Call all api's 
     * @created: 25th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var storeDataColorObj;
    function CusterApi(status) {
    	storeDataColorObj = status;
    	var ageValue = 4;
    	var dayPartUrl = 'DP1M_23_04:DP2M_04_06:DP3M_06_12:DP4M_12_1630:DP5M_1630_23';
        var dayOfWeekUrl = 'day_1_4:day_5:day_6:day_7';
    	if(filterParam && filterParam.integer_filters && filterParam.integer_filters.length){
	    	filterParam.integer_filters.map(function(d){
	    		if(d.attribute_name == 'age'){
	    			 ageValue = 1;
	    		}
	    	});
    	}
    	if(filterParam && filterParam.combine_filters && filterParam.combine_filters.length){
	    	filterParam.combine_filters.map(function(d){
	    		if(d.attribute_name == "Day Part"){
	    			dayPartUrl = d.values[0];
	    		}else if(d.attribute_name == "DAY OF THE WEEK"){
	    			dayOfWeekUrl = d.values[0];
	    		}
	    	});
    	}
    	
    	
        getDistribution(clusterId, 'gender', 'string', 3, filterParam);
        getDistribution(clusterId, 'age', 'integer', ageValue, filterParam);
        getDistribution(clusterId, 'bs_Score', 'integer', 12, filterParam);
        getDistribution(clusterId, 'as_Score', 'integer', 12, filterParam);
        getDistribution(clusterId, 'banking_card', 'string', 2, filterParam);
        getDistribution(clusterId, dayPartUrl, 'combine', 0, filterParam,'dayPart');
        getDistribution(clusterId, dayOfWeekUrl, 'combine', 0, filterParam,'dayOfTheWeek');
      ///  getCustomerDetail(clusterId, customerfilterParam)
        
    }

    var emptyObjForGetCluster = {};
    getCluster(clusterId, emptyObjForGetCluster);
    CusterApi(true);
    getDistribution(clusterId, 'CultureandLeasure:Communication:Insurance:Fuel:Apparel:Furniture:Electronics:Food:InfraandGovernmental:MedicalandCosmetics:Pharma:Transportation:Tourism:MarketPlaceandDutyfree:GiftsandJewleries:Others', 'combine', 0, {});

    /*
     * @purpose: set bar value 
     * @created: 28th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function setBarValue() {
        if ($scope.israCardClusterViewVariable.clusterDetail.cluster_avg_expenditure < $scope.israCardClusterViewVariable.clusterDetail.general_avg_expenditure) {
            $scope.israCardClusterViewVariable.avgExpenBar.push({
                perValue: ($scope.israCardClusterViewVariable.clusterDetail.cluster_avg_expenditure / $scope.israCardClusterViewVariable.clusterDetail.general_avg_expenditure) * 100,
                value: $scope.israCardClusterViewVariable.clusterDetail.cluster_avg_expenditure
            });
            $scope.israCardClusterViewVariable.generalExpenBar.push({
                perValue: 100,
                value: $scope.israCardClusterViewVariable.clusterDetail.general_avg_expenditure
            });
        } else {
            $scope.israCardClusterViewVariable.generalExpenBar.push({
                perValue: ($scope.israCardClusterViewVariable.clusterDetail.general_avg_expenditure / $scope.israCardClusterViewVariable.clusterDetail.cluster_avg_expenditure) * 100,
                value: $scope.israCardClusterViewVariable.clusterDetail.general_avg_expenditure
            });
            $scope.israCardClusterViewVariable.avgExpenBar.push({
                perValue: 100,
                value: $scope.israCardClusterViewVariable.clusterDetail.cluster_avg_expenditure
            });
        }
    }




    /* @purpose: plot pie chart
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function plotPiechart(data, id) {
        if (data.length == 0) {
            $(id).html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
            return false;
        } else {
            data.map(function(d) {
                d.key = d.value;
                d.value = d.customer_count;
            });
            var sum = d3.sum(data, function(d) {
                return d.value;
            });

            var alertsChartpieoption = { //Piechart option
                container: id,
                colors: colors,
                data: data,
                height: 130,
                txtSize: '10px',
                legendwidth: 40,
                txtColor: '4D6772',
                legendmargintop: 30,
                format:true,
                sum: (sum / data.length) * $scope.israCardClusterViewVariable.standardDeviationValue
            };
            if(id == '#gender_Pie'){
            	alertsChartpieoption.fixedColorWithKey = genderColorObj;
            }
            
            
            setTimeout(function() {
                new reusablePie(alertsChartpieoption); //load pie chart
            });
        }
    }


    /* @purpose: plot pie chart for type combine
     * @created: 14th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    function plotDataPartCombine(data, id, height, dataToCompare) {
        if (data.length == 0) {
            $(id).html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
            return false;
        } else {
            var newData = [];
            angular.forEach(data, function(k, v) {
                var index = dataToCompare.map(function(d) {
                    return d.key;
                }).indexOf(v);
                if (index != -1) {
                    newData.push({
                        key: dataToCompare[index].UIname,
                        value: k
                    });
                }
            });
            var sum = d3.sum(newData, function(d) {
                return d.value;
            });

            var alertsChartpieoption = { //Piechart option
                container: id,
                colors: colors,
                data: newData,
                height: height,
                txtSize: '10px',
                legendwidth: 40,
                txtColor: '4D6772',
                legendmargintop: 30,
                format: true,
                sum: (sum / newData.length) * $scope.israCardClusterViewVariable.standardDeviationValue
            };
            if(id == '#dataPart'){
            	alertsChartpieoption.fixedColorWithKey = dayPartColorObj;
            }else if(id == '#dayWeek'){
            	alertsChartpieoption.fixedColorWithKey = dayOfWeekColorObj;
            }
          
            
            setTimeout(function() {
                new reusablePie(alertsChartpieoption); //load pie chart
            });
        }
    }

    /* @purpose: plot progress bar chart for type combine
     * @created: 14th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function plotMerchantType(data) {
        var newData = [];
        angular.forEach(data, function(k, v) {
            newData.push({
                key: v,
                customer_count: k
            });
        });
       /* var maxvalue = d3.max(newData, function(d) {
            return d.customer_count;
        });*/
        var sum = d3.sum(newData, function(d) {
            return d.customer_count;
        });
        newData.map(function(d) {
            var dataToHigh = (sum / newData.length) * $scope.israCardClusterViewVariable.standardDeviationValue;
            var highlight;
            if (d.customer_count > dataToHigh || data.length == 1) {
                highlight = true;
            } else {
                highlight = false;
            }
            d.value = (d.customer_count / sum) * 100;
                d.tooltipValue = (d.customer_count / sum) * 100;
                d.highlight = highlight;
                d.percentToShow = (d.customer_count / sum) * 100;
        });
        newData.sort(function(x, y) {
            return d3.descending(x.value, y.value);
        });
        $scope.israCardClusterViewVariable.plotMerchantTypeData = newData;
    }

    /* @purpose: plot pie chart for type combine with value true false
     * @created: 14th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function plotBankingCard(data) {
        if (data.length == 0) {
            $('#card_Type_Pie').html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
            return false;
        } else {
            data.map(function(d) {
                if (d.value == 'true') {
                    d.key = 'Banking';
                    d.value = d.customer_count;
                } else {
                    d.key = 'Non Banking';
                    d.value = d.customer_count;
                }
            });
            var sum = d3.sum(data, function(d) {
                return d.value;
            });

            var alertsChartpieoption = { //Piechart option
                container: '#card_Type_Pie',
                colors: colors,
                data: data,
                height: 130,
                txtSize: '10px',
                legendwidth: 40,
                txtColor: '4D6772',
                legendmargintop: 30,
                format:true,
                fixedColorWithKey:bankingColorObj,
                sum: (sum / data.length) * $scope.israCardClusterViewVariable.standardDeviationValue
            };
            setTimeout(function() {
                new reusablePie(alertsChartpieoption); //load pie chart
            });
        }
    }


    /* @purpose: plot bar
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function plotBarChart(type, data) {
       /* var maxvalue = d3.max(data, function(d) {
            return d.customer_count;
        });*/
        var sum = d3.sum(data, function(d) {
            return d.customer_count;
        });
        data.map(function(d) {
            var dataToHigh = (sum / data.length) * $scope.israCardClusterViewVariable.standardDeviationValue;
            var highlight;
            if (d.customer_count > dataToHigh || data.length ==1) {
                highlight = true;
            } else {
                highlight = false;
            }
            var name = d.start + '-' + d.end;
                d.key = name.toString();
                d.value = (d.customer_count / sum) * 100;
                d.highlight = highlight;
                d.percentToShow = (d.customer_count / sum) * 100;
        });
        var sum = d3.sum(data, function(d) {
            return d.value;
        });
        angular.forEach(data,function(v){
            v.key = v.start == v.end ? v.start : (v.start+'-'+v.end) ; 
        });
        if (type == 'age') {
            $scope.israCardClusterViewVariable.plotAgeData = data;
            if($scope.israCardClusterViewVariable.plotAgeData.length==1){
              	 $scope.hidingOnfilter = false;
              }
        }
    }


    /* @purpose: Deviation range change
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    function deviationChange(value) {
        $scope.israCardClusterViewVariable.standardDeviationValue = value;
    }

    /* @purpose:Debouncing 
     * @created: 20th September 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    
    function debounce(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) {func.apply(context, args);}
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow){ func.apply(context, args);}
    	};
    }

    
    $('#significant').on('change',debounce(function() {
    	onSignificantvalueChange();
	}, 1000));
    
    $('#customerEleId').on('keyup',debounce(function(event) {
    	changeCustomerId(event.target.value);
	}, 1000));
    
    
    
    /*
     * @purpose: Pagination for customer list
     * @created: 3rd December 2018
     * @params: page number
     * @author: varsha
     */

    
    function changeTablePageNo(pageNum) {
        var customerfilterParam = jQuery.extend(true, {}, filterParam);
        customerfilterParam.total_pages = Math.round($scope.israCardClusterViewVariable.clusterDetail.cluster_user_count / totalCustomerValue);
        customerfilterParam.page_number_required = (pageNum - 1);

        // if (pageNum > 1) {
        //     customerfilterParam.results_from = (pageNum - 1) * totalCustomerValue;
        // } else {
        //     customerfilterParam.results_from = (pageNum - 1);
        // }
        // var diff = $scope.israCardClusterViewVariable.clusterDetail.cluster_user_count - ((pageNum - 1) * totalCustomerValue);
        // if (diff < totalCustomerValue) {
        //     customerfilterParam.results_count = diff;
        // } else {
        //     customerfilterParam.results_count = totalCustomerValue;
        // }
        getCustomerDetail(clusterId, customerfilterParam);
    }
    
    
    
    /* @purpose:On chane of customerId
     * @created: 3rd December 2018
     * @params: customerId
     * @return: success, error functions
     * @author: varsha
     */

    
    function changeCustomerId(customerId){
    	$scope.customerListPreloader = true;
    	if(customerId!=""){
    		var index = originalCustomerData.map(function(d){
        		return d.customer_id;
        	}).indexOf(customerId);
        	if(index!=-1){
        		var obj=[];
        		obj.push(originalCustomerData[index]);
        		 $scope.$apply(function() {
        			 $scope.customerListPreloader = false;
        			 $scope.israCardClusterViewVariable.customerData = obj;
                 });
        	}else{
        		getsearchCustomer(customerId);
        	}
    	}else{
    		$scope.$apply(function() {
    			$scope.customerListPreloader = false;
    			$scope.israCardClusterViewVariable.customerData = originalCustomerData;
            });
    	}
    	
    }

    

	  /* @purpose: On change of significant indicator
	     * @created: 2nd july 2018
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: varsha
	     */
   
	 function onSignificantvalueChange(){
        plotPiechart(jQuery.extend(true,[],genderData), '#gender_Pie');
        plotBarChart('age',  jQuery.extend(true,[],ageData));
        plotFixedAsBsScore(jQuery.extend(true,[],bs_scoreData), '#bs_score', 130);
        plotFixedAsBsScore(jQuery.extend(true,[],as_scoreData), '#as_score', 130);
        plotBankingCard(jQuery.extend(true,[],bankingData));
    	plotDataPartCombine( jQuery.extend(true,{},dayPartData), '#dataPart', 130, dayPartKeysUImodification);
        plotDataPartCombine( jQuery.extend(true,{},dayWeekData), '#dayWeek', 130, dayOfWeekKeysUImodification);
    	plotMerchantType(jQuery.extend(true,{},merchantData));
   }
	 
   $(window).resize(function(){
        plotPiechart(jQuery.extend(true,[],genderData), '#gender_Pie');
        plotBarChart('age',  jQuery.extend(true,[],ageData));
        plotFixedAsBsScore(jQuery.extend(true,[],bs_scoreData), '#bs_score', 130);
        plotFixedAsBsScore(jQuery.extend(true,[],as_scoreData), '#as_score', 130);
        plotBankingCard(jQuery.extend(true,[],bankingData));
        setTimeout(function(){
            plotDataPartCombine( jQuery.extend(true,{},dayPartData), '#dataPart', 130, dayPartKeysUImodification);
        },0);
        plotDataPartCombine( jQuery.extend(true,{},dayWeekData), '#dayWeek', 130, dayOfWeekKeysUImodification);
    	plotMerchantType(jQuery.extend(true,{},merchantData));
   });

    /* @purpose: check attribute to highlight
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    $scope.checkToHighlight = function(type) {
        var status;
        var merchantListList = [
            'cultureandleasure',
            'communication',
            'insurance',
            'fuel',
            'apparel',
            'furniture',
            'electronics',
            'food',
            'infraandgovernmental',
            'medicalandcosmetic',
            'pharma',
            'transportation',
            'tourism',
            'marketplaceanddutyfree',
            'giftsandjewleries',
            'others'
        ];
        if ($scope.israCardClusterViewVariable.clusterDetail.clustering_attributes) {
            $scope.israCardClusterViewVariable.clusterDetail.clustering_attributes.map(function(d) {
                if (d.attribute_name == type) {
                    status = true;
                } else if (type == 'day' && d.attribute_name.substring(0, 3) == 'day') {
                    status = true;
                } else if (type == 'dp' && d.attribute_name.substring(0, 2) == 'dp') {
                    status = true;
                } else if (type == 'Merchant') {
                    if (merchantListList.indexOf(d.attribute_name) >= 0) {
                        status = true;
                    }

                }
            });
        }
        return status;
    };

    /* @purpose: plot pie with fixed as_score and bs_score data
     * @created: 16th august 2018
     * @author: varsha
     */

    function plotFixedAsBsScore(data, id, height) {
        if (data.length == 0) {
            $(id).html("<div class='leadGenErrorDiv no-data-wrapper text-center'> <span>Data Not Found</span></div> ");
            return false;
        } else {
            for (var i = 0; i < 11; i++) {
                var index = data.map(function(d) {
                    return d.start;
                }).indexOf(i);
                if (index == -1) {
                    data.push({
                        'start': i,
                        'end': i,
                        'customer_count': 0
                    });
                }
            }
            data.sort(function(x, y) {
                return d3.ascending(x.start, y.start);
            });
            var manipulatedData = [];
            manipulatedData.push({
                'key': '0',
                'start': 0,
                'end': 0,
                'color':'#1C6EC1',
                'value': data[0].customer_count
            });
            manipulatedData.push({
                'key': '1',
                'start': 1,
                'end': 1,
                'color':'#CB493D',
                'value': data[1].customer_count
            });
            manipulatedData.push({
                'key': '2-3',
                'start': 2,
                'end': 3,
                'color':'#ADE3AC',
                'value': data[2].customer_count + data[3].customer_count
            });
            manipulatedData.push({
                'key': '4-5',
                'start': 4,
                'end': 5,
                'color':'#55D2E1',
                'value': data[4].customer_count + data[5].customer_count
            });
            manipulatedData.push({
                'key': '6-8',
                'start': 6,
                'end': 8,
                'color':'#F36030',
                'value': data[6].customer_count + data[7].customer_count + data[8].customer_count
            });
            manipulatedData.push({
                'key': '9-10',
                'start': 9,
                'end': 10,
                'color':'#CB3E4B',
                'value': data[9].customer_count + data[10].customer_count
            });

            
            if (id == '#bs_score') {
                $scope.israCardClusterViewVariable.bsScoreLegend = manipulatedData;
            } else if (id == '#as_score') {
                $scope.israCardClusterViewVariable.asScoreLegend = manipulatedData;
            } 
            
            var count = 0;
            var pieObj = [];
            var sum = d3.sum(manipulatedData, function(d) {
            	if(d.value>0){
            		count++;
            		pieObj.push(d);
            	}
            	return d.value;
            });
           
            
            var alertsChartpieoption = { //Piechart option
                container: id,
                colors: colors,
                data: pieObj,
                height: height,
                txtSize: '10px',
                legendwidth: 40,
                txtColor: '4D6772',
                legendmargintop: 30,
                format:true,
                dataLength:count,
                sum: (sum / count) * $scope.israCardClusterViewVariable.standardDeviationValue
            };
            new reusablePie(alertsChartpieoption); //load pie chart
        }
    }


    //-----------------------Filter implementation----------------------------------------------
    /* @purpose: Filters for pie legends
     * @created: 14th dec 2018
     * @params: id, data, currentData
     * @author: swathi
     */
    $scope.applyPieLegendFilters = function(id, data, currentData) {
    	var arcData = {};
    	if(id == "#as_score" || id == "#bs_score"){
    		arcData["data"] = {
    			"value": currentData.value, 
    			"customer_count": currentData.value, 
    			"key": currentData.key,
    			"color": currentData.color,
    			"start": currentData.start,
    			"end": currentData.end
    		};
    	}else{
    		arcData["data"] = {
    			"value": currentData.value, 
    			"customer_count": currentData.value, 
    			"key": currentData.name,
    			"color": currentData.color
    		};
    	}
    	window.pieAndWorldOnClick(id, data, arcData);
    };

    /* @purpose: Filter for pie
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    window.pieAndWorldOnClick = function(id, data, arcData) {
        if (id == '#gender_Pie') {
            var gender_stringList = [];
            gender_stringList.push(arcData.data.key);
            var index3 = filterParam.string_filters.map(function(d) {
                return d.attribute_name;
            }).indexOf('gender');
            if (index3 == -1) {
                filterParam.string_filters.push({
                    "attribute_name": "gender",
                    "values": gender_stringList
                });
            } else {
                filterParam.string_filters[index3].values = gender_stringList;
            }
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        } else if (id == '#bs_score') {
            checkForPie('bs_Score', arcData);
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        } else if (id == '#as_score') {
            checkForPie('as_Score', arcData);
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        } else if (id == '#card_Type_Pie') {
            var bankingCard_stringList = [];
            if (arcData.data.key == 'Non Banking') {
                bankingCard_stringList.push('false');
            } else {
                bankingCard_stringList.push('true');
            }
            var index8 = filterParam.string_filters.map(function(d) {
                return d.attribute_name;
            }).indexOf('banking_card');
            if (index8 == -1) {
                filterParam.string_filters.push({
                    "attribute_name": "banking_card",
                    "values": bankingCard_stringList
                });
            } else {
                filterParam.string_filters[index8].values = bankingCard_stringList;
            }
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        } else if (id == '#dataPart') {
            var dataPart_stringList = [];
            var indexDayPart = dayPartKeysUImodification.map(function(d) {
                return d.UIname;
            }).indexOf(arcData.data.key);
            dataPart_stringList.push(dayPartKeysUImodification[indexDayPart].key);
            var index9 = filterParam.combine_filters.map(function(d) {
                return d.attribute_name;
            }).indexOf("Day Part");
            if (index9 == -1) {
                filterParam.combine_filters.push({
                    "attribute_name": "Day Part",
                    "values": dataPart_stringList
                });
            } else {
                filterParam.combine_filters[index9].values = dataPart_stringList;
            }
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        } else if (id == '#dayWeek') {
            var dayWeek_stringList = [];
            var indexDayofWeek = dayOfWeekKeysUImodification.map(function(d) {
                return d.UIname;
            }).indexOf(arcData.data.key);
            dayWeek_stringList.push(dayOfWeekKeysUImodification[indexDayofWeek].key);
            var index10 = filterParam.combine_filters.map(function(d) {
                return d.attribute_name;
            }).indexOf("DAY OF THE WEEK");
            if (index10 == -1) {
                filterParam.combine_filters.push({
                    "attribute_name": "DAY OF THE WEEK",
                    "values": dayWeek_stringList
                });
            } else {
                filterParam.combine_filters[index10].values = dayWeek_stringList;
            }
            $scope.fullpageloader = true;
            CusterApi();
            setFilter();
        }

    };



    /* @purpose: Filter for pie 
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function checkForPie(type, arcData) {
        var index2 = filterParam.integer_filters.map(function(d) {
            return d.attribute_name;
        }).indexOf(type);
        if (index2 == -1) {
            filterParam.integer_filters.push({
                "attribute_name": type,
                "start": arcData.data.start,
                "end": arcData.data.end
            });
        } else {
            filterParam.integer_filters[index2].start = arcData.data.start;
            filterParam.integer_filters[index2].end = arcData.data.end;
        }
    }

    /* @purpose: Filter for Progress bar
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function progressBarFilter(type, value) {
        var index2 = filterParam.integer_filters.map(function(d) {
            return d.attribute_name;
        }).indexOf(type);
        if (index2 == -1) {
            filterParam.integer_filters.push({
                "attribute_name": type,
                "start": value.start,
                "end": value.end
            });
        } else {
            filterParam.integer_filters[index2].start = value.start;
            filterParam.integer_filters[index2].end = value.end;
        }
        $scope.fullpageloader = true;
        $scope.hidingOnfilter = false;
        CusterApi();
        setFilter();
    }



    /* @purpose: Filter array
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    $scope.israCardClusterViewVariable.addedFilter = [];

    function setFilter() {
        $scope.israCardClusterViewVariable.addedFilter = [];
        filterParam.integer_filters.map(function(d) {
            if(d.attribute_name == 'age' || d.attribute_name == 'bs_Score' || d.attribute_name == 'as_Score')
            {
                var val = d.start == d.end ? d.start : (d.start+'-'+d.end) ;
            }
            else
            {
                var val = d.start + '-' + d.end;
            }

            $scope.israCardClusterViewVariable.addedFilter.push({
                'showName': d.attribute_name.split("_").join(" ") + '-->' + val,
                'attribute_name': d.attribute_name,
                'start': d.start,
                'end': d.end,
                'type': 'integer_filters'
            });
        });
        filterParam.string_filters.map(function(d) {
            if (d.attribute_name == 'banking_card') {
                if (d.values[0] == 'true') {
                    $scope.israCardClusterViewVariable.addedFilter.push({
                        'showName': d.attribute_name.split("_").join(" ") + '--> BANKING',
                        'attribute_name': d.attribute_name,
                        'values': d.values,
                        'type': 'string_filters'
                    });
                } else {
                    $scope.israCardClusterViewVariable.addedFilter.push({
                        'showName': d.attribute_name.split("_").join(" ") + '-->NON BANKING',
                        'attribute_name': d.attribute_name,
                        'values': d.values,
                        'type': 'string_filters'
                    });
                }

            } else {
                $scope.israCardClusterViewVariable.addedFilter.push({
                    'showName': d.attribute_name.split("_").join(" ") + '-->' + d.values[0],
                    'attribute_name': d.attribute_name,
                    'values': d.values,
                    'type': 'string_filters'
                });
            }

        });
        filterParam.combine_filters.map(function(d) {
            if (d.attribute_name == "DAY OF THE WEEK") {
                var indexDayofWeek = dayOfWeekKeysUImodification.map(function(d) {
                    return d.key;
                }).indexOf(d.values[0]);
                $scope.israCardClusterViewVariable.addedFilter.push({
                    'showName': d.attribute_name + '-->' + dayOfWeekKeysUImodification[indexDayofWeek].UIname,
                    'attribute_name': d.attribute_name,
                    'values': d.values,
                    'type': 'combine_filters'
                });
            } else if (d.attribute_name == "Day Part") {
                var indexDayPart = dayPartKeysUImodification.map(function(d) {
                    return d.key;
                }).indexOf(d.values[0]);
                $scope.israCardClusterViewVariable.addedFilter.push({
                    'showName': d.attribute_name + '-->' + dayPartKeysUImodification[indexDayPart].UIname,
                    'attribute_name': d.attribute_name,
                    'values': d.values,
                    'type': 'combine_filters'
                });
            }

        });
      
    }



    /* @purpose: Remove filter
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function removeTab(k, data) {
        $scope.israCardClusterViewVariable.addedFilter.splice(k, 1);
        var index = filterParam[data.type].map(function(d) {
            return d.attribute_name;
        }).indexOf(data.attribute_name);
        filterParam[data.type].splice(index, 1);
        $scope.fullpageloader = true;
        //Hiding and showing % in age tooltip
        if(filterParam && filterParam.integer_filters && filterParam.integer_filters.filter(function(e) { return e.attribute_name === 'age'; }).length > 0){
            $scope.hidingOnfilter = false;
        }
        else if($scope.israCardClusterViewVariable.plotAgeData.length==1){
        	 $scope.hidingOnfilter = false;
        }else{
            $scope.hidingOnfilter = true;
        }
        CusterApi();
    }

    //------------------------End of filter implementation------------------------

    /* @purpose: Pie legend data
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    window.reusablePieLegendData = function(pieLegendData, containerid) {
        if (containerid == '#dayWeek') {
            $scope.$apply(function() {
                $scope.israCardClusterViewVariable.dayOfLegend = pieLegendData;
            });
        } else if (containerid == '#gender_Pie') {
            $scope.$apply(function() {
                $scope.israCardClusterViewVariable.genderLegend = pieLegendData;
            });
        } else if (containerid == '#card_Type_Pie') {
            $scope.$apply(function() {
                $scope.israCardClusterViewVariable.bankingLegend = pieLegendData;
            });

        } else if (containerid == '#dataPart') {
            $scope.$apply(function() {
                $scope.israCardClusterViewVariable.dataPartLegend = pieLegendData;
            });
        }


    };
    addScoll('#progressbar', 120);

    /* @purpose: Scroll function
     * @created: 2nd july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function addScoll(id, height) {
        $(document).ready(function() {
            $(id).mCustomScrollbar({
                axis: "X",
                theme: "minimal"
            }, function() {});
            $(id).css('height', height + 'px');
        });
    }



}