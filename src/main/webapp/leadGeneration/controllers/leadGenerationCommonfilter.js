'use strict';
angular.module('leadGenerationApp')
    .controller('LeadGenerationCommonfilterController', leadGenerationCommonfilterController);

    leadGenerationCommonfilterController.$inject = [
    '$scope',
    '$state',
    'LeadGenerationGraphService',
    'LeadGenerationApiService',
    '$rootScope',
    '$stateParams',
    '$q',
    'israDiamondnameConst',
    '$timeout'
];

function leadGenerationCommonfilterController(
    $scope,
    $state,
    LeadGenerationGraphService,
    LeadGenerationApiService,
    $rootScope,
    $stateParams,
    $q,
    israDiamondnameConst,
    $timeout
) {
    var params = {
        token: $rootScope.ehubObject.token
    };
    $scope.hidingOnfilter = true;
    var gender_Type_data = [];
    var gender_url;
    var age_url;
    var cardtype_url;
    var cardDurartion_url;
    var asbs_url;
    $scope.diamond_name = israDiamondnameConst[1];
    var card_duration_data = [];
    var card_Type_Pie_data = [];
    $scope.installmentsData = [];
    $scope.onlineOnsiteData = [];
    $scope.cardTypeData = [];
    $scope.localAbroadData = [];
    $scope.cardDurationData = [];
    $scope.genderData = [];
    $rootScope.maxSignificantValueChange = 1.5;
	$rootScope.maxSignificantValue = 5;
	
    function removeDuplicates(arr, prop) {
        var obj = {};
        for (var i = 0, len = arr.length; i < len; i++) {
            if (!obj[arr[i][prop]]){ obj[arr[i][prop]] = arr[i];}
        }
        var newArr = [];
        for (var key in obj){ newArr.push(obj[key]);}
        return newArr;
    }
    
    
    var bankingColorObj = [];
    var genderColorObj = [];
    var asScoreColorObj = [];
    var bsScoreColorObj = [];
    var cardDurationColorObj = [];
    var cardDurationClicked;
    var bsScoreClicked;
    var asScoreClicked;
    
    function pieWithColorObj(data,type){
		if(type == 'banking'){
		   	 data.map(function(d,k){
		   		 d.color = colors[k]
		   	 })
		   	 bankingColorObj = data;
		}else if(type == 'gender'){
			data.map(function(d,k){
		   		 d.color = colors[k]
		   	 })
		   	 genderColorObj = data;
		}else if(type == 'card'){
			data.map(function(d,k){
		   		 d.color = colors[k]
		   	 })
		   	 cardDurationColorObj = data;
		}else if(type == '#asScoreChart'){
			data.map(function(d,k){
		   		 d.color = colors[k]
		   	 })
		   	 asScoreColorObj = data;
		}else if(type == '#bsScoreChart'){
			data.map(function(d,k){
		   		 d.color = colors[k]
		   	 })
		   	 bsScoreColorObj = data;
		}
		 
	    

    }
    
    
    var data ={};
    function plotPie(data, id, colors, height, txtSize, txtColor, islegends, legendwidth, legendmargintop, noSplice) {
        var pieData = jQuery.extend(true, [], data);
        pieData = pieData.sort(function (a, b) {
            return b.value - a.value;
        });
        if (!noSplice) {
            pieData.splice(3);
        }
       /* var maxval = d3.max(pieData, function (d) {
            return d.value;
        });*/
        var sum = d3.sum(pieData, function (d) {
            return d.value;
        });

        var piechartoption = { //Piechart option
        		  container: id,
        	        colors: colors,
        	        data: pieData,
        	        height:height,
        	        txtSize: '10px',
        	        legendwidth: 40,
        	        txtColor: '4D6772',
        	        legendmargintop: 30,
        	        format:true,
        	        sum: (sum / data.length) * $rootScope.maxSignificantValueChange
        };
        if(previousCardDur1){
        	previousCardDur1.map(function(d,k){
		   		 d.color = colors[k];
		   	});
        	cardDurationColorObj = previousCardDur1;
        }
       
    	  if(id == '#card_Type_Pie'){
    		  piechartoption.fixedColorWithKey = bankingColorObj;
          }else if(id == '#gender_Pie'){
    		  piechartoption.fixedColorWithKey = genderColorObj;
          }else if(id == '#card_Duration_Pie'){
			 piechartoption.fixedColorWithKey = cardDurationColorObj;
          }
        new reusablePie(piechartoption);
    }

 
 
  function fetchGenderData(status) {
    	gender_Type_data = [];
        if($state.current.name === 'dualCardHolderDetails'){
            gender_url ='leadGeneration/uc2/2_3/gender_distribution/';
        }
        $scope.genderTypePreloader = true;
        LeadGenerationApiService.getGenderDetails($scope.clusterId, params,gender_url,filterUC2Object).then(function (response) {
            $scope.genderData = response.data.data;
            $scope.genderData.map(function (v) {
                gender_Type_data.push({
                    "key": v.gender,
                    "value": v.customer_count
                });
            });

            if(status){
            	pieWithColorObj(gender_Type_data,'gender')
            }
            
            plotPie(gender_Type_data, '#gender_Pie', colors, 75); /*  data,color,height*/
        }, function () {
            $scope.genderData = [];
        });
    }

  
		 /* 
     * purpose: get age details data from the api
     * created: 5 April 2018
     * return: No
     * author: Ram 
     */
    $scope.ageData = [];
    var ageDataForProgressBar = {};
  function getAge() {
        if($state.current.name === 'dualCardHolderDetails'){
            age_url ='leadGeneration/uc2/2_3/age_distribution/';
        }
        ageDataForProgressBar = {};
        $scope.agePreloader = true;
        var buckets = 4;
        if(Object.keys(filterUC2Object.distribution_filter.age_range).length > 0){
        	buckets = 1;
        }
        LeadGenerationApiService.getAgeDetails($scope.clusterId, buckets, params,age_url,filterUC2Object).then(function(response) {
        	ageDataForProgressBar = response;
        	loadAgeProgressBar(response);
        	
        }, function() {
            $scope.agePreloader = false;
            $scope.ageData = [];
        });
    }

    function loadAgeProgressBar(response){
    	 $scope.agePreloader = false;
         $scope.ageData = response.data.data;
        
         $scope.sumCustmrCount = d3.sum($scope.ageData, function(g) {
             return g.customer_count;
         });
        /* var maxCustmrCount = d3.max($scope.ageData, function(g) {
             return g.customer_count;
         });*/
         $scope.ageData.map(function(v) {
         	  var dataToHigh = ($scope.sumCustmrCount / $scope.ageData.length) * $rootScope.maxSignificantValueChange;
               var highlight;
               if (v.customer_count > dataToHigh || data.length == 1) {
                   highlight = true;
               } else {
                   highlight = false;
               }
             v.progress = ((v.customer_count/$scope.sumCustmrCount)*100).toFixed(2);
             v.highlight=highlight;
         });
         $scope.ageData.sort(function(a,b){
         	return parseInt(b.progress) - parseInt(a.progress);
         });
    }
   
    /* 
     * purpose: get Risk level data for as/bs from the api
     * created: 19th mar 2018
     * return: No
     * author: zameer
     */
    
    var asScoreData = [];
    var bsScoreData = [];
    var previousAsScoreData = [], previousAsFilterObj = {};
    var previousBsScoreData = [], previousBsFilterObj = {};
    
    function fetchRiskLevel(status, previousAs, previousBs) {
        if($state.current.name === 'dualCardHolderDetails'){
            asbs_url ='leadGeneration/uc2/2_3/as_bs_distribution/';
        }

        var asBuckets = 3, bsBuckets = 3;
        if(Object.keys(filterUC2Object.distribution_filter.as_range).length > 0){
        	asBuckets = 1;
        }
        if(Object.keys(filterUC2Object.distribution_filter.bs_range).length > 0){
        	bsBuckets = 1;
        }
        $scope.riskPreloader = true;
        LeadGenerationApiService.getRiskLevels($scope.clusterId, params,asbs_url,filterUC2Object, asBuckets, bsBuckets).then(function(response) {
        	asScoreData = response.data.data.as_ranges;
        	bsScoreData = response.data.data.bs_ranges;
        	$scope.riskPreloader = false;
        	if((Object.keys(previousAsFilterObj).length == 0 && Object.keys(filterUC2Object.distribution_filter.as_range).length == 0) || (JSON.stringify(previousAsFilterObj) !== JSON.stringify(filterUC2Object.distribution_filter.as_range))){
        		previousAsScoreData = asScoreData;
        	}
        	if((Object.keys(previousBsFilterObj).length == 0 && Object.keys(filterUC2Object.distribution_filter.bs_range).length == 0) || (JSON.stringify(previousBsFilterObj) !== JSON.stringify(filterUC2Object.distribution_filter.bs_range))){
        		previousBsScoreData = bsScoreData;
        	}
        	previousAsFilterObj = filterUC2Object.distribution_filter.as_range;
        	previousBsFilterObj = filterUC2Object.distribution_filter.bs_range;
            
            plotAsBsScore(response.data.data.as_ranges,'#asScoreChart',status, previousAs);
            plotAsBsScore(response.data.data.bs_ranges,'#bsScoreChart',status, previousBs);
        }, function() {
            $scope.riskPreloader = false;
        });
    }
    
	var colors = ['#00B9C9','#37B761','#FFA238','#FEE14D','#FCFFA8','#FF3143','#F2626B','#FFAFCD','#EEC2EB','#FF71FD','#BA8BDB','#9236C6','#5370FD','#5CAAF9','#C9B7B1','#A17F74','#5d4037','#775045','#a58074','#757575','#546e7a','#b0bec5','#a8a771','#968800','#6b6b3c','#9b5f0e','#86490c','#ff7400','#f7a248','#fdbd39','#fdd758','#fff6a3','#eeff41','#c9d200','#84a100','#346500','#00711f','#5dab60','#75b788','#a5d6a7','#80cbc4','#4db6ac','#00cab1','#00897b','#00695c','#006064','#008297','#00baca','#80deea','#3eb6ff','#488dfa','#4654a6','#1f3397','#5e35b1','#8661c6','#b89be0','#d8a8e0','#ff93ff','#9c27b0','#9c2162','#c64892','#ffb8d1','#e57373','#ef5350','#c40100'];
  //  var colors = ['#1C6EC1', '#72BE74', '#ADE3AC', '#55D2E1', '#F36030', '#A072FC', '#CB3E4B', '#CB8B3D', '#2B2F26', '#E66C40', '#00C4AB', '#CBA03D', '#76CB3D', '#CB493D', '#70685F']
	function plotAsBsScore(data,id,status, previousAsBs){
    	 if (data.length == 0) {
             $(id).html("<div class='leadGenErrorDiv'> <span>Data Not Found</span></div> ");
             return false;
         } else {
             data.map(function(d) {
            	 var name = d.start + '-' + d.end;
                 d.key = name.toString();
                     d.value = d.customer_count;
                     d.start = d.start;
                     d.end = d.end;
             });
             var sum = d3.sum(data, function(d) {
                 return d.value;
             });

             if(status){
             	pieWithColorObj(data,id)
             }
             if(id =='#bsScoreChart' && asScoreClicked){
            	 pieWithColorObj(data,id);
            	 asScoreClicked = false;
             }
             if(id =='#asScoreChart' && bsScoreClicked){
            	 pieWithColorObj(data,id);
            	 bsScoreClicked = false;
             }
             
             if(cardDurationClicked){
            	 pieWithColorObj(data,id);
            	 if(id =='#bsScoreChart'){
            		 cardDurationClicked = false;
            	 }
            	 
             }
             
             
             
             angular.forEach(data,function(v){
                v.key = v.start == v.end ? v.start : (v.start+'-'+v.end) ; 
            });
             var alertsChartpieoption = { //Piechart option
                 container: id,
                 colors: colors,
                 data: data,
                 height:80,
                 txtSize: '10px',
                 legendwidth: 40,
                 txtColor: '4D6772',
                 legendmargintop: 30,
                 format:true,
                 sum: (sum / data.length) * $rootScope.maxSignificantValueChange
             };
             if(previousAsBs){
            	 previousAsBs.map(function(d,k){
    		   		 d.color = colors[k];
    		   	 });
             }
             if(id == '#asScoreChart'){
            	 if(status){
            		 alertsChartpieoption.fixedColorWithKey = asScoreColorObj;
            	 }else{
            		 alertsChartpieoption.fixedColorWithKey = previousAsBs;
            	 }
             }else if(id == '#bsScoreChart'){
            	 if(status){
            		 alertsChartpieoption.fixedColorWithKey = bsScoreColorObj;
            	 }else{
            		 alertsChartpieoption.fixedColorWithKey = previousAsBs;
            	 }
             }
           
             setTimeout(function() {
                 new reusablePie(alertsChartpieoption); //load pie chart
             });
         }
    }
      /* 
     * purpose: get card duration data from the api
     * created: 21st mar 2018
     * return: No
     * author: zameer
     */
	var previousCardDurationData = [], previousCardDurFilterObj = {}, previousCardDur1;
    function fetchCardDurationDetails(status, previousCardDur) {
    	var cardDurationData = [];
    	card_duration_data = [];
    	previousCardDur1 = previousCardDur;
        if($state.current.name === 'dualCardHolderDetails'){
            cardDurartion_url ='leadGeneration/uc2/2_3/card_duration_distribution/';
        }
        var buckets = 3;
        if(Object.keys(filterUC2Object.distribution_filter.card_duration).length > 0){
        	buckets = 1;
        }
        $scope.cardDurationPreloader = true;
        LeadGenerationApiService.getCardDurationDetails($scope.clusterId, params,cardDurartion_url,filterUC2Object, buckets).then(function(response) {
        	$scope.cardDurationPreloader = false;
            cardDurationData = response.data.data;
//            cardDurationData.sort(function(a,b){
//            	return parseInt(a.start) - parseInt(b.start);
//            });
            cardDurationData.map(function(v) {
                var val = v.start == v.end ? (v.start+'(Duration)') : (v.start+'-'+v.end+'(Duration)') ;
                card_duration_data.push({
                    "key": val,
                    "value": v.customer_count,
                    "start":v.start,
                    "end":v.end
                });
            });
            if((Object.keys(previousCardDurFilterObj).length == 0 && Object.keys(filterUC2Object.distribution_filter.card_duration).length == 0) || (JSON.stringify(previousCardDurFilterObj) !== JSON.stringify(filterUC2Object.distribution_filter.card_duration))){
            	previousCardDurationData = card_duration_data;
           }
            previousCardDurFilterObj = filterUC2Object.distribution_filter.card_duration;
            if(status){
            	pieWithColorObj(card_duration_data,'card')
            }
            
            if(asScoreClicked){
            	pieWithColorObj(card_duration_data,'card');
            }
            
            if(bsScoreClicked){
            	pieWithColorObj(card_duration_data,'card');
            }
            plotPie(card_duration_data, '#card_Duration_Pie', colors, 80); /*  data,color,height*/
        }, function() {
            $scope.cardDurationPreloader = false;
            $scope.cardDurationData = [];
        });
    }
    
   
 
    /* 
     * purpose: get card type data from the api
     * created: 19th mar 2018
     * return: No
     * author: zameer
     * @modifiedBy:Varsha
     */

     function fetchCardTypeData(status) {
    	 var cardTypeData = [];
    	 card_Type_Pie_data = [];
        if($state.current.name === 'dualCardHolderDetails'){
            cardtype_url ='leadGeneration/uc2/2_3/banking_card_distribution/';
        }
        $scope.cardTypePreloader = true;
        LeadGenerationApiService.getCardTypeData($scope.clusterId, params,cardtype_url,filterUC2Object).then(function(response) {
            $scope.cardTypePreloader = false;
            cardTypeData = response.data.data;
            cardTypeData.map(function(v) {
                var key;
                if (v.card_distribution == 'true') {
                    key = 'Banking';
                } else if (v.card_distribution == 'false') {
                    key = "Non Banking";
                }
                card_Type_Pie_data.push({
                    "key": key,
                    "value": v.customer_count,
                    "originalKey":v.card_distribution
                });
            });
            if(status){
            	pieWithColorObj(card_Type_Pie_data,'banking')
            }
            plotPie(card_Type_Pie_data, '#card_Type_Pie', colors, 80); /*  data,color,height*/
        }, function() {
            $scope.cardTypePreloader = false;
            $scope.cardTypeData = [];
        });
    }
    
    
    /* 
	* purpose: Call all api at one
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
    var filterUC2Object = {
			  "distribution_filter": {
				    "as_range": {},
				    "bs_range": {},
				    "age_range": {},
				    "card_duration":{},
				    "banking_card":[],
				    "gender": []
				  }
				};

	apiCall(true);
	function apiCall(status){
		$('#sideBarForChordClick').css('display','none');
		$('#ribbonInfo').css('display','none');
		if(status){
			fetchCardDurationDetails(status);
		}else{
			fetchCardDurationDetails(status, previousCardDurationData);
		}
		
	    fetchCardTypeData(status);
	    getAge(status);
	    fetchGenderData(status);
	    if(!status){
	    	fetchRiskLevel(status, previousAsScoreData, previousBsScoreData);
	    }else{
	    	 fetchRiskLevel(status);
	    }
	    window.filterChordChartWithFilter(filterUC2Object);
	}
	
	
    
	 /* 
* purpose: Filter implementation
* created: 29th August 2018
* return: No
* author: Varsha 
*/
 
    //----------------------------FILTER IMPLEMENTATION START--------------------------------------
  
	$scope.legendClickForPie = function(container,data,d){
		window.pieAndWorldOnClick(container,data,d);
	}
    
    /* 
	* purpose: Set object in
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
    
    $scope.filterObjectForUI = [];
	window.pieAndWorldOnClick = function(container,data,d){
		
		if(container == '#card_Duration_Pie'){
			cardDurationClicked = true;
			setFilterForApiAndUI('card_duration','CARD DURATION',d);
		}else if(container == '#card_Type_Pie'){
			filterUC2Object.distribution_filter.banking_card = [];
			filterUC2Object.distribution_filter.banking_card.push(d.data.originalKey);
			var index = $scope.filterObjectForUI.map(function(d){
				return d.name;
			}).indexOf('CARD TYPE');
			if(index == -1){
				$scope.filterObjectForUI.push({
					'name':'CARD TYPE',
					'value':'('+d.data.key+')',
					'key':'banking_card'
				});
			}else{
				$scope.filterObjectForUI[index].value = '('+d.data.key+')';
			}
			apiCall();
		}else if(container == '#asScoreChart'){
			asScoreClicked = true;
			setFilterForApiAndUI('as_range','AS SCORE',d);
		}else if(container == '#bsScoreChart'){
			bsScoreClicked = true;
			setFilterForApiAndUI('bs_range','BS SCORE',d);
		}else if(container == '#gender_Pie'){
			filterUC2Object.distribution_filter.gender = [];
			filterUC2Object.distribution_filter.gender.push(d.data.key);
			var index = $scope.filterObjectForUI.map(function(d){
				return d.name;
			}).indexOf('GENDER');
			if(index == -1){
				$scope.filterObjectForUI.push({
					'name':'GENDER',
					'value':'('+d.data.key+')',
					'key':'gender'
				});
			}else{
				$scope.filterObjectForUI[index].value = '('+d.data.key+')';
			}
			apiCall();
		}
		//$scope.$apply(function() {
	    	 $scope.filterObjectForUI = $scope.filterObjectForUI;
	     //});
	//	console.log(filterUC2Object,'filterUC2Object',$scope.filterObjectForUI)
	};
	
	 /* 
	* purpose: Set object for progress bar
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
	
	$scope.progressChartClick = function(data){
        $scope.hidingOnfilter = false;
		var newData ={'data':data};
		setFilterForApiAndUI('age_range','AGE',newData);
	};
	
	 /* 
	* purpose: Set filter object
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
	function setFilterForApiAndUI(filter,filterUI,d){
		filterUC2Object.distribution_filter[filter].start = d.data.start;
		filterUC2Object.distribution_filter[filter].end = d.data.end;
		var index = $scope.filterObjectForUI.map(function(d){
			return d.name;
		}).indexOf(filterUI);
		if(index == -1){
            if(filterUI == 'AGE' || filterUI == 'BS SCORE' || filterUI == 'AS SCORE' || filterUI == 'CARD DURATION')
            {
                var val = d.data.start == d.data.end ? ('('+d.data.start+')') : ('('+d.data.start+'-'+d.data.end+')') ;
            }
            else
            {
                var val = '('+d.data.start+'-'+d.data.end+')';
            }
			$scope.filterObjectForUI.push({
				'name':filterUI,
				'value':val,
				'key':filter
			});
		}else{
			$scope.filterObjectForUI[index].value = val;
		}
		apiCall();
	}
	
	
	$scope.$watch('filterObjectForUI', function() {
		setTimeout(function(){
			 $('.remote-filters-wrapper').mThumbnailScroller("scrollTo","-=100");
		},0);
	  });
	 /* 
	* purpose: Remove filter
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
	
	$scope.removeTab = function(name,data){
    
        var index = $scope.filterObjectForUI.map(function(d,i){
			return d.name;
		}).indexOf(name);
		 $scope.filterObjectForUI.splice(index,1);
		 if(data.key == 'banking_card' || data.key == 'gender'){
			 filterUC2Object.distribution_filter[data.key] = [];
		 }else{
			 filterUC2Object.distribution_filter[data.key] = {};
         }
         //Hiding and showing % in age tooltip
         if(filterUC2Object && filterUC2Object.distribution_filter && filterUC2Object.distribution_filter.age_range && Object.keys(filterUC2Object.distribution_filter.age_range).length > 0){
             $scope.hidingOnfilter = false;
         }
         else{
             $scope.hidingOnfilter = true;
         }
		 apiCall(true);
		
	};
	
	//--------------------------- FILTER IMPLEMENTATION ENDS---------------------------
	
	
	 /* 
	* purpose: Set legend for pie
	* created: 29th August 2018
	* return: No
	* author: Varsha 
	*/
	 
	
	window.reusablePieLegendData = function(pieLegendData, containerid){
		var pieLegendData = removeDuplicates(pieLegendData,'name');
		if(containerid == '#asScoreChart')
		{
			$scope.asScoreLegendData = [];
			$timeout(function(){
				$scope.asScoreLegendData = pieLegendData;
			}, 0);
		}else if(containerid == '#bsScoreChart')
		{
		   $scope.bsScoreLegendData = [];
		   $timeout(function(){
			   $scope.bsScoreLegendData = pieLegendData;
			}, 0);
		}else if(containerid == '#card_Duration_Pie'){
			$scope.card_Duration_PieLegendData = [];
			$scope.card_Duration_PieLegendData = pieLegendData;
		}else if(containerid == '#card_Type_Pie'){
			$scope.card_Type_PieLegendData = [];
			$scope.card_Type_PieLegendData = pieLegendData;
		}else if(containerid == '#gender_Pie'){
			$scope.gender_PieLegendData = [];
			$scope.gender_PieLegendData = pieLegendData;
		}
	};
 
	
	$rootScope.setSignificantIndicator = function(maxSignificantValueChange){
		$rootScope.maxSignificantValueChange = maxSignificantValueChange;
		
		plotAsBsScore(asScoreData,'#asScoreChart');
        plotAsBsScore(bsScoreData,'#bsScoreChart');
        plotPie(gender_Type_data, '#gender_Pie', colors, 80);
        plotPie(card_duration_data, '#card_Duration_Pie', colors, 80);
        plotPie(card_Type_Pie_data, '#card_Type_Pie', colors, 80);
         loadAgeProgressBar(ageDataForProgressBar);
	};
}