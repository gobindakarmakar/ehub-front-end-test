'use strict';
angular.module('leadGenerationApp')
    .controller('LeadGenerationClusterController', leadGenerationClusterController);

leadGenerationClusterController.$inject = [
    '$scope',
    '$state',
    '$rootScope',
    'LeadGenerationApiService',
    '$q',
    '$timeout',
    'HostPathService'
];

function leadGenerationClusterController(
    $scope,
    $state,
    $rootScope,
    LeadGenerationApiService,
    $q,
    $timeout,
    HostPathService
) {

$scope.legendHideShow = true;
$scope.respDataForResize = {};
	try{
		$scope.israCardFunction = {
		        showCluster: showCluster,
		        changeView: changeView,
		        minMaxStory: minMaxStory,
		        resetFilters: resetFilters,
		        detailPge: detailPge,
		        setFavStory: setFavStory,
		        renameClusterName: renameClusterName,
		        renameStoryName: renameStoryName,
		        renameClusterNameKeyUp: renameClusterNameKeyUp,
		        initializeRenameData: initializeRenameData,
		        initializeRenameDataStory: initializeRenameDataStory,
		        renameStoryNameKeyUp: renameStoryNameKeyUp,
		        localizeTableAttributeNameChange: localizeTableAttributeNameChange,
		        updateClusterData:updateClusterData,
		        deleteStory:deleteStory,
		        sayYesorNo:sayYesorNo,
		        createNewStory:createNewStory,
		        selectWorkFlow:selectWorkFlow,
		        openCreateStoryModal:openCreateStoryModal,
		        cancelCreateStory:cancelCreateStory
		    };
		    $scope.israCardVariabe = {
		        storiesList: [],
		        minimize_maximize: true,
		        clusterPreloader: true,
		        clusterList: [],
		        legendData: [],
		        favStoryId: '',
		        showFav: '',
		        showAlertBox:false
		    };
		   
	}catch(err){
		console.log(err.message);
	}
	
	
	 /*
     * @purpose: Dummy function for testing 
     * @created: 10th Oct 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
	
	$scope.grade = function() {
	    var size = $scope.password.length;
	    if (size > 8) {
	      $scope.strength = 'strong';
	    } else if (size > 3) {
	      $scope.strength = 'medium';
	    } else {
	      $scope.strength = 'weak';
	    }
	  };
	
	    $rootScope.showDetailPage = false;
	    $rootScope.showComparisonPageBreadCrumbs = false;
	    $rootScope.showDetailPageActive = false;
	    $rootScope.showComparisonPageBreadCrumbsActive=false;
	  
	 //code for number formatting
    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
        var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };
    

    //$scope.israCardVariabe.clusterPreloader = true;
    $scope.checkedValue = 'business';
    $scope.showBusinessScienceView = true;
    var filterParams = {
        "customer_count_start": 1,
        "avg_expendi_start": 1
    };

    var minMaxExp = [];
    var colors = ['#e4006e', '#328FD7'];
    var colorScale;
    var actualColorscale;
    var color = ['#1C6EC1', '#72BE74', '#ADE3AC', '#55D2E1', '#F36030', '#A072FC', '#CB3E4B', '#CB8B3D', '#2B2F26', '#E66C40', '#00C4AB', '#CBA03D', '#76CB3D', '#CB493D', '#70685F'];
    var baroptions = {
        container: '#barChart',
        height: 180,
        marginRight: 5,
        marginLeft: 25,
        marginBottom: 1,
        marginTop: 1,
        color: color,
        gridColor: '#657F8B',
        //   label:true
    };

    $scope.hideStories = true;
    if(window.localStorage.getItem("cardHolderType") == "Predicting Dual Card Holders"){
    	window.leadGenerationHidesStories = true;
    }
    if(window.localStorage.getItem("cardHolderType") == "General Card Holder"){
    	window.leadGenerationHidesStories = false;
    }
    if (window.leadGenerationHidesStories) {
        $scope.hideStories = !window.leadGenerationHidesStories;
    }
    //-------------------------- API CALL--------------------------------------------------------------		
    /*
     * @purpose: getStories 
     * @created: 25th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    if (!window.leadGenerationHidesStories || window.leadGenerationHidesStories == undefined) {
        getfavouritestoryNew();
    } else {
        Uc3overall_stats();
    }

    //----------------------------------UC1 API-------------------------------------------------------
    var originalStoryList = [];

    function getStories() {
        LeadGenerationApiService.getStories().then(function(response) {
        try{
        	if(response.data.message == "no such index"){
        		 $scope.israCardVariabe.clusterPreloader = false;
        		 return false;
        	}
            $scope.israCardVariabe.storiesList = response.data.data;
            if($scope.israCardVariabe.storiesList.length > 0){
            	$scope.israCardVariabe.storiesList.map(function(d){
            		d.actual_total_user_count = d.total_user_count;
            		d.actual_total_expenditure = d.total_expenditure;
            	});
            }
            originalStoryList = jQuery.extend(true, [], response.data.data);
            if (favStoryId != null) {
                var index = $scope.israCardVariabe.storiesList.map(function(d) {
                    return d.story_id;
                }).indexOf(favStoryId);
                if (index != -1) {
                    var object = [];
                    object = $scope.israCardVariabe.storiesList[index];
                    $scope.israCardVariabe.storiesList.splice(index, 1);
                    $scope.israCardVariabe.storiesList.unshift(object);
                }
            }
            showCluster(0);
            getfavouritestory();
            filterParams.avg_expendi_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
            filterParams.avg_expendi_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
            filterParams.customer_count_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster;
            filterParams.customer_count_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster;
            minMaxExp[0] = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
            minMaxExp[1] = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
            colorScale = d3.scaleLinear().domain(minMaxExp).range(colors);
            actualColorscale = d3.scaleLinear().domain(minMaxExp).range(colors);
            var amountVal = $scope.israCardVariabe.storiesList[$scope.activeIndex].total_expenditure.toString().split('.');
            if (amountVal[0].length <= 8) {
                $scope.activeAmount = ($scope.israCardVariabe.storiesList[$scope.activeIndex].total_expenditure / 1000000).toFixed(2) + 'M';
            } else if (amountVal[1].length >= 9) {
                $scope.activeAmount = ($scope.israCardVariabe.storiesList[$scope.activeIndex].total_expenditure / 1000000000).toFixed(2) + 'B';
            }
	      }catch(err){
	    	  $scope.israCardVariabe.clusterPreloader = false;
	          $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>"+err.message+"</span></div> ");
	       }
        }, function() {
            $scope.israCardVariabe.clusterPreloader = false;
            $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed To Get Data From server</span></div> ");
        });
    }


    $scope.formatTotalAmount = function(amount) {
        if (amount != undefined) {
            var amountVal = amount.toString().split('.');
            var finalAmount;
            if (amountVal[0].length <= 8) {
                finalAmount = (amount / 1000000).toFixed(2) + 'M';
            } else if (amountVal[0].length >= 9) {
                finalAmount = (amount / 1000000000).toFixed(2) + 'B';
            }
            return finalAmount;
        }
    };

    $scope.formatNummerWithComma = function(amount) {
    	 var formatAmount;
         formatAmount = Number(Math.round(amount)).formatAmt();
         return formatAmount.split('.')[0];
    };

    /*
     * @purpose: get clusters
     * @created: 28th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var clusterData = [];
    var originalClusterList = [];

    function getCluster(stroryId, params) {
    	$scope.respDataForResize = {};
        $scope.scienceViewErrorMsg = false;
        LeadGenerationApiService.getCluster(stroryId, params).then(function(response) {
            try{
	        	$scope.israCardVariabe.clusterList = response.data.data;
	            originalClusterList = jQuery.extend(true, [], response.data.data);
	            clusterData = response.data;
	            $scope.israCardVariabe.clusterPreloader = false;
	            if (response.data.data.length == 0) {
                    $scope.legendHideShow = false;
	            	$('#barchartProgress').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
                    $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
                   
                    return false;
	            } else {
                     $('#barchartProgress').html(' ');
                     $scope.respDataForResize = response.data;
	                plotCluster(response.data);
	                setTimeout(function() {
	                    loadScienceViewChart();
                    }, 0);
                    $scope.legendHideShow = true;
	            }
            }catch(err){
                $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>"+err.message+"</span></div> ");
            }

        }, function() {
            $scope.israCardVariabe.clusterPreloader = false;
            $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed To Get Data From server</span></div> ");
        });
    }


    /*
     * @purpose: get fav story Id
     * @created: 11th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    var favStoryId;

    function getfavouritestoryNew() {
        LeadGenerationApiService.getfavouritestory().then(function(response) {
            favStoryId = response.data.storyId;
            getStories();
        }, function() {
        	 $scope.israCardVariabe.clusterPreloader = false;
             $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed To Get Data From server</span></div> ");
        });
    }

    function getfavouritestory() {
        $scope.israCardVariabe.favStoryId = favStoryId;
        $scope.israCardVariabe.showFav = $scope.israCardVariabe.storiesList[$scope.activeIndex].story_id == $scope.israCardVariabe.favStoryId;
       try{
    	   if ($scope.israCardVariabe.showFav) {
	           $('#star').addClass('markedFav');
               $('#star').removeClass('markedUnFav');
               setTimeout(function() {
                   $('#starText').html('Active Story');
               });
           } else {
               $('#star').addClass('markedUnFav');
               $('#star').removeClass('markedFav');
               setTimeout(function() {
                   $('#starText').html('Set as default');
               });
           }
       }catch(err){
    	   console.log(err.message);
       }
       
    }
    /*
     * @purpose: post story Id
     * @created: 11th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    function postFavouriteStory(stroryId, params) {
        LeadGenerationApiService.postFavouriteStory(stroryId, params).then(function() {
        	getfavouritestoryNew();
        }, function() {});
    }
    
    /*
     * @purpose: post story Id
     * @created: 4th september 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function getstageRequest() {
        LeadGenerationApiService.getstageRequest().then(function(response) {
           $scope.workflowData = response.data;
        }, function() {});
    }
    //state as state.workflow_data.name for state in workflowData
    getstageRequest();
    
    /*
     * @purpose: post story Id
     * @created: 4th september 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

      function postNewStoryName(data) {
        LeadGenerationApiService.postNewStoryName(data).then(function() {
           $scope.israCardVariabe.clusterPreloader = true;
           setTimeout(function(){
        	   getfavouritestoryNew();
           },5000);
        }, function() {
        	  $scope.israCardVariabe.clusterPreloader = false;
        });
    }
      
      /*
       * @purpose: get story by name
       * @created: 4th september 2018
       * @params: params(object)
       * @return: success, error functions
       * @author: varsha
       */
       /* function getStoryByName(name) {
          LeadGenerationApiService.getStoryByName(name).then(function() {
          }, function() {});
      }*/
   
        /*
         * @purpose: get story by name
         * @created: 4th september 2018
         * @params: params(object)
         * @return: success, error functions
         * @author: varsha
         */
          function deleteStoryById(storyId,storyName) {
            LeadGenerationApiService.deleteStoryById(storyId,storyName).then(function() {
               $scope.israCardVariabe.clusterPreloader = true;
               setTimeout(function(){
        	   getfavouritestoryNew();
               },5000);
            }, function() {});
        }
     
    
    //----------------------------UC1 API ENDS---------------------------------------------------

    //-----------------------------UC3 API STARTS------------------------------------------------

    /*
     * @purpose: getUc3clusters
     * @created: 1st August 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var clusterData = [];
    var originalClusterList = [];

    function getUc3clusters(params) {
    	$scope.respDataForResize = {};
        LeadGenerationApiService.getUc3clusters(params).then(function(response) {
            try{
            	$scope.israCardVariabe.clusterList = response.data.data;
            	 clusterData = response.data;
            	 if (response.data.data.length == 0) {
                    $scope.israCardVariabe.clusterPreloader = false;
                    $scope.legendHideShow = false;
                    $('#barchartProgress').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
                    $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
                     return false;
                 } else {
                	 $('#barchartProgress').html(' ');
                     originalClusterList = jQuery.extend(true, [], response.data.data);
                     $scope.israCardVariabe.clusterPreloader = false;
                     $scope.respDataForResize = response.data;
                     plotCluster(response.data);
                     setTimeout(function() {
                         loadScienceViewChart();
                     }, 0);
                     $scope.legendHideShow = true;
                     sessionStorage.setItem("storyId", undefined);
                     sessionStorage.setItem("StoryName", '');
                 }
            }catch(err){
               $scope.israCardVariabe.clusterPreloader = false;
         	   console.log(err.message);
            }

        }, function() {
            $scope.israCardVariabe.clusterPreloader = false;
            $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed to Get Data From Server</span></div> ");
             return false;
        });
    }


    /*
     * @purpose: Uc3overall_stats
     * @created: 1st August 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function Uc3overall_stats() {
        LeadGenerationApiService.Uc3overall_stats().then(function(response) {
        	 try{
        		 $scope.activeIndex = 0;
                 $scope.israCardVariabe.storiesList.push({
                     'max_user_Count_cluster': response.data.data.max_user_Count_cluster,
                     'min_user_Count_cluster': response.data.data.min_user_Count_cluster,
                     'max_avg_expenditure_cluster': response.data.data.max_avg_expenditure_cluster,
                     'min_avg_expenditure_cluster': response.data.data.min_avg_expenditure_cluster,
                     'total_user_count': response.data.data.total_user_count,
                     'total_expenditure': response.data.data.total_expenditure,
                     'clustering_attributes': response.data.data.clustering_attributes
                 });
                 filterParams.avg_expendi_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
                 filterParams.avg_expendi_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
                 filterParams.customer_count_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster;
                 filterParams.customer_count_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster;
                 minMaxExp[0] = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
                 minMaxExp[1] = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
                 colorScale = d3.scaleLinear().domain(minMaxExp).range(colors);
                 actualColorscale = colorScale;
                 slider1();
                 slider2();
                 setSliderValue();
                 getUc3clusters({});
            }catch(err){
               $scope.israCardVariabe.clusterPreloader = false;
         	   console.log(err.message);
            }
        }, function() {
            $scope.israCardVariabe.clusterPreloader = false;
            $('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Failed to Get Data From Server</span></div> ");
            return false;
        });
    }

    //---------------------------UC1 AND UC3 API-----------------------------------
    function postsetClusterName(stroryId, clusterId, data) {
        LeadGenerationApiService.postsetClusterName(stroryId, clusterId, data).then(function() {
        }, function() {});
    }

    function postsetStoryName(stroryId, data) {
        LeadGenerationApiService.postsetStoryName(stroryId, data).then(function() {
        }, function() {});
    }


    //---------------------------END OF UC1 AND UC3 API-------------------------------

    //-----------------------------ISRA CARD function starts----------------------------------------


    /*
     * @purpose:on click of any story  showCluster
     * @created: 25th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function showCluster(index) {
    	$scope.respDataForResize = {};
        $scope.activeIndex = index;
        getfavouritestory();
        sessionStorage.setItem("storyId", $scope.israCardVariabe.storiesList[$scope.activeIndex].story_id);
        sessionStorage.setItem("StoryName", $scope.israCardVariabe.storiesList[$scope.activeIndex].story_name);
        var params = {};
        slider1();
        slider2();
        setSliderValue();
        $(".Bubble_Chart_tooltip").remove();
        $scope.israCardVariabe.clusterPreloader = true;
        $scope.checkedValue = 'business';
        $scope.showBusinessScienceView = true;
        filterParams.avg_expendi_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
        filterParams.avg_expendi_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
        filterParams.customer_count_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster;
        filterParams.customer_count_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster;
  
        getCluster($scope.israCardVariabe.storiesList[$scope.activeIndex].story_id, params);

        $(".custom-filters-wrapper .custom-list").mThumbnailScroller({
            axis: "x"
        });
    }

    /*
     * @purpose: set data to plot bubble chart
     * @created: 26th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    var bubbleSocialOptions = {};

    function plotCluster(data) {
        var bubbledata = { //BubbleEntity Data
            name: '',
            children: []
        };
        if (data && data.data && data.data.length > 0) {
            angular.forEach(data.data, function(d) {
                if (d.cluster_avg_expenditure < d.general_avg_expenditure) {
                    bubbledata.children.push({
                        "name": d.cluster_name,
                        "clusterID": d.cluster_id,
                        "size": d.cluster_user_count,
                        "avgExp": d.cluster_avg_expenditure,
                        "totalAmount": d.cluster_expenditure,
                        "avgExpPerCustomerAmongGP": d.general_avg_expenditure,
                        "avgExpenditure": d.cluster_avg_expenditure,
                        "attributes": d.sd_attributes,
                        "customersOutOfIsracardPopulation": d.cluster_user_percentage,
                        "progress1": (d.cluster_avg_expenditure / d.general_avg_expenditure) * 100,
                        "progress2": 100
                    });

                } else {
                    bubbledata.children.push({
                        "name": d.cluster_name,
                        "clusterID": d.cluster_id,
                        "size": d.cluster_user_count,
                        "avgExp": d.cluster_avg_expenditure,
                        "totalAmount": d.cluster_expenditure,
                        "avgExpPerCustomerAmongGP": d.general_avg_expenditure,
                        "avgExpenditure": d.cluster_avg_expenditure,
                        "attributes": d.sd_attributes,
                        "customersOutOfIsracardPopulation": d.cluster_user_percentage,
                        "progress1": 100,
                        "progress2": (d.general_avg_expenditure / d.cluster_avg_expenditure) * 100
                    });
                }
            });
            plotBubbleChart('#israClusterBubbleChart', ['#8F4582'], bubbledata);
        } else {
        	$('#israClusterBubbleChart').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
        }

    }

    /*
     * @purpose: change attribute name to local table
     * @created: 20th August 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function localizeTableAttributeNameChange(name) {
        var index = commonLocalizationTable.map(function(d) {
            return d.key;
        }).indexOf(name.toUpperCase());
        if (index == -1) {
            return name;
        } else if (index != -1) {
            return commonLocalizationTable[index].UIname;
        }
    }

    /*
     * @purpose: plot bubble chart
     * @created: 26th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var commonLocalizationTable = [{
        key: 'DP1M_23_04',
        UIname: '23:00 - 4:00'
    }, {
        key: 'DP2M_04_06',
        UIname: '4:00 - 06:00'
    }, {
        key: 'DP3M_06_12',
        UIname: '6:00 - 12:00'
    }, {
        key: 'DP4M_12_1630',
        UIname: '12:00 - 16:30'
    }, {
        key: 'DP5M_1630_23',
        UIname: '16:30 - 23:00'
    }, {
        key: 'DAY_1_4',
        UIname: 'Sun - Wed'
    }, {
        key: 'DAY_5',
        UIname: 'Thu'
    }, {
        key: 'DAY_6',
        UIname: 'Fri'
    }, {
        key: 'DAY_7',
        UIname: 'Sat'
    }, {
        key: 'GENDER',
        UIname: 'Gender'
    }];
   
    function plotBubbleChart(id, color, data) {
    	$scope.israCardVariabe.storiesList[$scope.activeIndex].actual_total_user_count = format(d3.sum(data.children, function(g) {
			return g.size;
		}));
    	$scope.israCardVariabe.storiesList[$scope.activeIndex].actual_total_expenditure = d3.sum(data.children, function(g) {
			return g.totalAmount;
		});
        getMinMaxForSDchart();
        var bubbleSocialOptions1 = jQuery.extend(true, {}, bubbleSocialOptions);
        bubbleSocialOptions1.container = id;
        bubbleSocialOptions1.bubbleColor = color;
        bubbleSocialOptions1.data = data;
        bubbleSocialOptions1.specialToolTip = true;
        bubbleSocialOptions1.domainArrayForVerticalBarChart = [minStandardDEviationValue, maxStandardDEviationValue];
        bubbleSocialOptions1.detailPage = 'General Card Holder';
        var colorScalenew = d3.scaleLinear().domain([$scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster, $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster]).range([actualColorscale(filterParams.avg_expendi_start), actualColorscale(filterParams.avg_expendi_end)]);
        var bubbleSizeScale = d3.scaleLinear().domain([$scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster, $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster]).range([3, 30]);
        bubbleSocialOptions1.bubbleSizeScale = bubbleSizeScale;
        bubbleSocialOptions1.localTableData = commonLocalizationTable;
        bubbleSocialOptions1.bubbleColorScale = colorScalenew;
        BubbleHierarchyChart(bubbleSocialOptions1);
    }


    /*
     * @purpose: on change view
     * @created: 26th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var activeView;

    function changeView(checkedValue) {
        activeView = checkedValue;
        if (checkedValue == 'business') {
            $scope.showBusinessScienceView = true;
            $scope.respDataForResize = clusterData;
            setTimeout(function() {
                plotCluster(clusterData);
            });

        } else {
            $scope.showBusinessScienceView = false;
            setTimeout(function() {
                loadScienceViewChart();
            });

        }
    }


    /*
     * @purpose: set slider min max value
     * @created: 27th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function setSliderValue() {
        $("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster)).formatAmt().split('.')[0]);
        $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster)).formatAmt().split('.')[0]);
        $("#cluster_range").find(".price-range-min").html(Number(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster)).formatAmt().split('.')[0]);
        $("#cluster_range").find(".price-range-max").html(Number(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster)).formatAmt().split('.')[0]);
    }



    /*
     * @purpose: on expenditure slider change
     * @created: 27th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */


    function filterDataForExpenditure(values,plotChartOnChange) {
        var minMaxExp = [];
        minMaxExp[0] = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
        minMaxExp[1] = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
        var expScale = d3.scaleLinear().domain([0, 100]).range(minMaxExp);
        filterParams.avg_expendi_start = expScale(values[0]);
        filterParams.avg_expendi_end = expScale(values[1]);
        if(plotChartOnChange != "onChange"){
	        if (!window.leadGenerationHidesStories || window.leadGenerationHidesStories == undefined) {
	            getCluster($scope.israCardVariabe.storiesList[$scope.activeIndex].story_id, filterParams);
	        } else {
	            getUc3clusters(filterParams);
	        }
        }   
        $("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt(expScale(values[0]))).formatAmt().split('.')[0]);
        $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + Number(parseInt(expScale(values[1]))).formatAmt().split('.')[0]);
    }

    /*
     * @purpose: on user slider change
     * @created: 27th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function filterDataForUserCount(values,plotChartOnChange) {
        var minMaxUser = [];
        minMaxUser[0] = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster;
        minMaxUser[1] = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster;
        var expScale = d3.scaleLinear().domain([0, 100]).range(minMaxUser);
        filterParams.customer_count_start = expScale(values[0]);
        filterParams.customer_count_end = expScale(values[1]);
        if(plotChartOnChange !="onChange"){
	        if (!window.leadGenerationHidesStories || window.leadGenerationHidesStories == undefined) {
	            getCluster($scope.israCardVariabe.storiesList[$scope.activeIndex].story_id, filterParams);
	        } else {
	            getUc3clusters(filterParams);
	        }
        }
        $("#cluster_range").find(".price-range-min").html(Number(parseInt(expScale(values[0]))).formatAmt().split('.')[0]);
        $("#cluster_range").find(".price-range-max").html(Number(parseInt(expScale(values[1]))).formatAmt().split('.')[0]);
    }

    /*
     * @purpose: collapsible story list
     * @created: 27th june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function minMaxStory(status) {
        if (status == 'true') {
            $scope.israCardVariabe.minimize_maximize = true;
           if(!$scope.showBusinessScienceView){
        	   $timeout(function() {
                   loadScienceViewChart();
               }, 300);
           }
        } else {
            $scope.israCardVariabe.minimize_maximize = false;
            if(!$scope.showBusinessScienceView){
            	$timeout(function() {
                    loadScienceViewChart();
                },300);
            }
        }
    }




    // // slider call
    function slider1() {
        $('#slider').slider({
            range: true,
            min: 0,
            max: 100,
            values: [0, 100],
            slide: function(event,ui){     
            	filterDataForExpenditure(ui.values, "onChange");        
            	},
            stop: function(event, ui) {
                $scope.israCardVariabe.clusterPreloader = true;
                $(".Bubble_Chart_tooltip").remove();
                filterDataForExpenditure(ui.values);
                if (ui.values[0] == ui.values[1]) {
                    $('.price-range-both i').css('display', 'none');
                } else {
                    $('.price-range-both i').css('display', 'inline');
                }
                if (collision($('#slider .price-range-min'), $('#slider .price-range-max')) == true) {
                    $('#slider .price-range-min, #slider .price-range-max').css('opacity', '0');
                    $('#slider .price-range-both').css('display', 'block');
                } else {
                    $('#slider .price-range-min, #slider .price-range-max').css('opacity', '1');
                    $('#slider .price-range-both').css('display', 'none');
                }
            }
        });

        $('.ui-slider-range').append('<span class="price-range-both value1"><i>' + $('#slider').slider('values', 0) + ' - </i>' + $('#slider').slider('values', 1) + '</span>');
        $('.ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#slider').slider('values', 0) + '</span>');
        $('.ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#slider').slider('values', 1) + '</span>');
    }

    function slider2() {
        $('#slider2').slider({
            range: true,
            min: 0,
            max: 100,
            values: [0, 100],
            slide: function(event,ui){   
            	filterDataForUserCount(ui.values, "onChange");   
            	},
            stop: function(event, ui) {
                $scope.israCardVariabe.clusterPreloader = true;
                $(".Bubble_Chart_tooltip").remove();
                filterDataForUserCount(ui.values);
                if (ui.values[0] == ui.values[1]) {
                    $('#slider2 .price-range-both i').css('display', 'none');
                } else {
                    $('#slider2 .price-range-both i').css('display', 'inline');
                }

                if (collision($('#slider2 .price-range-min'), $('#slider2 .price-range-max')) == true) {
                    $('#slider2 .price-range-min, #slider2 .price-range-max').css('opacity', '0');
                    $('#slider2 .price-range-both').css('display', 'block');
                } else {
                    $('#slider2 .price-range-min,#slider2 .price-range-max').css('opacity', '1');
                    $('#slider2 .price-range-both').css('display', 'none');
                }
            }
        });

        $('#slider2 .ui-slider-range').append('<span class="price-range-both value1"><i>' + $('#slider2').slider('values', 0) + ' - </i>' + $('#slider2').slider('values', 1) + '</span>');
        $('#slider2 .ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#slider2').slider('values', 0) + '</span>');
        $('#slider2 .ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#slider2').slider('values', 1) + '</span>');
    }

    /* slider1();
     slider2();*/

    /*
     * @purpose: reset slider
     * @created: 2nd July 2018
     * @return: no
     * @author: varsha
     */

    function resetFilters(resetValue) {
        if (resetValue == 'expenditure') {
            slider1();
            $("#avg_expenditure").find(".price-range-min").html("<span class='fa fa-ils f-8'></span> " + parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster));
            $("#avg_expenditure").find(".price-range-max").html("<span class='fa fa-ils f-8'></span> " + parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster));
            filterParams.avg_expendi_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_avg_expenditure_cluster;
            filterParams.avg_expendi_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_avg_expenditure_cluster;
        } else {
            slider2();
            $("#cluster_range").find(".price-range-min").html(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster));
            $("#cluster_range").find(".price-range-max").html(parseInt($scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster));
            filterParams.customer_count_start = $scope.israCardVariabe.storiesList[$scope.activeIndex].min_user_Count_cluster;
            filterParams.customer_count_end = $scope.israCardVariabe.storiesList[$scope.activeIndex].max_user_Count_cluster;
        }
        if (!window.leadGenerationHidesStories || window.leadGenerationHidesStories == undefined) {
            getCluster($scope.israCardVariabe.storiesList[$scope.activeIndex].story_id, filterParams);
        } else {
            getUc3clusters(filterParams);
        }
    }


    /*
     * @purpose:  switch to details page as per UC selected on landing page
     * @created: 2nd July 2018
     * @return: no
     * @author: varsha
     */
    window.detailsPage = function(detailsPageToShow, id) {
        if (detailsPageToShow == 'General Card Holder') {
            $state.go('generalCardHolderDetails', {
                clusterId: id
            }, {
                totalCustomers: $scope.israCardVariabe.storiesList[$scope.activeIndex].total_user_count
            });
        }
    };

    function detailPge(detailsPageToShow, id) {
        window.detailsPage(detailsPageToShow, id);
    }

    function setFavStory(status) {
        if (status) {
            HostPathService.FlashSuccessMessage('Already set as Favourite', '');
        } else {
            postFavouriteStory($scope.israCardVariabe.storiesList[$scope.activeIndex].story_id, {});
        }
    }
    //-----------------------------------------Science View Starts--------------------------------------
    var minStandardDEviationValue, maxStandardDEviationValue;

    /*
     * @purpose:  get min and max value for standard deviation
     * @created: 24th July 2018
     * @return: no
     * @author: varsha
     */
    var allAttrList = [];
    var attrListWithColor = [];
    
    $scope.scienceViewErrorMsg = false;
    function getMinMaxForSDchart() {
        var allValueList = [];
        allAttrList = [];
        attrListWithColor = [];
        var colorList2 = ['#ce3939', '#ce6839', '#ce9c39', '#bace39', '#45ce39', '#39ce79', '#39cece', '#3997ce', '#3965ce', '#6539ce', '#ce39bf', '#ce3979', '#bc7676', '#63552c', '#39632c', '#632c49', '#66525c', '#e0c7d4', '#9e9398', '#c1608c', '#436a84'];
        if($scope.israCardVariabe.clusterList.detailMessage == "no such index"){
        	$scope.scienceViewErrorMsg = true;
//        	 $('#scienceView').html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Cluster Found</span></div> ");
             return false;
        }
        angular.forEach($scope.israCardVariabe.clusterList, function(d) {
            if (d.sd_attributes.length != null) {
                for (var i = d.sd_attributes.length - 1; i >= 0; i--) {
                    if (!d.sd_attributes[i].is_found) {
                        d.sd_attributes.splice(i, 1);
                    }
                }
                for (var i = d.sd_attributes.length - 1; i >= 0; i--) {
                    var name = localizeTableAttributeNameChange(d.sd_attributes[i].attribute_name);
                    if (allAttrList.indexOf(name) < 0) {
                    	   allAttrList.push(name);
                   }
                }


                d.sd_attributes.sort(function(x, y) {
                    return d3.ascending(x.attribute_sd_value, y.attribute_sd_value);
                });
                allValueList.push(d.sd_attributes[0].attribute_sd_value);
                allValueList.push(d.sd_attributes[d.sd_attributes.length - 1].attribute_sd_value);
            }
        });

        allAttrList.map(function(k, v) {
            attrListWithColor.push({
                name: k,
                color: colorList2[v]
            });
        });


        allValueList.sort(function(x, y) {
            return d3.ascending(x, y);
        });
        minStandardDEviationValue = allValueList[0];
        maxStandardDEviationValue = allValueList[allValueList.length - 1];
        sessionStorage.setItem("minStandardDEviationValue", minStandardDEviationValue);
        sessionStorage.setItem("maxStandardDEviationValue", maxStandardDEviationValue);
    }
    
    /*
     * @purpose:  Load science view charts
     * @created: 24th July 2018
     * @return: no
     * @author: varsha
     */
    
    var allChartOptions = [];
    function loadScienceViewChart() {
    	allChartOptions = [];
        if ($scope.israCardVariabe.clusterList.length == 0  || $scope.israCardVariabe.clusterList.length == undefined) {
        	$scope.scienceViewErrorMsg = true;
        	$scope.israCardVariabe.clusterList = [];
            $scope.israCardVariabe.legendData = [];
            $scope.legendHideShow = false;
            $('#barchartProgress').html("<div class='leadGenErrorDiv text-center mnh-300'style='position: relative'> <span class='no-data-wrapper'style='position: absolute'>No Cluster Found</span></div> ");
            return false;
        } else {
        	 $('#barchartProgress').html(' ');
            getMinMaxForSDchart();
            angular.forEach($scope.israCardVariabe.clusterList, function(d,k) {
            	$scope.israCardVariabe.clusterList[k].activeCluster = false;
                if (d.sd_attributes.length == null) {
                    $('#barChart' + d.cluster_id).html("<div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>No Standard deviation data found</span></div> ");
                    return false;
                } else {
                    var verticalBarChartData = [];
                    var localAttrList = [];
                    angular.forEach(d.sd_attributes, function(d1) {
                        if (d1.is_found) {
                            var attrName = localizeTableAttributeNameChange(d1.attribute_name);
                            verticalBarChartData.push({
                                'name': attrName,
                                'value': d1.attribute_sd_value
                            });
                            localAttrList.push(attrName);
                        }
                        baroptions.container = '#barChart' + d.cluster_id;
                        baroptions.data = verticalBarChartData;
                        baroptions.cluster_id = d.cluster_id;
                        baroptions.fixedColorWithKey = attrListWithColor;
                        baroptions.domainArray = [minStandardDEviationValue, maxStandardDEviationValue];
                        $scope.$apply(function() {
                        	 attrListWithColor.sort(function(x, y) {
                                 return d3.ascending(x.name, y.name);
                             });
                            $scope.israCardVariabe.legendData = attrListWithColor;
                        });
                    });
                    allAttrList.map(function(d) {
                        if (localAttrList.indexOf(d) < 0) {
                            baroptions.data.push({
                                'name': d,
                                'value': 0
                            });
                        }
                    });

                    baroptions.data.sort(function(x, y) {
                        return d3.ascending(x.name, y.name);
                    });
                    
                }
                baroptions.width = $('#barChart' + d.cluster_id).width();
                verticalBarChart(baroptions);
                var clusterOption = jQuery.extend(true,{}, baroptions);
                allChartOptions.push(clusterOption);
                
            });
            $scope.legendHideShow = true;
        }
    }
    
    $(window).resize(function () {
    	if(location.hash.indexOf('cluster') >= 0){
    		loadScienceViewChart();
    	}
    });
    
    /*
     * @purpose: Functionality of enable and disable the chart.
     * @created: 24th August 2018
     * @return: no
     * @author: varsha
     */
    
    var disableClusterCount = 0;
    function updateClusterData(status,value){
    	disableClusterCount = 0;
    	var index  = allChartOptions.map(function(d){
    		return d.cluster_id;
    	}).indexOf(value.cluster_id);
    	if(status == 'enable'){
    		allChartOptions[index].allowDisable = false;
    	}else if(status =='disable'){
    		allChartOptions[index].allowDisable = true;
    	}
    	 allChartOptions.map(function(d){
 			if(d.allowDisable){
 				disableClusterCount = disableClusterCount+1;
 			}
 		});
 		
    	if(disableClusterCount == allChartOptions.length){
    		 return false;
    	}else{
	    	if(status == 'enable'){
	    		$scope.israCardVariabe.clusterList[index].activeCluster = false;
	    		allChartOptions[index].DisableCluster = false;
	    		reCalculateDomainArray(allChartOptions);
	    		return false;
	    	}else if(status =='disable'){
	    		$scope.israCardVariabe.clusterList[index].activeCluster = true;
	    		allChartOptions[index].DisableCluster = true;
	    		reCalculateDomainArray(allChartOptions);
	    		return true;
	    	}
    	}
    }
   
    
    /*
     * @purpose: Recalculate the domain array 
     * @created: 24th August 2018
     * @return: no
     * @author: varsha
     */
    
    function reCalculateDomainArray(allChartOptions){
    var newDomain = [];
    var minMaxList = [];
    	allChartOptions.map(function(d){
    		if(!d.DisableCluster){
    			minMaxList.push(d3.min(d.data, function(d) {
    				return d.value;
    			}));
    			minMaxList.push(d3.max(d.data, function(d) {
    				return d.value;
    			}));
    			newDomain.push(d.data);
    		}
    	});
    	minMaxList.sort(function(x, y) {
            return d3.ascending(x, y);
        });
	    allChartOptions.map(function(d){
			if(!d.DisableCluster){
				d.domainArray[0]=minMaxList[0];
				d.domainArray[1]=minMaxList[minMaxList.length-1];
				verticalBarChart(d);
			}
		});
    	
    }
    
    
    /*
     * @purpose: Legend for mouseover for bubble chart
     * @created: 24th july 2018
     * @return: no
     * @author: varsha
     */
    
    
    window.VerticalNegativeBarChartlegendData = function(data, container) {
        if (container == "#barChart_tooltip") {
            var html = "";
            angular.forEach(data, function(val) {
                html = html + '<div class="d-ib mar-x5" ><span style="width:10px;height:10px;font-size:8px;border-radius:50%;margin-right:5px;background-color:' + val.color + '"></span><span style="font-size:10px;">' + val.name.split('_').join(' ') + '</span></div>';
            });

            $(container).siblings("#legendsDiv").append(html);
        }
        /*  $scope.$apply(function() {
              $scope.israCardVariabe.legendData = data
          });*/
    };

    var renameActiveIndex;
    var renameStoryId;
    var renameCluster;

    function initializeRenameData(activeIndex, Cluster, StoryId) {
        if (StoryId == undefined) {
            renameStoryId = 'UC3';
        } else {
            renameStoryId = StoryId;
        }
        renameCluster = Cluster;
        renameActiveIndex = activeIndex;
    }


    /* @purpose: action on esc and enter for cluster
     * @created: 14th august 2018
     * @author: varsha
     */

    function renameClusterNameKeyUp(event) {
        if (event != undefined) {
            if (event.which == 27) {
                renameClusterName(renameActiveIndex, 'discard');
                return false;
            } else if (event.which == 13) {
                renameClusterName(renameActiveIndex, renameCluster, renameStoryId);
                return false;
            } else {
                return true;
            }
        }
    }

    /* @purpose: save edited name for cluster
     * @created: 14th august 2018
     * @author: varsha
     */

    function renameClusterName(index, cluster, storyId) {
        if (cluster == 'discard') {
            $scope.israCardVariabe.clusterList[index].cluster_name = originalClusterList[index].cluster_name;
            return false;
        }
        if (storyId == undefined) {
            storyId = 'UC3';
        }
        if ($scope.israCardVariabe.clusterList[index].cluster_name == originalClusterList[index].cluster_name) {
            return false;
        } else if (cluster.cluster_name.length > 0 && $scope.israCardVariabe.clusterList[index].cluster_name != originalClusterList[index].cluster_name) {
            originalClusterList[index].cluster_name = cluster.cluster_name;
            postsetClusterName(storyId, cluster.cluster_id, {
                'name': cluster.cluster_name
            });
        } else {
            $scope.israCardVariabe.clusterList[index].cluster_name = originalClusterList[index].cluster_name;
        }
    }

    /* @purpose: store data on click of edit for rename story
     * @created: 14th august 2018
     * @author: varsha
     */

    var renameStoryActiveIndex;

    function initializeRenameDataStory(activeIndex) {
        renameStoryActiveIndex = activeIndex;
    }

    /* @purpose: action on esc and enter for story
     * @created: 14th august 2018
     * @author: varsha
     */
    function renameStoryNameKeyUp(event) {
        if (event != undefined) {
            if (event.which == 27) {
                renameStoryName(renameStoryActiveIndex, 'discard');
                return false;
            } else if (event.which == 13) {
                renameStoryName(renameStoryActiveIndex);
                $("#starDefault").removeClass('ng-hide');
                return false;
            } else {
                return true;
            }
        }
    }

    /* @purpose: save edited name for story
     * @created: 14th august 2018
     * @author: varsha
     */

    function renameStoryName(activeIndex, name) {
        if (name == 'discard') {
            $scope.israCardVariabe.storiesList[activeIndex].story_name = originalStoryList[activeIndex].story_name;
            return false;
        }
        if ($scope.israCardVariabe.storiesList[activeIndex].story_name == originalStoryList[activeIndex].story_name) {
            return false;
        } else if ($scope.israCardVariabe.storiesList[activeIndex].story_name.length > 0 && $scope.israCardVariabe.storiesList[activeIndex].story_name != originalStoryList[activeIndex].story_name) {
            originalStoryList[activeIndex].story_name = $scope.israCardVariabe.storiesList[activeIndex].story_name;
            postsetStoryName($scope.israCardVariabe.storiesList[activeIndex].story_id, {
                'name': $scope.israCardVariabe.storiesList[activeIndex].story_name
            });
        } else {
            $scope.israCardVariabe.storiesList[activeIndex].story_name = originalStoryList[activeIndex].story_name;
        }

    }
    
    /* @purpose: Delete Story
     * @created: 3rd September 2018
     * @author: varsha
     */
    var storyId;
    var storyName;
    function deleteStory(story,event){
    	 event.stopPropagation();
    	 storyId = story.story_id;
    	 storyName = story.story_name;
    	 $scope.israCardVariabe.showAlertBox = true;
    	 $scope.israCardVariabe.modalMessage = "Are you sure you wish to delete the story ["+story.story_name.split('_').join(' ')+"] ?";
    }
    
    /* @purpose: User Action for delete
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    function sayYesorNo(status){
    	if(!status){
    		 $scope.israCardVariabe.showAlertBox = false;
    	}else{
    		deleteStoryById(storyId,storyName);
    		$scope.israCardVariabe.showAlertBox = false;
    		$scope.israCardVariabe.clusterPreloader = true;
    	}
    }
    
    
    /* @purpose:Open modal for create new story
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    function openCreateStoryModal(){
    	$scope.enterName = false;
    	$scope.israCardVariabe.nameAlreadyExist = false;
    	$scope.israCardVariabe.selectWorkflow = false;
    	$scope.israCardVariabe.createNewStory = true;
    }
    
    
    /* @purpose:Cancel new story creation
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    function cancelCreateStory(){
     	$scope.createStoryName = '';
    	$scope.israCardVariabe.createNewStory = false;
    }
    
    
    
    
    /* @purpose: On workflow id selection
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    $scope.createStoryName = undefined;
    var setWorkflowId;
    function selectWorkFlow(id){
    	setWorkflowId = id.workflow_id;
    }
    
    
    /* @purpose: On click of create new Story
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    function createNewStory(){
    	$scope.israCardVariabe.selectWorkflow = false;
    	$scope.israCardVariabe.nameAlreadyExist = false;
    	$scope.enterName = false;
    	if($scope.createStoryName != undefined && $scope.createStoryName.trim() != ''){
    		var index =  $scope.israCardVariabe.storiesList.map(function(d){
    			 return d.story_name;
    		 }).indexOf($scope.createStoryName);
    		if(index == -1){
            	 $scope.israCardVariabe.nameAlreadyExist = false;
            	 callCreateNameApi();
             }else {
            	 $scope.israCardVariabe.nameAlreadyExist = true;
             }
    	}else{
    		$scope.enterName = true;
    	}
    }
    
    /* @purpose: Calling create story api if workflow is defined
     * @created: 3rd September 2018
     * @author: varsha
     */
    
    function callCreateNameApi(){
    	if(!$scope.israCardVariabe.nameAlreadyExist && setWorkflowId){
    		postNewStoryName({"name":$scope.createStoryName,"workflow_id": setWorkflowId});
           	$scope.createStoryName = '';
    		$scope.israCardVariabe.selectWorkflow = false;
    		$scope.israCardVariabe.selectWorkflowName = '';
    		$scope.israCardVariabe.createNewStory = false;
    	    $scope.israCardVariabe.clusterPreloader = true;
    	}else if(setWorkflowId == undefined){
    		$scope.israCardVariabe.selectWorkflow = true;
    	}
    }
    
    $(window).resize(function(){
        plotCluster($scope.respDataForResize);
    });

    
    //---------------------------------------------------------------------------------------------------	    
}