'use strict';
angular.module('leadGenerationApp')
	   .controller('LeadGenerationLandingController', leadGenerationLandingController);

		leadGenerationLandingController.$inject = [
			'$scope',
			'$state',
			'$rootScope',
		];
		
		function leadGenerationLandingController(
				$scope,
				$state,
				$rootScope
				) {
			
			$rootScope.addClassWhenGeneralCardHolder = false;
			/*Initializing scope variables*/ 
			$scope.israCardlandlingPage = {
					israCardLandingToClusterPage:israCardLandingToClusterPage,
					israCardLandingToEADPage:israCardLandingToEADPage,
			};		
			
			function israCardLandingToClusterPage(valueToShow) {
				if(valueToShow == 'Dual Card Holders'){
					window.localStorage.setItem("cardHolderType", valueToShow);
					$state.go('UCtwoCluster');
				}else if(valueToShow == 'General Card Holder'){
					window.localStorage.setItem("cardHolderType", valueToShow);
					window.leadGenerationHidesStories = false;
					$state.go('cluster');
					$rootScope.addClassWhenGeneralCardHolder = true;
				}else if(valueToShow =='Predicting Dual Card Holders'){
					window.localStorage.setItem("cardHolderType", valueToShow);
					window.leadGenerationHidesStories = true;
					$state.go('cluster');
					$rootScope.addClassWhenGeneralCardHolder = false;
				}else if(valueToShow == 'Early Adaptors'){
					window.localStorage.setItem("cardHolderType", valueToShow);
					$state.go('EAD');
				}
					
			}
			function israCardLandingToEADPage() {
				$state.go('EAD');
			}
}
