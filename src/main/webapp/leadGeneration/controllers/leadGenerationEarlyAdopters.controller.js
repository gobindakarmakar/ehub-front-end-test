'use strict';
angular.module('leadGenerationApp')
	   .controller('LeadGenerationEarlyAdoptersController', leadGenerationEarlyAdoptersController);

leadGenerationEarlyAdoptersController.$inject = [
			'$scope',
			'$state',
			'$rootScope',
			"pieChartsConst",
			'EntityGraphService',
			'areaLineChartConst',
			"worldMapConst",
			"LeadGenerationApiService"
		];
		
		function leadGenerationEarlyAdoptersController(
				$scope,
				$state,
				$rootScope,
				pieChartsConst,
				EntityGraphService,
				areaLineChartConst,
				worldMapConst,
				LeadGenerationApiService
				) {
			
			
			$scope.hidingOnfilter = true;
			$scope.israCardFunction = {
					progressBarFilter:progressBarFilter,
					removeTab:removeTab,
					changeTableView:changeTableView,
					changeTablePageNo:changeTablePageNo,
					checkDataNotFound:checkDataNotFound,
					changeTableViewByType:changeTableViewByType
			};
			$scope.israCardVariabe = {
			      totalCustomer:'' ,
			      ageProgressBarData:[],
			      genderPieLegendData :[],
			      topAdopterProgressBarData:[],
			      popularMerchantPieChartLegendData:[],
			      customerData:[]
			};
			
		
			
			$scope.fullpageloader= true;
	 //----------------------------------UC4 API-------------------------------------------------------
			 /*
		     * @purpose: get Customers_count
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function getCustomers_count(filterParam) {
		    	LeadGenerationApiService.getCustomers_count(filterParam).then(function(response) {
		            $scope.israCardVariabe.totalCustomer = response.data.data;
		        }, function() {
		        
		        });
		    }

		    
		    
		    /*
		     * @purpose: get UC3distributions
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

			var genderData = [];
			var ageData = [];
			var seniorityData = [];
			var cardTypeData = [];
			var asScoreData = [];
			var bsScoreData = [];
		    function postUC3distributions(attrName, attrType, distrubtionCount, params,id) {
		    	LeadGenerationApiService.postUC3distributions(attrName, attrType, distrubtionCount, params).then(function(response) {
//		    		genderData = [];
//					ageData = [];
//					seniorityData = [];
//					cardTypeData = [];
//					asScoreData = [];
//				    bsScoreData = [];
//		    		
		    		
		    		if(id == '#genderPie'){
						genderData = jQuery.extend(true,[],response.data.data);
						if(statusPie){
		            		setColorObjPies('gender',response.data.data)
		            	}
						plotPiechart(response.data.data, id);
		            }else if(id == 'age'){
						ageData = jQuery.extend(true,[],response.data.data);
						plotBarChart('age', response.data.data); 
		            }else if(id == 'seniority'){
						seniorityData = jQuery.extend(true,[],response.data.data);
						plotPiechartWithStartAndEnd(response.data.data, '#cardDurationPie');
		            }else if(id == 'cardTypePie'){
						cardTypeData = jQuery.extend(true,[],response.data.data);
						if(statusPie){
		            		setColorObjPies('banking_card',response.data.data)
		            	}
						plotPiechartBankingCard(response.data.data, '#cardTypePie');
		            	
		            }else if(id == 'asScoreChart'){
						asScoreData = jQuery.extend(true,[],response.data.data);
							plotPiechartWithStartAndEnd(response.data.data, '#asScoreChart');
						
		            }else if(id == 'bsScoreChart'){
						bsScoreData = jQuery.extend(true,[],response.data.data);
							plotPiechartWithStartAndEnd(response.data.data, '#bsScoreChart');
		            	
		            }
		            
		        }, function() {
		        	 if(id == '#genderPie'){
		        	        $(id).html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
				            return false;
			            }else if(id == 'age'){
			            }else if(id == 'seniority'){
			                $('#cardDurationPie').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
				            return false;
			            }else if(id == 'cardTypePie'){
			            	 $('#cardTypePie').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
					         return false;
			            }else if(id == 'asScoreChart'){
			            	 $('#asScoreChart').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
					         return false;
			            }else if(id == 'bsScoreChart'){
			            	$('#bsScoreChart').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
					         return false;
			            }
		        });
		    }

		    
		    /*
		     * @purpose: post Top_early_adapters
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function postTop_early_adapters(params) {
		    	LeadGenerationApiService.postTop_early_adapters(params).then(function(response) {
		            setDataForTopAdopterProgressBar(response.data.data);
		        }, function() {
		        
		        });
		    }
		    //code for number formatting
		    Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
		        var n = this,
		            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
		            decSeparator = decSeparator == undefined ? "." : decSeparator,
		            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
		            sign = n < 0 ? "-" : "",
		            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
		            j = (j = i.length) > 3 ? j % 3 : 0;
		        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
		    };
		    $scope.formatTotalAmount = function (amount) {
				if (amount != undefined) {
					var amountVal = amount.toString().split('.');
					var finalAmount;
					if (amountVal[0].length <= 6) {
						finalAmount = formatNummerWithComma(amount);
					} else if (amountVal[0].length <= 8 && amountVal[0].length > 6) {
						finalAmount = (amount / 1000000).toFixed(2) + 'M';
					} else if (amountVal[0].length >= 9) {
						finalAmount = (amount / 1000000000).toFixed(2) + 'B';
					}
					return finalAmount;
				}
			};

			function formatNummerWithComma(amount) {
				var formatAmount;
				formatAmount = Number(Math.round(parseFloat(amount))).formatAmt();
				return formatAmount.split('.')[0];
			}
			
			
			  var genderColorObj = [];
			    var bankingColorObj = [];
			    var asScoreColorObj = [];
			    var bsScoreColorObj = [];
			    var cardDurationColorObj = [];
			    var cardDurationClicked;
			    var bsScoreClicked;
			    var asScoreClicked;
			   
			    function setColorObjPies(type,data){
			    	 if (type == 'gender') {
			    		 data.map(function(d,k) {
			    			 genderColorObj.push({
			    				 'key':d.value,
			    				 'color':colors[k]
			    			 })
			             });
			    	 }else if (type == 'banking_card') {
			    		 data.map(function(d) {
			                 if (d.value == 'true') {
			                     d.key = 'Banking';
			                     d.color = colors[0];
			                 } else {
			                     d.key = 'Non Banking';
			                     d.color = colors[1]
			                 }
			             });
			    		 
			    		 bankingColorObj = data
			    	 }else if(type == 'card'){
			 			data.map(function(d,k){
					   		 d.color = colors[k]
					   	 })
					   	 cardDurationColorObj = data;
					}else if(type == '#asScoreChart'){
						data.map(function(d,k){
					   		 d.color = colors[k]
					   	 })
					   	 asScoreColorObj = data;
					}else if(type == '#bsScoreChart'){
						data.map(function(d,k){
					   		 d.color = colors[k]
					   	 })
					   	 bsScoreColorObj = data;
					}
			    	
			    }
			   
			    
			    
		    /*
		     * @purpose: post Popular_merchant_types
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
			var merchantTypeData = [];
		    function postPopular_merchant_types(params) {
		    	LeadGenerationApiService.postPopular_merchant_types(params).then(function(response) {
					merchantTypeData = jQuery.extend(true,[],response.data.data);
		            plotMerchantType(response.data.data);
		        }, function() {
		        	 $('#topMerchantPie').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Failed to load Data From Server</span></div> ");
			         return false;
		        });
		    }
		    
		    /*
		     * @purpose: post Customers
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function postUc3Customers(params) {
		    	$scope.merchantData = false;
		    	$scope.israCardVariabe.customerData = [];
		    	LeadGenerationApiService.postUc3Customers(params).then(function(response) {
		           $scope.israCardVariabe.customerData = response.data.data.customers;
		           if( $scope.israCardVariabe.customerData.length == 0 ){
		        	   $scope.merchantData = true;
		        	   $scope.merchantDataErrorMsg = "DATA NOT FOUND";
		           }
		           $scope.fullpageloader= false;
		        }, function() {
		        	 $scope.fullpageloader= false;
		        	 $scope.merchantData = true;
		        	 $scope.merchantDataErrorMsg = "Failed to get data from server";
		        });
		    }

		    
 //----------------------------------UC4 API  ENDS-------------------------------------------------------
		    
		    
		    var filterParam = {
		            string_filters: [],
		            integer_filters: [],
		    };
		    
		    var tableFilterObject = {
		    		"results_count":30,
		    		"results_from":0,
		    		"sorting":[{"sort_on":"adopters",
		    		"sort_order":"DESC"	}]
		    };
		    
		    var statusPie;
		    function distributionApiCall(status){
		    	statusPie = status
		    	var ageValue = 5;
		    	var asScore = 5;
		    	var bsScore = 5;
		    	var seniority = 5;
		    	if(filterParam && filterParam.integer_filters && filterParam.integer_filters.length){
			    	filterParam.integer_filters.map(function(d){
			    		if(d.attribute_name == 'age'){
			    			 ageValue = 1;
			    		}
			    		if(d.attribute_name == "asScore"){
			    			asScore = 1;
			    		}
			    		if(d.attribute_name == "bsScore"){
			    			bsScore = 1;
			    		}
			    		if(d.attribute_name == "seniority"){
			    			seniority = 1;
			    		}
			    	});
		    	}
		    	    postUC3distributions('gender', 'string', 3, filterParam,'#genderPie');  
				    postUC3distributions('age', 'integer', ageValue, filterParam,'age');  
				    postUC3distributions('seniority', 'integer', seniority, filterParam,'seniority');  
				    postUC3distributions('bankingCard', 'string', 5, filterParam,'cardTypePie');  
				    postUC3distributions('asScore', 'integer', asScore, filterParam,'asScoreChart');  
				    postUC3distributions('bsScore', 'integer', bsScore, filterParam,'bsScoreChart'); 
				    getCustomers_count(filterParam); 
				    postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    }
		    distributionApiCall(true);
		    postTop_early_adapters({});  
		    postPopular_merchant_types({});
		 //   getCustomers_count(); 
		   // postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    
		    
		    
//------------------Chart Integration------------------------------------------------------
		    
		    /* @purpose: plot pie chart
		     * @created: 27th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    var colors = ['#1C6EC1', '#72BE74', '#ADE3AC', '#55D2E1', '#F36030', '#A072FC', '#CB3E4B', '#CB8B3D', '#aaaba8', '#E66C40', '#00C4AB', '#CBA03D', '#76CB3D', '#CB493D', '#70685F'];
		    function plotPiechart(data, id) {
		        if (data.length == 0) {
		            $(id).html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Data Not Found</span></div> ");
		            return false;
		        } else {
		            data.map(function(d) {
		                d.key = d.value;
		                    d.value = d.customer_count;
		            });
		            var alertsChartpieoption = { //Piechart option
			                container: id,
			                colors: colors,
			                data: data,
			                height:90,
			                txtSize: '10px',
			                legendwidth: 40,
			                txtColor: '4D6772',
			                legendmargintop: 30,
			                format:true,
			                width:$(id).width()
			            };
		            
		            if(id =='#genderPie'){
		            	alertsChartpieoption.fixedColorWithKey = genderColorObj;
		            }
			            setTimeout(function() {
			                new reusablePie(alertsChartpieoption); //load pie chart
			            });
		        }
		    }
		    
		    
		    /* @purpose: plot bar
		     * @created: 2nd july 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function plotBarChart(type, data) {
/*		        var maxvalue = d3.max(data, function(d) {
		            return d.customer_count;
		        });*/
		        var sum = d3.sum(data, function(d) {
		            return d.customer_count;
		        });
		        data.map(function(d) {
		            var name = d.start + '-' + d.end;
		            d.key = name.toString();
		            d.value = (d.customer_count / sum) * 100;
		        });
		        var sum = d3.sum(data, function(d) {
		            return d.value;
				});
				angular.forEach(data,function(v){
					v.key = v.start == v.end ? v.start : (v.start+'-'+v.end) ; 
				});
		        if (type == 'age') {
		            $scope.israCardVariabe.ageProgressBarData = data;
		        }
		    }
		    
		    
		    
		    /* @purpose: plot pie with start and end data
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    function plotPiechartWithStartAndEnd(data,id){
		    	 if (data.length == 0) {
		             $(id).html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Data Not Found</span></div> ");
		             return false;
		         } else {
			    	data.map(function(d) {
						var name = d.start + '-' + d.end;
			    		  	d.key = name;
		                    d.value = d.customer_count;
		            });
		           /* var sum = d3.sum(data, function(d) {
		                return d.value;
		            });*/
					angular.forEach(data,function(v){
						v.key = String(v.start == v.end ? v.start : (v.start+'-'+v.end)); 
					});
					
					
					if(id == '#cardDurationPie'){
						if(statusPie){
		            		setColorObjPies('card',data)
		            	}
						if(bsScoreClicked){
							setColorObjPies('card',data);
						}
						if(asScoreClicked){
							setColorObjPies('card',data);
						}
			    	}else if(id == '#asScoreChart'){
			    		if(statusPie){
		            		setColorObjPies('#asScoreChart',data)
		            	}
						if(bsScoreClicked){
							setColorObjPies('#asScoreChart',data);
							bsScoreClicked = false;
						}
						 if(cardDurationClicked){
							 setColorObjPies('#asScoreChart',data);
			             }
						 
			    	}else if(id =='#bsScoreChart'){
			    		if(statusPie){
		            		setColorObjPies('#bsScoreChart',data);
		            	}
						if(asScoreClicked){
							setColorObjPies('#bsScoreChart',data);
							asScoreClicked = false;
						}
						 if(cardDurationClicked){
							 setColorObjPies('#bsScoreChart',data);
			            	 cardDurationClicked = false;
			             }
			    	}
					
					
				
					
					
					
					 
					
			    	var alertsChartpieoption = { //Piechart option
			                container: id,
			                colors: colors,
			                data: data,
			                height:90,
			                txtSize: '10px',
			                legendwidth: 40,
			                txtColor: '4D6772',
			                legendmargintop: 30,
			                format:true,
			                width:$(id).width()
			            };
			    	
			    	 setTimeout(function() {
					    	if(id == '#cardDurationPie'){
					    		alertsChartpieoption.fixedColorWithKey = cardDurationColorObj;
					    	}else if(id == '#asScoreChart'){
					    		alertsChartpieoption.fixedColorWithKey = asScoreColorObj;
					    	}else if(id =='#bsScoreChart'){
					    		alertsChartpieoption.fixedColorWithKey = bsScoreColorObj;
					    	}
					            
					                new reusablePie(alertsChartpieoption); //load pie chart
					            
			            
			    	 },2000);
		         }
		    }
		    
		    
		    /* @purpose: plot pie for banking card
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    function plotPiechartBankingCard(data,id){
		    	 if (data.length == 0) {
		             $(id).html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Data Not Found</span></div> ");
		             return false;
		         } else {
				    	data.map(function(d) {
				    		if(d.value == 'true'){
				    			d.key = 'Banking';
				    			d.originalValue = d.value;
			                    d.value = d.customer_count;
				    		}else{
				    			d.key = 'Non Banking';
				    			d.originalValue = d.value;
			                    d.value = d.customer_count;
				    		}
				    		  	
			            });
//			            var sum = d3.sum(data, function(d) {
//			                return d.value;
//			            });
				    	var alertsChartpieoption = { //Piechart option
				                container: id,
				                colors: colors,
				                data: data,
				                height:90,
				                txtSize: '10px',
				                legendwidth: 40,
				                txtColor: '4D6772',
				                legendmargintop: 30,
				                format:true,
				                fixedColorWithKey:bankingColorObj,
				                width:$(id).width()
				            };
				    	
				    	
				            setTimeout(function() {
				                new reusablePie(alertsChartpieoption); //load pie chart
				            });
		         }
		    }
		   
			$(window).resize(function(){
				plotPiechart(jQuery.extend(true,[],genderData), '#genderPie');
				plotBarChart('age', jQuery.extend(true,[],ageData));
				plotPiechartWithStartAndEnd(jQuery.extend(true,[],seniorityData), '#cardDurationPie');
				plotPiechartBankingCard(jQuery.extend(true,[],cardTypeData), '#cardTypePie');
				plotPiechartWithStartAndEnd(jQuery.extend(true,[],asScoreData), '#asScoreChart');
				plotPiechartWithStartAndEnd(jQuery.extend(true,[],bsScoreData), '#bsScoreChart');
				plotMerchantType(jQuery.extend(true,[],merchantTypeData));
			});
				    
		    
		    /* @purpose: Plot progress bar for Top adaptor
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    function setDataForTopAdopterProgressBar(data){
		    	/* var maxvalue = d3.max(data, function(d) {
			            return d.adopters;
			        });*/
		    	 var sum = d3.sum(data, function(d) {
			            return d.adopters;
			        });
			        data.map(function(d) {
			        	d.key = d.owner_id;
			            d.value = (d.adopters / sum) * 100;
			        });
		        $scope.israCardVariabe.topAdopterProgressBarData = data;
		    }
		    
		    
		    /* @purpose: Plot Merchant type Pie
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    
		    function plotMerchantType(data){
			   	 if (data.length == 0) {
		             $('#topMerchantPie').html("<div class='leadGenErrorDiv text-center no-data-wrapper'> <span>Data Not Found</span></div> ");
		             return false;
		         } else {
			    	data.map(function(d) {
			    		  	d.key = d.merchant_type;
		                    d.value = d.count;
		            });
			    	var alertsChartpieoption = { //Piechart option
			                container: '#topMerchantPie',
			                colors: colors,
			                data: data,
			                height:150,
			                txtSize: '10px',
			                legendwidth: 40,
			                txtColor: '4D6772',
			                legendmargintop: 30,
			                format:true,
			                cursor:'default'
			            };
			            setTimeout(function() {
			                new reusablePie(alertsChartpieoption); //load pie chart
			            });
		         }
		    }
		    
		    
		    /* @purpose: Set legend Data
		     * @created: 27th september 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    window.reusablePieLegendData = function(data,id){
		    	if(id == '#genderPie'){
		    		 $scope.$apply(function() {
		    			 $scope.israCardVariabe.genderPieLegendData = data;
		             });
		    	}else if(id == '#cardDurationPie'){
		    		 $scope.$apply(function() {
		    			 $scope.israCardVariabe.CardDurationPieLegendData = data;
		             });
		    		
		    	}else if(id == '#cardTypePie'){
		    		 $scope.$apply(function() {
		    			 $scope.israCardVariabe.cardTypePieLegendData = data;
		             });
		    	}else if(id == '#bsScoreChart'){
		    		 $scope.$apply(function() {
		    			 $scope.israCardVariabe.bsScorePieLegendData = data;
		             });
		    	}else if(id == '#asScoreChart'){
		    		$scope.$apply(function() {
		    			$scope.israCardVariabe.asScorePieLegendData = data;
		             });
		    	}else if(id == '#topMerchantPie'){
		    		 $scope.$apply(function() {
		    			 $scope.israCardVariabe.popularMerchantPieChartLegendData = data;
		             });
		    	}
		    	
		    };
		    
		    
		    //-----------------------Filter implementation----------------------------------------------
		    /* @purpose: Filters for pie legends
		     * @created: 14th dec 2018
		     * @params: id, data, currentData
		     * @author: swathi
		     */
		    $scope.applyPieLegendFilters = function(id, data, currentData) {
		    	var arcData = {};
		    	if(id == "#asScoreChart" || id == "#bsScoreChart" || id == "#cardDurationPie"){
		    		arcData["data"] = {
		    			"value": currentData.value, 
		    			"customer_count": currentData.value, 
		    			"key": currentData.name,
		    			"color": currentData.color,
		    			"start": currentData.name.toString().split("-")[0],
		    			"end": currentData.name.toString().split("-")[1]?currentData.name.toString().split("-")[1]:currentData.name.toString().split("-")[0]
		    		};
		    	}else{
		    		arcData["data"] = {
		    			"value": currentData.value, 
		    			"customer_count": currentData.value, 
		    			"key": currentData.name,
		    			"color": currentData.color
		    		};
		    	}
		    	window.pieAndWorldOnClick(id, data, arcData);
		    };
		    /* @purpose: Filter for pie
		     * @created:28th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		  
		    window.pieAndWorldOnClick = function(id, data, arcData) {
		        if (id == '#genderPie') {
		            var gender_stringList = [];
		            gender_stringList.push(arcData.data.key);
		            var index3 = filterParam.string_filters.map(function(d) {
		                return d.attribute_name;
		            }).indexOf('gender');
		            if (index3 == -1) {
		                filterParam.string_filters.push({
		                    "attribute_name": "gender",
		                    "name": "gender",
		                    "values": gender_stringList
		                });
		            } else {
		                filterParam.string_filters[index3].values = gender_stringList;
		            }
		            distributionApiCall();
		            setFilter();
		        } else if (id == '#bsScoreChart') {
		        	bsScoreClicked = true;
		            checkForPie('bsScore', arcData,'bs Score');
		           distributionApiCall();
		            setFilter();
		        } else if (id == '#asScoreChart') {
		        	asScoreClicked = true;
		            checkForPie('asScore', arcData,'as Score');
		          distributionApiCall();
		            setFilter();
		        } else if (id == '#cardTypePie') {
		            var bankingCard_stringList = [];
		            if (arcData.data.key == 'Non Banking') {
		                bankingCard_stringList.push('false');
		            } else {
		                bankingCard_stringList.push('true');
		            }
		            var index8 = filterParam.string_filters.map(function(d) {
		                return d.attribute_name;
		            }).indexOf('banking_card');
		            if (index8 == -1) {
		                filterParam.string_filters.push({
		                    "attribute_name": "bankingCard",
		                    "name": "banking Card",
		                    "values": bankingCard_stringList
		                });
		            } else {
		                filterParam.string_filters[index8].values= bankingCard_stringList;
		            }
		            distributionApiCall();
		            setFilter();
		        } else if (id == '#cardDurationPie') {
		        	cardDurationClicked = true;
		        	checkForPie('seniority', arcData,'seniority');
		            distributionApiCall();
		            setFilter();
		        } 
		    };



		    /* @purpose: Filter for pie 
		     * @created: 28th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function checkForPie(type, arcData,mainType) {
		        var index2 = filterParam.integer_filters.map(function(d) {
		            return d.attribute_name;
		        }).indexOf(type);
		        if (index2 == -1) {
		            filterParam.integer_filters.push({
		                "attribute_name": type,
		                "name": mainType,
		                "start": arcData.data.start,
		                "end": arcData.data.end
		            });
		        } else {
		            filterParam.integer_filters[index2].start = arcData.data.start;
		            filterParam.integer_filters[index2].end = arcData.data.end;
		        }
		    }

		    /* @purpose: Filter for Progress bar
		     * @created: 28th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    function progressBarFilter(type, value) {
		        var index2 =  filterParam.integer_filters.map(function(d) {
		            return d.attribute_name;
		        }).indexOf(type);
		        if (index2 == -1) {
		        	 filterParam.integer_filters.push({
		                "attribute_name": type,
		                "name": type,
		                "start": value.start,
		                "end": value.end
		            });
		        } else {
		        	 filterParam.integer_filters[index2].start = value.start;
		        	 filterParam.integer_filters[index2].end = value.end;
				}
				$scope.hidingOnfilter = false;
		        distributionApiCall();
		        setFilter();
		        
		    }



		    /* @purpose: Filter array
		     * @created: 28th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */

		    $scope.israCardVariabe.addedFilter = [];
		    function setFilter() {
		        $scope.israCardVariabe.addedFilter = [];
		        filterParam.integer_filters.map(function(d) {
					if(d.attribute_name == 'seniority' || d.attribute_name == 'bsScore' || d.attribute_name == 'asScore'){
						var val = d.start == d.end ? d.start : (d.start+'-'+d.end) ;
					}
					else{
						var val = d.start + '-' + d.end;
					}
		            $scope.israCardVariabe.addedFilter.push({
		                'showName': d.name + ' -->'+val,
		                'attribute_name': d.attribute_name,
		                'start': d.start,
		                'end': d.end,
		                'type': 'integer_filters'
		            });
		        });
		        filterParam.string_filters.map(function(d) {
		            if (d.attribute_name == 'bankingCard') {
		                if (d.values[0] == 'true') {
		                    $scope.israCardVariabe.addedFilter.push({
		                        'showName': d.name + ' --> BANKING',
		                        'attribute_name': d.attribute_name,
		                        'values': d.values,
		                        'type': 'string_filters'
		                    });
		                } else {
		                    $scope.israCardVariabe.addedFilter.push({
		                        'showName': d.name + ' -->NON BANKING',
		                        'attribute_name': d.attribute_name,
		                        'values': d.values,
		                        'type': 'string_filters'
		                    });
		                }

		            } else {
		                $scope.israCardVariabe.addedFilter.push({
		                    'showName': d.name + ' -->' + d.values[0],
		                    'attribute_name': d.attribute_name,
		                    'values': d.values,
		                    'type': 'string_filters'
		                });
		            }

		        });
		    }
		    
		    /* @purpose: Remove filter
		     * @created: 28th September 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    function removeTab(k, data) {
		        $scope.israCardVariabe.addedFilter.splice(k, 1);
		        var index = filterParam[data.type].map(function(d) {
		            return d.attribute_name;
		        }).indexOf(data.attribute_name);
				filterParam[data.type].splice(index, 1);
				//Hiding and showing % in age tooltip		
				if (filterParam && filterParam.integer_filters && filterParam.integer_filters.filter(function(e) { return e.attribute_name === 'age'; }).length > 0){
					$scope.hidingOnfilter = false;
				}
				else{
					$scope.hidingOnfilter = true;
				}
		        distributionApiCall(true);
		    }
		    
		   
		    /* @purpose: On change of asc and dsc
		     * @created: 1st Oct 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    $scope.checkedValue = "DESC";
		    function changeTableView(checkedValue){
		    	if(checkedValue == 'ASC'){
		    		tableFilterObject.sorting[0].sort_order = checkedValue;
		    		postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    	}else{
		    		tableFilterObject.sorting[0].sort_order= checkedValue;
		    		postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    	}
		    	//console.log(tableFilterObject,'tableFilterObject')
		    }

		    $scope.typeValue = 'adopters';
		    function changeTableViewByType(typeValue){
		    	if(typeValue == 'adopters'){
		    		tableFilterObject.sorting[0].sort_on = typeValue;
		    		postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    	}else{
		    		tableFilterObject.sorting[0].sort_on = typeValue;
		    		postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    	}
		    }
		    /* @purpose: On change of Page for table
		     * @created: 1st Oct 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    function changeTablePageNo(pageNum){
		    	if(pageNum > 1){
		    		tableFilterObject.results_from = (pageNum-1)*30;
		    	}else{
		    		tableFilterObject.results_from = (pageNum-1);
		    	}
		    	var diff = $scope.israCardVariabe.totalCustomer - ((pageNum-1)*30);
		    	if(diff < 30){
		    		tableFilterObject.results_count = diff;
	    		}else{
	    			tableFilterObject.results_count = 30;
	    		}
		    	postUc3Customers(Object.assign(filterParam, tableFilterObject));
		    }
		    
		    
		    /* @purpose: Progress bar data not found check
		     * @created: 1st Oct 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		     */
		    
		    function checkDataNotFound(data){
		    	if(data != undefined){
		    	    if(data.length === 0){
		    	    	return true;
		    	    }
		        }
		    }

		
}
