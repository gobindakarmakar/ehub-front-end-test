<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->
          <style>
        .legendHidden
        {
            display: none !important;
        }
        .legendShown
        {
            display: block !important;
        }
    </style>
        <!--  Cluster Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper  lead-generation-wrapper cluster-dashboard-wrapper bg-dark-grey1" id="wrap">
            <div ng-show="clusterPreloader" class="custom-spinner full-page-spinner">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <!-- Top Grid Wrapper Starts-->
            <div class="top-grid-wrapper side-remain container-fluid">
                <ul class="list-inline custom-tabs row top-filters-list">
                    <li class="col-sm-6 pad-t0 ">
                        <div class="col-sm-4 pad-x0">
                            <strong class="details-value c-arrow">{{clusterInfoToShow.bubblesNum | number}} Clusters </strong>
                            <p class="c-arrow">generated</p>
                        </div>
                        <div class="col-sm-4 pad-x0">
                            <strong class="details-value c-arrow">{{clusterInfoToShow.totalIsraCustomers}} </strong>
                            <p class="c-arrow">total customers</p>
                        </div>
                        <div class="col-sm-4 pad-x0">
                            <strong class="details-value c-arrow">₪ {{formatTotalAmount(clusterInfoToShow.totalExp) || 0}}</strong>
                            <p class="c-arrow">Total Spendings</p>
                        </div>
                    </li>
                    <li class="col-sm-6 pad-t0">
                        <div class="col-sm-5">
                            <div class="usage-feature-box">
                                    <div class="featurbox-top-caption mar-b15">  <p class="c-arrow mar-b0">
                                    Average Expenditure
                                </p>
                                <a href="" class="pull-right" ng-click="resetFilters('avgExpenditure')">
                                    reset
                                </a></div>
                                <div class="color-input-wrapper" id="avg_expenditure">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider" class="custom-range-slider" ng-change="avgExpenditureChange();" ng-model="myValue"></div>
                                        </div>
                                        <div class="col-sm-3 pull-left c-arrow" style="width:auto">MIN <span class="fa fa-ils f-8"></span> {{formatNummerWithComma(minMaxValues_exp[0]).split('.')[0]}}</div>
                                        
                                        <div class="col-sm-3 pull-right c-arrow" style="width:auto">MAX <span class="fa fa-ils f-8"></span> {{formatNummerWithComma(minMaxValues_exp[1]).split('.')[0]}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 ">
                            <div class="usage-feature-box right-box">
                                    <div class="featurbox-top-caption mar-b15">
                                <p class="usage-feature-box-heading c-arrow mar-b0">
                                    Cluster Range
                                </p>
                                <a href="" class="pull-right" ng-click="resetFilters('clusterRange')">
                                    reset
                                </a></div>
                                <div class="color-input-wrapper grey" id="cluster_range">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider2" class="custom-range-slider"></div>
                                        </div>
                                        <div class="col-sm-3 pull-left c-arrow" ng-change="clusterRangeChange();" style="width:auto" ng-model="myValue">MIN {{rangeMinCluster | number}}</div>
                                        <div class="col-sm-3 pull-right c-arrow" style="width:auto">MAX {{rangeMaxCluster |number}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-group pad-x0 pad-b10 custom-select-dropdown col-sm-2">
                            <span class="filter-key text-uppercase c-arrow">View</span>
                            <select id="displayByIconOrTable" ng-model="checkedValue" ng-change="changeView(checkedValue)" class="filter-value text-uppercase ">

                                <option value="business" selected="selected">Marketing</option>
                                <option value="science">Science</option>
                            </select>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- Top Grid Wrapper Ends -->

            <!--Cluster Content Wrapper Starts-->
            <div class="cluster-content-wrapper" ng-if="showView">
                <div id="israClusterBubbleChart" ng-click="check($event)"></div>
                <div class="list-notations" ng-if="legendHideShow">
                    <ul ng-repeat="legend in bubbleChartLegend">
                        <li class="notation-list">
                            <span class="icon-left" style="color:{{legend.color}};">
                                <i class="fa fa-circle"></i>
                            </span>
                            <span class="c-arrow" style="color:{{legend.color}};">{{legend.name}}</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Cluster Content Wrapper Ends -->
            <!-- Bar Chart Wrapper Starts -->
            
            <div class="barchart-wrapper div-141 p-rel ng-scope" ng-if="!showView" id="scienceView">
                    <div ng-if="verticalBarChartData.length === 0" class="width-100">
                        <div class='leadGenErrorDiv text-center mnh-300'> <span class='no-data-wrapper'>Data not found</span></div>
                    </div>
                <div class="side-remain panel-scroll" >
                    <ul class="custom-list pad-l25 item-4">

                        <li ng-repeat="(k,barChart) in verticalBarChartData">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper ">
                                <div class="panel-heading pad-b0">
                                    <div class="custom-input-group d-flex width-100 mar-b5 border-grey-b c-arrow input-group">
                                        <h3 ng-if="!clusterClick" class="h-30 text-overflow mar-b0 pad-8 pad-r25 ">{{barChart.name}}</h3>
                                        <span ng-show="!clusterClick" class="f-18 mar-r10 text-dark-grey fa fa-pencil" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'" ng-click="clusterClick = true;initializeRenameData(k,barChart,'UC2')"></span>
                                        <input ng-show="clusterClick" ng-model="barChart.name" type="text" name=""  class="border-b0 custom-input pad-x5 pad-r10 "
                                            ng-keydown="clusterClick= renameClusterNameKeyUp($event)">
                                        <span ng-show="clusterClick" class=" f-18 text-dark-grey fa fa-check" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Save" popover-trigger="'mouseenter'" ng-click="clusterClick = false;renameClusterName(k,barChart,'UC2')"></span>
                                        <span ng-show="clusterClick" class=" c-pointer mar-x5 f-18 text-dark-grey fa fa-times" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Cancel" popover-trigger="'mouseenter'" ng-click="clusterClick = false;renameClusterName(k,'discard')"></span>

								  		<span ng-show="!disableChart" class="minusPlusClass f-18 c-pointer  text-dark-grey fa fa-eye-slash" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Disable Chart" popover-trigger="'mouseenter'" ng-click="disableChart = updateClusterData('disable',barChart)"></span>
                                        <span ng-show="disableChart" class=" minusPlusClass f-18 c-pointer  text-dark-grey fa fa-eye" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Enable Chart" popover-trigger="'mouseenter'" ng-click="disableChart = updateClusterData('enable',barChart)"></span>
                                     
                                    </div>
                                </div>
                                <div class="panel-body pad-t0 pad-x15 mnh-100px" style="height:204px;">
                                    <p class="f-10 mar-b5 text-center c-arrow"> Total Customer :
                                        <span class="mar-r5">{{barChart.size | number}}</span>
                                    </p>
                                    <div class="location-map-wrapper">
                                        <div id="barChart{{barChart.clusterID}}"  ng-class="{'enableCss':!barChart.activeCluster,'disableCss':barChart.activeCluster}" ng-click="detailsPage('Dual Card Holders',barChart.clusterID)">

                                        </div>
                                    </div>
                                    <!-- ngIf: cluster['clustering_attributes'] == null -->
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </li>
                    </ul>
                    
                </div>
                <div class="side-defined " ng-if="legendHideShow">
                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper border-b0">
                        <div class="panel-heading border-grey-thin-b c-arrow">
                            <h3 class="mar-b0">Legend</h3>
                        </div>
                        <div class="panel-body panel-scroll border-b0">
                            <ul class="custom-list pad-l5 item-1">
                                <li class="legends mar-b5 f-12 text-uppercase c-arrow"  ng-repeat="legend in scienceViewLegendData">
                                    <span class="fa fa-circle mar-r5" style="color:{{legend.color}};"></span>
                                    {{legend.name}}
                                </li>
                                <!-- end ngRepeat: attr in israCardVariabe.legendData -->
                            </ul>
                        </div>
                    </div>
                    <!-- Segment Panel Ends -->
                </div>
            </div>
            <!-- Bar Chart Wrapper Ends -->
        </div>
        <!--  Cluster Dashboard Wrapper Ends  -->

        <script>
            function collision($div1, $div2) {
                var x1 = $div1.offset().left;
                var w1 = 40;
                var r1 = x1 + w1;
                var x2 = $div2.offset().left;
                var w2 = 40;
                var r2 = x2 + w2;

                if (r1 < x2 || x1 > r2) return false;
                return true;

            }
            $(".panel-scroll").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
           
        </script>