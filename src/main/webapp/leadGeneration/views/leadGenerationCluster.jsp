<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style>
    .legendHidden
        {
            display: none !important;
        }
        .legendShown
        {
            display: block !important;
        }
        .inputTextSize
        {
            font-size: 1.8rem;
        }
    </style>
<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Cluster Dashboard Wrapper Starts  -->
<div class="dashboard-wrapper lead-generation-wrapper mnh-dashboard cluster-dashboard-wrapper p-rel bg-dark-grey1">
    <div ng-show="israCardVariabe.clusterPreloader" class="custom-spinner full-page-spinner">
        <i class="fa fa-spinner fa-spin fa-3x"></i>
    </div>
    <div class="cluster-content-wrapper height-100 clearfix" ng-class="{'div-216 div-280':(israCardVariabe.minimize_maximize && hideStories) , 'div-40 div-50 ':!israCardVariabe.minimize_maximize && hideStories}">
        <div class="side-defined" ng-if="hideStories">
            <div class="  pad-y15 pad-x10">
                <span class="left pull-left mar-l5 mar-b15  fa fa-bars f-18 text-dark-blue  c-pointer" ng-if="!israCardVariabe.minimize_maximize"
                    ng-click="israCardFunction.minMaxStory('true')">
                </span>
                <h3 class="heading  text-left mar-b0 text-uppercase c-arrow">
                    Select A Story
                    <span class="right pull-right fa fa-angle-left f-21 text-dark-blue c-pointer" ng-click="israCardFunction.minMaxStory('false')"></span>
                </h3>
            </div>
            <ul class="custom-list story-list-wrapper pad-l0 item-1">
                <li class="lead-heading-list " id="Story{{storyList.story_id}}" ng-class="{ 'active': k==activeIndex }"
                    ng-repeat="(k,storyList) in israCardVariabe.storiesList" ng-click="israCardFunction.showCluster(k)">
                    <div class="heading-details-wrapper pad-y15 pad-r5 pad-l10  ">
                        <p class="story-heading d-ib width-90">
                            {{storyList.story_name.split('_').join(' ')}}
                        </p>
                        <span class="fa fa-trash pull-right text-dark-red f-14 c-pointer" style=" " ng-click="israCardFunction.deleteStory(storyList,$event)">
                        </span>
                        <p class="story-details">
                            <span class="details">
                                ₪ {{formatTotalAmount(storyList.total_expenditure)}} SPENDINGS
                            </span>|
                            <span class="details">
                                {{storyList.total_user_count | number}} CUSTOMERS
                            </span>

                        </p>
                    </div>
                </li>
                <li class="text-center mar-y10" ng-click="israCardFunction.openCreateStoryModal()">
                    <button type="button" class=" grad-button  ">
                        Create New Story
                    </button>
                </li>
            </ul>
            <ul class="custom-list story-icons-wrapper pad-l0 item-1" ng-if="!israCardVariabe.minimize_maximize">
                <li class="lead-icon-list " id="Story{{story.story_id}}" ng-class="{ 'active': k==activeIndex }"
                    ng-repeat="(k,story) in israCardVariabe.storiesList" ng-click="israCardFunction.showCluster(k)">
                    <div class="heading-icon  ">
                        <p class="story-heading d-ib width-100">
                            {{story.story_name.split('_')[0].substring(0,1)}}
                            {{story.story_name.split('_')[[story.story_name.split('_').length-1]].substring(0,1)}}
                        </p>
                    </div>
                </li>
                <!-- <li class="">
                            <div class=" text-center">
                                <p class="story-heading d-ib width-100 fa fa-chevron-down">

                                </p>


                            </div>
                        </li> -->
                <li class="lead-icon-list border-0 pad-l5" ng-click="israCardFunction.openCreateStoryModal()">
                    <div class="heading-icon bg-blue  ">
                        <p class="story-heading d-ib width-100 text-white fa fa-plus"></p>


                    </div>
                </li>
            </ul>
        </div>
        <div class="side-remain" ng-class="{'pull-right':hideStories}">
            <!-- Top Grid Wrapper Starts-->
            <div class="top-grid-wrapper container-fluid">
                <ul class="list-inline pad-l15 custom-tabs row top-filters-list">
                    <li class="col-sm-4 pad-t0">
                        <div class="story-name-wrapper d-flex ai-b mar-b10">
                            <p ng-show="hideStories" class="f-16 width-100 d-flex mar-b0 lh-18 story-name c-arrow" id="starDefault">
                                <span ng-if="!storyClick" class="f-14 two-line-wrapper">{{israCardVariabe.storiesList[activeIndex]['story_name']}}</span>
                                <span ng-show="!storyClick" class=" right-addon text-dark-grey fa fa-pencil" id="editPencil"
                                    popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                    uib-popover="Edit" popover-trigger="'mouseenter'" ng-click="storyClick = true;israCardFunction.initializeRenameDataStory(activeIndex)">
                                </span>
                            </p>
                            <input onkeypress="return AvoidSpace(event)" ng-show="storyClick" ng-model="israCardVariabe.storiesList[activeIndex]['story_name']"
                                type="text" name="" id="" class="custom-input pad-l5 pad-r10 pad-t0 pad-b10 width-100 f-14 border-grey-b"
                                ng-keydown="storyClick =  israCardFunction.renameStoryNameKeyUp($event)">
                            <span ng-show="storyClick" popover-class="top-pop-wrapper" popover-append-to-body="'true'" id="toshowNameCorrect"
                                popover-animation="true" uib-popover="Save" popover-trigger="'mouseenter'" class="  f-16 c-pointer  text-dark-blue fa fa-check"
                                ng-click="storyClick = false;israCardFunction.renameStoryName(activeIndex,israCardVariabe.storiesList[activeIndex]['story_name'])"></span>
                            <span ng-show="storyClick" popover-class="top-pop-wrapper" popover-append-to-body="'true'" id="toshowNameCros"
                                popover-animation="true" uib-popover="Cancel" popover-trigger="'mouseenter'" class="c-pointer  f-16   text-dark-blue fa fa-times"
                                ng-click="storyClick = false;israCardFunction.renameStoryName(activeIndex,'discard')"></span>
                            <a ng-show="hideStories" popover-class="top-pop-wrapper" popover-append-to-body="'true'"
                            popover-animation="true" uib-popover="Make Story Default" popover-trigger="'mouseenter'"
                            id="star" ng-click="storyClick = false;israCardFunction.setFavStory(israCardVariabe.showFav)"
                            class="fa fa-star f-16 mar-l15 lh-18 pull-right"></a>

                        </div>
                        <div class="col-sm-6 pad-x0">
                            <strong class="details-value c-arrow">{{israCardVariabe.storiesList[activeIndex]['actual_total_user_count']}}</strong>
                            <p class="details-caption c-arrow">total customers</p>
                        </div>
                        <div class="col-sm-6 pad-x0">
                            <strong class="details-value c-arrow" ng-if="israCardVariabe.storiesList[activeIndex]['actual_total_expenditure']>0">₪
                                {{formatTotalAmount(israCardVariabe.storiesList[activeIndex]['actual_total_expenditure'])}}
                            </strong>
                            <strong class="details-value c-arrow" ng-if="israCardVariabe.storiesList[activeIndex]['actual_total_expenditure']==0">₪
                                {{israCardVariabe.storiesList[activeIndex]['actual_total_expenditure']}}
                            </strong>
                            <p class="details-caption c-arrow">Total Spendings</p>
                        </div>
                    </li>
                    <li class="col-sm-8 pad-t0">
                        <div class="col-sm-5">
                            <div class="usage-feature-box">
                                <div class="featurbox-top-caption mar-b15">
                                <p class="c-arrow mar-b0">
                                    Average Expenditure
                                </p>
                                <a href="" class="pull-right" ng-click="israCardFunction.resetFilters('expenditure')">
                                    reset
                                </a></div>
                                <div class="color-input-wrapper" id="avg_expenditure">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider" class="custom-range-slider" ng-change="avgExpenditureChange();"
                                                ng-model="myValue"></div>
                                        </div>
                                        <div class="col-sm-3 pull-left" style="width:auto">MIN<span class="fa mar-l5 fa-ils f-8"></span>
                                            {{formatNummerWithComma(israCardVariabe.storiesList[activeIndex]['min_avg_expenditure_cluster'])}}
                                        </div>
                                        <div class="col-sm-3 pull-right" style="width:auto">MAX<span class="fa mar-l5 fa-ils f-8"></span>
                                            {{formatNummerWithComma(israCardVariabe.storiesList[activeIndex]['max_avg_expenditure_cluster'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 ">
                            <div class="usage-feature-box right-box">
                                    <div class="featurbox-top-caption mar-b15">    <p class="usage-feature-box-heading c-arrow mar-b0">
                                    Customers Range
                                </p>
                                <a href="" class="pull-right" ng-click="israCardFunction.resetFilters('')">
                                    reset
                                </a></div>
                                <div class="color-input-wrapper grey" id="cluster_range">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="slider2" class="custom-range-slider"></div>
                                        </div>
                                        <div class="col-sm-3 pull-left" ng-change="clusterRangeChange();" style="width:auto"
                                            ng-model="myValue">MIN
                                            {{israCardVariabe.storiesList[activeIndex]['min_user_Count_cluster'] |
                                            number}}
                                        </div>
                                        <div class="col-sm-3 pull-right" style="width:auto">MAX
                                            {{israCardVariabe.storiesList[activeIndex]['max_user_Count_cluster'] |
                                            number}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="select-group pad-b10 custom-select-dropdown col-sm-2">
                            <span class="filter-key text-uppercase c-arrow">View</span>
                            <select id="displayByIconOrTable" class="filter-value text-uppercase ng-pristine ng-valid ng-not-empty ng-touched"
                                ng-model="checkedValue" ng-change="israCardFunction.changeView(checkedValue)">
                                <option value="business" selected="selected">Marketing</option>
                                <option value="science">Science</option>
                            </select>
                        </div>
                    </li>
                </ul>
                <div class="custom-filters-wrapper mar-l10">
                    <p class="filters-title d-ib text-uppercase mar-b0 c-arrow">
                        Attributes in the story
                        <span class="fa fa-angle-right f-16 mar-r5 pull-right"></span>
                    </p>
                    <ul class="llist-inline d-ib pull-right pad-l5 custom-list" id="badgesStyle">
                        <li class="filters" ng-repeat="attribute in israCardVariabe.storiesList[activeIndex]['clustering_attributes']">
                            <button class="btn btn-cancel badges f-8 iso-btn d-ib">{{israCardFunction.localizeTableAttributeNameChange(attribute.attribute_name).split('_').join('
                                ').toUpperCase()}}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Top Grid Wrapper Ends -->

            <!--Cluster Content Wrapper Starts-->
            <div class="cluster-content-wrapper mar-t15" ng-if="showBusinessScienceView">
                <div id="israClusterBubbleChart" ng-click="check($event)"></div>
            </div>
            <!-- Cluster Content Wrapper Ends -->

            <!--Barchart Content Wrapper Starts-->
            <div class="barchart-wrapper mar-t15 div-141 p-rel " ng-if="!showBusinessScienceView" id="scienceView">

                <div class="side-remain">
                    <div id="barchartProgress" class="width-100"></div>
                    <ul class="custom-list pad-l25" ng-class="addClassWhenGeneralCardHolder ? 'item-3' : 'item-4'">
                        <li ng-repeat="(k,cluster) in israCardVariabe.clusterList">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper ">
                                <div class="panel-heading pad-b0">
                                    <div class="custom-input-group d-flex width-100 mar-b5 border-grey-b c-arrow top-pop-wrapper input-group">
                                        <h3 ng-if="!clusterClick" class="text-overflow h-30 mar-b0  ">{{cluster.cluster_name}}</h3>
                                        <span ng-show="!clusterClick" class="f-18 mar-r10 c-pointer text-dark-grey fa fa-pencil"
                                            popover-class="top-pop-wrapper" popover-append-to-body="'true'"
                                            popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'"
                                            ng-click="clusterClick = true;israCardFunction.initializeRenameData(k,cluster,israCardVariabe.storiesList[activeIndex]['story_id'])"></span>
                                        <input ng-show="clusterClick" ng-model="cluster.cluster_name" type="text" name=""
                                            id="" class="custom-input pad-x5 pad-r10 border-b0  width-100   "
                                            ng-keydown="clusterClick= israCardFunction.renameClusterNameKeyUp($event)">
                                        <span ng-show="clusterClick" popover-class="top-pop-wrapper"
                                            popover-append-to-body="'true'" popover-animation="true" uib-popover="Save"
                                            popover-trigger="'mouseenter'" class="f-18 c-pointer text-dark-grey fa fa-check"
                                            ng-click="clusterClick = false;israCardFunction.renameClusterName(k,cluster,israCardVariabe.storiesList[activeIndex]['story_id'])"></span>
                                        <span ng-show="clusterClick" popover-class="top-pop-wrapper"
                                            popover-append-to-body="'true'" popover-animation="true" uib-popover="Cancel"
                                            popover-trigger="'mouseenter'" class="c-pointer  text-dark-grey f-18 mar-x5 fa fa-times"
                                            ng-click="clusterClick = false;israCardFunction.renameClusterName(k,'discard')"></span>
                                        <span ng-show="!disableChart" popover-class="top-pop-wrapper"
                                            popover-append-to-body="'true'" popover-animation="true" uib-popover="Disable Chart"
                                            popover-trigger="'mouseenter'" class="minusPlusClass f-18 c-pointer  text-dark-grey fa fa-eye-slash right-11"
                                            ng-click="disableChart = israCardFunction.updateClusterData('disable',cluster)"></span>
                                        <span ng-show="disableChart" popover-class="top-pop-wrapper" class="minusPlusClass f-18 c-pointer  text-dark-grey fa fa-eye right-11"
                                            popover-append-to-body="'true'" popover-animation="true" uib-popover="Enable chart"
                                            popover-trigger="'mouseenter'" ng-click="disableChart = israCardFunction.updateClusterData('enable',cluster)"></span>

                                    </div>
                                </div>
                                <div class="panel-body pad-t0 pad-x15 mnh-100px" style="height:204px;">
                                    <!-- <p class="f-10 mar-b5"> Attributes :
<span class="mar-r5" ng-repeat="attr in israCardVariabe.clusterList[k]['clustering_attributes']">{{attr.attribute_name}} |</span>
                                            </p> -->
                                    <p class="f-10 mar-b5 text-center c-arrow"> Total Customer :
                                        <span class="mar-r5">{{cluster.cluster_user_count | number}}</span>
                                    </p>
                                    <div class="location-map-wrapper">
                                        <div id="{{'barChart'+cluster.cluster_id}}" ng-class="{'enableCss':!cluster.activeCluster,'disableCss':cluster.activeCluster}"
                                            ng-click="israCardFunction.detailPge('General Card Holder',cluster.cluster_id)"></div>
                                    </div>
                                    <div class=" leadGenErrorDiv text-center no-data-wrapper" ng-if="cluster['clustering_attributes'] == null">
                                        <span class="no-data-wrapper">Data Not Found</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </li>
                    </ul>
                </div>
                <div class="side-defined " ng-if="legendHideShow" id="legendHiddenWhenNoClusterFound">
                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper border-b0">
                        <div class="panel-heading border-grey-thin-b c-arrow">
                            <h3 class="mar-b0">Legend</h3>
                        </div>
                        <div class="panel-body border-b0">
                            <ul class="custom-list pad-l5 item-1">
                                <li class="legends mar-b5 text-uppercase f-12 c-arrow" ng-repeat="attr in israCardVariabe.legendData">
                                    <span class="fa fa-circle mar-r5 " style="color:{{attr.color}};"></span>
                                    {{attr.name.split('_').join(' ')}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Segment Panel Ends -->
                </div>
            </div>
            <!-- Barchart Content Wrapper Ends -->
        </div>
    </div>
</div>
<div id="" class="overlay sticky-modal  z-9999999  " ng-class="{ 'width-100': israCardVariabe.showAlertBox }">
    <div class="overlay-content mar-auto small-message-modal clearfix border-grey fixed-message-box bg-light-grey1">
        <p class="message-wrapper text-center mar-b5">
            {{israCardVariabe.modalMessage}}
        </p>
        <div class="btn-group mar-autox">
            <div class="btn  grad-button  mar-r5" ng-click="israCardFunction.sayYesorNo(true)">Yes</div>
            <div class="btn  bordered-button " ng-click="israCardFunction.sayYesorNo(false)">Cancel</div>
        </div>
    </div>
</div>

<div id="" class="overlay sticky-modal  z-9999999  " ng-class="{ 'width-100': israCardVariabe.createNewStory }">
    <div class="overlay-content p-rel mar-auto clearfix  fixed-message-box">
        <!-- <div class="custom-spinner full-page-spinner ">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div> -->
        <div class="panel-heading">
            <p class="text-left mar-b0 c-arrow">Add a story </p><span class="right-icon fa fa-times" ng-click="israCardFunction.cancelCreateStory()"></span>
        </div>
        <div class="story-details pad-y15 clearfix" id="storyNameParentDiv">

            <div id="storyNameDiv" class="input-group input-group-mat custom-input-group  d-ib mar-b0 custom-select-dropdown inner-element">
                <input type="text" placeholder="" maxlength="255" required ng-model="createStoryName" onkeypress="return AvoidSpace(event)"
                    class="mar-t15 custom-input h-30 pull-right pad-l15 pad-r25 width-100 d-ib border-0">
                <span class="label">
                    Name
                    <span class="fa fa-info-circle text-info-blue story-name-info" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span>
                </span>
                <span class="cluster-story-name hidden">StoryName should be the prefix of index in elasticsearch in
                    which you have inserted the data</span>
            </div>
            <p class="width-60 mar-xauto d-ib text-center mar-t5 text-dark-red f-12 mar-b0 " ng-if="israCardVariabe.nameAlreadyExist">Name
                Already Exists</p>
            <p class="width-60 mar-xauto d-ib text-center mar-t5 text-dark-red f-12 mar-b0 " ng-if="enterName">Please
                Enter Name</p>
            <p class="width-60 mar-xauto d-ib text-center mar-t5 text-dark-red f-12 mar-b0 d-none" id="isStoryNameValid">Spaces
                are not allowed</p>
        </div>
        <div class="story-details pad-y15 clearfix">
            <div id="typehead-wrapper" class="input-group input-group-mat custom-input-group d-ib mar-b0 custom-select-dropdown inner-element">

                <input id="typehead-inputbox" required="required" placeholder="" class="custom-input typehead-dropdown h-30 mar-t15 pull-right pad-l15 pad-r25 width-100 d-ib border-0"
                    type="text" maxlength="255" ng-model="israCardVariabe.selectWorkflowName" uib-typeahead="state as state.workflow_data.name for state in workflowData  | filter:$viewValue | limitTo:8"
                    typeahead-loading="loadingPersonsNames" typeahead-on-select="israCardFunction.selectWorkFlow($item, $model, $label)">
                {{state.split('_').join('
                ')}}
                <span class="label">
                    Link Workflow
                </span>
                <span class="right-icon right-10  fa fa-chevron-down"></span>
            </div>
            <p class="width-60 mar-xauto d-ib text-center mar-t5 text-dark-red f-12 mar-b0 " ng-if="israCardVariabe.selectWorkflow">Please
                select a workflow</p>

        </div>


        <div class="btn-group pull-right mar-y10 mar-autox">
            <div class="btn bordered-button  pad-x10 pad-y5 mar-r5 pull-right" ng-click="israCardFunction.cancelCreateStory()">Cancel</div>
            <div class="btn grad-button  pad-x10 pad-y5 mar-r5 pull-right" ng-click="israCardFunction.createNewStory($item, $model, $label,storyName)">Create
                a story</div>

        </div>
    </div>
</div>
<!--  Cluster Dashboard Wrapper Ends  -->
<script>
    $(".custom-filters-wrapper .custom-list").mThumbnailScroller({
        axis: "x",
        advanced:{ updateOnSelectorChange: true }
    });
    $('#typehead-wrapper .dropdown-menu').mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
</script>
<script>
    function collision($div1, $div2) {
        var x1 = $div1.offset().left;
        var w1 = 40;
        var r1 = x1 + w1;
        var x2 = $div2.offset().left;
        var w2 = 40;
        var r2 = x2 + w2;

        if (r1 < x2 || x1 > r2) return false;
        return true;


    }
    $("body").on("mouseover", "#star", function () {
        if (location.hash.indexOf('cluster') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#star", function (event) {
        if (location.hash.indexOf('cluster') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html('Set as default');
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    })
    // testingLead();


    /**
     * Function to get tool tip for story name
     * 
     */
    $("body").on("mouseover", "#storyNameParentDiv .story-name-info", function () {
        if (location.hash.indexOf('cluster') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
            div.style("z-index", 999999);
        }
    }).on("mousemove", "#storyNameParentDiv .story-name-info", function (event) {
        if (location.hash.indexOf('cluster') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.style("max-width", "290px");
            div.html($("span.cluster-story-name").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    function AvoidSpace(event) {
        $("#isStoryNameValid").addClass("d-none");
        var k = event ? event.which : window.event.keyCode;
        if (k == 32) {
            $("#isStoryNameValid").removeClass("d-none");
            return false;
        }
    }

     $("body").on("click", "#editPencil", function () {
         $("#starDefault").addClass('ng-hide');
     });
     $("body").on("click","#toshowNameCros,#toshowNameCorrect",function(){
        $("#starDefault").removeClass('ng-hide');
     });
</script>