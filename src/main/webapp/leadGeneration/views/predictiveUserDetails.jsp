<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<!--  SubMenu Starts  -->
	<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
		<!--  SubMenu Ends  -->

		<!--  Predictive User Dashboard Wrapper Starts  -->

		<div class="dashboard-wrapper lead-generation-wrapper segment-dashboard-wrapper predictive-user-wrapper bg-dark-grey1">

			<!-- Main Segment Content Starts Here -->
			<div class="row main-segment-wrapper">

				<!-- Right Col Starts -->
				<div class="col-sm-3 custom-col right-col">
					<div class="row custom-row">
						<div class="col-sm-12 custom-col segment-right-panel">

							<!-- Segment Panel Starts -->
							<div class="panel custom-panel-wrapper  segment-panel-wrapper">
								<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
									<div class="user-details-wrapper">
										<h4>
											name : Jane Palmer
										</h4>
										<h4>
											Age : 37 Years
										</h4>
										<h4>
											Maritial Status : Single
										</h4>
										<h4>
											Location : SunnyVale | USA
										</h4>
										<h4>
											Banking Card : Isra Platinium Plan
										</h4>
										<h4>
											ISSUED By : BANK
										</h4>
									</div>
									<div class="row score-card-wrapper">
										<div class="col-sm-4">
											<h4>Sei Score</h4>
											<strong>34</strong>
										</div>
										<div class="col-sm-4">
											<h4>Risk Level</h4>
											<strong>AB</strong>
										</div>
										<div class="col-sm-4">
											<h4>Seniority</h4>
											<strong>15<sub>YRS</sub></strong>
										</div>
									</div>
									<div class="credit-limit-wrapper">
										<h4>Credit Limit</h4>
										<span>$56,7456</span>
									</div>
									<!--  Progressbar List Starts -->
									<ul class="list-unstyled progressbar-list">

										<li class="progressbar-list-item range-list">
											<div class="left-col">credit Utilisation</div>
											<div class="progress progress-light-gray">
												<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
												 style="width:95%">
													<span class="sr-only ">100 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item range-list">
											<div class="left-col"> CREdit Consumption</div>
											<div class="progress progress-light-gray">
												<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
												 style="width:75%">
													<span class="sr-only ">100 Complete</span>
												</div>
											</div>
										</li>
									</ul>
									<!--  Progressbar List Ends -->
								</div>
							</div>
							<!-- Segment Panel Ends -->

							<!-- Segment Panel Starts
							<div class="panel custom-panel-wrapper  segment-panel-wrapper">
								<div class="panel-heading pad-b0">
									<h3>Merchants</h3>
								</div>
								<div class="panel-body panel-scroll pad-t0" style="position: relative; overflow: visible;">
									<!--  Progressbar List Starts
									<ul class="list-unstyled progressbar-list">
										<li class="progressbar-list-item">
											<div class="left-col">WALLMART</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:90%">
													<span class="sr-only ">1 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">KFC</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:80%">
													<span class="sr-only">1651 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">AMAZON</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:70%">
													<span class="sr-only">710 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">SUBWAY</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:60%">
													<span class="sr-only">5 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">NETFLIX</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:50%">
													<span class="sr-only">2921 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">EBAY</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:40%">
													<span class="sr-only">168 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item">
											<div class="left-col">AMAZON-PRIME</div>
											<div class="progress">
												<div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
												 style="width:30%">
													<span class="sr-only">32 Complete</span>
												</div>
											</div>
										</li>
									</ul>
									<!--  Progressbar List Ends 

								</div>
							</div>
							<!-- Segment Panel Ends

							<!-- Segment Panel Starts 
							<div class="panel custom-panel-wrapper segment-panel-wrapper">
								<div class="panel-heading pad-b0">
									<h3>Locations</h3>
								</div>
								<div class="panel-body border-b0">
									<div id=""></div>
								</div>
							</div>
							<!-- Segment Panel Ends

							<!-- Segment Panel Starts 
							<div class="panel custom-panel-wrapper segment-panel-wrapper">
								<div class="panel-heading pad-b0">
									<h3>AVErage Expenditure</h3>
								</div>
								<div class="panel-body border-b0">
									<div id=""></div>
								</div>
							</div>
							<!-- Segment Panel Ends -->
						</div>
						<!--  Twelfth Column Ends  -->
					</div>
				</div>
				<!-- Right Col Ends -->

				<!-- Left Col Starts -->
				<div class="col-sm-9 custom-col left-col">

					<!-- Top Bar Starts -->
					<div class="topbar">
						<ul class="list-inline top-bar-list row">
							<li class="col-md-4 pad-r0">
								<div class="custom-check">
									<label class="checkbox-inline pad-r0">
										<input type="checkbox" class="schemaField" name="input_0" value="SAR">
										<span>
											<i class="fa fa-check"></i>
										</span>
										<b class="input_0" title="SAR">Labels</b>
									</label>
									<label class="checkbox-inline">
										<input type="checkbox" class="schemaField" name="input_0" value="SAR">
										<span>
											<i class="fa fa-check"></i>
										</span>
										<b class="input_0" title="SAR">ATM</b>
									</label>
									<label class="checkbox-inline">
										<input type="checkbox" class="schemaField" name="input_0" value="SAR">
										<span>
											<i class="fa fa-check"></i>
										</span>
										<b class="input_0" title="SAR">Location</b>
									</label>
								</div>
							</li>
							<li class="col-md-5">
								<label class="checkbox-inline">Period</label>
								<select class="custom-select-box">
									<option selected="">Choose</option>
									<option>float</option>
									<option>integer</option>
									<option>double</option>
									<option>longitude</option>
									<option>latitude</option>
									<option>datetime</option>
									<option>timestamp</option>
									<option>Categorical</option>
									<option>Quantitative</option>
									<option>Vector</option>
									<option>Map</option>
								</select>
								<input type="text"></input>
							</li>
							<li class="col-md-3">
								<div class="dropdown">
									<button class="btn btn-dropdown dropdown-toggle" type="button" data-toggle="dropdown">DOWNLOAD
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li>
											<a href="#">DiamondCard</a>
										</li>
										<li>
											<a href="#">MasterCard</a>
										</li>
									</ul>
								</div>
								<button type="submit" class="btn btn-update pull-right">UPDATE</button>
							</li>
						</ul>
					</div>
					<!-- Top Bar Ends -->

					<div id=""></div>
				</div>
				<!-- Left Col Ends -->

			</div>
			<!-- Main Segment Content ends Here -->

		</div>
		<!--  Predictive User Dashboard Wrapper Ends  -->

		<script>
			$(document).ready(function () {
				setTimeout(function () {
					$(".top-filters-wrapper").mThumbnailScroller({
						axis: "x"
					});
				}, 500);
			});
			$(".chart-panel-wrapper .panel-scroll").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
		</script>