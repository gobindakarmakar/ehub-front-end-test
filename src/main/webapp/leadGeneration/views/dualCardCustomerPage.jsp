<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Details Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper lead-generation-wrapper club-dashboard-wrapper bg-dark-grey1">
            <!--  Main Filters Wrapper Starts  -->
            <div class="main-filters-wrapper">

                <!-- Top Grid Wrapper-->
                <div class="top-grid-wrapper container-fluid">
                    <ul class="list-inline custom-tabs row top-filters-list">
                        <li class="col-sm-4">
                            <div class="col-sm-9">
                                <p class="top-heading">
                                    <i class="fa fa-angle-left icon-right"></i> Family having two kids</p>
                            </div>
                            <div class="c100 p63 pink radial">
                                <span>63%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sm-4">
                            <div class="col-sm-6">
                                <strong class="value">672,889 </strong>
                                <h4 class="top-grid-heading">Total Costumers</h4>
                            </div>

                            <div class="col-sm-6">
                                <strong class="value">$348,990,12 </strong>
                                <h4 class="top-grid-heading">Total Spendings</h4>
                            </div>
                        </li>
                        <li class="col-sm-4">
                            <div class="top-filters segment-filters-wrappers pull-right">

                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Average Expenditure</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:95%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col">$34,990</div>
                                    </li>

                                    <li class="progressbar-list-item">
                                        <div class="left-col">GP Average</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col">$14,990</div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                    </ul>
                </div>
                <!--Top Grid Wrapper Ends-->

                <!--  Top Filters Wrapper Starts  -->
                <div class="top-filters-wrapper segment-filters-wrappers">
                    <ul class="top-filter-navigation list-inline ">
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Age</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item ">
                                            <div class="left-col">10-20</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:100%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item ">
                                            <div class="left-col">20-40</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item ">
                                            <div class="left-col">40-60</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item ">
                                            <div class="left-col">60-above</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Gender</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-3 filters-subhead-wrapper">
                                        <div class="filters-subhead">Male</div>
                                        <div class="filters-subhead">Female</div>
                                        <div class="filters-subhead">Other</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="genderPie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">SEI SCORE</h4>
                                <span class="top-filters-value">32.9</span>
                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Risk Level</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item  mar-t10">
                                            <div class="left-col">a-b</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">b-s</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Card Duration(YRS)</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-3 filters-subhead-wrapper">
                                        <div class="filters-subhead">0-5 </div>
                                        <div class="filters-subhead">5-10</div>
                                        <div class="filters-subhead">10-15</div>
                                        <div class="filters-subhead">15 Above</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="cardDurationPie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Card Types</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">Banks </div>
                                        <div class="filters-subhead">Credit Card Co</div>
                                        <div class="filters-subhead">Club</div>
                                        <div class="filters-subhead">Others</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="cardTypePie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Issued By</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">Diamond </div>
                                        <div class="filters-subhead">Visa</div>
                                        <div class="filters-subhead">MASTER Card</div>
                                        <div class="filters-subhead">Diners</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="issuedBYPie"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Credit</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list two-col">
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">limit</div>
                                            <div class="progress progress-light-gray">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">Utilisation</div>
                                            <div class="credit-utilise">
                                                <input type="range" min="0" max="100" value="24" class="range pink">
                                            </div>
                                            <div class="right-col-pink">24%</div>
                                        </li>
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">Consumption</div>
                                            <div class="select-box-wrapper light-blue">
                                                <select class="form-control custom-select-box" data-show-subtext="true">
                                                    <option>Select</option>
                                                    <option>Diamond Card</option>
                                                    <option>Diamond Card</option>
                                                </select>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                    </ul>
                </div>
                <!--  Top Filters Wrapper Ends  -->

            </div>
            <!--  Main Filters Wrapper Ends  -->

            <div class="customer-club-wrapper  isra-card-wrapper">
                <span ng-show="addMediaPreloader" class="custom-spinner case-dairy-spinner">
                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                </span>
                <div class="club-body-wrapper">
                    <div class="club-top-content">
                        <div class="row club-top-grid">
                            <div class="col-sm-6 club-budget">
                                <div class="col-sm-6">
                                    <p class="top-heading">
                                        <i class="fa fa-angle-left icon-right"></i> Family having two kids</p>
                                </div>
                                <div class="col-sm-6">
                                    <span> Total Spending</span>
                                    <strong>$781,211,990</strong>
                                </div>
                            </div>

                            <div class="col-sm-6 club-filter">
                                <div class="pull-right">
                                    <span class="pull-left">Merchants Type :</span>
                                    <select class="form-control text-uppercase custom-select-box">
                                        <option value="ASC" selected="selected">All</option>
                                        <option value="DESC">none</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="club-bottom-content">
                        <div class="row">
                            <div class="col-md-6 left-col">
                                <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                    <div class="panel-body row panel-scroll pad-b0" style="position: relative; overflow: visible;">
                                        <div class="col-sm-6">
                                            <img src="..\assets\images\isra-card\isracard.png" alt="" height="36" width="55">
                                            <span class="card-title">Diamond CARD</span>
                                            <div class="spending-wrapper">
                                                <p> Total Spending</p>
                                                <strong>$781,211,990</strong>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="pull-right spending-filter">
                                                <span class="pull-left">Merchants types :</span>
                                                <select class="form-control text-uppercase custom-select-box">
                                                    <option value="ASC" selected="selected">All</option>
                                                    <option value="DESC">none</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div id=""></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 right-col">
                                <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                    <div class="panel-body row panel-scroll pad-b0" style="position: relative; overflow: visible;">
                                        <div class="col-sm-6">
                                            <div class="pull-left">
                                                <img src="..\assets\images\isra-card\mastercard.png" alt="" height="36" width="55">
                                                <span class="card-title">MASTER CARD</span>
                                                <div class="spending-wrapper">
                                                    <p> Total Spending</p>
                                                    <strong>$781,211,990</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="pull-right spending-filter">
                                                <span class="pull-left">Merchants Type :</span>
                                                <select class="form-control text-uppercase custom-select-box">
                                                    <option value="ASC" selected="selected">All</option>
                                                    <option value="DESC">none</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="">


                                        <div class="club_segment_tooltip" style="position: absolute; z-index: 99999; opacity: 1; pointer-events: none; visibility: visible; display: block; text-transform: uppercase;  border-radius: 5px; border:0; left: 223px; top: 420px;">
                                            <h4>Dinning</h4>
                                            <p>
                                                Monthly Expenditure :<span> $56789</span>
                                            </p>
                                            <p>
                                                General Expenditure : <span>$56789</span>
                                            </p>
                                            <p>
                                                Expenditure Percentage :<span> 45%</span>
                                            </p>

                                        </div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>