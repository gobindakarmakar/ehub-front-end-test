<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Dual Card Dashboard Wrapper Starts  -->
<div class="dashboard-wrapper fix-tip lead-generation-wrapper segment-dashboard-wrapper dark-grey-dashboard"
    ng-controller="LeadGenerationCommonfilterController">
    <div class="div-360">
        <!-- Left Col Starts -->


        <div class="side-defined mxh-dashboard dark-black-panels pad-x15 custom-col right-col" ng-class="{'d-none':showProgressBar}">
            <div class="row custom-row">
                <div class="col-sm-12 custom-col segment-right-panel">

                    <!-- Segment Panel Starts -->
                    <div class="panel row custom-panel-wrapper  segment-panel-wrapper" id="idForToolTips">
                        <div id="dynamic-tabs" class=" pad-x0 pad-y10">
                            <div class="remote-filters-wrapper remote-filters-wrapper-blue">
                                <ul class="list-inline mar-b0 remote-filters-list-wrapper">
                                    <li class="remote-filters-list" ng-repeat="filterdata in filterObjectForUI" style="pointer-events:none;">
                                        <a class="remote-filter">
                                         	<span>{{filterdata.name}}  {{filterdata.value}}</span>
                                            <i class="fa fa-times c-pointer" ng-click="removeTab(filterdata.name,filterdata)" style="pointer-events:all;"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-heading border-thin-dark-cream-b p-rel d-flex ai-b pad-t10 pad-b0">
                            <h3 ng-if="!clusterClick" class="text-cream mar-b0 pull-left c-arrow two-line-wrapper details_avg_exp">{{clusterDetail['name']}}</h3>
                            <span ng-show="!clusterClick" class="text-cream c-pointer top-12 f-18  mar-x10  fa fa-pencil"
                                popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                uib-popover="Edit" popover-trigger="'mouseenter'" ng-click="clusterClick = true;"></span>
                            <!-- <span ng-show="!clusterClick" class="text-dark-red c-pointer top-12 f-12 icon-right1  mar-x10  fa fa-trash" popover-class="top-pop-wrapper"
                            popover-append-to-body="'true'" popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'"
                            ng-click="clusterClick = true;"></span> -->

                            <input ng-show="clusterClick" type="text" class="custom-input custom-input-box pad-0 pad-l5 pad-r10 pad-b5 text-uppercase f-18 border-blue-thin-b"
                                ng-model="clusterDetail['name']" aria-describedby="basic-addon1" ng-keydown="clusterClick= renameClusterNameKeyUp($event)">
                            <span ng-show="clusterClick" class="mar-x5  top-12 c-pointer f-18 text-dark-cream fa fa-check"
                                popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                uib-popover="Save" popover-trigger="'mouseenter'" ng-click="clusterClick = false;renameClusterName('save')"></span>
                            <span ng-show="clusterClick" class="mar-x5 f-18 c-pointer text-dark-cream fa fa-times"
                                popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true"
                                uib-popover="Cancel" popover-trigger="'mouseenter'" ng-click="clusterClick = false;renameClusterName('discard')"></span>
                        </div>
                        <!-- <div class="panel-body col-sm-12" style="position: relative; overflow: visible;">
                                    <div class=" pad-x10 d-ib left-details-panel c-arrow">
                                        <p class="details-name text-dark-cream mar-b10 roboto-regular">Total Customers</p><strong
                                            class="details-value roboto-bold"><span class="fa fa-users f-14 pad-r5"></span>{{formatTotalAmount(clusterDetail.customer_count)}}</strong></div>
                                    <div class="d-ib  pad-x10 c-arrow">
                                        <p class="details-name text-dark-cream mar-b10 roboto-regular">Total Spending</p><strong
                                            class="details-value roboto-bold"><span class="fa fa-ils f-16 pad-r5"></span>{{formatTotalAmount(clusterDetail.total_expenditure)}}</strong></div>
                                </div> -->
                        <div class="panel-body" style="position: relative; overflow: visible;">
                            <div class="pad-x0 pad-b0 cluster-details-wrapper">
                                <p class="details-name mar-b0  text-cream roboto-regular d-ib c-arrow"><span class="fa fa-users pull-left  f-14 pad-r5"></span>Total
                                    Customers
                                    <span class="fa fa-info-circle text-info-blue details_total_cust_tooltip" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span>
                                </p>
                                <strong class="details-value text-right roboto-bold c-arrow">
                                    {{formatTotalAmount(clusterDetail.customer_count)}}
                                    <span class="details_total_cust_tooltip">
                                        ({{(clusterDetail.customer_percentage).toFixed(2)}}%)</span>
                                </strong>
                            </div>
                        </div>
                        <div class="panel-body " style="position: relative; overflow: visible;">
                            <div class="pad-x0 pad-y10  cluster-details-wrapper">
                                <p class="details-name mar-b5 d-ib text-cream roboto-regular c-arrow">
                                    Total Expenditure
                                    <span class="fa fa-info-circle text-info-blue details_total_exp" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span>
                                </p>
                                <strong class="details-value text-right roboto-bold c-arrow">
                                    <span class="fa fa-ils f-14 "></span>
                                    {{formatTotalAmount(clusterDetail.total_expenditure)}}
                                    <span class="details_total_exp">
                                        ({{(clusterDetail.total_expenditure_percentage).toFixed(2)}}%)</span>
                                </strong>

                            </div>
                            <div class="pad-x0 pad-y10  cluster-details-wrapper">
                                <p class="details-name mar-b5 text-cream roboto-regular d-ib c-arrow">Average
                                    Expenditure
                                    <span class="fa fa-info-circle text-info-blue details_avg_exp" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p>
                                <strong class="details-value text-right roboto-bold c-arrow">
                                    <span class="fa fa-ils f-14"></span>
                                    {{formatTotalAmount(clusterDetail.avg_expenditure)}} <span class="details-value details_avg_exp">
                                        ({{(clusterDetail.avg_expenditure_percentage).toFixed(2)}}%)</span>
                                </strong>
                            </div>
                        </div>
                        <!--For tooltip-->
                        <span class="right-col-no-of-cust hidden">Percentage from the number of customers :
                            {{(clusterDetail.customer_percentage).toFixed(2)}}%</span>
                        <span class="right-col-total-exp-cluster hidden">Percentage from the total expenditure :
                            {{(clusterDetail.total_expenditure_percentage).toFixed(2)}}%</span>
                        <span class="right-col-avg-exp-cluster hidden">Percentage from the average expenditure :
                            {{(clusterDetail.avg_expenditure_percentage).toFixed(2)}}%</span>
                        <span class="right-col-cluster-name hidden">{{clusterDetail['name']}}</span>
                        <!--For tooltip end-->
                    </div>
                    <!-- Segment Panel Ends -->
                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-body" style="position: relative; overflow: visible;">
                            <div class="custom-range-wrapper correction-range track-5 width-100 lead-range-wrapper">
                                <h3 class="text-cream roboto-regular text-inherit mar-y10 f-14 text-overflow c-arrow">Number
                                    of Connections

                                    <div class="w-55 h-2_4 d-ib input-group-mat pull-right border-r3">
                                        <p id="currentValue" class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px">{{maxFilterValue}}</p>
                                    </div>
                                    <!-- <span class="pull-right lh-14 f-10" id="setConnectionValue">{{maxSegmentValue}}
                                            </span> -->
                                </h3>
                                <input type="range" min="1" max="{{maxFilterValue}}" step="1" id="no_connection" class="bg-transparent pad-y5">
                            </div>
                            <div class="custom-range-wrapper track-5 costumer-range width-100 lead-range-wrapper" id="avgExpenditure">
                                <h3 class="text-cream roboto-regular lh-22 va-m text-capitalize mar-b25 f-14 text-cream c-arrow roboto-regular text-overflow">Average
                                    Expenditure
                                    <div class="clearfix pull-right">
                                        <div class="w-65 h-2_4 d-ib left mar-r10 input-group-mat border-r3">
                                            <span class='fa fa-ils f-10 mar-x5'></span>
                                            <p value="" id="minexp" class="custom-input f-12 h-2_4 mar-0 pad-r5 pad-l0 pad-y0 d-if border-0 mnw-100px">{{formatTotalAmount(minimumExpenditure)}}</p>
                                        </div>
                                        <div class="w-65 h-2_4 d-ib input-group-mat border-r3">
                                            <span class='fa fa-ils f-10 mar-x5'></span>
                                            <p value="" id="maxexp" class="custom-input f-12 h-2_4 mar-0 pad-r5 pad-l0 pad-y0 d-if border-0 mnw-100px">{{formatTotalAmount(maximumExpenditure)}}</p>
                                        </div>
                                    </div>
                                </h3>
                                <div class="color-input-wrapper ">
                                    <div id="sliderAvgExpen" class="custom-range-slider pink round-thumb-slider expenditure-slider"
                                        ng-change="avgExpenditureChange();" ng-model="myValue"></div>
                                </div>
                            </div>
                            <div class="custom-range-wrapper correction-range track-5 width-100 lead-range-wrapper" id="customerCount">
                                <h3 class="text-cream roboto-regular lh-22 va-m  text-capitalize mar-b25 f-14 c-arrow text-overflow">Customer
                                    Range
                                    <div class="clearfix pull-right">
                                        <div class="w-55 h-2_4 d-ib left mar-r10 input-group-mat border-r3">
                                            <p value="" id="minCus" disabled class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px">{{formatTotalAmount(minimumCustomerCount)}}</p>
                                        </div>
                                        <div class="w-55 h-2_4 d-ib input-group-mat border-r3">
                                            <p value="" id="maxCus" disabled class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px">{{formatTotalAmount(maximumCustomerCount)}}</p>
                                        </div>
                                    </div>
                                </h3>
                                <div class="color-input-wrapper ">
                                    <div id="sliderCustomerCount" class="custom-range-slider blue round-thumb-slider customer-slider"
                                        ng-change="avgExpenditureChange();" ng-model="myValue"></div>
                                </div>
                            </div>
                            <div class="custom-range-wrapper correction-range track-5 width-100 lead-range-wrapper">
                                <h3 class="text-cream roboto-regular text-capitalize mar-y10 f-14 c-arrow text-overflow">Significant
                                    Indicator

                                    <div class="w-55 h-2_4 d-ib input-group-mat pull-right border-r3">
                                        <input type="text" disabled ng-model="maxSignificantValueChange" class="custom-input f-12 h-2_4 mar-0 pad-x5 pad-y0 width-100 border-0 mnw-100px"
                                            id="">
                                    </div>
                                </h3>
                                <input type="range" min="1" max="{{maxSignificantValue}}" step="0.1" ng-model="maxSignificantValueChange"
                                    class="bg-transparent pad-y5" ng-change="setSignificantIndicator(maxSignificantValueChange)">
                            </div>
                        </div>
                    </div>
                    <!-- Segment Panel Ends -->
                    <!-- Segment Panel Starts -->
                    <div class="panel row custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading p-rel pad-t10 pad-b0">
                            <p class="text-cream roboto-light text-capitalize c-arrow f-18">Cluster Attributes</p>
                        </div>
                        <div class="panel-body  mnh-100px" style="position: relative; overflow: visible;">
                            <div id="uc2Bar" class=""></div>
                        </div>
                    </div>
                    <div class="panel">

                        <div class="panel-heading p-rel pad-t10 pad-b0">
                            <p class="text-cream roboto-light text-capitalize c-arrow f-18">Attributes in the Cluster</p>
                        </div>
                        <div class="panel-body border-b0">
                            <ul class=" pad-l0 custom-list item-2 mxh-250 add-scroll-list">
                                <li ng-repeat="legendData in clusterLegend" class="text-overflow text-capitalize text-cream f-12 c-arrow">
                                    <span class="fa fa-circle" style="margin-right:5px;color:{{legendData.color}}">
                                    </span>
                                    {{legendData.name}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Segment Panel Ends -->

        </div>
        <!--  Twelfth Column Ends  -->

        <!-- Left Col Ends -->
        <div class="side-remain " ng-class="{'width-100':showProgressBar,'mxh-dashboard':!showProgressBar}">
            <!--  Main Filters Wrapper Starts  -->
            <div class="main-filters-wrapper ">
                <!--  Top Filters Wrapper Starts  -->
                <div class="top-filters-wrapper dark-black-panels segment-filters-wrappers" ng-show="!showComparisonPageBreadCrumbs">
                    <ul class="top-filter-navigation width-100 dual-filter-list list-inline">
                        <!-- ng-class="{'width-100':showProgressBar}" -->
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">Age</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item " ng-repeat="age in ageData">
                                            <div class="left-col c-arrow f-12">{{age.start}}<span ng-if="age.start != age.end">-{{age.end}}</span></div>

                                            <div class="progress progress-gradient-grey" ng-click="progressChartClick(age,'#age')">
                                                <div class="progress-bar progress-gradient-curious-blue" ng-class="{'progress-light-orange':age.highlight,'progress-dark-orange':!age.highlight}"
                                                    role="progressbar" aria-valuenow="{{age.progress}}" aria-valuemin="0"
                                                    aria-valuemax="100" style="width:{{age.progress}}%">
                                                    <span class="sr-only ">{{age.progress}}% Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col hidden">AGE({{age.start}}<span ng-if="age.start != age.end">-{{age.end}}</span>)
                                                :
                                                {{age.customer_count}}
                                                <span ng-if="hidingOnfilter">({{age.progress | number: 2}}%)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">Gender</h4>
                                <div class="row mar-x0 top-filters-charts-wrapper">

                                    <div class="pie-charts-wrapper  z-999">
                                        <div id="gender_Pie"></div>
                                    </div>
                                    <ul class="filters-subhead-wrapper pad-t5 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                        <li ng-repeat="legendData in  gender_PieLegendData" class="filters-subhead c-arrow" ng-click = "legendClickForPie(legendData.container,legendData.piedata,legendData.arcData)">
                                            <a href="javascript:void(0);" class="text-cream c-pointer">
                                                <i style="color: {{legendData.color}} !important" class="fa fa-circle pad-r5"></i>{{legendData.name}}
                                                <div class="right-col" style="display:none">
                                                    {{legendData.name}}: {{legendData.formattedVal}}
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">AS Score</h4>
                                <div class="row mar-x0 top-filters-charts-wrapper">
                                <div class="pie-charts-wrapper  z-999">
                                    <div id="asScoreChart"></div>
                                </div>
                                    <ul class=" filters-subhead-wrapper pad-t5 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                    <li ng-repeat="legendData in  asScoreLegendData" class="filters-subhead c-arrow" ng-click = "legendClickForPie(legendData.container,legendData.piedata,legendData.arcData)">
                                        <a href="javascript:void(0);" class="text-cream c-pointer">
                                            <i style="color: {{legendData.color}} !important" class="fa fa-circle pad-r5"></i>{{legendData.name}}
                                            <div class="right-col" style="display:none">
                                           		{{legendData.name}}: {{legendData.value}}
                                           	 </div>
                                        </a>
                                    </li>
                                </ul>
                               </div>
                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">BS Score</h4>
                                <div class="row mar-x0 top-filters-charts-wrapper">
                                        <div class="pie-charts-wrapper  z-999">
                                                <div id="bsScoreChart"></div>
                                            </div>
                                <ul class=" filters-subhead-wrapper pad-t5 width-36 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                    <li ng-repeat="legendData in  bsScoreLegendData" class="filters-subhead c-arrow" ng-click = "legendClickForPie(legendData.container,legendData.piedata,legendData.arcData)" >
                                        <a href="javascript:void(0);" class="text-cream c-pointer">
                                            <i style="color: {{legendData.color}} !important" class="fa fa-circle pad-r5"></i>{{legendData.name}}
                                           	<div class="right-col" style="display:none">
                                           		{{legendData.name}}: {{legendData.value}}
                                           	 </div>
                                        </a>
                                    </li>
                                </ul>
                                
                            </div></div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">Card Duration(yrs)</h4>
                                <div class="row mar-x0 top-filters-charts-wrapper">
                                    <div class="pie-charts-wrapper  z-999">
                                        <div id="card_Duration_Pie"></div>
                                    </div>
                                    <ul class=" filters-subhead-wrapper pad-t5 width-36 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                        <li ng-repeat="legendData in  card_Duration_PieLegendData" class="filters-subhead c-arrow" ng-click = "legendClickForPie(legendData.container,legendData.piedata,legendData.arcData)">
                                            <a href="javascript:void(0);" class="text-cream c-pointer">
                                                <i style="color: {{legendData.color}} !important" class="fa fa-circle pad-r5"></i>{{legendData.name}}
                                                <div class="right-col" style="display:none">
                                                    {{legendData.name}}: {{legendData.formattedVal}}
                                                </div>
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading c-arrow">Card Types</h4>
                                <div class="row mar-x0 top-filters-charts-wrapper">

                                    <div class="pie-charts-wrapper  z-999">
                                        <div id="card_Type_Pie"></div>
                                    </div>
                                    <ul class=" filters-subhead-wrapper pad-t5 width-36 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                        <li ng-repeat="legendData in  card_Type_PieLegendData" class="filters-subhead c-arrow" ng-click = "legendClickForPie(legendData.container,legendData.piedata,legendData.arcData)">
                                            <a href="javascript:void(0);" class="text-cream c-pointer">
                                                <i style="color: {{legendData.color}} !important" class="fa fa-circle pad-r5"></i>{{legendData.name}}
                                                <div class="right-col" style="display:none">
                                                    {{legendData.name}}: {{legendData.formattedVal}}
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
                <!--  Top Filters Wrapper Ends  -->

            </div>
            <!--  Main Filters Wrapper Ends  -->

            <!-- Main Segment Content Starts Here -->
            <div class="row main-segment-wrapper dark-black-panels">
                <div class="col-sm-12 pad-r0 custom-col left-col">
                    <div id="noData" class="mnh-300 p-rel" style="" ng-show="!dualcardObject.chord_chart_spinner"></div>
                    <div class="details-cheart-wrapper" ng-class="{'d-none':showProgressBar}">
                        <div ng-show="dualcardObject.chord_chart_spinner" class="custom-spinner full-page-spinner">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <div class="col-sm-8" id="chordDiagram">
                            <div id="customerUsageChart" class="" style="height:800px;width:800px;"></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel custom-panel-wrapper club-data-wrapper dark-black-panels border-0" id="sideBarForChordClick">
                                <div class="panel-body pad-x15 pad-b15 pad-t0">
                                    <div class="details-collection-wrapper pad-t25 h-320">
                                    </div>
                                    <div class="bar-chart-wrapper mar-top-20 mnh-300 p-rel">
                                        <div ng-show="clubTablePreloader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                        <table class="custom-data-table-wrapper leadgen-table sort-table table table-scroll leadgen-four-col col-four-unequal">
                                            <thead class="pad-r15" style="border-bottom: 1px solid #5d5d5d;">
                                                <tr>
                                                    <th class="chordTableData_tooltip text-capitalize c-arrow">Clubs
                                                        ({{chordTableData.length}})</th>
                                                    <th class="td-width-14 text-right pad-x0 fontWeightBold"><span
                                                            class="fa fa-users"></span></th>
                                                    <th class="text-capitalize text-right fontWeightBold c-arrow">Total
                                                        ( <span class="fa fa-ils"></span>)</th>
                                                    <th class="chordTablePercenatge_tooltip pad-x0 text-capitalize c-arrow fontWeightBold td-width-30"><span>Strength
                                                            (%)</span> <span class="fa fa-long-arrow-down text-dark-cream  f-14"></span></th>
                                                </tr>
                                            </thead>
                                            <tbody class="scroll-table-chord mxh-300 pad-r15 c-arrow">
                                                <tr ng-repeat="data in chordTableData | orderBy : '-avg_expenditure_percentage'">
                                                    <td><span ng-mouseover="chordTableFirstColTooltip(data.club_name)"
                                                            class="text-overflow height-a fontWeightNomal text-capitalize chordTableFirstCol_tooltip">{{data.club_name.toLowerCase()}}</span></td>
                                                    <td class="td-width-14 pad-x0"><span class="fontWeightNomal">{{formatTotalAmount(data.customer_count)
                                                            }}</span></td>
                                                    <td class=""><span class="fontWeightNomal">{{formatTotalAmount(data.total_expenditure)}}</span></td>
                                                    <td class="p-rel pad-r15"><span class="fontWeightNomal">{{data.avg_expenditure_percentage}}%</span><span
                                                            ng-click="shortcutToConnetion(data.club_name, data)" class="chordTableArrow_tooltip fa fa-angle-right text-dark-blue p-abs t-5 lh-18 c-pointer va-t"
                                                            style=""></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div ng-show="chordTableData.length == 0 && !clubTablePreloader" class="leadGenErrorDiv no-data-wrapper text-center mnh-300">
                                            <span class="no-data-wrapper">DATA NOT FOUND</span>
                                        </div>
                                        <!--For tool tip-->
                                        <!-- <span class="right-col hidden">{{chordClickName}}({{chordTableData.length}})</span> -->
                                        <span class="right-col-arrow hidden">Click for Connection Details</span>
                                        <span class="right-col-table-first-col hidden">{{chordTableFirstColTooltipScope}}</span>
                                        <span class="right-col-table-percentage text-capitalize hidden">Connection
                                            Strength based on Average expenditure</span>
                                        <!--For tool tip end-->
                                        <!-- <div ui-grid="gridOptions" ui-grid-selection class="grid-club"></div> -->
                                    </div>
                                </div>
                            </div>

                            <div class="panel custom-panel-wrapper connection-data-wrapper dark-black-panels border-0"
                                id="ribbonInfo">
                                <div class=" panel-heading pad-x0  bg-transparent pad-t25">
                                    <h3 class="f-18 text-cream mar-b0  text-capitalize c-arrow">
                                        {{connectionDetails.club_one_name.toLowerCase() + " - " +
                                        connectionDetails.club_two_name.toLowerCase()}}
                                    </h3>
                                </div>
                                <div class="panel-body pad-x0 pad-b15 pad-t0">
                                    <div class="connection-collection-wrapper c-arrow">
                                        <h4><a href="javascript:void(0);" ng-click="showComparisonPage('comparisonRibbonInfo')"
                                                class="d-flex pad-t10">go
                                                to connection details <span class="fa fa-angle-right pull-right"></span></a></h4>
                                        <p class="d-ib" style="float:left"> Total Customers in the connections <span
                                                class="fa fa-info-circle text-info-blue clusterDetail_Customer_Percentage"
                                                style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p>
                                        <strong class="pull-right">
                                            {{connectionDetails.common_customer_count}} <span class="clusterDetail_Customer_Percentage">
                                                ({{connectionDetails.connection_cust_percentage}}%)</span>
                                        </strong>
                                        <div class="right-col hidden">Percentage from the number of customers in
                                            cluster : {{connectionDetails.connection_cust_percentage}}%
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body pad-x0 pad-b15 pad-t10 cust-tot-exp-wrapper c-arrow">
                                    <p class="d-ib c-arrow" style="float:left">Total expenditure in the connections
                                        <span class="fa fa-info-circle text-info-blue clusterDetail_Customer_tot_exp_Percentage"
                                            style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p>

                                    <strong class="pull-right c-arrow">
                                        <span class="fa fa-ils f-16 d-inline"></span>
                                        {{formatTotalAmount(connectionDetails.total_expenditure)}} <span class="clusterDetail_Customer_tot_exp_Percentage">
                                            ({{connectionDetails.total_expenditure_percentage}}%)</span>
                                    </strong>
                                    <div class="right-col-cust-tot-exp hidden">Percentage from the total expenditure in
                                        Cluster : {{connectionDetails.total_expenditure_percentage}}%
                                    </div>
                                </div>
                                <div class="panel-body pad-x0 pad-b15 d-none pad-t0">
                                    <div class="pie-chart-data-details">
                                        <div class="top-heading">
                                            <h5 class="width-100 mar-b10 c-arrow">Total Customers</h5>
                                        </div>
                                        <div class="pie-chart-data">
                                            <div id="connection_tot_customers_Pie"></div>
                                        </div>
                                        <div class="chart-data-table-wrapper">
                                            <table class="custom-data-table-wrapper sort-table table table-scroll three-col col-four-unequal">
                                                <tbody class="scroll-table-chord">
                                                    <tr ng-repeat="data in connectionPieData">
                                                        <!--<td><span></span>{{data.key}}</td> -->
                                                        <td>{{data.value}}</td>
                                                        <td>{{data.percentage}}%</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body pad-x0 pad-b15 pad-t0">
                                    <div class="top-heading c-arrow">
                                        <h5 class="width-100 mar-b10">Average Expenditure per customer</h5>
                                    </div>
                                    <div class="bar-chart-wrapper">
                                        <div id="connection_avg_bar" style="margin-top: -15px"></div>
                                    </div>
                                </div>
                                <div class="panel-body pad-x0 pad-b15 pad-t0">
                                    <div class="top-heading c-arrow">
                                        <h5 class="width-100 mar-b10">Total Expenditure</h5>
                                    </div>
                                    <div class="bar-chart-wrapper">
                                        <div id="connection_total_bar" style="margin-top: -15px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="vlaCom"></div>
                <div class="comparison-chart-wrapper" ng-class="{'d-none':!showProgressBar}">
                    <div class="" id="connectionInfoTooltip">
                        <div class="side-defined pad-x15 pad-t25 pull-left">
                            <div class="panel row custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading clearfix border-thin-dark-cream-b p-rel pad-t10 pad-b0">
                                    <h3 class="text-cream mar-b0 pull-left height-a text-capitalize c-arrow">Connection
                                        information
                                    </h3>
                                </div>

                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <div class="pad-x0 pad-y0 cluster-details-wrapper">
                                        <p class="details-name mar-b0  text-cream roboto-regular d-ib c-arrow"><span
                                                class="fa fa-users pull-left  f-14 pad-r5"></span>Total
                                            Customers
                                            <span class="fa fa-info-circle text-info-blue comparison_total_cust_tooltip"
                                                style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span>
                                        </p>
                                        <strong class="details-value text-right roboto-bold c-arrow">

                                            {{formatTotalAmount(connectionDetails.common_customer_count)}}
                                            <span class="comparison_total_cust_tooltip">
                                                ({{connectionDetails.connection_cust_percentage}}%)</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <div class="pad-x0 pad-y10 cluster-details-wrapper">
                                        <p class="details-name mar-b5 d-ib va-m text-cream roboto-regular c-arrow">
                                            Total Expenditure
                                            <span class="fa fa-info-circle text-info-blue comparison_total_exp" style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span>
                                        </p>
                                        <strong class="details-value text-right roboto-bold c-arrow">

                                            <span class="fa fa-ils va-m f-14"></span>
                                            {{formatTotalAmount(connectionDetails.total_expenditure)}}
                                            <span class="comparison_total_exp">
                                                ({{connectionDetails.total_expenditure_percentage}}%)</span>
                                        </strong>

                                    </div>
                                    <div class="pad-x0 pad-y10 cluster-details-wrapper">
                                        <p class="details-name mar-b5 text-cream roboto-regular d-ib c-arrow">Average
                                            Expenditure <span class="fa fa-info-circle text-info-blue comparison_avg_exp"
                                                style="font-size: 12px;vertical-align: middle;padding-left: 2px;"></span></p>

                                        <strong class="details-value text-right roboto-bold c-arrow">
                                            <span class="fa fa-ils f-14"></span>
                                            {{formatTotalAmount(connectionDetails.avg_expenditure)}} <span class="details-value comparison_avg_exp">
                                                ({{connectionDetails.avg_expenditure_percentage}}%)</span>
                                        </strong>

                                    </div>
                                </div>
                                <!--For tooltip-->
                                <span class="right-col-cust hidden">Percentage from the number of customers :
                                    {{connectionDetails.connection_cust_percentage}}%</span>
                                <span class="right-col-total-exp hidden">Percentage from the total expenditure :
                                    {{connectionDetails.total_expenditure_percentage}}%</span>
                                <span class="right-col-avg-exp hidden">Percentage from the average expenditure :
                                    {{connectionDetails.avg_expenditure_percentage}}%</span>
                                <!--For tooltip end-->
                            </div>
                            <div class="panel-body hidden">
                                <div class="select-group mar-y10 text-cream input-group-mat pad-x0 width-100 pad-y10 custom-select-dropdown">

                                    <select class="filter-value text-cream  custom-input bg-transparent pull-right border-0  width-100 pad-l5 ">
                                        <option>Heaver Club</option>
                                    </select>
                                    <span class=" label f-10 l-0">Club-A</span>
                                </div>
                                <div class="select-group mar-y10 text-cream input-group-mat pad-x0 width-100 pad-y10 custom-select-dropdown">
                                    <select class="filter-value text-cream  custom-input bg-transparent pull-right border-0  width-100 pad-l5 ">
                                        <option>Link</option>
                                    </select>
                                    <span class=" label f-10 l-0">Club-B</span>
                                </div>
                            </div>
                            <div class="panel-body hidden">
                                <div class="select-group mar-y10 text-cream input-group-mat pad-x0 width-100 pad-y10 custom-select-dropdown">
                                    <select class="filter-value text-cream  custom-input bg-transparent pull-right border-0  width-100 pad-l5 ">
                                        <option>Customer Number</option>
                                    </select>
                                    <span class=" label f-10 l-0">Chart According to</span>
                                </div>
                                <div class="col-sm-12">
                                    <div id="slider3" class="custom-range-slider" ng-change="avgExpenditureChange();"
                                        ng-model="myValue"></div>
                                </div>
                                <div class="col-sm-12">
                                    <div id="slider4" class="custom-range-slider" ng-change="avgExpenditureChange();"
                                        ng-model="myValue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="side-remain">
                            <!-- ng-class="{'width-100':showProgressBar}" -->
                            <div class="">
                                <span ng-show="addMediaPreloader" class="custom-spinner case-dairy-spinner">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span>
                                <div class="club-body-wrapper">
                                    <div class="club-top-content">
                                        <div class="row club-top-grid">
                                            <div class="col-sm-10">
                                                <div class="col-sm-6 text-right club-budget">

                                                    <div class="comapny-name d-block text-capitalize lh-22 text-cream f-18 c-arrow">{{avoidClubKeys[0]}}
                                                    </div>
                                                    <div class="customer-spending-wrapper d-block" style="position: relative; overflow: visible;">
                                                        <div class="d-ib">
                                                            <strong class="details-value f-18 text-dark-cream roboto-bold text-capitalize c-arrow">(Avg
                                                                .Exp<span class="fa fa-ils f-16 mar-t0 text-dark-cream"></span>{{formatTotalAmount(clubDetails[0]['avgExp'].toFixed(2))}})</strong>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 club-filter">
                                                    <div class="comapny-name d-block text-capitalize text-cream f-18 c-arrow">{{avoidClubKeys[1]}}
                                                    </div>
                                                    <div class="customer-spending-wrapper d-block pull-left" style="position: relative; overflow: visible;">
                                                        <div class="d-ib">
                                                            <strong class="details-value f-18 text-dark-cream roboto-bold text-capitalize c-arrow">(Avg
                                                                .Exp <span class="fa fa-ils f-16 mar-t0 text-dark-cream pad-r5"></span>{{formatTotalAmount(clubDetails[1]['avgExp'].toFixed(2))}})</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="club-bottom-content transaction-ocbc-wrapper-split-view">
                                        <div class="row compareSplit">
                                            <div class="comapny-name d-ib text-capitalize lh-22 text-cream f-18 width-100 text-center pad-top-2 c-arrow">Merchant
                                                Deviation based on Average Expenditure</div>
                                            <div class="col-md-10 " id="compareChart">
                                                <div class="panel custom-panel-wrapper pad-y0  segment-panel-wrapper">
                                                    <div class="panel-body row pad-b0" style="position: relative; overflow: visible;">
                                                        <div class="vla-club-wrapper vlaCompareMain">
                                                            <div ng-show="dualcardObject.firstvlachartspinnner" class="custom-spinner full-page-spinner">
                                                                <i class="fa fa-spinner fa-spin fa-3x"></i>
                                                            </div>
                                                            <div id="vlaCompare1" style="position: absolute;min-width:320px;"
                                                                class="height-100 width-100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  pad-t40">
                                                <ul class=" pad-l0 custom-list item-1 mxh-400 comparechart-legend-wrapper add-scroll-list">
                                                    <li ng-repeat="legendData in comparisonProgressBarLegendData" class="ws-normal text-capitalize pad-t10 pad-l15 text-cream f-12" style="pointer-events: none;">{{colorObj}}
                                                        <span style="width:12px;height:12px;font-size:8px;border-radius:50%;position:absolute;left:0;top:13px;margin-right:5px;background-color:{{legendData.color}};">
                                                        </span>
                                                        {{legendData.name}}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
            <!-- Main Segment Content ends Here -->
        </div>
    </div>



</div>
<!--  Dual Card Dashboard Wrapper Ends  -->

<script>
    $(document).ready(function () {
        setTimeout(function () {
            $(".top-filters-wrapper").mThumbnailScroller({
                axis: "x"
            });
        }, 0);
    });
    $(document).ready(function () {
        setTimeout(function () {
            $(".remote-filters-wrapper").mThumbnailScroller({
                axis: "x",
                advanced: {
                    updateOnSelectorChange: true
                }
            });
        }, 0);
    });
    $(".panel .panel-scroll").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $(document).ready(function () {
        setTimeout(function () {
            $(".filters-subhead-wrapper,.mxh-dashboard, .scroll-table-chord").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
        }, 0);
    });

    $(".panel-subhead-wrapper").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $(".add-scroll-list").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });

    /**
     * Function to get tool tip for progres bars
     * 
     */
    $("body").on("mouseover", ".progressbar-list-item", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".progressbar-list-item", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($(this).find(".right-col").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for Total expenditure in the connections
     * 
     */
    $("body").on("mouseover", ".clusterDetail_Customer_Percentage", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".clusterDetail_Customer_Percentage", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX - 250 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.style("top", event.pageY + "px");
            div.html($(".connection-collection-wrapper .right-col").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    $("body").on("mouseover", ".clusterDetail_Customer_tot_exp_Percentage", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".clusterDetail_Customer_tot_exp_Percentage", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX - 250 - $(".clusterDetail_Customer_tot_exp_Percentage").width() +
                "px");
            div.style("top", event.pageY + "px");
            div.html($(".cust-tot-exp-wrapper .right-col-cust-tot-exp").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for club table arrow
     * 
     */
    $("body").on("mouseover", ".chordTableFirstCol_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".chordTableFirstCol_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("top", event.pageY + "px");
            div.style("left", event.pageX + 10 + "px");
            div.html($("#sideBarForChordClick .bar-chart-wrapper .right-col-table-first-col").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    $("body").on("mouseover", ".chordTablePercenatge_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".chordTablePercenatge_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("top", event.pageY + "px");
            div.style("left", event.pageX - 170 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.html($("#sideBarForChordClick .bar-chart-wrapper .right-col-table-percentage").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for club table arrow
     * 
     */
    $("body").on("mouseover", ".chordTableArrow_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".chordTableArrow_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("top", event.pageY + "px");
            div.style("left", event.pageX - 170 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.html($("#sideBarForChordClick .bar-chart-wrapper .right-col-arrow").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chordTableData
     * 
     */
    $("body").on("mouseover", ".comparison_total_cust_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".comparison_total_cust_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($(".comparison-chart-wrapper #connectionInfoTooltip .right-col-cust").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });
    /**
     * Function to get tool tip for chordTableData
     * 
     */
    $("body").on("mouseover", ".comparison_total_exp", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".comparison_total_exp", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($(".comparison-chart-wrapper #connectionInfoTooltip .right-col-total-exp").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });
    /**
     * Function to get tool tip for chordTableData
     * 
     */
    $("body").on("mouseover", ".comparison_avg_exp", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".comparison_avg_exp", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($(".comparison-chart-wrapper #connectionInfoTooltip .right-col-avg-exp").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#chordDaigram_tooltip .p-rel  span.value_for_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#chordDaigram_tooltip .p-rel  span.value_for_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX - 170 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.style("top", event.pageY + "px");
            div.html($("#chordDaigram_tooltip .p-rel .right-col").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#chordDaigram_tooltip .p-rel  span.primary_value_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#chordDaigram_tooltip .p-rel  span.primary_value_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX - 250 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.style("top", event.pageY + "px");
            div.html('<i class="fa fa-info-circle"></i>' + $(
                "#chordDaigram_tooltip .p-rel .right-col-percentage").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#chordDaigram_tooltip .p-rel  span.total_exp_for_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#chordDaigram_tooltip .p-rel  span.total_exp_for_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX - 250 - $(".clusterDetail_Customer_Percentage").width() + "px");
            div.style("top", event.pageY + "px");
            div.html($("#chordDaigram_tooltip .p-rel .right-col-total-expenditure").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#idForToolTips .panel-body  span.details_avg_exp", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#idForToolTips .panel-body span.details_avg_exp", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($("#idForToolTips span.right-col-avg-exp-cluster").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#idForToolTips .panel-heading  h3.details_avg_exp", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
            div.style("word-break", "break-all");
        }

    }).on("mousemove", "#idForToolTips .panel-heading h3.details_avg_exp", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($("#idForToolTips span.right-col-cluster-name").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });


    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#idForToolTips .panel-body  span.details_total_cust_tooltip", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#idForToolTips .panel-body  span.details_total_cust_tooltip", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($("#idForToolTips span.right-col-no-of-cust").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    /**
     * Function to get tool tip for chord Data
     * 
     */
    $("body").on("mouseover", "#idForToolTips .panel-body  span.details_total_exp", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", "#idForToolTips .panel-body  span.details_total_exp", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($("#idForToolTips span.right-col-total-exp-cluster").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    });

    // Function to show tool tips for pie legends
    $("body").on("mouseover", ".top-filters .filters-subhead .c-pointer", function () {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
            div.style("text-transform", "capitalize!important");
        }
    }).on("mousemove", ".top-filters .filters-subhead .c-pointer", function (event) {
        if (location.hash.indexOf('dualCardHolderDetails') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            var txt = "<span style='text-transform:capitalize !important'>" + $(this).find(".right-col").text() +
                "</span>"
            div.html(txt);

        }
    }).on("mouseout", function () {
        div.style("display", "none");
    })
</script>
<script>
    function collision($div1, $div2) {
        var x1 = $div1.offset().left;
        var w1 = 40;
        var r1 = x1 + w1;
        var x2 = $div2.offset().left;
        var w2 = 40;
        var r2 = x2 + w2;

        if (r1 < x2 || x1 > r2) return false;
        return true;


    }
    // window.onscroll = function (e) {
    //     if (this.scrollY > 147) {
    //         $("#chordDaigram_tooltip").addClass("fixed-tooltip");
    //     } else {
    //         $("#chordDaigram_tooltip").removeClass("fixed-tooltip");
    //     }
    // };
</script>

<style>
</style>