<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->
        <!--  Details Dashboard Wrapper Starts  -->
        <style>
            .min_height63 {
                min-height: 65px !important;
            }
            #inputHeadHeight{
                height: 25px !important;
            }
            #customerList .table tbody tr td.alignRight
            {
                text-align: right !important;
            }
        </style>
        <div class="dashboard-wrapper lead-generation-wrapper details-dashboard-wrapper bg-dark-grey1">
            <div class="custom-spinner full-page-spinner" ng-show="fullpageloader">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
            </div>
            <!--  Main Filters Wrapper Starts  -->
            <div class="main-filters-wrapper">
                <!-- Top Grid Wrapper-->
                <div class="top-grid-wrapper container-fluid">
                    <ul class="list-inline custom-tabs row top-filters-list">
                        <li class="col-sm-6">
                            <div class="lead-heading-list width-35 pull-left c-pointer" ui-sref="cluster" ng-if="hideStories">
                                <!-- <span class="toggler pull-left c-pointer mar-b10 mar-r5 fa fa-bars f-18 text-dark-blue"> </span> -->
                                <h3 class="heading mar-0 p-rel two-line-wrapper width-100 f-13 ws-normal text-uppercase">
                                    {{israCardClusterViewVariable.StoryName.split('_').join(' ')}}
                                    <!-- <span class="icon-right fa fa-angle-right f-21 text-dark-grey"></span> -->
                                </h3>
                            </div>
                            <div class="customer-ids width-40">
                                <p class="top-heading text-dark-blue pad-l15 d-flex pad-t0 mar-0 p-rel f-16 text-overflow">
                                    <span class="  fa fa-users pad-t3 pad-r5"></span>
                                    <span class="cluster-name f-13 two-line-wrapper mar-autor ws-normal c-arrow" ng-if="!clusterClick">{{israCardClusterViewVariable.clusterDetail.cluster_name}}</span>
                                    <span ng-show="!clusterClick" class="text-dark-grey c-pointer pull-right fa fa-pencil" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Edit" popover-trigger="'mouseenter'" ng-click="clusterClick = true"></span>
                                    <input ng-show="clusterClick" ng-model="israCardClusterViewVariable.clusterDetail.cluster_name" type="text" name="" id="inputHeadHeight"
                                        class="custom-input h-16 pad-0 pad-b5 pad-x5 border-blue-thin-b pad-r10" ng-keydown="clusterClick= israCardClusterViewFunction.renameClusterNameKeyUp($event)">
                                    <span ng-show="clusterClick" class="f-18 mar-r10 c-pointer text-dark-grey  fa fa-check" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Save" popover-trigger="'mouseenter'" ng-click="clusterClick = false;israCardClusterViewFunction.renameClusterName(israCardClusterViewVariable.clusterDetail.cluster_name)"></span>
                                    <span ng-show="clusterClick" class="f-18 mar-r10 c-pointer text-dark-grey  fa fa-times" popover-class="top-pop-wrapper" popover-append-to-body="'true'" popover-animation="true" uib-popover="Cancel" popover-trigger="'mouseenter'" ng-click="clusterClick = false;israCardClusterViewFunction.renameClusterName(israCardClusterViewVariable.clusterDetail.cluster_name)"></span>
                                </p>
                            </div>
                            <div class="customer-count  mar-t5 width-20">
                                <strong class="value mar-b10 c-arrow">{{israCardClusterViewVariable.clusterDetail.cluster_user_count | number}}</strong>
                                <h4 class="top-grid-heading mar-b0 c-arrow">Total Customers</h4>
                            </div>
                        </li>
                        <li class="col-sm-6 pad-t0">
                            <div class="custom-range-wrapper width-40 pull-left no-thumb-border lead-range-wrapper" id="sliderDiv">
                                <p class="  mar-b5 text-center f-13 text-uppercase c-arrow">Significant
                                    <span class="pull-right text-dark-blue c-arrow">({{israCardClusterViewVariable.standardDeviationValue}})</span>
                                </p>
                                <input type="range" id="significant" class="" max="5" min="1" step="0.1" ng-model="israCardClusterViewVariable.standardDeviationValue"
                                    ng-change="israCardClusterViewFunction.deviationChange(israCardClusterViewVariable.standardDeviationValue)">
                            </div>
                            <div class="top-filters segment-filters-wrappers pull-right">
	                                <!--  Progressbar List Starts -->
	                                <ul class="list-unstyled progressbar-list">
	                                    <li class="progressbar-list-item list_item_for_tooltip pad-t0">
	                                        <div class="left-col c-arrow">Average Expenditure</div>
	                                        <div class="progress c-arrow">
	                                            <div class="progress-bar progress-dark-orange" role="progressbar" aria-valuenow="{{israCardClusterViewVariable.avgExpenBar[0]['perValue']}}"
	                                                aria-valuemin="0" aria-valuemax="100" style="width:{{israCardClusterViewVariable.avgExpenBar[0]['perValue']}}%">
	                                                <span class="sr-only ">{{israCardClusterViewVariable.avgExpenBar[0]['perValue']}} Complete</span>
	                                            </div>
	                                        </div>
	                                        <div class="right-col c-arrow">₪ {{ formatNummerWithComma(israCardClusterViewVariable.avgExpenBar[0]['value'])}}</div>
	                                    </li>
	                                    <li class="progressbar-list-item list_item_for_tooltip">
	                                        <div class="left-col c-arrow">GP Average</div>
	                                        <div class="progress c-arrow">
	                                            <div class="progress-bar progress-dark-orange" role="progressbar" aria-valuenow="{{israCardClusterViewVariable.generalExpenBar[0]['perValue']}}"
	                                                aria-valuemin="0" aria-valuemax="100" style="width:{{israCardClusterViewVariable.generalExpenBar[0]['perValue']}}%">
	                                                <span class="sr-only ">{{israCardClusterViewVariable.generalExpenBar[0]['perValue']}} Complete</span>
	                                            </div>
	                                        </div>
	                                        <div class="right-col c-arrow">₪ {{ formatNummerWithComma(israCardClusterViewVariable.generalExpenBar[0]['value'])}}</div>
	                                    </li>
	                                </ul>
	                                <!--  Progressbar List Ends -->
                            </div>
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="clusterDetails.length === 0">
                                <span>Data Not Found</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--Top Grid Wrapper Ends-->
            </div>
            <!-- Main Segment Content Starts Here -->
            <div class="main-segment-content">
                <!-- Third Design Starts Here -->
                <div class="gen-det-wrapper">
                    <div class="row mar-0">
                        <!-- Right Col Starts -->
                        <div class="col-sm-4 custom-col left-col" onscroll="scrolled(this)">
                            <div class="top-table-content mar-b10">
                                <div id="dynamic-tabs" class="mar-b10">
                                    <div class="remote-filters-wrapper">
                                        <ul class="list-inline remote-filters-list-wrapper">
                                            <li class="remote-filters-list" ng-repeat="(k,data) in israCardClusterViewVariable.addedFilter" style="pointer-events:none;">
                                                <a class="remote-filter">
                                                    <span>{{data.showName}}</span>
                                                    <i class="fa fa-times c-pointer" ng-click="israCardClusterViewFunction.removeTab(k,data)" style="pointer-events:all;"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 class="c-arrow">Customer List</h3>
                                <div class="input-group custom-input-group pull-right">
                                    <span class="input-group-addon right-addon" id="basic-addon1">
                                        <i class="fa fa-search c-pointer"></i>
                                    </span>
                                    <input type="text" id="customerEleId" placeholder="Search" ng-model="customerSearch" class="form-control"  aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="transaction-adopters-wrapper p-rel  mnh-280 custom-data-table-wrapper" id="customerList">
                            	<div ng-show="customerListPreloader" class="custom-spinner">
					           		 <i class="fa fa-spinner fa-spin fa-3x"></i>
						        </div>
                                <table class="table border-b0 table-scroll two-col" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="text-left sorting c-arrow" rowspan="1" colspan="1" aria-label="Id">
                                                CUSTOMER ID
                                            </th>
                                            <th class="text-right sorting c-arrow" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending">
                                                Total Spending (₪)
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="custList" class="pad-r10 mxh-200">
                                        <tr id="" role="row" class="odd" ng-repeat="customer in israCardClusterViewVariable.customerData">
                                            <td>
                                                <a ui-sref="customer({cId: customer.customer_id})">
                                                    <h3 class="">
                                                        {{customer.customer_id}}
                                                    </h3>
                                                </a>
                                            </td>
                                            <td class="alignRight">
                                                <a ui-sref="customer({cId: customer.customer_id})" class="">
                                                    <strong>{{formatTotalAmount(customer.avg_spending)}} </strong>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
								 <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="israCardClusterViewVariable.customerData.length == 0 && !customerListPreloader">
                                    <span>{{custErrorMsg}}</span>
                                </div>
                                  <div class="text-right p-abs b-0 r-0 pagination-adoptors-wrapper" ng-style="{'display':false ? 'none': 'block'}" ng-if="israCardClusterViewVariable.customerData.length > 1">
                                    <ul class="transaction-alert-pagination mar-y0"  uib-pagination total-items="israCardClusterViewVariable.clusterDetail.cluster_user_count" items-per-page="50" ng-model="pageNum"
                                        ng-change="israCardClusterViewFunction.changeTablePageNo(pageNum)" max-size="2" class="pagination-sm"
                                        boundary-link-numbers="true">
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body pad-0" style="position: relative; overflow: visible">
                                <div class="row mar-x0">
                                    <div class="panel-heading pad-y10 pad-l0">
                                        <h3 class="pad-l10 c-arrow" style="color: #526C7B;">Cluster Attribute</h3>
                                    </div>
                                    <div class="pad-b10">
                                        <div id="clusterChart"></div>
                                    </div>
                                    <div class="chart-legend-wrapper h-75">
                                        <div class="d-ib mar-x5" ng-repeat="legendData in israCardClusterViewVariable.clusterLegend">
                                            <span style="width:12px;height:12px;font-size:12px;border-radius:50%;margin-right:5px;background-color:{{legendData.color}}">
                                            </span>
                                            <span style="font-size:12px;" class="c-arrow">{{legendData.name.split('_').join(" ").toUpperCase()}}</span>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="right-bottom-content border-b0 col-sm-8 pad-x0">
                            <div class=" col-sm-12 border-b0 custom-col middle-bottom-col pad-x0">
                                <div class="row mar-x0 custom-rows">
                                    <div class="col-sm-6 custom-panels pad-x0 border-b0 custom-row-panels">
                                        <!-- Segment Panel Ends -->
                                        <div class="panel custom-panel-wrapper segment-panel-wrapper">
                                            <div class="panel-heading pad-l0 pad-y10  tooltipBdges">
                                                <h3 class="pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('age')}">Age</h3>
                                                <h3 class='d-none'> Total customers by Age</h3>

                                            </div>
                                            <div class="panel-body h-120 border-b0 panel-scroll" style="position: relative; overflow: visible;">
                                                <div class="progress-bar-wrapper pad-r10">
                                                    <div class="list-unstyled progressbar-list">
                                                        <div class="progressbar-list-item age_list_for_tooltip two-col" ng-repeat="age in israCardClusterViewVariable.plotAgeData" ng-click="progressChartClick(age,'#age')"
                                                            ng-if="israCardClusterViewVariable.plotAgeData.length !=0">
                                                            <div class="left-col c-arrow">{{age.key}}</div>
                                                            <div class="progress progress-light-gray" ng-click="israCardClusterViewFunction.progressBarFilter('age',age)">
                                                                <div class="progress-bar" ng-class="{'progress-light-orange':age.highlight,'progress-dark-orange':!age.highlight}" role="progressbar"
                                                                    aria-valuenow="{{age.value}}" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width:{{age.value}}%">
                                                                    <span class="sr-only">{{age.value}}% Complete</span>
                                                                </div>
                                                                <div class="right-col" style="display:none">AGE({{age.start}}<span ng-if="age.start != age.end">-{{age.end}}</span>) :{{age.customer_count}}
                                                                    <span ng-if="hidingOnfilter">({{age.value | number: 2}}%)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="israCardClusterViewVariable.plotAgeData.length ==0">
                                                            <span>Failed To Get Data From server</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--  Progressbar List Ends -->
                                            </div>
                                        </div>
                                        <!-- Segment Panel Ends -->



                                    </div>
                                    <div class="col-sm-6 custom-panels border-b0 pad-x0 custom-row-panels">
                                        <!-- Segment Panel Starts -->
                                        <div class="panel pad-b10 custom-panel-wrapper segment-panel-wrapper ">
                                            <div class="panel-heading pad-y10 pad-l0 tooltipBdges">
                                                <h3 class="pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('Merchant')}">Merchant Type</h3>
                                                <h3 class='d-none'>Total Expenditure by Merchant Type</h3>

                                            </div>
                                            <div class="panel-body border-b0 h-120 min_height63" style="position: relative; overflow: visible;">
                                                <!--  Progressbar List Starts -->
                                                <div class="progress-bar-wrapper pad-r10" id="progressbar">
                                                    <div class="list-unstyled progressbar-list">
                                                        <div class="progressbar-list-item  large-progress two-col" ng-if="israCardClusterViewVariable.plotMerchantTypeData.length !=0"
                                                            ng-repeat="Score in israCardClusterViewVariable.plotMerchantTypeData">
                                                            <div class="left-col c-arrow">{{Score.key}}</div>
                                                            <div class="progress progress-light-gray" style="pointer-events:none;">
                                                                <div class="progress-bar" ng-class="{'progress-light-orange':Score.highlight,'progress-dark-orange':!Score.highlight}" role="progressbar"
                                                                    aria-valuenow="{{Score.tooltipValue}}" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width:{{Score.tooltipValue}}%">
                                                                    <span class="sr-only">{{Score.value}}% Complete</span>
                                                                </div>
                                                                <div class="right-col" style="display:none"><span>Merchant({{Score.key}}): </span><span class='fa fa-ils mar-l5 f-10'></span> {{formatNummerWithComma(Score.customer_count).split('.')[0] }} {{'('+(Score.tooltipValue
                                                                    | number: 2) +'%)'}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="israCardClusterViewVariable.plotMerchantTypeData.length ==0">
                                                            <span>Failed To Get Data From server</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Segment Panel Ends -->
                                        </div>
                                        <!-- Segment Panel Ends -->
                                    </div>
                                </div>
                                <div class="row mar-x0 custom-rows">
                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">
                                        <!-- Segment Panel Starts -->
                                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                            <div class="panel-heading pad-l0 pad-y10 tooltipBdges ">
                                                <h3 class="pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('gender')}">Gender</h3>
                                                 <h3 class='d-none'>Total customers by Gender</h3>

                                            </div>
                                            <div class="panel-body mnh-150" style="position: relative; overflow: visible">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                                        <li ng-repeat="legendData in israCardClusterViewVariable.genderLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#gender_Pie', israCardClusterViewVariable.genderLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.name.toUpperCase()}}
                                                            	 <div class="right-col" style="display:none">
	                                                        		{{legendData.name}}: {{legendData.formattedVal}}
	                                                        	 </div>
                                                             </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper" style="left:100px">
                                                        <div id="gender_Pie"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Segment Panel Ends -->



                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">

                                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                            <div class="panel-heading pad-l0 pad-y10 tooltipBdges ">

                                                <h3 class=" pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('banking_card')}">Banking Card
                                                </h3>
                                                <h3 class="d-none">Total customers by Banking Card</h3>
                                            </div>
                                            <div class="panel-body border-b0  " style="position: relative; overflow: visible;">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                                        <li ng-repeat="legendData in  israCardClusterViewVariable.bankingLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#card_Type_Pie', israCardClusterViewVariable.bankingLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.name}} 
                                                                 <div class="right-col" style="display:none">
	                                                        		{{legendData.name}}: {{legendData.formattedVal}}
	                                                        	 </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper">
                                                        <div id="card_Type_Pie"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">
                                        <!-- Segment Panel Starts -->
                                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                            <div class="panel-heading pad-y10 pad-l0 tooltipBdges">
                                                <h3 class="pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('as_Score')}">AS SCORE</h3>
                                                  <h3 class="d-none">Total customers by AS Score</h3>

                                            </div>
                                            <div class="panel-body mnh-150" style="position: relative; overflow: visible">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                                        <li ng-repeat="legendData in israCardClusterViewVariable.asScoreLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#as_score', israCardClusterViewVariable.asScoreLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.key.toUpperCase()}}
	                                                           	<div class="right-col" style="display:none">
	                                                        		{{legendData.key}}: {{legendData.value}}
	                                                        	 </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper">
                                                        <div id="as_score"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Segment Panel Ends -->
                                    </div>
                                </div>
                                <div class="row mar-x0 custom-rows">

                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">

                                        <!-- Segment Panel Starts -->
                                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                            <div class="panel-heading pad-l0 pad-y10 tooltipBdges">
                                                <h3 class="pad-l10 pad-y0 c-arrow" ng-class="{'custom-badge':checkToHighlight('dp')}">Day Part</h3>
                                                 <h3 class="d-none">Total Expenditure by Day Part</h3>

                                            </div>
                                            <div class="panel-body border-b0" style="position: relative; overflow: visible;height: 183px;">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                                        <li ng-repeat="legendData in israCardClusterViewVariable.dataPartLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#dataPart', israCardClusterViewVariable.dataPartLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer f-12">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.name.toUpperCase()}} 
                                                            	<div class="right-col" style="display:none">
	                                                        		{{legendData.name}}: <span class='fa fa-ils mar-l5 f-10'></span> {{legendData.formattedVal}}
	                                                        	 </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper" style="left: 100px;">
                                                        <div id="dataPart"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Segment Panel Ends -->
                                    </div>
                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">
                                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                            <div class="panel-heading pad-l0 pad-y10 tooltipBdges ">

                                                <h3 class=" pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('day')}">Day of the week
                                                </h3>
                                                
                                                 <h3 class="d-none">Total Expenditure by Day of the week</h3>
                                            </div>
                                            <div class="panel-body " style="position: relative; overflow: visible;">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify" style="padding-left: 0px;">
                                                        <li ng-repeat="legendData in israCardClusterViewVariable.dayOfLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#dayWeek', israCardClusterViewVariable.dayOfLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.name}} 
                                                                <div class="right-col" style="display:none">
	                                                        		{{legendData.name}}: <span class='fa fa-ils mar-l5 f-10'></span> {{legendData.formattedVal}}
	                                                        	</div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper">
                                                        <div id="dayWeek"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Segment Panel Starts -->
                                    <div class="col-sm-4 custom-col border-b0 right-col" style="background-color: #263842;">
                                        <div class="panel custom-panel-wrapper segment-panel-wrapper">
                                            <div class="panel-heading pad-y10 pad-l0 tooltipBdges">
                                                <h3 class=" pad-l10 c-arrow" ng-class="{'custom-badge':checkToHighlight('bs_Score')}"> BS SCORE </h3>
                                                 <h3 class="d-none">Total Customers by BS Score</h3>

                                            </div>
                                            <div class="panel-body mnh-150" style="position: relative; overflow: visible">
                                                <div class="row mar-x0 lead-charts-wrapper">
                                                    <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                                        <li ng-repeat="legendData in israCardClusterViewVariable.bsScoreLegend" class="filters-subhead c-arrow">
                                                            <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#bs_score', israCardClusterViewVariable.bsScoreLegend, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                                                <i style="color: {{legendData.color}} !important"></i>{{legendData.key.toUpperCase()}} 
                                                                 <div class="right-col" style="display:none">
	                                                        		{{legendData.key}}: {{legendData.value}}
	                                                        	 </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="pie-charts-wrapper">
                                                        <div id="bs_score"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Segment Panel Ends -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Right Col Ends -->



                </div>
            </div>
        </div>
        <!-- Third Design Starts Here -->
        <!--  Details Dashboard Wrapper Ends  -->
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".remote-filters-wrapper").mThumbnailScroller({
                        axis: "x",
                        advanced: { updateOnSelectorChange: true }
                    });
                }, 0);
            });

            $(".panel .panel-scroll").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
            $(".panel-subhead-wrapper").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });

            $(".filters-subhead-wrapper").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
            $('#custList').mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
            $(".main-filtered-user-wrapper , .chart-legend-wrapper").mCustomScrollbar({
                callbacks: {
                    onTotalScroll: function () {
                        angular.element('#autoScrollCluster').scope().clustersList_scroll();
                    }
                },
                axis: "y",
                theme: "minimal-dark"
            });

            /**
             * Function to get tool tip for progres bars
             * 
             */
             
            $("body").on("mouseover", ".progressbar-list-item", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                }

            }).on("mousemove", ".progressbar-list-item", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    //div.style("left", event.pageX + 10 + "px");
                    div.style("left", event.pageX + 80 - $(".progressbar-list-item").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($(this).find(".right-col").html());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            })

            /**
             * Function to get tool tip for progres bars
             * 
             */
             
             $("body").on("mouseover", ".age_list_for_tooltip", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                }

            }).on("mousemove", ".age_list_for_tooltip", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("left", event.pageX + 10 + "px");
                    //div.style("left", event.pageX + 220 - $(".age_list_for_tooltip").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($(this).find(".right-col").text());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            })

            /**
             * Function to get tool tip for progres bars
             * 
             */
             
             $("body").on("mouseover", ".list_item_for_tooltip", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                }

            }).on("mousemove", ".list_item_for_tooltip", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    //div.style("left", event.pageX + 10 + "px");
                    div.style("left", event.pageX + 220 - $(".list_item_for_tooltip").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($(this).find(".right-col").text());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            })
             /**
             * Function to get tool tip for Cluster Name
             * 
             */
             
             $("body").on("mouseover", ".lead-heading-list ", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                    div.style("word-break", "break-all");
                }

            }).on("mousemove", ".lead-heading-list", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    //div.style("left", event.pageX + 10 + "px");
                    div.style("left", event.pageX + 220 - $(".lead-heading-list").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($(this).find(".heading").text());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            })
            /**
             * Function to get tool tip for User Name
             * 
             */
             
             $("body").on("mouseover", ".top-heading .cluster-name", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                    div.style("word-break", "break-all");
                }

            }).on("mousemove", ".top-heading .cluster-name", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    //div.style("left", event.pageX + 10 + "px");
                    div.style("left", event.pageX + 50 - $(".top-heading .cluster-name").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($(this).text());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            });
            
           // Function to show tool tips for pie legends
            $("body").on("mouseover", ".lead-charts-wrapper .filters-subhead .c-pointer", function () {
               if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                   div.style("display", "inline-block");
                   div.style("visibility", "visible");
                  
               }
           }).on("mousemove", ".lead-charts-wrapper .filters-subhead .c-pointer", function (event) {
               if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                   div.style("left", event.pageX + 10 + "px");
                   div.style("top", event.pageY + "px");
                   var txt  ="<span style='text-transform:capitalize !important'>"+$(this).find(".right-col").html()+"</span>"
                   div.html(txt);
                   
               }
           }).on("mouseout", function () {
               div.style("display", "none");
           })
           
           
                 $("body").on("mouseover", ".tooltipBdges", function () {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    div.style("display", "inline-block");
                    div.style("visibility", "visible");
                    div.style("word-break", "break-all");
                }

            }).on("mousemove", ".tooltipBdges", function (event) {
                if (location.hash.indexOf('generalCardHolderDetails') >= 0) {
                    //div.style("left", event.pageX + 10 + "px");
                    div.style("left", event.pageX + 80 - $(".top-heading .cluster-name").width()+ "px");
                    div.style("top", event.pageY + "px");
                    div.html($($($(this)[0]).find('h3')[1]).text());
                }
            }).on("mouseout", function () {
                div.style("display", "none");
            });
        </script>