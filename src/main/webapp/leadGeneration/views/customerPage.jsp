<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<!--  SubMenu Starts  -->
	<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
		<!--  SubMenu Ends  -->
		<style>
			#inputHeadHeight{
                height: 25px !important;
            }
           #avgExpen .ui-slider-handle{
            cursor:move;
            }
		</style>
		<!--  Segment Dashboard Wrapper Starts  -->

		<div class="dashboard-wrapper lead-generation-wrapper segment-dashboard-wrapper predictive-user-wrapper bg-dark-grey1">
			<!-- Top Grid Wrapper-->
			<div class="top-grid-wrapper container-fluid" id = "hideOnEarlyAdopter">
				<ul class="list-inline custom-tabs row top-filters-list" style="margin: 20px;">
					<li class="col-sm-6 clearfix">
						<div class="lead-heading-list width-35 ai-b d-flex pull-left c-pointer" ui-sref="cluster" ng-if="hideStories">
							<span class="toggler pull-left mar-r5 fa fa-bars f-18 text-darker-pink"> </span>
							<h3 class="heading mar-0 p-rel as-st pad-t3 two-line-wrapper f-13 text-uppercase" style="color:#527180;">
								{{StoryName.split('_').join(' ')}}								
							</h3><span class="fa fa-angle-right f-21 text-dark-grey"></span>
						</div>
						<div class="customer-ids width-35 pull-left">
							<p class="top-heading text-darker-pink pad-r10 mar-0 p-rel f-16 ai-b  ws-normal d-flex">
								<span class="pull-left fa fa-users"></span>
								<span class="cluster-name f-13 as-st pad-t5 text-darker-pink pad-l5 two-line-wrapper c-arrow pad-r10" ng-if="!clusterClick">{{clusterDetail.cluster_name}}</span>
								<span ng-show ="!clusterClick" class="text-dark-grey c-pointer d-inline mar-r10  fa fa-pencil" ng-click="clusterClick = true;"></span> 
								<input ng-show ="clusterClick" id="inputHeadHeight" type="text" class="custom-input mar-b0 f-13 text-darker-pink pad-0 pad-l5 pad-r15 pad-b5 h-16 border-blue-thin-b" ng-model= clusterDetail.cluster_name aria-describedby="basic-addon1" ng-keydown = "clusterClick= renameClusterNameKeyUp($event)">
								<span  ng-show ="clusterClick" class=" mar-x5 c-pointer text-dark-grey fa fa-check" ng-click="clusterClick = false;renameClusterName(clusterDetail.cluster_name)"></span>
								<span  ng-show ="clusterClick" class=" mar-x5 c-pointer text-dark-grey fa fa-times" ng-click="clusterClick = false;renameClusterName(clusterDetail.cluster_name)"></span>
								<span class="fa fa-angle-right mar-autol f-21 text-dark-grey"></span>
							</p>
						</div>
						<div class="customer-ids width-30 pull-left">
							<p style="color:#527180; font-size:12px;" class="top-heading pad-x10 two-line-wrapper pad-t3 mar-0 p-rel f-12 text-overflow c-arrow">
								{{customerDetails.customer_name}}
							</p>
						</div>
					</li>
					<li class="col-sm-6 pad-t0">
						<div class="top-filters segment-filters-wrappers pull-right" style="width: 380px;">
							<!--  Progressbar List Starts -->
							<ul class="list-unstyled progressbar-list expenditure-details-item">
								<li class="progressbar-list-item  pad-t0">
									<div class="left-col c-arrow">Average Expenditure</div>
									<div class="progress">
										<div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="{{avgExpen['percent']}}" aria-valuemin="0"
										    aria-valuemax="100" style="width:{{avgExpen['percent']}}%">
											<span class="sr-only">{{avgExpen['percent']}} Complete</span>
										</div>
									</div>
									<div class="right-col c-arrow">₪ {{formatNummerWithComma(avgExpen['value']) }}</div>
								</li>
								<li class="progressbar-list-item">
									<div class="left-col c-arrow">GP Average</div>
									<div class="progress">
										<div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="{{generalExp['percent']}}" aria-valuemin="0"
										    aria-valuemax="100" style="width:{{generalExp['percent']}}%">
											<span class="sr-only">{{generalExp['percent']}} Complete</span>
										</div>
									</div>
									<div class="right-col c-arrow">₪ {{ formatNummerWithComma(generalExp['value'])}}</div>
								</li>
							</ul>
							<!--  Progressbar List Ends -->
						</div>
						<div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="clusterDetails.length === 0">
							<span>Data Not Found</span>
						</div>
					</li>
				</ul>
			</div>
			<!--Top Grid Wrapper Ends-->
			<!-- Main Segment Content Starts Here -->
			<div class="row main-segment-wrapper">


				<!-- Left Col Starts -->
				<div class="col-sm-3 custom-col right-col pad-l0">
					<div class="row custom-row">
						<div class="col-sm-12 custom-col segment-right-panel">

							<!-- Segment Panel Starts -->
							<div class="panel custom-panel-wrapper border-dark-grey-b  segment-panel-wrapper">

								<div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
									<div class="user-details-wrapper">
										<h4 class="c-arrow">
											{{customerDetails.customer_name}}
										</h4>
										<p class="c-arrow">
											Age : {{customerDetails.age}}
										</p>
										<p class="c-arrow">
											Gender : {{customerDetails.gender}}
										</p class="c-arrow">
										<p class="c-arrow">
											ISSUED By : {{customerDetails.issuing_company_name}}
										</p>
									</div>
									<div class="row score-card-wrapper">
										<!-- <div class="col-sm-4 pad-l0">
											<p>Sei Score</p>
											<strong>34</strong>
										</div> -->
										<div class="col-sm-6 pad-l0 c-arrow">
											<p>Risk Level</p>
											<h4 class="risk-level-wrapper">as
												<span class="c-arrow">{{customerDetails.as_Score}}</span>
											</h4>
											<h4 class="risk-level-wrapper">bs
												<span class="c-arrow">{{customerDetails.bs_Score}}</span>
											</h4>
										</div>
										<div class="col-sm-6 pad-l0 c-arrow">
											<p>Seniority</p>
											<strong>{{customerDetails.seniority}}
												<sub>YRS</sub>
											</strong>
										</div>
									</div>
									<h4 class="f-12 c-arrow" style ="padding-top: 30px;">
										filter by average transaction
									</h4>
									<div class="custom-range-wrapper z-9 no-thumb-border width-100">
										<div id="avgExpen">
											<div id="slider-range"></div>
										</div>
										<div class="col-sm-3 pull-left margin-top-class c-arrow" style="width:auto">MIN <span class="fa fa-ils f-10"></span> {{minAvgExp.toFixed(2)}}</div>
										<div class="col-sm-3 pull-right margin-top-class c-arrow" style="width:auto">MAX <span class="fa fa-ils f-10"></span> {{maxAvgExp.toFixed(2)}}</div>
									</div>
									<!-- <div class="credit-limit-wrapper">
										<p>Credit Limit</p>
										<span>$56,7456</span>
									</div> -->
									<!--  Progressbar List Starts -->
									<!-- <ul class="list-unstyled progressbar-list">

										<li class="progressbar-list-item range-list">
											<div class="left-col">credit Utilisation</div>
											<div class="progress progress-light-gray">
												<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
												 style="width:95%">
													<span class="sr-only ">100 Complete</span>
												</div>
											</div>
										</li>
										<li class="progressbar-list-item range-list">
											<div class="left-col"> CREdit Consumption</div>
											<div class="progress progress-light-gray">
												<div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
												 style="width:75%">
													<span class="sr-only ">100 Complete</span>
												</div>
											</div>
										</li>
									</ul> -->
									<!--  Progressbar List Ends -->
								</div>
							</div>
							<!-- Segment Panel Ends -->

							<!-- Segment Panel Starts -->
							<div class="panel custom-panel-wrapper  segment-panel-wrapper">
								<div class="panel-heading pad-b0">
									<h3></h3>
								</div>
								<div class="panel-body panel-scroll pad-t0" style="position: relative; overflow: visible;">
									<!--  Custom Accordion Starts  -->
									<div class="panel-group custom-accordion center-row1 " id="accordion" role="tablist" aria-multiselectable="true">
										<uib-accordion close-others="true">
											<div uib-accordion-group class="panel-default" is-open="status.isCustomHeaderOpen" ng-repeat="story in customer_story">
												<uib-accordion-heading>
													{{story.story_name.split("_").join(" ")}}
													<i class="pull-left glyphicon" style="left: 200px;" ng-class="{'glyphicon-chevron-up': status.isCustomHeaderOpen, 'glyphicon-chevron-down': !status.isCustomHeaderOpen}"></i>
												</uib-accordion-heading>
												{{ story.cluster_name.split("_").join(" ")}}
											</div>
										</uib-accordion>
									</div>
								</div>
								<!--  Custom Accordion Ends  -->
							</div>
						</div>
						<!-- Segment Panel Ends -->

						<!-- Segment Panel Starts 
							<div class="panel custom-panel-wrapper segment-panel-wrapper">
								<div class="panel-heading pad-b0">
									<h3>Locations</h3>
								</div>
								<div class="panel-body">
									<div id=""></div>
								</div>
							</div>
							<!-- Segment Panel Ends

								<div class="panel-heading pad-b0">
									<h3>AVErage Expenditure</h3>
								</div>
								<div class="panel-body border-b0">
									<div id=""></div>
								</div>
							</div>
							<!-- Segment Panel Ends -->

					</div>
					<!--  Twelfth Column Ends  -->

				</div>

				<!-- Right Col Ends -->
				<!-- Left Col Starts -->
				<div class="col-sm-9 pad-r0 custom-col left-col">
					<!-- Top Bar Starts -->
					<!-- 					<div class="topbar"> -->
					<!-- 						<ul class="list-inline top-bar-list row"> -->
					<!-- 							<li class="col-md-5 pad-r0"> -->
					<!-- 								<div class="custom-checkbox"> -->
					<!-- 									<label class="checkbox-inline pad-r0"> -->
					<!-- 										<input type="checkbox" class="schemaField" name="input_0" value="SAR"> -->
					<!-- 										<span> -->
					<!-- 											<i class="fa fa-check"></i> -->
					<!-- 										</span> -->
					<!-- 										<b class="input_0" title="SAR">Labels</b> -->
					<!-- 									</label> -->
					<!-- 									<label class="checkbox-inline"> -->
					<!-- 										<input type="checkbox" class="schemaField" name="input_0" value="SAR"> -->
					<!-- 										<span> -->
					<!-- 											<i class="fa fa-check"></i> -->
					<!-- 										</span> -->
					<!-- 										<b class="input_0" title="SAR">ATM</b> -->
					<!-- 									</label> -->
					<!-- 									<label class="checkbox-inline"> -->
					<!-- 										<input type="checkbox" class="schemaField" name="input_0" value="SAR"> -->
					<!-- 										<span> -->
					<!-- 											<i class="fa fa-check"></i> -->
					<!-- 										</span> -->
					<!-- 										<b class="input_0" title="SAR">Location</b> -->
					<!-- 									</label> -->
					<!-- 								</div> -->
					<!-- 							</li> -->
					<!-- 							<li class="col-md-4"> -->
					<!-- 								<label class="checkbox-inline">Period</label> -->
					<!-- 								<select class="custom-select-box"> -->
					<!-- 									<option selected="">12/09/2017</option> -->
					<!-- 									<option>12/08/2017</option> -->
					<!-- 									<option>12/07/2017</option> -->
					<!-- 									<option>12/06/2017</option> -->
					<!-- 									<option>12/05/2017</option> -->
					<!-- 								</select> -->
					<!-- 								<input type="text" value="12/09/2017"></input> -->
					<!-- 							</li> -->
					<!-- 							<li class="col-md-3"> -->
					<!-- 								<div class="dropdown"> -->
					<!-- 									<button class="btn btn-dropdown dropdown-toggle" type="button" data-toggle="">DOWNLOAD -->
					<!-- 										<span class="caret"></span> -->
					<!-- 									</button> -->
					<!-- 								</div> -->
					<!-- 								<button type="submit" class="btn btn-update pull-right">UPDATE</button> -->
					<!-- 							</li> -->
					<!-- 						</ul> -->
					<!-- 					</div> -->
					<!-- Top Bar Ends -->


					<div id="vla" class="customer-vla" style="width: 100%; min-height: calc(100vh - 130px);"></div>
					 <div class='leadGenErrorDiv text-center no-data-wrapper' ng-if='NoMerchantData'>
                                                            <span>DATA NOT FOUND</span>
                          </div>
				</div>
				<!-- Left Col Ends -->

			</div>
			<!-- Main Segment Content ends Here -->

		</div>
		<!--  Segment Dashboard Wrapper Ends  -->

		<script>
			function collision($div1, $div2) {
				var x1 = $div1.offset().left;
				var w1 = 40;
				var r1 = x1 + w1;
				var x2 = $div2.offset().left;
				var w2 = 40;
				var r2 = x2 + w2;

				if (r1 < x2 || x1 > r2) return false;
				return true;

			}
			$(document).ready(function () {
				setTimeout(function () {
					$(".top-filters-wrapper").mThumbnailScroller({
						axis: "x"
					});
				}, 500);
				$("body").on(
					"click",
					".dropdown",
					function (e) {
						$(this).toggleClass('open');
						$(this).find('.dropdown-menu').css(
							'display', '');
						$(this).find('.first_dropdown')
							.removeClass('first_dropdown');
						$(this).find('.second_dropdown')
							.removeClass('second_dropdown');
						e.stopPropagation();
						e.preventDefault();
					});

				$("body").on("click", ".dropdown-submenu a.test",
					function (e) {
						($(this).siblings('ul').not($(this).next('ul'))).css("display", "none");
						$(this).next('ul').toggle();
						$(this).next('ul').find("select").val("nodeBackground");
						e.stopPropagation();
						e.preventDefault();
					})
				$('body').on('click', 'a.menu_first_dropdown',
					function (e) {
						$(this).toggleClass('first_dropdown');
						$('#custom-rearrange').val(window.layout);
						//            $(this).parent().find('.dropdown-menu').css('display','none');   
						e.stopPropagation();
						e.preventDefault();
					});
				$('body').on('click', 'a.menu_second_dropdown',
					function (e) {
						$(this).toggleClass('second_dropdown');
						//            $(this).parent().find('.dropdown-menu').css('display','none');   
						e.stopPropagation();
						e.preventDefault();
					});

				$('body')
					.on(
						'click',
						'a.first_sub_menu',
						function (e) {
							var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
							for (var i = 0; i < window.CurentVLAIDs.length; i++) {
								if ('#' + Id[0].id === window.CurentVLAIDs[i]) {
									var curret_styles_splited = $(window.CurentVLAIDs[i]).attr("style").split("background-color:")
									if (curret_styles_splited[1]) {
										$(window.CurentVLAIDs[i])
											.attr("style", curret_styles_splited[0] + "background-color:#283c45 !important;" + curret_styles_splited[1]
												.split(";")[1])
									} else {
										$(window.CurentVLAIDs[i])
											.attr("style", curret_styles_splited[0] + "background-color:#283c45 !important;")
									}
									// 											$(window.CurentVLAIDs[i])
									// 												.css('background-color','#283c45 !important');
								}
							}

							$(this)
								.toggleClass(
									'second_first_sub_dropdown');
							if (!$(this)
								.hasClass(
									'second_first_sub_dropdown_check'))
								$(this)
								.addClass(
									'second_first_sub_dropdown_check');

							$(this)
								.parent()
								.find(
									'.second_second_sub_dropdown_check')
								.removeClass(
									'second_second_sub_dropdown_check');
							setTimeout(
								function () {
									var nodes = _cy_
										.elements();

									nodes.addClass('risks');
									nodes
										.removeClass('whiteTheme');
									nodes
										.removeClass('nodewithoutBackground');
									e.stopPropagation();
									e.preventDefault();
								}, 0);
						});
				$('body')
					.on(
						'click',
						'a.second_sub_menu',
						function (e) {
							var Id = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
							for (var i = 0; i < window.CurentVLAIDs.length; i++) {
								if ('#' + Id[0].id === window.CurentVLAIDs[i]) {

									var curret_styles_splited = $(window.CurentVLAIDs[i]).attr("style").split("background-color:")
									if (curret_styles_splited[1]) {
										$(window.CurentVLAIDs[i])
											.attr("style", curret_styles_splited[0] + "background-color:#F8F8FF !important;" + curret_styles_splited[1]
												.split(";")[1])
									} else {
										$(window.CurentVLAIDs[i])
											.attr("style", curret_styles_splited[0] + "background-color:#F8F8FF !important;")
									}
								}
							}
							$(this)
								.toggleClass(
									'second_second_sub_dropdown');
							if (!$(this)
								.hasClass(
									'second_second_sub_dropdown_check')) {
								$(this)
									.addClass(
										'second_second_sub_dropdown_check');
							}

							// console.log($('#configurations').parent());
							$(this)
								.parent()
								.find(
									'.second_first_sub_dropdown_check')
								.removeClass(
									'second_first_sub_dropdown_check');
							setTimeout(
								function () {
									var nodes = _cy_
										.elements();
									nodes
										.addClass('whiteTheme');
									nodes
										.removeClass('risks');
									nodes
										.removeClass('nodewithoutBackground');
									e.stopPropagation();
									e.preventDefault();
									//  $('.darkTheme').parent().css('display','none');
									// $('.whiteTheme').parent().css('display','block');
									// $('.second_second_sub_dropdown').siblings('.dropdown-menu').css('display','block');
								}, 0);

						});
			});
			$(".chart-panel-wrapper .panel-scroll").mCustomScrollbar({
				axis: "y",
				theme: "minimal-dark"
			});
			/**
			 * Function to get tool tip for progres bars
			 * 
			 */
			$("body").on("mouseover", ".progressbar-list-item", function () {
				if (location.hash.indexOf('customerPage') >= 0) {
					div.style("display", "inline-block");
					div.style("visibility", "visible");
				}

			}).on("mousemove", ".progressbar-list-item", function (event) {
				if (location.hash.indexOf('customerPage') >= 0) {
					//div.style("left", event.pageX + 10 + "px");
					div.style("left", event.pageX + 300 - $(".progressbar-list-item").width()+ "px");
					div.style("top", event.pageY + "px");
					div.html($(this).find(".right-col").text());
				}
			}).on("mouseout", function () {
				div.style("display", "none");
			})
			
		</script>