<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Usage Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper lead-generation-wrapper usage-dashboard-wrapper bg-dark-grey1">
            <p class="top-heading c-arrow">
                Personalization
            </p>
            <div class="row ">
                <div class="col-sm-3 usage-feature-box" ng-click="israCardlandlingPage.israCardLandingToClusterPage('General Card Holder')">
                    <h3 class="usage-feature-box-heading" title="General Cluster">
                        01.General Cluster
                    </h3>
                    <p class="usage-feature-box-text">
                        General Cluster enable investigation about different scenario by providing full view of all the cluster,as well as drilling down to specific cluster and a specific customer.
                    </p>
                </div>
                <div class="col-sm-3 usage-feature-box mar-l0" ng-click="israCardlandlingPage.israCardLandingToClusterPage('Dual Card Holders')">
                    <h3 class="usage-feature-box-heading" title="Dual Card Holders">
                        02.Dual Card Holders
                    </h3>
                    <p class="usage-feature-box-text">
                        Dual Card Holders core functionality is to cluster the customers that holds two or more cards while drilling down to lower level enables to investigate a connection between different clubs(card types)</p>
                </div>
                <div class="col-sm-3 usage-feature-box mar-l0" ng-click="israCardlandlingPage.israCardLandingToClusterPage('Predicting Dual Card Holders')">
                    <h3 class="usage-feature-box-heading" title="Predicting Dual Card Holders">
                        03. Predicting Dual Card Holders
                    </h3>
                    <p class="usage-feature-box-text">
                        Predicting Dual card enables to check the similarity between multicards holder who holds one or more cards issued by isra card and those who holds an external card. </p>
                </div>
            </div>
            <div class="row ">
                <div class="col-sm-3 usage-feature-box mar-t0" ng-click="israCardlandlingPage.israCardLandingToClusterPage('Early Adaptors')">
                    <h3 class="usage-feature-box-heading"  title="Early Adaptors">
                        04.Early Adaptors
                    </h3>
                    <p class="usage-feature-box-text">
                        The main role of Early Adaptors is to enable a full view of all the Early Adaptors customers and their Entity page </p>
                </div>
            </div>
        </div>
        <!--  Usage Dashboard Wrapper Ends  -->

        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".top-filters-wrapper").mThumbnailScroller({
                        axis: "x"
                    });
                }, 500);
            });
            $(".chart-panel-wrapper .panel-scroll").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
            
           
        </script>