<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Icdetails Dashboard Wrapper Starts  -->
        <div class="dashboard-wrapper lead-generation-wrapper icdetails-dashboard-wrapper bg-dark-grey1">

            <!--  Main Filters Wrapper Starts  -->
            <div class="main-filters-wrapper">

                <!-- Top Grid Wrapper-->
                <div class="top-grid-wrapper container-fluid">
                    <ul class="list-inline custom-tabs row top-filters-list">
                        <li class="col-sm-4">
                            <div class="col-sm-9">
                                <p class="top-heading">
                                    <i class="fa fa-angle-left icon-right"></i> Family having two kids</p>
                            </div>
                            <div class="c100 p63 pink radial">
                                <span>63%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sm-4">
                            <div class="col-sm-6">
                                <strong class="value">672,889 </strong>
                                <h4 class="top-grid-heading">Total Costumers</h4>
                            </div>
                            <div class="col-sm-6">
                                <strong class="value">$348,990,12 </strong>
                                <h4 class="top-grid-heading">Average Spendings</h4>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--Top Grid Wrapper Ends-->


                <!--  Top Filters Wrapper Starts  -->
                <div class="top-filters-wrapper segment-filters-wrappers">
                    <ul class="top-filter-navigation list-inline ">
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Age</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item">
                                            <div class="left-col">10-20</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:100%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">20-40</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">40-60</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">60-above</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Gender</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-3 filters-subhead-wrapper">
                                        <div class="filters-subhead">Male</div>
                                        <div class="filters-subhead">Female</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="genderTypePie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">SEI SCORE</h4>
                                <span class="top-filters-value">32.9</span>
                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Risk Level</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item  mar-t10">
                                            <div class="left-col">a-b</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">b-s</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Card Duration(YRS)</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-3 filters-subhead-wrapper">
                                        <div class="filters-subhead">0-5 </div>
                                        <div class="filters-subhead">5-10</div>
                                        <div class="filters-subhead">10-15</div>
                                        <div class="filters-subhead">15 Above</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="cardDurationPie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Card Types</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">Banks </div>
                                        <div class="filters-subhead">Credit Card Co</div>
                                        <div class="filters-subhead">Club</div>
                                        <div class="filters-subhead">Others</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="cardTypePie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Issued By</h4>
                                <div class="row mar-x0">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">Diamond </div>
                                        <div class="filters-subhead">Visa</div>
                                        <div class="filters-subhead">MASTER Card</div>
                                        <div class="filters-subhead">Diners</div>
                                    </div>
                                    <div class="pie-charts-wrapper">
                                        <div id="issuedBYPie"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="top-filter-lists">
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Credit</h4>

                                <!--  Progressbar List Starts -->
                                <div class="progress-bar-wrapper">
                                    <ul class="list-unstyled progressbar-list two-col">
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">limit</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">Utilisation</div>
                                            <div class="credit-utilise">
                                                <input type="range" min="0" max="100" value="24" class="range pink">
                                            </div>
                                            <div class="right-col-pink">24%</div>
                                        </li>
                                        <li class="progressbar-list-item range-list">
                                            <div class="left-col">Consumption</div>
                                            <div class="select-box-wrapper light-blue">
                                                <select class="form-control custom-select-box" data-show-subtext="true">
                                                    <option>Select</option>
                                                    <option>Diamond Card</option>
                                                    <option>Diamond Card</option>
                                                </select>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                    </ul>
                </div>
                <!--  Top Filters Wrapper Ends  -->

            </div>
            <!--  Main Filters Wrapper Ends  -->

            <!--  Bottom Content Wrapper Starts  -->
            <div class="bottom-content-wrapper row">
                <div class=" col-sm-10 custom-col left-bottom-col">
                    <div class="row">
                        <div class="col-sm-6 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h4>Single Card Trend</h4>
                                </div>
                                <div class="panel-body  panel-small-body pad-t0" style="position: relative; overflow: visible;">
                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list col-sm-8">
                                        <li class="progressbar-list-item top-two-panels">
                                            <div class="left-col">Average Expenditure</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col">$34,990</div>
                                        </li>
                                        <li class="progressbar-list-item top-two-panels">
                                            <div class="left-col">GP Average</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col">$14,990</div>
                                        </li>
                                    </ul>
                                    <!--  Progressbar List Ends -->
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                        <div class="col-sm-6 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h4>MultiCard Trend</h4>
                                </div>
                                <div class="panel-body  panel-small-body pad-t0" style="position: relative; overflow: visible;">
                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list col-sm-8">
                                        <li class="progressbar-list-item top-two-panels">
                                            <div class="left-col">Average Expenditure</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:95%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col">$34,990</div>
                                        </li>
                                        <li class="progressbar-list-item top-two-panels">
                                            <div class="left-col">GP Average</div>
                                            <div class="progress">
                                                <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only ">100 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col">$14,990</div>
                                        </li>
                                    </ul>
                                    <!--  Progressbar List Ends -->
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-6 custom-panels">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Average Expenditure Trends</h3>
                                </div>
                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <div id="singleCardAreaLineChart"></div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                        <div class="col-sm-6 custom-panels">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Average Expenditure Trends</h3>
                                </div>
                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <div id="multiCardAreaLineChart"></div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 custom-panels">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Top 10 spending Genere</h3>
                                </div>
                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Health Care</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:90%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">90%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Fashion</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">80%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Electronics</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:70%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">70%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> FMCS</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only">5 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">60%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Food And Dining</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:50%">
                                                    <span class="sr-only">2921 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">50%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Entertainment</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only">168 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">40%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Investments</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only">32 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:25%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">25%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:20%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">20%</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                        <div class="col-sm-3 custom-panels">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Locations</h3>
                                </div>
                                <div class="panel-body" style="position: relative; overflow: visible;">
                                    <div id="singleCardLocationMap"></div>

                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                        <div class="col-sm-3 custom-panels">
                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Top ten spending Genere</h3>
                                </div>
                                <div class="panel-body " style="position: relative; overflow: visible;">
                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Health Care</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:90%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">90%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Fashion</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">80%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Electronics</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:70%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">70%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> FMCS</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only">5 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">60%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Food And Dining</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:50%">
                                                    <span class="sr-only">2921 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">50%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Entertainment</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only">168 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">40%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Investments</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only">32 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:25%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">25%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:20%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">20%</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-3 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Locations</h3>
                                </div>
                                <div class="panel-body panel-scroll" style="position: relative; overflow: visible;">
                                    <div id="multiCardLocationMap"></div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-3 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>TOP 10 Merchant</h3>
                                </div>
                                <div class="panel-body  pad-b0" style="position: relative; overflow: visible;">

                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:90%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">90%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">80%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:70%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">70%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> SUBWAY</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only">5 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">60%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">AMAZON</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:50%">
                                                    <span class="sr-only">2921 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">50%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Ebay</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only">168 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">40%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Apple Store</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only">32 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:25%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">25%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:20%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">20%</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-3 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Gender Ratio</h3>
                                </div>
                                <div class="panel-body pad-b0 row mar-x0" style="position: relative; overflow: visible;">
                                    <div class="list-notations col-sm-4 pad-x0">
                                        <ul class="">
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>males</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>females</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>others</li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-8">
                                        <div id="singleGenderRatio_Pie"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-3 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>TOP 10 Merchant</h3>
                                </div>
                                <div class="panel-body  pad-b0" style="position: relative; overflow: visible;">

                                    <!--  Progressbar List Starts -->
                                    <ul class="list-unstyled progressbar-list">
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:90%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">90%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:80%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">80%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:70%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">70%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> SUBWAY</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:60%">
                                                    <span class="sr-only">5 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">60%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">AMAZON</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:50%">
                                                    <span class="sr-only">2921 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">50%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Ebay</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:40%">
                                                    <span class="sr-only">168 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">40%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col">Apple Store</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only">32 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Wallmart</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:20%">
                                                    <span class="sr-only ">1 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">20%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> Google Movies</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:25%">
                                                    <span class="sr-only">1651 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">25%</div>
                                        </li>
                                        <li class="progressbar-list-item">
                                            <div class="left-col"> KFC</div>
                                            <div class="progress">
                                                <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                    style="width:30%">
                                                    <span class="sr-only">710 Complete</span>
                                                </div>
                                            </div>
                                            <div class="right-col-pink">30%</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->

                        </div>
                        <div class="col-sm-3 custom-panels">

                            <!-- Segment Panel Starts -->
                            <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                                <div class="panel-heading pad-b0">
                                    <h3>Gender Ratio</h3>
                                </div>
                                <div class="panel-body pad-b0 row mar-x0" style="position: relative; overflow: visible;">
                                    <div class="list-notations col-sm-4 pad-x0">
                                        <ul class="">
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>males</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>females</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>others</li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-8">
                                        <div id="multiGenderRatio_Pie"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Segment Panel Ends -->
                        </div>
                    </div>
                </div>
                <!-- Right Col Starts -->
                <div class="col-sm-2 custom-col right-bottom-col">
                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Transaction</h3>
                        </div>
                        <div class="panel-body ">
                            <div class="row">
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Usage</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead">Local </div>
                                            <div class="panel-subhead">Abroad</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div id="usagepie"></div>
                                    </div>
                                </div>
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Form</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead">Pos </div>
                                            <div class="panel-subhead">Atm</div>
                                            <div class="panel-subhead">Online</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div id="formPie"></div>
                                    </div>
                                </div>
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Type</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead">Standard </div>
                                            <div class="panel-subhead">EMI</div>
                                            <div class="panel-subhead">Cashback</div>
                                            <div class="panel-subhead">Gift</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div id="typePie"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Segment Panel Ends -->

                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Locations</h3>
                        </div>
                        <div class="panel-body  pad-t0 border-b0">
                            <div id="overallWorldMap"></div>
                        </div>
                        <!-- Segment Panel Ends -->
                    </div>
                    <!--  Twelfth Column Ends  -->
                </div>
            </div>
            <!-- Right Col Ends -->

            <div class="transaction-predictive-wrapper custom-data-table-wrapper">
                <table id="" class="table table-striped border-b0" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Costumers
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                aria-label="Name: activate to sort column descending">
                                Age
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                aria-label="Name: activate to sort column descending">
                                Gender
                            </th>
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Profession
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                aria-label="Name: activate to sort column descending">
                                Location
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                aria-label="Name: activate to sort column descending">
                                Average Spending
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                        <tr id="" role="row" class="odd">
                            <td>JaneAustin</td>
                            <td>32</td>
                            <td>Male</td>
                            <td>Business Man</td>
                            <td>New York</td>
                            <td>$567990</td>
                        </tr>
                </table>
            </div>

            <!--  Bottom Content Wrapper Ends  -->
        </div>
        <!--  Icdetails Dashboard Wrapper Ends  -->

        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".top-filters-wrapper").mThumbnailScroller({
                        axis: "x"
                    });
                }, 500);
            });
            $(".panel .panel-scroll").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
        </script>