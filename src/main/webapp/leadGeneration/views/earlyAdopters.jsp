<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->

<!--  Adopters Dashboard Wrapper Starts  -->
<div class="dashboard-wrapper lead-generation-wrapper adopters-dashboard-wrapper bg-dark-grey1">
    <div class="custom-spinner full-page-spinner" ng-show="fullpageloader">
        <i class="fa fa-spinner fa-spin fa-3x"></i>
    </div>
    <!--  Main Filters Wrapper Starts  -->
    <div class="main-filters-wrapper">

        <!--  Top Filters Wrapper Starts  -->
        <div class="top-filters-wrapper segment-filters-wrappers">
            <ul class="top-filter-navigation width-100 early-adoptors-list list-inline">
                <li class="top-filter-lists">
                    <div class="top-filters height-100 c-arrow">
                        <h4 class="top-filter-heading">Total Customers</h4>
                        <strong class="value">{{israCardVariabe.totalCustomer | number}}
                        </strong>
                    </div>
                </li>

                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">Age</h4>

                        <!--  Progressbar List Starts -->
                        <div class="progress-bar-wrapper">
                            <ul class="list-unstyled progress-listbar h-60 progressbar-list">
                                <li class="progressbar-list-item " ng-repeat="age in israCardVariabe.ageProgressBarData">
                                    <div class="left-col c-arrow">{{age.key}} </div>
                                    <div class="progress" ng-click="israCardFunction.progressBarFilter('age',age)">
                                        <div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100"
                                            aria-valuemin="0" aria-valuemax="100" style="width:{{age.value}}%">
                                            <span class="sr-only ">{{age.value}} Complete</span>
                                        </div>
                                    </div>
                                   
                                    <div class="right-col" style="display:none">AGE({{age.start}}-{{age.end}}) :{{age.customer_count}}
                                                    <span ng-if="hidingOnfilter">({{age.value | number: 2}}%)</span>
                                             </div>
                                </li>
                                <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="israCardFunction.checkDataNotFound(israCardVariabe.ageProgressBarData)">
                                    <span>DATA NOT FOUND</span>
                                </div>
                            </ul>
                        </div>
                        <!--  Progressbar List Ends -->

                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">Gender</h4>
                        <div class="row mar-x0 top-filters-charts-wrapper">
                            <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                <li ng-repeat="legendData in israCardVariabe.genderPieLegendData" class="filters-subhead c-arrow">
                                    <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#genderPie', israCardVariabe.genderPieLegendData, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                        <i style="color: {{legendData.color}} !important"></i>{{legendData.name.toUpperCase()}}
                                        <div class="right-col" style="display:none">
                                       		{{legendData.name}}: {{legendData.formattedVal}}
                                       	 </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="pie-charts-wrapper">
                                <div id="genderPie" class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </li>

                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">As Score</h4>
                        <div class="top-filters-charts-wrapper mar-x0 row">
                            <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                <li ng-repeat="legendData in  israCardVariabe.asScorePieLegendData" class="filters-subhead c-arrow">
                                    <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#asScoreChart', israCardVariabe.asScorePieLegendData, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                        <i style="color: {{legendData.color}} !important"></i>{{legendData.name}} 
                                        <div class="right-col" style="display:none">
                                       		{{legendData.name}}: {{legendData.formattedVal}}
                                       	 </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="pie-charts-wrapper">
                                <div id='asScoreChart' class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">Bs Score</h4>
                        <div class="top-filters-charts-wrapper mar-x0 row">
                            <ul class=" filters-subhead-wrapper width-36 custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                <li ng-repeat="legendData in  israCardVariabe.bsScorePieLegendData" class="filters-subhead c-arrow">
                                    <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#bsScoreChart', israCardVariabe.bsScorePieLegendData, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                        <i style="color: {{legendData.color}} !important"></i>{{legendData.name}} 
                                        <div class="right-col" style="display:none">
                                       		{{legendData.name}}: {{legendData.formattedVal}}
                                       	 </div>
                                     </a>
                                </li>
                            </ul>
                            <div class="pie-charts-wrapper">
                                <div id='bsScoreChart' class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">Card Duration(YRS)</h4>
                        <div class="row mar-x0 top-filters-charts-wrapper">
                            <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                <li ng-repeat="legendData in israCardVariabe.CardDurationPieLegendData" class="filters-subhead c-arrow">
                                    <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#cardDurationPie', israCardVariabe.CardDurationPieLegendData, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                        <i style="color: {{legendData.color}} !important"></i>{{legendData.name.toString().toUpperCase()}}
                                        <div class="right-col" style="display:none">
                                       		{{legendData.name}}: {{legendData.formattedVal}}
                                       	 </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="pie-charts-wrapper">
                                <div id="cardDurationPie" class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading c-arrow">Card Types</h4>
                        <div class="row mar-x0 top-filters-charts-wrapper">
                            <ul class=" filters-subhead-wrapper custom-list pad-l0 item-1 circle chart-legends chart-legends-notify">
                                <li ng-repeat="legendData in israCardVariabe.cardTypePieLegendData" class="filters-subhead c-arrow">
                                    <a href="javascript:void(0);" ng-click="applyPieLegendFilters('#cardTypePie', israCardVariabe.cardTypePieLegendData, legendData)" style="color: {{legendData.color}} !important" class="c-pointer">
                                        <i style="color: {{legendData.color}} !important"></i>{{legendData.name.toUpperCase()}}
                                        <div class="right-col" style="display:none">
                                       		{{legendData.name}}: {{legendData.formattedVal}}
                                       	 </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="pie-charts-wrapper">
                                <div id="cardTypePie" class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </li>

            </ul>
        </div>
        <!--  Top Filters Wrapper Ends  -->

    </div>
    <!--  Main Filters Wrapper Ends  -->

    <!-- Main Adopters Content Starts Here -->
    <div class="row main-adopters-wrapper mar-t10">
        <div class="col-sm-9 pad-x0 height-100 custom-col left-col">

            <div class="transaction-adopters-wrapper custom-data-table-wrapper">
                <div class="select-box-wrapper">
                    <div id="dynamic-tabs" class="pad-l5 overflow-h width-45 pad-r15" >
                        <div class="remote-filters-wrapper">
                            <ul class="list-inline mar-b0 remote-filters-list-wrapper">
                                <li class="remote-filters-list" ng-repeat="(k,data) in israCardVariabe.addedFilter"
                                    style="pointer-events:none;">
                                    <a class="remote-filter">
                                        <span>{{data.showName}}</span>
                                        <i class="fa fa-times c-pointer" ng-click="israCardFunction.removeTab(k,data)"
                                            style="pointer-events:all;"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="select-group">
                        <span class="filter-key text-uppercase c-arrow">Display :</span>
                        <select id="" class="filter-value text-uppercase c-pointer " ng-model="typeValue" ng-change="israCardFunction.changeTableViewByType(typeValue)">
                            <option value="adopters" selected="selected">Early Adaptors Purchases</option>
                            <option value="dominentMerchant">Preferred Category</option>
                        </select>
                    </div>
                    <div class="select-group">
                        <span class="filter-key text-uppercase c-arrow">Sort By :</span>
                        <select id="" class="filter-value text-uppercase c-pointer " ng-model="checkedValue" ng-change="israCardFunction.changeTableView(checkedValue)">
                            <option value="ASC" selected="selected">Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
                <table id="" class="table table-scroll three-col-equal table-striped border-b0" role="grid">
                    <thead class="pad-r15 ">
                        <tr role="row" class="c-arrow">
                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                Customer Id
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Early Adaptors Purchases
                            </th>
                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending">
                                Preferred Category
                            </th>
                        </tr>

                    </thead>
                    <tbody id="cutlist" class="pad-r15 ">
                        <tr id="" role="row" class="odd" ng-repeat="data in israCardVariabe.customerData">
                            <td>
                                <a ui-sref="customer({cId: data.owner_id,story:false})">
                                    <h3 class="mar-t5 text-dark-grey2 mar-b5">
                                        {{data.owner_id}}
                                    </h3>
                                </a>
                            </td>
                            <td class="c-arrow">
                                <p class="mar-t5 mar-b5">{{formatTotalAmount(data.adopters)}}</p>
                            </td>


                            <td class="c-arrow">
                                <p class="mar-t5 mar-b5">{{data.dominentMerchant}}</p>
                            </td>

                        </tr>
                    </tbody>
                </table>
                <div class='leadGenErrorDiv text-center no-data-wrapper' ng-if='merchantData'>
                    <span>{{merchantDataErrorMsg}}</span>
                </div>
                <div class="text-right pagination-adoptors-wrapper" ng-style="{'display': israCardVariabe.totalCustomer.length === 0 ? 'none': 'block'}">
                    <ul class="transaction-alert-pagination" uib-pagination total-items="israCardVariabe.totalCustomer"
                        items-per-page="30" ng-model="pageNum" ng-change="israCardFunction.changeTablePageNo(pageNum)"
                        max-size="2" class="pagination-sm" boundary-link-numbers="true">
                    </ul>
                </div>
            </div>
        </div>

        <!-- Right Col Starts -->
        <div class="col-sm-3 custom-col height-panel-list right-col">
            <div class="row custom-row height-100">
                <div class="col-sm-12 height-100 custom-col">

                    <!-- Adopters Panel Starts -->
                    <div class="panel height-50 bg-transparent custom-panel-wrapper adopters-panel-wrapper">
                        <div class="panel-heading c-arrow">
                            <h3>Top Early Adopters</h3>
                        </div>
                        <div class="panel-body" style="position: relative; overflow: visible;">
                            <!--  Progressbar List Starts -->
                            <ul class="list-unstyled h-140 progress-listbar progressbar-list two-col">
                                <li class="progressbar-list-item" ng-repeat="data in israCardVariabe.topAdopterProgressBarData">
                                    <div class="left-col c-arrow">{{data.key}}</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100"
                                            aria-valuemin="0" aria-valuemax="100" style="width:{{data.value}}%">
                                            <span class="sr-only ">{{data.value}} Complete</span>
                                        </div>
                                    </div>
                                    <div class="right-col" style="display:none">{{data.key}} :{{(data.adopters | number
                                        )+'('+(
                                        data.value | number: 2) +'%)'}}
                                    </div>
                                </li>
                                <div class='leadGenErrorDiv text-center no-data-wrapper' ng-if='israCardFunction.checkDataNotFound(israCardVariabe.topAdopterProgressBarData)'>
                                    <span>DATA NOT FOUND</span>
                                </div>
                            </ul>
                            <!--  Progressbar List Ends -->
                        </div>
                    </div>
                    <!-- Adopters Panel Ends -->

                    <!-- Adopters Panel Starts -->
                    <div class="panel height-50 bg-transparent custom-panel-wrapper adopters-panel-wrapper">
                        <div class="panel-heading c-arrow">
                            <h3>Top Merchant Types</h3>
                        </div>
                        <div class="panel-body row mar-x0" style="position: relative; overflow: visible;">
                            <!--  List Notation Starts -->
                            <div class="col-sm-6 pad-x0">
                                <div class="list-notations">
                                    <ul class="list-inline h-140 pad-r15 progress-listbar custom-list circle charts-aml-legends">
                                        <li ng-repeat="legData in israCardVariabe.popularMerchantPieChartLegendData"
                                            class="c-arrow">
                                            <a href="javascript:void(0);" class="text-uppercase c-arrow" style="color: {{legData.color}} !important">
                                                <i class="fa fa-circle icon" style="color: {{legData.color}} !important"></i>{{legData.name}}
                                                <div class="right-col" style="display:none">
                                         			{{legData.name}}: {{legData.formattedVal}}
                                         	 	</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--  List Notation Ends -->
                            <div class="col-sm-6 pad-x0">
                                <div id="topMerchantPie"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Adopters Panel Ends -->


                </div>
                <!--  Twelfth Column Ends  -->
            </div>
        </div>
        <!-- Right Col Starts -->

    </div>
    <!-- Main Adopters Content ends Here -->

</div>
<!--  Adopters Dashboard Wrapper Ends  -->




<script>
    $(document).ready(function () {
        setTimeout(function () {
            $(".top-filters-wrapper").mThumbnailScroller({
                axis: "x"
            });
        }, 500);
    });
    $(".chart-panel-wrapper .panel-scroll, .height-panel-list").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });

    /**
     * Function to get tool tip for progres bars
     * 
     */
    $("body").on("mouseover", ".progressbar-list-item", function () {
        if (location.hash.indexOf('earlyAdopters') >= 0) {
            div.style("display", "inline-block");
            div.style("visibility", "visible");
        }

    }).on("mousemove", ".progressbar-list-item", function (event) {
        if (location.hash.indexOf('earlyAdopters') >= 0) {
            div.style("left", event.pageX + 10 + "px");
            div.style("top", event.pageY + "px");
            div.html($(this).find(".right-col").text());
        }
    }).on("mouseout", function () {
        div.style("display", "none");
    })

    $(document).ready(function () {
        setTimeout(function () {
            $(".remote-filters-wrapper").mThumbnailScroller({
                axis: "x",
                advanced: {
                    updateOnSelectorChange: true
                }
            });
        }, 0);
    });
    
 // Function to show tool tips for pie legends
    $("body").on("mouseover", ".top-filters .filters-subhead .c-pointer", function () {
       if (location.hash.indexOf('earlyAdopters') >= 0) {
           div.style("display", "inline-block");
           div.style("visibility", "visible");
           div.style("text-transform", "capitalize !important");
       }
   }).on("mousemove", ".top-filters .filters-subhead .c-pointer", function (event) {
       if (location.hash.indexOf('earlyAdopters') >= 0) {
           div.style("left", event.pageX + 10 + "px");
           div.style("top", event.pageY + "px");
           var txt  ="<span style='text-transform:capitalize !important'>"+$(this).find(".right-col").text()+"</span>"
           div.html(txt);
       }
   }).on("mouseout", function () {
       div.style("display", "none");
   });
    
 // Function to show tool tips for top merchant type pie legends
    $("body").on("mouseover", ".list-notations .c-arrow", function () {
       if (location.hash.indexOf('earlyAdopters') >= 0) {
           div.style("display", "inline-block");
           div.style("visibility", "visible");
           div.style("text-transform", "capitalize!important");
       }
   }).on("mousemove", ".list-notations .c-arrow", function (event) {
       if (location.hash.indexOf('earlyAdopters') >= 0) {
           div.style("left", event.pageX + 10 + "px");
           div.style("top", event.pageY + "px");
           var txt  ="<span style='text-transform:capitalize !important'>"+$(this).find(".right-col").text()+"</span>"
           div.html(txt);
       }
   }).on("mouseout", function () {
       div.style("display", "none");
   });

    $(".panel .panel-scroll").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $("#cutlist").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $(".progress-listbar").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    $(".filters-subhead-wrapper").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
</script>