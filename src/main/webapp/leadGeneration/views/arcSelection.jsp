<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <!--  SubMenu Starts  -->
    <%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
        <!--  SubMenu Ends  -->

        <!--  Arc Dashboard Wrapper Starts  -->

        <div class="dashboard-wrapper lead-generation-wrapper arc-dashboard-wrapper bg-dark-grey1">

            <!--  Main Filters Wrapper Starts  -->
            <div class="main-filters-wrapper">

                <!-- Top Grid Wrapper-->
                <div class="top-grid-wrapper container-fluid">
                    <ul class="list-inline custom-tabs row top-filters-list">
                        <li class="col-sm-4">
                            <div class="col-sm-9">
                                <p class="top-heading" ng-click="arcSelection.openNetworkChartModal();">
                                    <i class="fa fa-angle-left icon-right"></i> Family having two kids</p>
                            </div>
                            <div class="c100 p63 pink radial">
                                <span>63%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </li>
                        <li class="col-sm-4">
                            <div class="col-sm-6">
                                <strong class="value">672,889 </strong>
                                <h4 class="top-grid-heading">Total Costumers</h4>
                            </div>

                            <div class="col-sm-6">
                                <strong class="value">$348,990,12 </strong>
                                <h4 class="top-grid-heading">Total Spendings</h4>
                            </div>
                        </li>
                        <li class="col-sm-4">
                            <div class="top-filters segment-filters-wrappers">

                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Average Expenditure</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:95%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col">$34,990</div>
                                    </li>

                                    <li class="progressbar-list-item">
                                        <div class="left-col">GP Average</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col">$14,990</div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                    </ul>
                </div>
                <!--Top Grid Wrapper Ends-->

                <!--  Top Filters Wrapper Starts  -->
                <div class="top-filters-wrapper segment-filters-wrappers">
                    <ul class="top-filter-navigation list-inline ">
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Age</h4>

                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">10-20</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:100%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">20-40</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">40-60</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">60-above</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->

                            </div>
                        </li>
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Gender</h4>
                                <div class="row">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">male </div>
                                        <div class="filters-subhead">female</div>
                                        <div class="filters-subhead">Others</div>
                                    </div>
                                    <div>
                                        <div id="genderPie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Risk Level</h4>

                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list two-col">
                                    <li class="progressbar-list-item">
                                        <div class="left-col"></div>
                                        <div class="progress">

                                        </div>
                                    </li>
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">a-b</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:95%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="progressbar-list-item two-col">
                                        <div class="left-col">b-s</div>
                                        <div class="progress">
                                            <div title="946M" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only ">100 Complete</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--  Progressbar List Ends -->
                            </div>
                        </li>
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Average Spending</h4>
                                <div class="row">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">$500-$1500 </div>
                                        <div class="filters-subhead">$1500-$3000</div>
                                        <div class="filters-subhead">$3000-$10000</div>
                                        <div class="filters-subhead">>$10000</div>
                                    </div>
                                    <div>
                                        <div id="averageSpendingPie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Card Types</h4>
                                <div class="row">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">Debit Cards </div>
                                        <div class="filters-subhead">Credit Cards</div>
                                        <div class="filters-subhead">Gift Cards</div>
                                        <div class="filters-subhead">Others</div>
                                    </div>
                                    <div>
                                        <div id="cardTypePie"></div>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="top-filters">
                                <h4 class="top-filter-heading">Transaction Type</h4>
                                <div class="row">
                                    <div class="col-sm-6 filters-subhead-wrapper">
                                        <div class="filters-subhead">EMI </div>
                                        <div class="filters-subhead">Full</div>
                                        <div class="filters-subhead">Special Credit</div>
                                        <div class="filters-subhead">Dicounted</div>
                                    </div>
                                    <div>
                                        <div id="transactionTypePie"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--  Top Filters Wrapper Ends  -->
            </div>
            <!--  Main Filters Wrapper Ends  -->

            <!-- Main Segment Content Starts Here -->
            <div class="main-segment-content">
                <div class="row main-segment-wrapper">

                    <!--  Clipboard Dropdown old Starts  -->
                    <div class="dropdown-menu-wrapper clipboard-dropdown-menu-wrapper" ng-controller="MyClipBoardController" ng-show="showMyClipboard">
                        <div class="row search-clip-holder">
                            <div class="col-sm-8 search-clip-item">
                                <div class="input-group custom-input-group">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    <input type="text" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-sm-4 text-right search-clip-item">
                                <button class="btn btn-primary btn-blue" ng-click="toggleStickyNotesMenuDisplay()">CREATE</button>
                                <button class="btn btn-primary btn-blue" ng-click="clipboardUploadFile()">ADD MEDIA</button>
                            </div>
                        </div>
                        <div class="row context-menu">
                            <div class="col-sm-8 context-menu-item">
                                <h5>MY FILES</h5>
                            </div>
                            <div class="col-sm-4 text-right context-menu-item">
                                <span>
                                    <a href ng-click="toggleStickyNotesMenuDisplay()">
                                        <i class="fa fa-sticky-note"></i>
                                    </a>
                                </span>
                                <sticky-notes-menu-component ng-if="stickyNotesMenuVisible" on-click-sticky-notes-menu-close="onClickStickyNotesMenuClose()"
                                    toggle-sticky-notes-item-display="toggleStickyNotesItemDisplay()" sticky-notes-add="stickyNotesAdd()"></sticky-notes-menu-component>
                                <sticky-notes-component ng-repeat="notes in stickyNotesData.notes" ng-show="notes.visible" index=$index text=notes.text visible=notes.visible
                                    editable=notes.editable saveable=notes.saveable deleteable=notes.deleteable on-click-sticky-notes-close="onClickStickyNotesClose(ev, index)"
                                    on-click-sticky-notes-edit="onClickStickyNotesEdit(ev, index)" on-click-sticky-notes-save="onClickStickyNotesSave(ev, index)"
                                    on-click-sticky-notes-delete="onClickStickyNotesDelete(ev, index)"></sticky-notes-component>
                                <i class="fa fa-share-alt"></i>
                                <i class="fa fa-camera"></i>
                                <i class="fa fa-filter"></i>
                            </div>
                        </div>
                        <div class="row clipboard-items">
                            <ul class="list-inline clipboard-items-list">
                                <li class="clipboard-list-item" ng-repeat="doc in documents" ng-dblclick="downloadDocument(doc, 'download')" ng-cloak>
                                    <i class="fa fa-folder clipboard-icon"></i>
                                    <span class="clipboard-item-name">{{doc.title}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--  Clipboard Dropdown Ends  -->

                    <!--  Clipboard Dropdown new Starts  -->
                    <div class="dropdown-menu-wrapper clipboard-dropdown-menu-wrapper">
                        <div class="row search-clip-holder">
                            <div class="col-sm-5 search-clip-item">
                                <p>Total 34567 Files | 467MB used of 100GB</p>
                                <!--  Progressbar List Starts -->
                                <div class="list-unstyled progressbar-list">
                                    <div class="progressbar-list-item pad-t0">
                                        <div class="progress progress-light-gray">
                                            <div class="progress-bar progress-curious-blue" role="progressbar" aria-valuenow="{{progress1}}" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90px">
                                                <span class="sr-only "></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7 text-right search-clip-item pad-x0">
                                <h3>Add Media</h3>
                                <button class="btn btn-primary btn-blue">
                                    <span class="fa fa-camera"></span> Capture Screen</button>
                                <button class="btn btn-primary btn-blue">
                                    <span class="fa fa-plus-square"></span>Note</button>
                                <button class="btn btn-primary btn-blue">
                                    <span class="fa fa-upload"></span>Upload</button>
                            </div>
                        </div>
                        <div class="row context-menu">
                            <div class="col-sm-5 context-menu-item">
                                <ul>
                                    <li class="active">
                                        <a>All FILES</a>
                                    </li>
                                    <li>
                                        <a>Jane Palmer Files</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-sm-7 text-right context-menu-item">
                                <div class="input-group custom-input-group pull-left">
                                    <span class="input-group-addon" id="basic-addon1">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    <input type="text" class="form-control" aria-describedby="basic-addon1">
                                </div>
                                <div class="select-group">
                                    <span class="filter-key text-uppercase">Display :</span>
                                    <select class="filter-value text-uppercase">
                                        <option>Icons</option>
                                        <option>Tiles</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clipboard-items">
                            <div class="table-details-wrapper">
                                <div class="transaction-predictive-wrapper custom-data-table-wrapper">
                                    <table id="" class="table table-striped border-b0" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                                    Costumers
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Age
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Gender
                                                </th>
                                                <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                                    Profession
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Location
                                                </th>
                                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">
                                                    Average Spending
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                            <tr id="" role="row" class="odd">
                                                <td>JaneAustin</td>
                                                <td>32</td>
                                                <td>Male</td>
                                                <td>Business Man</td>
                                                <td>New York</td>
                                                <td>$567990</td>
                                            </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tiles-style hidden">
                                <div class="select-group pull-left">
                                    <span class="filter-key text-uppercase">List-By :</span>
                                    <select class="filter-value text-uppercase">
                                        <option>Regency</option>
                                        <option>Tiles</option>
                                    </select>
                                </div>
                                <ul class="list-inline clipboard-items-list">
                                    <li class="clipboard-list-item">
                                        <i class="fa fa-folder clipboard-icon"></i>
                                        <span class="clipboard-item-name">{{doc.title}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--  Clipboard Dropdown Ends  -->
                    <!--Right Click Starts-->
                    <div class="right-click-wrapper pull-right">
                        <ul class="right-click-menu">
                            <li class="right-click-options">View </li>
                            <li class="right-click-options">Link to case</li>
                            <li class="right-click-options">Share</li>
                            <li class="right-click-options">Download</li>
                            <li class="right-click-options">Remove</li>
                        </ul>
                    </div>
                    <!--Right Click Ends-->

                </div>
            </div>
            <!-- Main Segment Content ends Here -->

        </div>

        <!--  Arc Dashboard Wrapper Ends  -->

        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".top-filters-wrapper").mThumbnailScroller({
                        axis: "x"
                    });
                }, 500);
            });
        </script>




        <!--  Top Filters Wrapper Starts  -->
        <div class="top-filters-wrapper segment-filters-wrappers hidden">
            <ul class="top-filter-navigation list-inline ">
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <div ng-show="agePreloader" class="custom-spinner">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                        </div>
                        <h4 class="top-filter-heading">Age</h4>

                        <div class="progress-bar-wrapper">
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="ageData.length === 0">
                                <span>Data Not Found</span>
                            </div>
                            <div ng-show="clusterPreloader" class="custom-spinner">
                                <i class="fa fa-spinner fa-spin fa-3x"></i>
                            </div>
                            <div class="list-unstyled progressbar-list">
                                <div class="progressbar-list-item two-col" ng-repeat="age in ageData">
                                    <div class="left-col">{{age.start}}-{{age.end}}</div>
                                    <div class="progress progress-light-gray" ng-click="applyFilter()">
                                        <div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="{{age.progress}}" aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{age.progress}}%">
                                            <span class="sr-only">{{age.progress}}% Complete</span>
                                        </div>
                                    </div>
                                    <div class="right-col" style="display:none">AGE({{age.start}}-{{age.end}})-{{' CUSTOMER: '+age.customer_count}}</div>
                                </div>
                            </div>
                        </div>
                        <!--  Progressbar List Ends -->

                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">Gender</h4>
                        <div class="row mar-x0">
                            <div class="col-sm-3 pad-x0 filters-subhead-wrapper">
                                <div class="filters-subhead">Male</div>
                                <div class="filters-subhead">Female</div>
                            </div>
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="genderData.length === 0">
                                <span>Data Not Found</span>
                            </div>
                            <div class="pie-charts-wrapper">
                                <div ng-show="genderTypePreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div id="gender_Pie"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">Risk Level</h4>

                        <!--  Progressbar List Starts -->
                        <div class="progress-bar-wrapper">
                            <div class="list-unstyled progressbar-list" ng-if="riskData.as_ranges.length > 0 && riskData.bs_ranges.length > 0">
                                <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="riskData.as_ranges.length === 0 && riskData.bs_ranges.length === 0">
                                    <span>Data Not Found</span>
                                </div>
                                <div ng-show="riskPreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div class="progressbar-list-item mar-t10">
                                    <div class="left-col">as</div>
                                    <div class="progress progress-light-gray">
                                        <div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="{{asAvg}}" aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{asAvg}}%">
                                            <span class="sr-only ">{{asAvg}} Complete</span>
                                        </div>
                                    </div>
                                    <div class="right-col" style="display:none">{{'AS_CUSTOMERS: '+riskData.as_ranges[0].customer_count}}</div>
                                </div>
                                <div class="progressbar-list-item">
                                    <div class="left-col">bs</div>
                                    <div class="progress progress-light-gray">
                                        <div class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="{{bsAvg}}" aria-valuemin="0" aria-valuemax="100"
                                            style="width:{{bsAvg}}%">
                                            <span class="sr-only ">{{bsAvg}} Complete</span>
                                        </div>
                                    </div>
                                    <div class="right-col" style="display:none">{{'BS_CUSTOMERS: '+riskData.bs_ranges[0].customer_count}}</div>
                                </div>
                            </div>
                        </div>
                        <!--  Progressbar List Ends -->

                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">Card Duration(YRS)</h4>
                        <div class="row mar-x0">
                            <div class="col-sm-3 filters-subhead-wrapper">
                                <div class="filters-subhead" ng-repeat="duration in cardDurationData">{{duration.start}}-{{duration.end}}</div>
                                <!-- <div class="filters-subhead">5-10</div>
                            <div class="filters-subhead">10-15</div>
                            <div class="filters-subhead">15 above</div> -->
                            </div>
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="cardDurationData.length === 0">
                                <span>Data Not Found</span>
                            </div>
                            <div class="pie-charts-wrapper">
                                <div ng-show="cardDurationPreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div id="card_Duration_Pie"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">Card Types</h4>
                        <div class="row mar-x0">
                            <div class="col-sm-3 filters-subhead-wrapper">
                                <div class="filters-subhead">Diamond</div>
                                <div class="filters-subhead">Other</div>
                                <!-- <div class="filters-subhead">Club</div>
                            <div class="filters-subhead">Others</div> -->
                            </div>
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="cardTypeData.length === 0">
                                <span>Data Not Found</span>
                            </div>
                            <div class="pie-charts-wrapper">
                                <div ng-show="cardTypePreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div id="card_Type_Pie"></div>
                            </div>
                        </div>

                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">City</h4>
                        <div class="row mar-x0">
                            <div class="col-sm-6 filters-subhead-wrapper subhead-scroll" id="subhead-scroll">
                                <div class="filters-subhead" ng-repeat="city in cityData">{{city.city_name}}</div>
                                <!-- <div class="filters-subhead">Other</div> -->
                                <!-- <div class="filters-subhead">Club</div>
                            <div class="filters-subhead">Others</div> -->
                            </div>
                            <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="cityData.length === 0">
                                <span>Data Not Found</span>
                            </div>
                            <div class="pie-charts-wrapper">
                                <div ng-show="cityTypePreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div id="city_Pie"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="top-filter-lists">
                    <div class="top-filters">
                        <h4 class="top-filter-heading">Issued By</h4>
                        <div class="row mar-x0">
                            <div class="col-sm-6 filters-subhead-wrapper">
                                <div class="filters-subhead">Diamond </div>
                                <div class="filters-subhead">Other</div>
                                <div class="filters-subhead">MASTER Card</div>
                                <div class="filters-subhead">Diners</div>
                            </div>
                            <!-- <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="issuedByData.length === 0">
                            <span>Data Not Found</span>
                        </div> -->
                            <div class="pie-charts-wrapper">
                                <div ng-show="clusterPreloader" class="custom-spinner">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                                <div id="issued_By_Pie"></div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!--  Top Filters Wrapper Ends  -->


        <div class="row main-segment-wrapper hidden">
            <div class="col-sm-5 pad-r0 custom-col left-col">
                <div class="row custom-row usage-panel-wrapper">
                    <div class="col-sm-12 custom-col segment-right-panel">

                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                            <div class="panel-heading ">
                                <h4>Merchant types</h4>
                            </div>
                            <div class="panel-body " style="position: relative; overflow: visible;">
                                <div class="row">
                                    <div class="col-sm-4 pad-x0">
                                        <div class="list-notations panel-scroll">
                                            <ul class="">
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Healthcare</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Fashion</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Electronics</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>FMCG</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Food and Dinning</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Entertainments</li>
                                            </ul>
                                            <!--  <ul class="class="col-md-6 pad-r0">
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Investments</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Food and Dinning</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Entertainments</li>
                                        </ul>-->
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div id="spendingGenrePie"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                            <div class="panel-heading pad-b0">
                                <h4>Average expenditure trends</h4>
                            </div>
                            <div class="panel-body pad-t0" style="position: relative; overflow: visible;">
                                <div id="cashHoldingPatternLineChart"></div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                            <div class="panel-heading ">
                                <h4>Top Merchants</h4>
                            </div>
                            <div class="panel-body " style="position: relative; overflow: visible;">
                                <div class="row">
                                    <div class="col-sm-4 pad-x0">
                                        <div class="list-notations panel-scroll">
                                            <ul class="">
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Wallmart</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Google Movies</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>KFC</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>SUBWAY</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>AMAZON</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Ebay</li>
                                                <li class="notation-list">
                                                    <span class="icon-left">
                                                        <i class="fa fa-circle"></i>
                                                    </span>Apple Store</li>
                                            </ul>
                                            <!--   <ul class="col-sm-6">
                                          <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Netflix</li>
                                          <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span> AMazon Prime</li>
<li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Panway Bakers</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Grand Hyatt Hotels</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>REDSea Continental</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>General Electric Corp</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span> State Water Authority</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>Skybiz Cable TV</li>
                                            <li class="notation-list">
                                                <span class="icon-left">
                                                    <i class="fa fa-circle"></i>
                                                </span>HighMall Bar</li>
                                        </ul>-->
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div ng-show="clusterPreloader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                        <div id="topMerchantPie">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                    </div>
                    <!--  Twelfth Column Ends  -->

                </div>
            </div>


            <!-- Right Col Starts -->
            <div class="col-sm-3 custom-col right-col">
                <div class="row custom-row usage-panel-wrapper">
                    <div class="col-sm-12 custom-col segment-right-panel">

                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                            <div class="panel-heading pad-b0">
                                <h3>TOP 10 Merchant</h3>
                            </div>

                            <div class="panel-body panel-scroll pad-b0" style="position: relative; overflow: visible;">
                                <!--  Progressbar List Starts -->
                                <ul class="list-unstyled progressbar-list">
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> Wallmart</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:90%">
                                                <span class="sr-only ">1 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">90%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> Google Movies</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:80%">
                                                <span class="sr-only">1651 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">80%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> KFC</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:70%">
                                                <span class="sr-only">710 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">70%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> SUBWAY</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:60%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">60%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">AMAZON</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:50%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">50%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> Ebay</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:40%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">40%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Apple Store</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:30%">
                                                <span class="sr-only">32 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">30%</div>
                                    </li>


                                    <li class="progressbar-list-item">
                                        <div class="left-col"> Amner Spa</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:20%">
                                                <span class="sr-only">5 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">20%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col">Turkish Delight</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:25%">
                                                <span class="sr-only">2921 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">25%</div>
                                    </li>
                                    <li class="progressbar-list-item">
                                        <div class="left-col"> Netflix</div>
                                        <div class="progress">
                                            <div title="50" class="progress-bar progress-dark-red" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                style="width:10%">
                                                <span class="sr-only">168 Complete</span>
                                            </div>
                                        </div>
                                        <div class="right-col-pink">10%</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                        <!-- Segment Panel Starts -->
                        <!-- <div class="panel custom-panel-wrapper segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Locations</h3>
                        </div>
                        <div class="panel-body pad-t0">
                            <div id="locationWorldMap"></div>
                        </div>
                    </div> -->
                        <!-- Segment Panel Ends -->

                        <!-- Segment Panel Starts -->
                        <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                            <div class="panel-heading pad-b0">
                                <h3>Gender Mix</h3>
                            </div>
                            <div class="panel-body pad-b0 row mar-0" style="position: relative; overflow: visible;">
                                <div class="list-notations col-sm-4">
                                    <ul class="">
                                        <li class="notation-list">
                                            <span class="icon-left">
                                                <i class="fa fa-circle"></i>
                                            </span>male</li>
                                        <li class="notation-list">
                                            <span class="icon-left">
                                                <i class="fa fa-circle"></i>
                                            </span>female</li>
                                        <li class="notation-list">
                                            <span class="icon-left">
                                                <i class="fa fa-circle"></i>
                                            </span>others</li>
                                    </ul>
                                </div>
                                <div class="col-sm-8">
                                    <div ng-show="clusterPreloader" class="custom-spinner">
                                        <i class="fa fa-spinner fa-spin fa-3x"></i>
                                    </div>
                                    <div id="genderMix_Pie"></div>
                                </div>
                            </div>
                        </div>
                        <!-- Segment Panel Ends -->

                    </div>
                    <!--  Twelfth Column Ends  -->

                </div>
            </div>
            <!-- Middle Col Ends -->

            <!-- Right Col Starts -->
            <div class="col-sm-4 custom-col right-col" onscroll="scrolled(this)">
                <h3>Customer List</h3>
                <div class="transaction-adopters-wrapper custom-data-table-wrapper">
                    <table class="table border-b0 table-scroll two-col" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">
                                    Costumer NAME
                                </th>
                                <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                    aria-label="Name: activate to sort column descending">
                                    Average Spending
                                </th>
                            </tr>
                        </thead>
                        <tbody id="custList" onscroll="scrolled(this)">
                            <tr id="" role="row" class="odd" ng-repeat="customer in customerList">
                                <td>
                                    <h3>
                                        <a ui-sref="customer({name: customer.name, cId: customer.id})">{{customer.name}}</a>
                                    </h3>
                                    <p>{{customer.sex}} | {{customer.profession}}</p>
                                </td>
                                <td>
                                    <strong>{{customer.avg_spending}}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Right Col Ends -->

        </div>
        <!-- Main Segment Content ends Here -->

        <!-- User Cards Starts Here -->
        <div class="user-card-design hidden">
            <div class="row mar-0">
                <div class="col-sm-10 left-col pad-x0">
                    <div class="user-datatable-wrapper">
                        <div class="showing-results-wrapper clearfix">
                            <div class="showing-results">
                                <span class="text-uppercase">
                                    Displaying:
                                    <a href="javascript:void(0);"> 340 of 700 so far</a>
                                </span>
                                <div class="data-options-wrapper pull-right">
                                    <p class="sorting-results">
                                        <span class="filter-key text-uppercase">Sort By :</span>
                                        <select class="filter-value text-uppercase">
                                            <option>Customer Name</option>
                                            <option>Average Expenditure</option>
                                        </select>
                                    </p>
                                    <div class="search-filter">
                                        <label>SEARCH</label>
                                        <input class="" type="text">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <p class="sorting-results">
                                        <span class="filter-key text-uppercase">View By :</span>
                                        <!-- <select class="filter-value text-uppercase" >
                                        <option>Cards</option>
                                        <option>Table</option>
                                    </select> -->
                                        <select class="filter-value text-uppercase" ng-model="ViewBy" ng-options="c.name for c in ViewBytable_card"></select>
                                    </p>

                                </div>
                                <div id="dynamic-tabs" class="remote-filters-wrapper" ng-if="searchParamtab.length > 0">
                                    <ul class="list-inline remote-filters-list-wrapper">
                                        <li class="remote-filters-list" ng-repeat="searchParam in searchParamtab" ng-if="searchParam.details !==''">
                                            <a class="remote-filter">
                                                <span>{{searchParam.details}}</span>
                                                <i class="fa fa-times" ng-click="removeTab($index,searchParam)"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- User Cards Layout Starts Here -->
                        <div class="main-filtered-user-wrapper" id="autoScrollCluster">
                            <div class="row mar-r0">
                                <div class="col-sm-3" ng-repeat="customer in customerList" ng-if="ViewBy.name==='Cards'">
                                    <div class="usage-feature-box">
                                        <h3 class="usage-feature-box-heading">
                                            <a ui-sref="customer({cId: customer.customer_id})">{{customer.customer_name}}</a>
                                        </h3>
                                        <p class="usage-feature-box-text">
                                            Avg monthly Expenditure
                                        </p>
                                        <strong class="">
                                            {{customer.avg_spending}}
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="transaction-adopters-wrapper custom-data-table-wrapper">
                                <table class="table border-b0 table-scroll two-col" role="grid" ng-if="ViewBy.name==='Table'">
                                    <thead>
                                        <tr role="row">
                                            <th class="text-left sorting" rowspan="1" colspan="1" aria-label="Id">Name</th>
                                            <th class="text-left sorting" tabindex="0" aria-controls="landingTableData" rowspan="1" colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending">Avg monthly Expenditure</th>
                                        </tr>
                                    </thead>
                                    <tbody id="custList" onscroll="scrolled(this)">
                                        <tr role="row" class="odd" ng-repeat="customer in customerList">
                                            <td>
                                                <h3>
                                                    <a ui-sref="customer({cId: customer.customer_id})">{{customer.customer_name}}</a>
                                                </h3>
                                            </td>
                                            <td>
                                                <strong>{{customer.avg_spending}}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- User Cards Layout Ends Here -->

                    </div>
                </div>
                <div class="col-sm-2 custom-col right-col">

                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Transaction</h3>
                        </div>
                        <div class="panel-body ">
                            <div class="row">
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Usage</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead" ng-repeat="data in localAbroadData" style="color:{{data.color}}">{{data.group_level}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div ng-show="localAbroadPreloader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                        <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="localAbroadData.length === 0">
                                            <span>Data Not Found</span>
                                        </div>
                                        <div id="usagepie"></div>
                                    </div>
                                </div>
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Type</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead" ng-repeat="data in installmentsData" style="color:{{data.color}}">{{data.event_activity_code}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div ng-show="installmentsPreloader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                        <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="installmentsData.length === 0">
                                            <span>Data Not Found</span>
                                        </div>
                                        <div id="typePie"></div>
                                    </div>
                                </div>
                                <div class="side-panel-chart-wrapper row mar-0">
                                    <h4 class="top-panel-heading">Form</h4>
                                    <div class="col-sm-3 pad-r0">
                                        <div class="panel-subhead-wrapper">
                                            <div class="panel-subhead" ng-repeat="data in onlineOnsiteData" style="color:{{data.color}}">{{data.track_2_level}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 pad-0">
                                        <div ng-show="onlineOnsitePreloader" class="custom-spinner">
                                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                                        </div>
                                        <div class="leadGenErrorDiv text-center no-data-wrapper" ng-if="onlineOnsiteData.length === 0">
                                            <span>Data Not Found</span>
                                        </div>
                                        <div id="formPie"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Segment Panel Ends -->

                    <!-- Segment Panel Starts -->
                    <div class="panel custom-panel-wrapper  segment-panel-wrapper">
                        <div class="panel-heading pad-b0">
                            <h3>Locations</h3>
                        </div>
                        <div class="panel-body  pad-t0 border-b0">
                            <div id="locationWorldMap"></div>
                        </div>
                        <!-- Segment Panel Ends -->

                    </div>
                    <!--  Twelfth Column Ends  -->

                </div>
            </div>

        </div>
        <!-- User Cards Ends Here -->