'use strict';
angular.module('leadGenerationApp')
	   .factory('LeadGenerationApiService', leadGenerationApiService);

		leadGenerationApiService.$inject = [
			'$http',
			'$q',
			'EHUB_API',
			'$rootScope'
		];
		
		function leadGenerationApiService(
				$http,
				$q,
				EHUB_API,
				$rootScope
		) {
			return{
				getclustersInfo: getclustersInfo,
				getOverAllStats: getOverAllStats,
				getClusterFilteredData: getClusterFilteredData,
				getAgeDetails: getAgeDetails,
				getRiskLevels: getRiskLevels,
				getCardTypeData: getCardTypeData,
				getGenderDetails: getGenderDetails,
				getCityDetails: getCityDetails,
				getCustomerDetails: getCustomerDetails,
				getCardDurationDetails: getCardDurationDetails,
				getfetchissueDetails: getfetchissueDetails,
				getclustersInfoByClusterId: getclustersInfoByClusterId,
				getLocalAbroad: getLocalAbroad,
				getOnlineOnsite: getOnlineOnsite,
				getInstallmentsStandard: getInstallmentsStandard,
				getFilteredDataByClusterId: getFilteredDataByClusterId,
				getClubDataByClusterId:getClubDataByClusterId,
				getClubconnectionDetails:getClubconnectionDetails,
				getconnectedclubdetails:getconnectedclubdetails,
				
				//New api's uc1
				getStories:getStories,
				getCluster:getCluster,
				getDistribution:getDistribution,
				getCustomerDetail:getCustomerDetail,
				postFavouriteStory:postFavouriteStory,
				getfavouritestory:getfavouritestory,
				getCustomerDetailById:getCustomerDetailById,
				getClusterDetail:getClusterDetail,
				getGraphCustomerById:getGraphCustomerById,
				customer__stories_clusters:customer__stories_clusters,
				getUc3clusters:getUc3clusters,
				Uc3overall_stats:Uc3overall_stats,
				postsetStoryName:postsetStoryName,
				postsetClusterName:postsetClusterName,
				getstageRequest:getstageRequest,
				postNewStoryName:postNewStoryName,
				getStoryByName:getStoryByName,
				deleteStoryById:deleteStoryById,
				
				//uc2
				comparisonChartPlot:comparisonChartPlot,
				getClubConnections: getClubConnections,
				
				//uc4
				getCustomers_count:getCustomers_count,
				postUC3distributions:postUC3distributions,
				postTop_early_adapters:postTop_early_adapters,
				postPopular_merchant_types:postPopular_merchant_types,
				postUc3Customers:postUc3Customers,
				
				//uc3
				getsearchCustomer:getsearchCustomer
				
			}
			/* 
			 * purpose: get age details
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
			function getAgeDetails(clId, clSize, parmas,url,data){
				var apiUrl = EHUB_API + url + clId + "/" + clSize;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data :data
				});
				return(request
						.then(getAgeDetailsSuccess)
						.catch(getAgeDetailsError));
				
				/* getAgeDetails error function */
				function getAgeDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getAgeDetails success function */
				function getAgeDetailsSuccess(response){
					return(response);
				}
			}

			/* 
			 * purpose: get gender details
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
		
			function getGenderDetails(clId, parmas,url,data){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data :data
				});
				return(request
						.then(getGenderDetailsSuccess)
						.catch(getGenderDetailsError));
				
				/* getGenderDetails error function */
				function getGenderDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getGenderDetails success function */
				function getGenderDetailsSuccess(response){
					return(response);
				}
			}

			/* 
			 * purpose: get city details
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
			function getCityDetails(clId, parmas,url,data){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data :data
				});
				return(request
						.then(getCityDetailsSuccess)
						.catch(getCityDetailsError));
				
				/* getCityDetails error function */
				function getCityDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCityDetails success function */
				function getCityDetailsSuccess(response){
					return(response);
				}
			}

			/* 
			 * purpose: get risk level details
			 * created: 19th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */

			 function getRiskLevels(clId, parmas,url,data, asBuckets, bsBuckets){
			 	var apiUrl = EHUB_API + url+ clId + '/' + asBuckets + '/' + bsBuckets;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data:data
				});
				return(request
						.then(getRiskLevelsSuccess)
						.catch(getRiskLevelsError));
				
				/* getRiskLevels error function */
				function getRiskLevelsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getRiskLevels success function */
				function getRiskLevelsSuccess(response){
					return(response);
				}
			 }

			 /* 
			 * purpose: get card type data 
			 * created: 19th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */

			 function getCardTypeData(clId, parmas,url,data){
			 	var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data :data
				});
				return(request
						.then(getCardTypeDataSuccess)
						.catch(getCardTypeDataError));
				
				/* getCardTypeData error function */
				function getCardTypeDataError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCardTypeData success function */
				function getCardTypeDataSuccess(response){
					return(response);
				}
			 }

			/* 
			 * purpose: get clusters Information
			 * created: 20th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getclustersInfo(params,url){
				var apiUrl = EHUB_API +url;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getclustersInfoSuccess)
						.catch(getclustersInfoError));
				
				/* getclustersInfo error function */
				function getclustersInfoError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfo success function */
				function getclustersInfoSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get OverAll Status
			 * created: 20th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getOverAllStats(params,url){
				var apiUrl = EHUB_API + url;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getOverAllStatsSuccess)
						.catch(getOverAllStatsError));
				
				/* getOverAllStats error function */
				function getOverAllStatsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getOverAllStats success function */
				function getOverAllStatsSuccess(response){
					return(response);
				}
			}
			
			/* 
			 * purpose: get Cluster Filtered Data
			 * created: 20th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getClusterFilteredData(params, filteredParams,url){
				var apiUrl = EHUB_API + url;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: params,
					data: filteredParams
				});
				return(request
						.then(getClusterFilteredDataSuccess)
						.catch(getClusterFilteredDataError));
				
				/* getClusterFilteredData error function */
				function getClusterFilteredDataError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getClusterFilteredData success function */
				function getClusterFilteredDataSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get customer percentage per cluster
			 * created: 21st Mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
			function getCustomerDetails(cstmrId, params){
				var apiUrl = EHUB_API + 'leadGeneration/uc1/1_4/customer_details/' + cstmrId;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getCustomerDetailsSuccess)
						.catch(getCustomerDetailsError));
				
				/* getCustomerDetails error function */
				function getCustomerDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCustomerDetails success function */
				function getCustomerDetailsSuccess(response){
					return(response);
				}
			}

			/* 
			 * purpose: get card duration data
			 * created: 21st Mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
			function getCardDurationDetails(clId, params,url,data, bucketCount){
				var apiUrl = EHUB_API + url + clId + "/" + bucketCount;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: params,
					data :data
				});
				return(request
						.then(getCardDurationDetailsSuccess)
						.catch(getCardDurationDetailsError));
				
				/* getCardDurationDetails error function */
				function getCardDurationDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCardDurationDetails success function */
				function getCardDurationDetailsSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get issued duration data
			 * created: 30 Mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: ram
			 */
			function getfetchissueDetails(clId, params,url,data){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: params,
					data:data
				});
				return(request
						.then(getfetchissueDetailsSuccess)
						.catch(getfetchissueDetailsError));
				
				/* getfetchissueDetails error function */
				function getfetchissueDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getfetchissueDetails success function */
				function getfetchissueDetailsSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get clusters Info By ClusterId
			 * created: 22 Mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getclustersInfoByClusterId(clId, params, url, data){
				var apiUrl = EHUB_API + url+ clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(getclustersInfoByClusterIdSuccess)
						.catch(getclustersInfoByClusterIdError));
				
				/* getclustersInfoByClusterId error function */
				function getclustersInfoByClusterIdError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getclustersInfoByClusterId success function */
				function getclustersInfoByClusterIdSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Local Abroad
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getLocalAbroad(clId, params,url){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getLocalAbroadSuccess)
						.catch(getLocalAbroadError));
				
				/* getLocalAbroad error function */
				function getLocalAbroadError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getLocalAbroad success function */
				function getLocalAbroadSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Online Onsite
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getOnlineOnsite(clId, params,url){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getOnlineOnsiteSuccess)
						.catch(getOnlineOnsiteError));
				
				/* getOnlineOnsite error function */
				function getOnlineOnsiteError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getOnlineOnsite success function */
				function getOnlineOnsiteSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Installments Standard
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getInstallmentsStandard(clId, params,url){
				var apiUrl = EHUB_API + url + clId;
				var request = $http({
					url: apiUrl,
					method: 'GET',
					params: params
				});
				return(request
						.then(getInstallmentsStandardSuccess)
						.catch(getInstallmentsStandardError));
				
				/* getInstallmentsStandard error function */
				function getInstallmentsStandardError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getInstallmentsStandard success function */
				function getInstallmentsStandardSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Filtered Data By Cluster Id
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getFilteredDataByClusterId(clId, params, data){
				var apiUrl = EHUB_API + 'leadGeneration/uc1/1_3/data/' + clId;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: params,
					data: data
				});
				return(request
						.then(getFilteredDataByClusterIdSuccess)
						.catch(getFilteredDataByClusterIdError));
				
				/* getFilteredDataByClusterId error function */
				function getFilteredDataByClusterIdError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getFilteredDataByClusterId success function */
				function getFilteredDataByClusterIdSuccess(response){
					return(response);
				}
			}
			/* 
			 * purpose: get Filtered Data By Cluster Id
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: Ram singh
			 */
			function getClubDataByClusterId(clId,params, data){
					var apiUrl = EHUB_API + 'leadGeneration/uc2/2_3/club_stats/' + clId;
					var request = $http({
						url: apiUrl,
						method: 'GET',
						params: params,
						data: data
					});
					return(request
							.then(getClubDataByClusterIdSuccess)
							.catch(getClubDataByClusterIdError));
					
					/* getClubDataByClusterId error function */
					function getClubDataByClusterIdError(response){
						if(!angular.isObject(response.data) || !response.data.message){
							return($q.reject(response.data));
						}
						return($q.reject(response.data.message));
					}
					/* getClubDataByClusterId success function */
					function getClubDataByClusterIdSuccess(response){
						return(response);
					}
				}
				/* 
			 * purpose: get club connection  Data By Cluster Id
			 * created: 23 Mar 2018
			 * params: clId(integer), params(object)
			 * return: success, error functions
			 * author: Ram singh
			 */
			function getClubconnectionDetails(clId, params, data){
					var apiUrl = EHUB_API + 'leadGeneration/uc2/2_4/club_connections/' + clId;
					var request = $http({
						url: apiUrl,
						method: 'POST',
						params: params,
						data: data
					});
					return(request
							.then(getClubconnectionDetailsSuccess)
							.catch(getClubconnectionDetailsError));
					
					/* getClubconnectionDetails error function */
					function getClubconnectionDetailsError(response){
						if(!angular.isObject(response.data) || !response.data.message){
							return($q.reject(response.data));
						}
						return($q.reject(response.data.message));
					}
					/* getClubconnectionDetails success function */
					function getClubconnectionDetailsSuccess(response){
						return(response);
					}
				}
				/* 
			 * purpose: get connected club details
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: zameer
			 */
			function getconnectedclubdetails(clId, parmas,data,club){
				var apiUrl = EHUB_API + 'leadGeneration/uc2/2_4/club_connection_stats/' + clId+'/'+club.club_one_name+'/'+club.club_two_name;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					params: parmas,
					data :data
				});
				return(request
						.then(getCityDetailsSuccess)
						.catch(getCityDetailsError));
				
				/* getconnectedclubdetails error function */
				function getCityDetailsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getconnectedclubdetails success function */
				function getCityDetailsSuccess(response){
					return(response);
				}
			}
			
	//----------------------ISRA CARD  UC1_WITH_STORY---------------------------------------------//
			
			/*
			 * purpose: get stories
			 * created: 25th june 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getStories(){
				var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_2/stories'+'?token='+$rootScope.ehubObject.token;
					var request = $http({
						url: apiUrl,
						method: 'GET',
					});
				return(request
						.then(getStoriesSuccess)
						.catch(getStoriesError));
				
				/* getStories error function */
				function getStoriesError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function getStoriesSuccess(response){
					return(response);
				}
			}
			 /* purpose: get Cluster 
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getCluster(storyId,params){
				var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_2/clusters/'+storyId+'?token='+$rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(getClusterSuccess)
						.catch(getClusterError));
				
				/* getconnectedclubdetails error function */
				function getClusterError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getconnectedclubdetails success function */
				function getClusterSuccess(response){
					return(response);
				}
			}
			
			 /* purpose: get distribution 
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getDistribution(clusterId,attrName,attrType,distrubtionCount,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_2/distributions/'+StoryId +'/'+clusterId+'/'+attrName+'/'+attrType+'/'+distrubtionCount+'?token='+$rootScope.ehubObject.token
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_2/distributions/'+clusterId+'/'+attrName+'/'+attrType+'/'+distrubtionCount+'?token='+$rootScope.ehubObject.token
				}
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(getDistributionSuccess)
						.catch(getDistributionError));
				
				/* getconnectedclubdetails error function */
				function getDistributionError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getconnectedclubdetails success function */
				function getDistributionSuccess(response){
					return(response);
				}
			}

			 /* purpose: get Customer Detail 
			 * created: 17th mar 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getCustomerDetail(clusterId,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_2/customers/'+StoryId +'/'+clusterId+'?token='+$rootScope.ehubObject.token
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_2/customers/'+clusterId+'?token='+$rootScope.ehubObject.token
				}
		    	var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(getCustomerDetailSuccess)
						.catch(getCustomerDetailError));
				
				/* getCustomerDetail error function */
				function getCustomerDetailError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCustomerDetail success function */
				function getCustomerDetailSuccess(response){
					return(response);
				}
			}
			
			
			/*
			 * purpose: get favourite story
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			
			function getfavouritestory(){
				var apiUrl = EHUB_API + 'story/getFavouriteStory'+'?token='+$rootScope.ehubObject.token;
					var request = $http({
						url: apiUrl,
						method: 'GET',
					});
				return(request
						.then(getfavouritestorySuccess)
						.catch(getfavouritestoryError));
				
				/* getStories error function */
				function getfavouritestoryError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function getfavouritestorySuccess(response){
					return(response);
				}
			}
			
			 /* purpose: post favourite story
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postFavouriteStory(storyId,params){
				var apiUrl = EHUB_API + 'story/markAsFavourite'+'?token='+$rootScope.ehubObject.token+'&storyId='+storyId
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postFavouriteStorySuccess)
						.catch(postFavouriteStoryError));
				
				/* getCustomerDetail error function */
				function postFavouriteStoryError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCustomerDetail success function */
				function postFavouriteStorySuccess(response){
					return(response);
				}
			}
			
			
			
			/*
			 * purpose: get cluster detail by Id
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			
			function getClusterDetail(clusterId,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_2/cluster/'+StoryId+'/'+clusterId+'?token='+$rootScope.ehubObject.token
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_2/cluster/'+clusterId+'?token='+$rootScope.ehubObject.token
				}
				
				var request = $http({
						url: apiUrl,
						method: 'GET',
						data :params
						
					});
				return(request
						.then(getClusterDetailSuccess)
						.catch(getClusterDetailError));
				
				/* getStories error function */
				function getClusterDetailError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function getClusterDetailSuccess(response){
					return(response);
				}
			}
			
			
		
			/*

			 * purpose: get customer detail by customerId
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getCustomerDetailById(customerId,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_4/customer_details/'+customerId+'?token='+$rootScope.ehubObject.token
					
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_4/customer_details/'+customerId+'?token='+$rootScope.ehubObject.token
				}	
				var request = $http({
						url: apiUrl,
						method: 'GET',
						data :params
						
					});
				return(request
						.then(getCustomerDetailByIdSuccess)
						.catch(getCustomerDetailByIdError));
				
				/* getStories error function */
				function getCustomerDetailByIdError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function getCustomerDetailByIdSuccess(response){
					return(response);
				}
			}
			


			/*
			 * purpose: get favourite story
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getGraphCustomerById(customerId,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_4/graph/'+customerId+'?token='+$rootScope.ehubObject.token
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_4/graph/'+customerId+'?token='+$rootScope.ehubObject.token
				}	
				var request = $http({
						url: apiUrl,
						method: 'GET',
						data :params
						
					});
				return(request
						.then(getGraphCustomerByIdSuccess)
						.catch(getGraphCustomerByIdError));
				
				/* getStories error function */
				function getGraphCustomerByIdError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function getGraphCustomerByIdSuccess(response){
					return(response);
				}
			}
			

			/*

			 * purpose: get customer__stories_clusters by customerId
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function customer__stories_clusters(customerId,params){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API + 'leadGeneration/uc1_with_story/1_4/customer__stories_clusters/'+customerId+'?token='+$rootScope.ehubObject.token
				}else{
					var apiUrl = EHUB_API + 'leadGeneration/uc3/3_4/customer_stories_clusters/'+customerId+'?token='+$rootScope.ehubObject.token
				}		
				var request = $http({
						url: apiUrl,
						method: 'GET',
						data :params
						
					});
				return(request
						.then(customer__stories_clustersSuccess)
						.catch(customer__stories_clustersError));
				
				/* getStories error function */
				function customer__stories_clustersError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStories success function */
				function customer__stories_clustersSuccess(response){
					return(response);
				}
			}
			
//-------------------------------UC3 API---------------------------------------------------
			/*

			 * purpose: Uc3overall_stats
			 * created: 1st August 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function Uc3overall_stats(){
				var apiUrl = EHUB_API + 'leadGeneration/uc3/3_2/overall_stats'+'?token='+$rootScope.ehubObject.token
					var request = $http({
						url: apiUrl,
						method: 'GET',
						
					});
				return(request
						.then(Uc3overall_statsSuccess)
						.catch(Uc3overall_statsError));
				
				/* Uc3overall_stats error function */
				function Uc3overall_statsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* Uc3overall_stats success function */
				function Uc3overall_statsSuccess(response){
					return(response);
				}
			}
			
			
			
			
			
			/* purpose: getUc3clusters
			 * created: 1st August 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  getUc3clusters(params){
				var apiUrl = EHUB_API + 'leadGeneration/uc3/3_2/clusters'+'?token='+$rootScope.ehubObject.token
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(getUc3clustersSuccess)
						.catch(getUc3clustersError));
				
				/* getCustomerDetail error function */
				function getUc3clustersError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCustomerDetail success function */
				function getUc3clustersSuccess(response){
					return(response);
				}
			}
			
			
			//----------------Naming api-----------------------------------------------------------
			
			 /* purpose: post story name
			 * created: 4th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postsetStoryName(storyId,params){
				var apiUrl = EHUB_API + 'leadGeneration/naming/setStoryName/'+storyId+'?token='+$rootScope.ehubObject.token
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postsetStoryNameSuccess)
						.catch(postsetStoryNameError));
				
				/* postsetStoryName error function */
				function postsetStoryNameError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postsetStoryName success function */
				function postsetStoryNameSuccess(response){
					return(response);
				}
			}
			
			 /* purpose: get story name
			 * created: 5th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			
			function getStoryByName(storyName){
				var apiUrl = EHUB_API + 'leadGeneration/stories/'+storyName+'?token='+$rootScope.ehubObject.token
					var request = $http({
						url: apiUrl,
						method: 'GET',
						
					});
				return(request
						.then(getStoryByNameSuccess)
						.catch(getStoryByNameError));
				
				/* getStoryByName error function */
				function getStoryByNameError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getStoryByName success function */
				function getStoryByNameSuccess(response){
					return(response);
				}
			}
			
			
			 /* purpose: post cluster name
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postsetClusterName(storyId,clusterId,params){
				var apiUrl = EHUB_API + 'leadGeneration/naming/setClusterName/'+storyId+'/'+clusterId+'?token='+$rootScope.ehubObject.token
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(setClusterNameSuccess)
						.catch(setClusterNameError));
				
				/* setClusterName error function */
				function setClusterNameError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* setClusterName success function */
				function setClusterNameSuccess(response){
					return(response);
				}
			}
			
			
			/* purpose: Delete story by Id
			 * created: 5th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			
			function deleteStoryById(storyId,storyName){
				var apiUrl = EHUB_API + 'leadGeneration/stories/'+storyId+'?token='+$rootScope.ehubObject.token+'&storyName='+storyName;
					var request = $http({
						url: apiUrl,
						method: 'DELETE',
					});
				return(request
						.then(deleteStoryByIdSuccess)
						.catch(deleteStoryByIdError));
				
				/* deleteStoryById error function */
				function deleteStoryByIdError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* deleteStoryById success function */
				function deleteStoryByIdSuccess(response){
					return(response);
				}
			}
			
			 /* purpose: post story name
			 * created: 4th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postNewStoryName(params){
				var apiUrl = EHUB_API + "leadGeneration/stories"+'?token='+$rootScope.ehubObject.token+'&storyName='+params.name;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postNewStoryNameSuccess)
						.catch(postNewStoryNameError));
				
				/* postNewStoryName error function */
				function postNewStoryNameError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postNewStoryName success function */
				function postNewStoryNameSuccess(response){
					return(response);
				}
			}
			
			
			
			 /* purpose: post cluster name
			 * created: 10th july 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			function getstageRequest(){
                var apiUrl =EHUB_API + "workflow/es/workflow?token="+$rootScope.ehubObject.token;
		        var param = {};
                var request = $http({
		            method:'GET' ,
		            url: apiUrl,
                    data:param                    
		        });
		        return(request
		                .then(getstageRequestSuccess)
		                .catch(getstageRequestError));
		
		        /*getstageRequest error function*/
		        function getstageRequestError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getstageRequest success function*/
		        function getstageRequestSuccess(response) {
		            return(response);
		        }
			}
			
			
			function comparisonChartPlot(path){
				 var apiUrl = path;
			        var param = {};
	                var request = $http({
			            method:'GET' ,
			            url: apiUrl,
			        });
			        return(request
			                .then(comparisonChartPlotSuccess)
			                .catch(comparisonChartPlotError));
			
			        /*comparisonChartPlot error function*/
			        function comparisonChartPlotError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			       /*comparisonChartPlot success function*/
			        function comparisonChartPlotSuccess(response) {
			            return(response);
			        }
			}
		
			/* purpose: get Club Connections
			 * created: 13th november 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: swathi
			 */
			function getClubConnections(params){
				var apiUrl = EHUB_API + "leadGeneration/uc2/2_4/club_connections_stats/" + params.clusterId + "/" + params.clubName +'?token='+ $rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data: {}
				});
				return(request
						.then(getClubConnectionsSuccess)
						.catch(getClubConnectionsError));
				
				/* getClubConnections error function */
				function getClubConnectionsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getClubConnections success function */
				function getClubConnectionsSuccess(response){
					return(response);
				}
			}
			
	//--------------------UC4 Api's-------------------------------------------------
			
			 /* purpose: get Customers_count
			 * created: 27th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */
			
			function getCustomers_count(filterParam){
				var apiUrl = EHUB_API + 'leadGeneration/uc4/customers_count/?token='+$rootScope.ehubObject.token
				var request = $http({
						url: apiUrl,
						method: 'POST',
						data: filterParam
					});
				return(request
						.then(getCustomers_countSuccess)
						.catch(getCustomers_countError));
				
				/* getCustomers_count error function */
				function getCustomers_countError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getCustomers_count success function */
				function getCustomers_countSuccess(response){
					return(response);
				}
			}
			
			
			 /* purpose: post uc3 distributions
			 * created: 27th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postUC3distributions(attrName, attrType, distrubtionCount, params){
				var apiUrl = EHUB_API + "leadGeneration/uc4/distributions"+'/'+attrName+'/'+attrType+'/'+distrubtionCount+'?token='+$rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postUC3distributionsSuccess)
						.catch(postUC3distributionsError));
				
				/* postUC3distributions error function */
				function postUC3distributionsError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postUC3distributions success function */
				function postUC3distributionsSuccess(response){
					return(response);
				}
			}
			

			 /* purpose: post topEarlyAdopter
			 * created: 28th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postTop_early_adapters(params){
				var apiUrl = EHUB_API + "leadGeneration/uc4/top_early_adapters"+'?token='+$rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postTop_early_adaptersSuccess)
						.catch(postTop_early_adaptersError));
				
				/* postTop_early_adapters error function */
				function postTop_early_adaptersError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postTop_early_adapters success function */
				function postTop_early_adaptersSuccess(response){
					return(response);
				}
			}
			
			 /* purpose: post popular_merchant_types
			 * created: 28th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postPopular_merchant_types(params){
				var apiUrl = EHUB_API + "leadGeneration/uc4/popular_merchant_types"+'?token='+$rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postPopular_merchant_typesSuccess)
						.catch(postPopular_merchant_typesError));
				
				/* postPopular_merchant_types error function */
				function postPopular_merchant_typesError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postPopular_merchant_types success function */
				function postPopular_merchant_typesSuccess(response){
					return(response);
				}
			}

			
			 /* purpose: post customers
			 * created: 28th september 2018
			 * params: params(object)
			 * return: success, error functions
			 * author: varsha
			 */

			function  postUc3Customers(params){
				var apiUrl = EHUB_API + "leadGeneration/uc4/customers"+'?token='+$rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data :params
				});
				return(request
						.then(postUc3CustomersSuccess)
						.catch(postUc3CustomersError));
				
				/* postUc3Customers error function */
				function postUc3CustomersError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* postUc3Customers success function */
				function postUc3CustomersSuccess(response){
					return(response);
				}
			}
			
			
			 /* purpose: get search customer
			 * created: 3rd December 2018
			 * params: customerId
			 * author: varsha
			 */
			
			function getsearchCustomer(customerId,clusterId){
				var StoryId = sessionStorage.getItem("storyId")
				if(StoryId !='undefined'){
					var apiUrl = EHUB_API+'leadGeneration/uc1_with_story/1_4/search_customer/'+StoryId+'/'+clusterId+'/'+customerId+'?token='+$rootScope.ehubObject.token;
				}else{
					var apiUrl = EHUB_API+'leadGeneration/uc3/3_4/search_customer/'+clusterId+'/'+customerId+'?token='+$rootScope.ehubObject.token;
				}	
				var request = $http({
						url: apiUrl,
						method: 'GET',
						
					});
				return(request
						.then(getsearchCustomerSuccess)
						.catch(getsearchCustomerError));
				
				/* getsearchCustomer error function */
				function getsearchCustomerError(response){
					if(!angular.isObject(response.data) || !response.data.message){
						return($q.reject(response.data));
					}
					return($q.reject(response.data.message));
				}
				/* getsearchCustomer success function */
				function getsearchCustomerSuccess(response){
					return(response);
				}
			}
			
			
			

		}