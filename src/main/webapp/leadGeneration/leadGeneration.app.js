'use strict';

var elementApp = angular.module('leadGenerationApp', ['ui.bootstrap', 'ui.router','ngFileUpload','ngFlash', 'ngTouch', 'ui.grid', 'ui.grid.selection']);
var elementModule = elementApp;
