'use strict';

elementApp.config(['$stateProvider', '$provide', '$qProvider','$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', function($stateProvider, $provide,$qProvider, $urlRouterProvider, EHUB_FE_API, $locationProvider){
	 $provide.factory('$stateProvider', function () {
		 return $stateProvider;
	 });
 	$provide.factory('$urlRouterProvider', function () {
        return $urlRouterProvider;
    });
	
 	 $qProvider.errorOnUnhandledRejections(false);
//	FlashProvider.setTimeout(FLASH_ALERT_DURATION);
//	FlashProvider.setShowClose(FLASH_ALERT_CLOSE);

}]);
