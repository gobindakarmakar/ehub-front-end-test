<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

		<!DOCTYPE html>
		<html ng-app="leadGenerationApp" class="bst_element">

		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<link rel="shortcut icon" href="../assets/images/logo-new.png" />
			<title>Personalization</title>

			<!-------------------------------  Vendor Styles Starts  ------------------------------->
			<link href="./assets/css/lead-generation-vendor.min.css" rel="stylesheet" type="text/css" />
			<link rel="stylesheet" type="text/css" href="../vendor/jasmine/jasmine.css">
			
			<!-- -----------------------------  Vendor Styles Ends  --------------------------------- -->

			<!---------------------------------  Custom Styles Starts  ------------------------------ -->
			<link href="./assets/css/lead-generation-styles.min.css" rel="stylesheet" type="text/css" />
			<link href="../vendor/jquery/css/grid-table.min.css" rel="stylesheet" type="text/css" /><!-- 			<link href="../vendor/angular/css/ui-grid.min.css" rel="stylesheet" type="text/css" /> -->
			
			<!-------------------------------  Custom Styles Ends  --------------------------------->

			<style>input[type=number]::-webkit-inner-spin-button, 
				input[type=number]::-webkit-outer-spin-button { 
				  -webkit-appearance: none; 
				  -moz-appearance: none;
				  appearance: none;
				  margin: 0;      
				}</style>
		
		
		
	
		</head>

		<body ng-cloak class="custom-scroll-wrapper " ng-class="{'bst_element_header bst_element_body dark-grey-dashboard' : stateForIsra == 'dualCardHolderDetails'}">
			<div ng-show="topPanelPreview">
				<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
			</div>
			<div ui-view></div>
			<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>
			<!--  Chat Panel Wrapper Starts  -->
			<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
				<!--  Chat Panel Wrapper Ends  -->
				<!--==============================================================================
		*************************  Local Path Starts ********************************
		==============================================================================-->
				<!--------------------------------  Common Vendor Js Starts  ------------------------------->
				<script src='../vendor/jquery/js/jquery.min.js'></script>
				<script src='../vendor/jquery/js/jquery-ui.js'></script>
				<script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
				<script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
				<script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
				<script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
				<script src='../vendor/jquery/js/bootstrap.min.js'></script>
				<script src='../vendor/jquery/js/displacy-entity.min.js'></script>
				

				<script src='../vendor//angular/js/angular.min.js'></script>
				<script src='../vendor//angular/js/angular-ui-router.min.js'></script>
				<script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
				<script src='../vendor//angular/js/html2canvas.js'></script>
				<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
				
				
				<script src='../vendor/angular/js/angular-touch.js'></script>
				<script src='../vendor/angular/js/angular-animate.js'></script>
				<script src='../vendor/angular/js/angular-aria.js'></script>
				<script src='../vendor/angular/js/ui-grid.min.js'></script>
				
<!-- 				<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular-touch.js"></script> -->
<!-- 			    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular-animate.js"></script> -->
<!-- 			    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular-aria.js"></script> -->
<!-- 				<script src="https://cdn.jsdelivr.net/gh/angular-ui/bower-ui-grid/ui-grid.min.js"></script> -->
				
				<script src='../vendor//angular/js/angular-flash.min.js'></script>
				<script src='../vendor/angular/js/angular_multiselect.js'></script>

				<script src='../charts/d3.v3.min.js'></script>
				<script src='../charts/d3.v4.min.js'></script>
				<script src='../charts/d3v3.js'></script>
				<script src='../charts/d3js.js'></script>

				<script src='../scripts/VLA/js/cola.v3.min.js'></script>
				<script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
				<script src='../scripts/VLA/js/cytoscape-cola.js'></script>
				<script src='../scripts/VLA/js/jquery.qtip.js'></script>
				<script src='../scripts/VLA/js/cytoscape-qtip.js'></script>
				<!--------------------------------  Common Vendor Js Ends    ------------------------------->

				<!--------------------------------  Transaction Intelligence Vendor Js Starts  ------------------------------->
				<script src='../scripts/VLA/js/cytoscape-cose-bilkent.js'></script>
				<script src='../scripts/VLA/js/cytoscape-cxtmenu.js'></script>
				<script src='../scripts/VLA/js/cytoscape-markov-cluster.js'></script>
				<script src='../scripts/VLA/js/cytoscape-navigator.js'></script>
				<script src='../scripts/VLA/js/cytoscape-ngraph.forcelayout.js'></script>
				<script src='../scripts/VLA/js/cytoscape-panzoom.js'></script>
				<script src='../scripts/VLA/js/cytoscape-undo-redo.js'></script>
				<script src='../scripts/VLA/js/weaver.min.js'></script>
				<script src='../scripts/VLA/js/cytoscape-spread.js'></script>
				<script src='../scripts/VLA/js/random-color.js'></script>
				<script src='../scripts/VLA/js/spectrum.js'></script>
				<script src='../scripts/VLA/js/typeahead.bundle.js'></script>
				<script src='../scripts/VLA/js/jquery.selectric.min.js'></script>
				<script src='../scripts/VLA/js/custom_config.js'></script>
				<script src='../scripts/VLA/js/app.js'></script>
				
			<!-- 	<script src='../scripts/VLA/js/app_locale_ua.js'></script> 
				<script src='../scripts/VLA/js/app_locale_ar.js'></script>  -->
				
				<script src='../scripts/VLA/tagcloud/js/tagcloud.js'></script>
				<script src='../scripts/VLA/js/bluebird.min.js'></script>
				<script src='../scripts/VLA/js/base64toblob.js'></script>
				<script src='../scripts/VLA/js/loadash.js'></script>
				<script src='../vendor/jquery/js/moment.min.js'></script>
				<script src='../vendor/jquery/js/jquery-daterangepicker.js'></script>
				<script src='../vendor/jquery/js/bootstrap-daterangepicker.js'></script>
			 	<script src="../vendor/jquery/js/jquery-ui-timepicker-addon.js"></script>
		        <script src="../vendor/jquery/js/jquery-ui-sliderAccess.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-with-data.min.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-utils.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone.min.js"></script>
		        <script src="../vendor/jquery/js/moment-timezone-with-data-2012-2022.min.js"></script>
				<script src='../vendor/angular/js/angular-moment.min.js'></script>
				<script src='../vendor/angular/js/angular-datatables.min.js'></script>
				<script src='../vendor/angular/js/FileSaver.min.js'></script>
				
				<!--------------------------------  isra Card Vendor Js Ends    ------------------------------->

				<script src="../scripts/app.js"></script>
				<script src="./leadGeneration.app.js"></script>
				<script src="./leadGeneration.config.js"></script>
				<script src="./leadGeneration.run.js"></script>
				
				<script src='./constants/leadGenaration.constant.js'></script>
				<script src='../scripts/common/constants/app.constant.js'></script>
				<script src='../scripts/common/constants/common.constant.js'></script>
				<script src='../scripts/common/directives/common.directives.js'></script>
				<script src='../scripts/discover/constants/discover.constant.js'></script>
				<script src='../scripts/act/constants/act.constant.js'></script>
				<script src='../scripts/common/services/shared.service.js'></script>
				<script src='../scripts/common/services/top.panel.api.service.js'></script>
				<script src='../scripts/common/services/riskScore.service.js'></script>
				<script src='../scripts/common/services/upload.file.service.js'></script>
				<script src='../scripts/discover/services/discover.api.service.js'></script>
				<script src='../scripts/act/services/act.api.service.js'></script>
				<script src='../scripts/act/services/act.graph.service.js'></script>
				<script src="../scripts/common/services/common.service.js"></script>
<!-- 				<script src='../scripts/common/services/top.panel.api.service.js'></script> -->
<!-- 				<script src='../scripts/common/services/upload.file.service.js'></script> -->
<!-- 				<script src='../scripts/common/services/shared.service.js'></script> -->
<!-- 				<script src='../vendor//angular/js/angular-flash.min.js'></script> -->
				<script src="../scripts/common/js/chatbot.controller.js"></script>
				<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
				<!--------------------------------  Common Scripts Js Starts  ------------------------------>

				<!--------------------------------  Common Scripts Js Ends   ------------------------------->

				<!--------------------------------  isra Card Charts Js Starts  ------------------------------>
				<script src='../scripts/common/js/top.panel.controller.js'></script>
				<script src='../scripts/common/js/advanced.search.controller.js'></script>
				<script src='../scripts/common/js/user.events.controller.js'></script>
				<script src='../scripts/common/js/my.clipboard.controller.js'></script>
				<script src="../scripts/common/js/notification.controller.js"></script>
				<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
				<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
				<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
				<script src="../scripts/common/modal/js/create.event.controller.js"></script>
				<script src="../scripts/common/modal/js/participants.event.controller.js"></script> 
				<script src='../scripts/common/js/submenu.controller.js'></script>
				<script src="./services/leadGeneration.api.service.js"></script>
				<script src="./services/leadGeneration.graph.service.js"></script>
				<script src="./controllers/leadGenerationLanding.controller.js"></script>
				<script src="./controllers/leadGenerationCluster.controller.js"></script>
				<script src="./controllers/leadGenerationUc2ClustersController.js"></script>
				<script src="./controllers/leadGeneration_GeneralCardHolderDetails.controller.js"></script>
				<script src="./controllers/leadGeneration_DualCardHolderDetails.controller.js"></script>
				<script src="./controllers/leadGenerationEarlyAdopters.controller.js"></script>
				<script src="./controllers/leadGenerationCustomerPage.controller.js"></script>
				<script src="./controllers/leadGenerationCommonfilter.js"></script>
	
				<script src="./testing/leadGenerationCluster.test.js"></script>
				<!--------------------------------  isra Card Scripts Js Ends   ------------------------------->

				<!--------------------------------  Isra card Charts Js Starts  ---------------------------- -->
				<script src='../charts/topojson.min.js'></script>
				<script src='../charts/datamaps.all.js'></script>
				<script src='../charts/WorldMap/js/WorldChart.js'></script>
				<script src='../charts/lineChart.js'></script>
				<script src='../charts/reusablePie.js'></script>
				<script src='../charts/NegativeBarWithTooltip.js'></script>
				<script src='../charts/stackedtime.js'></script>
				<script src='../charts/cloud.js'></script>
				<script src='../charts/hotTopics.js'></script>
				<script src='../charts/groupedColumChart.js'></script>
				<script src="../scripts/enrich/services/enrich.graph.service.js"></script>
				<script src="../entity/services/entity.graph.services.js"></script>
				<script src="../charts/cloud.js"></script>
				<script src="../charts/bubbleEntity.js"></script>
				<script src="../charts/stackedtime.js"></script>
				<script src="../charts/topojson.min.js"></script>
				<script src="../charts/WorldMap/js/WorldChart.js"></script>
				<script src="../charts/timeformat.js"></script>
				<script src="../charts/timeformatsupport.js"></script>
				<script src="../charts/SimpleBarChart.js"></script>
				<script src="../charts/verticalNegativeBarChart.js"></script>
				<script src="../charts/ordinalLineChart.js"></script>
				<script src="../charts/areaLineChart.js"></script>
				<script src="../charts/networkChartEntity.js"></script>
				<script src="../charts/sunBurstIcicle.js"></script>
				<script src="../charts/comparisonProgressBar.js"></script>
				
				<!-------------------------------- Isra card  Chart scripts Js Ends   ------------------------------->


               <!-- --------------------------Jasmine Testing Js -------------------------------------------->
              <!--  <script src='../vendor/jasmine/jasmine.js'></script>
				<script src='../vendor/jasmine/jasmine-html.min.js'></script>
				<script src='../vendor/jasmine/boot.js'></script>
				<script src='../vendor/jasmine/angular.mock.js'></script> -->
					
				
			  	<!-------------------------------- Jasmine Testing Js Ends  ------------------------------->
			  
				<!--==============================================================================
			**********************  Local Path Ends *********************
		==============================================================================*/-->


				<!--==============================================================================
			**********************  Gulp Path Starts *********************
		==============================================================================*/-->

				<!-- 		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
				<!-- <script src="../vendor/common-vendor.min.js"></script> -->
				<!-- 		<!--------------------------------  Common Vendor Js Ends    ------------------------------->

				<!-- 		<!--------------------------------  leadgeneration Vendor Js Starts  ----------------------------- -->
				<!-- <script src="./assets/minifiedJs/leadgeneration-vendor.min.js"></script> -->
				<!-- 		<!--------------------------------  leadgeneration Vendor Js Ends    ------------------------------->

<!-- 				<script src="../scripts/app.js"></script>
				<script src="./leadGeneration.app.js"></script>
				<script src="./leadGeneration.config.js"></script>
				<script src="./leadGeneration.run.js"></script>
				<script src="../scripts/common/constants/app.constant.js"></script> -->
				<!-- 	    ------------------------------  Common Scripts Js Starts  ---------------------------- -->
				<!-- <script src="../scripts/common-scripts.min.js"></script> -->
				<!-- 		<!--------------------------------  Common Scripts Js Ends   ------------------------------->

				<!-- 		<!-------------------------------- leadgeneration Scripts Js Starts  ------------------------------>
				<!-- <script src="./assets/minifiedJs/leadgeneration-scripts.min.js"></script> -->
				<!-- 		<script src="./assets/minifiedJs/transactionIntelligence-scripts.min.js"></script> -->
				<!-- 		<!--------------------------------  leadgeneration Scripts Js Ends   ------------------------------->

				<!-- 		<!--------------------------------  leadgeneration Charts Js Starts  ------------------------------>
				<!-- <script src="./assets/minifiedJs/leadgeneration-charts.min.js"></script> -->
				<!-- 		<script src="./assets/minifiedJs/transactionIntelligence-charts.min.js"></script> -->
				<!-- 		<!--------------------------------  leadgeneration Scripts Js Ends   ------------------------------->
				
				<!--new minified Js start -->
				<!-- <script src="../vendor/common.vendor.new.min.js"></script>
				<script src="./assets/minifiedJs/leadGeneration.vendor.new.min.js"></script>
                <script src="./assets/minifiedJs/leadGeneration.intialize.new.min.js"></script>
                <script src="../scripts/common.scripts.new.min.js"></script>
				<script src="./assets/minifiedJs/leadGeneration.scripts.new.min.js"></script>
                <script src="./services/leadGeneration.api.service.js"></script>
				<script src="./services/leadGeneration.graph.service.js"></script>
				<script src="./controllers/leadGenerationLanding.controller.js"></script>
				<script src="./controllers/leadGenerationCluster.controller.js"></script>
				<script src="./controllers/leadGenerationUc2ClustersController.js"></script>
				<script src="./controllers/leadGeneration_GeneralCardHolderDetails.controller.js"></script>
				<script src="./controllers/leadGeneration_DualCardHolderDetails.controller.js"></script>
				<script src="./controllers/leadGenerationEarlyAdopters.controller.js"></script>
				<script src="./controllers/leadGenerationCustomerPage.controller.js"></script>
				<script src="./controllers/leadGenerationCommonfilter.js"></script>
                <script src="./testing/leadGenerationCluster.test.js"></script> 
                <script src="./assets/minifiedJs/leadGeneration.charts.new.min.js"></script> -->
				<!--new minified Js end -->
				
				<!--==============================================================================
				**********************  Gulp Path Ends *********************
		==============================================================================*/ -->

		</body>
<style>.avg_Exp_Per_Customer span:before{
    content:'';
    display:block;
    width:0;
    height:0;
    position:absolute;
    
    border-top: 8px solid transparent;
    border-bottom: 8px solid transparent; 
    border-left:8px solid #1d242c;
    right:-8px;
    
    top:7px;
}</style>
		</html>