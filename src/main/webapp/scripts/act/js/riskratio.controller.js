'use strict';
angular.module('ehubApp')
	   .controller('RiskRatioController', riskRatioController);

riskRatioController.$inject = [
			'$scope', 
			'$rootScope',
			'$stateParams',
			'ActApiService',
			'ActRiskRatioConst',
			'$uibModal'
		];
		
		function riskRatioController(
				$scope, 
				$rootScope,
				$stateParams,
				ActApiService,
				ActRiskRatioConst,
				$uibModal) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.riskRatiotoggleGraph = true;
		//$scope.riskRatioDatas = [];
		var clusterbubbleData = [];
		var actualHotTopicsData;
		var isApiLoaded = false;
		if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			$scope.riskRatioPreloader = true;
			loadRiskRatio();
			$rootScope.$on("loadRiskRatio", function(){
				loadRiskRatio();
		    });
		}else{
			$scope.riskRatioDatas = [];
			angular.element("#riskRatioBubbleChart").html('<p class="no-data">No Data Found</p>');
		}
		/*
	     * @purpose: load risk data of case 
	     * @created: 05 oct 2017
	     * @params: null
	     * @return: no
	     * @author: swathi
	    */
	    function loadRiskRatio(){
			var params = {
				token: $scope.token
			};
			ActApiService.getRiskDataByCaseId(params, $scope.case_id).then(function(response) {
				$scope.AllNames = [];
				if(response.data.body.length != 0){
					isApiLoaded = true;
					angular.forEach(response.data.body,function(d){
						$scope.AllNames.push(d.name);
					});
					$scope.AllNames = _.uniq($scope.AllNames);
					actualHotTopicsData = response.data;
					var options = {
					    Uri: null,
					    container: "#riskRatioBubbleChart",
					    dataParam: response.data,
					    height: ActRiskRatioConst.height,
					    isheader: false
					};
					ClusterBubble(options,"noFilter","noType");
					 $(window).on("resize", function() {
			              $("#riskRatioBubbleChart").empty();
			              ClusterBubble(options,"noFilter","noType");
			        });
					 
					$('#riskRatioWidget').mCustomScrollbar({
						axis : "y",
						theme : "minimal"
					});
				}else{
					angular.element("#riskRatioBubbleChart").html('<p class="no-data">No Data Found</p>');
				}
				$scope.riskRatioPreloader = false;
			},function(error){
				$scope.riskRatioPreloader = false;
				angular.element('#riskRatioBubbleChart').html('<p class="no-data">'+ error.responseMessage +'</p>');
			});
		}
	    /**
    	 * Function to load ClusterBubble chart
    	 */
	    function ClusterBubble(options,strings,Types) { 
	        loadclusterBubbleChart(options,strings,Types);
	        /* responsiveness */
	        $(window).on("resize", function() {
	              $("#riskRatioBubbleChart").empty();
	            loadclusterBubbleChart(options,strings,Types);
	        });
	    }
	        // ---------------------------------------------------------------------------
	        /**
	    	 * Function to handle data according to format of ClusterBubble Chart
	    	 * library
	    	 */

	        function handleClusteredChartData(data,string,type) {
	            var lowRiskArray = [];
	            var mediumRiskArray = [];
	            var highRiskArray = [];
	            angular.forEach(data, function(d) {
	            	if((d.name.toLowerCase().indexOf(string.toLowerCase()) > -1) || string =="noFilter"){
	                var cumilativeRisk = getriskRation(d.direct, d.indirect, d.transactional);
	                
	                if (cumilativeRisk > ActRiskRatioConst.highRiskVal) {
	                    highRiskArray.push({
	                        "name": d.name,
	                        "group": "HIGH",
	                        "size": cumilativeRisk
	                    });
	                } else if (cumilativeRisk > ActRiskRatioConst.mediumRiskVal) {
	                    mediumRiskArray.push({
	                        "name": d.name,
	                        "group": "MEDIUM",
	                        "size": cumilativeRisk
	                    });
	                } else {
	                    lowRiskArray.push({
	                        "name": d.name,
	                        "group": "LOW",
	                        "size": cumilativeRisk
	                    });
	                }
	            }
	            });
	            if(type == "noType"){
	            var finalData = [{
	                "label": "LOW",
	                "value": lowRiskArray.length,
	                "children": lowRiskArray
	            }, {
	                "label": "MEDIUM",
	                "value": mediumRiskArray.length,
	                "children": mediumRiskArray
	            }, {
	                "label": "HIGH",
	                "value": highRiskArray.length,
	                "children": highRiskArray
	            }];
	            }else{
	            	switch(type){
	            	case "HIGH":
	            		var finalData = [{
	    	                "label": "LOW",
	    	                "value": 0,
	    	                "children": []
	    	            }, {
	    	                "label": "MEDIUM",
	    	                "value": 0,
	    	                "children": []
	    	            }, {
	    	                "label": "HIGH",
	    	                "value": highRiskArray.length,
	    	                "children": highRiskArray
	    	            }];
	            		break;
	            	case "LOW":
	            		var finalData = [{
	    	                "label": "LOW",
	    	                "value": lowRiskArray.length,
	    	                "children": lowRiskArray
	    	            }, {
	    	                "label": "MEDIUM",
	    	                "value": 0,
	    	                "children": []
	    	            }, {
	    	                "label": "HIGH",
	    	                "value": 0,
	    	                "children": []
	    	            }];
	            		break;
	            	case "MEDIUM":
	            		var finalData = [{
	    	                "label": "LOW",
	    	                "value": 0,
	    	                "children": []
	    	            }, {
	    	                "label": "MEDIUM",
	    	                "value": mediumRiskArray.length,
	    	                "children": mediumRiskArray
	    	            }, {
	    	                "label": "HIGH",
	    	                "value": 0,
	    	                "children": []
	    	            }];
	            		break;
	            	} 
	            }
	            return finalData;
	        }
	        /* Calculate risk ratio */
	        function getriskRation(direct, indirect, transactional) {
	            return (1 - [(1 - direct) * (1 - indirect) * (1 - transactional)]) * 100;
	        }
	        /**
	    	 * Function to load data to plot ClusterBubble chart
	    	 */
	        function loadclusterBubbleChart(options,serachkey,riskType) {
	         	$scope.riskRatioDatas = [];
	        	if (options.Uri) {
	            	d3.json(options.Uri, function(error, data) {
	                    clusterbubbleData = handleClusteredChartData(data.body,serachkey,riskType);
	                    
	                    options.data = clusterbubbleData;
	                    if(clusterbubbleData[0].value ==0 && clusterbubbleData[2].value == 0 && clusterbubbleData[1].value == 0){
	    					
	                    }else{
		                    new clusterbubbleChart(options);
	                    }


	            	});	
	            } else {
    	        	clusterbubbleData = handleClusteredChartData(options.dataParam.body,serachkey,riskType);
    	            options.data = clusterbubbleData;
    	            angular.forEach(clusterbubbleData, function (v) {
    	            	angular.forEach(v.children, function (value) {
                			$scope.riskRatioDatas.push({
    	            			name: value.name,
    	            			size: value.size,
    	            			group:value.group 
                			});	
    	            	});
    	            });
    	            var actualOptions = jQuery.extend(true, {}, options);
    	            if($scope.riskRatioDatas.length > 0){
    	            	$scope.maxoptions = actualOptions;
    	            }else{
    	            	$scope.maxoptions = {};
    	            }
    	            if(clusterbubbleData[0].value ==0 && clusterbubbleData[2].value == 0 && clusterbubbleData[1].value == 0){
    					angular.element("#riskRatioBubbleChart").html('<p class="no-data">No Data Found</p>');
    	            }else{
        	            new clusterbubbleChart(actualOptions);
    	            }
	            }
	        }
	    
	    
		 /*
		 * @purpose: open filter modal
		 * @created: 26 Dec 2017		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.openFilterModalForChart =function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return $scope.keysTofilter;
		                },
		                FilteredKeyword:function(){
		                	if($scope.FilteredKeyword == "noFilter"){
			                	return "";
		                	}else{
			                	return $scope.FilteredKeyword;	
		                	}

		                },
		                FilteredKey : function(){
		                	if(!$scope.FilteredKey || $scope.FilteredKey == "noType"){
		                		return "ALL";
		                	}else{
			                	return $scope.FilteredKey;
		                	}

		                },
		                RiskRatio : function(){
		                	return true;
		                },
		                onlySearch : function(){
		                	return false;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
		                	return $scope.AllNames;
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {
	  			if(response)
	  				{handleFiltersApplied(response);}
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 26 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			$scope.FilteredKeyword = "";
			var actualData = jQuery.extend(true,{}, actualHotTopicsData);
			if(!resp.caseSearchKeyword){
				resp.caseSearchKeyword = "noFilter";
			}else{
				$scope.FilteredKeyword = resp.caseSearchKeyword;
			}
			if(!resp.filterApplied.priority){
				resp.filterApplied.priority = "noType";
				$scope.FilteredKey = "ALL";
			}else{
				$scope.FilteredKey = resp.filterApplied.priority;
			}
			
			var options = {
				    Uri: null,
				    container: "#riskRatioBubbleChart",
				    dataParam: actualData,
				    height: ActRiskRatioConst.height,
				    isheader: false
				};
			ClusterBubble(options,resp.caseSearchKeyword,resp.filterApplied.priority);
			ResizeFunction();
		}

		/*
		 * @purpose: open fullscreen modal
		 * @created: 3 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
				
			}, function(){
			});
			
		};
		$scope.ToggleGraphChart =function(){
//			debugger;
			$scope.riskRatiotoggleGraph = true;
//			$("#riskRatioWidget").css("height","280px");
			setTimeout(function(){
				
				ResizeFunction();
			},0);

		};
		$scope.UpdateToggle =function(){
			var heightResp = document.getElementById('riskRationdiv').clientHeight ? document.getElementById('riskRationdiv').clientHeight : 300;
			if(heightResp > 310){
				heightResp = 580; 
			} else{
				heightResp = 280;
			}
			$("#riskRatioWidget").css("height",heightResp+"px");
			$scope.riskRatiotoggleGraph = false;
		};


		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function() {
			if(isApiLoaded == true){
			ResizeFunction();
		}
		});
		
		function ResizeFunction(){
			var heightResp = document.getElementById('riskRationdiv').clientHeight ? document.getElementById('riskRationdiv').clientHeight : 300;
			if(heightResp > 310){
				heightResp = 580; 
			} else{
				heightResp = 280;
			}
			$("#riskRationdiv").find("tbody").css("height",heightResp);
			$scope.FilteredKey = $scope.FilteredKey ? $scope.FilteredKey : "noType";
			if($scope.FilteredKey == "ALL"){
				$scope.FilteredKey = "noType";
			}
			$scope.FilteredKeyword = $scope.FilteredKeyword ? $scope.FilteredKeyword :"noFilter";
			var actualData = jQuery.extend(true,{}, actualHotTopicsData);
			var options = {
				    Uri: null,
				    container: "#riskRatioBubbleChart",
				    dataParam: actualData,
				    height: heightResp,
				    isheader: false
				};
			if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
				ClusterBubble(options,$scope.FilteredKeyword,$scope.FilteredKey);
			}else{
				$scope.riskRatioDatas = [];
				angular.element("#riskRatioBubbleChart").html('<p class="no-data">No Data Found</p>');
			}
		}

}