    'use strict';
    angular.module('ehubApp')
    	   .controller('LinkAnalysisController', linkAnalysisController);

           linkAnalysisController.$inject = [
    		   '$scope',
        	   '$rootScope',
        	   '$timeout',
        	   '$state',
        	   '$stateParams',
        	   'HostPathService',
        	   'EntityApiService',
        	   'ActApiService',
        	   '$uibModal',
        	   'EHUB_API'
        	   
    		];
           
           function linkAnalysisController(
        		   $scope,
        		   $rootScope,
        		   $timeout,
        		   $state,
        		   $stateParams,
        		   HostPathService,
        		   EntityApiService,
        		   ActApiService,
        		   $uibModal,
        		   EHUB_API
        		   ){
        	   
        	   
        	   /*Initializing local variables*/ 
        	   var vla_inst,summaryURL;var isResoveCalled=false;
        	   var config_map = {
        			   "doc":{
        				   "graph":"http://46.101.163.245:9091/graph/api/graph/cybersecurity/0/100"
        			   },
        	            "Adverse News": {
        	            	"graph":EHUB_API+"adverseNews/graph/",
        	                
        	                "summary": EHUB_API+"adverseNews/summary/"
        	            },
        		        "mip":{
        		        	"graph":"http://46.101.209.160:9091/bstworkflowservices/api/graph/"
        		        },
        		        "uri1":"scripts/VLA/WorldMap/data/worldCountries.json",
        		        "uri2":"scripts/VLA/WorldMap/data/data.json"
        	    };
        	   $scope.isVlaLoad = false;
        	   if($stateParams.q || $stateParams.p ||  $stateParams.type || $stateParams.caseId){
        		   $scope.isVlaLoad = false;
        	   }else{
        		   $scope.isVlaLoad = true;
        	   }
        	   /*Initializing scope variables*/ 
        		$scope.model= {
        				showBasicInfo:true,
        				showOnNode:true,
        				showOnEdge:true,
        				showTagCloud:true,
        				showlocation:true,
        				showAdditionalDetails:true,
        				readMoreDetails:{
        					modalBody: ''
        				},
        				onBasicInfo:function(){
        					this.showBasicInfo = !this.showBasicInfo;
        				},
        				onNode:function(){
        					this.showOnNode = !this.showOnNode;
        				},
        				onedge:function(){
        					this.showOnEdge = !this.showOnEdge;
        				},
        				onadditionalDetails:function(){
        					this.showAdditionalDetails = !this.showAdditionalDetails;
        				},
        				onTagCloud:function(){
        					this.showTagCloud = !this.showTagCloud;
        				},
        				onlocation:function(){
        					this.showlocation = !this.showlocation;
        				}
        		};
        	         
        		/*Initializing global variables*/ 
        		window.global = {
            			dimensions:{},
            			graphURL:null,
            			openArticleModal:function(entityID){
            				var text = entityID.data();
            				var openRiskOverviewModal = $uibModal.open({
            		            templateUrl: 'scripts/act/modal/views/riskoverview.modal.html',
            		            resolve: {
            		            	text: function () {
            		                  return text;
            		                }
            		              },
            		            controller: 'RiskOverViewController',
            		            size: 'lg',
            		            backdrop: 'static',
            		            windowClass: 'marketModal-pulse-modal-wrapper',
            		        });

            			 openRiskOverviewModal.result.then(function () {
            		 			
            		        }, function () {
            		        });
            			}
            		 };
        		if(!window.current_data)
        			{window.current_data = null;}
        		/*
    		     * @purpose: Load World Chart Map 
    		     * @created: 09 oct 2017
    		     * @params: none
    		     * @return: none
    		     * @author: Ankit
    		    */ 
        		var bombMap = new Datamap({
    			    element: document.getElementById('worldmap_div'),
    			    scope: 'world',
    			    fills: {
    			        'red': '#884BA2',
    			        'green': '#54A063',
    			        'blue': '#2CBCFA',
    			        'seablue': '#37FEFF',
    			        defaultFill: '#334851'
    			    },
    			    geographyConfig: {
    			        highlightFillColor: '#1b2735',
    			        popupOnHover: false,
    			        highlightBorderOpacity: 0
    			    },
    			    bubblesConfig: {
    			        borderWidth: 0,
    			        borderOpacity: 0,
    			        borderColor: 'transparent',

    			    },
    			    done: function (datamap) {
    			        datamap.svg.call(d3_v3.behavior.zoom().on("zoom", redraw));
    			        function redraw() {
    			            datamap.svg.selectAll("g").attr("transform", "translate(" + d3_v3.event.translate + ")scale(" + d3_v3.event.scale + ")");
    			        }
    			    }

    			});
        		d3.select("#worldmap_div").selectAll(".datamaps-bubble").remove();
        		window.plotCountriesInlinkAnalysis =function(data){      
        			 if (data && data.vertices && data.vertices.length > 0) {
        				 var locations=[];
        				 var mapData =[];
     			        var nested_data = d3.nest().key(function (d) {     			           
     			                return d.labelV;
     			        }).entries(data.vertices);			       
     			        $timeout(function(){
	     			        angular.forEach(nested_data, function (d) {
	     			            if (d.key.toLowerCase() == "location") {     			            	
	     			            	locations = d.values;
	     			            }
	     			        });
	     			        if(locations.length>0){
		     			      var locationsMap = {};
		     			      window.locationsArr=[];
		     			      for (var i = 0, j = locations.length; i < j; i++) {
		     			    	  if(!locationsMap[locations[i].name]){
		     			    		 window.locationsArr.push(locations[i]);
		     			    	  }
		     			    	 locationsMap[locations[i].name] = (locationsMap[locations[i].name] || 0) + 1;
		     			      }
		     			     var geocoder = new google.maps.Geocoder();
		     			        angular.forEach(window.locationsArr, function (d) { 
		     			            if (d.name) {
		     				               var  latitude, longitude;/*jshint ignore : line */
		     			            		geocoder.geocode( { 'address':d.name}, function(results, status) {
		     			            			if (status == google.maps.GeocoderStatus.OK) {
		     			            				 mapData.push({
		     	     				                    "latitude": results[0].geometry.location.lat(),
		     	     				                    "longitude": results[0].geometry.location.lng(),
		     	     				                    "location": d.name,
		     	     				                    "count": locationsMap[d.name],
		     	     				                });
		     	     				              plotCirclesOnMap(bombMap, "worldmap_div", mapData);
		     			            		}
		     								});   			            		
		     			            		
		     			            }
		     			        });
	     			        }
     			       },0);
     			    }
        		};
       
      $('body').append('<div class="Bubble_Chart_tooltip" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
        		
         		/**
    			 * Funtoin to plot circles on map
    			 */
    			function plotCirclesOnMap(bombMap, id, data) {
    			    // plotting circles on map
    			    d3.select("#" + id).selectAll(".datamaps-bubble").remove();
    			    var currentData = [];
    			    if (data) {
    			        currentData = data;
    			    } else {
    			        return;
    			    }
    			    var bombScale = d3.scaleLinear().domain(d3.extent(currentData, function (d) {
    			        return d.count;
    			    })).range([5, 10]);

    			    if (currentData.length > 0) {
    			        currentData.map(function (d) {
    			            if (d.count < 20) {
    			                d.fillKey = "green";
    			            } else if (d.count < 80) {
    			                d.fillKey = "seablue";
    			            } else if (d.count < 130) {
    			                d.fillKey = "blue";
    			            } else {
    			                d.fillKey = "red";
    			            }    			
    			            d.weight = d.count;
    			            return d.radius = bombScale(d.count);/*jshint ignore :line*/
    			        });
    			        // circles on world map
    			        var bombs = currentData;
    			        // draw bubbles for bombs
    			        bombMap.bubbles(bombs, {
//    			            popupTemplate: function (geo, d) {
//    			                if (d.type) {
//    			                    return ["<div class='tooltip_datamap_all_data text-uppercase'><div style='color:white;margin-bottom:15px'><h5>" + (d.location) + "</h5></div><div style='margin-bottom:15px'>Funding Received <h4 style='color:#3372A8;margin-top:0'>" + d.count + "BN</h4></div><div style='margin-bottom:15px'>Type <h6 style='color:#3372A8;margin-top:0'>" + d.type + "</h6></div></div>"].join('');
//    			                } else {
//    			                    return ['<div class="hoverinfo text-uppercase"><div style="color:white;margin-bottom:15px"><h5>' + (d.location) + '</h5></div>Investments: ' + d.count,
//    			                        '</div>'].join('');
//    			                }
//
//    			            }
    			        });
    			        d3.select("#"+id).selectAll(".datamaps-hoverover").remove();
    			        d3.select("#"+id).selectAll(".datamaps-bubble")
    			        .on("mouseover",function(d){
    			        	 $(this).css("opacity", 1);
    			        	$(".Bubble_Chart_tooltip").html('<div class=" text-uppercase"><div style="color:white;margin-bottom:15px"><h5>' + (d.location) + '</h5></div><div>Count: '+d.count+'</div');
    		                
    		                return $(".Bubble_Chart_tooltip").css("display", "block");
    		           
    			        })
    			        .on("mousemove",function(){
    			        	var p=$("#" + id);
    		            	var position=p.offset();
    		            	var windowWidth=window.innerWidth;/*jshint ignore :line*/
    		            	var tooltipWidth=$(".Bubble_Chart_tooltip").width()+50;
    		            	var cursor=d3.event.x;
    		             	if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)){
    		            		var element = document.getElementsByClassName("Bubble_Chart_tooltip");
    		            		element[0].classList.remove("tooltip-left");
    		        			element[0].classList.add("tooltip-right");
    		               		$(".Bubble_Chart_tooltip").css("left",(d3.event.pageX - 15-$(".Bubble_Chart_tooltip").width()) + "px");
    		             	}else{
    		            		var element =document.getElementsByClassName("Bubble_Chart_tooltip");
    		               		element[0].classList.remove("tooltip-right");
    		           		    element[0].classList.add("tooltip-left");
    		           		    $(".Bubble_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
    		             	}
    		                return $(".Bubble_Chart_tooltip").css("top",d3.event.pageY-20 + "px");
    			
    			        })
    			        .on("mouseout",function(){
    			        	
    			        	 $(this).css("opacity", 1);
    		                    //hide tool-tip
    		                    return $(".Bubble_Chart_tooltip").css("display", "none");
    			        	
    			        });
    			        d3.select("#"+id).selectAll(".datamaps-bubble").attr("r", function (d) {
    			            return d.radius;
    			        }).style("fill-opacity", 0.5);
    			    }
    			}
    			
    			/*
    		     * @purpose: Initialize VLA
    		     * @created: 26 feb 2018
    		     * @params: none
    		     * @return: no
    		     * @author: Ankit
    		    */	
        	window.initializeVLA = function(create_graph){
        		$('#vla').empty();
        		$timeout(function(){
        			$scope.token = $rootScope.ehubObject.token;
        			var queryString = HostPathService.getParameterByName('q');
        		    var pageString = HostPathService.getParameterByName('p');
        		    var type = HostPathService.getParameterByName('type');
        		    var caseId = HostPathService.getParameterByName('caseId');
        		    window.global.queryString = queryString;
	        		global.dimensions.width = $(window).width() - 330;
	        	    global.dimensions.height = document.documentElement.clientHeight - 160;
	        	    global.dimensions.left = "320px";
	        	    global.dimensions.top = "152px";
	        	    var data = {
	        	    		 "fetchers": ["1008", "1006", "1013"],
	        	    		 "keyword": "",
	        	    		 "searchType": "Company", 
	        	    		 "lightWeight": true,  
	        	    		 "limit": 2,
	        	    		"iterations": 1,
	        	    		"alias": "linkAnalysis", 
	        	    		"create_new_graph": create_graph,
	        	    		"requires_expansion": true,
	        	    		"entity_resolution": true,
	        	    		"expansion_fetchers_lightweight": true,	
	    					"expansion_fetchers": ["1001","2001","1005","1002","1003","3001","1007","2003","1009","1010","1011","1012","3002","3003",  '10','11','12','13','14','16','17',"1021"]
	    				
	        	    		};
	        	    //29 is not working
	        	    var resolveData = {
	        	    		 "fetchers": ["1013", "1006", "1008","1021"],
	        	    		 "keyword": "",
	        	    		 "searchType": "Company",
	        	    		 "lightWeight": true, 
	        	    		 "limit": 20
	        	    	};

        	        function init(value) {
	        	    	if(caseId){
	    	        		  $('#displayadditionalDetails').css('display','none');
	    	        		  /*
	    	        		     * @purpose: Case Seed Details
	    	        		     * @created: 26 feb 2018
	    	        		     * @params: none
	    	        		     * @return: no
	    	        		     * @author: Ankit
	    	        		    */
    	        				var params = {
    	        					caseId: caseId,
    	        					token: $rootScope.ehubObject.token
    	        				};
    	        				if(!isResoveCalled){
    	        					isResoveCalled =true;
    	        					var resolveData = {
   	    	   	        	    		 "fetchers": ["1008", "1006", "1013","1021"],   	    	   	        	    		 
   	    	   	        	    		 "searchType": "Company",
   	    	   	        	    		 "lightWeight": true, 
   	    	   	        	    		 "limit": 20
   	    	   	        	    	};
    	        					if(!HostPathService.getParameterByName('entity')){
		    	        				ActApiService.getCaseDetails(params).then(function(response){
		    	        					resolveData.keyword = response.data.name;	    	        				
		    	        						
		    		    	        		   getResolveEntity(resolveData,"chartPlotted");
		    	        					
		    	        				}, function(){
		    	        					$('#basicInformation').css('display','none');
		    	        				});
    	        					}else{
    	        						resolveData.keyword = HostPathService.getParameterByName('entity');
    	        						 getResolveEntity(resolveData,"chartPlotted");
    	        					}
	    	        		  }
    	        				$('#vla').empty();
	  	        	     	    var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": caseId, "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
	                            if (typeof custom_config != 'undefined')
	                                {vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
	                            vla_inst =  PlotVLA(vla_options);
	    	        	  
      		            }else{
	    	        		  if (queryString) {
	      	        	        $('#displayadditionalDetails').css('display','none');
	      	        	    	$('.custom-spinner').hide();
	      	        	    }
	      	        	  
	      	        	    if (!pageString && !queryString) {
	      	        	        /*show error*/
	      	        	    	$("#vlaerror").html("Please provide query string to load data");
	      	        	    	$('.custom-spinner').hide();
	      	        	    	return false;
	      	        	    } else if (queryString && pageString) {
	      	        	    	/*plot for the curent page*/
	      	        	        if (!config_map[pageString]) {
	      	        	        	/*show error*/
	      	        	        	$("#vlaerror").html("Please Provide valid Page");
	      	        	            $('.custom-spinner').hide();
	      	        	            return false;
	      	        	        } else if (pageString == "Adverse News" && !type) {
	      	        	        	/*show error that adverse news needs an type*/
	      	        	        	$("#vlaerror").html("Please Provide valid type for entity");
	      	        	             $('.custom-spinner').hide();
	      	        	             return false;
	      	        	        } else {
	      	        	        	if(pageString == "doc"){	
	      	        	        		loadDocumentData(HostPathService.getParameterByName('identity'));

	      	        	        	}else
	      	        	            if (pageString == "Adverse News") {
	      	        	            	$('#displayadditionalDetails').css('display','block'); 
	  	    	        	                window.global.graphURL = config_map[pageString].graph + type + "/" + decodeURIComponent(queryString).trim()+"?token=" + $rootScope.ehubObject.token;
	  	    	        	                summaryURL = config_map[pageString].summary + type + "/" + decodeURIComponent(queryString).trim()+"?token=" + $rootScope.ehubObject.token;
	      	        	            	mainEntityDetails(summaryURL);
	      	        	                var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
	      	        	                if (typeof custom_config != 'undefined')
	      	        	                    {vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
	      	        	                if(window.current_data)
	      	        	                	{vla_options.data = window.current_data;}
	      	        	              $('#vla').empty();
	      	        	              vla_options.adverseNews = "adversenews";
	      	        	              vla_inst =  PlotVLA(vla_options);
	      	        	            }else   if (pageString == "mip") {        	            	
	      	        	                window.global.graphURL = config_map[pageString].graph  + queryString;
	      	        	               
	      	        	                var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
	      	        	                if (typeof custom_config != 'undefined')
	      	        	                   { vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
//	      	        	                vla_inst = new VLA.Main(vla_options);
	      	        	              $('#vla').empty();
	      	        	              vla_inst =  PlotVLA(vla_options);
	      	        	            }
	      	        	        }
	      	        	    }
	          	            if (value != void 0)
	          	               { data = value;}
	      		        	$('.custom-spinner').css('display', 'block');
	      		            $('.mainsidewrapper').addClass('sidebar-wrapper-main');
	      		           if(window.resolvedResponse && window.current_data){
	      		        	 $('#vla').empty();
	      		        	 var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
   	                            if (typeof custom_config != 'undefined')
   	                                {vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
   	                            	vla_options.data = window.current_data;
   	                            vla_inst =  PlotVLA(vla_options);
	      		        	 integrateResolved(window.resolvedResponse);
	      		           }else{
	      		        	 var resolveData = {
	    	   	        	    		 "fetchers": ["1008", "1006", "1013","1021"],   	    	   	        	    		 
	    	   	        	    		 "searchType": "Company",
	    	   	        	    		 "lightWeight": true, 
	    	   	        	    		 "limit": 20
	    	   	        	    	};
	      		        	resolveData.keyword = HostPathService.getParameterByName('q');
	      		        	   getResolveEntity(resolveData);
	      		           }
	      		            
	    	        	  }
        	        }
	        	    
	        	    /*
	        	     * @purpose: Resolve API
	        	     * @created: 26 feb 2018
	        	     * @params: resolveData(object)
	        	     * @return: none
	        	     * @author: Ankit
	        	    */  
	        	    function getResolveEntity(resolveData,isChartPLotted){
	        	    	 EntityApiService.getResolvedEntityDataOld(resolveData).then(function(response){
	      		            	var resolvedResponse = [], resolvedName = '', parentIndex = -1, childIndex = -1;
	      		            if(response.data.resolved.length > 0  && response.data.resolved[0].entities[0].name)  
	      		       			 {resolvedResponse = response.data.resolved;}
	      		       		 else if(response.data.sortedResults.length > 0){
									  resolvedName = response.data.sortedResults[0].name;
									  if(response.data.sortedResults[0].fetcher === 'wikipedia.com' ){
										parentIndex = 0;
									  } else if(response.data.sortedResults[0].fetcher === 'bloomberg.com'){
										parentIndex = 1;
									  } else if(response.data.sortedResults[0].fetcher === 'company.linkedin.com'){
										parentIndex = 2;
									  } else {
										parentIndex = 3;
									  }
	      		            		// response.data.sortedResults[0].fetcher === 'wikipedia.com' ? parentIndex = 0 : 
	      		            		// 	response.data.sortedResults[0].fetcher === 'bloomberg.com' ? parentIndex = 1 : 
	      		            		// 	response.data.sortedResults[0].fetcher === 'company.linkedin.com' ? parentIndex = 2 : parentIndex = 3;
	      		            		childIndex = response.data.sortedResults[0].index;
	      		            		resolvedResponse.push(response.data.results[parentIndex]);
	      		            	} else 
	      		       			 {resolvedResponse = response.data.results;}
	      		       		 	window.resolvedResponse = resolvedResponse;
	      		       		 	
	      		            if(pageString != "Adverse News"){
		      		            //call graph API from here
			      		       		 if(resolvedResponse &&  resolvedResponse.length>0 && resolvedResponse[0].entities && resolvedResponse[0].entities.length > 0 ){
			      		       			 data.keyword = resolvedResponse[0].entities[0].name;
			      		       		 }
			      		       	if(window.current_data && !isChartPLotted && pageString != "doc"){
			      		       		$('#vla').empty();
					      		       	var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": response.data.caseId, "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
		   	                            if (typeof custom_config != 'undefined')
		   	                                {vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
		   	                            	vla_options.data = window.current_data;
		   	                            vla_inst =  PlotVLA(vla_options);
			      		       	}else if(!isChartPLotted && pageString != "doc"){
			      		       		EntityApiService.getGraphData(data).then(function(response){
			           		    	   var foundData = false;/*jshint ignore :line*/
			   	   	                    if (response !== void 0) {
			   	   	                    	if (!pageString && queryString) {
			   	   	                    	$('#vla').empty();
			   	   	                            var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": response.data.caseId, "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
			   	   	                            if (typeof custom_config != 'undefined')
			   	   	                               { vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
			   	   	                            vla_inst =  PlotVLA(vla_options);
			   	   	                        }
			   	   	                    }
			           		       },function(error){
			           		    	   $('#vla').html(error.responseMessage);
			           		       });
			      		       		//end of graph API
			      		       	   }
			      		       		
	      		            	}
	      		       		    integrateResolved(resolvedResponse);
	     	                        
	      		            },function(){
	      		            	$('#vla').html("<p class='server-vla-error'>The server is busy, please try after sometime.</p>");
	      	        	    	$('.custom-spinner').hide();
	      		            });
	        	    } 
	        	    
	        	    /*
	        	     * @purpose: Basic Info
	        	     * @created: 26 feb 2018
	        	     * @params: resolvedResponse(object)
	        	     * @return: none
	        	     * @author: Ankit
	        	    */ 
	        	    function integrateResolved(resolvedResponse){
  		            	var foundData = false;
	        	    	resolvedResponse.forEach(function (entity) {
	                            if (entity.status) {
	                                entity.entities.forEach(function (entityID) {
	                                    if (entityID.name !== '' && entityID.name !== void 0 && !foundData) {
	                                        foundData = true;
	                                        window.entityName = entityID.name;
	                                        $('.basic-header span').html(entityID.name);
	                                        if (entityID.properties.details != void 0) {
	                                            window.count = entityID.properties.details;
	                                            var count1 = count ? count.substring(0, 300) :'';/*jshint ignore :line*/
	                                            $('.basic-content p').html(count1 + '<a onClick="openModal();" style="font-size:10px;" title="Read More"> READ MORE</a>');

	                                        } else {
	                                            count = entityID.properties.summary;/*jshint ignore :line*/
	                                            var count1 = count ? count.substring(0, 300) :'';/*jshint ignore :line*/
	                                            $('.basic-content p').html(count1 + '<a onClick="openModal();" href="javascript:void(0);" title="Read More"> READ MORE</a>');
	                                        }
	                                        var category = entityID.properties.business_type === undefined ? entityID.properties.organization_name : entityID.properties.business_type;
	                                        $('ul.basicInfo').html('<li>ID: ' + entityID.entityID + '</li><li>TYPE: ' + entityID.entityType + '</li><li>CATEGORY: ' + category + '</li><li style="color:#E32646;">RISK: ' + entityID.riskScore + '</li>');
	                                    }
	                                });
	                            }
	                        });
	        	    }
	        	    
        	        function getdimesionAndReplot() {
        	            var bodycls = $(".custom-vla").attr("class");
        	            var bodyclass = $("body").attr("class");
        	            if (bodycls.indexOf("sidebar-open") >= 0) {
        	            	global.dimensions.width = $(window).width() - 330;
        	            	global.dimensions.left = "320px";
        	            } else {
        	            	global.dimensions.width = $(window).width() - 20;
        	            	global.dimensions.left = "10px";
        	            }
        	            if (bodyclass && bodyclass.indexOf("header-close") >= 0) {
        	            	global.dimensions.height = document.documentElement.clientHeight - 20;
        	            	global.dimensions.top = "28px";
        	            } else {
        	            	global.dimensions.height = document.documentElement.clientHeight - 160;
        	            	global.dimensions.top = "152px";
        	            }
        	            $("#vla").empty();
        	            var data = {
        	            	 "fetchers": ["1008", "1006", "1013","1021"],
        	            	 "keyword": queryString,
        	            	 "searchType": "Company", 
        	            	 "lightWeight": true,  
        	            	 "limit": 5,
        	            	"iterations": 1,
        	            	"alias": "linkAnalysis", 
        	            	"create_new_graph": false,
        	            	"requires_expansion": true,
        	            	"entity_resolution": true,
        	            	"expansion_fetchers_lightweight": true,	
        					"expansion_fetchers": ["1001","2001","1005","1002","1003","3001","1007","2003","1009","1010","1011","1012","3002","3003",  '10','11','12','13','14','16','17',"1021"]	        
        				
        	            	}; //29 is not working 4-jan -18
        	          
        	            init(data);
        	        }

        	        if (queryString == '')
        	           { return;}
        	       
        	        data.keyword = queryString;
        	        resolveData.keyword = queryString;
        	        init();
        	        /*--  Toggle Header and Sidebar  --*/
        	        $(".toggle-sidebar").on("click", function () {
        	            $(".custom-vla").toggleClass("sidebar-open");
        	            $(".toggle-sidebar").toggleClass("open");
        	            $("#vla").toggleClass("filterNav");
        	            $(".toggle-header").toggleClass("filterNav");
        	            $('.custom-spinner').css('display', 'block');
        	            $('.mainsidewrapper').addClass('sidebar-wrapper-main');
        	            getdimesionAndReplot();
        	        });
        	        $(".toggle-header").on("click", function () {
        	            $("body").toggleClass("header-close");
        	            $(".navbar-static-top").toggleClass('navHeader');
        	            $(".middle-header").toggleClass('navHeader');
        	            $('.custom-spinner').css('display', 'block');
        	            $('.mainsidewrapper').addClass('sidebar-wrapper-main');
        	            getdimesionAndReplot();
        	        });

        	        /*-- Side bar height ----*/
    //    	        $(document).ready(function () {
        	            $(function(){
        	        $('.mainsidewrapper').css({'max-height': (($(window).height()) - 160) + 'px'});
        	////
        	        $(window).resize(function () {
        	            $('.mainsidewrapper').css({'max-height': (($(window).height()) - 160) + 'px'});
        	        });
        	});

        	        /*--  Custom ScrollBar for Sidebar Elements  --*/
        	        
        	         $(".selectric").on("click", function () {
        	             $(this).parents(".selectric-wrapper").find(".selectric-scroll").mCustomScrollbar();
        	         });
        	      // entityDetails
        	         function mainEntityDetails(summaryURL) {
        	             if (summaryURL) {
        	            	 ActApiService.loadMarketIntelligence(summaryURL).then(function(response){
        	            		 if(response){
        	            			 var r = response.data;
        	                         var netsted_risks = d3.nest()
        	                                 .key(function (d) {
        	                                     return d.risk;
        	                                 })
        	                                 .entries(r.connections);
        	                         var risks = [];
        	                         $.each(netsted_risks, function (i, val) {
        	                             risks[val.key] = val.values.length;
        	                         });
        	                         //handle sting for high risk articles and create array with 
        	                         var highriskarticles = [];
        	                         $.each(r['connected-high-risk-articles'], function (i, val) {
        	                             highriskarticles.push({
        	                                 val: (val.headline.main),
        	                                 url: val.web_url,

        	                             });
        	                         });
        	                         var indirect = [];
        	                         $.each(r.connections, function (i, val) {
        	                             var cls;
        	                             if (val.risk === "high") {
        	                                 cls = "table-high";
        	                             } else if (val.risk === "medium") {
        	                                 cls = "table-medium";
        	                             } else {
        	                                 cls = "table-low";
        	                             }
        	                             indirect.push({
        	                                 "name": val.name,
        	                                 "relation": val.relation,
        	                                 "cls": cls,
        	                                 "imgUrl": "static/VLA/KeylineICONS/" + val.type + ".svg"
        	                             });
        	                         });
        	                         highriskarticles = highriskarticles;
        	                         var summary = {
        	                             "article-references": (r['connected-high-risk-articles']).length,
        	                             "connected-entities": (r.connections.length),
        	                             "connected-high-risk-entities": risks.high ? risks.high : 0,
        	                             "connected-medium-risk-entities": risks.medium ? risks.medium : 0,
        	                             "connected-low-risk-entities": risks.low ? risks.low : 0,
        	                             "highriskarticles": highriskarticles,
        	                             "indirectrelations": indirect
        	                         };
        	                         var html = '';
        	                         html = html + '<div class="pulse-entities text-uppercase"><ul class="list-inline text-center"><li><h6 class="key">ArticleReferences</h6><span >' + summary['article-references'] + '</span></li><li><h6 class="key">Entity Connection</h6><span >' + summary['connected-entities'] + '</span></li></ul></div>';
        	                         html = html + '<div class="pulse-risk-counts text-uppercase"><ul class="list-inline text-center"><li class="table-high"><h6 class="key">High Risk</h6><span >' + summary['connected-high-risk-entities'] + '</span></li><li class="table-medium"><h6 class="key">Medium Risk</h6><span >' + summary['connected-medium-risk-entities'] + '</span></li><li class="table-low"><h6 class="key">Low Risk</h6><span >' + summary['connected-low-risk-entities'] + '</span></li></ul></div>';
        	                         //add direct article connections                  
        	                         html = html + '<div class="pulse-relations directRelationEntities"><h5>Direct Relation with the following high risk Articles</h5><ul class="list-unstyled">';
        	                         $.each(summary.highriskarticles, function (i, val) {
        	                             html = html + '<li><a target="_blank" title="' + val.val + '" href="' + val.url + '"><img src="scripts/VLA/KeylineICONS/Article.svg" width="15" height="15">' + val.val + '</a></li>';
        	                         });

        	                         html = html + '</ul></div>';

        	                         //add indirect connection
        	                         html = html + '<div class="pulse-relations indirectRelationEntities"><h5>Indirect Relation with the following Entities</h5><ul class="list-unstyled text-uppercase">';
        	                         $.each(summary.indirectrelations, function (i, val) {
        	                             html = html + '<li><span class="' + val.cls + '" title"' + val.name + '"><img src="' + val.imgUrl + '" width="15" height="15"></img>' + val.name + ' ' + val.relation + '</span></li>';
        	                         });
        	                         html = html + '</ul></div>';
        	                         $('#entityDetails').html(html);
        	            		 }
        	            	 },function(){
        	            		 $('#entityDetails').html("No Information available for the entity");
        	            	 });
        	             }
        	         }
        		},0);
        	};
		function loadDocumentData(identifier){
			window.global.page = "doc";
			$('.custom-spinner').css('display', 'block');
			ActApiService.getNews(identifier).then(function (response) {				
				var data = {
						"basic":response.data.basic,
						"technology": "internet-of-things"
				};
				ActApiService.getGraphData(data).then(function (response) {
					var currentCompany = data.basic['vcard:organization-name'];
					d3.extent(response.data.vertices,function(d){
						if(d.properties.event_count && d.labelV !='Event'){
							return parseFloat(d.properties.event_count);
						}
						if(d.labelV =='Company' && d.name.toLowerCase() == currentCompany.toLowerCase()){
							d.evt_color  ="#53e06a";
						}
					});
					 var colorScale = d3.scaleLinear().domain([1,10]).range(["#ed8080","#e80202"]);
					
					response.data.vertices.map(function(d){
						if(d.properties.event_count  && d.labelV !='Event'){
							d.evt_color  =  parseFloat(d.properties.event_count)<10?colorScale(parseFloat(d.properties.event_count)):"#e80202";
						}
						
					});
					window.current_data = response.data;
					$('.custom-spinner').css('display', 'none');
					  window.global.graphURL = "";
		        		   var vla_options = {"target_html_element": "vla", "userId": 21220, "layout": "cola", "tip_event": "hover", "autoload": {"type": "json", "caseId": "", "limit": 100, "queryParams": true, "queryPath": EHUB_API, "token": $rootScope.ehubObject.token}};
			                if (typeof custom_config != 'undefined')
			                    {vla_options = $.extend({}, custom_config, vla_options);}/*jshint ignore :line*/
			                if(window.current_data)
			                	{vla_options.data = window.current_data;}
			              $('#vla').empty();
			              vla_inst =  PlotVLA(vla_options);
				}, function () {
					HostPathService.FlashErrorMessage('No Data Found', '');
					$scope.docDetailsPreloader = false;
				});
			}, function () {
				$('.custom-spinner').css('display', 'none');
		
			});
		}
    		initializeVLA(false);
           }