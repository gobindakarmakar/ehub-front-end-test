'use strict';
angular.module('ehubApp')
	   .controller('ActEntitiesInFocusController', actEntitiesInFocusController);

		actEntitiesInFocusController.$inject = [
			'$scope', 
			'$rootScope',
			'$stateParams',
			'ActApiService',
			'DiscoverService',
			'ActEntitiesInFocusConst',
			'$uibModal'
		];
		
		function actEntitiesInFocusController(
				$scope, 
				$rootScope,
				$stateParams,
				ActApiService,
				DiscoverService,
				ActEntitiesInFocusConst,
				$uibModal) {
    
		$scope.token = $rootScope.ehubObject.token;
	    $scope.case_id = $stateParams.caseId;
	    $scope.actEntitiesInFocusDatas = [];
	    $scope.EntitiesData = [];
	    var params = {
			token: $scope.token,
			caseId:  $scope.case_id
	    };
	    
	    /*
		 * @purpose: open filter modal
		 * @created: 15 Dec 2017		
		 * @author: prasanthi
		 */
		var openFilterModalForEntitiesInFocusChart;
		var isApiLoaded = false;
		$scope.openFilterModalForEntitiesInFocusChart =function(){
			openFilterModalForEntitiesInFocusChart = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             //backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return $scope.keysTofilter;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword ? $scope.FilteredKeyword : "" ;
		                },
		                FilteredKey : function(){
		                	return $scope.FilteredKey;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return false;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
		                	return $scope.AllNames;
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		              }
	         });

			openFilterModalForEntitiesInFocusChart.result.then(function (response) {
				if(response)
					{handleFiltersApplied(response);}
	         }, function () {
	         });
	         $scope.modalInstance = openFilterModalForEntitiesInFocusChart;
	         return openFilterModalForEntitiesInFocusChart.result;
		};
		var actualinFocusData;
		$scope.AllNames = [];
		var resultedArray = [];
	    if($scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
	    	$scope.actEntitiesInFocusPreloader = true;
	    	var params = {
				token: $rootScope.ehubObject.token
			};
			ActApiService.getCaseSummaryById(params, $scope.case_id).then(function(response){
				$scope.actEntitiesInFocusPreloader = false;
				resultedArray = response.data;
				delete resultedArray.caseId;
	    		if(response.data && response.data.exceptionMessage == undefined){
	    			 isApiLoaded = true;
		    		var actData = handleBubbleData(resultedArray);
		    		if(actData.children.length > 0){
			    		angular.forEach(actData.children,function(d){
			    			if(d.children.length > 0){
			    				angular.forEach(d.children,function(key){
			    					$scope.AllNames.push(key.name);
			    				});
			    			}
			    		});
		    		}
		    		$scope.AllNames = _.uniq($scope.AllNames);
		    		actualinFocusData = jQuery.extend(true,{}, resultedArray);
					$scope.keysTofilter = d3.keys(resultedArray);
					$scope.EntitiesData = resultedArray;
					getRiskDataByCaseId(resultedArray);
	    		}else{
					angular.element('#actEntitiesInFocusBubbleChart').html('<p class="no-data">No Data Found</p>');
				}
			}, function(error){
				$scope.actEntitiesInFocusPreloader = false;
		    	if(error.responseMessage){
		    		angular.element('#actEntitiesInFocusBubbleChart').html('<p class="no-data">'+ error.responseMessage +'</p>');
		    	}
		    });
		}
	    /*
	     * @purpose: get risk data by case
	     * @created: 05 oct 2017
	     * @params: null
	     * @return: no
	     * @author: swathi
	    */
	    function getRiskDataByCaseId(response){
	    	var heightResp = document.getElementById('actentitiesInFocusdiv').clientHeight ? document.getElementById('actentitiesInFocusdiv').clientHeight : 300;
	    	if(heightResp > 310){
	    		heightResp = 580;
	    	} else{
	    		heightResp = 280;
	    	}
	    	$scope.actEntitiesInFocusDatas = [];
	    	var keys = Object.keys(response);
	    	var len = keys.length;
	    	if(len > 0){
		        var options = {
		            container: '#actEntitiesInFocusBubbleChart',
		            header: "ENTITY IN FOCUS",
		            uri: response,
		            height: heightResp,
		            isheader: false
		        };
		        var bubbleData = handleBubbleData(response);
		        options.data = bubbleData;
		       
		        angular.forEach(bubbleData.children,function(groupData){
		        	angular.forEach(groupData.children,function(obj){
		        		$scope.actEntitiesInFocusDatas.push({
		        			name : obj.name,
		        			size : obj.size,
		        			group : groupData.name
		        		});	
		        	});
		        });
		        var actualOptions = jQuery.extend(true, {}, options);
	            $scope.maxoptions=actualOptions;
		        DiscoverService.setEntitiesInFocusBubblesChart(actualOptions);
		    	
		    	
		        $('#entitiesInFocusWidget').mCustomScrollbar({
					axis : "y",
					theme : "minimal"
				});
	    }else{
	    	$scope.maxoptions = {};
    		$scope.acthottopicsDatas = [];
	    	angular.element('#actEntitiesInFocusBubbleChart').html('<p class="no-data">No Data Found</p>');
	    }
    }
	    
	    
	    /**
		 * Function to handle data according to format of Bubble Chart
		 * library
		 */
        function handleBubbleData(data) {
            var childrenData = [];
            angular.forEach(data, function(value, key){
            	var groupData = {
            		name: key,
            		children: []
            	};
            	angular.forEach(value, function(objSize, objName){
            		groupData.children.push({
            			name: objName,
            			size: objSize
            		});
            	});
            	childrenData.push(groupData);
            });
            var finalData = {
                "children": childrenData
            };
            return finalData;
        }
        
        
	    
	    
	    /*
		 * @purpose: handle/filter data
		 * @created: 15 Dec 2017
		 * @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			var filteredData={};
			$scope.FilteredKeyword = "";
			var actualData = jQuery.extend(true,{}, actualinFocusData);
			if(resp.filterApplied.entity){
				var keys =[resp.filterApplied.entity];
				$scope.FilteredKey = resp.filterApplied.entity;
			}else{
				var keys = d3.keys(actualData);
				$scope.FilteredKey = "ALL";
			}
			angular.forEach(keys,function(d){				
				var innerKeys =d3.keys(actualData[d]); 							
				var filtered={};
				if(!resp.caseSearchKeyword){
					filtered = actualData[d];
				}else{
					$.grep(innerKeys, function(v,i) {
						
						if(v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1){
							filtered[v]=i;
						}
					});
					$scope.FilteredKeyword = resp.caseSearchKeyword;
				}
				if(d3.keys(filtered).length>0)
					{filteredData[d]=filtered;}
			});
			$scope.EntitiesData = filteredData;
			getRiskDataByCaseId(filteredData);
		}

		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function() {
			setGraphs();
		});
		
		
		$scope.ResizeToggleGraph = function(){
			$scope.toggleCase = false;
			setGraphs();
		};
		
		
		function setGraphs(){
			var heightResp = document.getElementById('actentitiesInFocusdiv').clientHeight ? document.getElementById('actentitiesInFocusdiv').clientHeight : 300;
			$("#actentitiesInFocusdiv").find("tbody").css("height",heightResp);
			if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
				if($scope.EntitiesData.length != 0 && isApiLoaded == true ){
					getRiskDataByCaseId($scope.EntitiesData);
				}
			}
			else{
				angular.element('#actEntitiesInFocusBubbleChart').html('<p class="no-data">No Data Found</p>');
			}
		}
		/*
		 * @purpose: open fullscreen modal
		 * @created: 3 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
				
			}, function(){
			});
			
		};
}