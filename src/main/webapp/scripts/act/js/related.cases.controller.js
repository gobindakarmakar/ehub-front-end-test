'use strict';
angular.module('ehubApp')
	   .controller('RelatedCasesController', relatedCasesController);

		relatedCasesController.$inject = [
			'$scope', 
			'$rootScope',
			'ActApiService',
			'$stateParams',
			'$uibModal'
		];
		
		function relatedCasesController(
				$scope, 
				$rootScope,
				ActApiService,
				$stateParams,
				$uibModal) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.relatedCasesPreloader = false;
		var actualRelatedData;
		var params = {
			token: $scope.token
		};
		var count = 1000;
		if($scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			relatedCases();
		}
		var companyImage = 'assets/images/menu_icons/co.png';
		var personImage = 'assets/images/menu_icons/user.png';
		/*
	     * @purpose: get related case details
	     * @created: 04 oct 2017
	     * @params: null
	     * @return: null
	     * @author: swathi
	    */
		$scope.relatedCasesData = [];
		$rootScope.relatedCasesDataCopy = [];
		function relatedCases(){
			$scope.relatedCasesPreloader = true;
			ActApiService.getRelatedCases(params, $scope.case_id, count).then(function successCallback(response) {
				var list = response.data;
				if(response.data.length != 0 && response.data.exceptionMessage == undefined){
					angular.forEach(list, function(itemData, key) {
						params = {
							token: $scope.token,
							caseId: itemData.id
						};
						$scope.AllNames= [];
						ActApiService.getCaseDetails(params).then(function(result){
							$scope.AllNames.push(result.data.name);
							if(result.data.type == 'Corporate-KYC'){
								$scope.relatedCasesData.push({
									caseName: result.data.name,
									proximity: itemData.proximity,
									directRisk: result.data.directRisk,
									indirectRisk: result.data.indirectRisk,
									transactionalRisk: result.data.transactionalRisk,
									imageBank: companyImage,
									type: result.data.type
								});
							}
							if(result.data.type == 'Individual-KYC'){
								$scope.relatedCasesData.push({
									caseName: result.data.name,
									proximity: itemData.proximity,
									directRisk: result.data.directRisk,
									indirectRisk: result.data.indirectRisk,
									transactionalRisk: result.data.transactionalRisk,
									imagePerson: personImage,
									type: result.data.type
								});
							}
							if(list.length - 1 == key){
								$scope.relatedCasesPreloader = false;
								$('#relatedCasesWidget').mCustomScrollbar({
									axis : "y",
									theme : "minimal"
								});
							}
							actualRelatedData = $scope.relatedCasesData;
							$rootScope.relatedCasesDataCopy = $scope.relatedCasesData;
						}, function(){
							$scope.relatedCasesPreloader = false;
						});
					});
				}else{
					$scope.relatedCasesPreloader = false;
				}

			}, function() {
				$scope.relatedCasesPreloader = false;
			});
		}
		
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('actRelatedCasesDiv')){
				var heightResp = document.getElementById('actRelatedCasesDiv').clientHeight ? document.getElementById('actRelatedCasesDiv').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#actRelatedCasesDiv").find("tbody").css("height",heightResp);
			}
		});

		
		 /*
		 * @purpose: open filter modal
		 * @created: 26 Dec 2017		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.openFilterModalForChart =function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             //backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return false;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return false;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return true;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return false;
		                },
		                AllNames:function(){
		                	if($scope.AllNames && $scope.AllNames.length >0){
			                	$scope.AllNames = _.uniq($scope.AllNames);
			                	return $scope.AllNames;
		                	}else{
		                		return [];
		                	}

		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		            }
	         });

	  		openCasefiltersModal.result.then(function (response) {	
	  			if(response)
	  				{handleFiltersApplied(response);}
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 26 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			var filteredData;
			$scope.FilteredKeyword = "";
			var actualData = jQuery.extend(true,[], actualRelatedData);
			filteredData=actualData;
			if(resp.caseSearchKeyword){
				filteredData=[];
				$scope.FilteredKeyword = resp.caseSearchKeyword;
				angular.forEach(actualData,function(d){
					if(d.caseName.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1){
						filteredData.push(d);
					}
				});
			}
			
			if(filteredData.length > 0){
				$('#relatedCasesWidget').mCustomScrollbar({
					axis : "y",
					theme : "minimal"
				});
				$scope.relatedCasesData = filteredData;
				$rootScope.relatedCasesDataCopy = $scope.relatedCasesData;
				$('#relatedCasesWidget').mCustomScrollbar('update');
			}else{
				$scope.relatedCasesData = [];
				$rootScope.relatedCasesDataCopy = [];
				$('#relatedCasesWidget').mCustomScrollbar('destroy');
			}
		}		
}