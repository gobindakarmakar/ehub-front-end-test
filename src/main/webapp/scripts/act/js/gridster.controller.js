'use strict';
angular.module('ehubApp')
	   .controller('ActGridsterController', actGridsterController);

		actGridsterController.$inject = [
			'$scope', 
			'$rootScope',
			'ActGridsterConst'
		];
		
		function actGridsterController(
				$scope, 
				$rootScope,
				ActGridsterConst) {
    
		$scope.gridsterConfiguration = ActGridsterConst;
		$scope.toggleGraph = false;
}