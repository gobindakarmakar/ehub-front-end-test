'use strict';
angular.module('ehubApp')
	   .controller('RelatedCompanyController', relatedCompanyController);

	   relatedCompanyController.$inject = [
			'$scope', 
			'$rootScope',
			'ActApiService',
			'$stateParams'
		];
		
		function relatedCompanyController(
				$scope, 
				$rootScope,
				ActApiService,
				$stateParams) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.relatedCompaniesPreloader = false;
		$scope.comapniesData=[];
	
		if($scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){

		}
		
		
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('relatedCompaniesDiv')){
				var heightResp = document.getElementById('relatedCompaniesDiv').clientHeight ? document.getElementById('relatedCompaniesDiv').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#relatedCompaniesDiv").find("tbody").css("height",heightResp);
			}
		});		
}