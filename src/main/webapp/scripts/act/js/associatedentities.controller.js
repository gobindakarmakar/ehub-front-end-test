'use strict';
angular.module('ehubApp')
	   .controller('AssociatedEntitiesController', associatedEntitiesController);

		associatedEntitiesController.$inject = [
			'$scope', 
			'$rootScope',
			'$stateParams',
			'AssociatedEntitiesConst',
			'$uibModal',
			'ActApiService'
		];
		
		function associatedEntitiesController(
				$scope, 
				$rootScope,
				$stateParams,
				AssociatedEntitiesConst,
				$uibModal,
				ActApiService) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.associatedEntities = [];
		$scope.associatedEntitiesDatas = [];
		$scope.maxoptions = [];
		$scope.actAssociatedEntitiestoggleGraph = true;
		var timescalebubbleData = [];
		var isAPiLoaded = false;
		$scope.associatedEntitiesPreloader = false;
		if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			loadAssociatedEntities();
		}else{
			angular.element("#associatedEntitiesChart").html('<p class="no-data">No Data Found</p>');
		}
		/*
		 * @purpose: load associated entities
		 * @created: 06 oct 2017
		 * @params: null
		 * @returns: no 
		 * @author: swathi
		 */
		function loadAssociatedEntities(){
			$scope.associatedEntitiesPreloader = true;
			$scope.AllNames = [];
			var params = {
				token: $scope.token
			};
			var data = [];
			
			ActApiService.getAssociatedEntities(params, $scope.case_id).then(function(response){
				$scope.associatedEntitiesPreloader = false;
				data = response.data;
				if(data.length != 0){
					 isAPiLoaded = true;
			    	angular.forEach(data,function(d){
			    		var date = new Date(d.date).getTime();
		    			d.date = date;
		    			d.date =  new Date(d.date).toLocaleDateString();
		    			$scope.AllNames.push(d.entityName);
			    	});
			    	$scope.AllNames = _.uniq($scope.AllNames);
			         timescalebubbleData = data;
			         $scope.associatedEntitiesDatas = timescalebubbleData;
			         $scope.AED = timescalebubbleData;
			         $scope.maxoptions = timescalebubbleData;
			         if($scope.associatedEntitiesDatas.length != 0){
			        	 $('#associatedEntitiesWidget').mCustomScrollbar({
			 				axis : "y",
			 				theme : "minimal"
			 			});
			         }
			         appendGraph();
			         /*responsiveness*/
				        $(window).on("resize",  function () {
				        	$('#associatedEntitiesChart').empty();
				        	new timescalebubbleChart({
					        	 container: '#associatedEntitiesChart',
					        	 data: timescalebubbleData,
					        	 height: AssociatedEntitiesConst.height,
					        	 isheader: AssociatedEntitiesConst.isheader,
					        	 ytext: AssociatedEntitiesConst.ytext,
					        	 xticks: AssociatedEntitiesConst.xticks,
					        	 margin: AssociatedEntitiesConst.margin
					         });
				        });
				}else{
					angular.element('#associatedEntitiesChart').html('<p class="no-data">No data found</p>');
				}
		    }, function(error){
		    	$scope.associatedEntitiesPreloader = false;
		    	if(error.responseMessage){
		    		angular.element('#associatedEntitiesChart').html('<p class="no-data">'+ error.responseMessage +'</p>');
		    	}
		    });
		}
			
		$rootScope.$on("addEntities", function(event, entities){
			if (entities != null){
				angular.extend($scope.associatedEntities, entities);
			}
	     });

		
		 /*
		 * @purpose: open filter modal
		 * @created: 26 Dec 2017		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.openFilterModalForChart = function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return $scope.keysTofilter;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return $scope.FilteredKey;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return true;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
			                return $scope.AllNames;	
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }

		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {	  			
	  			handleFiltersApplied(response);
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 26 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			var actualData = jQuery.extend(true,{}, $scope.AED);
			var filtered= [];
			if(resp.caseSearchKeyword){
				$scope.FilteredKeyword = resp.caseSearchKeyword;
			angular.forEach(actualData,function(i){				
				var name = i.entityName ? i.entityName : "noName" ;
				if(name != "noName"){
					if(name.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1){
						filtered.push(i);
					}
				}
			});
			$scope.associatedEntitiesDatas = filtered;
				if(filtered.length > 0){
					$scope.maxoptions=filtered;
			        new timescalebubbleChart({
			        	 container: '#associatedEntitiesChart',
			        	 data: filtered,
			        	 width: AssociatedEntitiesConst.width,
			        	 height: AssociatedEntitiesConst.height,
			        	 isheader: AssociatedEntitiesConst.isheader,
			        	 ytext: AssociatedEntitiesConst.ytext,
			        	 xticks: AssociatedEntitiesConst.xticks,
			        	 margin: AssociatedEntitiesConst.margin
			         });
				}else{
					$scope.maxoptions= [];
					angular.element('#associatedEntitiesChart').html('<p class="no-data">No data found</p>');
				}
			}else{
				$scope.FilteredKeyword = "";
				loadAssociatedEntities();
			}
			
		}
		
		/*
		 * @purpose: open fullscreen modal
		 * @created: 3 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
			}, function(){
			});
			
		};
		
		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function() {
			if($scope.maxoptions.length != 0 &&  isAPiLoaded == true){
				appendGraph();
			}
		});
		
		$scope.OnToggleGraph = function(){
			$scope.actAssociatedEntitiestoggleGraph = true;
			if($scope.maxoptions.length != 0 &&  isAPiLoaded == true){
				appendGraph();
			}else{
				angular.element("#associatedEntitiesChart").html('<p class="no-data">No Data Found</p>');
			}
		};
		
		function appendGraph(){
			var heightResp = document.getElementById('actAssociatedEntitiesDiv').clientHeight ? (document.getElementById('actAssociatedEntitiesDiv').clientHeight - 65) : 300;
			if(heightResp < 320){
				heightResp = 230;
			}else{
				heightResp = 530;
			}
			var widthresp = $("#actAssociatedEntitiesDiv").width();
			$("#actAssociatedEntitiesDiv").find("tbody").css("height",heightResp);
			if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
				$('#associatedEntitiesChart').empty();
				 new timescalebubbleChart({
		        	 container: '#associatedEntitiesChart',
		        	 data: $scope.maxoptions,
		        	 width: widthresp,
		        	 height: heightResp,
		        	 isheader: AssociatedEntitiesConst.isheader,
		        	 ytext: AssociatedEntitiesConst.ytext,
		        	 xticks: AssociatedEntitiesConst.xticks,
		        	 margin: AssociatedEntitiesConst.margin
		         });
			}else{
				angular.element("#associatedEntitiesChart").html('<p class="no-data">No Data Found</p>');
			}
		}
}