'use strict';
angular.module('ehubApp')
	   .controller('CaseTimelineController', caseTimelineController);

		caseTimelineController.$inject = [
			'$scope', 
			'$rootScope',
			'$stateParams',
			'ActApiService',
			'HostPathService',
			'Flash',
			'$uibModal',
			'ActService'
		];
		function caseTimelineController(
				$scope, 
				$rootScope,
				$stateParams,
				ActApiService,
				HostPathService,
				Flash,
				$uibModal,
				ActService) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		var options, current_data = [];
		$scope.caseTimelinePreloader = false;
		var count = 10;
		
		$scope.logs = [];
		if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			$scope.caseTimelinePreloader = true;
			getCaseTimelineData();
		}else{
			angular.element("#caseTimelineChart").html('<p class="no-data">No Data Found</p>');
		}
		
		var CaseTime;
		function setCaseTimeLine(current_data){
			clearInterval(CaseTime);
			options = {
			        "container": "#caseTimelineChart", 
			        "header": "CASE TIMELINE", 
			        "customData": current_data,
			        "height": $("#caseTimelineChart").parent().height()+40,
			        "width": $("#caseTimelineChart").parent().width(),
			        "margin": {top:12, right: 20, bottom: 0, left: 10},
			        "headeroption": "false"
			    };
				$rootScope.OptionsDataCT = options;
				$rootScope.current_dataCT = current_data;
				ActService.chartCaseTimeLine(options);
		}
		
		/*
	     * @purpose: get Case Timeline Data
	     * @created: 18 jan 2018
	     * @params: none
	     * @return: no
	     * @author: swathi
	    */
		function getCaseTimelineData(){
			var params = {
				token: $rootScope.ehubObject.token
			};
			ActApiService.getCaseTimeline(params, $stateParams.caseId, count).then(function(response){
				$scope.caseTimelinePreloader = false;
				if(response.data.length > 0){
					HostPathService.setTimelineData(response.data);
					current_data = HostPathService.getTimelineData(current_data);
							CaseTime = setInterval(function(){
								if($('#caseTimelineChart').length > 0)
								{setCaseTimeLine(current_data);}
							},100);
				}else{
					angular.element('#caseTimelineChart').html('<p class="no-data">No data found</p>');
				}
				
			}, function(error){
				$scope.caseTimelinePreloader = false;
				if(error.responseMessage){
					angular.element('#caseTimelineChart').html('<p class="no-data">'+ error.responseMessage +'</p>');
				}
			});
		}
		
	
		 /*
		 * @purpose: open filter modal
		 * @created: 26 Dec 2017		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.filtercaseTimeline =function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return false;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return false;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return true;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return false;
		                },
		                AllNames: function(){
		                	if($rootScope.OptionsDataCT.data.length > 0){
		                		$scope.AllNames = [];
		                		angular.forEach($rootScope.OptionsDataCT.data,function(d){
		                			$scope.AllNames.push(d.label);
		                		});
		                		$scope.AllNames = _.uniq($scope.AllNames);
		                		return $scope.AllNames;
		                	}else{
			                	return [];	
		                	}

		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		            }
	         });

	  		openCasefiltersModal.result.then(function (response) {	  			
	  			handleFiltersApplied(response);
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 26 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			var filteredData=[];
			$scope.FilteredKeyword = '';
			var actualData = jQuery.extend(true,[], $rootScope.OptionsDataCT);
			if(resp.caseSearchKeyword){
				angular.forEach(actualData.data,function(d){
					var v  = d.label;
					var w = d.description;
					if((v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1) || (w.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1)){
						filteredData.push(d);
					}
				});
				$scope.FilteredKeyword = resp.caseSearchKeyword;
				actualData.data = filteredData; 
			}
			   
			   var data= jQuery.extend(true,[], actualData);
			   
			    if(data.data.length > 0){
			    	$scope.maxoptions = data;
			    }else{
			    	$scope.maxoptions = {};
			    	angular.element("#caseTimelineChart").html('<p class="no-data">No Data Found</p>');
			    }

		}
		
		/*
		 * @purpose: open fullscreen modal
		 * @created: 9 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
				
			}, function(){
			});
			
		};
		
		

}
