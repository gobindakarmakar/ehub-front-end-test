'use strict';
angular.module('ehubApp')
	   .controller('ActhottopicsController', acthottopicsController);

		acthottopicsController.$inject = [
			'$scope', 
			'$rootScope',
			'$http',
			'$stateParams', 
			'ActApiService', 
			'ActService',
			'EHUB_API',
			'ActHotTopicsConst',
			'$uibModal'
		];
		
		function acthottopicsController(
				$scope, 
				$rootScope,
				$http,
				$stateParams, 
				ActApiService, 
				ActService,
				EHUB_API,
				ActHotTopicsConst,
				$uibModal) {
			$scope.token = $rootScope.ehubObject.token;
			$scope.case_id = $stateParams.caseId;
			$scope.acthottopicstoggleGraph =true;	
			$scope.acthottopicsDatas=[];
			var actualHotTopicsData;
			$scope.totalDataLength = 0;
			var ActHotTopicOption = {
				container : "#hottopics",
				header : "HOT TOPICS",
				uri : EHUB_API + 'workflow/es/cases/' + $scope.case_id + '/summary?token=' + $scope.token,
				speed : ActHotTopicsConst.speed,
				slower : ActHotTopicsConst.slower,
				height : ActHotTopicsConst.height,
				timer : ActHotTopicsConst.timer,
				fontMultiplier : ActHotTopicsConst.fontMultiplier,
				isheader : ActHotTopicsConst.isheader,
				hoverStyle : ActHotTopicsConst.hoverStyle,
				mouseOutStyle : ActHotTopicsConst.mouseOutStyle
			};
			var params = {
				token: $rootScope.ehubObject.token
			};
			if($scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
				$scope.actHottopicsPreloader = true;
				caseSummary();
			}else{
				ActHotTopicOption = {};
				angular.element("#hottopicsTable").find('tbody').html('<tr>'
                    	+'<td colspan="2">'
                		+'<p class="no-data">No Data Found</p>'
                	+'</td>'
                +'</tr>');
				angular.element("#hottopics").html('<p class="no-data">No Data Found</p>');
			}
			
			/*
		     * @purpose: get list of case summary
		     * @created: 04 oct 2017
		     * @params: null
		     * @return: no
		     * @author: swathi
		    */
			var resultedArray = [];
			var isAPiLoaded = false;
			function caseSummary(){
				ActApiService.getCaseSummaryById(params, $scope.case_id).then(function(response) {
					resultedArray =  response.data;
					delete resultedArray.caseId;
					$scope.AllNames = [];
					$scope.actHottopicsPreloader = false;
					if(response.data && response.data.exceptionMessage == undefined){
						isAPiLoaded = true;
						actualHotTopicsData = resultedArray;
						$scope.keysTofilter = d3.keys(resultedArray);
						var finalData = handleTagCloudData(resultedArray);
						ActHotTopicOption.data = finalData[0];
						ActHotTopicOption.data1 = finalData[1];
						$scope.acthottopicsDatas = finalData[1];
						
						if($scope.totalDataLength > 0){
							angular.forEach($scope.acthottopicsDatas,function(d){
								$scope.AllNames.push(d.name);
							});
							$scope.AllNames = _.uniq($scope.AllNames);
							var actualOptions = jQuery.extend(true, {}, ActHotTopicOption);
				            $scope.maxoptions=actualOptions;
							ActService.setHotTopicsCloudChart(actualOptions);
							
							$('#hotTopicsWidget').mCustomScrollbar({
								axis : "y",
								theme : "minimal"
							});
						}else{
							angular.element("#hottopicsTable").find('tbody').html('<tr>'
			                    	+'<td colspan="2">'
			                		+'<p class="no-data">No Data Found</p>'
			                	+'</td>'
			                +'</tr>');
							angular.element('#hottopics').html('<p class="no-data">No Data Found</p>');
						}
					}else{
						angular.element("#hottopicsTable").find('tbody').html('<tr>'
		                    	+'<td colspan="2">'
		                		+'<p class="no-data">No Data Found</p>'
		                	+'</td>'
		                +'</tr>');
						angular.element('#hottopics').html('<p class="no-data">No Data Found</p>');
					}
				},function(error){
					$scope.actHottopicsPreloader = false;
					if(error.responseMessage){
						angular.element("#hottopicsTable").find('tbody').html('<tr>'
		                    	+'<td colspan="2">'
		                		+'<p class="no-data">No Data Found</p>'
		                	+'</td>'
		                +'</tr>');
						angular.element('#hottopics').html('<p class="no-data">'+ error.responseMessage +'</p>');
					}
				});
			}
			 /*
			 * @purpose: handle TagCloudData
			 * @created: 04 oct 2017
			 * @params: response(object)
			 * @returns: newData(object), newDataArray(object)
			 * @author: swathi
			 */
			function handleTagCloudData(response) {
				$scope.totalDataLength = 0;
				var keys = d3.keys(response);/*jshint ignore : line */
				var newData = {};
				var newDataArray =[];
				angular.forEach(response, function(objVal, objKey){
					var j = 0;
					if(objVal)
						{var keys = Object.keys(objVal);}
					if(keys){
				    	var len = keys.length;
				    	$scope.totalDataLength  = $scope.totalDataLength  + len;
					}
					angular.forEach(objVal, function(val, key){
						if(j < 15){
							newDataArray.push({
								name: key,
								count: val,
								group: objKey
							});
							j++;
							newData[key] = val;
						}
					});
				});
				return [newData,newDataArray];
			}
			
			 /*
			 * @purpose: open filter modal
			 * @created: 26 Dec 2017		
			 * @author: prasanthi
			 */
			var openCasefiltersModal;
			$scope.openFilterModalForChart =function(){
				openCasefiltersModal = $uibModal.open({
		             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
		             scope: $scope,
			         controller: 'filterCaseModalController',
		             //backdrop: 'static',
					 windowClass: 'custom-modal',
					 resolve: {
			            	isOnlyKeyword: function () {
			                  return true;
			                },
			                statusList:function(){
			                	return false;
			                },
			                keysTofilter:function(){
			                	return $scope.keysTofilter;
			                },
			                FilteredKeyword:function(){
			                	return $scope.FilteredKeyword;
			                },
			                FilteredKey : function(){
			                	return $scope.FilteredKey;
			                },
			                RiskRatio : function(){
			                	return false;
			                },
			                onlySearch : function(){
			                	return false;
			                },
			                CaseDiarySelections : function(){
			                	return false;
			                },
			                StatusOption :function(){
			                	return $scope.StatusOption;
			                },
			                AllNames:function(){
			                	return $scope.AllNames;
			                },
			                isdateRequired: function(){
			                	return false;
			                },
			                itemsPerPage: function(){
			                	return false;
			                }
			            }
		         });

		  		openCasefiltersModal.result.then(function (response) {	
		  			if(response)
		  				{handleFiltersApplied(response);}
		         }, function () {
		         });
		         $scope.modalInstance = openCasefiltersModal;
		         return openCasefiltersModal.result;
			};
			
			/*
			 * @purpose: handle/filter data
			 * @created: 26 Dec 2017
			 *  @params: response of modal(object)		 
			 * @author: prasanthi
			 */
			function handleFiltersApplied(resp){
				var filteredData={};
				$scope.FilteredKeyword = "";
				var actualData = jQuery.extend(true,{}, actualHotTopicsData);
				if(resp.filterApplied.entity){
					var keys =[resp.filterApplied.entity];
					$scope.FilteredKey = resp.filterApplied.entity;
				}else{
					var keys = d3.keys(actualData);
					$scope.FilteredKey = "ALL";
				}
				angular.forEach(keys,function(d){				
					var innerKeys =d3.keys(actualData[d]); 							
					var filtered={};
					if(!resp.caseSearchKeyword){
						filtered = actualData[d];
					}else{
						$.grep(innerKeys, function(v,i) {
							
							if(v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1){
								filtered[v]=i;
							}
						});
						$scope.FilteredKeyword = resp.caseSearchKeyword;
					}
					filteredData[d]=filtered;
				});

				$(ActHotTopicOption.container).empty();
				var finalData = handleTagCloudData(filteredData);
				ActHotTopicOption.data = finalData[0];
				ActHotTopicOption.data1 = finalData[1];
				$scope.acthottopicsDatas = finalData[1];
				if($scope.totalDataLength > 0){
					var actualOptions = jQuery.extend(true, {}, ActHotTopicOption);
		            $scope.maxoptions = actualOptions;
					ActService.setHotTopicsCloudChart(actualOptions);
				}else{
					$scope.maxoptions = {};
					angular.element("#hottopicsTable").find('tbody').html('<tr>'
	                    	+'<td colspan="2">'
	                		+'<p class="no-data">No Data Found</p>'
	                	+'</td>'
	                +'</tr>');
					angular.element('#hottopics').html('<p class="no-data">No Data Found</p>');
				}
			}

			/*
			 * @purpose: open fullscreen modal
			 * @created: 3 Jan 2018		
			 * @author: varsha
			 */		
			$scope.maximizeDiv=function(Chart){
				var maximizeModal = $uibModal.open({
					templateUrl: 'scripts/common/modal/views/maximize.modal.html',
					controller: 'MaximizeModalController',
					size: 'lg',
					backdrop: 'static',
					windowClass: 'custom-modal capture-modal',
					resolve: {
						Id: function(){
							return Chart;
						},
					    ChartOptions:function(){
					    	return $scope.maxoptions;
					   }
					}
				});
				maximizeModal.result.then(function(){
					
				}, function(){
				});
				
			};

			/* on resize of Div */
			$scope.$on('gridster-draggable-changed', function() {
				setHotTopics();
			});

			$scope.resizeGraph =function(){
				$scope.acthottopicstoggleGraph = true;
				setTimeout(function(){
					setHotTopics();
				},0);

			};
			
			
			function setHotTopics(){
				var heightResp = document.getElementById('acthottopicsdiv').clientHeight ? document.getElementById('acthottopicsdiv').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 230;
				}
				$("#acthottopicsdiv").find("tbody").css("height",heightResp);
				
				if($scope.totalDataLength == 0 && isAPiLoaded == true ){
					angular.element("#hottopicsTable").find('tbody').html('<tr>'
	                    	+'<td colspan="2">'
	                		+'<p class="no-data">No Data Found</p>'
	                	+'</td>'
	                +'</tr>');
					angular.element("#hottopics").html('<p class="no-data">No Data Found</p>');
				}else if($scope.totalDataLength > 0){
					ActService.setHotTopicsCloudChart(ActHotTopicOption);
				}
			}
}