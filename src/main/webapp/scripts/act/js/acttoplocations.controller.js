'use strict';
angular.module('ehubApp')
	   .controller('ActTopLocationsController', actTopLocationsController);

		actTopLocationsController.$inject = [
			'$scope', 
			'$rootScope',
			'ActApiService',
			'DiscoverService',
			'DiscoverApiService',
			'ActTopLocationsConst',
			'$stateParams',
			'$state'
		];
		
		function actTopLocationsController(
				$scope, 
				$rootScope,
				ActApiService,
				DiscoverService,
				DiscoverApiService,
				ActTopLocationsConst,
				$stateParams,
				$state) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.actTopLocationstoggleGraph = true;
		$scope.actworldDatas = {};
		var locations = {};
		var zoomOptions = ActTopLocationsConst.zoomOptions;
		
		if($scope.token != undefined && $scope.token != null && $scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			$scope.topLocationsPreloader = true;
			loadTopLocations();
		}else{
			$scope.actworldDatas = {};
			setTimeout(function(){
				var leafletMap = DiscoverService.setWorldMap(locations, zoomOptions);
				$rootScope.leafletMap = leafletMap;
			}, 0);
		}
		/*
	     * @purpose: load top locations
	     * @created: 26 sep 2017
	     * @params: null
	     * @return: no
	     * @author: swathi
	    */
		function loadTopLocations(){
			var params = {
				token: $rootScope.ehubObject.token
			};
			ActApiService.getCaseSummaryById(params, $scope.case_id).then(function(response){
				$scope.topLocationsPreloader = false;
				if(response.data && response.data.exceptionMessage == undefined){
					$scope.actworldDatas = response.data.locations;
				    $scope.totalLocation = Object.keys(response.data.locations).length;
					$('#actTopLocationsTable').mCustomScrollbar({
						axis : "y",
						theme : "minimal"
					});
				}else{
					$scope.actworldDatas = {};
					$scope.totalLocation = 0;
				}
			    setTimeout(function(){
				    var leafletMap = DiscoverService.setWorldMap($scope.actworldDatas, zoomOptions);
				    $rootScope.leafletMap = leafletMap;
			    }, 0);
			}, 
			function(error) {
				$scope.topLocationsPreloader = false;
				if(error.responseMessage){
					angular.element('#mapLeaflet').html('<p class="no-data">'+ error.responseMessage +'</p>');
				}
			});
		}
//		var markersLoaded;
//		function callMap(){
//			if(markersLoaded != undefined){
//			   clearInterval(test);
//			   $scope.topLocationsPreloader = false;
//			}
//			markersLoaded = DiscoverService.getWorldMap();
//		};
//		
//		var test = setInterval(function(){
//			callMap();
//		}, 100);
		if($state.current.name == 'act'){
			$(window).resize(function () {
				 if(map != undefined || map != null){/*jshint ignore : line */
					var heightResp = document.getElementById('actTopLocationsdiv').clientHeight ? (document.getElementById('actTopLocationsdiv').clientHeight) : 300;
					var widthresp = $("#actTopLocationsdiv").width();
					    map.remove();/*jshint ignore : line */
					   $("#map").html("");
					   $( "<div id=\"map\" style=\"height:" + heightResp +"px" + ";width:" + widthresp +"px" + "\"></div>" ).appendTo("#mapLeaflet");
					}
			    	var leafletMap= DiscoverService.setWorldMap($scope.actworldDatas, zoomOptions);
				    $rootScope.leafletMap = leafletMap;
			});
		}
		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function() {
			var heightResp = document.getElementById('actTopLocationsdiv').clientHeight ? (document.getElementById('actTopLocationsdiv').clientHeight): 300;
			var widthresp = $("#actTopLocationsdiv").width();
			if(heightResp > 310){
				heightResp = 530;
			}else{
				heightResp = 230;
			}
			$("#actTopLocationsdiv").find("tbody").css("height", heightResp);
			$("#actTopLocationsdiv").find("tbody").css("width", widthresp);
		});
}