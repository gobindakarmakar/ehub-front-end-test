'use strict';
angular.module('ehubApp')
	   .controller('ActDashboardController', actDashboardController);

		actDashboardController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal',
			'ActApiService',
			'HostPathService',
			'Flash',
			'$stateParams',
			'$interval',
			'EntityApiService'
		]; 
		function actDashboardController(
				$scope, 
				$rootScope,
				$state,
				$uibModal,
				ActApiService,
				HostPathService,
				Flash,
				$stateParams,
				$interval,
				EntityApiService) {
		
		$scope.actDashboard = {
			contentMenu: 'actMainMenu',
			contentMenu0: 'actSubMenu0',
			contentMenu1: 'actSubMenu1',
			contentMenu2: 'actSubMenu2',
			contentMenu3: 'actSubMenu3',
			contentMenu4: 'actSubMenu4',
			contentMenu5: 'actSubMenu5'
		};
		$scope.hyperLinksNewTab = $rootScope.hyperLinksNewTab;
		$scope.alertsData = [];
		$scope.Dataalerts = [];
		var caseId = $stateParams.caseId;
		var filteredContant = false;
		/*
	     * @purpose: open captured widget modal
	     * @created: 26 sep 2017
	     * @params: widgetId(string)
	     * @return: no
	     * @author: swathi
	    */
		$scope.actCaptureDiv = function(widgetId, actTopLocationstoggleGraph){
			var openWidgetCaptureModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
				controller: 'WidgetCaptureModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					widgetId: function(){
						return widgetId;
					},
					canvasnew:function(){
						return '';
					},
					topLocMapView: function(){
						return actTopLocationstoggleGraph;
					}
				}
			});
			openWidgetCaptureModal.result.then(function(){
				
			}, function(){
			});
		};
		var params = {
			caseId: caseId,
			token: $rootScope.ehubObject.token
		};
		var token = {
			token: $rootScope.ehubObject.token
		};
		var count = 100;
		$scope.alertsPreloader1 = true;
		$scope.relatedCompaniesPreloader = true;
		$scope.actualPeergroupData = [];
		$scope.relatedCompaniesData = [];
		
		if($stateParams.caseId != undefined && $stateParams.caseId != null && $stateParams.caseId != ''){
			$scope.alertsPreloader = true;
			$scope.alertsPreloader1 = true;
			$scope.relatedCompaniesPreloader = true;
			ActApiService.getCaseDetails(params).then(function(response){
				if(response.data.name){
					$scope.caseName = response.data.name;
					getProfileInfo(response.data.name);
				}else{
					 getRadarThreatAttackType(); 
					 getTableData($scope.pageNum);
				}
			}, function(){
				 getRadarThreatAttackType(); 
				 getTableData($scope.pageNum);
			});
			getAlerts();
			$interval(function(){
				getAlerts();
			}, 20000);
		}else{
			$scope.alertsPreloader1 = false;
			$scope.relatedCompaniesPreloader = false;
			angular.element("#notifications-wrapper").html('<p class="no-data">NO DATA FOUND</p>');
			 }
		
		
		/*
	     * @purpose: get Alerts
	     * @created: 17 jan 2018
	     * @params: null
	     * @return: no
	     * @author: swathi
	    */
		function getAlerts(){
			ActApiService.getCaseByIdAlerts(token, caseId, count).then(function(response){
				$scope.alertsPreloader = false;
				if(response.data.length != 0){
					$scope.alertsData = response.data;
					$scope.Dataalerts = response.data;
					$('#alertsWidget').mCustomScrollbar({
						axis : "y",
						theme : "minimal"
					});
				}else{
					angular.element("#notifications-wrapper").html('<p class="no-data">NO DATA FOUND</p>');
				}
				if(filteredContant){
					var data =[];
					data.filterApplied = [];
					data.caseSearchKeyword = $scope.FilteredKeyword ? $scope.FilteredKeyword : "";
					data.filterApplied.endDate = $scope.EndD ? $scope.EndD : "";
					data.filterApplied.startDate = $scope.StartD ? $scope.StartD  : "";
					handleFiltersApplied(data);
				}
			}, function(error){
				$scope.alertsPreloader = false;
				if(error.responseMessage){
					angular.element('#alertsWidget').html('<p class="no-data">'+ error.responseMessage +'</p>');
				}
			});
		}
		
		/*
	     * @purpose: on click navigate to transaction Intelligence page
	     * @created: 10 nov 2017
	     * @params: null
	     * @return: no
	     * @author: swathi
	    */  
		$scope.onTransactionIntelligence = function(){
			var url;
			if($state.current.name == 'actCase'){
				url = window.location.href.split("/#/")[0]+"/transactionIntelligence#!/tI?q=" + $scope.caseName + '&type=entity&entityID=' + $scope.caseName + '&searchName=' + $scope.caseName + '&caseId=' + caseId;
			}else{
				url = window.location.href.split("/#/")[0]+"/transactionIntelligence#!/landing";
			}
			window.open(url, '_blank');
		};
		
		 window.showriskTransaction = function(title) {
			 if($scope.isRisk == true){
		        function callAlert() { /*jshint ignore:line */
					HostPathService.FlashErrorMessage("<span style='margin-top:15px; display: block;'>" + title + " on 12/02/2017  <a id='readMoreLink' class='read-more-link' target='_blank' style='display: inline-block; color:#fff;'> Read More</a></span>", '');
		        }
		        setTimeout(callAlert, 100);
			 }
        };
		 
		 $("body").on("click",".read-more-link",function(e){
			 e.preventDefault();
			 $scope.onTransactionIntelligence();
		 });
		 
		 
		 /*
			 * @purpose: Open Filter Modal for Alerts
			 * @created: 26 Dec 2017		
			 * @author: prasanthi
			 */
			var openCasefiltersModal;
			$scope.filterAlertsData =function(){
				openCasefiltersModal = $uibModal.open({
		             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
		             scope: $scope,
			         controller: 'filterCaseModalController',
		             backdrop: 'static',
					 windowClass: 'custom-modal',
					 resolve: {
			            	isOnlyKeyword: function () {
			                  return true;
			                },
			                statusList:function(){
			                	return false;
			                },
			                keysTofilter:function(){
			                	return false;
			                },
			                FilteredKeyword:function(){
			                	return $scope.FilteredKeyword;
			                },
			                FilteredKey : function(){
			                	return false;
			                },
			                RiskRatio : function(){
			                	return false;
			                },
			                onlySearch : function(){
			                	return true;
			                },
			                CaseDiarySelections : function(){
			                	return false;
			                },
			                StatusOption :function(){
			                	return false;
			                },
			                AllNames : function(){
			                	
			                	if($scope.Dataalerts.length > 0){
				                	$scope.AllNames = [];
				                	angular.forEach($scope.Dataalerts,function(d){
				                		$scope.AllNames.push(d.label);
				                	});
				                	$scope.AllNames = _.uniq($scope.AllNames);
				                	return $scope.AllNames;
			                	}else{
				                	return [];
			                	}

			                },
			                isdateRequired: function(){
			                	return [
			                		{"startDate":$scope.StartD},
			                		{"endDate":$scope.EndD}
			                		];
			                },
			                itemsPerPage: function(){
			                	return false;
			                }
			            }
		         });

		  		openCasefiltersModal.result.then(function (response) {	  			
		  			handleFiltersApplied(response);
		         }, function () {
		         });
		         $scope.modalInstance = openCasefiltersModal;
		         return openCasefiltersModal.result;
			};
			
			/*
			 * @purpose: handle/filter data
			 * @created: 26 Dec 2017
			 *  @params: response of modal(object)		 
			 * @author: prasanthi
			 */
			function handleFiltersApplied(resp){
				if(resp){
					var filteredData=[];
					filteredContant  = true;
					var actualData = jQuery.extend(true,[], $scope.Dataalerts);
					filteredData = actualData;
					if(resp.caseSearchKeyword){
						filteredData=[];
						$scope.EndD = resp.filterApplied.endDate ?resp.filterApplied.endDate : "skip" ;
						$scope.StartD = resp.filterApplied.startDate ? resp.filterApplied.startDate : "skip" ;
						angular.forEach(actualData,function(d){
							var v  = d.label;
							var w = d.description;
							var y = d.datetime;
							if((v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1) || (w.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1)){
								if((y > $scope.StartD && y < $scope.EndD)|| ($scope.EndD == "skip" && $scope.StartD == "skip"))
								{filteredData.push(d);}
							}
						});
						$scope.FilteredKeyword = resp.caseSearchKeyword;
//						actualData["data"] = filteredData; 
						
					}else{
						$scope.EndD = resp.filterApplied.endDate ?resp.filterApplied.endDate : "skip" ;
						$scope.StartD = resp.filterApplied.startDate ? resp.filterApplied.startDate : "skip" ;
						$scope.FilteredKeyword = '';
						if($scope.EndD != "skip" && $scope.StartD != "skip"){
						$scope.FilteredKeyword = '';
						filteredData=[];
						angular.forEach(actualData,function(d){
							var y = d.datetime;
							if((y > $scope.StartD && y < $scope.EndD))
								{filteredData.push(d);}
						});
						}
					}
					if(filteredData.length > 0){
						$('#alertsWidget').mCustomScrollbar({
							axis : "y",
							theme : "minimal"
						});
						$scope.alertsData = filteredData;
						$('#alertsWidget').mCustomScrollbar('update');
					}else{
						$scope.alertsData = [];
//						$scope.NOdata  = true;
						$('#alertsWidget').mCustomScrollbar('destroy');
						angular.element("#notifications-wrapper").find("ul#noAlerts").html('<li class="no-data">No Data Found</li>');
					}
				}
				

				
//		            var exampleChart = new CaseTimeLineChart(actualData);
			}
		 /*------------------------*/
			 var todayDt = new Date();
			 var lastYr = new Date();
		 		lastYr = lastYr.setFullYear(lastYr.getFullYear() - 1);
		 		
			var lastSixYrs = new Date();
				lastSixYrs = lastSixYrs.setFullYear(lastSixYrs.getFullYear() - 6);
			 
			 var yesterdaydt = new Date();
			 	yesterdaydt = yesterdaydt.setDate(yesterdaydt.getDate() - 1);
			 	
		 	 var lastMonth = new Date();
		 	 	lastMonth = lastMonth.setMonth(lastMonth.getMonth() - 1);
		 	 	
		 	 var lastThirteenMonths = new Date();
		 	 	lastThirteenMonths = lastThirteenMonths.setMonth(lastThirteenMonths.getMonth() - 13);
		 	 	
			 var lastYrFromDate = new Date(lastYr);
			 var fromMonth = String(lastYrFromDate.getMonth() + 1);
			 var fromDay = String(lastYrFromDate.getDate());
			 var toMonth = String(todayDt.getMonth() + 1);
			 var toDay = String(todayDt.getDate());
			 
			 if (fromMonth.length < 2) {fromMonth = '0' + fromMonth;}
			 if (fromDay.length < 2) {fromDay = '0' + fromDay;}
			 if (toMonth.length < 2) {toMonth = '0' + toMonth;}
			 if (toDay.length < 2) {toDay = '0' + toDay;}
			 
			 var fromDate = lastYrFromDate.getFullYear() + '-' + fromMonth + '-' + fromDay;
			 var toDate = todayDt.getFullYear() + '-' + toMonth + '-' + toDay;
			 
		$scope.token = $rootScope.ehubObject.token;
		var radarParams = {
			"country-code":{
			 	"terms_set":[""],
			 	"minimum_should_match": 0.2
			 },
			 "industry":{
				 "terms_set":[""],
				 "minimum_should_match": 0.2
			 },
			 "technology":{
				 "terms_set":[""],
				 "minimum_should_match": 0.2 
			 }
		 };
		 $scope.checkRadarParams = {
			'industry': false,
			'technology': false,
			'country-code': false,
			'legal-type': false
		 };
		/*
	     * @purpose: funciton to get profile info
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		var companyWebsiteUrl;
		function getProfileInfo(name){
			var initateData = {
				 "keyword": name,
				 "searchType": 'Company',
				 "limit": 1
		 	};
			EntityApiService.getResolvedEntityData(initateData).then(function(response) {
				if (response.data && response.data.data && response.data.data.results && response.data.data.results.length > 0 && response.data.data.results["0"] != null && response.data.data.results["0"]["merged-result"] && response.data.data.results["0"]["merged-result"] != null && response.data.data.results["0"]["merged-result"] != '') {
	                if(response.data.data.results["0"]["merged-result"].basic && response.data.data.results["0"]["merged-result"].basic.length > 0 && response.data.data.results["0"]["merged-result"].basic["0"].value){
	                	 var keepGoingLegal = true;
						 response.data.data.results["0"]["merged-result"].basic.map(function(d) {
							 if(d.value["lei:legalForm"] && d.value["lei:legalForm"].code && keepGoingLegal){
								 keepGoingLegal = false;
								 radarParams["legal-type"] = d.value["lei:legalForm"].code;
								 $scope.checkRadarParams["legal-type"] = true;
								 $scope.checkOptions.peerLegalType = true;
							 }
						 });
						 if(response.data.data.results["0"]["merged-result"].basic["0"].value.isDomiciledIn){
							 radarParams['country-code'].terms_set = [response.data.data.results["0"]["merged-result"].basic["0"].value.isDomiciledIn];
							 $scope.checkRadarParams['country-code'] = true;
							 $scope.checkOptions.peerLocation = true;
						 }
						 var Isic = {};
						 var keepGoingUrl = true;
						 response.data.data.results["0"]["merged-result"].basic.map(function(d) {
							 if(d.value.hasURL && d.value.hasURL.length > 0 && keepGoingUrl){
								 keepGoingUrl = false;
								 if(Array.isArray(d.value.hasURL)){
									 companyWebsiteUrl = d.value.hasURL[0];
								 }else{
									 companyWebsiteUrl = d.value.hasURL;
								 }
								 companyWebsiteUrl = companyWebsiteUrl.replace(/(^\w+:|^)\/\//, '');
							 }
							 if(d.value["bst:businessClassifier"] && d.value["bst:businessClassifier"].length > 0){
								 Isic =  d.value["bst:businessClassifier"];
								 if(Isic.length > 0){
									 var keepGoingCode = true;
									 angular.forEach(Isic, function(v){
										 if(v.standard && v.standard == 'ISIC' && keepGoingCode){
											 var code = v.code.charAt(0);
											 keepGoingCode = false;
											 radarParams.industry.terms_set = [code];
											 $scope.checkRadarParams.industry = true;
											 $scope.checkOptions.peerIndustry = true;
										 }
									 });
								 }
							 }
						 });
					 }
	               /*  if(companyWebsiteUrl){
//	  				  	console.log("companyWebsiteUrl", companyWebsiteUrl);
	  				  	var techData = {
  				  			"hosts": [companyWebsiteUrl] 
	  				  	};
		       			EntityApiService.getTechnologies(techData).then(function(response) {
		       				if(response && response.data && response.data.length > 0 && response.data[0].technologies && response.data[0].technologies.length > 0){
		       					var techArray = [];
		       					angular.forEach(response.data[0].technologies, function(v){
		       						if(v.type){
		       							techArray.push(v.type);
		       							$scope.checkRadarParams.technology = true;
		       							$scope.checkOptions.peerTechnology = true;
		       						}
		       					});
		       					if(techArray.length > 0){
		       						radarParams.technology.terms_set = techArray;
		       					}
		       					getRadarThreatAttackType(radarParams);	
			       				getTableData($scope.pageNum);
		       				}else{
		       					getRadarThreatAttackType(radarParams);	
			       				getTableData($scope.pageNum);
		       				}
		       			}, function(){
		       				getRadarThreatAttackType(radarParams);	
		       				getTableData($scope.pageNum);
		       			});
	                }else { */
		            	 getRadarThreatAttackType(); 
		            	 getTableData($scope.pageNum);
		         //   }
	            } else {
	            	 getRadarThreatAttackType(); 
	            	 getTableData($scope.pageNum);
	            }
	        }, function() {
	        	 getRadarThreatAttackType(); 
	        	 getTableData($scope.pageNum);
	        });
		}
		

			var threatsRadarData = [], attacksData = [],attacksDataForCompany = [];
			$scope.threatsRadarList = [];
			
			$scope.checkOptions = {
				'peerIndustry': false,
				'peerTechnology': false,
				'peerLocation': false,
				'peerLegalType': false	
			};
			/*
		     * @purpose: get Peer Options
		     * @created: 23rd august 2018
		     * @author: swathi
		     */
			$scope.getPeerOptions = function(){
				var finalRadarParams = {};
				var obj = {
					'industry': $scope.checkOptions.peerIndustry,
					'technology': $scope.checkOptions.peerTechnology,
					'country-code': $scope.checkOptions.peerLocation,
					'legal-type': $scope.checkOptions.peerLegalType
				};		
				angular.forEach(radarParams, function(v, k){
					if(v.terms_set && v.terms_set.length > 0 && obj[k] == true){
						finalRadarParams[k] = v;
					}
					if(k == "legal-type" && v && obj[k] == true){
						finalRadarParams[k] = v;
					}
				});
				getRadarThreatAttackType(finalRadarParams);
				$scope.pageNum = 1;
				getTableData($scope.pageNum);
			};
			/*
		     * @purpose: plot peer group without attributes
		     * @created: 23rd august 2018
		     * @params: params(object)
		     * @author: swathi
		     */
			function getRadarThreatAttackType(params){
				$("#global_company_radar_chart").empty();
				$("#peerGroupanalysisForRadar").empty();
				threatsRadarData = [], attacksData = [], attacksDataForCompany = [];/*jshint ignore:line */
				$scope.threatsRadarList = [];
				$scope.actualPeergroupData = [];
				$scope.alertsPreloader1 = true;
				var isAttributesExist = false;
				 var data = {
					"query": {
						'date-from': fromDate,
						'date-to': toDate
					}, 
					"agg": {
						"key": "attack-type"
					}
				 };
				 var data1 = {
					"query": {
						'date-from': fromDate,
						'date-to': toDate
					}, 
					"agg": {
						"key": "attack-type"
					}
				 };
				 if(params){
					 angular.forEach(params, function(v, k){
						 if(v.terms_set && v.terms_set[0].length > 0){
							 isAttributesExist = true;
							 data1.query[k] = v;
						 }
						 if(k == "legal-type" && v){
							 isAttributesExist = true;
							 data1.query[k] = v;
						 }
					 });
				 }
				 ActApiService.getThreatAggregates(data).then(function(response) {
					 if(response.data.aggregations[0].buckets.length > 0){
//						 attacksData.push(response.data.aggregations[0].buckets);
						 attacksData = response.data.aggregations[0].buckets;
					 }
			       
					 if(attacksData.length > 0){
						 var totIncidentsCount = d3.sum(attacksData, function(d){
							return d.doc_count;
						 });
						//map data for chart
						attacksData.map(function(d){
							d.axis = d.key;
							d.value = parseInt(((d.doc_count / totIncidentsCount) * 100).toFixed(2));
							d.actualValue = d.doc_count;
							d.group = "global";
							d.totIncidentsCount = totIncidentsCount;
							d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
						});
						//push global data
						if(attacksData && attacksData.length>0)
						 	{threatsRadarData.push(attacksData);}
						if(!isAttributesExist){
							$scope.threatsRadarList = threatsRadarData;
							$scope.alertsPreloader1 = false;
							plotRadarChart(threatsRadarData);
						}
						else{
							plotPeerGroupByAttributes(data1);
						}
					}else{
						$scope.alertsPreloader1 = false;
						$("#global_company_radar_chart").empty();
						$("#peerGroupanalysisForRadar").empty();
					}
			     }, function() {
			    	 $scope.alertsPreloader1 = false;
			    	 $("#global_company_radar_chart").empty();
					$("#peerGroupanalysisForRadar").empty();
			     });
			}
			/*
		     * @purpose: plot peer group by industry/country/technology attributes
		     * @created: 23rd august 2018
		     * @params: data(object)
		     * @author: swathi
		     */
			function plotPeerGroupByAttributes(data){
				ActApiService.getThreatAggregates(data).then(function(response) {
					 if(response.data.aggregations[0].buckets.length > 0){
//						 attacksDataForCompany.push(response.data.aggregations[0].buckets);
						 attacksDataForCompany = response.data.aggregations[0].buckets;
					 }
					 if(attacksDataForCompany && attacksDataForCompany.length>0){
						 var totIncidentsCount = d3.sum(attacksDataForCompany, function(d){
							return d.doc_count;
						 });
						//map data for chart for industry 
						attacksDataForCompany.map(function(d){
							d.axis = d.key;
							d.value = parseInt(((d.doc_count / totIncidentsCount) * 100).toFixed(2));
							d.actualValue = d.doc_count;
							d.group = "company";
							d.totIncidentsCount = totIncidentsCount;
							d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
						});
						threatsRadarData.push(attacksDataForCompany);
						$scope.alertsPreloader1 = false;	
					     // call funciton to plot radar chart
						$scope.threatsRadarList = threatsRadarData;
						plotRadarChart(threatsRadarData);
		 			}else{
			 			$scope.threatsRadarList = threatsRadarData;
						plotRadarChart(threatsRadarData);
						$scope.alertsPreloader1 = false;
			 		}
			     }, function() {
			    	 // call funciton to plot radar chart
			    	 $scope.threatsRadarList = threatsRadarData;
			    	 plotRadarChart(threatsRadarData);
			    	 $scope.alertsPreloader1 = false;
			     });
			}

		/*--------------------------------------------------radar chart attack Types Ends --------------------------------------------------------------*/

		/*
	     * @purpose: funciton to plot radar
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		function plotRadarChart(data){
			$scope.actualPeergroupData = data;
			if(!data || data.length==0){
				return false;
			}
			var d = jQuery.extend(true, [],data);
			  //Options for the Radar chart, other than default
		        var options = {
		            container: "#global_company_radar_chart",
		            width:235,
		            height:210,
		            ExtraWidthX: 230,
		            circleRadius:1.5,
		            strokeWidth:0.1,
		            opacityArea:0.5,
		            marginTop:30,
		            marginBottom:30,
		            iswhiteBackground:true,
		            colorObj:{"global":"#BA4441","company":"#BB9800"}
		        };
			  //Call function to draw the Radar chart
			  //Will expect that data is in %'s
			  RadarChart("#global_company_radar_chart", d, options);
		}
		
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('alertsWidget')){
				var heightResp = document.getElementById('alertsWidget').clientHeight ? document.getElementById('alertsWidget').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#alertsWidget").find("tbody").css("height",heightResp);
			}
		});

		
		/*--------------------------------------------------table handling starts here--------------------------------------------------------------*/
		
		$scope.pageNum = 1;
		$scope.recordsPerPage = 4;
		 /*
	     * @purpose: on page change
	     * @created: 24 aug 2017
	     * @params: page(number)
	     * @author: swathi
	    */ 
	    $scope.pageChanged = function (page) {
	        $scope.pageNum = page;
	        getTableData(page);
	    };
		/*
	     * @purpose: funciton to get data for table
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		function getTableData(page){
			$scope.relatedCompaniesData = [];
			$scope.relatedCompaniesPreloader = true;
			var data =	{
				"actor-a-type": "organization",
				"class" : "Cyber Security",
				"size" : $scope.recordsPerPage,
				"offset": page ? (page - 1): 0
			};
			var multiData = {
				"country-code":{
				 	"terms_set":[""],
				 	"minimum_should_match": 0.2
				 },
				 "industry":{
					 "terms_set":[""],
					 "minimum_should_match": 0.2
				 },
				 "technology":{
					 "terms_set":[""],
					 "minimum_should_match": 0.2 
				 }
			};
			if($scope.checkOptions.peerTechnology && radarParams.technology.terms_set[0]){
				multiData.technology.terms_set = radarParams.technology.terms_set;
			}
			if($scope.checkOptions.peerIndustry && radarParams.industry.terms_set[0]){
				multiData.industry.terms_set =  radarParams.industry.terms_set;
			}
			if($scope.checkOptions.peerLocation && radarParams["country-code"].terms_set[0]){
				multiData["country-code"].terms_set = radarParams["country-code"].terms_set;
			}
			if($scope.checkOptions.peerLegalType && radarParams["legal-type"]){
				multiData["legal-type"] = radarParams["legal-type"];
			}
			 angular.forEach(multiData, function(v, k){
				 if(v.terms_set && v.terms_set[0].length > 0){
					 data[k] = v;
				 }
				 if(k == "legal-type" && v){
					 data[k] = v;
				 }
			 });
			ActApiService.getRelatedCompanies(data).then(function(response) {				
				$scope.relatedCompaniesPreloader = false;
				if(response && response.data){
					$scope.similarPeerLength = response.data.totalResults;
					if(response.data.peerDataDto && response.data.peerDataDto.length>0){
						$scope.relatedCompaniesData = response.data.peerDataDto;
						$scope.avgScore = ((d3.sum(response.data.peerDataDto,function(d){
							return d.riskScore;
						}))/$scope.relatedCompaniesData.length);
					}
				}
				else{
					//data not found
					$scope.relatedCompaniesData = [];
				}
				
	        }, function() {
	        	$scope.relatedCompaniesData = [];
	        	$scope.relatedCompaniesPreloader = false;
	        	//data not found
	        });
			
		}
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('relatedCompaniesDiv')){
				var heightResp = document.getElementById('relatedCompaniesDiv').clientHeight ? document.getElementById('relatedCompaniesDiv').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#relatedCompaniesDiv").find("tbody").css("height",heightResp);
			}
		});
		/*
	     * @purpose: function to open scoring details
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		$scope.openriskModal = function(riskDetails){
			riskDetails.cumulativeRisk = riskDetails.riskScore;
			 var modalInstance = $uibModal.open({
		            templateUrl: 'scripts/common/modal/views/entityRiskScore.modal.html',
		            controller: 'EntityRiskScoreModalController',
					windowClass: 'custom-modal entity-risk-score-modal',
					resolve:{
						overviewRisks: function(){
		            		return riskDetails;
		            	},
		            	caseID:function(){
		            		return "";
		            	},
		            	caseName:function(){
		            		return "";
		            	}
		            }
						
		        });

		        modalInstance.result.then(function(result){
		            console.log(result,'result');
		        }, function(err){
		            console.log(err,'error');
		        });
		};
		/*
	     * @purpose: function to open entity page
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		$scope.openEntityPage = function(data){
			var  url =window.location.href.split("/#/")[0]+"/entity#!/company/"+data.name+'?eid='+data.identifier;
		      if($scope.hyperLinksNewTab){
					window.open(url, '_blank');
		      }else{
		    		window.open(url, '_self');
		      }
		};

}