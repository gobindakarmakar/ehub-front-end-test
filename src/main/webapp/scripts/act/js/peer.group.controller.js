'use strict';
angular.module('ehubApp')
	   .controller('PeerGroupController', peerGroupController);

peerGroupController.$inject = [
			'$scope', 
			'$rootScope',
			'ActApiService',
			'$stateParams'
		];
		
		function peerGroupController(
				$scope, 
				$rootScope,
				ActApiService,
				$stateParams) {
			 var todayDt = new Date();
			 var lastYr = new Date();
		 		lastYr = lastYr.setFullYear(lastYr.getFullYear() - 1);
		 		
			var lastSixYrs = new Date();
				lastSixYrs = lastSixYrs.setFullYear(lastSixYrs.getFullYear() - 6);
			 
			 var yesterdaydt = new Date();
			 	yesterdaydt = yesterdaydt.setDate(yesterdaydt.getDate() - 1);
			 	
		 	 var lastMonth = new Date();
		 	 	lastMonth = lastMonth.setMonth(lastMonth.getMonth() - 1);
		 	 	
		 	 var lastThirteenMonths = new Date();
		 	 	lastThirteenMonths = lastThirteenMonths.setMonth(lastThirteenMonths.getMonth() - 13);
		 	 	
			 var lastYrFromDate = new Date(lastYr);
			 var fromMonth = String(lastYrFromDate.getMonth() + 1);
			 var fromDay = String(lastYrFromDate.getDate());
			 var toMonth = String(todayDt.getMonth() + 1);
			 var toDay = String(todayDt.getDate());
			 
			 if (fromMonth.length < 2) {fromMonth = '0' + fromMonth;}
			 if (fromDay.length < 2) {fromDay = '0' + fromDay;}
			 if (toMonth.length < 2) {toMonth = '0' + toMonth;}
			 if (toDay.length < 2) {toDay = '0' + toDay;}
			 
			 var fromDate = lastYrFromDate.getFullYear() + '-' + fromMonth + '-' + fromDay;
			 var toDate = todayDt.getFullYear() + '-' + toMonth + '-' + toDay;
			 
		$scope.token = $rootScope.ehubObject.token;
		$scope.case_id = $stateParams.caseId;
		$scope.alertsPreloader = false;
		$scope.relatedCompaniesPreloader = false;
		$scope.actualPeergroupData;/*jshint ignore : line */
		var params = {
			token: $scope.token,
			caseId:$scope.case_id
		};
		var count = 100;/*jshint ignore : line */
		if($scope.case_id != undefined && $scope.case_id != null && $scope.case_id != ''){
			getCaseDetails();
		}
		
		/*
	     * @purpose: get related case details
	     * @created: 04 oct 2017
	     * @params: null
	     * @return: null
	     * @author: swathi
	    */
		
		function getCaseDetails(){
			console.log("here");
			$scope.alertsPreloader = true;
			$scope.relatedCompaniesPreloader = true;
			ActApiService.getCaseDetails(params).then(function successCallback(response) {
				if(response.data.name){
					getProfileInfo(response.data.name);
				}else{
					 getRadarThreatAttackType(); 
					 getTableData();
//				$scope.alertsPreloader = false;
				}
			}, function() {
				 getRadarThreatAttackType(); 
				 getTableData();
//				$scope.alertsPreloader = false;
			});
		}
		/*
	     * @purpose: funciton to get profile info
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		function getProfileInfo(name){
			ActApiService.getIdentifier(name).then(function(response) {
	            if(response.data.hits.length > 0) {
	                getProfile(response.data.hits[0]['@identifier']);	               
	            } else {
	            	 getRadarThreatAttackType(); 
	            	 getTableData();
//	            	$scope.alertsPreloader = false;
	            }
	        }, function() {
	        	 getRadarThreatAttackType(); 
	        	 getTableData();
//	        	$scope.alertsPreloader = false;
	        });
		}
		var radarParams = {
			"country-code":{
			 	"terms_set":[""],
			 	"minimum_should_match":1
			 },
			 "industry":{
				 "terms_set":[""],
				 "minimum_should_match":1
			 },
			 "technology":{
				 "terms_set":[""],
				 "minimum_should_match":1 
			 }
		 };
		 $scope.checkRadarParams = {
			'industry': false,
			'technology': false,
			'country-code': false
		 };
		 function getProfile(searchText){	
				 if(searchText){
				 ActApiService.getNews( searchText).then(function(response){
					 var code;
					 if(response.data.basic['lei:legalForm'] && response.data.basic['lei:legalForm'].code){
//						 radarParams['legl-type']['terms_set'] = response.data.basic['lei:legalForm'].code;
					 }
					 if(response.data.technologies && response.data.technologies.length > 0){
						 var keepGoing = true;
						 angular.forEach(response.data.technologies, function(v){
							 if(v['bst:category'] && keepGoing){
								 radarParams.technology.terms_set = v['bst:category'];
								 keepGoing = false;
								 $scope.checkRadarParams.technology = true;
								 $scope.peerTechnology = true;
							 }
						 });
					 }
					 if(response.data.basic.isDomiciledIn){
						 radarParams['country-code'].terms_set = [response.data.basic.isDomiciledIn];
						 $scope.checkRadarParams['country-code'] = true;
						 $scope.peerLocation = true;
					 }
//					 if(response.data.basic['bst:businessClassifier'] && response.data.basic['bst:businessClassifier'][0].code){
//						 var code = response.data.basic['bst:businessClassifier'][0].code.charAt(0);
//						 radarParams['industry']['terms_set'] = [code];
					 if(response.data.basic['bst:businessClassifier'] && response.data.basic['bst:businessClassifier'].length > 0){ 
							 var keepGoingCode = true;
							 angular.forEach(response.data.basic['bst:businessClassifier'], function(v){
								 if(v.standard && v.standard == 'ISIC' && keepGoingCode){
									 code = v.code.charAt(0);
									 radarParams.industry.terms_set = [code];
									 keepGoingCode = false;
									 $scope.checkRadarParams.industry = true;
									 $scope.peerIndustry = true;
								 }
							 });
							 if(!isNaN(code)){
								 ActApiService.getStandardCodeForIndustry(response.data.basic['bst:businessClassifier'][0].code).then(function(resp){
									var responseCode = resp.data.code.charAt(0);
									radarParams.industry.terms_set = [responseCode];
									$scope.checkRadarParams.industry = true;
									$scope.peerIndustry = true;
									getRadarThreatAttackType(radarParams);	
									getTableData();
								}, function(){
									getRadarThreatAttackType();		
									getTableData();
								});
							 }else{
								getRadarThreatAttackType(radarParams);	
								getTableData();
							 }
							
						 }else{
							 getRadarThreatAttackType(radarParams);		
							 getTableData();
						 }					
					
					 }, function(){					
						 getRadarThreatAttackType();
						 getTableData();
						
					 });
				 }else{
					 getRadarThreatAttackType(); 
					 getTableData();
				 }
			 }
			var threatsRadarData = [], attacksData = [],attacksDataForCompany = [];
			$scope.threatsRadarList = [];
			$scope.peerIndustry = false;
			$scope.peerTechnology = false;
			$scope.peerLocation = false;
			$scope.peerLegalType = false;
			
			/*
		     * @purpose: get Peer Options
		     * @created: 23rd august 2018
		     * @author: swathi
		     */
			$scope.getPeerOptions = function(){
				var finalRadarParams = {};
				var obj = {
					'industry': $scope.peerIndustry,
					'technology': $scope.peerTechnology,
					'country-code': $scope.peerLocation,
					'legal-type': $scope.peerLegalType
				};		
				angular.forEach(radarParams, function(v, k){
					if(v.terms_set && v.terms_set.length > 0 && obj[k] == true){
						finalRadarParams[k] = v;
					}
				});
				getRadarThreatAttackType(finalRadarParams);
				getTableData();
			};
			/*
		     * @purpose: plot peer group without attributes
		     * @created: 23rd august 2018
		     * @params: params(object)
		     * @author: swathi
		     */
			function getRadarThreatAttackType(params){
				threatsRadarData = [], attacksData = [], attacksDataForCompany = [];/*jshint ignore : line */
				$scope.threatsRadarList = [];
				$scope.alertsPreloader = true;
				var isAttributesExist = false;
				 var data = {
					"query": {
						'date-from': fromDate,
						'date-to': toDate
					}, 
					"agg": {
						"key": "attack-type"
					}
				 };
				 var data1 = {
					"query": {
						'date-from': fromDate,
						'date-to': toDate
					}, 
					"agg": {
						"key": "attack-type"
					}
				 };
				 if(params){
					 angular.forEach(params, function(v, k){
						 if(v.terms_set && v.terms_set[0].length > 0){
							 isAttributesExist = true;
							 data1.query[k] = v;
						 }
					 });
				 }
				 ActApiService.getThreatAggregates(data).then(function(response) {
					 if(response.data.aggregations[0].buckets.length > 0){
						 attacksData.push(response.data.aggregations[0].buckets);
						 attacksData = attacksData[0];
					 }
			       
					 if(attacksData.length > 0){
						 var totIncidentsCount = d3.sum(attacksData, function(d){
							return d.doc_count;
						 });
						//map data for chart
						attacksData.map(function(d){
							d.axis = d.key;
							d.value = d.doc_count;
							d.group = "global";
							d.totIncidentsCount = totIncidentsCount;
							d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
						});
						//push global data
						if(attacksData && attacksData.length>0)
						 	{threatsRadarData.push(attacksData);}
						if(!isAttributesExist){
							$scope.threatsRadarList = threatsRadarData;
							$scope.alertsPreloader = false;
							plotRadarChart(threatsRadarData);
						}
						else{
							plotPeerGroupByAttributes(data1);
						}
					}else{
						$scope.alertsPreloader = false;
						$("#global_company_radar_chart").empty();
						$("#peerGroupanalysisForRadar").empty();
					}
			     }, function() {
			    	 $scope.alertsPreloader = false;
			    	 $("#global_company_radar_chart").empty();
					$("#peerGroupanalysisForRadar").empty();
			     });
			}
			/*
		     * @purpose: plot peer group by industry/country/technology attributes
		     * @created: 23rd august 2018
		     * @params: data(object)
		     * @author: swathi
		     */
			function plotPeerGroupByAttributes(data){
				ActApiService.getThreatAggregates(data).then(function(response) {
					 if(response.data.aggregations[0].buckets.length > 0){
						 attacksDataForCompany.push(response.data.aggregations[0].buckets);
						 attacksDataForCompany = attacksDataForCompany[0];
					 }
					 if(attacksDataForCompany && attacksDataForCompany.length>0){
						 var totIncidentsCount = d3.sum(attacksDataForCompany, function(d){
							return d.doc_count;
						 });
						//map data for chart for industry 
						attacksDataForCompany.map(function(d){
							d.axis = d.key;
							d.value = d.doc_count;
							d.group = "company";
							d.totIncidentsCount = totIncidentsCount;
							d.incidentsValPercentage = ((d.doc_count / totIncidentsCount) * 100).toFixed(2);
						});
						threatsRadarData.push(attacksDataForCompany);
						$scope.alertsPreloader = false;	
					     // call funciton to plot radar chart
						$scope.threatsRadarList = threatsRadarData;
						plotRadarChart(threatsRadarData);
		 			}else{
			 			$scope.threatsRadarList = threatsRadarData;
						plotRadarChart(threatsRadarData);
						$scope.alertsPreloader = false;
			 		}
			     }, function() {
			    	 // call funciton to plot radar chart
			    	 $scope.threatsRadarList = threatsRadarData;
			    	 plotRadarChart(threatsRadarData);
			    	 $scope.alertsPreloader = false;
			     });
			}

		/*--------------------------------------------------radar chart attack Types Ends --------------------------------------------------------------*/

		/*
	     * @purpose: funciton to plot radar
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		function plotRadarChart(data){
			$scope.actualPeergroupData = data;
			if(!data || data.length==0){
				return false;
			}
			var d = jQuery.extend(true, [],data);
			  //Options for the Radar chart, other than default
		        var options = {
		            container: "#global_company_radar_chart",
		            width:235,
		            height:210,
		            ExtraWidthX: 230,
		            circleRadius:1.5,
		            strokeWidth:0.1,
		            opacityArea:0.5,
		            marginTop:30,
		            marginBottom:30,
		            iswhiteBackground:true,
		            colorObj:{"global":"#BA4441","company":"#BB9800"}
		        };
			  //Call function to draw the Radar chart
			  //Will expect that data is in %'s
			  RadarChart("#global_company_radar_chart", d, options);
		}
		
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('alertsWidget')){
				var heightResp = document.getElementById('alertsWidget').clientHeight ? document.getElementById('alertsWidget').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#alertsWidget").find("tbody").css("height",heightResp);
			}
		});

		
		/*--------------------------------------------------table handling starts here--------------------------------------------------------------*/

		/*
	     * @purpose: funciton to get data for table
	     * @created: 16 Aug 2018
	     * @params: null
	     * @return: null
	     * @author: Prasanthi
	    */
		function getTableData(page){
			$scope.relatedCompaniesPreloader = true;
			var data =	{
					"actor-a-type": "organization",
					"class" : "Cyber Security",
					"size" : 5,
					"offset":page?page:0
					};
			if($scope.peerTechnology && radarParams.technology.terms_set[0]){
				data.technology = radarParams.technology.terms_set[0];
			}
			if($scope.peerIndustry && radarParams.industry.terms_set[0]){
				data.industry = radarParams.industry.terms_set[0];
			}
			if($scope.peerLocation && radarParams["country-code"].terms_set[0]){
				data['country-code'] = radarParams["country-code"].terms_set[0];
			}		
			ActApiService.getRelatedCompanies(data).then(function(response) {				
				$scope.relatedCompaniesPreloader = false;
				if(response && response.data &&  response.data.peerDataDto && response.data.peerDataDto.length>0){
					$scope.relatedCompaniesData = response.data.peerDataDto;
					$scope.avgScore = ((d3.sum(response.data.peerDataDto,function(d){
						return d.riskScore;
					}))/$scope.relatedCompaniesData.length);
				}else{
					//data not found
				}
				
	        }, function() {
	        	$scope.relatedCompaniesPreloader = false;
	        	//data not found
	        });
			
		}
		$scope.$on('gridster-draggable-changed', function() {
			if(document.getElementById('relatedCompaniesDiv')){
				var heightResp = document.getElementById('relatedCompaniesDiv').clientHeight ? document.getElementById('relatedCompaniesDiv').clientHeight : 300;
				if(heightResp > 310){
					heightResp = 530;
				}else{
					heightResp = 250;
				}
				$("#relatedCompaniesDiv").find("tbody").css("height",heightResp);
			}
		});
		
		
}