'use strict';
angular.module('ehubApp')
	.controller('auditTrailController', auditTrailController);
auditTrailController.$inject = [
	'$scope',
	'$rootScope',
	'$timeout',
	'HostPathService',
	'ActApiService'
];

function auditTrailController(
	$scope,
	$rootScope,
	$timeout,
	HostPathService,
	ActApiService) { 
	$scope.datefilterSelect = "Days";
	$scope.itemActive = '';
	$scope.headerspinner = true;
	$scope.footerspinner = true;
	$scope.PieGraphisloading = false;
	var getlist_scroll = true;
	var pagenumber = 1;
	var searchlog_pagenumber = 1;
	$scope.events = [];
	var activity_data = [];
	var caseEvents = [];
	var mainEvent = [];
	$scope.byActivityType = "All";
	$scope.activityTypeLists = ["All"];
	var token = {
		token: $rootScope.ehubObject.token
	};
	/*------------------------------------get user profile----------------------------------*/
	ActApiService.getUserProfile(token).then(function (response) {
		var r = response.data;
		$timeout(function () {
			$scope.userDetails = r;
		}, 0);
	}, function () {});
	/*------------------------------------get logs list----------------------------------*/
	var max_pagennumber = 0;
	$scope.getLogList = function (pagenumber,type) {
		searchlog_pagenumber = 1;
		var data = {};
		data.pageNumber = pagenumber;
		data.recordsPerPage = 100;
		if(type && type !== 'All'){
			data.type = type;
		}
		ActApiService.getAuditLogs(token, data).then(function (response) {
			var events = response.data.result;
			var listTypes = response.data.types;
			$scope.activityTypeLists = _.uniq($scope.activityTypeLists.concat(listTypes));
			max_pagennumber = response.data.paginationInformation.startIndex;
			$timeout(function () {
				caseEvents = events;
				mainEvent = mainEvent.concat(events);
				$scope.events = events;
				//filtereventData(events);
				$scope.footerspinner = false;
				$scope.headerspinner = false;
				plotGraph();
			}, 0);
		}, function () {});
	};
	$scope.getLogList(1,$scope.byActivityType);
	$scope.loglist_scroll = function () {
		if (getlist_scroll) {
			if (max_pagennumber > 1) {
				$scope.footerspinner = true;
				pagenumber = pagenumber + 1;
				$scope.getLogList(pagenumber,$scope.byActivityType);
			}
		} else {
			if (searchlog_pagenumber > 1) {
				$scope.footerspinner = true;
				$scope.searchLogs(searchlog_pagenumber);
				searchlog_pagenumber = searchlog_pagenumber + 1;
			}
		}
	};
	var spinnerforonce = true;
	/*------------------------------------search logs list----------------------------------*/
	$scope.searchLogs = function (search_pagenumber, firstCall) {
		if (firstCall) {
			searchlogEvent = {
				eventFiltered: [],
				caseFiltered: [],
				allFiltered: [],
				timeline: []
			};
		}
		if ($scope.searchTxt && $scope.searchTxt.length > 2) {
			pagenumber = 1;
			getlist_scroll = false;
			if (spinnerforonce) {
				$scope.headerspinner = true;
				spinnerforonce = false;
			}
			var data = {
				'keyword': $scope.searchTxt,
				'pageNumber': search_pagenumber
			};
			ActApiService.searchAuditLogs(token, data).then(function (response) {
				var events = response.data.result;
				searchlog_pagenumber = response.data.paginationInformation.startIndex;
				$timeout(function () {
					caseEvents = events;
					$scope.events = events;
					//filtereventData(events);
					$scope.footerspinner = false;
					$scope.headerspinner = false;
				}, 0);
			}, function () {
				$scope.footerspinner = false;
				$scope.headerspinner = false;
				searchlog_pagenumber = 1;
				getlist_scroll = false;
			});
		} else if (!$scope.searchTxt) {
			$scope.getLogList(1,$scope.byActivityType);
			searchlogEvent.timeline = timestamp($scope.byActivityType);
			//ActivityFilteredData();
			getlist_scroll = true;
		} else {
			HostPathService.FlashErrorMessage('Mininmum 3 letters', '');
			$scope.searchTxt = '';
			getlist_scroll = true;
			$scope.getLogList(1,$scope.byActivityType);
			searchlogEvent.timeline = timestamp($scope.byActivityType);
			//ActivityFilteredData();
		}
		plotGraph();
	};
	/*------------------------------------filter by activity----------------------------------*/
	$scope.filterByActivity = function () {
		$scope.searchTxt = "";
		$scope.getLogList(1,$scope.byActivityType);
		//ActivityFilteredData();
	};
	//--------------------------------------------------------------
	/**
	 * Function to get final array and to call functions for charts
	 */
	$scope.timeline_date = [];
	$scope.timesline = {
		day: [],
		month: [],
		year: []
	};

	function timestamp(events) {
		$scope.timesline = {
			day: [],
			month: [],
			year: []
		};
		for (var i = 0; i < events.length; i++) {
			var date = new Date(events[i].timestamp);
			var split = date.toString().split(' ');
			if (new Date(events[i].timestamp) > (new Date().getTime() - 654524560)) {
				if (($scope.timesline.day.indexOf(split[0]) === -1)) {
					$scope.timesline.day.push(split[0]);
					events[i].day = split[0];
				}
			}
			if (($scope.timesline.year.indexOf(split[3]) === -1)) {
				$scope.timesline.year.push(split[3]);
				events[i].year = split[3];
			}
			if (($scope.timesline.month.indexOf(split[1]) === -1)) {
				$scope.timesline.month.push(split[1]);
				events[i].month = split[1];
			}
		}
		return $scope.timesline;
	}
	/* filter list with Entity Data */
	function filterEntitylist(events, type) {
		var entity = [];
		for (var i = 0; i < events.length; i++) {
			if (events[i].type === type) {
				if (type === 'ENTITY' || type === 'CASE') {
					entity.push(events[i]);
				} else {
					entity.push(events[i]);
				}
			}
		}
		return entity;
	}
	var shouldempty = '';
	$scope.query = '';
	$scope.filterbytimeline = function (events, filter) {
		$scope.query = filter;
		var qu = '#' + $scope.query;
		$('#auditTrailActivity').mCustomScrollbar("scrollTo", (qu));
		if (shouldempty !== filter) {
			$scope.itemActive = filter;
			shouldempty = filter;
		} else {
			$scope.itemActive = '';
			shouldempty = '';
		}
	}; //filterbytimeline function ends
	/*------------------------------------filter entities with top date/day filter----------------------------------*/
	/*------------------------------------Pie code starts here----------------------------------*/
	$scope.InitializeandPlotPie = function (data, id, colorsObj, colorsArr) {
		var newData = [];
		var keys = [];
		angular.forEach(data, function (d) {
			if (d.value && (d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(), keys) == -1)) {
				newData.push(d);
				keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
			}
		});
		var maxval = d3.max(data, function (d) {
			return d.value;
		});
		var sum = d3.sum(data, function (d) {
			return d.value;
		});
		var colors;
		if (colorsArr) {
			colors = colorsArr;
		} else {
			colors = ["#5d96c8", "#b753cd", "#69ca6b", "#69ca6b", "#c1bd4f", "#db3f8d", "#669900", "#334433"];
		}
		var options = {
			container: "#" + id,
			data: newData,
			height: 160,
			colors: colors,
			colorsObj: colorsObj,
			islegends: true,
			islegendleft: true,
			legendwidth: 30,
			istxt: sum == 0 ? 0 : parseInt((maxval / sum) * 100) + "%",
			legendmargintop: 30
		};
		setTimeout(function () {
			new reusablePie(options);
		});
	};
	/*---------------------------------------------- Pie chart Ends---------------------------------------------*/
	/*---------------------------------------------- Line chart Starts---------------------------------------------*/
	function plotLineChart(lineChartData, id, lineWidth, height, xDomain,isXDomainConsider) {
		var lineData = [];
		lineData = [{
			"key": "",
			"values": lineChartData
		}];
		xDomain = lineChartData.length > 0 ? xDomain : '';
		var options = {
			container: id,
			height: height != undefined ? height : '',
			width: lineWidth != undefined ? lineWidth : '',
			axisX: true,
			axisY: true,
			gridy: true,
			xDomain: isXDomainConsider ? xDomain:null,
			tickfomat: "days",
			yTickFormat: "string",
			data: lineData,
			marginRight: 20,
			gridcolor: "#2e424b",
			actualData: lineData[0].values,
			ystartsFromZero: true,
			labelRotate: -65
		};
		new InitializeandPlotLine(options);
	}
	/*---------------------------------------------- Line chart Ends---------------------------------------------*/
	/*---------------------------------------------- filter items starts---------------------------------------------*/
	//function to get data for filtered values
	$scope.data = [];
	var getlogEvent = {
		eventFiltered: [],
		caseFiltered: [],
		allFiltered: [],
		otherFiltered: [],
		timeline: []
	};
	var searchlogEvent = {
		eventFiltered: [],
		caseFiltered: [],
		allFiltered: [],
		otherFiltered: [],
		timeline: []
	};

	function filtereventData(mainevents) {
		if ($scope.searchTxt) {
			searchlogEvent.eventFiltered = searchlogEvent.eventFiltered.concat(filterEntitylist(mainevents, 'ENTITY'));
			searchlogEvent.caseFiltered = searchlogEvent.caseFiltered.concat(filterEntitylist(mainevents, 'CASE'));
			searchlogEvent.allFiltered = searchlogEvent.allFiltered.concat(mainevents);
			var otherarray = jQuery.extend(searchlogEvent.allFiltered, [], true);
			searchlogEvent.otherFiltered = otherarray.filter(function (val) {
				if (val.type !== 'ENTITY' && val.type !== 'CASE') {
					val.type = "OTHER";
					return val;
				}
			});
		} else {
			getlogEvent.eventFiltered = getlogEvent.eventFiltered.concat(filterEntitylist(mainevents, 'ENTITY'));
			getlogEvent.caseFiltered = getlogEvent.caseFiltered.concat(filterEntitylist(mainevents, 'CASE'));
			getlogEvent.allFiltered = getlogEvent.allFiltered.concat(mainevents);
			var otherarray = jQuery.extend(getlogEvent.allFiltered, [], true);
			getlogEvent.otherFiltered = otherarray.filter(function (val) {
				if (val.type !== 'ENTITY' && val.type !== 'CASE') {
					val.type = "OTHER";
					return val;
				}
			});
		}
		ActivityFilteredData();
	}

	function ActivityFilteredData() {
		$scope.events = [];
		$scope.headerspinner = true;
		if ($scope.searchTxt) {
			if ($scope.byActivityType === "All") {
				$scope.events = searchlogEvent.allFiltered;
				searchlogEvent.timeline = timestamp(searchlogEvent.allFiltered);
			} else if ($scope.byActivityType === "Case") {
				$scope.events = searchlogEvent.caseFiltered;
				searchlogEvent.timeline = timestamp(searchlogEvent.caseFiltered);
			} else if ($scope.byActivityType === "Entity") {
				$scope.events = searchlogEvent.eventFiltered;
				searchlogEvent.timeline = timestamp(searchlogEvent.eventFiltered);
			} else {
				$scope.events = searchlogEvent.otherFiltered;
				searchlogEvent.timeline = timestamp(searchlogEvent.otherFiltered);
			}
		} else {
			if ($scope.byActivityType === "All") {
				$scope.events = getlogEvent.allFiltered;
				getlogEvent.timeline = timestamp(getlogEvent.allFiltered);
			} else if ($scope.byActivityType === "Case") {
				$scope.events = getlogEvent.caseFiltered;
				getlogEvent.timeline = timestamp(getlogEvent.caseFiltered);
			} else if ($scope.byActivityType === "Entity") {
				$scope.events = getlogEvent.eventFiltered;
				getlogEvent.timeline = timestamp(getlogEvent.eventFiltered);
			} else {
				$scope.events = getlogEvent.otherFiltered;
				getlogEvent.timeline = timestamp(getlogEvent.otherFiltered);
			}
		}
		$scope.headerspinner = false;
		plotGraph();
	}

	function plotGraph() {
		var xDomain;
		$scope.events.sort(function (a, b) {
			return b.timestamp - a.timestamp;
		});
		var nested_by_date = d3.nest().key(function (d) {
			return d3.time.day(new Date(d.timestamp));
		}).rollup(function (d) {
			return d.length;
		}).entries($scope.events);
		if (nested_by_date.length < 2) {
			var day_date = nested_by_date;
			//do for a day
			var nested_by_hr = d3.nest().key(function (d) {
				return d3.time.hour(new Date(d.timestamp));
			}).rollup(function (d) {
				return d.length;
			}).entries($scope.events);
			if(nested_by_hr.length < 2){
				nested_by_hr = d3.nest().key(function (d) {
					return  d3.time.minute(new Date(d.timestamp));
				}).rollup(function (d) {
					return d.length;
				}).entries($scope.events);
			}
			nested_by_date = nested_by_hr;
			//do for a day
			// xDomain = [(Date.now() - 86400000), Date.now()];		
			if(day_date.length){
				var min_xdate = new Date(day_date[0].key);
				xDomain = [(min_xdate), Date.now()];
			}	
		} else {
			xDomain = [(Date.now() - 604800000), Date.now()];
		}
		//create line chart data
		var activityLineData = [];
		var isXDomainConsider= true;
		angular.forEach(nested_by_date, function (d) {
			if (d.key && new Date(d.key).getTime() > (Date.now() - 604800000)) { //604800000 is a week
				activityLineData.push({
					"date": new Date(d.key),
					"x": new Date(d.key),
					"value": d.value,
					"time": new Date(d.key)
				});
			}else{
				// if the data is more than a week
				activityLineData.push({
					"date": new Date(d.key),
					"x": new Date(d.key),
					"value": d.value,
					"time": new Date(d.key)
				});
				isXDomainConsider = false;
			}
		});
		$scope.LineGraphisloading = activityLineData.length > 0 ? false : true;
		plotLineChart(activityLineData, "#activityGraphChart", $("#activityGraphChart").width(), '200', xDomain,isXDomainConsider);
		//create pie chart data
		var piegraph_data = [];
		activity_data = $scope.events;
		var new_nestedData = d3.nest()
			.key(function (d) {
				return d.type;
			})
			.entries(activity_data);
		for (var i = 0; i < new_nestedData.length; i++) {
			if (new_nestedData[i].key !== '' && new_nestedData[i].key !== 'null' && new_nestedData[i].key !== 'undefined') {
				piegraph_data.push({
					key: new_nestedData[i].key,
					value: new_nestedData[i].values.length
				});
			}
		}
		$scope.PieGraphisloading = new_nestedData.length > 0 ? false : true;
		$scope.InitializeandPlotPie(piegraph_data, 'activityRatioChart');
		var nested_data = d3.nest().key(function (d) {
			if (d.name && (d.title != 'Logged in')) {
				return d.name;
			}
		}).rollup(function (events) {
			return events.length;
		}).entries($scope.events);
		var deleteindex = _.findIndex(nested_data, ['key', 'undefined']);
		if (deleteindex !== -1) {
			var nested_data1 = nested_data.splice(deleteindex, 1); /*jshint ignore : line */
		}
		var ratioData = jQuery.extend(true, [], nested_data);
		var max_occurances = d3.max(nested_data, function (d) {
			if (d.key) {
				return d.value;
			}
		});
		ratioData.sort(function (a, b) {
			return b.value - a.value;
		});
		//consider top 3
		$scope.ratioData = ratioData.slice(0, 3);
		$scope.ratioData.map(function (d) {
			d.value = parseInt((d.value / max_occurances) * 100);
			return d.value;
		});
	}
}