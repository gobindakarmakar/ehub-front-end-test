'use strict';
elementApp
	    .constant('ActTopLocationsConst',{
	    	zoomOptions: {
			   minZoom: 1,
			   zoom: 1
	    	}
	     })
	    .constant('ActGridsterConst',{
	   		isMobile: false,
	   		pushing: true,
	   		floating: true,
	   		swapping: true,
	   		rowHeight: 320,
	   		maxSizeY: 2,
	   		resizable: {
	   			enabled: true
	   		},
	   		draggable: {
	   			enabled: true,
	   			handle: '.widget-header'	   
	   		},
	   		margin: [10,10]
	   })
	   .constant('ActHotTopicsConst',{
		   	speed : 0.1,
			slower : 0.1,
			height : 230,
			timer : 5,
			fontMultiplier : 15,
			isheader : false,
			hoverStyle : {
				border : 'none',
				color : '#0b2e6f'
			},
			mouseOutStyle : {
				border : '',
				color : ''
			},
			colors: ["#3990CF", "#1DBF6A", "#E1C433", "#E61874", "#E58B19"],
			ballSize: 50,
			colorObj: {
				persons      : '#9C6CC4',
				companies     : '#50B0A2',
				locations    : '#AFA960',
				phrases 	  : '#C2776A',
				organisation: '#5C94CE',
				industries  : '#5C94CE',
				country     : '#61B45F'
			}
			
	   })
	   .constant('ActEntitiesInFocusConst',{
		   chartHeight: 300
	   }) 
	   .constant('ActRiskRatioConst',{
		   height: 300,
		   highRiskVal: 60,
		   mediumRiskVal: 40
	   })
	   .constant('AssociatedEntitiesConst', {
		   height: '250',
		   width: '400',
		   isheader: false,
		   ytext: "Similarity Percent",
		   xticks: 5,
		   margin: {top: 20, right: 0, bottom: 30, left: 50}
	   }).constant('isicCodeConstants', {
			"A": "AGRICULTURE",
			"B": "MINING",
			"C": "MANUFACTURING",
			"D": "ELECTRICITY",
			"E": "WATER SUPPLY",
			"F": "CONSTRUCTION",
			"G": "WHOLESALE",
			"H": "TRANSPORTATION",
			"I": "ACCOMODATION",
			"J": "INFORMATION",
			"K": "FINANCIAL",
			"L": "REAL ESTATE",
			"M": "PROFESSIONAL",
			"N": "ADMINISTRATIVE",
			"O": "PUBLIC ADMINISTRATION",
			"P": "EDUCATION",
			"Q": "HUMAN HEALTH",
			"R": "ARTS",
			"S": "OTHER SERVICE",
			"T": "ACTIVITIES OF HOUSEHOLDS",
			"U": "ACTIVITIES OF EXTRATERRITORIAL",
			"V": "CHEMICALS",
			"W": "",
			"X": "",
			"Y": "",
			"Z": ""
	});