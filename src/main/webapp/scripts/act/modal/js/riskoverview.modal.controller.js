'use strict';
angular.module('ehubApp')
	   .controller('RiskOverViewController', riskOverViewController);

        riskOverViewController.$inject = [
			'$scope',
			'$http',
			'$timeout',
			'text',
			'$rootScope',
			'ActApiService',
			'$uibModalInstance'
		];
		
		function riskOverViewController(
				$scope,
				$http,
				$timeout,
				text,
				$rootScope,
				ActApiService,
				$uibModalInstance) {
			
			
			/*Initializing scope variables*/ 
			$scope.riskOverView = {};
			$scope.riskOverviewLoader = true;
			$scope.text = text;
			$scope.closeOverviewModal = function(){
				$uibModalInstance.close('close');
			};
			$scope.showHideMenu = function(key){
				var _this =$("#custom-check-text"+key);
			  if ($(_this).parents(".pulseVisualizerList-wrapper-li").hasClass('active')) {
		          $(_this).parents(".pulseVisualizerList-wrapper-li").removeClass('active');
		          $(_this).parents('.toggle').next('.treeview-menu').slideUp();
		      } else {
		          $(".toggleBarMenu li").removeClass("active").find(".treeview-menu").slideUp();
		          $(_this).parents(".pulseVisualizerList-wrapper-li").addClass('active');
		          $(_this).parents('.toggle').next('.treeview-menu').slideDown();
		      }
			};
			$scope.entities=[];
			$scope.entitiesDetailed=[];

			var txt =$scope.text.article_body;
			var header = ($scope.text.headline);
			var header1 =(header.split(",")[0].split("main")[1]).split('"').join("");
//			var header2 =(header.split(",")[1].split("kicker")[1]).split('"').join("").replace("}","")
			$scope.articelHeader =header1;
			 
			 ActApiService.loadMarketIntelligenceENT(txt).then(function(response){
				 $timeout(function(){
			    		$scope.riskOverviewLoader = false;
			    	},0);
			    	
			      var entities =[];
			      var entitiesUnique=[];
			        angular.forEach(response.data.entities, function (v) {
			        	  if($.inArray(v.type.toLowerCase(),$scope.entities) == -1){
			              	$scope.entities.push(v.type.toLowerCase());
			              	entities[v.type.toLowerCase()]=[];
			              	entitiesUnique[v.type.toLowerCase()]={};
			              }
			        	 
			        	  var currenttxt = txt.slice(v.start, v.end);
			        	  var currentobj ={
			        			  currenttxt:currenttxt,
			        			  start:v.start,
			        			  end:v.end,
			        			  type:v.type.toLowerCase()
			        	  };
			        	  if(!entitiesUnique[v.type.toLowerCase()][currenttxt]){
			        		  entitiesUnique[v.type.toLowerCase()][currenttxt]=[]; 
			        	  }
			        	  entities[v.type.toLowerCase()].push(currentobj);
			        	  entitiesUnique[v.type.toLowerCase()][currenttxt].push(currentobj);
			        	  
			        });
			      var entitiesDetailed =[];
			      $.each(d3.keys(entities),function(i,d){
			   	   entitiesDetailed.push({
			   		   key:d,
			   		   values:entitiesUnique[d]
			   	   });
			      });
			        $scope.loadEntity(txt, response.data.entities,$scope.entities);
			        $timeout(function(){
			        	$scope.entitiesDetailed = entitiesDetailed;
			        	 $('.marketModal-pulse-modal-wrapper .modal-body .pulseVisualizerList-wrapper-li')
			     		.mCustomScrollbar({
			     			axis : "y"
			     		});
			        },0);
			       
			 },function(){
				 $scope.riskOverviewLoader = false;
			 });
			 // --------------------------------------------------------------------------
		    /**
			 * Function to show entity visualization
			 */     
		    $scope.loadEntity = function(text, entities,entityTypes){
		        // Your API
		          var api = '';  
		          // Init displaCY ENT
		            var displacy = new displaCyENT(api, { 
		                container: '#marketPulseText'   
		            });
		            var text = text;  
		            var model = 'en';/*jshint ignore : line */
		// Entities to visualise
		            var ents = entityTypes;
		            var spans = entities;
		            // Render text
		            displacy.render(text, spans, ents);
		         
		        };
		   //---------------------------------------------------------------------------
		   /**
		    * Function to filter entity visualizer
		    */
		       $scope.filterEntities =function(entity){
		    	   if(entity){
		    		   $(".childCheck_"+entity).prop("checked",$("#parentCheck_"+entity).prop("checked"));
		    	   }
//		    	   $(".marketpulsechildrenCheck").prop("checked",false);
		    	   var entityTypes=[];
		    	   $(".marketpulseparentCheck:checked").each(function(){
//		    		   $.each($(this).siblings().find("li"),function(){
//		    			   $(this).find("input:checkbox").prop("checked",true)
//		    			   });
		    		   entityTypes.push($(this).val());
		    	   });   
		    	   var entities=[];
		    	   $(".marketpulsechildrenCheck:checked").each(function(){
		    		   var curnt = JSON.parse($(this).val());  
		    		  angular.forEach(curnt,function(val){
		    			  if($.inArray(val.type,entityTypes) ==-1){
		      		  		entityTypes.push(val.type);
		      		  	}
		    			  entities.push({
		    				  start:val.start,
		    				  type:val.type,
		    				  end:val.end
		    			  });   
		    		   });
		    			   		  
		    	   });
		    	   entities.sort(function(a,b){
		    		   return a.start-b.start;
		    	   });
		    	   
		    	     $scope.loadEntity(txt, entities,entityTypes);
//		    	     displacy.render(text, entities, entityTypes);
		    	  
		       };
			
		}
