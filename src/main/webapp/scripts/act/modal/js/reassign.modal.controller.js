'use strict';
angular.module('ehubApp')
	   .controller('ReassignCaseController', reassignCaseController);

		reassignCaseController.$inject = [
			'$scope',
			'$rootScope',
			'ActApiService',
			'$stateParams',
			'$uibModalInstance',
			'HostPathService'
		];
		
		function reassignCaseController(
				$scope,
				$rootScope,
				ActApiService,
				$stateParams,
				$uibModalInstance,
				HostPathService) {
			
			
			/*Initializing scope variables*/ 
			$scope.reassign = {
					userList: [],
					params:{
						token: $rootScope.ehubObject.token
					},
					reassignCase:reassignCase,
					closeReassign:closeReassign,
					getUserOrderBy:getUserOrderBy
			};
			$scope.token = $rootScope.ehubObject.token;
			/*
		     * @purpose: Get User Lists
		     * @created: 10 oct 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function getUserLists(){
				ActApiService.getUserList($scope.reassign.params).then(function(response){
					$scope.usersLength = response.data.paginationInformation.totalResults;
					var data = {
		 				token: $scope.token,
		 				pageNumber: 1,
		 				recordsPerPage: $scope.usersLength 
			 		};
					ActApiService.getUserList(data).then(function(response){
						$scope.reassign.userList = response.data.result;
					},function(){
						
					});
				},function(){
					
				});
			}
			getUserLists();
			
			/*
		     * @purpose: Reaassign Case
		     * @created: 10 oct 2017
		     * @params: reassign(object)
		     * @return: no
		     * @author: Ankit
		    */
			function reassignCase(reassign){
				$scope.reassign.params.caseId =  $stateParams.caseId, 
				$scope.reassign.params.newUserId = reassign.reassignUser;/*jshint ignore : line */
				$scope.reassign.params.statusComment = reassign.reassignNotes;	
				$scope.reassign.params.orderBy = reassign.orderBy;
				ActApiService.reassignCase($scope.reassign.params).then(function(){
					$scope.reassign.closeReassign();
					HostPathService.FlashSuccessMessage('SUCCESSFUL REASSIGNING CASE', 'Successfully reassigned case!');
				},function(error){
					$scope.reassign.message = error.responseMessage;
					$scope.reassign.closeReassign();
    	    		HostPathService.FlashErrorMessage('ERROR REASSIGNING CASE', error.responseMessage || 'Failed to reassign the case!');
				});
			}
			
			/*
		     * @purpose: Close modal
		     * @created: 10 oct 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closeReassign(){
				$uibModalInstance.close();
			}
			
			/*
		     * @purpose: Get User list Order By
		     * @created: 10 oct 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function getUserOrderBy(order){
				if(order){
					var data = {
		 				token: $scope.token,
		 				pageNumber: 1,
		 				recordsPerPage: $scope.usersLength,
		 				orderBy: order
			 		};
					ActApiService.getUserList(data).then(function(response){
						$scope.reassign.userList = response.data.result;
					},function(){
						
					});
				}
			}
		}
