'use strict';
elementApp
	.factory('ActApiService', actApiService);

	actApiService.$inject = [
		'$http',
		'$rootScope',
		'EHUB_API',
		'$q'
	];

	function actApiService(
			$http,
			$rootScope,
			EHUB_API,
			$q){

		return {
			getCaseDetails:getCaseDetails,
			getAllCaseSummary: getAllCaseSummary,
			getCaseSummaryById: getCaseSummaryById,
			getRelatedCases: getRelatedCases,
			getRelatedCaseDetails: getRelatedCaseDetails,
			getUserList:getUserList,
			getUserProfile:getUserProfile,
			reassignCase:reassignCase,
			getRiskDataByCaseId: getRiskDataByCaseId,
			getCaseAlmiraGraph: getCaseAlmiraGraph,
			getCaseCommentNotification: getCaseCommentNotification,
			loadMarketIntelligence:loadMarketIntelligence,
			loadMarketIntelligenceENT:loadMarketIntelligenceENT,
			getAuditLogs:getAuditLogs,
			searchAuditLogs:searchAuditLogs,
			getCaseByIdAlerts: getCaseByIdAlerts,
			getCaseTimeline: getCaseTimeline,
			getAssociatedEntities: getAssociatedEntities,
			getReport: getReport,
			getGraphData:getGraphData,
			getIdentifier: getIdentifier,
			getNews: getNews,
			getThreatAggregates: getThreatAggregates,
			getStandardCodeForIndustry: getStandardCodeForIndustry,
			getRelatedCompanies:getRelatedCompanies
		};
		/*
	     * @purpose: get list of case summary
	     * @created: 26 sep 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getAllCaseSummary(params){
			var apiUrl = EHUB_API + 'investigation/caseSummary';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getAllCaseSummarySuccess)
	                .catch(getAllCaseSummaryError));

	        /* getAllCaseSummary error function */
	        function getAllCaseSummaryError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /* getAllCaseSummary success function */
	        function getAllCaseSummarySuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case summary by case Id
	     * @created: 26 sep 2017
	     * @params: token(object), caseId(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseSummaryById(token, caseId){
			var apiUrl = EHUB_API + 'workflow/es/cases/'+ caseId +'/summary';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: token
	        });
	        return(request
	                .then(getCaseSummaryByIdSuccess)
	                .catch(getCaseSummaryByIdError));

	        /* getCaseSummaryById error function */
	        function getCaseSummaryByIdError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /* getCaseSummaryById success function */
	        function getCaseSummaryByIdSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get related cases
	     * @created: 04 oct 2017
	     * @params: params(object), caseId(integer), count(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getRelatedCases(params, caseId, count){
			var apiUrl = EHUB_API + 'workflow/es/cases/' + caseId +'/similar/' + count;
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getRelatedCasesSuccess)
	                .catch(getRelatedCasesError));

	        /* getRelatedCases error function */
	        function getRelatedCasesError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getRelatedCases success function */
	        function getRelatedCasesSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get related case details
	     * @created: 04 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getRelatedCaseDetails(params){
			var apiUrl = EHUB_API + 'investigation/getRelatedCaseDetails';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getRelatedCaseDetailsSuccess)
	                .catch(getRelatedCaseDetailsError));

	        /* getRelatedCaseDetails error function */
	        function getRelatedCaseDetailsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	           /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getRelatedCaseDetails success function */
	        function getRelatedCaseDetailsSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case details
	     * @created: 07 nov 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseDetails(params){
			var apiUrl = EHUB_API + 'case/getCaseById';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getCaseDetailsSuccess)
	                .catch(getCaseDetailsError));

	        /* getCaseDetails error function */
	        function getCaseDetailsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	           /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getCaseDetails success function */
	        function getCaseDetailsSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get list of case summary
	     * @created: 04 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getAllCaseSummary(params){
			var apiUrl = EHUB_API + 'investigation/caseSummary';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getAllCaseSummarySuccess)
	                .catch(getAllCaseSummaryError));

	        /* getAllCaseSummary error function */
	        function getAllCaseSummaryError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getAllCaseSummary success function */
	        function getAllCaseSummarySuccess(response) {
	            return(response);
	        }
		}

		/*
	     * @purpose: get list of Users
	     * @created: 05 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function getUserList(params){
			var apiUrl = EHUB_API + 'user/getUserListing';
			var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getUserListSuccess)
	                .catch(getUserListError));

	        /* getlistofUsers error function */
	        function getUserListError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getlistofUsers success function */
	        function getUserListSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get list of Users
	     * @created: 05 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function getUserProfile(params){
			var apiUrl = EHUB_API + 'user/getUserProfile';
			var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getUserProfileSuccess)
	                .catch(getUserProfileError));

	        /* getlistofUsers error function */
	        function getUserProfileError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getlistofUsers success function */
	        function getUserProfileSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get risk data by case id
	     * @created: 05 oct 2017
	     * @params: params(object), caseId(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getRiskDataByCaseId(params, caseId){
			var apiUrl = EHUB_API + 'graph/getRiskDataByCase/' + caseId;
			var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
			return(request
					.then(getRiskDataByCaseIdSuccess)
               .catch(getRiskDataByCaseIdError));

       /* getRiskDataByCaseId error function */
       function getRiskDataByCaseIdError(response) {
           if (!angular.isObject(response.data) || !response.data.message) {
               return($q.reject(response.data));
           }
            /* Otherwise, use expected error message.*/
           return($q.reject(response.data.message));
       }

        /* getRiskDataByCaseId success function */
       function getRiskDataByCaseIdSuccess(response) {
           return(response);
       }
	}

		/*
	     * @purpose: Reassign Case
	     * @created: 05 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function reassignCase(params){
			var apiUrl = EHUB_API + 'investigation/reassignCase';
	        var request = $http({
	            method: "POST",
	            url: apiUrl,
	            params: params,
	            data:params
	        });
	        return(request
	                .then(reassignCaseSuccess)
	                .catch(reassignCaseError));

	        /* getlistofUsers error function */
	        function reassignCaseError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getlistofUsers success function */
	        function reassignCaseSuccess(response) {
	        		return(response);
		        }
	      }
		/*
	     * @purpose: get case almira graph
	     * @created: 06 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseAlmiraGraph(params){
			var apiUrl = EHUB_API + '';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getCaseAlmiraGraphSuccess)
	                .catch(getCaseAlmiraGraphError));

	        /* getCaseAlmiraGraph error function */
	        function getCaseAlmiraGraphError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getCaseAlmiraGraph success function */
	        function getCaseAlmiraGraphSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case comment notification
	     * @created: 06 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseCommentNotification(params){
			var apiUrl = EHUB_API + 'investigation/getCaseCommentNotification';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: params
	        });
	        return(request
	                .then(getNotificationAlertSuccess)
	                .catch(getNotificationAlertError));

	        /* getNotificationAlert error function */
	        function getNotificationAlertError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getNotificationAlert success function */
	        function getNotificationAlertSuccess(response) {
	            return(response);
	        }
		}

		/*
	     * @purpose: load MarketIntelligence
	     * @created: 10 oct 2017
	     * @params: apiUrl(string)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function loadMarketIntelligence(apiUrl){
	        var request = $http({
	            method: "GET",
	            url: apiUrl
	        });
	        return(request
	                .then(loadMarketIntelligenceSuccess)
	                .catch(loadMarketIntelligenceError));

	        /* getNotificationAlert error function */
	        function loadMarketIntelligenceError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getNotificationAlert success function */
	        function loadMarketIntelligenceSuccess(response) {
	            return(response);
	        }
		}

		/*
	     * @purpose: load MarketIntelligenceENT
	     * @created: 11 oct 2017
	     * @params: apiUrl(string)
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function loadMarketIntelligenceENT(txt){
			 var url =	EHUB_API+"entityVisualizer/ent?token=" + $rootScope.ehubObject.token;
			var request = $http({
				method: 'POST',
		    	url: url,
		    	data: JSON.stringify({text: txt})
	        });
	        return(request
	                .then(loadMarketIntelligenceENTSuccess)
	                .catch(loadMarketIntelligenceENTError));

	        /* getNotificationAlert error function */
	        function loadMarketIntelligenceENTError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	        /* getNotificationAlert success function */
	        function loadMarketIntelligenceENTSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get audit logsof Users
	     * @created: 30 Nov 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Prasanthi
	    */
		function getAuditLogs(params,data){
			/* data.recordsPerPage =100; */
			//var apiUrl = EHUB_API + 'audit/listLogs';
			var apiUrl = EHUB_API + 'audit/listLogs?token='+params.token;
			var request = $http({
	            method: "POST",
	            url: apiUrl,
				data :data
	        });
	        return(request
	                .then(getAuditLogsSuccess)
	                .catch(getAuditLogsError));

	        /* getAuditLogs error function */
	        function getAuditLogsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getAuditLogs success function */
	        function getAuditLogsSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: search audit logsof Users
	     * @created: 30 Nov 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Prasanthi
	    */
		function searchAuditLogs(params,data){
			data.recordsPerPage  = 200;
			//var apiUrl = EHUB_API + 'audit/searchLogs';
				var apiUrl = EHUB_API + 'audit/searchLogs?token='+params.token;
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data:data
	        });
	        return(request
	                .then(searchAuditLogsSuccess)
	                .catch(searchAuditLogsError));

	        /* searchAuditLogs error function */
	        function searchAuditLogsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* searchAuditLogs success function */
	        function searchAuditLogsSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case by id alerts
	     * @created: 17 jan 2018
	     * @params: token(object), caseId(integer), count(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseByIdAlerts(token, caseId, count){
			var apiUrl = EHUB_API + 'workflow/es/cases/' + caseId +'/alerts/' + count;
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: token
	        });
	        return(request
	                .then(getCaseByIdAlertsSuccess)
	                .catch(getCaseByIdAlertsError));

	        /* getCaseByIdAlerts error function */
	        function getCaseByIdAlertsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /* getCaseByIdAlerts success function */
	        function getCaseByIdAlertsSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case timeline
	     * @created: 18 jan 2018
	     * @params: token(object), caseId(integer), count(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getCaseTimeline(token, caseId, count){
//			var apiUrl = EHUB_API + 'workflow/es/cases/' + caseId +'/timeline/' + count;
			var apiUrl = EHUB_API + 'workflow/es/cases/' + caseId +'/timeline/founded/' + count;
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: token
	        });
	        return(request
	                .then(getCaseTimelineSuccess)
	                .catch(getCaseTimelineError));

	        /* getCaseTimeline error function */
	        function getCaseTimelineError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /* getCaseTimeline success function */
	        function getCaseTimelineSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get Associated Entities
	     * @created: 19 jan 2018
	     * @params: token(object), caseId(integer)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function getAssociatedEntities(token, caseId){
			var apiUrl = EHUB_API + 'workflow/graph/case/' + caseId +'/entities';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: token
	        });
	        return(request
	                .then(getAssociatedEntitiesSuccess)
	                .catch(getAssociatedEntitiesError));

	        /* getAssociatedEntities error function */
	        function getAssociatedEntitiesError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /* getAssociatedEntities success function */
	        function getAssociatedEntitiesSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: generate template report 
	     * @created: Apr 10, 2018
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: Amritesh
	    */
		function getReport(params,data){
//			var data = {"name":"Maninder"}
			var apiUrl = EHUB_API+'templates/TableTest/generate?token='+params.token;
			var request = $http({
				headers: {
					'Accept': 'multipart/form-data',
					'Content-Type': 'application/json'
				},
				responseType:'arraybuffer',
	            method: "POST",
	            url: apiUrl,
				data :data,
//				params: params.token
	        });
	        return(request
	                .then(getReportSuccess)
	                .catch(getReportError));

	        /* getReport error function */
	        function getReportError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	             /* Otherwise, use expected error message. */
	            return($q.reject(response.data.message));
	        }

	         /* getReport success function */
	        function getReportSuccess(response) {
	            return(response);
	        }
		}
		 /*
	     * @purpose: getGraphData
	     * @created: 4th May 2018
	     * @params: company name
	     * @return: success, error functions
	     * @author: Prasanthi
	     */

	    function getGraphData(data) {
	        var defer = $q.defer();     
//	        var apiUrl = 'http://46.101.163.245:9091/graph/api/graph/cybersecurity ' ;
	        var apiUrl = EHUB_API+'graph/cyberSecurity?token='+$rootScope.ehubObject.token;
	        var request = $http({
	            method: 'POST',
	            data:JSON.stringify(data),
	            headers: {
	                "Content-Type": "application/json"
	            },
	            url: apiUrl,
	        });
//	        var apiUrl = "vendor/data/petorTI.json";
//	        var request = $http({
//	            method: 'GET',
//	            headers: {
//	                "Content-Type": "application/json"
//	            },
//	            url: apiUrl,
//	        });
	        
	        request
	            .then(getGraphDataSuccess)
	            .catch(getGraphDataError);

	        /*getGraphData error function*/
	        function getGraphDataError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                defer.reject(response.data);
	            }
	            /*Otherwise, use expected error message.*/
	            defer.reject(response.data.message);
	        }

	        /*getGraphData success function*/
	        function getGraphDataSuccess(response) {
	            defer.resolve(response);
	        }

	        return defer.promise;
	    }
	    /*
         * @purpose: getIdentifier
         * @created: 4th May 2018
         * @params: company name
         * @return: success, error functions
         * @author: Prasanthi
         */

         function getIdentifier(name) {
             var defer = $q.defer();

             var apiUrl = EHUB_API + 'entity/org/suggest-name' + "?query=" + name + "&token=" + $rootScope.ehubObject.token;
             var request = $http({
                 method: 'GET',
                 headers: {
                     "Content-Type": "application/json"
                 },
                 url: apiUrl,
             });
             request
                 .then(getIdentifierSuccess)
                 .catch(getIdentifierError);

             /*getIdentifier error function*/
             function getIdentifierError(response) {
                 if (!angular.isObject(response.data) || !response.data.message) {
                     defer.reject(response.data);
                 }
                 /*Otherwise, use expected error message.*/
                 defer.reject(response.data.message);
             }

             /*getIdentifier success function*/
             function getIdentifierSuccess(response) {
                 defer.resolve(response);
             }

             return defer.promise;
         }
	    /*
	     * @purpose: get news details
	     * @created: 4th May 2018
	     * @params: docId
	     * @return: success, error functions
	     * @author: zameer
	     */

	    function getNews(identifier) {
	        var defer = $q.defer();

	        var apiUrl = EHUB_API + 'tuna/profile/'+identifier + "?token=" + $rootScope.ehubObject.token+"&orgOrPerson=org";
	        var request = $http({
	            method: 'GET',
	            url: apiUrl
	        });
	        request
	            .then(getNewsSuccess)
	            .catch(getNewsError);

	        /*getNews error function*/
	        function getNewsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                defer.reject(response.data);
	            }
	            /*Otherwise, use expected error message.*/
	            defer.reject(response.data.message);
	        }

	        /*getNews success function*/
	        function getNewsSuccess(response) {
	            defer.resolve(response);
	        }

	        return defer.promise;

	    }
	    /* 
		 * purpose: get Industry standard code.
		 * created: 27th JUly 2018
		 * params: params(object)
		 * return: success, error functions
		 * author: Swathi
		 */
		function getStandardCodeForIndustry(code){
			var apiUrl = EHUB_API + 'entity/industry/SIC/' +code+ '/isic?token=' +$rootScope.ehubObject.token;
			var request = $http({
				url: apiUrl,
				method: 'GET',
			});
			return(request
					.then(getIndustryStandardCodeSuccess)
					.catch(getIndustryStandardCodeError));
			
			/* getclustersInfo error function */
			function getIndustryStandardCodeError(response){
				if(!angular.isObject(response.data) || !response.data.message){
					return($q.reject(response.data));
				}
				return($q.reject(response.data.message));
			}
			/* getclustersInfo success function */
			function getIndustryStandardCodeSuccess(response){
				return(response);
			}
		}
		 /* @purpose: get Threat Aggregates
	     * @created: 19th July 2018
	     * @params: data(object)
	     * @return: success, error functions
	     * @author: Swathi
	    */
	    function getThreatAggregates(data) {
	    	data.query["class"] = 'Cyber Security';
	        var defer = $q.defer();
	        var apiUrl = EHUB_API + 'adverseNews/events/aggregate?token=' + $rootScope.ehubObject.token;
	        var request = $http({
	            method: 'POST',
	            url: apiUrl,
	            data: data
	        });
	        request
	            .then(getThreatAggregatesSuccess)
	            .catch(getThreatAggregatesError);

	        /*get Threat Aggregates error function*/
	        function getThreatAggregatesError(response) {
	        	if (!angular.isObject(response.data) || !response.data.message) {
	                defer.reject(response.data);
	            }
	            /*Otherwise, use expected error message.*/
	            defer.reject(response.data.message);
	        }
	        /*get Threat Aggregates success function*/
	        function getThreatAggregatesSuccess(response) {
	            defer.resolve(response);
	        }
	        return defer.promise;
        }
        /* @purpose: getRelatedCompanies
	     * @created: 19th July 2018
	     * @params: data(object)
	     * @return: success, error functions
	     * @author: Swathi
	    */
	    function getRelatedCompanies(data) {	    	
	        var defer = $q.defer();
	        var apiUrl = EHUB_API + 'entity/getPeerGroupData?token=' + $rootScope.ehubObject.token;
	        var request = $http({
	            method: 'POST',
	            url: apiUrl,
	            data: data,
	            headers: {
	                "Content-Type": "application/json; charset=utf-8"
	            }
	        });
	        request
	            .then(getRelatedCompaniesSuccess)
	            .catch(getRelatedCompaniesError);

	        /*getRelatedCompanies error function*/
	        function getRelatedCompaniesError(response) {
	        	if (!angular.isObject(response.data) || !response.data.message) {
	                defer.reject(response.data);
	            }
	            /*Otherwise, use expected error message.*/
	            defer.reject(response.data.message);
	        }
	        /*getRelatedCompanies success function*/
	        function getRelatedCompaniesSuccess(response) {
	            defer.resolve(response);
	        }
	        return defer.promise;
        }
	}