'use strict';
elementApp
	   .service('ActService', actService);

		actService.$inject = [
			'ActHotTopicsConst',
			'$state',
			'$timeout'
		];
		
		function actService(
				ActHotTopicsConst,
				$state,
				$timeout){
			/*
			 * @purpose: setting cloud chart for hot topics grid 
			 * @created: 04 oct 2017
			 * @params: options(object)
			 * @returns: no 
			 * @author: swathi
			 */
			/*jshint validthis: true */
			this.setHotTopicsCloudChart = function(options){
				$timeout(function(){
					$("#hottopics").empty();
					hotTopic(options);
				}, 0);

				/*responsiveness*/
		        $(window).on("resize",  function () {
		        	if($state.current.name == 'actCase'){
			        	$("#hottopics").empty();
			        	hotTopic(options);
		        	}
		        });
			};
			
			function hotTopic(options){

				var colorScale = d3.scaleOrdinal().range(ActHotTopicsConst.colors);
				var lis = "";
				
				angular.forEach(options.data1,function(d){ 
					d.color = ActHotTopicsConst.colorObj[d.group.toLowerCase()] ? ActHotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
					d.size  =d.count;
					d.text = d.name;
					lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
		        });

				
				var width = $('#acthottopicsdiv').width();
				var height = document.getElementById('acthottopicsdiv').clientHeight != null ? document.getElementById('acthottopicsdiv').clientHeight: 0;				
				var setTemp = width >= height ? height : width;
				var radius = setTemp/2;
				

				cloudTag({width:width,height:height,data:options.data1,ballSize: radius - ActHotTopicsConst.ballSize, "id":"#hottopics"});
				

			}
			
			this.chartCaseTimeLine = function(options, customData)
			{
			    var current_options = options;
			    loadCaseTimeLineChart(current_options, customData);
			    
			    function loadCaseTimeLineChart(current_options, customData) {
			        if (customData) {
			        	options.data = jQuery.extend(true, [], customData);
			            var chart_options = jQuery.extend(true, {}, options);
			            new CaseTimeLineChart(chart_options);
			        } else {

				            options.data = jQuery.extend(true, [], current_options.customData);
				            var chart_options = jQuery.extend(true, {}, options);
				            new CaseTimeLineChart(chart_options);

			        }
			    }

			};
				
		}