'use strict';
angular.module('ehubApp')
		.factory('ScreeningTestingApiService',screeningTestingApiService);

	screeningTestingApiService.$inject = [
			'$http',
			'$q',
			'Upload',
			'EHUB_API',
		];
		
		function screeningTestingApiService(
				$http,
				$q,
				Upload,
				EHUB_API){
			
			return {
				getEntitiesData: getEntitiesData
			};
			
			/*
		     * @purpose: get entities data
		     * @created: 5 Nov 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: Prasanthi
		    */
			function getEntitiesData(loadedData,params){
				var apiUrl =  EHUB_API + 'screeningRiskScore/Stage/screenings';
//				var apiUrl = "https://yffwor5y12.execute-api.eu-west-1.amazonaws.com/Stage/screenings";
		        var request = $http({
		            method: "POST",
//		            headers: {
//		                'Content-Type': 'application/json'
//		            },
		            url: apiUrl,
		            data: loadedData,
		            params:params
		        });
		        return(request
		                .then(getEntitiesDataSuccess)
		                .catch(getEntitiesDataError));
		
		        /*getEntitiesData error function*/
		        function getEntitiesDataError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getEntitiesData success function*/
		        function getEntitiesDataSuccess(response) {
		            return(response);
		        }
			}
			
	};