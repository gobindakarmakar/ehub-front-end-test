'use strict';
angular.module('ehubApp')
	   .controller('ScreeningTestingController', ScreeningTestingController);

ScreeningTestingController.$inject = [
			'$scope', 
			'$rootScope',
			'$timeout',
			'$state',
			'ScreeningTestingApiService',			
			'HostPathService',
			'Flash',
			'$uibModal'
		];
		
		function ScreeningTestingController(
				$scope, 
				$rootScope,
				$timeout,
				$state,
				ScreeningTestingApiService,				
				HostPathService,
				Flash,
				$uibModal
				) {
//			$scope.startAnalysisDisabled = true;
			console.log("ScreeningTesting");
			var loadedData;
			$(".navbar-right").css("display","none");
			$(".element-navbar-logo").css("pointer-events","none")
			$scope.screeningDataFinal=[];
			var token = $rootScope.ehubObject.token;
			var params = {
				token: token
			};
			 /**
	   		 * Function to upload file
	   		 */
		   	 $scope.upload = function(file,event){
					$scope.uploadFIlenameOne = file;
					if(file && file.name)
					{
						$scope.uploadnameone = file.name;
						var ext = file.name.split('.').pop().toLowerCase();
						var size = file.size;
						console.log("ext,size",ext,size);		
					}
		   			var reader = new FileReader();
					if (ext !== undefined && ext && $.inArray(ext, ['json']) == -1) {
						HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: json");
						$scope.uploadFIlenameOne = null;
						return;
					}
					else if(size && size !== null && size > 10240000){
						HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', file.name);
						$scope.uploadFIlenameOne = null;
						return;
					}
					else{
						reader.onload = function(event) {
							var jsonObj = JSON.parse(event.target.result);
							console.log(jsonObj);
							loadedData = jsonObj;	 
					}
		   		  }
					
				if(event.target.files){
					reader.readAsText(event.target.files[0]);
				}
		   	 };
		   	 
		   	 //load data 
		   	 $scope.loaddata = function(){		
		   		$scope.showMarketPulseLoader = true;
		   		$scope.screeningDataFinal = [];
		   		ScreeningTestingApiService.getEntitiesData(loadedData,params).then(function(response){		   			
		   			$scope.showMarketPulseLoader = false;
		   			if(response && response.data && response.data.message  =="Success" && response.data.results && response.data.results.length>0){
		   				angular.forEach(response.data.results,function(value,key){
		   					if(value.hits && value.hits.length>0){
		   						value.hits.map(function(d){
			   						d.rules.sort(function(a,b){
			   							return b.matchScore - a.matchScore;
			   						});
			   						angular.forEach(d.fields,function(v,k){
			   							if(v.value && typeof(v.value)!='string'){
			   								v.value = v.value.join(", ");
			   							}
			   						});
			   						d.maxmatchsocre  = d.rules[0].matchScore;
			   					})
			   					value.hits.sort(function(a,b){
			   						return b.maxmatchsocre -a.maxmatchsocre;
			   					})
			   					$scope.screeningDataFinal.push({
			   						type:value.query.type,
			   						name:value.query.name,
			   						dob:value.query.date,
			   						maxScore:value.hits[0].maxmatchsocre,
			   						hits:value.hits,
			   						ruleName:value.hits[0].rules[0].ruleName
			   					})
		   					}
		   				})
		   				
		   			}else{
		   				$scope.showMarketPulseLoader = false;
		   			 	HostPathService.FlashErrorMessage('ERROR SCREENING THE FILE', 'Uploaded Failed, Please try again later..');
			   		
		   			}
		   			
		   		}, function(error){
		   			$scope.showMarketPulseLoader = false;
	   			 	HostPathService.FlashErrorMessage('ERROR SCREENING THE FILE', 'Uploaded Failed, Please try again later..');
		   		});
		   	 }
		   	 
		   	 $scope.openMoreDetails = function(screeningData){
					//open modal here
					$scope.openScreeningTestingModal(screeningData)
				}
			
			/**
			* Function to load screening testing modal
			*/
			$scope.openScreeningTestingModal = function(data,selectedindex){
				// var articles = $scope.entitySearchResult.list['adverse_news'];
				// 		articles = articles.concat($scope.officersAdverseNews);
				// 		console.log('articles: ', articles);
				//var  findIndex = _.findIndex(articles, {title: selectedArticle.title});
				//selectedArticle = articles[findIndex];

				console.log("data",data);
				var screeningTesingModalInstance = $uibModal.open({
						templateUrl: 'scripts/common/modal/views/screeningTesting.modal.html',
						controller: 'ScreeningTestingModalController',
						size: 'lg',
						backdrop: 'static',
						windowClass: 'custom-modal full-analysis-wrapper',
						resolve: {
							data: function(){
								return data;
							},
							// articles: function(){
							// 	return articles;
							// },
							// header: function(){
							// 	return selectedArticle;
							// },
							// selectedindex:function(){
							// 	return findIndex
							// }
						}
					});

					screeningTesingModalInstance.result.then(function (response) {
					}, function (error) {
					});
				}
		};