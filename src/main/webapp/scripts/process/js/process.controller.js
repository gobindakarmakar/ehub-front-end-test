'use strict';
angular.module('ehubApp')
	   .controller('ProcessController', processController);

		processController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'ACTIVITI_FE_PATH',
			'UploadFileService',
			'DataCurationApiService',
			'HostPathService',
			'$stateParams',
			'EHUB_FE_API',
			'KYC_QUESTIONNAIRE_PATH',
			'POLICY_ENFORCEMENT_PATH',
			'CommonService',
			'GeneralSettingsApiService'
		]; 
		
		function processController(
				$scope, 
				$rootScope,
				$state,
				ACTIVITI_FE_PATH,
				UploadFileService,
				DataCurationApiService,
				HostPathService,
				$stateParams,
				EHUB_FE_API,
				KYC_QUESTIONNAIRE_PATH,
				POLICY_ENFORCEMENT_PATH,
				CommonService,
				GeneralSettingsApiService) {
    
			/*
		     * @purpose: Array index positioning 
		     * @created: 22 sep 2017
		     * @params: old_index(number), new_index(number)
		     * @return: array
		     * @author: Ankit
		    */  
			
			Array.prototype.moveArray = function (old_index, new_index) {
			    if (new_index >= this.length) {
			        var k = new_index - this.length;
			        while ((k--) + 1) {
			            this.push(undefined);
			        }
			    }
			    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
			    return this; // for testing purposes
			};
			 /*get drop down menu items for submenu*/
		    var dashboarDropDownMenuItems = jQuery.extend(true, [], HostPathService.getdashboarDropDownMenuItems());
			var dropDownDisableBydomains =HostPathService.getdashboardDisableBydomains();
			var current_domain =localStorage.getItem("domain")
			if(current_domain && dropDownDisableBydomains[current_domain]){
				angular.forEach(dashboarDropDownMenuItems,function(val){
					angular.forEach(val.content,function(v){
						if ($.inArray(v['name'],dropDownDisableBydomains[current_domain]) != -1){
							v['disabled'] ="yes";
						}
					})
					
				})
			}
			var alldashboardItems =dashboarDropDownMenuItems;
			$scope.currentState = $state.current.name;
			
			 
		//Initializing scope variables
			$rootScope.process = {
					dashboardItems: alldashboardItems.moveArray(0, 1).moveArray(2,3),
					checkSubmenuDisable: checkSubmenuDisable,
					checkMainmenuDisable: checkMainmenuDisable,
					subMenuDropdownClick: subMenuDropdownClick,
					getSubMenu: getSubMenu,
					addClass: addClass,
					iconColorClass: ['light','medium','dark'],
			}; 
			
		/*
	     * @purpose: disable submenus
	     * @created: 24 oct 2017
	     * @params: null
	     * @return: no
	     * @author: ankit
	    */  
		function checkSubmenuDisable(value){
			return CommonService.checkSubmenuDisable(value);
		}
		
		/*
	     * @purpose: disable main menu
	     * @created: 21 feb 2018
	     * @params: value
	     * @return: checkMainmenuDisable value
	     * @author: swathi
	    */  
		function checkMainmenuDisable(value){
			return CommonService.checkMainmenuDisable(value);
		}
 
		 /*
		     * @purpose: Click submenu
		     * @created: 21 sep 2017
		     * @params: submenuName(string)
		     * @return: no
		     * @author: ankit
		    */  
		function subMenuDropdownClick(submenuname){
			 var submenuname = submenuname.toLowerCase();
			 if(submenuname != 'predict'){
				 window.location.href= EHUB_FE_API +'#/'+ submenuname;
			 }else if(submenuname === 'predict'){			
				window.location.href= EHUB_FE_API +'#/leadGeneration/#!/landing';
			 } else{
				 window.location.href= EHUB_FE_API + submenuname;
			 }
		}
		
		
		/*
	     * @purpose: Active submenu
	     * @created: 5 oct 2017
	     * @params: submenuname(string)
	     * @return: no
	     * @author: ankit
	    */
	   function	getSubMenu(submenuname,  content, dashboardItemNames){	
		   CommonService.getSubMenuData(submenuname,  content, dashboardItemNames, $scope.currentState);
	   }
	   /*
	     * @purpose: go to discover page
	     * @created: 21 feb 2018
	     * @params: no
	     * @return: no
	     * @author: swathi
	    */
	   $scope.toDiscover = function(){
		   $state.go('discover');
	   };
		
	   /*
	     * @purpose: class
	     * @created: 24 oct 2017
	     * @params: null
	     * @return: no
	     * @author: ankit
	    */  
		function addClass(index){
			if(index==0 || index%3==0){
				return $scope.process.iconColorClass[0];
			}
			else if(index==1 || (index-1)%3==0){
				return $scope.process.iconColorClass[1];
			}
			else if(index==2 || (index-2)%3==0){
				return $scope.process.iconColorClass[2];
			}
		}
		/*
	     * @purpose: TO set a default module from the process page
	     * @created: 9th Aug 2019
	     * @params: no
	     * @return: no
	    */
	   $scope.processPageLoader = false;
		$scope.updateVal = function (setting,$event) {
				var data = {
					"attributeId": setting.attributeId,
					"selectedValue": "",
					"settingId": "",
					"systemSettingsType": setting.systemSettingType
				}
				$scope.processPageLoader = true;
			GeneralSettingsApiService.updateValue(data).then(function (response) {
				if (response) {
					HostPathService.FlashSuccessMessage('SUCCESSFULLY UPDATED NEW DEFAULT MODULE TO '+setting.name.toUpperCase(),'');
					$rootScope.defaultModuleName = setting.name.toLowerCase();
					$scope.processPageLoader = false;
				} else {
					HostPathService.FlashErrorMessage('FAILED TO UPDATE.. PLEASE TRY AGAIN LATER', '');
					$scope.processPageLoader = false;
				}
			}, function () {
				HostPathService.FlashErrorMessage('FAILED TO UPDATE.. PLEASE TRY AGAIN LATER', '');
				$scope.processPageLoader = false;
			});
			$event.preventDefault();
			$event.stopPropagation();
		};
};