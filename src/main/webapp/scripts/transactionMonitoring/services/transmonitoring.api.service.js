'use strict';
angular.module('ehubApp')
	   .factory('TransMonitoringApiService', transMonitoringApiService);

		transMonitoringApiService.$inject = [
			'$http',
			'EHUB_API'
		];
		
		function transMonitoringApiService(
				$http,
				EHUB_API){
	
			return {
				getRiskyTransaction: getRiskyTransaction
			};
			/*
		     * @purpose: get risky transaction
		     * @created: 10 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getRiskyTransaction(params){
				var apiUrl = EHUB_API + 'transMonitor/getRiskyTransaction';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getRiskyTransactionSuccess)
		                .catch(getRiskyTransactionError));
		
		        /*getRiskyTransaction error function*/
		        function getRiskyTransactionError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getRiskyTransaction success function*/
		        function getRiskyTransactionSuccess(response) {
		            return(response);
		        }
			}
		};