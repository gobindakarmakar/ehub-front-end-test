/*'use strict';*/
angular.module('ehubApp')
	   .service('TransMonitoringService', transMonitoringService);

		transMonitoringService.$inject = [
			'HostPathService'
		];
		
		function transMonitoringService(
				HostPathService){
			
			/*
			 * @purpose: load collapse chart for transaction linkage section
			 * @created: 11 oct 2017
			 * @params: options(object) 
			 * @returns: no 
			 * @author: swathi
			 */
			this.chartcollapse = function(options)
			{
			    var collapseData = [];
			    var current_options = options;
			    loadcollapseChart(current_options);
				/* responsiveness */
				 $(window).on("resize", function () {
					 if ($(options.container).find("svg").length != 0) {
						 $(options.container).empty();
						 var data = jQuery.extend(true, [], options);
						 new collapseChart(data);
					 }
				 });
			}    
			function loadcollapseChart(current_options) {
		        var uri = current_options.uri;
		        if(uri == null) {
		        	current_options.data = jQuery.extend(true, [], current_options.data);
		            var chart_options = jQuery.extend(true, {}, current_options);
		            var exampleChart = new collapseChart(chart_options);
		        } else {
		            d3.json(uri, function (error, data) {
		            	current_options.data = jQuery.extend(true, [], data);
		                var chart_options = jQuery.extend(true, {}, current_options);
		                var exampleChart = new collapseChart(chart_options);

		            });
		        }
		    }
		};