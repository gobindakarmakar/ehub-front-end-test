'use strict';
angular.module('ehubApp')
	   .constant('brushConst', {
		   height: 50,
		   margin: {top: 0, bottom: 20, left: 0, right: 0},
		   data: ["2017-02-17", "2017-02-21", "2017-02-24", "2017-02-27", "2017-02-28",  "2017-03-02", "2017-03-04", "2017-03-06", "2017-03-08", "2017-03-10", "2017-03-12", "2017-03-14", "2017-03-16", "2017-03-18", "2017-03-20","2017-03-22", "2017-03-24", "2017-03-26"]
	   }).constant('TransLinkageConst', {
		   height: 490
	   }).constant('GroupedPeerConst', {
		   height: 420,
		   islegends: false,
		   ytext: "Sum of transactions currency ($)",
		   xtext: "TRX_type",
		   groupedData: [{
		   		 "x": "Jan",
				 "Tokyo": 49900000,
				 "New York": 83600000,
				 "London": 48900000,
				 "Berlin": 42400000
			 }, 
			 {
				 "x": "Feb",
				 "Tokyo": 71500000,
				 "New York": 78800000,
				 "London": 38800000,
				 "Berlin": 33200000
			 },
			 {
				 "x": "Mar",
				 "Tokyo": 106400000,
				 "New York": 98500000,
				 "London": 39300000,
				 "Berlin": 34500000
			 },
			 {
				 "x": "Apr",
				 "Tokyo": 129200000,
				 "New York": 93400000,
				 "London": 41400000,
				 "Berlin": 39700000
			 },
			 {
				 "x": "May",
				 "Tokyo": 144000000,
				 "New York": 106000000,
				 "London": 47000000,
				 "Berlin": 52600000
			 },
			 {
				 "x": "Jun",
				 "Tokyo": 176000000,
				 "New York": 84500000,
				 "London": 48300000,
				 "Berlin": 75500000
			 },
			 {
				 "x": "Jul",
				 "Tokyo": 135600000,
				 "New York": 105000000,
				 "London": 59000000,
				 "Berlin": 57400000
			 },
			 {
				 "x": "Aug",
				 "Tokyo": 148500000,
				 "New York": 104300000,
				 "London": 59600000,
				 "Berlin": 60400000
			 },
			 {
				 "x": "Sep",
				 "Tokyo": 216400000,
				 "New York": 91200000,
				 "London": 52400000,
				 "Berlin": 47600000
			 },
			 {
				 "x": "Oct",
				 "Tokyo": 194100000,
				 "New York": 83500000,
				 "London": 65200000,
				 "Berlin": 39100000
			 },
			 {
				 "x": "Nov",
				 "Tokyo": 95600000,
				 "New York": 106600000,
				 "London": 59300000,
				 "Berlin": 46800000
			 },
			 {
				 "x": "Dec",
				 "Tokyo": 54400000,
				 "New York": 92300000,
				 "London": 51200000,
				 "Berlin": 51100000
			 }]
	   }).constant('TransJourneyConst', {
		   height: 300
	   }).constant('VolumeConst', {
		   height: 300,
		   ytext: 'Number of transactions',
		   xticks: 4
	   });
			