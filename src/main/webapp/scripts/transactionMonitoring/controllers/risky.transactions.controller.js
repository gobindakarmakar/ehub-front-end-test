'use strict';
angular.module('ehubApp')
	   .controller('RiskyTransactionsController', riskyTransactionsController);

 		riskyTransactionsController.$inject = [
			'$scope', 
			'$rootScope',
			'TransMonitoringApiService'
		];
		
		function riskyTransactionsController(
				$scope, 
				$rootScope,
				TransMonitoringApiService) {
    
		
		$scope.token = $rootScope.ehubObject.token;
		$scope.riskyTransDatas =[];
		
		$scope.$on("dateEvent", function(evt,data){ 
			$scope.riskyTransactionsLoader = true;
	    	var params = {
				"token": $scope.token,
				"clientId": 1,
				"fromDate": data.fromDate,
				"toDate": data.toDate
    		};
	    	TransMonitoringApiService.getRiskyTransaction(params).then(function(response){
	    		$scope.riskyTransactionsLoader = false;
	    		$scope.riskyTransDatas = response.data;
	    	}, function(error){
	    		$scope.riskyTransactionsLoader = false;
	    	});
		});
};