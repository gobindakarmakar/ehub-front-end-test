'use strict';
angular.module('ehubApp')
	   .controller('TransactionsJourneyController', transactionsJourneyController);

		transactionsJourneyController.$inject = [
			'$scope', 
			'$rootScope',
			'TransJourneyConst'
		];
		
		function transactionsJourneyController(
				$scope, 
				$rootScope,
				TransJourneyConst) {
    
	
		var options = {
			header: "TRANSACTION JOURNEY",
			Uri : "vendor/data/sank.json",
			container : "#transactionJourney",
			height: TransJourneyConst.height
		};
	    sankey_diagram(options);
		function sankey_diagram(options)
		{
		    loadsankey(options);
		    $(window).on("resize", function () {
	    		if ($(options.container).find("svg").length != 0) {
		    		$(options.container).empty();
		    		var currentOptions  = jQuery.extend(true, {}, options);
		    		new sankey_chart(currentOptions);
		    	}
		    });
		    function loadsankey(options) {
				$(options.container).siblings(".headerDiv").html(options.header);
				d3.json(options.Uri, function (error, data) {
			        options.data = data;			        
			        var currentOptions  = jQuery.extend(true, {}, options);
		    		new sankey_chart(currentOptions);
			    });
			}
		}
};