'use strict';
angular.module('ehubApp')
	   .controller('GroupedPeerAnalysisController', groupedPeerAnalysisController);

		groupedPeerAnalysisController.$inject = [
			'$scope', 
			'$rootScope',
			'GroupedPeerConst'
		];
		
		function groupedPeerAnalysisController(
				$scope, 
				$rootScope,
				GroupedPeerConst) {
    
		
		var data = GroupedPeerConst.groupedData;
		var options = {
		    container: "#groupbar",
		    header: "GROUPED PEER ANALYSIS",
		    data: data,
		    height: GroupedPeerConst.height,
		    islegends: GroupedPeerConst.islegends,
		    ytext: GroupedPeerConst.ytext,
		    xtext: GroupedPeerConst.xtext,
		    showAxisX:false
		};
		groupedColumChart(options);
		
		$(window).on("resize", function () {
	    	if ($(options.container).find("svg").length != 0) {
	    		$(options.container).empty();
	    		groupedColumChart(options);
	    	}
	    });
};