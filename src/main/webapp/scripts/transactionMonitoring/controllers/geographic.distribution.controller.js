'use strict';
angular.module('ehubApp')
	   .controller('GeographicDistributionController', geographicDistributionController);

		geographicDistributionController.$inject = [
			'$scope', 
			'$rootScope',
			'TopLocationsConst',
			'DiscoverApiService',
			'DiscoverService'
		];
		
		function geographicDistributionController	(
				$scope, 
				$rootScope,
				TopLocationsConst,
				DiscoverApiService,
				DiscoverService
				) {
    
		$scope.toggleGraphTopLocation = true;
		$scope.worldDatas = [];
		$scope.geographicPreloader = true;
		$scope.token = $rootScope.ehubObject.token;
		var markersData = [],
			params = {
		    	token: $scope.token
			};
		var zoomOptions = TopLocationsConst.zoomOptions;
		var count = 40;
		var data = {
			'status': [4, 5, 8, 9]	
		};
		/*
	     * @purpose: get list of locations
	     * @created: 26 sep 2017
	     * @params: params(object)
	     * @return: no
	     * @author: swathi
	    */
		DiscoverApiService.getAllCaseSummaryByStatusList(params, data, count).then(function(response) {
		    $scope.geographicPreloader = false;
		    if(response.data){
		    	$scope.locationsData = response.data.locations;
			    $scope.totalLocation=Object.keys(response.data.locations).length;
			    $scope.locationsData['New York'] = 13;
			}else{
			    $scope.locationsData = [];
				$scope.totalLocation = 0;
			}
		    var leafletMap=DiscoverService.setWorldMap($scope.locationsData, zoomOptions);
		    $rootScope.leafletMap = leafletMap;
		}, 
		function(response) {
			$scope.geographicPreloader = false;    
		});
};