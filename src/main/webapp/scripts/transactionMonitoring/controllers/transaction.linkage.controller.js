'use strict';
angular.module('ehubApp')
	   .controller('TransactionLinkageController', transactionLinkageController);

		transactionLinkageController.$inject = [
			'$scope', 
			'$rootScope',
			'TransMonitoringService',
			'TransLinkageConst'
		];
		
		function transactionLinkageController(
				$scope, 
				$rootScope,
				TransMonitoringService,
				TransLinkageConst) {
    
		
		var options = {
	        "container": "#transactionLinkage",
	        "header": "TRANSACTION LINKAGE", 
	        "uri": "vendor/data/miserables.json", 
	        "height": TransLinkageConst.height
	    };
		TransMonitoringService.chartcollapse(options);
};