'use strict';
angular.module('ehubApp')
	   .controller('TransactionMonitoringController', transactionMonitoringController);

		transactionMonitoringController.$inject = [
			'$scope', 
			'$rootScope',
			'$window',
			'brushConst',
			'$uibModal',
			'HostPathService',
			'$stateParams',
			'UploadFileService',
			'ActApiService'
		];
		
		function transactionMonitoringController(
				$scope, 
				$rootScope,
				$window,
				brushConst,
				$uibModal,
				HostPathService,
				$stateParams,
				UploadFileService,
				ActApiService) {
    
		
		$scope.token = $rootScope.ehubObject.token;
		var IR, DR, TR;
		function calculateRandomRisk(caseName){
			HostPathService.setRandomRisk(caseName);
			$scope.scaledNumArr = HostPathService.getRandomRisk();
			IR = ($scope.scaledNumArr[0]/100);
			DR = ($scope.scaledNumArr[1]/100);
			TR = ($scope.scaledNumArr[2]/100);
		}
		
		var uploadedFilesLength;
		if($stateParams.caseId != undefined && $stateParams.caseId != '' && $stateParams.caseId != null){
			var caseDocsparams = {
				caseId: $stateParams.caseId,
				token: $rootScope.ehubObject.token
			};
			ActApiService.getCaseDetails(caseDocsparams).then(function(response){
				calculateRandomRisk(response.data.name);
			}, function(error){
				
			});
			UploadFileService.getCaseDocuments(caseDocsparams).then(function(response){
				uploadedFilesLength = response.data.result.length;
				if(uploadedFilesLength > 0){
	        		if(uploadedFilesLength >= 3){
	        			$scope.cumulativeRisk = "46";
	    				if(uploadedFilesLength >= 4)
	    					$scope.cumulativeRisk = "72";
	    			}else{
	    				$scope.cumulativeRisk = "67";
	    			}
	    		}else{
	    			$scope.cumulativeRisk = "67";
	    		}
			}, function(error){
				
			});
		}else{
			calculateRandomRisk('petrosaudi international ltd');
			$scope.cumulativeRisk = (1 - (1 - DR) * (1 - IR) * (1 - TR))*100;
		}
		
		/* Calling Brush */
		var options = {
	        container: ".timeline-wrapper",
	        height: brushConst.height,
	        margin: brushConst.margin,
	        data: brushConst.data
		};
		
		$(document).ready(function () {
			createBrush(options);
		});
		/* get date range */
		$window.getDateRange = function(x0, x1){
			var fromDateStr = moment(x0).format('YYYY-MM-DD hh:mm:ss');
			var toDateStr = moment(x1).format('YYYY-MM-DD hh:mm:ss');
			$scope.$broadcast("dateEvent", {fromDate:fromDateStr, toDate:toDateStr});
		}
		/*
	     * @purpose: open captured modal
	     * @created: 11 oct 2017
	     * @params: widgetId(string)
	     * @return: no
	     * @author: swathi
	    */
		$scope.TransCaptureDiv = function(widgetId, toggleGraphTopLocation){
			var openWidgetCaptureModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
				controller: 'WidgetCaptureModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					widgetId: function(){
						return widgetId;
					},
					canvasnew:function(){
						return '';
					},
					topLocMapView: function(){
						return toggleGraphTopLocation;
					}
				}
			});
			openWidgetCaptureModal.result.then(function(response){
				
			}, function(reject){
				
			});
		};
};