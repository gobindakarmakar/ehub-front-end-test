'use strict';
angular.module('ehubApp')
	   .controller('VolumeMonitorController', volumeMonitorController);

		volumeMonitorController.$inject = [
			'$scope', 
			'$rootScope',
			'VolumeConst'
		];
		
		function volumeMonitorController(
				$scope, 
				$rootScope,
				VolumeConst) {
    
		
		var options = {
		    header : "VOLUME MONITOR",
		    uri1: "vendor/data/1000.json",
		    uri2: "vendor/data/risk.json",
		    container :"#associatedEntitiesChart",
		    height: VolumeConst.height,
		    ytext: VolumeConst.ytext,
		    xticks: VolumeConst.xticks
		}
		timescaleBubble(options);
		
		function timescaleBubble(options)
		{
		    var timescalebubbleData = [];
		    loadtimescalebubbleChart(options)
		    $(window).on("resize", function () {
		    	if ($(options.container).find("svg").length != 0) {
		    		$(options.container).empty();
		    		new timescalebubbleChart2(options);
		    	}
		    });
		    function loadtimescalebubbleChart(options) {
		        var loadflag = 0;
		        var vertexData = [];
		        var riskData = [];
		        d3.json(options.uri1, function (data) {
		            loadflag++;
		            vertexData = data.vertices;
		            options.data = timescalebubbleData;
		            if (loadflag == 2) {
		                var exampleChart = new timescalebubbleChart2(options);
		            }
		        });
		        d3.json(options.uri2, function (data) {
		            loadflag++;
		            riskData = data.body;
		            if (loadflag == 2) {
		                timescalebubbleData = handleTImeBubleData(vertexData, riskData)
		                options.data = timescalebubbleData;
		                var exampleChart = new timescalebubbleChart2(options);
		            }
		        });
		    }
		    /**
			 * FUnction to handle time line bubble data
			 */
		    function handleTImeBubleData(vertexData, riskData) {
		        var finalData = [];
		        $.each(vertexData, function (i, v) {
		            $.each(riskData, function (i1, v1) {
		                if (v.id == v1.vertex) {
		                    var rate = getriskRation(v1.direct, v1.indirect, v1.transactional);
		                    var size = parseInt(rate / 10);
		                    finalData.push({
		                        date: v.start,
		                        rate: rate,
		                        size: size
		                    })
		                }
		            });
		        });
		        return finalData;
		    }
		    function getriskRation(direct, indirect, transactional) {
		        return (1 - [(1 - direct) * (1 - indirect) * (1 - transactional)]) * 100;
		    }
		}
};