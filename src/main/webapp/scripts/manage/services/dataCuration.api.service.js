'use strict';
angular.module('ehubApp')
    .factory('DataCurationApiService', dataCurationApiService);


dataCurationApiService.$inject = [
    '$http',
    'EHUB_API',
    '$q',
    '$rootScope',
    'DataCurationAPIConstant',
    '$stateParams'
];

function dataCurationApiService(
    $http,
    EHUB_API,
    $q,
    $rootScope,
    DataCurationAPIConstant,
    $stateParams) {


    return {
        getAllCountriesNames: getAllCountriesNames,
        saveOrgDataCurationDetails: saveOrgDataCurationDetails,
        savePersonDataCurationDetails: savePersonDataCurationDetails,
        getAllIndustriesNames: getAllIndustriesNames,
        getAllNationalityData: getAllNationalityData,
        getAllEconomicSectorsData: getAllEconomicSectorsData,
        getbusinessSectorsData: getbusinessSectorsData,
        getPersonDetails: getPersonDetails,
        getlistofOrganisations: getlistofOrganisations,
        getIdentifierInfo: getIdentifierInfo,
        getAllDirectorRoles: getAllDirectorRoles,
        getAllOfficerRoles: getAllOfficerRoles,
        saveRelationdata: saveRelationdata,
        updateOrgDataCurationDetails: updateOrgDataCurationDetails,
        getDirectorOfficeriInfodetails: getDirectorOfficeriInfodetails,
        updatePersonDataCurationDetails: updatePersonDataCurationDetails,
        getOrganationNamesSuggestions: getOrganationNamesSuggestions,
        getPersonNamesSuggestions: getPersonNamesSuggestions,
        getPersonInDetails: getPersonInDetails
    };

    /*
     * @purpose: countries Names
     * @created: 19 jan 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getAllCountriesNames() {
        var apiUrl = EHUB_API + 'datacuration/meta/countries';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllCountriesNamesSuccess)
            .catch(getAllCountriesNamesError));

        /*getAllCaseSummaryByLimit error function*/
        function getAllCountriesNamesError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getAllCaseSummaryByLimit success function*/
        function getAllCountriesNamesSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }
    
    /*
     * @purpose: list of Industries Group
     * @created: 03 Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getAllIndustriesNames() {
        var apiUrl = EHUB_API + 'datacuration/meta/industry-groups';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllIndustriesNamesSuccess)
            .catch(getAllIndustriesNamesError));

        /*getAllIndustriesNamesError error function*/
        function getAllIndustriesNamesError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getAllIndustriesNamesSuccess success function*/
        function getAllIndustriesNamesSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }

    /*
     * @purpose: list of nationalities 
     * @created: 03 Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getAllNationalityData() {
        var apiUrl = EHUB_API + 'datacuration/meta/nationalities';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllNationalityDataSuccess)
            .catch(getAllNationalityDataError));

        /*getAllNationalityData error function*/
        function getAllNationalityDataError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getAllNationalityData success function*/
        function getAllNationalityDataSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }

    /*
     * @purpose: save Org DataCuration Details
     * @created: 19 jan 2018
     * @params: data(object)
     * @return: success, error functions
     * @author: Ankit
     */
    function saveOrgDataCurationDetails(data) {
        var apiUrl = EHUB_API + 'entity/org/get-or-create';
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(saveOrgDataCurationDetailsSuccess)
            .catch(saveOrgDataCurationDetailsError));

        /*saveOrgDataCurationDetails error function*/
        function saveOrgDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*saveOrgDataCurationDetails success function*/
        function saveOrgDataCurationDetailsSuccess(response) {
            return (response);
        }
    }

    /*
     * @purpose: list of nationalities 
     * @created: 03 Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getAllNationalityData() {
        var apiUrl = EHUB_API + 'datacuration/meta/nationalities';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllNationalityDataSuccess)
            .catch(getAllNationalityDataError));

        /*getAllNationalityData error function*/
        function getAllNationalityDataError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getAllNationalityData success function*/
        function getAllNationalityDataSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }
    /*
     * @purpose: getAllEconomicSectorsData 
     * @created: 03 Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getAllEconomicSectorsData() {
        var Url = EHUB_API + 'datacuration/meta/economic-sectors';
        var request = $http({
            method: 'GET',
            url: Url,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request.then(getAllEconomicSectorsDataSuccess)
            .catch(getAllEconomicSectorsDataError));


        function getAllEconomicSectorsDataError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        function getAllEconomicSectorsDataSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }
    /*
     * @purpose: getbusinessSectorsData 
     * @created: 03 Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */
    function getbusinessSectorsData() {
        var Url = EHUB_API + 'datacuration/meta/business-sectors';
        var request = $http({
            method: "GET",
            url: Url,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request.then(getbusinessSectorsDataSuccess)
            .catch(getbusinessSectorsDataError));

        function getbusinessSectorsDataError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        function getbusinessSectorsDataSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }



    /*
     * @purpose: save Person DataCuration Details
     * @created: 19 jan 2018
     * @params: data(object)
     * @return: success, error functions
     * @author: Ankit
     */
    function savePersonDataCurationDetails(data) {
        var apiUrl = EHUB_API + 'entity/person/get-or-create';
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(savePersonDataCurationDetailsSuccess)
            .catch(savePersonDataCurationDetailsError));

        /*savePersonDataCurationDetails error function*/
        function savePersonDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*savePersonDataCurationDetails success function*/
        function savePersonDataCurationDetailsSuccess(response) {
            return (response);
        }
    }

    /*
     * @purpose: person data
     * @created: 6th Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */

    function getPersonDetails() {
        var apiUrl = EHUB_API + 'entity/person/search';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getPersonDetailsSuccess)
            .catch(getPersonDetailsError));

        // savePersonDataCurationDetails error function
        function getPersonDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        //savePersonDataCurationDetails success function
        function getPersonDetailsSuccess(response) {
            return (response);
        }
    }

    /*
     * @purpose: To get list of Organisations
     * @created: 6th Feb 2018
     * @params: name(string),limit(number)
     * @return: success, error functions
     * @author: Virendra
     */

    function getlistofOrganisations(name, limit) {
        var apiUrl = EHUB_API + 'entity/org/search';
        var key = {};
        if (name) {
            //				apiUrl +=  name  ? "?query=" + name : "";
            angular.extend(key, {
                "vcard:organization-name": name
            });
        }
        limit = parseInt(limit);
        if (limit) {
            //				apiUrl +=  limit  ? "&size=" + limit : "";
            angular.extend(key, {
                "size": limit
            });
        } else {
            angular.extend(key, {
                "size": 10
            });
        }
        //			angular.extend(Parameters,{"token": $rootScope.ehubObject.token});
        //			console.log(key,typeof(limit));
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: key,
            params: {
                token: $rootScope.ehubObject.token
            }
        });

        return (request
            .then(getlistofOrganisationsSuccess)
            .catch(getlistofOrganisationsError));

        /*getlistofOrganisations error function*/
        function getlistofOrganisationsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getlistofOrganisations success function*/
        function getlistofOrganisationsSuccess(response) {
            return (response);
        }

    }

    /*
     * @purpose: get org name suggestion
     * @created: 19 jan 2018
     * @params: name(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function getOrganationNamesSuggestions(name) {
        var apiUrl = EHUB_API + 'entity/org/suggest-name';
        if (name) {
            apiUrl += name ? "?query=" + name : "";
        }
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });

        return (request
            .then(getOrganationNamesSuggestionsSuccess)
            .catch(getOrganationNamesSuggestionsError));

        /*getOrganationNamesSuggestions error function*/
        function getOrganationNamesSuggestionsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getOrganationNamesSuggestions success function*/
        function getOrganationNamesSuggestionsSuccess(response) {
            return (response);
        }

    }
    /*
     * @purpose: get person name suggestion
     * @created: 19 jan 2018
     * @params: name(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function getPersonNamesSuggestions(name) {
        var apiUrl = EHUB_API + 'entity/person/suggest-name';
        if (name) {
            apiUrl += name ? "?query=" + name : "";
        }
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });

        return (request
            .then(getPersonSuggestionsSuccess)
            .catch(getPersonSuggestionsError));

        /*getPersonSuggestions error function*/
        function getPersonSuggestionsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*getPersonSuggestions success function*/
        function getPersonSuggestionsSuccess(response) {
            return (response);
        }
    }
    /*
     * @purpose: getIdentifierInfo
     * @created: 19 jan 2018
     * @params: id(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function getIdentifierInfo(id) {
        var request = $http({
            method: "GET",
            url: id,
            params: {
                token: $rootScope.ehubObject.token
            }
        });

        return (request
            .then(savePersonDataCurationDetailsSuccess)
            .catch(savePersonDataCurationDetailsError));

        /*savePersonDataCurationDetails error function*/
        function savePersonDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*savePersonDataCurationDetails success function*/
        function savePersonDataCurationDetailsSuccess(response) {
            return (response);
        }
    }


    /*
     * @purpose: director data
     * @created: 8th Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */

    function getAllDirectorRoles() {
        var apiUrl = EHUB_API + 'datacuration/meta/director-roles';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllDirectorRolesSuccess)
            .catch(getAllDirectorRolesError));

        // getAllDirectorRoles error function
        function getAllDirectorRolesError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        //getAllDirectorRoles success function
        function getAllDirectorRolesSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }


    /*
     * @purpose: director data
     * @created: 8th Feb 2018
     * @params: none
     * @return: success, error functions
     * @author: Virendra
     */

    function getAllOfficerRoles() {
        var apiUrl = EHUB_API + 'datacuration/meta/officer-roles';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getAllOfficerRolesSuccess)
            .catch(getAllOfficerRolesError));

        // getAllOfficerRoles error function
        function getAllOfficerRolesError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        //getAllOfficerRoles success function
        function getAllOfficerRolesSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            return (response);
        }
    }

    /*
     * @purpose: save relations
     * @created: 8th Feb 2018
     * @params: data(object),token(object)
     * @return: success, error functions
     * @author: Virendra
     */

    function saveRelationdata(role, data) {
        var apiUrl = EHUB_API + 'datacuration/relation/' + role;
        var request = $http({
            method: "POST",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            },
            data: data
        });
        return (request
            .then(saveRelationdataSuccess)
            .catch(saveRelationdataError));

        // saveRelationdata error function
        function saveRelationdataError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        //saveRelationdata success function
        function saveRelationdataSuccess(response) {
            return (response);
        }
    }
    /*
     * @purpose: updateOrgDataCurationDetails
     * @created: 19 jan 2018
     * @params: data(object)
     * @return: success, error functions
     * @author: Virendra
     */
    function updateOrgDataCurationDetails(data) {
        var id = $stateParams.identifier.split("#")[0];
        var apiUrl = EHUB_API + 'entity/org/' + id + '/attr';
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(saveOrgDataCurationDetailsSuccess)
            .catch(saveOrgDataCurationDetailsError));

        /*saveOrgDataCurationDetails error function*/
        function saveOrgDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*saveOrgDataCurationDetails success function*/
        function saveOrgDataCurationDetailsSuccess(response) {
            return (response);
        }
    }
    /*
     * @purpose: getDirectorOfficeriInfodetails
     * @created: 19 jan 2018
     * @params: apiUrl(string),identifier(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function getDirectorOfficeriInfodetails(apiUrl, identifier) {
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: identifier,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(saveOrgDataCurationDetailsSuccess)
            .catch(saveOrgDataCurationDetailsError));

        /*saveOrgDataCurationDetails error function*/
        function saveOrgDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*saveOrgDataCurationDetails success function*/
        function saveOrgDataCurationDetailsSuccess(response) {
            return (response);
        }
    }
    /*
     * @purpose: getDirectorOfficeriInfodetails
     * @created: 19 jan 2018
     * @params: data(Object),identifier(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function updatePersonDataCurationDetails(data, identifier) {

        var apiUrl = EHUB_API + 'entity/person/' + identifier + '/attr';
        var request = $http({
            method: "POST",
            url: apiUrl,
            data: data,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(savePersonDataCurationDetailsSuccess)
            .catch(savePersonDataCurationDetailsError));

        /*savePersonDataCurationDetails error function*/
        function savePersonDataCurationDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            /*Otherwise, use expected error message.*/
            return ($q.reject(response.data.message));
        }

        /*savePersonDataCurationDetails success function*/
        function savePersonDataCurationDetailsSuccess(response) {
            return (response);
        }

    }
    /*
     * @purpose: getDirectorOfficeriInfodetails
     * @created: 19 jan 2018
     * @params: data(Object),apiUrl(string)
     * @return: success, error functions
     * @author: Virendra
     */
    function getPersonInDetails(data, apiUrl) {
        //var apiUrl = EHUB_API + 'datacuration/meta/officer-roles';
        var request = $http({
            method: "GET",
            url: apiUrl,
            params: {
                token: $rootScope.ehubObject.token
            }
        });
        return (request
            .then(getPersonInDetailsSuccess)
            .catch(getPersonInDetailsError));

        // getAllOfficerRoles error function
        function getPersonInDetailsError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject(response.data));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }

        //getAllOfficerRoles success function
        function getPersonInDetailsSuccess(response) {
            if (response.data.length > 0) {
                response.data.sort();
            }
            var a = [];
            a.push(response);
            a.push(data);
            return a;
        }

    }
}