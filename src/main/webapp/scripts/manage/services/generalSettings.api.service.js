'use strict';
angular.module('ehubApp')
	.factory('GeneralSettingsApiService', generalSettingsApiService);

generalSettingsApiService.$inject = [
	'$http',
	'EHUB_API',
	'$q',
	'$rootScope'
];

function generalSettingsApiService(
	$http,
	EHUB_API,
	$q,
	$rootScope) {

	return {
		getGeneralSettings: getGeneralSettings,
		updateValue: updateValue,
		getListItemsByListType: getListItemsByListType,
		deleteListItem:deleteListItem,
		AddUpdateListItem:AddUpdateListItem,
		getColorList:getColorList,
		getIconList:getIconList,
		getJurisdictionList:getJurisdictionList
	};

	/*
	 * @purpose: get general settings
	 * @created: 9 Aug 2018
	 * @params: searchString(object)
	 * @return: success, error functions
	 * @author: prasanthi
	*/
	function getGeneralSettings(searchString) {
		var apiUrl = EHUB_API + 'systemSettings/getSystemSettings';
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: {
				token: $rootScope.ehubObject.token,
				searchKey: searchString == '' ? undefined : searchString
			}
		});
		return (request
			.then(getGeneralSettingSuccess)
			.catch(getGeneralSettingError));

		/*getGeneralSettings error function*/
		function getGeneralSettingError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getGeneralSettings success function*/
		function getGeneralSettingSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
	/*
	 * @purpose: update settings value
	 * @created: 9 Aug 2018
	 * @params: token(object)
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Prasanthi
	*/
	function updateValue(data) {
		var apiUrl = EHUB_API + 'systemSettings/updateSystemSettings?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(updateValueSuccess)
			.catch(updateValueError));

		/*updateValue error function*/
		function updateValueError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function updateValueSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}

	/*
	* @purpose: get List item by Type
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function getListItemsByListType(ListType) {
		var apiUrl = EHUB_API + 'listManagement/getListItemsByListType?listType=' + ListType + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getListItemsByListTypeSuccess)
			.catch(getListItemsByListTypeError));

		/*updateValue error function*/
		function getListItemsByListTypeError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function getListItemsByListTypeSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
	/*
	* @purpose: delete List item by id
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function deleteListItem(deleteItemID) {
		var apiUrl = EHUB_API + 'listManagement/deleteListItem?listItemId=' + deleteItemID + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "DELETE",
			url: apiUrl,
		});
		return (request
			.then(deleteListItemeSuccess)
			.catch(deleteListItemError));

		/*updateValue error function*/
		function deleteListItemError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function deleteListItemeSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
	/*
	* @purpose: delete List item by display Name
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function AddUpdateListItem(data) {
		var apiUrl = EHUB_API + 'listManagement/saveOrUpdateListItem?token=' +  $rootScope.ehubObject.token;
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(AddUpdateListItemSuccess)
			.catch(AddUpdateListItemError));

		/*updateValue error function*/
		function AddUpdateListItemError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function AddUpdateListItemSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
		/*
	* @purpose: get color
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function getColorList() {
		var apiUrl = EHUB_API + 'listManagement/getColors?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getColorListSuccess)
			.catch(getColorListError));

		/*updateValue error function*/
		function getColorListError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function getColorListSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
		/*
	* @purpose: get Icon list
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function getIconList() {
		var apiUrl = EHUB_API + 'listManagement/getIcons?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getIconListSuccess)
			.catch(getIconListError));

		/*updateValue error function*/
		function getIconListError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function getIconListSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}
/*
	* @purpose: get jurisdiction list
	* @created: 27 july 2019
	* @params: token(object)
	* @params: data(object)
	* @return: success, error functions
	* @author: Amritesh
	*/
	function getJurisdictionList() {
		var apiUrl = EHUB_API + 'sourceJurisdiction/getSourceJurisdiction?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getJurisdictionListSuccess)
			.catch(getJurisdictionListError));

		/*updateValue error function*/
		function getJurisdictionListError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateValue success function*/
		function getJurisdictionListSuccess(response) {
			if (response.data.length > 0) {
				response.data.sort();
			}
			return (response);
		}
	}

}