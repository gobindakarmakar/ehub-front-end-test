'use strict';
elementApp
	.constant('DataCurationAPIConstant','http://95.85.58.85:3001/')
	.constant('entity_org_search', {
		"hits": [
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "3a32be57365449639f73a6c704132a73",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "ЭСЭЙ МИРОВЫЕ МЕТАЛЛЫ",
			"@created": "2018-05-08T19:56:08.231Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678050137",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"ЭСЭЙ МИРОВЫЕ МЕТАЛЛЫ\"",
			  "ООО \"ЭСЭЙ МИРОВЫЕ МЕТАЛЛЫ\""
			],
			"bst:businessClassifier": [
			  {
				"code": "46.12",
				"label": "Деятельность агентов по оптовой торговле топливом, рудами, металлами и химическими веществами"
			  },
			  {
				"code": "46.77",
				"label": "Торговля оптовая отходами и ломом"
			  },
			  {
				"code": "46.75",
				"label": "Торговля оптовая химическими продуктами"
			  },
			  {
				"code": "46.72",
				"label": "Торговля оптовая металлами и металлическими рудами"
			  },
			  {
				"code": "47.52.73",
				"label": "Торговля розничная металлическими и неметаллическими конструкциями в специализированных магазинах"
			  }
			],
			"@updated": "2018-05-08T19:56:08.231Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "0352a1fa45c343899ca11e69df855f32",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "КОМПАНИЯ ПРОФИТРОЛЬ",
			"@created": "2018-05-08T19:56:08.176Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678055054",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"КОМПАНИЯ ПРОФИТРОЛЬ\"",
			  "ООО \"КОМПАНИЯ ПРОФИТРОЛЬ\""
			],
			"bst:businessClassifier": [
			  {
				"code": "62.02",
				"label": "Деятельность консультативная и работы в области компьютерных технологий"
			  },
			  {
				"code": "63.11",
				"label": "Деятельность по обработке данных, предоставление услуг по размещению информации и связанная с этим деятельность"
			  }
			],
			"@updated": "2018-05-08T19:56:08.176Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "c59d2a990008464f9771c6b4af876273",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "ПК ЛОГОТЭК",
			"@created": "2018-05-08T19:56:08.695Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678051243",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"ПК ЛОГОТЭК\"",
			  "ООО \"ПК ЛОГОТЭК\""
			],
			"bst:businessClassifier": [
			  {
				"code": "52.29",
				"label": "Деятельность вспомогательная прочая, связанная с перевозками"
			  },
			  {
				"code": "46.49",
				"label": "Торговля оптовая прочими бытовыми товарами"
			  },
			  {
				"code": "46.33",
				"label": "Торговля оптовая молочными продуктами, яйцами и пищевыми маслами и жирами"
			  },
			  {
				"code": "46.15",
				"label": "Деятельность агентов по оптовой торговле мебелью, бытовыми товарами, скобяными, ножевыми и прочими металлическими изделиями"
			  },
			  {
				"code": "46.90",
				"label": "Торговля оптовая неспециализированная"
			  },
			  {
				"code": "82.99",
				"label": "Деятельность по предоставлению прочих вспомогательных услуг для бизнеса, не включенная в другие группировки"
			  },
			  {
				"code": "46.62",
				"label": "Торговля оптовая станками"
			  },
			  {
				"code": "46.34",
				"label": "Торговля оптовая напитками"
			  },
			  {
				"code": "46.69",
				"label": "Торговля оптовая прочими машинами и оборудованием"
			  },
			  {
				"code": "45.3",
				"label": "Торговля автомобильными деталями, узлами и принадлежностями"
			  },
			  {
				"code": "46.37",
				"label": "Торговля оптовая кофе, чаем, какао и пряностями"
			  },
			  {
				"code": "46.18",
				"label": "Деятельность агентов, специализирующихся на оптовой торговле прочими отдельными видами товаров"
			  },
			  {
				"code": "52.10",
				"label": "Деятельность по складированию и хранению"
			  },
			  {
				"code": "46.16",
				"label": "Деятельность агентов по оптовой торговле текстильными изделиями, одеждой, обувью, изделиями из кожи и меха"
			  },
			  {
				"code": "46.32.3",
				"label": "Торговля оптовая консервами из мяса и мяса птицы"
			  },
			  {
				"code": "46.32",
				"label": "Торговля оптовая мясом и мясными продуктами"
			  },
			  {
				"code": "46.13",
				"label": "Деятельность агентов по оптовой торговле лесоматериалами и строительными материалами"
			  },
			  {
				"code": "52.24",
				"label": "Транспортная обработка грузов"
			  },
			  {
				"code": "46.17",
				"label": "Деятельность агентов по оптовой торговле пищевыми продуктами, напитками и табачными изделиями"
			  },
			  {
				"code": "46.38",
				"label": "Торговля оптовая прочими пищевыми продуктами, включая рыбу, ракообразных и моллюсков"
			  },
			  {
				"code": "46.36",
				"label": "Торговля оптовая сахаром, шоколадом и сахаристыми кондитерскими изделиями"
			  },
			  {
				"code": "46.73",
				"label": "Торговля оптовая лесоматериалами, строительными материалами и санитарно-техническим оборудованием"
			  },
			  {
				"code": "46.11",
				"label": "Деятельность агентов по оптовой торговле сельскохозяйственным сырьем, живыми животными, текстильным сырьем и полуфабрикатами"
			  },
			  {
				"code": "49.41.2",
				"label": "Перевозка грузов неспециализированными автотранспортными средствами"
			  },
			  {
				"code": "45.20",
				"label": "Техническое обслуживание и ремонт автотранспортных средств"
			  },
			  {
				"code": "46.12",
				"label": "Деятельность агентов по оптовой торговле топливом, рудами, металлами и химическими веществами"
			  },
			  {
				"code": "71.20.5",
				"label": "Технический осмотр автотранспортных средств"
			  },
			  {
				"code": "46.14",
				"label": "Деятельность агентов по оптовой торговле машинами, промышленным оборудованием, судами и летательными аппаратами"
			  },
			  {
				"code": "46.66",
				"label": "Торговля оптовая прочей офисной техникой и оборудованием"
			  },
			  {
				"code": "46.72",
				"label": "Торговля оптовая металлами и металлическими рудами"
			  }
			],
			"@updated": "2018-05-08T19:56:08.695Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "028bf9f78d394715b0218d4e224ebd77",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "ВАГНЕР АВТО ПАРТС",
			"@created": "2018-05-08T19:56:08.379Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678050874",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"ВАГНЕР АВТО ПАРТС\"",
			  "ООО \"ВАГНЕР АВТО ПАРТС\""
			],
			"bst:businessClassifier": [
			  {
				"code": "45.3",
				"label": "Торговля автомобильными деталями, узлами и принадлежностями"
			  },
			  {
				"code": "62.09",
				"label": "Деятельность, связанная с использованием вычислительной техники и информационных технологий, прочая"
			  },
			  {
				"code": "45.20",
				"label": "Техническое обслуживание и ремонт автотранспортных средств"
			  },
			  {
				"code": "45.1",
				"label": "Торговля автотранспортными средствами"
			  },
			  {
				"code": "47.9",
				"label": "Торговля розничная вне магазинов, палаток, рынков"
			  },
			  {
				"code": "79.11",
				"label": "Деятельность туристических агентств"
			  },
			  {
				"code": "45.40",
				"label": "Торговля мотоциклами, их деталями, узлами и принадлежностями; техническое обслуживание и ремонт мотоциклов"
			  },
			  {
				"code": "52.29",
				"label": "Деятельность вспомогательная прочая, связанная с перевозками"
			  },
			  {
				"code": "47.79",
				"label": "Торговля розничная бывшими в употреблении товарами в магазинах"
			  },
			  {
				"code": "81.29.9",
				"label": "Деятельность по чистке и уборке прочая, не включенная в другие группировки"
			  },
			  {
				"code": "46.90",
				"label": "Торговля оптовая неспециализированная"
			  },
			  {
				"code": "47.99",
				"label": "Торговля розничная прочая вне магазинов, палаток, рынков"
			  }
			],
			"@updated": "2018-05-08T19:56:08.379Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "025e029cecd24896882e2925bbcb7396",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "СПХ АБРАМОВСКОЕ",
			"@created": "2018-05-08T19:56:12.349Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678058866",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"СПХ АБРАМОВСКОЕ\"",
			  "ООО \"СПХ АБРАМОВСКОЕ\""
			],
			"bst:businessClassifier": [
			  {
				"code": "01.2",
				"label": "Выращивание многолетних культур"
			  },
			  {
				"code": "01.41",
				"label": "Разведение молочного крупного рогатого скота, производство сырого молока"
			  },
			  {
				"code": "01.42",
				"label": "Разведение прочих пород крупного рогатого скота и буйволов, производство спермы"
			  },
			  {
				"code": "10.11",
				"label": "Переработка и консервирование мяса"
			  },
			  {
				"code": "01.47",
				"label": "Разведение сельскохозяйственной птицы"
			  },
			  {
				"code": "46.31.11",
				"label": "Торговля оптовая свежим картофелем"
			  },
			  {
				"code": "47.9",
				"label": "Торговля розничная вне магазинов, палаток, рынков"
			  },
			  {
				"code": "46.32.3",
				"label": "Торговля оптовая консервами из мяса и мяса птицы"
			  },
			  {
				"code": "10.13",
				"label": "Производство продукции из мяса убойных животных и мяса птицы"
			  },
			  {
				"code": "01.61",
				"label": "Предоставление услуг в области растениеводства"
			  },
			  {
				"code": "01.49",
				"label": "Разведение прочих животных"
			  },
			  {
				"code": "47.22",
				"label": "Торговля розничная мясом и мясными продуктами в специализированных магазинах"
			  },
			  {
				"code": "10.51",
				"label": "Производство молока (кроме сырого) и молочной продукции"
			  },
			  {
				"code": "01.50",
				"label": "Смешанное сельское хозяйство"
			  },
			  {
				"code": "01.62",
				"label": "Предоставление услуг в области животноводства"
			  },
			  {
				"code": "47.21",
				"label": "Торговля розничная фруктами и овощами в специализированных магазинах"
			  },
			  {
				"code": "46.31",
				"label": "Торговля оптовая фруктами и овощами"
			  },
			  {
				"code": "46.32",
				"label": "Торговля оптовая мясом и мясными продуктами"
			  },
			  {
				"code": "10.12",
				"label": "Производство и консервирование мяса птицы"
			  },
			  {
				"code": "47.99",
				"label": "Торговля розничная прочая вне магазинов, палаток, рынков"
			  }
			],
			"@updated": "2018-05-08T19:56:12.349Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "6584554daa6f4a2d828cd6fd0544583c",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "МЕБЕЛЬЩИКЪ",
			"@created": "2018-05-08T19:56:12.202Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678054967",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"МЕБЕЛЬЩИКЪ\"",
			  "ООО \"МЕБЕЛЬЩИКЪ\""
			],
			"bst:businessClassifier": [
			  {
				"code": "31.01",
				"label": "Производство мебели для офисов и предприятий торговли"
			  },
			  {
				"code": "46.15.1",
				"label": "Деятельность агентов по оптовой торговле мебелью"
			  },
			  {
				"code": "46.65",
				"label": "Торговля оптовая офисной мебелью"
			  },
			  {
				"code": "31.09",
				"label": "Производство прочей мебели"
			  },
			  {
				"code": "47.59.1",
				"label": "Торговля розничная мебелью в специализированных магазинах"
			  },
			  {
				"code": "46.15",
				"label": "Деятельность агентов по оптовой торговле мебелью, бытовыми товарами, скобяными, ножевыми и прочими металлическими изделиями"
			  },
			  {
				"code": "46.47.1",
				"label": "Торговля оптовая бытовой мебелью"
			  },
			  {
				"code": "47.59",
				"label": "Торговля розничная мебелью, осветительными приборами и прочими бытовыми изделиями в специализированных магазинах"
			  },
			  {
				"code": "31.01",
				"label": "Производство мебели для офисов и предприятий торговли"
			  },
			  {
				"code": "46.47",
				"label": "Торговля оптовая мебелью, коврами и осветительным оборудованием"
			  }
			],
			"@updated": "2018-05-08T19:56:12.202Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "5629b9e09cd3433182288b0fcf203267",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "МИР СЕРВИСА 777",
			"@created": "2018-05-08T19:56:19.911Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678055400",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"МИР СЕРВИСА 777\"",
			  "ООО \"МИР СЕРВИСА 777\""
			],
			"bst:businessClassifier": [
			  {
				"code": "31.01",
				"label": "Производство мебели для офисов и предприятий торговли"
			  },
			  {
				"code": "43.31",
				"label": "Производство штукатурных работ"
			  },
			  {
				"code": "95.11",
				"label": "Ремонт компьютеров и периферийного компьютерного оборудования"
			  },
			  {
				"code": "52.21",
				"label": "Деятельность вспомогательная, связанная с сухопутным транспортом"
			  },
			  {
				"code": "43.33",
				"label": "Работы по устройству покрытий полов и облицовке стен"
			  },
			  {
				"code": "86.90.9",
				"label": "Деятельность в области медицины прочая, не включенная в другие группировки"
			  },
			  {
				"code": "52.24",
				"label": "Транспортная обработка грузов"
			  },
			  {
				"code": "71.12.6",
				"label": "Деятельность в области технического регулирования, стандартизации, метрологии, аккредитации, каталогизации продукции"
			  },
			  {
				"code": "31.01",
				"label": "Производство мебели для офисов и предприятий торговли"
			  },
			  {
				"code": "96.04",
				"label": "Деятельность физкультурно- оздоровительная"
			  },
			  {
				"code": "53.10",
				"label": "Деятельность почтовой связи общего пользования"
			  },
			  {
				"code": "43.34",
				"label": "Производство малярных и стекольных работ"
			  },
			  {
				"code": "95.22.1",
				"label": "Ремонт бытовой техники"
			  },
			  {
				"code": "31.09",
				"label": "Производство прочей мебели"
			  },
			  {
				"code": "47.99",
				"label": "Торговля розничная прочая вне магазинов, палаток, рынков"
			  },
			  {
				"code": "47.7",
				"label": "Торговля розничная прочими товарами в специализированных магазинах"
			  },
			  {
				"code": "71.12.5",
				"label": "Деятельность в области гидрометеорологии и смежных с ней областях, мониторинга состояния окружающей среды, ее загрязнения"
			  },
			  {
				"code": "82.99",
				"label": "Деятельность по предоставлению прочих вспомогательных услуг для бизнеса, не включенная в другие группировки"
			  },
			  {
				"code": "52.29",
				"label": "Деятельность вспомогательная прочая, связанная с перевозками"
			  },
			  {
				"code": "31.02",
				"label": "Производство кухонной мебели"
			  },
			  {
				"code": "47.9",
				"label": "Торговля розничная вне магазинов, палаток, рынков"
			  },
			  {
				"code": "96.02",
				"label": "Предоставление услуг парикмахерскими и салонами красоты"
			  },
			  {
				"code": "47.59",
				"label": "Торговля розничная мебелью, осветительными приборами и прочими бытовыми изделиями в специализированных магазинах"
			  },
			  {
				"code": "43.32",
				"label": "Работы столярные и плотничные"
			  },
			  {
				"code": "71.1",
				"label": "Деятельность в области архитектуры, инженерных изысканий и предоставление технических консультаций в этих областях"
			  },
			  {
				"code": "95.29",
				"label": "Ремонт прочих предметов личного потребления и бытовых товаров"
			  },
			  {
				"code": "43.39",
				"label": "Производство прочих отделочных и завершающих работ"
			  }
			],
			"@updated": "2018-05-08T19:56:19.911Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "c98ceb5a7b914f2e82c7942d0f3245a4",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "ШОУ МЭДЖИК",
			"@created": "2018-05-08T19:56:19.937Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678061146",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"ШОУ МЭДЖИК\"",
			  "ООО \"ШОУ МЭДЖИК\""
			],
			"bst:businessClassifier": [
			  {
				"code": "93.29.2",
				"label": "Деятельность танцплощадок, дискотек, школ танцев"
			  },
			  {
				"code": "73.11",
				"label": "Деятельность рекламных агентств"
			  },
			  {
				"code": "56.30",
				"label": "Подача напитков"
			  },
			  {
				"code": "47.25.2",
				"label": "Торговля розничная безалкогольными напитками в специализированных магазинах"
			  },
			  {
				"code": "90.04",
				"label": "Деятельность учреждений культуры и искусства"
			  },
			  {
				"code": "63.91",
				"label": "Деятельность информационных агентств"
			  },
			  {
				"code": "93.29.9",
				"label": "Деятельность зрелищно-развлекательная прочая, не включенная в другие группировки"
			  },
			  {
				"code": "90.01",
				"label": "Деятельность в области исполнительских искусств"
			  },
			  {
				"code": "56.10.1",
				"label": "Деятельность ресторанов и кафе с полным ресторанным обслуживанием, кафетериев, ресторанов быстрого питания и самообслуживания"
			  },
			  {
				"code": "47.25.1",
				"label": "Торговля розничная алкогольными напитками, включая пиво, в специализированных магазинах"
			  },
			  {
				"code": "90.03",
				"label": "Деятельность в области художественного творчества"
			  },
			  {
				"code": "47.26",
				"label": "Торговля розничная табачными изделиями в специализированных магазинах"
			  },
			  {
				"code": "47.25",
				"label": "Торговля розничная напитками в специализированных магазинах"
			  },
			  {
				"code": "56.10",
				"label": "Деятельность ресторанов и услуги по доставке продуктов питания"
			  },
			  {
				"code": "56.10.3",
				"label": "Деятельность ресторанов и баров по обеспечению питанием в железнодорожных вагонахресторанах и на судах"
			  },
			  {
				"code": "82.99",
				"label": "Деятельность по предоставлению прочих вспомогательных услуг для бизнеса, не включенная в другие группировки"
			  },
			  {
				"code": "18.12",
				"label": "Прочие виды полиграфической деятельности"
			  },
			  {
				"code": "47.25.12",
				"label": "Торговля розничная пивом в специализированных магазинах"
			  },
			  {
				"code": "93.29",
				"label": "Деятельность зрелищно-развлекательная прочая"
			  },
			  {
				"code": "58.11.1",
				"label": "Издание книг, брошюр, рекламных буклетов и аналогичных изданий, включая издание словарей и энциклопедий, в том числе для слепых, в печатном виде"
			  }
			],
			"@updated": "2018-05-08T19:56:19.937Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-08-01T00:00:00.000",
			"@identifier": "ec7c603961a64324ab7a49e6c4166aa7",
			"mdaas:RegisteredAddress": [
			  {
				"city": "ЕКАТЕРИНБУРГ",
				"country": "Russia",
				"fullAddress": "ЕКАТЕРИНБУРГ Russia"
			  }
			],
			"vcard:organization-name": "БЕКА",
			"@created": "2018-05-08T19:56:19.631Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "6678058672",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"БЕКА\"",
			  "ООО \"БЕКА\""
			],
			"bst:businessClassifier": [
			  {
				"code": "47.71.3",
				"label": "Торговля розничная изделиями из меха в специализированных магазинах"
			  },
			  {
				"code": "46.16",
				"label": "Деятельность агентов по оптовой торговле текстильными изделиями, одеждой, обувью, изделиями из кожи и меха"
			  },
			  {
				"code": "47.72",
				"label": "Торговля розничная обувью и изделиями из кожи в специализированных магазинах"
			  },
			  {
				"code": "47.71.4",
				"label": "Торговля розничная одеждой из кожи в специализированных магазинах"
			  },
			  {
				"code": "46.24",
				"label": "Торговля оптовая шкурами и кожей"
			  },
			  {
				"code": "46.42.13",
				"label": "Торговля оптовая изделиями из меха"
			  }
			],
			"@updated": "2018-05-08T19:56:19.631Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  },
		  {
			"@status": "CREATED",
			"isDomiciledIn": "RU",
			"hasLatestOrganizationFoundedDate": "2016-11-10T00:00:00.000",
			"@identifier": "1c3cc45cfae34a81b68025936e9f3e82",
			"mdaas:RegisteredAddress": [
			  {
				"city": "КАЗАНЬ",
				"country": "Russia",
				"fullAddress": "КАЗАНЬ Russia"
			  }
			],
			"vcard:organization-name": "ТАТРЕГСТРОЙ",
			"@created": "2018-05-08T19:56:21.331Z",
			"@source-id": "org-repository.registry-importer.ru",
			"bst:registrationId": "1657230348",
			"bst:aka": [
			  "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"ТАТРЕГСТРОЙ\"",
			  "ООО \"ТАТРЕГСТРОЙ\""
			],
			"bst:businessClassifier": [
			  {
				"code": "46.73",
				"label": "Торговля оптовая лесоматериалами, строительными материалами и санитарно-техническим оборудованием"
			  },
			  {
				"code": "47.52.3",
				"label": "Торговля розничная стеклом в специализированных магазинах"
			  },
			  {
				"code": "47.52.74",
				"label": "Торговля розничная сборными деревянными строениями в специализированных магазинах"
			  },
			  {
				"code": "47.52.71",
				"label": "Торговля розничная пиломатериалами в специализированных магазинах"
			  },
			  {
				"code": "47.52.6",
				"label": "Торговля розничная садово-огородной техникой и инвентарем в специализированных магазинах"
			  },
			  {
				"code": "47.52.2",
				"label": "Торговля розничная лакокрасочными материалами в специализированных магазинах"
			  },
			  {
				"code": "47.52.7",
				"label": "Торговля розничная строительными материалами, не включенными в другие группировки, в специализированных магазинах"
			  },
			  {
				"code": "47.52.79",
				"label": "Торговля розничная прочими строительными материалами, не включенными в другие группировки, в специализированных магазинах"
			  },
			  {
				"code": "46.74",
				"label": "Торговля оптовая скобяными изделиями, водопроводным и отопительным оборудованием и принадлежностями"
			  },
			  {
				"code": "47.52.73",
				"label": "Торговля розничная металлическими и неметаллическими конструкциями в специализированных магазинах"
			  },
			  {
				"code": "47.52.1",
				"label": "Торговля розничная скобяными изделиями в специализированных магазинах"
			  },
			  {
				"code": "47.52",
				"label": "Торговля розничная скобяными изделиями, лакокрасочными материалами и стеклом в специализированных магазинах"
			  },
			  {
				"code": "46.90",
				"label": "Торговля оптовая неспециализированная"
			  },
			  {
				"code": "47.52.72",
				"label": "Торговля розничная кирпичом в специализированных магазинах"
			  },
			  {
				"code": "47.52.5",
				"label": "Торговля розничная санитарно-техническим оборудованием в специализированных магазинах"
			  }
			],
			"@updated": "2018-05-08T19:56:21.331Z",
			"lei:legalForm": {
			  "code": "N/A",
			  "label": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"
			}
		  }
		],
		"total": 17519764
	  }
	)
	.constant('getAllCountriesNames',["CR","TG","TJ","ZA","IM","PE","LC","CH","RU","MP","CK","SI","AU","KR","IT","FI","GF","SC","SX","ZZ","TT","TK","MY","SY","MN","TF","KP","AM","DZ","UY","TD","DJ","BI","MK","MU","LI","NU","GR","GY","CG","NF","ML","AX","GM","SA","CX","BH","NE","BN","XK","MF","CD","DK","BJ","ME","SJ","BO","JO","CV","VE","CI","UZ","TN","IS","EH","TS","TM","GA","LS","TZ","AT","LT","NP","BG","IL","GU","PK","PT","HR","VU","PF","BM","MR","GE","HU","TW","MM","VG","YE","SR","PN","VA","AN","PR","KW","SE","GB","UM","VN","CF","PA","VC","JP","IR","AF","LY","MZ","RO","QA","CM","GG","BY","SD","BQ","MO","KY","AR","BR","ZW","NR","NZ","AW","FJ","ID","SV","CN","FM","HT","CC","RW","BA","TL","JM","KM","KE","WS","TO","PY","SH","CY","GH","MA","SG","LK","PH","SM","WF","TR","PS","BZ","CU","TV","AD","SB","DM","LR","OM","SO","DO","AL","BL","FR","GW","MS","BB","CA","MG","KH","LA","GP","BV","HN","TH","DE","LB","KZ","AS","EC","NO","AO","FK","ET","GS","MD","AG","BE","MV","SZ","CZ","CL","BT","NL","EG","MQ","SN","FO","EE","AQ","ST","KN","BW","MH","NI","PG","VI","IQ","KG","US","ZM","MC","GI","NC","GT","BF","YT","LU","UA","IE","LV","GD","MW","BS","AZ","SK","GQ","TC","RE","IN","ES","GL","KI","HK","CO","SS","RS","IO","NG","UG","CW","SL","ER","JE","AE","HM","PM","BD","MT","AI","GN","PW","NA","MX","PL"])
	.constant('getAllIndustriesNames',["Electric Utilities & IPPs","Metals & Mining","Passenger Transportation Services","Multiline Utilities","Industrial Conglomerates","Food & Drug Retailing","Diversified Retail","Collective Investments","Communications & Networking","Banking Services","Personal & Household Products & Services","Textiles & Apparel","Specialty Retailers","Containers & Packaging","Biotechnology & Medical Research","Machinery, Equipment & Components","Hotels & Entertainment Services","Oil & Gas Related Equipment and Services","Media & Publishing","Insurance","Residential & Commercial REITs","Office Equipment","Household Goods","Automobiles & Auto Parts","Real Estate Operations","Software & IT Services","Professional & Commercial Services","Telecommunications Services","Holding Companies","Aerospace & Defense","Coal","Healthcare Providers & Services","Beverages","Paper & Forest Products","Investment Banking & Investment Services","Uranium","Pharmaceuticals","Renewable Energy","Electronic Equipment & Parts","Homebuilding & Construction Supplies","Diversified Trading & Distributing","Leisure Products","Computers, Phones & Household Electronics","Construction & Engineering","Chemicals","Construction Materials","Transport Infrastructure","Freight & Logistics Services","Oil & Gas","Semiconductors & Semiconductor Equipment","Healthcare Equipment & Supplies","Food & Tobacco","Water Utilities","Natural Gas Utilities"])
	.constant('getbusinessSectorsData',["Industrial Conglomerates","Food & Drug Retailing","Collective Investments","Food & Beverages","Applied Resources","Real Estate","Personal & Household Products & Services","Transportation","Technology Equipment","Energy - Fossil Fuels","Retailers","Industrial & Commercial Services","Healthcare Services","Insurance","Automobiles & Auto Parts","Software & IT Services","Mineral Resources","Telecommunications Services","Cyclical Consumer Products","Holding Companies","Pharmaceuticals & Medical Research","Uranium","Renewable Energy","Industrial Goods","Chemicals","Banking & Investment Services","Utilities","Cyclical Consumer Services"])
	.constant('getAllEconomicSectorsData',["Technology","Basic Materials","Consumer Non-Cyclicals","Consumer Cyclicals","Healthcare","Energy","Industrials","Telecommunications Services","Utilities","Financials"])
	.constant('getAllDirectorRoles',["Non-Executive Director","Trustee","Director","Non-Executive Vice Chairman","Independent Director","Non-Executive Independent Director","Non-Executive Chairman","Supervisory Board","Lead Director","Executive Director"])
	.constant('getAllOfficerRoles',["Executive Vice President - Finance","Chairman of the Shariah Supervisory Board","Managing Partner","Primary Contact","Director of Investor Relations","Director of Sales","Vice Chairman - Supervisory","Senior Partner","Director General","Venture Partner","General Manager","Vice President - Research & Development","Deputy Chief Executive Officer","Chief Information Officer","Associate","Corporate Executive","Partner","Chief Risk Officer","Vice President","Co-Founder","Co-Chairman","Deputy Managing Director","General Counsel","Deputy General Director","Finance Director","Senior Vice President","Treasurer","Chief Accounting Officer","Executive Partner","Managing Principal","Executive Board","General Partner","Senior Vice President - Finance","Vice President - Business Development","Senior Executive Officer","Director of Human Resources","Senior Director","Chief Technology Officer","Secretary","Chairman","Vice Chairman - Management","Chairman - Supervisory","Chief Administrative Officer","Executive Officer","Chief Human Resource Officer","Investment Manager","Secretary to the Shariah Supervisory Board","Operating Partner","Senior Managing Director","President","Chief Marketing Officer","Chairman - Management","Chief Executive Officer","Deputy General Manager","Management Board","Vice President - Finance","Executive Vice President","Co-President","Assistant Treasurer","IR Contact","Vice President - Operations","Vice Chairman of the Shariah Supervisory Board","Founder","Co-Chief Executive Officer","Senior Managing Executive Officer","Vice Chairman","Chief Operating Officer","General Director","Vice President - Administration","Chief Compliance Officer","Controller","Chief Investment Officer","Senior Principal","Managing Director","Member of the Shariah Supervisory Board","Chief Scientific Officer","Principal","Contact Person","Assistant Secretary","Executive Director","Managing Executive Officer","Director of Marketing","Other","Chief Financial Officer","Senior Associate","Advisor to the Shariah Supervisory Board","Legal Representative"])
	.constant('getOrganationNamesSuggestions',{"hits":[{"vcard:organization-name":"Microsoft Microsoft","@identifier":"5f20e2cb37ed4b4c905aa39422e6b761"},{"vcard:organization-name":"YouTube/Microsoft Microsoft","@identifier":"04377e42f6084b24bfe26391c8ff2af3"},{"vcard:organization-name":"Microsoft","@identifier":"e15dc512e8cc4603a1fe31980ed80dfa"},{"vcard:organization-name":"Microsoft Microsoft Corp.","@identifier":"b65f06b9cb174782b4ca4e773f7785a0"},{"vcard:organization-name":"MIcrosoft","@identifier":"4c1f3f6e4bb14b4db2e7a3e3cf2c14ea"},{"vcard:organization-name":"Microsoft­­","@identifier":"d8cd2cde6b394faebeb398de76aa7f41"},{"vcard:organization-name":"Microsoft","@identifier":"0838ae3fa0504c458eeeb5a51374fdaa"},{"vcard:organization-name":"MicroSoft","@identifier":"12f21caef9174906bcc0a1e50e55d749"},{"vcard:organization-name":"Microsoft","@identifier":"04e6e9850b7644229803e9adbb3388dc","isDomiciledIn":"BE","hasLatestOrganizationFoundedDate":"1989-07-24T00:00:00.000"},{"vcard:organization-name":"MICROSOFT","@identifier":"200da73a1341417498377e379004743d","isDomiciledIn":"BE","hasLatestOrganizationFoundedDate":"1998-01-01T00:00:00.000"}],"total":1760})
	.constant('getPersonNamesSuggestions',{"hits":[{"vcard:hasName":"Abu Al Athir Amr Al Absi","@identifier":"e99726d9ddae4d3ebc2b1522bed406e4"},{"vcard:hasName":"Al Absi Abu Al Athir Amr","@identifier":"5721fef7beb14e11bc5b10341b578c34"},{"vcard:hasName":"A. A. Ayida","@identifier":"817833c5b1b9462c8788c8c99d326827"},{"vcard:hasName":"Ali A’ata","@identifier":"5869dce1302b4e02983a354c8b59b199"},{"vcard:hasName":"Abu Ala’a","@identifier":"4c8bc5ccc1a6479eb2db614dbb4f6033"},{"vcard:hasName":"A. A. Atoll","@identifier":"a62b824fd7f54db78fd76a1b945ad25c"},{"vcard:hasName":"A.L.A. Aziz","@identifier":"8dd54e1dd73f4a58bb246ceec1c571eb"},{"vcard:hasName":"A.A. Azula","@identifier":"775fe20d5b054a04842e119b6cdc8cba"},{"vcard:hasName":"Ala'a Abo Alrob","@identifier":"0a5edc5401b645d993676c21e7651435"},{"vcard:hasName":"Abu Abd Al-‘Aziz ","@identifier":"e33bc40c79c347fea2a806d60c6758c0"}],"total":2192014})
	.constant('personIdentifier',{"vcard:hasAddress":{"streetAddress":"Beech Hyde Farm, Dyke Lane Wheathampstead","city":"St. Albans","zip":"AL4 8EN","country":"England","fullAddress":"Beech Hyde Farm, Dyke Lane Wheathampstead St. Albans AL4 8EN England"},"@status":"CREATED","vcard:hasName":"Michal Michal","vcard:honorific-prefix":"Mr","@identifier":"8be64a4da495403b80677773b342e70b","@is-manual":"false","vcard:bday":"1987-05-01T00:00:00.000","vcard:family-name":"Michal","@created":"2018-06-19T09:17:17.740Z","@source-id":"companies-house-psc","vcard:given-name":"Michal","bst:nationality":["British"],"@updated":"2018-06-19T09:17:17.740Z"})
	.constant('organisationIdentifier',{"@status":"CREATED","isDomiciledIn":"RU","hasLatestOrganizationFoundedDate":"2016-08-01T00:00:00.000","@identifier":"ec7c603961a64324ab7a49e6c4166aa7","mdaas:RegisteredAddress":[{"city":"ЕКАТЕРИНБУРГ","country":"Russia","fullAddress":"ЕКАТЕРИНБУРГ Russia"}],"vcard:organization-name":"БЕКА","@created":"2018-05-08T19:56:19.631Z","@source-id":"org-repository.registry-importer.ru","bst:registrationId":"6678058672","bst:aka":["ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"БЕКА\"","ООО \"БЕКА\""],"bst:businessClassifier":[{"code":"47.71.3","label":"Торговля розничная изделиями из меха в специализированных магазинах"},{"code":"46.16","label":"Деятельность агентов по оптовой торговле текстильными изделиями, одеждой, обувью, изделиями из кожи и меха"},{"code":"47.72","label":"Торговля розничная обувью и изделиями из кожи в специализированных магазинах"},{"code":"47.71.4","label":"Торговля розничная одеждой из кожи в специализированных магазинах"},{"code":"46.24","label":"Торговля оптовая шкурами и кожей"},{"code":"46.42.13","label":"Торговля оптовая изделиями из меха"}],"@updated":"2018-05-08T19:56:19.631Z","lei:legalForm":{"code":"N/A","label":"ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ"}});