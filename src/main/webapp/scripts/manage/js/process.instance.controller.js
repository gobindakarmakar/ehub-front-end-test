'use strict';
angular.module('ehubApp')
	   .controller('ProcessInstanceController', processInstanceController);

		processInstanceController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModal'
		];
		
		function processInstanceController(
				$scope, 
				$rootScope,
				$uibModal) {
    
		
		$scope.openModal = function(){
			var openModalInstance = $uibModal.open({
				templateUrl: 'scripts/manage/modal/views/candidateInfoModal.html',
				controller: 'CandidateInfoModalController',
				backdrop: 'static',
				windowClass: 'custom-modal request-candidate-modal'
			});
			openModalInstance.result.then(function(){
				
			});
		};
}