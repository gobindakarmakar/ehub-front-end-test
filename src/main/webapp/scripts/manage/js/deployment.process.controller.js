'use strict';
angular.module('ehubApp')
	   .controller('DeploymentProcessController', deploymentProcessController);

		deploymentProcessController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModal'
		];
		
		function deploymentProcessController(
				$scope, 
				$rootScope,
				$uibModal) {
			
			$scope.openModal = function(){
				var openModalInstance = $uibModal.open({
					templateUrl: 'scripts/manage/modal/views/deploymentProcessModal.html',
					controller: 'DeploymentProcessModalController',
					backdrop: 'static',
					windowClass: 'custom-modal deployment-process-modal'
				});
				openModalInstance.result.then(function(){
					
				});
			};
		}