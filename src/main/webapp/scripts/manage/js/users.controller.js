'use strict';
angular.module('ehubApp')
	   .controller('UserController', userController);

			userController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function userController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			
			$scope.user = {
					 userData:[
						    {
							    "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
							
						    },{  "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
						    },{ "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
						    },{ "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
						    },{ "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
						    },{ "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"
						    },{ "Id":"1",
								"Name":"Engineering",
								"Type":"Assignment",
								"Action":"fa fa-times light-red"}
							],
					 userData2:[
						    {
						    	'image':"assets/images/activity/peter.jpg",
						    	'altname':'peter',
						    	'name':'James D Palmer',
						    	'role':'Administrator',
						    	'active':'active'
						    },
						    {
						    	'image':"assets/images/activity/ontario.jpg",
						    	'altname':'ontario',
						    	'name':'Miranda Taylor',
						    	'role':'Manager',
						    	
						    },
						    {
						    	'image':"assets/images/activity/alberta.jpg",
						    	'altname':'alberta',
						    	'name':'Victoria Diaz',
						    	'role':'User',
						    
						    }
					 ]
					  
					};
			
		}