'use strict';
angular.module('ehubApp')
	   .controller('ManageDbController', manageDbController);

		manageDbController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function manageDbController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.manageDb = {
					manageDbData:[
						    {
							    "Id":"1",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"2",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"3",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"4",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"5",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"6",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"7",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"8",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"9",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    } ,{
							    "Id":"10",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }, {
							    "Id":"11",
								"Rev_":"Engineering",
								"Name_":"Management",
								"Type_":"Assignment"
							
						    }],
				manageDbData2:[{
								'name':'Act_EVT_Log',
								'value':'14',
								'class':''
							},{
								'name':'Act_Bpt_Bytearry',
								'value':'23',
								'class':''
							},{
								'name':'Act_GE_Property',
								'value':'325',
								'class':'fa fa-cog'
							},{
								'name':'Act_BPT_Linkarry',
								'value':'89',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Taskinst',
								'value':'127',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Bytearray',
								'value':'456',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Propert',
								'value':'323',
								'class':'fa fa-user'
							},{
								'name':'Act_GE_Bytearray',
								'value':'34',
								'class':'fa fa-user'
							},{
								'name':'Act_GE_Property',
								'value':'16',
								'class':'fa fa-file-o'
							}]

			};
		}
		
	