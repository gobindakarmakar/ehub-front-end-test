'use strict';
angular.module('ehubApp')
	.controller('StagingMipController', stagingMipController);
stagingMipController.$inject = [
	'$scope',
	'$state',
	'$rootScope',
	'$uibModal',
	'DTOptionsBuilder',
	'DTColumnDefBuilder',
	'$http',
	'$q',
	'entity_org_search',
	'DataCurationApiService',
	'DTDefaultOptions'
];
function stagingMipController(
	$scope,
	$state,
	$rootScope,
	$uibModal,
	DTOptionsBuilder,
	DTColumnDefBuilder,
	$http,
	$q,
	entity_org_search,
	DataCurationApiService,/*jshint ignore:line*/
	DTDefaultOptions) {/*jshint ignore:line*/
	var count = 0;
	//initialzing staging variables
	$scope.staging = {
		isLoading: true,
		list: [],
		dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10)
			.withOption('bFilter', false)
			.withOption('lengthChange', false).withOption('initComplete', function () {
				count++;
				if (count > 1)
					{$scope.staging.isLoading = false;}
			}),
		dtColumnDefs: [
			DTColumnDefBuilder.newColumnDef(5).notSortable()
		],
		onClickAddStaging: onClickAddStaging,
		onClickRemoveStaging: onClickRemoveStaging,
		onClickEditStaging: onClickEditStaging,
		onClickViewEntity: onClickViewEntity
	};

	$scope.staging.isLoading = true;

	$scope.searchEntity = {
		entityQuery: "",
		entitySize: "10",
		searchQuery: searchQuery
	};

	//    $scope.persons = {
	//    		isLoading: true,
	//        	list: [],
	//        	dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10)
	//    		        .withOption('bFilter', true)
	//    		        .withOption('lengthChange', false),
	//            dtColumnDefs: [
	//    		    DTColumnDefBuilder.newColumnDef(4).notSortable()
	//    		],
	//        	onClickAddStaging: onClickAddStaging,
	//        	onClickRemoveStaging: onClickRemoveStaging,
	//        	onClickEditStaging: onClickEditStaging
	//        };


    /*
     * @purpose: Getting person details
     * @created: 6th Feb 2018
     * @params: params
     * @return: no
     * @author: Zameer
     */

	//    function getPersonData(){
	//		DataCurationApiService.getPersonDetails().then(function(response){
	//			angular.forEach(response.data.hits,function(d,i){
	//				var key = {
	//						"name":d["vcard:hasName"],
	//						"nationality":d["bst:nationality"],
	//						"email":d["bst:email"],
	//						"type": "person",
	//						"url":d["vcard:hasURL"],
	//						"identifier":d["@identifier"],
	//						"familyname" : d["vcard:family-name"],
	//						gender : d["vcard:hasGender"],
	//						status:d["@status"]
	//
	//				}
	//				$scope.persons.list.push(key);
	//			})
	//		}, function(error){
	//			HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
	//		})
	//	}
	//
	//	getPersonData();

	/*   getPersonData end   */

    /*
     * @purpose: get list of staging data
     * @created: 05 oct 2017
     * @params: no
     * @return: no
     * @author: sandeep
     */
	//    function getListOfStaging(){
	//    	$scope.staging.isLoading = false;
	////    	MIPApiService.getAllStaging().then(function(response){
	////    		console.log(response);
	////    		$scope.staging.list = response.data.data;
	////    	}, function(error){
	////    		console.log(error);
	////    	});
	//    }
	//
	//    getListOfStaging();
	//
    /*
     * @purpose: onClickAddStaging function
     * @created: 06 oct 2017
     * @params: no
     * @return: no
     * @author: sandeep
     */
	function onClickAddStaging() {
		var addStagingModalInstance = $uibModal.open({
			templateUrl: 'static/js/modal/views/add-staging-modal.html',
			controller: 'AddStagingModalController',
			windowClass: 'update-entities-modal related-person-modal editStaging-entity-modal addWidth',
			backdrop: 'static'
			//		    resolve: {
			//		    	StagingFormSchemaJSON: function(){
			//		    		return getJSONData('static/data/staging.form.schema.json');
			//		    	},
			//		    	StagingFormJSON: function(){
			//		    		return getJSONData('static/data/staging.form.json');
			//		    	}
			//		    }
		});
		addStagingModalInstance.result.then(function () {
		}, function (error) {
			console.log(error);
		});
	}
    /*
     * @purpose: onClickRemoveStaging function
     * @created: 06 oct 2017
     * @params: index(number)
     * @return: no
     * @author: sandeep
     */
	function onClickRemoveStaging() {
		var removeStagingModalInstance = $uibModal.open({
			templateUrl: 'static/js/modal/views/remove-staging-modal.html',
			controller: 'RemoveStagingModalController',
			windowClass: 'update-entities-modal related-person-modal removeStaging-entity-modal addWidth',
			backdrop: 'static'
		});
		removeStagingModalInstance.result.then(function (response) {
			console.log(response);
		}, function (error) {
			console.log(error);
		});
	}
    /*
     * @purpose: onClickEditStaging function
     * @created: 05 oct 2017
     * @params: index
     * @return: no
     * @author: sandeep
     */
	function onClickEditStaging(stage) {
		$scope.staging.isLoading = true;
		$scope.$apply;/*jshint ignore:line*/
		var type1;
		if (stage.type == "Company") {
			type1 = "#02";
		} else {
			type1 = "#01";
		}
		$state.go('addDataCuration', { 'identifier': stage.identifier + type1 });

		//    	var editStagingModalInstance = $uibModal.open({
		//		    templateUrl: 'static/js/modal/views/add-staging-modal.html',
		//		    controller: 'EditStagingModalController',
		//		    windowClass: 'update-entities-modal related-person-modal editStaging-entity-modal addWidth',
		//		    backdrop: 'static',
		//		    resolve: {
		////		    	StagingFormSchemaJSON: function(){
		////		    		return getJSONData('static/data/staging.form.schema.json');
		////		    	},
		////		    	StagingFormJSON: function(){
		////		    		return getJSONData('static/data/staging.form.json');
		////		    	},
		//		    	isEditView: function(){
		//		    		return true;
		//		    	}
		//		    }
		//	    });
		//    	editStagingModalInstance.result.then(function(response){
		//    	    console.log(response);
		//	    }, function(error){
		//    	    console.log(error);
		//	    });
	}

	//    function getJSONData(url){
	//    	var deferred = $q.defer();
	//    	$http.get(url).then(function(response){
	//			console.log(response);
	//			deferred.resolve(response.data);
	//		});
	//    	return deferred.promise;
	//    }

	$scope.staging.isLoading = true;
	// DataCurationApiService.getlistofOrganisations().then(function (resp) {
	// 	if (resp && resp.data && resp.data.hits && resp.data.hits.length > 0) {
			angular.forEach(entity_org_search.hits, function (d) {
				var updatedTime = d["@updated"] ? moment(d["@updated"]).format("YYYY-MM-DD HH:mm:ss") : "-";
				var createdTime = d["@created"] ? moment(d["@created"]).format("YYYY-MM-DD HH:mm:ss") : "-";
				var key = {
					name: d["vcard:organization-name"] ? d["vcard:organization-name"] : "-",
					headquater: d["mdaas:HeadquartersAddress"] ? d["mdaas:HeadquartersAddress"].fullAddress ? d["mdaas:HeadquartersAddress"].fullAddress : "-" : "-",
					location: d.isIncorporatedIn ? d.isIncorporatedIn : "-",
					domicile: d.isDomiciledIn ? d.isDomiciledIn : "-",
					lastUpdatedOn: updatedTime,
					createdOn: createdTime,
					identifier: d["@identifier"],
					status: d["@status"],
					type: "Company",
					'@identifier': d["@identifier"]
				};
				//if(key.location !== '' && key.location)
				$scope.staging.list.push(key);

			});
		// } else {
			$scope.staging.isLoading = false;
	// 	}
	// 	//	DTDefaultOptions.setLoadingTemplate('<i class="fa fa-spinner fa-spin fa-3x custom-spinner full-page-spinner"></i>');
	// }, function (respError) {
	// 	$scope.staging.isLoading = false;
	// });

	$scope.onClickAddDataCuration = function () {
		$scope.staging.isLoading = true;
		$state.go('addDataCuration', { 'identifier': 'new' });
	};


	$scope.openPreviewModal = function (val) {
		var allRelationsData = {};
		if (val) {
			allRelationsData = val;
		}
		var relationtModalInstance = $uibModal.open({
			templateUrl: 'scripts/manage/modal/views/previewModal.modal.html',
			controller: 'previewModalController',
			windowClass: 'custom-modal preview-entity-modal',
			resolve: {
				AllRelationsData: function () {
					return allRelationsData;
				}
			}
		});
		relationtModalInstance.result.then(function () {
		}, function () {
		});
	};


	function searchQuery() {
		console.log($scope.searchEntity, "wvvl");
		$scope.staging.isLoading = true;
		// DataCurationApiService.getlistofOrganisations($scope.searchEntity.entityQuery, $scope.searchEntity.entitySize).then(function (resp) {
			$scope.staging.list = [];
			if (resp && resp.data && resp.data.hits && resp.data.hits.length > 0) {/*jshint ignore:line*/
				angular.forEach(entity_org_search.hits, function (d) {
					var updatedTime = d["@updated"] ? moment(d["@updated"]).format("YYYY-MM-DD HH:mm:ss") : "-";
					var createdTime = d["@created"] ? moment(d["@created"]).format("YYYY-MM-DD HH:mm:ss") : "-";
					var key = {
						name: d["vcard:organization-name"] ? d["vcard:organization-name"] : "-",
						headquater: d["mdaas:HeadquartersAddress"] ? d["mdaas:HeadquartersAddress"].fullAddress ? d["mdaas:HeadquartersAddress"].fullAddress : "-" : "-",
						location: d.isIncorporatedIn ? d.isIncorporatedIn : "-",
						domicile: d.isDomiciledIn ? d.isDomiciledIn : "-",
						lastUpdatedOn: updatedTime,
						createdOn: createdTime,
						identifier: d["@identifier"],
						status: d["@status"],
						type: "Company"
					};
					//if(key.location !== '' && key.location)
					$scope.staging.list.push(key);

				});
			} else {
				$scope.staging.isLoading = false;
			}
			//	DTDefaultOptions.setLoadingTemplate('<i class="fa fa-spinner fa-spin fa-3x custom-spinner full-page-spinner"></i>');
		// }, function (respError) {
		// 	$scope.staging.isLoading = false;
		// });
	}

	function onClickViewEntity() {
	//	previewModalController.previewModalfunctions.searchEntity("hello");
	}


}