'use strict';
angular.module('ehubApp')
	   .controller('ManageDashboardController', manageDashboardController);

		manageDashboardController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'EHUB_FE_API'
		];
		
		function manageDashboardController(
				$scope, 
				$rootScope,
				$state,
				EHUB_FE_API) {
			
			$scope.onWorkFlow = function(){
				 window.location.href= EHUB_FE_API + 'workflow/'; 
			};
}