'use strict';
angular.module('ehubApp')
	   .controller('JobsController', jobsController);

			jobsController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function jobsController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.jobs = {
					jobsData:[
						    {
							  'name':'Calendar',
							  'value':'Due week ago'
						    }, {
						    	'name':'Timer',
								'value':'Due week ago'
							
						    }, {
						    	'name':'Interval',
								 'value':'Due week ago'
							
						    }, {
						    	'name':'Escalations',
								'value':'Due week ago'
							
						    }, {
						    	'name':'Cancellation',
								'value':'Due week ago'
							
						    }],
				manageDbData2:[{
								'name':'Act_EVT_Log',
								'value':'14',
								'class':''
							},{
								'name':'Act_Bpt_Bytearry',
								'value':'23',
								'class':''
							},{
								'name':'Act_GE_Property',
								'value':'325',
								'class':'fa fa-cog'
							},{
								'name':'Act_BPT_Linkarry',
								'value':'89',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Taskinst',
								'value':'127',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Bytearray',
								'value':'456',
								'class':'fa fa-line-chart'
							},{
								'name':'Act_GE_Propert',
								'value':'323',
								'class':'fa fa-user'
							},{
								'name':'Act_GE_Bytearray',
								'value':'34',
								'class':'fa fa-user'
							},{
								'name':'Act_GE_Property',
								'value':'16',
								'class':'fa fa-file-o'
							}]

			};
		}
		
	