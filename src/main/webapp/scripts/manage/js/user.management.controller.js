'use strict';
angular.module('ehubApp')
	   .controller('UserManagementController', userManagementController);

		userManagementController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function userManagementController(
				$scope, 
				$rootScope,
				$state,
				$uibModal) {

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.usermanagement = {
					 usermanagementdata:[
						    {"name":"users",
						    	"class":"fa fa-user",
						    	"active":"active"
						    },{ "name":"groups",
							"class":"fa fa-university",
							"active":""
						    },{"name":"roles",
						    	"class":"fa fa-trophy",
						    	"active":""
						    },{"name":"passwords",
						    	"class":"fa fa-key",
						    	"active":""
						    },{"name":"monitor",
						        "class":"fa fa-eye",
						        "active":""
						    },{"name":"monitor",
						        "class":"fa fa-eye",
						        "active":""
						    },{"name":"accessControls",
						        "class":"fa fa-unlock-alt",
						        "active":""}
							],
					   addUserModel:addUserModel,
			           navigatepage:navigatepage
					};
			
				 /*
				 * @purpose: Open Add User Model
				 * @created: 09 oct 2017
				 * @params: null
				 * @returns: no
				 * @author: Varsha
				 */
				
			  function  addUserModel() {
				 var AddUserModelInstance = $uibModal.open({/*jshint ignore:line*/
					    templateUrl: "scripts/manage/modal/views/addUserModal.html",
				        controller: 'AddUserModalController',
						windowClass: 'custom-modal create-case-modal',
					});
				
			  }
			

			 /*
			 * @purpose: Open subpage
			 * @created: 09 oct 2017
			 * @params: null
			 * @returns: no
			 * @author: Varsha
			 */
			
		     function navigatepage(submenu){
		    	
		    	 $state.go(submenu);
		    	 
		     }
}