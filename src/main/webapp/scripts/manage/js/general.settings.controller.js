'use strict';
angular.module('ehubApp')
	.controller('GeneralSettingsController', generalSettingsController);

generalSettingsController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'GeneralSettingsApiService',
	'$uibModal',
	'HostPathService',
	'$timeout'
];

function generalSettingsController(
	$scope,
	$rootScope,
	$state,
	GeneralSettingsApiService,
	$uibModal,
	HostPathService,
	$timeout) {
	$scope.isSettingsError = false;
	$scope.generalDefaultExists = false;
	$scope.generalUserInteractionExists = false;
	$scope.indexPosition = "General Settings";
	$scope.nameOfIcon = "";
	$scope.nameOfColor = "";
	$scope.dataToShowAsperList = [];
	$scope.answerToshowInTable = [];
	$scope.selectedValueAsperList = "";
	$scope.selectedStatusOnDropdown = "";
	var modalInstanceDel = "", modalInstanceAdd = "";
	$scope.deleteObjValue = {};
	$scope.storeBeforeEditing = {};
	$scope.colorList = [];
	$scope.iconList = [];
	$scope.nameOfFlag = "";
	$scope.addColorIconObj = {};
	$scope.jurisdictionList = [];
	$scope.setJurisdiction = {};
	$scope.addDisplayName = '';
	$scope.codeTodisplay = '';
	/*
	 * @purpose: sort array of objects by key
	 * @created: 10 Aug 2018	
	 * @author: Prasanthi
	*/

	function dynamicSort(property) {
		var sortOrder = 1;
		if (property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return function (a, b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		};
	}
	/*
	 * @purpose: load system settings
	 * @created: 9 Aug 2018	
	 * @author: Prasanthi
	*/
	function loadGeneralSettings(search) {
		// start of defining scope variables
		if (!search) {
			$scope.settingsloader = true;
		}
		$scope.generalDefaultExists = false;
		$scope.generalUserInteractionExists = false;
		$scope.isGeneralError = false;
		$scope.isReportError = false;
		$scope.isRegionalError = false;
		$scope.isMailError = false;
		$scope.selectedVal = {};
		// end of defining scope variables
		// To sort an Array
		Array.prototype.moveArray = function (old_index, new_index) {
			if (new_index >= this.length) {
				var k = new_index - this.length;
				while ((k--) + 1) {
					this.push(undefined);
				}
			}
			this.splice(new_index, 0, this.splice(old_index, 1)[0]);
			return this; // for testing purposes
		};
		GeneralSettingsApiService.getGeneralSettings($scope.searchSettings).then(function (response) {
			//remove loader
			$scope.settingsloader = false;
			if (response && response.data) {
				var keys = Object.keys(response.data).moveArray(1,0).moveArray(1,3);
				angular.forEach(keys, function (value) {
					if (response.data[value]) {
						response.data[value] = d3.nest().key(function (d) {
							return d.section;
						}).entries(response.data[value]);
					} else {
						delete response.data[value];
					}
					angular.forEach(response.data[value], function (values) {
						angular.forEach(values.values, function (val, k) {
							if (val.name == "List Type") {
								$scope.selectedValueAsperList = val.selectedValue;
								getTableData(val.selectedValue, 'ListTable');

							}
							if (val.name == "Predefined answers for :") {
								$scope.selectedStatusOnDropdown = val.selectedValue;
								getTableData(val.selectedValue, "AnswerTable");
							}
							if (val.name == "PEP Slider" || val.name == "Time In Status Slider" || val.name == "Sanction Slider") {
								var indexPos = values.values.findIndex(function (value) { return (value.name == "PEP Slider" || value.name == "Time In Status Slider" || value.name == "Sanction Slider") });
								val.splitName = val.name.split(' ').join('_');
								val.keyToIdentify = val.name.replace(/Slider/, 'Period')
								$timeout(function () {
									sliderAvgExpen(0, val.selectedValueMax, val.sliderFrom, val.sliderTo, val.splitName, val, indexPos);
								}, 0)

							}

							if (val.options) {
								val.options.sort(dynamicSort("attributeName"));
								angular.forEach(val.options, function (v1, k1) {
									if (v1.selected) {
										$scope.selectedVal["name" + (value.split(' ').join('_')) + "_" + (values.key.split(' ').join('_')) + "sub" + k] = k1.toString();
										if (val.name.toLowerCase() == 'questionnaries') {
											$rootScope.qbServeyID = v1.attributeId;
										}
									}
								});

							} else if (val.name.split(" ").join('') === 'Allowchecklist') {
								$scope.selectedVal["name" + (value.split(' ').join('_')) + "sub" + val.name.split(" ").join('')] = (val.selectedValue == 'On') ? true : false;
							}
						});
					});
				});
				$scope.settingsData = response.data;

				// $scope.settingsData = Object.keys($scope.settingsData).sort().reduce((accumulator, d) => {
				// 	return Object.assign(accumulator, { [d]: $scope.settingsData[d] });
				// }, {});

				for (var val in $scope.settingsData) {
					if ($scope.settingsData[val]) {
						$scope.settingsData[val].forEach(function (v) {
							if (v.values && v.values.length) {
								console.log(v.values);
								v.values.forEach(function (val2) {
									if (val2.type === 'Toggle On/Off' && (val2.section === 'User Interaction' || val2.section === 'defaults') && val2.systemSettingType === 'GENERAL_SETTING') {
										val2.classType = 'settings-toggle-wrapper';
									}
									else if (val2.type === 'Toggle On/Off' && (val2.systemSettingType === 'MAIL_SETTING' || val2.systemSettingType === 'ALERT_MANAGEMENT')) {
										val2.classType = 'settings-toggle-wrapper';
									} else if (val2.type === 'Slider' && (val2.systemSettingType === 'MAIL_SETTING' || val2.systemSettingType === 'ALERT_MANAGEMENT')) {
										val2.classType = 'settings-slider-wrapper';
									}
									else if (val2.type === 'Dropdown' && (val2.systemSettingType === 'ALERT_MANAGEMENT') && val2.section === 'Predefined Answers') {
										val2.secondaryClassType = 'left_104';
										val2.classType = 'settings-toggle-wrapper';
									}
									else if (val2.section === 'Branding' && val2.systemSettingType === 'GENERAL_SETTING') {
										val2.classType = 'branding-wrapper';
									}
									else if (val2.type === 'Toggle On/Off' && val2.section === 'File Settings' && val2.systemSettingType === 'GENERAL_SETTING') {
										val2.classType = 'file-switch-wrapper';
										switch (val2.name) {
											case "pdf":
												// code block
												val2.iconClass = 'file-pdf-o';
												break;
											case "xlsx":
												// code block
												val2.iconClass = 'file-excel-o';
												break;
											case "txt":
												// code block
												val2.iconClass = 'file-text-o';
												break;
											case "jpg":
											case "png":
											case "gif":
												// code block
												val2.iconClass = 'file-image-o';
												break;
											case "docx":
												// code block
												val2.iconClass = 'file-word-o';
												break;
											default:
												val2.iconClass = 'file-o';
											// code block
										}
									} else if (val2.type === 'Dropdown' && val2.section === 'File Settings' && val2.systemSettingType === 'GENERAL_SETTING') {
										val2.classType = 'width-100 bst_input_group';
									}
								});
							}
						})
					}
				}

				if (Object.keys($scope.settingsData).length == 0) {
					$scope.isSettingsError = true;
				} else {
					$scope.isSettingsError = false;
				}
			} else {
				//show error and remove loader
				$scope.isSettingsError = true;
			}
		}, function () {
			//show error and remove loader
			$scope.isSettingsError = true;
			$scope.settingsloader = false;
		});
	}
	//call function to load the settings
	loadGeneralSettings();
	/*
	 * @purpose: function to validate email
	 * @created: 9 Aug 2018	
	 * @author: Prasanthi
	*/
	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
	/*
	 * @purpose: load system settings
	 * @created: 9 Aug 2018	
	 * @author: Prasanthi
	*/
	$scope.updateVal = function (setting, selected, id, email, Questionnaries) {
		if ($(".validated").hasClass("ng-invalid")) {
			return;
		}
		if (email) {
			var validate = validateEmail(setting.selectedValue);
			if (validate) {
				setting.validationFail = false;
			} else {
				setting.validationFail = true;
				return;
			}
		}
		var data;
		//		    	$scope.settingsloader = true;
		if (!selected && setting.section !== "Branding" && setting.section !== "Predefined Answers") {
			//handle for checkbox
			if (id) {
				setting.selectedValue = ($("#hyperCheck_" + id).prop("checked") ? "On" : "Off");
			}
			//handle for checkbox and text 
			data = {
				"attributeId": "",
				"selectedValue": setting.selectedValue,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType
			};
		} else if (setting.name == "Default Module") {
			data = {
				"attributeId": setting.options[selected].attributeId,
				"selectedValue": "",
				"settingId": "",
				"systemSettingsType": setting.systemSettingType
			};
		} else if (setting.name == "Questionnaries") {
			data = {
				// "attributeId": setting.options[selected].attributeId,			    			  
				"selectedValue": setting.options[selected].attributeId,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType
			};
		} else if (setting.section == "Branding") {
			data = {
				"customerLogoImage": setting.customerLogoImage,
				"selectedValue": setting.defaultValue,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType,
				"attributeId": null
			};

		} else if (setting.section == "List Management") {
			data = {
				"attributeId": setting.options[selected].attributeId,
				"selectedValue": setting.options[selected].attributeName,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType
			};
			$scope.selectedValueAsperList = data.selectedValue;
			getTableData(data.selectedValue, 'ListTable');
		} else if (setting.name == "Predefined answers for :") {
			data = {
				"attributeId": setting.options[selected].attributeId,
				"selectedValue": setting.options[selected].attributeName,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType
			};
			$scope.selectedStatusOnDropdown = data.selectedValue
			getTableData(data.selectedValue, "AnswerTable");
		}
		else if (setting.type == "Slider") {
			data = {
				"attributeId": null,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType,
				"selectedValue": setting.selectedValue,
				"selectedValueMax": setting.selectedValueMax,
				"sliderFrom": setting.sliderFrom,
				"sliderTo": setting.sliderTo
			}

		} else if (setting.name == "PEP Period" || setting.name == "Time In Status Period" || setting.name == "Sanction Period") {
			data = {
				"attributeId": setting.options[selected].attributeId,
				"selectedValue": setting.options[selected].attributeName,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType,
				"selectedValueMax": setting.options[selected].attributeValue
			};
		} else {
			data = {
				"attributeId": setting.options[selected].attributeId,
				"selectedValue": setting.options[selected].attributeName,
				"settingId": setting.settingId,
				"systemSettingsType": setting.systemSettingType

			};
		}

		GeneralSettingsApiService.updateValue(data).then(function (response) {
			$scope.settingsloader = false;
			if (response) {
				//		    			HostPathService.FlashSuccessMessage('SUCCESSFULLY UPDATED..', '');
				if (Questionnaries) {
					$rootScope.qbServeyID = data.selectedValue;
				}
				if (setting.section == "Branding") {
					$scope.settingsData['General Settings'].filter(function (d) { return d.key == 'Branding'; })[0].values[0].customerLogoImage = setting.customerLogoImage;
				}
				if (setting.options) {
					$rootScope.defaultModuleName = setting.options[selected].attributeName.toLowerCase();
				}
				if (setting.name == "PEP Period" || setting.name == "Time In Status Period" || setting.name == "Sanction Period") {
					Object.values($scope.settingsData).map(function (d) {
						angular.forEach(d, function (val) {
							angular.forEach(val.values, function (value, key) {
								if (value.keyToIdentify === setting.name) {
									value.selectedValueMax = data.selectedValueMax;
									value.selectedValue = data.selectedValue;
									$timeout(function () {
										sliderAvgExpen(0, value.selectedValueMax, value.sliderFrom, value.sliderTo, value.splitName, value, key, "onchange");
									}, 0)
								}
								// 
							})
						})

					})
				}
				// var indexPos = values.values.findIndex(function (value) { return (value.name == "PEP Slider" || value.name == "Time In Status Slider" || value.name == "Sanction Slider") });
				// val.splitName = val.name.split(' ').join('_');
				// val.keyToIdentify =val.name.split(' ')[0]+" Period"

			} else {
				HostPathService.FlashErrorMessage('FAILED TO UPDATE.. PLEASE TRY AGAIN LATER', '');
			}
		}, function () {
			//show error and remove loader
			HostPathService.FlashErrorMessage('FAILED TO UPDATE.. PLEASE TRY AGAIN LATER', '');
			$scope.settingsloader = false;
		});

	};
	/*
	 * @purpose: search system settings
	 * @created: 13 Aug 2018	
	 * @author: Prasanthi
	*/
	$scope.searchedSettings = function () {
		if ($scope.searchSettings.length == 0 || $scope.searchSettings.length >= 3) {
			loadGeneralSettings($scope.searchSettings);
		}
	};

	/*
			  * @purpose: Toggle function for the show and hide of list
			  * @created: 13 Aug 2018	
			  * @author: Asheesh
			 */

	$scope.switchActive = function (index) {
		$scope.indexPosition = index;
	};
	$scope.openUploadCustomerLogoModal = function (setting) {
		var customerLogoModalInstance = $uibModal.open({
			templateUrl: 'uploadCustomerLogo.html',
			controller: 'UploadCustomerLogoModalController',
			size: 'md',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow bst_modal ',
			resolve: {
				settingId: function () {
					return setting;
				}
			}
		});

		customerLogoModalInstance.result.then(function (response) {
			if (response && !_.isEmpty(response)) {
				$scope.updateVal(response);
			}
		}, function () {
		});
	};

	$scope.removeCustomerLogo = function (setting) {
		setting.customerLogoImage = '';//setting.defaultValue;
		$scope.updateVal(setting);
	};
	/*
	 * @purpose: function to Add icon from popover
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/
	$scope.addIcon = function (iconName) {
		$scope.nameOfIcon = iconName;
		$scope.nameOfFlag = "";
	};
	/*
	 * @purpose: function to Add color from popover
	 * @created:25 july 2019	
	 * @author: Amritesh
	*/
	$scope.addColor = function (colorName) {
		$scope.nameOfColor = colorName;
	};
	/*
	 * @purpose: function to Add flag from popover
	 * @created:25 july 2019	
	 * @author: Amritesh
	*/
	$scope.addCountryFlag = function (flagName) {
		$scope.nameOfFlag = flagName;
		$scope.nameOfIcon = ""
	};
	$scope.iconNameFromPopOver = "ban";
	/*
	 * @purpose: on click of ADD from popover change icon & color
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/
	$scope.changeColorNIconToTable = function () {
		$scope.dataToShowAsperList[$scope.addColorIconObj.index].colorCode = $scope.nameOfColor;
		$scope.dataToShowAsperList[$scope.addColorIconObj.index].icon = $scope.nameOfIcon;
		$scope.dataToShowAsperList[$scope.addColorIconObj.index].flagName = $scope.nameOfFlag
		$scope.dataToShowAsperList[$scope.addColorIconObj.index].flagPath = 'vendor/jquery/flags/1x1/' + $scope.nameOfFlag.toLowerCase() + '.svg'
		callSaveUdpateAPI($scope.addColorIconObj.obj);
		$scope.isOpenPop = false;
	};
	/*
	 * @purpose: function to get Table data as per
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/
	$scope.closePopover = function () {
		$scope.isOpenPop = false;
		$('.custom-data-table-wrapper').click();
	};
	/*
	 * @purpose: function to get Table data as per list type selected from dropdown
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/
	function getTableData(listType, sectionFrom) {
		GeneralSettingsApiService.getListItemsByListType(listType).then(function (response) {
			if (sectionFrom === "AnswerTable") {
				$scope.answerToshowInTable = tableSorting(response.data);
				angular.forEach($scope.answerToshowInTable, function (v1) {
					v1.disableInput = true;
				});
			} else {
				$scope.dataToShowAsperList = tableSorting(response.data);
				angular.forEach($scope.dataToShowAsperList, function (v1) {
					v1.disableInput = true;
					v1.flagPath = v1.flagName ? 'vendor/jquery/flags/1x1/' + v1.flagName.toLowerCase() + '.svg' : "";
				});
			}

			setTimeout(() => {
				$(".panel .panel-body-wrappe, .scroll-tbody").mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});
			}, 0);
		});
	}
	/*
	 * @purpose: function to delete row from table
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/

	$scope.deleteRow = function (deleteObj) {
		$scope.deleteObjValue = deleteObj;
		modalInstanceDel = $uibModal.open({
			templateUrl: 'deleteFromTable.html',
			size: 'sm',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow center bst_modal add-ownership-modal add-new-officer',
			scope: $scope
		});
		modalInstanceDel.result.then(function (response) {
			console.log('response: ', response);

		}, function () {
		});
	};
	/*
   * @purpose: function to add row from dropdown
   * @created:25 July 2019	
   * @author: Amritesh
  */
	$scope.addRow = function () {
		apiCALLToGetListofColorFlagIcon();
		modalInstanceAdd = $uibModal.open({
			templateUrl: 'AddItemToTable.html',
			size: 'sm',
			backdrop: 'static',
			windowClass: 'custom-modal c-arrow center bst_modal setting-icons-modal',
			scope: $scope
		});
		modalInstanceAdd.result.then(function () {
			// modalInstanceAdd.dismiss('close');
		}, function () {
		});
	};
	/*
	* @purpose: function toclose data pop from delete modal
	* @created:25 July 2019	
	* @author: Amritesh
   */
	$scope.closeDataPopUp = function () {
		modalInstanceDel.dismiss('close');
	};
	/*
	* @purpose: function toclose data pop from Add modal
	* @created:31 July 2019	
	* @author: Amritesh
   */
	$scope.closeAddDataPopUp = function () {
		modalInstanceAdd.dismiss('close');
	};
	/*
	 * @purpose: function to handle delete modal
	 * @created:25 July 2019	
	 * @author: Amritesh
	*/
	$scope.deleteItemFromList = function () {
		GeneralSettingsApiService.deleteListItem($scope.deleteObjValue.listItemId).then(function (response) {
			if (response) {
				if ($scope.deleteObjValue.listType == $scope.selectedValueAsperList) {
					getTableData($scope.selectedValueAsperList, 'ListTable');

				} else {
					getTableData($scope.selectedStatusOnDropdown, 'AnswerTable');
				}

			}

		});
		modalInstanceDel.dismiss('close');
	};
	/*
 * @purpose: function to handle ADD list in table from Add modal
 * @created:25 July 2019	
 * @author: Amritesh
*/
	$scope.addItemToTable = function (DisplayName, codeAdded) {
		var data = { "displayName": DisplayName ? DisplayName : codeAdded, "listType": "Alert Status", "allowDelete": true, code: codeAdded, icon: $scope.nameOfIcon, flagName: $scope.nameOfFlag, colorCode: $scope.nameOfColor };
		callSaveUdpateAPI(data, "", 'ListTable');
		callUpdateValFunction(data);
		modalInstanceAdd.dismiss('close');
	
	};
	/*
 * @purpose: function on click of edit icon
 * @created:25 July 2019	
 * @author: Amritesh
*/
	$scope.editUpdateListInTable = function (index, beforeEditObj, tableType) {
		if (tableType == 'ListTable') {
			$scope.dataToShowAsperList[index].disableInput = false;
		} else {
			$scope.answerToshowInTable[index].disableInput = false;
		}
		$scope.storeBeforeEditing = jQuery.extend(true, {}, beforeEditObj);
	};
	/*
 * @purpose: function to handle save edit value in table
 * @created: 1 August 2019	
 * @author:  Amritesh
*/
	$scope.saveEditUpdateListInTable = function (index, edittedObj, tableType) {
		if (tableType == 'ListTable') {
			if ($scope.dataToShowAsperList[index].displayName != $scope.storeBeforeEditing.displayName || $scope.dataToShowAsperList[index].code != $scope.storeBeforeEditing.code) {
				setTimeout(() => {
					callSaveUdpateAPI(edittedObj, index, tableType);
				}, 0);
			}
			$scope.dataToShowAsperList[index].disableInput = true;
		} else {
			if ($scope.answerToshowInTable[index].displayName != $scope.storeBeforeEditing.displayName || $scope.answerToshowInTable[index].code != $scope.storeBeforeEditing.code) {
				setTimeout(() => {
					callSaveUdpateAPI(edittedObj, index, tableType);
				}, 0);
			}
			$scope.answerToshowInTable[index].disableInput = true;
		}
	};
	/*
 * @purpose: function to handle cancel from edit value in table
 * @created: 1 August 2019	
 * @author:  Amritesh
*/
	$scope.cancelEditUpdateListInTable = function (index, tableType) {
		if (tableType == 'ListTable') {
			if (Object.keys($scope.storeBeforeEditing).length == 0) {
				$scope.dataToShowAsperList.splice(index, 1)
			} else {
				$scope.dataToShowAsperList[index].disableInput = true;
				$scope.dataToShowAsperList[index].displayName = $scope.storeBeforeEditing.displayName;
				$scope.dataToShowAsperList[index].code = $scope.storeBeforeEditing.code;
			}

		} else {
			if (Object.keys($scope.storeBeforeEditing).length == 0) {
				$scope.answerToshowInTable.splice(index, 1)
			} else {
				$scope.answerToshowInTable[index].disableInput = true;
				$scope.answerToshowInTable[index].displayName = $scope.storeBeforeEditing.displayName;
				$scope.answerToshowInTable[index].code = $scope.storeBeforeEditing.code;
			}
		}

		// }

	};
	/*
 * @purpose: function to show pop with color ,icon and flag on click from table
 * @created: 2 August 2019	
 * @author:  Amritesh
*/
	$scope.handlingPopUpFlagColorIcon = function (index, objeditColor) {
		$scope.addColorIconObj = { index: index, obj: objeditColor };
		$scope.nameOfColor = $scope.addColorIconObj.obj.colorCode;
		$scope.nameOfIcon = $scope.addColorIconObj.obj.icon;
		$scope.nameOfFlag = $scope.addColorIconObj.obj.flagName ? $scope.addColorIconObj.obj.flagName : "";
		apiCALLToGetListofColorFlagIcon();
		setTimeout(function () {
			$(".check-btn").click(function () {
				$(".check-btn").removeClass("activeCheck");
				// $(".tab").addClass("active"); // instead of this do the below 
				$(this).addClass("activeCheck");
			});
			// Add active class to the current button (highlight it)
			$(document).ready(function () {
				$(".icons-btn-wrapper").click(function () {
					$(".icons-btn-wrapper").removeClass("active");
					// $(".tab").addClass("active"); // instead of this do the below 
					$(this).addClass("active");
				});
				$(".setting-icons-popover").mCustomScrollbar({
					axis: "y",
					theme: "minimal-dark"
				});
			});
		}, 1000);

	};
	/*
 * @purpose: function to get color with # added
 * @created: 12 August 2019	
 * @author:  Amritesh
*/
	$scope.setColor = function (colorToSet) {
		return "#" + colorToSet;
	};
	/*
	* @purpose: function to call save/Update API
	* @created: 12 August 2019
	* @author:  Amritesh
	* @param :data(obj),index(number)
	*/
	function callSaveUdpateAPI(data, index, tableType) {
		GeneralSettingsApiService.AddUpdateListItem(data).then(function (response) {
			if (response) {
				if (tableType == 'ListTable') {
					getTableData($scope.selectedValueAsperList, tableType);
					if($scope.selectedValueAsperList =="Alert Status"){
						callUpdateValFunction(data);
					}
				} else {
					getTableData($scope.selectedStatusOnDropdown, tableType);
				}
			}

		}, function (error) {
			//show error and remove loader
			if (index || index == 0) {
				$scope.dataToShowAsperList[index].code = $scope.storeBeforeEditing.code
			}
			HostPathService.FlashErrorMessage(error.responseMessage, '');

		});
	}
	/*
	* @purpose: function to sort table Alphabetically
	* @created: 12 August 2019
	* @author:  Amritesh
	* @param :arrayToSort(array)
	*/
	function tableSorting(arrayToSort) {
		arrayToSort = arrayToSort.sort(function (a, b) {
			return a.code.localeCompare(b.code);
		});
		return arrayToSort
	}

	/*
	* @purpose: function on slider slides to set min max and initialize slideers
	* @created: 12 August 2019
	* @author:  Amritesh
	* @param :min, max, sliderfrom, sliderto,id,objSlider,index,whereFrom
	*/
	function sliderAvgExpen(min, max, sliderfrom, sliderto, id, objSlider, index, whereFrom) {
		// if(whereFrom =="onchange"){
		// 	$scope.updateVal(objSlider, index);
		// }
		$("#" + id).find(".price-range-min").html(sliderfrom);
		$("#" + id).find(".price-range-max").html(sliderto);
		$("#" + id).slider({
			range: true,
			min: 0,
			max: max,
			values: [sliderfrom, sliderto],
			slide: function (event, ui) {
				percentageRange(ui.values, false, id);
				// $scope.activateDisablebutton(ui.values[0], ui.values[1]);
				if (whereFrom == "onchange") {
					$scope.updateVal(objSlider, index);
				}
			},
			stop: function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					$('#' + id + '.price-range-both i').css('display', 'none');
				} else {
					$('#' + id + '.price-range-both i').css('display', 'inline');
				}
				if (collision(ui.values[0], ui.values[1])) {
					$('#' + id + '.price-range-min', '#' + id + '.price-range-max').css('opacity', '0');
					$('#' + id + '.price-range-both').css('display', 'block');
				} else {
					$('#' + id + '.price-range-min', '#' + id + '.price-range-max').css('opacity', '1');
					$('#' + id + '.price-range-both').css('display', 'none');
				}
				objSlider.sliderFrom = ui.values[0];
				objSlider.sliderTo = ui.values[1];
				$scope.updateVal(objSlider, index);
			}
		});

		//	 	$('#sliderAvgExpenCompliance .ui-slider-handle:eq(0)').append('<span class="price-range-min value1">' + $('#sliderAvgExpenCompliance').slider('values', 0) + '</span>');
		//	 	$('#sliderAvgExpenCompliance .ui-slider-handle:eq(1)').append('<span class="price-range-max value1">' + $('#sliderAvgExpenCompliance').slider('values', 1) + '</span>');
	}
	/*
* @purpose: to set percent on slider
* @created: 12 August 2019
* @author:  Amritesh
* @param :values, yearslider,id
*/
	function percentageRange(values, yearslider, id) {
		$timeout(function () {
			$("#" + id).find(".price-range-min").html(values[0]);
			$("#" + id).find(".price-range-max").html(values[1]);
		}, 0);
	}
	/*
* @purpose: to detect collision of slider
* @created: 12 August 2019
* @author:  Amritesh
* @param :$div1, $div2
*/
	function collision($div1, $div2) {
		//var x1 = $div1.offset().left;
		var x1 = $div1;
		var w1 = 0.5;
		var r1 = x1 + w1;
		//var x2 = $div2.offset().left;
		var x2 = $div2;
		var w2 = 0.5;
		var r2 = x2 + w2;

		if (r1 < x2 || x1 > r2) return false;
		return true;
	}
	/*
	 * @purpose: function to get color and icon list and flag
	 * @created: 23 August 2019	
	 * @author:  Amritesh
	*/
	function apiCALLToGetListofColorFlagIcon() {
		GeneralSettingsApiService.getColorList().then(function (response) {
			$scope.colorList = response.data;
		});
		GeneralSettingsApiService.getIconList().then(function (response) {
			$scope.iconList = response.data;
		});
		GeneralSettingsApiService.getJurisdictionList().then(function (response) {
			if (response && response.data && response.data.length > 0) {
				angular.forEach(response.data, function (juridisction) {
					if (juridisction && juridisction.jurisdictionOriginalName && juridisction.jurisdictionOriginalName.toUpperCase() !== 'ALL' && juridisction.jurisdictionOriginalName.toUpperCase() !== 'ANY') { //we are not showing ALL option and the null names
						juridisction.jurisdictionName = juridisction.jurisdictionName ? juridisction.jurisdictionName.toUpperCase() : '';
						if (!(_.find($scope.jurisdictionList, { 'jurisdictionName': juridisction.jurisdictionName, 'jurisdictionOriginalName': juridisction.jurisdictionName }))) {
							juridisction.key = juridisction.jurisdictionName;
							juridisction.name = juridisction.jurisdictionOriginalName;
							juridisction.key2 = juridisction.jurisdictionName.toLowerCase();
							juridisction.flag = 'vendor/jquery/flags/1x1/' + juridisction.key2 + '.svg';
							$scope.jurisdictionList.push(juridisction);
						}
					}
				});//for each ends
				var singlearray = $scope.jurisdictionList.map((val) => val.key);
				var arr2 = $scope.jurisdictionList.filter((item, pos) => {
					if (pos == singlearray.indexOf(item.key)) {
						return item;
					}
				})

				$scope.jurisdictionList = jQuery.extend(true, [], arr2);
			}
		}, function () { });
	}

	/*
* @purpose: function to add row from table
* @created:25 July 2019	
* @author: Amritesh
*/
	$scope.addRowInTable = function (tableType) {
		var obj = {
			allowDelete: true,
			code: "",
			colorCode: "",
			disableInput: false,
			displayName: "",
			flagName: "",
			flagPath: "",
			icon: "ban",
			listItemId: "",
			listType: tableType == 'ListTable' ? $scope.selectedValueAsperList : $scope.selectedStatusOnDropdown,
		}
		if (tableType == 'ListTable') {
			$scope.dataToShowAsperList.unshift(obj);
		} else {
			$scope.answerToshowInTable.unshift(obj);
		}

	};
	function callUpdateValFunction(dataObj) {
		Object.values($scope.settingsData).map(function (d) {
			angular.forEach(d, function (val) {
				angular.forEach(val.values, function (value, key) {
					if (value.section == "Predefined Answers") {
						if(!value.options){
							var obj={attributeId: 0,
								attributeName:dataObj.code,
								attributeValue: dataObj.code,
								selected: true
							}
							value.options =[];
							value.options.push(obj)
						} else{
							var obj={attributeId: 0,
								attributeName:dataObj.code,
								attributeValue: dataObj.code,
								selected: false
							}
							value.options.push(obj)
						}
						$scope.updateVal(value, key);
					}
				})
			})

		})
		setTimeout(() => {
			loadGeneralSettings();
		}, 1000);
		
	}
	function deleteListFromPredefinedDropDown(delteMapObj){
		Object.values($scope.settingsData).map(function (d) {
			angular.forEach(d, function (val) {
				angular.forEach(val.values, function (value, key) {
					if (value.section == "Predefined Answers") {
						var optionIndex = value.options.findIndex(function(d){
								return d.attributeName == delteMapObj.code;
						})
						$scope.updateVal(value, key);
					}
				})
			})

		})
			setTimeout(() => {
			loadGeneralSettings();
		}, 1000);
	}
}