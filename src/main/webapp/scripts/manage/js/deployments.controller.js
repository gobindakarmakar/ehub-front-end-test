'use strict';
angular.module('ehubApp')
	   .controller('DeploymentsController', deploymentsController);

		deploymentsController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function deploymentsController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.deployment = {
					deploymentData:[
						    {
						    	"name":"Event Calendar"
							
						    }, {
						    	"name":" Leave Management"
							
						    }, {
						    	"name":"Simple Task Process"
							
						    }, {
						    	"name":"Customer Complaints"
						    }, {
						    	"name":"Call Status"
							
						    }],
						    deploymentData2:[{
								'process':'Create Timers Process',
								'resource':'org/activiti/explorer/demo/process/FixSystemFailureProcess.bpmn20.xml'
							},{
								'process':'Fix system failure',
								'resource':'org/activiti/explorer/demo/process/FixSystemFailureProcess.png'
							},{
								'process':'Helpdesk Process',
								'resource':'org/activiti/explorer/demo/process/Helpdesk.bpmn20.xml'
							},{
								'process':'Review Sales lead',
								'resource':'org/activiti/explorer/demo/process/Helpdesk.png'
							},{
								'process':'Simple approval process',
								'resource':'org/activiti/explorer/demo/process/VacationRequest.bpmn20.xml'
							},{
								'process':'Vacation request',
								'resource':' org/activiti/explorer/demo/process/VacationRequest.png'
							},{
								
								'resource':'org/activiti/explorer/demo/process/createTimersProcess.bpmn20.xml'
							},{
								
								'resource':' org/activiti/explorer/demo/process/reviewSalesLead.bpmn20.xml'
							},{
								
								'resource':'org/activiti/explorer/demo/process/reviewSalesLead.reviewSaledLead.png'
							}]

			};
		}
		
	