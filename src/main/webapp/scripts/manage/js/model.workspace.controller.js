'use strict';
angular.module('ehubApp')
	   .controller('ModelWorkspaceController', modelWorkspaceController);

		modelWorkspaceController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModal'
		];
		
		function modelWorkspaceController(
				$scope, 
				$rootScope,
				$uibModal) {
			
			$scope.openModal = function(){
				var openModalInstance = $uibModal.open({
					templateUrl: 'scripts/manage/modal/views/modelWorkspaceModal.html',
					controller: 'ModelWorkspaceModalController',
					backdrop: 'static',
					windowClass: 'custom-modal model-workspace-modal'
				});
				openModalInstance.result.then(function(){
					
				});
			};
		}