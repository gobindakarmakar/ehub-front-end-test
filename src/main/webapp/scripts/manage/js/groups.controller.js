'use strict';
angular.module('ehubApp')
	   .controller('GroupController', groupController);

		groupController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function groupController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */

			$scope.group = {
					 groupData:[
						    {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    },  {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    },  {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    },  {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    },  {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    },  {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    }, {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    }, {
							    "Id":"James",
								"name":"Jammision",
								"email":"jammison@gmail.com",
								"Action2":"Fresco",
								"Action":"fa fa-times light-red"
						    }],
					groupData2:[
							{
							'name':'ADMIN',
							'value':'12',
							'active':'active'
							},{
							'name':'ENGINEERING',
							'value':'89'
							},{
							'name':'MANAGEMENT',
							'value':'190'
							},{
							'name':'SALES',
							'value':'40'
							},{
							'name':'ACCOUNTS',
							'value':'09'
						}]
				
			};
			
		}