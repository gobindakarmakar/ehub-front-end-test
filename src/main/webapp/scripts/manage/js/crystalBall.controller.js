'use strict';
angular.module('ehubApp')
	   .controller('CrystalBallController', crystalBallController);

			crystalBallController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function crystalBallController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */

			$scope.crystalBall = {
					crystalBallData:[
						    {
							    "Id":"3215",
								"name":"HelpDesk Process",
								"StartedBy":"Jos Angel Sandose",
								"Ended":""
						    },     {
						    	"Id":"3215",
								"name":"Fix System Failure",
								"StartedBy":"Jame Cavannah",
								"Ended":""
						    },     {
						    	"Id":"3215",
								"name":"Call Center Process",
								"StartedBy":"Victor Bayle",
								"Ended":""
						    },   {
						    	"Id":"3215",
								"name":"Help Desk Process 2",
								"StartedBy":"Benitton Felix",
								"Ended":""
						    },     {
						    	"Id":"3215",
								"name":"System Failure",
								"StartedBy":"Camaroon Melinda",
								"Ended":""
						    },   {
						    	"Id":"3215",
								"name":"Call Senter support",
								"Instances":"Theo Henry"
						    }]
				
			};
			
		}