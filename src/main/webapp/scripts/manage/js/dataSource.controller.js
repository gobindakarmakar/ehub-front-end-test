'use strict';
angular.module('ehubApp')
	   .controller('DataSourceController', dataSourceController);

		dataSourceController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function dataSourceController(
				$scope, 
				$rootScope,
				$state,
				$uibModal) {

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.dataSource = {
					   addSourceModel:addSourceModel
			       
			};
			
				 /*
				 * @purpose: Open Add User Model
				 * @created: 13 oct 2017
				 * @params: null
				 * @returns: no
				 * @author: Varsha
				 */
				
			  function  addSourceModel() {
				 var AddSourceModelInstance = $uibModal.open({/*jshint ignore:line*/
					    templateUrl: "scripts/manage/modal/views/addSourceModal.html",
				        controller: 'AddSourceModalController',
						windowClass: 'custom-modal create-case-modal',
					});
				
			  }
}