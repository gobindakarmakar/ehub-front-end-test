'use strict';
angular.module('ehubApp')
	   .controller('AdministrationsController', administrationsController);

             administrationsController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal'
		];
		
		function administrationsController(
				$scope, 
				$rootScope,/*jshint ignore:line*/
				$state,/*jshint ignore:line*/
				$uibModal) {/*jshint ignore:line*/

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */

			$scope.administrations = {
					administrationsTable1Data:[
						    {
							    "Id":"Escalationexample:1:35",
								"name":"Help Desk Process",
								"Instances":"3"
						    },    {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    },     {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    },   {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    },    {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    },    {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    },   {
							    "Id":"Escalationexample:1:35",
								"name":"Help Desk Process",
								"Instances":"3"
						    },    {
							    "Id":"Vacationrequest:1:35",
								"name":"Vacation Request",
								"Instances":"3"
						    }],
					administrationsTable2Data:[
					    {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"JOS Angels Sandose",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    },   {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"Jame Cavannah",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    },  {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"Victor Bayle",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    },   {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"Benitton Felix",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    },    {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"Camaroon Melinda",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    },   {
						    "Id":"3215",
							"BusinessKey":"56YHED278YUI",
							"StartedBy":"Theo Henry",
							"StartedActivityId":"Thestart",
							"Started":"Wed Mar 22 03:22:17 GMT 2017"
					    }],	    
					administrationsData2:[
							{
							'name':'ADMIN',
							'value':'12',
							'active':'active'
							},{
							'name':'ENGINEERING',
							'value':'89'
							},{
							'name':'MANAGEMENT',
							'value':'190'
							},{
							'name':'SALES',
							'value':'40'
							},{
							'name':'ACCOUNTS',
							'value':'09'
						}]
				
			};
			
		}