'use strict';
angular.module('ehubApp')
.controller('AddDataCurationController', addStagingModalController);

addStagingModalController.$inject = [
	'$scope',
	'$rootScope',
	'$timeout',
	'DataCurationApiService',
	'$filter',
	'$window',
	'$cookies',
	'$stateParams',
	'EHUB_API',
	'$uibModal',
	'$state',
	'HostPathService',
	'DTOptionsBuilder',
	'DTColumnDefBuilder',
	'getAllCountriesNames',
	'getAllIndustriesNames',
	'getbusinessSectorsData',
	'getAllEconomicSectorsData',
	'getAllDirectorRoles',
	'getAllOfficerRoles',
	'getOrganationNamesSuggestions',
	'getPersonNamesSuggestions',
	'organisationIdentifier',
	'personIdentifier'
];

function addStagingModalController(
		$scope,
		$rootScope,
		$timeout,
		DataCurationApiService,
		$filter,
		$window,
		$cookies,
		$stateParams,
		EHUB_API,
		$uibModal,
		$state,
		HostPathService,
		DTOptionsBuilder,
		DTColumnDefBuilder,
		getAllCountriesNames,
		getAllIndustriesNames,
		getbusinessSectorsData,
		getAllEconomicSectorsData,
		getAllDirectorRoles,
		getAllOfficerRoles,
		getOrganationNamesSuggestions,
		getPersonNamesSuggestions,
		organisationIdentifier,
		personIdentifier
		){

	// Initalizing local variables
	var allRelationsData = {
			 directorRoles : [],
			 officerRoles : []
	};
	$scope.editTab = true;
	$scope.isOrganisationPerson = false;   // to determine the entered personal
											// information is individual or for
											// current organization.
	// Initializing scope variables
	$scope.dataCuration = {
		tabActive : 0,
		locationsModel: [],
		countriesData : [],
		nationalityData: [],
		popupfoundedDate: {
			opened: false
		},
		popupbirthDate: {
			opened: false
		},
		popupdeathDate: {
			opened: false
		},
		dateOptionsto: {
			formatYear: 'yyyy',
		    maxDate: new Date(2020, 5, 22),
		    minDate: new Date(),
		    startingDay: 1
		},
	    openfoundedDate: openfoundedDate,
	    popupInactiveDate: {
			opened: false
		},
		inactiveDateOptions: {
			formatYear: 'yyyy',
		    maxDate: new Date(2020, 5, 22),
		    minDate: new Date(),
		    startingDay: 1
		},
		openInactiveDate: openInactiveDate,
		popupIPODate: {
			opened: false
		},
		dateOptionsfounded :{
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1850, 1, 1),
		    startingDay: 1
		},
		IPODateOptions: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1950, 1, 1),
		    startingDay: 1
		},
		dateOptionsbirth: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1935, 1, 1),
		    startingDay: 1
		},
		dateOptionsdeath: {
			formatYear: 'yyyy',
		    maxDate: new Date(),
		    minDate: new Date(1850, 1, 1),
		    startingDay: 1
		},
		openIPODate: openIPODate,
	    saveCurationDetails: saveCurationDetails,
	    birthDateModal: '',
		popupfromDate: {
			opened: false
		},
		 deathDateModal: '',
		 openbirthDate: openbirthDate,
		 opendeathDate: opendeathDate,
		 CurrentDay : "",
		 genderval : "male",
		 toggleSwitch: toggleSwitch,
		 changeGender: changeGender,
		 changePrefix: changePrefix,
		 resetFields :resetFields,
		 getCompanySuggestions: getCompanySuggestions,
		 addNewPerson:addNewPerson,
		 getPersonSuggestions: getPersonSuggestions,
		 directorLength : 0,
	 	 officerLength : 0,
	   	 directorsRetireve : false,
	  	 officersRetrieve : false,
	  	 ResetOrgFields : ResetOrgFields,
	  	 selectedAllDetails : true,
	  	 selectedAll : false,
	     CopyAddressDetails : [],
	     CheckDate : CheckDate,
	     OrgFoundedDate : "",
	     fromRelatedPerson : "",
	     toRelatedPerson :"",
	     personBirthDate : "",
		 personDeathDate : "",
//		 chkAddnewRelationPerson : chkAddnewRelationPerson,
		 chkDatePattern : "^[0-9-]+$"
	};

	$scope.allFormsModal = {
			organizationModal: {
//				"description": "",
	  			"bst:facebook": null,
	  			"mdaas:HeadquartersAddress": null,
	  			"isDomiciledIn": null,
	  			"bst:youtube": null,
	  			"hasLatestOrganizationFoundedDate": null,
	  			"tr-org:hasRegisteredPhoneNumber": null,
	  			"@is-manual": "true",
	  			"bst:linkedin": null,
	  			"mdaas:RegisteredAddress": null,
	  			"hasPrimaryEconomicSector": null,
//	  			"disambiguatingDescription": "",
				"hasHoldingClassification": null,
	  			"vcard:organization-name":null,
	  			"bst:email": null,
	  			"hasPrimaryBusinessSector": null,
	  			"hasRegisteredFaxNumber":null,
	  			"hasURL": null,
//	  			"hasIPODate": "",
	  			"@source-id": "data-curation-ui",
	  			"isIncorporatedIn": null,
//	  			"tr-org:hasInactiveDate": "",
//	  			"hasPrimaryInstrument": "",
	  			"tr-org:hasHeadquartersFaxNumber":null,
				"hasPrimaryIndustryGroup": null,
//	  			"hasActivityStatus": "false",
	  			"bst:VAT": null,
//	  			"tr-org:hasLEI": "",
	  			"bst:instagram": null,
	  			"bst:gplus": null,
	  			"tr-org:hasHeadquartersPhoneNumber": null,
	  			"bst:twitter": null,
//				"hasOrganizationPrimaryQuote": "",
				"bst:registrationId":null,
				"personalDetails" : []
			},
			personModal: {
				  "bst:facebook": null,
				  "vcard:hasAddress": [],
				  "vcard:hasGender": "male",
				  "vcard:hasName": null,
				  "bst:deathDate": null,
				  "bst:youtube": null,
				  "vcard:honorific-prefix": "Mr.",
				  "@is-manual": "true",
				  "bst:linkedin": null,
				  "vcard:hasPhoto": null,
				  "vcard:bday": null,
				  "vcard:hasTelephone": null,
				  "vcard:family-name": null,
				  "vcard:additional-name": null,
				  "bst:email": null,
				  "@source-id": "data-curation-ui",
				  "vcard:hasURL": null,
				  "vcard:honorific-suffix": null,
				  "tr-vcard:preferred-name": null,
				  "bst:aka": null,
				  "bst:description": null,
				  "vcard:given-name": null,
				  "bst:nationality": null,
				  "bst:instagram": null,
				  "bst:gplus": null,
				  "bst:twitter": null
				},
				relatedEntities:{

				}
	};
	$scope.ShowClicked = false;
	$scope.clickedPerson = {
			"personName" : "",
			"relation":"",
			"title": "Director",
			"from":$filter('date')(new Date(), 'yyyy-MM-dd'),
			"to":$filter('date')(new Date(), 'yyyy-MM-dd')
			};
//	var count = 0;
	$scope.persons = {
        	list: [],
        	list1 : [],
        	retrieve : true,
        	dtOptions: DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10)
    		        .withOption('bFilter', false)
    		        .withOption('lengthChange', false)
    		        .withDOM('lrtip'),
            dtColumnDefs: [
    		    DTColumnDefBuilder.newColumnDef(5).notSortable()
    		],
        	onClickEditStaging: onClickEditStaging
        };
	
	/*For display list of contents in Address table*/
    
    $scope.ContentAdd = function(){
		if($scope.dataCuration.tabActive == 1){
			if(!$scope.allFormsModal.personModal['vcard:hasAddress']){
				$scope.allFormsModal.personModal['vcard:hasAddress'] = [];
			}
			$scope.allFormsModal.personModal['vcard:hasAddress'].push({ 
	            'streetAddress': "",
	            'city': "",
	            'zip':'',
	            'country':'',
	            'selected' : true
	        });
	        setTimeout(function(){
	            $(".addressCountryDatas").chosen({allow_single_deselect: true});
	            $(".addressCountryDatas").trigger("chosen:updated");
	        },0);
		}else if($scope.dataCuration.tabActive == 0){
	        $scope.allFormsModal.organizationModal.personalDetails.push({ 
	        	'type' : 'HQ address',
	            'streetAddress': "",
	            'city': "",
	            'zip':'',
	            'country':'',
	            'selected' : true
	        });
	        setTimeout(function(){
	            $(".addressCountryDatas").chosen({allow_single_deselect: true});
	            $(".addressCountryDatas").trigger("chosen:updated");
	        },0);
		}	
    };

    $scope.ContentRemove = function(){
    	var newDataList=[];
    	if($scope.dataCuration.tabActive == 1){
            $scope.dataCuration.selectedAllDetails = false;
            angular.forEach($scope.allFormsModal.personModal['vcard:hasAddress'], function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            }); 
            $scope.allFormsModal.personModal['vcard:hasAddress'] = newDataList;
    	}else if($scope.dataCuration.tabActive == 0){
            $scope.dataCuration.selectedAll = false;
            angular.forEach($scope.allFormsModal.organizationModal.personalDetails, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            }); 
            $scope.allFormsModal.organizationModal.personalDetails = newDataList;
    	}

    };

$scope.checkAll = function () {
	if($scope.dataCuration.tabActive == 1){
	    if ($scope.dataCuration.selectedAllDetails) {
	        $scope.dataCuration.selectedAllDetails = true;
	    } else {
	        $scope.dataCuration.selectedAllDetails = false;
	    }
	    angular.forEach($scope.allFormsModal.personModal['vcard:hasAddress'], function(personalDetail) {
	        personalDetail.selected = $scope.dataCuration.selectedAllDetails;
	    });
	}else if($scope.dataCuration.tabActive == 0){
	    if ($scope.dataCuration.selectedAll) {
	    	$scope.dataCuration.selectedAll = true;
	    } else {
	        $scope.dataCuration.selectedAll = false;
	    }
	    angular.forEach($scope.allFormsModal.organizationModal.personalDetails, function(personalDetail) {
	        personalDetail.selected = $scope.dataCuration.selectedAll;
	    });
	}
};    

$scope.checkUncheckAll = function(){
	if($scope.dataCuration.tabActive == 1){
		$scope.dataCuration.selectedAllDetails = true;
		$("#CheckAllIdDetails").prop("checked",true);
	    for (var i = 0; i < $scope.allFormsModal.personModal['vcard:hasAddress'].length; i++) {
	        if (!$scope.allFormsModal.personModal['vcard:hasAddress'][i].selected) {
	            $scope.dataCuration.selectedAllDetails = false;
	            $("#CheckAllIdDetails").prop("checked",false);
	            break;
	        }
	    }
	}else if($scope.dataCuration.tabActive == 0){
		$scope.dataCuration.selectedAll = true;
		$("#CheckAllId").prop("checked",true);
	    for (var i = 0; i < $scope.allFormsModal.organizationModal.personalDetails.length; i++) {
	        if (!$scope.allFormsModal.organizationModal.personalDetails[i].selected) {
	            $scope.dataCuration.selectedAll = false;
	            $("#CheckAllId").prop("checked",false);
	            break;
	        }
	    }
	}
};

$scope.checkUncheckAll();
	
	
	
	/*
	 * @purpose: toggle switch @created: 8 feb 2018 @params: null @return: no
	 * @author: Ankit
	 */
	function toggleSwitch(hasStatus){
		$scope.allFormsModal.organizationModal.hasActivityStatus = !hasStatus;
	}

	if($stateParams.identifier !== 'new'){
		$scope.editTab = false;
		$scope.isLoading = true;
		var type =  $stateParams.identifier.split("#")[1];
		var id = $stateParams.identifier.split("#")[0];
		if(type == "02"){
			// getdirectorOfficerData();
			$scope.dataCuration.tabActive = 0;
			var url = EHUB_API + "entity/org/"+ id;
			getData(url,type);
		}else if(type == "01"){
			var url = EHUB_API + "entity/person/"+ id;
			getData(url,type);
			$scope.dataCuration.tabActive = 1;

		}
	}
	/*
	 * @purpose: change gender @created: 8 feb 2018 @params: null @return: no
	 * @author: Ankit
	 */
	 function changeGender(prefix){
		if(prefix == "Mrs." ||  prefix == "Miss"){
			$scope.allFormsModal.personModal['vcard:hasGender'] = "female";
		}else{
			$scope.allFormsModal.personModal['vcard:hasGender'] = "male";
		}

	 }
	 /*
		 * @purpose: get company suggestions @created: 8 feb 2018 @params: null
		 * @return: no @author: Ankit
		 */
	 function getCompanySuggestions(){
		//  return DataCurationApiService.getOrganationNamesSuggestions(val,10).then(function(orgResponse){
			 return getOrganationNamesSuggestions.hits;
		//  },function(orgError){

		//  });
	 }

	 /*
	     * @purpose: get person suggestions
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
	*/
	 function getPersonSuggestions(){
		//  return DataCurationApiService.getPersonNamesSuggestions(val).then(function(personResponse){
			 return getPersonNamesSuggestions.hits;
		//  },function(personError){

		//  });
	 }

	 /*
	     * @purpose: get company details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
		*/

	 $scope.$watch("allFormsModal.organizationModal['vcard:organization-name']", function(newVal){
		 if(typeof newVal === "object" && newVal){
			 $state.go("addDataCuration",{'identifier': newVal['@identifier']+"#02"}, {notify:true});
			 $stateParams.identifier = newVal['@identifier'];
			//  getdirectorOfficerData();
			 getData(EHUB_API + "entity/org/"+ newVal['@identifier'],"02");
		 }

	 });

	 /*
	     * @purpose: get person details
	     * @created: 8 feb 2018
	     * @params: null
	     * @return: no
	     * @author: Ankit
		*/

	 $scope.$watch("allFormsModal['relatedEntities'].entitySearch", function(newVal){
		$scope.persons.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0).notSortable(),
			DTColumnDefBuilder.newColumnDef(1).notSortable(),
			DTColumnDefBuilder.newColumnDef(2).notSortable(),
			DTColumnDefBuilder.newColumnDef(3).notSortable(),
			DTColumnDefBuilder.newColumnDef(4).notSortable(),
			DTColumnDefBuilder.newColumnDef(5).notSortable(),
		];
		 if(typeof newVal === "object" && newVal){
			 $scope.clickedPerson.identifier = newVal["@identifier"];
			 allRelationsData.personIdentifier = newVal["@identifier"];
			 $scope.clickedPerson.personName = newVal["vcard:hasName"];
//			 $scope.isOrganisationPerson = true;
//				openRelationModal();
			 
			 getlistforRelationship();
			 $scope.ShowClicked = true;
			 setTimeout(function(){
	 		     $("#relatedPersonId").chosen({allow_single_deselect: true});
			}, 100);

		 }

	 });

	 
	 $scope.$watch("allFormsModal.personModal['vcard:hasName']", function(newVal){
		 if(typeof newVal === "object" && newVal){
			 $scope.isLoading = true;
			 var url = EHUB_API + "entity/person/"+ newVal["@identifier"];
				getData(url,"01");
		 }

	 });

	 
	 function getlistforRelationship(){
		 $scope.allrelationsList = [];
		 angular.extend($scope.allrelationsList, allRelationsData.officerRoles);
		 angular.extend($scope.allrelationsList, allRelationsData.directorRoles);
		 $scope.allrelationsList = _.uniq($scope.allrelationsList);
	 }
	 
	 
//	 /*
//	     * @purpose: update chosen details
//	     * @created: 8 feb 2018
//	     * @params: null
//	     * @return: no
//	     * @author: Ankit
//		*/
//	 function update_all_chosen(){
//		 setTimeout(function(){
//			 $("#domiciledData").trigger("chosen:updated");
//
//			 $('#countriesData').trigger("chosen:updated")
//
//			 $('#industriesData').trigger("chosen:updated")
//
//			 $('#EconomicSectorData').trigger("chosen:updated")
//
//			 $('#businessSectorDatas').trigger("chosen:updated")
//
//			 $("#nationalityDatas").trigger("chosen:updated");
//			 
//			 $(".addressCountryDatas").trigger("chosen:updated");
//		 }, 100);
//	 }
//
	 
	 $scope.changeRoleType = function(){
		// console.log('selected Option',$scope.clickedPerson.relation);
		 if($scope.clickedPerson.relation == "Executive Director"){
			 $scope.clickedPerson.title = "officer/director";
		 }else{
			 if(allRelationsData.officerRoles.indexOf($scope.clickedPerson.relation) > -1){
				 $scope.clickedPerson.title = "officer";
			 }else{
				 $scope.clickedPerson.title = "director";
			 } 
		 }

	 };
	 
	 $scope.addNewRelation = function(){
		 //console.log($scope.dataCuration.fromRelatedPerson, $scope.dataCuration.toRelatedPerson,$scope.dataCuration.fromRelatedPerson !== "" , $scope.dataCuration.toRelatedPerson !== "","whats the situation??");
		 if($scope.dataCuration.fromRelatedPerson !== "" || $scope.dataCuration.toRelatedPerson !== ""){
			 HostPathService.FlashErrorMessage('Dates must be in yyyy-mm-dd format','');
			 return false;
		 }
//		 return false;
		 if(!$scope.clickedPerson.relation){
			HostPathService.FlashErrorMessage('Relation field cannot be empty','');
			return false;
		 }
		 var role;
		 //console.log($scope.clickedPerson.title,"whay??",$scope.clickedPerson.relation);
		 if($stateParams.identifier != "new"){
			 if($scope.clickedPerson.title == 'director')
					{role = 'directorship';}
				else if($scope.clickedPerson.title == 'officer')
					{role = 'officership';}
				else if($scope.clickedPerson.title == "officer/director")
					{role = 'officership';}
			 
				var rolesData = {};
				rolesData['@source-id'] = 'data-curation-ui';
				rolesData.to = $scope.clickedPerson.to || null;
				rolesData.from = $scope.clickedPerson.from;
//				rolesData['hasReportedTitle'] = relationVal['hasReportedTitle'];
				rolesData.hasPositionType= $scope.clickedPerson.relation;
				rolesData.hasHolder = $scope.clickedPerson.identifier;
				rolesData.isPositionIn = $stateParams.identifier.split("#")[0];
				rolesData['@is-manual'] = "true";
				DataCurationApiService.saveRelationdata(role, rolesData).then(function(relationResponse){
					var response = angular.extend(relationResponse,{"name":$scope.clickedPerson.personName});
					$rootScope.$broadcast('eventFired', {
		                data: response
		            });
					HostPathService.FlashSuccessMessage('Added Role Successfully','');
					$scope.ShowClicked = false;
					$scope.persons.dtColumnDefs = [
						DTColumnDefBuilder.newColumnDef(5).notSortable()
					];
				}, function(relationError){
					console.log(relationError);
					HostPathService.FlashErrorMessage('Sorry, Failed To Add Person id '+rolesData.hasHolder,'');
				}); 
		 }else{
			 HostPathService.FlashErrorMessage('Sorry, Organisation Id is not available','');
		 }
	 };
	 
	 function changePrefix(gender){
		 if(gender == "female")
			 {$scope.allFormsModal.personModal['vcard:honorific-prefix'] = "Miss";}
		 else
			 {$scope.allFormsModal.personModal['vcard:honorific-prefix'] = "Mr.";}
	 }

	function openfoundedDate(){
		$scope.dataCuration.popupfoundedDate.opened = true;
	}

	function openbirthDate(){
		$scope.dataCuration.dateOptionsbirth.maxDate = $scope.allFormsModal.personModal['bst:deathDate'];
		$scope.dataCuration.popupbirthDate.opened = true;
	}

	function opendeathDate(){
		$scope.dataCuration.dateOptionsdeath.minDate = $scope.allFormsModal.personModal['vcard:bday'];
		$scope.dataCuration.popupdeathDate.opened = true;
	}

	function openInactiveDate(){
		$scope.dataCuration.popupInactiveDate.opened = true;
	}

	function openIPODate(){
		$scope.dataCuration.popupIPODate.opened = true;
	}

	/*
	 * @purpose: get countries/locations names @created: 2 feb 2018 @params:
	 * null @return: no @author: Ankit
	 */
	// DataCurationApiService.getAllCountriesNames().then(function(countriesResponse){
		$scope.dataCuration.countriesData = getAllCountriesNames;
	// },function(countriesError){
	// 	console.log(countriesError)
	// });
	/*
	 * @purpose: get Industries names @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */
	// DataCurationApiService.getAllIndustriesNames().then(function(IndustriesResponse){
		$scope.dataCuration.industriesData = getAllIndustriesNames;
	// },function(industriesError){
	// 	console.log(industriesError)
	// });

	/*
	 * @purpose: get business Sector Data @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */

	// DataCurationApiService.getbusinessSectorsData().then(function(IndustriesResponse){
		$scope.dataCuration.businessSectorData = getbusinessSectorsData;
	// },function(industriesError){
	// 	console.log(industriesError)
	// });

	/*
	 * @purpose: get Economic Sector names @created: 3 feb 2018 @params: null
	 * @return: no @author: Virendra
	 */
	// DataCurationApiService.getAllEconomicSectorsData().then(function(IndustriesResponse){
		$scope.dataCuration.economicSectorData =getAllEconomicSectorsData;
	// },function(industriesError){
	// 	console.log(industriesError)
	// });

	/*
	 * @purpose: get nationality names @created: 2 feb 2018 @params: null
	 * @return: no @author: Ankit
	 * not working
	 */
	// DataCurationApiService.getAllNationalityData().then(function(nationalityResponse){
	// 	$scope.dataCuration.nationalityData = nationalityResponse.data;
	// },function(nationalityError){
	// 	console.log(nationalityError)

	// });

	/*
	 * @purpose: get director roles @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	// DataCurationApiService.getAllDirectorRoles().then(function(directorResponse){
		allRelationsData.directorRoles = getAllDirectorRoles;
	// },function(directorError){
	// 	console.log(directorError)
	// });

	/*
	 * @purpose: get officer roles @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	// DataCurationApiService.getAllOfficerRoles().then(function(officerResponse){
		allRelationsData.officerRoles = getAllOfficerRoles;
	// },function(officerError){
	// 	console.log(officerError)
	// });

	/*
	 * @purpose: get nationality names @created: 2 feb 2018 @params: null
	 * @return: no @author: Ankit
	 */
	function getData(data,type){
		var resp = {};/*jshint ignore:line*/
		// DataCurationApiService.getIdentifierInfo(data).then(function(resp){
			if(type == "02"){
				var addressData = []; 
				angular.extend($scope.allFormsModal.organizationModal, organisationIdentifier);
				$scope.allFormsModal.organizationModal.hasLatestOrganizationFoundedDate = new Date(organisationIdentifier.hasLatestOrganizationFoundedDate);
				$scope.allFormsModal.organizationModal.hasIPODate = new Date(organisationIdentifier.hasIPODate);
				$scope.allFormsModal.organizationModal['tr-org:hasInactiveDate'] = new Date(organisationIdentifier["tr-org:hasInactiveDate"]);
				$scope.allFormsModal.organizationModal.hasActivityStatus = organisationIdentifier.hasActivityStatus ==='Inactive' ? false :true;
				$scope.allFormsModal.organizationModal.hasActivityStatus.toString();
				if(organisationIdentifier["mdaas:HeadquartersAddress"] && organisationIdentifier["mdaas:HeadquartersAddress"].length > 0){
					angular.forEach(organisationIdentifier["mdaas:HeadquartersAddress"],function(d){
						var updateType = $.extend({},d,{"type":"HQ address",'selected' : true});
						addressData.push(updateType);
					});
				}
				if(organisationIdentifier["mdaas:RegisteredAddress"] && organisationIdentifier["mdaas:RegisteredAddress"].length > 0){
					angular.forEach(organisationIdentifier["mdaas:RegisteredAddress"],function(d){
						var updateType = $.extend({},d,{"type":"Registered address",'selected' : true});
						addressData.push(updateType);	
					});
				}
				$scope.allFormsModal.organizationModal.personalDetails= addressData;
			}else if(type == "01"){
				$scope.allFormsModal.personModal = {};
				angular.extend($scope.allFormsModal.personModal, personIdentifier);
				AddSelectKey($scope.allFormsModal.personModal["vcard:hasAddress"]);
				$scope.allFormsModal.personModal['bst:deathDate'] =personIdentifier["bst:deathDate"] ? new Date(personIdentifier["bst:deathDate"]) : "";
				$scope.allFormsModal.personModal['vcard:bday'] =personIdentifier["vcard:bday"] ? new Date(personIdentifier["vcard:bday"]) : "";
			}
			$scope.isLoading = false;
		// },function(nationalityError){
		// 	$scope.isLoading = false;
		// });
	}

	/*
	 * @purpose: save DataCuration Details @created: 3 feb 2018 @params: null
	 * @return: no @author: Ankit
	 */
	function saveCurationDetails(curationData,createNew){
		if($scope.dataCuration.tabActive == 1){
			curationData.personModal["vcard:bday"]  = $filter('date')(curationData.personModal["vcard:bday"], 'yyyy-MM-dd') || null ;
			curationData.personModal["bst:deathDate"] = $filter('date')(curationData.personModal["bst:deathDate"], 'yyyy-MM-dd') || null ;
			angular.forEach(curationData.personModal["vcard:hasAddress"],function(d){
				delete  d.selected;
			});
			if(typeof(curationData.personModal["bst:aka"]) == "string")
			{curationData.personModal["bst:aka"] = curationData.personModal["bst:aka"].split(",");}
				if(curationData.personModal.hasOwnProperty("@status") || curationData.personModal.hasOwnProperty("@identifier")  || curationData.personModal.hasOwnProperty("@created")){
					var identifier = curationData.personModal["@identifier"];
					// var identifier = $stateParams.identifier.split("#")[0];
					curationData.personModal["@status"] ? delete curationData.personModal["@status"] : "";/*jshint ignore:line*/
					curationData.personModal["@identifier"] ? delete curationData.personModal["@identifier"] : " ";/*jshint ignore:line*/
					curationData.personModal["@created"] ?  delete curationData.personModal["@created"] : "";/*jshint ignore:line*/
					curationData.personModal["@updated"] ?  delete curationData.personModal["@updated"] : "";/*jshint ignore:line*/
					curationData.personModal["@is-manual"] =  "true";
					DataCurationApiService.updatePersonDataCurationDetails(curationData.personModal,identifier).then(function(){
						HostPathService.FlashSuccessMessage('Updated Suceesfully','');
						$scope.allFormsModal.personModal["@identifier"] = identifier;
						$state.go("addDataCuration",{'identifier': $stateParams.identifier});
						AddSelectKey($scope.allFormsModal.personModal["vcard:hasAddress"]);
					},function(personError){
						HostPathService.FlashErrorMessage('Sorry, Failed To Add Person details','');
						$scope.allFormsModal.personModal["@identifier"] = identifier;
						console.log(personError,'personError');
						AddSelectKey($scope.allFormsModal.personModal["vcard:hasAddress"]);
					});
				}else{
					DataCurationApiService.savePersonDataCurationDetails(curationData.personModal).then(function(personResponse){
						AddSelectKey($scope.allFormsModal.personModal["vcard:hasAddress"]);
						if(personResponse.data.identifier && $scope.isOrganisationPerson){
							allRelationsData.personIdentifier = personResponse.data.identifier;
							allRelationsData.personName = curationData.personModal["vcard:hasName"];
							openRelationModal();
						}
					},function(personError){
						HostPathService.FlashErrorMessage('Sorry, Failed To Add Person details','');
						AddSelectKey($scope.allFormsModal.personModal["vcard:hasAddress"]);
						console.log(personError,'personError');
					});
				}
			}else if($scope.dataCuration.tabActive == 0){
			curationData.organizationModal.hasActivityStatus = curationData.organizationModal.hasActivityStatus ? curationData.organizationModal.hasActivityStatus.toString() : "true";
			//delete curationData.organizationModal.personalDetails;
			curationData.organizationModal["mdaas:RegisteredAddress"] = [];
			curationData.organizationModal["mdaas:HeadquartersAddress"] = [];
			angular.forEach(curationData.organizationModal.personalDetails,function(d){
				if(d.type == "HQ address"){
					var key = {
							   "streetAddress": d.streetAddress,  
						       "city": d.city,
						       "zip": d.zip,  
						       "country": d.country
					};
					curationData.organizationModal["mdaas:HeadquartersAddress"].push(key);

				}else if(d.type =="Registered address"){
					var key = {
							   "streetAddress": d.streetAddress,  
						       "city": d.city,
						       "zip": d.zip,  
						       "country": d.country
					};
					curationData.organizationModal["mdaas:RegisteredAddress"].push(key);
				}
			});
			$scope.CopyAddressDetails = [];
			$scope.CopyAddressDetails = curationData.organizationModal.personalDetails;
			delete curationData.organizationModal.personalDetails;
			if(typeof(curationData.organizationModal.hasURL) == "string")
				{curationData.organizationModal.hasURL = curationData.organizationModal.hasURL.split(",");}
			if($stateParams.identifier == "new"){
				if(curationData.organizationModal['vcard:organization-name'] && curationData.organizationModal['bst:registrationId'] && curationData.organizationModal.hasURL){
				DataCurationApiService.saveOrgDataCurationDetails(curationData.organizationModal).then(function(organizationResponse){
					allRelationsData.orgIdentifier = organizationResponse.data.identifier;
					$stateParams.identifier = organizationResponse.data.identifier;
// $scope.allFormsModal.organizationModal.identifier =
// organizationResponse.data.identifier;
				$stateParams.identifier = organizationResponse.data.identifier;
				HostPathService.FlashSuccessMessage('Organisation added Suceesfully','');
					if(createNew){
						$state.go("addDataCuration",{'identifier': "new"}, {notify:true});
					}else{
						$state.go("addDataCuration",{'identifier': organizationResponse.data.identifier+"#02"}, {notify:false});
					}
					$scope.editTab = false;
					$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
				},function(organizatioError){
					HostPathService.FlashErrorMessage('Sorry, Failed To Add Organisation details','');
					console.log(organizatioError,'organizatioError');
					$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
				});
			}else{
					HostPathService.FlashErrorMessage('All * marked are mandetory','');
					$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
				}
			}else{
				if(curationData.organizationModal['vcard:organization-name'] && curationData.organizationModal['bst:registrationId'] && curationData.organizationModal.hasURL){
					$scope.samplevar = curationData.organizationModal["@identifier"];
					delete curationData.organizationModal["@created"];
					delete curationData.organizationModal["@identifier"];
					delete curationData.organizationModal["@status"];
					delete curationData.organizationModal["@updated"];
					DataCurationApiService.updateOrgDataCurationDetails(curationData.organizationModal).then(function(){
					$state.go("addDataCuration", {notify:true});
					HostPathService.FlashSuccessMessage('Updated','');
					$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
					},function(organizatioError){
						console.log(organizatioError,'organizatioError');
						HostPathService.FlashErrorMessage('Sorry, Failed To Add Organisation details','');
						$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
					});
			}else{
					HostPathService.FlashErrorMessage('All * marked are mandetory','');
					$scope.allFormsModal.organizationModal.personalDetails = $scope.CopyAddressDetails;
				}
			}
		}

	}

	/*
	 * @purpose: open Relation modal @created: 8 feb 2018 @params: null @return:
	 * no @author: Ankit
	 */
	function openRelationModal(){
		var relationtModalInstance = $uibModal.open({
            templateUrl: 'scripts/manage/modal/views/relation.modal.html',
            controller: 'RelationModalController',
            windowClass: 'custom-modal related-person-modal relation-curation-modal',
            resolve: {
            	AllRelationsData: function(){
            		return allRelationsData;
            	}
            }

		});

		relationtModalInstance.result.then(function () {
        }, function () {
        });
	}
	/*
	 * @purpose: append same value @created: 3 feb 2018 @params: null @return:
	 * no @author: Virendra
	 */
	$scope.appendSameValue = function(status){
		if(status){
			$scope.allFormsModal.organizationModal['mdaas:HeadquartersAddress'] = $scope.allFormsModal.organizationModal['mdaas:RegisteredAddress'];
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersPhoneNumber'] = $scope.allFormsModal.organizationModal['tr-org:hasRegisteredPhoneNumber'];
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersFaxNumber'] = $scope.allFormsModal.organizationModal.hasRegisteredFaxNumber;
		}else{
			$scope.allFormsModal.organizationModal['mdaas:HeadquartersAddress'] = "";
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersPhoneNumber'] = "";
			$scope.allFormsModal.organizationModal['tr-org:hasHeadquartersFaxNumber'] = "";
		}

	};

	document.addEventListener("keydown", function(e) {
		  if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {/*jshint ignore:line*/
		    e.preventDefault();
		    saveCurationDetails($scope.allFormsModal);
		  }
		}, false);

//	$scope.searchSelectAllSettings = { template: '{{option}}', smartButtonTextConverter(skip, option) { return option; }, enableSearch: true, showSelectAll: true, keyboardControls: true ,selectedToTop:true, smartButtonMaxItems: 1,selectionLimit: 1,showCheckAll : false,showUncheckAll:false,scrollableHeight: '200px', scrollable: true};
//	$scope.SearchalternativeNamesData = [ {id: 1, label: "David"}, {id: 2, label: "Jhon"}, {id: 3, label: "Danny7"},{id: 1, label: "David6"},{id: 1, label: "David5"},{id: 1, label: "David4"},{id: 1, label: "David3"},{id: 1, label: "David2"},{id: 1, label: "David1"}, ];
//	$scope.SearchalternativeNamesModel = [];
//	$scope.SetNames = {
//			onSelectionChanged() { // This event is not firing on selection of
//									// max limit
//				$scope.staging['general_information']['other_names'] = "";
//				if($scope.SearchalternativeNamesModel.length > 0)
//				{
//
//				angular.forEach($scope.SearchalternativeNamesModel,function(d,i){
//					if(i != 0)
//						$scope.staging['general_information']['other_names'] += " , ";
//
//					$scope.staging['general_information']['other_names'] +=	d.label;
//				});
//				}
//			},
//	};

	$rootScope.ChangeForm =function(val){
		$cookies.put("selection", val);
		if(val == "simple")
			{$rootScope.CheckDisable = false;}
		else
			{$rootScope.CheckDisable= true;}
	};

	setTimeout(function(){
		if(!$cookies.get('selection')){
			$rootScope.CheckDisable = false;
			$("#simpleForm").prop("checked",true);
			$cookies.put("selection", 'simple');
		}else{

				var val = $cookies.get('selection');
				if(val == "simple"){
					$rootScope.CheckDisable = false;
					$("#simpleForm").prop("checked",true);
				}else{
					$rootScope.CheckDisable= true;
					$("#fullForm").prop("checked",true);
				}
		}
	},10);
	$scope.dataCuration.locationEvents = {

			onInitDone: function(){ // This event is not firing on selection of max
							// limit
				$(".OtherLocationsScroll").find("ul").mCustomScrollbar({
					axis : "y",
					theme : "minimal"
				});
// $scope.staging['general_information']['other_locations'] = "";
// if($scope.dataCuration.locationsModel.length > 0)
// {
//
// angular.forEach($scope.dataCuration.locationsModel,function(d,i){
// if(i != 0)
// $scope.staging['general_information']['other_locations'] += " , ";
//
// $scope.staging['general_information']['other_locations'] += d.label;
// });
// }
			}
	};

	/*-----functions----*/
	  $scope.onSelect = function (item) {
		var aa = $scope.names.indexOf(item);
		$scope.job_title= $scope.names[aa].title;
		$scope.job_category = $scope.names[aa].catagory;
		$scope.addNewDetails.hide();
	  };

	  $scope.getStates = function(current) {
		    var statesCopy = $scope.groups.slice(0);
		    if (current) {
		      statesCopy.unshift(current);
		    }
		    return statesCopy;
		  };

	  $scope.onSet=function(item){
		  if($scope.groups.indexOf(item) < 0){
			  $scope.groups.push(item);
		  }
	  };
	  
//	  setTimeout(function(){
//		    $('#domiciledData').chosen({allow_single_deselect: true});
//
//		    $('#countriesData').chosen({allow_single_deselect: true});
//
//		    $('#industriesData').chosen({allow_single_deselect: true});
//
//		    $('#EconomicSectorData').chosen({allow_single_deselect: true});
//
//		    $('#businessSectorDatas').chosen({allow_single_deselect: true});
//
//		    $("#nationalityDatas").chosen({allow_single_deselect: true});
//		    
//		    
//		    $(".addressCountryDatas").chosen({allow_single_deselect: true});
//
//// $("#nationalityDatas_chosen").width("289px");
//	  },100);

	  $scope.$on('eventFired', function(event, data) {
		  var key = {
				"name" : data.data.name,
				"position":data.data.config.data.hasPositionType, //d.hasPositionType,
				"title": "person",
				"from": data.data.config.data.from, //d.from,
				"to": data.data.config.data.to, //.to,
				"orgId":data.data.config.data.isPositionIn, //.isPositionIn,
				"personId":data.data.config.data.hasHolder   ///.hasHolder
		  };
			$scope.persons.list.push(key);
	  });

	    /*
		 * @purpose: Getting person details @created: 6th Feb 2018 @params:
		 * params @return: no @author: Zameer
		 */
	  	var officersTotal,DirectorTotal;
	    function getdirectorOfficerData(){/*jshint ignore:line*/
		var dirApi = EHUB_API + 'datacuration/relation/directorship/search';
		var officerAPI =  EHUB_API + 'datacuration/relation/officership/search';
		var id = $stateParams.identifier.split("#")[0];
		var data = {  "isPositionIn": id };
		/*--- for list of Directorship ----*/
		  DataCurationApiService.getDirectorOfficeriInfodetails(dirApi, data).then(function(response){
				if(response.data.hits.length > 0){
					DirectorTotal = response.data.hits.length;
					$scope.dataCuration.directorLength = 0;
					angular.forEach(response.data.hits,function(d){
						  var url = EHUB_API + 'entity/person/'+ d.hasHolder;
						  DataCurationApiService.getPersonInDetails(d,url).then(function(res){
								$scope.dataCuration.directorLength++;
							  var key = {
								  		"name" : res[0].data["vcard:hasName"],
										"position":res[1].hasPositionType,
										"title": "person",
										"from":res[1].from,
										"to":res[1].to,
										"orgId":res[1].isPositionIn,
										"personId":res[1].hasHolder
								};
								$scope.persons.list1.push(key);
							  	if($scope.dataCuration.directorLength == DirectorTotal){
									$scope.dataCuration.directorsRetireve = true;
									checkContent();
								}
						  },function(error){
							  HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
						  });
						});
				}else{
					$scope.dataCuration.directorsRetireve = true;
					checkContent();
				}
				$scope.isLoading = false;
			}, function(error){
				$scope.isLoading = false;
				HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
			});

		  /*----  for list of officership  -----*/
		  DataCurationApiService.getDirectorOfficeriInfodetails(officerAPI, data).then(function(response){
			  if(response.data.hits.length > 0){
				  officersTotal = response.data.hits.length;
				  $scope.dataCuration.officerLength = 0;
				  angular.forEach(response.data.hits,function(d){
				  var url = EHUB_API + 'entity/person/'+ d.hasHolder;
				  DataCurationApiService.getPersonInDetails(d,url).then(function(res){
					  $scope.dataCuration.officerLength++;
					  var key = {
					  		"name" : res[0].data["vcard:hasName"],
							"position":res[1].hasPositionType,
							"title": "person",
							"from":res[1].from,
							"to":res[1].to,
							"orgId":res[1].isPositionIn,
							"personId":res[1].hasHolder

					};
					$scope.persons.list1.push(key);
					  if( $scope.dataCuration.officerLength == officersTotal){
						  $scope.dataCuration.officersRetrieve = true;
						  checkContent();
					  }
				  },function(error){
					  HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
				  });
				});
			  }else{
				  $scope.dataCuration.officersRetrieve = true;
				  checkContent();
			  }
			}, function(error){
				HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
			});
		}

		function onClickEditStaging(stage){
			$scope.dataCuration.tabActive = 1;
			var url = EHUB_API + "entity/person/"+ stage.personId;
			var type = "01";
			$scope.isLoading = true;
			getData(url,type);
		}

		function resetFields(){
			$scope.allFormsModal.personModal = {
					  "bst:facebook": null,
					  "vcard:hasAddress": [],
					  "vcard:hasGender": "male",
					  "vcard:hasName": null,
					  "bst:deathDate": null,
					  "bst:youtube": null,
					  "vcard:honorific-prefix": "Mr.",
					  "@is-manual": "true",
					  "bst:linkedin": null,
					  "vcard:hasPhoto": null,
					  "vcard:bday": null,
					  "vcard:hasTelephone": null,
					  "vcard:family-name": null,
					  "vcard:additional-name": null,
					  "bst:email": null,
					  "@source-id": "data-curation-ui",
					  "vcard:hasURL": null,
					  "vcard:honorific-suffix": null,
					  "tr-vcard:preferred-name": null,
					  "bst:aka": null,
					  "bst:description": null,
					  "vcard:given-name": null,
					  "bst:nationality": null,
					  "bst:instagram": null,
					  "bst:gplus": null,
					  "bst:twitter": null
					};
		}

		function addNewPerson(){
			// chkAddnewRelationPerson("addnew");
			$scope.dataCuration.tabActive = 1;
			$scope.isOrganisationPerson = true;
			resetFields();
		}
		
		function checkContent(){
			if($scope.dataCuration.directorsRetireve && $scope.dataCuration.officersRetrieve){
				$scope.persons.list = $scope.persons.list1;
			}
		}

		
		/*
		 * Function to reset the Organisation current fields data
		 * 
		 * */
		
		function ResetOrgFields(){
			 $state.go("addDataCuration",{'identifier': "new"}, {notify:true});
			 $stateParams.identifier = "new";
		}
		
		/*
		 * For Adding Selection key for Address in Person
		 * */
		function AddSelectKey(data){
			angular.forEach(data,function(d){
				angular.extend(d,{"selected": true});
			});
		}
		
		/*
		 * @purpose: openDatepickerModal function @created: 09 oct 2017 @params:
		 * modelType(string), fieldType(string), index(nnumber) @returns: none
		 * @author: sandeep
		 */
	 	function openDatepickerModal($event, dateObject){/*jshint ignore:line*/
	 	    $event.preventDefault();
	 	    $event.stopPropagation();
	 	    dateObject.isModalOpened = true;
	 	}

		$scope.disableSave = function(){
			$scope.dsbl = true;
		};
		
		$scope.enableSave = function(){
			$scope.dsbl = false;
		};
//		function CheckDate(){
//			if($("#OrgFoundedDate").val().indexOf("-",4)!== -1 && $("#OrgFoundedDate").val().indexOf("-",7)!== -1 && $("#OrgFoundedDate").val().indexOf("-",8)=== -1){
//				if(!isNaN(new Date($("#OrgFoundedDate").val()).getTime()) &&  new Date($("#OrgFoundedDate").val()).getTime() > new Date().getTime()){
//					$scope.dataCuration.alertErrorMessage = "Founded date can't be greater than today";
//				}else{
//					$scope.dataCuration.alertErrorMessage = "";
//				}
//			}else{
//				if($("#OrgFoundedDate").val().length > 0){
//					var dates = $filter('date')(new Date(),'yyyy-MM-dd');
//					$scope.dataCuration.alertErrorMessage = "Date Format Should be " + dates;
//				}else{
//					$scope.dataCuration.alertErrorMessage = "";
//				}
//			}
//		}
		
		function CheckDate(idValue,checkVal){
			//console.log(new Date($("#"+idValue).val()).getTime(),' szv ',$("#"+idValue).val().length,idValue);
			if( $("#"+idValue).val().length == 10 && !isNaN(new Date($("#"+idValue).val()).getTime()) && $("#"+idValue).val().indexOf("-",4)!== -1 && $("#"+idValue).val().indexOf("-",7)!== -1 && $("#"+idValue).val().indexOf("-",8)=== -1 ){
					if(new Date($("#"+idValue).val()).getTime() > new Date().getTime()){
						$scope.dataCuration[idValue] = "Founded date can't be greater than today";
						if(checkVal){
							return false;
						}
					}else{
						$scope.dataCuration[idValue] = "";
						if(idValue != 'OrgFoundedDate' && !checkVal){
							getComparedDates(idValue);
						}else if(checkVal){
							return true;
						}
					}
			}else{
				if($("#"+idValue).val().length > 0){
					var dates = $filter('date')(new Date(),'yyyy-MM-dd');
					$scope.dataCuration[idValue] = "Date Format Should be " + dates;
					if(checkVal){
						return false;
					}					
				}else{
					$scope.dataCuration[idValue] = "";
				}
			}
		}
		
		function getComparedDates(val){
			var received_Val = val;
			var compareVal = "";
			if(val == "fromRelatedPerson"){
				compareVal = "toRelatedPerson";
			}else if(val == "toRelatedPerson"){
				compareVal = "fromRelatedPerson";
				[val,compareVal] = [compareVal,val];
			}else if(val == "personBirthDate"){
				compareVal = "personDeathDate";
			}else if(val == "personDeathDate"){
				compareVal = "personBirthDate";
				[val,compareVal] = [compareVal,val];
			}
			var getVal = CheckDate(received_Val,"checkVal");
			if(getVal)
			if(new Date($("#"+val).val()).getTime() > new Date($("#"+compareVal).val()).getTime()){/*jshint ignore:line*/
				$scope.dataCuration[received_Val] = "Please check the date,somethings wrong!";
			}else{
				$scope.dataCuration[val] = "";
				$scope.dataCuration[compareVal] = "";
			}
		}

		// function chkAddnewRelationPerson(checkRP){
		// 	if(checkRP){
		// 		$scope.isOrganisationPerson = true;
		// 		$scope.dataCuration.tabActive = 1;
		// 	}else{
		// 		$scope.isOrganisationPerson = false;
		// 	}
		// 	console.log("dkajs",$scope.isOrganisationPerson);

		// }
}