'use strict';
angular.module('ehubApp')
	   .controller('AddUserModalController', addUserModalController);

       addUserModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModal',
			'$uibModalInstance'
		];
		
		function addUserModalController(
				$scope, 
				$rootScope,
				$state,
				$uibModal,
				$uibModalInstance) {

			/*
			 * @purpose: Defining scope variables
			 * @author: Varsha
			 */
			$scope.addUserModel = {
					cancelAddModal:cancelAddModal
			};
			
			 /*
			 * @purpose: Close Add User Model
			 * @created: 09 oct 2017
			 * @params: null
			 * @returns: no
			 * @author: Varsha
			 */
			function cancelAddModal(){
				$uibModalInstance.dismiss('close');
			}
			
}