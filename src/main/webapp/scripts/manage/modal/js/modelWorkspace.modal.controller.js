'use strict';
angular.module('ehubApp')
	   .controller('ModelWorkspaceModalController', modelWorkspaceModalController);

		modelWorkspaceModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance'
		];
		
		function modelWorkspaceModalController(
				$scope, 
				$rootScope,
				$uibModalInstance) {
			
			/*
			 * @purpose: Defining scope variables
			 * @author: swathi
			 */
			$scope.modelWorkspaceModal = {
				cancelModal: cancelModal
			};
			
			 /*
			 * @purpose: Close model workspace Modal
			 * @created: 17 oct 2017
			 * @params: null
			 * @returns: no
			 * @author: swathi
			 */
			function cancelModal(){
				$uibModalInstance.dismiss('close');
			}
			
}