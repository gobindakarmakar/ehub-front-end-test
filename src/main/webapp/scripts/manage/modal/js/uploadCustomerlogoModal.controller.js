'use strict';
var app = angular.module('ehubApp');
app.controller('UploadCustomerLogoModalController', uploadCustomerLogoModalController);

       uploadCustomerLogoModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance',
			'settingId',
			'GeneralSettingsApiService',
			'HostPathService'
		];
		
		function uploadCustomerLogoModalController(
				$scope, 
				$rootScope,
				$uibModalInstance,
				settingId,
				GeneralSettingsApiService,/*jshint ignore:line*/
				HostPathService) {/*jshint ignore:line*/
					$scope.image = null;
					$scope.imageFileName = '';
					
					$scope.uploadme = {};
					$scope.uploadme.src = '';
					$scope.closeUploadCustomerLogoModal = function () {
						// $uibModalInstance.close(changeSignificance);
						$scope.uploadData = {};
						if($scope.uploadme.src){
							$scope.uploadData ={
								customerLogoImage : $scope.uploadme.src.split(',')[1],
								settingId:settingId.settingId,
								systemSettingType:settingId.systemSettingType,
								name:settingId.name,
								section:settingId.section,
								selectedValue:settingId.selectedValue,
								defaultValue:settingId.selectedValue
							};
						}
						$uibModalInstance.close($scope.uploadData);
					};
				}


				app.directive('fileDropzone', function() {
					return {
					  restrict: 'A',
					  scope: {
						file: '=',
						fileName: '=',
						closemodal:"&",
						readfile:'='
					  },
					  link: function(scope, element, attrs) {
						var checkSize,
							isTypeValid,
							processDragOverOrEnter,
							validMimeTypes;
						
						processDragOverOrEnter = function (event) {
						  if (event != null) {
							event.preventDefault();
						  }
						  $('#imageDiv').css({ 'border': '2px dashed #545454'});
						  event.originalEvent.dataTransfer.effectAllowed = 'copy';
						  return false;
						};
						
						validMimeTypes = attrs.fileDropzone;
						
						checkSize = function(size,loadend) {
						  var _ref;
						  if (((_ref = attrs.maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < attrs.maxFileSize) {
							return true;
						  } else {
							if(!loadend){
								alert("File must be smaller than " + attrs.maxFileSize + " MB");/*jshint ignore:line*/
							}
							return false;
						  }
						};
				  
						isTypeValid = function(type,loadend) {
						  if ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1) {
							return true;
						  } else {
							if(!loadend){
								alert("Invalid file type.  File must be one of following types " + validMimeTypes);/*jshint ignore:line*/
						  	}
							return false;
						  }
						};				
						element.bind('dragover', processDragOverOrEnter);
						element.bind('dragenter', processDragOverOrEnter);
				  
						return element.bind('drop', function(event) {
						  var file, name, reader, size, type;
						  if (event != null) {
							event.preventDefault();
						  }
						  reader = new FileReader();
						  reader.onload = function(evt) {
							if (checkSize(size) && isTypeValid(type)) {
							
								//Initiate the JavaScript Image object.
							var image = new Image();/*jshint ignore:line*/
							//Set the Base64 string return from FileReader as source.
							image.src = evt.target.result;
							image.onload = function () {
								//Determine the Height and Width.
								var height = this.height;
								var width = this.width;
								if (height > 70 || width > 320) {
										alert("Image resolution must be  70px X 320px.");/*jshint ignore:line*/
									return false;
								}
								return scope.$apply(function() {
									scope.file = evt.target.result;
									scope.readfile = evt.target.result;
									if (angular.isString(scope.fileName)) {
									  return scope.fileName = name;/*jshint ignore:line*/
									}
								  });
							};								
							
							}
						  };
						  reader.onloadend = function(evt){
							if (checkSize(size,'loadend') && isTypeValid(type,'loadend')) {
								//Initiate the JavaScript Image object.
								var image = new Image();/*jshint ignore:line*/
								//Set the Base64 string return from FileReader as source.
								image.src = evt.target.result;
								image.onload = function () {
									//Determine the Height and Width.
									var height = this.height;
									var width = this.width;
									if (height > 70 || width > 320) {
										return false;
									}									
									scope.$apply(function () {
										scope.closemodal();
									});
								};
							  }
							  $('#imageDiv').css({ 'border': '1px dashed #545454'});
						  };
						  file = event.originalEvent.dataTransfer.files[0];
						  name = file.name;
						  type = file.type;
						  size = file.size;
						  reader.readAsDataURL(file);
						  return false;
						});
					  }
					};
				  })
				  
				  
				  .directive("customerfileread", [function () {
					  return {
						  scope: {
							  customerfileread: "=",
							  closemodal:"&"
						  },
						  link: function (scope, element, attrs) {/*jshint ignore:line*/
								var checkSize,validMimeTypes,
								isTypeValid;
								validMimeTypes = "[image/png, image/jpeg, image/svg]";
								checkSize = function(size,loadend) {
									var fileSize = 4;
									if ((size / 1024) / 1024 < fileSize) {
									  return true;
									} else {
										if(!loadend){
											alert("File size should be less than " + fileSize + " MB");/*jshint ignore:line*/
										}
									  return false;
									}
								  };
							
								  isTypeValid = function(type,loadend) {
									if ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1) {
									  return true;
									} else {
										if(!loadend){
									  		alert("Invalid file type.  File must be one of following types " + validMimeTypes);/*jshint ignore:line*/
										}
										return false;
									}
								  };
							  element.bind("change", function (changeEvent) {
								var file, name, reader, size, type;
								if (changeEvent != null) {
								  event.preventDefault();/*jshint ignore:line*/
								}
								  var reader = new FileReader();
								  reader.onload = function (loadEvent) {
									if (checkSize(size) && isTypeValid(type)) {
										//Initiate the JavaScript Image object.
										var image = new Image();/*jshint ignore:line*/
										//Set the Base64 string return from FileReader as source.
										image.src = loadEvent.target.result;
										image.onload = function () {
											//Determine the Height and Width.
											var height = this.height;
											var width = this.width;
											if (height > 70 || width > 320) {
												alert("Image resolution must be  70px X 320px.");/*jshint ignore:line*/
												return false;
											}
											scope.$apply(function () {
												scope.customerfileread = loadEvent.target.result;
											});
										};
									
									}
								  };
								  reader.onloadend = function(loadEvent){
									if (checkSize(size,'loadend') && isTypeValid(type,'loadend')) {
										//Initiate the JavaScript Image object.
										var image = new Image();/*jshint ignore:line*/
										//Set the Base64 string return from FileReader as source.
										image.src = loadEvent.target.result;
										image.onload = function () {
											//Determine the Height and Width.
											var height = this.height;
											var width = this.width;
											if (height > 70 || width > 320) {
												return false;
											}
											scope.$apply(function () {
												scope.closemodal();
											});
										};
										
									  }
								  };
								  file = changeEvent.currentTarget.files[0];
								  name = file.name;
								  type = file.type;
								  size = file.size;
								  reader.readAsDataURL(changeEvent.target.files[0]);
							  });
						  }
					  };
					}]);
					//to prevent the image file while Dragging
					
	window.addEventListener("dragover", function (e) {
		e = e || event;/*jshint ignore:line*/
		e.preventDefault();
	}, false);
	window.addEventListener("drop", function (e) {
		e = e || event;/*jshint ignore:line*/
		e.preventDefault();
	}, false);