'use strict';
angular.module('ehubApp')
	   .controller('DeploymentProcessModalController', deploymentProcessModalController);

		deploymentProcessModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance'
		];
		
		function deploymentProcessModalController(
				$scope, 
				$rootScope,
				$uibModalInstance) {
			/*
			 * @purpose: Defining scope variables
			 * @author: swathi
			 */
			$scope.deploymentProcessModal = {
				cancelModal: cancelModal
			};
			
			 /*
			 * @purpose: Close deployment process Modal
			 * @created: 17 oct 2017
			 * @params: null
			 * @returns: no
			 * @author: swathi
			 */
			function cancelModal(){
				$uibModalInstance.dismiss('close');
			}
			
}