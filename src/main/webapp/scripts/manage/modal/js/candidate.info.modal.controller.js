'use strict';
angular.module('ehubApp')
	   .controller('CandidateInfoModalController', candidateInfoModalController);

		candidateInfoModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance'
		];
		
		function candidateInfoModalController(
				$scope, 
				$rootScope,
				$uibModalInstance) {
			/*
			 * @purpose: Defining scope variables
			 * @author: swathi
			 */
			$scope.candidateInfoModal = {
				cancelModal: cancelModal
			};
			
			 /*
			 * @purpose: Close candidate info Modal
			 * @created: 17 oct 2017
			 * @params: null
			 * @returns: no
			 * @author: swathi
			 */
			function cancelModal(){
				$uibModalInstance.dismiss('close');
			}
			
}