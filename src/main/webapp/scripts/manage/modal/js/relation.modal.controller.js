'use strict';
angular.module('ehubApp')
	   .controller('RelationModalController', relationModalController);

		relationModalController.$inject = [
			'$scope',
			'$state',
			'AllRelationsData',
			'$uibModalInstance',
			'DataCurationApiService',
			'$stateParams',
			'HostPathService',
			'$rootScope'
		];

		function relationModalController(
				$scope,
				$state,
				AllRelationsData,
				$uibModalInstance,
				DataCurationApiService,
				$stateParams,HostPathService,
				$rootScope) {

			/*
			 * @purpose: Initializing scope variables
			 * @author: Ankit
			 */
			$scope.relationModal = {
					directorsData: [],
					officersData: [],
					dateOptionsfrom: {
					    formatYear: 'yyyy',
					    maxDate: new Date(),
					    minDate: new Date(1950, 5, 22),
					    startingDay: 1
					},
				    openFromDate:openFromDate,
				    openToDate:openToDate,
					closeModal: closeModal,
					saveModalDetails: saveModalDetails,
					newRoles: [
						{
							whichRole: 'director',
							hasPositionType: '',
//							hasReportedTitle: '',
							'@source-id': 'data-curation-ui',
							fromDateModal: '',
							toDateModal: '',
							popupfromDate: {
								opened: false
							},
							popuptoDate: {
								opened: false
							},
							dateOptionsto: {
								formatYear: 'yyyy',
							    maxDate: new Date(),
							    minDate: this.fromDateModal,/*jshint ignore:line*/
							    startingDay: 1
							},
						}],
					addNewRole: addNewRole,
					removeRole: removeRole,
					changeRole: changeRole
			};
			console.log(AllRelationsData,'AllRelationsData');

			/*
			 * @purpose: Initializing modal variables
			 * @author: Ankit
			 */
			$scope.relationData = {
					hasHolder: '',
					isPositionIn: '',
			};

			$scope.relationModal.directorsData = AllRelationsData.directorRoles;
			$scope.relationModal.officersData = AllRelationsData.officerRoles;
			$scope.relationData.isPositionIn = AllRelationsData.orgIdentifier;
			$scope.relationData.hasHolder =  AllRelationsData.personIdentifier;
			var PersonName = AllRelationsData.personName;
			/*
			 * @purpose: add New Role
			 * @created: 9 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			function addNewRole(){
				var newItemNo = $scope.relationModal.newRoles.length+1;
				startNewInterval($scope.relationModal.newRoles.length);
				$('#directorData' + newItemNo).chosen({allow_single_deselect: true});
			    $('#officerData' + newItemNo).chosen({allow_single_deselect: true});
				$scope.relationModal.newRoles.push(
						{
							whichRole: 'director',
							hasPositionType: '',
//							hasReportedTitle: '',
							'@source-id': 'data-curation-ui',
							fromDateModal: '',
							toDateModal: ''	,
							popupfromDate: {
								opened: false
							},
							popuptoDate: {
								opened: false
							},
						});

			}

			/*
			 * @purpose: remove role
			 * @created: 9 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			function removeRole($index){
				$scope.relationModal.newRoles.splice($index, 1);
			}
			/*
			 * @purpose: Initialize chosen plugin
			 * @created: 8 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			var startInterval;
			function setChosenPlugin(index){
	        	if($("#directorData" + index).length > 0 || $('#officerData'+ index).length > 0){
	        		clearInterval(startInterval);
	        		$('#directorData'+ index).chosen({allow_single_deselect: true});
				    $('#officerData' + index).chosen({allow_single_deselect: true});
	        	}else{
	        		$('#directorData'+ index).css({'display':'none'});
				    $('#officerData' + index).css({'display':'none'});
	        	}
	        }

			function startNewInterval(index){
				 startInterval = setInterval(function(){
			        	setChosenPlugin(index);
			        },100);
			}
			startNewInterval(0);

			/*
			 * @purpose: Change Role
			 * @created: 8 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			function changeRole(role, index){
				var newItemNo = $scope.relationModal.newRoles.length+1;/*jshint ignore:line*/
				startNewInterval(index);
			}
			/*
			 * @purpose: close modal
			 * @created: 8 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			var role;
			function saveModalDetails(){
				console.log($scope.relationModal.newRoles);
				angular.forEach($scope.relationModal.newRoles, function(relationVal, relationKey){/*jshint ignore:line*/
					console.log(relationVal,'relationVal');
					if(relationVal.whichRole == 'director')
						{role = 'directorship';}
					else if(relationVal.whichRole == 'officer')
						{role = 'officership';}
					var rolesData = {};
					rolesData['@source-id'] = 'data-curation-ui';
					rolesData.to = relationVal.toDateModal || null;
					rolesData.from = relationVal.fromDateModal;
//					rolesData['hasReportedTitle'] = relationVal['hasReportedTitle'];
					rolesData.hasPositionType = relationVal.hasPositionType;
					rolesData.hasHolder = AllRelationsData.personIdentifier;
					rolesData.isPositionIn = $stateParams.identifier.split("#")[0];
					rolesData['@is-manual'] = "true";

					DataCurationApiService.saveRelationdata(role, rolesData).then(function(relationResponse){
						console.log(relationResponse);
						var response = angular.extend(relationResponse,{"name":PersonName});
						$rootScope.$broadcast('eventFired', {
			                data: response
			            });
						HostPathService.FlashSuccessMessage('Added Role Successfully','');
						closeModal();
					}, function(relationError){
						console.log(relationError);
						HostPathService.FlashErrorMessage('Sorry, Failed To Add SourceId '+rolesData['@source-id'],'');
//						closeModal();
						console.log(rolesData['@source-id']);
					});
				});

			}

			 /*
			 * @purpose: close modal
			 * @created: 8 feb 2017
			 * @params: null
			 * @returns: no
			 * @author: Ankit
			 */
			function closeModal(){
				$uibModalInstance.dismiss('close');
			}

			function openFromDate(index){
				  $scope.relationModal.newRoles[index].popupfromDate.opened = true;
			}

			function openToDate(index, fromDateModal){/*jshint ignore:line*/
				$scope.relationModal.newRoles[index].popuptoDate.opened = true;
				$scope.relationModal.newRoles[index].dateOptionsto.minDate = $scope.relationModal.newRoles[index].fromDateModal;
			}

}