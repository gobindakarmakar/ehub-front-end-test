'use strict';
angular.module('ehubApp')
	   .controller('myNewTasksModalCtrl', myNewTasksModalCtrl);

myNewTasksModalCtrl.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModalInstance'
		];
		
		function myNewTasksModalCtrl(
				$scope, 
				$rootScope,
				$uibModalInstance) {
			
			/*
			 * @purpose: Defining scope variables		 
			 */
			
			$scope.newTasksModal = {				
				cancel: cancel
			};
			
			 /*
			 * @purpose: Close candidate info Modal										
			 */
			
			function cancel(){
				
//				alert("Inside cancel button....");
				$uibModalInstance.dismiss('close');
			}
			
						
}