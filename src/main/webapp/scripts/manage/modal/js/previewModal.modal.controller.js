'use strict';
angular.module('ehubApp')
	.controller('previewModalController', previewModalController);
previewModalController.$inject = [
	'$scope',
	'$state',
	'AllRelationsData',
	'$uibModalInstance',
	'DataCurationApiService',
	'$stateParams',
	'HostPathService',
	'$rootScope',
	'EHUB_API',
	'getOrganationNamesSuggestions',
	'getPersonNamesSuggestions',
	'organisationIdentifier',
	'personIdentifier'
];
function previewModalController(
	$scope,
	$state,
	AllRelationsData,
	$uibModalInstance,
	DataCurationApiService,
	$stateParams, HostPathService,
	$rootScope,
	EHUB_API,
	getOrganationNamesSuggestions,
	getPersonNamesSuggestions,
	organisationIdentifier,
	personIdentifier
	) {

	$scope.previewModal = {
		inputPreviewNameOrg: "",
		selectEntity: "organisation",
		inputPreviewNamePerson: ""
	};

	$scope.previewModalfunctions = {
		closeModal: closeModal,
		searchEntity: searchEntity,
		getCompanySuggestions: getCompanySuggestions,
		getPersonSuggestions: getPersonSuggestions,
		companySearch: false,
		personSearch: false,
		editCurrentEntity: editCurrentEntity,
		checkEntityStatus: checkEntityStatus
	};

	$scope.previewOP = {
		organizationModal: {
			//				"description": "",
			"bst:facebook": null,
			"mdaas:HeadquartersAddress": null,
			"isDomiciledIn": null,
			"bst:youtube": null,
			"hasLatestOrganizationFoundedDate": null,
			"tr-org:hasRegisteredPhoneNumber": null,
			"@is-manual": "true",
			"bst:linkedin": null,
			"mdaas:RegisteredAddress": null,
			"hasPrimaryEconomicSector": null,
			//	  			"disambiguatingDescription": "",
			"hasHoldingClassification": null,
			"vcard:organization-name": null,
			"bst:email": null,
			"hasPrimaryBusinessSector": null,
			"hasRegisteredFaxNumber": null,
			"hasURL": null,
			//	  			"hasIPODate": "",
			"@source-id": "data-curation-ui",
			"isIncorporatedIn": null,
			//	  			"tr-org:hasInactiveDate": "",
			//	  			"hasPrimaryInstrument": "",
			"tr-org:hasHeadquartersFaxNumber": null,
			"hasPrimaryIndustryGroup": null,
			//	  			"hasActivityStatus": "false",
			"bst:VAT": null,
			//	  			"tr-org:hasLEI": "",
			"bst:instagram": null,
			"bst:gplus": null,
			"tr-org:hasHeadquartersPhoneNumber": null,
			"bst:twitter": null,
			//				"hasOrganizationPrimaryQuote": "",
			"bst:registrationId": null,
			//				"personalDetails" : []
		},
		personModal: {
			"bst:facebook": null,
			"vcard:hasAddress": [],
			"vcard:hasGender": "male",
			"vcard:hasName": null,
			"bst:deathDate": null,
			"bst:youtube": null,
			"vcard:honorific-prefix": "Mr.",
			"@is-manual": "true",
			"bst:linkedin": null,
			"vcard:hasPhoto": null,
			"vcard:bday": null,
			"vcard:hasTelephone": null,
			"vcard:family-name": null,
			"vcard:additional-name": null,
			"bst:email": null,
			"@source-id": "data-curation-ui",
			"vcard:hasURL": null,
			"vcard:honorific-suffix": null,
			"tr-vcard:preferred-name": null,
			"bst:aka": null,
			"bst:description": null,
			"vcard:given-name": null,
			"bst:nationality": null,
			"bst:instagram": null,
			"bst:gplus": null,
			"bst:twitter": null
		}
	};

	if (AllRelationsData && Object.keys(AllRelationsData).length >0) {
		console.log("woah!!!", AllRelationsData);
		$scope.previewModal = {
			inputPreviewNameOrg: AllRelationsData,
			selectEntity: "organisation",
		};
		console.log($scope.previewModal, "slkvns");
		searchEntity();
		setTimeout(function () {
			$('#PreviewName').val(AllRelationsData.name);
		}, 10);
	}

	/*
	 * @purpose: close modal
	 * @created: 16 Mar 2018
	 * @params: null
	 * @returns: no
	 * @author: Virendra
	 */
	function closeModal() {
		$uibModalInstance.dismiss('close');
	}


	function searchEntity() {
		if ($scope.previewModal.selectEntity == "organisation") {
			if (typeof ($scope.previewModal.inputPreviewNameOrg) != "object") {
				return false;
			}
			$scope.previewModalfunctions.companySearch = true;
			$scope.previewModalfunctions.personSearch = false;
			var data = EHUB_API + "entity/org/" + $scope.previewModal.inputPreviewNameOrg["@identifier"];/*jshint ignore:line*///$scope.previewModal.inputPreviewNamePerson["@identifier"];
			// DataCurationApiService.getIdentifierInfo(data).then(function (resp) {
				angular.extend($scope.previewOP.organizationModal, organisationIdentifier);
				$scope.previewOP.organizationModal.hasLatestOrganizationFoundedDate = new Date(organisationIdentifier.hasLatestOrganizationFoundedDate);
				$scope.previewOP.organizationModal.hasIPODate = new Date(organisationIdentifier.hasIPODate);
				$scope.previewOP.organizationModal['tr-org:hasInactiveDate'] = new Date(organisationIdentifier["tr-org:hasInactiveDate"]);
			// }, function (nationalityError) {
			// 	$scope.isLoading = false;
			// });
		} else if ($scope.previewModal.selectEntity == "person") {
			if (typeof ($scope.previewModal.inputPreviewNamePerson) != "object") {
				return false;
			}
			$scope.previewModalfunctions.companySearch = false;
			$scope.previewModalfunctions.personSearch = true;
			//			var identitfier = $scope.previewModal.inputPreviewNamePerson["@identifier"];
			var data = EHUB_API + "entity/person/" + $scope.previewModal.inputPreviewNamePerson["@identifier"];
			// DataCurationApiService.getIdentifierInfo(data).then(function (resp) {
				$scope.previewOP.personModal = {};
				angular.extend($scope.previewOP.personModal, personIdentifier);
				$scope.previewOP.personModal['bst:deathDate'] = personIdentifier["bst:deathDate"] ? new Date(personIdentifier["bst:deathDate"]) : "";
				$scope.previewOP.personModal['vcard:bday'] = personIdentifier["vcard:bday"] ? new Date(personIdentifier["vcard:bday"]) : "";
				if (personIdentifier["bst:aka"])
					{$scope.previewOP.personModal["bst:aka"] = typeof (personIdentifier["bst:aka"] != "string") ? personIdentifier["bst:aka"].join(",") :personIdentifier["bst:aka"];}
			// }, function (nationalityError) {
			// 	$scope.isLoading = false;
			// });
		}
	}

	/*
		* @purpose: get company suggestions 
		* @created: 16 Mar 2018 
		* @params: null
		* @return: no @author: Virendra
		*/
	function getCompanySuggestions() {
		// return DataCurationApiService.getOrganationNamesSuggestions(val, 10).then(function (orgResponse) {
			return getOrganationNamesSuggestions.hits;
		// }, function (orgError) {

		// });
	}

	/*
	  * @purpose: get person suggestions
	  * @created: 16 Mar 2018
	  * @params: null
	  * @return: no
	  * @author: Virendra
	*/
	function getPersonSuggestions() {
		// return DataCurationApiService.getPersonNamesSuggestions(val).then(function (personResponse) {
			return getPersonNamesSuggestions.hits;
		
	}

	/*
	 * @purpose: To rediect to either Person/Organisation Details
	 * @created: 19 Mar 2018
	 * @params: null
	 * @return: no
	 * @author: Virendra
	*/
	function editCurrentEntity(val) {
		$scope.previewModalfunctions.closeModal();
		switch (val) {
			case "org":
				$state.go("addDataCuration", { 'identifier': $scope.previewOP.organizationModal['@identifier'] + "#02" }, { notify: true });
				break;

			case "person":
				$state.go("addDataCuration", { 'identifier': $scope.previewOP.personModal['@identifier'] + "#01" }, { notify: true });
				break;
		}
	}

	function checkEntityStatus() {
		if ($scope.previewModal.selectEntity == "organisation") {
			return $scope.previewModalfunctions.companySearch;
		} else if ($scope.previewModal.selectEntity == "person") {
			return $scope.previewModalfunctions.personSearch;
		}
	}


}

