(function(){
	var locale = "ua";
	var data = {
		"Node" : "Вузел",
		"Edge" : "Зв'язок",
		"Search" : "Пошук",
		"Restore all hidden" : "Відновити усі скриті",
		"Filter":"Фільтр",
		"Show all labels" : "Імена",
		"Show time panel" : "Стрічка часу",
		"Show map" : "Мапа",
		"Icon" : "Знак",
		"Font" : "Шрифт",
		"Font-size" : "Розмір шрифту",
		"Color" : "Колір",
		"Border" : "Кайма",
		"Type" : "Тип",
		"Width" : "Ширина",
		"Re-Arrange" : "Перебудувати",
		"Snapshot as": "Знімок",
		"All" : "Все",
		"Selected" : "Вибране",
		"Import JSON" : "Імпорт JSON",
		"Load Case data" : "Завантажити з АПИ",
		"Custom styled" : "Custom styled",
		"Delete note" : "Видалити",
		"Send" : "Відправити"
	};
	VLA._registerLocale(locale, data);
})();
