var custom_config = {
	filter_fields : [ "degree", "name", "id", "labelV", "__jobId",
			"__jobItemId" ],
	view_fields : [ "name", "id", "labelV" ],

	default_type : "default",
	default_connection_type : "default",

	connection_types : [ {
		name : "default",
		dir : "normal",
		color : "#838383",
		targetArrow : "triangle",
		sourceArrow : "none"
	}, {
		name : "Family",
		dir : "mutual",
		color : "#00ff00",
		targetArrow : "triangle",
		sourceArrow : "none"
	}, {
		name : "Colleague",
		dir : "none",
		color : "#0078ff",
		targetArrow : "triangle",
		sourceArrow : "none"
	}, {
		name : "Professional",
		dir : "normal",
		color : "#00ffff",
		targetArrow : "triangle",
		sourceArrow : "none"
	}, {
		name : "Factual",
		dir : "normal",
		color : "#000000",
		targetArrow : "triangle",
		sourceArrow : "none"
	}, {
		name : "Concluded",
		dir : "normal",
		color : "#ff9d00",
		targetArrow : "triangle",
		sourceArrow : "none"
	} ],

	types : [ {
		name : "person",
		rule : "labelV='person'",
		icon : "user",
		fillColor : "#fd607b",
		borderColor : "orange"
	}, {
		name : "website",
		rule : "labelV='website'",
		icon : "internet-explorer",
		fillColor : "blue"
	}, {
		name : "company",
		rule : "labelV='company'",
		icon : "company",
		fillColor : "#ffa71d"
	}, {
		name : "company_number",
		rule : "labelV='company_number'",
		icon : "user",
		fillColor : "#00d87b"
	}, {
		name : "officer_id",
		rule : "labelV='officer_id'",
		icon : "tag",
		fillColor : "#00cee1"
	}, {
		name : "location",
		rule : "labelV='location'",
		icon : "map-marker",
		fillColor : "#00AAFF"
	}, {
		name : "Skill",
		rule : "labelV='skill'",
		icon : "skill",
		fillColor : "#00AAFF"
	}, {
		name : "Article",
		rule : "labelV='article'",
		icon : "article",
		fillColor : "#00AAFF"
	}, {
		name : "Root_Company",
		rule : "labelV='root_Company'",
		icon : "root-company",
		fillColor : "#00AAFF"
	}, {
		name : "Root",
		rule : "labelV='root'",
		icon : "root",
		fillColor : "#00AAFF"
	}, {
		name : "Sentence",
		rule : "labelV='sentence'",
		icon : "sentence",
		fillColor : "#00AAFF"
	}, {
		name : "Transaction",
		rule : "labelV='transaction'",
		icon : "transaction",
		fillColor : "#00AAFF"
	}, {
		name : "Organization",
		rule : "labelV='organization'",
		icon : "organization",
		fillColor : "#00AAFF"
	}, {
		name : "Industry",
		rule : "labelV='industry'",
		icon : "industry",
		fillColor : "#00AAFF"
	},

	{
		name : "alert",
		rule : "labelV='alert'",
		icon : "alert",
		fillColor : "#00AAFF"
	}, {
		name : "customer",
		rule : "labelV='customer'",
		icon : "user",
		fillColor : "#00AAFF"
	}, {
		name : "account",
		rule : "labelV='account'",
		icon : "account",
		fillColor : "#00AAFF"
	}, {
		name : "tx",
		rule : "labelV='tx'",
		icon : "transaction",
		fillColor : "#00AAFF"
	}, {
		name : "Accounts",
		rule : "labelV='accounts'",
		icon : "account",
		fillColor : "#00AAFF"
	}, {
		name : "Follows",
		rule : "labelV='follows'",
		icon : "follows",
		fillColor : "#00AAFF"
	},  {
		name : "Address",
		rule : "labelV='address'",
		icon : "address",
		fillColor : "#00AAFF"
	}, {
		name : "Investment",
		rule : "labelV='investment'",
		icon : "investment",
		fillColor : "#00AAFF"
	}, {
		name : "Returns",
		rule : "labelV='returns'",
		icon : "returns",
		fillColor : "#00AAFF"
	}, {
		name : "Stock",
		rule : "labelV='stock'",
		icon : "stock",
		fillColor : "#00AAFF"
	}, {
		name : "Stock_Change",
		rule : "labelV='stock_change'",
		icon : "stock_change",
		fillColor : "#00AAFF"
	}, {
		name : "Text",
		rule : "labelV='text'",
		icon : "text",
		fillColor : "#00AAFF"
	}, {
		name : "Product",
		rule : "labelV='product'",
		icon : "product",
		fillColor : "#00AAFF"
	}, {
		name : "Vulnerability",
		rule : "labelV='vulnerability'",
		icon : "vulnerability",
		fillColor : "#00AAFF"
	}, {
		name : "Date",
		rule : "labelV='date'",
		icon : "date",
		fillColor : "#00AAFF"
	}, {
		name : "Person",
		rule : "labelV='person'",
		icon : "user",
		fillColor : "#00AAFF"
	}, {
		name : "Tweet",
		rule : "labelV='tweet'",
		icon : "tweet",
		fillColor : "#00AAFF"
	}, {
		name : "UserMention",
		rule : "labelV='usermention'",
		icon : "usermention",
		fillColor : "#00AAFF"
	}, {
		name : "Follower_Following_Count",
		rule : "labelV='follower_following_count'",
		icon : "follower_following_count",
		fillColor : "#00AAFF"
	}, {
		name : "Product",
		rule : "labelV='product'",
		icon : "product",
		fillColor : "#00AAFF"
	}, {
		name : "TRADE_NAME",
		rule : "labelV='trade_name'",
		icon : "trade",
		fillColor : "#00AAFF"
	}, {
		name : "Industry",
		rule : "labelV='industry'",
		icon : "industry",
		fillColor : "#00AAFF"
	}, {
		name : "FOUNDER",
		rule : "labelV='founder'",
		icon : "founder",
		fillColor : "#00AAFF"
	}, {
		name : "Follower",
		rule : "labelV='follower'",
		icon : "follower",
		fillColor : "#00AAFF"
	}, {
		name : "Open_Port",
		rule : "labelV='open_port'",
		icon : "open_port",
		fillColor : "#00AAFF"
	}, {
		name : "GPS",
		rule : "labelV='gps'",
		icon : "gps",
		fillColor : "#00AAFF"
	}, {
		name : "Service",
		rule : "labelV='service'",
		icon : "service",
		fillColor : "#00AAFF"
	}, {
		name : "Metadata",
		rule : "labelV='metadata'",
		icon : "metadata",
		fillColor : "#00AAFF"
	}, {
		name : "Cyber_info",
		rule : "labelV='cyber_info'",
		icon : "cyber_info",
		fillColor : "#00AAFF"
	}, {
		name : "Share_change",
		rule : "labelV='share_change'",
		icon : "share_change",
		fillColor : "#00AAFF"
	}, {
		name : "Photo",
		rule : "labelV='photo'",
		icon : "photo",
		fillColor : "#00AAFF"
	},
	{
		name : "Assets_chart",
		rule : "labelV='assets_chart'",
		icon : "assets_chart",
		fillColor : "#00AAFF"
	},
	{
		name : "Video",
		rule : "labelV='video'",
		icon : "video",
		fillColor : "#00AAFF"
	},
	{
		name : "Profile_Chart",
		rule : "labelV='profile_chart'",
		icon : "profile_chart",
		fillColor : "#00AAFF"
	},
	{
		name : "Cover",
		rule : "labelV='cover'",
		icon : "cover",
		fillColor : "#00AAFF"
	},
	{
		name : "Revanue_Chart",
		rule : "labelV='revanue_chart'",
		icon : "revanue_chart",
		fillColor : "#00AAFF"
	},
	{
		name : "Comment",
		rule : "labelV='comment'",
		icon : "comment",
		fillColor : "#00AAFF"
	},
	{
		name : "Post",
		rule : "labelV='post'",
		icon : "post",
		fillColor : "#00AAFF"
	},
	{
		name : "Stock",
		rule : "labelV='stock'",
		icon : "stock",
		fillColor : "#00AAFF"
	},
	{
		name : "Filling",
		rule : "labelV='filling'",
		icon : "filling",
		fillColor : "#00AAFF"
	},
	{
		name : "Compensation",
		rule : "labelV='compensation'",
		icon : "compensation",
		fillColor : "#00AAFF"
	},
	{
		name : "board_member",
		rule : "labelV='board_member'",
		icon : "board_member",
		fillColor : "#00AAFF"
	},
	{
		name : "membership",
		rule : "labelV='membership'",
		icon : "membership",
		fillColor : "#00AAFF"
	},
	{
		name : "Profit_Chart",
		rule : "labelV='profit_chart'",
		icon : "profit_chart",
		fillColor : "#00AAFF"
	},
	{
		name : "MerchantType",
		rule : "labelV='merchanttype'",
		icon : "merchanttype",
		fillColor : "#00AAFF"
	},
	{
		name : "Merchant",
		rule : "labelV='merchant'",
		icon : "merchant",
		fillColor : "#00AAFF"
	},
	{
		name : "Card",
		rule : "labelV='card'",
		icon : "credit-card",
		fillColor : "#00AAFF"
	},{
		name : "Technology",
		rule : "labelV='technology'",
		icon : "technology",
		fillColor : "#00AAFF"
	},{
		name : "Event",
		rule : "labelV='event'",
		icon : "comment",
		fillColor : "#00AAFF"
	}, {
		name : "Club",
		rule : "labelV='club'",
		icon : "organization",
		fillColor : "#00AAFF"
	}
	

	]
};