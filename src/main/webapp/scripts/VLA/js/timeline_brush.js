//------------------------------------------------------------------------------
/**
 * Function to create brush
 */
function timelineBrush(container_id,xdomain){
    var height = 100;
    var Svgwidth =$(window).width()-350;//400-> width of side menu and spaces
  
    var margin={ top: 20, bottom: 50,left:20,right:20}
      var width = Svgwidth - margin.left-margin.right;
    var svg = d3.select(container_id).append("svg").attr("width",Svgwidth).attr("height",height+margin.top+margin.bottom);
     g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
     
    
 var x = d3.scaleTime()
    .domain(xdomain)
    .rangeRound([0, width]);
   var  y = d3.randomNormal(height / 2, height / 8);

var brush = d3.brushX()
    .extent([[0, 0], [width, height]])
    .on("start brush end", brushmoved);

g.append("g")
    .attr("class", "axis axis--x-brush")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));


var gBrush = g.append("g")
    .attr("class", "brush")
    .call(brush);



gBrush.call(brush.move, [0.3, 0.5].map(x));

function brushmoved() {
  var s = d3.event.selection;
  if (s != null) {
  var sx = s.map(x.invert);
//  console.log(sx)
  VLA.UI.HTML_UI.trigger("time-range-change", "here")
//     handle.attr("display", "none");
//     circle.classed("active", false);
//   } else {
//     var sx = s.map(x.invert);
//     circle.classed("active", function(d) { return sx[0] <= d && d <= sx[1]; });
//     handle.attr("display", null).attr("transform", function(d, i) { return "translate(" + s[i] + "," + height / 2 + ")"; });
   }
}
}
