$(document).ready(function () {
   plotWorldMap("worldmap_div");
});

function plotWorldMap(id) {
// initiallizing the map by setting  geographical coordinates and a zoom level
        var mymap = L.map(id, {
            center: [52.03222, 16.52344],          
            zoom: 0,
            zoomControl:false ,
            attributionControl: false
        });
// ading Mapbox Streets tile layer setView
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, \n\
                 <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, \n\
                 Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 5,
            minZoom: 0,
            id: 'mapbox.streets-basic',         
            accessToken: "pk.eyJ1IjoiZ3NhbmdyYW0iLCJhIjoiY2o0cXh1Ymc5MmJldjMybnU3ZmE2N291eCJ9.OD8iQgZWre9BHy2f5Tl_nw"
        
    }).addTo(mymap) 

function getColor(d) {
return d=='CHN' ? '#1c9099':
    d=='IND' ? '#1c9099':
    d=='USA' ? '#1c9099':
    d=='IDN' ? '#1c9099':
    d=='BRA' ? '#1c9099':
    d=='PAK' ? '#1c9099':
    d=='BGD' ? '#1c9099': 
    d=='NGA' ? '#1c9099':
    d=='RUS' ? '#1c9099':
    d=='JPN' ? '#1c9099':
    d=='MEX' ? '#1c9099':
    d=='PHL' ? '#1c9099':
    d=='VNM' ? '#1c9099':
    d=='ETH' ? '#1c9099':
    d=='DEU' ? '#1c9099':
    d=='EGY' ? '#1c9099':
    d=='TUR' ? '#1c9099':
    d=='COD' ? '#1c9099':
    d=='IRN' ? '#1c9099':
    d=='THA' ? '#1c9099':
               'none';                   
}
//'#435dbe';'#3778bf'
// adding the required color and density
function style(feature) {
    return {
        fillColor: getColor(feature.id), 
        weight: 1.5,
        opacity: 0.5,
        color: 'black',
        dashArray: '3',
        fillOpacity: 0.5
    };
}

// assigning the geojson data to map for custom selection 
L.geoJson(hello, {style: style}).addTo(mymap);

}
