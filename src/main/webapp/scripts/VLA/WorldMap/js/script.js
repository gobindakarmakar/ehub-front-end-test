var WorldData = [];
$(document).ready(function () {
//    load data
    loadWorldChart();

});
//responsivenss
$(window).on("resize", function () {
    $("#worldmap_div").empty();
    new WorldChart({
        container: "#worldmap_div",
         data: [WorldData, populationData],
        height: '600'
    });
})
var WorldData=[],populationData=[];
function loadWorldChart() {
    var uri1 = "static/VLA/WorldMap/data/worldCountries.json";
    var uri2 = "static/VLA/WorldMap/data/data.json";
    var flag_for_req = 0;
    d3.json(uri1, function (error, data) {
        flag_for_req++;
         WorldData = data;
        if (flag_for_req == 2) {           
            var exampleChart = new WorldChart({
                container: "#worldmap_div",
                data: [WorldData, populationData],
                height: '600'
            });
        }
    });
    d3.json(uri2, function (error, data) {
        flag_for_req++;
        populationData = data;
        if (flag_for_req == 2) {
            var exampleChart = new WorldChart({
                container: "#worldmap_div",
                data: [WorldData, populationData],
                height: '600'
            });
        }
    });
}

