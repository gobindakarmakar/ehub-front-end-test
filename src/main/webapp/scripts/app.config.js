elementApp
	   .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider', '$provide','$httpProvider', 'FlashProvider','FLASH_ALERT_DURATION','FLASH_ALERT_CLOSE',function($stateProvider, $urlRouterProvider, $locationProvider, $qProvider, $provide, $httpProvider, FlashProvider,FLASH_ALERT_DURATION,FLASH_ALERT_CLOSE) {
		 
		window.appName = '';   
		$provide.factory('$stateProvider', function () {
			 return $stateProvider;
		 });
	 	$provide.factory('$urlRouterProvider', function () {
	        return $urlRouterProvider;
	    });
		$locationProvider.html5Mode({  
			enabled : false,
			requireBase : false  
		});
		$locationProvider.hashPrefix('');
		
//		FlashProvider.setTimeout(FLASH_ALERT_DURATION);
		FlashProvider.setShowClose(FLASH_ALERT_CLOSE);

}]);                                                                                                                    