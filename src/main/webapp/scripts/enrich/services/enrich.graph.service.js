'use strict';
elementApp
.service('EnrichSearchGraph', enrichSearchGraph);

enrichSearchGraph.$inject = [
	'$q',
	'$state'
];

function enrichSearchGraph(
		$q,
		$state){
	
	this.getDFDNetworkChart = function(collapseOptions) {
	    if (collapseOptions.container) {
	        $(collapseOptions.container).empty();
	    }
	    if (collapseOptions) {
	        this.container = collapseOptions.container ? collapseOptions.container : "body"
	        this.data = (collapseOptions.data) ? collapseOptions.data : []
	        this.height = collapseOptions.height ? collapseOptions.height : 600;
	        this.uri = collapseOptions.uri;
	        this.collapseHeader = collapseOptions.header ? collapseOptions.header : "FORCE COLLAPSE CHART";

	        this.randomIdString = Math.floor(Math.random() * 10000000000);
	        this.headerOptions = collapseOptions.headerOptions== false?collapseOptions.headerOptions:true;


	    } else {
	        return false;
	    }
	    var header = collapseOptions.header;
	    var actualOptions = jQuery.extend(true, {}, collapseOptions);
	    var randomSubstring = this.randomIdString;
	    var hl = this.height
	    var h = parseInt(hl) + 50;
	    var _this = this;
	    var modalwidth=$(window).width()-200;
	     var modal = ' <div id="modal_' + randomSubstring + '"class="modal fade " tabindex="-1" role="dialog"> <div class="modal-dialog modal-lg" style="width:'+modalwidth+'px"> <form ><div class="modal-content"><div class="modal-body"  style="padding:0;background-color:#334d59" id="modal_chart_container' + randomSubstring + '"></div></div></form></div></div>';
	   var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 100%; margin: auto; margin-top: 0px; font-size: 14px;font-style: inherit;"> <div class="graphBox" style="height:' + h + 'px;max-height: ' + h + 'px;min-height: ' + h + 'px;margin: auto; background-color: #374c59; width: 100%;left: 0px;top: 0px;position: relative;"> <div class="headerDiv" style="font-weight:bold;background-color: #425661;text-align: left;color: #239185; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + header + '</div><div id="collapse_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'
	    $(this.container).html(chartContainerdiv);
	    $(this.container).append(modal);
	    var chart_container = "#collapse_chart_div" + randomSubstring;
	    this.width = collapseOptions.width ? collapseOptions.width : $(chart_container).width() - 10;
	    if (this.height > this.width) {
	        this.height = this.width
	    }

	    this.diameter = this.height > this.width ? this.width - 50 : this.height - 50;
	    
	   if (!this.headerOptions) {
	     var closebtn =  '<button type="button" class="cancel" style="float: right;padding:0 8px;border:0;opacity: 1;background-color: transparent;float:right" data-dismiss="modal" aria-label="Close"> <span class="fa fa-remove"></span></button>'
	   $(chart_container).siblings(".headerDiv").append(closebtn);
	    }
	    if ($(chart_container).siblings(".headerDiv").find(".bstheme_menu_button").length == 0)
	        var header_options = '<div style="float: right; padding: 0 10px; cursor: pointer;" class="bstheme_menu_button bstheme_menu_button_' + randomSubstring + '" data-toggle="collapse" data-target="#opt_' + randomSubstring + '"><i class="fa fa-ellipsis-v" aria-hidden="true"></i><div class="bstheme_options" style="position:absolute;right:10px;z-index:2000"> <div style="display:none;" id="opt_' + randomSubstring + '" class="collapse"><span class="header_fullscreen_chart' + randomSubstring + '"><i class="fa fa-expand" aria-hidden="true"></i></span><span class="header_refresh_' + randomSubstring + '"><i class="fa fa-refresh" aria-hidden="true"></i></span> <span class="header_table_' + randomSubstring + '"> <i class="fa fa-table" aria-hidden="true"></i></span> <span class="header_chart_' + randomSubstring + '" style="display:none" ><i class="fa fa-bar-chart" aria-hidden="true"></i></span></div></div> </div>';
	    $(chart_container).siblings(".headerDiv").append(header_options);
	   
	      if (!this.headerOptions) {
	          $(chart_container).siblings(".headerDiv").find(".bstheme_options").css("right","28px");
	           $('.header_fullscreen_chart' + randomSubstring).css("display","none");
	      }else{
	          $(chart_container).siblings(".headerDiv").find(".bstheme_options").css("right","0");
	      }

	    $(".force_tooltip").remove();
	    var tool_tip = $('body').append('<div class="force_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;background-color:#0cae96; padding: 10px;border-radius: 5px;border: 1px solid gray;font-size: 10px;color:#000;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

	    var data = this.data;
	    var curretn_uri = this.Uri;
	    var collapse_header = this.collapseHeader;
	    var containerid = this.container;

	    var colorScale = d3.scaleOrdinal(d3.schemeCategory10);
	    var colorScale = d3.scaleOrdinal().range(["#46a2de", "#5888C8", "#31d99c", "#de5942", "#ffa618"]);
	    var width = this.width;
        var	height = this.height;
        var diameter = this.diameter;
        var node, root;
        var radius = Math.min(width, height) / 2;

	    var svg = d3.select(chart_container).append("svg:svg")
	            .attr("width", this.width)
	            .attr("height", this.height)
	            .append("svg:g")
	            .attr("transform", "translate(" + width / 6 + "," + height / 6 + ")");

	    var simulation = d3.forceSimulation()
	            .force("link", d3.forceLink().id(function (d) {
	                return d.id;
	            }))
	            .force("charge", d3.forceManyBody())
	            .force("center", d3.forceCenter(width / 3, height / 3));

	    var link = svg.append("g")
	            .attr("class", "links")
	            .selectAll("line")
	            .data(data.links)
	            .enter().append("line")
	            .attr("stroke-width", function (d) {
	                return Math.sqrt(d.value);
	            })
	            .style("stroke", "steelblue")
	            .style("stroke-opacity", "0.6");

	    var node = svg.append("g")
	            .attr("class", "nodes")
	            .selectAll("circle")
	            .data(data.nodes)
	            .enter().append("circle")
	            .attr("r", 5)
	            .attr("fill", function (d) {
	                return colorScale(d.group);
	            }).style("stroke", "#fff")
	            .style("stroke-width", "1.5px")
	            .call(d3.drag()
	                    .on("start", dragstarted)
	                    .on("drag", dragged)
	                    .on("end", dragended)).on("mouseover", function (d) {


	        $(".force_tooltip").html('<span> ' + d.id + '</span>');

	        $(".force_tooltip").css("visibility", "visible");
	        return   $(".force_tooltip").css("display","block");
      
	    })
	            .on("mousemove", function () {
	                $(".force_tooltip").css("top", (d3.event.pageY - 10) + "px")
	                return  $(".force_tooltip").css("left", (d3.event.pageX + 10) + "px");

	            })
	            .on("mouseout", function () {

	                return $(".force_tooltip").css("display", "none");
	            });


	    simulation
	            .nodes(data.nodes)
	            .on("tick", ticked);

	    simulation.force("link")
	            .links(data.links);

	    function ticked() {
	        link
	                .attr("x1", function (d) {
	                    return d.source.x;
	                })
	                .attr("y1", function (d) {
	                    return d.source.y;
	                })
	                .attr("x2", function (d) {
	                    return d.target.x;
	                })
	                .attr("y2", function (d) {
	                    return d.target.y;
	                });

	        node
	                .attr("cx", function (d) {
	                    return d.x;
	                })
	                .attr("cy", function (d) {
	                    return d.y;
	                });
	    }


	    function dragstarted(d) {
	        if (!d3.event.active)
	            simulation.alphaTarget(0.3).restart();
	        d.fx = d.x;
	        d.fy = d.y;
	    }

	    function dragged(d) {
	        d.fx = d3.event.x;
	        d.fy = d3.event.y;
	    }

	    function dragended(d) {
	        if (!d3.event.active)
	            simulation.alphaTarget(0);
	        d.fx = null;
	        d.fy = null;
	    }

	/*------------------------------------------------------------------------------*/
	    /**
	     * Fuction to handle show hide of header options
	     */
	    $("body").on("click", ".bstheme_menu_button_" + randomSubstring, function () {
	        
	        var id = ($(this).attr("data-target"));
	        if ($(id).css("display") == "none") {
	            $(id).css("display", "inline-block");
	        } else {
	            $(id).css("display", "none");
	        }
	    });
/*	------------------------------------------------------------------------------*/	    
	    /**
	     * Fuction to handle show hide of header options
	     */
	    $("body").on("click", ".header_refresh_" + randomSubstring, function () {
	        var chartId = $(this).parent().parent().parent().parent().siblings("div").attr("id");
	        if ("#" + chartId == chart_container) {
	            $(containerid).empty();
	           
	            chartcollapse(actualOptions);
	        }
	    });
	/*//------------------------------------------------------------------------------
*/
	    /**
	     * Fuction to handle show hide of header options
	     */
	    $("body").on("click", ".header_table_" + randomSubstring, function () {

	        var a = parseInt(hl / 55);

	        $(chart_container).empty();
	        $(this).css("display", "none");
	        $(".header_chart_" + randomSubstring).css("display", "inline-block");

	        var tbl = "<div id ='collapseChart_table_" + randomSubstring + "'   style='overflow:hidden;background-color:#374C59;padding:5px;width:100%;height:" + (_this.height) + "px'><table id ='collapse_table_" + randomSubstring + "'class='table-striped table-condensed' style='width:100%;padding:5px;background-color:#283C45;color:#5A676E;'><thead><tr><th>Label</th><th>Group</th></tr></thead><tbody>";
	      
	        $.each(data.nodes, function (i, v) {

	            tbl = tbl + "<tr><td>" + (v.id.toUpperCase()) + "</td><td>" + v.group + "</td></tr>"

	        });
	        tbl = tbl + "</tbody></table></div>";
	        $(chart_container).append(tbl);
	        $("#collapse_table_" + randomSubstring).DataTable({"bLengthChange": false, "paging": false, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	                if (iDisplayIndex % 2 == 1) {
	                    $('td', nRow).css('background-color', '#32464F');
	                } else {
	                    $('td', nRow).css('background-color', '#283C45');
	                }
	            }});
	        $("#collapseChart_table_" + randomSubstring).mCustomScrollbar({
	            axis: "y"
	        });
	        $("#collapse_table_" + randomSubstring + " tr:first").css("background-color", "#0CB29A");


	        var id1 = $("#collapse_table_" + randomSubstring).children('div').find('div').eq(0);
	        var id2 = $("#collapse_table_" + randomSubstring).children('div').find('div').eq(1);
	        var id3 = $("#collapse_table_" + randomSubstring).children('div').find('div').eq(2);
	        var id1attr = id1.attr("id");
	        var id2attr = id2.attr("id");
	        var id3attr = id3.attr("id");



	        $("#" + id1attr + " " + "label").css("color", "#666666")
	        $("#" + id2attr + " " + "label").css("color", "#666666")
	        $("#" + id3attr).css("color", "#666666")

	        $(" .dataTables_filter input").css({"margin-left": "0.5em", "position": "relative", "border": "0", "min-width": "240px",
	            "background": "transparent",
	            "border-bottom": "1px solid #666666",
	            " border-radius": " 0",
	            "padding": " 5px 25px",
	            "color": "#ccc",
	            "height": " 30px",
	            "-webkit-box-shadow": " none",
	            "box-shadow": " none"
	        });

	        $(".dataTables_wrapper").css("background-color", "#374C59");


	    });
/*	//------------------------------------------------------------------------------
*/	    /**
	     * Fuction to handle show hide of header options
	     */
	    $("body").on("click", ".header_chart_" + randomSubstring, function () {
	        var chartId = $(this).parent().parent().parent().parent().siblings("div").attr("id");
	        if ("#" + chartId == chart_container) {
	            $(this).css("display", "none");
	            $(".header_table_" + randomSubstring).css("display", "inline-block");
	            $(containerid).empty();
	            new collapseChart(actualOptions);
	        }
	    });
	    $("body").on("click", ".header_fullscreen_chart" + randomSubstring, function () {
	        $("#modal_" + randomSubstring).modal('show');
	        var options = jQuery.extend(true, {}, actualOptions);
	        setTimeout(function () {
	            $("#modal_chart_container" + randomSubstring).css("width", "100%")
	            options.container = "#modal_chart_container" + randomSubstring;
	           
	            options.headerOptions=false;
	            options.height =450;
	            new collapseChart(options);
	        }, 500);
	    });		
	}
    /*
	  * @purpose: getting entity data by name
	  * @created: 04 sep 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	 */
	function getLatLngByName(name){
		var deferred = $q.defer();
		var geocoder = new google.maps.Geocoder();
		var address = name, latitude, longitude;

		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			    latitude = results[0].geometry.location.lat();
			    longitude = results[0].geometry.location.lng();
				deferred.resolve({lat: latitude, lng: longitude});
		    } else {
		    	deferred.reject('error');
		    }
		}); 
		
		return deferred.promise;
	}
	/*
	  * @purpose: getting entity data by name
	  * @created: 09 aug 2017
	  * @returns: promise object of entitySearchResult
	  * @pramas: name(string)
	  * @author: sandeep
	  */
	this.plotWorldLocationChart = function(barOptions){
		    if (barOptions.container) {
		        $(barOptions.container).empty();
		    }
		    if (barOptions) {
		        this.container = barOptions.container ? barOptions.container : "body"
		        this.barColor = barOptions.barColor ? barOptions.barColor : "blue"
		        this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false
		        this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
		        this.data = (barOptions.data) ? barOptions.data : []
		        this.showTicks = barOptions.showTicks ? barOptions.showTicks : true;
		        this.ticks = barOptions.ticks ? barOptions.ticks : 'all';
		        this.showLegends = (barOptions.showLegends !== undefined) ? barOptions.showLegends : false;
		        this.xLabelRotation = barOptions.xLabelRotation ? barOptions.xLabelRotation : 0;
		        this.yLabelRotation = barOptions.yLabelRotation ? barOptions.yLabelRotation : 0;
		        this.margin = barOptions.margin ? {
		            top: barOptions.margin.top ? barOptions.margin.top : 20,
		            right: barOptions.margin.right ? barOptions.margin.right : 20,
		            bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
		            left: barOptions.margin.left ? barOptions.margin.left : 40
		        } : {top: 20, right: 20, bottom: 50, left: 50};
		        this.height = barOptions.height ? barOptions.height : 600;
		        this.width = barOptions.width ? barOptions.width : $(this.container).width() - 10;
		        this.showAxisX = (barOptions.showAxisX !== undefined) ? barOptions.showAxisX : true;
		        this.showAxisY = (barOptions.showAxisY !== undefined) ? barOptions.showAxisY : true;
		        this.showXaxisTicks = barOptions.showXaxisTicks !== undefined ? barOptions.showXaxisTicks : true;
		        this.showYaxisTicks = barOptions.showYaxisTicks !== undefined ? barOptions.showYaxisTicks : true;
		        this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
		        this.randomIdString = Math.floor(Math.random() * 10000000000);
		        barOptions.mapscale=barOptions.mapscale?barOptions.mapscale:6;
		        barOptions.mapscaleRight=barOptions.mapscaleRight?barOptions.mapscaleRight:2;
		        barOptions.mapscaleTop=barOptions.mapscaleTop?barOptions.mapscaleTop:2;
		    } else {
		        return false;
		    }
		    var randomSubstring = this.randomIdString;

		    var margin = this.margin,
		            width = this.width - margin.left - margin.right,
		            height = this.height - margin.top - margin.bottom,
		            barColor = this.barColor;
		    if(height>width){
		        height = width;
		        this.height = this.width;
		    }
		    $(".world_map_tooltip1").remove();
		    var tool_tip = $('body').append('<div class="world_map_tooltip1" style="padding:10px;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none; z-index: 1000"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
		    var colorScale = d3.scaleOrdinal(d3.schemeCategory10);
            var centered;

		    var worlddata = this.data[0];
		    var populationData = this.data[1];
		    var path = d3.geoPath();
		    var svg = d3.select(this.container)
		            .append("svg")
		            .attr('height', this.height)
		            .attr('width', this.width)
		            .attr('id', 'mainSvg-' + randomSubstring)
		            .append('g')
		            .attr('class', 'map');
		    var mapScale=Math.floor(width/barOptions.mapscale);

		    var projection = d3.geoEquirectangular().scale(mapScale).translate([width /barOptions.mapscaleRight , height / barOptions.mapscaleTop]);
		    var path = d3.geoPath().projection(projection);

		    var WorldData = this.data;
		    if (WorldData) {
		        if (WorldData.length)
		            drawWorld(worlddata, populationData);
		    } else {
		    }


		    function drawWorld(data, population) {

		        var populationById = {};

		        population.forEach(function (d) {
		            populationById[d.id] = +d.population;
		        });

		        svg.append("g")
		                .attr("class", "countries")
		                .selectAll("path")
		                .data(data.features)
		                .enter()
		                .append("path")
		                .attr("d", path)
		                .style("fill", function (d) {
		                    if(populationById[d.id]){
		                        return "#435DBE";
		                    }else{
		                        return "#3778BF";
		                    }
		                   
		                    return colorScale(populationById[d.id]);
		                })
		                .style('stroke', 'white')
		                .style('stroke-width', 0.5)
		                .style("opacity", 0.8);
		                
		        var marks = [{name: "Malaysia", long:96.2, lat: 15.210484, mark: "assets/images/redpin.png",amount:9500},
			                	{name: "Saudi Arabia", long:37.779332, lat:34.347891, mark: "assets/images/bluepin.png",amount:1500},
			                	{name: "Iraq", long:32.679291, lat: 42.223191, mark: "assets/images/redpin.png",amount:9500},
			                	{name: "Argentina", long:-72.61667199999999, lat: -28.416097, mark: "assets/images/bluepin.png",amount:3500},
			                	{name: "Paraguay", long:-64.443832, lat: -11.542503, mark: "assets/images/bluepin.png",amount:1500},
			                	{name: "Venezuela", long: -70.5, lat: 20, mark: "assets/images/redpin.png",amount:5500},
			                	{name: "France", long:-3.213749, lat: 54.227638, mark: "assets/images/bluepin.png",amount:2500},
			                	{name: "Germany", long:3.851526, lat: 56.565691, mark: "assets/images/bluepin.png",amount:6500},
			                	{name: "Switzerland", long:2.9, lat: 54.8, mark: "assets/images/bluepin.png",amount:4500},
			                	{name: "UK", long:-8.435973, lat: 59.378051, mark: "assets/images/bluepin.png",amount:8500}
		             	];
           	svg.selectAll(".mark")
				    .data(barOptions.markers);
           	
           	angular.forEach(barOptions.markers, function(markerV, markerK){            		
			    	getLatLngByName(markerV.name).then(function(latlng, latlngkey){
			    		markerV.lat = latlng.lat;
			    		markerV.long = latlng.lng;
			    		var projectedValue = projection([latlng.lng,latlng.lat])
			    		projectedValue[0] = projectedValue[0] -10; 
			    		projectedValue[1] = projectedValue[1] -20;
		        		svg
	            		.append("image")
					    .attr('class','mark')
					    	.on("mouseover", function () {
					           $(".world_map_tooltip1").html('<div class="custom-tooltip right" style="top:'+(d3.event.pageY - 10)+'"px";left:'+(d3.event.pageX + 10)+'"px";"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="text-align:left;"><span>Location:'+markerV.name+'</span></div></div>');

				                       return $(".world_map_tooltip1").css("display", "block");

			                })
			                .on("mousemove", function () {
		                      $(".world_map_tooltip1").css("top", (d3.event.pageY - 10) + "px")
		                      return  $(".world_map_tooltip1").css("left", (d3.event.pageX + 10) + "px");
		                    })
		                    .on("mouseout", function () {
		                      return $(".world_map_tooltip1").css("display", "none");
		                    })
					    .attr('width', 20)
					    .attr('height', 20)
					    .attr("xlink:href",markerV.mark)
					    .attr("transform","translate(" + projectedValue + ")");
					});
		    	});
				    
		                
		                
		        svg.append("path")
		                .datum(topojson.mesh(data.features, function (a, b) {
		                    return a.id !== b.id;
		                }))
		                .attr("class", "names")
		                .attr("d", path);

		var zoom = d3.zoom().on("zoom",function() {
		        svg.attr("transform",d3.event.transform)
		        svg.selectAll("path")  
		            .attr("d", path.projection(projection)); 
		})
		  svg.call(zoom)
	    }
	};
    /*
	 * @purpose: plotTagCloudChartForLiveFeed function
	 * @created: 05 sep 2017
	 * @returns: promise object of entitySearchResult
	 * @pramas: barOptions(object)
	 * @author: sandeep
	*/
	this.plotTagCloudChartForLiveFeed = function(barOptions){
        if (barOptions.container) {
            $(barOptions.container).empty();
        }
        if (barOptions) {
            this.container = barOptions.container ? barOptions.container : "body"
            this.barColor = barOptions.barColor ? barOptions.barColor : "blue"
            this.readFromFile = (barOptions.readFromFile !== undefined) ? barOptions.readFromFile : false
            this.dataFileLocation = (barOptions.readFromFile !== undefined || barOptions.readFromFile) ? barOptions.dataFileLocation : undefined;
            this.data = (barOptions.data) ? jQuery.extend(true, [], barOptions.data ): []
            this.showTicks = barOptions.showTicks ? barOptions.showTicks : true;
            this.ticks = barOptions.ticks ? barOptions.ticks : 'all';
            this.showLegends = (barOptions.showLegends !== undefined) ? barOptions.showLegends : false;
            this.xLabelRotation = barOptions.xLabelRotation ? barOptions.xLabelRotation : 0;
            this.yLabelRotation = barOptions.yLabelRotation ? barOptions.yLabelRotation : 0;
            this.margin = barOptions.margin ? {
                top: barOptions.margin.top ? barOptions.margin.top : 20,
                right: barOptions.margin.right ? barOptions.margin.right : 20,
                bottom: barOptions.margin.bottom ? barOptions.margin.bottom : 30,
                left: barOptions.margin.left ? barOptions.margin.left : 40
            } : {top: 20, right: 20, bottom: 50, left: 50};
            this.height = barOptions.height ? barOptions.height : 600;
            this.width = barOptions.width ? barOptions.width : $(this.container).width() - 10;
            this.showAxisX = (barOptions.showAxisX !== undefined) ? barOptions.showAxisX : true;
            this.showAxisY = (barOptions.showAxisY !== undefined) ? barOptions.showAxisY : true;
            this.showXaxisTicks = barOptions.showXaxisTicks !== undefined ? barOptions.showXaxisTicks : true;
            this.showYaxisTicks = barOptions.showYaxisTicks !== undefined ? barOptions.showYaxisTicks : true;
            this.groupedStacked = barOptions.groupedStacked ? barOptions.groupedStacked : "grouped";
            this.randomIdString = Math.floor(Math.random() * 10000000000);
            this.domain = {
            	x: 	barOptions['domain'] ? barOptions['domain']['x'] : 10,
            	y: barOptions['domain'] ? barOptions['domain']['y'] : 5000
            }


        } else {
            return false;
        }
    
        var randomSubstring = this.randomIdString;

        var margin = this.margin,
                width = this.width - margin.left - margin.right,
                height = this.height - margin.top - margin.bottom,
                barColor = this.barColor;
        var colorScale = d3.scaleOrdinal(d3.schemeCategory10);
        this.data.sort(function(a,b){
  		   return b.size-a.size;
  	   })
  	   this.data.splice(20);
    	var textcloudData=this.data;
    	var domainX = barOptions['domain'] ? barOptions['domain']['x'] : 10;
    	var domainY = barOptions['domain'] ? barOptions['domain']['y'] : 5000;
        var sizeScale = d3.scaleLinear().domain([domainX,domainY]).range([12,35]);
        var domain = d3.extent(textcloudData,function(d){return d.size})
        if(domain[0] == domain[1]){
        	var sizeScale = d3.scaleLinear().domain(domain).range([12,20]);
        }else{
 		var sizeScale = d3.scaleLinear().domain(domain).range([10,18]);
        }
        var svg = d3.select(this.container)
                .append("svg")
                .attr('height', this.height)
                .attr('width', this.width)
                .attr('id', 'mainSvg-' + randomSubstring)
                .attr("class", "wordcloud")
                .append("g")
                .attr("transform", "translate(" + (parseInt(this.width/2)) + "," + (this.height/2) + ")");
    	d3.layout.cloud()
                .size([this.width, this.height])
                .words(textcloudData)
                .padding(2)
                .rotate(0)
                .font('monospace')
                .fontSize(function(d) {  return parseInt(sizeScale(d.size)); })
                .on("end", draw)
                .start();

        function draw(words) {
           svg.selectAll("g text")
                .data(words, function(d) { return d.text; })
                .enter()
                .append("text")
                .style('cursor', 'pointer')
                .style("font-family", "Roboto Regular")
                .style("fill", '#415158')
                .style("font-size", function(d, i) { 
                	 return parseInt(sizeScale(d.size));
                     })
                .attr("text-anchor", "middle")
                .attr("transform", function(d) {
                        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                 })
                .text(function(d) { return d.text; })
                .on('click', function(d){
                	d.text.replace(/\//g, ' ')
                	if(d.type === 'person') {
                        var url = '#!/person/'+ d.text;
                        window.open(url, '_blank');
                    } else if(d.type === 'organization'){
                        var url = '#!/company/'+ d.text;                       
                        window.open(url, '_blank');
                    }
                    else if(d.type === 'organization'){
                        var url = '#!/company/'+ d.text;                       
                        window.open(url, '_blank');
                    }
                    else if(d.type === 'document'){
                        var url = d.url;
                        console.log(url);
                        window.open(url, '_blank');
                    }
                    else if(!d.type){
                    	window.pushFilters(d.key, false, 'technology', d.text, 'technologyMultiple', 'locationMultipleEnd');
                    }
                	
                })
                .on('mouseover', function(d){
	               	 $(this).css("fill","#337ab7");
	            })
		        .on('mouseout', function(d){
		        	 $(this).css("fill","#415158");
		        });

        }
	};

    /*
	 * @purpose: plotAreaLineChartForLiveFeed function
	 * @created: 06 sep 2017
	 * @returns: no
	 * @pramas: options(object)
	 * @author: sandeep
	*/
	this.plotAreaLineChartForLiveFeed = function(options){
		    if (options.container) {
		         
		         $(options.container).empty();
		     }
		     if(options)
		 {
		     options.container=options.container?options.container:"body"
		     options.width=options.width?options.width:$(options.container).width()-10
		     options.height=options.height?options.height:300
		     options.marginTop=options.marginTop?options.marginTop:20
		     options.marginBottom=options.marginBottom?options.marginBottom:50
		     options.marginRight=options.marginRight?options.marginRight:20
		     options.marginLeft=options.marginLeft?options.marginLeft:20
		     options.xParam=options.xParam?options.xParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
		     options.yParam=options.yParam?options.yParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
		     options.gridx=options.gridx?options.gridx:false
		     options.gridy=options.gridy?options.gridy:false
		     options.axisX=options.axisX?options.axisX:false
		     options.axisY=options.axisY?options.axisY:false
		     options.randomIdString = Math.floor(Math.random() * 10000000000)
		     options.bloombergHeader = options.header ? options.header :"";
		     options.headerOptions = options.headerOptions == false ? options.headerOptions : true;
		  }   

		 var data = [];


		    var actualOptions = jQuery.extend(true, {}, options);

		     var randomSubstring = options.randomIdString;
		     var h = options.height;
		     var header =  options.bloombergHeader;
		     var containerid = options.container;
		     var _this = options;
		     var modalwidth = $(window).width() - 200;
		     var modal = ' <div id="modal_' + randomSubstring + '"class="modal fade " tabindex="-1" role="dialog"> <div class="modal-dialog modal-lg" style="width:' + modalwidth + 'px"> <form ><div class="modal-content"><div class="modal-body"  style="padding:0;background-color:#334d59" id="modal_chart_container' + randomSubstring + '"></div></div></form></div></div>';

		     var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 100%; margin: auto; margin-top: 0px; font-size: 14px;font-style: inherit;"> <div class="graphBox" style="height:' + h + 'px;max-height: ' + h + 'px;min-height: ' + h + 'px;margin: auto; background-color: transparent; width: 100%;left: 0px;top: 0px;position: relative;"> <div class="headerDiv" style="font-weight:bold;background-color: transparent;text-align: left;color: #239185; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + header + '</div><div id="bloomberg_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'

		     $(containerid).html(chartContainerdiv);
		     $(containerid).append(modal);
		     var chart_container = "#bloomberg_chart_div" + randomSubstring;
		     var root = options.data;

		     var data = options.data;
		     var curretn_uri = options.Uri;
		     var bubble_header = options.bloombergHeader;


		 var tool_tip = $('body').append('<div class="line_Chart_tooltip" style="position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;background-color:#FB8B1E;padding: 10px;border-radius: 5px;border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

/*		 // basic SVG setup
*/		 var margin = {top:  options.marginTop, right:  options.marginRight, bottom: options.marginBottom, left:  options.marginLeft},
		     width =   options.width - margin.left - margin.right,
		     height =  options.height - margin.top - margin.bottom;


		 var svg = d3.select(chart_container).append("svg")
		         .attr("width", width + margin.left + margin.right)
		         .attr("height", height + margin.top + margin.bottom)
		         .append("g")
		         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



/*		 // setup scales - the domain is specified inside of the function called when we load the data
*/		 var xScale = d3.scaleTime().rangeRound([0, width]);
		 var yScale = d3.scaleLinear().rangeRound([height, 0]);


/*		 // set the line attributes
*/		 var line = d3.line()
		         .x(function (d) {
		             return xScale(d.year); })  
		         .y(function (d) {
		             return yScale(d.value); }); 

		 var area = d3.area()
		         .x(function (d) {
		             return xScale(d.year); })
		         .y1(function (d) {
		             return yScale(d.value); }); 

		 var lineSvg = svg.append("g");                             
		     


/*		 // gridlines function in y axis function
*/		 function make_y_gridlines() {		
		     return d3.axisLeft(yScale)
		         .ticks(5)
		 };
		 function make_x_gridlines() {   
		     return d3.axisBottom(yScale)
		         .ticks(5)
		 };


		 var parseTime = d3.timeParse("%d-%b-%y");
		 var bisectDate = d3.bisector(function(d) { return d.year; }).left ;

		 data=options.data;

		     data.forEach(function(d) {
		       d.year = parseTime(d.year);
		       d.value = +d.value;
		   });


/*		     // adding domain range to the x-axis 
*/		     xScale.domain(d3.extent(data, function(d) { return d.year; }));
/*		     // add domain ranges to the x and y scales
*/		     yScale.domain([
		         d3.min(data, function (c) {
		             return c.value ; }),        
		         d3.max(data, function (c) {
		             return c.value ; })
		         ]);
		         area.y0(height);

/*		   // add the x axis
*/		      var x_g=svg.append("g")
		             .attr("class", "x axis x_axis")
		             .attr("transform", "translate(0," + height + ")")
		             .call(d3.axisBottom(xScale).ticks(5)).style("stroke","#7B7D80").style("font-family","Roboto Light");
		      x_g.selectAll("path").style("stroke", "none")
		                 .style("shape-rendering", "crispEdges")
		                 .style("fill", "none");
		         x_g.selectAll("line").style("stroke", "none")
		                 .style("shape-rendering", "crispEdges")
		                 .style("fill", "none");
		         x_g.selectAll("text").style("fill", "#3b515a")
		                 .style("font-size", "10px")
		                 .style("stroke", "none");
/*		     // add the y axis
*/		       var y_g=svg.append("g")
		             .attr("class", "y axis y_axis")
		             .attr("transform", "translate("+width+",0)")
		             .call(d3.axisRight(yScale).ticks(5)).style("stroke","#7B7D80").style("font-family","Roboto Light");
		  y_g.append("text")
		                 .attr("class", "axis-title")
		                 .attr("transform", "rotate(-90)")
		                 .attr("y", 6)
		                 .attr("dy", ".71em")
		                 .style("text-anchor", "end")
		                 .attr("fill", "#5D6971")
		                 .style("font-size", "20px")
		         y_g.selectAll("text").style("fill", "none")
		                 .style("font-size", "10px")
		                 .style("stroke", "none");
		         y_g.selectAll("path").style("stroke", "none")
		                 .style("shape-rendering", "crispEdges")
		                 .style("fill", "none");
		         y_g.selectAll("line").style("stroke","none")
		                 .style("shape-rendering", "crispEdges")
		                 .style("fill", "none");
		     svg.append("path")
		        .attr("fill",'#245a73')
		        .attr("class", "area")
		        .attr("d", area(data));

		     svg.append("path")
		       .data([data])
		       .attr("class", "line")
		       .attr("d", line).attr("fill",'none').attr('stroke','#245a73').attr('stroke-width','3px');
		      
		    


		      var focus = svg.append("g")                  
		     .style("display", "none");

		     var rect= svg.append("rect").attr('class','tool')                                    
		         .attr("width", width)                              
		         .attr("height", height)                            
		         .style("fill", "none")                           
		         .style("pointer-events", "all")                    
		         .on("mouseover", function() { focus.style("display",'block'); })
		         .on("mouseout", function() { focus.style("display", "none"); })
		         .on("mousemove", mousemove);                       

		     
		 function mousemove() {

		         var x0 = xScale.invert(d3.mouse(this)[0]),              
		             i = bisectDate(data, x0, 1),                   
		             d0 = data[i - 1],                             
		             d1 = data[i],                                  
		             d = x0 - d0.year > d1.year - x0 ? d1 : d0;   
		             
		             var myDate = new Date(d.year);
		            
		       focus.selectAll(".x").remove() ;       
		       focus.append("line")
		         .attr("class", "x")
		         .style("stroke", "#245a73")
		         .style("stroke-dasharray", "3,3")
		         .style("opacity", 1)
		         .attr("y1", 0)
		         .attr("y2", height); 
		     

		    focus.selectAll(".y0").remove()   
		     focus.append("circle")
		         .attr("class", "y0")
		         .style("fill", "#245a73")
		         .style("stroke", "white")
		         .attr("r", 6);

		   focus.selectAll(".y1").remove()   
		   focus.append("rect")
		    .attr("class", "y1")
		    .attr("width", "100px").style('fill','#245a73')
		    .attr("height", "20px") .attr("transform",
		             "translate(" + (xScale(d.year)-50) + "," +
		                            (height+5)+ ")");;
		   focus.append("text")
		     .attr("class", "y1").style("font-weight","normal").style("font-family","Roboto Light")
		     .style("stroke", "#6d818a")
		     .attr("dx", 12)
		     .attr("dy", "13px");

		 focus.select("circle.y0")
		       .attr("transform",
		             "translate(" + xScale(d.year) + "," +
		                            yScale(d.value) + ")");
		                            
		   
		 focus.select("text.y1").style("font-weight","normal")
		   .attr("transform",
		             "translate(" + (xScale(d.year)-55) + "," +
		                            (height+7) + ")")
		                           .text(myDate.getFullYear()+':'+d.value);

		 focus.select(".x")
		   .attr("transform",
		               "translate(" + xScale(d.year) + ",0)")                       
		                  .attr("y2", height);

		           } 

/*       //------------------------------------------------------------------------------
*/	     /**
	      * Fuction to handle show hide of header options
	      */
	     $("body").on("click", ".bstheme_menu_button_" + randomSubstring, function () {
	         var id = ($(this).attr("data-target"));
	         if ($(id).css("display") == "none") {
	             $(id).css("display", "inline-block");
	         } else {
	             $(id).css("display", "none");
	         }
	     });
/*		 //------------------------------------------------------------------------------
*/	     /**
	      * Fuction to handle show hide of header options
	      */
	     $("body").on("click", ".header_refresh_" + randomSubstring, function () {
	         var chartId = $(this).parent().parent().parent().parent().siblings("div").attr("id");
	         if ("#" + chartId == chart_container) {
	             $(containerid).empty();

	             loadlineData(actualOptions);
	         }
	     });
/*		 //------------------------------------------------------------------------------
*/		     /**
		      * Fuction to handle show hide of header options
		      */

	     $("body").on("click", ".header_table_" + randomSubstring, function () {

	         $(chart_container).empty();
	         $(this).css("display", "none");
	         $(".header_chart_" + randomSubstring).css("display", "inline-block");

	         var tbl = "<div id ='bloomberg_table_" + randomSubstring + "'  style='padding:5px;background-color: #425661;overflow:overflow:hidden;height:" + (_this.height) + "px'><table id ='bloomberg_table1_" + randomSubstring + "' class='table-striped ' style='width:100%;background-color:#283C45;padding:5px;color:#5A676E; ' ><thead><tr><th>VALUE</th><th>YEAR</th></tr></thead><tbody>";
	 
	         $.each(data, function (i, v) {
                 tbl = tbl + "<tr><td>" + (v.value) + "</td><td>" + v.year + "</td></tr>"
	         });
	         tbl = tbl + "</tbody></table></div>";
	         $(chart_container).append(tbl);
	         $("#bloomberg_table1_" + randomSubstring).DataTable({"bLengthChange": false, "paging": false, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	                 if (iDisplayIndex % 2 == 1) {
	                     $('td', nRow).css('background-color', '#32464F');
	                 } else {
	                     $('td', nRow).css('background-color', '#283C45');
	                 }
	             }});
	         $("#bloomberg_table_" + randomSubstring).mCustomScrollbar({
	             axis: "y"
	         });

	         $("#bloomberg_table_" + randomSubstring + " tr:first").css("background-color", "#0CB29A");
	         var id1 = $("#bloomberg_table_" + randomSubstring).children('div').find('div').eq(0);
	         var id2 = $("#bloomberg_table_" + randomSubstring).children('div').find('div').eq(1);
	         var id3 = $("#bloomberg_table_" + randomSubstring).children('div').find('div').eq(2);
	         var id1attr = id1.attr("id");
	         var id2attr = id2.attr("id");
	         var id3attr = id3.attr("id");



	         $("#" + id1attr + " " + "label").css("color", "#666666")
	         $("#" + id2attr + " " + "label").css("color", "#666666")
	         $("#" + id3attr).css("color", "#666666")

	         $(" .dataTables_filter input").css({"margin-left": "0.5em", "position": "relative", "border": "0", "min-width": "240px",
	             "background": "transparent",
	             "border-bottom": "1px solid #666666",
	             " border-radius": " 0",
	             "padding": " 5px 25px",
	             "color": "#ccc",
	             "height": " 30px",
	             "-webkit-box-shadow": " none",
	             "box-shadow": " none"
	         })

	     });

/*		 ------------------------------------------------------------------------------
*/	     /**
	      * Fuction to handle show hide of header options
	      */
	     $("body").on("click", ".header_chart_" + randomSubstring, function () {
	         $(chart_container).css("overflow", "hidden");
	         var chartId = $(this).parent().parent().parent().parent().siblings("div").attr("id");
	         if ("#" + chartId == chart_container) {
	             $(this).css("display", "none");
	             $(".header_table_" + randomSubstring).css("display", "inline-block");
	             $(containerid).empty();
	             new lineChart(actualOptions);
	         }
	     });

/*		 //------------------------------------------------------------------------------------------------
*/
	     $("body").on("click", ".header_fullscreen_chart" + randomSubstring, function () {
	         $("#modal_" + randomSubstring).modal('show');
	         var options = jQuery.extend(true, {}, actualOptions);
	         setTimeout(function () {
	             $("#modal_chart_container" + randomSubstring).css("width", "100%")
	             options.container = "#modal_chart_container" + randomSubstring;
	             options.headerOptions = false;
	             options.height = 450;
	             new lineChart(options);
	         }, 500);
	     });
	};
}