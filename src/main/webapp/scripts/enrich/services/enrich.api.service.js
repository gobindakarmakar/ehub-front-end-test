'use strict';
elementApp
.factory('EnrichApiService', enrichApiService);

		enrichApiService.$inject = [
			'$http',
			'$rootScope',
			'EHUB_API',
			'Upload',
			'$q'
		//	'SearchAPIEndpoint'
		]
		
		function enrichApiService(
				$http,
				$rootScope,
				EHUB_API,
				Upload,
				$q
				/* SearchAPIEndpoint */){
	
			return {
				CSV2JSON:CSV2JSON,
				createNewCase: createNewCase,
				caseSeedUpload:caseSeedUpload,
				getResolvedResultByCategoryTexts: getResolvedResultByCategoryTexts,
				getSearchResultByCategoryTexts: getSearchResultByCategoryTexts,
				getDFDGraphData: getDFDGraphData,
				getAllSearchHistory: getAllSearchHistory,
				getsavesearchHistory:getsavesearchHistory,
				createCaseFromCSVFile: createCaseFromCSVFile,
				createIndividualJSON: createIndividualJSON,
				createCorporateJSON: createCorporateJSON,
				getEntityVisualizer:getEntityVisualizer,
				createCaseFromQuestioner: createCaseFromQuestioner,
				AdvanceNewSearch:AdvanceNewSearch,//this is V1 multisource will be depracted soon 5 dec 2018
				getEntityResolvedV2:getEntityResolvedV2,
				addSearchEntity:addSearchEntity,
				getAllEntityComplexStructureData:getAllEntityComplexStructureData,
				getEntityComplexStructureData:getEntityComplexStructureData,
				saveOrUpdateEntityComplexStructureData:saveOrUpdateEntityComplexStructureData
				
			};
			/*
		     * @purpose: create case
		     * @created: 14 sep 2017
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Ankit
		    */
			function createNewCase(data, params){
	                var apiUrl = EHUB_API + "case/createCase";
			        var request = $http({
			            method: "POST",
			            url: apiUrl,
			            params: params,
			            data: data
			        });
			        return(request
			                .then(createCaseSuccess)
			                .catch(createCaseError));
			
			        /*getAllCaseSummary error function*/
			        function createCaseError(response) {
			            if (!angular.isObject(response.data) || !response.data.message) {
			                return($q.reject(response.data));
			            }
			            /*Otherwise, use expected error message.*/
			            return($q.reject(response.data.message));
			        }
			
			       /*getAllCaseSummary success function*/
			        function createCaseSuccess(response) {
			            return(response);
			        }
				
		}
		
		/*
	     * @purpose: caseSeedUpload
	     * @created: 25 sep 2017
	     * @params: caseId, fd,token, analystId
	     * @return: success, error functions
	     * @author: Ankit
	    */	
		function caseSeedUpload(caseId, fd,token, analystId){
			var apiUrl = EHUB_API + "case/uploadSeed";
			
	        var request = $http({
	            method: "POST",
	            url: apiUrl,
	            transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                params: {
                    "caseId":caseId,
                    "token":token,
                    "analystId": analystId
                },
	            data: fd
	        });
	        return(request
	                .then(caseSeedUploadSuccess)
	                .catch(caseSeedUploadError));
	
	        /*getAllCaseSummary error function*/
	        function caseSeedUploadError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	       /*getAllCaseSummary success function*/
	        function caseSeedUploadSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: CSVJSON
	     * @author: Ankit
	    */
		function CSV2JSON(csv){
	    	 var array = CSVToArray(csv);
	            var objArray = [];
	            for (var i = 1; i < array.length; i++) {
	                objArray[i - 1] = {};
	                for (var k = 0; k < array[0].length && k < array[i].length; k++) {
	                    var key = array[0][k];
	                    objArray[i - 1][key] = array[i][k]
	                }
	            }
	            var json = JSON.stringify(objArray);
	            var str = json.replace(/},/g, "},\r\n");
	            return str;
	    }
		
		/*
	     * @purpose: CSVArray
	     * @author: Ankit
	    */
		 function CSVToArray(strData, strDelimiter){

	            strDelimiter = (strDelimiter || ",");
	            var objPattern = new RegExp((
	                    // Delimiters.
	                    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
	                    // Quoted fields.
	                    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
	                    // Standard fields.
	                    "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
	            var arrData = [[]];
	            var arrMatches = null;
	            while (arrMatches = objPattern.exec(strData)) {
	                var strMatchedDelimiter = arrMatches[1];
	                if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
	                    arrData.push([]);
	                }
	                if (arrMatches[2]) {
	                    var strMatchedValue = arrMatches[2].replace(
	                            new RegExp("\"\"", "g"), "\"");
	                } else {
	                    var strMatchedValue = arrMatches[3];
	                }
	                arrData[arrData.length - 1].push(strMatchedValue);
	            }
	            return (arrData);
        }
		 /*
	     * @purpose: create Individual JSON
	     * @author: swathi
	     */
		 function createIndividualJSON(jsonData, response){
			 var persons = [
				 {
					 "name": jsonData[0]["Name Of Person"],
        			 "grossYearlyIncome": jsonData[0]["Gross yearly income"],
        	         "placeOfBirth": jsonData[0]["Place of Birth"],
        	         "address": jsonData[0]["Address"],
        	         "gender": jsonData[0]["Gender"],
        	         "dateOfBirth": jsonData[0]["Date Of Birth"],
        	         "ageGroup": jsonData[0]["Age Group"],
        	         "employmentStatus": jsonData[0]["Employment Status"],
        	         "countryOfResidence": jsonData[0]["Country of residence"],
        	         "countryOfCitizenship": jsonData[0]["Country of citizenship"],
        	         "phoneNumber": jsonData[0]["Phone Number"],
        	         "taxId": jsonData[0]["Tax ID/Tax Number"],
        	         "email": jsonData[0]["Email"]
				 }
			 ];
             var forJsonBlob = {
                 "type": jsonData[0]["Is Customer Individual Or Corporate"] + '-' + jsonData[0]["Type"],
                 "customerType": jsonData[0]["Is Customer Individual Or Corporate"],
                 "remark": jsonData[0]["Remark"],
                 "description": jsonData[0]["Description"],
                 "priority": jsonData[0]["Priority [0/1/2]"],
                 "rule": jsonData[0]["Rule"],
                 "persons": persons,
                 "companies": []
             };
             return forJsonBlob;
		 }
		 /*
	     * @purpose: create Corporate JSON
	     * @author: swathi
	     */
		 function createCorporateJSON(jsonData, response){
			 var persons = [], companies = [];
             var _d = jsonData[0]["DirectorList(Name/DateOfBirth/Nationality/Role/Address)"].split(",");
             for (var i = 0; i < _d.length; i++) {
                 if (_d[i].length > 0) {
                     var info = new Object();
                     var d_details = _d[i].split("|");
                     info["name"] = d_details[0];
                     info["dateOfBirth"] = d_details[1];
                     info["nationality"] = d_details[2];
                     info["role"] = d_details[3];
                     info["address"] = d_details[4];
                     info["type"] = "Director";
                     persons.push(info);
                 }
             }
             
             var _c = jsonData[0]["Customer List(Name/Address)"].split(",");
             for (var i = 0; i < _c.length; i++) {
                 if (_c[i].length > 0) {
                     var info2 = new Object();
                     var c_details = _c[i].split("|");
                     info2["name"] = c_details[0];
                     info2["address"] = c_details[1];
                     info2["type"] = "Customer";
                     companies.push(info2);
                 }
             }
             
             var _s = jsonData[0]["Suppliers List(Supplier Name/Date Of Incorporation/Place Of Incorporation/Address)"].split(",");
             for (var i = 0; i < _s.length; i++) {
                 if (_s[i].length > 0) {
                     var info3 = new Object();
                     var s_details = _s[i].split("|");
                     info3["name"] = s_details[0];
                     info3["dateOfIncorporation"] = s_details[1];
                     info3["placeOfIncorporation"] = s_details[2];
                     info3["address"] = s_details[3];
                     info3["type"] = "Supplier";
                     companies.push(info3);
                 }
             }
             companies.push({
                 "name": jsonData[0]["Parent Company"],
                 "type": "Parent"
             });
             companies.push({
            	 "operatesRiskCountry": jsonData[0]["Does company operate in high risk countries ?"],
                 "numberOfShareholders": jsonData[0]["Number of Shareholders"],
                 "name": jsonData[0]["Company Name"],
                 "yearsInOperation": jsonData[0]["Years in Operation"],
                 "secondaryBusiness": jsonData[0]["Secondary Type of Business"],
                 "numberOfCustomers": jsonData[0]["Number of Customers"],
                 "numberOfSuppliers": jsonData[0]["Number of Suppliers"],
                 "primaryBusiness": jsonData[0]["Primary Type of Business"],
                 "taxId": jsonData[0]["Tax ID/Tax Number"],
                 "countriesOfOperation": jsonData[0]["Countries of Operation"],
                 "beneficialOwner": jsonData[0]["Ultimate Beneficial Owner"],
                 "highRiskCountry": jsonData[0]["If yes, Please select country"],
                 "corporateId": jsonData[0]["Corporate ID"],
                 "dateOfIncorporation": jsonData[0]["Date of Incorporation"],
                 "numberOfOwners": jsonData[0]["Number of Owners"],
                 "website": jsonData[0]["Website"],
                 "address": jsonData[0]["Address"],
                 "bussinessAddress": jsonData[0]["Business Address"],
                 "tradingAsName": jsonData[0]["Trading as name"],
                 "authorizedSignatory": jsonData[0]["Authorized Signatory"],
                 "numberOfDirectors": jsonData[0]["Number of Directors"],
                 "faxNumber": jsonData[0]["Fax Number"],
                 "headQuarterAddress": jsonData[0]["Head Quarter Address"],
                 "type": "Current"
		 	  });
             var forJsonBlob = {
        		 "type": jsonData[0]["Is Customer Individual Or Corporate"] + '-' + jsonData[0]["Type"],
                 "customerType": jsonData[0]["Is Customer Individual Or Corporate"],
                 "remark": jsonData[0]["Remark"],
                 "description": jsonData[0]["Description"],
                 "priority": jsonData[0]["Priority [0/1/2]"],
                 "rule": jsonData[0]["Rule"],
                 "persons": persons,
                 "companies": companies
             };
             
             return forJsonBlob;
		 }
		 /*
	     * @purpose: getResolvedResultByCategoryTexts
	     * @created: 27 sep 2017
	     * @params: data(object)
	     * @return: success, error functions
	     * @author: sandeep
	    */
		function getResolvedResultByCategoryTexts(data){
			//var apiUrl = SearchAPIEndpoint + 'search/resolve';
		  var apiUrl =EHUB_API+'search/getData?token='+$rootScope.ehubObject.token+'&searchFlag=resolve';
	        var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data
	        });
	        return(request
	                .then(getResolvedResultByCategoryTextsSuccess)
	                .catch(getResolvedResultByCategoryTextsError));

	        /*getResolvedResultByCategoryTexts error function*/
	        function getResolvedResultByCategoryTextsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /*getResolvedResultByCategoryTexts success function*/
	        function getResolvedResultByCategoryTextsSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: getSearchResultByCategoryTexts
	     * @created: 27 sep 2017
	     * @params: data(object)
	     * @return: success, error functions
	     * @author: sandeep
	    */
		function getSearchResultByCategoryTexts(data){
			//var apiUrl = SearchAPIEndpoint + 'search';
			var apiUrl =EHUB_API+'search/getData?token='+$rootScope.ehubObject.token+'&searchFlag=search';
	        var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data
	        });
	        return(request
	                .then(getSearchResultByCategoryTextsSuccess)
	                .catch(getSearchResultByCategoryTextsError));

	        /*getSearchResultByCategoryTexts error function*/
	        function getSearchResultByCategoryTextsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /*getSearchResultByCategoryTexts success function*/
	        function getSearchResultByCategoryTextsSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: getDFDGraphData function
	     * @created: 27 sep 2017
	     * @params: queryText(staring), userId(number)
	     * @return: success, error functions
	     * @author: sandeep
	    */
		function getDFDGraphData(queryText, userId) {
			var apiUrl = EHUB_API + 'graph/getDataFineData?token='+ userId +'&searchQuery='+ queryText +'&depth=2';
	        var request = $http({
	            method: "GET",
	            url: apiUrl
	        });
	        return(request
	                .then(getSearchResultByCategoryTextsSuccess)
	                .catch(getSearchResultByCategoryTextsError));

	        /*getSearchResultByCategoryTexts error function*/
	        function getSearchResultByCategoryTextsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /*getSearchResultByCategoryTexts success function*/
	        function getSearchResultByCategoryTextsSuccess(response) {
	            return(response);
	        }
		}

		/*
	     * @purpose: getAllSearchHistory function
	     * @created: 27 sep 2017
	     * @params: queryText(staring), userId(number)
	     * @return: success, error functions
	     * @author: sandeep
	    */
		function getAllSearchHistory(recordsPerPage, userId,favourite,pagenumber) {
		//	var apiUrl = EHUB_API + 'elasticSearch/listSearchHistory?token='+ userId +'&recordsPerPage='+ recordsPerPage;
			var apiUrl = EHUB_API + 'search/recentSearches?token='+ userId +'&pageNumber='+ pagenumber +'&recordsPerPage='+recordsPerPage+'&favourite='+favourite+'&flag=search&dateFrom='+''+'&dateTo='+'';
	        var request = $http({
	            method: "GET",
	            url: apiUrl
	        });
	        return(request
	                .then(getAllSearchHistorySuccess)
	                .catch(getAllSearchHistoryError));

	        /*getAllSearchHistory error function*/
	        function getAllSearchHistoryError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /*getAllSearchHistory success function*/
	        function getAllSearchHistorySuccess(response) {
	            return(response);
	        }
		}

			/*
	     * @purpose: getsavesearchHistory function
	     * @created: 11 jan 2018
	     * @params: queryText(staring), userId(number)
	     * @return: success, error functions
	     * @author: ram singh
	    */
		function getsavesearchHistory(userId,searchid) {
		
			var apiUrl = EHUB_API + 'search/setFavourite?token='+ userId +'&searchId='+searchid;
	         var request = $http({
	            method: "POST",
	            url: apiUrl
	            });
	        return(request
	                .then(getsavesearchHistorySuccess)
	                .catch(getsavesearchHistoryError));

	        /*getsavesearchHistory error function*/
	        function getsavesearchHistoryError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /* Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }

	        /*getsavesearchHistory success function*/
	        function getsavesearchHistorySuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: create case from CSV File
	     * @created: 30 jan 2018
	     * @params: params, data
	     * @return: success, error functions
	     * @author: swathi
	    */	
		function createCaseFromCSVFile(params, data){
			var apiUrl = EHUB_API + "case/createCaseFromCSVFile";
			
	        var request = Upload.upload({
	            method: "POST",
	            url: apiUrl,
	            headers: {
	                'Content-Type': 'multipart / form-data'
	            },
	            params: params,
	            data: data
	        });
	        return(request
	                .then(createCaseFromCSVFileSuccess)
	                .catch(createCaseFromCSVFileError));
	
	        /*createCaseFromCSVFile error function*/
	        function createCaseFromCSVFileError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	       /*createCaseFromCSVFile success function*/
	        function createCaseFromCSVFileSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: getEntityVisualizer
	     * @created: 30 jan 2018
	     * @params: params, data
	     * @return: success, error functions
	     * @author: swathi
	    */	
	   function getEntityVisualizer( data){
			var apiUrl = EHUB_API +'entityVisualizer/ent?token='+$rootScope.ehubObject.token;
			var request = $http({
			   method: "POST",
			   url: apiUrl,
			   data:data
			   });
		   return(request
				   .then(getEntityVisualizerSuccess)
				   .catch(getEntityVisualizerError));
	
		   /*getEntityVisualizer error function*/
		   function getEntityVisualizerError(response) {
			   if (!angular.isObject(response.data) || !response.data.message) {
				   return($q.reject(response.data));
			   }
			   /* Otherwise, use expected error message.*/
			   return($q.reject(response.data.message));
		   }
	
		   /*getEntityVisualizer success function*/
		   function getEntityVisualizerSuccess(response) {
			   return(response);
		   }
	   }
	   /*
	     * @purpose: create Case From Questioner
	     * @created: 28th Jun 2018
	     * @params: params, file
	     * @return: success, error functions
	     * @author: Swathi
	     */

	    function createCaseFromQuestioner(params, file) {
	        var defer = $q.defer();
	        var apiUrl = EHUB_API + 'case/createCaseFromQuestioner';
	        var request = Upload.upload({
	            method: 'POST',
	            url: apiUrl,
	            timeout: defer.promise,
	            params: params,
	            data: file
	        });
	        request
	            .then(createCaseFromQuestionerSuccess)
	            .catch(createCaseFromQuestionerError);
	        
	        setTimeout(function(){
	        	if(!success){
	        		 defer.resolve('Cancelled');
	        	}
	        },600000)
	         var success;
	        /*create Case From Questioner error function*/
	        function createCaseFromQuestionerError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                defer.reject(response.data);
	            }
	            /*Otherwise, use expected error message.*/
	        	if(response.status == -1){
	        		defer.reject(response);
	        	}else{
	        		 /*Otherwise, use expected error message.*/
		            defer.reject(response.data.message);
	        	}
	           
	        }

	        /*create Case From Questioner success function*/
	        function createCaseFromQuestionerSuccess(response) {
	        	success = true ;
	            defer.resolve(response);
	        }
	        return defer.promise;
		};
		
		
		/*
		     * @purpose:  AdvancedSearch Akthar's  NEW API
		     * @created: 25 July 2018
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Ram Singh
		    */
		   function AdvanceNewSearch(data){
			var apiUrl = EHUB_API + "advancesearch/graph/search/entity?token="+$rootScope.ehubObject.token;
			var request = $http({
				method: "POST",
				url: apiUrl,
				data: data
			});
			return(request
					.then(AdvanceNewSearchSuccess)
					.catch(AdvanceNewSearchError));
	
			/*getAllCaseSummary error function*/
			function AdvanceNewSearchError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
		   /*getAllCaseSummary success function*/
			function AdvanceNewSearchSuccess(response) {
				return(response);
			}
		
		}
	/*
     * @purpose: Get EntityResolved using the multi-source V2 version
     * @created: 26 jul 2017
     * @params: data(object)
     * @return: success, error functions
     * @author: Ram Singh
    */
   function getEntityResolvedV2(data){
	/* new URL query using Idenntifier */
	//var apiUrl = EHUB_API + "advancesearch/graph/search/entity?token="+$rootScope.ehubObject.token;
	var apiUrl = EHUB_API + 'advancesearch/entity/multisource?token='+$rootScope.ehubObject.token+'&query='+data.keyword+( data.jurisdiction ? '&jurisdiction='+data.jurisdiction :'')+(data.website ?'&website='+data.website:'');
		//var apiUrl = 'entity/controllers/data.json';
	 //var apiUrl =  "http://159.89.108.32:9091/graph/api/search/new";
	var request = $http({
		method: "GET",
		url: apiUrl,
		//data: data
	});
	return(request
			.then(getEntityResolvedV2Success)
			.catch(getEntityResolvedV2Error));

/*        getEntityResolvedV2 error function*/
	function getEntityResolvedV2Error(response) {
		if (!angular.isObject(response.data) || !response.data.message) {
			return($q.reject(response.data));
		}
		 /*Otherwise, use expected error message.*/
		return($q.reject(response.data.message));
	}

	/*getEntityResolvedV2 success function*/
	function getEntityResolvedV2Success(response) {
		return(response);
	}
}
		/*
		* @purpose: Get EntityResolved using the multi-source V2 version
		* @created: 26 jul 2017
		* @params: data(object)
		* @return: success, error functions
		* @author: Ram Singh
		*/
	

	function addSearchEntity(data){
		/* new URL query using Idenntifier */
		var apiUrl = EHUB_API + 'search/saveSearch?token='+$rootScope.ehubObject.token+'&searchFlag=search';
		data = JSON.stringify(data);
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return(request
				.then(addSearchEntitySuccess)
				.catch(addSearchEntityError));

	/*        addSearchEntity error function*/
		function addSearchEntityError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return($q.reject(response.data.message));
		}

		/*addSearchEntity success function*/
		function addSearchEntitySuccess(response) {
			return(response);
		}
	}

		/*
		* @purpose: Get All Entity Complex structure
		* @created: 01 jul 2019
		* @params: data(object)
		* @return: success, error functions
		* @author: Karunakar
		*/
	
		function getAllEntityComplexStructureData(){
			var apiUrl = EHUB_API + 'entityComplexStructure/getAllEntityComplexStructure?token='+$rootScope.ehubObject.token;
			var request = $http({
				method: "GET",
				url: apiUrl,
				//data: data
			});
			return(request
					.then(getAllEntityComplexStructureDataSuccess)
					.catch(getAllEntityComplexStructureDataError));
	
			/*getAllEntityComplexStructureData error function*/
			function getAllEntityComplexStructureDataError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
			/*getAllEntityComplexStructureData success function*/
			function getAllEntityComplexStructureDataSuccess(response) {
				return(response);
			}
		}

		/*
		* @purpose: Get Entity Complex structure
		* @created: 01 jul 2019
		* @params: data(object)
		* @return: success, error functions
		* @author: Karunakar
		*/
	
		function getEntityComplexStructureData(data){
			var apiUrl = EHUB_API + 'entityComplexStructure/getEntityComplexStructure?token='+$rootScope.ehubObject.token;
			var request = $http({
				method: "GET",
				url: apiUrl,
				data: data
			});
			return(request
					.then(getEntityComplexStructureDataSuccess)
					.catch(getEntityComplexStructureDataError));
	
		/*        getEntityComplexStructureData error function*/
			function getEntityComplexStructureDataError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
			/*getEntityComplexStructureData success function*/
			function getEntityComplexStructureDataSuccess(response) {
				return(response);
			}
		}

		/*
		* @purpose: Save Or Update Entity Complex structure
		* @created: 01 jul 2019
		* @params: data(object)
		* @return: success, error functions
		* @author: Karunakar
		*/
	
		function saveOrUpdateEntityComplexStructureData(data){
			var apiUrl = EHUB_API + 'entityComplexStructure/saveOrUpdateEntityComplexStructure?token='+$rootScope.ehubObject.token;
			var request = $http({
				method: "POST",
				url: apiUrl,
				data: data
			});
			return(request
					.then(saveOrUpdateEntityComplexStructureDataSuccess)
					.catch(saveOrUpdateEntityComplexStructureDataError));
	
		/*        saveOrUpdateEntityComplexStructureData error function*/
			function saveOrUpdateEntityComplexStructureDataError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}
	
			/*saveOrUpdateEntityComplexStructureData success function*/
			function saveOrUpdateEntityComplexStructureDataSuccess(response) {
				return(response);
			}
		}
};