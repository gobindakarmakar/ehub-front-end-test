'use strict';
elementApp
	.service('EntityorgChartService', entityorgChartService);
entityorgChartService.$inject = [
	'$q',
	'$timeout',
	'$state',
	'customEntites',
	'TopPanelApiService',
	'$rootScope'
	
];

function entityorgChartService(
	$q,
	$timeout,
	$state,
	customEntites,
	TopPanelApiService,
	$rootScope
	) {
		var scope = {
			documentListsData:[],
			sourceList_forModal:[],
			evidenceDocumentsListsData:[]
		};

	/*
	 * @purpose: add custom entity into chart
	 *  Ram Singh
	 */
	this.addEntityToChart = function (customAddEntity) {
		customEntites.adddeleteEntity.push(customAddEntity);
	}; //add entityfunctionend
	/*
	 * @purpose: delete custom entity into chart
	 *  Ram Singh
	 */
	this.deleteEntityFomChart = function (selectedItem) {
		customEntites.deleteEntityChart.push(selectedItem);
	}; //delete entity
	this.editChartEntity = function(editEntity){
		customEntites.editedEnties.push(editEntity);
	};
	this.addscreeningTotable = function (customAddScreening) {
		customEntites.screeningaddition.push(customAddScreening);
	}; //add entityfunctionend
		
	/*
	* @purpose : Get all documents list 
	* @author : Amarjith Kumar
	* @Date : 30-March-2019
	*/
	scope.documentListsData = [];
	this.getllSourceDocuments = function () {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": 9,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		scope.documentListsData = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllDocuments(params).then(function (res) {
				var documents = getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'docs');
				var evidences  = getEvidenceListDocs(params);
				$q.all([documents, evidences]).then(function(values) {
					resolve(values);
				  });
			}).catch(function (err) {
				reject(false);
			});
		});
	}

	/*
	* @purpose : Get all evidence documents list 
	* @author : karnakar
	* @Date : 01-july-2019
	*/

	function getEvidenceListDocs(paramFromApiFunc) {
		var params = paramFromApiFunc;
		params.docFlag = 6;
		scope.documentListsData = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllEvidenceDocuments(params).then(function (res) {
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'evidence_docs').then(function(response){
					resolve(response)
				});
			}).catch(function (err) {
				reject(false);
			});
		});
	}

	function getSuccessReportDocuments(pagerecords, docOrSticky) {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		 
		if (docOrSticky === "evidence_docs") {
			params.docFlag = 6;
		}
	return new Promise(function (resolve, reject) {
		TopPanelApiService.getAllDocuments(params).then(function (res) {
			var source_type = docOrSticky === "evidence_docs" ? 'png' : 'doc';
			if (res && res.data && res.data.result && res.data.result.length) {
				res.data.result.forEach(function (val, key) {
					val.source_type = val.type || source_type;
					val.sourceName = val.title || val.docName;
					val.sourceUrl = val.docName;
					val.docId = val.docId;
					val.docFlag = val.docFlag;
				});
				if (docOrSticky === 'docs') {
					scope.documentListsData = res.data.result;
					scope.sourceList_forModal = scope.sourceList_forModal.concat(scope.documentListsData);
					resolve({'documentListsData':scope.documentListsData });
				}
				if (docOrSticky === "evidence_docs") {
					scope.evidenceDocumentsListsData = res.data.result;
					scope.sourceList_forModal = scope.sourceList_forModal.concat(scope.evidenceDocumentsListsData);
					resolve({'evidenceDocumentsListsData':scope.evidenceDocumentsListsData});
				}
			}else if(res && res.data && res.data.result && res.data.result.length === 0 && docOrSticky === 'docs'){
				resolve({'documentListsData':{} });
			}else if(res && res.data && res.data.result && res.data.result.length === 0 && docOrSticky === 'evidence_docs'){
				resolve({'evidenceDocumentsListsData':{} });
			}
		}).catch(function (err) {
			reject(false);
		});
	});
	}

}