'use strict';
elementApp
	.factory('EntityApiService', entityApiService);
entityApiService.$inject = [
	'$http',
	'$rootScope',
	'$q',
	'SearchAPIEndpoint',
	'EHUB_API'
];

function entityApiService(
	$http,
	$rootScope,
	$q,
	SearchAPIEndpoint,
	EHUB_API) {
	return {
		getResolvedEntityDataOld: getResolvedEntityDataOld,
		getResolvedEntityData: getResolvedEntityData, //this is V1 multisource will be depracted soon 5 dec 2018
		getEntityDataByTextId: getEntityDataByTextId,
		getEntityCompanyDetails: getEntityCompanyDetails,
		getEntityPersonDetails: getEntityPersonDetails,
		getTwitterEntityDetails: getTwitterEntityDetails,
		getEntityCompanyLogo: getEntityCompanyLogo,
		getSanctions: getSanctions,
		getEntityList: getEntityList,
		getGraphData: getGraphData,
		getNewsByEntityTypeID: getNewsByEntityTypeID,
		getVLAdata: getVLAdata,
		loadAdverseVLA: loadAdverseVLA,
		newsSearchDetail: newsSearchDetail,
		textAnalystics: textAnalystics,
		getCaseDocuments: getCaseDocuments,
		downloadDocument: downloadDocument,
		caseFullTextSearch: caseFullTextSearch,
		getlistofOrganisations: getlistofOrganisations,
		getScreeningDetails: getScreeningDetails,
		getManagementDetailInformation: getManagementDetailInformation,
		getDirectorsIdentifier: getDirectorsIdentifier,
		getAssociatedDocumentDetailInformation: getAssociatedDocumentDetailInformation,
		getOrganizationData: getOrganizationData,
		getAdverseNewsForThreat: getAdverseNewsForThreat,
		getIdentifier: getIdentifier,
		saveIntoDatabase: saveIntoDatabase,
		generateJsonResponse: generateJsonResponse,
		getDocumentDetails: getDocumentDetails,
		getNews: getNews,
		getInfoCompanyHouse: getInfoCompanyHouse,
		updateAnswer: updateAnswer,
		updateRanking: updateRanking,
		downloadQuestionnaire: downloadQuestionnaire,
		generateReportData: generateReportData,
		CeriMultiSourcesPost: CeriMultiSourcesPost,
		updateEvaluation: updateEvaluation,
		getThreatAggregates: getThreatAggregates,
		getSharedhoolder: getSharedhoolder,
		DownloadAnnualReport: DownloadAnnualReport,
		updateIsoCategoryEvaluation: updateIsoCategoryEvaluation,
		getStandardCodeForIndustry: getStandardCodeForIndustry,
		getTechnologies: getTechnologies,
		getShareholderByUrl: getShareholderByUrl,
		getScreeningDetailsInternally: getScreeningDetailsInternally,
		getScreeningDetailsCompany: getScreeningDetailsCompany,
		addCustomShareHolder: addCustomShareHolder,
		recurrsiveAPiownBackend: recurrsiveAPiownBackend,
		postCompanyscreening: postCompanyscreening,
		getBEentitylevelCompanies: getBEentitylevelCompanies,
		getOwnershipPath: getOwnershipPath,
		getCorporateStructure: getCorporateStructure,
		markAsSignificant: markAsSignificant,
		getAllSignificantNews: getAllSignificantNews,
		markEntityAsfavorite: markEntityAsfavorite,
		deleteEntityAsfavorite: deleteEntityAsfavorite,
		getEntityResolvedV2: getEntityResolvedV2,
		complianceFieldsUrl: complianceFieldsUrl,
		getJurisdiction: getJurisdiction,
		sentimentUpdation: sentimentUpdation,
		getComplianceWidgets: getComplianceWidgets,
		getAllWidgetsByEntityId: getAllWidgetsByEntityId,
		markWidgetsVerified: markWidgetsVerified,
		getEntityQuestionsByEntityId: getEntityQuestionsByEntityId,
		getSourceIndustryList: getSourceIndustryList,
		saveEntityAttributes: saveEntityAttributes,
		saveSourceIndustryList: saveSourceIndustryList,
		getRiskScoreData: getRiskScoreData,
		getListofArticles: getListofArticles,
		getArticlesDetails: getArticlesDetails,
		postSanctionPepsignificance: postSanctionPepsignificance,
		deleteSanctionPepsignificance: deleteSanctionPepsignificance,
		selectEntity: selectEntity,
		customerOutreach: customerOutreach,
		getAPIListOfComments: getAPIListOfComments,
		getRequestIdOfScreening: getRequestIdOfScreening,
		getEntitiesScreening: getEntitiesScreening,
		getClassificationForSource: getClassificationForSource,
		getSources: getSources,
		stopGetcorporateAPI: stopGetcorporateAPI,
		deleteCustomEntites: deleteCustomEntites,
		addNewSourceAPI: addNewSourceAPI,
		APIgetScreenShot:APIgetScreenShot,
		saveUpdateEntityOfficer:saveUpdateEntityOfficer,
		updateComplexStructureAPI:updateComplexStructureAPI,
		getComplexStructureOnRefresh:getComplexStructureOnRefresh,
		officerSourceRoleManagement:officerSourceRoleManagement,
		auditLogGenerateReport:auditLogGenerateReport
	};
	/*
	 * @purpose: getResolvedEntityDataOld
	 * @created: 26 jul 2017
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getResolvedEntityDataOld(data) {
		var apiUrl = EHUB_API + 'search/getData?token=' + $rootScope.ehubObject.token + '&searchFlag=resolve';
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getResolvedEntityDataOldSuccess)
			.catch(getResolvedEntityDataOldError));
		/*        getResolvedEntityDataOld error function*/
		function getResolvedEntityDataOldError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getResolvedEntityData success function*/
		function getResolvedEntityDataOldSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getResolvedEntityData
	 * @created: 26 jul 2017
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getResolvedEntityData(data) {
		/* new URL query using Idenntifier */
		var apiUrl = EHUB_API + "advancesearch/graph/search/entity?token=" + $rootScope.ehubObject.token;
		//	var apiUrl = '../entity/controllers/data.json';
		//var apiUrl =  "http://159.89.108.32:9091/graph/api/search/new";
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getResolvedEntityDataSuccess)
			.catch(getResolvedEntityDataError));
		/*        getResolvedEntityData error function*/
		function getResolvedEntityDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getResolvedEntityData success function*/
		function getResolvedEntityDataSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getEntityDataByTextId
	 * @created: 26 jul 2017
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getEntityDataByTextId(data) {
		var apiUrl = EHUB_API + 'search/getData?token=' + $rootScope.ehubObject.token + '&searchFlag=search';
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getEntityDataByTextIdSuccess)
			.catch(getEntityDataByTextIdError));
		/*getEntityDataByTextId error function*/
		function getEntityDataByTextIdError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityDataByTextId success function*/
		function getEntityDataByTextIdSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getEntityCompanyDetails
	 * @created: 03 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getEntityCompanyDetails(searchText) {
		var apiUrl = 'http://46.101.242.80:9091/bstworkflowservices/api/search/company/' + searchText;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getEntityCompanyDetailsSuccess)
			.catch(getEntityCompanyDetailsError));
		/*getEntityCompanyDetails error function*/
		function getEntityCompanyDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityCompanyDetails success function*/
		function getEntityCompanyDetailsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getEntityPersonDetails
	 * @created: 03 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getEntityPersonDetails(searchText) {
		var apiUrl = 'http://46.101.242.80:9091/bstworkflowservices/api/search/person/' + searchText;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getEntityPersonDetailsSuccess)
			.catch(getEntityPersonDetailsError));
		/*getEntityPersonDetails error function*/
		function getEntityPersonDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityPersonDetails success function*/
		function getEntityPersonDetailsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getTwitterEntityDetails
	 * @created: 17 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getTwitterEntityDetails(searchText) {
		var apiUrl = 'http://104.198.13.21/api?source=twitter.com&query=' + searchText + '&api_key=xdty-0997-drt22345-009011';
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getTwitterEntityDetailsSuccess)
			.catch(getTwitterEntityDetailsError));
		/*getTwitterEntityDetails error function*/
		function getTwitterEntityDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getTwitterEntityDetails success function*/
		function getTwitterEntityDetailsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getEntityCompanyLogo
	 * @created: 18 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getEntityCompanyLogo(searchText) {
		var apiUrl = 'http://104.198.13.21/api?source=logo.google.com&query=' + searchText + '&api_key=xdty-0997-drt22345-009011';
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getEntityCompanyLogoSuccess)
			.catch(getEntityCompanyLogoError));
		/*getEntityCompanyLogo error function*/
		function getEntityCompanyLogoError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityCompanyLogo success function*/
		function getEntityCompanyLogoSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getSanctions
	 * @created: 18 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getSanctions(keyword, entityType) {
		//var apiUrl = EHUB_API + 'entity/search/'+ entityType +'?query=' + keyword;
		var apiUrl = EHUB_API + 'entity/' + entityType + '/search';
		var request = $http({
			method: "POST",
			url: apiUrl,
			params: {
				token: $rootScope.ehubObject.token
			},
			data: {
				"vcard:hasName": keyword
			},
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getSanctionsSuccess)
			.catch(getSanctionsError));
		/*getEntityCompanyLogo error function*/
		function getSanctionsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityCompanyLogo success function*/
		function getSanctionsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getEntityList
	 * @created: 18 aug 2017
	 * @params: searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getEntityList(entityName, entityListType) {
		//var apiUrl = EHUB_API + 'entity/lists/'+ entityListType + '/' + entityName;
		var apiUrl = EHUB_API + 'tuna/profile/' + entityName + '?token=' + $rootScope.ehubObject.token + '&orgOrPerson=' + entityListType;
		var request = $http({
			method: "GET",
			url: apiUrl
			// params: {
			// 	token: $rootScope.ehubObject.token
			// }
		});
		return (request
			.then(getSanctionsSuccess)
			.catch(getSanctionsError));
		/*getEntityCompanyLogo error function*/
		function getSanctionsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getEntityCompanyLogo success function*/
		function getSanctionsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getGraphData
	 * @created: 26 jul 2017
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getGraphData(data) {
		var apiUrl = EHUB_API + 'search/getData?token=' + $rootScope.ehubObject.token + '&searchFlag=graph';
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getGraphDataSuccess)
			.catch(getGraphDataError));
		/*        getGraphDataError error function*/
		function getGraphDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function getGraphDataSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getNewsByEntityTypeID
	 * @created: 31 oct 2017
	 * @params: entityType(string), searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function getNewsByEntityTypeID(entityType, searchText) {
		//	var deferred = $q.defer();
		// getSanctions(searchText, 'org').then(function(response){
		// 	if(response.data['orgs'].length > 0) {
		//var apiUrl = EHUB_API + 'adverseNews/news/'+ entityType +'/'+ response.data['orgs'][0]['_identifier'];
		var apiUrl = EHUB_API + 'adverseNews/profile/org/' + searchText + '?token=' + $rootScope.ehubObject.token;
		//var apiUrl =  'data/adverseNews.json';
		//var apiUrl = EHUB_API + 'marketIntelligence/entity/search?entity=Carphone'+'&token='+$rootScope.ehubObject.token;
		//  var apiUrl =  "http://95.85.58.85:3105/v1/search?entity="+searchText;
		var request = $http({
			method: "GET",
			url: apiUrl
			/*  params: {
			     token: $rootScope.ehubObject.token
			 } */
		});
		return (request
			.then(getNewsByEntityTypeIDSuccess)
			.catch(getNewsByEntityTypeIDError));

		function getNewsByEntityTypeIDError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function getNewsByEntityTypeIDSuccess(response) {
			return (response);
		}
	}

	function getVLAdata(caseId) {
		var apiUrl = EHUB_API + 'search/graphData/' + caseId;
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(getVLAdataSuccess)
			.catch(getVLAdataError));
		/*        getGraphDataError error function*/
		function getVLAdataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function getVLAdataSuccess(response) {
			return (response);
		}
	}

	function loadAdverseVLA(searchText) {
		var apiUrl = EHUB_API + 'adverseNews/graph/organization/' + searchText;
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(loadAdverseVLASuccess)
			.catch(loadAdverseVLAError));
		/*        getGraphDataError error function*/
		function loadAdverseVLAError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function loadAdverseVLASuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: newsSearch
	 * @created: 31 oct 2017
	 * @params: entityType(string), searchText(string)
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function newsSearchDetail(searchText) {
		var apiUrl = EHUB_API + 'entity/getCognitiveMicrosoft?q=' + searchText + '&count=10&offset=0&mkt=en-us&safeSearch=Moderate';
		//var apiUrl = 'https://api.cognitive.microsoft.com/bing/v7.0/news/search?q=' + searchText + '&count=50&offset=0&mkt=en-us&safeSearch=Moderate';
		var request = $http({
			method: "GET",
			url: apiUrl,
			/*  headers:{
			     'Ocp-Apim-Subscription-Key': '8c55f7aeccf1488589c46392f65de1a7'
			 }, */
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(newsSearchDetailSuccess)
			.catch(newsSearchDetailError));
		/*        getGraphDataError error function*/
		function newsSearchDetailError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function newsSearchDetailSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: textAnalystics
	 * @created: 31 oct 2017
	 * @params: none
	 * @return: success, error functions
	 * @author: sandeep
	 */
	function textAnalystics(data) {
		var apiUrl = EHUB_API + 'textSentiment/sentiment';
		var request = $http({
			method: "POST",
			url: apiUrl,
			/*  headers:{
                'Ocp-Apim-Subscription-Key': '5ba1ce076c134412944c9d5a367f7d3c',
//            	'Retry-After': '800000'
            }, */
			params: {
				token: $rootScope.ehubObject.token
			},
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(textAnalysticsSuccess)
			.catch(textAnalysticsError));
		/*        getGraphDataError error function*/
		function textAnalysticsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getGraphDataError success function*/
		function textAnalysticsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: getCaseDocuments
	 * @created: 11 nov 2017
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: swathi
	 */
	function getCaseDocuments(params) {
		var apiUrl = EHUB_API + 'case/getCaseDocuments';
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: params
		});
		return (request
			.then(getCaseDocumentsSuccess)
			.catch(getCaseDocumentsError));
		/*getCaseDocuments error function*/
		function getCaseDocumentsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getCaseDocuments success function*/
		function getCaseDocumentsSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: download Document
	 * @created: 11 nov 2017
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: swathi
	 */
	function downloadDocument(params) {
		var apiUrl = EHUB_API + 'case/downloadDocument';
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: params,
			responseType: "arraybuffer"
		});
		return (request
			.then(downloadDocumentSuccess)
			.catch(downloadDocumentError));
		/*downloadDocument error function*/
		function downloadDocumentError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*downloadDocument success function*/
		function downloadDocumentSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: case Full Text Search
	 * @created: 11 nov 2017
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: swathi
	 */
	function caseFullTextSearch(params) {
		var apiUrl = EHUB_API + 'case/fullTextSearch';
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: params
		});
		return (request
			.then(caseFullTextSearchSuccess)
			.catch(caseFullTextSearchError));
		/*caseFullTextSearch error function*/
		function caseFullTextSearchError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*caseFullTextSearch success function*/
		function caseFullTextSearchSuccess(response) {
			return (response);
		}
	}

	function getlistofOrganisations(name) {
		var apiUrl = EHUB_API + 'entity/person/suggest-name-loc';
		//			var apiUrl = EHUB_API +'tuna/search/companies?token='+$rootScope.ehubObject.token+'&q='+name;
		if (name) {
			apiUrl += name ? "?query=" + name + '&locationCode=GB' : "";
		}
		var request = $http({
			method: "GET",
			url: apiUrl,
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(getOrganationNamesSuggestionsSuccess)
			.catch(getOrganationNamesSuggestionsError));
		/*getOrganationNamesSuggestions error function*/
		function getOrganationNamesSuggestionsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getOrganationNamesSuggestions success function*/
		function getOrganationNamesSuggestionsSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: get Concerned Company Information
	 * created: 1st May 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Amritesh
	 */
	function getScreeningDetails(query, entitytype) {
		var apiUrl = EHUB_API + 'tuna/profile/' + query + '?token=' + $rootScope.ehubObject.token + '&orgOrPerson=' + entitytype;
		//  var apiUrl = EHUB_API +'/advancesearch/getProfileData?token='+$rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'GET'
			// data:query
			//					 params: {
			//			            	token: $rootScope.ehubObject.token
			//			            }
		});
		return (request
			.then(getScreeningDetailsSuccess)
			.catch(getScreeningDetailsError));
		/* getclustersInfo error function */
		function getScreeningDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getScreeningDetailsSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: post the officer or comapny for screening Results
	 * created: 1 oct 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Ram singh
	 */
	function getScreeningDetailsInternally(query) {
		//var apiUrl = EHUB_API +'tuna/profile/'+query+'?token='+$rootScope.ehubObject.token+'&orgOrPerson='+entitytype;
		var apiUrl = EHUB_API + 'advancesearch/getProfileData?token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: query,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getScreeningDetailsSuccess)
			.catch(getScreeningDetailsError));
		/* getclustersInfo error function */
		function getScreeningDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getScreeningDetailsSuccess(response) {
			return (response);
		}
	}
	/* 
	/* 
	 * purpose: post the comapny for screening Results
	 * created: 9 jan 2019
	 * params:no params
	 * return: success, error functions
	 * author: Ram singh
	 */
	function getScreeningDetailsCompany(query) {
		// var apiUrl = EHUB_API + 'advancesearch/getEntityScreening?token=' + $rootScope.ehubObject.token+'&organizationName='+query.name+( query.country ? '&country='+query.country : '')+(query.website ?  '&website='+query.website : '');
		var apiUrl = EHUB_API + 'advancesearch/getEntityScreening?token=' + $rootScope.ehubObject.token + '&name=' + query.name + (query.country ? '&country=' + query.country : '') + (query.entityType ? '&entityType=' + query.entityType : '') + (query.website ? '&website=' + query.website : '') + (query.dob ? '&dob=' + query.dob : '') + (query.identifier ? '&identifier=' + query.identifier : '') + (query.requestId ? '&requestId=' + query.requestId : '');

		var request = $http({
			url: apiUrl,
			method: 'GET'
		});
		return (request
			.then(getScreeningDetailsCompanySuccess)
			.catch(getScreeningDetailsCompanyError));
		function getScreeningDetailsCompanyError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		function getScreeningDetailsCompanySuccess(response) {
			return (response);
		}
	}
	/* 
	/* 
	 * purpose: post the shareholder added to the company
	 * created: 9 jan 2019
	 * params:no params
	 * return: success, error functions
	 * author: Ram singh
	 */
	function addCustomShareHolder(query, data) {

		var apiUrl = EHUB_API + 'advancesearch/addCustomShareHolder?token=' + $rootScope.ehubObject.token + '&mainIdentifier=' + query.mainIdentifier + '&isSubsidiare=' + query.isSubsidiare;
		var request = $http({
			url: apiUrl,
			method: 'PUT',
			data: data
		});
		return (request
			.then(addCustomShareHolderSuccess)
			.catch(addCustomShareHolderError));
		function addCustomShareHolderError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		function addCustomShareHolderSuccess(response) {
			return (response);
		}
	}

	/* 
	/* 
	 * purpose: DElete the shareholder added to the company
	 * created: 9 jan 2019
	 * params:no params
	 * return: success, error functions
	 * author: Ram singh
	 */
	function deleteCustomEntites(query) {

		var apiUrl = EHUB_API + 'advancesearch/deleteCustomEntity/' + query.identifier + '?token=' + $rootScope.ehubObject.token + '&field=' + query.field;

		var request = $http({
			url: apiUrl,
			method: 'DELETE',
		});
		return (request
			.then(addCustomShareHolderSuccess)
			.catch(addCustomShareHolderError));
		function addCustomShareHolderError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		function addCustomShareHolderSuccess(response) {
			return (response);
		}
	}

	/* 
	 * purpose: get Management Details Information
	 * created: 1st May 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Amritesh
	 */
	function getManagementDetailInformation(companyNumber) {
		var apiUrl = EHUB_API + 'tuna/getCompanyOfficers/' + companyNumber + '/officers';
		var request = $http({
			url: apiUrl,
			method: 'GET',
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(getManagementDetailInformationSuccess)
			.catch(getManagementDetailInformationError));
		/* getclustersInfo error function */
		function getManagementDetailInformationError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getManagementDetailInformationSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: get Concerned Company Information
	 * created: 1st May 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Ram Singh
	 */
	function getDirectorsIdentifier(data) {
		var apiUrl = EHUB_API + 'entity/person/search';
		var key = {
			"size": 2,
			"vcard:hasName": data.name,
			"vcard:hasAddress": {
				"country": ''
			}
		};
		if ((data && data.country_of_residence) || (data && data.address && data.address.country)) {
			key["vcard:hasAddress"].country = data.country_of_residence ? data.country_of_residence : (data.address ? (data.address.country ? data.address.country : "") : "");
		}
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: key,
			params: {
				token: $rootScope.ehubObject.token
			},
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getDirectorsIdentifierSuccess)
			.catch(getDirectorsIdentifierError));
		/*getDirectorsIdentifier error function*/
		function getDirectorsIdentifierError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getDirectorsIdentifier success function*/
		function getDirectorsIdentifierSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: get Associated Documents Details Information
	 * created: 1st May 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Amritesh
	 */
	function getAssociatedDocumentDetailInformation(companyNumber) {
		var apiUrl = EHUB_API + 'tuna/getFilingHistory/' + companyNumber + '/filing-history';
		var request = $http({
			url: apiUrl,
			method: 'GET',
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(getAssociatedDocumentDetailInformationSuccess)
			.catch(getAssociatedDocumentDetailInformationnError));
		/* getclustersInfo error function */
		function getAssociatedDocumentDetailInformationnError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getAssociatedDocumentDetailInformationSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: get Organization Data
	 * created: 18 May 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: swathi
	 */
	function getOrganizationData(companyNumber) {
		var apiUrl = EHUB_API + 'tuna/company/' + companyNumber + '/persons-with-significant-control';
		var request = $http({
			url: apiUrl,
			method: 'GET',
			params: {
				token: $rootScope.ehubObject.token
			}
		});
		return (request
			.then(getOrganizationDataSuccess)
			.catch(getOrganizationDataError));
		/* getOrganizationData error function */
		function getOrganizationDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getOrganizationData success function */
		function getOrganizationDataSuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: get Adverse News for threat
	 * @created: 12th July 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: swathi
	 */
	function getAdverseNewsForThreat(data) {
		data["class"] = 'Cyber Security';
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'adverseNews/events/search?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			data: data,
			headers: {
				"Content-Type": "application/json"
			},
			url: apiUrl
		});
		request
			.then(getAdverseNewsForThreatSuccess)
			.catch(getAdverseNewsForThreatError);
		/*getAdverseNewsForThreat error function*/
		function getAdverseNewsForThreatError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getAdverseNewsForThreat success function*/
		function getAdverseNewsForThreatSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: getIdentifier
	 * @created: 4th May 2018
	 * @params: company name
	 * @return: success, error functions
	 * @author: Prasanthi
	 */
	function getIdentifier(name) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'entity/org/suggest-name' + "?query=" + name + "&token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			headers: {
				"Content-Type": "application/json"
			},
			url: apiUrl
		});
		request
			.then(getIdentifierSuccess)
			.catch(getIdentifierError);
		/*getIdentifier error function*/
		function getIdentifierError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getIdentifier success function*/
		function getIdentifierSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: saveIntoDatabase
	 * @created: 30th April 2018
	 * @params: docId
	 * @return: success, error functions
	 * @author: varsha
	 */
	function saveIntoDatabase(docId) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentParser/auditDatabase' + "?token=" + $rootScope.ehubObject.token + "&docId=" + docId;
		var request = $http({
			method: 'GET',
			headers: {
				"Content-Type": "application/json"
			},
			url: apiUrl
		});
		request
			.then(saveIntoDatabaseSuccess)
			.catch(saveIntoDatabaseError);
		/*saveIntoDatabase error function*/
		function saveIntoDatabaseError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*saveIntoDatabase success function*/
		function saveIntoDatabaseSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: generateJsonResponse
	 * @created: 30th April 2018
	 * @params: docId
	 * @return: success, error functions
	 * @author: varsha
	 */
	function generateJsonResponse(docId, action) {
		var defer = $q.defer(),
			apiUrl;
		if (action == 'Refresh') {
			apiUrl = EHUB_API + 'documentParser/getUpdatedISODocumentParseResponse' + "?token=" + $rootScope.ehubObject.token + "&docId=" + docId;
		}
		else {
			apiUrl = EHUB_API + 'documentParser/getDocumentParseResponse' + "?token=" + $rootScope.ehubObject.token + "&docId=" + docId;
		}
		//    var apiUrl = "scripts/data.json"
		var request = $http({
			method: 'GET',
			headers: {
				"Content-Type": "application/json"
			},
			url: apiUrl
		});
		request
			.then(generateJsonResponseSuccess)
			.catch(generateJsonResponseError);
		/*generateJsonResponse error function*/
		function generateJsonResponseError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*saveIntoDatabase success function*/
		function generateJsonResponseSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: get document details
	 * @created: 1st May 2018
	 * @params: docId
	 * @return: success, error functions
	 * @author: zameer
	 */
	function getDocumentDetails(Id) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentStorage/getDocumentDetails' + "?token=" + $rootScope.ehubObject.token + "&docId=" + Id;
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(getDocumentDetailsSuccess)
			.catch(getDocumentDetailsError);
		/*getDocumentDetails error function*/
		function getDocumentDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getDocumentDetails success function*/
		function getDocumentDetailsSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: get news details
	 * @created: 4th May 2018
	 * @params: docId
	 * @return: success, error functions
	 * @author: zameer
	 */
	function getNews(identifier) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'tuna/profile/' + identifier + "?token=" + $rootScope.ehubObject.token + "&orgOrPerson=org";
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(getNewsSuccess)
			.catch(getNewsError);
		/*getNews error function*/
		function getNewsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getNews success function*/
		function getNewsSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: get company details from company house
	 * @created: 9th May 2018
	 * @params: companyId
	 * @return: success, error functions
	 * @author: Prasanthi
	 */
	function getInfoCompanyHouse(companyId) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'tuna/getComapniesHouse/' + companyId + "?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(getInfoCompanyHouseSuccess)
			.catch(getInfoCompanyHouseError);
		/*getInfoCompanyHouse error function*/
		function getInfoCompanyHouseError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getInfoCompanyHouse success function*/
		function getInfoCompanyHouseSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: update answer
	 * @created: 11th May 2018
	 * @params: id, answer
	 * @return: success, error functions
	 * @author: Prasanthi
	 */
	function updateAnswer(data) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentParser/updateAnswer?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(updateAnswerSuccess)
			.catch(updateAnswerError);
		/*updateAnswer error function*/
		function updateAnswerError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*updateAnswer success function*/
		function updateAnswerSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: updateRanking
	 * @created: 11th May 2018
	 * @params: id, answer
	 * @return: success, error functions
	 * @author: Prasanthi
	 */
	function updateRanking(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentParser/updateAnswerRank';
		var request = $http({
			method: 'POST',
			url: apiUrl,
			params: params,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(updateRankingSuccess)
			.catch(updateRankingError);
		/*updateRanking error function*/
		function updateRankingError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*updateRanking success function*/
		function updateRankingSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: download Questionnaire
	 * @created: 26th Jun 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function downloadQuestionnaire(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'case/downloadQuestioner?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
			params: params,
			dataType: "binary",
			processData: false,
			responseType: 'arraybuffer'
		});
		request
			.then(downloadQuestionnaireSuccess)
			.catch(downloadQuestionnaireError);
		/*download Questionnaire error function*/
		function downloadQuestionnaireError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*download Questionnaire success function*/
		function downloadQuestionnaireSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: update answer
	 * @created: 11th May 2018
	 * @params: id, answer
	 * @return: success, error functions
	 * @author: Ram singh
	 */
	function generateReportData(data) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'generateReport?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(generateReportDataSuccess)
			.catch(generateReportDataError);
		/*generateReportData error function*/
		function generateReportDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*generateReportData success function*/
		function generateReportDataSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: Ceri Single APi 
	 * @created: 27 june 2018
	 * @params: id, answer
	 * @return: success, error functions
	 * @author: Ram singh
	 */
	function CeriMultiSourcesPost(data) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'advancesearch/graph/search/entity?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(CeriMultiSourcesPostSuccess)
			.catch(CeriMultiSourcesPostError);
		/*CeriMultiSourcesPost error function*/
		function CeriMultiSourcesPostError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*CeriMultiSourcesPost success function*/
		function CeriMultiSourcesPostSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: update Evaluation
	 * @created: 29th Jun 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function updateEvaluation(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentParser/updateEvaluation';
		var request = $http({
			method: 'POST',
			url: apiUrl,
			params: params,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(updateEvaluationSuccess)
			.catch(updateEvaluationError);
		/*update Evaluation error function*/
		function updateEvaluationError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*update Evaluation success function*/
		function updateEvaluationSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: get Threat Aggregates
	 * @created: 19th July 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function getThreatAggregates(data) {
		data.query["class"] = 'Cyber Security';
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'adverseNews/events/aggregate?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(getThreatAggregatesSuccess)
			.catch(getThreatAggregatesError);
		/*get Threat Aggregates error function*/
		function getThreatAggregatesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*get Threat Aggregates success function*/
		function getThreatAggregatesSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: update Evaluation
	 * @created: 29th Jun 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function getSharedhoolder(url) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'entity/getAnnualReturns?url=' + url + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(getSharedhoolderSuccess)
			.catch(getSharedhoolderError);
		/*update Evaluation error function*/
		function getSharedhoolderError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*update Evaluation success function*/
		function getSharedhoolderSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: update Evaluation
	 * @created: 29th Jun 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function DownloadAnnualReport(url) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + "case/downloadAnnualReturnsDocument?url=" + url + "&token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
			dataType: "binary",
			processData: false,
			responseType: 'arraybuffer'
		});
		request
			.then(DownloadAnnualReportSuccess)
			.catch(DownloadAnnualReportError);
		/*update Evaluation error function*/
		function DownloadAnnualReportError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*update Evaluation success function*/
		function DownloadAnnualReportSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: updateIsoCategoryEvaluation
	 * @created: 27th July 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function updateIsoCategoryEvaluation(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'documentParser/updateIsoCategoryEvaluation';
		var request = $http({
			method: 'POST',
			url: apiUrl,
			params: params,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(updateIsoCategoryEvaluationSuccess)
			.catch(updateIsoCategoryEvaluationError);
		/*updateIsoCategoryEvaluation error function*/
		function updateIsoCategoryEvaluationError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*updateIsoCategoryEvaluation success function*/
		function updateIsoCategoryEvaluationSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* 
	 * purpose: get Industry standard code.
	 * created: 27th JUly 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Swathi
	 */
	function getStandardCodeForIndustry(code) {
		var apiUrl = EHUB_API + 'entity/industry/SIC/' + code + '/isic?token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'GET'
		});
		return (request
			.then(getIndustryStandardCodeSuccess)
			.catch(getIndustryStandardCodeError));
		/* getclustersInfo error function */
		function getIndustryStandardCodeError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getIndustryStandardCodeSuccess(response) {
			return (response);
		}
	}
	/* 
	 * purpose: get shareholders by name.
	 * created: 27th JUly 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Ram singh
	 */
	function getShareholderByUrl(url) {
		var apiUrl = EHUB_API + 'advancesearch/hierarchyLink?token=' + $rootScope.ehubObject.token;
		var data = {
			'url': url
		};
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		return (request
			.then(getShareholderByUrlSuccess)
			.catch(getShareholderByUrlError));
		/* getShareholderByUrlError error function */
		function getShareholderByUrlError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getShareholderByUrlSuccess success function */
		function getShareholderByUrlSuccess(response) {
			return (response);
		}
	}
	/* @purpose: get Technologies
	 * @created: 10th sep 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function getTechnologies(data) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'companydata/v1/company-data?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(getTechnologiesSuccess)
			.catch(getTechnologiesError);
		/*getTechnologies error function*/
		function getTechnologiesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getTechnologies success function*/
		function getTechnologiesSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * @purpose: Ceri Single APi 
	 * @created: 27 june 2018
	 * @params: id, answer
	 * @return: success, error functions
	 * @author: Ram singh
	 */
	function recurrsiveAPiownBackend(data) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'advancesearch/getOrgDataOfEntity?token=' + $rootScope.ehubObject.token;
		//var apiUrl = EHUB_API + 'advancesearch/graph/search/entity?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(CeriMultiSourcesPostSuccess)
			.catch(CeriMultiSourcesPostError);
		/*CeriMultiSourcesPost error function*/
		function CeriMultiSourcesPostError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*CeriMultiSourcesPost success function*/
		function CeriMultiSourcesPostSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: updateIsoCategoryEvaluation
	 * @created: 27th July 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Swathi
	 */
	function postCompanyscreening(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'advancesearch/getOrgData?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: params,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(postCompanyscreeningSuccess)
			.catch(postCompanyscreeningError);
		/*postCompanyscreening error function*/
		function postCompanyscreeningError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*postCompanyscreening success function*/
		function postCompanyscreeningSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
	 * purpose: get Industry standard code.
	 * created: 27th JUly 2018
	 * params: params(object)
	 * return: success, error functions
	 * author: Swathi
	 */
	function getBEentitylevelCompanies(url) {
		var apiUrl = url;
		var request = $http({
			url: apiUrl,
			method: 'GET'
		});
		return (request
			.then(getBEentitylevelCompaniesSuccess)
			.catch(getBEentitylevelCompaniesError));
		/* getclustersInfo error function */
		function getBEentitylevelCompaniesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getclustersInfo success function */
		function getBEentitylevelCompaniesSuccess(response) {
			return (response);
		}
	}
	/* @purpose: to get the ownership path
	 * @created: 26 oct 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Ram Singh
	 */
	function getOwnershipPath(params) {
		var defer = $q.defer();
		//  var apiUrl = '../entity/data/path.json';

		//  var apiUrl = EHUB_API + 'advancesearch/ownershipStructure?identifier=' + params.identifier + '&maxlevel=' + params.numnberoflevel + '&lowRange=' + params.lowRange + '&highRange=' + params.highRange + '&noOfSubsidiaries='+params.noOfSubsidiaries+ '&token=' + $rootScope.ehubObject.token + '&organisationName=' + params.organisationName + '&juridiction=' + params.juridiction+'&isSubsidiariesRequired='+ params.isSubsidiariesRequired+'&Start_date='+ params.Start_date+'&End_date='+ params.End_date;
		var apiUrl = EHUB_API + 'advancesearch/ownershipStructure?maxSubsidiarielevels=' + params.numnberoflevel + '&lowRange=' + params.lowRange + '&highRange=' + params.highRange + '&token=' + $rootScope.ehubObject.token + '&identifier=' + params.identifier + '&noOfSubsidiaries=' + params.noOfSubsidiaries + '&organisationName=' + params.organisationName + '&juridiction=' + params.juridiction + '&isSubsidiariesRequired=' + params.isSubsidiariesRequired + '&startDate=' + params.Start_date + '&endDate=' + params.End_date + (params.source ? '&source=' + params.source : '');

		// var apiUrl = '../entity/data/path.json';
		var request = $http({
			 method: 'POST',
			// method: 'GET',
			url: apiUrl,
			data: params.url,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(getOwnershipPathSuccess)
			.catch(getOwnershipPathError);
		/*getOwnershipPath error function*/
		function getOwnershipPathError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getOwnershipPath success function*/
		function getOwnershipPathSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: updateownership for every 30 sec 
	 * @created: 27th July 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Ram
	 */
	var getCorporatedeferObject = {};
	function getCorporateStructure(path) {
		var defer = $q.defer();
		getCorporatedeferObject = defer;
		var apiUrl = EHUB_API + 'advancesearch/getCorporateStructure?path=' + path + '&token=' + $rootScope.ehubObject.token;
		// var apiUrl = '../entity/data/entity.json';
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(getCorporateStructureSuccess)
			.catch(getCorporateStructureError);
		/*getCorporateStructure error function*/
		function getCorporateStructureError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*getCorporateStructure success function*/
		function getCorporateStructureSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	function stopGetcorporateAPI() {
		if (!_.isEmpty(getCorporatedeferObject)) {

			getCorporatedeferObject.reject(new Error('Source Changed'));
			console.log(getCorporatedeferObject);
		}
	}

	/*
	 * @purpose: getAllSignificantNews
	 * @created: 07th Mar 2019
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: karnakar
	 */
	function getAllSignificantNews(params) {
		var apiUrl = EHUB_API + 'significantArticles/getAllSignificantNews?entityId=' + params.identifier + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: params
		});
		return (request
			.then(getAllSignificantNewsSuccess)
			.catch(getAllSignificantNewsError));
		/*        getAllSignificantNews error function*/
		function getAllSignificantNewsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getAllSignificantNews success function*/
		function getAllSignificantNewsSuccess(response) {
			return (response);
		}
	}

	/*
	 * @purpose: markAsSignificant
	 * @created: 13th Nov 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Prasanthi
	 */
	function markAsSignificant(data, type) {
		var apiUrl = EHUB_API + 'significantArticles/saveSignificantArticles?token=' + $rootScope.ehubObject.token;
		var send = type === 'save' ? "POST" : "POST";
		if (send === 'DELETE') {
			apiUrl = EHUB_API + 'significantArticles/saveSignificantArticles?uuid=' + data.uuid + '&token=' + $rootScope.ehubObject.token;
		}
		var request = $http({
			method: send,
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: data
		});
		return (request
			.then(markAsSignificantSuccess)
			.catch(markAsSignificantError));
		/*        markAsSignificant error function*/
		function markAsSignificantError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*markAsSignificant success function*/
		function markAsSignificantSuccess(response) {
			return (response);
		}
	}
	/* @purpose: mark as entity Favrouite
	 * @created: 26 oct 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Ram Singh
	 */
	function markEntityAsfavorite(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'advancesearch/saveAdvanceNewsFavourite?token=' + $rootScope.ehubObject.token;
		//     var apiUrl = '../entity/controllers/level1.json';
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: params,
			headers: {
				"Content-Type": "application/json; charset=utf-8"
			}
		});
		request
			.then(markEntityAsfavoriteSuccess)
			.catch(markEntityAsfavoriteError);
		/*markEntityAsfavorite error function*/
		function markEntityAsfavoriteError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*markEntityAsfavorite success function*/
		function markEntityAsfavoriteSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/* @purpose: delete as favourite entity
	 * @created: 26 oct 2018
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Ram Singh
	 */
	function deleteEntityAsfavorite(params) {
		var defer = $q.defer();
		var apiUrl = EHUB_API + 'advancesearch/deleteAdvanceNewsFavourite?entityId=' + params.entityId + '&token=' + $rootScope.ehubObject.token;
		//     var apiUrl = '../entity/controllers/level1.json';
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		request
			.then(deleteEntityAsfavoriteSuccess)
			.catch(deleteEntityAsfavoriteError);
		/*deleteEntityAsfavorite error function*/
		function deleteEntityAsfavoriteError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				defer.reject(response.data);
			}
			/*Otherwise, use expected error message.*/
			defer.reject(response.data.message);
		}
		/*deleteEntityAsfavorite success function*/
		function deleteEntityAsfavoriteSuccess(response) {
			defer.resolve(response);
		}
		return defer.promise;
	}
	/*
 * @purpose: Get EntityResolved using the multi-source V2 version
 * @created: 26 jul 2017
 * @params: data(object)
 * @return: success, error functions
 * @author: Ram Singh
 */
	function getEntityResolvedV2(data) {
		/* new URL query using Idenntifier */
		var apiUrl = EHUB_API + 'advancesearch/entity/multisource?token=' + $rootScope.ehubObject.token + '&query=' + data.keyword + (data.jurisdiction ? '&jurisdiction=' + data.jurisdiction : '&jurisdiction=');
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getEntityResolvedV2Success)
			.catch(getEntityResolvedV2Error));

		/*        getEntityResolvedV2 error function*/
		function getEntityResolvedV2Error(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getEntityResolvedV2 success function*/
		function getEntityResolvedV2Success(response) {
			return (response);
		}
	}
	/*
	 * @purpose: Get compliance Fields Url
	 * @created: 12 dec 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Ram Singh
	 */
	function complianceFieldsUrl(data, json) {
		var apiUrl = '';
		if (json) {
			apiUrl = '../entity/data/' + data.name + '_' + data.fileld + '.json';
		} else {
			apiUrl = EHUB_API + 'advancesearch/entity/fetchLinkData?token=' + $rootScope.ehubObject.token + '&identifier=' + data.identifier + '&fields=' + data.type +'&jurisdiction='+data.jurisdiction;
		}
		// apiUrl = '../entity/data/' +  data.type+ '.json';
		// apiUrl = "../entity/data/overview.json"

		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(complianceFieldsUrlResolvedV2Success)
			.catch(complianceFieldsUrlResolvedV2Error));
		/*        complianceFieldsUrl error function*/
		function complianceFieldsUrlResolvedV2Error(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*complianceFieldsUrl success function*/
		function complianceFieldsUrlResolvedV2Success(response) {
			return (response);
		}
	}
	/*
	 * @purpose: Get compliance Fields Url
	 * @created: 12 dec 2018
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Ram Singh
	 */
	function getJurisdiction(token) {
		var apiUrl = EHUB_API + 'sourceJurisdiction/getSourceJurisdiction?token=' + token;
		var request = $http({
			method: "GET",
			url: apiUrl,
		});
		return (request
			.then(getJurisdictionSuccess)
			.catch(getJurisdictionError));
		/*        complianceFieldsUrl error function*/
		function getJurisdictionError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*complianceFieldsUrl success function*/
		function getJurisdictionSuccess(response) {
			return (response);
		}
	}
	/*
 * @purpose: mark sentiment as positive/negative/neutral
 * @created: 21 jan 2019
 * @params: data(object)
 * @return: success, error functions
 * @author: Ram Singh
 */
	function sentimentUpdation(data) {
		//	var apiUrl = EHUB_API + 'significantNews/saveNewsSentiment?token=' + $rootScope.ehubObject.token;
		var apiUrl = EHUB_API + 'significantArticles/saveArticleSentiment?token=' + $rootScope.ehubObject.token;

		var request = $http({
			method: 'POST',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: data
		});
		return (request
			.then(sentimentUpdationSuccess)
			.catch(sentimentUpdationError));
		/*        sentimentUpdation error function*/
		function sentimentUpdationError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*sentimentUpdation success function*/
		function sentimentUpdationSuccess(response) {
			return (response);
		}
	}

	/*
	 * @purpose: Get all compliance widgets
	 * @created: 5th Feb 2019
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Zameer
	 */
	function getComplianceWidgets() {
		var apiUrl = EHUB_API + 'widget/getAllComplianceWidgets?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			}
		});
		return (request
			.then(getComplianceWidgetsSuccess)
			.catch(getComplianceWidgetsError));

		/*        getComplianceWidgets error function*/
		function getComplianceWidgetsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getComplianceWidgets success function*/
		function getComplianceWidgetsSuccess(response) {
			return (response);
		}
	}

	/*
	 * @purpose: update widget as verified
	 * @created: 5th Feb 2019
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Zameer
	 */
	function markWidgetsVerified(data) {
		var apiUrl = EHUB_API + 'widget/saveWidgetReviews?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: data
		});
		return (request
			.then(markWidgetVerifiedSuccess)
			.catch(markWidgetVerifiedError));

		/*        markWidgetVerified error function*/
		function markWidgetVerifiedError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*markWidgetVerified success function*/
		function markWidgetVerifiedSuccess(response) {
			return (response);
		}
	}

	/*
	 * @purpose: Get all compliance widgets
	 * @created: 5th Feb 2019
	 * @params: data(object)
	 * @return: success, error functions
	 * @author: Zameer
	 */
	function getAllWidgetsByEntityId(entityId) {
		var apiUrl = EHUB_API + 'widget/getWidgetReviewsByEntityId?entityId=' + entityId + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			}
		});
		return (request
			.then(getAllWidgetsByEntityIdSuccess)
			.catch(getAllWidgetsByEntityIdError));

		/*        getAllWidgetsByEntityId error function*/
		function getAllWidgetsByEntityIdError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*getAllWidgetsByEntityId success function*/
		function getAllWidgetsByEntityIdSuccess(response) {
			return (response);
		}
	}

	/*
     * @purpose: getEntityQuestionsByEntityId function
     * @created: 8th feb 2019
     * @author: varsha
    */
	function getEntityQuestionsByEntityId(entityId) {

		var apiUrl = EHUB_API + 'advancesearch/getEntityQuestionsByEntityId?token=' + $rootScope.ehubObject.token + "&entityId=" + entityId + "&isUserBase=" + false + "&surveyId=" + $rootScope.qbServeyID;
		//		var apiUrl = 'http://159.65.192.32/ehubrest/ehubrest/api/advancesearch/getEntityQuestionsByEntityId?token='+$rootScope.ehubObject.token+"&entityId="+entityId
		// var apiUrl = "../entity/data/entityQuestions.json";
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getEntityQuestionsByEntityIdSuccess)
			.catch(getEntityQuestionsByEntityIdError));

		/*getEntityQuestionsByEntityId error function*/
		function getEntityQuestionsByEntityIdError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/* Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getEntityQuestionsByEntityId success function*/
		function getEntityQuestionsByEntityIdSuccess(response) {
			return (response);
		}
	}
	/* 
			* purpose: get source industry list details
			* created: 19th Feb 2019
			* author: Amarjith
			*/

	function getSourceIndustryList() {
		var apiUrl = EHUB_API + "sourceIndustry/getSourceIndustry?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl
		});
		return (request
			.then(getSourceIndustryListSuccess)
			.catch(getSourceIndustryListError));

		/*getSourceIndustryList error function*/
		function getSourceIndustryListError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getSourceIndustryList success function*/
		function getSourceIndustryListSuccess(response) {
			return (response);
		}
	}
	/* 
			* purpose: update company details
			* created: 19th Feb 2019
			* author: Amarjith
			*/

	function saveEntityAttributes(data) {
		var apiUrl = EHUB_API + "advancesearch/saveEntityAttributes?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: data
		});
		return (request
			.then(saveEntityAttributesSuccess)
			.catch(saveEntityAttributesError));

		/*saveEntityAttributes error function*/
		function saveEntityAttributesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*saveEntityAttributes success function*/
		function saveEntityAttributesSuccess(response) {
			return (response);
		}
	}

	/* purpose: save Source Industry List
	* created: 21-February-209
	* params: params(object)
	* return: success, error functions
	* author: Amarjith
	*/
	function saveSourceIndustryList(params) {
		var apiUrl = EHUB_API + "sourceIndustry/saveSourceIndustry" + '?token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: params
		});
		return (request
			.then(saveSourceIndustrySuccess)
			.catch(saveSourceIndustryError));

		/* saveSourceIndustry error function */
		function saveSourceIndustryError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* saveSourceIndustry success function */
		function saveSourceIndustrySuccess(response) {
			return (response);
		}
	}

	/*
     * @purpose: get riskScore
     * @created: 20th Feb
     * @params: params(object)
     * @return: success, error functions
     * @author: Varsha
    */
	function getRiskScoreData(entityId) {
		var apiUrl = EHUB_API + 'riskScore/score/' + entityId + '?token=' + $rootScope.ehubObject.token;

		//var apiUrl = 'https://4947kpl4ib.execute-api.eu-west-1.amazonaws.com/dev/risk/score/'+entityId;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getRiskScoreDataSuccess)
			.catch(getRiskScoreDataError));

		/*getRiskScoreData error function*/
		function getRiskScoreDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getRiskScoreData success function*/
		function getRiskScoreDataSuccess(response) {
			return (response);
		}
	}
	/*
	* @purpose: get list of Arrticles in the Adverseneews modal
	* @created: 25th Feb 2019
	* @params: params(object)
	* @return: success, error functions
	* @author: Ram
    */
	function getListofArticles(data) {
		var apiUrl = EHUB_API + 'entity/getArticles?token=' + $rootScope.ehubObject.token;
		//  var apiUrl = '../entity/data/articles.json';
		//	var apiUrl = EHUB_API + 'entity/getArticles?token='+$rootScope.ehubObject.token+'&entityId=&entityName=&newsClass=';
		//var apiUrl = 'https://4947kpl4ib.execute-api.eu-west-1.amazonaws.com/dev/risk/score/'+entityId;
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(getListofArticlesSuccess)
			.catch(getListofArticlesError));

		/*getListofArticles error function*/
		function getListofArticlesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getListofArticles success function*/
		function getListofArticlesSuccess(response) {
			return (response);
		}
	}
	/*
     * @purpose: get list of Arrticles in the Adverseneews modal
     * @created: 25th Feb 2019
     * @params: params(object)
     * @return: success, error functions
     * @author: Ram
    */
	function getArticlesDetails(data) {
		var apiUrl = EHUB_API + 'entity/getArticle?token=' + $rootScope.ehubObject.token;
		//		var apiUrl = EHUB_API + 'entity/getArticle?token='+$rootScope.ehubObject.token+'&entityId=&entityName=&newsClass=';
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(getArticlesDetailsSuccess)
			.catch(getArticlesDetailsError));

		/*getArticlesDetails error function*/
		function getArticlesDetailsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getArticlesDetails success function*/
		function getArticlesDetailsSuccess(response) {
			return (response);
		}
	}
	/*
     * @purpose: get list of Arrticles in the Adverseneews modal
     * @created: 25th Feb 2019
     * @params: params(object)
     * @return: success, error functions
     * @author: Ram
    */
	function postSanctionPepsignificance(data, requestType) {
		var apiUrl;
		if (requestType == "DELETE") {
			apiUrl = EHUB_API + 'significantNews/deleteWatchListSignificant?token=' + $rootScope.ehubObject.token;
		} else {
			apiUrl = EHUB_API + 'significantNews/saveSignificantwatchList?token=' + $rootScope.ehubObject.token;
		}
		var request = $http({
			method: requestType,
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: data
		});
		return (request
			.then(getpostSanctionPepsignificanceSuccess)
			.catch(getpostSanctionPepsignificanceError));

		/*getpostSanctionPepsignificance error function*/
		function getpostSanctionPepsignificanceError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getpostSanctionPepsignificance success function*/
		function getpostSanctionPepsignificanceSuccess(response) {
			return (response);
		}
	}

	/*
		 * @purpose: Delete the significant of Pep or sanction artcle
		 * @created: 18- 03 - 2019
		 * @params: params(object)
		 * @return: success, error functions
		 * @author: Amarjith Kumar
		*/
	function deleteSanctionPepsignificance(significantType, watchListId, comment) {
		var apiUrl = EHUB_API + 'significantNews/deleteWatchList/' + significantType + '?' + 'watchListId=' + watchListId + '&comment=' + comment + '&token=' + $rootScope.ehubObject.token;

		var request = $http({
			method: 'DELETE',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			}
		});
		return (request
			.then(getdeleteSanctionPepsignificanceSuccess)
			.catch(getdeleteSanctionPepsignificanceError));

		/*getpostSanctionPepsignificance error function*/
		function getdeleteSanctionPepsignificanceError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getpostSanctionPepsignificance success function*/
		function getdeleteSanctionPepsignificanceSuccess(response) {
			return (response);
		}
	}

	/*
		 * @purpose: getListOfComments
		 * @created: 15- 04 - 2019
		 * @params: significantType(string),significantId(number)
		 * @return: success, error functions
		 * @author: Amritesh
		*/
	function getAPIListOfComments(significantId) {
		var apiUrl = EHUB_API + 'significantNews/getSignificantComments?' + 'significantId=' + significantId + '&token=' + $rootScope.ehubObject.token;

		var request = $http({
			method: "GET",
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			}
		});
		return (request
			.then(getListOfCommentsSuccess)
			.catch(getListOfCommentsError));

		/*getpostSanctionPepsignificance error function*/
		function getListOfCommentsError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getpostSanctionPepsignificance success function*/
		function getListOfCommentsSuccess(response) {
			return (response);
		}
	}

	/*
	 * @purpose: get riskScore
	 * @created: 20th Feb
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Varsha
	*/
	function selectEntity(entityId, eid) {
		//	http://localhost:8080/ehubrest/api/advancesearch/selectEntity?token=2cca0ac4-6cfc-435a-bb83-2cbcd222fe46&identifier=549300v9qsig4wx4gj96
		var apiUrl = EHUB_API + 'advancesearch/selectEntity?token=' + $rootScope.ehubObject.token + '&identifier=' + entityId + '&requestId=' + eid;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(selectEntitySuccess)
			.catch(selectEntityError));

		/*selectEntity error function*/
		function selectEntityError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*selectEntity success function*/
		function selectEntitySuccess(response) {
			return (response);
		}
	}
	/*
	 * @purpose: get customerOutreach
	 * @created: 20th march
	 * @params: params(object)
	 * @return: success, error functions
	 * @author: Varsha
	*/
	function customerOutreach() {
		var apiUrl = EHUB_API + 'entityType/customerOutreach/getEntityRequirements?token=' + $rootScope.ehubObject.token;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(customerOutreachSuccess)
			.catch(customerOutreachError));

		/*customerOutreach error function*/
		function customerOutreachError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*customerOutreach success function*/
		function customerOutreachSuccess(response) {
			return (response);
		}
	}

	function getRequestIdOfScreening(mainEntityId, startdate, enddate, data) {
		var apiUrl = EHUB_API + 'advancesearch/getRequestIdOfScreening?token=' + $rootScope.ehubObject.token + '&mainEntityId=' + mainEntityId + '&startDate=' + startdate + '&endDate=' + enddate;
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: data
		});
		return (request
			.then(getRequestIdOfScreeningSuccess)
			.catch(getRequestIdOfScreeningError));

		/*getRequestIdOfScreening error function*/
		function getRequestIdOfScreeningError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getRequestIdOfScreening success function*/
		function getRequestIdOfScreeningSuccess(response) {
			return (response);
		}
	}

	function getEntitiesScreening(mainEntityId, requestId) {
		var apiUrl = EHUB_API + 'advancesearch/getEntitiesScreening?token=' + $rootScope.ehubObject.token + '&mainEntityId=' + mainEntityId + '&requestId=' + requestId;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getEntitiesScreeningSuccess)
			.catch(getEntitiesScreeningError));

		/*getEntitiesScreening error function*/
		function getEntitiesScreeningError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getEntitiesScreening success function*/
		function getEntitiesScreeningSuccess(response) {
			return (response);
		}
	}
	function getClassificationForSource() {
		var apiUrl = EHUB_API + "classification/getClassifications?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
		});
		return (request
			.then(ClassificationForSourceSuccess)
			.catch(ClassificationForSourceError));

		/*ClassificationForSource error function*/
		function ClassificationForSourceError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*ClassificationForSource success function*/
		function ClassificationForSourceSuccess(response) {
			return (response);
		}
	}

	function getSources(sourceFilterDto) {

		var apiUrl = EHUB_API + "sourceCredibility/getSources" + '?isAllSourcesRequired=' + true + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: sourceFilterDto ? sourceFilterDto : []
		});
		return (request
			.then(getSourcesSuccess)
			.catch(getSourcesError));

		/* getSources error function */
		function getSourcesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getSources success function */
		function getSourcesSuccess(response) {
			return (response);
		}
	}

	/* purpose: Add source
* created: 4th jan 2019
* author: karnakar
*/

	function addNewSourceAPI(params) {
		var apiUrl = EHUB_API + "sourceCredibility/saveGeneralSource?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data: params
		});
		return (request
			.then(addNewSourceAPISuccess)
			.catch(addNewSourceAPIError));

		/*addNewSourceAPI error function*/
		function addNewSourceAPIError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*addNewSourceAPI success function*/
		function addNewSourceAPISuccess(response) {
			return (response);
		}
	}
	
	function APIgetScreenShot(screenShotObj) {
		var apiUrl = EHUB_API + "advancesearch/getScreenShotData?token=" + $rootScope.ehubObject.token + '&url=' + screenShotObj.SourceValue['bst:registryURI'] + '&sourceName=' + screenShotObj.sourceName + '&entityId=' + screenShotObj.SourceValue.identifier;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getScreenShotSuccess)
			.catch(getScreenShotError));

		/*getEntitiesScreening error function*/
		function getScreenShotError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getEntitiesScreening success function*/
		function getScreenShotSuccess(response) {
			return (response);
		}
	}
	function saveUpdateEntityOfficer(params) {
		var apiUrl = EHUB_API + "advancesearch/customKeyManager?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: "PUT",
			url: apiUrl,
			data: params
		});
		return (request
			.then(saveUpdateEntityOfficerSuccess)
			.catch(saveUpdateEntityOfficerError));

		/*getEntitiesScreening error function*/
		function saveUpdateEntityOfficerError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getEntitiesScreening success function*/
		function saveUpdateEntityOfficerSuccess(response) {
			return (response);
		}
	}

	function updateComplexStructureAPI(paramData) {
		var apiUrl = EHUB_API + "entityComplexStructure/saveOrUpdateEntityComplexStructure?token="+ $rootScope.ehubObject.token+"&entityName="+paramData.entityName+"&entityId="+paramData.entityId+"&entitySource="+paramData.entitySource+"&isComplexStructure="+ paramData.isComplexStructure;
		var request = $http({
			method: "POST",
			url: apiUrl,
			data: paramData
		});
		return (request
			.then(updateComplexStructureAPISuccess)
			.catch(updateComplexStructureAPIError));

		/*updateComplexStructureAPI error function*/
		function updateComplexStructureAPIError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*updateComplexStructureAPI success function*/
		function updateComplexStructureAPISuccess(response) {
			return (response);
		}
	}
	function getComplexStructureOnRefresh(paramData) {
		var apiUrl = EHUB_API + "entityComplexStructure/getEntityComplexStructure?token="+ $rootScope.ehubObject.token+"&entityId="+paramData.entityId+"&entitySource="+paramData.entitySource+"&entityName="+paramData.entityName;
		var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(getComplexStructureOnRefreshSuccess)
			.catch(getComplexStructureOnRefreshError));

		/*getComplexStructureOnRefresh error function*/
		function getComplexStructureOnRefreshError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*getComplexStructureOnRefresh success function*/
		function getComplexStructureOnRefreshSuccess(response) {
			return (response);
		}
	}
	function officerSourceRoleManagement(data) {
			var apiUrl  = EHUB_API + "advancesearch/saveEntityOfficerInfo"; 
			var request = $http({
				method: "PUT",
				url: apiUrl,
				params:{'token':$rootScope.ehubObject.token},
				data:data
			});
			return (request
				.then(officerSourceRoleManagementSuccess)
				.catch(officerSourceRoleManagementError));

			/*officerSourceRoleManagement error function*/
			function officerSourceRoleManagementError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return ($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return ($q.reject(response.data.message));
			}

			/*officerSourceRoleManagement success function*/
			function officerSourceRoleManagementSuccess(response) {
				return (response);
			}
		}
	/* purpose: Audit log report generation
	* created: 10 sep  2019
	* author: Ram Singh
	*/
	function auditLogGenerateReport(data) {
			var apiUrl = EHUB_API + 'audit/auditLogForGeneratedReport?entityId=' + data.identifier + '&entityName=' + data.name+ '&token=' + $rootScope.ehubObject.token;
			var request = $http({
			method: "GET",
			url: apiUrl
		});
		return (request
			.then(auditLogGenerateReportSuccess)
			.catch(auditLogGenerateReportError));
		/*auditLogGenerateReport error function*/
		function auditLogGenerateReportError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}
		/*auditLogGenerateReport success function*/
		function auditLogGenerateReportSuccess(response) {
			return (response);
		}
	}

}