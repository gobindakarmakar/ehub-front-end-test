'use strict';
angular.module('ehubApp')
		.controller('DFDController', dfdController);

		dfdController.$inject = [
		    '$scope',
		    '$rootScope',
		    '$stateParams',
		    'EnrichApiService',
		    '$timeout'
		];
		
		function dfdController(
		        $scope,
		        $rootScope,
		        $stateParams,
		        EnrichApiService,
		        $timeout) {
			
		    $scope.disableSearchButton = true;
		    $scope.token = $rootScope.ehubObject.token;
		    var chordChartData = [];
		    var highRelevanceThreshold=7;
		    $scope.searchText = $stateParams.queryText;
		    $scope.totalNodesCount = 0, $scope.totalHighRelevanceCount = 0, 
		    $scope.totalHoverNodesCount = 0, $scope.totalHoverHighRelevanceCount = 0, $scope.isChordHovered = false;
		    /*
		     * @purpose: get dfd graph chart data
		     * @created: 25 jul 2017
		     * @returns: no
		     * @params: no
		     * @author: sandeep 
		     */
		    EnrichApiService.getDFDGraphData($stateParams.queryText, $scope.token).then(function (response) {
		    	$scope.totalNodesCount = response.data.nodeset.length;
		        angular.forEach(response.data.nodeset, function (node, key) {
		            chordChartData.push({
		                name: 'flare.' + node + '.flare',
		                size: key + 2000,
		                imports: [],
		                strength: 1
		            });
		        });
		        angular.forEach(response.data.adj, function (node, nodeKey) {
		            angular.forEach(chordChartData, function (chord, chordKey) {
		                if ('flare.' + nodeKey + '.flare' === chord.name) {
		                    angular.forEach(node, function (v, k) {
		                        chord.imports.push('flare.' + v + '.flare');
		                    });
		                    if(chord.imports.length>highRelevanceThreshold){
		                    	$scope.totalHighRelevanceCount = $scope.totalHighRelevanceCount+1;
		                    }
		                    chord.strength *= node.length;
		                }
		            });
		        });
		        $scope.noResultFound = response.data.nodeset.length === 0 ? true : false;
		        chordChartData.sort(function(a, b) {
		            return b.strength - a.strength;
		        });
		       
		        
		        chordChartD3V3(chordChartData);
		        $scope.disableSearchButton = false;
		    }, function () {
		        $scope.disableSearchButton = false;
				$scope.noResultFound  =true;
		    });
		    /*--------------------------------Chord Chart D3 V3---------------------*/
		    var windowHeight = $(window).height();
		    var headerHeight = $('.navbar-fixed-top').height() || 72;
		    var chordChartHeight = windowHeight - (headerHeight + 20);
		    var outerRadius = (chordChartHeight) / 2,
		            innerRadius = outerRadius - 130;
		    var fill = d3.scale.category20c();
		    var redPicker = 57, greenPicker = 76, bluePicker = 90;
		    var neonColorList = [];
		    var chord = d3.layout.chord()
		            .padding(.04)
		            .sortSubgroups(d3.descending)
		            .sortChords(d3.descending);
		
		    var arc = d3.svg.arc()
		            .innerRadius(innerRadius)
		            .outerRadius(innerRadius + 20);
		    var svg = d3.select("#forceCollapse").append("svg")
		            .attr("width", outerRadius * 2)
		            .attr("height", outerRadius * 2)
		//            .style('margin-bottom', '30')
		            .append("g")
		            .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");
		
		//	d3.json("static/data/chord_data.json", function(error, imports) {
		    function chordChartD3V3(imports) {
		        var indexByName = d3.map(),
		                nameByIndex = d3.map(),
		                matrix = [],
		                n = 0;
		
		        // Returns the Flare package name for the given class name.
		        function name(name) {
		            return name.substring(0, name.lastIndexOf("."));
		        }
		
		        // Compute a unique index for each package name.
		        imports.forEach(function (d) {
		            if (!indexByName.has(d = name(d.name))) {
		                nameByIndex.set(n, d);
		                indexByName.set(d, n++);
		            }
		        });
		
		        // Construct a square matrix counting package imports.
		        imports.forEach(function (d) {
		            var source = indexByName.get(name(d.name)),
		                    row = matrix[source];
		            if (!row) {
		                row = matrix[source] = [];
		                for (var i = -1; ++i < n; )
		                    row[i] = 0;
		            }
		            d.imports.forEach(function (d) {
		                row[indexByName.get(name(d))]++;
		            });
		        });
		        chord.matrix(matrix);
		        var g = svg.selectAll(".group")
		                .data(chord.groups)
		                .enter().append("g")
		                .attr("class", "group");
		
		        g.append("path")
		                .style("fill", function (d) {
		                	redPicker += 8, greenPicker += 10, bluePicker += 12;
		                	d.color = 'rgb('+ (redPicker) +','+ (greenPicker) +','+ (bluePicker) +')';
		                	neonColorList.push(d.color);
		                    return d.color;
		                })
		                .style("stroke", function (d) {
		                    return d.color;
		                })
		                .attr("d", arc)
		                .attr("id", function (d) {
		                    return 'chord-index-' + d.index
		                })
		                .on('mouseover', function (d) {
		                    var index = $(this).attr('id').split('-')[2];		                  
		                    $scope.totalHoverHighRelevanceCount=0;
		                    d3.selectAll('.chord-path-' + index).each(function(d){
		                    	if(imports[d.target.index].imports.length>highRelevanceThreshold){
		                    		$scope.totalHoverHighRelevanceCount = $scope.totalHoverHighRelevanceCount+1;
		                    	}
		                    })
		                    $timeout(function(){
		                    	$scope.totalHoverNodesCount = imports[index].imports.length;
		                        $('#counts-on-hover').css('display', 'inline-block');
		                    },1);
		                    
		                    $('.chord').css('opacity', '0.2');
		                    $('.chord-path-' + index).css('opacity', '1.0');
		
		                    var fillColor = $(this).css('fill');
		                    $('#text-index-'+ index).css('fill', fillColor);
		                    $('#counts-on-hover label').css('color', fillColor);
		                    $('#counts-on-hover span').css('color', fillColor);
		                })
		                .on('mouseout', function () {
		                    var index = $(this).attr('id').split('-')[2];
		                    $('.chord').css('opacity', '1.0');
		                    $('.node-text-custom').css('fill', '#4c626e');
		                    $('#counts-on-hover').css('display', 'none');
		                });
		
		        g.append("text")
		                .each(function (d) {
		                    d.angle = (d.startAngle + d.endAngle) / 2;
		                })
		                .attr('class', 'node-text-custom')
		                .attr("dy", ".35em")
		                .attr("transform", function (d) {
		                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
		                            + "translate(" + (innerRadius + 26) + ")"
		                            + (d.angle > Math.PI ? "rotate(180)" : "");
		                })
		                .attr("id", function (d) {
		                    return 'text-index-' + d.index;
		                })
		                .style('font-size', function(d){
		                	return imports[d.index].strength + 12;
		                })
		                .style('text-transform', 'capitalize')
		                .style('fill', '#4c626e')
		                .style("text-anchor", function (d) {
		                    return d.angle > Math.PI ? "end" : null;
		                })
		                .text(function (d) {
		                    return nameByIndex.get(d.index).split('flare.')[1];
		                });
		
		        svg.selectAll(".chord")
		                .data(chord.chords)
		                .enter().append("path")
		                .attr("class", function (d) {
		                    return "chord chord-path-" + d.source.index
		                })
		                .style("stroke", function (d) {
		                    return d3.rgb(neonColorList[d.source.index]).darker();
		                })
		                .style("fill", function (d) {
		                    return neonColorList[d.source.index];
		                })
		                .attr("d", d3.svg.chord().radius(innerRadius));
		    }
		//		});
		
		    d3.select(self.frameElement).style("height", outerRadius * 2 + "px");
		}