'use strict';
angular.module('ehubApp')
.controller('SocialLiveFeedContentController', socialLiveFeedContentController);
socialLiveFeedContentController.$inject = [
		'$scope',
		'$state',
		'$rootScope',
		'$stateParams',
		'EntityApiService',
		'$q',
		'EnrichSearchGraph',
		'WebSocketTwitterUrl',
		'$timeout'
	];
	function socialLiveFeedContentController(
			$scope, 
			$state, 
			$rootScope, 
			$stateParams,
			EntityApiService,
			$q,
			EnrichSearchGraph,
			WebSocketTwitterUrl,
			$timeout) {
	/*initialzing livefeed variables*/
    $scope.liveFeed = {
		nodeList: []
	};
    $scope.disableSearchButton = true;
    $('.custom-spinner').css('display', 'block');
	var nodes = [], links = [], tweetList = [], retweetList = [], mentionList = [], tagCloudNameList = [], 
		locationList = [], finalLocationList = [], locationNames = [], randomGeneratedId = Math.floor((Math.random() * 10) + 19999);
	$scope.entitySearchResult = {
		name: $stateParams.searchText,
		tweetCounts: 0,
		reTweetCounts: 0,
		mentionCounts: 0,
		selectedTab: 'graph',
		positivePercentage: Math.floor(Math.random() * (90 - 70 + 1)) + 70,
		negativePercentage: Math.floor(Math.random() * (10 - 1 + 1)) + 1,
		neutralPercentage: 0
	};
	$scope.entitySearchResult.neutralPercentage = 100 - ($scope.entitySearchResult.positivePercentage + $scope.entitySearchResult.negativePercentage);
	
	var socket = {};
	
	$('#entity-company-content').css('height', $(window).height() - (60 + 61));
	$('.live-feed-content-wrapper .live-feed-wrapper').css('height', $(window).height() - (60 + 61));
	$('#tweets .live-feed-wrapper').css('height', $(window).height() - (60 + 61 + $('.nav-tabs').outerHeight(true)));

	var worldChartOptions1 = {
	   container :"#live-feed-location-chart",
	   uri1 : "vendor/data/worldCountries.json",
	   uri2 :"vendor/data/worldMapData.json",
	   height: $(window).height() - (60 + 61 + $('.nav-tabs').outerHeight(true)),
	   width: ($(window).width() - 420),
	   markers: {},
	   mapscale:6
	};  

	var worldChartOptions2 = {
	   container :"#locationchart",
	   uri1 : "vendor/data/worldCountries.json",
	   uri2 :"vendor/data/worldMapData.json",
	   height: $(".entity-company-content-right").width()-200,
	   width: ($(".entity-company-content-right").width() -50),
	   markers: {},
	   mapscale:5,
	   mapscaleRight:1.5,
	   mapscaleTop:1.5
	};  
	 
	
	/*tag cloud variables initializations*/ 
    var tagCloudOptions = {
        header: "MY ENTITIES",
        container: "#tagcloud",
        height: $(".entity-company-content-right").width()-200,
        data: [],
        margin: {
        	bottom: 10,
        	top: 10,
        	right: 10,
        	left: 10
        }
    }

	/*
	  * @purpose: getWebSocketTwitterData function
	  * @created: 21 aug 2017
	  * @params: name(string)
	  * @returns: no
	  * @author: sandeep
	  */
	 function getWebSocketTwitterData(name){
		 var tweetObjectOneOrigin = {
			id: randomGeneratedId,
			name: $stateParams.searchText,
			screen_name: $stateParams.searchText,
			text: $stateParams.searchText,
			profile_image: '',
			createdOn: new Date(),
			type: 'main'
		 };		
		 nodes.push(tweetObjectOneOrigin);
		 socket = io.connect(WebSocketTwitterUrl, {'forceNew': true});
		
		 socket.on('message', function(response){
			$scope.disableSearchButton = false;
		    $('.custom-spinner').css('display', 'none');
			var isString = false;
			if(response.indexOf('{') === -1){
				isString = true;
			} else {
				response = JSON.parse(response);
				getSourceTargetData(response);
				if(locationList.length > 0){
			        angular.forEach(locationList, function(locationValue, locationKey) {
			        	if(locationNames.indexOf(locationValue.name) === -1) {
			        		locationNames.push(locationValue.name);
				        	finalLocationList.push(locationValue);
			        	}
			        });
			        worldChartOptions1.markers = finalLocationList;
			        worldChartOptions2.markers=finalLocationList;
			   	 	/*calling world map chart function*/
			        World(worldChartOptions1);
			        World(worldChartOptions2);
			        
				}
				if(tagCloudNameList.length > 0){
					tagCloudOptions.data = tagCloudNameList; 
				    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOptions); 
				}
			}
		});
		
		socket.on('connect', function() {
			socket.send(name);
		});

	    socket.on('disconnect', function() {
		});
	 }
	 
	 getWebSocketTwitterData($stateParams.searchText);
	 
	 
	 function getSourceTargetData(tweetData){
		 var linkDataOrigin = {
			source: null,
			target: null
		 };
		 if(tweetList.indexOf(tweetData.user.screen_name) === -1) {
			 var tweetObjectOne = {
				id: tweetData.id,
				name: tweetData.user.name,
				screen_name: tweetData.user.screen_name,
				text: tweetData.text,
				profile_image: tweetData.user.profile_image_url,
				createdOn: new Date(tweetData.user.created_at),
				likeCounts: tweetData.user.favourites_count || 0,
				retweetCounts: tweetData.user.retweet_count || 0,
				replyCounts: tweetData.user.reply_count || 0,
				type: 'tweet'
			};
			 tagCloudNameList.splice(0, 0, {
				 text: tweetData.user.name,
				 size: tweetData.user.favourites_count || 5 
			 });
			if(tagCloudNameList.length === 21) {
				tagCloudNameList.pop();
			} 
			 if(tweetData.user.location !== '' && tweetData.user.location) {
				 locationList.push({
     				name: tweetData.user.location,
    				long: '',
    				lat: '',
    				mark: 'assets/images/redpin.png'
    			});
			 }
			 nodes.push(tweetObjectOne);
			 $timeout(function(){
				 $scope.entitySearchResult.tweetCounts++;
				 $scope.liveFeed.nodeList.push(tweetObjectOne);
			 },0);
			 tweetList.push(tweetData.user.screen_name);
			 linkDataOrigin.source = randomGeneratedId;
			 linkDataOrigin.target = tweetData.id;
			 links.push(linkDataOrigin);
		 }
		 if(tweetData.entities['user_mentions'].length > 0){
			 angular.forEach(tweetData.entities['user_mentions'], function(mention, mentionKey){
				if(mentionList.indexOf(mention.screen_name) === -1) {
					var mentionObject = {
						id: 'mention-' + mention.id,
						name: mention.name,
						screen_name: mention.screen_name,
						type: 'mention'
					};
					 $timeout(function(){
						 $scope.entitySearchResult.mentionCounts++;
					 },0);
					nodes.push(mentionObject);
				    mentionList.push(mention.screen_name);
				}
				links.push({
					source: tweetData.id,
					target: 'mention-' + mention.id
				});
			 });
		 }
		 var linkData = {
			source: null,
			target: null
		 };
		 linkData.source = tweetData.id;
		 if(_.isObject(tweetData.retweeted_status)){
			 if(retweetList.indexOf(tweetData.retweeted_status.user.screen_name) === -1) {
				var tweetObject = {
					id: tweetData.retweeted_status.id,
					name: tweetData.retweeted_status.user.name,
					screen_name: tweetData.retweeted_status.user.screen_name,
					text: tweetData.retweeted_status.text,
					profile_image: tweetData.retweeted_status.user.profile_image_url,
					createdOn: new Date(tweetData.retweeted_status.user.created_at),
					likeCounts: tweetData.retweeted_status.user.favourites_count || 0,
					retweetCounts: tweetData.retweeted_status.user.retweet_count || 0,
					replyCounts: tweetData.retweeted_status.user.reply_count || 0,
					type: 'retweet'
				};
				tagCloudNameList.splice(0, 0, {
					 text: tweetData.retweeted_status.user.name,
					 size: tweetData.retweeted_status.user.favourites_count || 5 
				});
				if(tagCloudNameList.length === 21) {
					tagCloudNameList.pop();
				} 
				 if(tweetData.retweeted_status.user.location !== '' && tweetData.retweeted_status.user.location){
					 locationList.push({
	     				name: tweetData.retweeted_status.user.location,
	    				long: '',
	    				lat: '',
	    				mark: 'assets/images/redpin.png'
	    			 });
				 }
				nodes.push(tweetObject);
				 $timeout(function(){
					 $scope.entitySearchResult.reTweetCounts++;
					 $scope.liveFeed.nodeList.push(tweetObject);
				 },0);
				retweetList.push(tweetData.retweeted_status.user.screen_name);
			 }
			 linkData.target = tweetData.retweeted_status.id;
		 }
		 if(linkData.source && linkData.target)
			 links.push(linkData);
		 var twitterFinalData = {'links': links, 'nodes': nodes};
		 var twitterFinalDataNew = jQuery.extend(true, [], twitterFinalData);
		 twitterPlotNetworkChart(twitterFinalDataNew);
	 }
	 
	 /*
	  * @purpose: onClickliveFeedTabs activate tab
	  * @created: 04 sep 2017
	  * @params: tabName(string)
	  * @returns: no
	  * @author: sandeep
	  */
	 $scope.onClickliveFeedTabs = function(tabName){
		$scope.entitySearchResult.selectedTab = tabName;
		if(tabName === 'graph')
			$('#socialMedialLegend').css("display", "block");
		else 
			$('#socialMedialLegend').css("display", "none");
	 };	 
	 
 /*----------------------------Twitter Network Chart----------------------------------------*/

	 var tool_tip = $('body').append('<div class="CaseTimeLine_Chart_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

	 var legendtool = $('body').append('<div id="socialMedialLegend" class="legend"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
	 var width = $(window).width() - 325,
     height = $(window).height() - (60 + 61 + $('.nav-tabs').outerHeight(true));
	 	
	 	  var svg = d3.select("#twitter-network-chart").append("svg")
	 	  .attr("width", width)
	 	    .attr("height", height),
	       color = d3.scaleOrdinal(d3.schemeCategory10);
	 /*build the arrow. */
	 svg.append("svg:defs").selectAll("marker")
	       .data(["end"])      /* Different link/path types can be defined here */
	       .enter().append("svg:marker")    /* This section adds in the arrows */
	       .attr("id", String)
	       .attr("viewBox", "0 -5 10 10")
	       .attr("refX", 15)
	       .attr("refY", -1.5)
	       .attr("markerWidth", 6)
	       .attr("markerHeight", 6)
	       .attr("orient", "auto")
	       .append("svg:path")
	       .attr("d", "M0,-5L10,0L0,5")
	       .style("fill", "#666");

	 var a = {id: "Sarah"},
	       b = {id: "Alice"},
	       c = {id: "Eveie"},
	       d = {id: "Peter"},
	       nodesnew = [],
	        linksnew = [];
	 var simulation = d3.forceSimulation(nodesnew)
	       .force("charge", d3.forceManyBody().strength(-300))
	       .force("link", d3.forceLink(linksnew).distance(60))
	       .force("x", d3.forceX())
	       .force("y", d3.forceY())
	       .alphaTarget(1)
	       .on("tick", ticked);

	 var g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
	       link = g.append("g").attr("stroke", "#000").attr("stroke-width", 1.5).selectAll(".link"),
	       node = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");



	 function twitterPlotNetworkChart(data) {
	 	nodesnew = data.nodes;
	 	linksnew=[];
	 	
		data.links.forEach(function(d) {	
	       linksnew.push({
	           "source":nodesnew[nodesnew.findIndex( function (el) { return el.id == d.source; })],
	           "target":nodesnew[nodesnew.findIndex( function (el) { return el.id == d.target})]
	       });
		});
		restart();
	 }
	 restart();


	 var colornew;
	 var radius;
	 function restart() {
		 legend(nodesnew);
	   /* Apply the general update pattern to the nodes. */
	   node = node.data(nodesnew, function (d) {
	       return d.id;
	   });
	   node.exit().remove();
	   node = node.enter().append("circle").attr("fill", function (d) {
	      if(d.type=="main"){
			   colornew="#000080"/*blue*/
				}
			else if(d.type=="tweet"){
				colornew="#FFD700"/*yellow*/
			}
			else if(d.type=="mention"){
				colornew="#800000"
			}
			else if(d.type=="retweet"){
				colornew="#008000"/*green*/
			}
	       return colornew;
	   }).attr("r",  function (d) {
		      if(d.type=="main"){
				   radius="10"/*blue*/
					}
				else {
					radius="6"/*green*/
				}
		       return radius;
		   })
		   .on("mouseover", function (d) {
			   var appendHTMLTooltip = '';
			   if(d.type === 'tweet' || d.type === 'retweet')
				   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p><p><i class="fa fa-retweat icon"></i>' +d.text + '</p><p class="progress-text"><i class="fa fa-wechat icon"></i><span>'+d.replyCounts+'</span><img  class="img-responsive icon" src="assets/images/icon/arrow-right-square.png" alt="icon" /><span>'+d.retweetCounts+'</span><i class="fa fa-heart icon"></i><span>'+d.likeCounts+'</span></p></div>';
			   else
				   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="assets/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p></div>';
		      $(".CaseTimeLine_Chart_tooltip").html(appendHTMLTooltip);
		      return $(".CaseTimeLine_Chart_tooltip").css("display", "block");
		   }).on("mousemove", function (event) {
			  var value= $(this).offset();
			 var top=value.top
			 var left=value.left+20
		      $(".CaseTimeLine_Chart_tooltip").css("top", top + "px")
		      return  $(".CaseTimeLine_Chart_tooltip").css("left", left+ "px");

		   }).on("mouseout", function () {
		     /* $(this).css("opacity", 0.4); */
		      /*hide tool-tip*/
		      return $(".CaseTimeLine_Chart_tooltip").css("display", "none");
		   }).merge(node);

	   /* Apply the general update pattern to the links.*/
	   link = link.data(linksnew, function (d) {
	       return d.source.id + "-" + d.target.id;
	   });
	   link.exit().remove();
	   link = link.enter().append("line").merge(link);

	   /* Update and restart the simulation.*/
	   simulation.nodes(nodesnew);
	   simulation.force("link").links(linksnew);
	   simulation.alpha(1).restart();
	 }
	 

	 function legend(nodesnew)
	 {
		 $(".legend").empty()
		 var type=[];
		 nodesnew.forEach(function(d)
		 {
			 type.push(d.type) 
		 })
		 var outputArray = [];
		 for (var i = 0; i < type.length; i++)
		    {
		        if ((jQuery.inArray(type[i], outputArray)) == -1)
		        {
		            outputArray.push(type[i]);
		        }
		    }
		   var legendcolor;
		 outputArray.forEach(function(d){
			 if(d=="main"){
				 d="Search query"
					 legendcolor="#000080"
					}
				else if(d=="tweet"){
					legendcolor="#FFD700"
				}
				else if(d=="mention"){
					legendcolor="#800000"
				}
				else if(d=="retweet"){
					legendcolor="#008000"
				}
				else {
					legendcolor="#000"
				}
			 $(".legend").append('<div style="display: inline-block;vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;text-transform: capitalize;margin: 0 10px 0 0">'+d+'</span><span style="margin: 0 10px 0 0;vertical-align: middle;border-radius: 50%;display: inline-block;width: 15px;height: 15px;color: #435561;font-size: 12px;line-height: 20px;text-align: center;padding: 1px 4px;border: 2px solid #435561;background-color: '+legendcolor+'"></span></div>')
		 })
		 return $(".legend").css("display", "none");
	 }

	 function ticked() {
	   node.attr("cx", function (d) {
	       return d.x;
	   })
	           .attr("cy", function (d) {
	               return d.y;
	           })

	   link.attr("x1", function (d) {
	       return d.source.x;
	   })
	           .attr("y1", function (d) {
	               return d.source.y;
	           })
	           .attr("x2", function (d) {
	               return d.target.x;
	           })
	           .attr("y2", function (d) {
	               return d.target.y;
	           }).attr("marker-end", "url(#end)");
	 }
/*--------------------force collapse chart end-------------------------------------------------------------------------------------*/
 
/*-------------------------world location chart----------------------------------*/
 	 
	 function World(options) {
	     var worldmapHeader = options.header;
	     var worldmapId = options.container;
	     var worldmapuri1 = options.uri1;
	     var worldmapuri2 = options.uri2;
	     loadWorldChart(options);
	     /*$(window).on("resize", function () {
	         if ($(options.container).find("svg").length != 0) {
	             $(options.container).empty();
	             EnrichSearchGraph.plotWorldLocationChart(options);
	         }
	     });*/

	     /**
	      *Function to load data to plot WorldMap chart 
	      */
	
	     function loadWorldChart(options) {
	         var WorldData = [];
	         var populationData = [];
	         var flag_for_req = 0;
	
	         d3.json(options.uri1, function (error, data) {
	
	             flag_for_req++;
	             WorldData = data;
	             options.data=[WorldData,populationData];
	             if (flag_for_req == 2) {
/*	            	 var exampleChart = new WorldChart(options); */
	            	 EnrichSearchGraph.plotWorldLocationChart(options);	
	             }
	         });
	         d3.json(options.uri2, function (error, data) {	
	             flag_for_req++;
	             populationData = handleMapData(data.locations);
	             options.data=[WorldData,populationData];
	             if (flag_for_req == 2) {
/*	            	 var exampleChart = new WorldChart(options); */
	            	 EnrichSearchGraph.plotWorldLocationChart(options);
	             }
	         });
	     }
	
	     /**
	      *Function to handle data according to format of WorldMap Chart library
	      */	
	     function handleMapData(data) {
	         var finalData = [];
	         $.each(data, function (i, d) {
	             finalData.push({
	                 name: i,
	                 population: d
	             });
	         });
	         return finalData;
	     }
	 }
	/*-------------------------world location chart END----------------------------------*/
	/*-------------------------Area chart STARTS-----------------------------------------*/
	 var Areadata=[
	     {"year" : "01-jan-05", "value": 775900},
	     {"year" : "01-jan-06", "value": 774500},
	     {"year" : "01-jan-09", "value": 776000},
	     {"year" : "01-jan-11", "value": 774100},
	     {"year" : "01-jan-12", "value": 776700},
	     {"year" : "01-jan-13", "value": 774100},
	     {"year" : "01-jan-14", "value": 779200},
	     {"year" : "01-jan-15", "value": 777700},
	     {"year" : "01-jan-16", "value": 775400},
	     {"year" : "01-jan-17", "value": 773800},
	     {"year" : "01-jan-18", "value": 772900},
	     {"year" : "01-jan-19", "value": 773300},
	     {"year" : "01-jan-20", "value": 779300},
	     {"year" : "01-jan-21", "value": 774900},
	     {"year" : "01-jan-22", "value": 770900},
	     {"year" : "01-jan-23", "value": 774900},
	     {"year" : "01-jan-24", "value": 779900}  
	 ]
 	 var options={
 	    container:"#areachart",
 	    height:$(".entity-company-content-right").width()-100,
 	    width:$(".entity-company-content-right").width()-20,
 	   
 	    gridy:"true",
 	    uri :"data/data.json" ,
 	    axisY:"true",
 	    axisX:"true",
 	    data:Areadata
 	 }
	 EnrichSearchGraph.plotAreaLineChartForLiveFeed(options);	 

	 /*-----------------------AREA CHART ENDS--------------------------------------------------------------------------------------------*/

	 $(window).focus(function () { 
	 	$(window).click();
	 });
	 
};


'use strict';
angular.module('ehubApp')
.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});