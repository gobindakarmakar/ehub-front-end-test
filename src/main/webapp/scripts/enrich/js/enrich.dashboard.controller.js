'use strict';
elementApp
	.controller('EnrichDashboardController', enrichDashboardController);
enrichDashboardController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'$uibModal',
	'$timeout',
	'HostPathService',
	'OpenSourceCategoryConstant',
	'SourceCategoryConstant',
	'FetcherCategoryList',
	'EnrichApiService',
	'FetcherImageList',
	'DomainCategoryConstant',
	'EntityApiService'
];

function enrichDashboardController(
	$scope,
	$rootScope,
	$state,
	$uibModal,
	$timeout,
	HostPathService,
	OpenSourceCategoryConstant,
	SourceCategoryConstant,
	FetcherCategoryList,
	EnrichApiService,
	FetcherImageList,
	DomainCategoryConstant,
	EntityApiService) {
	/*initialize scope variables*/
	$scope.enrichDashboard = {
		contentMenu: 'enrichMainMenu',
		contentMenu0: 'enrichSubMenu0',
		contentMenu1: 'enrichSubMenu1',
		contentMenu2: 'enrichSubMenu2',
		contentMenu3: 'enrichSubMenu3',
		contentMenu4: 'enrichSubMenu4',
		contentMenu5: 'enrichSubMenu5'
	};
	$scope.newAdvancedSearch = {
		postAdvancedSearch: postAdvancedSearch
	};
	$scope.filteredDataLength = [];
	$scope.fileterdDisplayedData2 = [];
	/*token initialization*/
	$scope.token = $rootScope.ehubObject.token;
	$scope.rootPath = HostPathService.getHostPath();
	/*setting height of gridster based on window height*/
	var windowHeight = $(window).height();
	var headerHeight = $('.navbar-fixed-top').height() || 72,
		utilitiesPanel = $('.utilities-panel').height() || 55;
	var gridsterHeight = windowHeight - (headerHeight + utilitiesPanel);
	$('#nodeSearchContentParent').css('height', gridsterHeight - ($('#panelentities').height() + 10));
	$timeout(function () {
		$('.save-search-item .panel-body').css('height', $('.save-search-item').height() - ($('.save-search-item .title').outerHeight(true) + $('.save-search-item .panel-heading').outerHeight(true)));
		$('.recent-search-wrapper .panel-body').css('height', $('.recent-search-wrapper').height() - $('.recent-search-wrapper .panel-heading').outerHeight(true));
	}, 0);
	/*        setting height for search result tab*/
	var searchHeaderHeight = $('#criteria-searches').height() || 70;
	var searchResultBoxHeight = gridsterHeight - (searchHeaderHeight + 75);
	$('#widgetFilter').css('height', searchResultBoxHeight);
	$('.company-person-collapse-result').css('height', searchResultBoxHeight);
	// $('body').css('overflow', 'hidden');
	$scope.gridsterConfiguration = {
		isMobile: false,
		pushing: true,
		floating: true,
		swapping: true,
		rowHeight: (gridsterHeight / 2) - 10,
		maxSizeY: 2,
		resizable: {
			enabled: true
		},
		draggable: {
			enabled: true,
			handle: '.widget-header'
		},
		margin: [10, 10]
	};
	/*
	 * var isShowBstNamedEntity = false; if (!isShowBstNamedEntity) {
	 * removeIFrame(); } function showBstNamedEntity() {
	 * $("#bst-named-entity").show(); }
	 */
	$scope.hasDomain = true;
	$scope.hasDomainDetails = false;
	$scope.hasSource = true;
	$scope.hasSourceDetails = false;
	/*$scope.hasOpenSource = true;
	$scope.hasOpenSourceDetails = false;*/
	$scope.hasPeople = false;
	$scope.hasPeopleDetails = false;
	$scope.hasCompany = false;
	$scope.hasCompanyAdded = false;
	$scope.hasLocation = true;
	$scope.hasLocationDetails = false;
	$scope.hasRules = true;
	$scope.hasRulesAdded = false;
	$scope.hasCase = true;
	$scope.hasCaseAdded = false;
	$scope.hasAccount = true;
	$scope.hasAccountAdded = false;
	$scope.hasIdentifiers = true;
	$scope.hasIdentifiersAdded = false;
	$scope.showDomainUpdate = false;
	$scope.showSourceUpdate = false;
	/*        $scope.showOpenSourceUpdate = false;*/
	$scope.showPeopleUpdate = false;
	$scope.showCompanyUpdate = false;
	$scope.showLocationUpdate = false;
	$scope.showRulesUpdate = false;
	$scope.showCaseUpdate = false;
	$scope.showAccountUpdate = false;
	$scope.showIdentifiersUpdate = false;
	/*        $scope.openSourceCategory = '';
	        $scope.isSourceTypeAsOSINIT = false;*/
	$scope.showPeopleErrorMsg = '';
	$scope.showCompanyErrorMsg = '';
	$scope.openSourceObject = OpenSourceCategoryConstant;
	$scope.sourceCategoryConstant = SourceCategoryConstant;
	$scope.domainCategoryList = DomainCategoryConstant;
	$scope.jurisdictionList =[];
	$scope.selectedJurisdiction = '';
	$scope.domainSelected = 'General';
	$scope.jurisdictionSelected = '';
	$scope.isSearchStarted = false;
	$scope.entityType = 'Select Type';
	$scope.keywordData = '';
	$rootScope.updatePeopleFullName = "";
	$rootScope.updateCompanyName = "";
	$scope.caseName = "";
	$scope.locationName = "";
	$scope.recentsearch_keyword = [];
	$scope.savesearch_data = {};
	$scope.onChangeEntityType = function (type) {
		$scope.entityType = type;
		if (type === 'Person') {
			if (!$scope.hasCompanyDetails) {
				$scope.showPeopleUpdate = true;
			} else {
				$scope.showConfirmChecking = true;
			}
		} else if (type === 'Company') {
			if (!$scope.hasPeopleDetails) {
				$scope.showCompanyUpdate = true;
			} else {
				$scope.showConfirmChecking = true;
			}
		}
	};
	$scope.onChangeDomainType = function (type) {
		$scope.domainSelected = type;
	};
	$scope.onChangeJurisdictionType = function (type, key) {
		$scope.SelectedFlagSource =key.flag;
		$scope.jurisdictionSelected = type;
		$scope.selectedJurisdiction = type;
		$scope.jurisdictionKey = key;
	};
	$scope.person = {
		fullName: '',
		firstName: '',
		lastName: '',
		email: '',
		phone: ''
		/*relatedCompanies: [{
		        name: ''
		    }]*/
	};
	$scope.isNamesRequired = false;
	$scope.addRelatedCompany = function () {
		$scope.person.relatedCompanies.push({
			name: ''
		});
	};
	$scope.removeRelatedCompany = function (index) {
		$scope.person.relatedCompanies.splice(index, 1);
	};
	$scope.onExpandNames = function (isRequired) {
		if (isRequired) {
			$scope.person.fullName = '';
			$scope.isNamesRequired = true;
		} else {
			$scope.person.firstName = '';
			$scope.person.lastName = '';
			$scope.isNamesRequired = false;
		}
	};
	$scope.company = {
		fullName: '',
		founded: '',
		industry: '',
		headquarter: '',
		phone: '',
		website: ''
	};
	$scope.location = {
		country: '',
		city: '',
		street: ''
	};
	$scope.cases = {
		number: '',
		name: ''
	};
	$scope.ruleText = '';
	$scope.accountText = '';
	$scope.personalIdentifier = {
		number: '',
		email: '',
		socialAccount: '',
		website: ''
	};
	var filterArray = [];
	$scope.entitiesSearchResult = [];
	$scope.nodeSearchDatas = [];
	$scope.enrichObject = {
		loadingText:false
	};
	$scope.selectedOption = '';
	$scope.sourceParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showSourceUpdate = true;
		}
	};
	$scope.sourceDetailsAddedUpdate = function () {
		$scope.hasSourceDetails = true;
	};
	$scope.sourceDetailsRemoveUpdate = function () {
		$scope.hasSourceDetails = false;
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			if (source.value) {
				source.value = false;
			}
			angular.forEach(source.categories, function (category) {
				if (category.value) {
					//isSelected = false;
				}
			});
		});
	};
	$scope.peopleParamUpdate = function () {
		$scope.showConfirmChecking = false;
		$timeout(function () {
			$scope.showPeopleUpdate = true;
		}, 500);
	};
	$scope.peopleDetailsAddedUpdate = function () {
		$scope.showPeopleUpdate = false;
	};
	$scope.peopleDetailsRemovedUpdate = function (isRemoveall) {
		$scope.person = {
			fullName: '',
			firstName: '',
			lastName: '',
			email: '',
			phone: ''
			/*relatedCompanies: [{
			        name: ''
			    }]*/
		};
		$scope.entityType = '';
		$scope.isNamesRequired = false;
		$scope.hasPeopleDetails = false;
		if ($scope.hasCompanyDetails) {
			$scope.entityType = 'Company';
		} else {
			$scope.entityType = 'Select Type';
		}
		if (isRemoveall) {
			$scope.isSourceTypeAsOSINIT = false;
			$scope.locationRemoveUpdate();
			$scope.accountRemoveUpdate();
			$scope.rulesRemoveUpdate();
			$scope.caseRemoveUpdate();
			$scope.identifiersRemoveUpdate();
			//                $scope.openSourceDetailsRemoveUpdate();
			$scope.sourceDetailsRemoveUpdate();
		}
	};
	$scope.companyParamUpdate = function () {
		$scope.showConfirmChecking = false;
		$timeout(function () {
			$scope.showCompanyUpdate = true;
		}, 500);
	};
	$scope.companyAddedUpdate = function () {
		$scope.showCompanyUpdate = false;
	};
	$scope.companyRemoveUpdate = function (isRemoveall) {
		$scope.hasCompanyDetails = false;
		$scope.company = {
			fullName: '',
			founded: '',
			industry: '',
			headquarter: '',
			phone: '',
			website: ''
		};
		if ($scope.hasPeopleDetails) {
			$scope.entityType = 'Person';
		} else {
			$scope.entityType = 'Select Type';
		}
		if (isRemoveall) {
			$scope.isSourceTypeAsOSINIT = false;
			$scope.locationRemoveUpdate();
			$scope.accountRemoveUpdate();
			$scope.rulesRemoveUpdate();
			$scope.caseRemoveUpdate();
			$scope.identifiersRemoveUpdate();
			/*        $scope.openSourceDetailsRemoveUpdate();*/
			$scope.sourceDetailsRemoveUpdate();
		}
	};
	$scope.locationParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showLocationUpdate = !$scope.showLocationUpdate;
		}
	};
	$scope.locationAddedUpdate = function () {
		$scope.hasLocationAdded = true;
	};
	$scope.locationRemoveUpdate = function () {
		$scope.hasLocationAdded = false;
		$scope.location = {
			country: '',
			city: '',
			street: ''
		};
	};
	$scope.rulesParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showRulesUpdate = !$scope.showRulesUpdate;
		}
	};
	$scope.rulesAddedUpdate = function () {
		$scope.hasRulesAdded = true;
		$scope.showRulesUpdate = false;
	};
	$scope.rulesRemoveUpdate = function () {
		$scope.hasRulesAdded = false;
		$scope.ruleText = '';
		$('#rulesname').val('');
	};
	$scope.caseParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showCaseUpdate = !$scope.showCaseUpdate;
		}
	};
	$scope.caseAddedUpdate = function () {
		$scope.hasCaseAdded = true;
	};
	$scope.caseRemoveUpdate = function () {
		$scope.hasCaseAdded = false;
		$scope.cases = {
			number: '',
			name: ''
		};
	};
	$scope.accountParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showAccountUpdate = !$scope.showAccountUpdate;
		}
	};
	$scope.accountAddedUpdate = function () {
		$scope.hasAccountAdded = true;
	};
	$scope.accountRemoveUpdate = function () {
		$scope.hasAccountAdded = false;
		$scope.accountText = '';
		$('#accountname').val('');
	};
	$scope.identifiersParamUpdate = function () {
		if ($scope.hasPeopleDetails || $scope.hasCompanyDetails) {
			$scope.showIdentifiersUpdate = !$scope.showIdentifiersUpdate;
		}
	};
	$scope.identifiersAddedUpdate = function () {
		$scope.hasIdentifiersAdded = true;
	};
	$scope.identifiersRemoveUpdate = function () {
		$scope.hasIdentifiersAdded = false;
		$scope.personalIdentifier = {
			number: '',
			email: '',
			socialAccount: '',
			website: ''
		};
	};
	/*
	 * @purpose: checks updated node and shows
	 * @created: 01 aug 2017
	 * @params: type(string)
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.updateNodeData = function (type) {
		$timeout(function () {
			if (type === 'person') {
				$scope.peopleParamUpdate();
			}
			if (type === 'company') {
				$scope.companyParamUpdate();
			}
			if (type === 'source') {
				$scope.sourceParamUpdate();
			}
			/*                if (type === 'open source')
			                    $scope.openSourceParamUpdate();*/
			if (type === 'location') {
				$scope.locationParamUpdate();
			}
			if (type === 'rules') {
				$scope.rulesParamUpdate();
			}
			if (type === 'case') {
				$scope.caseParamUpdate();
			}
			if (type === 'account') {
				$scope.accountParamUpdate();
			}
			if (type === 'personal identifier') {
				$scope.identifiersParamUpdate();
			}
		}, 10);
	};
	/*
	 * @purpose: checks removed nodes and remove
	 * @created: 01 aug 2017
	 * @params: index(number)
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.removeUpdateNodeData = function (index) {
		if ($scope.nodeSearchDatas[index].name === 'person') {
			$scope.nodeSearchDatas = [];
			$scope.peopleDetailsRemovedUpdate(true);
		} else if ($scope.nodeSearchDatas[index].name === 'company') {
			$scope.nodeSearchDatas = [];
			$scope.companyRemoveUpdate(true);
		} else if ($scope.nodeSearchDatas[index].name === 'source') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.sourceDetailsRemoveUpdate();
			/*} else if ($scope.nodeSearchDatas[index].name === 'open source') {
			    $scope.nodeSearchDatas.splice(index, 1);
			    $scope.openSourceDetailsRemoveUpdate();*/
		} else if ($scope.nodeSearchDatas[index].name === 'location') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.locationRemoveUpdate();
		} else if ($scope.nodeSearchDatas[index].name === 'rules') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.rulesRemoveUpdate();
		} else if ($scope.nodeSearchDatas[index].name === 'case') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.caseRemoveUpdate();
		} else if ($scope.nodeSearchDatas[index].name === 'account') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.accountRemoveUpdate();
		} else if ($scope.nodeSearchDatas[index].name === 'personal identifier') {
			$scope.nodeSearchDatas.splice(index, 1);
			$scope.identifiersRemoveUpdate();
		}
	};
	/*
	 * @purpose: showBstNamedEntity function
	 * @created: 26 sep 2017
	 * @params: data(object), index(number)
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.showBstNamedEntity = function (data) {
		var details = (data["bst:description"]).replace(/,\s*$/, "");
		var openEntityVisualiserModal = $uibModal.open({
			templateUrl: 'scripts/common/modal/views/entity.visualiser.modal.html',
			controller: 'EntityVisualiserController',
			size: 'lg',
			windowClass: 'entity-visualiser-modal update-entities-modal',
			resolve: {
				getDetails: function () {
					return details;
				},
				getTitle: function () {
					return data.title;
				},
				getUrl: function () {
					return data.source;
				},
				getToken: function () {
					return $scope.token;
				}
			}
		});
		openEntityVisualiserModal.result.then(function () {}, function () {});
	};
	$scope.updatePeopleParam = function () {
		if (($scope.person.fullName !== '' && !$scope.isNamesRequired) || ($scope.person.firstName !== '' && $scope.person.lastName !== '' && $scope.isNamesRequired)) {
			$scope.showPeopleErrorMsg = '';
			var index = getIndexOfCategory('company');
			if (index === -1) {
				index = getIndexOfCategory('person');
			} else {
				index = '';
			}
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'person',
					content: []
				});
				index = $scope.nodeSearchDatas.length - 1;
			} else {
				$scope.nodeSearchDatas[index].name = 'person';
				$scope.nodeSearchDatas[index].content = [];
			}
			$scope.person.fullName !== '' ? $scope.nodeSearchDatas[index].content.push($scope.person.fullName) : '';/* jshint ignore:line */
			$scope.person.firstName !== '' ? $scope.nodeSearchDatas[index].content.push($scope.person.firstName) : '';/* jshint ignore:line */
			$scope.person.lastName !== '' ? $scope.nodeSearchDatas[index].content.push($scope.person.lastName) : '';/* jshint ignore:line */
			$scope.person.phone !== '' ? $scope.nodeSearchDatas[index].content.push($scope.person.phone) : '';/* jshint ignore:line */
			$scope.person.email !== '' ? $scope.nodeSearchDatas[index].content.push($scope.person.email) : '';/* jshint ignore:line */
			//                        $scope.nodeSearchDatas[index].content.push(relatedCompany.trim());
			$scope.hasPeopleDetails = true;
			$scope.selectedOption = 'Person';
			$scope.hasCompanyDetails = false;
			$scope.companyRemoveUpdate();
			$scope.peopleDetailsAddedUpdate();
		} else {
			$scope.showPeopleErrorMsg = 'Please fill the required fields!';
			$timeout(function () {
				$scope.showPeopleErrorMsg = '';
			}, 5000);
		}
	};
	$scope.updateCompanyParam = function () {
		if ($scope.company.fullName !== '' && $scope.selectedJurisdiction !== '') {
			$scope.showCompanyErrorMsg = '';
			var index = getIndexOfCategory('person');
			index === -1 ? index = getIndexOfCategory('company') : '';/* jshint ignore:line */
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'company',
					content: []
				});
				index = $scope.nodeSearchDatas.length - 1;
			} else {
				$scope.nodeSearchDatas[index].name = 'company';
				$scope.nodeSearchDatas[index].content = [];
			}
			$scope.company.fullName !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.fullName) : '';/* jshint ignore:line */
			$scope.company.founded !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.founded) : '';/* jshint ignore:line */
			$scope.company.headquarter !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.headquarter) : '';/* jshint ignore:line */
			$scope.company.phone !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.phone) : '';/* jshint ignore:line */
			$scope.company.website !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.website) : '';/* jshint ignore:line */
			$scope.company.industry !== '' ? $scope.nodeSearchDatas[index].content.push($scope.company.industry) : '';/* jshint ignore:line */
			$scope.hasPeopleDetails = false;
			$scope.hasCompanyDetails = true;
			$scope.selectedOption = 'Company';
			$scope.peopleDetailsRemovedUpdate();
			$scope.companyAddedUpdate();
		} else {
			$scope.showCompanyErrorMsg = 'Please Provide Mandatory Fields!';
			$timeout(function () {
				$scope.showCompanyErrorMsg = '';
			}, 5000);
		}
	};
	$scope.updateSourceParam = function () {
		var sourcename = [];
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			var isSelected = false;
			angular.forEach(source.categories, function (category) {
				if (category.value) {
					isSelected = true;
				}
			});
			if (isSelected) {
				sourcename.push(source.key);
			}
		});
		if (sourcename.length > 0) {
			var index = getIndexOfCategory('source');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'source',
					content: sourcename
				});
			} else {
				$scope.nodeSearchDatas[index].content = sourcename;
			}
			$scope.sourceDetailsAddedUpdate();
		}
	};
	$scope.updateLocationParam = function () {
		var locationDetails = [];
		$scope.location.country !== '' ? locationDetails.push($scope.location.country) : '';/* jshint ignore:line */
		$scope.location.city !== '' ? locationDetails.push($scope.location.city) : '';/* jshint ignore:line */
		$scope.location.street !== '' ? locationDetails.push($scope.location.street) : '';/* jshint ignore:line */
		if (locationDetails.length > 0) {
			var index = getIndexOfCategory('location');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'location',
					content: locationDetails
				});
			} else {
				$scope.nodeSearchDatas[index].content = locationDetails;
			}
			$scope.locationAddedUpdate();
		}
	};
	$scope.updateRulesParam = function (ruleText) {
		var rulesname = [];
		if ($.trim(ruleText) !== "") {
			rulesname.push(ruleText);
			var index = getIndexOfCategory('rules');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'rules',
					content: rulesname
				});
			} else {
				$scope.nodeSearchDatas[index].content = rulesname;
			}
			$scope.rulesAddedUpdate();
		}
	};
	$scope.updateCaseParam = function () {
		var caseDetails = [];
		$scope.cases.name !== '' ? caseDetails.push($scope.cases.name) : '';/* jshint ignore:line */
		$scope.cases.number !== '' ? caseDetails.push($scope.cases.number) : '';/* jshint ignore:line */
		if (caseDetails.length > 0) {
			var index = getIndexOfCategory('case');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'case',
					content: caseDetails
				});
			} else {
				$scope.nodeSearchDatas[index].content = caseDetails;
			}
			$scope.caseAddedUpdate();
		}
	};
	$scope.updateAccountParam = function (accountText) {
		var accountname = [];
		if ($.trim(accountText) !== "") {
			accountname.push($.trim(accountText));
			var index = getIndexOfCategory('account');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'account',
					content: accountname
				});
			} else {
				$scope.nodeSearchDatas[index].content = accountname;
			}
			$scope.accountAddedUpdate();
		}
	};
	$scope.updateIdentifiersParam = function () {
		var identifiersDetails = [];
		$scope.personalIdentifier.number !== '' ? identifiersDetails.push($scope.personalIdentifier.number) : '';/* jshint ignore:line */
		$scope.personalIdentifier.email !== '' ? identifiersDetails.push($scope.personalIdentifier.email) : '';/* jshint ignore:line */
		$scope.personalIdentifier.socialAccount !== '' ? identifiersDetails.push($scope.personalIdentifier.socialAccount) : '';/* jshint ignore:line */
		$scope.personalIdentifier.website !== '' ? identifiersDetails.push($scope.personalIdentifier.website) : '';/* jshint ignore:line */
		if (identifiersDetails.length > 0) {
			var index = getIndexOfCategory('personal identifier');
			if (index === -1) {
				$scope.nodeSearchDatas.push({
					name: 'personal identifier',
					content: identifiersDetails
				});
			} else {
				$scope.nodeSearchDatas[index].content = identifiersDetails;
			}
			$scope.identifiersAddedUpdate();
		}
	};
	/*
	 * @purpose: getIndexOfCategory function @created: 1 aug 2017
	 * @returns: no @params: type(string) @author: sandeep
	 */
	function getIndexOfCategory(type) {
		var index = -1;
		angular.forEach($scope.nodeSearchDatas, function (node, key) {
			if (node.name === type) {
				index = key;
			}
		});
		return index;
	}
	/*
	 * @purpose: close modal on click cancel button @created: 14 jul 2017
	 * @returns: no @params: no @author: sandeep
	 */
	$scope.close = function (modalType) {
		if (modalType === 'source') {
			$scope.showSourceUpdate = false;
			$scope.selectedSourceName = '';
			$scope.steps = 1; $scope.categoryAvailabilityCount = 0;
			$scope.categorySelectedCount = 0; 
			$scope.totalCategorySelectedCount = 0;
			$scope.sourceSelectedIndex = -1;
			$scope.onDeselectCategoryAll();
		}
		if (modalType === 'person') {
			$scope.showPeopleUpdate = false;
			if (!$scope.hasPeopleDetails){
				$('#source-toggle-hl-1').prop('checked', false);
			}
		}
		if (modalType === 'company') {
			$scope.showCompanyUpdate = false;
			if (!$scope.hasCompanyDetails){
				$('#source-toggle-hl-2').prop('checked', false);
			}
		}
		if (modalType === 'location') {
			$scope.showLocationUpdate = false;
		}
		if (modalType === 'rule') {
			$scope.showRulesUpdate = false;
		}
		if (modalType === 'case') {
			$scope.showCaseUpdate = false;
		}
		if (modalType === 'account') {
			$scope.showAccountUpdate = false;
		}
		if (modalType === 'identifiers') {
			$scope.showIdentifiersUpdate = false;
		}
		if (modalType === 'unchecking') {
			$scope.showConfirmChecking = false;
			if ($scope.selectedOption === 'Person') {
				$('#source-toggle-hl-2').prop('checked', false);
			} else {
				$('#source-toggle-hl-1').prop('checked', false);
			}
		}
	};
	$scope.cancelSearchCriteria = function () {
		$scope.isSearchStarted = false;
		$scope.peopleDetailsRemovedUpdate();
		$scope.companyRemoveUpdate();
		$scope.cancelFilters();
		$scope.nodeSearchDatas = [];
		$scope.entitiesSearchResolveResult = [];
		$scope.filteredDataLength = [];
		$scope.fileterdDisplayedData2 = [];
		$scope.entitiesFilteredResult = [];
		$scope.keywordData = '';
		$scope.close('source');
	};
	$scope.editSearchCriteria = function () {
		$scope.isSearchStarted = false;
		$scope.isSearchFilteredStarted = false;
	};
	var searchTexts = '',
		paramsObject = {};
	$scope.isSearchResultNoData = false;
	$scope.totalSearchResultCount = 0;
	$scope.companyFilterList = [];
	$scope.personFilterList = [];
	/*
	 * @purpose: delaySearchutility function
	 * @created: 26 sep 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.delaySearchutility = function (recent_fav_search) {
		$scope.disableSearchButton = true;
		$scope.entitiesSearchResolveResult = [];
		$scope.entitiesFilteredResult = [];//Empty the filtered data
		$scope.entitiesSearchZeroIndexResult = [];
		$scope.entitiesSearchNonZeroIndexResult = [];
		$scope.companyFilterList = [];
		$scope.personFilterList = [];
		$scope.totalSearchResultCount = 0;
		$scope.token = $rootScope.ehubObject.token;
		searchTexts = '';
		//	var fetcherCategoryList = FetcherCategoryList;
		/*, '26', '27', '28', '29' - ID not working 24 is removed as not in new list replace by 2006*/
		/* var fetcherIds = ['1008', '1006', '1021', '8', '3', '1', '5', '6', '7', '20', '4', '1005', '1002', '1003', '1004', '1007', '1009', '2',
				'2001', '2002', '2003', '2006', '2005', '3003', '4001', '7001'
			],
			sourceTypeList = [],
			selectedDomain = $scope.domainSelected,
			categpryIds = [],
			domainFetchers = [];
		searchTexts = '', paramsObject = {};
		angular.forEach($scope.sourceCategoryConstant, function (source, sourcekey) {
			if (source.value)
				sourceTypeList.push(source.key);
		});
		angular.forEach(fetcherCategoryList, function (fetcherObject, fetcherKey) {
			angular.forEach(sourceTypeList, function (value, key) {
				if (fetcherObject.sourceTypes.indexOf(value) !== -1) {
					categpryIds.indexOf(fetcherObject.fetcher_id) === -1 ? categpryIds.push(fetcherObject.fetcher_id) : '';
				}
			});
			if (fetcherObject.domainTypes.indexOf(selectedDomain) !== -1) {
				domainFetchers.indexOf(fetcherObject.fetcher_id) === -1 ? domainFetchers.push(fetcherObject.fetcher_id) : '';
			}
		});
		var domainOpenSourceIds = domainFetchers.concat(categpryIds);
		if (domainOpenSourceIds.length > 0) {
			fetcherIds = fetcherIds.filter(function (id) {
				return domainOpenSourceIds.indexOf(id) >= 0;
			});
		} */
		if ($scope.hasPeopleDetails) {
			$scope.person.fullName !== '' ? searchTexts += $scope.person.fullName + ' ' : '';/* jshint ignore:line */
			$scope.person.firstName !== '' ? searchTexts += $scope.person.firstName + ' ' : '';/* jshint ignore:line */
			$scope.person.lastName !== '' ? searchTexts += $scope.person.lastName + ' ' : '';/* jshint ignore:line */
			$scope.person.phone !== '' ? paramsObject.phone = $scope.person.phone : '';/* jshint ignore:line */
			$scope.person.email !== '' ? paramsObject.email = $scope.person.email : '';/* jshint ignore:line */
		}
		if ($scope.hasCompanyDetails) {
			/* by defaulult juridisctions  is addded as headquarters  */
			//$scope.company.headquarter = $scope.company.headquarter === ''  ? $scope.selectedJurisdiction : $scope.company.headquarter;
			$scope.company.fullName !== '' ? searchTexts += $scope.company.fullName + ' ' : '';/* jshint ignore:line */
			$scope.company.founded !== '' ? paramsObject.founded= $scope.company.founded : '';/* jshint ignore:line */
			$scope.company.headquarter !== '' ? paramsObject.headquarter = $scope.company.headquarter : '';/* jshint ignore:line */
			$scope.company.industry !== '' ? paramsObject['business_type'] = $scope.company.industry : '';/* jshint ignore:line */
			$scope.company.phone !== '' ? paramsObject.phone = $scope.company.phone : '';/* jshint ignore:line */
		}
		searchTexts = searchTexts.substring(0, searchTexts.length - 1);
	
		if (recent_fav_search) {
			data = {
				// fetchers: recent_fav_search.type === 'Company' ? ['1013','1008','1006','1021'] : ['27','3','8'], /* ["9","12","13"] */
				keyword: recent_fav_search.searchData,
				searchType: recent_fav_search.type ? recent_fav_search.type :'',
				lightWeight: true,
				/*saveGraph: true,*/
				query_parameters: paramsObject,
				jurisdiction: recent_fav_search.jurisdiction ? recent_fav_search.jurisdiction :'',
				website: recent_fav_search.website ? recent_fav_search.website :''
			};
		}else{
			var data = {
				//  fetchers: $scope.hasCompanyDetails ? ['1013','1008','1006','1021'] : ['27','3','8'], /* ["9","12","13"] */
				keyword: searchTexts,
				searchType: $scope.hasCompanyDetails ? 'Company' : 'Person',
				lightWeight: true,
				/*saveGraph: true,*/
				query_parameters: paramsObject,
				jurisdiction: $scope.jurisdictionKey && $scope.jurisdictionKey.key ?  $scope.jurisdictionKey.key  :'',
				"website": $scope.company.website
			};
		}
		if(data.searchType === 'Person'){
			  data = {
				 fetchers  :['27','3','8'], /* ["9","12","13"] */
				keyword: searchTexts,
				searchType:'Person',
				lightWeight: true,
			};			
				EnrichApiService.getResolvedResultByCategoryTexts(data).then(function (response) {
					$scope.isSearchStarted = true;
					$scope.disableSearchButton = false;
					 
				},function () {
					$scope.disableSearchButton = false;
				});
		}else{
			EnrichApiService.addSearchEntity(data).then(function (response) {
				$scope.savesearch_data.searchId = '';
				if(response && response.data){
					$scope.savesearch_data.searchId = response.data;
				}
			postAdvancedSearch(data);
			},function () {
				$scope.disableSearchButton = false;
			});
		}
		
	};
	/*
	 * @purpose: navigate to network page
	 * @created: 21 jul 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.navigateToNetworkPage = function () {
		$state.go('network-analysis');
	};
	/*
	 * @purpose: navigate to dfd page
	 * @created: 21 jul 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.navigateToDFDPage = function () {
		var url = $state.href('data-find-data', {
			queryText: $scope.entitiesSearchResolveResult[0]["vcard:organization-name"]
		});
		if ($rootScope.hyperLinksNewTab) {
			window.open(url, '_blank');
		} else {
			window.open(url, '_self');
		}
		// window.open(url, '_blank');
	};
	/*
	 * @purpose: navigate to entity page by type
	 * @created: 21 jul 2017
	 * @params: index(number), resultType(string)
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onClickFetcherGoToEntity = function (index, resultType, data) {
		var stateUrl = '';
		var url;
		var web = data.hasURL ?( data.hasURL).replace(/(^\w+:|^)\/\//, '') : "";
		if (resultType === 'resolve') {
			if ($scope.entitiesSearchResolveResult[index].type === 'Person') {
				stateUrl = 'entityPageNew';
			} else if ($scope.entitiesSearchResolveResult[index].type === 'Company') {
				stateUrl = 'entityCompanyNew';
			}
		} else if (resultType === 'zero-index') {
			if ($scope.entitiesSearchZeroIndexResult[index].type === 'Person') {
				stateUrl = 'entityPageNew';
			} else if ($scope.entitiesSearchZeroIndexResult[index].type === 'Company') {
				stateUrl = 'entityCompanyNew';
			}
		} else if (resultType === 'non-zero') {
			if ($scope.entitiesSearchNonZeroIndexResult[index].type === 'Person') {
				stateUrl = 'entityPageNew';
			} else if ($scope.entitiesSearchNonZeroIndexResult[index].type === 'Company') {
				stateUrl = 'entityCompanyNew';
			}
		} else if (resultType === 'filter-result') {
			if ($scope.entitiesFilteredResult[index].type === 'Person') {
				stateUrl = 'entityPageNew';
			} else if ($scope.entitiesFilteredResult[index].type === 'Company') {
				stateUrl = 'entityCompanyNew';
			}
		}
		if (data.type === "dfd") {
			$scope.navigateToDFDPage();
			return;
		}
		if (stateUrl == 'entityPageNew') {
			url = $state.href(stateUrl, {query: $scope.entitiesSearchResolveResult[0].query});
			$scope.person.phone !== '' ? url += '?phone=' + $scope.person.phone : '';/* jshint ignore:line */
			$scope.person.email !== '' ? url += '?email=' + $scope.person.email : '';/* jshint ignore:line */
			url = window.location.href.split("/#/")[0] + "/entity#!/person/" + $scope.entitiesSearchResolveResult[index].query;
		} else {
			url = $state.href(stateUrl, {
				query: data["vcard:organization-name"],
				eid: data.identifier,
				website: web
			});
			$scope.company.phone !== '' ? url += '?phone=' + $scope.company.phone : '';/* jshint ignore:line */
			$scope.company.website !== '' ? url += '?website=' + $scope.company.website : '';/* jshint ignore:line */
			// var url =window.location.href.split("/#/")[0]+"/entity#!/company/"+$scope.entitiesSearchResolveResult[index].query
			//please dont remove below commented code
			/*  if($scope.jurisdictionKey){
			         if(web){
			            url =window.location.href.split("/#/")[0]+"/entity#!/company/"+data["vcard:organization-name"]+'&eid='+data["identifier"]+'?cid='+$scope.jurisdictionKey+'&website='+web;
			         }else{
			             url =window.location.href.split("/#/")[0]+"/entity#!/company/"+data["vcard:organization-name"]+'&eid='+data["identifier"]+'?cid='+$scope.jurisdictionKey;
			         }
			} else {
			    if(web){
			        url = window.location.href.split("/#/")[0] + "/entity#!/company/" + data["vcard:organization-name"] + '?eid=' + data["identifier"]+'&website='+web;
			    }else{
			        url = window.location.href.split("/#/")[0] + "/entity#!/company/" + data["vcard:organization-name"] + '?eid=' + data["identifier"];
			    }
			} */
			var jurisdictionParam = data.isDomiciledIn ? data.isDomiciledIn.toUpperCase() :( $scope.jurisdictionKey && $scope.jurisdictionKey.jurisdictionName) ?  $scope.jurisdictionKey.jurisdictionName.toUpperCase() :'';
			url = window.location.href.split("/#/")[0] + "/entity#!/company/" + data.identifier+'?eid='+data['select_entity']+'&cid='+jurisdictionParam; //this is only for juridsdiction
		}
		if ($rootScope.hyperLinksNewTab) {
			window.open(url, '_blank');
		} else {
			window.open(url, '_self');
		}
		// window.open(url, '_blank');
	};
	/*
	 * @purpose: go to Network Analysis page
	 * @created: 08 nov 2017
	 * @returns: no
	 * @params: queryVal(string)
	 * @author: swathi
	 */
	$scope.goToNetworkAnalysis = function (queryVal) {
		var url = $state.href('linkAnalysis', {
			q: queryVal
		});
		// window.open(url, '_blank');
		if ($rootScope.hyperLinksNewTab) {
			window.open(url, '_blank');
		} else {
			window.open(url, '_self');
		}
	};
	$scope.entitiesFilteredResult = [];
	$scope.isSearchFilteredStarted = false;
	/*
	 * @purpose: cancelFilters function
	 * @created: 28 jul 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.cancelFilters = function () {
		$scope.keywordData = '';
		$scope.personData = false;
		$scope.companyData = false;
		$scope.entitiesFilteredResult = [];
		$scope.isSearchFilteredStarted = false;
	};
	/*---------------Radial Menu Implementation-----------------*/
	$scope.selectedSourceName = '';
	$scope.steps = 1;
	$scope.categoryAvailabilityCount = 0;
	$scope.categorySelectedCount = 0;
	$scope.totalCategorySelectedCount = 0;
	$scope.sourceSelectedIndex = -1;
	var categoriesCheckedList = [];
	/*
	 * @purpose: onSelectSource function
	 * @created: 02 aug 2017
	 * @params: index(number)
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onSelectSourceWithName = function (index) {
		$scope.selectedSourceName = $scope.sourceCategoryConstant[index].key;
		$scope.categoryAvailabilityCount = $scope.sourceCategoryConstant[index].categories.length;
		$scope.steps = 2;
		categoriesCheckedList = [];
	};
	/*
	 * @purpose: onChangeCategoryStatus function
	 * @created: 02 aug 2017
	 * @params: categoryName(string)
	 * @returns: no
	 * @author: sandeep
	 * @modified: 25 sep 2017
	 * @modified by: sandeep(added global select count of category)
	 */
	$scope.onChangeCategoryStatus = function (categoryName) {
		if (categoriesCheckedList.indexOf(categoryName) === -1) {
			$scope.totalCategorySelectedCount++;
			categoriesCheckedList.push(categoryName);
		} else {
			$scope.totalCategorySelectedCount--;
			categoriesCheckedList.splice(categoriesCheckedList.indexOf(categoryName), 1);
		}
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			if (source.key === $scope.selectedSourceName) {
				var isChecked = false;
				angular.forEach(source.categories, function (category) {
					if (category.value) {
						isChecked = true;
					}
				});
				var sourceClass = source.key.replace(' ', '');
				if (isChecked) {
					$('#sourceMenu').addClass('weelnav-custom-' + sourceClass);
				} else {
					$('#sourceMenu').removeClass('weelnav-custom-' + sourceClass);
				}
			}
		});
		$scope.steps = 3;
	};
	/*
	 * @purpose: onSelectAllCategoryPerSource function
	 * @created: 02 aug 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onSelectAllCategoryPerSource = function () {
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			if (source.key === $scope.selectedSourceName) {
				var sourceClass = source.key.replace(' ', '');
				$('#sourceMenu').addClass('weelnav-custom-' + sourceClass);
				angular.forEach(source.categories, function (category) {
					if (!category.value) {
						$scope.totalCategorySelectedCount++;
						category.value = true;
						categoriesCheckedList.push(category.name);
					}
				});
				$scope.categorySelectedCount = source.categories.length;
			}
		});
		$scope.steps = 3;
	};
	/*
	 * @purpose: onDeselectCategoryAllPerSource function
	 * @created: 02 aug 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onDeselectCategoryAllPerSource = function () {
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			if (source.key === $scope.selectedSourceName) {
				var sourceClass = source.key.replace(' ', '');
				$('#sourceMenu').removeClass('weelnav-custom-' + sourceClass);
				angular.forEach(source.categories, function (category) {
					if (category.value) {
						$scope.totalCategorySelectedCount--;
						category.value = false;
					}
				});
				categoriesCheckedList = [];
			}
		});
		$scope.categorySelectedCount = 0;
		$scope.steps = 3;
	};
	/*
	 * @purpose: doneSelectingCategory function
	 * @created: 02 aug 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.doneSelectingCategory = function () {
		var totalCount = 0;
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			angular.forEach(source.categories, function (category) {
				if (category.value) {
					totalCount++;
				}
			});
		});
		$scope.totalCategorySelectedCount = totalCount;
		//	    	$scope.selectedSourceName = '';
		$scope.steps = 4;
	};
	/*
	 * @purpose: onSelectAllCategory function
	 * @created: 02 aug 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onSelectAllCategory = function () {
		var totalCount = 0;
		categoriesCheckedList = [];
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			var sourceClass = source.key.replace(' ', '');
			$('#sourceMenu').addClass('weelnav-custom-' + sourceClass);
			if (!source.value) {
				source.value = true;
			}
			angular.forEach(source.categories, function (category) {
				if (!category.value) {
					category.value = true;
				}
				categoriesCheckedList.push(category.name);
			});
			totalCount += source.categories.length;
		});
		$scope.totalCategorySelectedCount = totalCount;
	};
	/*
	 * @purpose: onDeselectCategoryAll function
	 * @created: 02 aug 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.onDeselectCategoryAll = function () {
		angular.forEach($scope.sourceCategoryConstant, function (source) {
			var sourceClass = source.key.replace(' ', '');
			$('#sourceMenu').removeClass('weelnav-custom-' + sourceClass);
			if (source.value) {
				source.value = false;
				angular.forEach(source.categories, function (category) {
					if (category.value) {
						category.value = false;
					}
				});
			}
		});
		categoriesCheckedList = [];
		$scope.totalCategorySelectedCount = 0;
	};
	$scope.onClickViewSources = function () {
		$scope.steps = 5;
	};
	//----------------------Save Search Result Starts------------------------------------
	$scope.searchHistory = [];
	/*
	 * @purpose: getSearchHistory function
	 * @created: 27 sep 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	var favourite = '';

	function getSearchHistory(pagenumber,favourite) {
		EnrichApiService.getAllSearchHistory(20, $scope.token, favourite, pagenumber).then(function (response) {
			if (response.data != null && response.data.result != null) {
				parseSearchHistoryResult(response.data.result);
			}
		}).catch(function () {});
	}
	$scope.maxSize = 1;
	$scope.pagenumber = 1;
	$scope.gettotal_savesearch = 0;

	function getsavesearchData(pagenumber, load_more) {
		/* for savesearches*/
		favourite = true;
		EnrichApiService.getAllSearchHistory(20, $scope.token, favourite, pagenumber).then(function (response) {
			if (response.data != null && response.data.result != null) {
				$scope.maxSize = response.data.paginationInformation.startIndex;
				$scope.gettotal_savesearch = response.data.paginationInformation.totalResults;
				parseSaveHistory(response.data.result, load_more);
			}
		}).catch(function () {});
	}
	getSearchHistory(1,'');
	getsavesearchData(1, true);
	/*
	 * @purpose: parseSearchHistoryResult function
	 * @created: 27 sep 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	/*purpose Used for recent searches @Ram Singh 11 jan 2018*/
	function parseSearchHistoryResult(results) {
		$scope.recentsearch_keyword = [];
		_.forEach(results, function (value) {
			var searchDatajson = JSON.parse(value.searchData);
			var keyword = searchDatajson.keyword ? searchDatajson.keyword.toLowerCase() : '';
			value.createdOn = new Date(value.createdOn);
			value.updatedOn = new Date(value.updatedOn);
			var type = searchDatajson.searchType ? searchDatajson.searchType : value.searchData.split('"searchType":')[1].split(',')[0].replace(/"/g, "");
			var jurisdiction = searchDatajson.jurisdiction ? searchDatajson.jurisdiction : '';
			var website = searchDatajson.website ? searchDatajson.website : '';
			var newarra = _.find($scope.recentsearch_keyword, {
				searchData: keyword
			});
			if (!newarra && keyword !== '') {
				$scope.recentsearch_keyword.push({
					'searchData': keyword,
					'createdOn': value.createdOn,
					'favourite': value.favourite,
					'searchFlag': value.searchFlag,
					'searchId': value.searchId,
					'updatedOn': value.updatedOn,
					'userId': value.userId,
					'type': type,
					'website': website,
					'jurisdiction': jurisdiction
				});
			}
		});
	}

	function parseSaveHistory(results, load_more) {
		if (load_more) {
			$scope.searchHistory = [];
		}
		_.forEach(results, function (value) {
			var searchDatajson = JSON.parse(value.searchData);
			var keyword = searchDatajson.keyword ? searchDatajson.keyword.toLowerCase() :value.searchData.split('"keyword":')[1].split(",")[0].replace(/"/g, "").toLowerCase();
			var jurisdiction = searchDatajson.jurisdiction ? searchDatajson.jurisdiction : '';
			var website = searchDatajson.website ? searchDatajson.website : '';value.createdOn = new Date(value.createdOn);
			value.updatedOn = new Date(value.updatedOn);
			var type = searchDatajson.searchType ? searchDatajson.searchType : value.searchData.split('"searchType":')[1].split(',')[0].replace(/"/g, "");
			var fav = _.find($scope.searchHistory, {
				searchData: keyword
			});
			if (!fav && value.favourite && keyword !== '') {
				$scope.searchHistory.push({
					'searchData': keyword,
					'createdOn': value.createdOn,
					'favourite': value.favourite,
					'searchFlag': value.searchFlag,
					'searchId': value.searchId,
					'updatedOn': value.updatedOn,
					'userId': value.userId,
					'type': type,
					'website': website,
					'jurisdiction': jurisdiction
				});
			}
		});
	}
	/*
	 * @purpose: displayResult function
	 * @created: 27 sep 2017
	 * @params: no
	 * @returns: no
	 * @author: sandeep
	 */
	$scope.displayResult = function (data) {
		$scope.savesearch_data = data;
		$scope.nodeSearchDatas = [];//empty the nodes
		$scope.delaySearchutility(data);
	};
	/*
	 * @purpose: savefavioutesearch function
	 * @created:11 jan 2018
	 * @params: no
	 * @returns: no
	 * @author: Ram singh
	 */
	$scope.savefavioutesearch = function () {
		var searchId = $scope.savesearch_data.searchId != undefined ? $scope.savesearch_data.searchId : '';
		EnrichApiService.getsavesearchHistory($scope.token, searchId).then(function () {
			getSearchHistory(1,'');
			getsavesearchData(1, true);
			HostPathService.FlashSuccessMessage('Search History Successfully Marked As Favorite.', '');
		}).catch(function () {
			HostPathService.FlashErrorMessage('Something Went Wrong', '');
		});
		//$scope.cancelSearchCriteria();
		$scope.maxSize = 1;
		$scope.pagenumber = 1;
	};
	$scope.loadMore = function () {
		if ($scope.maxSize > 0) {
			getsavesearchData($scope.pagenumber, false);
			$scope.pagenumber++;
		}
	};
	/*----------------------Save Search Result Ends------------------------------------*/
	/*----------------------------------------------------------*/
	/*
	 * @purpose: on scope destory clear unrequired data
	 */
	$scope.$on('$destroy', function () {
		$('body').css('overflow', 'auto');
	});
	/*Enrich Context Menu Start*/
	/*Enrich Context Menu End*/
	/* Enrich  New Advanced SEarch API */
	/*
	 * @purpose: New Advanced Search
	 * @created:25 july 2018
	 * @params: no
	 * @returns: no
	 * @author: Ram singh
	 */
	function postAdvancedSearch(data) {
		var dfdSearchResult = {
			name: 'Data find data chart',
			type: 'dfd',
			title: 'Please click here to see DFD Network Chart of search query',
			details: '',
			image: 'assets/images/enrich/network_force_collapse.png',
			'vcard:organization-name': '',
			entitySiginificance: false
		};
		EnrichApiService.getEntityResolvedV2(data, data.searchType).then(function (response) {
			$scope.isSearchStarted = true;
			handleNewData(response, dfdSearchResult);
			if($scope.entitiesSearchResolveResult.length === 0){
				$scope.disableSearchButton = true;
			}else{
				$scope.disableSearchButton = false;
			}
			if(!response.data['is-completed']){
				$scope.entitiesSearchResolveResult =[];
				$scope.companyFilterList = [];
				$scope.enrichObject.loadingText = true;
				$timeout(function () {
					if(data && data.jurisdiction){
						postAdvancedSearch(data);
					}else{
						$scope.disableSearchButton = false;
					}
				}, 5000);
			}else{
				$scope.disableSearchButton = false;
				$scope.enrichObject.loadingText = false;	
			}			
		}, function () {
			$scope.disableSearchButton = false;
			$scope.enrichObject.loadingText = false;		
		});
	}

	function handleNewData(response, dfdSearchResult) {
		var data = [];
		filterArray = [];
		if (response && response.data && response.data.data && response.data.data.results) {
			var result = response.data.data.results;
			for (var i = 0; i < result.length; i++) {
				var firstData = [];
				var firstName = true;
				if (result[i].result && result[i].result.basic && !_.isEmpty(result[i].result.basic)) {
					var singleObject = result[i].result.basic;
					for (var fetcher in singleObject) { //loop through each object key
						if (singleObject[fetcher]['vcard:organization-name']) {
							var matched_source = _.find(FetcherImageList, { //get the fetcher image
								fetcher_name: fetcher
							});
							//set Blackswanlogo as default
							singleObject[fetcher].key = setSourcePriority(fetcher);
							singleObject[fetcher].image = matched_source ? (matched_source.fetcher_image ? matched_source.fetcher_image : 'assets/images/enrich/blackswan_tech_logo.png ') : 'assets/images/enrich/blackswan_tech_logo.png ';
							singleObject[fetcher].identifier = result[i].identifier ? result[i].identifier : ''; //set the identifier
							singleObject[fetcher]['vcard:organization-name'] = singleObject[fetcher]['vcard:organization-name'].toLowerCase();
							if (singleObject[fetcher].hasURL) {
								singleObject[fetcher].hasURL = (typeof singleObject[fetcher].hasURL) === "string"  ? singleObject[fetcher].hasURL :( (angular.isArray(singleObject[fetcher].hasURL) && singleObject[fetcher].hasURL.length > 0) ? singleObject[fetcher].hasURL[0] :'');
							}
							if(($scope.companyFilterList.indexOf(singleObject[fetcher]['vcard:organization-name']) === -1) && firstName){
								firstName =false;
								$scope.companyFilterList.push(singleObject[fetcher]['vcard:organization-name']); //left side list name
							}
							firstData.push(singleObject[fetcher]);
						}
					} //inner for loop ends
					firstData.sort(function (a, b) {
						return a.key - b.key;
					});
				 data = firstData.length > 0 ?	data.push(firstData[0]) : data;
				} //inner if ends
			} //outer for loop ends	
		} //outer if ends 
		else if (response && response.data  && response.data.results) {
			var result = response.data.results;
			for (var i = 0; i < result.length; i++) {
				var firstData = [];
				var firstName = true;
				var link = result[i]['links'] && result[i]['links']['select-entity'] ?  result[i]['links']['select-entity'].split('/select')[0].split("/") :[];
				var select_entity = link &&  link.length > 0 ? link[link.length-1] :'';
				
				if (result[i] && result[i].overview && !_.isEmpty(result[i].overview)) {
					var singleObject = result[i].overview;
					for (var fetcher in singleObject) { //loop through each object key
						if (singleObject[fetcher]['vcard:organization-name']) {
							var matched_source = _.find(FetcherImageList, { //get the fetcher image
								fetcher_name: fetcher
							});
							//set Blackswanlogo as default
							singleObject[fetcher].key = setSourcePriority(fetcher);
							singleObject[fetcher].image = matched_source ? (matched_source.fetcher_image ? matched_source.fetcher_image : 'assets/images/enrich/blackswan_tech_logo.png ') : 'assets/images/enrich/blackswan_tech_logo.png ';
							singleObject[fetcher].identifier = result[i].identifier ? result[i].identifier : ''; //set the identifier
							singleObject[fetcher]['vcard:organization-name'] = singleObject[fetcher]['vcard:organization-name'].toLowerCase();
							singleObject[fetcher]['select_entity'] = select_entity;
							if (singleObject[fetcher].hasURL) {
								singleObject[fetcher].hasURL = (typeof singleObject[fetcher].hasURL) === "string"  ? singleObject[fetcher].hasURL :( (angular.isArray(singleObject[fetcher].hasURL) && singleObject[fetcher].hasURL.length > 0) ? singleObject[fetcher].hasURL[0] :'');
							}
							if(($scope.companyFilterList.indexOf(singleObject[fetcher]['vcard:organization-name']) === -1) && firstName){
								firstName =false;
								$scope.companyFilterList.push(singleObject[fetcher]['vcard:organization-name']); //left side list name
							}
							firstData.push(singleObject[fetcher]);
						}
					} //inner for loop ends
					firstData.sort(function (a, b) {
						return a.key - b.key;
					});
					data.push(firstData[0]);
				} //inner if ends
			} //outer for loop ends	
		}
		//$scope.entitiesSearchResolveResult = data; //assign the data to the list shown
		data = data.filter((val)=>val);
		if (data.length > 0) {
			$scope.entitiesSearchResolveResult = _.sortBy(data, 'entitySiginificance');//sort the total data by entitySiginificance
			$scope.entitiesSearchResolveResult[0].image = "assets/images/enrich/blackswan_tech_logo.png";
			dfdSearchResult["vcard:organization-name"] = $scope.entitiesSearchResolveResult[0]["vcard:organization-name"]; //set DFD object as second Result
			if ( (_.findIndex(data, {entitySiginificance: true})) === -1 ){//if there is no signinficance or datalength 1 
				$scope.entitiesSearchResolveResult.splice(1, 0, dfdSearchResult);
			}else if(_.every(data, {entitySiginificance: true})){
				$scope.entitiesSearchResolveResult.push(dfdSearchResult);
			}else {
				for (var index = 0; index < $scope.entitiesSearchResolveResult.length; index++) {
					if (!$scope.entitiesSearchResolveResult[index].entitySiginificance) {//this check for undefined entitySiginificance
						$scope.entitiesSearchResolveResult.splice(index, 0, dfdSearchResult);//here we are adding the DFD object after the favourites entites
						break;//breaking the loop as we need that object to be inserted only once
					}
				}
			}
			if($scope.company.fullName && $scope.company.fullName.toLowerCase() =='prayon'){
           	 var prayonTechObj = {
           			"vcard:organization-name": "PRAYON TECHNOLOGIES SA",
           			"isDomiciledIn": "BE",
           			"hasURL": "www.prayon.com",
           			"bst:description": "Prayon Technologies (PRT) sells the know-how and techniques developed by our Group, and also supplies consultancy and support services. PRT has over 50 years' experience in designing and operating phosphoric acid manufacturing units.Over 60% of the phosphoric acid currently manufactured in the world comes from plants which use Prayon technology",
           			"size": "XL",
           			"identifiers": {
           				"trade_register_number": "6593007dz11o8hcxy84",
           				"european_vat_number": "6593007dz11o8hcxy84",
           				"vat_tax_number": "6593007dz11o8hcxy84",
           				"legal_entity_identifier": "6593007dz11o8hcxy84",
           				"other_company_id_number": "6593007dz11o8hcxy84"
           			},
           			"mdaas:RegisteredAddress": {
           				"zip": "4480",
           				"country": "Belgium",
           				"streetAddress": "Rue Joseph Wauters 144",
           				"city": "Engis",
           				"fullAddress": "Rue Joseph Wauters 144 Engis Belgium 4480"
           			},
           			"bst:stock_info": {
           				"ticket_symbol": "Unlisted",
           				"main_exchange": "Unlisted"
           			},
           			"key": 2,
           			"image": "assets/images/enrich/blackswan_tech_logo.png ",
           			"identifier": "6593007dz11o8hcxy84"
           		};
           	$scope.entitiesSearchResolveResult.push(prayonTechObj);
           }
		}
	}
	/*
	 * @purpose: filter search result based on filters
	 * @created: 28 jul 2018
	 * @returns: no
	 * @params: text(string), type(string)
	 * @author: Ram Singh
	 */
	$scope.FilterTheEntity = function (name) {
		$scope.disableSearchButton = true;
		$scope.entitiesFilteredResult = [];
		var indexoffilter = filterArray.indexOf(name);
		if (indexoffilter === -1) {
			filterArray.push(name);
		} else {
			filterArray.splice(indexoffilter, 1);
		}
		if (filterArray.length === 0) {
			$scope.entitiesFilteredResult = $scope.entitiesSearchResolveResult;
		} else {
			for (var index = 0; index < filterArray.length; index++) {
				var filteredData = _.filter($scope.entitiesSearchResolveResult, ['vcard:organization-name', filterArray[index]]);
				$scope.entitiesFilteredResult = $scope.entitiesFilteredResult.concat(filteredData);
			}
		}
		$scope.isSearchFilteredStarted = true;
		$scope.disableSearchButton = false;
	};
	/*
	 * @purpose: set the Source Priority
	 * @created:6 DEC 2018
	 * @params: source
	 * @returns: source priority number
	 * @author: Ram Singh
	 */
	function setSourcePriority(source) {
		var sources = ['companieshouse', 'ec.europa', 'business.data','handelsregister'];
		var Primary = sources.indexOf(source);
		if (Primary !== -1) {
			return 1;
		} else if (source === 'BST') {
			return 2;
		} else {
			return 3;
		}
	} //function Ends
	/*
	 * @purpose: Get the jurisdictions list from the backend
	 * @created:16 JAn 2019
	 * @params: source
	 * @returns: no retun
	 * @author: Ram Singh
	 * the API is called on page load
	 */
	EntityApiService.getJurisdiction($rootScope.ehubObject.token).then(function (response) {
		if (response && response.data && response.data.length > 0) {
			angular.forEach(response.data, function (juridisction) {
				if (juridisction && juridisction.jurisdictionOriginalName && juridisction.jurisdictionOriginalName.toUpperCase() !== 'ALL' && juridisction.jurisdictionOriginalName.toUpperCase() !== 'ANY') { //we are not showing ALL option and the null names
					juridisction.jurisdictionName = juridisction.jurisdictionName && juridisction.jurisdictionName ? juridisction.jurisdictionName.toUpperCase() : '';
					if (!(_.find($scope.jurisdictionList, {'jurisdictionName': juridisction.jurisdictionName,'jurisdictionOriginalName': juridisction.jurisdictionName}))) {
						juridisction.key = juridisction.jurisdictionName;
						juridisction.name = juridisction.jurisdictionOriginalName;
						juridisction.key2 = juridisction.jurisdictionName.toLowerCase();
						juridisction.flag = 'vendor/jquery/flags/1x1/'+juridisction.key2+'.svg';
						$scope.jurisdictionList.push(juridisction);
					}
				}
			});//for each ends
			$('.jurisdiction-scroll').mCustomScrollbar({
				autoHideScrollbar: true,
				theme: "minimal"
			});
		}
	}, function () {});
	$scope.jurisdictionListScrollInitialize=function(){		
		$('.typehead-dropdown + .dropdown-menu').mCustomScrollbar({
			axis: "y"
		});
		
	}
	
}