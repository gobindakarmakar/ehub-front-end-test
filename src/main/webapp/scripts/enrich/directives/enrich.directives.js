'use strict';
elementApp
	.directive('updatepeople', updatepeopleDirective)
	   
	.directive('confirmunchecking', confirmuncheckingDirective)
	
	.directive('updatecompany', updatecompanyDirective)
	
	.directive('updatesource', updatesourceDirective)
	
	.directive('updateopensource', updateopensourceDirective)
	
	.directive('updatedomain', updatedomainDirective)
	
	.directive('updatelocation', updatelocationDirective)
	
	.directive('updaterules', updaterulesDirective)
	
	.directive('updatecase', updatecaseDirective)
	
	.directive('updateaccount', updateaccountDirective)
	
	.directive('updateidentifiers', updateidentifiersDirective)
	
	.directive('numbersonly', numbersonlyDirective)

	.directive('whenScrolled', whenScrolledDirective);

	
    whenScrolledDirective.$inject = ['$timeout'];

    function whenScrolledDirective($timeout) {
	    return function (scope, elm, attr) {
	        var raw = elm[0];
		    elm.bind('scroll', function () {
			    if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
				    scope.$apply(attr.whenScrolled);
			    }
		    });
	    };
   }
	
	numbersonlyDirective.$inject = ['$timeout'];	

	 function numbersonlyDirective($timeout) {
	    return {
	        require: 'ngModel',
	        link: function (scope, element, attr, ngModelCtrl) {
	            function fromUser(text) {
	                if (text) {
	                    var transformedInput = text.replace(/[^0-9]/g,'');

	                    if (transformedInput !== text) {
	                        ngModelCtrl.$setViewValue(transformedInput);
	                        ngModelCtrl.$render();
	                    }
	                    return transformedInput;
	                }
	                return undefined;
	            }            
	            ngModelCtrl.$parsers.push(fromUser);
	        }
	    };
	}

	updatepeopleDirective.$inject = ['$timeout'];
	
	function updatepeopleDirective($timeout) {
	 return {
     templateUrl: 'scripts/enrich/modal/view/people-company-modal.html',
	 restrict: 'E',
	 replace: true,
	 scope: true,
	 link: function postLink(scope, element, attrs) {
	     scope.$watch(attrs.visible, function (newValue, oldValue) {
	         if (newValue !== oldValue) {
	             if (newValue == true) {
	                 $(element).modal({
	                     show: true,
	                     backdrop: 'static'
	                 });
	             }
	             else
	                 $(element).modal('hide');
	         }
	     });
		
	     $(element).on('shown.bs.modal', function () {
	         $timeout(function () {
	             scope.$parent[attrs.visible] = true;
	         }, 0);
	     });
	
	     $(element).on('hidden.bs.modal', function () {
	             $timeout(function () {
	                 scope.$parent[attrs.visible] = false;
	             }, 0);
	         });
	     }
	 };
	}
	confirmuncheckingDirective.$inject = ['$timeout'];
	function confirmuncheckingDirective($timeout) {
		return {
		    template: '<div class="modal custom-modal  people-options-modal">' +
	            '<div class="modal-dialog">' +
	            '<div class="modal-content">' +
	            '<div class="modal-header">' +
	            '<button type="button" class="close" ng-click="close(\'unchecking\')"><span>&#x2716;</span></button>'+
	            '<h4 class="modal-title">Do you want to continue?</h4>' +
	            '</div>' +
	            '<div class="modal-body">' +
	            '<p class="message">Once you select continue your {{selectedOption}} data will be lost. Are you sure to continue?</p>' +
	            '</div>' +
	            '<div class="modal-footer">' +
	            '<button type="button" class="btn btn-primary btn-update" ng-if="selectedOption === \'Person\'" ng-click="companyParamUpdate()">Continue</button> ' +
	            '<button type="button" class="btn btn-primary btn-update" ng-if="selectedOption === \'Company\'" ng-click="peopleParamUpdate()">Continue</button> ' +
	            '<button type="button" class="btn btn-default btn-cancel" ng-click="close(\'unchecking\')">Cancel</button>' +
	            '</div>' +
	            '</div>' +
	            '</div>' +
	            '</div>',
	    restrict: 'E',
	    replace: true,
	    scope: true,
	    link: function postLink(scope, element, attrs) {
	        scope.$watch(attrs.visible, function (newValue, oldValue) {
	            if (newValue !== oldValue) {
	                if (newValue == true) {
	                    $(element).modal({
	                        show: true,
	                        backdrop: 'static'
	                    });
	                }
	                else
	                    $(element).modal('hide');
	            }
	        });
	
	
	        $(element).on('shown.bs.modal', function () {
	            $timeout(function () {
	                scope.$parent[attrs.visible] = true;
	
	            }, 0);
	        });
	
	        $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	
	        }
	    };
	   }
	updatecompanyDirective.$inject = ['$timeout'];
	function updatecompanyDirective($timeout) {
	 return {
	     templateUrl: 'scripts/enrich/modal/view/company-profile-modal.html',
	     restrict: 'E',
	     replace: true,
	     scope: true,
	     link: function postLink(scope, element, attrs) {
	         scope.$watch(attrs.visible, function (newValue, oldValue) {
	             if (newValue !== oldValue) {
	                 if (newValue == true) {
	                     $(element).modal({
	                         show: true,
	                         backdrop: 'static'
	                     });
	                 }
	                 else
	                     $(element).modal('hide');
	             }
	         });
	
	         $(element).on('shown.bs.modal', function () {
	             $timeout(function () {
	                 scope.$parent[attrs.visible] = true;
	
	             }, 0);
	         });
	
	         $(element).on('hidden.bs.modal', function () {
	             $timeout(function () {
	                 scope.$parent[attrs.visible] = false;
	             }, 0);
	         });
	     }
	 };
	}
	updatesourceDirective.$inject = ['$timeout'];
	function updatesourceDirective($timeout) {
	    return {
	        templateUrl: 'scripts/enrich/modal/view/enrich-source-modal.html',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function (scope, element, attrs) {
	        	var sourceMenu = {};
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                        sourceMenu.navigateWheel(0);
	                		scope.onSelectSourceWithName(0);
	                		scope.sourceSelectedIndex = 0;
	                		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	                			if(key === 0)
	                				source.value = true;
	                		});
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	            
	            var sourceList = [];
	            angular.forEach(scope.sourceCategoryConstant, function(source, sourcekey){
	        		sourceList.push(source.key);
	            });
	            
	            sourceMenu = new wheelnav('sourceMenu', null, 300, 300);
	
	            sourceMenu.slicePathFunction = slicePath().DonutSlice;
	            sourceMenu.slicePathCustom = slicePath().DonutSliceCustomization();
	            sourceMenu.slicePathCustom.minRadiusPercent = 0.5;
	            sourceMenu.slicePathCustom.maxRadiusPercent = 0.9;
	            sourceMenu.sliceSelectedPathCustom = sourceMenu.slicePathCustom;
	            sourceMenu.sliceInitPathCustom = sourceMenu.slicePathCustom;
	
	            sourceMenu.animatetime = 100;
	            sourceMenu.navAngle = 0;
	            sourceMenu.spreaderRadius = 0;
	            sourceMenu.markerPathFunction = markerPath().PieLineMarker;
	            sourceMenu.markerEnable = true;
	            sourceMenu.spreaderInTitle = "";
	            sourceMenu.spreaderOutTitle = "";
	            sourceMenu.cssMode = true;
	            sourceMenu.spreaderEnable = false;
	            sourceMenu.clickModeRotate = true;
	            sourceMenu.createWheel(sourceList);
	
	            sourceMenu.titleRotateAngle = 90;
	            
	            sourceMenu.navItems[0].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(0);
	            		scope.sourceSelectedIndex = 0;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 0)
	            				source.value = true;
	            		});
		        	},1);
	            };
	            sourceMenu.navItems[1].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(1);
	            		scope.sourceSelectedIndex = 1;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 1)
	            				source.value = true;
	            		});
		        	},1);
	            };
	            sourceMenu.navItems[2].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(2);
	            		scope.sourceSelectedIndex = 2;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 2)
	            				source.value = true;
	            		});
		        	},1);
	            };
	            sourceMenu.navItems[3].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(3);
	            		scope.sourceSelectedIndex = 3;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 3)
	            				source.value = true;
	            		});
		        	},1);
	            };
	            sourceMenu.navItems[4].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(4);
	            		scope.sourceSelectedIndex = 4;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 4)
	            				source.value = true;
	            		});
		        	},1);
	            };
	            sourceMenu.navItems[5].navigateFunction = function () {
	            	$timeout(function(){
	            		scope.onSelectSourceWithName(5);
	            		scope.sourceSelectedIndex = 5;
	            		angular.forEach(scope.sourceCategoryConstant, function(source, key){
	            			if(key === 5)
	            				source.value = true;
	            		});
		        	},1);
	            };
	        }
	    };
	}
	updateopensourceDirective.$inject = ['$timeout'];
	function updateopensourceDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal people-options-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' +   
	                '<button type="button" class="close" ng-click="close(\'open source\')"><span>&#x2716;</span></button>'+
	                '<h4 class="modal-title">Open Source Categories</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="company-options-form">' +
	                '<div class="row form-group">' +
	                '<ul class="custom-multiselect-container list-unstyled">' +
	                '<li class="multiselect-group multiselect-item title" ng-repeat-start="(key,  openSourceList) in openSourceObject">' +
	                '<a href="javascript:void(0);"><label class="control-label"><b>{{key}}.</b></label></a>' +
	                '</li>' +
	                '<li class="multiselect-item" ng-repeat-end ng-repeat="openSource in openSourceList"><span><label class="checkbox checkbox-inline">' +
	                '<input type="checkbox" ng-model="openSource.value"> {{openSource.key}} </label></span>' +
	                '</li>' +
	                '</ul>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '<div class="modal-footer">'+
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'open source\')" aria-hidden="true">Cancel</button>' +
	                '<button type="button" class="btn btn-update" ng-click="updateOpenSourceParam()" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updatedomainDirective.$inject = ['$timeout'];
	function updatedomainDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' +
	                '<div class="modal-btn-wrapper">' +
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'domain\')" aria-hidden="true">Cancel</button>' +
	                '<button type="button" class="btn btn-update" ng-click="updateDomainParam()" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '</div>' +
	                '<h4 class="modal-title">Domain</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="custom-radio radio-circle">' +
	                '<label class="radio-inline">' +
	                '<input class="warmup" type="radio" id="compliance" name="checkbox-radio" value="compliance" checked="">' +
	                '<span><i class="fa fa-circle"></i></span>' +
	                'Compliance' +
	                '</label>' +
	                '</div>' +
	                '<div class="custom-radio radio-circle">' +
	                '<label class="radio-inline">' +
	                '<input class="warmup" type="radio" id="financial-crime" name="checkbox-radio" value="financial">' +
	                '<span><i class="fa fa-circle"></i></span>' +
	                'Financial Crime' +
	                '</label>' +
	                '</div>' +
	                '<div class="custom-radio radio-circle">' +
	                '<label class="radio-inline">' +
	                '<input class="warmup" type="radio" id="cyber" name="checkbox-radio" value="cyber">' +
	                '<span><i class="fa fa-circle"></i></span>' +
	                'Cyber' +
	                '</label>' +
	                '</div>' +
	                '<div class="custom-radio radio-circle">' +
	                '<label class="radio-inline">' +
	                '<input class="warmup" type="radio" id="business-intelligence" name="checkbox-radio" value="business-intelligence">' +
	                '<span><i class="fa fa-circle"></i></span>' +
	                'Business Intelligence' +
	                '</label>' +
	                '</div>' +
	                '<div class="custom-radio radio-circle">' +
	                '<label class="radio-inline">' +
	                '<input class="warmup" type="radio" id="market-intelligence" name="checkbox-radio" value="market-intelligence">' +
	                '<span><i class="fa fa-circle"></i></span>' +
	                'Market Intelligence' +
	                '</label>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updatelocationDirective.$inject = ['$timeout'];
	function updatelocationDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal people-options-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' +     
	                '<button type="button" class="close" ng-click="close(\'location\')"><span>&#x2716;</span></button>'+
	                '<h4 class="modal-title">Location</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="form">' +
	                '<div class="row form-group"><label class="col-sm-4">Location</label><div class="col-sm-8"><input type="text" id="locationname" class="form-control" placeholder="Location" ng-model="location.country"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-4">City</label><div class="col-sm-8"><input type="text" id="location-city" class="form-control" placeholder="City" ng-model="location.city"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-4">Street</label><div class="col-sm-8"><input type="text" id="locatio-street" class="form-control" placeholder="Street" ng-model="location.street"/></div></div>' +
	                '</div>' +
	                '</div>' +
	                '<div class="modal-footer">'+
	                '<button type="button" class="btn btn-update" ng-click="updateLocationParam()" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'location\')" aria-hidden="true">Cancel</button>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updaterulesDirective.$inject = ['$timeout'];
	function updaterulesDirective($timeout) {
	    return {
	        templateUrl: 'scripts/enrich/modal/view/enrich-rules-modal.html',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updatecaseDirective.$inject = ['$timeout'];
	function updatecaseDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal people-options-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' + 
	                '<button type="button" class="close" ng-click="close(\'case\')"><span>&#x2716;</span></button>'+
	                '<h4 class="modal-title">Case</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="form">' +
	                '<div class="row form-group"><label class="col-sm-4">Case Name</label><div class="col-sm-8"><input type="text" id="casename" class="form-control" placeholder="Case Name" ng-model="cases.name"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-4">Case Number</label><div class="col-sm-8"><input type="text" id="casenumber" class="form-control" placeholder="Case Number" ng-model="cases.number"/></div></div>' +
	                '</div>' +
	                '</div>' +
	                '<div class="modal-footer">'+
	                '<button type="button" class="btn btn-update" ng-click="updateCaseParam()" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'case\')" aria-hidden="true">Cancel</button>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updateaccountDirective.$inject = ['$timeout'];
	function updateaccountDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal people-options-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' +  
	                '<button type="button" class="close" ng-click="close(\'account\')"><span>&#x2716;</span></button>'+
	                '<h4 class="modal-title">Account</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="form">' +
	                '<div class="row form-group"><label class="col-sm-4">Account</label><div class="col-sm-8"><input type="text" id="accountname" class="form-control" placeholder="Account" ng-model="accountText"/></div></div>' +
	                '</div>' +
	                '</div>' +
	                '<div class="modal-footer">'+
	                '<button type="button" class="btn btn-update" ng-click="updateAccountParam(accountText)" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'account\')" aria-hidden="true">Cancel</button>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}
	updateidentifiersDirective.$inject = ['$timeout'];
	function updateidentifiersDirective($timeout) {
	    return {
	        template: '<div class="modal custom-modal people-options-modal">' +
	                '<div class="modal-dialog">' +
	                '<div class="modal-content">' +
	                '<div class="modal-header">' +   
	                '<button type="button" class="close" ng-click="close(\'identifiers\')"><span>&#x2716;</span></button>'+
	                '<h4 class="modal-title">Personal Identifiers</h4>' +
	                '</div>' +
	                '<div class="modal-body">' +
	                '<div class="form">' +
	                '<div class="row form-group"><label class="col-sm-5">Personal Identifiers</label><div class="col-sm-7"><input type="text" id="identifiers-number" class="form-control" placeholder="Personal Identifiers" ng-model="personalIdentifier.number"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-5">Email Address</label><div class="col-sm-7"><input type="text" id="identifiers-email" class="form-control" placeholder="Email Address" ng-model="personalIdentifier.email"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-5">Social Media Account</label><div class="col-sm-7"><input type="text" id="identifiers-social-account" class="form-control" placeholder="Social Media Account" ng-model="personalIdentifier.socialAccount"/></div></div>' +
	                '<div class="row form-group"><label class="col-sm-5">Website</label><div class="col-sm-7"><input type="text" id="identifiers-website" class="form-control" placeholder="Website" ng-model="personalIdentifier.website"/></div></div>' +
	                '</div>' +
	                '</div>' +
	                '<div class="modal-footer">'+
	                '<button type="button" class="btn btn-update" ng-click="updateIdentifiersParam()" data-dismiss="modal" aria-hidden="true">Update</button>' +
	                '<button type="button" class="btn btn-cancel btn-default" ng-click="close(\'identifiers\')" aria-hidden="true">Cancel</button>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>' +
	                '</div>',
	        restrict: 'E',
	        replace: true,
	        scope: true,
	        link: function postLink(scope, element, attrs) {
	            scope.$watch(attrs.visible, function (newValue, oldValue) {
	                if (newValue !== oldValue) {
	                    if (newValue == true) {
	                        $(element).modal({
	                            show: true,
	                            backdrop: 'static'
	                        });
	                    }
	                    else
	                        $(element).modal('hide');
	                }
	            });
	
	            $(element).on('shown.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = true;
	
	                }, 0);
	            });
	
	            $(element).on('hidden.bs.modal', function () {
	                $timeout(function () {
	                    scope.$parent[attrs.visible] = false;
	                }, 0);
	            });
	        }
	    };
	}