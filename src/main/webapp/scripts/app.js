'use strict';

var elementApp = angular.module('ehubApp', ['ui.bootstrap', 'ui.router', 'gridster', 'datatables', 'ngFileUpload', 'ngFlash','angular-timeline','angularMoment', 'ngLocalStorage','angularjs-dropdown-multiselect','ngCookies', 'localytics.directives','rzModule']);
var elementModule = elementApp;            

                     