'use strict';
function displayCalendar(eventDates, id) {
	$( "#"+id ).datepicker({
		beforeShowDay: function(date) {
			var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
			
			for (var i = 0; i < eventDates.length; i++) {
				if($.inArray(y + '-' + (m+1) + '-' + d,eventDates) != -1) {												
					return [true, 'has-event', ''];
				}
			}
			
			return [true];
		},
		onSelect: function(dateText) {
			angular.element(document.getElementById('setEvents')).scope().showModal(dateText);
		},
		dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
		autoclose: true
	});
}

angular.module('ehubApp')
	   .directive("calendarEvent", ['$timeout', '$filter', function($timeout, $filter) {
    return {
    	restrict: 'E',
        scope:{
        	events: '='
        },
        template: "<div id='datepicker' style=\"text-align:center;\"><div class=\"loader\" style=\"padding-top:10px;\"><?xml version=\"1.0\" encoding=\"utf-8\"?><svg width='100px' height='100px' xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\" class=\"uil-gears\"><rect x=\"0\" y=\"0\" width=\"100\" height=\"100\" fill=\"none\" class=\"bk\"></rect><g transform=\"translate(-20,-20)\"><path d=\"M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z\" fill=\"#8f7f59\"><animateTransform attributeName=\"transform\" type=\"rotate\" from=\"90 50 50\" to=\"0 50 50\" dur=\"1s\" repeatCount=\"indefinite\"></animateTransform></path></g><g transform=\"translate(20,20) rotate(15 50 50)\"><path d=\"M79.9,52.6C80,51.8,80,50.9,80,50s0-1.8-0.1-2.6l-5.1-0.4c-0.3-2.4-0.9-4.6-1.8-6.7l4.2-2.9c-0.7-1.6-1.6-3.1-2.6-4.5 L70,35c-1.4-1.9-3.1-3.5-4.9-4.9l2.2-4.6c-1.4-1-2.9-1.9-4.5-2.6L59.8,27c-2.1-0.9-4.4-1.5-6.7-1.8l-0.4-5.1C51.8,20,50.9,20,50,20 s-1.8,0-2.6,0.1l-0.4,5.1c-2.4,0.3-4.6,0.9-6.7,1.8l-2.9-4.1c-1.6,0.7-3.1,1.6-4.5,2.6l2.1,4.6c-1.9,1.4-3.5,3.1-5,4.9l-4.5-2.1 c-1,1.4-1.9,2.9-2.6,4.5l4.1,2.9c-0.9,2.1-1.5,4.4-1.8,6.8l-5,0.4C20,48.2,20,49.1,20,50s0,1.8,0.1,2.6l5,0.4 c0.3,2.4,0.9,4.7,1.8,6.8l-4.1,2.9c0.7,1.6,1.6,3.1,2.6,4.5l4.5-2.1c1.4,1.9,3.1,3.5,5,4.9l-2.1,4.6c1.4,1,2.9,1.9,4.5,2.6l2.9-4.1 c2.1,0.9,4.4,1.5,6.7,1.8l0.4,5.1C48.2,80,49.1,80,50,80s1.8,0,2.6-0.1l0.4-5.1c2.3-0.3,4.6-0.9,6.7-1.8l2.9,4.2 c1.6-0.7,3.1-1.6,4.5-2.6L65,69.9c1.9-1.4,3.5-3,4.9-4.9l4.6,2.2c1-1.4,1.9-2.9,2.6-4.5L73,59.8c0.9-2.1,1.5-4.4,1.8-6.7L79.9,52.6 z M50,65c-8.3,0-15-6.7-15-15c0-8.3,6.7-15,15-15s15,6.7,15,15C65,58.3,58.3,65,50,65z\" fill=\"#9f9fab\"><animateTransform attributeName=\"transform\" type=\"rotate\" from=\"0 50 50\" to=\"90 50 50\" dur=\"1s\" repeatCount=\"indefinite\"></animateTransform></path></g></svg></div></div>",
        link: function(scope) {
        	$timeout(function() {
        		var dates = [];
              	angular.forEach(scope.events, function(event){
              		dates.push($filter('date')(event.timeFrom, 'yyyy-M-d'));
              	});
              	
              	$('.loader').hide();
              	
              	displayCalendar(dates, 'datepicker');
        	}, 5000);
        	
        	$().ready(function(){
        		$('a#event-calendar-menu').on('click', function () {
        			$(this).parent().toggleClass('open');
        			 $(this).parent().find(".dropdown-menu").css('display','block');
        			 $('.calendar-event-container').show();
        			 
        			 $('#navbar-dropdown-event-modal').show();
        			 $('#navbar-dropdown-event-modal').click(function () {
        				 $('.calendar-event-container').hide();
        				 $('#event-calendar-menu').parent().removeClass('open');
             	        $('#event-calendar-menu').parent().find(".dropdown-menu").css('display','none');
             	       $('#navbar-dropdown-event-modal').hide();
        			 });
        		});

        	});

          }
    };
}]);



angular.module('ehubApp')
	   .controller("EventsModalDialogController", ['$scope', '$modalInstance', '$cookies', '$rootScope', '$filter', '$http', 
		function ($scope, $modalInstance, $cookies, $rootScope, $filter, $http) {
		$scope.token = $rootScope.ehubObject.token;
		$scope.yourEventsData = {};
		
		$scope.ok = function() {
			var fromDate = $filter('date')($rootScope.dateSelected, "yyyy-MM-dd") + ' '+ $filter('date')($scope.yourEventsData.timeFrom, "HH:mm:ss");
			var toDate = $filter('date')($rootScope.dateSelected, "yyyy-MM-dd") + ' '+ $filter('date')($scope.yourEventsData.timeTo, "HH:mm:ss");
			
		  $http({
	      	method: 'POST',
	  		url: 'api/events/createEvent',
	  		params: {
	  			"userId": $scope.token, 
	  			"description": $scope.yourEventsData.description, 
	  			"venue": $scope.yourEventsData.venue, 
	  			"timeFrom": fromDate, 
	  			"timeTo": toDate
			}
		  }).then(function successCallback() {
			  
			  $modalInstance.close();
		  }, function errorCallback() {
			  
		  });
		  
		  $modalInstance.close();
		};
	
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
}]);

function showSuccessAlert(title, message) {/*jshint ignore:line*/
    var opts = {
        addclass : "stack-modal"
    };
    opts.title = "<span><b>" + title + "</b></span>";
    opts.text = "<b>" + message + "</b>";
    opts.type = "success";
    opts.icon = " fa fa-info-circle fa-3x";
    PNotify.prototype.options.delay = 5000;/*jshint ignore:line*/
    new PNotify(opts);/*jshint ignore:line*/
}

function showErrorAlert(title, message) {/*jshint ignore:line*/
    var opts = {
        addclass : "stack-modal"
    };
    opts.title = "<span><b>" + title + "</b></span>";
    opts.text = "<b>" + message + "</b>";
    opts.type = "error";
    opts.icon = " fa fa-exclamation-triangle fa-3x";
    PNotify.prototype.options.delay = 5000;/*jshint ignore:line*/
    new PNotify(opts);/*jshint ignore:line*/
}