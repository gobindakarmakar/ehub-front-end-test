/** * START --- CONTEXTMENU DIRECTIVES *** */
'use strict';
// angular.module('ehubApp')
elementApp.directive('wheelNavContextMenu', wheelNavContextMenuDirective);
wheelNavContextMenuDirective.$inject = ['HostPathService', '$state'];

function wheelNavContextMenuDirective( ) {
    return {
        restrict: 'E',
        scope: {
            contextmenuname: '=',
            contextmenuname0: '=',
            contextmenuname1: '=',
            contextmenuname2: '=',
            contextmenuname3: '=',
            contextmenuname4: '=',
            contextmenuname5: '=',
        },
        template: "<div id='{{contextmenuname}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname0}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname1}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname2}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname3}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname4}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>" +
            "<div id='{{contextmenuname5}}' class='custom-menu' data-wheelnav data-wheelnav-slicepath='DonutSlice' data-wheelnav-spreader data-wheelnav-spreaderpath='PieSpreader' data-wheelnav-rotateoff data-wheelnav-navangle='180' data-wheelnav-cssmode></div>",
        controller: 'ContextController'
    };
}
/** * END --- CONTEXTMENU DIRECTIVES ** */

// angular.module('ehubApp')
elementApp.controller('ContextController', contextController);

contextController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$stateParams',
    '$timeout',
    'HostPathService'
];

function contextController(
    $scope,
    $rootScope,
    $state,
    $stateParams,
    $timeout,
    HostPathService) {

//    console.log('in custom directive');
    $scope.rootPath = HostPathService.getHostPath();
    $scope.currentStateName = $state.current.name;
    if ($scope.currentStateName == 'actCase') {
        $scope.caseId = $stateParams.caseId;
    }
    if ($scope.currentStateName == 'enrich') {
        $scope.caseId = HostPathService.getParameterByName('caseId');
    }

    //			if($stateParams.caseId != undefined && $stateParams.caseId != null && $stateParams.caseId != ''){
    //				var params = {
    //					caseId: $stateParams.caseId,
    //					token: $rootScope.ehubObject.token
    //				};
    //				ActApiService.getCaseDetails(params).then(function(response){
    //					$rootScope.caseDetails = response.data;
    //					$scope.caseName = response.data.name;
    //				}, function(error){
    //					
    //				});
    //			}
    $timeout(function() {
        if (location.hash.indexOf('#!/') < 0) {
            var tooltips = [{
                    "maninMenu": ["Settings", "Search", "View", "Act", "Navigate"]
                },
                {
                    "subMenu0": ["Logic", "Ontology", "Profile Settings", "Checklist", "Datamodel", "Workflow", "Back"],
                    "subMenu1": [null, "Search", "Advanced Search", "Wiki", "Forum", "Back"],
                    "subMenu2": [null, null, "View", "Back", null, null],
                    "subMenu3": [null, null, "Act", "Back", null, null],
                    "subMenu4": ["Back", "Act", "Discover", "Predict", "Manage", "Enrich"]
                }
            ];
        } else {
            var tooltips = [{
                "maninMenu": ["Notification", "Dashboard", "Transaction", "Risk", "Corporate Structure", "Counter Parties"]
            }];
        }
        var contextmenuname = new wheelnav($scope.contextmenuname, null, 400, 400);/*jshint ignore:line*/
        var contextmenuname0 = new wheelnav($scope.contextmenuname0, contextmenuname.raphael);/*jshint ignore:line*/
        var contextmenuname1 = new wheelnav($scope.contextmenuname1, contextmenuname.raphael);/*jshint ignore:line*/
        var contextmenuname2 = new wheelnav($scope.contextmenuname2, contextmenuname.raphael);/*jshint ignore:line*/
        var contextmenuname3 = new wheelnav($scope.contextmenuname3, contextmenuname.raphael);/*jshint ignore:line*/
        var contextmenuname4 = new wheelnav($scope.contextmenuname4, contextmenuname.raphael);/*jshint ignore:line*/
        var contextmenuname5 = new wheelnav($scope.contextmenuname5, contextmenuname.raphael);/*jshint ignore:line*/

        // Main Menu Configuration
        contextmenuname.wheelRadius = 150;
        contextmenuname.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname.sliceSelectedPathCustom = contextmenuname.slicePathCustom;
        contextmenuname.sliceInitPathCustom = contextmenuname.slicePathCustom;


        // Sub-Menu0 Configuration
        contextmenuname0.wheelRadius = 150;
        contextmenuname0.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname0.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname0.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname0.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname0.sliceSelectedPathCustom = contextmenuname0.slicePathCustom;
        contextmenuname0.sliceInitPathCustom = contextmenuname0.slicePathCustom;


        // Sub-Menu1 Configuration
        contextmenuname1.wheelRadius = 150;
        contextmenuname1.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname1.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname1.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname1.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname1.sliceSelectedPathCustom = contextmenuname1.slicePathCustom;
        contextmenuname1.sliceInitPathCustom = contextmenuname1.slicePathCustom;


        // Sub-Menu2 Configuration
        contextmenuname2.wheelRadius = 150;
        contextmenuname2.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname2.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname2.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname2.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname2.sliceSelectedPathCustom = contextmenuname2.slicePathCustom;
        contextmenuname2.sliceInitPathCustom = contextmenuname2.slicePathCustom;

        // Sub-Menu3 Configuration
        contextmenuname3.wheelRadius = 150;
        contextmenuname3.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname3.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname3.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname3.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname3.sliceSelectedPathCustom = contextmenuname3.slicePathCustom;
        contextmenuname3.sliceInitPathCustom = contextmenuname3.slicePathCustom;


        // Sub-Menu4 Configuration
        contextmenuname4.wheelRadius = 150;
        contextmenuname4.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname4.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname4.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname4.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname4.sliceSelectedPathCustom = contextmenuname4.slicePathCustom;
        contextmenuname4.sliceInitPathCustom = contextmenuname4.slicePathCustom;

        // Sub-Menu5 Configuration
        contextmenuname5.wheelRadius = 150;
        contextmenuname5.slicePathFunction = slicePath().DonutSlice;/*jshint ignore:line*/
        contextmenuname5.slicePathCustom = slicePath().DonutSliceCustomization();/*jshint ignore:line*/
        contextmenuname5.slicePathCustom.minRadiusPercent = 0.3;
        contextmenuname5.slicePathCustom.maxRadiusPercent = 0.6;
        contextmenuname5.sliceSelectedPathCustom = contextmenuname5.slicePathCustom;
        contextmenuname5.sliceInitPathCustom = contextmenuname5.slicePathCustom;

        contextmenuname.animatetime = 0;
        contextmenuname.spreaderRadius = contextmenuname.wheelRadius * 0.30;

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname.spreaderInTitle = "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/home.png";
            contextmenuname.spreaderOutTitle = "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/home.png";
        } else {
            contextmenuname.spreaderInTitle = "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/home.png";
            contextmenuname.spreaderOutTitle = "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/home.png";
        }

        contextmenuname.spreaderOutTitleHeight = 26;
        contextmenuname.spreaderInTitleHeight = 26;

        contextmenuname0.spreaderRadius = 0;
        contextmenuname0.spreaderInTitle = "";
        contextmenuname0.spreaderOutTitle = "";

        contextmenuname1.spreaderRadius = 0;
        contextmenuname1.spreaderInTitle = "";
        contextmenuname1.spreaderOutTitle = "";

        contextmenuname2.spreaderRadius = 0;
        contextmenuname2.spreaderInTitle = "";
        contextmenuname2.spreaderOutTitle = "";

        contextmenuname3.spreaderRadius = 0;
        contextmenuname3.spreaderInTitle = "";
        contextmenuname3.spreaderOutTitle = "";

        contextmenuname4.spreaderRadius = 0;
        contextmenuname4.spreaderInTitle = "";
        contextmenuname4.spreaderOutTitle = "";

        contextmenuname5.spreaderRadius = 0;
        contextmenuname5.spreaderInTitle = "";
        contextmenuname5.spreaderOutTitle = "";

        angular.element('#' + $scope.contextmenuname).hide();

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname.initWheel([
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_settings.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/search.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/watchlist.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_act.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_navigate.png"
            ]);
        } else {
            contextmenuname.initWheel([
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/Notifications.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/Dashboard.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/Tran.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/risk.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/corstru.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/alert-icons/cunterp.png"

            ]);
        }

        if (location.hash.indexOf('#!/') < 0) {
            if (!$scope.caseName) {
                contextmenuname.navItems[2].enabled = false;
            }
        }

        contextmenuname.createWheel();


        // contextmenuname.setTooltips([icon.check, "Search", "View", "Act","Navigate"]);

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname0.initWheel([
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_logic.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_ontology.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_profile_settings.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_checklist.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_datamodel.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_workflow.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_back.png"
            ]);
        } else {
            contextmenuname0.initWheel([
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_logic.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_ontology.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_profile_settings.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_checklist.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_datamodel.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_workflow.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_back.png"
            ]);
        }


        contextmenuname0.navItems[0].enabled = false;
        contextmenuname0.navItems[1].enabled = false;
        contextmenuname0.navItems[2].enabled = false;
        contextmenuname0.navItems[3].enabled = false;
        contextmenuname0.navItems[4].enabled = false;

        contextmenuname0.createWheel();

        //		            contextmenuname0.setTooltips([
        //		                "Logic",
        //		                "Ontology",
        //		                "Profile Settings",
        //		                "Checklist",
        //		                "Datamodel",
        //		                "Workflow",
        //		                "Back"
        //		            ]);

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname1.initWheel([
                null,
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/search.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_advance_search.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_wiki.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_forum.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_back.png"
            ]);
        } else {
            contextmenuname1.initWheel([
                null,
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/search.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_advance_search.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_wiki.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_forum.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_back.png"
            ]);
        }


        contextmenuname1.navItems[1].enabled = false;
        contextmenuname1.navItems[4].enabled = false;

        contextmenuname1.createWheel();

        //		            contextmenuname1.setTooltips([
        //		                null,
        //		                "Search",
        //		                "Advanced Search",
        //		                "Wiki",
        //		                "Forum",
        //		                "Back"
        //		            ]);

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname2.createWheel([
                null,
                null,
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/watchlist.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_back.png",
                null,
                null
            ]);
        } else {
            contextmenuname2.createWheel([
                null,
                null,
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/watchlist.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_back.png",
                null,
                null
            ]);
        }

        //		            contextmenuname2.setTooltips([
        //		                null,
        //		                null,
        //		                "View",
        //		                "Back",
        //		                null,
        //		                null
        //		            ]);

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname3.createWheel([
                null,
                null,
                null,
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_act.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_back.png",
                null
            ]);
        } else {
            contextmenuname3.createWheel([
                null,
                null,
                null,
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_act.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_back.png",
                null
            ]);
        }

        //		            contextmenuname3.setTooltips([
        //		                null,
        //		                null,
        //		                null,
        //		                "Act",
        //		                "Back",
        //		                null
        //		            ]);

        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname4.createWheel([
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_back.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_act.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_discover.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_predict.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_manage.png",
                "imgsrc:" + $scope.rootPath + "assets/images/menu_icons/icon_enrich.png"
            ]);
        } else {
            contextmenuname4.createWheel([
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_back.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_act.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_discover.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_predict.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_manage.png",
                "imgsrc:" + $scope.rootPath + "../assets/images/menu_icons/icon_enrich.png"
            ]);
        }

        //		            contextmenuname4.setTooltips([
        //		                "Back",
        //		                "Act",
        //		                "Discover",
        //		                "Predict",
        //		                "Manage",
        //		                "Enrich"
        //		            ]);

        hideAllSubMenus();
        removeSelection();

        var isenrichMainMenuSelected = false;
        if (location.hash.indexOf('#!/') < 0) {
            contextmenuname.navItems[0].navigateFunction = function() {
                hideenrichMainMenu();
                hideAllSubMenus();
                showSubMenu(0);

            };
            contextmenuname.navItems[1].navigateFunction = function() {
                hideenrichMainMenu();
                hideAllSubMenus();
                showSubMenu(1);
            };
            var url;
            contextmenuname.navItems[2].navigateFunction = function() {
                if ($scope.currentStateName == 'actCase') {
                    url = window.location.href.split("/#/")[0] + "/entity#!/company/" + $scope.caseName;
                }
                window.open(url, '_blank');
            };
            contextmenuname.navItems[3].navigateFunction = function() {
                hideenrichMainMenu();
                hideAllSubMenus();
                $state.go('act');
            };
            contextmenuname.navItems[4].navigateFunction = function() {
                hideenrichMainMenu();
                hideAllSubMenus();
                showSubMenu(4);
            };

        } else {
            contextmenuname.navItems[0].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#notification_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};

            };
            contextmenuname.navItems[1].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#dashboard_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};

            };
            contextmenuname.navItems[2].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#transaction_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};

            };
            contextmenuname.navItems[3].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#risk_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};
            };
            contextmenuname.navItems[4].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#corporate_structure_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};
            };
            contextmenuname.navItems[5].navigateFunction = function() {
            	window.isTabChangeFromContext = true;
                aplyAlertDasboardFilters(window.transSelectedFilter.data, window.transSelectedFilter.id, window.transSelectedFilter.chartType);/*jshint ignore:line*/
                $('#counter_Parties_tab a').click();
                $('#alertMainMenu').css('display', 'none');
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
                window.transSelectedFilter = {};
            };

        }


        contextmenuname0.navItems[0].navigateFunction = function() {
            // alert('Logic');
        };
        contextmenuname0.navItems[1].navigateFunction = function() {
            // alert('Ontology');
        };
        contextmenuname0.navItems[2].navigateFunction = function() {
            // alert('Profile Settings');
        };
        contextmenuname0.navItems[3].navigateFunction = function() {
            // alert('Checklist');
        };
        contextmenuname0.navItems[4].navigateFunction = function() {
            // alert('Datamodel');
        };
        contextmenuname0.navItems[5].navigateFunction = function() {
            url = window.location.href.split("/#/")[0] + "/workflow/#!/";
            window.open(url, '_blank');
            // alert('Workflow');
        };
        contextmenuname0.navItems[6].navigateFunction = function() {
            hideAllSubMenus();
            showenrichMainMenu();
        };

        contextmenuname1.navItems[1].navigateFunction = function() {
            // alert('Search');
        };
        contextmenuname1.navItems[2].navigateFunction = function() {
            $state.go('enrich');
        };
        contextmenuname1.navItems[3].navigateFunction = function() {
            // alert('Wiki');
            var url = "http://" + window.location.hostname + '/elementwiki/';
            window.open(url, '_blank');
        };
        contextmenuname1.navItems[4].navigateFunction = function() {
            // alert('Forum');
        };
        contextmenuname1.navItems[5].navigateFunction = function() {
            hideAllSubMenus();
            showenrichMainMenu();
        };

        contextmenuname2.navItems[2].navigateFunction = function() {
            if ($scope.currentStateName == 'actCase') {
                url = window.location.href.split("/#/")[0] + "/entity#!/company/" + $scope.caseName;
            } else {
                url = window.location.href.split("/#/")[0] + "/entity#!/company/petro saudi";
            }
            window.open(url, '_blank');
        };

        contextmenuname2.navItems[3].navigateFunction = function() {
            hideAllSubMenus();
            showenrichMainMenu();
        };

        contextmenuname3.navItems[4].navigateFunction = function() {
            hideAllSubMenus();
            showenrichMainMenu();
        };

        contextmenuname4.navItems[0].navigateFunction = function() {
            hideAllSubMenus();
            showenrichMainMenu();
        };
        contextmenuname4.navItems[1].navigateFunction = function() {
            $state.go('actCase', {
                'caseId': $scope.caseId
            });
        };
        contextmenuname4.navItems[2].navigateFunction = function() {
            $state.go('discover');
        };
        contextmenuname4.navItems[3].navigateFunction = function() {
            $state.go('predict');
        };
        contextmenuname4.navItems[4].navigateFunction = function() {
            $state.go('manage');
        };
        contextmenuname4.navItems[5].navigateFunction = function() {
            $state.go('enrich');
        };

        function showenrichMainMenu() {
            removeSelection();
            for (var i = 0; i < contextmenuname.navItems.length; i++) {
                if (contextmenuname.navItems[i]) {
                    contextmenuname.navItems[i].navItem.show();
                }
            }
        }

        function hideenrichMainMenu() {
            for (var i = 0; i < contextmenuname.navItems.length; i++) {
                if (contextmenuname.navItems[i]) {
                    contextmenuname.navItems[i].navItem.hide();
                }
            }
        }

        function hideAllSubMenus() {
            for (var i = 0; i < contextmenuname0.navItems.length; i++) {
                if (contextmenuname0.navItems[i]) {
                    contextmenuname0.navItems[i].navItem.hide();
                }
            }
            for (var i = 0; i < contextmenuname1.navItems.length; i++) {
                if (contextmenuname1.navItems[i]) {
                    contextmenuname1.navItems[i].navItem.hide();
                }
            }
            for (var i = 0; i < contextmenuname2.navItems.length; i++) {
                if (contextmenuname2.navItems[i]) {
                    contextmenuname2.navItems[i].navItem.hide();
                }
            }
            for (var i = 0; i < contextmenuname3.navItems.length; i++) {
                if (contextmenuname3.navItems[i]) {
                    contextmenuname3.navItems[i].navItem.hide();
                }
            }
            for (var i = 0; i < contextmenuname4.navItems.length; i++) {
                if (contextmenuname4.navItems[i]) {
                    contextmenuname4.navItems[i].navItem.hide();
                }
            }
        }

        function showSubMenu(index) {
            removeSelection();
            if (index === 0) {
                for (var i = 0; i < contextmenuname0.navItems.length; i++) {
                    if (contextmenuname0.navItems[i]) {
                        contextmenuname0.navItems[i].navItem.show();
                    }
                }
            } else if (index === 1) {
                for (var i = 0; i < contextmenuname1.navItems.length; i++) {
                    if (contextmenuname1.navItems[i]) {
                        contextmenuname1.navItems[i].navItem.show();
                    }
                }
            } else if (index === 2) {
                for (var i = 0; i < contextmenuname2.navItems.length; i++) {
                    if (contextmenuname2.navItems[i]) {
                        contextmenuname2.navItems[i].navItem.show();
                    }
                }
            } else if (index === 3) {
                for (var i = 0; i < contextmenuname3.navItems.length; i++) {
                    if (contextmenuname3.navItems[i]) {
                        contextmenuname3.navItems[i].navItem.show();
                    }
                }
            } else if (index === 4) {
                for (var i = 0; i < contextmenuname4.navItems.length; i++) {
                    if (contextmenuname4.navItems[i]) {
                        contextmenuname4.navItems[i].navItem.show();
                    }
                }
            } else if (index === 5) {}
        }

        function showMinimizedRadialMenu() {
            isenrichMainMenuSelected = false;
            hideenrichMainMenu();
            hideAllSubMenus();
            removeSelection();
            contextmenuname.currentPercent = 0.01;
            contextmenuname.spreader.setCurrentTransform();
            contextmenuname.refreshWheel(true);
        }

        function showMaximizedRadialMenu() {
            isenrichMainMenuSelected = true;
            hideAllSubMenus();
            removeSelection();
            contextmenuname.currentPercent = 1;
            contextmenuname.spreader.setCurrentTransform();
            showenrichMainMenu();
            contextmenuname.refreshWheel(true);

             $('body').append('<div class="context_tooltip tooltip-left" style="position: absolute;z-index: 99999;opacity: 1; pointer-events: none; visibility: visible;display:none;text-transform:uppercase;background-color:#0cae96;  padding: 10px;border-radius: 5px; border: 1px solid gray;font-size: 10px;color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

            $('#discoverMainMenu, #enrichMainMenu, #actMainMenu svg path, image').on("mouseover", function(e) {
                    if (!isNaN(Number(e.currentTarget.id.charAt(e.currentTarget.id.length - 1)))) {
                        if (e.currentTarget.id.indexOf('MainMenu') >= 0) {
                            $(".context_tooltip").html('<span>' + tooltips[0].maninMenu[Number(e.currentTarget.id.charAt(e.currentTarget.id.length - 1))] + '</span>');
                            return $(".context_tooltip").css("display", "block");
                        } else {
                            var tempVar = e.currentTarget.id.split('-')[1];

                            $(".context_tooltip").html('<span>' + tooltips[1]['subMenu' + Number(tempVar.charAt(tempVar.length - 1))][Number(e.currentTarget.id.charAt(e.currentTarget.id.length - 1))] + '</span>');
                            return $(".context_tooltip").css("display", "block");
                        }
                    }

                })
                .on("mousemove", function(e) {
                    $(".context_tooltip").css("left", e.originalEvent.clientX + 20 + "px");
                    $(".context_tooltip").css("top", e.originalEvent.clientY - 20 + "px");


                })
                .on("mouseout", function() {
                    //    $(this).css("opacity", 0.4);
                    //hide tool-tip
                    return $(".context_tooltip").css("display", "none");
                });
        }

        function removeSelection() {
            for (var i = 0; i < contextmenuname.navItems.length; i++) {
                contextmenuname.navItems[i].selected = false;
            }

            for (var i = 0; i < contextmenuname0.navItems.length; i++) {
                contextmenuname0.navItems[i].selected = false;
            }
            for (var i = 0; i < contextmenuname1.navItems.length; i++) {
                contextmenuname1.navItems[i].selected = false;
            }
            for (var i = 0; i < contextmenuname2.navItems.length; i++) {
                contextmenuname2.navItems[i].selected = false;
            }
            for (var i = 0; i < contextmenuname3.navItems.length; i++) {
                contextmenuname3.navItems[i].selected = false;
            }
            for (var i = 0; i < contextmenuname4.navItems.length; i++) {
                contextmenuname4.navItems[i].selected = false;
            }
            contextmenuname.refreshWheel(true);
            contextmenuname0.refreshWheel(true);
            contextmenuname1.refreshWheel(true);
            contextmenuname2.refreshWheel(true);
            contextmenuname3.refreshWheel(true);
            contextmenuname4.refreshWheel(true);
        }

        $('#wheelnav-' + $scope.contextmenuname + '-spreader').click(function() {
            if (isenrichMainMenuSelected) {
                showMinimizedRadialMenu();
            } else {
                showMaximizedRadialMenu();
            }

        });
        $('#wheelnav-' + $scope.contextmenuname + '-spreadertitle').click(function() {
            if (isenrichMainMenuSelected) {
                showMinimizedRadialMenu();
            } else {
                showMaximizedRadialMenu();
            }
        });

        function showContextMenu(topVal, leftVal) {
            angular.element('#' + $scope.contextmenuname).finish().toggle(0).css({
                top: topVal + 'px',
                left: leftVal + 'px'
            });
        }

        function hideContextMenu() {/*jshint ignore:line*/
            $("#" + $scope.contextmenuname).finish().toggle(0);
        }
        $(document).unbind("contextmenu");

        if (location.hash.indexOf('#!/') < 0) {
            $(document).bind("contextmenu", function(event) {
                event.preventDefault();
                showMinimizedRadialMenu();
                var topVal = event.pageY - 200;
                var leftVal = event.pageX - 200;
                showContextMenu(topVal, leftVal);
            });
        } else {
            $(document).bind("contextmenu", function(event) {
            	window.isTabChangeFromContext = true;
              	window.isTabChangeFromContextTrans = true;
//                console.log(event, "event")
                event.preventDefault();
                if ($('#alertMainMenu').css('display') == 'none' && $(event.target).attr('ng-controller') != "SubMenuController") {
                	var pieCompareEvent = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;
                    var pieIdList = ['alertsByStatus30Chart', 'alertsByStatuschart', 'linechartnotification', 'riskMfFtChart', 'productRiskChart', 'riskTypeChart', 'inputOutputRatio', 'topScenariosChart', 'alertStatusChart', 'TransactipTypesPieChart', 'RiskPieChart', 'alertsByPeriodPieChart', 'Transaction_pieChart', 'monthlyTurnoverPieChart', 'riskPieChartOne', 'corpoStructurePieChartOne', 'corpoStructurePieChartTwo', 'corpoStructurePieChartThree', 'corpoStructurePieChartFour', 'corpoStructurePieChartFive', 'corpoStructurePieChartSix', 'counterPartiestopActivity', 'counterPartiestopGeography', 'counterPartiestopBanks', 'counterPartyAlertActvityRatio', 'counterPartiesGeographyRatio', 'counterPartiesBanksRatio', 'counterPartiesBanksLocationsRatio'];

                    var lineCompareEvent = event.target.parentElement.parentElement.id;
                    var lineCompareEventCls = event.target.parentElement.parentElement.className;
                    var lineIdLIst = ['transLineChart'];
                    var linClsList = ['transactionTypeCharts'];

                    var barComapreEvent = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.className;
                    var barComapreEventId = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;
                    var barIdList = ['barchart1'];
                    var barClassList = ['transactionTypeCharts'];

                    var bubbleCompareEvent = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.id;
                    var bubbleIdList = ['riskBloomBergChartOne', 'riskBloomBergChartTwo', 'riskBloomBergChartThree', 'riskBloomBergChartFour', 'riskBloomBergChartFive', 'corpoStructureBloombergChartOne', 'corpoStructureBloombergChartTwo', 'counterPartiesBloombergChartOne', 'counterPartiesBloombergChartTwo', 'counterPartiesBloombergChartThree'];

                    var simpleBarCompareEvent = event.target.parentElement.parentElement.parentElement.id;
                    var simpleBarIdList = ['riskBarChartOne', 'riskBarChartTwo', 'riskBarChartThree', 'riskBarChartFour', 'riskBarChartFive', 'corpoStructureBarChartOne', 'corpoStructureBarChartTwo', 'corpoStructureBarChartTree', 'counterPartiesBarChartOne', 'counterPartiesBarChartTwo', 'counterPartiesBarChartThree','counterpartyWorldMap','transfersByGeographyChart','counterparty_CorruptionRisk','counterparty_PoliticalRisk','corpoStructureWorldMapOne','counterPartiesWorldMapOne','counterPartiesWorldMapTwo'];
                    
                    var negativeBarIdList = ['barchartnotification','transactionComparisonChart'];
                    var negativBarCompareEvent = event.target.parentElement.parentElement.parentElement.parentElement.id;
                    
                    var progressBarCompareEvent = event.target.parentElement.classList[0];
                    var progressBarCls = ['progress'];
                   
                    if (pieIdList.indexOf(pieCompareEvent) >= 0 || lineIdLIst.indexOf(lineCompareEvent) >= 0 || linClsList.indexOf(lineCompareEventCls) >= 0 || barIdList.indexOf(barComapreEventId) >= 0 || barClassList.indexOf(barComapreEvent) >= 0 || bubbleIdList.indexOf(bubbleCompareEvent) >= 0 || simpleBarIdList.indexOf(simpleBarCompareEvent) >= 0 || negativeBarIdList.indexOf(negativBarCompareEvent) >= 0) {
                        showMinimizedRadialMenu();
                        var topVal = event.pageY - 200;
                        var leftVal = event.pageX - 200;
                        showContextMenu(topVal, leftVal);
                    } else if(progressBarCls.indexOf(progressBarCompareEvent) >= 0){
                    	window.transSelectedFilter.data = [event.target.parentElement.parentElement.children[0].innerText,event.target.parentElement.parentElement.parentElement.classList[0]];
                      	window.transSelectedFilter.id = 'progressBarChart';
                        window.transSelectedFilter.chartType ="progressBar";
                     	 showMinimizedRadialMenu();
                         var topVal = event.pageY - 200;
                         var leftVal = event.pageX - 200;
                         showContextMenu(topVal, leftVal);
                    }else {
                        return false;
                    }
                } else {
                    $('#alertMainMenu').css('display', 'none');
                    window.transSelectedFilter = {};
                }
            });
        }


    }, 0);

}