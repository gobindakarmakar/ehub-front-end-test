'use strict';
elementApp
	   .controller('SubMenuController', subMenuController);

subMenuController.$inject = [
	'$scope',   
	'$rootScope', 
	'$state',
	'$stateParams',
	'$uibModal',
	'HostPathService',
	'$location',
	'UploadFileService',
	'UPLOADING_FILES_NUM',
	'DiscoverApiService',
	'ActApiService',
	'ActService',
	'UPLOADING_KYC_FILES_NUM',
	'alertsDataConst',
	'$timeout',
	'EHUB_FE_API',
	'ACTIVITI_FE_PATH',
	'KYC_QUESTIONNAIRE_PATH',
	'POLICY_ENFORCEMENT_PATH',
	'CommonService',
	'$http',
	'$window',
	'TopPanelApiService',
	'Generate_report_Survey_URL'
];
function subMenuController(
		$scope,
		$rootScope,
		$state,
		$stateParams,
		$uibModal,
		HostPathService,
		$location,
		UploadFileService,
		UPLOADING_FILES_NUM,
		DiscoverApiService,
		ActApiService,
		ActService,
		UPLOADING_KYC_FILES_NUM,
		alertsDataConst,
		$timeout,
		EHUB_FE_API,
		ACTIVITI_FE_PATH,
		KYC_QUESTIONNAIRE_PATH,
		POLICY_ENFORCEMENT_PATH,
		CommonService,
		$http,
		$window,
		TopPanelApiService,
		Generate_report_Survey_URL) {
	    $scope.storyClick =false;
		$scope.hyperLinksNewTab = $rootScope.hyperLinksNewTab;
		$scope.currentState = $state.current.name;
		var currentLocation = _.compact(window.location.pathname.split('/'))[1];
		$scope.currentLocationPath = '';
		if(currentLocation === 'sourceManagement'){
			$scope.currentState = currentLocation;
			$scope.currentLocationPath = currentLocation;
		}

		var edittedTemplateData = TopPanelApiService.returnEdittedTemplateData(); /*jshint ignore:line*/ //docparser table edit template name
		
		$scope.view = window.localStorage.getItem("cardHolderType");
		window.localStorage.setItem("cardHolderType", $scope.view);
		$scope.locationValue = window.location.hash.indexOf('#!/') >= 0;
		if($scope.view == 'General Card Holder'){
	        $scope.detailsPageType = 'generalCardHolderDetails';
    	}else if($scope.view =='Dual Card Holders'){
    		$scope.detailsPageType = 'dualCardHolderDetails';
    	} else if($scope.view =='Predicting Dual Card Holders'){
    		$scope.detailsPageType = 'predictingDualCardHolderDetails';
		}
		
		if(window.appName != ""){
			$scope.logo = '../assets/images/logo.png';
		}else{
			$scope.logo = 'assets/images/logo.png';
		}
		

		$scope.redirectToDetailPageWithId = function(activeUc){
			if(activeUc == 'UC1'){
			     $state.go('generalCardHolderDetails', {clusterId:sessionStorage.getItem("clusterId")});
			}else if(activeUc == 'UC2'){
				$state.go('generalCardHolderDetails', {clusterId:sessionStorage.getItem("clusterId")});
			}
		};
		
		$scope.checkHashToDisable = false;
		if(location.hash.indexOf('docParserPage') >= 0){
			if(window.location.href.split('?')[1]=='true'){
				$scope.checkHashToDisable = true;
			}else{	
				$scope.checkHashToDisable = false;
			}
		}
		
		
		/* generalCardHolderDetails */
		$scope.goDetailsPageType = function(){
			$window.history.back();
		};
		//check whether user is admin or not.
		$scope.admin = $rootScope.ehubObject.adminUser;
		
		var onBoadingLocal = {
			isUploadDocuments:false,
			uploadedFiles: [],
			uploadedKYCFiles: [],
			progressbarValue:0
		};
		
		$scope.generateReport = function(){
			$window.generateReport();
		};
//		$scope.data1 = {};
//	    $scope.data1.exportFilename = '';
//	    $scope.data1.displayLabel = 'Download CSV';
	    
//	    if($scope.currentState == 'cluster' || $scope.currentState == 'generalCardHolderDetails' || $scope.currentState == 'dualCardHolderDetails' || $scope.currentState == 'predictingDualCardHolderDetails' ||$scope.currentState == 'customer'){
//		    $http.get("data/data.json").then(function(response){
//		    	 $scope.data1.myInputArray = response.data.children;
//		    });
//	    }
	    $scope.JSONToCSVConvertor = function(JSONData, ReportTitle, ShowLabel) {
	        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
	        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	        
	        var CSV = '';    
	        //Set Report title in first row or line
	        
	        CSV += ReportTitle + '\r\n\n';

	        //This condition will generate the Label/Header
	        if (ShowLabel) {
	            var row = "";
	            //This loop will extract the label from 1st index of on array
	            for (var index in arrData[0]) {
	                //Now convert each value to string and comma-seprated
	                row += index + ',';
	            }
	            row = row.slice(0, -1);
	            //append Label row with line break
	            CSV += row + '\r\n';
	        }
	        
	        //1st loop is to extract each row
	        for (var i = 0; i < arrData.length; i++) {
	            var row = "";
	            
	            //2nd loop will extract each column and convert it in string comma-seprated
	            for (var index in arrData[i]) {
	                row += '"' + arrData[i][index] + '",';
	            }

	            row.slice(0, row.length - 1);
	            
	            //add a line break after each row
	            CSV += row + '\r\n';
	        }

	        if (CSV == '') {     
	            return;
	        }   
	        
	        //Generate a file name
	        var fileName = "clusterCSV";
	        //this will remove the blank-spaces from the title and replace it with an underscore
	        fileName += ReportTitle.replace(/ /g,"_");   
	        
	        //Initialize file format you want csv or xls
	        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
	        
	        // Now the little tricky part.
	        // you can use either>> window.open(uri);
	        // but this will not work in some browsers
	        // or you will not get the correct file extension    
	        
	        //this trick will generate a temp <a /> tag
	        var link = document.createElement("a");    
	        link.href = uri;
	        
	        //set the visibility hidden so it will not effect on your web-layout
	        link.style = "visibility:hidden";
	        link.download = fileName + ".csv";
	        
	        //this part will append the anchor tag and remove it after automatic click
	        document.body.appendChild(link);
	        link.click();
	        document.body.removeChild(link);
	    };
	    /*get drop down menu items for submenu*/
	    var dashboarDropDownMenuItems = jQuery.extend(true, [], HostPathService.getdashboarDropDownMenuItems());
	    var dropDownDisableBydomains =HostPathService.getdashboardDisableBydomains();
		var current_domain =localStorage.getItem("domain");
		if(current_domain && dropDownDisableBydomains[current_domain]){
			angular.forEach(dashboarDropDownMenuItems,function(val){
				angular.forEach(val.content,function(v){
					if ($.inArray(v.name,dropDownDisableBydomains[current_domain]) != -1){
						v.disabled ="yes";
					}
				});
				
			});
		}
	    /*Initializing scope variables*/ 
	    $scope.submenu = {
			activeMenu:'',
			activeDropdown:0,
			dashboardName:'',
			isUploadQuestionaire:false,
			fetchingcaseDocuments:false,
			reassignment:false,
			dashboarDropDownMenuItems:dashboarDropDownMenuItems,
			countAllCaseStatus: [
				{
					label : 'CASE IN FOCUS',
					value : 42
				}, {
					label : 'CASE RESOLVED',
					value : 2
				}, {
					label : 'CASE FORWARED',
					value : 4
				}, {
					label : 'CASE DELAYED',
					value : 75
				}, {
					label : 'TOTAL ENTITIES',
					value : 23
				}, {
					label : 'ENTITIES IN FOCUS',
					value : 22
				} 
			],
			onboarding:onboarding,
			entityClipboard:entityClipboard,
			caseDocsparams:{
				caseId: $stateParams.caseId,
				token: $rootScope.ehubObject.token
			},
			reAssignCase:reAssignCase,
			caseSeedStatus:caseSeedStatus,
			getCaseSeedDetails:getCaseSeedDetails,
			uploadDocuments:uploadDocuments,
			onclicksubMenuDropdown:onclicksubMenuDropdown,
			onhoversubMenuDropdown:onhoversubMenuDropdown,
			getSubMenu:getSubMenu,
			hoverdashboardName: '',
			submenuWidth: 0,
			subMenuContent: 0,
			addClass:addClass,
			iconColorClass: ['light','medium','dark'],
			checkSubmenuDisable: checkSubmenuDisable,
			checkMainmenuDisable: checkMainmenuDisable
	};
	HostPathService.setdashboardname($location.absUrl(), $scope.submenu.dashboarDropDownMenuItems); /* Setting dashboard name*/

	/*
     * @purpose: Array index positioning 
     * @created: 22 sep 2017
     * @params: old_index(number), new_index(number)
     * @return: array
     * @author: Ankit
    */  
	
	var moveArray = function (old_index, new_index, newArray) {
	    if (new_index >= newArray.length) {
	        var k = new_index - newArray.length;
	        while ((k--) + 1) {
	        	newArray.push(undefined);
	        }
	    }
	    newArray.splice(new_index, 0, newArray.splice(old_index, 1)[0]);
	    return newArray; // for testing purposes
	};
	function setDashboardName(){
		if($scope.currentState == 'mipLanding' || $scope.currentState == 'mip' || $scope.currentState == 'customerUsage' || $scope.currentState == 'investigationLanding' || $scope.currentState == 'transactionIntelligenceLanding' || $scope.currentState == 'transactionIntelligence' || $scope.currentState == 'dashboard' || $scope.currentState == "ceriLanding" || $scope.currentState == "ceriSearchResult"){
			$scope.submenu.dashboardName = 'discover';
		}else if($scope.currentState == 'underwriting' || $scope.currentState == 'casePageLanding' ||
		$scope.currentState == 'uploadDocuments' || $scope.currentState == 'analyzeDetails' || $scope.currentState == 'linkAnalysis' || $scope.currentState == 'auditTrail' || $scope.currentState == 'userCaseLanding' || $scope.currentState == 'userCaseDetails' || $scope.currentState == 'alertsDashboard') {
			$scope.submenu.dashboardName = 'act';
		}
		else if ($scope.currentState == 'enrich' || $scope.currentState == 'entityCompany' || $scope.currentState == 'entityPage' || $scope.currentState == 'social-live-feed' || $scope.currentState == 'entityCompanyNew' ||   
				$scope.currentState == 'entityCompanyStatic'|| $scope.currentState == 'entityPageNew' || $scope.currentState == 'data-find-data' || $scope.currentState == 'entityPerson'){
			$scope.submenu.dashboardName = 'enrich';
		}else if($scope.currentState == 'workflow' || $scope.currentState == 'addDataCuration' || $scope.currentState == 'editWorkflow' || $scope.currentState == 'identityUsers' || $scope.currentState == 'identityGroups' || $scope.currentState == 'identityPersonal' || $scope.currentState == 'docparser' || $scope.currentState == 'docParserPage'){
			$scope.submenu.dashboardName = 'manage';
		}
		else if($scope.currentState == 'predict' || $scope.currentState == 'UCtwoCluster' ||  $scope.currentState == "leadGeneration" || $scope.currentState == "cluster" || $scope.currentState == "generalCardHolderDetails" || $scope.currentState == "EAD"  || $scope.currentState == "dualCardHolderDetails" || $scope.currentState == "predictingDualCardHolderDetails" || $scope.currentState == "customer" || $scope.currentState == "leadneneration" || $scope.currentState == "dualCardCustomerPage"){
			$scope.submenu.dashboardName = 'predict';
		}
		else{
			$scope.submenu.dashboardName = HostPathService.getdashboardname();/* Getting dashboard name*/
		}
		// if($scope.currentState == "leadGeneration" || $scope.currentState == "cluster" || $scope.currentState == "generalCardHolderDetails" || $scope.currentState == "EAD"  || $scope.currentState == "dualCardHolderDetails" || $scope.currentState == "predictingDualCardHolderDetails" || $scope.currentState == "customer"){
		// 	$scope.submenu.dashboardName = "discover";
		// 
		
		if ($scope.submenu.dashboardName == 'enrich' || $scope.submenu.dashboardName == 'entityPage' || $scope.submenu.dashboardName == 'entityCompany' || $scope.submenu.dashboardName == 'entityCompanyNew' || $scope.submenu.dashboardName == 'social-live-feed'){
			moveArray(0, 1, $scope.submenu.dashboarDropDownMenuItems);
		}else if($scope.submenu.dashboardName == 'act'){
			moveArray(2, 0, $scope.submenu.dashboarDropDownMenuItems);
		}else if($scope.submenu.dashboardName == 'manage'){
			moveArray(4, 0, $scope.submenu.dashboarDropDownMenuItems);
		}else if($scope.submenu.dashboardName == 'predict'){
			moveArray(3, 0, $scope.submenu.dashboarDropDownMenuItems);
		}

	}
	setDashboardName();
	
	/* Showing on boarding modal by default in enrich page when click on ON BOARDING option from sub menu and process page */
	if((HostPathService.getParameterByName('onBoard') || $stateParams.onBoard) && $state.current.name !== 'discover'){
		$scope.submenu.onboarding(true);
	}
	
	
	/*
     * @purpose: disable submenus
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: ankit
    */  
	function checkSubmenuDisable(value){
		return CommonService.checkSubmenuDisable(value);

	}
	/*
     * @purpose: disable main menu
     * @created: 21 feb 2018
     * @params: value
     * @return: checkMainmenuDisable value
     * @author: swathi
    */  
	function checkMainmenuDisable(value){
		return CommonService.checkMainmenuDisable(value);
	}
	/*
     * @purpose: class
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: ankit
    */  
	function addClass(index){
		if(index==0 || index%3==0){
			return $scope.submenu.iconColorClass[0];
		}
		else if(index==1 || (index-1)%3==0){
			return $scope.submenu.iconColorClass[1];
		}
		else if(index==2 || (index-2)%3==0){
			return $scope.submenu.iconColorClass[2];
		}
	}
	
	/*
     * @purpose: Document Ready
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	angular.element(document).ready(function () {
		angular.element('.db-util-process-tab-discover').mThumbnailScroller({
			axis : "x",
			speed : 10
		});
		angular.element('#createCaseWrapper').mCustomScrollbar({
			axis : "y",
			theme : "minimal"     
		});
		$scope.submenu.submenuWidth = angular.element('.utility-group').width();
		var dropdown = angular.element('.utility-group .dropdown-menu').width();
		$scope.submenu.subMenuContent = dropdown - $scope.submenu.submenuWidth;
		angular.element('.utility-group').children().find('.tab-content').css('width',$scope.submenu.subMenuContent);
		$scope.submenu.workDiaryWidth = angular.element('#work-diary-dropdown-button').width() + 'px';
		
		
    });
 
	
	/*
     * @purpose: Open Upload Modal
     * @created: 6 sep 2018
     * @params: none
     * @return: no
     * @author: prasanthi
    */
$scope.displayUploadModal = function (){
	//	closeonBoarding();
		var displayIDVQuestionnaireModalInstance = $uibModal.open({
			templateUrl: function(){
				
	       		    return '../docparser/modal/views/uploadFiles.modal.html';
			},
			controller: 'UploadDocparserFileController',
			windowClass: 'custom-modal bst_modal upload-questionnaire-modal ',
		});
		displayIDVQuestionnaireModalInstance.result.then(function(response){
			if(response){
			
			}
		}, function(){
			
		});
	};
	
	/*
     * @purpose: on click download template report
     * @created: Apr 10 2018
     * @params: null
     * @return: no
     * @author: Amritesh
    */ 
	$scope.onDissemination = function(){
		var params = {
				token: $rootScope.ehubObject.token
			};
		var data ={
				   "CASENAME":($scope.submenu.caseSeedDetails.name).toString(),
				   "CASEID":($scope.submenu.caseSeedDetails.caseId).toString(),
				   "TYPE":($scope.submenu.caseSeedDetails.type).toString(),
				   "RISKTRANSACTION":($scope.submenu.cumulativeRisk).toString(),
				   "DESCRIPTION":($scope.submenu.caseSeedDetails.description).toString(),
				   "REMARKS":($scope.submenu.caseSeedDetails.remarks).toString(),
		};
		ActApiService.getReport(params,data).then(function(response){
			 var blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});
                saveAs(blob, "CaseDetails.docx");
                HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title:CaseDetails.docx');
		}, function(){
			
		});
	};
	
	
	
	
	
	/*
     * @purpose: on click navigate to link analysis page
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: swathi
    */  
	$scope.onLinkAnalysis = function(){
		var url;
		if($scope.currentState == 'actCase'){
			url = EHUB_FE_API +'#/linkAnalysis?caseId=' + $stateParams.caseId;
		}else if($stateParams.query){
			url = EHUB_FE_API +'#/linkAnalysis?q=' + $stateParams.query;
		}else{
			url = EHUB_FE_API +'#/linkAnalysis';
		}
		window.open(url, '_blank');
	};
	/*
     * @purpose: on click navigate to link analysis page
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: swathi
    */  
	$scope.onDocLinkAnalysis = function(){
		window.loadDocLinkAnalysis();
		
	};
	setUploadedFiles(); /*Set Uploaded Files*/
	
	/*
     * @purpose: on click navigate to entity page
     * @created: 02 Jul 2018
     * @author: swathi
    */  
	$scope.onEntity = function(){
		var url;
		if($scope.currentState == 'actCase'){
		   url = EHUB_FE_API + 'entity/#!/company/' + $scope.submenu.caseSeedDetails.name +"?qId="+$stateParams.caseId;
		}
		if($scope.hyperLinksNewTab){
			window.open(url, '_blank');	
		}else{
			window.open(url, '_self');	
		}
	};
	
	if($stateParams.caseId){
		onBoadingLocal.isUploadQuestionaire = true;
		$scope.submenu.isUploadQuestionaire = onBoadingLocal.isUploadQuestionaire;
		$scope.submenu.reassignment = true;
		if($state.current.name != 'linkAnalysis'){
			getCaseSeedDetails($stateParams.caseId);}  /*Get Case Details*/
	}
	if(HostPathService.getParameterByName('q')){
		if(is_numeric(HostPathService.getParameterByName('q'))){
			getCaseSeedDetails(HostPathService.getParameterByName('q'));}
	}
	
	/*
     * @purpose: Check Is Numeric
     * @created: 10 oct 2017
     * @params: caseId(anything)
     * @return: array
     * @author: Ankit
    */  
	function is_numeric(caseId) {
        return !isNaN(parseFloat(caseId)) && isFinite(caseId);
    }
	

	
	
	 /*
	     * @purpose: Active submenu
	     * @created: 21 sep 2017
	     * @params: submenuIndex(string)
	     * @return: no
	     * @author: ankit
	    */  
	 function onhoversubMenuDropdown(submenuIndex, dashboardName, $event, disabled){
		 if(disabled == 'no'){
			 $scope.submenu.activeDropdown = submenuIndex;   
			 $scope.submenu.hoverdashboardName = dashboardName;
			 $timeout(function(){
				 var maindivElement = $event.currentTarget.parentElement.parentElement;
				 for(var i = 0; i < maindivElement.lastElementChild.childElementCount; i++){
					 for(var j = 0; j < $scope.submenu.dashboarDropDownMenuItems.length; j++){
						 if(maindivElement.lastElementChild.children[i].classList.contains('bg-' + $scope.submenu.dashboarDropDownMenuItems[j].menu.toLowerCase())){
							 maindivElement.lastElementChild.children[i].classList.remove('bg-' + $scope.submenu.dashboarDropDownMenuItems[j].menu.toLowerCase());
						 }
					 }
				 }
				 var divElement  = maindivElement.querySelectorAll('.tab-content .active');
				 divElement[0].classList.add('bg-'+dashboardName.toLowerCase());
			 },0);
		 }
	 }
	 
	 /*
	     * @purpose: Click submenu
	     * @created: 21 sep 2017
	     * @params: submenuName(string)
	     * @return: no
	     * @author: ankit
	    */  
	 function onclicksubMenuDropdown(submenuname){
		 var submenuname = submenuname.toLowerCase();
		 if(submenuname != 'predict'){
			 window.location.href= EHUB_FE_API +'#/'+ submenuname;
		 } else if(submenuname === 'predict'){			
			window.location.href= EHUB_FE_API +'#/leadGeneration/#!/landing';
		 }else{
			 window.location.href= EHUB_FE_API + submenuname;
		 }
	 }

	 /*
     * @purpose: Get Case Documents
     * @created: 02 jan 2018
     * @params: modalBoolean, uploadedFileBoolean
     * @return: no
     * @author: swathi
    */
		
	 function getCaseDocuments(modalBoolean, uploadedFileBoolean){
		 var params = {
			caseId: $stateParams.caseId,
			token: $rootScope.ehubObject.token
		 };
		 UploadFileService.docAggregator(params).then(function(response2){
			 if(response2.data != undefined && response2.data.length != 0){
				 angular.forEach(response2.data, function(v){
					UploadFileService.getCaseDocuments($scope.submenu.caseDocsparams, v.docFlag).then(function(response){			
						$scope.submenu.fetchingcaseDocuments = false;
						if(response.data.result.length > 0){
							onBoadingLocal.isUploadDocuments = true;}
						
						if(!uploadedFileBoolean){
							onBoadingLocal.uploadedFilesLength = onBoadingLocal.uploadedFilesLength != undefined ? (onBoadingLocal.progressbar + response.data.result.length) : response.data.result.length;
							angular.forEach(response.data.result, function(v){
								onBoadingLocal.uploadedFiles.push(v);
							});
							onBoadingLocal.progressbar = onBoadingLocal.progressbar != undefined ? (onBoadingLocal.progressbar + response.data.result.length) : response.data.result.length;
							onBoadingLocal.progressbarValue = onBoadingLocal.progressbar / UPLOADING_FILES_NUM;
						}
						
						setUploadedFiles();												
						if(uploadedFileBoolean){
							uploadFilesDocs();
						}
					},function(error){
						$scope.submenu.fetchingcaseDocuments = false;
						HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
					});
				 });
			 }else{
				 onBoadingLocal.uploadedFilesLength = 0;
				 onBoadingLocal.progressbar = 0;
				 onBoadingLocal.progressbarValue = onBoadingLocal.progressbar / UPLOADING_FILES_NUM;
				 if(uploadedFileBoolean){
					uploadFilesDocs();
				 }
			 }
		 }, function(){
				
		 });
	 }
	 /*
     * @purpose: getCase Details
     * @created: 14 sep 2017
     * @params: widgetId(string)
     * @return: no
     * @author: Ankit
    */
	function onboarding(){
		openOnBoardingModal();
//		$scope.submenu.fetchingcaseDocuments = true;
	}
	 /*
     * @purpose: open onBoarding Modal
     * @created: 14 sep 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function openOnBoardingModal(){
		 $uibModal.open({
			templateUrl: function(){
				if(window.location.hash.indexOf("#!/") < 0){
            		return 'scripts/common/modal/views/onboarding.modal.html';
				}
	       		  else{
	       		    return '../scripts/common/modal/views/onboarding.modal.html';
	       		  }
			},
			controller: 'OnboardingController',
			//backdrop: 'static',
			windowClass: 'custom-modal onboarding-modal',
			resolve:{
				onBoadingDetails: function(){
					return onBoadingLocal;
				}
			}
		}).result.then(function() {	
		}, function() {
		});
	}
	
	/*
     * @purpose: Count Case Seed
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function caseSeedStatus(){
	}
	
	
	
	/*
     * @purpose: Case Seed Details
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function getCaseSeedDetails(caseId){
		var params = {
			caseId: caseId,
			token: $rootScope.ehubObject.token
		};
		ActApiService.getCaseDetails(params).then(function(response){
			$scope.submenu.caseSeedDetails = response.data;
			$rootScope.actCaseDetail = response.data;
			
			$scope.submenu.cumulativeRisk = (1 - (1 - $scope.submenu.caseSeedDetails.directRisk) * (1 - $scope.submenu.caseSeedDetails.indirectRisk) * (1 - $scope.submenu.caseSeedDetails.transactionalRisk))*100;
			getCaseDocuments();
		}, function(){
			
		});
	}
	
	/*
     * @purpose: Set Uploaded Files
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function setUploadedFiles(){
		for(var i = onBoadingLocal.uploadedFiles.length; i< UPLOADING_FILES_NUM; i++){
			onBoadingLocal.uploadedFiles.push({
				fileTitle : ''
			});
		}
		for(var i = onBoadingLocal.uploadedKYCFiles.length; i< UPLOADING_KYC_FILES_NUM; i++){
			onBoadingLocal.uploadedKYCFiles.push({
				fileTitle : ''
			});
		}
	}
	
	
	
	/*
     * @purpose: Get Case Docs
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function uploadDocuments(){
		getCaseDocuments(false,true);				
	}
	
	/*
     * @purpose: Upload File
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function uploadFilesDocs(){
		onBoadingLocal.getUploadedFilesLimit = 6;
		var uploadDocumentsModalInstance = $uibModal.open({
			templateUrl: function(){
				if(window.location.hash.indexOf("#!/") < 0){
	        		return 'scripts/common/modal/views/upload.documents.modal.html';
				}
	       		  else{
	       		    return '../scripts/common/modal/views/upload.documents.modal.html';
	       		  }
			},
			controller: 'UploadDocumentsController',
			backdrop: 'static',
			windowClass: 'custom-modal upload-documents-modal',
			resolve:{
				onBoadingDetails : function(){
					return onBoadingLocal;
				}
			}
		});
		uploadDocumentsModalInstance.result.then(function(response){
			if(response){
			}	
		}, function(){
		});
	}
	
	/*
     * @purpose: Reassign Case
     * @created: 10 oct 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function reAssignCase(){
		var reAssignCaseModalInstance = $uibModal.open({
			templateUrl: 'scripts/act/modal/views/reassign.modal.html',
			controller: 'ReassignCaseController',
			backdrop: 'static',
			windowClass: 'custom-modal re-assign-case'
		});
		reAssignCaseModalInstance.result.then(function(response){
			if(response){
			}	
		}, function(){
		});
	}
	
	 /*
     * @purpose: Active submenu
     * @created: 5 oct 2017
     * @params: submenuname(string)
     * @return: no
     * @author: ankit
    */
   function	getSubMenu(submenuname,  content, dashboardItemNames){	
	   
	  return CommonService.getSubMenuData(submenuname,  content, dashboardItemNames, $scope.currentState, $scope.submenu.caseSeedDetails);
   }
   
   

		   
		   /*
		     * @purpose:  submenu capture
		     * @created: 21 dec 2017
		     * @params: submenucapture(string)
		     * @return: no
		     * @author: varsha
		    */
		  
		  $scope.submenuCaptureDiv = function(id){
			   $('#' + id).mCustomScrollbar("destroy");
			   $('#' + id).attr('style','max-height: 100% !important');
			   $('#' + id).attr('style','padding: 0 !important');
			   html2canvas($('#' + id), {
				   
				    userCORS:true,
					 onrendered : function(canvas) {
					var openWidgetCaptureModal = $uibModal.open({
							templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
							controller: 'WidgetCaptureModalController',
							size: 'lg',
							backdrop: 'static',
							windowClass: 'custom-modal capture-modal',
							resolve: {
								widgetId: function(){
									return id;
								},
								canvasnew:function(){
									return canvas;
								},
								topLocMapView: function(){
									return false;
								}
							}
						});
						
						openWidgetCaptureModal.result.then(function(){
							
						}, function(){
						});
					}
			   });
			  
			   $('#' + id).attr('style','max-height: 470px !important');
			//   $('#' + id).attr('style','padding: 0 0 150px !important');
			   $('#' + id).mCustomScrollbar({
				   axis:'y',
				   theme:'minimal-dark'
			   }); 		
		};
		
		$scope.checkLocationForIcon = function(content){
			if((content.name.toLowerCase() == 'fraud' || content.name.toLowerCase() == 'lead generation' ||content.name.toLowerCase() == 'personalization') && window.location.hash.indexOf("#!/") < 0){
				return true;
			}
		};
		
		$scope.goToBpm = function(){
		   window.open(ACTIVITI_FE_PATH, '_self');
		};

		$scope.markAllWidgets = function(markAll){
			$scope.$emit('markWidgets', markAll);
		};
		
		
		/*
	     * @purpose: Open IDV Questionnaire Modal
	     * @created: 22 sep 2017
	     * @params: none
	     * @return: no
	     * @author: Ankit
	    */
		$scope.displayIDVQuestionnaire = function(){
			var generate_report_surveyUrl = Generate_report_Survey_URL.replace('{serveyID}',$rootScope.qbServeyID);
			var url = generate_report_surveyUrl+$rootScope.ehubObject.token+"&entityId="+$rootScope.entityObj["@identifier"];
			if ($rootScope.hyperLinksNewTab) {
				window.open(url, '_blank');
			} else {
				window.open(url, '_self');
			}
			// window.open(, '_blank');
//			$rootScope.openCheckList  = true;
//			$uibModal.open({
//				templateUrl: function(){
//					if(window.location.hash.indexOf("#!/") < 0){
//	            		return 'idv.questionaire.modal.html';
//					}
//		       		  else{
//		       		    return 'idv.questionaire.modal.html';
//		       		  }
//				},
//				controller: 'IdvQuestionaireController',
//				windowClass: 'custom-modal full-window  upload-questionnaire-modal',
//				size:'lg'
//			}).result.then(function() {	
//			}, function() {
//			});
		};
		function entityClipboard(){
			console.log(TopPanelApiService.entityselection);
			if(TopPanelApiService.entityselection){
				$rootScope.showMyEntityClipboard = false;
			}
			$rootScope.showMyEntityClipboard = !$rootScope.showMyEntityClipboard;
			$rootScope.showMyClipboard = false;
			$rootScope.isFromEntitySection = false;
			$rootScope.isUploadFromEntitySection = false;
			TopPanelApiService.entityselection ='';
			TopPanelApiService.clipBoardObject.modal_postion = {top:'116px',left:''};
			TopPanelApiService.clipBoardObject.file_wraper ='';
			$(".popover-inner").parent().addClass("d-none");
		}
		
		/*
		 * @purpose: Convert the the string into json and return the riskValue
		 * @created: 21st Feb 2019	
		 * @author: varsha
		 */		
		$scope.getRiskScoreValue = function(data){
			var overrallScore;
			if(Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score'])>1){
				 overrallScore = Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score'])/100;
			}else{
				overrallScore = Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score']);
			}
			return overrallScore;
		};
		/*
		 * @purpose: Add A class to the body when Utilities Panel
		 * @created: 21st Feb 2019	
		 * @author: varsha
		 */	
		$('#single-button').on('click', function(e) {
			$('body').addClass('utilities-open');
			e.stopPropagation();
		  });
		  
		  $('html').on('click', '.utilities-open', function() {
			$('body.utilities-open').removeClass('utilities-open');
		  });
}
