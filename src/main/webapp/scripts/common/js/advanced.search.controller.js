'use strict';
elementApp
       .controller('AdvancedSearchController', advancedSearchController);

		advancedSearchController.$inject = [
			'$scope',
			'$rootScope',
			'$state',   
			'$window',
			'TopPanelApiService',
			'EHUB_FE_API',
			'$location'
		];
	    function advancedSearchController(
	    		$scope, 
	    		$rootScope, 
	    		$state, 
	    		$window,
	    		TopPanelApiService,
	    		EHUB_FE_API,
	    		$location
	    		){
    	$scope.token = $rootScope.ehubObject.token;
    	var params = {
	    	token: $scope.token		    	
	    };
    	
    	$scope.search_organization_name ='';
    	$scope.ceriLandingObject={
    			getSuggestedName : getSuggestedName,
    			organization_name : ''
    		};
    	/*
		 * @purpose: on search change
		 * @created: 12 sep 2017
		 * @params: value(string)
		 * @returns: no
		 * @author: swathi
		 */
		$scope.onSearchChange = function(value){
			
				TopPanelApiService.getOrganationNamesSuggestions(value).then(function(response){
				
					return response.data.hits;
//					$scope.items = response.data;
//			        angular.forEach($scope.items.payload, function(item){
//			        	$scope.arr = item.options;
//			        	angular.forEach($scope.arr,function(t){	  
//			        		$scope.names.push(t.text);
//			        	});
//			        });
			        /*$scope.names = $scope.name;*/
				}, function() {
			    });
			
	};
	/*
	 * @purpose: search
	 * @created: 13 sep 2017
	 * @params: searchItem(string)
	 * @returns: no
	 * @author: swathi
	 */
	$scope.doSearch = function(searchItem) {
		$state.go('enrichSearchItem',{'searchItem': searchItem});
		$("ul.typeahead").hide();
		$scope.searchText = '';
		var data =  {
    		"query": {
			"bool": {
				"must": [{
        			"prefix": {
        				"ner_location": "",
        				"ner_person": searchItem,
        				"ner_organization": ""
        			}
    			}],
    			"must_not": [],
    			"should": []
				}
			},
			"from": 0,
			"size": 10,
			"sort": [],
			"aggs": {}
		};
		TopPanelApiService.getElasticSearchData(params, data).then(function(){
			
		}, function() {
	    });
	};
	function getSuggestedName(val){
		return TopPanelApiService.getOrganationNamesSuggestions(val,10).then(function(orgResponse){
			return orgResponse.data.hits;
//			return orgResponse.data.items;
		},function(){

		});
	}
	$scope.ceriSearchRouting = function(item,event){
		if(event.type == 'click'){
			console.log(item);
			$location.$$absUrl = EHUB_FE_API +'entitySearch/#!/entitySearchResult/'+item['@identifier'];
			 location.reload();
//		$state.go("ceriSearchResult",{companyName: item['bst:registrationId']});
		} 
	};
	$scope.SearchNewRouting = function(){
		$location.$$absUrl = EHUB_FE_API +'entitySearch/#!/entitySearchLanding';
		location.reload();
	};
}
