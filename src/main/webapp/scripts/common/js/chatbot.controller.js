'use strict';
elementApp.controller('ChatBotController', chatBotController);

chatBotController.$inject = [ '$scope', '$rootScope', '$state',
		'UploadFileService', '$uibModal', '$timeout' ];

function chatBotController($scope, $rootScope, $state, UploadFileService,
		$uibModal, $timeout) {
	// 

	/* Initializing scope variables */
	$scope.ChatBotScope = {
		chatBot : false,
		chatBotToken : {
			token : $rootScope.ehubObject.token
		},
		chatbotData : [],
		chatBotInput : "",
		submitChatData : submitChatData,
		dialogResponse : '',
		showLoader : false,
		toggleChatBot : toggleChatBot,
		minimizeChatBot : minimizeChatBot,
		closeChatBot : closeChatBot,
		patternError : '',
		errorHandle : '',
		resetChatbot : resetChatbot,
		activeMenu : [],
		checkvalue : checkvalue,
		checkChatBotColor : checkChatBotColor,
		disabledChat : false,
		addNewChat: addNewChat,
		backToHome: backToHome,
		backToHomeScreen: false,
		profileSettings: profileSettings,
		showProfile: false,
		expandChatbot: expandChatbot
	};
	var data;
	function init() {
		if (window.location.hash.indexOf("#!/") < 0
				&& window.location.hash.indexOf("#") >= 0) {
			$scope.chatBotlogo = 'assets/images/logo-new.png';
		} else{
			$scope.chatBotlogo = '../assets/images/logo-new.png';
		}
		if (window.location.hash.indexOf("#!/") < 0
				&& window.location.hash.indexOf("#") >= 0) {
			$scope.chatBotGreylogo = 'assets/images/logo-grey.png';
		} else
			{$scope.chatBotGreylogo = '../assets/images/logo-grey.png';}
	}
	$timeout(function() {
		init();
	}, 0);
	
	/*
	 * @purpose: expand @created: 14 feb 2018 @params: none @return: no
	 * @author: Ankit
	 */
	function expandChatbot(){
		var chatbotPanel =  document.getElementsByClassName('chat-panel-wrapper');
		if(chatbotPanel[0].classList.contains('expand-chatbot')){
			chatbotPanel[0].classList.remove('expand-chatbot');
			$('.chat-bot-wrapper')[0].style.cssText = "height: 300px; max-height: 100%;";
		}
		else{
			chatbotPanel[0].classList.add('expand-chatbot');
			$('.expand-chatbot .chat-bot-wrapper')[0].style.cssText = "height: 335px; max-height: 100%;";
		}
		updateScroll();
	}
	/*
	 * @purpose: add new chat @created: 14 feb 2018 @params: none @return: no
	 * @author: Ankit
	 */
	function addNewChat() {
		$scope.ChatBotScope.backToHomeScreen = true;
		updateScroll();
	}
	
	/*
	 * @purpose: add new chat @created: 14 feb 2018 @params: none @return: no
	 * @author: Ankit
	 */
	function backToHome(){
		$scope.ChatBotScope.backToHomeScreen = false;
		$scope.ChatBotScope.showProfile = false;
	}
	
	/*
	 * @purpose: profile settings @created: 14 feb 2018 @params: none @return: no
	 * @author: Ankit
	 */
	function profileSettings(){
		$scope.ChatBotScope.showProfile = true;
		addNewChat();
	}

	function checkvalue(value) {
		if (value.length === 0){
			return false;}
		if (!value[value.length - 1]['input-choices']){
			return false;}
		if (value[value.length - 1]['input-choices'].length > 0){
			return true;}
	}
	

	/*
	 * @purpose: Toggle Chat Box @created: 28 dec 2017 @params: none @return: no
	 * @author: Ankit
	 */
	var count; //Temporary implementation
	function initChatBot(value) {
	    count = 0; 
		data = {};
		if (value) {			
				if(value.data && value.data.entity_type && value.data.entity_type == 'Insurance'){
					data.message = "car_" + value.data.entity_type.toLowerCase();}
				else{
					data.message = "kyc_" + value.data.entity_type.toLowerCase();}
		} else {
			data.message = "name";
		}
		if (Object.keys(data).length != 0) {
			$scope.ChatBotScope.showLoader = true;
			$scope.ChatBotScope.disabledChat = false;
			UploadFileService
					.startchatDialog(data, $scope.ChatBotScope.chatBotToken)
					.then(
							function(response) {
								$scope.ChatBotScope.chatBotMessage = '';
								$scope.ChatBotScope.showLoader = false;
								$scope.ChatBotScope.dialogResponse = response.data;
								if ($scope.ChatBotScope.dialogResponse['input-choices'].length > 0) {
									$scope.ChatBotScope.dialogResponse['input-choices']
											.push('Other');
								}
								$scope.ChatBotScope.chatbotData
										.push($scope.ChatBotScope.dialogResponse);
								$scope.ChatBotScope.patternMatch = response.data['input-validation-regexp'];
								$scope.ChatBotScope.patternError = $scope.ChatBotScope.dialogResponse['input-type'];
								updateScroll();
							},
							function(error) {
								$scope.ChatBotScope.showLoader = false;
								$scope.ChatBotScope.errorHandle = error.responseMessage;
							});
		}
	}
	/*
	 * @purpose: toggle Chat Data @created: 28 dec 2017 @params: none @return:
	 * no @author: Ankit
	 */
	function toggleChatBot() {
		$scope.ChatBotScope.chatBot = !$scope.ChatBotScope.chatBot;
	}
	

	/*
	 * @purpose: minimize Chat Data @created: 28 dec 2017 @params: none @return:
	 * no @author: Ankit
	 */
	function minimizeChatBot() {
		$scope.ChatBotScope.chatBot = false;
	}

	function checkChatBotColor(value, chatBotInput) {
		for (var i = 0; i < value['input-choices'].length; i++) {
			if (chatBotInput === value['input-choices'][i]){
				return i;}
		}
	}
	/*
	 * @purpose: close Chat Data @created: 28 dec 2017 @params: none @return: no
	 * @author: Ankit
	 */
	function closeChatBot() {
		var chatBotModalInstance = $uibModal.open({
			templateUrl : function() {
				if (window.location.hash.indexOf("#!/") < 0){
					return 'scripts/common/modal/views/chatbot.modal.html';}
				else{
					return '../scripts/common/modal/views/chatbot.modal.html';}
			},
			controller : 'ChatBotModalController',
			windowClass : 'custom-modal upload-questionnaire-modal',
		});
		chatBotModalInstance.result.then(function(response) {
			if (response === 'reset'){
				resetChatbot();}
		}, function() {
			
		});
	}

	/*
	 * @purpose: reset Chat Data @created: 28 dec 2017 @params: none @return: no
	 * @author: Ankit
	 */
	function resetChatbot() {
		$scope.ChatBotScope.backToHomeScreen = true;
		$scope.ChatBotScope.chatbotData = [];
		$scope.ChatBotScope.showLoader = false;
		$scope.ChatBotScope.errorHandle = '';
		$scope.ChatBotScope.chatBotInput = '';
		initChatBot();
		$scope.showUpload = false;
	}

	function setSelectionRange(input, selectionStart, selectionEnd) {
		if (input.setSelectionRange) {
			input.focus();
			input.setSelectionRange(selectionStart, selectionEnd);
		} else if (input.createTextRange) {
			var range = input.createTextRange();
			range.collapse(true);
			range.moveEnd('character', selectionEnd);
			range.moveStart('character', selectionStart);
			range.select();
		}
	}

	function setCaretToPos(input, pos) {
		setSelectionRange(input, pos, pos);
	}
	/*
	 * @purpose: Submit Chat Data @created: 28 dec 2017 @params: none @return:
	 * no @author: Ankit
	 */
	$scope.showUpload = false;
	function submitChatData(event, chatBotInput) {
		if (!$scope.chatForm.chtbot.$error.pattern) {
			if (event.type === 'click') {
				if ($scope.ChatBotScope.chatbotData[$scope.ChatBotScope.chatbotData.length - 1]['input-choices'].length > 0) {
					$scope.ChatBotScope.chatbotData[$scope.ChatBotScope.chatbotData.length - 1].disableChat = true;
					var chatbotColorIndex = checkChatBotColor(
							$scope.ChatBotScope.chatbotData[$scope.ChatBotScope.chatbotData.length - 1],
							chatBotInput);
					$scope.ChatBotScope.chatbotData[$scope.ChatBotScope.chatbotData.length - 1].chatbotColor = chatbotColorIndex;
				}
			}
			if (event.which === 13 || event.type === 'click') {
				$scope.ChatBotScope.activeMenu.push(chatBotInput);
				var data = {
					"message" : chatBotInput,
					"input-name" : $scope.ChatBotScope.dialogResponse['input-name'],
					"input-value" : chatBotInput
				};
				$scope.ChatBotScope.showLoader = true;
				$scope.ChatBotScope.chatBotInput = '';
				UploadFileService
						.enterDataDialog(data,
								$scope.ChatBotScope.chatBotToken,
								$scope.ChatBotScope.dialogResponse['dialog-id'])
						.then(
								function(response) {
									if (response.data.message === 'We got all the info we needed. Thanks!') {
										$scope.ChatBotScope.disabledChat = true;
										var requestPro = {};
										UploadFileService
												.requestProposal(
														requestPro,
														$scope.ChatBotScope.chatBotToken,
														$scope.ChatBotScope.dialogResponse['dialog-id'])
												.then(
														function(
																proposalResponse) {
															$scope.ChatBotScope.showLoader = false;
															var lastInput = {};
															lastInput.chatBotInput = chatBotInput;
															$scope.ChatBotScope.chatbotData
																	.push(lastInput);
															
															if(count > 0){
																$scope.ChatBotScope.chatbotData
																.push(response.data);
															}
															$scope.ChatBotScope.chatBotInput = '';
															$scope.ChatBotScope.patternMatch = $scope.ChatBotScope.dialogResponse['input-validation-regexp'];
															$scope.ChatBotScope.patternError = $scope.ChatBotScope.dialogResponse['input-type'];
															setCaretToPos(
																	$("#chatBotTextArea")[0],
																	0);
															initChatBot(proposalResponse.data);
															updateScroll();
															count++;
														},
														function(err) {
															$scope.ChatBotScope.showLoader = false;
															$scope.ChatBotScope.errorHandle = err.responseMessage;
														});
									} else {
										$scope.ChatBotScope.disabledChat = false;
										$scope.ChatBotScope.showLoader = false;
										$scope.ChatBotScope.chatBotInput = '';
										$scope.ChatBotScope.dialogResponse = response.data;
										$scope.ChatBotScope.dialogResponse.chatBotInput = chatBotInput;
										if ($scope.ChatBotScope.dialogResponse['input-choices'].length > 0){
											$scope.ChatBotScope.dialogResponse['input-choices']
													.push('Other');
												}
										$scope.ChatBotScope.chatbotData
												.push($scope.ChatBotScope.dialogResponse);
										$scope.ChatBotScope.patternMatch = $scope.ChatBotScope.dialogResponse['input-validation-regexp'];
										$scope.ChatBotScope.patternError = $scope.ChatBotScope.dialogResponse['input-type'];
										setCaretToPos($("#chatBotTextArea")[0],
												0);
										updateScroll();
										if($scope.ChatBotScope.dialogResponse.message === 'Please upload your proof of address and proof of identification'){
											$scope.showUpload = true;
										}else{
											$scope.showUpload = false;
										}
									}

									//						
								},
								function(err) {
									$scope.ChatBotScope.showLoader = false;
									$scope.ChatBotScope.errorHandle = err.responseMessage;
								});
			}
		}
	}

	function updateScroll() {
		$timeout(function() {
			$('.chat-bot-wrapper').mCustomScrollbar("scrollTo", 'bottom');
		}, 100);

	}
	$scope.picFiles = '';
	$scope.goToDiscover = function(){
		$state.go('discover');
		$scope.ChatBotScope.minimizeChatBot();
	};

}

