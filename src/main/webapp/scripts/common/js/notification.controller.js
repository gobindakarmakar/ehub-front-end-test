'use strict';
elementApp
	   .controller('NotificationsController', notificationsController);

notificationsController.$inject = [
			'$scope',
			'$rootScope',
			'$state', 
			'$window',
			'TopPanelApiService',
			'HostPathService',
			'$uibModal'
		];
	    function notificationsController(
	    		$scope, 
	    		$rootScope, 
	    		$state, 
	    		$window,
	    		TopPanelApiService,
	    		HostPathService,
	    		$uibModal){
	    	
			$scope.token = $rootScope.ehubObject.token;
			var params = {
		    	token: $scope.token		    	
		    };

//Notification panel starts---------------------------------------------------------------------------//
    	
    	$scope.notificationVariable = {
    			userListNotification:[],
    			notificationList:[],
    			selectedUser:[],
    			checkInCount:''
    	};
    	
    	$scope.notificationFunctions = {
    			shareNotificationToUser:shareNotificationToUser,
    			sendNotification:sendNotification,
    			removeUser:removeUser,
    			openFilterModal:openFilterModal,
    			applyFilters:applyFilters
    	};
    	
    	function removeDuplicates( arr, prop ) {
    	  	  var obj = {};
    	  	  for ( var i = 0, len = arr.length; i < len; i++ ){
    	  	    if(!obj[arr[i][prop]]) {obj[arr[i][prop]] = arr[i];}
    	  	  }
    	  	  var newArr = [];
    	  	  for ( var key in obj ) {newArr.push(obj[key]);}
    	  	  return newArr;
    	  }
    		
    	addScroll('#showAllNoti','200px');
    		
    	   	
    /*
		     * @purpose: get notification
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    var params = {};
    		function getNotification(params){
				TopPanelApiService.getNotificationList(params).then(function(response) {
					  if($('.notifications-container').width() > 0){
			            	$scope.notificationVariable.notificationList = $.merge($.merge([],  $scope.notificationVariable.notificationList), response.data.result);
			            }else {
			            	$scope.notificationVariable.notificationList =  response.data.result;
			            }
			            $scope.notificationVariable.notificationList = removeDuplicates($scope.notificationVariable.notificationList,'notificationId');
			            $rootScope.checkCount = $scope.notificationVariable.notificationList.length;

		        }, function() {
		        });
			}
    	
//    		getNotification();
    		
    		/*
		     * @purpose: post notification
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	
	    	function postNotificationData(data){
				TopPanelApiService.postNotification(data).then(function() {
		            
		            HostPathService.FlashSuccessMessage('SUCCESSFULLY SENT','');
		        }, function() {
		        });
			}
    		
    		   /* * @purpose: Get list of all user *
    	     *   @created: 18th may 2018
    	     *   @returns: no
    	     *   @author: varsha */

    	    function allUserList() {/*jshint ignore:line*/
    	        var obj = {
    	            "token": $scope.token
    	        };
    	        TopPanelApiService.getUserListing(obj).then(function(response) {
    	            $scope.notificationVariable.userListNotification = response.data.result;
    	        }, function(e) {
    	            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
    	        });
    	    }
//    	    allUserList();
    	    var data = {
	    			userId:[],
	    			body:''
	    	};
    	    
    	    
    	    /*
		     * @purpose: share notification user
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    
    	    function shareNotificationToUser($item){
    	    	// var userId = [];
    	    	data.userId.push($item.userId);
    	    	$scope.notificationVariable.selectedUser.push($item);
    	    	
    	    }
    	    
    	    /*
		     * @purpose: send notification
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    
    	    function sendNotification(notiBody){
    	    	data.body = notiBody;
    	    	var notiData ={
		    			"subject" : "Test Notification",
		    			"body" : data.body,
		    			"recipients" : data.userId
		    	};
    	    	postNotificationData(notiData);
    	    }
    	    
    	    /*
		     * @purpose: remove user
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    
    	    function removeUser(index){
                data.userId.splice(index,1);
    	    	$scope.notificationVariable.selectedUser.splice(index,1);
    	    	
    	    }
    	    

			/*
		     * @purpose: open filter modal
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    
    	    function openFilterModal(){
    	    	    	if (window.location.hash.indexOf("#!/") >= 0) {
    	    	    		var modalPath ='../scripts/common/modal/views/notification.filter.modal.html';
    	    			}else{
    	    				var modalPath ='scripts/common/modal/views/notification.filter.modal.html';
    	    	        }
    	    	    	$uibModal.open({
    	    	    		templateUrl:modalPath ,
    	    	    		size: 'md',
    	    	    		scope: $scope,
    	    	    		backdrop: 'static',
    	    	    		windowClass: 'custom-modal sticky-prompt-modal',
    	    	    	}).result.catch(function (res) {
    	    	    		if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
    	    				}else if(res){
    	    					throw res;
    	    				}else{
    	    					
    	    				}
    	    	    	}, function() {
    	    	    	});
    	    	    	 setTimeout(function() {
    	    	    	        //load datePicker
    	    	    	        var start = moment().subtract(2, 'year');
    	    	    	        var end = moment();
    	    	    	        initializeDatePicker('notification', start, end);
    	    	    	    },1000);
    	    	  

    	    }
    	    
    		/*
		     * @purpose: get Notification Count
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    
    	    function getNotificationCount(){
    	    	TopPanelApiService.getNotificationCount().then(function(response) {
    	    		$scope.notificationVariable.checkInCount = response.data.notificationCount;
    	    		getNotificationCheckIn();
    	        }, function() {
    	        });
    	    }
//    	    getNotificationCount();
    	    
    	    /*
		     * @purpose: get Notification CheckIn
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	    

    	    function getNotificationCheckIn(){
    	    	TopPanelApiService.getNotificationCheckIn().then(function() {
    	    		getNotification();
    	        }, function() {
    	        });
    	    }

    	 
    	    
    	   /*
		     * @purpose: addScroll
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
    	  
    	    function addScroll(element,height){
    	    	$(document).ready(function() {
        	        /*Custom Scroll Bar*/
        	        $(element).mCustomScrollbar({
        	            axis: "y",
        	            theme: "minimal"
        	        });
        	        $(element).css("height", height);
        	    });
    	    }
    	    
    	    
    	    
    	    /*
    	     * @purpose:load datePicker
    	     * @created: 5 feb 2018
    	     * @author: varsha
    	     */

    	    function initializeDatePicker(tab, start, end) {
    	        var id = "#" + tab + '_reportrange';

    	        function cb(start, end) {
    	            $scope.Date = [];
    	            $scope.Date[0] = new Date(start._d).getDate() + "-" + ((new Date(start._d).getMonth()) + 1) + "-" + new Date(start._d).getFullYear();
    	            $scope.Date[1] = new Date(end._d).getDate() + "-" + ((new Date(end._d).getMonth()) + 1) + "-" + new Date(end._d).getFullYear();

    	            $(id + ' ' + 'span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    
    	        }

    	        $(id).daterangepicker({
    	            startDate: start,
    	            endDate: end,
    	            ranges: {
    	                'Today': [moment(), moment()],
    	                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    	                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    	                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    	                'This Month': [moment().startOf('month'), moment().endOf('month')],
    	                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	            }
    	        }, cb);

    	        cb(start, end);

    	    }
    	    
    	    
    	    function applyFilters(){
    	    	 params = {
    	     		   'dateFrom':$scope.Date[0],
    	     		   'dateTo':$scope.Date[1]
    	     		};
    	    	 getNotification(params);
    	    	 
    	     	
    	    }
    	    $scope.openNoti = false;
    	    $rootScope.onClickOfNotification = function(){
    	    	getNotificationCount();
    	    };
//    	    setInterval(function(){ 
//    	    	 getNotification();
//    	    }, 60000);
    	    
    		$scope.notificationFunctions.CaptureDiv = function(widgetId){
    			var openWidgetCaptureModal = $uibModal.open({
    				templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
    				controller: 'WidgetCaptureModalController',
    				size: 'lg',
    				//backdrop: 'static',
    				windowClass: 'custom-modal capture-modal',
    				resolve: {
    					widgetId: function(){
    						return widgetId;
    					},
    					canvasnew:function(){
    						return '';
    					},
    					topLocMapView: function(){
    						return '';
    					}
    				}
    			});
    			openWidgetCaptureModal.result.then(function(){
    				
    			}, function(){
    			});
    		};
    	    
    	    
   
}