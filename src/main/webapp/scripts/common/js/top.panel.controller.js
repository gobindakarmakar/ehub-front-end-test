'use strict';
elementApp
	   .controller('TopPanelController', topPanelController);

	    topPanelController.$inject = [
			'$scope',
			'$rootScope',
			'$state', 
			'$window',
			'$location',
			'TopPanelApiService',
			'EHUB_FE_API',
			'$uibModal'
		];
	    function topPanelController(
	    		$scope, 
	    		$rootScope, 
	    		$state, 
	    		$window,
	    		$location,
	    		TopPanelApiService,
	    		EHUB_FE_API,
	    		$uibModal){
	    
		$scope.fullName = $rootScope.ehubObject.email ? $rootScope.ehubObject.email : '';
		$scope.token = $rootScope.ehubObject.token;
		$scope.isCollapsed = false;
		var currentlocation = _.compact(window.location.pathname.split('/'))[1];
		if(currentlocation == 'entity'){
			$scope.path = currentlocation;
		}else{
			$scope.path = $location.path().slice(1);
		}
		$rootScope.isFromEntitySection = false;
		$rootScope.isUploadFromEntitySection =false;
		/*
		 * @purpose: logout
		 * @created: 13 sep 2017
		 * @params: null
		 * @returns: no
		 * @author: swathi
		 */
		$scope.logout = function(){
			TopPanelApiService.getLogout().then(function(){
				window.location.href = $rootScope.rootPath?$rootScope.rootPath:EHUB_FE_API + 'login';

				window.localStorage.removeItem('ehubObject');
			}, function(){
			});
		};
		$rootScope.showMyClipboard = false;
		$rootScope.showMyEntityClipboard = false;
		$rootScope.showUserEvents = false;
		$rootScope.showNotification = false;
		$rootScope.showLogout = false;
		/*
		 * @purpose: toggle the tabs drop down menu
		 * @created: 12 sep 2017
		 * @params: type(string)
		 * @returns: no
		 * @author: swathi
		 */
		$scope.toggleTabsDropdown = function(type){
			if(type === 'clipboard'){
			//	$rootScope.addContextOnNotes();
				$rootScope.showUserEvents = false;
				$rootScope.showNotification = false;
				$rootScope.showLogout = false;
		    	$rootScope.showMyClipboard = !$rootScope.showMyClipboard;
				$rootScope.showMyEntityClipboard = false;
			} else if(type === 'userEvents') {
				$rootScope.showNotification = false;
				$rootScope.showMyClipboard = false;
				$rootScope.showLogout = false;
				$rootScope.showUserEvents = !$rootScope.showUserEvents;
			} else if(type === 'notification') {
				$rootScope.showUserEvents = false;
				$rootScope.showMyClipboard = false;
				$rootScope.showLogout = false;
				$rootScope.onClickOfNotification();
				$rootScope.showNotification = !$rootScope.showNotification;
			} else if(type === 'logout') {
				$rootScope.showMyClipboard = false;
				$rootScope.showUserEvents = false;
				$rootScope.showNotification = false;
				$rootScope.showLogout = !$rootScope.showLogout;
			}				
		};
		
		$scope.navigateOtherPage = function(submenu){
			window.location.href= EHUB_FE_API +'#/'+ submenu;
		};
		if(window.appName != ""){
			$scope.logo = '../assets/images/logo-new.png';
			$scope.userImg = '../assets/images/user-img.png';
			$scope.a01 = '../assets/images/a01.png';
		}else{
			$scope.logo = 'assets/images/logo-new.png';
			$scope.userImg = 'assets/images/user-img.png';
			$scope.a01 = 'assets/images/a01.png';
		}
		
		$scope.onEnrich = function(){
			if(window.location.hash.indexOf("#!/") < 0){
				$state.go('enrich');}
       		  else{
       			window.location.href= EHUB_FE_API +'#/enrich'; }
		};
		$scope.SearchNewRouting = function(){
			/* if(!window.location.hash.includes("#!/"))
				$state.go('enrich');
       		  else */
				 $location.$$absUrl = EHUB_FE_API +'entitySearch/#!/entitySearchLanding'; 
				   location.reload();
		};
		
		$scope.onManage = function(){
			if(window.location.hash.indexOf("#!/") < 0){
				$state.go('manage');}
       		  else{
       			window.location.href= EHUB_FE_API +'#/manage'; }
		};
		
		var getNotesuibModalInstance;
		$scope.riskScoreModal = function(){
			getNotesuibModalInstance = $uibModal.open({
	    		templateUrl:'scripts/common/modal/views/riskScore.modal.html' ,
	    		size: 'md',
	    		//scope: $scope,
	    		backdrop: 'static',
	    		windowClass: 'custom-modal settings-modal ',
	    	}).result.catch(function (res) {
	    		if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
				}else if(res){
					throw res;
				}else{
					
				}
	    	}, function() {
	    	});
	    };



		
		/*
		 * @purpose: Open riskScore Modal on click of setting 
		 * @created: 18th April 2018
		 * @returns: no
		 * @author: varsha
		 */
		
		// var riskScoreuibModalInstance;
		/*$rootScope.riskScoreNewModal = function(){
			if (window.location.hash.includes("#!/")) {
	    		var modalPath ='../scripts/common/modal/views/riskScore.modal.html'
			}else{
				var modalPath ='scripts/common/modal/views/riskScore.modal.html'
	        }
			riskScoreuibModalInstance = $uibModal.open({
	    		templateUrl:modalPath ,
	    		size: 'md',
	    	    controller: 'RiskScoreNewController',
	    		backdrop: 'static',
	    		windowClass: 'custom-modal settings-modal',
	    	}).result.catch(function (res) {
	    		if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
				}else if(res){
					throw res;
				}else{
					
				}
	    	}, function() {
	    	});
			
	    }
		*/
		
		$rootScope.riskScoreModal = function(){
			if (window.location.hash.indexOf("#!/") >= 0) {
	    		var modalPath ='../scripts/common/modal/views/riskScoringNew.modal.html';
			}else{
				var modalPath ='scripts/common/modal/views/riskScoringNew.modal.html';
	        }
		 $uibModal.open({
	    		templateUrl:modalPath ,
	    		size: 'md',
	    	   controller: 'RiskScoreController',
	    		backdrop: 'static',
	    		windowClass: 'custom-modal decision-modal',
	    	}).result.catch(function (res) {
	    		if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
				}else if(res){
					throw res;
				}else{
					
				}
	    	}, function() {
	    	});
	    };
    }
 
angular.module('ehubApp').controller('logonUserCtrl', ['$scope', function ($scope) {/*jshint ignore:line*/
	
}]);

elementApp.controller('notificationsController', ['$scope', function ($scope) {/*jshint ignore:line*/

}]);
