'use strict';
elementApp
    .controller('MyClipBoardController', myClipBoardController);

myClipBoardController.$inject = [
    '$scope',
    '$rootScope',
    '$timeout',
    '$uibModal',
    'TopPanelApiService',
    'HostPathService',
    'Flash',
    '$uibModalStack',
    'CommonService'
];

function myClipBoardController(
    $scope,
    $rootScope,
    $timeout,
    $uibModal,
    TopPanelApiService,
    HostPathService,
    Flash,
    $uibModalStack,
    CommonService) {


    /** * START --- Sticky Notes *** */

    $scope.token = $rootScope.ehubObject.token;
    $scope.documents = [];
    /*
     * @purpose: get list of documents
     * @created: 20 sep 2017
     * @params: null
     * @return: no
     * @author: swathi
     */
    $scope.stickyNotesDocs = [];
    $scope.documents = [];
    $scope.checkedValueDisplay = 'icon';
    $scope.checkedValueListBy = 'desc';

    /*
     * @purpose: Initialize page number
     * @created: 2 april 2018
     * @return: no
     * @author: varsha
     */

    var pageMydocumentSticky = 1;
    var pageMydocumentUploaded = 1;
    var pageMysharedSticky = 1;
    var pageMysharedUploaded = 1;

    /*
     * @purpose: declare length * 
     * @created: 2 april 2018  
     * @return: no
     * @author: varsha
     */
    var stickydocTotalLength;
    var shareddocTotalLength;
    var uploaddocTotalLength;

    /*
     * @purpose: On page change for mysticky notes
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmydocumentSticky = function (page) {
        pageMydocumentSticky = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for myDocument uploaded
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmydocumentUploaded = function (page) {
        pageMydocumentUploaded = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for shared sticky notes
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmysharedSticky = function (page) {
        pageMysharedSticky = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for shared uploaded document
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmysharedUploaded = function (page) {
        pageMysharedUploaded = page;
        getListOfDocuments();
    };
    /*
     * @purpose: Get all mysticky notes and uploaded document
     * @created: 1 april 2018  
     * @return: no
     * @author: varsha
     */

    function getListOfDocuments(orderIn) {
        $scope.loadDocument = true;
        $scope.documents = [];
        if (orderIn != undefined) {
            var params2 = {
                "token": $scope.token,
                "docFlag": 5,
                "pageNumber": pageMydocumentSticky,
                "orderIn": orderIn,
                "orderBy": 'uploadedOn',
                "recordsPerPage": 9
            };
        } else {
            var params2 = {
                "token": $scope.token,
                "docFlag": 5,
                "pageNumber": pageMydocumentSticky,
                "orderIn": 'desc',
                "orderBy": 'uploadedOn',
                "recordsPerPage": 9
            };
        }
        if (caseId != undefined) {
            params2.caseId = caseId;
        }
        TopPanelApiService.getAllDocuments(params2).then(function (response2) {
        	$scope.stickydocLength = response2.data.paginationInformation.totalResults;
            stickydocTotalLength = response2.data.paginationInformation.totalResults;
            $scope.mydocumentSticky = response2.data.result;
            $scope.documents = response2.data.result;
            var params3 = {
                "token": $scope.token,
                "pageNumber": pageMydocumentUploaded,
                "recordsPerPage": 9,
                "orderBy": 'uploadedOn',
                "orderIn": 'desc'
            };
            if (orderIn != undefined) {
                params3.orderIn = orderIn;
            }

            if (caseId != undefined) {
                params3.caseId = caseId;
            }
            TopPanelApiService.getAllDocuments(params3).then(function (response3) {
                uploaddocTotalLength = response3.data.paginationInformation.totalResults;
                $scope.mydocumentUploaded = response3.data.result;
                $scope.documents = $.merge($.merge([], $scope.documents), response3.data.result);
                getAllSharedDocument($scope.documents, orderIn);
            });
        });
    }
    getListOfDocuments();
    /*
     * @purpose: download document
     * @created: 20 sep 2017
     * @params: doc(object)
     * @return: no
     * @author: swathi
     */
    $scope.downloadDocument = function downloadDocument(doc, type) {
        var docId = doc.docId;
        //var docName = doc.docName;
        var docType = doc.type;
        var docTitle = doc.title;
        var params = {
            "docId": docId,
            "token": $scope.token
        };
        TopPanelApiService.downloadDocument(params).then(function (response) {
            var blob = new Blob([response.data], {
                type: "application/" + docType,
            });
            if (type == 'download') {
                if (docType == '') {
                    saveAs(blob, (docTitle|| doc.docName) + '.txt');// jshint ignore:line
                } else {
                    saveAs(blob, (docTitle || doc.docName) + '.' + docType);// jshint ignore:line
                }
                HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title: ' + docTitle);
            } else {
                var reader = new FileReader();
                reader.addEventListener('loadend', function (e) {
                    var text = e.srcElement.result;/*jshint unused:false*/
                });
                // Start reading the blob as text.
                var x = reader.readAsText(blob);/*jshint unused:false*/
            }
        }, function () {
            HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
        });
    };
    $scope.showClipboardUploadFile = false;
    /*
     * @purpose: open add media modal
     * @created: 20 sep 2017
     * @params: null
     * @return: no
     * @author: swathi
     */
    $scope.clipboardUploadFile = function () {
        if (window.location.hash.indexOf("#!/") >= 0) {
            var modalPath = '../scripts/common/modal/views/add.media.modal.html';
        } else {
            var modalPath = 'scripts/common/modal/views/add.media.modal.html';
        }
        var addMediaModalInstance = $uibModal.open({
            templateUrl: modalPath,
            controller: 'AddMediaModalController',
            backdrop: 'static',
            windowClass: 'custom-modal upload-media-modal'
        });

        addMediaModalInstance.result.then(function (response) {
            getListOfDocuments();
            HostPathService.FlashSuccessMessage('SUCCESSFUL UPLOAD DOCUMENT', 'Successfully uploaded document with file title: ' + response);
        }, function (error) {
            if (error !== 'close') {
                HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Failed to upload document with file title: ' + error);
            }
        });
    };
    /*
     * @purpose: upload sticky notes
     * @created: 09 mar 2018
     * @params: file(object)
     * @returns: no
     * @author: swathi
     */
    function uploadStickyNotes(file, name) {
        var params = {
            "token": $scope.token,
            "fileTitle": name,
            "remarks": "",
            "docFlag": 5,
            "docName": name
        };
        var data = {
            uploadFile: file
        };
        TopPanelApiService.uploadDocument(params, data,"isSticky").then(function (response) {
            $scope.documentId = response.data.docId;
            getListOfDocuments();
            var data =
            {
                "subject": "upload sticky",
                "body": 'New sticky is created',
                "recipients": [$rootScope.ehubObject.userId]
            };
            CommonService.createNotificationMethod(data);
            linkToCaseByDefaut(response.data.docId);
        }, function () {
            $scope.addMediaPreloader = false;
        });
    }

    function linkToCaseByDefaut(docId) {
        if ($rootScope.actCaseDetail != undefined) {
            $scope.linkToCase($rootScope.actCaseDetail.caseId, docId);
        }
    }
    /*
     * @purpose: update sticky notes
     * @created: 30 mar 2018
     * @params: file(object)
     * @returns: no
     * @author: varsha
     */
    function updateStickyNotes(file, id) {
        var params = {
            "token": $scope.token,
            "docId": id
        };
        var data = {
            uploadFile: file
        };
        TopPanelApiService.updateDocumentContent(params, data).then(function () {
        }, function () { });
    }

    /*
     * @purpose: Open menu on click of document
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */

    $scope.getMenuForDoc = function (e, doc) {
        $scope.loadDocument = true;
        $scope.doc = doc;
        $scope.permissionDocument = "";
        $('#toShareLinkTab').css('display', 'block');
        $('#deleteDocument').css('display', 'block');
        $('#documentText').val('').empty();
        documentAssociation();
        var CickMenu = document.getElementById('context_menu');
        CickMenu.style.left = e.originalEvent.layerX - 60 - $('#context_menu').width() + 'px';
        CickMenu.style.top = (e.pageY - $('.folder-wrapper').offset().top) + 'px';
        if ($scope.doc.type != '') {
            $('#openstickyNotes').css('display', 'none');
        } else {
            $('#openstickyNotes').css('display', 'block');
        }
        $scope.userValue = "";
        $scope.casevalue = "";
        listDocComment();
        allCaseList();
        allUserList();
        dragElement(document.getElementById(("context_menu")));
    };

    /*
     * @purpose: view sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    $scope.viewStickyNotes = function () {
        $scope.loadDocumentContent = true;
        $('#rightClickMenu').css('display', 'none');
        $('#context_menu').css('display', 'none');
        var stickyNotes = document.getElementById('stickyNotes');
        $('#stickyNotes').css('display', 'block');
        stickyNotes.style.left = "-360px";
        stickyNotes.style.top = "25px";
        displayDocumentOnView($scope.doc);
        dragElement(document.getElementById(("stickyNotes")));
    };
    /*
     * @purpose: close sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    $scope.closeSticky = function () {
        $('#stickyNotes').css('display', 'none');
        getListOfDocuments();
    };

    /*
     * @purpose: auto save sticky content
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    var name;
    $scope.stickyAutoSave = function (e) {
        /*jshint unused:false*/
        var myBlob = new Blob([' '], {
            type: "text/plain"
        });
        if (e.target.value == "") {
            var file = new File(["  "], name);
        } else {
            var file = new File([e.target.value], name);
        }
        if ($scope.documentId) {
            updateStickyNotes(file, $scope.documentId);
        }
    };

    /*
     * @purpose: auto save sticky title
     * @created: 03 april 2018
     * @returns: no 
     * @author: varsha
     */

    $scope.stickyTitleAutoSave = function (e) {
        if (e.target.value == "") {
            updateDocumentTitle($scope.doc, " ");
        } else
            {updateDocumentTitle($scope.doc, e.target.value);}
    };

    function updateDocumentTitle(doc, title) {
        var param = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
            "title": title
        };
        TopPanelApiService.updateDocumentTitle(param).then(function () { }, function (e) {
            HostPathService.FlashErrorMessage('ERROR UPDATE STICKY NOTES', e.responseMessage);
        });

    }
    /*
     * @purpose: open sticky
     * @created: 30 mar 2018
     * @returns: no 
     * @author: varsha
     */
    var getNotesuibModalInstance;
    $scope.confirmDocument = function (status,sticky) {
        $uibModalStack.dismissAll();
        if (status != 'close') {
            var getTitle = $('#getTitle').val();
            if(sticky){
                getTitle =getTitle + ".snt";
             }
            if (getTitle == '' || getTitle == undefined) {
                HostPathService.FlashErrorMessage('ERROR', 'Please enter title');
            } else {
                addNewSticky(getTitle);
            }
        }
    };

    /*
     * @purpose: Upload sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */

    function addNewSticky(getTitle) {
        $('#documentText').val('').empty();
        var stickyNotes = document.getElementById('stickyNotes');
        $('#stickyNotes').css('display', 'block');
        $('#documentTitle').val(getTitle);
        stickyNotes.style.left = "-360px";
        stickyNotes.style.top = "25px";
        var myBlob = new Blob([' '], {
            type: "text/plain"
        });
        var file = new File([myBlob], getTitle);
        uploadStickyNotes(file, getTitle);
        dragElement(document.getElementById(("stickyNotes")));
    }

    /*
     * @purpose: Create sticky Notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */

    $scope.createSticky = function () {
        if (window.location.hash.indexOf("#!/") >= 0) {
            var modalPath = '../scripts/common/modal/views/getNotesName.modal.html';
        } else {
            var modalPath = 'scripts/common/modal/views/getNotesName.modal.html';
        }
        getNotesuibModalInstance = $uibModal.open({
            templateUrl: modalPath,
            size: 'md',
            scope: $scope,
            backdrop: 'static',
            windowClass: 'custom-modal sticky-prompt-modal',
        }).result.catch(function (res) {
            if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
            } else if (res) {
                throw res;
            } else {

            }
        }, function () {
        });
    };
    /*
     * @purpose: Display content of sticky notes
     * @created: 30 mar 2018
     * @returns: no 
     * @param : clicked document object
     * @author: varsha
     */
    function displayDocumentOnView(doc) {
        $('#documentText').html('');
        $('#documentTitle').val(doc.title);
        $scope.documentId = doc.docId;
        var docId = doc.docId;
        //var docName = doc.title;
        var docType = doc.type;
        var docTitle = doc.title;
        var params = {
            "docId": docId,
            "token": $scope.token
        };
        TopPanelApiService.downloadDocument(params).then(function (response) {
            var blob = new Blob([response.data], {
                type: "application/" + docType,
            });
            var reader = new FileReader();
            reader.addEventListener('loadend', function (e) {
                var text = e.srcElement.result;
                $('#documentText').val(text);
            });
            // Start reading the blob as text.
            var x = reader.readAsText(blob);// jshint ignore:line
            $scope.loadDocumentContent = false;
        }, function () {
            $scope.loadDocumentContent = false;
            $('#stickyNotes').css('display', 'none');
            HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
        });
    }
    /*
     * @purpose: on change display dropdown
     * @created: 30 mar 2018
     * @returns: no 
     * @author: varsha
     */

    $scope.changeDropDown = function (checkedValue) {
        var ele = document.getElementById('tableDetail');
        if ((checkedValue == 'table')) {
            $('.tiles-style').css('display', 'none');
            ele.classList.remove("hidden");
        } else if (checkedValue == 'icon') {
            $('.tiles-style').css('display', 'block');
            ele.classList.add("hidden");
        }
    };

    /*
     * @purpose: Delete sticky
     * @created: 03 mar 2018
     * @returns: no 
     * @author: varsha
     */

    $scope.onClickStickyNotesDelete = function () {
        var params = {
            "token": $scope.token,
            "docId": $scope.doc.docId
        };
        if ($scope.doc.docId) {
            TopPanelApiService.deleteDocument(params).then(function () {
                getListOfDocuments();
                $('#rightClickMenu').css('display', 'none');
                $("#context_menu").css('display', 'none');
                $('#stickyNotes').css('display', 'none');
                HostPathService.FlashSuccessMessage('SUCCESSFUL DELETE STICKY NOTES', '');
            }, function (e) {
                HostPathService.FlashErrorMessage('ERROR DELETE STICKY NOTES', e.responseMessage);
            });
        }
    };
    /*  @purpose: full text search *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.fullTextSearch = function (e) {
        if (e.target.value == '') {
        	pageMydocumentSticky = 1;
            getListOfDocuments();
        } else {
            var obj = {
                "token": $scope.token,
                "keyword": e.target.value,
                "docFlag": 5
            };
            var obj2 = {
                "token": $scope.token,
                "searchKeyWord": e.target.value,
                "docFlag": 5
            };
            var obj4 = {
                "token": $scope.token,
                "searchKeyWord": e.target.value
            };
            var obj3 = {
                "token": $scope.token,
                "keyword": e.target.value,
            };
            TopPanelApiService.fullTextSearchDocument(obj).then(function (response) {
                $scope.mydocumentSticky = response.data.result;
                $scope.mydocumentStickyLength = response.data.paginationInformation.totalResults;
                TopPanelApiService.fullTextSearchSharedDocument(obj2).then(function (response2) {
                    $scope.mysharedSticky = response2.data.result;
                    $scope.mysharedStickyLength = response2.data.paginationInformation.totalResults;
                    $scope.documents = $.merge($.merge([], response.data.result), response2.data.result);
                    TopPanelApiService.fullTextSearchSharedDocument(obj4).then(function (response4) {
                        $scope.mysharedUploaded = response4.data.result;
                        $scope.mysharedUploadedLength = response4.data.paginationInformation.totalResults;
                        $scope.documents = $.merge($.merge([], $scope.documents), response4.data.result);
                        TopPanelApiService.fullTextSearchDocument(obj3).then(function (response3) {
                            $scope.mydocumentUploaded = response3.data.result;
                            $scope.mydocumentUploadedLength = response3.data.paginationInformation.totalResults;;
                            $scope.documents = $.merge($.merge([], $scope.documents), response3.data.result);
                        });
                    });
                });

            }, function (e) {
                HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
            });
        }
    };
    /* * @purpose: List By data dropdown*
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */
    $scope.changeListByDropDown = function (checkedValue) {
        getListOfDocuments(checkedValue);
    };

    /* * @purpose: on click of document remove context menu*
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    document.addEventListener('click', function () {
        if ($('#rightClickMenu').css('display') == 'block') {
            $('#rightClickMenu').css('display', 'none');

        }
    });
    /* * @purpose: Icon path for different dashboard *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.pathChange = function (type) {
        if (window.location.hash.indexOf("#!/") < 0 && type != '	') {
            return true;
        }
    };
    /* * @purpose: scroll bar to the folders panels *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        /*Custom Scroll Bar*/
        $('#dataList').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        });
        $('#dataList').css("height", "200px");
    });

    /* * @purpose: Get list of all user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    function allUserList() {
        var obj = {
            "token": $scope.token
        };
        TopPanelApiService.getUserListing(obj).then(function (response) {
            $scope.userList = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: Display block to share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.shareUser = function () {
        allUserList();
        $("#shareBoxWrapper").css('display', 'block');
    };

    /* * @purpose: On select of any user  *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.onSelectUser = function ($item, $model, $label) {/*jshint unused:false*/
        $('#onclickSharewithUser').css('pointer-events', 'all');
        $scope.userValue = $item;
    };

    /* * @purpose: share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.shareWithUser = function (permission) {
        if ($scope.userValue.userId == undefined) {
            HostPathService.FlashErrorMessage('ERROR', 'Please select user first to share');
            return false;
        }
        $('#sharewithuser').val('');
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "userId": $scope.userValue.userId,
            "permission": permission,
            "docId": $scope.doc.docId

        };
        TopPanelApiService.shareDocumentWithUser(obj).then(function () {
            documentAssociation();
            $scope.userValue.userId == undefined;// jshint ignore:line
            $('#onclickSharewithUser').css('pointer-events', 'none');
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };

    $scope.removeUser = function (userDetail) {
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "userId": userDetail.userId,
            "permission": 1,
            "docId": $scope.doc.docId,
            "comment": " "
        };
        TopPanelApiService.unshareDocumentWithUser(obj).then(function () {
            documentAssociation();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };
    /* * @purpose: Listing all shared document *
     *   @created: 04 apr 2018
     *   @returns: no
     *   @author: varsha */
    $scope.sharedDocumentList = [];
    function getAllSharedDocument(alldocument, orderIn) {
        var obj = {
            "token": $scope.token,
            "docFlag": 5,
            "pageNumber": pageMysharedSticky,
            "recordsPerPage": 9,
            "orderBy": 'uploadedOn',
            "orderIn": 'desc'
        };
        var obj1 = {
            "token": $scope.token,
            "pageNumber": pageMysharedUploaded,
            "recordsPerPage": 9,
            "orderBy": 'uploadedOn',
            "orderIn": 'desc'
        };
        if (caseId != undefined) {
            obj.caseId = caseId;
            obj1.caseId = caseId;
        }
        if (orderIn != undefined) {
            obj.orderIn = orderIn;
            obj1.orderIn = orderIn;
        }
        TopPanelApiService.sharedDocument(obj).then(function (response) {
            $scope.mysharedSticky = response.data.result;
            TopPanelApiService.sharedDocument(obj1).then(function (response1) {
                $scope.mysharedUploaded = response1.data.result;
                shareddocTotalLength = response.data.paginationInformation.totalResults + response1.data.paginationInformation.totalResults;
                $scope.totalDocumentLength = stickydocTotalLength + shareddocTotalLength + uploaddocTotalLength;
                $scope.sharedDocumentList = $.merge($.merge([], response.data.result), response1.data.result);
                $scope.documents = $.merge($.merge([], alldocument), $scope.sharedDocumentList);
                $scope.loadDocument = false;
                $scope.mydocumentStickyLength = stickydocTotalLength;
                $scope.mydocumentUploadedLength = uploaddocTotalLength;
                $scope.mysharedUploadedLength = response1.data.paginationInformation.totalResults;
                $scope.mysharedStickyLength = response.data.paginationInformation.totalResults;
            });
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: Display map case block *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */
    $scope.shareCase = function () {
        $("#shareCaseWrapper").css('display', 'block');
        allCaseList();
    };

    /* * @purpose: Get list of all case *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */
    allCaseList();

    function allCaseList() {
        var obj = {
            "token": $scope.token
        };
        TopPanelApiService.listCases(obj).then(function (response) {
            $scope.caseDetail = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: If any case get selected *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.onSelectCase = function ($item, $model, $label) {// jshint ignore:line
        console.log($item, '$scope.caseValue$scope.caseValueitrm');
        $scope.caseValue = $item;
    };
    /* * @purpose: share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.linkToCase = function (case_Id, doc_Id) {
        if ($scope.caseValue == undefined && case_Id == undefined && doc_Id == undefined) {
            HostPathService.FlashErrorMessage('ERROR', 'Please select case first to link');
            return false;
        }
        $('#linkWithUser').val('');
        $scope.shareLoader = true;
        if (case_Id != undefined && doc_Id != undefined) {
            var obj = {
                "token": $scope.token,
                "caseId": case_Id,
                "permission": 1,
                "docId": doc_Id
            };
        } else {
            var obj = {
                "token": $scope.token,
                "caseId": $scope.caseValue.caseId,
                "permission": 1,
                "docId": $scope.doc.docId
            };
        }
        TopPanelApiService.shareDocumentWithCase(obj).then(function () {
            documentAssociation();
            $scope.caseValue = undefined;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };
    /* * @purpose: unlink the case*
     *   @created: 06 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.removeCase = function (caselist) {
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "caseId": caselist.caseId,
            "permission": 1,
            "docId": $scope.doc.docId
        };
        TopPanelApiService.unshareDocumentWithCase(obj).then(function () {
            documentAssociation();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };

    function documentAssociation() {
        $scope.shareLoader = true;
        $scope.docUserList = [];
        $scope.caseList = [];
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId
        };
        TopPanelApiService.getCaseMapping(obj).then(function (response) {
            if (response.data.responseMessage == "Permission is denied or Document not found") {
                $scope.permissionDocument = "Read Only";
                var elem = $('#toShareLinkTab').find('ul');
                $(elem[0]).css('pointer-events', 'none');
                $('#context_menu').css('display', 'block');
                $('#deleteDocument').css('display', 'none');
            } else {
                $scope.permissionDocument = "";
                var elem = $('#toShareLinkTab').find('ul');
                $(elem[0]).css('pointer-events', 'all');
                $('#context_menu').css('display', 'block');
                $scope.docUserList = response.data.docUserList;
                $scope.caseList = response.data.caseList;
            }
            $scope.shareLoader = false;
            $scope.loadDocument = false;
        }, function (e) {
            $scope.loadDocument = false;
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: comment section *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.changeCommentBox = function ($event, comment) {// jshint ignore:line
        if ($event.keyCode == 13) {
            var tb = document.getElementById("comentInput");
            $scope.commentDescription = tb.value;
            $('#comentInput').val('');
            addComment();
        }
    };
    /* * @purpose: add comment section *
     *   @created: 07 apr 2018
     *   @returns: no
     *   @author: varsha */

    function addComment() {
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
            "commentdesc": $scope.commentDescription
        };
        TopPanelApiService.addComment(obj).then(function () {
            listDocComment();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }

    /* * @purpose: list all added comment section *
     *   @created: 07 apr 2018
     *   @returns: no
     *   @author: varsha */

    function listDocComment() {
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
        };
        TopPanelApiService.listDocComment(obj).then(function (response) {
            $scope.commentDes = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    var caseId;
    $scope.displayFiles = function (actCaseDetail) {
        if (actCaseDetail.caseId) {
            caseId = actCaseDetail.caseId;
            getListOfDocuments();
            $("#filterByCase").addClass("active");
            $("#removefilterByCase").removeClass("active");
        } else {
            caseId = undefined;
            getListOfDocuments();
            $("#removefilterByCase").addClass("active");
            $("#filterByCase").removeClass("active");
        }
    };

    /* * @purpose: close share user and map case block *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.closeBox = function (name) {
        if (name == 'shareCase') {
            $("#shareCaseWrapper").css('display', 'none');
        } else if (name == 'shareDoc') {
            $("#shareBoxWrapper").css('display', 'none');
        } else if (name == 'contextMenu') {
            $scope.shareLoader = false;
            $("#context_menu").css('display', 'none');
            getListOfDocuments();
        }
    };
    /* * @purpose: scroll bar to the Share And Link panels *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        $('.data-table').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.data-table').css('height', '200px');
    });

    $(document).ready(function () {
        $('.user-list-wrapper .dropdown-menu').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.user-list-wrapper .dropdown-menu').css('height', '200px');
    });
    /* * @purpose: scroll bar to the Comment panel *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        $('.top-comment-wrapper').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.top-comment-wrapper').css('height', '200px');
    });
    $(document).ready(function () {
        $('.folder-wrapper').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.folder-wrapper').css('height', '230px');
    });
    /* * @purpose: draggable sticky notes *
    *   @created: 11 apr 2018
    *   @returns: no
    *   @author: varsha */

    function dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "header")) {
            /* if present, the header is where you move the DIV from:*/
            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
    window.onhashchange = myFunction;
    function myFunction() {
        if (location.hash == "#/domain") {
            $rootScope.showMyClipboard = false;
        }
    }

    /*  $scope.caseWithId = function(caseValue){
             return caseValue.name + ' (' + caseValue.caseId + ')';
     }*/

    /** * END --- Sticky Notes ** */

}