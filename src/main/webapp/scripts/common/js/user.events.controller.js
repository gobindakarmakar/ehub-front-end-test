'use strict';
elementApp
	   .controller('UserEventsController', userEventsController);

		userEventsController.$inject = [
			'$scope',
			'$rootScope',
			'$state', 
			'$window',
			'TopPanelApiService',
			'HostPathService',
			'DiscoverApiService',
			'Flash',
			'$uibModal'
		];
	    function userEventsController(
	    		$scope, 
	    		$rootScope, 
	    		$state, 
	    		$window,
	    		TopPanelApiService,
	    		HostPathService,
	    		DiscoverApiService,
	    		Flash,
	    		$uibModal){
	    	
			$scope.token = $rootScope.ehubObject.token;
			var params = {
		    	token: $scope.token		    	
		    };
			
			// var monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
	    	// var date = new Date();
	    	/*$scope.day = date.getDate();
	    	$scope.month = monthNames[date.getMonth()];
	    	$scope.year = date.getFullYear();*/
	    	$scope.currentEvents = [];
	    	$scope.current = [];
	    	$scope.allEvents = [];
	    	$scope.next = [];
	    	$scope.data = [];
			/*
		     * @purpose: get user events
		     * @created: 13 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
	    	/*TopPanelApiService.getUserEvents(params).then(function(response){
				var monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    	    	var date = new Date();
    	    	$scope.day = date.getDate();
    	    	$scope.month = monthNames[date.getMonth()];
    	    	$scope.year = date.getFullYear();
    	    	$scope.currentEvents = [];
    	    	$scope.current = response.data['currentEvents'];
    	    	$scope.allEvents = response.data['allEvents'];
    	    	$scope.next = response.data['nextEvents'];*/
    	    	
    	    	/*shows only the first current event - not sure on how to show multiple events on the same day*/
	    	/*$scope.data = response.data['currentEvents'][0];
			}, function(error){
				console.log(error);
			});*/
			/*$rootScope.dateSelected;*/
			var caseId =  getParameterByName('caseId');
			if(caseId != undefined && caseId != null && caseId != ''){
	    		$scope.showModal = function(date){
	    			params = {
	 		        	"caseId": caseId,
	 		        	"token": $scope.token
	 		        };
	    			var dateSelected = new Date(date);
	    			$rootScope.dateSelected = dateSelected;
	    			TopPanelApiService.getNotificationAlert(params).then(function(response){
	    				if (response.data != null && response.data.length > 0) {
	    		    		var i = 0, dataLength = response.data.length;
	    		    		function loop() {/*jshint ignore:line*/
	    		    			var data = response.data[i];
	    		    			var dataDate = new Date(data.createdOn);
	    		    			
	    			    	 	if(dateSelected.toDateString() === dataDate.toDateString()) {
	    			    	 		showSuccessAlert("INFO", data.message);/*jshint ignore:line*/
	    			    	 		i++;
	    			    		    if( i < dataLength ){
	    			    		        setTimeout(loop, 2000);
	    			    		    }
	    			    	 	} else {
	    			    	 		i++;
	    			    	 		if( i < dataLength ){
	    			    		        setTimeout(loop, 0);
	    			    		    }
	    			    	 	}
	    		    		}
	    		    		loop();
	    		    	}
	    			}, function(){
	    			});
    	    	};
			}
	    		
    		function getParameterByName(name, url) {
    		    if (!url) {
    		      url = window.location.href;
    		    }
    		    name = name.replace(/[\[\]]/g, "\\$&");
    		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    		        results = regex.exec(url);
    		    if (!results) {return null;}
    		    if (!results[2]) {return '';}
    		    return decodeURIComponent(results[2].replace(/\+/g, " "));
    		}
    		
			var userListParams = {
				token: $scope.token
		    };
			var userList = [];
		 	/* Get List of Users */
			DiscoverApiService.getUserList(userListParams).then(function(response){
		 		var usersLength = response.data.paginationInformation.totalResults;
		 		var data = {
	 				token: $scope.token,
	 				pageNumber: 1,
	 				recordsPerPage: usersLength 
		 		};
		 		if(usersLength > 10){
			 		DiscoverApiService.getUserList(data).then(function(response1){
			 			userList = response1.data.result;
			 		},function(){
				 	});
		 		}else{
		 			userList = response.data.result;
		 		}
		 	},function(){
		 	});
    		/*
    		 * @purpose: get Calendar events List
    		 * @created: 5th Jun 2018
    		 * @author: Swathi
    		 */
    		var params = {
				token: $scope.token
    		};
    		function getCalendarEventsList(){
    			$scope.eventText = 'Create an Event';
    			event = {};
    			TopPanelApiService.getCalendarList(params).then(function(response){
    				var eventsLength = response.data.paginationInformation.totalResults;
    				if(eventsLength > 10){
    					params = {
							token: $scope.token,
							recordsPerPage: eventsLength,
							pageNumber: 1
			    		};
    					TopPanelApiService.getCalendarList(params).then(function(response1){
    						$scope.calendarList = response1.data.result;
    			 		},function(){
    				 	});
    		 		}else{
    		 			$scope.calendarList = response.data.result;
    		 		}
    				$('#eventsListId').css({'height': '450px'});
    				$('#eventsListId').mCustomScrollbar({
      					axis : "y",
      					theme : "minimal"
      				});
    			}, function(){
    				
    			});
    		}
    		getCalendarEventsList();
    		
    		/*
    		 * @purpose: participated Event List
    		 * @created: 7th Jun 2018
    		 * @author: Swathi
    		 */
    		function getParticipatedEventList(){
    			$scope.eventText = 'Create an Event';
    			event = {};
    			TopPanelApiService.participatedEventList(params).then(function(response){
    				var eventsLength1 = response.data.paginationInformation.totalResults;
    				if(eventsLength1 > 10){
    					params = {
							token: $scope.token,
							recordsPerPage: eventsLength1,
							pageNumber: 1
			    		};
    					TopPanelApiService.participatedEventList(params).then(function(response1){
    						$scope.ParticipatedEventList = response1.data.result;
    			 		},function(){
    				 	});
    		 		}else{
    		 			$scope.ParticipatedEventList = response.data.result;
    		 		}
    				$('#participatedListId').css({'height': '450px'});
    				$('#participatedListId').mCustomScrollbar({
      					axis : "y",
      					theme : "minimal"
      				});
    			}, function(){
    				
    			});
    		}
    		getParticipatedEventList();
    		
    		$scope.calendarSelected = 'Created';
    		$scope.eventText = 'Create an Event';
     	    var event;
    		$scope.calendarDropDown = function(value){
    			$scope.eventText = 'Create an Event';
    			event = {};
    			$scope.calendarSelected = value;
    			if(value == 'Participated'){
	    			angular.forEach($scope.calendarList, function(eventItem){
	    				if(eventItem.checked){
	    					eventItem.checked = false;}
	    	    	});}
    			if(value == 'Created'){
	    			angular.forEach($scope.ParticipatedEventList, function(eventItem){
	    				if(eventItem.checked){
	    					eventItem.checked = false;}
	    	    	});
    			}
    		};
    		$scope.participantsSelected = '';
    		var isRemove = false;
    		$scope.participantsDropDown = function(value){
    			$scope.participantsSelected = value;
    			if($scope.participantsSelected == 'Add'){
    				isRemove = false;
    				openParticipantsEventModal();
    			}else if($scope.participantsSelected == 'Remove'){
    				isRemove = true;
    				openParticipantsEventModal();
    			}
    		};
    		/*
    		 * @purpose: Open create an event modal 
    		 * @created: 4th Jun 2018
    		 * @returns: no
    		 * @author: Swathi
    		 */
    		var isEdit = false;
    		$scope.openCreateEvent = function(eventText){
    			if(eventText == 'Create an Event'){
    				isEdit = false;
    			}else{
    				isEdit = true;
    			}
    			if (window.location.hash.indexOf("#!/") >= 0) {
    	    		var modalPath ='../scripts/common/modal/views/create.calendar.event.modal.html';
    			}else{
    				var modalPath ='scripts/common/modal/views/create.calendar.event.modal.html';
    	        }
    			var createEventModalInstance = $uibModal.open({
    	    		templateUrl: modalPath ,
    	    		size: 'md',
    	    	    controller: 'CreateEventController',
    	    		backdrop: 'static',
    	    		windowClass: 'custom-modal upload-media-modal',
    	    		resolve: {
    	    			calendarEvent: function(){
    	    				if(event){
    	    					return event;}
    	    				else{
    	    					return false;
    	    				}
    	    			},
    	    			isEdit: function(){
    	    				return isEdit;
    	    			},
    	    			userList: function(){
    	    				return userList;
    	    			}
    	    		}
    	    	});
    			createEventModalInstance.result.then(function(response) {
    				if(response == 'success'){
    					HostPathService.FlashSuccessMessage('SUCCESSFUL CREATE EVENT', 'Successfully created event');
					}
					if(response == 'updateSuccess'){
    					HostPathService.FlashSuccessMessage('SUCCESSFUL UPDATE EVENT', 'Successfully updated event');
					}
					 getCalendarEventsList();
    	        }, function(error) {
    	            if (error !== 'close') {
    	            	if(error == 'failure'){
    	            		HostPathService.FlashErrorMessage('ERROR CREATE EVENT', 'Failed to create event');
						}
						if(error == 'updateFailure'){
    	            		HostPathService.FlashErrorMessage('ERROR UPDATE EVENT', 'Failed to update event');
						} }
    	        });
    	    };
    	    /*
    		 * @purpose: toggle event check box selection 
    		 * @created: 6th Jun 2018
    		 * @params: position, list, eventData
    		 * @author: Swathi
    		 */
    	    $scope.toggleEventSelection = function(position, list, eventData){
    	    	event = eventData;
    	    	$scope.eventText = 'Create an Event';
    	    	$scope.participantsSelected = '';
    	    	angular.forEach(list, function(eventItem, k){
    	    		if(position != k){
    	    			eventItem.checked = false;
    	    		}
    	    		if(eventItem.checked == true){
	    				$scope.eventText = 'Update an Event';
	    			}
    	    	});
    	    };
    	    
    	    /*
    		 * @purpose: Open Participants Event Modal 
    		 * @created: 6th Jun 2018
    		 * @author: Swathi
    		 */
    		function openParticipantsEventModal(){
    			if (window.location.hash.indexOf("#!/") >= 0) {
    	    		var modalPath ='../scripts/common/modal/views/participants.event.modal.html';
    			}else{
    				var modalPath ='scripts/common/modal/views/participants.event.modal.html';
    	        }
    			var participantsEventModalInstance = $uibModal.open({
    	    		templateUrl: modalPath ,
    	    		size: 'md',
    	    	    controller: 'ParticipantsEventController',
    	    		backdrop: 'static',
    	    		windowClass: 'custom-modal upload-media-modal',
    	    		resolve: {
    	    			eventId: function(){
    	    				if(event){
    	    					return event.notificationId;}
    	    				else{
    	    					return false;
    	    				}
    	    			},
    	    			isRemove: function(){
    	    				return isRemove;
    	    			},
    	    			userList: function(){
    	    				return userList;
    	    			}
    	    		}
    	    	});
    			participantsEventModalInstance.result.then(function(response) {
    				$scope.participantsSelected = '';
    				if(response == 'success'){
    					HostPathService.FlashSuccessMessage('SUCCESSFUL ADD PARTICIPANTS', 'Successfully added participants');
					}
					if(response == 'removeSuccess'){
    					HostPathService.FlashSuccessMessage('SUCCESSFUL REMOVE PARTICIPANTS', 'Successfully removed participants');
					}
					 getCalendarEventsList();
    	        }, function(error) {
    	            if (error !== 'close') {
    	            	if(error == 'failure'){
    	            		HostPathService.FlashErrorMessage('ERROR ADD PARTICIPANTS', 'Failed to add participants');
						}
						if(error == 'removeFailure'){
    	            		HostPathService.FlashErrorMessage('ERROR REMOVE PARTICIPANTS', 'Failed to remove participants');
						} }
    	            $scope.participantsSelected = '';
    	        });
    	    }
	    }