'use strict';
elementApp
.controller('EntityVisualiserController', ['$scope', '$http', 'getUrl', 'getTitle', 'getDetails', '$uibModalInstance','EnrichApiService', function($scope, $http, getUrl, getTitle, getDetails, $uibModalInstance,EnrichApiService){
	$scope.entityLoader = true;
	$scope.details = getDetails;
	$scope.url = getUrl;
	$scope.title = getTitle;
	$scope.selectedItem = 'English';
	$scope.languageTransition =['Afrikaans','Arabic','Bangla','Bosnian (Latin)','Bulgarian','Cantonese (Traditional)','Catalan','Chinese SimplifiedCS','Chinese TraditionalCS','Croatian','Czech','Danish','Dutch','English','Estonian','Fijian','Filipino','Finnish','French','German','Greek','Haitian Creole','Hebrew','Hindi','Hmong Daw','Hungarian','Icelandic','Indonesian','Italian','Japanese','Kiswahili','Klingon','Klingon (plqaD)','Korean','Latvian','Lithuanian','Malagasy','Malay','Maltese','Norwegian','Persian','Polish','Portuguese','Queretaro Otomi','Romanian','Russian','Samoan','Serbian (Cyrillic)','Serbian (Latin)','Slovak','Slovenian','Spanish','Swedish','Tahitian','Tamil','Thai','Tongan','Turkish','Ukrainian','Urdu','Vietnamese','Welsh','Yucatec Maya'];
 	/*
 	*@purpose: load entity visualiser by using displacy
 	*@created: 18 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
	$(document).ready(function(){
 		
		// Your API
 		
		EnrichApiService.getEntityVisualizer({"text":getDetails}).then(function (response) {
			$scope.entityLoader = false;
			const api = '';
	   		 
	       	
       	  	// Init displaCY ENT
       	  	const displacy = new displaCyENT(api, {
       	  	    container: '#displacy'
       	  	});
       	
   	  		const text = response.data.text;
   	  		const spans = response.data.entities;
       	
       	  	// Entities to visualise
       	  	const ents = ['person', 'organization', 'location', 'date'];
       	
       	  	// Parse text
				 displacy.render(text, spans, ents);
		}).catch(function () {
			$scope.entityLoader = false;
		});
	 	
	 });
	 	 // --------------------------------------------------------------------------
		    /**
			 * Function to show entity visualization
			 */     
			function loadEntity (text, entityTypes){/*jshint ignore:line*/
		    	
		          
		        }
 	
 	/*
 	*@purpose: close entity visualiser modal
 	*@created: 18 aug 2017
 	*@params: none
 	*@returns: none
 	*@author: swathi
 	*/
 	$scope.closeEntityModal = function(){
 		$uibModalInstance.close('close');
 	};
 	
}]);
