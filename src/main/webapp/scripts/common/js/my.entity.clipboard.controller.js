'use strict';
elementApp
    .controller('MyEntityClipBoardController', MyEntityClipBoardController);

MyEntityClipBoardController.$inject = [
    '$scope',
    '$rootScope',
    '$timeout',
    '$uibModal',
    'TopPanelApiService',
    'HostPathService',
    'Flash',
    '$uibModalStack',
    'CommonService',
    '$compile',
    '$filter',
    'EHUB_API'
];

function MyEntityClipBoardController(
    $scope,
    $rootScope,
    $timeout,
    $uibModal,
    TopPanelApiService,
    HostPathService,
    Flash,
    $uibModalStack,
    CommonService,
    $compile,
    $filter,
    EHUB_API) {

    /** * START --- Sticky Notes *** */

    $scope.token = $rootScope.ehubObject.token;
    $scope.documents = [];
    $scope.dataPopObjectEntityClipBoard = {
        overideSearchedInputSourceEdit:'',
        overideSearchedInputURLEdit:'',
        overideSearchedInputDateEdit:'',
        selectedSourceTitle:''
    };
    $scope.stickySavedText = '';
    $scope.source_list_clipboard = jQuery.extend(true, [], $rootScope.sourcesListsData);
    var source_list_clipboard_for_edit = jQuery.extend(true, [], $rootScope.sourcesListsData);
    // topPanelApiService.entityselection ='';
    $scope.clipBoardObject = TopPanelApiService.clipBoardObject;
    $scope.clipBoardObject.modal_postion.top = TopPanelApiService.clipBoardObject.modal_postion.top ? TopPanelApiService.clipBoardObject.modal_postion.top :'';
    /*
     * @purpose: get list of documents
     * @created: 20 sep 2017
     * @params: null
     * @return: no
     * @author: swathi
     */
    $scope.stickyNotesDocs = [];
    $scope.checkedValueDisplay = 'icon';
    $scope.checkedValueListBy = 'desc';

    /*
     * @purpose: Initialize page number
     * @created: 2 april 2018
     * @return: no
     * @author: varsha
     */

    var pageMydocumentSticky = 1;
    var pageMydocumentUploaded = 1;
    var pageMysharedSticky = 1;
    var pageMysharedUploaded = 1;

    /*
     * @purpose: declare length * 
     * @created: 2 april 2018  
     * @return: no
     * @author: varsha
     */
    var stickydocTotalLength;
    var shareddocTotalLength;
    var uploaddocTotalLength;
    $scope.evidenceReportAddToPageFromclip =TopPanelApiService.getEvidenceArray;
    /*
     * @purpose: On page change for mysticky notes
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmydocumentSticky = function (page) {
        pageMydocumentSticky = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for myDocument uploaded
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmydocumentUploaded = function (page) {
        pageMydocumentUploaded = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for shared sticky notes
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmysharedSticky = function (page) {
        pageMysharedSticky = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On page change for shared uploaded document
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    $scope.pageChangedmysharedUploaded = function (page) {
        pageMysharedUploaded = page;
        getListOfDocuments();
    };
    /*
     * @purpose: On click of Sticky from entity call this controller
     * @created: 11 april 2018  
     * @return: no
     * @author: varsha
     */
    if(TopPanelApiService.entityselection){
        getListOfDocuments();
    }
    /*
     * @purpose: Get all mysticky notes and uploaded document
     * @created: 1 april 2018  
     * @return: no
     * @author: varsha
     */
    
    function getListOfDocuments(event,orderIn) {
        $scope.loadDocument = true;
        $scope.documents = [];
        $scope.mydocumentUploaded = [];
        if (orderIn != undefined) {
            var params2 = {
                "token": $scope.token,
                "docFlag": 5,
                "pageNumber": pageMydocumentSticky,
                "orderIn": orderIn,
                "orderBy": 'uploadedOn',
                "recordsPerPage": 9,
                "entityId":$rootScope.entityObj["@identifier"]
            };
        } else {
            var params2 = {
                "token": $scope.token,
                "docFlag": 5,
                "pageNumber": pageMydocumentSticky,
                "orderIn": 'desc',
                "orderBy": 'uploadedOn',
                "recordsPerPage": 9,
                "entityId":$rootScope.entityObj["@identifier"]
            };
        }
        if (caseId != undefined) {
            params2.caseId = caseId;
        }
        if(TopPanelApiService.entityselection){
            params2.docSection = TopPanelApiService.entityselection;
        }
        TopPanelApiService.getAllDocuments(params2).then(function (response2) {
        	$scope.stickydocLength = response2.data.paginationInformation.totalResults;
            stickydocTotalLength = response2.data.paginationInformation.totalResults;
            $scope.mydocumentStickyLength = response2.data.paginationInformation.totalResults;
            if(!TopPanelApiService.entityselection && response2.data.result.length > 0){
                $scope.mydocumentSticky = response2.data.result.filter(function(val){
                    return !val.docSection;
                });
            }else{
                $scope.mydocumentSticky = response2.data.result;

            }
            $scope.documents = response2.data.result;
            var params3 = {
                "token": $scope.token,
                "pageNumber": pageMydocumentUploaded,
                "recordsPerPage": 9,
                "orderBy": 'uploadedOn',
                "orderIn": 'desc',
                "entityId":$rootScope.entityObj["@identifier"]
            };
            if (orderIn != undefined) {
                params3.orderIn = orderIn;
            }

            if (caseId != undefined) {
                params3.caseId = caseId;
            }
            if(TopPanelApiService.entityselection){
                params3.docSection = TopPanelApiService.entityselection;
            }
            TopPanelApiService.getAllDocuments(params3).then(function (response3) {
                uploaddocTotalLength = response3.data.paginationInformation.totalResults;
                $scope.mydocumentUploadedLength = response3.data.paginationInformation.totalResults;
                if(!TopPanelApiService.entityselection && response3.data.result.length > 0){
                    $scope.mydocumentUploaded = response3.data.result.filter(function(val){
                        return !val.docSection;
                    });
                }else{
                    $scope.mydocumentUploaded = response3.data.result;

                }
                $scope.documents = $.merge($.merge([], $scope.documents), response3.data.result);
                $scope.loadDocument = false;
                $scope.totalDocumentLength = stickydocTotalLength + uploaddocTotalLength;
//                getAllSharedDocument($scope.documents, orderIn);
                var params4 = {
                    "token": $scope.token,
                    "docFlag": 6,
                    "pageNumber": pageMydocumentUploaded,
                    "orderIn": 'desc',
                    "orderBy": 'uploadedOn',
                    "recordsPerPage": 9,
                    "entityId":$rootScope.entityObj["@identifier"]
                };
                TopPanelApiService.getAllEvidenceDocuments(params4).then(function (response4) {
                    uploaddocTotalLength = response4.data.paginationInformation.totalResults;
                    $scope.evidenceReportAddToPageFromclipLength = response4.data.paginationInformation.totalResults;
                    $scope.evidenceReportAddToPageFromclip = response4.data.result;
                    $scope.evidenceReportAddToPageFromclip.forEach(function(val,key){
                        if(val.docSection && TopPanelApiService.entityselection && (val.docSection == TopPanelApiService.entityselection)){
                            $scope.mydocumentUploaded.push(val);
                        }
                    });
                    $scope.documents = $.merge($.merge([], $scope.documents), response4.data.result);
                    $scope.loadDocument = false;
                    $scope.totalDocumentLength = stickydocTotalLength + uploaddocTotalLength;
                    //getAllSharedDocument($scope.documents, orderIn);
                });
            });
        });
    }
    $('body').off('click', '#entityClipboard', function(e){
        e.stopPropagation();
        getListOfDocuments(e);
    });
    $('#entityClipboard').unbind().click( function(e){
        e.stopPropagation();
        getListOfDocuments(e);
    });
    
   
    
    /*
     * @purpose: download document
     * @created: 20 sep 2017
     * @params: doc(object)
     * @return: no
     * @author: swathi
     */
    $scope.downloadDocument = function downloadDocument(doc, type) {
        console.log
        var docId = doc.docId;
        //var docName = doc.docName;
        var docType = doc.type;
        var docTitle = doc.title;
        var params = {
            "docId": docId,
            "token": $scope.token
        };
        TopPanelApiService.downloadDocument(params).then(function (response) {
            var blob = new Blob([response.data], {
                type: "application/" + docType,
            });
            if (type == 'download') {
                if (docType == '') {
                    saveAs(blob, (docTitle || doc.docName)+ '.txt');// jshint ignore:line
                } else {
                    saveAs(blob, (docTitle || doc.docName) + '.' + docType);// jshint ignore:line
                }
                HostPathService.FlashSuccessMessage('SUCCESSFUL DOWNLOAD DOCUMENT', 'Successfully downloaded document with file title: ' + docTitle);
            } else {
                var reader = new FileReader();
                reader.addEventListener('loadend', function (e) {
                    var text = e.srcElement.result;/*jshint unused:false*/
                });
                // Start reading the blob as text.
                var x = reader.readAsText(blob);/*jshint unused:false*/
            }
        }, function () {
            HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
        });
    };
    $scope.showClipboardUploadFile = false;
    /*
     * @purpose: open add media modal
     * @created: 20 sep 2017
     * @params: null
     * @return: no
     * @author: swathi
     */
    $scope.clipboardUploadFile = function () {
    	
        if (window.location.hash.indexOf("#!/") >= 0) {
            var modalPath = '../scripts/common/modal/views/add.media.modal.html';
        } else {
            var modalPath = 'scripts/common/modal/views/add.media.modal.html';
        }
        var addMediaModalInstance = $uibModal.open({
            templateUrl: modalPath,
            controller: 'AddMediaModalController',
            backdrop: 'static',
            windowClass: 'custom-modal upload-media-modal'
        });

        addMediaModalInstance.result.then(function (response) {
            getListOfDocuments();
            HostPathService.FlashSuccessMessage('SUCCESSFUL UPLOAD DOCUMENT', 'Successfully uploaded document with file title: ' + response);
        }, function (error) {
            if (error !== 'close') {
                HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Failed to upload document with file title: ' + error);
            }
        });
    };
    /*
     * @purpose: upload sticky notes
     * @created: 09 mar 2018
     * @params: file(object)
     * @returns: no
     * @author: swathi
     */
    function uploadStickyNotes(file, name) {
        var params = {
            "token": $scope.token,
            "fileTitle": name,
            "remarks": "",
            "docFlag": 5,
            "docName": name,
            "entityId":$rootScope.entityObj["@identifier"],
            "entityName": $rootScope.entityObj.name,
            "entitySource":""
        };
        var data = {
            uploadFile: file
        };
        if(TopPanelApiService.entityselection){
            params.docSection = TopPanelApiService.entityselection;
        }
    
        return new Promise(function(resolve, reject) {
            TopPanelApiService.uploadDocument(params, data,"isSticky").then(function (response) {
                $scope.documentId = response.data.docId;
                $scope.doc = response.data;
                $scope.doc.title = params.fileTitle;
                getListOfDocuments();
                var data =
                {
                    "subject": "upload sticky",
                    "body": 'New sticky is created',
                    "recipients": [$rootScope.ehubObject.userId]
                };
                CommonService.createNotificationMethod(data);
                linkToCaseByDefaut(response.data.docId);
                resolve(true);
            }, function () {
                $scope.addMediaPreloader = false;
                reject(false);
                HostPathService.FlashErrorMessage('ERROR', 'file format not supported');
            });
        });
        
    }

    function linkToCaseByDefaut(docId) {
        if ($rootScope.actCaseDetail) {
            $scope.linkToCase($rootScope.actCaseDetail.caseId, docId);
        }
    }
    /*
     * @purpose: update sticky notes
     * @created: 30 mar 2018
     * @params: file(object)
     * @returns: no
     * @author: varsha
     */
    function updateStickyNotes(file, id) {
        var params = {
            "token": $scope.token,
            "docId": id
        };
        var data = {
            uploadFile: file
        };
        TopPanelApiService.updateDocumentContent(params, data).then(function () {
        }, function () { });
    }

    /*
     * @purpose: Open menu on click of document
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    getAllDocuments();
    $scope.source_list_clipboard = jQuery.extend(true, [], $rootScope.sourcesListsData); 
    $scope.getMenuForDoc = function (e, doc) {
//        $scope.loadDocument = true;
          $scope.doc = doc;
          var  isFromEntitySection = $scope.$parent.isFromEntitySection;

	        

//        $scope.permissionDocument = "";
//        $('#toShareLinkTab').css('display', 'block');
//        $('#deleteDocument').css('display', 'block');
//        $('#documentText').val('').empty();
//        documentAssociation();
//        var CickMenu = document.getElementById('context_menu');
//        CickMenu.style.left = e.originalEvent.layerX - 60 - $('#context_menu').width() + 'px';
//        CickMenu.style.top = (e.pageY - $('.folder-wrapper').offset().top) + 'px';
//        if ($scope.doc.type != '') {
//            $('#openstickyNotes').css('display', 'none');
//        } else {
//            $('#openstickyNotes').css('display', 'block');
//        }
//        $scope.userValue = "";
//        $scope.casevalue = "";
//        listDocComment();
//        allCaseList();
//        allUserList();
        dragElement(document.getElementById(("context_menu")));
    	$scope.viewStickyNotes(isFromEntitySection);
        // console.log($(".textarea-scrollbar").length);
        // $(".textarea-body").mCustomScrollbar({
        //     axis: "y",
        //     theme: "minimal-dark"
        // });
    };

    /*
     * @purpose: view sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    $scope.viewStickyNotes = function (isFromEntitySection) {
  //      $scope.loadDocumentContent = true;
        var stickyNotes = document.getElementById('stickyNotesEntity');
        $('#stickyNotesEntity').css('display', 'block');
        if(!isFromEntitySection){
            stickyNotes.style.left = "-360px";
            stickyNotes.style.top = "25px";
        }else{
            stickyNotes.style.left = "100px";
            stickyNotes.style.top = "110px";
        }
       displayDocumentOnView($scope.doc);
        dragElement(document.getElementById(("stickyNotesEntity")));
    };
    /*
     * @purpose: close sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    $scope.closeSticky = function () {
        $('#stickyNotesEntity').css('display', 'none');
        getListOfDocuments();
        if ($scope.documentId) {
            if(!$scope.stickyNoteContent){
                var texts = $scope.stickySavedText;
                $scope.stickyNoteContent = new File([texts], $scope.doc.title);
            }
            var file = $scope.stickyNoteContent;
            if(file){
                updateStickyNotes(file, $scope.documentId);
            }
            $scope.stickyNoteContent = '';
        }
    };

    $scope.sourceSearchInputOverideEntityClipBoard = function(value,type){
        $scope.fiteredSourceListEntityClipBoard =[];
        $scope.sourceUrlListEntityClipBoard = true;
        if(value === ""){
            $scope.sourceUrlListEntityClipBoard = false;
        }
        if(value){
            $scope.showfiteredSourceListEntityClipBoard =true;
            angular.forEach($scope.source_list_clipboard,function(source){
                if(source && source.sourceName.toLowerCase().indexOf(value.toLowerCase())>=0){
                    $scope.fiteredSourceListEntityClipBoard.push(
                        {
                            sourceName : source.sourceName ? source.sourceName : '', 
                            source_type: source.source_type ? source.source_type : ''
                        });
                } else{
                    $scope.showAddNew =true;
                }
            });
        } else{
            $scope.showAddNew =false;
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit = '';
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit = '';
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputDateEdit = '';
        }
        if(event.keyCode === 13){
            if($scope.fiteredSourceListEntityClipBoard && $scope.fiteredSourceListEntityClipBoard.length > 0){
                $scope.fillSourceSearchedInputOverideEntityClipBoard($scope.fiteredSourceListEntityClipBoard[0].sourceName,type);
                // $scope.updateCompanyDetails(output[0],obj,tabname);
            }else if( $scope.fiteredSourceListEntityClipBoard.length == 0 ){
                // $scope.saveIndustryOption(obj,compkey);
                $scope.showAddNew =false;
                // $scope.updateCompanyDetails(value,obj,tabname);
            }
        }
        setTimeout(function(){
            $(".custom-list.auto-complete-list.searchSource").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
        },0);
    }

    function addHttpProtoCol(val){
        if(val && !val.match(/^[a-zA-Z]+:\/\//)){
            var val = window.location.protocol + '//' + val;            
        }
        return val;	
    }

    $scope.sourcelink_title = '';
    $scope.selected_doc_docId = '';
    $scope.applySourceEvidenceEntityClipBoard = function(sourceData){ // href="'+selectedSourceLink+'" 
        var source = sourceData;
        if(source && source.sourceURL && source.sourceURL.$$attr){
            var selectedSourceLink = source.sourceURL.$$attr.value;
            $scope.sourcelink_title =  $scope.dataPopObjectEntityClipBoard.selectedSourceTitle;
            var link_type = $scope.dataPopObjectEntityClipBoard.srctype;
            var sourceurlvalue = (link_type === 'link') ? selectedSourceLink :  ($scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit || $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit);
            var target_blank = (link_type === 'link') ? ' target="_blank"' : '';
            if(link_type === 'link'){
                sourceurlvalue = addHttpProtoCol(sourceurlvalue);
            }
            var srcName = $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit;
            var srcId = $scope.dataPopObjectEntityClipBoard.selectedSourceTitle;
            if(link_type !== 'link' && link_type !== 'png' && link_type !== 'jpg' && link_type !== 'jpeg' && link_type !== 'gif' && link_type !== 'tiff' && link_type !== 'bmp'){
                sourceurlvalue = sourceurlvalue.split(" ").join("_");
                sourceurlvalue = EHUB_API  + 'documentStorage/downloadDocument?docId=' + $scope.selected_doc_docId + '&token='+ $rootScope.ehubObject.token;
                srcName = $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit.split(" ").join("_");
                srcId = $scope.dataPopObjectEntityClipBoard.selectedSourceTitle.split(" ").join("_");
                target_blank = ' target="_blank"';
            }
            if(link_type === 'png' || link_type === 'jpg' || link_type === 'jpeg' || link_type === 'gif' || link_type === 'tiff' || link_type === 'bmp'){
                sourceurlvalue = '#'+ sourceurlvalue.split(" ").join("_");                
                srcName = $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit.split(" ").join("_");
                srcId = $scope.dataPopObjectEntityClipBoard.selectedSourceTitle.split(" ").join("_");
            }
            var appendLinkText = $scope.stickySavedText +
            '<a class="editableDiv" sourceType= '+link_type+' href="' + (link_type === 'link' ? '' : '')+sourceurlvalue+'" id="source_'+ srcId +'" '+target_blank+' sourcename=' + srcName + ' doc_id='+ $scope.selected_doc_docId + '><span id="sourcelinkname">'+$scope.dataPopObjectEntityClipBoard.selectedSourceTitle+'</span></a>';
            $scope.stickyNoteContent = new File([appendLinkText], $scope.doc.title);
            $('#documentTextEntity').html(appendLinkText);
            appendLinkText = '';
        }
        $scope.sourceUrlListEntityClipBoard = false;
        $(".sticky-link-popover").css("cssText", "display: none !important;");
        showCustomSourceLinkEditPopover();
    }

    // $scope.htmlPopover = function (data) { return $scope.sticklinkPopover(data); };

    // var trusted = {};
    // $scope.sticklinkPopover = function(){
    //     
    //     var template = '';
	// 	template = '<div class="col-sm-6 pad-l0">' +
	// 			'<div class="top-heading border-thin-dark-cream-b d-flex ai-c">' +
    //             '<h4>Primary</h4>' +
    //             '</div>';
    // return trusted[template] || (trusted[template] = $sce.trustAsHtml(template));
    // }

    $scope.showSourceLinkModal = function(){
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit = "";
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit = "";
        $scope.overideSearchedInputSourceEditValEntityClipBoard = "";
        $scope.overideSearchedInputDateEdit = ""
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputDateEdit = "";
        $scope.sourceUrlListEntityClipBoard = false;
        $scope.showfiteredSourceListEntityClipBoard =false;
        $scope.dataPopObjectEntityClipBoard.selectedSourceTitle = "";
    }

    $scope.closeSourceListPopUp = function (){
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit = "";
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit = "";
        $scope.overideSearchedInputSourceEditValEntityClipBoard = "";
        $scope.overideSearchedInputDateEdit = ""
        $scope.dataPopObjectEntityClipBoard.overideSearchedInputDateEdit = "";
        $scope.sourceUrlListEntityClipBoard = false;
        $scope.showfiteredSourceListEntityClipBoard =false;
        $scope.dataPopObjectEntityClipBoard.selectedSourceTitle = "";
        $(".sticky-link-popover").css("cssText", "display: none !important;")
    }
    $scope.fillSourceSearchedInputOverideEntityClipBoard = function(value,type){
        
        type = value.source_type ? value.source_type : '';
         if(value){
            var sourceIndex = $scope.source_list_clipboard.findIndex(function(d){
                    return d.sourceName == value.sourceName;
                });
            // $scope.showfiteredSourceList =false;
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit= ($scope.source_list_clipboard[sourceIndex] && $scope.source_list_clipboard[sourceIndex].sourceName) ? $scope.source_list_clipboard[sourceIndex].sourceName : '';
            $scope.overideSearchedInputSourceEditValEntityClipBoard= ($scope.source_list_clipboard[sourceIndex] && $scope.source_list_clipboard[sourceIndex].sourceName) ? $scope.source_list_clipboard[sourceIndex].sourceName : '';
            $scope.dataPopObjectEntityClipBoard.srctype = '';
            if(type === 'link'){
                $scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit = ($scope.source_list_clipboard[sourceIndex] && $scope.source_list_clipboard[sourceIndex].sourceUrl) ? $scope.source_list_clipboard[sourceIndex].sourceUrl : '';
                $scope.dataPopObjectEntityClipBoard.srctype = 'link';
            }else if(type === 'doc' || type === 'docx' || type ==="png" || type === "pdf"){
                $scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit = ($scope.source_list_clipboard[sourceIndex] && $scope.source_list_clipboard[sourceIndex].docName) ? $scope.source_list_clipboard[sourceIndex].docName : '';
                $scope.dataPopObjectEntityClipBoard.srctype = type;
                $scope.selected_doc_docId = $scope.source_list_clipboard[sourceIndex].docId;
            }
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputDateEdit  = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
    };


    /*
     * @purpose: auto save sticky content
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */
    var name;
    $scope.stickyNoteContent = '';
    $scope.stickyAutoSave = function (e) {
        /*jshint unused:false*/
        var myBlob = new Blob([' '], {
            type: "text/plain"
        });//e.target.value
        if (e.target.innerHTML == "") {
            $scope.stickyNoteContent = new File(["  "], $scope.doc.title);
        } else {
            var text = e.target.innerHTML;
            $scope.stickySavedText = e.target.innerHTML;
            $scope.stickyNoteContent = new File([text], $scope.doc.title);
            //$scope.stickyNoteContent = new File([e.target.value], $scope.doc.title);
        }
    };

    /*
     * @purpose: auto save sticky title
     * @created: 03 april 2018
     * @returns: no 
     * @author: varsha
     */

    $scope.stickyTitleAutoSave = function (e) {
        if (e.target.value == "") {
            updateDocumentTitle($scope.doc, " ");
        } else
            {updateDocumentTitle($scope.doc, e.target.value);}
    };

    function updateDocumentTitle(doc, title) {
        var param = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
            "title": title
        };
        TopPanelApiService.updateDocumentTitle(param).then(function () { }, function (e) {
            HostPathService.FlashErrorMessage('ERROR UPDATE STICKY NOTES', e.responseMessage);
        });

    }
    /*
     * @purpose: open sticky
     * @created: 30 mar 2018
     * @returns: no 
     * @author: varsha
     */
    var getNotesuibModalInstance;
    $scope.confirmDocument = function (status,sticky) {
        $uibModalStack.dismissAll();
        if (status != 'close') {
            var getTitle = $('#getTitle').val();
            if(sticky){
               getTitle =getTitle + ".snt";
            }
            if (getTitle == '' || getTitle == undefined) {
                HostPathService.FlashErrorMessage('ERROR', 'Please enter title');
            } else {
                addNewSticky(getTitle);
            }
        }
    };

    /*
     * @purpose: Upload sticky notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */

    function addNewSticky(getTitle) {
        $('#documentTextEntity').val('').empty();
        
        var myBlob = new Blob([' '], {
            type: "text/plain"
        });
        var file = new File([myBlob], getTitle);
        $scope.loadDocument = true;
        uploadStickyNotes(file, getTitle).then(function(res){
            var stickyNotes = document.getElementById('stickyNotesEntity');
            $('#stickyNotesEntity').css('display', 'block');
            $('#documentTitleEntitySec').val(getTitle);
            if(getTitle.indexOf('.snt') !== -1){
            $('#documentTitleEntitySec').val(getTitle.split('.snt')[0]);
            }
            var  isFromEntitySection = $scope.$parent.isFromEntitySection;
            if(!isFromEntitySection){
                stickyNotes.style.left = "-360px";
                stickyNotes.style.top = "25px";
            }else{
                stickyNotes.style.left = "100px";
                stickyNotes.style.top = "110px";
            }
        }).catch(function(err){
            console.log("Could not be created ", err);
        });
        dragElement(document.getElementById(("stickyNotesEntity")));
    }

    /*
     * @purpose: Create sticky Notes
     * @created: 30 mar 2018
     * @returns: no
     * @author: varsha
     */

    $scope.createSticky = function () {
        if (window.location.hash.indexOf("#!/") >= 0) {
            var modalPath = '../scripts/common/modal/views/getNotesName.modal.html';
        } else {
            var modalPath = 'scripts/common/modal/views/getNotesName.modal.html';
        }
        getNotesuibModalInstance = $uibModal.open({
            templateUrl: modalPath,
            size: 'md',
            scope: $scope,
            backdrop: 'static',
            windowClass: 'custom-modal sticky-prompt-modal',
        }).result.catch(function (res) {
            if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
            } else if (res) {
                throw res;
            } else {

            }
        }, function () {
        });
    };

    $scope.closeEntityClipboard = function(){
        setTimeout(function () {
            $scope.$apply(function () {
                $rootScope.showMyEntityClipboard = false;
               
            });
        }, 0);
    }
    /*
     * @purpose: Display content of sticky notes
     * @created: 30 mar 2018
     * @returns: no 
     * @param : clicked document object
     * @author: varsha
     */

    var popover_open = false; 
    function displayDocumentOnView(doc) {
        $('#documentTextEntity').html('');
        $('#documentTitleEntity').val(doc.title);
        
        $scope.documentId = doc.docId;
        var docId = doc.docId;
        //var docName = doc.title;
        var docType = doc.type;
        var docTitle = doc.title;
        var params = {
            "docId": docId,
            "token": $scope.token
        };
        TopPanelApiService.downloadDocument(params).then(function (response) {
            var blob = new Blob([response.data], {
                type: "application/" + docType,
            });
            var reader = new FileReader();
            reader.addEventListener('loadend', function (e) {
                var text = e.srcElement.result;
                popover_open = false;  
                var showPopIndex = text.indexOf('<div id="showPop"');
                if(showPopIndex !== -1){
                    text = text.slice(0,showPopIndex) + "</a>";
                }
                $('#documentTextEntity').html(text);
                $('#documentTitleEntitySec').val(doc.title);                
                if(doc.title.indexOf('.snt') !== -1){
                    $('#documentTitleEntitySec').val(doc.title.split('.snt')[0]);
                }
                $scope.stickySavedText = text;  
                showCustomSourceLinkEditPopover();
            });
            // Start reading the blob as text.
            var x = reader.readAsText(blob);// jshint ignore:line
            $scope.loadDocumentContent = false;
        }, function () {
            $scope.loadDocumentContent = false;
            HostPathService.FlashErrorMessage('ERROR DOWNLOAD DOCUMENT', 'Failed to download document with file title: ' + docTitle);
        });
    }
    var current_link,curr_title;
    var source_link_val = '';
    function showCustomSourceLinkEditPopover(){
        $('.editableDiv').on('click', function(ev) {
            $('#showPop').removeClass('d-none');
            ev.preventDefault();
            ev.stopPropagation();
            if($(ev.target).attr('class') === 'editableDiv' || $(ev.target).parent().attr("class") === 'editableDiv'){
            $(this).find('#showPop').remove();
            if(!popover_open){
                var current_link = $(this).attr("href").split("//")[1] ? $(this).attr("href").split("//")[1] : $(this).attr("href").split("//")[0];
                var link_type = $scope.dataPopObjectEntityClipBoard.srctype ? $scope.dataPopObjectEntityClipBoard.srctype : $(this).attr("sourceType");
                // var sourceurlvalue = (link_type === 'link') ? selectedSourceLink :  ($scope.dataPopObjectEntityClipBoard.overideSearchedInputURLEdit || $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit);
                $scope.dataPopObjectEntityClipBoard.srctype  = link_type;
                current_link = (link_type === 'link') ? current_link : $(this).attr("href").split("#")[1];
                var id_index = $(this).attr('id').indexOf("_");
                $scope.sourcelink_title = $(this).attr('id').slice((id_index+1), $(this).attr('id').length);

                source_link_val = current_link;
                current_link = $(this).attr("sourcename") || $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit;
                var curr_title = $(this).text();
                if(link_type !== 'link'){
                	current_link = current_link.split("_").join(" ");
                }
                if(link_type !== 'link' && link_type !== 'png' && link_type !== 'jpg' && link_type !== 'jpeg' && link_type !== 'gif' && link_type !== 'tiff' && link_type !== 'bmp'){
                $scope.selected_doc_docId = $(this).attr("doc_id") ? $(this).attr("doc_id") : '' ;
                }
                if(link_type === 'png' || link_type === 'jpg' || link_type === 'jpeg' || link_type === 'gif' || link_type === 'tiff' || link_type === 'bmp'){
                   $scope.selected_doc_docId = '';
                }
                // var current_xpos = ((ev.pageX - $('#stickyNotesEntity').offset().left) - 20) + 'px' ;
                // var current_ypos = (ev.pageY - $('#stickyNotesEntity').offset().top) + 'px';
                var parent_offset = $(this).parent().parent().offset();
                var child_offset = $(this).offset();
                var current_xpos = ( (child_offset.left - parent_offset.left) ) + 'px' ;
                var current_ypos = ((child_offset.top - parent_offset.top) + 67) + 'px';               
                $(this).append(
                    `<div id="showPop" contenteditable="false" class="popover d-block bottom sticky-link-popover width-100 bottom-pop-wrapper mxh-none bottom-right" style="top:${current_ypos};left:${current_xpos};height: 180px;">
                        <div class="arrow" style="top: -11px; left:30px; right: 6px;"></div>
                        <div class="popover-inner  height-100">   
			                <div class="popover-content  height-100">
                        <div class="width-100 p-rel height-100 d-block">
                            <form class="form jc-sb pad-x0" name="form">
                                <div class="bst_input_group p-rel bst_input_group_r  height-a">
                                    <input onkeyup="window.editSourceTitleEntityClipBoard(this)" id="sourceNameId" class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10" value="${curr_title}"/>
                                    <span class="label f-12 text-dark-cream">Title</span>
                                </div>
                                <div class="bst_input_group bst_input_group_r mar-t10 p-rel z-999 height-a">
                                    <input 
                                        class="custom-input pad-r10 pad-l15 pad-t15 lh-18 height-a mar-r10"
                                        autocomplete="no-place"
                                        placeholder="source"
                                        onkeyup="window.editSourceLinkEntityClipBoard(this)"
                                        value="${current_link}"
                                        id="sourceUrlId"
                                        />
                                    <span class="label f-12 text-dark-cream">Source</span>
                                    <ul class="custom-list searchSource z-99 mxh-140 l-0 pad-b35 pad-y10 item-1" style="display:none">
                                        
                                    </ul>
                                </div>
                                <div class="pad-x10 width-100 d-flex p-abs  b-0 ai-c mar-t10">
                                    <button type="button" class="btn mar-autol sm-btns mar-r10 bordered-button" onclick="window.closeSourceListEditPopUp()">Cancel</button>
                                    <button type="button" class="btn grad-button sm-btns" onclick="window.updateStickySource(this)">Apply</button>
                                </div>
                            </form>
                        </div>
                    </div></div></div>`
                );
                popover_open = true;
            }else{
                popover_open = false;
            }
        }
        });
    }
    $scope.changeDropDown = function (checkedValue) {
        if ((checkedValue == 'table')) {
            $('.tiles-style').css('display', 'none');
            ele.classList.remove("hidden");
        } else if (checkedValue == 'icon') {
            $('.tiles-style').css('display', 'block');
            ele.classList.add("hidden");
        }
    };
    /*
     * @purpose: Edit source link in sticky
     * @created: 12 july 2019
     * @returns: no 
     * @author: karnakar
     */
    window.closeSourceListEditPopUp = function(){
        event.preventDefault();
        event.stopPropagation()
        $('#sourceNameId').val(curr_title);
        $('#sourceUrlId').val(current_link);
        $('#showPop').addClass("d-none");
        popover_open = false;
    }
    /*
     * @purpose: Edit source link in sticky
     * @created: 12 july 2019
     * @returns: no 
     * @author: karnakar
     */
    window.editSourceLinkEntityClipBoard = function(that){
        var cur_val = that.value;
        var editFiteredSourceListEntityClipBoard =[];
        if(cur_val === ""){
            $('.editableDiv ul').css("display","none");
        }
        if(cur_val){
            $('.editableDiv ul').css("display","block");
            $('.editableDiv ul').html('');
            source_list_clipboard_for_edit.map(function(val){
                if(val && val.sourceName.toLowerCase().indexOf(cur_val.toLowerCase())>=0){
                    editFiteredSourceListEntityClipBoard.push(
                    {
                        sourceName : val.sourceName ? val.sourceName : '', 
                        source_type: val.source_type ? val.source_type : '',
                        source_url: (val.source_type && val.source_type === 'link') ? (val.sourceUrl ? val.sourceUrl : '') : (val.docName ? val.docName : ''),
                        title : val.title ? val.title : '',
                        doc_id : val.docId ? val.docId : ''
                    });
                }
            });
            $('.editableDiv ul').append(`<div class="width-95 mar-b0 mar-x10 p-rel d-block">	
                <div class="bst_input_group bst_input_group_r  height-a">						
                    <input type="text" ng-disabled="disableInput" id="sourceLinkValue" placeholder="sourceURL"  value="" name="sourceURL" class="custom-input text-cream mar-b0 pad-x15 pad-t15 f-12 height-100"/>
                    <span class="label">Source URL *</span>
                </div>
            </div>`);
            editFiteredSourceListEntityClipBoard.map(function(val){
            	if(val.source_type !== 'link'){
            		val.source_url = val.source_url.split(" ").join("_");
                }
                var liTab = '<li onclick="window.editSelectedValue(this)" class="pad-x10 f-14 h-30 d-flex ai-c" sourceUrl='+(val.source_type === 'link' ? val.source_url : val.source_url)+' sourceType= '+val.source_type+ ' doc_id= '+(val.doc_id ? val.doc_id : '')+'><span>'+
                (val.source_type === 'link' ? '<i class="fa pad-r10 c-pointer fa-link"></i>' : '')+
                (val.source_type === 'doc' || val.source_type === 'docx'? '<i class="fa pad-r10 c-pointer fa-file-word-o"></i>' : '')+
                (val.source_type === 'pdf' ? '<i class="fa pad-r10 c-pointer fa-file-pdf-o"></i>' : '')+
                (val.source_type === 'png' ? '<i class="fa pad-r10 c-pointer fa-file-image-o"></i>' : '')+
                '</span>'+val.sourceName+'</li>';

                $('.editableDiv ul').append(liTab);
            });
            // $('.editableDiv ul').append(``);
        } else{
            $('.editableDiv ul').html('');
        }
        setTimeout(function(){
            $(".custom-list.auto-complete-list.searchSource").mCustomScrollbar({
                axis: "y",
                theme: "minimal-dark"
            });
        },0);
    }
   $scope.editedSourcename = '';
    window.editSelectedValue = function(value){
        if($(value).attr("sourceUrl")){            
            $scope.dataPopObjectEntityClipBoard.srctype = $(value).attr("sourceType");
            // source_link_val = $scope.dataPopObjectEntityClipBoard.srctype === 'link' ? $(value).attr("sourceUrl") : $(value).text();
            source_link_val =  $(value).attr("sourceUrl") ;
            if($scope.dataPopObjectEntityClipBoard.srctype === 'link'){
                source_link_val = addHttpProtoCol(source_link_val);
                $scope.selected_doc_docId = '';
            }
            if($scope.dataPopObjectEntityClipBoard.srctype !== 'link' && $scope.dataPopObjectEntityClipBoard.srctype !== 'png' && $scope.dataPopObjectEntityClipBoard.srctype !== 'jpg' && $scope.dataPopObjectEntityClipBoard.srctype !== 'jpeg' && $scope.dataPopObjectEntityClipBoard.srctype !== 'gif' && $scope.dataPopObjectEntityClipBoard.srctype !== 'tiff' && $scope.dataPopObjectEntityClipBoard.srctype !== 'bmp'){
                source_link_val = source_link_val.split(" ").join("_");
                $scope.selected_doc_docId = $(value).attr("doc_id") ;
            }
            if($scope.dataPopObjectEntityClipBoard.srctype === 'png' || $scope.dataPopObjectEntityClipBoard.srctype === 'jpg' || $scope.dataPopObjectEntityClipBoard.srctype === 'jpeg' || $scope.dataPopObjectEntityClipBoard.srctype === 'gif' || $scope.dataPopObjectEntityClipBoard.srctype === 'tiff' || $scope.dataPopObjectEntityClipBoard.srctype === 'bmp'){
                source_link_val = source_link_val.split(" ").join("_");
                $scope.selected_doc_docId = '';
            }
            $("#sourceLinkValue").val(source_link_val);
            $("#sourceUrlId").val($(value).text());
            $scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit = $(value).text();
            $('.editableDiv ul').html('');
            $('.editableDiv ul').hide();
        }
    }

    window.updateStickySource = function(that){
        var link_type = $scope.dataPopObjectEntityClipBoard.srctype;
        var sourceHrefVal = '';
        if(link_type === 'link'){
            source_link_val = addHttpProtoCol(source_link_val);
            sourceHrefVal = source_link_val;
        }
        if(link_type !== 'link' && link_type !== 'png' && link_type !== 'jpg' && link_type !== 'jpeg' && link_type !== 'gif' && link_type !== 'tiff' && link_type !== 'bmp'){
            source_link_val = source_link_val ? source_link_val.split(" ").join("_") : '';
            sourceHrefVal = source_link_val;
            sourceHrefVal = EHUB_API  + 'documentStorage/downloadDocument?docId=' + $scope.selected_doc_docId + '&token='+ $rootScope.ehubObject.token;
        }
        if(link_type === 'png' || link_type === 'jpg' || link_type === 'jpeg' || link_type === 'gif' || link_type === 'tiff' || link_type === 'bmp'){
            source_link_val = source_link_val.split(" ").join("_");
            sourceHrefVal = '#' + source_link_val;
        }
        $(that).closest("a#source_"+$scope.sourcelink_title).attr("href", (link_type ==='link' ? sourceHrefVal : (sourceHrefVal))  );
        if(link_type !== 'link' && link_type !== 'png' && link_type !== 'jpg' && link_type !== 'jpeg' && link_type !== 'gif' && link_type !== 'tiff' && link_type !== 'bmp'){
            $(that).closest("a#source_"+$scope.sourcelink_title).attr('target',"_blank");
            $(that).closest("a#source_"+$scope.sourcelink_title).attr('doc_id',$scope.selected_doc_docId);
        }else if(link_type === 'png' || link_type === 'jpg' || link_type === 'jpeg' || link_type === 'gif' || link_type === 'tiff' || link_type === 'bmp'){
            $(that).closest("a#source_"+$scope.sourcelink_title).removeAttr('target',"_blank");
            $(that).closest("a#source_"+$scope.sourcelink_title).attr('doc_id','');
        }else{
            $(that).closest("a#source_"+$scope.sourcelink_title).attr('target',"_blank");
            $(that).closest("a#source_"+$scope.sourcelink_title).attr('doc_id','');
        }
        $(that).closest("a#source_"+$scope.sourcelink_title).find('#sourcelinkname').text($("#sourceNameId").val());
        $(that).closest("a#source_"+$scope.sourcelink_title).attr('id',("source_"+$("#sourceNameId").val()));
        $(that).closest("a#source_"+$scope.sourcelink_title).attr('sourceType',link_type);
        $(that).closest("a#source_"+$scope.sourcelink_title).attr("sourcename",$scope.dataPopObjectEntityClipBoard.overideSearchedInputSourceEdit);
        $scope.sourcelink_title = $("#sourceNameId").val();
        
        $(that).closest('#showPop').remove();
        // source_link_val = '';
        popover_open = false;
        var appendLinkText = $("#documentTextEntity").html();
        $scope.stickyNoteContent = new File([appendLinkText], $scope.doc.title);
        // $scope.dataPopObjectEntityClipBoard.selectedSourceTitle = $("#sourceNameId").val();

    }

    /*
     * @purpose: Edit source title in sticky
     * @created: 12 july 2019
     * @returns: no 
     * @author: karnakar
     */
    window.editSourceTitleEntityClipBoard = function(that){
        var cur_title = that.value;
    }

    /*
     * @purpose: Delete sticky
     * @created: 03 mar 2018
     * @returns: no 
     * @author: varsha
     */

    $scope.onClickStickyNotesDelete = function () {
        var params = {
            "token": $scope.token,
            "docId": $scope.doc.docId
        };
        if ($scope.doc.docId) {
            TopPanelApiService.deleteDocument(params).then(function () {
                getListOfDocuments();
                $('#rightClickMenu').css('display', 'none');
                $("#context_menu").css('display', 'none');
                $('#stickyNotesEntity').css('display', 'none');
                HostPathService.FlashSuccessMessage('SUCCESSFUL DELETE STICKY NOTES', '');
            }, function (e) {
                HostPathService.FlashErrorMessage('ERROR DELETE STICKY NOTES', e.responseMessage);
            });
        }
    };
    /*  @purpose: full text search *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.fullTextSearch = function (e) {
        if (e.target.value == '') {
        	pageMydocumentSticky = 1;
            getListOfDocuments();
        } else {
            var obj = {
                "token": $scope.token,
                "keyword": e.target.value,
                "entityId":$rootScope.entityObj["@identifier"],              
                "docFlag": 5
            };
            var obj2 = {
                "token": $scope.token,
                "searchKeyWord": e.target.value,
                "entityId":$rootScope.entityObj["@identifier"], 
                "docFlag": 5
            };
            var obj4 = {
                "token": $scope.token,
                "entityId":$rootScope.entityObj["@identifier"], 
                "searchKeyWord": e.target.value
            };
            var obj3 = {
                "token": $scope.token,
                "entityId":$rootScope.entityObj["@identifier"], 
                "keyword": e.target.value,
            };
            TopPanelApiService.fullTextSearchDocument(obj).then(function (response) {
                $scope.mydocumentSticky = response.data.result;
                $scope.mydocumentStickyLength = response.data.paginationInformation.totalResults;
                TopPanelApiService.fullTextSearchSharedDocument(obj2).then(function (response2) {
                    $scope.mysharedSticky = response2.data.result;
                    $scope.mysharedStickyLength = response2.data.paginationInformation.totalResults;
                    $scope.documents = $.merge($.merge([], response.data.result), response2.data.result);
                    TopPanelApiService.fullTextSearchSharedDocument(obj4).then(function (response4) {
                        $scope.mysharedUploaded = response4.data.result;
                        $scope.mysharedUploadedLength = response4.data.paginationInformation.totalResults;
                        $scope.documents = $.merge($.merge([], $scope.documents), response4.data.result);
                        TopPanelApiService.fullTextSearchDocument(obj3).then(function (response3) {
                            $scope.mydocumentUploaded = response3.data.result;
                            $scope.mydocumentUploadedLength = response3.data.paginationInformation.totalResults;
                            $scope.documents = $.merge($.merge([], $scope.documents), response3.data.result);
                        });
                    });
                });

            }, function (e) {
                HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
            });
        }
    };
    /* * @purpose: List By data dropdown*
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */
    $scope.changeListByDropDown = function (checkedValue) {
        getListOfDocuments($event,checkedValue);
    };

    /* * @purpose: on click of document remove context menu*
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    document.addEventListener('click', function () {
        if ($('#rightClickMenu').css('display') == 'block') {
            $('#rightClickMenu').css('display', 'none');

        }
    });
    /* * @purpose: Icon path for different dashboard *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.pathChange = function (type) {
        if (window.location.hash.indexOf("#!/") < 0 && type != '	') {
            return true;
        }
    };
    /* * @purpose: scroll bar to the folders panels *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        /*Custom Scroll Bar*/
        $('#dataList').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        });
        $('#dataList').css("height", "200px");
    });

    /* * @purpose: Get list of all user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    function allUserList() {
        var obj = {
            "token": $scope.token
        };
        TopPanelApiService.getUserListing(obj).then(function (response) {
            $scope.userList = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: Display block to share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.shareUser = function () {
        allUserList();
        $("#shareBoxWrapper").css('display', 'block');
    };

    /* * @purpose: On select of any user  *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.onSelectUser = function ($item, $model, $label) {/*jshint unused:false*/
        $('#onclickSharewithUser').css('pointer-events', 'all');
        $scope.userValue = $item;
    };

    /* * @purpose: share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.shareWithUser = function (permission) {
        if ($scope.userValue.userId == undefined) {
            HostPathService.FlashErrorMessage('ERROR', 'Please select user first to share');
            return false;
        }
        $('#sharewithuser').val('');
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "userId": $scope.userValue.userId,
            "permission": permission,
            "docId": $scope.doc.docId

        };
        TopPanelApiService.shareDocumentWithUser(obj).then(function () {
            documentAssociation();
            $scope.userValue.userId == undefined;// jshint ignore:line
            $('#onclickSharewithUser').css('pointer-events', 'none');
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };

    $scope.removeUser = function (userDetail) {
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "userId": userDetail.userId,
            "permission": 1,
            "docId": $scope.doc.docId,
            "comment": " "
        };
        TopPanelApiService.unshareDocumentWithUser(obj).then(function () {
            documentAssociation();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };
   
    /* * @purpose: Display map case block *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */
    
    $scope.shareCase = function () {
        $("#shareCaseWrapper").css('display', 'block');
        allCaseList();
    };

    /* * @purpose: Get list of all case *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */
    
    allCaseList();

    function allCaseList() {
        var obj = {
            "token": $scope.token
        };
        TopPanelApiService.listCases(obj).then(function (response) {
            $scope.caseDetail = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: If any case get selected *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.onSelectCase = function ($item, $model, $label) {// jshint ignore:line
        
        $scope.caseValue = $item;
    };
    /* * @purpose: share with user *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.linkToCase = function (case_Id, doc_Id) {
        if ($scope.caseValue == undefined && case_Id == undefined && doc_Id == undefined) {
            HostPathService.FlashErrorMessage('ERROR', 'Please select case first to link');
            return false;
        }
        $('#linkWithUser').val('');
        $scope.shareLoader = true;
        if (case_Id != undefined && doc_Id != undefined) {
            var obj = {
                "token": $scope.token,
                "caseId": case_Id,
                "permission": 1,
                "docId": doc_Id
            };
        } else {
            var obj = {
                "token": $scope.token,
                "caseId": $scope.caseValue.caseId,
                "permission": 1,
                "docId": $scope.doc.docId
            };
        }
        TopPanelApiService.shareDocumentWithCase(obj).then(function () {
            documentAssociation();
            $scope.caseValue = undefined;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };
    /* * @purpose: unlink the case*
     *   @created: 06 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.removeCase = function (caselist) {
        $scope.shareLoader = true;
        var obj = {
            "token": $scope.token,
            "caseId": caselist.caseId,
            "permission": 1,
            "docId": $scope.doc.docId
        };
        TopPanelApiService.unshareDocumentWithCase(obj).then(function () {
            documentAssociation();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    };

    function documentAssociation() {
        $scope.shareLoader = true;
        $scope.docUserList = [];
        $scope.caseList = [];
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId
        };
        TopPanelApiService.getCaseMapping(obj).then(function (response) {
            if (response.data.responseMessage == "Permission is denied or Document not found") {
                $scope.permissionDocument = "Read Only";
                var elem = $('#toShareLinkTab').find('ul');
                $(elem[0]).css('pointer-events', 'none');
                $('#context_menu').css('display', 'block');
                $('#deleteDocument').css('display', 'none');
            } else {
                $scope.permissionDocument = "";
                var elem = $('#toShareLinkTab').find('ul');
                $(elem[0]).css('pointer-events', 'all');
                $('#context_menu').css('display', 'block');
                $scope.docUserList = response.data.docUserList;
                $scope.caseList = response.data.caseList;
            }
            $scope.shareLoader = false;
            $scope.loadDocument = false;
        }, function (e) {
            $scope.loadDocument = false;
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    /* * @purpose: comment section *
     *   @created: 03 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.changeCommentBox = function ($event, comment) {// jshint ignore:line
        if ($event.keyCode == 13) {
            var tb = document.getElementById("comentInput");
            $scope.commentDescription = tb.value;
            $('#comentInput').val('');
            addComment();
        }
    };
    /* * @purpose: add comment section *
     *   @created: 07 apr 2018
     *   @returns: no
     *   @author: varsha */

    function addComment() {
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
            "commentdesc": $scope.commentDescription
        };
        TopPanelApiService.addComment(obj).then(function () {
            listDocComment();
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }

    /* * @purpose: list all added comment section *
     *   @created: 07 apr 2018
     *   @returns: no
     *   @author: varsha */

    function listDocComment() {
        var obj = {
            "token": $scope.token,
            "docId": $scope.doc.docId,
        };
        TopPanelApiService.listDocComment(obj).then(function (response) {
            $scope.commentDes = response.data.result;
        }, function (e) {
            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
        });
    }
    var caseId;
    $scope.displayFiles = function (actCaseDetail) {
        if (actCaseDetail.caseId) {
            caseId = actCaseDetail.caseId;
            getListOfDocuments();
            $("#filterByCase").addClass("active");
            $("#removefilterByCase").removeClass("active");
        } else {
            caseId = undefined;
            getListOfDocuments();
            $("#removefilterByCase").addClass("active");
            $("#filterByCase").removeClass("active");
        }
    };

    /* * @purpose: close share user and map case block *
     *   @created: 05 apr 2018
     *   @returns: no
     *   @author: varsha */

    $scope.closeBox = function (name) {
        if (name == 'shareCase') {
            $("#shareCaseWrapper").css('display', 'none');
        } else if (name == 'shareDoc') {
            $("#shareBoxWrapper").css('display', 'none');
        } else if (name == 'contextMenu') {
            $scope.shareLoader = false;
            $("#context_menu").css('display', 'none');
            getListOfDocuments();
        }
    };
    /* * @purpose: scroll bar to the Share And Link panels *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        $('.data-table').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.data-table').css('height', '200px');
    });

    $(document).ready(function () {
        $('.user-list-wrapper .dropdown-menu').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.user-list-wrapper .dropdown-menu').css('height', '200px');
    });
    /* * @purpose: scroll bar to the Comment panel *
     *   @created: 2 apr 2018
     *   @returns: no
     *   @author: Anil */

    $(document).ready(function () {
        $('.top-comment-wrapper').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.top-comment-wrapper').css('height', '200px');
    });
    $(document).ready(function () {
        $('.folder-wrapper').mCustomScrollbar({
            axis: "y",
            theme: "minimal"
        }, function () {
        });
        $('.folder-wrapper').css('height', '230px');
    });
    /* * @purpose: draggable sticky notes *
    *   @created: 11 apr 2018
    *   @returns: no
    *   @author: varsha */

    function dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "header")) {
            /* if present, the header is where you move the DIV from:*/
            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
    
    /*
     * @purpose: Delete sticky/documents
     * @created: 25th April 2019
     * @params: null
     * @return: no
     * @author: Amritesh
     */
    
    $scope.deleteDoc = function(event, docId,documentObject){
    	event.stopPropagation();
        
        var confirmDeleteModalInstance = $uibModal.open({
            templateUrl: 'confirmDeleteDoc.html',
            controller: 'ConfirmDelStickyController',
            size: 'xs',
            scope: $scope,
            backdrop: 'static',
            windowClass: 'custom-modal bst_modal update-entities-modal related-person-modal data-popup-wrapper',
        }).result.then(function(response){
        	$scope.loadDocument = true;
        	TopPanelApiService.deleteEntityDocument(docId).then(function(res){
                getListOfDocuments();
                if(documentObject.type && documentObject.type.toLowerCase() !== 'snt'){
                    saveAddtoPage(documentObject);
                }                
        		$scope.loadDocument = false;
        	});
        }, function(error){
        	$scope.loadDocument = false;
        });
    }
    
    function saveAddtoPage(documentObject){
               var srcName = documentObject.entitySource ? documentObject.entitySource :"";
            //    var srcName = TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value ? TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value.sourceName:"";
               TopPanelApiService.deleteSourceAddtoPage(srcName, $rootScope.entityObj["@identifier"]).then(function(response){
                  // 
                  TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj = {};
                  TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showHideScreenIcon = false;
                  TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.uploadedFileName = "";
                 TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.downloadLink = "";
                  TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showUploadIcon = true;
                      var addtopagedata = [{
                       "entityId": $rootScope.entityObj["@identifier"],
                           "isAddToPage": false,
                          "sourceName": TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value ? TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value.sourceName:"",
                      }];
                     TopPanelApiService.saveSourcesAddToPage(addtopagedata).then(function(res){
                          TopPanelApiService.uploadFromScreenShotPopupUploadIcon.deletedSrcObj.showHideAddtoPage = false;
                        // $scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideAddtoPage = false;
                        // $scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideScreenIcon = false;
                    
                    
                      }, function(err){
              
                      }).catch(function(){
              
                     });
             },function(error){
                console.log(error);  
              }).catch(function(err){
                console.log(err)
              });

    
              
           }
    
    window.onhashchange = myFunction;
    function myFunction() {
        if (location.hash == "#/domain") {
            $rootScope.showMyClipboard = false;
        }
    }
    $scope.deleteEvidenceDoc = function(index){
       var SpliceData = $scope.evidenceReportAddToPageFromclip.splice(index,1);
       TopPanelApiService.spliceValue=SpliceData;
    }
 
    /*  $scope.caseWithId = function(caseValue){
             return caseValue.name + ' (' + caseValue.caseId + ')';
     }*/

    /*
	* @purpose : Get all documents list 
	* @author : Amarjith Kumar
	* @Date : 30-March-2019
	*/
	$scope.documentListsData_Clip = [];
	function getAllDocuments() {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": 9,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		$scope.documentListsData_Clip = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllDocuments(params).then(function (res) {
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'docs');
				getEvidenceListDocs(params);
				resolve(true);
			}).catch(function (err) {
				console.log(err);
				reject(false);
			});
		});
	}


	/*
	* @purpose : Get all evidence documents list 
	* @author : karnakar
	* @Date : 01-july-2019
	*/
	
	function getEvidenceListDocs(paramFromApiFunc){
		var params = paramFromApiFunc;
		params.docFlag = 6;
		$scope.documentListsData_Clip = [];
		return new Promise(function(resolve, reject) {
			TopPanelApiService.getAllEvidenceDocuments(params).then(function(res){
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'evidence_docs');
				resolve(true);
			}).catch(function(err){
				console.log(err);
				reject(false);
			});
		});
	}

	function getAllStickyNotes() {
		var params = {
			"token": $rootScope.ehubObject.token,
			"docFlag": 5,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": 9,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		$scope.documentListsData_Clip = [];
		return new Promise(function (resolve, reject) {
			TopPanelApiService.getAllDocuments(params).then(function (res) {
				getSuccessReportDocuments(res.data.paginationInformation.totalResults, 'sticky');
				resolve(true);
			}).catch(function (err) {
				console.log(err);
				reject(false)
			});
		});
	}

	function getSuccessReportDocuments(pagerecords, docOrSticky) {
		var params = {
			"token": $rootScope.ehubObject.token,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId": $rootScope.entityObj["@identifier"]
		};
		var params3 = {
			"token": $rootScope.ehubObject.token,
			"docFlag": 6,
			"pageNumber": 1,
			"orderIn": 'desc',
			"orderBy": 'uploadedOn',
			"recordsPerPage": pagerecords,
			"entityId":$rootScope.entityObj["@identifier"]
		};

		if (docOrSticky === 'docs') {
			return new Promise(function (resolve, reject) {
				TopPanelApiService.getAllDocuments(params).then(function (res) {
					if(res && res.data && res.data.result && res.data.result.length){
						res.data.result.forEach(function(val,key){
							val.source_type = val.type || 'doc';
							val.sourceName = val.title || val.docName;
						});
						$scope.documentListsData_Clip = res.data.result;
                        $scope.source_list_clipboard = $scope.source_list_clipboard.concat($scope.documentListsData_Clip);
                        source_list_clipboard_for_edit = source_list_clipboard_for_edit.concat($scope.documentListsData_Clip);
					}
					resolve(true);
				}).catch(function (err) {
					console.log(err);
					reject(false);
				});
			});
		} 
		else if(docOrSticky === "evidence_docs"){
			return new Promise(function(resolve, reject) {
				TopPanelApiService.getAllDocuments(params3).then(function(res){			
					angular.forEach(res.data.result, function (val, key) {
						val.source_type = val.type || 'png';
						val.sourceName = val.title || val.docName;
					});				
					$scope.evidenceDocumentsListsData_clip = res.data.result;

                    $scope.source_list_clipboard = $scope.source_list_clipboard.concat($scope.evidenceDocumentsListsData_clip);
                    source_list_clipboard_for_edit = source_list_clipboard_for_edit.concat($scope.evidenceDocumentsListsData_clip);
					resolve(true);
				}).catch(function(err){
					
					reject(false);
				});
			});
		}
	}

    /** * END --- Sticky Notes ** */

}