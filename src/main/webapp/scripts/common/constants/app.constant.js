'use strict';

/*--  server  ---*/

/*--  ISRA server ---*/
// elementApp 
// 	.constant('EHUB_API', 'http://172.20.159.11/ehubrest/api/')
// 	.constant('EHUB_FE_API', 'http://172.20.159.11/element/')
// 	.constant('TI_API', 'http://172.20.159.11/transaction/')
// 	.constant('ACTIVITI_FE_PATH', 'http://172.20.159.11/activiti-app/#/')
// 	.constant('KYC_QUESTIONNAIRE_PATH', 'http://172.20.159.11:9393/element-kyc-questioner/index.php/admin')
// 	.constant('POLICY_ENFORCEMENT_PATH', 'http://172.20.159.11:9292/policyEnforcement');


/*--  server 7070 ---*/
/*
 elementApp
	    .constant('EHUB_API', 'http://188.166.40.105:7070/ehubrest/api/')
	   	.constant('EHUB_FE_API', 'http://188.166.40.105:7070/elementexploration/')
	    .constant('ACTIVITI_FE_PATH', 'http://188.166.40.105:7070/elementbpm/#/')
	    .constant('KYC_QUESTIONNAIRE_PATH', 'http://188.166.40.105/element-questionnaire-builder/index.php/admin')
		.constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement'); 
		*/
		
/*--  server 8081 ---*/
/* elementApp
	    .constant('EHUB_API', 'http://188.166.40.105:8081/ehubrest/api/')
	   	.constant('EHUB_FE_API', 'http://188.166.40.105:8081/element/')
	    .constant('ACTIVITI_FE_PATH', 'http://188.166.40.105:8081/elementbpm/#/')
	    .constant('KYC_QUESTIONNAIRE_PATH', 'http://188.166.40.105/element-questionnaire-builder/index.php/admin')
		.constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement');
 */
/*--  server 8080 ---*/
elementApp
	.constant('EHUB_API', window.location.protocol+"//"+window.location.hostname+":"+window.location.port+'/ehubrest/api/')
	.constant('EHUB_FE_API', window.location.protocol+"//"+window.location.hostname+":"+window.location.port+'/element/')
	.constant('ACTIVITI_FE_PATH', window.location.protocol+"//"+window.location.hostname+":"+window.location.port+'/elementbpm/#/')
	.constant('KYC_QUESTIONNAIRE_PATH', window.location.protocol+"//"+window.location.hostname+"/element-questionnaire-builder/index.php/admin")
	.constant('POLICY_ENFORCEMENT_PATH', window.location.protocol+"//"+window.location.hostname+':7070/policyEnforcement');

/*---  local  ---*//* 
elementApp 
	.constant('EHUB_API', 'http://localhost:8080/ehubrest/api/')
	.constant('EHUB_FE_API', 'http://localhost:8080/ehubui/')
	.constant('TI_API', 'http://localhost:8080/transaction/')
	.constant('ACTIVITI_FE_PATH', 'http://localhost:8080/activiti-app/#/')
	.constant('KYC_QUESTIONNAIRE_PATH', 'http://188.166.40.105/element-questionnaire-builder/index.php/admin')
	.constant('POLICY_ENFORCEMENT_PATH', 'http://188.166.40.105:7070/policyEnforcement') */;

/*--  server 6060 ---*/
/*elementApp
	    .constant('EHUB_API', 'http://159.65.192.32:6060/ehubrest/api/')
	   	.constant('EHUB_FE_API', 'http://159.65.192.32:6060/element/')
	    .constant('ACTIVITI_FE_PATH', 'http://159.65.192.32:6060/elementbpm/#/')
	    .constant('KYC_QUESTIONNAIRE_PATH', 'http://159.65.192.32/element-questionnaire-builder/index.php/admin')
		.constant('POLICY_ENFORCEMENT_PATH', 'http://159.65.192.32:7070/policyEnforcement'); */
