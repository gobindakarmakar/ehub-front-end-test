'use strict';
elementApp
	   .constant('OpenSourceCategoryConstant', {
		Persons: [
			{
				key: 'People and company profiles BL',
				value: false
			},
			{
				key: 'People LD',
				value: false
			},
			{
				key: 'Financial Payment Data US',
				value: false
			},
			{
				key: 'People Search SK',
				value: false
			},
			{
				key: 'Watch List Interpol',
				value: false
			},
			{
				key: 'Sanctions Wrapper',
				value: false
			},
			{
				key: 'Sanctions OFAC',
				value: false
			},
			{
				key: 'People ZS',
				value: false
			}
		],
		Company: [
			{
				key: 'Cyber Attacks X',
				value: false
			},
			{
				key: 'Cyber S',
				value: false
			},
			{
				key: 'Companies Investments TC',
				value: false
			},
			{
				key: 'Companies UK HL',
				value: false
			},
			{
				key: 'Companies UK CL',
				value: false
			},
			{
				key: 'Companies Global BL',
				value: false
			},
			{
				key: 'Cyber DB RD',
				value: false
			},
			{
				key: 'Companies LD',
				value: false
			},
			{
				key: 'Companies Stock BL',
				value: false
			}
		],
		Text: [
			{
				key: 'Cyber Hackers Tools',
				value: false
			},
			{
				key: 'Twitter',
				value: false
			},
			{
				key: 'Social News aggregation Adom',
				value: false
			},
			{
				key: 'Cyber privacy-enhancing technologies',
				value: false
			},
			{
				key: 'Dark Web ATB',
				value: false
			},
			{
				key: 'Dark Web DTW',
				value: false
			},
			{
				key: 'deepsec.net',
				value: false
			}
		],
		Person: [
			{
				key: 'Companies perosonel HU',
				value: false
			}
		],
		Domain: [
			{
				key: 'URL ownership WI',
				value: false
			}
		],
		Event: [
			{
				key: 'Offline Group Meetings',
				value: false
			}
		],
		Phone: [
			{
				key: 'People search Mobile TC',
				value: false
			}
		]
	})
	.constant('SourceCategoryConstant', [
		{
			key: 'OSINT',
			value: false,
			fitcher: '',
			categories: [
				{
					name: 'People profiles BL',
					value: false
				},
				{
					name: 'People LD',
					value: false
				},
				{
					name: 'People and company profiles BL',
					value: false
				},
				{
					name: 'People Search SK',
					value: false
				},
				{
					name: 'People ZS',
					value: false
				}
			]
		},
		{
			key: 'Web Service',
			value: false,
			fitcher: '',
			categories: [
				{
					name: 'Watch List Interpol',
					value: false
				},
				{
					name: 'People search Mobile TC',
					value: false
				},
				{
					name: 'URL ownership WI',
					value: false
				}
			]
		},
		{
			key: 'Index',
			value: false,
			fitcher: '',
			categories: [
				{
					name: 'Sanctions Wrapper',
					value: false
				},
				{
					name: 'Sanctions OFAC',
					value: false
				},
				{
					name: 'Companies Investments TC',
					value: false
				},
				{
					name: 'Twitter',
					value: false
				},
				{
					name: 'Social News aggregation Adom',
					value: false
				}
			]
		},
		{
			key: 'Dark Web',
			value: false,
			fitcher: '',
			categories: [
				{
					name: 'Dark Web DTW',
					value: false
				},
				{
					name: 'Dark Web ATB',
					value: false
				},
				{
					name: 'Financial Crime',
					value: false
				}
			]
		},
		{
			key: 'Retrieve',
			value: false,
			fitcher: '',
			categories: [

				{
					name: 'Companies UK HL',
					value: false
				},
				{
					name: 'Offline Group Meetings',
					value: false
				},
				{
					name: 'Companies UK CL',
					value: false
				},
				{
					name: 'Companies perosonel HU',
					value: false
				}
			]
		},
		{
			key: 'News',
			value: false,
			fitcher: '',
			categories: [
				{
					name: 'Cyber Attacks X',
					value: false
				},
				{
					name: 'Cyber S',
					value: false
				},
				{
					name: 'Cyber Hackers Tools',
					value: false
				},
				{
					name: 'Cyber privacy-enhancing technologies',
					value: false
				}
			]
		}
	])
	.constant('DomainCategoryConstant', [
		{
			name: 'General',
			key: 'BI'
		},
		{
			name:'Financial Crime',
			key: 'FC'
		},
		{
			name: 'Cyber',
			key: 'CB'
		},
		{
			name:  'Compliance',
			key: 'CM'
		},
		{
			 name: 'Market Intelligence',
		     key: 'MI'
		}
	])
	.constant('FetcherCategoryList', [
		{
			fetcher_id: '8',
			fetcher_name: 'Bloomberg Business Person Profiles',
			sourceTypes: ['Index'],
			domainTypes: ['General'],
			categories: []
		},
		{
			fetcher_id: '3',
			fetcher_name: 'Linkedin Personal Profiles',
			sourceTypes: ['Web Service'],
			domainTypes: ['Compliance','General'],
			categories: ['People and company profiles BL','People LD','Financial Payment Data US','People Search SK','Watch List Interpol','People ZS']
		},
		{
			fetcher_id: '6',
			fetcher_name: 'interpol.int-wanted-list',
			sourceTypes: ['OSINT'],
			domainTypes: ['General'],
			categories: ['Sanctions Wrapper','Sanctions OFAC']
		},
		{
			fetcher_id: '1012',
			fetcher_name: 'Littlesis.org',
			sourceTypes: ['Dark Web'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Investments TC','Companies UK HL','Companies UK CL','Companies Global BL','Companies LD','Companies perosonel HU']
		},
		{
			fetcher_id: '1005',
			fetcher_name: 'companieslist.co.uk',
			sourceTypes: ['Dark Web'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Investments TC','Companies UK HL','Companies UK CL','Companies Global BL','Companies LD','Companies perosonel HU']
		},
		{
			fetcher_id: '5',
			fetcher_name: 'Spokeo',
			sourceTypes: ['Web Service'],
			domainTypes: ['General'],
			categories: ['People and company profiles BL','People LD','Financial Payment Data US','People Search SK','Watch List Interpol','People ZS']
		},
		{
			fetcher_id: '1001',
			fetcher_name: 'IBM-xforce-Cloud',
			sourceTypes: ['OSINT'],
			domainTypes: ['Cyber','Cyber DB RD','Cyber S','Cyber Attacks X','Cyber Hackers Tools','Cyber privacy-enhancing technologies','General'],
			categories: []
		},
		{
			fetcher_id: '23',
			fetcher_name: 'Darkweb-Answerstedhctbek',
			sourceTypes: ['Dark Web'],
			domainTypes: ['General'],
			categories: ['Dark Web ATB','Dark Web DTW']
		},
		{
			fetcher_id: '1006',
			fetcher_name: 'Bloomberg.com - Company Profiles',
			sourceTypes: ['Dark Web'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Investments TC','Companies UK HL','Companies UK CL','Companies Global BL','Companies LD','Companies perosonel HU']
		},
		{
			fetcher_id: '1010',
			fetcher_name: 'Yahoo Finance Stocks',
			sourceTypes: ['OSINT'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Stock BL','Business Intelligence']
		},
		{
			fetcher_id: '1011',
			fetcher_name: 'CNN Money',
			sourceTypes: ['Web Service'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Stock BL','Business Intelligence']
		},
		{
			fetcher_id: '1008',
			fetcher_name: 'Linkedin-Company profiles',
			sourceTypes: ['Dark Web'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Investments TC','Companies UK HL','Companies UK CL','Companies Global BL','Companies LD','Social News aggregation Adom','Companies perosonel HU']
		},
		{
			fetcher_id: '18',
			fetcher_name: 'Gov.uk - Sanction List',
			sourceTypes: ['Web Service'],
			domainTypes: ['General'],
			categories: ['Sanctions Wrapper','Sanctions OFAC']
		},
		{
			fetcher_id: '1002',
			fetcher_name: 'Shodan',
			sourceTypes: ['Index'],
			domainTypes: ['Cyber','Cyber DB RD','Cyber S','Cyber Attacks X','Cyber Hackers Tools','Cyber privacy-enhancing technologies','General'],
			categories: []
		},
		{
			fetcher_id: '1003',
			fetcher_name: 'Techcrunch.com',
			sourceTypes: ['Market Intelligence'],
			domainTypes: [],
			categories: []
		},
		{
			fetcher_id: '1007',
			fetcher_name: 'Risidata',
			sourceTypes: ['OSINT'],
			domainTypes: ['Cyber','Cyber DB RD','Cyber S','Cyber Attacks X','Cyber Hackers Tools','Cyber privacy-enhancing technologies','General'],
			categories: []
		},
		{
			fetcher_id: '1009',
			fetcher_name: 'Bloomberg.com - Stocks',
			sourceTypes: ['Web Service'],
			domainTypes: ['Compliance','General'],
			categories: ['Companies Stock BL','Business Intelligence']
		},
		{
			fetcher_id: '3001',
			fetcher_name: 'jsonwhois.com',
			sourceTypes: ['OSINT'],
			domainTypes: ['General'],
			categories: ['URL ownership WI']
		},
		{
			fetcher_id: '2001',
			fetcher_name: 'Twitter',
			sourceTypes: ['OSINT'],
			domainTypes: ['Compliance','General'],
			categories: ['People and company profiles BL','People LD','Financial Payment Data US','People Search SK','Watch List Interpol','People ZS','Twitter']
		},
		{
			fetcher_id: '1021',
			fetcher_name: 'Glassdoor',
			sourceTypes: ['OSINT'],
			domainTypes: ['Compliance','General'],
			categories: ['People and company profiles BL','People LD','Financial Payment Data US','People Search SK','Watch List Interpol','People ZS','Twitter']
		},
		{
			fetcher_id: '26',
			fetcher_name: 'everypolitician.org',
			sourceTypes: ['Index'],
			domainTypes: ['General'],
			categories: []
		},
		{
			fetcher_id: '2005',
			fetcher_name: 'Darkweb-Deepdotweb.com',
			sourceTypes: ['Dark Web'],
			domainTypes: ['General'],
			categories: ['Dark Web ATB','Dark Web DTW']
		},
		{
			fetcher_id: '28',
			fetcher_name: 'cidob.org',
			sourceTypes: ['News'],
			domainTypes: ['Compliance','General'],
			categories: []
		},
		{
			fetcher_id: '2006',
			fetcher_name: 'Darkweb-deepsec.net',
			sourceTypes: ['Dark Web'],
			domainTypes: ['Compliance','General'],
			categories: ['Dark Web ATB','Dark Web DTW']
		},
		{
			fetcher_id: '3003',
			fetcher_name: 'who.is',
			sourceTypes: ['Retrieve','General'],
			domainTypes: [],
			categories: ['URL ownership WI']
		},
		{
			fetcher_id: '4001',
			fetcher_name: 'Meetup.com',
			sourceTypes: ['NEWS'],
			domainTypes: ['General'],
			categories: ['Offline Group Meetings']
		},
		{
			fetcher_id: '67',
			fetcher_name: 'Digitalattackmap.com',
			sourceTypes: ['OSINT'],
			domainTypes: ['Cyber','Cyber DB RD','Cyber S','Cyber Attacks X','Cyber Hackers Tools','Cyber privacy-enhancing technologies','General'],
			categories: []
		},
		{
			fetcher_id: '27',
			fetcher_name: 'cia.gov-world-leaders',
			sourceTypes: ['Web Service'],
			domainTypes: ['General'],
			categories: []
		},
		{
			fetcher_id: '2',
			fetcher_name: 'Hunter-io',
			sourceTypes: ['OSINT'],
			domainTypes: [],
			categories: []
		},
		{
			fetcher_id: '11',
			fetcher_name: 'exclusions.oig.hhs.gov',
			sourceTypes: ['Retrieve'],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '10',
			fetcher_name: 'fbi.gov',
			sourceTypes: ['Retrieve'],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '3002',
			fetcher_name: 'whois.com',
			sourceTypes: ['Retrieve'],
			domainTypes: ['General'],
			categories: ['URL ownership WI']
		},
		{
			fetcher_id: '9',
			fetcher_name: 'unlimitedcriminalchecks.com',
			sourceTypes: ['Retrieve'],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '12',
			fetcher_name: 'denied_personlist.gov',
			sourceTypes: ['Retrieve'],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '3004',
			fetcher_name: 'blackhatworld.com',
			sourceTypes: ['Retrieve'],
			domainTypes: ['General'],
			categories: ['URL ownership WI']
		},
		{
			fetcher_id: '3005',
			fetcher_name: 'scamdetector.info',
			sourceTypes: ['Retrieve'],
			domainTypes: ['General'],
			categories: ['URL ownership WI']
		},
		{
			fetcher_id: '13',
			fetcher_name: 'sex-offenders.data.cityofchicago.org',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '14',
			fetcher_name: 'crimestoppers-uk.org',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '48',
			fetcher_name: 'SCAM_SELLER_PROFILE',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '47',
			fetcher_name: 'SCAM_POST_LOOKUP',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '27',
			fetcher_name: 'WIKIPEDIA_COMPANY_PROFILE',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '3007',
			fetcher_name: 'SCAM_ADVISOR_DOMAINS',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '17',
			fetcher_name: 'US_CRIMINAL_LOOKUP_PROFILE',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '1',
			fetcher_name: 'Open_payment_data_lookup',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '3008',
			fetcher_name: 'Sanctions_OFAC',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '2',
			fetcher_name: 'Hunter_lookup',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '1004',
			fetcher_name: 'Companies_UK_HL',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '1003',
			fetcher_name: 'Techcrunch_lookup',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '4',
			fetcher_name: 'People_ZS',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '20',
			fetcher_name: 'RIP_AND_SCAM_scammers_lookup',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '2002',
			fetcher_name: 'Social_News_aggregation_Adom',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '2003',
			fetcher_name: 'Cyber_privacy_enhancing_technologies_CP',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '2004',
			fetcher_name: 'Dark_Web_ATB',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '3003',
			fetcher_name: 'URL_ownership_WI',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '7001',
			fetcher_name: 'People_search_Mobile_TC',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		},
		{
			fetcher_id: '7',
			fetcher_name: 'Sanctions_OFAC',
			sourceTypes: [],
			domainTypes: ['Financial Crime','General'],
			categories: []
		}

	])
	.constant('EntityAPIUrl', 'http://95.85.58.85:9000/')
	.constant('FetcherImageList', [
		{
			fetcher_name: 'profiles.bloomberg.com',
			fetcher_image: 'assets/images/enrich/trans_bloomberg_pic.png'
		},
		{
			fetcher_name: 'profiles.linkedin.com',
			fetcher_image: 'assets/images/enrich/trans_linkedin_tran_pic.png'
		},
		{
			fetcher_name: 'openpaymentsdata.cms.gov',
			fetcher_image: 'assets/images/enrich/trans_openpaymentsdata_pic.png'
		},
		{
			fetcher_name: 'spokeo.com',
			fetcher_image: 'assets/images/enrich/trans_spokeo_pic.png'
		},
		{
			fetcher_name: 'interpol.int',
			fetcher_image: 'assets/images/enrich/trans_interpol_pic.png'
		},
		{
			fetcher_name: 'sanctions_wrapper',
			fetcher_image: 'assets/images/enrich/trans_sanctions_pic.png',
			display_name: 'Sanctions Wrapper'
		},
		{
			fetcher_name: 'sanctionssearch.ofac.treas.gov',
			fetcher_image: 'assets/images/enrich/trans_sanctions_pic.png'
		},
		{
			fetcher_name: 'zabasearch.com',
			fetcher_image: 'assets/images/enrich/trans_zabasearch_pic.png'
		},
		{
			fetcher_name: 'bloomberg.com',
			fetcher_image: 'assets/images/enrich/trans_bloomberg_pic.png'
		},
		{
			fetcher_name: 'OpenCorporates',
			fetcher_image: 'assets/images/enrich/trans_OpenCorporates_pic.png'
		},
		{
			fetcher_name: 'DBPedia',
			fetcher_image: 'assets/images/enrich/trans_DBPedia_pic.png'
		},
		{
			fetcher_name: 'companieslist.co.uk',
			fetcher_image: 'assets/images/enrich/trans_ukcompanies_pic.png',
			display_name: 'UK Companines List'
		},
		{
			fetcher_name: 'company.linkedin.com',
			fetcher_image: 'assets/images/enrich/trans_linkedin_tran_pic.png'
		},
		{
			fetcher_name: 'xforce.ibmcloud.com',
			fetcher_image: 'assets/images/enrich/trans_xforce_ibm_pic.png'
		},
		{
			fetcher_name: 'shodan.com',
			fetcher_image: 'assets/images/enrich/trans_shodan_pic.png'
		},
		{
			fetcher_name: 'techcrunch.com',
			fetcher_image: 'assets/images/enrich/trans_techcrunch_pic.png'
		},
		{
			fetcher_name: 'hl.co.uk',
			fetcher_image: 'assets/images/enrich/trans_hluk_pic.png',
			display_name: 'HL CO UK'
		},
		{
			fetcher_name: 'risidata.com',
			fetcher_image: 'assets/images/enrich/trans_risidata_pic.png'
		},
		{
			fetcher_name: 'stocks.bloomberg.com',
			fetcher_image: 'assets/images/enrich/trans_bloomberg_pic.png'
		},
		{
			fetcher_name: 'hunter.io',
			fetcher_image: 'assets/images/enrich/trans_hunter_pic.png'
		},
		{
			fetcher_name: 'twitter.com',
			fetcher_image: 'assets/images/enrich/trans_twiiter_pic.png'
		},
		{
			fetcher_name: 'reddit.com',
			fetcher_image: 'assets/images/enrich/trans_reddit_pic.png'
		},
		{
			fetcher_name: 'cypherpunk.at',
			fetcher_image: 'assets/images/enrich/trans_cypherpunk_pic.png'
		},
		{
			fetcher_name: 'answerstedhctbek.onion',
			fetcher_image: 'assets/images/enrich/trans_answersted_pic.png',
			display_name: 'Answerstedhctbek Onion'
		},
		{
			fetcher_name: 'deepdotweb.com',
			fetcher_image: 'assets/images/enrich/trans_deepdotweb_pic.png'
		},
		{
			fetcher_name: 'deepsec.net',
			fetcher_image: 'assets/images/enrich/trans_deepsec_pic.png'
		},
		{
			fetcher_name: 'who.is',
			fetcher_image: 'assets/images/enrich/trans_who_is_pic.png'
		},
		{
			fetcher_name: 'meetup.com',
			fetcher_image: 'assets/images/enrich/trans_meetup_pic.png'
		},
		{
			fetcher_name: 'truecaller.com',
			fetcher_image: 'assets/images/enrich/trans_truecaller_pic.png'
		},
		{
			fetcher_name: 'tools4hackers.com',
			fetcher_image: 'assets/images/enrich/trans_tools4hackers_pic.png',
			display_name: 'Tools 4 Hackers'
		},
		{
			fetcher_name: 'everypolitician.org',
			fetcher_image: 'assets/images/enrich/trans_everypolitician_pic.png'
		},
		{
			fetcher_name: 'cia.gov-world-leaders',
			fetcher_image: 'assets/images/enrich/trans_cia_gov_world_leaders_pic.png'
		},
		{
			fetcher_name: 'cidob.org',
			fetcher_image: 'assets/images/enrich/trans_cidob_pic.png'
		},
		{
			fetcher_name: 'Meetup.com',
			fetcher_image: 'assets/images/enrich/trans_meetup_pic.png'
		},
		{
			fetcher_name: 'ec.europa.eu',
			fetcher_image: 'assets/images/enrich/trans_ec_pic.png'
		},
		{
			fetcher_name: 'glassdoor.com',
			fetcher_image: 'assets/images/enrich/trans_glassdoor_pic.png'
		},
		{
			fetcher_name: 'beta.companieshouse.gov.uk',
			fetcher_image: 'assets/images/enrich/companies_house.png'
		},
		{
			fetcher_name: 'opencorporates.com',
			fetcher_image: 'assets/images/enrich/OpenCo.png'
		},
		{
			fetcher_name: 'handelsregister.de',
			fetcher_image: 'assets/images/enrich/handlesRegister.png'
		},
		{
			fetcher_name: 'companies-house',
			fetcher_image: 'assets/images/enrich/companies_house.png'
		},
		{
			fetcher_name: 'companieslist',
			fetcher_image: 'assets/images/enrich/trans_ukcompanies_pic.png',
		}

	])
	.constant('SearchAPIEndpoint', 'http://46.101.163.245:9091/graph/api/'); /* IP: 46.101.209.160:9091, 46.101.163.245:9091 */