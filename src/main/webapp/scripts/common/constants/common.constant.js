'use strict';
elementApp	   
		.constant('FLASH_ALERT_DURATION',5000)
	    .constant('FLASH_ALERT_CLOSE',true)
	    .constant('UPLOADING_FILES_NUM',12)
	    .constant('UPLOADING_KYC_FILES_NUM',6)
	    
//for 6060
// .constant('Onboarding_Survey_URL', 'http://159.65.192.32/element-questionnaire-builder/index.php/342162?newtest=Y&lang=en&userToken=')
// .constant('Generate_report_Survey_URL', 'http://159.65.192.32/element-questionnaire-builder/index.php/{serveyID}?newtest=Y&lang=en&token=');


	// for 7070
//	.constant('Onboarding_Survey_URL', 'http://188.166.40.105/element-questionnaire-builder/index.php/367223?newtest=Y&lang=en&userToken=');

	// for 8081
	// .constant('Onboarding_Survey_URL', 'http://52.211.59.166:8080/element-questionnaire-builder/index.php/531225?newtest=Y&lang=en&userToken=')
	// .constant('Generate_report_Survey_URL','http://52.211.59.166:8080/element-questionnaire-builder/index.php/531697?newtest=Y&lang=en&token=');

	// for 8080
	.constant('Onboarding_Survey_URL', 'http://52.211.59.166/element-questionnaire-builder/index.php/342162?newtest=Y&lang=en&userToken=')
	.constant('Generate_report_Survey_URL', 'http://52.211.59.166/element-questionnaire-builder/index.php/{serveyID}?newtest=Y&lang=en&token=');
