/** * START --- Common Filters *** */
'use strict';

elementApp
	   .filter('zpad', function(){
		   return function(input, n) {
				if(input === undefined) {
					input = "0";
				}			
				else if(input.length >= n) {
					return input;
				}
				else {
					var zeros = "0".repeat(n);
					return (zeros + input).slice(-1 * n);
				}
			};
	   });

	   
elementApp
	   .filter('titlecase', function() {
		    return function (input) {
		        var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
		        input = input.toLowerCase();
		        return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
		            if (index > 0 && index + match.length !== title.length &&
		                match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
		                (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
		                title.charAt(index - 1).search(/[^\s-]/) < 0) {
		                return match.toLowerCase();
		            }
		            if (match.substr(1).search(/[A-Z]|\../) > -1) {
		                return match;
		            }
		            return match.charAt(0).toUpperCase() + match.substr(1);
		        });
		    };
		});
elementApp
		.filter('unique', function() {
		   return function(collection, keyname) {
		      var output = [], 
		          keys = [];
	
		      angular.forEach(collection, function(item) {
		          var key = item[keyname];
		          if(keys.indexOf(key) === -1) {
		              keys.push(key);
		              output.push(item);
		          }
		      });
	
		      return output;
		   };
		});
elementApp
		.filter('strReplace', function () {
		  return function (input, from, to) {
		    input = input || '';
		    from = from || '';
		    to = to || '';
		    return input.replace(new RegExp(from, 'g'), to);
		  };
		});

elementApp
		.filter('capitalize', function() {
		return function(input) {
		  return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		};
});
elementApp
	.filter('toArray', function () {
		'use strict';/*jshint ignore:line*/
		return function (obj) {
			if (!(obj instanceof Object)) {
				return obj;
			}
			return Object.keys(obj).map(function (key) {
				return Object.defineProperty(obj[key], '$key', {
					__proto__: null,/*jshint ignore:line*/
					value: key
				});
			});
		};
	});
elementApp
	.filter('sourceurlbase', function () {
		return function (obj) {
			var Url = '';
			if (obj && typeof obj === 'string') {
				Url = 	obj.replace('http://','').replace('https://','').split(/[/?#]/)[0];
				// Url = Url.replace(/^(https?:\/\/)?(www.)?/i, 'www.');
			}
			return Url;
		};
	});
elementApp
	.filter('reportsharesfilter', function () {
		return function (data) {			
			return (data ?(!isNaN(data)? Number(data).formatAmt(0):data) :'-');
		};
	});
elementApp
	.filter('addhttpProtocol', function(){
		return function(val){
			var httpUrl = '';
			if(val && !val.match(/^[a-zA-Z]+:\/\//)){
				httpUrl = window.location.protocol + '//' + val;
				return httpUrl;	
			}else{
				return val;
			}
		};
	});

	elementApp
	.filter('removeSntExt', function(){
		return function(val){
			if(val){
				var findIndexSnt = val.lastIndexOf(".");
				if(findIndexSnt !== -1){
					val = val.slice(0,findIndexSnt);
					return val;
				}
			}
		};
	});
	elementApp
	.filter('replaceSpaceWithUnderscore', function(){
		return function(val){
			if(val){
				val = val.split(" ").join("_");
			}
			return val;
		};
	});