'use strict';
elementApp
	   .controller('UploadquestionaireController', uploadquestionaireController);
	   
	  uploadquestionaireController.$inject = [
			'$scope',
			'Flash',
			'$state',
			'$uibModal',
			'$rootScope',
			'HostPathService',
			'EnrichApiService',
			'$uibModalInstance',
			'EHUB_FE_API'
		];
		
		function uploadquestionaireController(
			$scope,
			Flash,
			$state,
			$uibModal,
			$rootScope,
			HostPathService,
			EnrichApiService,
			$uibModalInstance,
			EHUB_FE_API){
			
			/*
			 * @purpose: Defining scope variables
			 * @author: Ankit
			 */
			$scope.uploadquestionaire = {
					picFile:'',
					token:'',
					createcaseParams:{
						'token':''
					},
					createCase:createCase,
					questionaireStatus:false,
					closeUploadQuestionnaire:closeUploadQuestionnaire,
					returnmodalUploadQuestionnaire:returnmodalUploadQuestionnaire
			};
			$scope.uploadquestionaire.token = $rootScope.ehubObject.token;
			$scope.uploadquestionaire.createcaseParams.token = $rootScope.ehubObject.token;

			/*
		     * @purpose: Open
		     * @created: 31 jan 2018
		     * @params: file(file)
		     * @return: no
		     * @author: swathi
		    */
			
//			function createCase(file){
//
//				var filename=file.name;
//				var index = filename.lastIndexOf(".");
//				var strsubstring = filename.substring(index, filename.length);
//
//				if (strsubstring == '.pdf' || strsubstring == '.doc' || strsubstring == '.docx' || strsubstring == '.csv')
//                {
//					
//					$scope.uploadquestionaire.questionaireStatus = true;
//
//					var reader = new FileReader();
//					reader.readAsText(file, "UTF-8");
//					
//					reader.onload = function (evt) {
//						var fileContents = evt.target.result;
//						
//						var jsonData = JSON.parse(EnrichApiService.CSV2JSON(fileContents));
//						var data =  {
//							"name": jsonData[0]["Is Customer Individual Or Corporate"] == 'Individual' ? jsonData[0]["Name Of Person"] : jsonData[0]["Company Name"],
//							"description": jsonData[0]["Description"],
//							"remarks": jsonData[0]["Remark"],
//							"priority": jsonData[0]["Priority [0/1/2]"],
//							"directRisk": 0,
//							"type": jsonData[0]["Is Customer Individual Or Corporate"] + '-' + jsonData[0]["Type"],
//							"health": 0,
//							"indirectRisk": 0,
//							"transactionalRisk": 0
//						};
//						EnrichApiService.createNewCase(data,$scope.uploadquestionaire.createcaseParams).then(function(createCaseStatus) {
//							
//							if(createCaseStatus.status == '200'){ 
//								if(jsonData[0]["Is Customer Individual Or Corporate"] == "Individual"){
//									var forJsonBlob = EnrichApiService.createIndividualJSON(jsonData, createCaseStatus);
//								}else{
//									var forJsonBlob = EnrichApiService.createCorporateJSON(jsonData, createCaseStatus);
//								}
//								var fd = new FormData();
//								var jsonArr = []
//								var jsonStr = JSON.stringify(forJsonBlob);
//								jsonArr.push(jsonStr);
//								var jsonBlob = new Blob(jsonArr, {type: "application/json"});
//								var filename = "Case_" + createCaseStatus.data.id + ".json";
//								fd.append("uploadFile", jsonBlob, filename);
//								
//								EnrichApiService.caseSeedUpload(createCaseStatus.data.id,fd,$scope.uploadquestionaire.token, $rootScope.ehubObject.userId).then(function(uploadedStatus){
//									if(uploadedStatus.status == '200'){
//										$scope.uploadquestionaire.questionaireStatus = false;
//										HostPathService.FlashSuccessMessage('QUESTIONNAIRE UPLOADED SUCCESSFULLY','Document has been uploaded');
//										returnmodalUploadQuestionnaire();
//										if(!window.location.hash.includes("#!/"))
//											$state.go('discover');
//										else
//											window.location.href= EHUB_FE_API +'#/discover'  
//									}
//								},function(){
//									$scope.uploadquestionaire.questionaireStatus = false;
//									HostPathService.FlashErrorMessage('ERROR UPLOADING CASE', jsonData[0]["Is Customer Individual Or Corporate"] == 'Individual' ? jsonData[0]["Name Of Person"] : jsonData[0]["Company Name"]);
//									closeUploadQuestionnaire();
//								});
//								
//							}else{
//								HostPathService.FlashErrorMessage('ERROR UPLOADING CASE','Error');
//								
//							}
//						}, function(error) {
//							
//							HostPathService.FlashErrorMessage('ERROR UPLOADING CASE',error.responseMessage);
//							$scope.uploadquestionaire.questionaireStatus = false;
//					});
//					}
//				}
//				else
//				{
//					HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: csv, word, pdf");
//					$scope.uploadquestionaire.questionaireStatus = false;
//				}
//             }
			function createCase(file){
				var filename = file.name;
				var index = filename.lastIndexOf(".");
				var strsubstring = filename.substring(index, filename.length)?filename.substring(index, filename.length).toLowerCase():filename.substring(index, filename.length);
				
				if(file.size > 10240000){
					HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', file.name);
				}
				else if (strsubstring == '.pdf' || strsubstring == '.doc' || strsubstring == '.docx' || strsubstring == '.csv')
                {
					$scope.uploadquestionaire.questionaireStatus = true;
					var params = {
		                "token": $rootScope.ehubObject.token
					};
		            var data = {
		                uploadFile: file
		            };
		            EnrichApiService.createCaseFromQuestioner(params, data).then(function(response) {
		            	if(response == 'Cancelled'){
		            		$scope.uploadquestionaire.questionaireStatus = false;
		            		 HostPathService.FlashErrorMessage('Time limit exceed for uploading file.', '');
		            	}else{
		            		$scope.uploadquestionaire.questionaireStatus = false;
					    	HostPathService.FlashSuccessMessage('QUESTIONNAIRE UPLOADED SUCCESSFULLY','Document has been uploaded');
							returnmodalUploadQuestionnaire();
							if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
								{$state.go('discover');}
							else
								{window.location.href= EHUB_FE_API +'#/discover'; } 
		            	}

					},function(e){
						$scope.uploadquestionaire.questionaireStatus = false;
						closeUploadQuestionnaire();
						 if(e.responseMessage ==="Document is not matched with any template"){
							var ext = file.name.split('.').pop().toLowerCase();
							if ($.inArray(ext, ['pdf']) == -1) { //checking uploaded file type
								HostPathService.FlashErrorMessage("ERROR", "Document not matched with any template");
								$uibModalInstance.close();
								$rootScope.docsLoader = false;
								return;
							}
							else
							{
								$scope.openModal(file);
							}
						 }else{
							 HostPathService.FlashErrorMessage('ERROR UPLOADING CASE', e.responseMessage);
						 }
					});
				}
				else{
					HostPathService.FlashErrorMessage("UNSUPPORTED FILE FORMAT", "Invalid File Type, allowed file types are: csv, word, pdf");
					$scope.uploadquestionaire.questionaireStatus = false;
				}
             }
			
			/*
		     * @purpose: close modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function returnmodalUploadQuestionnaire(){
				$uibModalInstance.close(true);
			}
			/*
		     * @purpose: close modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closeUploadQuestionnaire(){
				$uibModalInstance.close();
			}
			
			 /*
			 * @purpose: to open modal popup To confirm to navigate to docparser page
			 * @created: 7th sep 2018
			 * @params: params
			 * @returns: data
			 * @author: prasanthi
			 */
			 $scope.openModal = function (data) {
		         var dataModal = $uibModal.open({
					templateUrl: function(){
						if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
			        		{return 'scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
			       		  else
			       		    {return '../scripts/common/modal/views/confirmationForDocparserPage.modal.html';}
					},
					controller: 'ConfirmationForDocparser',
					size: 'xs',
//					 backdrop: 'static',
					windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',
					resolve: {
						data: function () {
							return data;
						}
					}
				});
				dataModal.result.then(function () {}, function () {});
		  };
		}