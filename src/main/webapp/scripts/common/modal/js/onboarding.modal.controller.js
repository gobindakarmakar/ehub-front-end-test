'use strict';

/*elementApp.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);*/

elementApp
	   .controller('OnboardingController', onboardingController);

		onboardingController.$inject = [
			'$scope',
			'$uibModal',
			'$stateParams',
			'$uibModalInstance',
			'onBoadingDetails'
		];

		function onboardingController(
			$scope,
			$uibModal,
			$stateParams,
			$uibModalInstance,
			onBoadingDetails){
			
			/*
			 * @purpose: Defining scope variables
			 * @author: Ankit
			 */
			
			$scope.onBoardingModal = {
				isUploadQuestionaire: false,
				isUploadDocuments:onBoadingDetails.isUploadDocuments,
				closeonBoarding:closeonBoarding,
				uploadDocuments:uploadDocuments,
				displayUploadQuestionnaire:displayUploadQuestionnaire,
				uploadAdditionalDocs:uploadAdditionalDocs,
				uploadKYCQuestionaire:uploadKYCQuestionaire,
				displayIDVQuestionnaire:displayIDVQuestionnaire
			};
			
			if($stateParams.caseId){
				$scope.onBoardingModal.isUploadQuestionaire = true;
			}
			$scope.onBoardingModal.uploadedFilesLength = onBoadingDetails.uploadedFilesLength;

			/*
		     * @purpose: Open Upload Questionnaire Modal
		     * @created: 22 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function displayUploadQuestionnaire(){
				var uploadQuestionnaireModalInstance = $uibModal.open({
					templateUrl: function(){
						if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
			        		{return 'scripts/common/modal/views/uploadquestionaire.modal.html';}
			       		  else
			       		    {return '../scripts/common/modal/views/uploadquestionaire.modal.html';}
					},
					controller: 'UploadquestionaireController',
					windowClass: 'custom-modal upload-questionnaire-modal',
				});
				uploadQuestionnaireModalInstance.result.then(function(response){
					if(response){
						$scope.onBoardingModal.isUploadQuestionaire = true;
						closeonBoarding();
					}
				}, function(){
					closeonBoarding();
				});
			}
			
			/*
		     * @purpose: Open IDV Questionnaire Modal
		     * @created: 22 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function displayIDVQuestionnaire(){
				$scope.onBoardingModal.closeonBoarding();
				var displayIDVQuestionnaireModalInstance = $uibModal.open({
					templateUrl: function(){
						if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
			        		{return 'scripts/common/modal/views/idv.questionaire.modal.html';}
			       		  else
			       		    {return '../scripts/common/modal/views/idv.questionaire.modal.html';}
					},
					controller: 'IdvQuestionaireController',
					windowClass: 'custom-modal upload-questionnaire-modal idv-questionnaire full-width-modal',
				});
				displayIDVQuestionnaireModalInstance.result.then(function(response){
					if(response){
						$scope.onBoardingModal.isUploadQuestionaire = true;
						$scope.onBoardingModal.closeonBoarding();
					}
				}, function(){
					$scope.onBoardingModal.closeonBoarding();
				});
			}
			/*
		     * @purpose: upload documents
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function uploadDocuments(){
				onBoadingDetails.getUploadedFilesLimit = 6;
				var uploadDocumentsModalInstance = $uibModal.open({
					templateUrl: function(){
						if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
			        		{return 'scripts/common/modal/views/upload.documents.modal.html';}
			       		  else
			       		    {return '../scripts/common/modal/views/upload.documents.modal.html';}
					},
					controller: 'UploadDocumentsController',
					windowClass: 'custom-modal upload-documents-modal',
					resolve:{
						onBoadingDetails : function(){
							return onBoadingDetails;
						}
					}
				});
				uploadDocumentsModalInstance.result.then(function(response){
					if(response){
						$scope.onBoardingModal.uploadedFilesLength = response.uploadedFilesLength;
						$scope.onBoardingModal.isUploadDocuments = response.isUploadDocuments;
					}	
				}, function(){
				});
			}
			
			/*
		     * @purpose: upload Additional documents
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function uploadAdditionalDocs(){
				onBoadingDetails.getUploadedFilesLimit = -6;
				var uploadDocumentsModalInstance = $uibModal.open({
					templateUrl: function(){
						if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
			        		{return 'scripts/common/modal/views/upload.documents.modal.html';}
			       		  else
			       		    {return '../scripts/common/modal/views/upload.documents.modal.html';}
					},
					controller: 'UploadDocumentsController',
					windowClass: 'custom-modal upload-documents-modal',
					resolve:{
						onBoadingDetails : function(){
							return onBoadingDetails;
						}
					}
				});
				uploadDocumentsModalInstance.result.then(function(response){
					if(response){
					}	
				}, function(){
					
				});
			}
			
			/*
		     * @purpose: upload KYC Questionaire
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function uploadKYCQuestionaire(){
				var uploadKYCQuestionaireModalInstance = $uibModal.open({
						templateUrl: function(){
							if(!(window.location.hash.indexOf("#!/") >= 0))/*jshint ignore:line*/
				        		{return 'scripts/common/modal/views/upload.KYCquestionaire.modal.html';}
				       		  else
				       		    {return '../scripts/common/modal/views/upload.KYCquestionaire.modal.html';}
						},
						controller: 'UploadKYCQuestionaireController',
						windowClass: 'custom-modal upload-questionaire-modal',
						resolve:{
							onBoadingDetails : function(){
								return onBoadingDetails;
							}
						}
					});
				  uploadKYCQuestionaireModalInstance.result.then(function(response){
						if(response){
						}	
					}, function(){
						
					});
			}
			
			
			/*
		     * @purpose: close modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closeonBoarding(){
				$uibModalInstance.dismiss('close');

			}
			
		}
		
		