'use strict';
angular.module('ehubApp')
		.controller('ScreeningTestingModalController', screeningTestingModalController);

screeningTestingModalController.$inject = [
	'$scope',
	'$http',
	'$timeout',
	'data',
	//'articles',
	//'header',
	'$rootScope',	 
	'$uibModalInstance'
	//'selectedindex'
];

function screeningTestingModalController(
	$scope,
	$http,
	$timeout,
	data,
	//articles,
	//header,
	$rootScope,	 
	$uibModalInstance
	//selectedindex
	){
	
		$scope.data = data;
		$scope.listtabledata  = $scope.data.hits[0].fields;
		$scope.slectedHit =0;
		$scope.closeScreeningTestingModal = function(){
			$uibModalInstance.close('close');
		};
		
		//-----------------------------------------------------------------------------
		/**
		 * Function to update table data
		 */
		$scope.updatetableData = function(hit,index){
			$scope.listtabledata  = hit.fields;
			$scope.slectedHit = index;
		};
} 