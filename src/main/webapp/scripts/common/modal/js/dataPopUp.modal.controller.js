'use strict';
elementApp
	   .controller('DataModalController', dataModalController);

		dataModalController.$inject = [
			'$scope',
			'$rootScope',
			'data',
			'$uibModalInstance',
			'$timeout',
			'$uibModal'
		];
	    function dataModalController(
	    		$scope,
	    		$rootScope,
	    		data,
	    		$uibModalInstance,
				$timeout,
				$uibModal){
	    	
	    	/*
	         * @purpose:Set data in modal
	         * @created: 9 Jan 2018
	         * @author: varsha
	        */
	    	$timeout(function(){
	    		  $("#mainData").html(data.txt)	;
	    		  console.log(data);
	    	},0);
	    	if(data.name && data.type){
	    		$scope.header=data.name+'('+data.type+')';
	    	}
	    	else{
	    		$scope.header="Additional Information";
	    	}
	    	$scope.closeDataPopUp=function(){
	    		$uibModalInstance.close();
	    	};
	    	 $("body").off("click", ".bs-link-modal",window.findINdexForAdverse);
	    	window.findINdexForAdverse = function (e) {
	          	e.preventDefault();
	          	var listItem = $(".bs-link-modal");/*jshint ignore:line*/
	          	var indexofItem = (($('#mainData table tr').index($(this).closest('tr'))) - 1);
	          	$scope.openComanyBst(indexofItem);
	          };
	     
          $("body").on("click", ".bs-link-modal",window.findINdexForAdverse);
          $scope.openComanyBst = function (index) {
          	console.log('index: ', index);
          	console.log('data: ', $scope.totalData);

          	var articles = (data.adverseNews_url && data.adverseNews_url.length > 0)  ? data.adverseNews_url:(data.FalseNews? data.FalseNews : []);
          	console.log('articles: ', articles);
          	var articleVisualizerModalInstance = $uibModal.open({
          		templateUrl: 'direct-adverse-news-modal.html',
          		controller: 'adverseModalController',
          		size: 'lg',
          		backdrop: 'static',
          		windowClass: 'custom-modal full-analysis-wrapper',
          		resolve: {
          			text: function () {
          				return articles[index].text;
          			},
          			articles: function () {
          				return articles;
          			},
          			header: function () {
          				return articles[index];
          			},
          			selectedindex: function () {
          				return index;
          			}
          		}
          	});

          	articleVisualizerModalInstance.result.then(function () {}, function () {});
		  };
		  
	    }
	    	
	    	