'use strict';
elementApp
	   .controller('IdvQuestionaireController', idvQuestionaireController);

		idvQuestionaireController.$inject = [
			'$sce',
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'Onboarding_Survey_URL',
			'Generate_report_Survey_URL'
		];

		function idvQuestionaireController(
			$sce,
			$scope,
			$rootScope,
			$uibModalInstance,
			Onboarding_Survey_URL,
			Generate_report_Survey_URL){
			
			/*
			 * @purpose: Defining scope variables
			 * @author: Ankit
			 */
			$scope.idvQuestionaire = {
				surveyLink: Onboarding_Survey_URL + $rootScope.ehubObject.token,
				filterUrl: filterUrl,
				closeidvQuestionaireModal: closeidvQuestionaireModal,
				onClickAgree: onClickAgree,
				closeOnBoardingQuestionnaireModal: closeOnBoardingQuestionnaireModal
			};
			if($rootScope.openCheckList){
				var generate_report_surveyUrl = Generate_report_Survey_URL.replace('{serveyID}',$rootScope.qbServeyID);
				$scope.idvQuestionaire.surveyLink = generate_report_surveyUrl+$rootScope.ehubObject.token+"&entityId="+$rootScope.entityObj["@identifier"];
				
			}
			$scope.showOnBoardingIntro = true;
        	$scope.showOnBoardingQuestionary = false;
			
			/*
		     * @purpose: Iframe src
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function filterUrl(){
				return $sce.trustAsResourceUrl($scope.idvQuestionaire.surveyLink);
			}
			
			/*
		     * @purpose: close Id&v questionnaire modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closeidvQuestionaireModal(){
				$uibModalInstance.dismiss('close');
				$rootScope.openCheckList = false;
			}
			/*
		     * @purpose: close onBoarding questionnaire modal
		     * @created: 24 oct 2017
		     * @params: none
		     * @return: no
		     * @author: swathi
		    */
			function closeOnBoardingQuestionnaireModal(){
				$uibModalInstance.dismiss('close');
				$rootScope.openCheckList = false;
			}
			/*
		     * @purpose: on click agree button
		     * @created: 24 oct 2017
		     * @params: none
		     * @return: no
		     * @author: swathi
		    */
			function onClickAgree(){
            	$scope.showOnBoardingIntro = false;
            	$scope.showOnBoardingQuestionary = true;
            	$rootScope.isLimeServerModalOpened = true;
            }
			
			$scope.$on("LimeServerOnCloseEvent", function(evt,data){ /*jshint ignore:line*/
			   closeOnBoardingQuestionnaireModal();
			});
		}
		
		
		