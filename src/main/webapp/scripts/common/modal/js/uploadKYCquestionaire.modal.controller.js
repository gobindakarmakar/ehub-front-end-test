'use strict';
elementApp
	   .controller('UploadKYCQuestionaireController', uploadKYCQuestionaireController);

uploadKYCQuestionaireController.$inject = [
			'$scope',
			'$uibModal',
			'$stateParams',
			'$uibModalInstance',
			'onBoadingDetails'
		];

		function uploadKYCQuestionaireController(
			$scope,
			$uibModal,
			$stateParams,
			$uibModalInstance,
			onBoadingDetails){
			/*
			 * @purpose: Defining scope variables
			 * @author: Ankit
			 */
			$scope.uploadKYCQuestionaire = {
					progressbarValue: 0,
					closeUploadKYCModal:closeUploadKYCModal,
					uploadFiles:uploadFiles,
					changeFileDetails:changeFileDetails,
					deleteFile:deleteFile
			};
			$scope.uploadKYCQuestionaire.uploadedKYCFiles = onBoadingDetails.uploadedKYCFiles;
			function uploadFiles(){
				
			}
			
			function changeFileDetails(){
				
			}
			
			function deleteFile(){
				
			}
			/*
		     * @purpose: close modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closeUploadKYCModal(){
				$uibModalInstance.dismiss('close');
			}
			
		}
		
		