'use strict';
angular.module('ehubApp')
	   .controller('WidgetCaptureModalController', widgetCaptureModalController);

		widgetCaptureModalController.$inject = [
			'$scope',
			'$rootScope',
			'widgetId',
			'canvasnew',
			'$uibModalInstance',
			'topLocMapView'
		];
	    function widgetCaptureModalController(
	    		$scope,
	    		$rootScope,
	    		widgetId,
	    		canvasnew,
	    		$uibModalInstance,
	    		topLocMapView){
	    	
	    	$scope.widgetCapturePreloader = true;
	    	var mainGetCanvas;
	    	/*
		     * @purpose: captured widget preview
		     * @created: 14 sep 2017
		     * @params: null
		     * @return: no
		     * @author: varsha
		    */
	   $(document).ready(function(){
	       var base_image;
	       var icon_image;
	    	if((widgetId=='topLocationsdiv' && topLocMapView == true)|| (widgetId=='actTopLocationsdiv' && topLocMapView == true) || (widgetId=='geographicDistribution' && topLocMapView == true)){
	        	var leafLetId = widgetId;
	        	if($rootScope.leafletMap != undefined){
		    		html2canvas($('#map'), {
		    			    useCORS:true,
				 			onrendered : function(canvas) {
				 				 var img = document.createElement('img');
				 				 img.src = canvas.toDataURL();
				 				base_image=img.src;
				 				$('#mainWidgetCapturePreview').html(img);
				 				html2canvas($('#mapInfo'), {
				    			    useCORS:true,
						 			onrendered : function(canvas1) {
						 				 var img2 = document.createElement('img');
						 				 img2.src = canvas1.toDataURL();
						 				var header_image=img2.src;
						 			 img2.setAttribute("style", "position:absolute;left:50%;top:0px;transform: translateX(-50%);z-index:999;");
				 		  		leafletImage($rootScope.leafletMap, function(err, canvas) {/*jshint ignore:line*/
					    		    var img = document.createElement('img');
					    		    var dimensions = $rootScope.leafletMap.getSize();
		 //			    		    img.width = dimensions.x;
		//			    		    img.height = dimensions.y;
					    		    img.src = canvas.toDataURL();
					    		    icon_image= img.src ;
					    		    img.setAttribute("style", "position:absolute;left:50%;top:50%;transform: translate(-50%,-50%);z-index:3;");
					    		    $('#mainWidgetCapturePreview').append(img);
					    		    $('#mainWidgetCapturePreview').append(img2);
					    		    $scope.widgetCapturePreloader = false;
					    			$scope.$apply();
						    			if(base_image!=undefined&&icon_image!=undefined) { 
							    		  var b_canvas1 = document.getElementById("downloadleafletImage");
								    		  b_canvas1.setAttribute("width",dimensions.x);
								    		  b_canvas1.setAttribute("height",dimensions.y);
								    		  	var b_context1 = b_canvas1.getContext("2d");
								    		  		var base_image1 = new Image();/*jshint ignore:line*/
									    	        base_image1.src=base_image;
									    	        base_image1.onload=function(){
									    	        b_context1.drawImage(base_image1,0,0);
									    	        };
									    	        var icon_image1 = new Image();/*jshint ignore:line*/
									    	        icon_image1.src=icon_image;	
									    	        icon_image1.onload=function(){
									                b_context1.drawImage(icon_image1,0,0);/*jshint ignore:line*/
									    	        };
									                var header_image1 = new Image();/*jshint ignore:line*/
									                header_image1.src=header_image;	
									                header_image1.onload=function(){
									                b_context1.drawImage(header_image1,0,0);
									    	        };
							    	   }	
			    	    	 				mainGetCanvas=b_canvas1;
				 		  		});
						 			}
				 				});
			 	 }
	        	});
        	}else{
        		html2canvas($('#' + leafLetId), {		
	    			 onrendered : function(canvas) {	
	    				 console.log(canvas);
	    			 	mainGetCanvas = canvas;	
	    			 	var img = document.createElement('img');
	    			 	img.src = canvas.toDataURL();
	    			 	img.id='captureID';
	    			 	var width=$("#mainWidgetCapturePreview").width();
	    			 	width=width-300;
	    			 	 img.style.width=width+'px';
			    	 	$('#mainWidgetCapturePreview').html(img);
	    			 	$scope.widgetCapturePreloader = false;
	    			 	$scope.$apply();
	    				}		 	
	    		});
        	}
	    }else if(widgetId=='createCaseWrapper'){
	    	$('#mainWidgetCapturePreview').html(canvasnew);
	    	mainGetCanvas = canvasnew;	
	    	$scope.widgetCapturePreloader = false;
	    }else if(widgetId=='myCaseDiaryGridster'){
	    	var node = document.getElementById("myCaseDiaryGridster");
	    	domtoimage.toPng(node).then(function(dataUrl){/*jshint ignore:line*/
	    		var img = new Image();/*jshint ignore:line*/
	    		img.src=dataUrl;
	    		$('#mainWidgetCapturePreview').html(img);
	    			$scope.widgetCapturePreloader = false;
	    			$scope.$apply();
	    	}).catch(function(error){
	    		console.log("error Found",error);
	    	});
//			$scope.widgetCapturePreloader = false;
	    }else if(widgetId=='chartContentDiv'){
	    	var node = document.getElementById("chartContentDiv");
	    	domtoimage.toPng(node).then(function(dataUrl){/*jshint ignore:line*/
	    		var img = new Image();/*jshint ignore:line*/
	    		img.src=dataUrl;
	    		$('#mainWidgetCapturePreview').html(img);
	    		$('#mainWidgetCapturePreview').find("img").addClass("img-responsive");
	    			$scope.widgetCapturePreloader = false;
	    			$scope.$apply();
	    	}).catch(function(error){
	    		console.log("error Found",error);
	    	});
//			$scope.widgetCapturePreloader = false;
	    	    }else if(widgetId=='actRelatedCasesDiv'){
	    	$("#relatedCaseDivAp").show();
	    	var width1 = $("#actRelatedCasesDiv").width();
	    	var heightResp = document.getElementById('actRelatedCasesDiv').clientHeight ? document.getElementById('actRelatedCasesDiv').clientHeight : 300;
			if(heightResp > 310){
				heightResp = "530px";
			}else{
				heightResp = "230px";
			}
	    	$("#relatedCaseDivAp").css({"width":width1,"height":heightResp,"z-index":-1});
	    	$("#relatedCaseDivAp").find("li table").css({"height":heightResp});
	    	var node = document.getElementById("casesDiv");
	    	//node.prepend("RELATED CASES");
	    	domtoimage.toPng(node).then(function(dataUrl){/*jshint ignore:line*/
	    		var img = new Image();/*jshint ignore:line*/
	    		img.src=dataUrl;
	    		img.setAttribute("style", "position:absolute;left:30%");
	    		$('#mainWidgetCapturePreview').html(img);
	    		$('#mainWidgetCapturePreview').find("img").addClass("img-responsive");
				$scope.widgetCapturePreloader = false;
				$("#relatedCaseDivAp").hide();
				$scope.$apply();
	    	}).catch(function(error){
	    		$("#relatedCaseDivAp").hide();
	    		console.log("error Found",error);
	    	});

	    }else{
	    		html2canvas($('#' + widgetId), {		
	    			 onrendered : function(canvas) {	
	    				 console.log(canvas);
	    			 	mainGetCanvas = canvas;	
	    			 	var img = document.createElement('img');
	    			 	img.src = canvas.toDataURL();
	    			 	img.id='captureID';
	    			 	var width=$("#mainWidgetCapturePreview").width();
	    			 	width=width-300;
	    			 	 img.style.width=width+'px';
			    	 	$('#mainWidgetCapturePreview').html(img);
	    			 	$scope.widgetCapturePreloader = false;
	    			 	$scope.$apply();
	    				}		 	
	    		});
	    }
});
	 		/*
		     * @purpose: download captured widget
		     * @created: 14 sep 2017
		     * @params: null
		     * @return: no
		     * @author: swathi
		    */
	 		$scope.mainDownloadWidgetCapture = function(){
	 			var fileName, filePattern = /\.[0-9a-z]+$/i;
	 			if(!$scope.captureFilename){
	 				fileName = 'Download.png';
	 			}else{
	 				if($scope.captureFilename.match(filePattern)){
	 					fileName = $scope.captureFilename;
	 				}else{
	 					fileName = $scope.captureFilename + '.png';
	 				}
	 			}
	 			
	 			if(widgetId=='myCaseDiaryGridster'  || widgetId == 'chartContentDiv'){
	 				domtoimage.toBlob(document.getElementById(widgetId),{bgcolor:"#2a3843"})/*jshint ignore:line*/
	 			    .then(function (blob) {
	 			        window.saveAs(blob, fileName);
	 			    });
	 			}else if(widgetId=='actRelatedCasesDiv'){
	 				$("#relatedCaseDivAp").show();
	 				domtoimage.toBlob(document.getElementById("casesDiv"),{bgcolor:"#2a3843"})/*jshint ignore:line*/
	 			    .then(function (blob) {
	 			        window.saveAs(blob, fileName);
	 			       $("#relatedCaseDivAp").hide();
	 			    });
	 			}else{
	 				var imageData = mainGetCanvas.toDataURL("image/png");
	 				var newData = imageData.replace(/^data:image\/png/, "data:application/octet-stream");
	 				$('#mainDownloadWidgetCapture').attr("download", fileName).attr("href", newData);
	 			}

 			};
 			 /*
 		 	*@purpose: close widget capture modal
 		 	*@created: 14 sep 2017
 		 	*@params: none
 		 	*@returns: none
 		 	*@author: swathi
 		 	*/
 		 	$scope.closeWidgetCaptureModal = function(){
 		 		$uibModalInstance.dismiss('close');
 		 	};
	    }














