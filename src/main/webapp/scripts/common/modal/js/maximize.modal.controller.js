'use strict';
angular.module('ehubApp')
	   .controller('MaximizeModalController', maximizeModalController);

			maximizeModalController.$inject = [
			'$scope',
			'$rootScope',
			'Id',
			'$uibModalInstance',
			'TimeScaleBubbleConst',
			'HotTopicsConst',
			'EHUB_API',
			'DiscoverApiService',
			'DiscoverService',
			'EntitiesInFocusConst',
			'ChartOptions',
			'$timeout',
			'AssociatedEntitiesConst',
			'ActService',
			'ActHotTopicsConst'
		];
	    function maximizeModalController(
	    		$scope,
	    		$rootScope,
	    		Id,
	    		$uibModalInstance,
	    		TimeScaleBubbleConst,
	    		HotTopicsConst,
	    		EHUB_API,
	    		DiscoverApiService,
	    		DiscoverService,
	    		EntitiesInFocusConst,
	    		ChartOptions,
	    		$timeout,
	    		AssociatedEntitiesConst,
	    		ActService,
	    		ActHotTopicsConst
	    		){
	    	$scope.modalOpen = true;
	    	
	    	$scope.closeWidgetCaptureModal = function(){
	    		if(ChartOptions != undefined && (Object.keys(ChartOptions)).length != 0){
		    		if(Id=='hottopicsdiv'){
			    		$('#tagcloud').empty();
			    		ChartOptions.container='#tagcloud';
				    	ChartOptions.height=HotTopicsConst.height;
			    		DiscoverService.setHotTopicsCloudChart(ChartOptions, 'discover');
		    		}else if(Id=='acthottopicsdiv'){
		    			$('#hottopics').empty();
			    		ChartOptions.container='#hottopics';
				    	ChartOptions.height=ActHotTopicsConst.height;
		    			ActService.setHotTopicsCloudChart(ChartOptions);
		    		}
	    		}
	    		$uibModalInstance.dismiss('close');
	    		$scope.modalOpen = false;
	    	};
	    	/*
			 * @purpose: Append chart for full screen 
			 * @created: 3 Jan 2018		
			 * @author: varsha
			 */
		    if(Id=='myEntitiesdiv'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
		    	  $timeout(function(){
		    		  var reduceheight=$('#maximizeChart').height()/7;
		    		  var exampleChart = new timescalebubbleChart({/*jshint ignore:line*/
	                    container:'#maximizeChart',
	                    data: ChartOptions,
	                    header: TimeScaleBubbleConst.header,
	                    height: $('#maximizeChart').height()-reduceheight,
	                    isheader: TimeScaleBubbleConst.isheader,
	                    xticks: TimeScaleBubbleConst.xticks,
	                    margin: TimeScaleBubbleConst.margin                        
		    		  },0);
		    	  });
		    	  
		    	  /*responsiveness*/
			        $(window).on("resize",  function () {
			        	 if($scope.modalOpen == true){
			                $('#maximizeChart').empty();
			                var reduceheight=$('#maximizeChart').height()/7;
			                setTimeout(function(){
			 	               var exampleChart = new timescalebubbleChart({/*jshint ignore:line*/
			 	                    container:'#maximizeChart',
			 	                    data:ChartOptions,
			 	                    header: TimeScaleBubbleConst.header,
			 	                    height:$('#maximizeChart').height()-reduceheight,
			 	                    isheader: TimeScaleBubbleConst.isheader,
			 	                    xticks: TimeScaleBubbleConst.xticks,
			 	                    margin: TimeScaleBubbleConst.margin                        
			 		            },0);
			 		         });
			        	 }
			        });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
		    	$scope.header='MY ENTITIES';
		     }else if(Id=='discoverEntitiesInFocusdiv'){
		    	 if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
			    	 setTimeout(function(){
			    	    var reduceheight=$('#maximizeChart').height()/7;
			    		ChartOptions.container='#maximizeChart';
			    	    ChartOptions.height=$('#maximizeChart').height()-reduceheight;
			      		DiscoverService.setEntitiesInFocusBubblesChart(ChartOptions);
			    	 },0);
		    	      $(window).on("resize",  function () {
		    	    	  if($scope.modalOpen == true){
			    	    	  $('#maximizeChart').empty();
			    	    	  var reduceheight=$('#maximizeChart').height()/7;
			    	    	  ChartOptions.container='#maximizeChart';
		    	    		  ChartOptions.height=$('#maximizeChart').height()-reduceheight;	
		      				  DiscoverService.setEntitiesInFocusBubblesChart(ChartOptions);
		    	    	  }
		    	      });
		    	 }else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	 }
			   $scope.header='ENTITIES IN FOCUS';
		    }else if(Id=='hottopicsdiv'){
		    	console.log("ChartOptions", ChartOptions, (Object.keys(ChartOptions)).length);
		    	if(ChartOptions != undefined && (Object.keys(ChartOptions)).length != 0){
		    	   $timeout(function(){
			    		var reduceheight=$('#maximizeChart').height()/7;
			    		var colorScale = d3.scaleOrdinal().range(HotTopicsConst.colors);
						var lis = "";
						
						angular.forEach(ChartOptions.data1,function(d){  
							d.color = HotTopicsConst.colorObj[d.group.toLowerCase()] ? HotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
							d.size  =d.count;
							d.text = d.name;
							lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
						});
						var width = $('#maximizeChart').width();
						var height = $('#maximizeChart').height()-reduceheight;
						var radius = Math.sqrt((width * height) / 6.14);
						cloudTag({width:width,height:height,data:ChartOptions.data1,ballSize: radius - HotTopicsConst.ballSize, "id":"#maximizeChart"});
						
//						$("#maximizeChart").append(lis);
//						$("#maximizeChart").cloudTag({ballSize: radius - HotTopicsConst.ballSize, "id":"maximizeChart"});
		    	   }, 0);
		    	   
		    	   $(window).on("resize",  function () {
		    		   if($scope.modalOpen == true){
			    		   	$('#maximizeChart').empty();
		    		   		var reduceheight=$('#maximizeChart').height()/7;
		    		   		var colorScale = d3.scaleOrdinal().range(HotTopicsConst.colors);
		    		   		var lis = "";
							
		    		   		angular.forEach(ChartOptions.data1,function(d){  
		    		   			d.color = HotTopicsConst.colorObj[d.group.toLowerCase()] ? HotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
		    					d.size  =d.count;
		    					d.text = d.name;
		    		   			lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
		    		   		});
		    		   		var width = $('#maximizeChart').width();
		    		   		var height = $('#maximizeChart').height()-reduceheight;
		    		   		var radius = Math.sqrt((width * height) / 7.14);
		    		   		cloudTag({width:width,height:height,data:ChartOptions.data1,ballSize: radius - HotTopicsConst.ballSize, "id":"#maximizeChart"});
							
//							$("#maximizeChart").append(lis);
//							$("#maximizeChart").cloudTag({ballSize: radius - HotTopicsConst.ballSize, "id":"maximizeChart"});
		    		   }
			      });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
		    	$scope.header = 'HOT TOPICS';
		    }else if(Id=='associatedEntities'){
		    	if(ChartOptions != undefined && ChartOptions.length != 0){
			     	$timeout(function(){
				        var reduceheight=$('#maximizeChart').height()/7;
				    	 var exampleChart = new timescalebubbleChart({/*jshint ignore:line*/
				        	 container: '#maximizeChart',
				        	 data: ChartOptions,
				        	 width: $('#maximizeChart').width(),
				        	 height:$('#maximizeChart').height()-reduceheight,
				        	 isheader: AssociatedEntitiesConst.isheader,
				        	 ytext: AssociatedEntitiesConst.ytext,
				        	 xticks: AssociatedEntitiesConst.xticks,
				        	 margin: AssociatedEntitiesConst.margin
				         });
			    	},0);
			    	
			    	$(window).on("resize",  function () {
			    		 if($scope.modalOpen == true){
		    	    	    $('#maximizeChart').empty();
	    	    	    	var reduceheight=$('#maximizeChart').height()/7;
	    	    	    	var exampleChart = new timescalebubbleChart({/*jshint ignore:line*/
					        	 container: '#maximizeChart',
					        	 data: ChartOptions,
					        	 width:$('#maximizeChart').width(),
					        	 height:$('#maximizeChart').height()-reduceheight,
					        	 isheader: AssociatedEntitiesConst.isheader,
					        	 ytext: AssociatedEntitiesConst.ytext,
					        	 xticks: AssociatedEntitiesConst.xticks,
					        	 margin: AssociatedEntitiesConst.margin
					    	 });
			    		 }
			    	});
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
			   $scope.header='ASSOCIATED ENTITIES'; 
		    }else if(Id=='riskRationdiv'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
			    	$timeout(function(){
				    	 var reduceheight=$('#maximizeChart').height()/7;
				    	  ChartOptions.container='#maximizeChart';
				    	  ChartOptions.height=$('#maximizeChart').height()-reduceheight;
				    	  ChartOptions.isheader=false;
		    	    	  var exampleChart = new clusterbubbleChart(ChartOptions);/*jshint ignore:line*/
			    	 },0)  ;
		        /* responsiveness */
			        $(window).on("resize", function() {
			        	 if($scope.modalOpen == true){
				            $("#maximizeChart").empty();
			            	var reduceheight=$('#maximizeChart').height()/7;
					    	  ChartOptions.container='#maximizeChart';
					    	  ChartOptions.height=$('#maximizeChart').height()-reduceheight;
					    	  ChartOptions.isheader=false;
			    	    	  var exampleChart = new clusterbubbleChart(ChartOptions);/*jshint ignore:line*/
			        	 }
			        });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
		        $scope.header='RISK RATIO';
		    }else if(Id=='actentitiesInFocusdiv'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
			    	 setTimeout(function(){
			    	    var reduceheight=$('#maximizeChart').height()/7;
			    		ChartOptions.container='#maximizeChart';
			    	    ChartOptions.height=$('#maximizeChart').height()-reduceheight;
			      		DiscoverService.setEntitiesInFocusBubblesChart(ChartOptions);
			    	 },0);
		    	      $(window).on("resize",  function () {
		    	    	  if($scope.modalOpen == true){
			    	    	  $('#maximizeChart').empty();
			    	    	  var reduceheight=$('#maximizeChart').height()/7;
					    		ChartOptions.container='#maximizeChart';
					    	    ChartOptions.height=$('#maximizeChart').height()-reduceheight;
					      		DiscoverService.setEntitiesInFocusBubblesChart(ChartOptions);
		    	    	  }
					   });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
			   $scope.header='ENTITIES IN FOCUS';
		    }else if(Id=='acthottopicsdiv'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
				     setTimeout(function(){
				    	 var reduceheight=$('#maximizeChart').height()/7;
			    		var colorScale = d3.scaleOrdinal().range(HotTopicsConst.colors);
						var lis = "";
						
						angular.forEach(ChartOptions.data1,function(d){  
							d.color = HotTopicsConst.colorObj[d.group.toLowerCase()] ? HotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
							d.size  =d.count;
							d.text = d.name;
							lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
						});
						var width = $('#maximizeChart').width();
						var height = $('#maximizeChart').height()-reduceheight;
						var radius = Math.sqrt((width * height) / 6.14);
						cloudTag({width:width,height:height,data:ChartOptions.data1,ballSize: radius - HotTopicsConst.ballSize, "id":"#maximizeChart"});
						
//						$("#maximizeChart").append(lis);
//						$("#maximizeChart").cloudTag({ballSize: radius - HotTopicsConst.ballSize, "id":"maximizeChart"});
//				    	
				     },0);
					/*responsiveness*/
			        $(window).on("resize",  function () {
			        	 if($scope.modalOpen == true){
				        	$("#maximizeChart").empty();
				        	var reduceheight=$('#maximizeChart').height()/7;
				    		var colorScale = d3.scaleOrdinal().range(HotTopicsConst.colors);
							var lis = "";
							
							angular.forEach(ChartOptions.data1,function(d){  
								d.color = HotTopicsConst.colorObj[d.group.toLowerCase()] ? HotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
								d.size  =d.count;
								d.text = d.name;
								lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
							});
							var width = $('#maximizeChart').width();
							var height = $('#maximizeChart').height()-reduceheight;
							var radius = Math.sqrt((width * height) / 6.14);
							cloudTag({width:width,height:height,data:ChartOptions.data1,ballSize: radius - HotTopicsConst.ballSize, "id":"#maximizeChart"});
							
//							$("#maximizeChart").append(lis);
//							$("#maximizeChart").cloudTag({ballSize: radius - HotTopicsConst.ballSize, "id":"maximizeChart"});
			        	 }
			        });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
			     $scope.header='HOT TOPICS';
			     
		    }else if(Id=='actCaseTimelineDiv'){
			    	setTimeout(function(){
			            if(ChartOptions ==undefined&&$rootScope.current_dataCT==undefined && $rootScope.OptionsDataCT !=undefined){
					    	var actualOptions = jQuery.extend(true, {}, $rootScope.OptionsDataCT);
					    	var reduceheight=$('#maximizeChart').height()/7;
				            actualOptions.container='#maximizeChart';
				            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
					    	ActService.chartCaseTimeLine(actualOptions);
			            }else if(ChartOptions==undefined && $rootScope.current_dataCT!=undefined){
			            	var actualOptions = jQuery.extend(true, {}, $rootScope.OptionsDataCT);
					    	var currentOptions = jQuery.extend(true, {}, $rootScope.current_dataCT);
					    	var reduceheight=$('#maximizeChart').height()/7;
				            actualOptions.container='#maximizeChart';
				            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
					    	ActService.chartCaseTimeLine(actualOptions,currentOptions);
				        }else if(ChartOptions!=undefined && Object.keys(ChartOptions).length != 0){
				        	var actualOptions = jQuery.extend(true, {}, ChartOptions);
			 		    	var reduceheight=$('#maximizeChart').height()/7;
			 	            actualOptions.container='#maximizeChart';
			 	            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
			 		     	console.log(actualOptions,"actualOptions");
			 		    	var exampleChart = new CaseTimeLineChart(actualOptions);/*jshint ignore:line*/
			            }else{
			            	setTimeout(function(){
				    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
				    		}, 0);
			            }
			    	},0);
			    	/*responsiveness*/
			        $(window).on("resize", function() {
			        	if($scope.modalOpen == true){
				        	$("#maximizeChart").empty();
			        		if(ChartOptions==undefined&&$rootScope.current_dataCT==undefined){
				 		    	var actualOptions = jQuery.extend(true, {}, $rootScope.OptionsDataCT);
				 		    	var reduceheight=$('#maximizeChart').height()/7;
				 	            actualOptions.container='#maximizeChart';
				 	            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
				 		    	ActService.chartCaseTimeLine(actualOptions);
			 	            }else if(ChartOptions==undefined && $rootScope.current_dataCT!=undefined){
			 			    	var actualOptions = jQuery.extend(true, {}, $rootScope.OptionsDataCT);
			 			    	var currentOptions = jQuery.extend(true, {}, $rootScope.current_dataCT);
			 			    	var reduceheight=$('#maximizeChart').height()/7;
			 		            actualOptions.container='#maximizeChart';
			 		            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
			 			    	ActService.chartCaseTimeLine(actualOptions,currentOptions);
			 		        }else if(ChartOptions!=undefined){
			 		        	var actualOptions = jQuery.extend(true, {}, ChartOptions);
				 		    	var reduceheight=$('#maximizeChart').height()/7;
				 	            actualOptions.container='#maximizeChart';
				 	            actualOptions.height=$('#maximizeChart').height()-reduceheight;	
				 		     	console.log(actualOptions,"actualOptions");
				 		    	var exampleChart = new CaseTimeLineChart(actualOptions);/*jshint ignore:line*/
			 	            }else{
					    		setTimeout(function(){
					    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
					    		}, 0);
					    	}
			        	}
			        });

		        $scope.header='CASE TIMELINE';
		    	
		    }else if(Id=='myCaseDiaryGridster'){
		    	if(ChartOptions != undefined && Object.keys(ChartOptions).length != 0){
			    	 setTimeout(function(){
			    	    var reduceheight=$('#maximizeChart').height()/7;
			    		ChartOptions.container='#maximizeChart';
			    	    ChartOptions.height=$('#maximizeChart').height()-reduceheight;	
			    		groupedColumChart(ChartOptions);
			    	 },0);
			    	 $(window).on("resize",  function () {
			    		 if($scope.modalOpen == true){
			    	    	  $('#maximizeChart').empty();
			    	    	  var reduceheight=$('#maximizeChart').height()/7;
					    	  ChartOptions.container='#maximizeChart';
					    	  ChartOptions.height=$('#maximizeChart').height()-reduceheight;	
					    	  groupedColumChart(ChartOptions);
			    		 }
		    	      });
		    	}else{
		    		setTimeout(function(){
		    			angular.element("#maximizeChart").html('<p class="no-data">No Data Found</p>');
		    		}, 0);
		    	}
		    	$scope.header='MY CASE DIARY';
		    }
	    }