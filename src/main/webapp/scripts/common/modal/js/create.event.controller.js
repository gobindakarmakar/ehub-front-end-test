'use strict';
elementApp
	   .controller('CreateEventController', createEventController);

		createEventController.$inject = [
			'$scope',
			'$rootScope',
			'$state', 
			'$window',
			'TopPanelApiService',
			'DiscoverApiService',
			'HostPathService',
			'$uibModalInstance',
			'calendarEvent',
			'isEdit',
			'userList'
		];
	    function createEventController(
    		$scope, 
    		$rootScope, 
    		$state, 
    		$window,
    		TopPanelApiService,
    		DiscoverApiService,
    		HostPathService,
    		$uibModalInstance,
    		calendarEvent,
    		isEdit,
    		userList){
    	
		$scope.token = $rootScope.ehubObject.token;
		$scope.isEdit = isEdit;
		$scope.user = {
			userList: userList,
			userIds: []
 		};
		if($scope.isEdit == true){
			$scope.subject = calendarEvent.subject;
			$scope.description = calendarEvent.body;
			$scope.StartDate = new Date(calendarEvent.scheduledFrom);
			$scope.EndDate = calendarEvent.scheduledTo != 'Invalid date' ? new Date(calendarEvent.scheduledTo) : '';
			
			$scope.StartDate = [$scope.StartDate.getMonth()+1, $scope.StartDate.getDate(), $scope.StartDate.getFullYear()].join('/')+' '+
							   [$scope.StartDate.getHours(), $scope.StartDate.getMinutes(), $scope.StartDate.getSeconds()].join(':');
			if($scope.EndDate != ''){
				$scope.EndDate = [$scope.EndDate.getMonth()+1, $scope.EndDate.getDate(), $scope.EndDate.getFullYear()].join('/')+' '+
						  		 [$scope.EndDate.getHours(), $scope.EndDate.getMinutes(), $scope.EndDate.getSeconds()].join(':');
			}
			$('#rest_example_3_start').datetimepicker({
	 			defaultDate: $scope.StartDate
	 		});
	 		$('#rest_example_3_end').datetimepicker({
	 			defaultDate: $scope.EndDate
	 		});
		}
		// var params = {
	    // 	token: $scope.token		    	
	    // };
    	
    	$scope.todayVariable = {
			userListNotification: [],
			notificationList: [],
			selectedUser: [],
			checkInCount: '',
			dateOptions: {},
			fromDate: false,
			toDate: false
    	};
    	
    	$scope.todayFunctions = {
			openscheduleFrom: openscheduleFrom,
			openscheduleTo: openscheduleTo,
			onChangeDateFrom: onChangeDateFrom,
			onChangeDateTo: onChangeDateTo
    	};
    	function today() {
	        $scope.todayVariable.dtFrom = null;
	        $scope.todayVariable.dtTo = null;
    	}
    	today();

    	$scope.clear = function() {
    		$scope.dt = null;
    	};

    	      
	   // Disable weekend selection
      	function disabled(data) {/*jshint ignore:line*/
      		var date = data.date,
      		mode = data.mode;
	        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
      	}
	      
      	$scope.todayVariable.dateOptions = {
	    //    dateDisabled: disabled,
	        formatYear: 'yy',
	        maxDate: new Date(2020, 5, 22),
	        minDate: new Date(1900, 1, 1),
	        startingDay: 1
      	};
	   
      	function openscheduleFrom() {
      		$scope.todayVariable.fromDate = true;
     	}
     	function openscheduleTo() {
	    	 $scope.todayVariable.toDate = true;
     	}
 		function onChangeDateFrom(date){
    		 console.log(date);
 		}
 		function onChangeDateTo(date){
    		 console.log(date);
 		}
 		
 		$("body").on("click", "#rest_example_3_start", function () {
//		  setTimeout(function(){
			  var startDateTextBox = $('#rest_example_3_start');
			  var endDateTextBox = $('#rest_example_3_end');
			  $(this).datetimepicker().datetimepicker("show");
			  $("#ui-datepicker-div").css('z-index',9999);
			  startDateTextBox.datetimepicker({
			      timeFormat: 'HH:mm',
			      dateFormat: 'mm/dd/yy',
			      maxDate: $scope.EndDate,
			      onClose: function (dateText) {
			          if (endDateTextBox.val() != '') {
			              var testStartDate = startDateTextBox.datetimepicker('getDate');
			              var testEndDate = endDateTextBox.datetimepicker('getDate');
			              if (testStartDate > testEndDate)
			                  {endDateTextBox.datetimepicker('setDate', testStartDate);}
			          } else {
			              endDateTextBox.val(dateText);
			          }
			      },
			      onSelect: function () {
			          endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
			      }
			  });
			  endDateTextBox.datetimepicker({
			      timeFormat: 'HH:mm',
			      dateFormat: 'mm/dd/yy',
			      minDate: $scope.StartDate,
			      onClose: function (dateText) {
			          if (startDateTextBox.val() != '') {
			              var testStartDate = startDateTextBox.datetimepicker('getDate');
			              var testEndDate = endDateTextBox.datetimepicker('getDate');
			              if (testStartDate > testEndDate)
			                  {startDateTextBox.datetimepicker('setDate', testEndDate);}
			          } else {
			              startDateTextBox.val(dateText);
			          }
			      },
			      onSelect: function () {
			          startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
			      }
			  });
//		  }, 0);
		});									
 		
 		/*
		 * @purpose: create calendar event
		 * @created: 4th Jun 2018
		 * @author: swathi
		 */
	    $scope.createEvent = function(){
	    	$scope.createEventPreloader = true;
	    	$scope.StartDate = moment.tz(new Date($scope.StartDate), "Africa/Abidjan").format("YYYY-MM-DDTHH:mm:ss");
			$scope.EndDate = moment.tz(new Date($scope.EndDate), "Africa/Abidjan").format("YYYY-MM-DDTHH:mm:ss");
			var data = {
    			"subject" : $scope.subject,
    			"body" : $scope.description,
    			"recipients" : $scope.user.userIds,
    			"scheduledFrom" : $scope.StartDate,
    			"scheduledTo" : $scope.EndDate != 'Invalid date' ? $scope.EndDate : ''
	    	};
	    	TopPanelApiService.createcalendarEvent(data).then(function(){
	    		$scope.createEventPreloader = false;
	    		$uibModalInstance.close('success');
	    	}, function(){
	    		$scope.createEventPreloader = false;
	    		$uibModalInstance.dismiss('failure');
	    	});
	    };
	    /*
		 * @purpose: update calendar event
		 * @created: 5th Jun 2018
		 * @author: swathi
		 */
	    $scope.updateEvent = function(){
	    	$scope.createEventPreloader = true;
	    	$scope.StartDate = moment.tz(new Date($scope.StartDate), "Africa/Abidjan").format("YYYY-MM-DDTHH:mm:ss");
			$scope.EndDate = moment.tz(new Date($scope.EndDate), "Africa/Abidjan").format("YYYY-MM-DDTHH:mm:ss");
			var params = {
				'token': $scope.token,
				'eventId': calendarEvent.notificationId
			};
			var data = {
				"notificationId": calendarEvent.notificationId,
    			"subject" : $scope.subject,
    			"body" : $scope.description,
    			"scheduledFrom" : $scope.StartDate,
    			"scheduledTo" : $scope.EndDate != 'Invalid date' ? $scope.EndDate : '',
				"recipients" : $scope.user.userIds
	    	};
	    	TopPanelApiService.updateCalendarEvent(params, data).then(function(){
	    		$scope.createEventPreloader = false;
	    		$uibModalInstance.close('updateSuccess');
	    	}, function(){
	    		$scope.createEventPreloader = false;
	    		$uibModalInstance.dismiss('updateFailure');
	    	});
	    };
 		/*
		 * @purpose: close modal
		 * @created: 4th Jun 2018
		 * @author: swathi
		 */
	    $scope.closeCreateEvent =  function() {
	    	$uibModalInstance.dismiss('close');
	    };
    }