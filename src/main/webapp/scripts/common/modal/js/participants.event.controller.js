'use strict';
elementApp
	   .controller('ParticipantsEventController', participantsEventController);

		participantsEventController.$inject = [
			'$scope',
			'$rootScope',
			'TopPanelApiService',
			'DiscoverApiService',
			'HostPathService',
			'$uibModalInstance',
			'eventId',
			'isRemove',
			'userList'
		];
	    function participantsEventController(
    		$scope, 
    		$rootScope, 
    		TopPanelApiService,
    		DiscoverApiService,
    		HostPathService,
    		$uibModalInstance,
    		eventId,
    		isRemove,
    		userList){
    	
		$scope.token = $rootScope.ehubObject.token;
		$scope.isRemove = isRemove;
		var params = {
			'token': $scope.token,
			'eventId': eventId
    	};
		$scope.user = {
			userList: [],
			userIds: []
 		};					
 		
 		if(isRemove == true){
 			getEventDetails();
 		}else{
 			$scope.user.userList = userList;
 		}
 		/*
	     * @purpose: get Calendar Event Details
	     * @created: 6th Jun 2018
	     * @author: Swathi
	    */
 		function getEventDetails(){
	    	$scope.participantsPreloader = true;
	    	TopPanelApiService.getCalendarEventDetails(params).then(function(response){
	    		$scope.participantsPreloader = false;
	    		$scope.user.userList = response.data.recipients;
	    	}, function(){
	    		$scope.participantsPreloader = false;
	    	});
	    }
 		/*
		 * @purpose: Add participants to an event
		 * @created: 6th Jun 2018
		 * @author: swathi
		 */
	    $scope.addParticipants = function(){
	    	$scope.participantsPreloader = true;
			var data = {
    			"participants" : $scope.user.userIds
	    	};
	    	TopPanelApiService.addParticipantsToEvent(params, data).then(function(){
	    		$scope.participantsPreloader = false;
	    		$uibModalInstance.close('success');
	    	}, function(){
	    		$scope.participantsPreloader = false;
	    		$uibModalInstance.dismiss('failure');
	    	});
	    };
	    /*
		 * @purpose: Remove participants from an event
		 * @created: 6th Jun 2018
		 * @author: swathi
		 */
	    $scope.removeParticipants = function(){
	    	$scope.participantsPreloader = true;
			var data = {
    			"participants" : $scope.user.userIds
	    	};
	    	TopPanelApiService.removeParticipantsFromEvent(params, data).then(function(){
	    		$scope.participantsPreloader = false;
	    		$uibModalInstance.close('removeSuccess');
	    	}, function(){
	    		$scope.participantsPreloader = false;
	    		$uibModalInstance.dismiss('removeFailure');
	    	});
	    };
 		/*
		 * @purpose: close modal
		 * @created: 6th Jun 2018
		 * @author: swathi
		 */
	    $scope.closeModal =  function() {
	    	$uibModalInstance.dismiss('close');
	    };
    }