'use strict';
elementApp
	   .controller('AddMediaModalController', addMediaModalController);

		addMediaModalController.$inject = [
			'$scope',
			'$rootScope',
			'$uibModalInstance',
			'TopPanelApiService',
			'HostPathService'
		];

		function addMediaModalController(
			$scope,
			$rootScope,
			$uibModalInstance,
			TopPanelApiService,
			HostPathService){
			
			$scope.token = $rootScope.ehubObject.token;
			$scope.uploadDocumentUploadedFile = null;
			$scope.uploadDocumentFileTitle = '';
			$scope.uploadDocumentRemarks = '';
			/*
			 * @purpose: submit uploadDocument 
			 * @created: 20 sep 2017
			 * @params: file(object)
			 * @returns: no
			 * @author: swathi
			 */
		    $scope.uploadDocumentOnSubmit = function (file,event) {
				if(event.type && event.type == 'change'){
		    	if(file){
		    	var ext = file.name.split('.').pop().toLowerCase();
					if($.inArray(ext, $rootScope.uploadDocTypeList ? $rootScope.uploadDocTypeList : ['doc','png','jpg','rtf','jpeg','docx','ppt','gif','pdf','txt','csv','json','xls','xlsx']) == -1) {
						HostPathService.FlashErrorMessage('ERROR','The uploaded file must be of '+ ($rootScope.uploadDocTypeList ? $rootScope.uploadDocTypeList : "'doc','png','jpg','rtf','jpeg','docx','ppt','gif','pdf','txt','csv','json','xls','xlsx'"));
						return;
					}else if (file.size > ($rootScope.maxFileSize ? $rootScope.maxFileSize :  20480000)) {//checking file size
						HostPathService.FlashErrorMessage('The file size exceeds the limit allowed ' +($rootScope.maxFileSize ? $rootScope.maxFile_size : '(20MB)', file.name));/*jshint ignore:line*/
						return;
					}
					else{
						if($(".custom-modal.upload-media-modal").hasClass("fromScreenShot")){
							console.log(TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value );
							$scope.addMediaPreloader = true;
							var params = {
								"token": $scope.token,
								"fileTitle": $scope.uploadDocumentFileTitle?$scope.uploadDocumentFileTitle:file.name.split('.')[0],
								"entityId":TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value ?TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value .SourceValue.identifier :"",
								"sourceName":TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value ? TopPanelApiService.uploadFromScreenShotPopupUploadIcon.value .sourceName:"",
								"docFlag" : 4,
								"isAddToPage":false
							};
							var data = {
								uploadFile: file	
							};
							console.log($scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex]);
							TopPanelApiService.uploadDocumentFromUploadIconInScreenshot(params,data).then(function(response){
								console.log('response: ', response);
								response.data.fileName = file.name;
								$uibModalInstance.close($scope.uploadDocumentFileTitle);
								$scope.addMediaPreloader = false;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].downloadLink =response.data.downloadLink;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].uploadedFileName =response.data.fileName;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].docid =response.data.docId;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideScreenIcon =true;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showUploadIcon =false;
								$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].disable =false;
								saveAddtoPage(params);
							},function() {
								$uibModalInstance.dismiss($scope.uploadDocumentFileTitle);
								$scope.addMediaPreloader = false;
								 HostPathService.FlashErrorMessage('ERROR', 'file format not supported');
							});
							
						} else{
							$scope.addMediaPreloader = true;
							var params = {
								"token": $scope.token,
								"fileTitle": $scope.uploadDocumentFileTitle,
								"remarks": $scope.uploadDocumentRemarks,
								"entityId":$rootScope.entityObj["@identifier"],
								"entityName": $rootScope.entityObj.name
							};
							if(TopPanelApiService.entityselection){
								params.docSection = TopPanelApiService.entityselection;
							}
							var data = {
								uploadFile: file	
							};
							if(ext == "png"){
								if(params.docSection){
									params.docFlag = 6;
								}
								else{
									params.docFlag= 6;
									params.docSection = "";	
								}
							}
							TopPanelApiService.uploadDocument(params, data).then(function() {
								$uibModalInstance.close($scope.uploadDocumentFileTitle);
								$scope.addMediaPreloader = false;
							}, function() {
								$uibModalInstance.dismiss($scope.uploadDocumentFileTitle);
								$scope.addMediaPreloader = false;
								HostPathService.FlashErrorMessage('ERROR', 'file format not supported');
							});
						}
					} 
				}else{
					HostPathService.FlashErrorMessage('ERROR', 'file format not supported');
				}
			}
			};
		    /*
			 * @purpose: close uploadDocument
			 * @created: 20 sep 2017
			 * @params: null
			 * @returns: no
			 * @author: swathi
			 */
		    $scope.closeUploadDocument =  function() {
		    	$uibModalInstance.dismiss('close');
			};
			
			function saveAddtoPage(params){
				var addtopagedata = [{
					"entityId": $rootScope.entityObj["@identifier"],
					"isAddToPage": false,
					"sourceName": params.sourceName
				}];
				TopPanelApiService.saveSourcesAddToPage(addtopagedata).then(function(){

					$scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideAddtoPage = false;
					// $scope.sourceWithBSTRegistry[TopPanelApiService.uploadFromScreenShotPopupUploadIcon.rowIndex].showHideScreenIcon = false;
				
				
				}, function(){
		
				}).catch(function(){
		
				});
			}
		}
		
		