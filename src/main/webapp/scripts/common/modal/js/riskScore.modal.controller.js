'use strict';
elementApp
    .controller('RiskScoreController', riskScoreController);

riskScoreController.$inject = [
    '$scope',
    '$rootScope',
    '$uibModalInstance',
    'TopPanelApiService',
    'HostPathService',
    'RiskScoreService',
    '$uibModalStack'
];

function riskScoreController(
    $scope,
    $rootScope,
    $uibModalInstance,
    TopPanelApiService,
    HostPathService,
    RiskScoreService,
    $uibModalStack) {
    $scope.domainsMap = {
        "entityRiskModel": "Company",
        "individualRiskModel": "Person",
        "riskDefaultParameters": "Default Parameters",
        "riskTolerance": "Aggregation"
    };

    setTimeout(function() {
        $('.modal-dialog').addClass('w-90vp');
    },0);
    
    
   
    
    $scope.tilesWrapper = false;
    $scope.modelTabsWrapper = false;
    $scope.riskToleranceWrapper = false;
    $scope.defaultPramsWrapper = false;
    $scope.domainWrapper = true;
    $scope.entityWrapper = false;
    $scope.disableInput = true;
    $scope.modelTabsWrapper = false;
    $scope.searchOsintValue = '';

    $scope.riskScoringFunction = {
        closeRiskModal: closeRiskModal,
        riskHomePage: riskHomePage,
        getEntityPage: getEntityPage,
        selectDomain: selectDomain,
        saveUpdatedChange: saveUpdatedChange,
        setRangeForRiskTolerance: setRangeForRiskTolerance,
        deleteDomain: deleteDomain,
        getAttributePage: getAttributePage,
        renameModel: renameModel,
        openPersonPage: openPersonPage,
        openCompanyPage: openCompanyPage,
        showEntityPage:showEntityPage,
        setDescription:setDescription,
        createNewDomainPlus:createNewDomainPlus,
        cloneDomain:cloneDomain,
        deleteYesOrNo:deleteYesOrNo,
        domainSelection:domainSelection,
        addAttribute:addAttribute,
        deleteAttribute:deleteAttribute,
        deleteOsintUI : deleteOsintUI,
        searchNewAttribute:searchNewAttribute,
        searchAttribute:searchAttribute,
        closeTemplate:closeTemplate,
        openTempletewithAttributeValue:openTempletewithAttributeValue,
        removeAttributeValues:removeAttributeValues,
        addRelationship:addRelationship,
        addOsint : addOsint,
        searchOsint:searchOsint,
        searchOsintRepoSearch:searchOsintRepoSearch,
        deleteRelationship:deleteRelationship,
        searchNewRelationship:searchNewRelationship,
        searchRelationshipValue:searchRelationshipValue,
        setRangeForRelationRiskScoresUsingSlider:setRangeForRelationRiskScoresUsingSlider,
        setRangeForOsintRiskScoresUsingSlider : setRangeForOsintRiskScoresUsingSlider,
        sayYesorNo:sayYesorNo,
        runRiskScoring:runRiskScoring,
        setRangeForAttributeRiskScoresUsingSlider:setRangeForAttributeRiskScoresUsingSlider,
        addClassToRun:addClassToRun,
        addAttributeValue:addAttributeValue,
        addConnection:addConnection,
        storeListOfDeletedAttr:storeListOfDeletedAttr,
        deleteConnection:deleteConnection,
        showDropdown:showDropdown,
        addNewAttribute:addNewAttribute,
        addNewRelationship:addNewRelationship,
        attrTypeChangeValue:attrTypeChangeValue,
        insighSelectedValue:insighSelectedValue
       
    };
    $scope.riskScoringVariable = {
        riskAttributeValue: [],
        riskRelationshipValue: [],
        showSaveButton: false,
        riskScoringLoader: true,
        riskSelectNewAttr: true,
        saveLoader: false,
        saveLoaderForAggregationTab: false,
        saveLoaderForDefaultTab: false,
        getDefaultModelValue: [],
        sampleDomainData: {},
        showRunButton: true,
        modalAttribute: [],
        riskDefaultParametersData: [],
        riskToleranceData: [],
        showModalBox:false,
        attributeValueData:[],
        CompanyPersonData:[],
        riskScoringTabLoader:false,
        showModalBox2:false,
        addAtributeIsOpen:false,
        osintData:[],
        closeAddingNewAttr:false,
        closeAddingNewRel: false
       
    };

   
    $scope.riskScoringVariable.sampleDomainData = {
    	    "decisionModelName": "AML TEST 2",
    	    "individualRiskModel": {
    	      "attributeRiskScores": [
    	       
    	      ],
    	      "connectionsScores": [
    	             ],
    	      "relationshipRiskScores": [
    	        
    	      ],
    	      "osintRiskScores": {
    	        "scores": []
    	      },
    	      "transactionScores": {
    	        "transactionGreaterThanKScore": 0.7,
    	        "kAmount": 500,
    	        "transactionsMoreThanMScore": 0.6,
    	        "mValue": 0.65,
    	        "transactionRelationshipName": "transferred",
    	        "transactionAmountFieldName": "Amount"
    	      }
    	    },
    	    "entityRiskModel": {
    	      "attributeRiskScores": [
    	        
    	      ],
    	      "connectionsScores": [
    	        
    	      ],
    	      "relationshipRiskScores": [
    	        
    	      ],
    	      "osintRiskScores": {
    	        "scores": []
    	      },
    	      "transactionScores": {
    	        "transactionGreaterThanKScore": 0.9,
    	        "kAmount": 1000,
    	        "transactionsMoreThanMScore": 0.8,
    	        "mValue": 2,
    	        "transactionRelationshipName": "transferred",
    	        "transactionAmountFieldName": "Amount"
    	      }
    	    },
    	    "riskTolerance": {
    	      "score": 60,
    	      "riskMethod": "Average"
    	    },
    	    "riskDefaultParameters": {
    	      "individualDefaultScore": 0,
    	      "entityDefaultScore": 0
    	    },
    	    "createdOn": "19/06/2018",
    	    "modifiedOn": "",
    	    "showValid": false
    	  };

  
    
    var optionBasedOnAttrType = [{
    	'key':'String',
    	'value':['equals','starting','contains','not_contains']
    },{
    	'key':'Date',
    	'value':['before','last_year','last_month','last_week','last_7_days']
    },{
    	'key':'Currency',
    	'value':['equals','greater_than','less_than','range']
    },{
    	'key':'Numeric',
    	'value':['equals','greater_than','less_than','range']
    }];


    function setDropDownData(type){
    	var index = optionBasedOnAttrType.map(function(d){
    		return d.key.toLowerCase();
    	}).indexOf(type);
    	return optionBasedOnAttrType[index];
    }
    function removeDuplicates(arr, prop) {
        var obj = {};
        for (var i = 0, len = arr.length; i < len; i++) {
            if (!obj[arr[i][prop]]) {obj[arr[i][prop]] = arr[i];}
        }
        var newArr = [];
        for (var key in obj) {newArr.push(obj[key]);}
        return newArr;
    }

    //---------------------------Api function's starts------------------------------------------------//			


    /*
     * @purpose: get all risk 
     * @created: 19th april 2018
     * @return: success, error functions
     * @author: varsha
     */
    function getRisk() {
        RiskScoreService.getrisk().then(function(response) {
//            var index = response.data.data.map(function(x) {
//            	x.id = x.id;
//                return x.id;
//            }).indexOf('Default');
//            if(index != -1){
//            	  response.data.data.splice(index, 1);
//            }
            $scope.domains = response.data.data;
            $scope.domains.sort(function(x, y) {
                return d3.ascending(x.decisionModelName, y.decisionModelName);
            });
            $scope.saveDomain = jQuery.extend(true, {},$scope.domains);
            $scope.saveDomainOriginal = jQuery.extend(true, {},$scope.domains);
            $scope.riskScoringVariable.riskScoringLoader = false;
        }, function() {
            $scope.showModal = [];
        });
    }
    getRisk();
    /*
     * @purpose: Post  risk 
     * @created: 19th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    function postRisk(data) {
        RiskScoreService.postRisk(data).then(function() {
            HostPathService.FlashSuccessMessage('DOMAIN CREATED SUCCESSFULLY', '');
            setTimeout(function(){
            	getRisk();
            },1000);
        }, function() {});
    }


    /*
     * @purpose: put risk 
     * @created: 19th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function putRisk(data,activeTime) {
        RiskScoreService.putRisk(data).then(function() {
            $scope.riskScoringFunction.saveLoader = false;
            $scope.riskScoringVariable.riskScoringTabLoader = false;
            if(activeTime){
            	getRisk();
            }
           // HostPathService.FlashSuccessMessage('SAVED SUCCESSFULLY', '');
        }, function() {});
    }


    /*
     * @purpose: delete risk 
     * @created: 19th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function deleteRisk(modelId) {
        RiskScoreService.deleteRisk(modelId).then(function() {
            HostPathService.FlashSuccessMessage('Deletion completed', '');
            setTimeout(function(){
            	 getRisk();
            },1000);
        }, function() {});
    }

    /*
     * @purpose: get default model
     * @created: 15th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function getDefaultModel() {
        RiskScoreService.getDefaultModel().then(function(response) {
            getRisk();
            $scope.getDefaultModelValue = response.data;
        }, function() {});
    }
   // getDefaultModel();
    
    
    /*
     * @purpose: get Insight Value
     * @created: 7th feb 2019
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    var attributeInsight = [];
    $scope.attributeInsightStatus=false;
    $scope.attributeInsightDataLoader = true;
    function getInsightValue(type,setForPersonOrCompany) {
    	attributeInsight = [];
    	$scope.addAttributeTypeValue = [];
        RiskScoreService.getInsightValue(type).then(function(response) {
            
            $scope.attributeInsightDataLoader = false;
            attributeInsight = response.data;
            if(attributeInsight[setForPersonOrCompany] && attributeInsight[setForPersonOrCompany].length>0){
    	    	attributeInsight[setForPersonOrCompany].map(function(d){
    	      		if($scope.addAttributeTypeValue.indexOf(d)==-1){
    	      			$scope.addAttributeTypeValue.push(d);
    	      		}
    	      	});
        	}
            if($scope.addAttributeTypeValue.length <=0){
            	$scope.attributeInsightStatus=true;
            }
          
        }, function() {
        	$scope.attributeInsightStatus=true;
        });
    }
    
    
    
    
    /*
     * @purpose: post default model
     * @created: 15th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function postDefaultModel(data) {/*jshint ignore:line*/
        RiskScoreService.postDefaultModel(data).then(function() {
            //getDefaultModel();
        }, function() {});
    }
    
    


    /*
     * @purpose: get risk attribute Values
     * @created: 19th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var attributeValue = [];
    var attributeValueOriginal =[];
    var attributeValueOriginal2 = [];
    function getRiskattributeValues() {
        RiskScoreService.getriskattributeValues().then(function(response) {
            attributeValue = jQuery.extend(true, {}, response);
            attributeValueOriginal = jQuery.extend(true, {}, response);
            attributeValueOriginal2 = jQuery.extend(true, {}, response);
            $scope.riskScoringVariable.riskSelectNewAttr = false;
            $scope.riskScoringVariable.riskScoringTabLoader = false;
        }, function() {
        	 $scope.riskScoringVariable.riskScoringTabLoader = false;
        });

    }
    getRiskattributeValues();
   

    
    /*
     * @purpose:get risk Relationship Values
     * @created: 19th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var riskRelationValues = [];
    var riskRelationValuesChange =[];
    $scope.relationShipValues = [];
    var companyRelationshipNames = [];
    var personRelationshipNames = [];
    var riskRelationValuesChangeOriginal = [];
    function getriskRelationshipValues() {
        RiskScoreService.getriskRelationshipValues().then(function(response) {
        	$scope.riskRelationOrignal = jQuery.extend(true, [], response.data);
        	riskRelationValues = [];
            riskRelationValuesChange =[];
        	$scope.relationShipValues = [];
        	companyRelationshipNames = [];
        	personRelationshipNames = [];
        	riskRelationValuesChangeOriginal = [];
            // console.log(response, 'getriskRelationshipValues')
            riskRelationValues = jQuery.extend(true, {}, response.data);
            $scope.relationShipValues = jQuery.extend(true, {}, response.data);
            $scope.relationShipValues.companyRelationshipNames.map(function(k){
            		companyRelationshipNames.push({'label':k,
            					'riskScore':0  });
            });
             $scope.relationShipValues.personRelationshipNames.map(function(k){
            	 personRelationshipNames.push({'label':k,
            					'riskScore':0});
            });
             $scope.relationShipValues.companyRelationshipNames = companyRelationshipNames;
             $scope.relationShipValues.personRelationshipNames = personRelationshipNames;
             riskRelationValuesChange = jQuery.extend(true, [],  $scope.relationShipValues);
             riskRelationValuesChangeOriginal = jQuery.extend(true, [],  $scope.relationShipValues);
        }, function() {});
    }

   
    /*
     * @purpose:post run risk score 
     * @created: 18th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */

    function postRunRiskScore(data) {
        RiskScoreService.runRiskScoring(data).then(function() {
            $scope.riskScoringVariable.riskScoringLoader = false;
        }, function() {});
    }
    
    
    /*
     * @purpose: get getSanctionsList
     * @created: 24th july 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
    var osintRepoOrigindata = [];
    function getSanctionsList() {/*jshint ignore:line*/
        osintRepoOrigindata = [];
        $scope.riskScoringVariable.osintData = [];
        RiskScoreService.getSanctionsList().then(function(response) {
            console.log(response, 'responseresponse');
            angular.forEach(response.data.data,function(v){
            	$scope.riskScoringVariable.osintData.push({
            		'relationshipName':v,
            		'riskScore':0
            	});
            });
            osintRepoOrigindata = jQuery.extend(true, [], $scope.riskScoringVariable.osintData);
            setOsinData();
        }, function() {});
    }
    
    

  
    
    /*
     * @purpose:risk name exists
     * @created: 1st june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha
     */
  
    var nameExistResult = [];
    function riskNameExists(data) {
        RiskScoreService.riskNameExists(data).then(function(response) {
            if(createNew){
	            createDefaultDomain(response);
	            createNew = false;
            }else if(renameVar){
            	renameDomain(response);
            	renameVar = false;
            }else if(cloneNew){
            	cloneToNewDomain(response,data);
            	cloneNew = false;
            }
            nameExistResult = response;
            }, function() {});
        return nameExistResult;
    }

    //---------------------------Api function's ends------------------------------------------------//



    //------------------------------------Home page functionality-----------------------//

    /*  
     * @purpose:On selecting any domain
     * @created: 21th april 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */


    var updateRiskScoringData = [];
    var setIndexValue;
    var isDomainActive = false;
    function selectDomain(modal,k,isActive) {
    	getRiskattributeValues();
    	getriskRelationshipValues();
    	isDomainActive = isActive;
    	if(modal.decisionModelName == ""){
    		modal.decisionModelName = $scope.saveDomain[k].decisionModelName;
    	}
    	$scope.setIndexValue = k;
    	setIndexValue = k;
    	$scope.riskScoringVariable.modalAttribute = [];
        $scope.selectedModalNmae = modal.decisionModelName;
        $scope.riskScoringVariable.modalAttribute = jQuery.extend(true, {}, modal); // Unchanged selected domain data
        $scope.riskScoringVariable.showRunButton = false;
        $scope.riskScoringVariable.riskScoringTabLoader = true;
        showEntityPage();
        updateRiskScoringData = $scope.riskScoringVariable.modalAttribute;
    }
    
  //----------------------------------------------------------------------------------------------------  
    
    /*
     * @purpose: select domain
     * @created: 5th june 2018
     * @author: varsha
     */
    
    var selectDomainStatus = false;
    var selectDomainStatus2 = false;
    var selectedDomainModal = [];
    function domainSelection($event,modal,isActive){
    	if(!isActive){
	    	deleteModalStatus = false;
	    	selectedDomainModal = modal;
	    	$scope.riskScoringVariable.modalMessage = 'This change will recalculate the score for all entities in the system, are you sure you wish to proceed ?';
	    	$scope.riskScoringVariable.showModalBox =true;
	    	selectDomainStatus = true;
	    	selectDomainStatus2 = false;
    	}else{
    		return false;
    	}
    }

  //---------------------------------------------------------------------------------------------------  
   
    /*
     * @purpose: delete domain
     * @created: 20th april 2018
     * @author: varsha
     */
    var deleteDomainModal = [];
    var deleteModalStatus = false;
    function deleteDomain(event, modal) {
    		selectDomainStatus =false;
    		deleteDomainModal = modal;
    		$scope.riskScoringVariable.modalMessage = 'Are you sure you want to delete this domain ?';
    	    $scope.riskScoringVariable.showModalBox =true;
    	    deleteModalStatus = true;
    }
    
    

    
//------------------------------------------------------------------------------------------------
    /*
     * @purpose: rename domain
     * @created: 20th april 2018
     * @author: varsha
     */
    var renameModal = [];
    var mainModal = [];
    var renameVar = false;
    var domainPosition;
    function renameModel(modal,k) {
    	if(k == 'clear'){
    		 modal.decisionModelName = '';
    		 return false;
    	}else{
	    	renameVar = true;
	    	mainModal = modal;
	    	domainPosition = k ;
	    	renameModal = jQuery.extend(true, {}, modal); // Unchanged selected domain data
	    	var name = modal.decisionModelName;
	    	modal.decisionModelName = name.toUpperCase();
	    	var name={'name': name};
	    	mainModal.showValid = false;
	        riskNameExists(name);
    	}
      
    }
   
   function renameDomain(nameExistResponse){
		 if(mainModal.decisionModelName.toUpperCase() ==$scope.saveDomain[domainPosition].decisionModelName.toUpperCase() || mainModal.decisionModelName ==''){
			 mainModal.decisionModelName =$scope.saveDomain[domainPosition].decisionModelName;
			 return false;
		 }else {
			 if(nameExistResponse.data.data.exists == false){
	           var name = renameModal.decisionModelName;
	           renameModal.decisionModelName = name.toUpperCase();
	           renameModal.model_id = renameModal.id;
	           renameModal.id = renameModal.id;
	           delete renameModal.$$hashKey;
	           putRisk(renameModal);
	    	}else {
	    		 mainModal.showValid = true;
	    		 mainModal.decisionModelName = $scope.saveDomain[domainPosition].decisionModelName;
	    		 setTimeout(function() {
	                delete mainModal.showValid;
	             },0);
	    	}
	   	}
   }
  
   //-------------------------------------------------------------------------------------------------
   
    /*
     * @purpose: create new domain
     * @created: 1st june 2018
     * @author: varsha
     */
    var createNew = false;
    function createNewDomainPlus(){
    	$scope.riskScoringVariable.riskScoringLoader = true;
        var name={'name':'NEW DOMAIN'};
        riskNameExists(name);
        createNew =true;
    }

    
    
    function createDefaultDomain(response){
    	 if(response){
    	    	var createNewDomain = [];
    	        createNewDomain = jQuery.extend(true, {}, $scope.riskScoringVariable.sampleDomainData);
    	            if(response.data.data.exists == 'false'){
    	            	createNewDomain.decisionModelName = 'NEW DOMAIN';
    	            	 postRisk(createNewDomain);
    	            }else {
    	            	var count = response.data.data.counter;
    	            	if(count == 0 ){
    	            		createNewDomain.decisionModelName = 'NEW DOMAIN';
    	            	}else{
    	            		createNewDomain.decisionModelName = 'NEW DOMAIN ('+ count+')';
    	            	}
    		            postRisk(createNewDomain);
    	            }
    	 }
    }
    
  //---------------------------------------------------------------------------------------------------  
    
    /*
     * @purpose: clone  domain
     * @created: 1st june 2018
     * @author: varsha
     */
    
    var cloneNew = false;
    var cloneNewDomain = [];
    function cloneDomain(modal){
    	$scope.riskScoringVariable.riskScoringLoader = true;
    	cloneNewDomain = jQuery.extend(true, {}, modal);
        var name={'name':modal.decisionModelName};
        riskNameExists(name);
        cloneNew =true;
    }
    
    function cloneToNewDomain(response,data){
		if(response){
	    	    if(response.data.data.exists == true){
	    	    	var count = response.data.data.counter;
	    	    	cloneNewDomain.decisionModelName = data.name +' '+'('+ count+')';
		            postRisk(cloneNewDomain);
	            }else {
	            	$scope.riskScoringVariable.riskScoringLoader = false;
	            }
		}
    }

    //----------------------------End of Home page functionality---------------------------------------

    //----------------------------Entity page functionality starts-------------------------------------


    $scope.riskScoringVariable.showRunButtonNew = true;
    $scope.riskScoringVariable.showSaveNew = false;


    
    /*  
     * @purpose: Set data for entity tab
     * @created: 29th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */


    function getEntityPage() {
        $scope.riskScoringVariable.riskDefaultParametersData = $scope.riskScoringVariable.modalAttribute.riskDefaultParameters;
        $scope.riskScoringVariable.riskToleranceData = $scope.riskScoringVariable.modalAttribute.riskTolerance;
        setMessageForAggregation($scope.riskScoringVariable.riskToleranceData.riskMethod);
    }
    
    
    /*  
     * @purpose: set Message For Aggregation
     * @created: 29th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */

    
    function setMessageForAggregation(method){
    	if(method == 'Least Conservative'){
    		$scope.entityHeading = 'Least Conservative';
    		$scope.setMessageAgg = 'Risk score for entity decreases as more risk factors are added.';
    	}else if(method == 'Minimum'){
    		$scope.setMessageAgg = 'Takes the minimum of all risk factors as score for an entity (e.g., min[RF1,RF2]).';
    		$scope.entityHeading = 'Minimum';
    	}else if(method == 'Average'){
    		$scope.entityHeading = 'Average';
    		$scope.setMessageAgg = 'A balanced aggregation approach that averages all risk factors as score for an entity.';
    	}else if(method == 'sum'){
    		$scope.entityHeading = 'Summation';
        	$scope.setMessageAgg ='Implements a traditional rate-and-weight approach for decision scoring.';
    	}else if(method == 'Max'){
    		$scope.entityHeading = 'Maximum';
    		$scope.setMessageAgg = 'Takes the maximum of all risk factors as a score for an entity (e.g., max[RF1,RF2]). This approach is suitable for compliance environment.';
    	}else if(method == 'Most Conservative'){
    		$scope.entityHeading = 'Most Conservative';
    		$scope.setMessageAgg = 'Risk score for entity increases as more risk factors are added.This approach is suitable for compliance environment.';
    	}
    }
    


    /*  
     * @purpose: Set risk tolerance
     * @created: 29th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */


    function setRangeForRiskTolerance() {
        if ($scope.riskScoringVariable.riskToleranceData.score == 0) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'Least Conservative';
        } else if ($scope.riskScoringVariable.riskToleranceData.score == 20) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'Minimum';
        } else if ($scope.riskScoringVariable.riskToleranceData.score == 60) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'Average';
        } else if ($scope.riskScoringVariable.riskToleranceData.score == 40) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'sum';
        } else if ($scope.riskScoringVariable.riskToleranceData.score == 80) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'Max';
        } else if ($scope.riskScoringVariable.riskToleranceData.score == 100) {
            $scope.riskScoringVariable.riskToleranceData.riskMethod = 'Most Conservative';
        }
    }
    

    /*  
     * @purpose: set desciption for Aggregation type
     * @created: 1st june 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */

    
    function  setDescription(){
        setRangeForRiskTolerance();
      	setMessageForAggregation( $scope.riskScoringVariable.modalAttribute.riskTolerance.riskMethod);
    }



    //------------------------End of Entity page functionality----------------------------------- -------

    //-----------------------Person page functionality starts-------------------------------------
    /*
     * @purpose: On click of Person page update data
     * @created: 20th april 2018
     * @author: varsha
     */
    var activePerson =false;
    function openPersonPage() {
      //  getSanctionsList();
    	 $scope.attributeInsightDataLoader = true;
        getInsightValue('person','personInsights');
    	$scope.showConnection = false;
    	activePerson = true;
    	activeCompany = false;
    	$scope.openPerson_Company ='individualRiskModel';
    	$scope.activeName = 'PERSON';
    	$scope.ativeRelationShip = 'personRelationshipNames';
    	$scope.activeAttribute = 'personAttributes';
    		console.log($scope.riskRelationOrignal[$scope.ativeRelationShip]);
    	getAttributePage();
    	$scope.addAttributeTypeValue = [];
    	if(attributeValue && attributeValue.data && attributeValue.data.personAttributes){
    		 for (var i = 0, len = $scope.domains[setIndexValue].individualRiskModel.attributeRiskScores.length; i < len; i++) { 
     	        for (var j = 0, len2 = attributeValue.data.personAttributes.length; j < len2; j++) { 
     	            if ($scope.domains[setIndexValue].individualRiskModel.attributeRiskScores[i].attributeName === attributeValue.data.personAttributes[j].attributeName) {
     	            	attributeValue.data.personAttributes.splice(j, 1);
     	                len2 = attributeValue.data.personAttributes.length;
     	            }
     	        }
     	    }
    	}
    	

    	if($scope.relationShipValues.personRelationshipNames && riskRelationValuesChange.personRelationshipNames){
	    	for (var i = 0, len = $scope.domains[setIndexValue].individualRiskModel.relationshipRiskScores.length; i < len; i++) { 
     	        for (var j = 0, len2 = riskRelationValuesChange.personRelationshipNames.length; j < len2; j++) { 
     	            if ($scope.domains[setIndexValue].individualRiskModel.relationshipRiskScores[i].label === riskRelationValuesChange.personRelationshipNames[j].label) {
     	            	riskRelationValuesChange.personRelationshipNames.splice(j, 1);
     	                len2 =riskRelationValuesChange.personRelationshipNames.length;
     	            }
     	        }
     	    }
    	}
    	$scope.relationShipValues[$scope.ativeRelationShip] = riskRelationValuesChange[$scope.ativeRelationShip];
    	if(attributeValue.data){
    		$scope.riskScoringVariable.attributeValueData = attributeValue.data.personAttributes;	
    	    
    	}
    	
    	$scope.addAttributeTypeValue.sort();
    	console.log($scope.addAttributeTypeValue,'$scope.addAttributeTypeValue');
    	addScoll('#showAttrValue',180);
    	addScoll('#docsList3',280);
    	addScoll('#attrSlider',280);
        $scope.riskScoringVariable.CompanyPersonData = $scope.domains[setIndexValue].individualRiskModel;
    }

    //-------------------------End of Person page functionality -------------------------------------

    //-----------------------Company page functionality starts-------------------------------------
    /*
     * @purpose: On click of company page update data
     * @created: 20th april 2018
     * @author: varsha
     */
    var activeCompany = false;
    function openCompanyPage() {
        //getSanctionsList();
    	 $scope.attributeInsightDataLoader = true;
        getInsightValue('company','companyInsights');
    	$scope.showConnection = false;
    	activeCompany = true;
    	activePerson =false;
    	$scope.openPerson_Company ='entityRiskModel';
    	$scope.activeName = 'COMPANY';
    	$scope.ativeRelationShip = 'companyRelationshipNames';
    	$scope.activeAttribute = 'companyAttributes';
    	getAttributePage();
    	$scope.addAttributeTypeValue = [];
    	if(attributeValue && attributeValue.data && attributeValue.data.companyAttributes){
	    	for (var i = 0, len = $scope.domains[setIndexValue].entityRiskModel.attributeRiskScores.length; i < len; i++) { 
     	        for (var j = 0, len2 = attributeValue.data.companyAttributes.length; j < len2; j++) { 
     	            if ($scope.domains[setIndexValue].entityRiskModel.attributeRiskScores[i].attributeName === attributeValue.data.companyAttributes[j].attributeName) {
     	            	attributeValue.data.companyAttributes.splice(j, 1);
     	                len2 = attributeValue.data.companyAttributes.length;
     	            }
     	        }
     	    }
    	}
    	if($scope.relationShipValues.companyRelationshipNames){
	    	for (var i = 0, len = $scope.domains[setIndexValue].entityRiskModel.relationshipRiskScores.length; i < len; i++) { 
     	        for (var j = 0, len2 = riskRelationValuesChange.companyRelationshipNames.length; j < len2; j++) { 
     	            if ($scope.domains[setIndexValue].entityRiskModel.relationshipRiskScores[i].label === riskRelationValuesChange.companyRelationshipNames[j].label) {
     	            	riskRelationValuesChange.companyRelationshipNames.splice(j, 1);
     	                len2 =riskRelationValuesChange.companyRelationshipNames.length;
     	            }
     	        }
     	    }
    	}
    	 $scope.relationShipValues[$scope.ativeRelationShip] = riskRelationValuesChange[$scope.ativeRelationShip];
    	 addScoll('#showAttrValue',180);
    	addScoll('#attrSlider',280);
    	addScoll('#docsList3',280);
    	$scope.riskScoringVariable.attributeValueData = attributeValue.data.companyAttributes;	
    	
      	 $scope.addAttributeTypeValue.sort();
    	$scope.riskScoringVariable.CompanyPersonData = $scope.domains[setIndexValue].entityRiskModel;
    }



    //-------------------------End of Company page functionality -------------------------------------

    //---------------------------common functionality for person and company----------------------
    
    // ---------------------------------------Attribute tab starts--------------------------
    /*
     * @purpose: selecting new attribute
     * @created: 20th april 2018
     * @author: varsha
     */
    $scope.searchValue='';
    function addAttribute(index,searchName){
     	var serachIndex = attributeValue.data[$scope.activeAttribute ].map(function(x){ return x.attributeName; }).indexOf(searchName);
	    if(typeof(attributeValue.data[$scope.activeAttribute ][serachIndex].attributeScores) != 'object') {
//	       var attributeScores = {};
//	  		  angular.forEach(attributeValue.data[$scope.activeAttribute ][serachIndex].attributeValues,function(k,v){
//	           	if(v!='moveArray'){
//	           		attributeScores[k] = 0;
//	           	}
//	           });
	        attributeValue.data[$scope.activeAttribute ][serachIndex].attributeScores = attributeValue.data[$scope.activeAttribute ][serachIndex].attributeValues;
          delete attributeValue.data[$scope.activeAttribute ][serachIndex].attributeValues;
	    }
	     $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.push(attributeValue.data[$scope.activeAttribute][serachIndex]);
	     $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores,'attributeName');
	    $scope.riskScoringVariable.CompanyPersonData.attributeRiskScores =  $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores;
	     var serachIndexToDelete = attributeValue.data[$scope.activeAttribute ].map(function(x){ return x.attributeName; }).indexOf(searchName);
	     attributeValue.data[$scope.activeAttribute ].splice(serachIndexToDelete,1);
	     $scope.riskScoringVariable.attributeValueData = attributeValue.data[$scope.activeAttribute];
	     $('#attrList').val('');
	     $('#attrRepo').val('');
    }
    
    /*
     * @purpose: deleting  attribute
     * @created: 20th april 2018
     * @author: varsha
     */
    var deleteAttrIndex;
    var deleteAttr = false;
    var pushBckToRepoName;
    function deleteAttribute(index,name){
        console.log(name);
    	 var serachIndexToRemove = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.map(function(x){ return x.attributeName; }).indexOf(name);
	    	deleteAttrIndex = serachIndexToRemove;
	    	deleteAttr = true;
	    	pushBckToRepoName = name;
    	    $scope.riskScoringVariable.modalMessage = 'Are you sure you want to exclude the selected attribute from scoring?';
    	    $scope.riskScoringVariable.showModalBox2 =true;
    }
    
 
    
    /*
     * @purpose: search new  attribute
     * @created: 6th june 2018
     * @author: varsha
     */
    function searchNewAttribute(event){
    	if(event.target.value.trim()!= ''){
	    	if(event.target.value.length  > 0){
	           		var results = [];
	               	for(var i=0; i< attributeValue.data[$scope.activeAttribute].length; i++) {
	            	  for(var key in attributeValue.data[$scope.activeAttribute ][i]) {
	            		  if(key == 'attributeName'){
		            	    if(attributeValue.data[$scope.activeAttribute][i][key].split('_').join(' ').toLowerCase().indexOf(event.target.value.toLowerCase())!=-1) {
		            	      results.push(attributeValue.data[$scope.activeAttribute ][i]);
		            	    }
	            		  }
	            	  }
	            	}
	               	$scope.riskScoringVariable.attributeValueData = results;
	    	}
    	}else{
	    			$scope.riskScoringVariable.attributeValueData = attributeValue.data[$scope.activeAttribute];
	    }
    	
    }
    
    
    function searchAttribute(event) {
        var results = [];
        if (event.target.value.trim() != '') {
            if (event.target.value.length > 0) {
                for (var i = 0; i < $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.length; i++) {
                    
                            if ($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[i].attributeName.indexOf('_') !== -1) {
                                if ($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[i].attributeName.split('_').join(' ').toLowerCase().indexOf(event.target.value.toLowerCase()) != -1) {
                                    results.push($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[i]);
                                }

                            } else{
                                if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[i].attributeName.toLowerCase().indexOf(event.target.value.toLowerCase())!=-1){
                                 results.push($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[i]);
                                }
                           
                    }
                }
                $scope.riskScoringVariable.CompanyPersonData.attributeRiskScores = results;
            }
        } else {
            $scope.riskScoringVariable.CompanyPersonData.attributeRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores;
        }
    }
    
    $scope.selectedAttributeType = '-1';
    var indexTo = '-1' ;
    var openTemplateName ;
    $scope.dropDownDataAttribute = [];
    var openForFirstTime = true;
    function openTempletewithAttributeValue(index,searchName){
     	openTemplateName = searchName;
     	openForFirstTime = true;
    	$scope.selectedAttributeType = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.map(function(x){ 
    		return x.attributeName; }).indexOf(searchName);
    	$scope.selectedAttributeName = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeName;
    	if(indexTo != '-1'){
    		$scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[indexTo].isOpen = false;
    	}
    	  indexTo  = index;
    	$scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[index].isOpen = true;
		if($scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[index].attributeType){
		   	$scope.dropDownDataAttribute = setDropDownData($scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[index].attributeType.toLowerCase());
		}else{
		   	$scope.dropDownDataAttribute = setDropDownData('string');
		}
    	setTimeout(function(){
    		addScoll('#attrValueList',150);
    		openForFirstTime = false;
    	},500);
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.map(function(x){ 
    		x.validToSave = false;
    		/*$scope.dropDownDataAttribute.value.map(function(d,k){
    			if(d == x.operator){
    				x.dropdownIndex = k;
    			}
    		})*/
    	 });
    	
    
    }
    function closeTemplate(){
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.map(function(x,k){ 
    		if(x.validToSave){
    			$scope.updateAttrValue(false,k);
    		}
    	 });
    	$scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[indexTo].isOpen= false;
    	delete $scope.riskScoringVariable.CompanyPersonData.attributeRiskScores[indexTo].isOpen;
    }
    
    function removeAttributeValues(setIndexValue,openPerson_Company,selectedAttributeType,k){
    	if($scope.saveDomain[setIndexValue][openPerson_Company].attributeRiskScores[selectedAttributeType].attributeScores[k].operator =='Other'){
    		$scope.noMoreOther = true;
    	}
    	$scope.saveDomain[setIndexValue][openPerson_Company].attributeRiskScores[selectedAttributeType].attributeScores.splice(k,1);
    	setRangeForAttributeRiskScoresUsingSlider();
    //	storeListOfDeletedAttr();
    }
    
    function getTime(){
    	var today = new Date();
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; 
    	var yyyy = today.getFullYear();
    	if(dd<10) 
    	{
    	    dd='0'+dd;
    	} 

    	if(mm<10) 
    	{
    	    mm='0'+mm;
    	} 
    	today = dd+'/'+mm+'/'+yyyy;
    	return today;
    }
    
    function setRangeForAttributeRiskScoresUsingSlider(){
    	if(!openForFirstTime){
	    	var today = getTime();
	    	$scope.domains[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].modifiedOn = today;
	    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].modifiedOn = today;
    	}
    }
 
    function storeListOfDeletedAttr(){
      	if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType == 'range'){
    		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.push({
    			'score' : 0,
    			'value' :{'from':'','to':''},
    			'operator':	$scope.dropDownDataAttribute.value[0],
    			'validToSave':false,
    		});
    	}else if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType == 'string'){
    		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.push({
    			'score' : 0,
    			'value' :'',
    			'operator':	$scope.dropDownDataAttribute.value[0],
    			'validToSave':false,
    		});
    	}else{
    		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.push({
    			'score' : 0,
    			'value' :'',
    			'operator':	$scope.dropDownDataAttribute.value[0],
    			'validToSave':false,
    		});
    	}
    	
       	$scope.riskScoringVariable.addAtributeIsOpen = false;
    	console.log($scope.attrNameList);
    }
    
    $scope.noMoreOther = true;
    $scope.addOtherAttribute = function(){
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.push({
			'score' : 0,
			'value' :'',
			'operator':	'Other'
		});
    	$scope.noMoreOther = false;
      	$scope.riskScoringVariable.addAtributeIsOpen = false;
    };
    
    $scope.closeADdAttributeModal = function(){
    	$scope.riskScoringVariable.closeAddingNewAttr = false;
    };
    
    function addAttributeValue(attrName){
    	var dropDownDtaArry = [];
    	if(attrName != undefined && attrName.trim() != ''){
    		if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType){
	    	   	dropDownDtaArry = setDropDownData($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType.toLowerCase());
	    	}
    		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.push({
    			'score' : 0,
    			'value' :attrName,
    			'operator':dropDownDtaArry.value[0]
    		});
	       	$scope.riskScoringVariable.addAtributeIsOpen = false;
	    	$('#typeheadAttr').val('');
    	}
    }
    
    $scope.attributeNameNotSelected = false;
	$scope.attributeTypeNotSelected = false;
	$scope.attributeInsightNotSelected = false;
    function addNewAttribute(name){
    	if(!name){
    		$scope.attributeNameNotSelected = true;
    	}
    	if(name){
    		$scope.attributeNameNotSelected = false;
    	} 
    	if(!currentAttrType){
    		$scope.attributeTypeNotSelected = true;
    	}
    	if(currentAttrType){
    		$scope.attributeTypeNotSelected = false;
    	} 
    	if(!selectedInsightValue){
    		$scope.attributeInsightNotSelected = true;
    	} 
    	if(selectedInsightValue){
    		$scope.attributeInsightNotSelected = false;
    	}
    	if(name && name.length > 0 && currentAttrType && currentAttrType.length>0 && selectedInsightValue && selectedInsightValue.length>0){
    		$scope.riskScoringVariable.closeAddingNewAttr = false;
	    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.push({
		    	'attributeName':name,
		    	'attributeScores':[],
		    	'modifiedOn':'',
		    	'attributeType':currentAttrType,
		    	'attributeInsight':selectedInsightValue
	    	});
	    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores,'attributeName');
	    	$scope.riskScoringVariable.CompanyPersonData.attributeRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores;
	    	console.log($scope.saveDomain[setIndexValue][$scope.openPerson_Company]);
	    	name='';
	    	currentAttrType = '';
	    	selectedInsightValue = '';
	    	
    	}
    	
    }
    
    var selectedValueForSelectAttrVal ;
    $scope.updateAttrValue = function(status,k){
    	if(status ){
    		if(selectedValueForSelectAttrVal){
    			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  selectedValueForSelectAttrVal;
    		}else{
    			if($scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k]){
    				$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator;
    			}
    		}
    		
    		if(!$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value == ''  && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.length<0){
    			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value;
    		}else{
    			$scope.saveDomainOriginal = $scope.saveDomain ;
    		}
    		$scope.riskScoringFunction.setRangeForAttributeRiskScoresUsingSlider(k);
    		if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.from == '' || $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.to == ''){
    			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value;
    			HostPathService.FlashErrorMessage("Attribute values can't be epmty", '');
    		}else if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.from > $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.to)
    	    	{
    			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value;
    			HostPathService.FlashErrorMessage('Attribute value must be from lower to upper value', '');
    	    }
    	}else{
    		if($scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType] && $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k]){
    			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator;
        		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value;
        		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].score =  $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].score;
    		}else{
    			var dropDownDtaArry = [];
    			if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType){
    	    	   	dropDownDtaArry = setDropDownData($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeType.toLowerCase());
    	    		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  dropDownDtaArry.value[0];
            		if(dropDownDtaArry.value[0] == 'range'){
            			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  {'from':'','to':''};
            		}else{
            			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value = '';
            		}
            
    			}
    		}
    			
    	}
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores.map(function(x){ 
    		x.validToSave = false;
    	 });
    };
   
    
    $scope.validateInputValue = function(name,k){
    		if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator == 'range'){
    			if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.to >=0 && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.from >=0 && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.to !=null && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.from !=null){
    				$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSaveRange =  false;
    				$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSave =  false;
    				if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.from > $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value.to){
            			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSaveRange =  true;
            			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSave =  true;
            		}
    			}else{
    				$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSaveRange =  false;
        			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSave =  true;
                    
        		}
    		}else{
    			if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value != null && $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value!=""){
    				$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSave =  false;
        		}else{
        			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSaveRange =  false;
        			$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].validToSave =  true;
        		}
    		}
    		
    	
    };
    
    var currentAttrType;
    function attrTypeChangeValue(value){
    	currentAttrType = value;
    }
    
    $scope.selectnewTypeBasedOnAttrType = function(type,k){
    	selectedValueForSelectAttrVal = type;
//    if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator.toLowerCase() != 'range'){
//    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  selectedValueForSelectAttrVal;
//    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value =  '';
//    }	
    if(selectedValueForSelectAttrVal == 'range'){
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].operator =  selectedValueForSelectAttrVal;
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores[$scope.selectedAttributeType].attributeScores[k].value = {
    			'to':'',
    			'from':''
    	};
    }
    };
    
    var selectedInsightValue;
    function insighSelectedValue(val){
    	selectedInsightValue = val;
    }
    
    $scope.insightValueChange = function(){
    	//selectedInsightValue = val;
    };
  //-----------------------------------end of attribute tab-----------------------------------------  
  //--------------------------------------Relationship tab starts--------------------------------
    
    /*
     * @purpose: selecting new relationship
     * @created: 8th june 2018
     * @author: varsha
     */
    
    function addRelationship(index,searchName){
    	var serachIndexToAdd = riskRelationValuesChange[$scope.ativeRelationShip].map(function(x){ return x.label; }).indexOf(searchName);
	    		 $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.push(riskRelationValuesChange[$scope.ativeRelationShip][serachIndexToAdd]);
	    	     $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores,'label');
	      		 $scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores =  $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores;
	    		  riskRelationValuesChange[$scope.ativeRelationShip].splice(serachIndexToAdd,1);
	    		  $scope.relationShipValues[$scope.ativeRelationShip] = riskRelationValuesChange[$scope.ativeRelationShip];
	    		 $('#relatioRepo').val('');
	    		 $('#relationList').val('');
    }
    
   

    /*
     * @purpose: deleting  relationship
     * @created: 20th april 2018
     * @author: varsha
     */
    
    var deleteRelIndex;
    var deleteRelation = false;
    var pushBckToRepoNameRel;
    function deleteRelationship(index,name){
    	    pushBckToRepoNameRel = name;
    	    var serachIndexToRemove = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.map(function(x){ return x.label; }).indexOf(pushBckToRepoNameRel);
    	    deleteRelIndex = serachIndexToRemove;
    	    deleteRelation = true;
    	    $scope.riskScoringVariable.modalMessage = 'Are you sure you want to exclude the selected relationship  from scoring?';
        	$scope.riskScoringVariable.showModalBox2 =true;
    }
    
    /*
     * @purpose: search new relationship
     * @created: 11th april 2018
     * @author: varsha
     */
    
    function searchNewRelationship(event){
    	var results = [];
    	if(event.target.value.trim()!= ''){
	    	if(event.target.value.length  > 0){
	    		for(var i=0; i< riskRelationValuesChange[$scope.ativeRelationShip].length; i++) {
	          	  for(var key in riskRelationValuesChange[$scope.ativeRelationShip][i]) {
	          		if(key != 'riskScore'){
		            	    if(riskRelationValuesChange[$scope.ativeRelationShip][i][key].split('_').join(' ').toLowerCase().indexOf(event.target.value.toLowerCase())!=-1) {
		            	      results.push(riskRelationValuesChange[$scope.ativeRelationShip][i]);
		            	    }
	          		}
	          	  }
	          	}
	             	$scope.relationShipValues[$scope.ativeRelationShip] = results;
	    	}
    	}else{
    			$scope.relationShipValues[$scope.ativeRelationShip] = riskRelationValuesChange[$scope.ativeRelationShip];
    	}
    } 
    
    /*
     * @purpose:  search  relationship
      * @created: 11th april 2018
     * @author: varsha
     */
    
    function searchRelationshipValue(event){
    	var results = [];
    	if(event.target.value.trim()!= ''){
	    	if(event.target.value.length  > 0){
	    		for(var i=0; i< $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.length; i++) {
	          	  for(var key in $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[i]) {
	          		if(key == 'label'){
		            	    if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[i][key].split('_').join(' ').toLowerCase().indexOf(event.target.value.toLowerCase())!=-1) {
		            	      results.push($scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[i]);
		            	    }
	          		}
	          	  }
	          	}
	    	 	$scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores= results;
	    	}
    	}else{
    		$scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores;
    	}
    } 

   
    /*
     * @purpose: On change of slider for relationship values
      * @created: 11th april 2018
     * @author: varsha
     */
    
    function setRangeForRelationRiskScoresUsingSlider(name,scoreValue){
    	var index = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.map(function(x){ return x.label; }).indexOf(name);
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[index].riskScore = scoreValue;
    	var today = new Date();
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; 
    	var yyyy = today.getFullYear();
    	if(dd<10) 
    	{
    	    dd='0'+dd;
    	} 

    	if(mm<10) 
    	{
    	    mm='0'+mm;
    	} 
    	today = dd+'/'+mm+'/'+yyyy;
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[index].modifiedOn = today;
    	$scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores[index].modifiedOn = today;
    }

   function addNewRelationship(name){
	   $scope.riskScoringVariable.closeAddingNewRel = false;
	   if(name && name.length > 0){
		   $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.push({
			   'label':name,
			   'modifiedOn':'',
			   'riskScore':0
		   });
		   $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores,'label');
		   $scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores;
	   }else{
		   return false;
	   }
   }
    
    //----------------------------end of common functionality for person and company ---------------------------------------------------------
    /*
     * @purpose: show modal when user tries to delete attribute or relationship values
      * @created: 11th april 2018
     * @author: varsha
     */
    
    
    function setDataToRepoForAttribute(){
    	var index1 = attributeValueOriginal.data[$scope.activeAttribute].map(function(x){ return x.attributeName; }).indexOf(pushBckToRepoName);
		if(index1 != -1) {
    		if(typeof(attributeValueOriginal.data[$scope.activeAttribute ][index1].attributeScores) != 'object') {
//	    		var attributeScores = {};
//			  		  angular.forEach(attributeValueOriginal.data[$scope.activeAttribute ][index1].attributeValues,function(k,v){
//			           	if(v!='moveArray'){
//			           		attributeScores[k] = 0;
//			           	}
//			           });
			        attributeValueOriginal.data[$scope.activeAttribute ][index1].attributeScores = attributeValueOriginal.data[$scope.activeAttribute ][index1].attributeValues;
		          delete attributeValueOriginal.data[$scope.activeAttribute ][index1].attributeValues;
	    		var obj =jQuery.extend(true, {}, attributeValueOriginal.data[$scope.activeAttribute ][index1]);
	    		attributeValue.data[$scope.activeAttribute].push(obj);
    		} else{
    			var obj =jQuery.extend(true, {}, attributeValueOriginal.data[$scope.activeAttribute ][index1]);
    			attributeValue.data[$scope.activeAttribute].push(obj);
    		}
		}else{
			 var index2 = $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores.map(function(x){ return x.attributeName; }).indexOf(pushBckToRepoName);
			if(index2!=-1){
			var obj2 =jQuery.extend(true, {}, $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].attributeRiskScores[index2]);
			 attributeValue.data[$scope.activeAttribute].push(obj2);
			}
		}
		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores.splice(deleteAttrIndex, 1);
		$scope.riskScoringVariable.CompanyPersonData.attributeRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].attributeRiskScores;
		$('#attrList').val('');
    	$('#attrRepo').val('');
		deleteAttr = false;
    }
    
    
    function setDataToRepoRelationship(){
    	var index1 = riskRelationValuesChangeOriginal[$scope.ativeRelationShip].map(function(x){ return x.label; }).indexOf(pushBckToRepoNameRel);
    	if(index1 != -1) {
    		var obj1 =jQuery.extend(true, {},riskRelationValuesChangeOriginal[$scope.ativeRelationShip][index1]);
    		riskRelationValuesChange[$scope.ativeRelationShip].push(obj1);
    	}else{
    		 var index2 = $scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.map(function(x){ return x.label; }).indexOf(pushBckToRepoNameRel);
    		if(index2 != -1){
    			 var obj2 =jQuery.extend(true, {},$scope.saveDomainOriginal[setIndexValue][$scope.openPerson_Company].relationshipRiskScores[index2]);
         		 riskRelationValuesChange[$scope.ativeRelationShip].push(obj2);
    		}
    	}
    	riskRelationValuesChange[$scope.ativeRelationShip] = removeDuplicates(riskRelationValuesChange[$scope.ativeRelationShip],'label');
    	$scope.relationShipValues[$scope.ativeRelationShip] = riskRelationValuesChange[$scope.ativeRelationShip];
    	$('#relatioRepo').val('');
    	$('#relationList').val('');
		$scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores.splice(deleteRelIndex, 1);
		$scope.riskScoringVariable.CompanyPersonData.relationshipRiskScores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].relationshipRiskScores;
		deleteRelation = false;
    }
    function sayYesorNo(status){
    	if(status =='yes'){
    		if(deleteAttr){
	    		$scope.riskScoringVariable.showModalBox2 = false;
	    		setDataToRepoForAttribute();
    		}else if(deleteRelation){
    			$scope.riskScoringVariable.showModalBox2 = false;
    			setDataToRepoRelationship();
    		}else if(deleteOsint){
    			deleteOsintAttr(osintIndex,osintName);
    		}
    	}else{
    		$scope.riskScoringVariable.showModalBox2 = false;
    	}
    }
    
    //------------------------------Osint tab --------------------------------------------------------
   
    /*
     * @purpose: Filter osint repo if already added to list
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function setOsinData()
    {
        angular.forEach($scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores,function(val){
            angular.forEach($scope.riskScoringVariable.osintData,function(v,k){
                if(val.relationshipName == v.relationshipName)
                {
                    $scope.riskScoringVariable.osintData.splice(k,1);
                    osintRepoOrigindata.splice(k,1);
                }
            });
        });
    }

    /*
     * @purpose: search osint list
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function searchOsint(event){
     	var results = [];
     	if(event.trim()!= ''){
	    	if(event.length  > 0){
		               	for(var i=0; i< $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores.length; i++) {
		            	  for(var key in $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores[i]) {
		            	    if(key == 'relationshipName'){
		            		  if($scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores[i][key].split('_').join(' ').toLowerCase().indexOf(event.toLowerCase())!=-1) {
		            	      results.push($scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores[i]);
		            	    }}
		            	  }
		            	}
		               	$scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores = results;
		    	}
     	}else{
		    			$scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores;
		}
	     	
    }
    
    /*
     * @purpose: search osint repo
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function searchOsintRepoSearch(event)
    {
        var results = [];
     	if(event.trim()!= ''){
	    	if(event.length  > 0){
		               	for(var i=0; i< osintRepoOrigindata.length; i++) {
		            	  for(var key in osintRepoOrigindata[i]) {
		            	    if(key == 'relationshipName'){
		            		  if(osintRepoOrigindata[i][key].split('_').join(' ').toLowerCase().indexOf(event.toLowerCase())!=-1) {
		            	      results.push(osintRepoOrigindata[i]);
		            	    }}
		            	  }
		            	}
                        $scope.riskScoringVariable.osintData = results;
		    	}
     	}else{
                $scope.riskScoringVariable.osintData = osintRepoOrigindata;
		}
    }

    /*
     * @purpose: add osint
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function addOsint(index,searchName){
        var serachIndexToAdd = $scope.riskScoringVariable.osintData.map(function(x){ return x.relationshipName; }).indexOf(searchName);
            $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores.push($scope.riskScoringVariable.osintData[serachIndexToAdd]);
            $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores,'relationshipName');
            $scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores =  $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores;
            $scope.riskScoringVariable.osintData.splice(serachIndexToAdd,1);
            $scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores;
            
            var serachIndexToAddOrginal = osintRepoOrigindata.map(function(x){ return x.relationshipName; }).indexOf(searchName);
            osintRepoOrigindata.splice(serachIndexToAddOrginal,1);
            $scope.riskScoringVariable.osintData = osintRepoOrigindata;
            $('#osintRepo').val('');
            $('#osintList').val('');
    }

    /*
     * @purpose: deleting  osint
     * @created: 20th april 2018
     * @author: varsha
     */
    var deleteOsintIndex;
    var deleteOsintVar = false;
    var pushBckToRepoOsintName;
    var osintOriginaData = [];
    var deletedValue=[];
    function deleteOsintAttr(index,name){
    	 var serachIndexToRemove = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores.map(function(x){ return x.relationshipName; }).indexOf(name);
         deleteOsintIndex = serachIndexToRemove;
         deleteOsintVar = true;
         pushBckToRepoOsintName = name;
         osintOriginaData = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores;
         angular.forEach($scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores,function(val){
             if(pushBckToRepoOsintName == val.relationshipName)
             {
                deletedValue = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores.splice(deleteOsintIndex,1);
                osintRepoOrigindata.push(deletedValue[0]);
                console.log(deletedValue[0]);
                $scope.riskScoringVariable.osintData = osintRepoOrigindata;
                $scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores;
             }
         });
         $('#osintList').val('');
         $('#osintRepo').val('');
         deleteOsint = false;
         $scope.riskScoringVariable.showModalBox2 = false;
    }
    
    /*
     * @purpose: alert user before deleting osint
     * @created: 20th april 2018
     * @author: varsha
     */
    
    var osintName,osintIndex;
    var deleteOsint = false;
    function deleteOsintUI(index,name){
    	osintName = name;
    	osintIndex= index;
    	deleteOsint = true;
	    $scope.riskScoringVariable.modalMessage = 'Are you sure you want to exclude the selected osint  from scoring?';
		$scope.riskScoringVariable.showModalBox2 =true;
    }
    
    /*
     * @purpose: On change of slider for Osint values
      * @created: 11th april 2018
     * @author: varsha
     */
    
    function setRangeForOsintRiskScoresUsingSlider(name,scoreValue)
    {
        var index = $scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores.map(function(x){ return x.relationshipName; }).indexOf(name);
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores[index].riskScore = scoreValue;
    	var today = new Date();
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; 
    	var yyyy = today.getFullYear();
    	if(dd<10) 
    	{
    	    dd='0'+dd;
    	} 

    	if(mm<10) 
    	{
    	    mm='0'+mm;
    	} 
    	today = dd+'/'+mm+'/'+yyyy;
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].osintRiskScores.scores[index].modifiedOn  = today;
    	$scope.riskScoringVariable.CompanyPersonData.osintRiskScores.scores[index].modifiedOn = today;
    }
	//-----------------------------End of osint tab----------------------------------------------------
    //--------------------------------Connection Tab-----------------------------------------------------
    
    function addConnection(item){
    	$scope.saveDomain[setIndexValue][$scope.openPerson_Company].connectionsScores.push({
    		connectionName:item,
    		connectionScore:0
    	});
    	$scope.showConnection = false;
    	 $scope.saveDomain[setIndexValue][$scope.openPerson_Company].connectionsScores = removeDuplicates($scope.saveDomain[setIndexValue][$scope.openPerson_Company].connectionsScores,'connectionName');
	    	
    }
    
    function deleteConnection(index){
    	 $scope.saveDomain[setIndexValue][$scope.openPerson_Company].connectionsScores.splice(index,1);
    }
    
    function showDropdown(){
    	$scope.showConnection = true;
    	 addScoll('#addscrollToconnection',100);
    }
    //---------------------------------end of connection tab----------------------------------------
    //-------------------------Common functionality------------------------------------------------------


    /*
     * @purpose:Go back to main page
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function riskHomePage() {
    	addScoll('#homePageMenu',340);
    	$scope.modelTabsWrapper = false;
        $scope.domainWrapper = true;
        $scope.entityWrapper = false;
        $('.modal-dialog').addClass('w-90vp');
    	$scope.riskScoringVariable.showRunButtonNew = true;
        $scope.riskScoringVariable.showSaveNew = false;
        getDefaultModel();
    }
    addScoll('#homePageMenu',340);
    /*
     * @purpose:Go back to entity  page
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function showEntityPage() {
    	getEntityPage();
        $scope.entityWrapper = true;
        $scope.domainWrapper = false;
        $scope.closeADdAttributeModal();
        $scope.modelTabsWrapper = false;
        $('.modal-dialog').removeClass('w-90vp');
        $scope.riskScoringVariable.showRunButtonNew = false;
        $scope.riskScoringVariable.showSaveNew = true;
        if(indexTo != -1){
        	closeTemplate();
        	indexTo = '-1';
        }
    }


    /*
     * @purpose:Go back to person /company  page
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function getAttributePage() {
        $scope.entityWrapper = false;
        $scope.domainWrapper = false;
        $scope.modelTabsWrapper = true;
        $('.modal-dialog').addClass('w-90vp');
        $scope.riskScoringVariable.showRunButtonNew = false;
        $scope.riskScoringVariable.showSaveNew = true;
    }

    
    /*  
     * @purpose: Run the default domain
     * @created: 15th June 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */
    
    function runRiskScoring(caseID){
   	 if(caseID != '' && caseID !=undefined){
	    	 var data ={
	    			 'caseId':caseID,
	    			 'riskModelId':$scope.getDefaultModelValue.data.id,
	    			 'isDebug':true
	    	 };
		    	 $scope.riskScoringVariable.riskScoringLoader = true;
		    	 postRunRiskScore(data);
   	 }else{
	    		 return false;
	      }
    }
   

    /*  
     * @purpose: Update the changes
     * @created: 29th may 2018
     * @params: params(object)
     * @return: success, error functions
     * @author: varsha  */
    
    var saveDomain = false;
    function saveUpdatedChange() {
    	if(isDomainActive){
    		saveDomain = true;
        	$scope.riskScoringVariable.modalMessage = 'This change will recalculate the score for all entities in the system, are you sure you wish to proceed ?';
            $scope.riskScoringVariable.showModalBox =true;
    	}else{
    		confirmSave();
    	}
    	
    }

    function confirmSave(){
    	$scope.riskScoringVariable.riskScoringTabLoader = true;
	$scope.saveDomain[setIndexValue].individualRiskModel.attributeRiskScores.map(function(k,v){
		delete $scope.saveDomain[setIndexValue].individualRiskModel.attributeRiskScores[v].isOpen;
	 });
	  $scope.saveDomain[setIndexValue].entityRiskModel.attributeRiskScores.map(function(k,v){
		delete $scope.saveDomain[setIndexValue].entityRiskModel.attributeRiskScores[v].isOpen;
	 });
        $scope.riskScoringVariable.modalAttribute.individualRiskModel = $scope.saveDomain[setIndexValue].individualRiskModel;
        $scope.riskScoringVariable.modalAttribute.entityRiskModel = $scope.saveDomain[setIndexValue].entityRiskModel;
    	updateRiskScoringData = jQuery.extend(true, {}, $scope.riskScoringVariable.modalAttribute);
        updateRiskScoringData.model_id = updateRiskScoringData.id;
        var name = updateRiskScoringData.decisionModelName;
        updateRiskScoringData.decisionModelName = name.toUpperCase();
//        updateRiskScoringData.id;
        delete updateRiskScoringData.$$hashKey;
        putRisk(updateRiskScoringData);
        closeRiskModal();
//        if(isDomainActive){
//        	var updateDefaultDomain = jQuery.extend(true, {},updateRiskScoringData);
//        	updateDefaultDomain.model_id = "Default";
//        	updateDefaultDomain.id = "Default";
//        	console.log(updateDefaultDomain,'active');
//        	 putRisk(updateDefaultDomain);
//        }
    }
    
    function closeRiskModal() {
        $uibModalStack.dismissAll();
    }
    
    /*
     * @purpose: common actions on click of yes or no 
     * @created: 20th april 2018
     * @author: varsha
     */
    
    function deleteYesOrNo(status){
    	if(deleteModalStatus){ //action while deleting
	    	if(status == 'yes'){
	    		$scope.riskScoringVariable.showModalBox = false;
	    		$scope.riskScoringVariable.riskScoringLoader = true;
		        deleteRisk(deleteDomainModal.id);
	    	}else if(status == 'no'){
	    		$scope.riskScoringVariable.showModalBox = false;
	    	}
	    	deleteModalStatus = false;
    	}else if(selectDomainStatus){//action while selecting
    		if(status == 'yes'){
	    		$scope.riskScoringVariable.showModalBox = false;
	    		$scope.riskScoringVariable.riskScoringLoader = true;
	    		selectedDomainModal.id ='default';
	    		putRisk(selectedDomainModal,true);
	    	//	postDefaultModel(selectedDomainModal);
	    	}else if(status == 'no'){
	    		$scope.riskScoringVariable.showModalBox = false;
	    	}
    		selectDomainStatus = false;
    	}else if(saveDomain){//action while saving
    		if(status == 'yes'){
    			confirmSave();
    			$scope.riskScoringVariable.showModalBox = false;
	    	}else if(status == 'no'){
	    		$scope.riskScoringVariable.showModalBox = false;
	    	}
    		saveDomain = false;
    	}
    	
    }
    
    function addClassToRun(){
    	  $('.popover').css('top','-77px; !important');
    }
    $scope.resetPOpOver = function(){
    	$('.top-pop.decision-score-pop.welcome-page.run .popover').css("cssText", "top: -270px !important;");
    	$scope.attributeNameNotSelected = false;
    	$scope.attributeTypeNotSelected = false;
    	$scope.attributeInsightNotSelected = false;
    };
    function addScoll(id,height){
   	 $(document).ready(function() {
            $(id).mCustomScrollbar({
                axis: "y",
                theme: "minimal"
            }, function() {
            });
            $(id).css('height', height+'px');
        });
   }



}