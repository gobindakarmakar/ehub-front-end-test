'use strict';
elementApp
	   .controller('ConfirmationForDocparser', confirmationForDocparser);

confirmationForDocparser.$inject = [
			'$scope',
			'$rootScope',
			'data',
			'$uibModalInstance',
			'HostPathService',
			'TopPanelApiService',
			'EHUB_FE_API',
			'$timeout',
			'$state',
			'$window'
		];
	    function confirmationForDocparser(
	    		$scope,
	    		$rootScope,
	    		data,
	    		$uibModalInstance,
				HostPathService,
	    		TopPanelApiService,
	    		EHUB_FE_API,
				$timeout,
				$state,
				$window){
			
	    	/*
	         * @purpose:Set data in modal
	         * @created: 7th sep 2018
	         * @author: prasanthi
	        */
	    	$timeout(function(){
	    		 if(data.position == 0 ||data.position){
	    			 $("#mainData").html(" Are you sure you wish to delete this template?");
				 }
				 else if(data.id && data.docId && data.tId)
				 {
					$("#mainData").html("Template already exists for this uploaded file,Do you want edit?");
				 }
				 else{
	    			 $("#mainData").html("Document Not Matched with Any Existing Template, Would You Like To create New Template?");	 
				 }
	    	},0);
	    	if(data){
					//uploadDocumentandRedirectWithId(data)	
	    	}
	    	else{
	    		
	    	}
	    	$scope.closeDataPopUp=function(){
	    		$uibModalInstance.close();
	    	};
          
	    	 $scope.ok = function () {
	    		 if(data.position == 0 ||data.position){
					 data.dataTemplate.splice(data.position,1);
					 //delete template from landing page
					 TopPanelApiService.docparserdeleteLandingPageTemplate(data.templateId).then(
					function() {
						HostPathService.FlashSuccessMessage("Template Deleted Succesfully",'');
						$timeout(function(){
							//get remaining templates after deleting
							$scope.getTemplates();
						},100);
					},
					function() {
					});
				 }
				 else if(data.id && data.docId && data.tId)
				 {
					//while uploading the template it will redirecte to docparserpage if the file already exists
					$state.go('docParserPage', {id:data.id,docId: data.docId,tId:data.tId},  {notify: false});
					setTimeout(function(){
						$window.location.reload();
					},1000);
				 }
				 else 
				 {
					//upload document from docparser,underwriting,unboarding and create case 
	 	    		uploadDocumentandRedirectWithId(data);
				 }
	    	    $uibModalInstance.close();
	    	 };
	    	 
	    	 /*
	 		 * @purpose: upload Document
	 		 * @created: 5th Sep 2018
	 		 * @params: file
	 		 * @author: Prasanthi
	 		 */
	 		function uploadDocumentandRedirectWithId(files){
	 			$scope.docsLoader = true;
	 			var data = {
	 		    		uploadFile: files	
	 		    	};
	 			var params ={
	 					docFlag:11,
	 					token: $rootScope.ehubObject.token,
	 					fileTitle:files.name,
	 					remarks:''
	 			};
	 	    	TopPanelApiService.uploadDocument(params, data).then(function(response) {
	 	    		$scope.docsLoader = false;	    		
	 				if(response.data && response.data.docId){
	 					if(data.uploadFile.caseId){
							//redirected to docaprser if it is having caseId
							window.location.href= EHUB_FE_API +'docparser/#!/docParserPage?onDoc=true&docId='+response.data.docId+"&caseId="+data.uploadFile.caseId;
							
						}else{
							//redirected to docparser if it is not having caseId
							window.location.href= EHUB_FE_API +'docparser/#!/docParserPage?onDoc=true&docId='+response.data.docId;
						}
	 				}else{
	 					  HostPathService.FlashErrorMessage("Failed to Upload file",'');
	 				}
	 	    	}, function(error) {
	 	    		$scope.docsLoader = false;
	 	    		  HostPathService.FlashErrorMessage('ERROR', error.responseMessage);
	 	    	});	
	 		}
		
	    }
	    	
	    	