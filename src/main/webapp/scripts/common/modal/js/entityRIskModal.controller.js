"use strict"
angular.module('ehubApp')
.controller('EntityRiskScoreModalController', entityRiskScoreModalController);

entityRiskScoreModalController.$inject = [
    '$scope',
    '$uibModalInstance',
    'overviewRisks',
    'caseID',
    'caseName',
    'TopPanelApiService',
    '$rootScope',
    'riskScoreData'
];

function entityRiskScoreModalController(
    $scope,
    $uibModalInstance,
    overviewRisks,
    caseID,
    caseName,
    TopPanelApiService,
    $rootScope,
    riskScoreData
){
	if(!riskScoreData){
		riskScoreData = '';
	}
	$scope.noData = false;
	plotChart();
	$scope.aggregationType = riskScoreData['operator-used'];
	function plotChart(){
		if(riskScoreData){
			if(!riskScoreData && !riskScoreData.attributeRiskScores && !riskScoreData.attributeRiskScores.length>0){
				$scope.noData = true;
				return;
				}
		}else{
			$scope.noData = true;
			return;
		}
		if(Math.abs(riskScoreData['overall-score'])>1){
			riskScoreData['overall-score'] = Math.abs(riskScoreData['overall-score'])/100;
		}
		 $scope.cumulativeRisk =riskScoreData['overall-score']*100;
		if(!caseName){
			$scope.noData =  true;
			$scope.isloading = false;
			return;
		}
		
		var finalData ={"nodes":[],"links":[]};
		var nodesId=0;
		finalData.nodes.push({
			"name":caseName?caseName.split("_").join(" "):"",
			"id":nodesId
		});
		nodesId++;
		finalData.nodes.push({
			"name":"",
			"id":nodesId
		});
		nodesId++;
		if(riskScoreData && riskScoreData.attributeRiskScores && riskScoreData.attributeRiskScores.length>0){
			angular.forEach(riskScoreData.attributeRiskScores,function(v,k){
				if(v.riskFactor.score){
					finalData.nodes.push({
						"name":v.attributeName?v.attributeName.split("_").join(" "):"",
						"id":nodesId
					})
					if(Math.abs(v.riskFactor.score)>1){
						v.riskFactor.score = Math.abs(v.riskFactor.score)/100;
					}
					finalData.links.push({
						"source":0,
						"target":nodesId,
						"value":v.riskFactor.score,
						"label": "",
						"txt":"<div style='word-wrap:break-word;color':'#FFFFFF'>"+v.attributeName+" : "+(v.riskFactor.score*100).toFixed(2)+"%</div>"
				
					});
					finalData.links.push({
						"source":nodesId,
						"target":1,
						"value":v.riskFactor.score,
						"label": "",
						"txt":"<div style='word-wrap:break-word;color':'#FFFFFF'>"+v.attributeName+" : "+(v.riskFactor.score*100).toFixed(2)+"%</div>"
				
					});
					nodesId++;
				}
			});
			console.log(finalData,'finalDatafinalData')
			if(finalData.links.length == 0){
				$scope.noData = true;
			}
	}

		if(!$scope.noData){
		 setTimeout(function(){
			 var options = {     
					    entityName:caseName,
				        container: "#chartContentDiv",
				        height: 300,
				        width:700,
				        rectWidth:5,
				        rectColor:"#4294D9",
				        isReport:true
				        // path_labels :['FFB','CPO','CPO','PK','CPKO']
				    } 
			  var nodesList ={};
			  finalData['nodes'].map(function(d,i){
		            d.nodeId = d.id;
		            d.id = i;
		            nodesList[d.nodeId]=i;
	
		        })
		        finalData['links'].map(function(d){
		            d.value  = d['value'];
		            d.source= nodesList[d.source];
		            d.target = nodesList[d.target];
		        })
		        // options.data = data;
		        console.log(finalData,'finalDatafinalData')
		        options.data =  (finalData)
		        options.edge_labels = $.map(finalData.links,function(i,d){return i.label});
		       
		        
		       new sankey_chart_risk_score(options);
		      setTimeout(function(){
		    	  new loadRiskpieChart("",options);
		      },100)
		       
		 },1000)
		}
	}
window.loadRiskpieChart = function(setTitle,options) {
    var data = [
        { name: "Risk Score", value: $scope.cumulativeRisk  },
        { name: " ", value: 100-$scope.cumulativeRisk  },
       
    ];
    var text = Math.round($scope.cumulativeRisk);

		var container= "#finalDiv";
		var width = $('.lastRect').attr('height');
		var height = $('.lastRect').attr('height')
var ht;
    
    var thickness = 20;
    var duration = 750;
    var labeltxtY=115;
    if(options.edge_labels.length > 4){
    	labeltxtY=95;
        width = width * 1.5;
        height = height * 1.5;
        ht = $('.lastRect').attr('height') ? $('.lastRect').attr('height')/2 : 15;
    }
    var radius = Math.min(width, height) / 2;
    var color = d3.scaleOrdinal(d3.schemeCategory10);
    var colorObj = {" ":"#566a71","Risk Score":"rgb(66, 148, 217)"}

    var svg = d3.select('#finalDiv').attr('height',120).attr('width',120)
        .append('svg')
       .attr("class","sankPie")
        .attr('width', width)
        .attr('height', parseFloat(height)+50);

    var temp_width = $('.lastRect').attr('height') ? $('.lastRect').attr('height')/2 : 20;
//    $('#finalDiv').attr('transform', 'translate(' + (width / 2) + ',' + (-parseInt(temp_width)) + ')');
    $('#finalDiv').attr('transform', 'translate(' + (20) + ',' + 0 + ')');
var g = svg.append('g')
    .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

  

    var arc = d3.arc()
        .innerRadius(radius - thickness)
        .outerRadius(radius);

    var pie = d3.pie()
        .value(function (d) { return d.value; })
        .sort(null);

    var path = g.selectAll('path')
        .data(pie(data))
        .enter()
        .append("g")
        
        .append('path')
        .attr('d', arc)
        .attr('fill', function(d, i) { 
        	return colorObj[d.data.name]})
       
        .each(function (d, i) { this._current = i; });


    g.append('text')    
        .attr('text-anchor', 'middle')
        .attr('dy', '.35em')
        .attr('font-size', '14px')
        .attr("fill","#ccc")
        .text(text);
  
    
//    if(setTitle){
    	 svg.append("text")
    	    .attr('x',40)
    	    .attr('y', labeltxtY)
    	    .attr('width', 50)
    	    .attr('height', 80)
    	    .attr("text-anchor", "middle") 
    	    .attr("stroke",'none')
    	    .attr("font-weight",'normal')
    	     .attr("fill",'#9B9FA2')
    	    .style("font-size",'12px') 
    	    .text('RISK SCORE');
//    }
  
 
}
	 
	

	 
	//close the modal 
    $scope.ok = function(n){
            $uibModalInstance.close(n);
    };
    
  //close the modal 
    $scope.cancel = function(){
        $uibModalInstance.dismiss('clicked cancel');
    };
}