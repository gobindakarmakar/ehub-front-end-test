'use strict';
elementApp
	   .controller('ChatBotModalController', chatBotModalController);
	   
		chatBotModalController.$inject = [
			'$scope',
			'$uibModalInstance'
		];
		
		function chatBotModalController(
			$scope,
			$uibModalInstance){
			
			/*
			 * @purpose: Defining scope variables
			 * @author: Ankit
			 */
			$scope.chatBotModalController = {
					resetchatbot: resetchatbot,
					closechatbot: closechatbot
			};
			/*
		     * @purpose: reset chatbot
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function resetchatbot(){
				$uibModalInstance.close('reset');
				
			}
			/*
		     * @purpose: close modal
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function closechatbot(){
				$uibModalInstance.close();
			}
		}