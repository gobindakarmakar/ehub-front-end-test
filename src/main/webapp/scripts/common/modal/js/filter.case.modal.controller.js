'use strict';
angular.module('ehubApp')
	   .controller('filterCaseModalController', filterCaseModalController);

filterCaseModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModalInstance',
			'EnrichApiService',
			'HostPathService',
			'Flash',
			'isOnlyKeyword',
			'statusList',
			'keysTofilter',
			'FilteredKeyword',
			'FilteredKey',
			'RiskRatio',
			'onlySearch',
			'CaseDiarySelections',
			'StatusOption',
			'AllNames',
			'isdateRequired',
			'itemsPerPage'
		];
		
function filterCaseModalController(
			$scope, 
			$rootScope,
			$state,
			$uibModalInstance,
			EnrichApiService,
			HostPathService,
			Flash,
			isOnlyKeyword,
			statusList,
			keysTofilter,
			FilteredKeyword,
			FilteredKey,
			RiskRatio,
			onlySearch,
			CaseDiarySelections,
			StatusOption,
			AllNames,
			isdateRequired,
			itemsPerPage) {
	
			$scope.CompleteList = AllNames;
			$scope.onlySearch  = onlySearch;
			$scope.isOnlyKeyword=isOnlyKeyword;			
			$scope.statusList =statusList;
			$scope.token = $rootScope.ehubObject.token;
			$scope.keysTofilter = keysTofilter;
			$scope.FilteredKey = FilteredKey;
			$scope.seletedtype = CaseDiarySelections ? CaseDiarySelections[0].MCDtype ? CaseDiarySelections[0].MCDtype :"ALL" : "ALL";
			$scope.seletedOrderby = CaseDiarySelections ? CaseDiarySelections[1].MCDorderby ? CaseDiarySelections[1].MCDorderby :"createdOn" : "createdOn";
			$scope.seletedOrderIn = CaseDiarySelections ? CaseDiarySelections[2].MCDorderin ?CaseDiarySelections[2].MCDorderin : "asc": "asc";
			$scope.seletedPriority = FilteredKey ? FilteredKey :"ALL" ;
			var statusUpdate = StatusOption ? StatusOption : "ALL";
			if(statusUpdate == 0)
				{statusUpdate = "ALL";}
			$scope.seletedStatus = statusUpdate+"";
			$scope.seletedEntityType = FilteredKey ? FilteredKey :"ALL" ;
			$scope.searchKeyword = FilteredKeyword ? FilteredKeyword : "";
			$scope.RiskRatio = RiskRatio;
			$scope.CaseDiarySelections= CaseDiarySelections;  
			$scope.SearchHints = [];
			$scope.isdateRequired = isdateRequired;
			var d = new Date();
			$scope.StartDate = d.getTime();
			$scope.EndDate = d.getTime();
			if(isdateRequired){
				if($scope.isdateRequired[0].startDate && $scope.isdateRequired[0].startDate != "skip")
					{$scope.StartDate = $scope.isdateRequired[0].startDate; }
				if($scope.isdateRequired[0].endDate &&  $scope.isdateRequired[0].endDate != "skip")
					{$scope.EndDate = $scope.isdateRequired[1].endDate;}
			}
			$scope.StartDate = moment.tz(new Date($scope.StartDate), "Africa/Abidjan").format("MM/DD/YYYY HH:mm");
			$scope.EndDate = moment.tz(new Date($scope.EndDate), "Africa/Abidjan").format("MM/DD/YYYY HH:mm");
			
			$scope.itemsPerPage = itemsPerPage;

			/*
		 	*@purpose: close social media articles modal
		 	*@created: 23 aug 2017
		 	*@params: none
		 	*@returns: none
		 	*@author: swathi
		 	*/  
		 	$scope.closeModal = function(){
		 		$uibModalInstance.close('');
		 	};
		 	$scope.applyFilters =function(){
			 	 var filterApplied ={};
			 	
			 	filterApplied.token =$scope.token;
			 	 if($scope.seletedtype && $scope.seletedtype !="ALL"){
			 		filterApplied.type = $scope.seletedtype;
			 		
			 	 }
			 	if($scope.seletedPriority && $scope.seletedPriority !="ALL"){
			 		filterApplied.priority = $scope.seletedPriority;
			 	 }
			 	
			 	if($scope.seletedOrderby){
			 		filterApplied.orderBy = $scope.seletedOrderby;
			 	 }if($scope.seletedOrderIn ){
			 		filterApplied.orderIn = $scope.seletedOrderIn;
			 	 }
			 	 if($scope.seletedStatus && $scope.seletedStatus !="ALL"){
			 		filterApplied.status=[];
			 		filterApplied.status.push( parseInt($scope.seletedStatus));
			 	 }
			 	 if(isOnlyKeyword && $scope.seletedEntityType != "ALL"){
			 		filterApplied.entity = $scope.seletedEntityType;
			 	 }
			 	if($("#rest_example_3_start").attr("class").indexOf("ng-untouched") > -1){
			 		filterApplied.startDate  = "";
			 	} else{
				 	var sd = new Date($("#rest_example_3_start").val());
				 	filterApplied.startDate = sd.getTime() - (sd.getTimezoneOffset() * 60000);	
			 	}
			 	if($("#rest_example_3_start").attr("class").indexOf("ng-untouched") > -1){
			 		filterApplied.endDate = "";
			 	}else{
				 	var ed = new Date($("#rest_example_3_end").val());
				 	filterApplied.endDate = ed.getTime() - (ed.getTimezoneOffset() * 60000);	
			 	}
			 	if($scope.recordsPerPage){
			 		filterApplied.recordsPerPage = $scope.recordsPerPage;
			 	}
			 	var filterObj ={
			 			caseSearchKeyword : $scope.searchKeyword,
			 			filterApplied:filterApplied
			 	};				
			 	$uibModalInstance.close(filterObj);
		 	};
		 	
		 	$scope.findKeyHints = function(event,val){
		 		$scope.SearchHints = [];
		 		
		 		if(val != "" && $scope.CompleteList.length > 0)
		 		{angular.forEach($scope.CompleteList,function(d){
		 			if(d.toLowerCase().indexOf(val.toLowerCase()) > -1){
		 				$scope.SearchHints.push(d);
		 			}
		 		});}
		 		
		 	};
		 	$scope.setsVal = function(value){
		 		$scope.searchKeyword = $scope.SearchHints[value];
		 		$("#SearchList").hide();
		 	};
			$("body").on("click", "#rest_example_3_start", function () {
				  var startDateTextBox = $('#rest_example_3_start');
				  var endDateTextBox = $('#rest_example_3_end');
				  $(this).datetimepicker().datetimepicker("show");
				  $("#ui-datepicker-div").css('z-index',9999);
				  startDateTextBox.datetimepicker({
				      timeFormat: 'HH:mm',
				      dateFormat: 'mm/dd/yy',
				      onClose: function (dateText) {
				          if (endDateTextBox.val() != '') {
				              var testStartDate = startDateTextBox.datetimepicker('getDate');
				              var testEndDate = endDateTextBox.datetimepicker('getDate');
				              if (testStartDate > testEndDate)
				                  {endDateTextBox.datetimepicker('setDate', testStartDate);}
				          } else {
				              endDateTextBox.val(dateText);
				          }
				      },
				      onSelect: function () {
				          endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
				      }
				  });
				  endDateTextBox.datetimepicker({
				      timeFormat: 'HH:mm',
				      dateFormat: 'mm/dd/yy',

				      onClose: function (dateText) {
				          if (startDateTextBox.val() != '') {
				              var testStartDate = startDateTextBox.datetimepicker('getDate');
				              var testEndDate = endDateTextBox.datetimepicker('getDate');
				              if (testStartDate > testEndDate)
				                  {startDateTextBox.datetimepicker('setDate', testEndDate);}
				          } else {
				              startDateTextBox.val(dateText);
				          }
				      },
				      onSelect: function () {
				          startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
				      }
				  });
				});									
				$scope.selected = undefined;
			}

				
				
				
				
				
				
		