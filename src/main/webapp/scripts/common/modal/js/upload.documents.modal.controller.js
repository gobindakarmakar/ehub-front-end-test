'use strict';
elementApp
	   .controller('UploadDocumentsController', uploadDocumentsController);

	uploadDocumentsController.$inject = [
		'Flash',
		'$scope',
		'$rootScope',
		'$stateParams',
		'HostPathService',
		'$uibModalInstance',
		'UploadFileService',
		'onBoadingDetails',
		'UPLOADING_FILES_NUM',
		'ActService',
		'ActApiService',
		'alertsDataConst',
		'$state'
	];

	function uploadDocumentsController(
		Flash,
		$scope,
		$rootScope,
		$stateParams,
		HostPathService,
		$uibModalInstance,
		UploadFileService,
		onBoadingDetails,
		UPLOADING_FILES_NUM,
		ActService,
		ActApiService,
		alertsDataConst,
		$state){
	
	$scope.placeHolders =["ARTICLE OF INCORPORATION","BENEFICIAL OWNERS ","PASSPORT"];
	/*
	 * @purpose: Defining scope variables
	 * @author: Ankit
	 */
	$scope.uploadDocumentsModal = {
			file:'',
			progressbarMax:0,
			progressbarValue:0,
			uploadFileStatus:false,
			uploadFiles:uploadFiles,
			changeFileDetails:changeFileDetails,
			deleteFile:deleteFile,
			closeUploadModal:closeUploadModal,
			fileUploadParams:{
				caseId: $stateParams.caseId,
				remarks: '',
				fileTitle: '',
				token: $rootScope.ehubObject.token
			}
	};
	var uploadedFiles = [];
	var docFlagsArray = [];
	angular.forEach(onBoadingDetails.uploadedFiles, function(v){
		if(v.docFlag != undefined){
			uploadedFiles.splice((v.docFlag - 1), 0, v);
			docFlagsArray.push(v.docFlag - 1);
		}
	});
	angular.forEach(onBoadingDetails.uploadedFiles, function(v, k){
		if(docFlagsArray.indexOf(k) == -1 && v.docFlag == undefined){
			uploadedFiles.splice(k, 0, v);
		}
	});
	$scope.uploadDocumentsModal.getUploadedFiles = uploadedFiles;
	$scope.uploadDocumentsModal.progressbarValue = parseFloat(onBoadingDetails.progressbarValue * 100).toFixed(2);
	$scope.uploadDocumentsModal.getUploadedFilesLimit = onBoadingDetails.getUploadedFilesLimit;
	
	function getCaseSeedDetails(caseId){
		var params = {
			caseId: caseId,
			token: $rootScope.ehubObject.token
		};
		ActApiService.getCaseDetails(params).then(function(response){
			$scope.caseSeedDetails = response.data;
		});
	}
	getCaseSeedDetails($stateParams.caseId);
	/*
     * @purpose: Upload Case File
     * @created: 22 sep 2017
     * @params: none
     * @return: no
     * @author: Ankit
    */
	function uploadFiles(file,uploadfile, index){
		if(onBoadingDetails.uploadedFilesLength >= 6)
			{index+=6;}	
		$scope.uploadDocumentsModal.file = file;
        if (file) {
        	$scope.uploadDocumentsModal.uploadFileStatus = true;     
        	$scope.uploadDocumentsModal.fileUploadParams.remarks= uploadfile.remarks || '';
        	$scope.uploadDocumentsModal.fileUploadParams.fileTitle = file.name;
//        	if(index < 3){
        		$scope.uploadDocumentsModal.fileUploadParams.docFlag = index + 1;
//        	}else{
//        		$scope.uploadDocumentsModal.fileUploadParams['docFlag'] = 0;
//        	}
        	UploadFileService.uploadDocument($scope.uploadDocumentsModal.fileUploadParams, file).then(function(response){
        		
        		$scope.uploadDocumentsModal.getUploadedFiles[index].docId = response.data.docId;
        		$scope.uploadDocumentsModal.getUploadedFiles[index].fileTitle = file.name;
        		$scope.uploadDocumentsModal.getUploadedFiles[index].remarks = uploadfile.remarks;
        		$scope.uploadDocumentsModal.uploadFileStatus = false;
        		onBoadingDetails.isUploadDocuments = true;
        		$scope.uploadDocumentsModal.progressbarValue = parseFloat((++onBoadingDetails.progressbar / UPLOADING_FILES_NUM) * 100).toFixed(2);
        		onBoadingDetails.progressbarValue = $scope.uploadDocumentsModal.progressbarValue / 100;
        		HostPathService.FlashSuccessMessage('DOCUMENT UPLOADED SUCCESSFULLY','Successfully uploaded');
        		++onBoadingDetails.uploadedFilesLength;
        		
        		$state.go('actCase', {caseId: $stateParams.caseId}, {reload: true});
        	},function(error){
        		$scope.uploadDocumentsModal.uploadFileStatus = false;
        		HostPathService.FlashErrorMessage('ERROR ON UPLOADING FILE',error.responseMessage);
        	});
        }   
	}
	/*
     * @purpose: Change File Metadata
     * @created: 22 sep 2017
     * @params: uploadfile(object)
     * @return: no
     * @author: Ankit
    */
	function changeFileDetails(file, uploadfile, index){
		var params = {
			token: $rootScope.ehubObject.token,
			docId: uploadfile.docId
		};
		if(onBoadingDetails.uploadedFilesLength >= 6)
			{index+=6;}
		$scope.uploadDocumentsModal.fileUploadParams.docId = uploadfile.docId;
		$scope.uploadDocumentsModal.fileUploadParams.remarks = uploadfile.remarks || '';
		$scope.uploadDocumentsModal.fileUploadParams.fileTitle= file.name;
		$scope.uploadDocumentsModal.uploadFileStatus = true; 
		$scope.uploadDocumentsModal.getUploadedFiles[index].docId = uploadfile.docId;
		
		UploadFileService.updateDocument(params, file).then(function(){
			UploadFileService.alterFileDetail($scope.uploadDocumentsModal.fileUploadParams).then(function(){
				$scope.uploadDocumentsModal.uploadFileStatus = false; 
				onBoadingDetails.isUploadDocuments = true;
	    		$scope.uploadDocumentsModal.getUploadedFiles[index].fileTitle = file.name;
	    		$scope.uploadDocumentsModal.getUploadedFiles[index].remarks = uploadfile.remarks;
	    		
				HostPathService.FlashSuccessMessage('DOCUMENT UPDATED SUCCESSFULLY','Successfully updated');
				$state.go('actCase', {caseId: $stateParams.caseId}, {reload: true});
	    	},function(error){
	    		$scope.uploadDocumentsModal.uploadFileStatus = false;
	    		HostPathService.FlashErrorMessage('ERROR ON UPDATING FILE',error.responseMessage);
	    	});
    	},function(error){
    		$scope.uploadDocumentsModal.uploadFileStatus = false;
    		HostPathService.FlashErrorMessage('ERROR ON UPDATING FILE',error.responseMessage);
    	});
	}
	
	/*
     * @purpose: Delete File
     * @created: 22 sep 2017
     * @params: uploadfile(object)
     * @return: no
     * @author: Ankit
    */
	function deleteFile(uploadfile,index){
		if(onBoadingDetails.uploadedFilesLength >= 6)
			{index+=6;}
		$scope.uploadDocumentsModal.fileUploadParams.docId = uploadfile.docId;
		UploadFileService.deleteFile($scope.uploadDocumentsModal.fileUploadParams).then(function(response){
			$scope.uploadDocumentsModal.getUploadedFiles[index].fileTitle = '';
			$scope.uploadDocumentsModal.getUploadedFiles[index].remarks = '';
    		$scope.uploadDocumentsModal.uploadFileStatus = false; 
    		$scope.uploadDocumentsModal.progressbarValue = parseFloat((--onBoadingDetails.progressbar / UPLOADING_FILES_NUM) * 100).toFixed(2);
    		onBoadingDetails.progressbarValue = $scope.uploadDocumentsModal.progressbarValue / 100;
    		HostPathService.FlashSuccessMessage(response.data.responseMessage,'Successfully deleted');
    		--onBoadingDetails.uploadedFilesLength;
    		
    		$state.go('actCase', {caseId: $stateParams.caseId}, {reload: true});
    		if(onBoadingDetails.progressbar < 1)
    			{onBoadingDetails.isUploadDocuments = false;}
    	},function(error){
    		$scope.uploadDocumentsModal.uploadFileStatus = false;
    		HostPathService.FlashErrorMessage('ERROR ON DELETING FILE',error.responseMessage);
    	});
	}

	/*
     * @purpose: Close Upload Modal
     * @created: 22 sep 2017
     * @params: none;
     * @return: no
     * @author: Ankit
    */
	function closeUploadModal(){
		$uibModalInstance.close(onBoadingDetails);
	}
}