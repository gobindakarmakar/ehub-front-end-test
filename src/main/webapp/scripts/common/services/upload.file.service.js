'use strict';
elementApp
		.factory('UploadFileService', uploadFileService);

	uploadFileService.$inject = [
			'$http',
			'Upload',
			'EHUB_API',
			'$q',
			'$rootScope'
		];
	
	function uploadFileService( 
			$http,
			Upload,
			EHUB_API,
			$q,
			$rootScope){
		
		return {
			getGeneralSettings: getGeneralSettings,
			deleteFile:deleteFile,
			alterFileDetail:alterFileDetail,
			updateDocument: updateDocument,
			uploadDocument: uploadDocument,
			docAggregator: docAggregator,
			getCaseDocuments:getCaseDocuments,
			getSubmenuData:getSubmenuData,
			startchatDialog:startchatDialog,
			enterDataDialog:enterDataDialog,
			requestProposal:requestProposal
		};
		/*
	     * @purpose: get general settings
	     * @created: 9 Aug 2018
	     * @params: token(object)
	     * @return: success, error functions
	     * @author: prasanthi
	    */
		function getGeneralSettings(){
			var apiUrl = EHUB_API + 'systemSettings/getSystemSettings';
	        var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: {
	            	token: $rootScope.ehubObject.token
	            }
	        });
	        return(request
	                .then(getGeneralSettingSuccess)
	                .catch(getGeneralSettingError));
	
	        /*getGeneralSettings error function*/
	        function getGeneralSettingError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	       /*getGeneralSettings success function*/
	        function getGeneralSettingSuccess(response) {
	        	if(response.data.length > 0)
	        	{response.data.sort();}
	        	return(response);
	        }
		}
		/*
	     * @purpose: upload document
	     * @created: 22 jan 2018
	     * @params: params(object), file(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function uploadDocument(params,file){
			var apiUrl =  EHUB_API + 'case/uploadDocument';
			var fd = new FormData();/*jshint ignore:line*/
	        fd.append("uploadDoc",file);
	        
			var request = $http({
	            method: "POST",
	            url: apiUrl,
                headers: {
                    'Content-Type': undefined
                },
                params: params,
	            data: fd
	        });
	        return(request
	                .then(uploadDocumentSuccess)
	                .catch(uploadDocumentError));
	
	        /*getElasticSearchSuggest error function*/
	        function uploadDocumentError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function uploadDocumentSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: doc Aggregator
	     * @created: 02 feb 2018
	     * @params: params(object
	     * @return: success, error functions
	     * @author: swathi
	    */
		function docAggregator(params){
			var apiUrl =  EHUB_API + 'case/docAggregator';
			var request = $http({
	            method: "GET",
	            url: apiUrl,
                params: params
	        });
	        return(request
	                .then(docAggregatorSuccess)
	                .catch(docAggregatorError));
	
	        /*docAggregator error function*/
	        function docAggregatorError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*docAggregator success function*/
	        function docAggregatorSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: get case document
	     * @created: 26 sep 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function getCaseDocuments(params, docFlag){
			var apiUrl =  EHUB_API + 'case/getCaseDocuments?docFlag=' + docFlag;
			var request = $http({
	            method: "GET",
	            url: apiUrl,
                params: params
	        });
	        return(request
	                .then(getCaseDocumentsSuccess)
	                .catch(getCaseDocumentsError));
	
	        /*getElasticSearchSuggest error function*/
	        function getCaseDocumentsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function getCaseDocumentsSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: get submenu data
	     * @created: 2 Feb 2018
	     * @params: 
	     * @return: success, error functions
	     * @author: Zameer
	    */
		function getSubmenuData(url){
			var apiUrl = url;
			var request = $http({
	            method: "GET",
	            url: apiUrl,
	            params: {
	            	token: $rootScope.ehubObject.token
	            }
	        });
	        return(request
	                .then(getSubmenuDataSuccess)
	                .catch(getSubmenuDataError));
	
	        /*getSubmenuData error function*/
	        function getSubmenuDataError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getSubmenuData success function*/
	        function getSubmenuDataSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: update document
	     * @created: 29 jan 2018
	     * @params: params(object), file(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		function updateDocument(params, file){
			var apiUrl =  EHUB_API + 'case/updateDocumentContent';
			var fd = new FormData();/*jshint ignore:line*/
	        fd.append("uploadDoc", file);
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            headers: {
                    'Content-Type': undefined
                },
                params: params,
	            data: fd
	        });
	        return(request
	                .then(updateDocumentSuccess)
	                .catch(updateDocumentError));
	
	        /*updateDocument error function*/
	        function updateDocumentError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*updateDocument success function*/
	        function updateDocumentSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: Change File Detail
	     * @created: 26 sep 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function alterFileDetail(params){
			var apiUrl =  EHUB_API + 'case/updateDocument';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
                params: params
	        });
	        return(request
	                .then(alterFileDetailSuccess)
	                .catch(alterFileDetailError));
	
	        /*getElasticSearchSuggest error function*/
	        function alterFileDetailError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function alterFileDetailSuccess(response) {
	            return(response);
	        }
		}
		/*
	     * @purpose: Delete File
	     * @created: 28 sep 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function deleteFile(params){
			var apiUrl =  EHUB_API + 'case/deleteDocument';
			var request = $http({
	            method: "DELETE",
	            url: apiUrl,
                params: params
	        });
	        return(request
	                .then(deleteFileSuccess)
	                .catch(deleteFileError));
	
	        /*getElasticSearchSuggest error function*/
	        function deleteFileError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function deleteFileSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: Start Chat Dialog
	     * @created: 28 dec 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function startchatDialog(data, params){
			var apiUrl =  EHUB_API + 'insuranceAssist/dialog';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data,
	            params:params
	        });
	        return(request
	                .then(startchatDialogSuccess)
	                .catch(startchatDialogError));
	
	        /*getElasticSearchSuggest error function*/
	        function startchatDialogError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function startchatDialogSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: Enter Data Dialog
	     * @created: 28 dec 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function enterDataDialog(data, params, dialogId){
			var apiUrl =  EHUB_API + 'insuranceAssist/dialog/' + dialogId +'/message';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data,
	            params:params
	        });
	        return(request
	                .then(enterDataDialogSuccess)
	                .catch(enterDataDialogError));
	
	        /*getElasticSearchSuggest error function*/
	        function enterDataDialogError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function enterDataDialogSuccess(response) {
	            return(response);
	        }
		}
		
		/*
	     * @purpose: Request Proposal
	     * @created: 28 dec 2017
	     * @params: 
	     * @return: success, error functions
	     * @author: Ankit
	    */
		function requestProposal(data, params, dialogId){
			var apiUrl =  EHUB_API + 'insuranceAssist/dialog/' + dialogId +'/proposal';
			var request = $http({
	            method: "POST",
	            url: apiUrl,
	            data: data,
	            params:params
	        });
	        return(request
	                .then(requestProposalSuccess)
	                .catch(requestProposalError));
	
	        /*getElasticSearchSuggest error function*/
	        function requestProposalError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function requestProposalSuccess(response) {
	            return(response);
	        }
		}
		
		
		
	}
		