'use strict';
elementApp
	   .service('RiskScoreService', riskScoreService);

		riskScoreService.$inject = [
			'$http',
			'EHUB_API',
			'$q',
			'$rootScope',
			
		];
		
		function riskScoreService(
				$http,
				EHUB_API,
				$q,
				$rootScope
			) {
						
			return {
				getrisk :getrisk,
				postRisk:postRisk,
				putRisk:putRisk,
				deleteRisk:deleteRisk,
				getRiskByModalId:getRiskByModalId,
				getriskattributeValues:getriskattributeValues,
				postriskattributeValues:postriskattributeValues,
				getriskRelationshipValues:getriskRelationshipValues,
				postriskRelationshipValues:postriskRelationshipValues,
				postDefaultModel:postDefaultModel,
				getDefaultModel:getDefaultModel,
				runRiskScoring:runRiskScoring,
				riskNameExists:riskNameExists,
				getSanctionsList:getSanctionsList,
				getInsightValue:getInsightValue
			};
			
			
			/*
		     * @purpose: Get all risk Modal
		     * @created: 19th april 2018
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getrisk(){
				var apiUrl = EHUB_API+'riskScore/risk'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl
		        });
		        return(request
		                .then(getriskSuccess)
		                .catch(getriskError));
		
		        /*getrisk error function*/
		        function getriskError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getrisk success function*/
		        function getriskSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Post all risk Modal
		     * @created: 19th april 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function postRisk(data){
				var apiUrl = EHUB_API+'riskScore/risk'+"?token="+$rootScope.ehubObject.token;
         		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:data
			        });
		        return(request
		                .then(postRiskSuccess)
		                .catch(postRiskError));

		        /*postRisk error function*/
		        function postRiskError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*postRisk success function*/
		        function postRiskSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: put  risk Modal
		     * @created: 19th april 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function putRisk(data){
				var apiUrl = EHUB_API+'riskScore/risk'+"?token="+$rootScope.ehubObject.token;
         		 var request = $http({
						method: 'PUT', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:data
			        });
		        return(request
		                .then(putRiskSuccess)
		                .catch(putRiskError));

		        /*putRisk error function*/
		        function putRiskError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*putRisk success function*/
		        function putRiskSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: delete risk
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function deleteRisk(modelId){
				var apiUrl = EHUB_API+'riskScore/risk/'+modelId+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "DELETE",
		            url: apiUrl,
		        });
		        return(request
		                .then(deleteRiskSuccess)
		                .catch(deleteRiskError));

		        /*deleteRisk error function*/
		        function deleteRiskError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*deleteRisk success function*/
		        function deleteRiskSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: get risk by modelId
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getRiskByModalId(modelId){
				var apiUrl = EHUB_API+'riskScore/risk/'+modelId+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getRiskByModalIdSuccess)
		                .catch(getRiskByModalIdError));

		        /*getRiskByModalId error function*/
		        function getRiskByModalIdError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getRiskByModalId success function*/
		        function getRiskByModalIdSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: get risk attributeValues
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getriskattributeValues(){
				var apiUrl = EHUB_API+'riskScore/risk/attributeValues'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getriskattributeValuesSuccess)
		                .catch(getriskattributeValuesError));

		        /*getriskattributeValues error function*/
		        function getriskattributeValuesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getriskattributeValues success function*/
		        function getriskattributeValuesSuccess(response) {
		            return(response);
		        }
			}
			
				
			/*
		     * @purpose: post risk attributeValues
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function postriskattributeValues(params){
				var apiUrl = EHUB_API+'riskScore/risk/attributeValues'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params:params
		        });
		        return(request
		                .then(postriskattributeValuesSuccess)
		                .catch(postriskattributeValuesError));

		        /*postriskattributeValues error function*/
		        function postriskattributeValuesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*postriskattributeValues success function*/
		        function postriskattributeValuesSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: get risk relationshipValues
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getriskRelationshipValues(){
				var apiUrl = EHUB_API+'riskScore/risk/relationshipValues'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getriskRelationshipValuesSuccess)
		                .catch(getriskRelationshipValuesError));

		        /*getriskattributeValues error function*/
		        function getriskRelationshipValuesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getriskattributeValues success function*/
		        function getriskRelationshipValuesSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: get risk relationshipValues
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function postriskRelationshipValues(){
				var apiUrl = EHUB_API+'riskScore/risk/relationshipValues'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(postriskRelationshipValuesSuccess)
		                .catch(postriskRelationshipValuesError));

		        /*getriskattributeValues error function*/
		        function postriskRelationshipValuesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getriskattributeValues success function*/
		        function postriskRelationshipValuesSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: get default model
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getDefaultModel(){
				var apiUrl = EHUB_API+'riskScore/risk/defaultModel'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getDefaultModelSuccess)
		                .catch(getDefaultModelError));

		        /*getDefaultModel error function*/
		        function getDefaultModelError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getDefaultModel success function*/
		        function getDefaultModelSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: post default model
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			
			function postDefaultModel(data){
				var apiUrl = EHUB_API+'riskScore/risk/defaultModel'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            headers: {
		            	"Content-Type": "application/json" 
		            		},
		            url: apiUrl,
		            data:data
		        });
		        return(request
		                .then(postDefaultModelSuccess)
		                .catch(postDefaultModelError));

		        /*postDefaultModel error function*/
		        function postDefaultModelError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*postDefaultModel success function*/
		        function postDefaultModelSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: Run risk Scoring
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function runRiskScoring(data){
				var apiUrl = EHUB_API+'workflow/es/graph/risk/score'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data:data
		        });
		        return(request
		                .then(runRiskScoringSuccess)
		                .catch(runRiskScoringError));

		        /*runRiskScoring error function*/
		        function runRiskScoringError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*runRiskScoring success function*/
		        function runRiskScoringSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Run domain name exists
		     * @created: 1st  june 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function riskNameExists(data){
				var apiUrl = EHUB_API+'riskScore/risk/nameExists'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data:data
		        });
		        return(request
		                .then(riskNameExistsSuccess)
		                .catch(riskNameExistsError));

		        /*riskNameExists error function*/
		        function riskNameExistsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*riskNameExists success function*/
		        function riskNameExistsSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: get getSanctions List
		     * @created: 19th april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getSanctionsList(){
				var apiUrl = EHUB_API+'riskScore/risk/sanctions-list'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getSanctionsListSuccess)
		                .catch(getSanctionsListError));

		        /*getDefaultModel error function*/
		        function getSanctionsListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getDefaultModel success function*/
		        function getSanctionsListSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: get Insight Value
		     * @created: 7th feb 2019
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getInsightValue(type){
			//	var apiUrl = 'https://4947kpl4ib.execute-api.eu-west-1.amazonaws.com/dev/risk/attributeinsights/'+type
				var apiUrl = EHUB_API+'riskScore/risk/attributeinsights'+"?token="+$rootScope.ehubObject.token+'&entityType='+type;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getInsightValueSuccess)
		                .catch(getInsightValueError));

		        /*getInsightValue error function*/
		        function getInsightValueError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getInsightValue success function*/
		        function getInsightValueSuccess(response) {
		            return(response);
		        }
			}
			
			
			
}