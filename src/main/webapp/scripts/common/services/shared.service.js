'use strict';
elementApp
	   .service('HostPathService', hostPathService);

		hostPathService.$inject = [
			'$location',
			'Flash'
		];
		
		function hostPathService(
				$location,
				Flash) {
			var hostPath = '',dashboardname = '', topPanel = '';/*jshint ignore:line*/
			
			this.setHostPath = function(data) {/*jshint ignore:line*/
				data = data.substring(data.indexOf($location.port()));
				data = data.replace($location.port(), '');
				if (data.indexOf($location.path()) > 0) {
					data = data.replace($location.path(), '');
					data = data.replace("#", '');
					data = data.split("?")[0];
				}
				hostPath = data;
			};
			this.getHostPath = function() {/*jshint ignore:line*/
				return hostPath;
			};
			
			this.setdashboardname = function(data, dashboarDropDownMenuItems){/*jshint ignore:line*/
				if(data.search('#') != -1)
					{data =  data.split('#')[1];}	
				if(data.search('/') != -1)
					{data = data.split('/')[1];}
				if(data.search(' ') == -1){
					if(data.match(/[A-Z]/)){
						var subStr = data.match(/[A-Z]/).toString();
						var index = data.indexOf(subStr);
						data = data.slice(0,index) + " " + data.slice(index);
					}
				}
				data = data.charAt(0).toUpperCase() + data.slice(1);
				angular.forEach(dashboarDropDownMenuItems,function(val){
					if(data == val.menu){
						dashboardname = val.menu;
						return;
					}
					  angular.forEach(val.content,function(val1){
						  if(data == val1.name){
							  dashboardname = val.menu;
						  }
					  });
				   });
			};
						
			this.getdashboardname = function(){/*jshint ignore:line*/
				 return dashboardname.charAt(0).toLowerCase() + dashboardname.slice(1);
			};
			
			/*
		     * @purpose: Flash Success Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function successflashAlert(data){
				var message = '<div class="alert alert-success  d-flex ai-c alert-dismissible ehub-alert-success">'+
							  '<span class="fa fa-info-circle fa-3x"></span>'+
							  '<p class="mar-x15 mar-b0">' + data.title +'<strong>' + data.message + '</strong></p>'+							  
							  '<div type="button" class="f-24 c-pointer text-dark-cream mar-autol" ng-show="flash.showClose" close-flash="{{flash.id}}">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                              '</div>'+
							  '</div>';
				return message;
			}
			
			/*
		     * @purpose: Flash Error Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			function dangerflashAlert(data){
				var message = '<div class="alert  d-flex ai-c  alert-danger alert-dismissible ehub-alert-error">'+ 
							  '<span class="fa fa-exclamation-triangle fa-3x"></span>'+
							  '<p class="mar-x15 mar-b0">' + data.title +  '<strong>' + data.message + '</strong></p>'+							
							  '<div type="button" class="f-24 c-pointer text-dark-cream mar-autol" ng-show="flash.showClose" close-flash="{{flash.id}}">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                              '</div>'+
							  '</div>';
				return message;
			}
			
			/*
		     * @purpose: Flash Success Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			this.FlashSuccessMessage = function(title, message){/*jshint ignore:line*/
				Flash.create('success', successflashAlert({
        			title: title,
        			message: message
        		}), 5000,{class: 'main-flash-wrapper'}, true);
			};
			
			/*
		     * @purpose: Flash Error Message
		     * @created: 25 sep 2017
		     * @params: none
		     * @return: no
		     * @author: Ankit
		    */
			this.FlashErrorMessage = function(title, message){/*jshint ignore:line*/
				Flash.create('danger', dangerflashAlert({
           			title: title,
        			message: message
        		}),5000,{class: 'main-flash-wrapper'}, true);
			};
			
			this.getParameterByName = function(name, url){/*jshint ignore:line*/
				if (!url) {
    	            url = window.location.href;
    	        }
    	        name = name.replace(/[\[\]]/g, "\\$&");
    	        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    	                results = regex.exec(url);
    	        if (!results)
    	            {return null;}
    	        if (!results[2])
    	            {return '';}
    	        return decodeURIComponent(results[2].replace(/\+/g, " "));
			};
			/*
		     * @purpose: calculate random risk values
		     * @created: 10 nov 2017
		     * @params: searchText(string)
		     * @return: no
		     * @author: swathi
		    */
			var  risk = 0, scaledNumArr = [];
			this.setRandomRisk = function(searchText){/*jshint ignore:line*/
				scaledNumArr = [];
				var unscaledIR = (searchText + 'indirect').toUpperCase(),
				unscaledDR = (searchText + 'direct').toUpperCase(),
				unscaledTR = (searchText + 'transaction').toUpperCase();
				
				var searchTextRiskArray = [unscaledIR, unscaledDR, unscaledTR];
				var scale = d3.scaleBand().domain(searchTextRiskArray).range([40,80]);
				
				for(var j=0, length = searchTextRiskArray.length; j<length; j++){
					risk = (scale(searchTextRiskArray[j])) + searchText.length;
					if(risk > 80){
						risk = risk - searchText.length;
					}
					scaledNumArr.push(risk);
				}
			};
			
			this.getRandomRisk = function(){/*jshint ignore:line*/
				return scaledNumArr;
			};
			/*
		     * @purpose: get timeline data
		     * @created: 18 jan 2018
		     * @params: data(object)
		     * @return: no
		     * @author: swathi
		    */
			var current_data = [];
			this.setTimelineData = function(data){/*jshint ignore:line*/
				current_data = [];
				angular.forEach(data, function(v){
					var startDate = new Date(v.datetime);
						startDate =  startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();
					var currentDate = new Date();
						currentDate = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getDate();
						
					current_data.push({
						"start": startDate,
						"description": v.description,
						"detailedDescription": v.description,
						"end": currentDate,
						"label": v.label,
						"group": v.group
					});
				});
			};
			
			this.getTimelineData = function(){/*jshint ignore:line*/
				return current_data;
			};
			/*
		     * @purpose: setting dashboardItems
		     * @created: 18 jan 2018
		     * @params: data(object)
		     * @return: no
		     * @author: Ankit
		    */
			var dropDownMenuItems = [];
			this.setdashboarDropDownMenuItems = function(items){/*jshint ignore:line*/
				dropDownMenuItems = items;
			};
			
			this.getdashboarDropDownMenuItems = function(){/*jshint ignore:line*/
				return dropDownMenuItems;
			};
			/*
		     * @purpose: setting dashboardItems
		     * @created: 18 jan 2018
		     * @params: data(object)
		     * @return: no
		     * @author: Ankit
		    */
			var dropDownDisableBydomains = [];
			this.setdashboardDisableBydomains = function(items){/*jshint ignore:line*/
				dropDownDisableBydomains = items;
			};
			
			this.getdashboardDisableBydomains = function(){/*jshint ignore:line*/
				return dropDownDisableBydomains;
			};
			
		}
		