'use strict';
elementApp
	.service('CommonService', commonService);

commonService.$inject = [
	'$http',
	'EHUB_API',
	'$q',
	'$rootScope',
	'$stateParams',
	'HostPathService',
	'EHUB_FE_API',
	'KYC_QUESTIONNAIRE_PATH',
	'POLICY_ENFORCEMENT_PATH',
	'ACTIVITI_FE_PATH',
	'TopPanelApiService'
	
];

function commonService(
	$http,
	EHUB_API,
	$q,
	$rootScope,
	$stateParams,
	HostPathService,
	EHUB_FE_API,
	KYC_QUESTIONNAIRE_PATH,
	POLICY_ENFORCEMENT_PATH,
	ACTIVITI_FE_PATH,
	TopPanelApiService
	) {

	return {
		getSubMenuData: getSubMenuData,
		checkSubmenuDisable: checkSubmenuDisable,
		checkMainmenuDisable: checkMainmenuDisable,
		createNotificationMethod: createNotificationMethod,
		createGridTable: createGridTable,
		jsUcfirst:jsUcfirst,
		compareStrings:compareStrings
	};

    /*
     * @purpose: disable submenus
     * @created: 24 oct 2017
     * @params: null
     * @return: no
     * @author: ankit
     */
	function checkSubmenuDisable(value) {
		if (value == "yes"){
			return true;
		}
		if (value == "no"){
			return false;
		}
		if (value == "maybe" && $stateParams.caseId){
			return false;
		}
		else{
			return true;
		}
	}
    /*
     * @purpose: disable main menu
     * @created: 21 feb 2018
     * @params: value
     * @return: boolean value
     * @author: swathi
     */
	function checkMainmenuDisable(value) {
		if (value == "yes"){
			return true;
		}
		if (value == "no"){
			return false;
		}
		if (value == "maybe" && $stateParams.caseId){
			return false;
		}
		else{
			return true;
		}
			
	}
	
	function getValueForRangeSlider(input){
		var mapHash = {
		    'NONE':0,
		    'LOW':1,
		    'MEDIUM':2,
		    'HIGH':3
		  };
		return mapHash[input];
	}
	
	
	$rootScope.mediaFilterOptions =[];
	
    /*
     * @purpose: setting dashboardItems
     * @created: 18 jan 2018
     * @params: data(object)
     * @return: no
     * @author: Ankit
     */

	function getSubMenuData(submenuname, content, dashboardItems, currentState, caseSeedDetails) {

		var disable = checkSubmenuDisable(content);
		if (disable){
			return false;
		}
		HostPathService.setdashboardname(submenuname.name, dashboardItems);
		var submenuname = submenuname.name.replace(/ /g, '');
		submenuname = submenuname.charAt(0).toLowerCase() + submenuname.slice(1);
		if (submenuname == 'bigDataSciencePlatform') {
			submenuname = 'workflow';
			window.location.href = EHUB_FE_API + submenuname + '/';
		}
		if (submenuname == 'questionnaireBuilder') {
			var url = KYC_QUESTIONNAIRE_PATH;
			window.open(url, '_blank');
		}
		if (submenuname == 'policyEnforcement') {
			var url = POLICY_ENFORCEMENT_PATH;
			window.open(url, '_blank');
		}
		if (submenuname == 'dashboard') {
			submenuname = 'discover';
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'investigationConsole') {
			if ($stateParams.caseId != undefined) {
				window.location.href = EHUB_FE_API + '#/' + submenuname + '/' + $stateParams.caseId;
			} else {
				window.location.href = EHUB_FE_API + '#/' + submenuname + '/';
			}
		}
		if (submenuname == 'advancedSearch') {
			submenuname = 'enrich';
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'onboarding') {
			submenuname = 'enrich';
			window.location.href = EHUB_FE_API + '#/' + submenuname + '?onBoard=true';
		}
		if (submenuname == 'underwriting') {
			submenuname = 'underwriting';
			window.location.href = EHUB_FE_API + 'uploadDocuments/#!/' + "landing";
		}
		if (submenuname == 'marketIntelligence') {
			submenuname = 'mip';
			window.location.href = EHUB_FE_API + submenuname + '/';
		}
		if (submenuname == 'cases') {
			submenuname = 'casePageLanding';
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'auditTrail') {
			submenuname = 'auditTrail';
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'entity') {
			submenuname = 'entity';
			var url;
			if (currentState == 'actCase') {
				url = EHUB_FE_API + submenuname + '/#!/company/' + caseSeedDetails.name + "?qId=" + caseSeedDetails.caseId;
			} else {
				url = EHUB_FE_API + submenuname + '/#!/company/oracle';
			}
			if ($rootScope.hyperLinksNewTab) {
				window.open(url, '_blank');
			} else {
				window.open(url, '_self');
			}

		}
		if (submenuname == 'linkAnalysis') {
			var url;
			if (currentState == 'actCase') {
				url = EHUB_FE_API + '#/linkAnalysis?caseId=' + $stateParams.caseId;
			} else if ($stateParams.query) {
				url = EHUB_FE_API + '#/linkAnalysis?q=' + $stateParams.query;
			} else {
				url = EHUB_FE_API + '#/linkAnalysis';
			}
			window.open(url, '_blank');
		}
		if (submenuname == 'fraud') {
			window.location.href = EHUB_FE_API + 'transactionIntelligence';
		}
		if (submenuname == 'userManagement') {
			window.location.href = EHUB_FE_API + 'identityManagement';
		}
		if (submenuname == 'personalization') {
			window.location.href = EHUB_FE_API + 'leadGeneration';
		}
		if (submenuname == 'transactionIntelligence') {
			window.location.href = EHUB_FE_API + 'transactionIntelligence';
		}
		if (submenuname == 'adverseTransactions') {
			submenuname = 'adverseTransactions';
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'workspace' || submenuname == 'systemMonitoring' || submenuname == 'appManager' || submenuname == 'dataManagement' || submenuname == 'systemSettings' || submenuname == 'dataCuration') {
			window.location.href = EHUB_FE_API + '#/' + submenuname;
		}
		if (submenuname == 'transactionMonitoring') {
			if ($stateParams.caseId != undefined) {
				window.location.href = EHUB_FE_API + '#/' + submenuname + '/' + $stateParams.caseId;
			} else {
				window.location.href = EHUB_FE_API + '#/' + submenuname;
			}
		}
		if (submenuname == 'decisionScoring') {
			$rootScope.riskScoreModal();
		}
		if (submenuname == 'orchestration') {
			var activityUrl = ACTIVITI_FE_PATH;
			window.open(activityUrl, '_self');
			//					   window.location.href= EHUB_FE_API + 'identityManagement';
		}
		if (submenuname == 'documentParsing') {
			window.location.href = EHUB_FE_API + 'docparser/#!/' + "landing";
		}
		if (submenuname == 'sourceManagement') {
			window.location.href = EHUB_FE_API + 'sourceManagement/';
		}
	}

    /*
		     * @purpose: Generate notification from anywhere 
		     * @created: 24th may 2018
		     * @params: data(object)
		     * @return: no
		     * @author: Varsha
		     * @dataFormat: data = {
	    			userId:[],
	    			body:''
	    	}
		    */

	function createNotificationMethod(data) {
		TopPanelApiService.postNotification(data).then(function () {
		}, function () {
		});
	}

	/*@Purpose: populate dropdown of filter data
	 * params: data(Array) 
	 * return : json Object
	 * @author: Amarjith Kumar
	 * @Date: 24th-Dec-2018
	 */

	function populateFilterDropdown(data, uiGridConstants,selectedvalue) {
		var mappedData = [];
		mappedData = $.map(data, function (d) {
			return { value: (d === 'All' ? '' : d), label: d };
		});

		if(data[0]=="visible"){
			mappedData[0].value =true;
			mappedData[1].value =false;
		}
		
		
		if(data[1]=="NONE"){
			mappedData.map(function(d){
				if(d.label == 'ALL'){
					d.value = '';
				}else{
					d.value = getValueForRangeSlider(d.value);
				}
				
			});
		}
		return ({
			term: (selectedvalue !== '' && selectedvalue !== undefined) ? selectedvalue:'',
			type: uiGridConstants.filter.SELECT,
			selectOptions: mappedData
		});
	}
	
	
	/*Purpose:Dynamic slider 
	/*@Description: Using grid appscore for getting the dynamic value in ng model. 
	 * @author: Varsha
	 * @Date: 5th Jan 2019
	 */
	function getSlider() {
		return ('<div class="d-flex width-100  ai-c" ng-class="{\'pe-none\' : row.entity.Visible == false}" style="justify-content:center"><div class=""><div style="text-align: center" class= "text-dark-cream f-12">{{grid.appScope.getDecryptValueForRangeSlider(row.entity[col.colDef.field])}}</div> <input type="range"  ng-model="row.entity[col.colDef.field]" ng-disabled = "row.entity[col.field+ \'_isDisabled\' ]" ng-class="{\'c-ban\' : row.entity[col.field+ \'_isDisabled\']}" name="range" min="0" max="3" ng-change="grid.appScope.onRangeSliderChange(row.entity[col.colDef.field],row,col,col.colDef.field,rowRenderIndex)" class="Sliderrange blue"></div><button popover-placement="bottom" popover-class="pop_source-financial bottom-pop-wrapper" class="c-pointer text-dark-cream f-18 left fade in" ng-show="grid.appScope.showHide(col.field);" uib-popover-html="grid.appScope.htmlPopover(row,col,rowRenderIndex)" popover-is-open="window[col.field+row.entity.sourceId + \'_isOpen\' ]" popover-trigger="\'outsideClick\'" ng-disabled = "!row.entity.Visible" style="background: transparent;border: transparent;" ><i ng-class="{ \'text-blue\' : row.entity[col.field + \'_isDisabled\' ]}" class="fa fa-sliders mar-l10 mar-t10"></i></button>');
	}
	function getMediaIcons() {
		return ('<div class="width-50 d-flex ai-c" ng-class="{\'pe-none\' : row.entity.Visible == false}"><i ng-repeat="x in grid.appScope.mapMedia(row.entity.sourceMedia) track by $index" class="fa fa-{{x}} mar-r5 f-10" ></i></div><button  ng-disabled = "!row.entity.Visible" popover-placement="bottom" class="c-pointer" ng-class="{\'c-ban\' : row.entity.Visible == false}"  uib-popover-html="grid.appScope.htmlMediaPopover(row,col,rowRenderIndex)" popover-class="pop_source-financial pop_source_media bottom-pop-wrapper" popover-placement="bottom" popover-trigger="\'outsideClick\'" style="background: transparent;border: transparent;"><i class="fa fa-sliders f-18 text-dark-cream" ng-class="{\'pe-none\' : row.entity.Visible == false}"></i></button>');
	}

	function getFilterMediaTemplate(){/*jshint ignore:line*/
		var template ='<div class="ui-grid-filter-container"><div><select class="ui-grid-filter-select ui-grid-filter-input-0" id="media"><option value="" class=""></option><option label="ALL" id="media_All" value="" selected="selected">ALL</option>';
			var optionTemplate = '';
			var mediaOption = [];

			$rootScope.mediaFilterOptions.map(function(d,k){
					 mediaOption.push({
						 'name':d,
						 'value':k
					 }) ;
				  });			
				  mediaOption.map(function(d){
					  optionTemplate = optionTemplate+'<option label="'+d.name+'" value="'+d.value+'"  id="media_'+d.name+'" >'+d.name+' </option>';
				  });
				template = template+ optionTemplate+'</select></div></div>';
		return template;			
	
	}

	/*@Purpose: Formatting the data and assigning to new variable as per UI-Grid layout
	 *  
	 * 
	 * @author: Amarjith Kumar
	 * @Date: 24th-Dec-2018
	 */
	function uiGridFormatdata(sample_jsonData, uiGridConstants,vm) {

		var col_defs = [];
		var count = 0;
	
		angular.forEach(sample_jsonData, function (value) {
			if (value.columnType === 'Text') {
				var  selected= (vm.filtersValue[value.columnFieldName] !== '')?vm.filtersValue[value.columnFieldName]:'';
				vm['filterSourceValue_'+vm.currentTab] = selected;
				this.push({
					field: value.columnFieldName,
                    cellTemplate: '<div ng-class="{\'pe-none\' : row.entity.Visible == false}"><p class="mar-b0 text-cream f-14 two-line-wrapper">{{COL_FIELD}}</p></div>',
					filterHeaderTemplate:'<div class="ui-grid-filter-container"><input type="text" class="ui-grid-filter-input pad-r25" ng-model="grid.appScope.filterSourceValue_'+vm.currentTab+'" ng-keydown="grid.appScope.filterSourceLink(col,grid.appScope.filterSourceValue_'+vm.currentTab+',$event)" /><button class="bg-transparent border-0 p-abs c-pointer z-999 r-20 f-14" style="top:10px" ng-click="grid.appScope.filterSourceLink(col,grid.appScope.filterSourceValue_'+vm.currentTab+',$event)"><i class="fa fa-search"></i></button></div>',
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					sortType: 'Text',
					displayName:value.columnHeaderName,
					width: value.width,
					headerCellClass: 'text-center',
					columnType : 'Text',
					enableSorting:true,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});
			} else if (value.columnType === 'Hyperlink') {
				var  selected= (vm.filtersValue[value.columnFieldName] !== '')?vm.filtersValue[value.columnFieldName]:'';
				vm['filterLinkValue_'+vm.currentTab] = selected;
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,	
					filterHeaderTemplate:'<div class="ui-grid-filter-container"><input type="text" class="ui-grid-filter-input pad-r25" ng-model="grid.appScope.filterLinkValue_'+vm.currentTab+'" ng-keydown="grid.appScope.filterSourceLink(col,grid.appScope.filterLinkValue_'+vm.currentTab+',$event)"/><button class="bg-transparent border-0 p-abs c-pointer z-999 r-20 f-14" style="top:10px" ng-click="grid.appScope.filterSourceLink(col,grid.appScope.filterLinkValue_'+vm.currentTab+',$event)"><i class="fa fa-search"></i></button></div>',
					width: value.width,
					headerCellClass: 'text-center',
					columnType : 'Text',
					sortType: 'Text',
					cellTemplate: '<div><a target = "_blank" ng-href ="{{row.entity.sourceLink}}">{{row.entity.sourceLinkName}}</a></div>',
					enableSorting:true,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'Multi-Select') {
				var  selected= (vm.filtersValue[value.columnFieldName] !== '')?vm.filtersValue[value.columnFieldName]:'';
				if(value && value.columnFieldName && value.columnFieldName.toLowerCase() === 'jurisdiction'){
					var countryCode = vm.JurisdictionList.find(function(d){
                         return d.jurisdictionName === selected;
					});
					if(countryCode){
					   selected = countryCode && countryCode.jurisdictionOriginalName ? countryCode.jurisdictionOriginalName : selected;
					}
				}
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					cellTemplate: '<div class="d-flex ai-c" ng-class="{\'pe-none\' : row.entity.Visible == false}"><p class="text-overflow f-14 text-cream mar-b0 ">{{COL_FIELD}}</p></div>',
					headerCellClass: 'text-center',
					sortType: 'Text',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants,selected),
//					filterHeaderTemplate: '<div ng-dropdown-multiselect="" options="grid.appScope[col.field+\'_data\']" selected-model="grid.appScope[col.field+\'_model\']" extra-settings="grid.appScope[col.field+\'_settings\']")" events="grid.appScope[col.field]"  ></div>',
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableSorting:true,
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn // hide or show columns based on api response
				});

			} else if (value.columnType === 'Slider') {
				var  selected= vm.filtersValue[value.columnFieldName] !== ''&& vm.filtersValue[value.columnFieldName] !== undefined?vm.filtersValue[value.columnFieldName]:'';
				count = count+1;
				var sortDirection = uiGridConstants.ASC;
				if(vm.SortValues[value.columnFieldName] == 'asc'){
					sortDirection = { direction: uiGridConstants.ASC, priority: 1  };
				}else if(vm.SortValues[value.columnFieldName] == 'desc'){
					sortDirection = { direction: uiGridConstants.DESC, priority: 1  };
				}else{
					sortDirection = null;
				}
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					headerCellClass: 'text-center',
					sort: sortDirection,
					sortType: 'icon',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants,selected),
					type: 'number',
					cellTemplate: '<div style="display:flex;justify-content: center;" ng-class="{\'c-ban\' : row.entity.Visible == false }">' + getSlider(value.columnHeaderName,count) + '</div>',
					width: value.width,
					filterClassificationKey:value.subclassifications,
					subclassificationId:value.subclassificationId,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'media') {
				$rootScope.mediaFilterOptions = value.FilterDropdownData;
				var  selected= vm.filtersValue[value.columnFieldName];
				var sortDirection = uiGridConstants.ASC;
				if(vm.SortValues[value.columnFieldName] == 'asc'){
					sortDirection = { direction: uiGridConstants.ASC, priority: 1  };
				}else if(vm.SortValues[value.columnFieldName] == 'desc'){
					sortDirection = { direction: uiGridConstants.DESC, priority: 1  };
				}else{
					sortDirection = null;
				}
				this.push({
					field: value.columnFieldName,
//					filterHeaderTemplate: getFilterMediaTemplate(),
					enableSorting: true,
					headerName: value.columnHeaderName,
					enableFiltering:true,
					sort: sortDirection,
					name:value.columnSortName,
					sortType: 'icon',
					displayName:value.columnHeaderName,
					headerCellClass: 'text-center',
					filter: populateFilterDropdown(value.FilterDropdownData.sort(), uiGridConstants,selected),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn , // hide or show columns based on api response
					cellTemplate: '<div style="display:flex;justify-content: space-around;" ng-class="{\'c-ban\' : row.entity.Visible == false}">' + getMediaIcons() + '</div>',
				});

			} else if (value.columnType === 'Icon') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					sortType: 'Text',
					displayName:value.columnHeaderName,
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn, // hide or show columns based on api response
					cellTemplate: '<div style="display:flex;justify-content: center;"></div>'
				});

			} else if (value.columnType === 'textImage') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					sortType: 'Text',
					displayName:value.columnHeaderName,
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					headerCellClass: 'text-center',
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});
			} else if (value.columnType === 'popover') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					sortType: 'Text',
					displayName:value.columnHeaderName,
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'currency') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					sortType: 'Text',
					headerCellClass: 'text-center',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'dateTime') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					sortType: 'Text',
					headerCellClass: 'text-center',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'visible') {
				var  selected= vm.filtersValue[value.columnFieldName];
				var sortDirection = uiGridConstants.ASC;
				if(vm.SortValues[value.columnFieldName] == 'asc'){
					sortDirection = { direction: uiGridConstants.ASC, priority: 1  };
				}else if(vm.SortValues[value.columnFieldName] == 'desc'){
					sortDirection = { direction: uiGridConstants.DESC, priority: 1  };
				}else{
					sortDirection = null;
				}
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					sort: sortDirection,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					sortType: 'icon',
//					filterClassificationKey:value.columnFieldName,
					headerCellClass: 'text-center',
					enableFiltering:true,
					enableSorting: true,
					type: 'boolean',
					cellTemplate:'<div  class = "c-pointer" ng-click="grid.appScope.changeVisibilty(col,row,rowRenderIndex)"><i ng-if="row.entity.Visible == true" class="fa fa-eye f-16 text-dark-cream"></i><i ng-if="row.entity.Visible == false" class="fa fa-eye-slash f-16 text-cream"></i></div>',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants,selected),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class centerAlign';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else if (value.columnType === 'edit') {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					enableSorting: false,
					headerCellClass: 'text-center',
					cellTemplate: '<button style="display:flex;justify-content:center;background: transparent;border: transparent;" ng-class="{\'c-ban\' : row.entity.Visible == false}"  ng-disabled = "!row.entity.Visible" ng-click="grid.appScope.editmodal(col,row,rowRenderIndex)" ><i class="fa fa-edit f-16 text-dark-cream" ng-class="{\'pe-none\' : row.entity.Visible == false}"></i></button>',
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					cellClass: function () {
						return 'source-cell-class centerAlign';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			} else {
				this.push({
					field: value.columnFieldName,
					headerName: value.columnHeaderName,
					name:value.columnSortName,
					displayName:value.columnHeaderName,
					filter: populateFilterDropdown(value.FilterDropdownData, uiGridConstants),
					width: value.width,
					headerCellClass: 'text-center',
					cellClass: function () {
						return 'source-cell-class';
					},
					enableColumnMenu:value.enableColumnMenu,
					enableHiding: value.enableHiding,
					visible:value.enableColumn  // hide or show columns based on api response
				});

			}
		}, col_defs);

		return col_defs;
	}


	/*
	 * @purpose: Generate UI grid Table
	 * @created: 24th December 2018
	 * @params: vm: $scope, gridOptions:Object, gridData: data(Array)
	 * @return: no
	 * @author: Amarjith
	 * 
	*/


	function createGridTable(vm, gridOptions, gridData, uiGridConstants) {
		var firstTimeLoad = false;
		vm.mapMedia = function(sourcemedia) {
			var mapData = [];
			
			angular.forEach(sourcemedia,function(val){
				if(val.mediaName == 'All'){
					angular.forEach(vm.mapMediaHash,function(value){
						if(value !="files-o"){
							mapData.push(value);
						}
					});
				}else{
					mapData.push(vm.mapMediaHash[val.mediaName]);				
				}
			});
			return mapData;
		};
		vm.gridOptions = {
			totalItems : gridOptions.totalItems ? gridOptions.totalItems : 500,
			enableHorizontalScrollbar: gridOptions.enableHorizontalScrollbar?gridOptions.enableHorizontalScrollbar: 1,
			enableVerticalScrollbar:gridOptions.enableVerticalScrollbar?gridOptions.enableVerticalScrollbar: 1,
			enableInfiniteScroll:false,
			suppressRemoveSort : true,
			enableFiltering: gridOptions.enableFiltering ? gridOptions.enableFiltering : true,
			useExternalFiltering: true,
			enableSorting: gridOptions.enableSorting?gridOptions.enableSorting:true,
			enablePinning: gridOptions.enablePinning ? gridOptions.enablePinning : false,
			enablePaginationControls:gridOptions.enablePaginationControls?gridOptions.enablePaginationControls:true,
			enableColumnMoving: gridOptions.enableColumnMoving ? gridOptions.enableColumnMoving : true,
			showGridFooter: gridOptions.showGridFooter ? gridOptions.showGridFooter : true,
			enablePagination: gridOptions.enablePagination ? gridOptions.enablePagination : true,
			useExternalPagination: true, // custom 
			paginationPageSizes: gridOptions.paginationPageSizes ? gridOptions.paginationPageSizes : [5,10,20,50,100],
			paginationPageSize: gridOptions.paginationPageSize ? gridOptions.paginationPageSize : 50,
			enableGridMenu: gridOptions.enableGridMenu ? gridOptions.enableGridMenu : true,
			exporterMenuPdf: gridOptions.exporterMenuExcel ? gridOptions.exporterMenuExcel : false,
			exporterMenuExcel: gridOptions.exporterMenuExcel ? gridOptions.exporterMenuExcel : false,
			enableSelectAll: gridOptions.enableSelectAll ? gridOptions.enableSelectAll : false,
			exporterCsvFilename: gridOptions.exporterCsvFilename ? gridOptions.exporterCsvFilename : 'source-management.csv',
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
			exporterFieldCallback: function(grid, row, gridCol, cellValue) {
				var formatterId = null;
				if(row.entity[gridCol.colDef.field+'_value']) {formatterId = (row.entity[gridCol.colDef.field+'_value'].subClassificationCredibility);}
				if (formatterId) {
				   cellValue = formatterId;
			     } 
				if(gridCol.colDef.field == 'media'){
					formatterId = row.entity.sourceMedia.map(function(d){return d.mediaName;});
					cellValue = formatterId.join(",");
				}
				if(gridCol.colDef.field == 'Link'){
					cellValue = row.entity.sourceLink;
				}

			    return cellValue;
			},
		
			exporterFieldApplyFilters: true,
			exporterExcelFilename: gridOptions.exporterExcelFilename ? gridOptions.exporterExcelFilename : 'myFile.xlsx',
			exporterExcelSheetName: gridOptions.exporterExcelSheetName ? gridOptions.exporterExcelSheetName : 'Sheet1',
			excessRows :100,
			exporterSuppressColumns: [ 'edit' ], // not to show column menu in the exported file
			columnDefs : uiGridFormatdata(gridOptions.columnDefs, uiGridConstants,vm)
		};

		vm.mapMultiSlidert = function (input) {
			var mapHash = {
				'none': 0,
				'low': 1,
				'medium': 2,
				'high': 3
			};
			return mapHash[input];
		};
		vm.mediaFilterOptions = $rootScope.mediaFilterOptions;
		angular.forEach(gridOptions.columnDefs, function (value) {

			if (value.columnType == 'Multi-Select') {
				if (value.columnFieldName == "domain") {
					vm[value.columnFieldName + '_model'] = [];
					vm[value.columnFieldName + '_data'] = vm.DomainList;
					vm[value.columnFieldName + '_settings'] = {
						idProperty: 'id',
					};
					vm[value.columnFieldName] = {
						onInitDone: function () {

						},
						onItemDeselect: function (item) {
							selectedDomain = $.grep(selectedDomain, function (e) {
								return e != item.id;
							});
							if (selectedIndustry.length > 0 && selectedDomain.length > 0) {
								filteredData = gridData;
								filteredData = filteredData.filter(function (d) {
									if ($.inArray(d.industry, selectedIndustry) != -1 && $.inArray(d.domain, selectedDomain) != -1) {
										return d;
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							} else {
								if (selectedDomain.length > 0) {
									filteredData = gridData;
									filteredData = filteredData.filter(function (d) {
										if ($.inArray(d.domain, selectedDomain) != -1) {
											return d;
										}
									});
									vm.gridOptions.data = filteredData;
									vm.gridApi.grid.refresh();
								} else if (selectedIndustry.length > 0) {
									filteredData = gridData;
									filteredData = filteredData.filter(function (d) {
										if ($.inArray(d.industry, selectedIndustry) != -1) {
											return d;
										}
									});
									vm.gridOptions.data = filteredData;
									vm.gridApi.grid.refresh();
								} else {
									filteredData = [];
									vm.gridOptions.data = gridData;
									vm.gridApi.grid.refresh();
								}
							}
						},
						onItemSelect: function (item) {
							selectedDomain.push(item.id);
							if (selectedIndustry.length > 0) {
								filteredData = gridData;
								filteredData = filteredData.filter(function (d) {
									if ($.inArray(d.industry, selectedIndustry) != -1 && $.inArray(d.domain, selectedDomain) != -1) {
										return d;
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							} else {
								angular.forEach(gridData, function (val) {
									if (val.domain == item.id) {
										filteredData.push(val);
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							}

						},
						onSelectAll: function () {
							vm.gridOptions.data = gridData;
							vm.gridApi.grid.refresh();
						},
						onDeselectAll: function () {
							vm.gridOptions.data = gridData;
							vm.gridApi.grid.refresh();
						}

					};
				}
				if (value.columnFieldName == "industry") {
					vm[value.columnFieldName + '_model'] = [];
					vm[value.columnFieldName + '_data'] = vm.IndustryList;
					
					vm[value.columnFieldName + '_settings'] = {
						idProperty: 'id',
					};
					vm[value.columnFieldName] = {
						onInitDone: function () {

						},
						onItemDeselect: function (item) {
							selectedIndustry = $.grep(selectedIndustry, function (e) {
								return e != item.id;
							});
							if (selectedIndustry.length > 0 && selectedDomain.length > 0) {
								filteredData = gridData;
								filteredData = filteredData.filter(function (d) {
									if ($.inArray(d.industry, selectedIndustry) != -1 && $.inArray(d.domain, selectedDomain) != -1) {
										return d;
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							} else {
								if (selectedDomain.length > 0) {
									filteredData = gridData;
									filteredData = filteredData.filter(function (d) {
										if ($.inArray(d.domain, selectedDomain) != -1) {
											return d;
										}
									});
									vm.gridOptions.data = filteredData;
									vm.gridApi.grid.refresh();
								} else if (selectedIndustry.length > 0) {
									filteredData = gridData;
									filteredData = filteredData.filter(function (d) {
										if ($.inArray(d.industry, selectedIndustry) != -1) {
											return d;
										}
									});
									vm.gridOptions.data = filteredData;
									vm.gridApi.grid.refresh();
								} else {
									filteredData = [];
									vm.gridOptions.data = gridData;
									vm.gridApi.grid.refresh();
								}
							}
						},
						onItemSelect: function (item) {
							selectedIndustry.push(item.id);
							if (selectedDomain.length > 0) {
								filteredData = gridData;
								filteredData = filteredData.filter(function (d) {
									if ($.inArray(d.industry, selectedIndustry) != -1 && $.inArray(d.domain, selectedDomain) != -1) {
										return d;
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							} else {
								angular.forEach(gridData, function (val) {
									if (val.industry == item.id) {
										filteredData.push(val);
									}
								});
								vm.gridOptions.data = filteredData;
								vm.gridApi.grid.refresh();
							}
						},
						onSelectAll: function () {
							vm.gridOptions.data = gridData;
							vm.gridApi.grid.refresh();
						},
						onDeselectAll: function () {
							vm.gridOptions.data = gridData;
							vm.gridApi.grid.refresh();
						}
					};

				}
			}
		});
		
		
		 // server side filter on search of source or link
		vm.filterSourceLink =function(column,value,event){
			 if(value === undefined){
				 return;
			 }
		  vm.filtersValue[column.name]=value ? value : '';
   		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
   		  if(columnIndex < 0){
   			vm.appliedFilters.push({
	   				    columnName: column.name,
	   				    filters: [value ? value : '']
	        	   });
   		  }else{
   			vm.appliedFilters[columnIndex].filters = [value ? value : ''];
   		  }
   		 vm.gridApi.grid.columns.forEach( function(column) {
	          if(column.filter.term !=='' && column.filter.term !== undefined) {		        	  
	        	  if(column.colDef.filterClassificationKey){
	        		  vm.filtersValue[column.name] = column.filter.term ? column.filter.term : vm.filtersValue[column.name];
	        		  if(Object.keys(vm.tabChangeFiltersVal).length>0){
	        			  if(Object.keys(vm.tabChangeFiltersVal).indexOf(vm.currentTab)>=0){
	        				  var tabIndex = vm.tabChangeFiltersVal[vm.currentTab].findIndex(function(d){return d.columnName == column.name;});
  						      if(tabIndex < 0){
  						    	vm.tabChangeFiltersVal[vm.currentTab].push({
				   				    columnName: 'subclassifications',
				   				    filters: [vm.getDecryptValueForRangeSlider(column.filter.term)],
				   				    id: column.colDef.subclassificationId
				        	   });
  						      } else{
  						    	vm.tabChangeFiltersVal[vm.currentTab][tabIndex].filters = [vm.getDecryptValueForRangeSlider(column.filter.term)];
  						      }
	        			  } else{
	        				  vm.tabChangeFiltersVal[vm.currentTab]=[];
	    					  vm.tabChangeFiltersVal[vm.currentTab].push({
				   				    columnName: 'subclassifications',
				   				    filters: [vm.getDecryptValueForRangeSlider(column.filter.term)],
				   				    id: column.colDef.subclassificationId
				        	   });
	        			  }
	  						
	        		   
	        		  } else{
	        			  vm.tabChangeFiltersVal[vm.currentTab]=[];
						  vm.tabChangeFiltersVal[vm.currentTab].push({
			   				    columnName: 'subclassifications',
			   				    filters: [vm.getDecryptValueForRangeSlider(column.filter.term)],
			   				    id: column.colDef.subclassificationId
			        	   });
	        		  }
	  					
	        		  
	        		  
	        		  
	        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.id == column.colDef.subclassificationId;});
	        		  if(columnIndex < 0){
			        	   vm.appliedFilters.push({
			   				    columnName: 'subclassifications',
			   				    filters: [vm.getDecryptValueForRangeSlider(column.filter.term)],
			   				    id: column.colDef.subclassificationId
			        	   });
	        		  }else{
	        			  vm.appliedFilters[columnIndex].filters = [vm.getDecryptValueForRangeSlider(column.filter.term)];
	        		  }
	        	  } else {
					var filtered_value = column.filter.term;
					if(column && column.name.toLowerCase() === 'jurisdiction'){
						var countryCode = vm.JurisdictionList.find(function(d){ return d.jurisdictionOriginalName.toLowerCase() === filtered_value.toLowerCase();});
						if(countryCode){
						  filtered_value = countryCode && countryCode.jurisdictionName ? countryCode.jurisdictionName : filtered_value;
						}
					 }
	        		  vm.filtersValue[column.name]=(filtered_value !== '')  ? filtered_value : (vm.filtersValue[column.name] ? vm.filtersValue[column.name] : '');
	        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
	        		  if(columnIndex < 0){
	        			  if(column.colDef.field.toLowerCase() == 'visible'){
	        				  vm.appliedFilters.push({
				   				    columnName: column.name.toLowerCase()
				        	   });
	        			  }else{
				        	   vm.appliedFilters.push({
				   				    columnName: column.name,
				   				    filters: [filtered_value ? filtered_value : 'All']
				        	   });
	        			  }
	        		  }else{
	        			  vm.appliedFilters[columnIndex].filters = [filtered_value ? filtered_value : 'All'];
	        		  }			        	   
	        	  }	        	
	          }else{
	        	  if(column.colDef.filterClassificationKey){
	        		  vm.filtersValue[column.name]=column.filter.term ? column.filter.term : vm.filtersValue[column.name];
	        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.id == column.colDef.subclassificationId;});
	        		  if(columnIndex >= 0){
	        			  vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.id != column.colDef.subclassificationId ;});
	        		  }
	        	  } else {
	        		  if(column.colDef.columnType === 'Text' || column.colDef.columnType === 'Hyperlink'){		        			  
	        			  vm.filtersValue[column.name] = vm.filtersValue[column.colDef.name];
	        		  }else{
	        			  vm.filtersValue[column.name]=column.filter.term ? column.filter.term : (vm.filtersValue[column.name] ? vm.filtersValue[column.name] : '');
	        			  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
		        		  if(columnIndex >= 0 && !vm.filtersValue[column.name]){
		        			  vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.columnName != column.name; });
		        		  }		
	        		  }	        		  
	        		  	        	   
	        	  }	
	          }
	        });
   		 	vm.appliedFilters =  _.uniqWith(vm.appliedFilters , _.isEqual);
			var sendReq = false;
			if(event.type == 'click'){
				sendReq = true;
				if(value === ''){
					vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.columnName != column.name; });
				}
			}else if(event.keyCode == 13){
				sendReq = true;
				if(value === ''){
					vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.columnName != column.name; });
				}
			}
			if(sendReq){
		   		var pageNum = 1;
		   	  if(Object.keys(vm.tabChangeFiltersVal).length>0){
					if(Object.keys(vm.tabChangeFiltersVal).indexOf(vm.currentTab)>=0){
						  var tabIndex = vm.tabChangeFiltersVal[vm.currentTab].findIndex(function(d){return d.columnName == column.name;});
						      if(tabIndex < 0){
							          vm.tabChangeFiltersVal[vm.currentTab].push({
						   				    columnName: column.name,
						   				    filters: [value ? value : '']
						        	   });
					   		  }else{
					   			vm.tabChangeFiltersVal[vm.currentTab][tabIndex].filters = [value ? value : ''];
					   		  }
					} else{
						vm.tabChangeFiltersVal[vm.currentTab]=[];
						  vm.tabChangeFiltersVal[vm.currentTab].push({
			 				    columnName: column.name,
			   				    filters: [value ? value : '']
			        	   });
					}
				  } else{
					  vm.tabChangeFiltersVal[vm.currentTab]=[];
					  vm.tabChangeFiltersVal[vm.currentTab].push({
		 				    columnName: column.name,
		   				    filters: [value ? value : '']
		        	   });
				  }
		   		checkEmptinessForAppliedFilters();
		   		console.log(vm.tabChangeFiltersVal);
		   		if(value === ''){
		   			pageNum = vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum;
		   		}
				vm.getSources(vm.mainClassificationData[vm.tabIndexValue],vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize,pageNum,true,vm.appliedFilters,'','','',vm.filtersValue.Visible) ;   
			}			
		};
		 vm.showHide = function(columnName){
			 var checkbit = true;
			 angular.forEach(gridData, function (val) {
			  if(val[columnName+"_value"] && val[columnName+"_value"].dataAttributes.length >0){
				  checkbit = true;
			  } else {
				  checkbit = false;
			  }
			 });
			return checkbit;
			};
	    function checkEmptinessForAppliedFilters(){
	    	vm.tabChangeFiltersVal[vm.currentTab]=vm.tabChangeFiltersVal[vm.currentTab].filter(function(d){ return _.compact(d.filters).length > 0;});
			if(vm.tabChangeFiltersVal[vm.currentTab].length == 0){
				delete vm.tabChangeFiltersVal[vm.currentTab];
			}
	    }
		vm.gridOptions.data = gridData;
		var filteredData = [];
		var selectedIndustry = [];
		var selectedDomain = [];
		vm.gridOptions.data =[];
		vm.gridOptions.data = gridData;
		if(!vm.gridApi){
			vm.mainClassificationData[vm.tabIndexValue].state = {};
		}
		  vm.saveState = function() {
			  vm.mainClassificationData[vm.tabIndexValue].state = vm.gridApi.saveState.save();
		  };
		 
		  vm.restoreState = function() {
		    vm.gridApi.saveState.restore( vm, vm.mainClassificationData[vm.tabIndexValue].state);
		  };
		vm.gridOptions.onRegisterApi = function(gridApi) {
		    vm.gridApi = gridApi;
		    vm.gridApi.colResizable.on.columnSizeChanged(vm, function(colDef, deltaChange) {
		        console.log(colDef, deltaChange);
		    });
		    vm.gridApi.core.on.sortChanged(vm, function(grid, sortColumns) {
		        if (sortColumns.length == 0) {
		        	vm.SortValues = {};
		        	// on sort reset
		        	vm.getSources(vm.mainClassificationData[vm.tabIndexValue],vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize,vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum,true,vm.appliedFilters,'','','',vm.filtersValue.Visible ) ;   
		        } else {
		            var orderBy = sortColumns[0].colDef.name;
		            var orderIn = sortColumns[0].sort.direction;	
		            var subSlassificationId = '';
		            if(sortColumns[0].colDef.sortType == 'icon'){
		            	orderBy = sortColumns[0].colDef.name;
		            	if(!vm.SortValues[sortColumns[0].colDef.name]){
		            		vm.SortValues = {};
		            	}
		            	vm.SortValues[sortColumns[0].colDef.name] = orderIn;
		            	subSlassificationId = sortColumns[0].colDef.subclassificationId;
		            }else{
		            	vm.SortValues = {};
		            }
		            angular.forEach(vm.appliedFilters,function(val){
		            	if(val.columnName === sortColumns[0].colDef.name){
		            		val.filters = [vm.filtersValue[sortColumns[0].colDef.name]];
		            	}		            	 
		            });
		            vm.getSources(vm.mainClassificationData[vm.tabIndexValue],vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize,vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum,true,vm.appliedFilters,orderBy,orderIn,subSlassificationId,vm.filtersValue.Visible) ;   
		        }
		    });
		   
		    vm.gridApi.core.on.filterChanged(vm,function () {
		        vm.gridApi.grid.columns.forEach( function(column) {
		          if(column.filter.term !=='' && column.filter.term !== undefined) {		        	  
		        	  if(column.colDef.filterClassificationKey){
		        		  vm.filtersValue[column.name]=column.filter.term;
		        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.id == column.colDef.subclassificationId;});
		        		  if(columnIndex < 0){
				        	   vm.appliedFilters.push({
				   				    columnName: 'subclassifications',
				   				    filters: [vm.getDecryptValueForRangeSlider(column.filter.term)],
				   				    id: column.colDef.subclassificationId
				        	   });
		        		  }else{
		        			  vm.appliedFilters[columnIndex].filters = [vm.getDecryptValueForRangeSlider(column.filter.term)];
		        		  }
		        	  } else {
		        		  vm.filtersValue[column.name]=(column.filter.term !== '')  ? column.filter.term : '';
		        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
		        		  if(columnIndex < 0){
		        			  if(column.colDef.field.toLowerCase() == 'visible'){
		        				  vm.appliedFilters.push({
					   				    columnName: column.name.toLowerCase()
					        	   });
		        			  }else{
								var filtered_value = column.filter.term;
								if(column && column.displayName.toLowerCase() === 'jurisdiction'){
									var countryCode = vm.JurisdictionList.find(function(d){ return d.jurisdictionOriginalName.toLowerCase() === filtered_value.toLowerCase();});
									if(countryCode){
									  filtered_value = countryCode && countryCode.jurisdictionName ? countryCode.jurisdictionName : filtered_value;
									}
								 }
					        	   vm.appliedFilters.push({
					   				    columnName: column.name,
					   				    filters: [filtered_value ? filtered_value : 'All']
					        	   });
		        			  }
		        		  }else{
							  var filtered_value = column.filter.term;
							  if(column && column.displayName.toLowerCase() === 'jurisdiction'){
								  var countryCode = vm.JurisdictionList.find(function(d){ return d.jurisdictionOriginalName.toLowerCase() === filtered_value.toLowerCase();});
							      if(countryCode){
									filtered_value = countryCode && countryCode.jurisdictionName ? countryCode.jurisdictionName : filtered_value;
								  }
							   }
		        			  vm.appliedFilters[columnIndex].filters = [filtered_value ? filtered_value : 'All'];
		        		  }			        	   
		        	  }	        	
		          }else{
		        	  if(column.colDef.filterClassificationKey){
		        		  vm.filtersValue[column.name]=column.filter.term;
		        		  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.id == column.colDef.subclassificationId;});
		        		  if(columnIndex >= 0){
		        			  vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.id != column.colDef.subclassificationId; });
		        		  }
		        	  } else {
		        		  if(column.colDef.columnType === 'Text' || column.colDef.columnType === 'Hyperlink'){	
		        			  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
		        			  vm.filtersValue[column.name] = vm.filtersValue[column.colDef.name];
		        			  if(columnIndex >= 0){
		        				  vm.appliedFilters[columnIndex].filters = [vm.filtersValue[column.colDef.name]];
		        			  }
		        		  }else{
		        			  vm.filtersValue[column.name]=column.filter.term ? column.filter.term : '';
		        			  var columnIndex = vm.appliedFilters.findIndex(function(d){return d.columnName == column.name;});
			        		  if(columnIndex >= 0){
			        			  vm.appliedFilters = vm.appliedFilters.filter(function(d){return d.columnName != column.name; });
			        		  }		
		        		  }	        		  
		        		  	        	   
		        	  }	
		          }
		        });
	        	vm.appliedFilters =  _.uniqWith(vm.appliedFilters , _.isEqual);	
		        var pageNum = 1;
		   		if(vm.appliedFilters.length === 0){
		   			pageNum = vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum;
				   }
		        vm.getSources(vm.mainClassificationData[vm.tabIndexValue],vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize,pageNum,true,vm.appliedFilters,'','','',vm.filtersValue.Visible) ;   
			});
		    vm.gridApi.pagination.on.paginationChanged(vm, function(newPage, pageSize) {
		    	 angular.forEach(vm.appliedFilters,function(val){
		            	if(val.filters){
		            		val.filters = [vm.filtersValue[val.columnName]];
		            	}		            	 
		            });	
		        vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum = newPage;    
		        vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize = pageSize;
                vm.totalPage = Math.ceil(vm.gridOptions.totalItems / vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize);    
               if(!vm.isPageNumChanged){
                vm.getSources(vm.mainClassificationData[vm.tabIndexValue],vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize,vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum,true,vm.appliedFilters,'','','',vm.filtersValue.Visible) ; 
               }
		    });
		    vm.gridApi.core.on.columnVisibilityChanged(vm, function(grid){
		    	if(vm.mainClassificationData[vm.tabIndexValue].hiddenColumns.indexOf(grid.field) < 0){
		    		vm.mainClassificationData[vm.tabIndexValue].hiddenColumns.push(grid.field);
		    	}else{
		    		var filterMenu = vm.mainClassificationData[vm.tabIndexValue].hiddenColumns.filter(function(d){return d !== grid.field;});
		    		vm.mainClassificationData[vm.tabIndexValue].hiddenColumns = filterMenu;
		    	}
		    });
		    vm.gridApi.colMovable.on.columnPositionChanged(vm, function(grid,oldPosition, newPosition){/*jshint ignore:line*/
//		    	vm.mainClassificationData[vm.tabIndexValue].columnOrders[grid.field] = newPosition;
//		    	console.log(vm.mainClassificationData[vm.tabIndexValue].columnOrders);
		    	vm.saveState();
		    });
		};
		if(vm.gridApi){
			vm.gridApi.grid.options.totalItems= gridOptions.totalItems ? gridOptions.totalItems : 500;
            vm.totalPage = Math.ceil(vm.gridOptions.totalItems / vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageSize);    

			if(vm.totalPage === 1){
				vm.isPageNumChanged = true;
			}
			vm.gridApi.grid.columns = [];
			vm.gridApi.grid.options.paginationCurrentPage = vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum ? vm.mainClassificationData[vm.tabIndexValue].paginationOptions.pageNum : 1;
			vm.gridApi.core.refresh();

			setTimeout(() => {
				vm.restoreState ();				
			}, 500);
		}
		  
		vm.onRangeSliderChange = function (value,row,col,sliderName,rowRenderIndex) {/*jshint ignore:line */
			if(firstTimeLoad){
				vm.btnSubmit = false;
				vm.currRowIndex = row;
				$rootScope.setSliderDataForSource(row,sliderName);
			}
			
		};

		vm.getDecryptValueForRangeSlider = function(input){
			var mapHash = {
			    0:'NONE',
			    1:'LOW',
			    2:'MEDIUM',
			    3: 'HIGH'
			  };
				  return mapHash[input];
		};
		
		window.mediaFilter = function(value,mediaGridValue){
			var finalData = [];
			gridData.map(function(data){
				var push = false;
				data.sourceMedia.map(function(d){
					if(d.mediaName.toLowerCase() == mediaGridValue.toLowerCase()){
						push = true;
					}
					else if(d.mediaName.toLowerCase() == 'all'){
						push = true;
					}
				});
				if(push){
					finalData.push(data);
				}
			});
				vm.gridOptions.data = finalData;
				vm.gridApi.grid.refresh();
		};
		
		var rowValue ={};
			vm.changeVisibilty = function (col, row) {
				vm.btnSubmit = false;
				rowValue = row;
				// disable or enable sliders based on visible change
				if(row.entity.Classification.length > 0){
					var classification_index = row.entity.Classification.findIndex(function(d){ return d.classifcationName === vm.currentTab; });
					var subclassification_data =  row.entity.Classification[classification_index].subClassifications;
					if(subclassification_data && subclassification_data.length > 0){
						angular.forEach(subclassification_data,function(val){
							if(row.entity[val.subClassifcationName.split(' ').join('')+'_value'].dataAttributes && row.entity[val.subClassifcationName.split(' ').join('')+'_value'].dataAttributes.length > 0){
								var allEqual =  row.entity[val.subClassifcationName.split(' ').join('')+'_value'].dataAttributes.every(  v => v.credibilityValue === row.entity[val.subClassifcationName.split(' ').join('')+'_value'].dataAttributes[0].credibilityValue);
				    			if(allEqual){
				    				// row.entity[val.subClassifcationName.split(' ').join('')+'_isDisabled'] = rowValue.entity.Visible;
				    			}else{
				    				row.entity[val.subClassifcationName.split(' ').join('')+'_isDisabled'] = true;
				    			}
							}
						});
					}
				}
		     $rootScope.updateVisibity(col.colDef.field,row);
			};
			
			 $rootScope.updateVisibityAccordingToStatus = function(status){
				 if(status){
					 rowValue.entity.Visible = !rowValue.entity.Visible;
				 }
				 
			 };	
				
	/*@Purpose: To render template of data attributes of the source
	 * @author: Amarjith Kumar
	 * @ return : null
	 * @Date: 7th Jan 2019
	 */
		vm.htmlPopover = function(row,col,rowRenderIndex){ return vm.getTemplate(row,col,rowRenderIndex);};	
	
		
		vm.htmlMediaPopover = function(row,col,rowRenderIndex){ return vm.getMediaTemplate(row,col,rowRenderIndex);};
		/*
		 * @purpose: Generate UI grid Table
		 * @created: 4th JAN 2019
		 * @params:  row:Object, col: Object, rowindex:number
		 * @return: no
		 * @author: Amarjith
		 * 
		*/
		vm.editmodal = function (row, col, rowindex) {
			vm.openModalForEditSource(col.entity,rowindex,col);
		};

		setTimeout(function(){
			firstTimeLoad = true;
		},0);
		
	}  
	

	/*
	 * @purpose: Capitalize the first character of a String
	 * @created: 24th JAN 2019
	 * @params:  string<type:  String>
	 * @return: string
	 * @author: Amarjith
	 * 
	*/
	function jsUcfirst(string) { 
		var splitString = string.split(" ");
		var StringChanged = [];
		angular.forEach(splitString,function(val){
			StringChanged.push(val.charAt(0).toUpperCase() + val.slice(1)); 
		});
		return StringChanged.join(" ");
	}
	
	/*
	 * @purpose: Compare the string for sorting
	 * @created: 24th JAN 2019
	 * @params:  a<String>,b<String>
	 * @return: 0 || 1
	 * @author: Amarjith
	 * 
	*/
	function compareStrings(a,b) {
		if (a.label < b.label)
		  {return -1;}
		if (a.label > b.label)
		  {return 1;}
		return 0;
	}

}

