'use strict';
elementApp
		.factory('TopPanelApiService', topPanelApiService);

		topPanelApiService.$inject = [
			'$http',
			'$rootScope',
			'$q',
			'EHUB_API',
			'EHUB_FE_API',
			'Upload'
		];
		
		function topPanelApiService(
				$http,
				$rootScope,
				$q,
				EHUB_API,
				EHUB_FE_API,
				Upload){
			var templateData;
			return {
				getOrganationNamesSuggestions: getOrganationNamesSuggestions,
				getElasticSearchSuggest: getElasticSearchSuggest,
				getElasticSearchData: getElasticSearchData,
				getUserEvents: getUserEvents,
				getNotificationAlert: getNotificationAlert,
				getLogout: getLogout,
				getAllDocuments: getAllDocuments,
				getAllEvidenceDocuments:getAllEvidenceDocuments,
				downloadDocument: downloadDocument,
				uploadDocument: uploadDocument,
				deleteDocument: deleteDocument,
				updateDocumentContent:updateDocumentContent,
				updateDocumentTitle:updateDocumentTitle,
				fullTextSearchDocument:fullTextSearchDocument,
				fullTextSearchSharedDocument:fullTextSearchSharedDocument,
				getUserListing:getUserListing,
				shareDocumentWithUser:shareDocumentWithUser,
				unshareDocumentWithUser:unshareDocumentWithUser,
				sharedDocument:sharedDocument,
				listCases:listCases,
				shareDocumentWithCase:shareDocumentWithCase,
				unshareDocumentWithCase:unshareDocumentWithCase,
				getCaseMapping:getCaseMapping,
				addComment:addComment,
				listDocComment:listDocComment,
				documentParser:documentParser,
				getNotificationList:getNotificationList,
				postNotification:postNotification,
				getNotificationCheckIn:getNotificationCheckIn,
				getNotificationCount:getNotificationCount,
				createcalendarEvent: createcalendarEvent,
				updateCalendarEvent: updateCalendarEvent,
				getCalendarList: getCalendarList,
				addParticipantsToEvent: addParticipantsToEvent,
				removeParticipantsFromEvent: removeParticipantsFromEvent,
				getCalendarEventDetails: getCalendarEventDetails,
				participatedEventList: participatedEventList,
				getVLAdata:getVLAdata,
				getEdittedTemplateData:getEdittedTemplateData,
			    returnEdittedTemplateData:returnEdittedTemplateData,
			    docparserEditRenameTemplate:docparserEditRenameTemplate,
				docparserdeleteLandingPageTemplate:docparserdeleteLandingPageTemplate,
				existTemplate : existTemplate,
				deleteEntityDocument: deleteEntityDocument,
				entityselection :'',
				clipBoardObject :{ modal_postion:{'top':'','left':''},'file_wraper':''},
				getEvidenceArray:[],
				spliceValue:[],
				uploadFromScreenShotPopupUploadIcon:{'value':{},'rowIndex':''},
				uploadDocumentByScreenShotURLData:uploadDocumentByScreenShotURLData,
				uploadDocumentFromUploadIconInScreenshot:uploadDocumentFromUploadIconInScreenshot,
				saveSourcesAddToPage:saveSourcesAddToPage,
				deleteSourceAddtoPage:deleteSourceAddtoPage
			};
			
			
			/*
		     * @purpose: get org name suggestion
		     * @created: 3rd May 2018
		     * @params: token(object)
		     * @return: success, error functions
		     * @author: Amritesh
		    */
		   function getOrganationNamesSuggestions(name){
			var apiUrl = EHUB_API + 'entity/person/suggest-name-loc';
//			var apiUrl = EHUB_API +'tuna/search/companies?token='+$rootScope.ehubObject.token+'&q='+name;
			
		 /* 	if(name){
				apiUrl +=  name  ? "?query=" + name+'&locationCode=gb' : "";
			}  */
			var apiUrl = EHUB_API + 'entity/org/suggest-name' + "?query=" + name +'&locationCode=gb'+ "&token=" + $rootScope.ehubObject.token;
	        var request = $http({
	            method: "GET",
	            url: apiUrl, 
	            params: {
	            	token: $rootScope.ehubObject.token
	            } 
	        });
	        
	        return(request
	                .then(getOrganationNamesSuggestionsSuccess)
	                .catch(getOrganationNamesSuggestionsError));
	
	        /*getOrganationNamesSuggestions error function*/
	        function getOrganationNamesSuggestionsError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	       /*getOrganationNamesSuggestions success function*/
	        function getOrganationNamesSuggestionsSuccess(response) {
	            return(response);
	        }
			
		}
		   
			
			
			
			/*
		     * @purpose: get elastic search suggest
		     * @created: 12 sep 2017
		     * @params: params(object), data(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getElasticSearchSuggest(params, data){
				var apiUrl =  EHUB_API + 'elasticsearch/_suggest';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(getElasticSearchSuggestSuccess)
		                .catch(getElasticSearchSuggestError));
		
		        /*getElasticSearchSuggest error function*/
		        function getElasticSearchSuggestError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getElasticSearchSuggest success function*/
		        function getElasticSearchSuggestSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get elastic search data
		     * @created: 12 sep 2017
		     * @params: params(object), data(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getElasticSearchData(params, data){
				var apiUrl = EHUB_API + 'elasticsearch/getData';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(getElasticSearchDataSuccess)
		                .catch(getElasticSearchDataError));
		
		        /*getElasticSearchData error function*/
		        function getElasticSearchDataError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		         /*getElasticSearchData success function*/
		        function getElasticSearchDataSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get user events
		     * @created: 13 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getUserEvents(params){
				var apiUrl = EHUB_API + 'events/getUserEvents';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params,
		            headers: {
		                'Content-Type': 'application/json'
		            }	
		        });
		        return(request
		                .then(getUserEventsSuccess)
		                .catch(getUserEventsError));
		
		        /*getUserEvents error function*/
		        function getUserEventsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getUserEvents success function*/
		        function getUserEventsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get notification alert
		     * @created: 13 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getNotificationAlert(params){
				var apiUrl = EHUB_API + 'notification/getNotificationAlert';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getNotificationAlertSuccess)
		                .catch(getNotificationAlertError));
		
		        /*getNotificationAlert error function*/
		        function getNotificationAlertError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getNotificationAlert success function*/
		        function getNotificationAlertSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get logout
		     * @created: 13 sep 2017
		     * @params: null
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getLogout(){
				var apiUrl = EHUB_FE_API + 'logout';
		        var request = $http({
		            method: 'GET',
		            url: apiUrl
		        });
		        return(request
		                .then(getLogoutSuccess)
		                .catch(getLogoutError));
		
		        /*getLogout error function*/
		        function getLogoutError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getLogout success function*/
		        function getLogoutSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of documents
		     * @created: 20 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllDocuments(params){
				params.isAllRequired =true;
				var apiUrl = EHUB_API + 'documentStorage/myDocuments';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getAllDocumentsSuccess)
		                .catch(getAllDocumentsError));

		        /*getAllDocuments error function*/
		        function getAllDocumentsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		         /*getAllDocuments success function*/
		        function getAllDocumentsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of evidence documents
		     * @created: 02 july 2019
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: karnakar
		    */
		   function getAllEvidenceDocuments(params){
			var apiUrl = EHUB_API +'documentStorage/myDocuments?token='+$rootScope.ehubObject.token+'&pageNumber='+params.pageNumber+'&recordsPerPage='+params.recordsPerPage+'&docFlag='+params.docFlag+'&entityId='+params.entityId+'&orderBy='+params.orderBy+'&orderIn='+params.orderIn+'&isAllRequired='+true;
			// var apiUrl = EHUB_API +'documentStorage/myDocuments?token='+$rootScope.ehubObject.token+'&pageNumber='+params.pageNumber+'&recordsPerPage='+params.recordsPerPage+'&entityId='+params.entityId+'&orderBy='+params.orderBy+'&orderIn='+params.orderIn+'&docFlag='+params.docFlag;
			var request = $http({
				method: "GET",
				url: apiUrl,
				//params: params
			});
			return(request
					.then(getAllDocumentsSuccess)
					.catch(getAllDocumentsError));

			/*getAllDocuments error function*/
			function getAllDocumentsError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				 /*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

			 /*getAllDocuments success function*/
			function getAllDocumentsSuccess(response) {
				return(response);
			}
		}
			
			/*
		     * @purpose: download document
		     * @created: 20 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function downloadDocument(params){
				var apiUrl = EHUB_API + 'documentStorage/downloadDocument';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params,
			        responseType: "arraybuffer"
		        });
		        return(request
		                .then(getAllDocumentsSuccess)
		                .catch(getAllDocumentsError));

		        /*getAllDocuments error function*/
		        function getAllDocumentsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		         /*getAllDocuments success function*/
		        function getAllDocumentsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: upload document
		     * @created: 20 sep 2017
		     * @params: params(object), File(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			
			
			function uploadDocument(params, file){
				console.log('file: ', file);
				console.log('params: ', params);
				
				var deferredAbort = $q.defer();
				var apiUrl = EHUB_API + 'documentStorage/uploadDocument';
				console.log('apiUrl: ', apiUrl);
		        var request = Upload.upload({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: file,
		            timeout: deferredAbort.promise
		        });
		        var promise = request
		                .then(uploadDocumentSuccess)
		                .catch(uploadDocumentError);
		        /*uploadDocument error function*/
		        function uploadDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function uploadDocumentSuccess(response) {
		            return(response);
		        }
		        
		        promise.abort = function() {
					deferredAbort.resolve();
				};
				
				promise.finally(
						function() {
							promise.abort = angular.noop;
							deferredAbort = request = promise = null;
						}
					);
					return( promise );
			}
			
			/*
		     * @purpose: exist Template
		     * @created: 25 sep 2018
		     * @params: params(object), File(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			
			
		   function existTemplate(params, file){
				
			var deferredAbort = $q.defer();
			var apiUrl = EHUB_API + 'documentParser/existTemplate';
			var request = Upload.upload({
				method: "POST",
				url: apiUrl,
				params: params,
				data: file,
				timeout: deferredAbort.promise
			});
			var promise = request
					.then(existTemplateSuccess)
					.catch(existTemplateError);
			/*existTemplate error function*/
			function existTemplateError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

		   /*existTemplate success function*/
			function existTemplateSuccess(response) {
				return(response);
			}
			
			promise.abort = function() {
				deferredAbort.resolve();
			};
			
			promise.finally(
					function() {
						promise.abort = angular.noop;
						deferredAbort = request = promise = null;
					}
				);
				return( promise );
		}
			
			/*
		     * @purpose: update document
		     * @created: 20 sep 2017
		     * @params: params(object), File(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function updateDocumentContent(params, file){
				var apiUrl = EHUB_API + 'documentStorage/updateDocumentContent';
		        var request = Upload.upload({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: file
		        });
		        return(request
		                .then(updateDocumentContentSuccess)
		                .catch(updateDocumentContentError));

		        /*uploadDocument error function*/
		        function updateDocumentContentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function updateDocumentContentSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: update document title
		     * @created: 03 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function updateDocumentTitle(params){
		    	var apiUrl =EHUB_API+'documentStorage/updateDocument'+"?token="+params.token+"&docId="+params.docId;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId,
				    		'title':params.title,
				    		'remark':' '
				    	
				    	}
			        });
		        return(request
		                .then(updateDocumentTitleSuccess)
		                .catch(updateDocumentTitleError));

		        /*uploadDocument error function*/
		        function updateDocumentTitleError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function updateDocumentTitleSuccess(response) {
		            return(response);
		        }
			}

			
			
			/*
		     * @purpose: delete document
		     * @created: 12 mar 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function deleteDocument(params){
				var apiUrl = EHUB_API + 'documentStorage/softDeleteDocument';
		        var request = Upload.upload({
		            method: "DELETE",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(deleteDocumentSuccess)
		                .catch(deleteDocumentError));

		        /*deleteDocument error function*/
		        function deleteDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*deleteDocument success function*/
		        function deleteDocumentSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: Full test search
		     * @created: 03 April 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function fullTextSearchDocument(params){
				var apiUrl = EHUB_API + 'documentStorage/fullTextSearchMyDocuments';
		        var request = Upload.upload({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(fullTextSearchDocumentSuccess)
		                .catch(fullTextSearchDocumentError));

		        /*fullTextSearchDocument error function*/
		        function fullTextSearchDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*fullTextSearchDocument success function*/
		        function fullTextSearchDocumentSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: Full text search shared
		     * @created: 03 April 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function fullTextSearchSharedDocument(params){
				var apiUrl = EHUB_API + 'documentStorage/fullTextSearchDocument';
		        var request = Upload.upload({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(fullTextSearchSharedDocumentSuccess)
		                .catch(fullTextSearchSharedDocumentError));

		        /*fullTextSearchSharedDocument error function*/
		        function fullTextSearchSharedDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*fullTextSearchSharedDocument success function*/
		        function fullTextSearchSharedDocumentSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Get all user detail
		     * @created: 04 April 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getUserListing(params){
				var apiUrl = EHUB_API + 'user/getUserListing'+"?token="+params.token;
				  var request = $http({
			            method: "GET",
			            url: apiUrl
			        });
		        return(request
		                .then(getUserListingSuccess)
		                .catch(getUserListingError));

		        /*fullTextSearchDocument error function*/
		        function getUserListingError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*fullTextSearchDocument success function*/
		        function getUserListingSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Get all case detail
		     * @created: 05 April 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function listCases(params){
				var apiUrl = EHUB_API + 'case/listCases'+"?token="+params.token;
				  var request = $http({
			            method: "GET",
			            url: apiUrl
			        });
		        return(request
		                .then(listCasesSuccess)
		                .catch(listCasesError));

		        /*fullTextSearchDocument error function*/
		        function listCasesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*fullTextSearchDocument success function*/
		        function listCasesSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: share Document
		     * @created: 03 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function shareDocumentWithUser(params){
		    	var apiUrl =EHUB_API+'documentStorage/shareDocumentWithUser'+"?token="+params.token+"&userId="+params.userId +"&docId="+params.docId+"&permission="+params.permission;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId
				    	}
			        });
		        return(request
		                .then(shareDocumentWithUserSuccess)
		                .catch(shareDocumentWithUserError));

		        /*uploadDocument error function*/
		        function shareDocumentWithUserError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function shareDocumentWithUserSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Unshare Document
		     * @created: 03 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function unshareDocumentWithUser(params){
		    	var apiUrl =EHUB_API+'documentStorage/unshareDocumentWithUser'+"?token="+params.token+"&userId="+params.userId +"&docId="+params.docId +"&comment="+params.comment;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId,
				    		'comment':params.comment
				    	}
			        });
		        return(request
		                .then(unshareDocumentWithUserSuccess)
		                .catch(unshareDocumentWithUserError));

		        /*uploadDocument error function*/
		        function unshareDocumentWithUserError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function unshareDocumentWithUserSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: share document list
		     * @created: 04  april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function sharedDocument(params){
		    	var apiUrl =EHUB_API+'documentStorage/sharedDocument';
		    	 var request = $http({
			            method: "GET",
			            url: apiUrl,
			            params: params
			        });
		        return(request
		                .then(sharedDocumentSuccess)
		                .catch(sharedDocumentError));

		        /*uploadDocument error function*/
		        function sharedDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function sharedDocumentSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: link case to document
		     * @created: 03 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function shareDocumentWithCase(params){
		    	var apiUrl =EHUB_API+'documentStorage/shareDocumentWithCase'+"?token="+params.token+"&caseId="+params.caseId +"&docId="+params.docId;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId
				    	}
			        });
		        return(request
		                .then(shareDocumentWithCaseSuccess)
		                .catch(shareDocumentWithCaseError));

		        /*uploadDocument error function*/
		        function shareDocumentWithCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function shareDocumentWithCaseSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Unlink case to document
		     * @created: 06 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function unshareDocumentWithCase(params){
		    	var apiUrl =EHUB_API+'documentStorage/unshareDocumentWithCase'+"?token="+params.token+"&caseId="+params.caseId +"&docId="+params.docId;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId
				    	}
			        });
		        return(request
		                .then(unshareDocumentWithCaseSuccess)
		                .catch(unshareDocumentWithCaseError));

		        /*uploadDocument error function*/
		        function unshareDocumentWithCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function unshareDocumentWithCaseSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: Document Associations
		     * @created: 05 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getCaseMapping(params){
		    	var apiUrl =EHUB_API+'documentStorage/getCaseMapping'+"?token="+params.token+"&docId="+params.docId;
		    	 var request = $http({
			            method: "GET",
			            url: apiUrl
			        });
		        return(request
		                .then(getCaseMappingSuccess)
		                .catch(getCaseMappingError));

		        /*uploadDocument error function*/
		        function getCaseMappingError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function getCaseMappingSuccess(response) {
		            return(response);
		        }
			}

			/*
		     * @purpose: Add Comment
		     * @created: 06 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function addComment(params){
		    	var apiUrl =EHUB_API+'documentStorage/addComment'+"?token="+params.token+"&docId="+params.docId ;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:{
				    		'docId':params.docId,
				    		'title':'testComment',
				    		'description':params.commentdesc
				    	}
			        });
		        return(request
		                .then(addCommentSuccess)
		                .catch(addCommentError));

		        /*uploadDocument error function*/
		        function addCommentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*uploadDocument success function*/
		        function addCommentSuccess(response) {
		            return(response);
		        }
			}
			
			
			
			/*
		     * @purpose: List Comments
		     * @created: 05 april 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function listDocComment(params){
		    	var apiUrl =EHUB_API+'documentStorage/listComments'+"?token="+params.token+"&docId="+params.docId;
		    	 var request = $http({
			            method: "GET",
			            url: apiUrl
			        });
		        return(request
		                .then(listDocCommentSuccess)
		                .catch(listDocCommentError));
		        /*listDocComment error function*/
		        function listDocCommentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*listDocComment success function*/
		        function listDocCommentSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: documentParser
		     * @created: 16th april 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function documentParser(params){
				var apiUrl =  EHUB_API + 'documentParser/generateJsonResponse';
				var request = $http({
		            method: "GET",
		            url: apiUrl,
	                params: params
		        });
		        return(request
		                .then(documentParserSuccess)
		                .catch(documentParserError));
		
		        /*docAggregator error function*/
		        function documentParserError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*docAggregator success function*/
		        function documentParserSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Get Notification list
		     * @created: 17th may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getNotificationList(params){
				var apiUrl =  EHUB_API + 'notification/getUserNotificationList'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getNotificationListSuccess)
		                .catch(getNotificationListError));
		
		        /*getNotificationList error function*/
		        function getNotificationListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getNotificationList success function*/
		        function getNotificationListSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose:  Post Notification list
		     * @created: 18th may 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: varsha
		    */
			function postNotification(data){
		    	var apiUrl =EHUB_API+'notification/createNotificationAlert'+"?token="+$rootScope.ehubObject.token;
          		 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
				    	data:data
			        });
		        return(request
		                .then(postNotificationSuccess)
		                .catch(postNotificationError));

		        /*postNotification error function*/
		        function postNotificationError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*postNotification success function*/
		        function postNotificationSuccess(response) {
		            return(response);
		        }
			}
			
			
			/*
		     * @purpose: Get Notification checkIn
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getNotificationCheckIn(){
				var apiUrl =  EHUB_API + 'notification/checkInNotifications'+"?token="+$rootScope.ehubObject.token;
				 var request = $http({
						method: 'POST', 
						headers: {
				            	"Content-Type": "application/json" 
				            		},
				    	url: apiUrl ,
			        });
		        return(request
		                .then(getNotificationCheckInSuccess)
		                .catch(getNotificationCheckInError));
		
		        /*getNotificationCheckIn error function*/
		        function getNotificationCheckInError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getNotificationCheckIn success function*/
		        function getNotificationCheckInSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: Get Notification count
		     * @created: 23rd may 2018
		     * @params: params(object
		     * @return: success, error functions
		     * @author: varsha
		    */
			function getNotificationCount(){
				var apiUrl =  EHUB_API + 'notification/getNotificationCount	'+"?token="+$rootScope.ehubObject.token;
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		        });
		        return(request
		                .then(getNotificationCountSuccess)
		                .catch(getNotificationCountError));
		
		        /*getNotificationCount error function*/
		        function getNotificationCountError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getNotificationCount success function*/
		        function getNotificationCountSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: create calendar Event
		     * @created: 4th Jun 2018
		     * @params: data(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function createcalendarEvent(data){
				var apiUrl =  EHUB_API + 'calendar/createEvent?token=' + $rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data: data
		        });
		        return(request
		                .then(createcalendarEventSuccess)
		                .catch(createcalendarEventError));
		
		        /*createcalendarEvent error function*/
		        function createcalendarEventError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*createcalendarEvent success function*/
		        function createcalendarEventSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: update Calendar Event
		     * @created: 5th Jun 2018
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function updateCalendarEvent(params, data){
				var apiUrl =  EHUB_API + 'calendar/updateEvent';
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(updateCalendarEventSuccess)
		                .catch(updateCalendarEventError));
		
		        /*update Calendar Event error function*/
		        function updateCalendarEventError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*update Calendar Event success function*/
		        function updateCalendarEventSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get Calendar List
		     * @created: 5th Jun 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function getCalendarList(params){
				var apiUrl =  EHUB_API + 'calendar/getEventListByCreator';
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getCalendarListSuccess)
		                .catch(getCalendarListError));
		
		        /*getCalendarList error function*/
		        function getCalendarListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getCalendarList success function*/
		        function getCalendarListSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: add Participants To an Event
		     * @created: 6th Jun 2018
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function addParticipantsToEvent(params, data){
				var apiUrl =  EHUB_API + 'calendar/addEventParticipants';
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(addParticipantsToEventSuccess)
		                .catch(addParticipantsToEventError));
		
		        /*add Participants To an Event error function*/
		        function addParticipantsToEventError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*add Participants To an Event success function*/
		        function addParticipantsToEventSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: remove Participants From Event
		     * @created: 6th Jun 2018
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function removeParticipantsFromEvent(params, data){
				var apiUrl =  EHUB_API + 'calendar/removeEventParticipants';
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(removeParticipantsFromEventSuccess)
		                .catch(removeParticipantsFromEventError));
		
		        /*remove Participants From Event error function*/
		        function removeParticipantsFromEventError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*remove Participants From Event success function*/
		        function removeParticipantsFromEventSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get Calendar Event Details
		     * @created: 6th Jun 2018
		     * @params: data(object), params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function getCalendarEventDetails(params){
				var apiUrl =  EHUB_API + 'calendar/getEventDetail';
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getCalendarEventDetailsSuccess)
		                .catch(getCalendarEventDetailsError));
		
		        /*get Calendar Event Details error function*/
		        function getCalendarEventDetailsError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*get Calendar Event Details success function*/
		        function getCalendarEventDetailsSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: participated Event List
		     * @created: 7th Jun 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: Swathi
		    */
			function participatedEventList(params){
				var apiUrl =  EHUB_API + 'calendar/getUserEventList';
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(participatedEventListSuccess)
		                .catch(participatedEventListError));
		
		        /*participated Event List error function*/
		        function participatedEventListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*participated Event List success function*/
		        function participatedEventListSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get case details
		     * @created: 07 nov 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getVLAdata(caseId){
				var url = EHUB_API + 'search/graphData/' + caseId + "?token=" + $rootScope.ehubObject.token;
		        var request = $http({
		            method: "GET",
		            url: url
		        });
		        return(request
		                .then(getVLAdataSuccess)
		                .catch(getVLAdataError));

		        /* getVLAdata error function */
		        function getVLAdataError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }

		        /* getVLAdata success function */
		        function getVLAdataSuccess(response) {
		            return(response);
		        }
			}
			function getEdittedTemplateData(dataEditted){
			 		templateData = dataEditted;
			}
		 	function returnEdittedTemplateData(){
			 		return templateData;
			}
		 	/*
		     * @purpose: edit rename docparser
		     * @created: 9th sep 2018
		     * @params: data(object)
		     * @return: success, error functions
		     * @author: Prasanthi
		    */
			function docparserEditRenameTemplate( data){
				var apiUrl = EHUB_API + 'documentParser/updateTemplateName?' + "&token=" + $rootScope.ehubObject.token;
				var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data: data
		        });
		        return(request
		                .then(docparserEditRenameTemplateSuccess)
		                .catch(docparserEditRenameTemplateError));
		
		        /*remove Participants From Event error function*/
		        function docparserEditRenameTemplateError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*remove Participants From Event success function*/
		        function docparserEditRenameTemplateSuccess(response) {
		            return(response);
		        }
			}
			
	        /*
		     * @purpose: deleteLandingPageTemplate
		     * @created: 7 sep 2018
		     * @params: data(object)
		     * @return: success, error functions
		     * @author: Prasanthi
		    */
			function docparserdeleteLandingPageTemplate(templateId){

		        var apiUrl =EHUB_API+'documentParser/deleteTemplate?templateId='+templateId+'&token='+$rootScope.ehubObject.token;			

		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            data:templateId
		        });
		        return(request
		                .then(deleteLandingPageTemplateSuccess)
		                .catch(deleteLandingPageTemplateError));

		/*        deleteLandingPageTemplate error function*/
		        function deleteLandingPageTemplateError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		        /*deleteLandingPageTemplate success function*/
		        function deleteLandingPageTemplateSuccess(response) {
		            return(response);
		        }
			}
			
			
			function deleteEntityDocument(docId){

		        var apiUrl =EHUB_API+'documentStorage/softDeleteDocument?docId='+docId+'&token='+$rootScope.ehubObject.token;			

		        var request = $http({
		            method: "DELETE",
		            url: apiUrl
		        });
		        return(request
		                .then(deleteEntityDocumentSuccess)
		                .catch(deleteEntityDocumentError));

		/*        deleteLandingPageTemplate error function*/
		        function deleteEntityDocumentError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		        /*deleteLandingPageTemplate success function*/
		        function deleteEntityDocumentSuccess(response) {
		            return(response);
		        }
			}

			/*
		     * @purpose: update document by screen shot URL
		     * @created: 01 July 2019
		     * @params: params(object), URL
		     * @return: success, error functions
		     * @author: Karunakar
		    */

		   function uploadDocumentByScreenShotURLData(data){
			   
			if(data.docID){
				var apiUrl = EHUB_API + 'documentStorage/uploadDocumentByScreenShotURL?fileTitle='+(data.fileTitle ? data.fileTitle : '')+"&remarks="+(data.remarks ? data.remarks : '')+"&docFlag="+data.docFlag+"&token="+(data.token ? data.token : '')+'&docID='+(data.docID ? data.docID : '') + '&entityId='+(data.entityId ? data.entityId : '')+'&entitySource='+(data.entitySource ? data.entitySource : '');
			} else{
				var apiUrl = EHUB_API + 'documentStorage/uploadDocumentByScreenShotURL?url='+(data.url ? data.url : '')+"&fileTitle="+(data.fileTitle ? data.fileTitle : '')+"&remarks="+(data.remarks ? data.remarks : '')+"&docFlag="+data.docFlag+"&token="+(data.token ? data.token : '')+'&entityId='+(data.entityId ? data.entityId : '')+'&entityName='+(data.entityName ? data.entityName : '')+'&entitySource='+(data.entitySource ? data.entitySource : '');
			}
			
			var request = $http({
				method: "GET",
				headers:{
					"Content-Type":"multipart/form-data"
				},
				url: apiUrl,
				// params:params
			});
			return(request
					.then(uploadDocumentByScreenShotURLDataSuccess)
					.catch(uploadDocumentByScreenShotURLDataError));

			/*uploadDocument error function*/
			function uploadDocumentByScreenShotURLDataError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

		   /*uploadDocument success function*/
			function uploadDocumentByScreenShotURLDataSuccess(response) {
				return(response);
			}
		}
		/*
		     * @purpose: upload Document From UploadIcon In Screenshot
		     * @created: 09 July 2019
		     * @params: params(object), URL
		     * @return: success, error functions
		     * @author: Amritesh
		    */

			
		function uploadDocumentFromUploadIconInScreenshot(params, file){
			console.log('file: ', file);
			console.log('params: ', params);
			
			var deferredAbort = $q.defer();
			var apiUrl = EHUB_API + 'sourceAddToPage/uploadAddToPageDocument';
			console.log('apiUrl: ', apiUrl);
			var request = Upload.upload({
				method: "POST",
				url: apiUrl,
				params: params,
				data: file,
				timeout: deferredAbort.promise
			});
			var promise = request
					.then(uploadDocumentFromUploadIconInScreenshotSuccess)
					.catch(uploadDocumentFromUploadIconInScreenshotError);
			/*uploadDocument error function*/
			function uploadDocumentFromUploadIconInScreenshotError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

		   /*uploadDocument success function*/
			function uploadDocumentFromUploadIconInScreenshotSuccess(response) {
				return(response);
			}
			
			promise.abort = function() {
				deferredAbort.resolve();
			};
			
			promise.finally(
					function() {
						promise.abort = angular.noop;
						deferredAbort = request = promise = null;
					}
				);
				return( promise );
		}
		function saveSourcesAddToPage(data){
			var apiUrl = EHUB_API + 'sourceAddToPage/saveSourcesAddToPage?token='+$rootScope.ehubObject.token;	
			var request = $http({
				method: "POST",
				url: apiUrl,
				data: data
			});
			return(request
					.then(saveSourcesAddToPageSuccess)
					.catch(saveSourcesAddToPageError));

			/*sourceAdd to page error function*/
			function saveSourcesAddToPageError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

		   /*ourceAdd to page success function*/
			function saveSourcesAddToPageSuccess(response) {
				return(response);
			}
		}

		function deleteSourceAddtoPage(sourceName,entityId){			

			var apiUrl = EHUB_API + 'sourceAddToPage/deleteSourceAddToPage?token='+$rootScope.ehubObject.token + "&entityId="+ entityId + "&sourceName="+ sourceName;	
			var request = $http({
				method: "DELETE",
				url: apiUrl
			});
			return(request
					.then(deleteSourceAddtoPageSuccess)
					.catch(deleteSourceAddtoPageError));

			/*deleteSourceAddtoPage error function*/
			function deleteSourceAddtoPageError(response) {
				if (!angular.isObject(response.data) || !response.data.message) {
					return($q.reject(response.data));
				}
				/*Otherwise, use expected error message.*/
				return($q.reject(response.data.message));
			}

		   /*deleteSourceAddtoPage  success function*/
			function deleteSourceAddtoPageSuccess(response) {
				return(response);
			}
		}
}