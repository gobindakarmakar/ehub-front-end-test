'use strict';
angular.module('ehubApp')
	   .controller('DomainController', domainController);

		domainController.$inject = [
			'$scope', 
			'$rootScope',
			'$state'
		];
		
		function domainController(
				$scope, 
				$rootScope,
				$state) {
    
	    var userDetails = getTokenBySession();
		$rootScope.ehubObject = userDetails;
		$rootScope.ehubObject.lastLoginDate = userDetails.lastLoginDate;
		$rootScope.showUserEvents = false;
	   /*
	     * @purpose: Navigate to process page
	     * @created: 11 sep 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		$scope.toDashboard = function(domain){
			var url;
			window.current_domain = domain;
			localStorage.setItem("domain", domain);			
			$state.go('process');
			
		};
		
		/*
	     * @purpose: Navigate to mip page
	     * @created: 11 sep 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		$scope.toMip = function(){
			$state.go('mipLanding');
		};
		/*
	     * @purpose: Navigate to workspace page
	     * @created: 11 sep 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		$scope.toWorkspace = function(){
			$state.go('discover');
		};
};