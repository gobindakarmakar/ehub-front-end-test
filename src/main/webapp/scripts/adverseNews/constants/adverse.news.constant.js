'use strict';
angular.module('ehubApp')
	   .constant('adverseNewsConst', {
		   TransAmtTrend: {
			   height:  '200',
			   axisX: true,
			   axisY: true,
			   gridy: true,
			   gridcolor:"#283c45"
		   },
		   RiskTrend: {
			   height: '90'  
		   },
		   riskColors: {
			   "high":"#DD4040",
			   "medium":"#3990CF",
			   "low":"#1DBF6A"
   		   }
	   });