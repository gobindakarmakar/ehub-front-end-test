'use strict';
angular.module('ehubApp')
	   .controller('AdverseNewsController', adverseNewsController);

		adverseNewsController.$inject = [
			'$scope', 
			'$rootScope',
			'$timeout',
			'$state',
			'AdverseNewsApiService',
			'AdverseNewsService',
			'HostPathService',
			'Flash',
			'adverseNewsConst'
		];
		
		function adverseNewsController(
				$scope, 
				$rootScope,
				$timeout,
				$state,
				AdverseNewsApiService,
				AdverseNewsService,
				HostPathService,
				Flash,
				adverseNewsConst) {
		var ActualAdverseTxData =[];
		$scope.setPeriod = "Year";
	   	$scope.showMarketPulseLoader = false;
	   	var token = $rootScope.ehubObject.token;
		var params = {
			token: token
		};
	   	var div = d3.select("body").append("div").attr("class", "toolTip_horizontal text-uppercase").style("position", "absolute").style("z-index", 1000).style("background","#283c45").style("padding","5px 10px").style("border-radius","10px").style("color","rgb(36, 115, 181)").style("font-size","10px").style("display","none");
	   	/* data table options */
	   	$scope.dtOptions = {
//				bFilter: false,
			    bInfo: false,
			    "pageLength": 25
   		};
	   	$scope.uploadFIleTwo;
	   	/* on market table Click navigate to linkAnalysis page */
     	$scope.marketModalClick = function(entityID){
 			$scope.entityID = entityID;  	
 			var url = $state.href('linkAnalysis', {'q':entityID['entity-name'],'p':'Adverse News','type':entityID['entity-type']});
 			window.open(url, '_blank');
     	};
	   	/**
	   	 * Function to load market pulse data
	   	 */
	   	$scope.loadmarketPulsedata = function() {
   			$scope.uploadFIlenameOne = '';
   			$scope.uploadnameone = '';
   			$scope.uploadFIleTwo = '';
   			$scope.uploadnameTwo = '';
   			$scope.startAnalysisDisabled = true;
   			$scope.showMarketPulseLoader = true;
   			
	   		/*var url = $rootScope.start_url + "api/marketIntelligence/taxonomy";
	   		var url = "http://95.85.58.85:3301/v1/entities";*/
   			
	   		 AdverseNewsApiService.getEntitiesData(params).then(function(response){
	   			ActualAdverseTxData =response.data;
	   			 $scope.adversTxData = response.data;
	   			 angular.forEach($scope.adversTxData, function(v, k){
	        		if(v['risk'] !== null || v[risk] !== ""){
		        		if(v['risk'] === "high"){
		        			v.tableColor = "'table-high'";
		        		}else if(v['risk'] === "medium"){
		        			v.tableColor = "'table-medium'";
		        		}else{
		        			v.tableColor = "'table-low'";
		        		}
	        		}
	   			 });
	   			 $timeout(function(){
	   				 $scope.showMarketPulseLoader = false;
	   			 },1000);
	   			 handleData(response.data);
	   		 }, function(error){
	   			$scope.showMarketPulseLoader = false;
	   		 });
	   	};
	   	$scope.loadmarketPulsedata();
	   	
	 	/**
	   	 * Function to handle data
	   	 */
	   	function handleData(data){
	   		var actualData = jQuery.extend(true, [], data);
	   		$scope.riskRatio = [];
	   		$scope.risks = [];
	   		
	   		/* create data for risk ratio */
	   		var nested_data_risk = d3.nest()
							   		.key(function(d) { return d.risk; })
							   		.entries(data);
	   		angular.forEach(nested_data_risk, function(d){
	   			d.val = (d.values.length/data.length)*100
	   		});
	   		AdverseNewsService.InitializeandPlotPie(nested_data_risk, "riskRatio");
	   		
	   		/* create data for risk total */
	   		var risk_total = [];
	   		$scope.totalRiskAmount = 0;
	   		angular.forEach(nested_data_risk,function(d){
	   			var amt = 0;
	   			angular.forEach(d.values,function(d1){
	   				amt = amt+parseFloat(d1.amount);
	   				$scope.totalRiskAmount = parseFloat($scope.totalRiskAmount)+parseFloat(d1.amount);
	   			});
	   			risk_total[(d.key).toLowerCase()] = amt;			
	   		})
	   		$scope.riskTotal = [(risk_total['high']?risk_total['high']:0),(risk_total['medium']?risk_total['medium']:0),(risk_total['low']?risk_total['low']:0)];
	   		$scope.riskTotalPct = [Math.ceil((risk_total['high']/$scope.totalRiskAmount)*100),Math.ceil((risk_total['medium']/$scope.totalRiskAmount)*100),Math.ceil((risk_total['low']/$scope.totalRiskAmount)*100)];
//	   		if(d['entity-type']){
	   		/* create data for entity type */
	   		var nested_data_entity_type = d3.nest()
									   		.key(function(d) { return d['entity-type']; })
									   		.entries(data);
	   		angular.forEach(nested_data_entity_type, function(d){
	   			d.val =(d.values.length/data.length)*100
	   		});
	   		//remove undeifned index from array
	   		var index_undefined = _.findIndex(nested_data_entity_type, {"key": 'undefined'});
	   		if(index_undefined>-1){
	   			nested_data_entity_type.splice(index_undefined,1)
	   		}
	   		AdverseNewsService.InitializeandPlotPie(nested_data_entity_type,"entityRatio");
//	   		}
	   		/* create data for transaction amount trend */
	   		var trans_amount_data = data;
	   		trans_amount_data.map(function(d){
	   			d.x = new Date(d.time),
	   			d.value = d.amount
	   		});
	   		var trans_amount_data_final = [];
	   		trans_amount_data_final.push({
	   			key: "",
	   			values: trans_amount_data
	   		})
	   		var options = {
	   		    container: "#trans_amt_trend",
	   		    height: adverseNewsConst.TransAmtTrend.height,
	   		    axisX: adverseNewsConst.TransAmtTrend.axisX,
	   		    axisY: adverseNewsConst.TransAmtTrend.axisY,
	   		    gridy: adverseNewsConst.TransAmtTrend.gridy,
	   		    data: trans_amount_data_final,
	   		    actualData: actualData
	   		 };
	   		new InitializeandPlotLine(options);
	   		
	   		/* create data for trend chart */
	   		var trnd_data_nested = d3.nest()
				   					.key(function(d){return d.risk})
				   					.key(function(d){return d.time})					
				   					.entries(data);
	   		angular.forEach(trnd_data_nested,function(d){
	   			angular.forEach(d.values,function(innerVal){
	   				innerVal.x = new Date(innerVal.key);
	   				innerVal.time = innerVal.key;
	   				var innervol=0;
	   				angular.forEach(innerVal.values,function(d1){
	   					innervol= innervol+parseFloat(d1.amount);
	   				})
	   				innerVal.value =innervol;
	   			});
	   		});
	   		var colors = adverseNewsConst.riskColors;
	   		var colorsObj = colors;
	   		var options = {
			    container: "#riskTrend",
			    height: adverseNewsConst.RiskTrend.height,	
			    data: trnd_data_nested,
			    actualData: actualData,
			    colorsObj: colorsObj
	   		}
	   		new InitializeandPlotLine(options);
	   	};
   		

	   	/**
   		 * Function to show uploading file one
   		 */
	   	 $scope.uploadFile = function(file, item){	
	   		 if($scope.uploadFIlenameOne && $scope.uploadFIleTwo){
	   			 	$scope.showMarketPulseLoader = true;
		   		 	var files = {
		   				 "tx-file": $scope.uploadFIlenameOne,
		   				 "ent-file": $scope.uploadFIleTwo
	   		 		};
			   		AdverseNewsApiService.uploadFile(params, files).then(function(response){
			   			$scope.startAnalysisDisabled = false;
			   			$scope.showMarketPulseLoader = false;
		   			 	HostPathService.FlashSuccessMessage('SUCCESSFUL UPLOAD DOCUMENT', 'Successfully uploaded documents');
			   		}, function(error){
			   			$scope.showMarketPulseLoader = false;
		   			 	HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Uploaded Failed, Please try again later..');
			   		});
			   	}else{
			   	 	HostPathService.FlashErrorMessage('ERROR UPLOAD DOCUMENT', 'Please Upload Required Files');
			   	}
		   	 }
		   	 /**
	   		 * Function to upload file
	   		 */
		   	 $scope.upload = function(file,item){		
		   		 if(item == 'one'){
		   			 $scope.uploadFIlenameOne = file;
		   			 $scope.uploadnameone = file.name;
		   		 }else{
		   			 $scope.uploadFIleTwo = file;
		   			 $scope.uploadnameTwo = file.name;
		   		 }
		   	 };
		   	 /**
		   	  * Function to set period
		   	  */
		   	 $scope.ChangePeriod = function(){
		   		var dt =new Date().getTime();
		   		var totalData = jQuery.extend(true, [], ActualAdverseTxData);
		   		var currentData =[];
		   		if($scope.setPeriod =="Year"){
		   			currentData = totalData;
		   		}else if($scope.setPeriod =="Month"){		   			
		   			currentData = totalData.filter(function(d){		   			
		   			   return (dt-new Date(d.time).getTime())<=(86400000*30)
		   			});
		   			
		   		}else if($scope.setPeriod =="Week"){		   		
		   			currentData = totalData.filter(function(d){		   				
			   			   return (dt-new Date(d.time).getTime())<=(86400000*7)
			   			});
		   		}else if($scope.setPeriod =="Day"){
		   			currentData = totalData.filter(function(d){
			   			   return (dt-new Date(d.time).getTime())<=(86400000)
			   			});
		   		}
		   	 angular.forEach($scope.currentData, function(v, k){
	        		if(v['risk'] !== null || v[risk] !== ""){
		        		if(v['risk'] === "high"){
		        			v.tableColor = "'table-high'";
		        		}else if(v['risk'] === "medium"){
		        			v.tableColor = "'table-medium'";
		        		}else{
		        			v.tableColor = "'table-low'";
		        		}
	        		}
	   			 });
	   			 $scope.adversTxData = currentData;
		   	 }
		};