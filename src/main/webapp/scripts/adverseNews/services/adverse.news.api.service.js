'use strict';
angular.module('ehubApp')
		.factory('AdverseNewsApiService', adverseNewsApiService);

		adverseNewsApiService.$inject = [
			'$http',
			'$q',
			'Upload',
			'EHUB_API',
		];
		
		function adverseNewsApiService(
				$http,
				$q,
				Upload,
				EHUB_API){
			
			return {
				getEntitiesData: getEntitiesData,
				uploadFile: uploadFile
			};
			
			/*
		     * @purpose: get entities data
		     * @created: 21 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getEntitiesData(params){
				var apiUrl =  EHUB_API + 'adverseNews/entities';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getEntitiesDataSuccess)
		                .catch(getEntitiesDataError));
		
		        /*getEntitiesData error function*/
		        function getEntitiesDataError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*getEntitiesData success function*/
		        function getEntitiesDataSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: upload file
		     * @created: 21 oct 2017
		     * @params: params(object), data(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function uploadFile(params, data){
				var apiUrl =  EHUB_API + 'adverseNews/transactions';
		        var request = Upload.upload({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(uploadFileSuccess)
		                .catch(uploadFileError));
		
		        /*uploadFile error function*/
		        function uploadFileError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*uploadFile success function*/
		        function uploadFileSuccess(response) {
		            return(response);
		        }
			}
	};