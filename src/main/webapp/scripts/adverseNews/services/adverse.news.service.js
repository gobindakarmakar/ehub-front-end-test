'use strict';
angular.module('ehubApp')
	   .service('AdverseNewsService', adverseNewsService);

		adverseNewsService.$inject = [
			'$location'
		];
		
		function adverseNewsService(
				$location) {
			
			
//			var myAdverseService = this;
			/**
		   	 * Function to plot pie
		   	 */
			this.InitializeandPlotPie = function(data, id) {
		   		 data.sort(function(a,b){
		   			 return a.key-b.key
		   		 });
		   		 var colors = {
	   				 "high":"#DD4040",
	   				 "medium":"#3990CF",
	   				 "low":"#1DBF6A"
		   		 };
		   		 var colorsObj = colors;
		   		 if(id == "entityRatio"){
		   			 colors=["#34495E","#4D637A"];
		   			 colorsObj=undefined;
		   		 }
		   		 $("#"+id).empty();
		   		 data.map(function (d) {
		   	        return d.value = d.val;
		   		 });
		   		 var maxval = d3.max(data,function(d){
		   	    	return d.val;
		   		 })
		   	    var options = {
		   	        container: "#" + id,
		   	        data: data,
		   	        maxval: maxval,
		   	        height: 125,
		   	        colors: colors,
		   	        colorsObj: colorsObj,
		   	        ispadding:0.2,
			        istxt:parseFloat(maxval).toFixed(1)+"%",
			        islegends:false,
			      

		   	    };
		   	    setTimeout(function () {
		   	        new reusablePie(options);
		   	    });
		   	};
		}
		
		
		
