'use strict';
angular.module('ehubApp')
		.directive('dashboardUtilitiesDiscoverWorkDiaryCaseList', workdiaryDirective);
		
		workdiaryDirective.$inject = [];
			
		function workdiaryDirective(){
        return {
            scope: {
                statusDataForEachCase:'=',
                index: '=',
                name: '=',
                type: '=',
                purpose: '=',
                assignedby: '=',
                trisk: '=',
                irisk: '=',
                drisk: '=',
                seedid: '=',
                currentstatus: '=',
                onClickActDropdown: '&',
                isHideActDropdown: '&',
                onClickActDropdownItem: '&',
                onClickWorkDiaryCard: '&',
                isHideDetailSection: '&'
            },
            restrict: 'E',
            /*template: ''
                    + '<div class="work-diary-case-list" ng-click="onClickWorkDiaryCard({index:index})">'
                    + '<div class="row">'
                    + '<div class="col-md-7">'
                    + '<div class="work-diary-case-list-index">'
                    + '<span>{{ index < 9 ? "0" + (index+1) : index+1}}</span>'
                    + '</div>'
                    + '<div class="work-diary-case-list-name">'
                    + '<span title="{{name | uppercase}}">{{name | uppercase}}</span>'
                    + '</div>'
                    + '<div class="work-diary-case-list-others">'
                    + '<span>TYPE: {{type | uppercase | limitTo:30}}</span>'
                    + '</div>'
                    + '<div class="work-diary-case-list-others">'
                    + '<span>ASSIGNED BY: {{assignedby | uppercase | limitTo:30}}</span>'
                    + '</div>'
                    + '</div>'

                    + '<div class="col-md-5">'
                    + '<div class="work-diary-case-list-risk-graph">'
                    + '<div class="c100 p{{(1 - (1 - drisk) * (1 - irisk) * (1 - trisk))*100 | number:0}} pink radial">'
                    + '<span class="overlay">{{(1 - (1 - drisk) * (1 - irisk) * (1 - trisk))*100 | number:0}}%</span>'
                    + '<div class="slice">'
                    + '<div class="bar"></div>'
                    + '<div class="fill"></div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    
                    + '</div>'
                    + '<div class="row">'
                    + '<div class="col-md-4">'
                    + '<div class="work-diary-case-list-dropdown">'
                    + '<div class="work-diary-case-list-button-panel">'
                    + '<button class="btn btn-green dropdown-toggle act-btn" type="button" ng-click="onClickActDropdown({event:$event, index:index})">ACT<span class="fa fa-angle-down icon"></span></button>'
                    + '<div class="act-dropdown dropdown-submenu work_diary_act_dropdown_div" ng-hide="isHideActDropdown({index: index})">'
                    + '<ul class="work_diary_act_dropdown_list">'

                    + '<li ng-if="currentstatus == 2"><a href="javascript:void(0)"; class="act_dropdown_ACKNOWLEDGED" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_ACKNOWLEDGED\', index: index, seedid: seedid, currentstatus: currentstatus})">Acknowledge</a></li>'
                    
                    + '<li ng-if="currentstatus != 4 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_ACCEPT" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_ACCEPT\', index: index, seedid: seedid, currentstatus: currentstatus})">Accept</a></li>'
                    + '<li ng-if="currentstatus == 4 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_ACCEPT work_diary_act_dropdown_not_active" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_ACCEPT\', index: index, seedid: seedid, currentstatus: currentstatus})">Accept</a></li>'

                    + '<li ng-if="currentstatus != 5 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_PAUSE" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_PAUSE\', index: index, seedid: seedid, currentstatus: currentstatus})">Pause</a></li>'
                    + '<li ng-if="currentstatus == 5 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_PAUSE work_diary_act_dropdown_not_active" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_PAUSE\', index: index, seedid: seedid, currentstatus: currentstatus})">Pause</a></li>'

                    + '<li ng-if="currentstatus != 6 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_REJECT" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_REJECT\', index: index, seedid: seedid, currentstatus: currentstatus})">Decline</a></li>'
                    + '<li ng-if="currentstatus == 6 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_REJECT work_diary_act_dropdown_not_active" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_REJECT\', index: index, seedid: seedid, currentstatus: currentstatus})">Decline</a></li>'

                    + '<li ng-if="currentstatus != 0 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_FORWARD" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_FORWARD\', index: index, seedid: seedid, currentstatus: currentstatus})">Forward </a></li>'
                    + '<li ng-if="currentstatus == 0 && currentstatus != 2"><a href="javascript:void(0)"; class="act_dropdown_FORWARD work_diary_act_dropdown_not_active" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_FORWARD\', index: index, seedid: seedid, currentstatus: currentstatus})">Forward </a></li>'

                    + '</ul>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-md-8">'
                    + '<div class="work-diary-case-list-risk-text pull-right">'
                    + '<span>CUMULATIVE RISK</span>'
                    + '</div>'
                    + '</div>'

                    + '</div>'
                    + '</div>'
                    
                                        
                    + '<div class="work-diary-case-list" ng-hide="isHideDetailSection({index:index})">'
                    
                    + '<div class="work-diary-case-list-description">'
                    + '<p>{{purpose}}</p>'
                    
                    + '<div class="caseriskprofile">'
                    + '<div class="risk-progressbar-holder">'
                    + '<div class="inline">DR</div>'
                    + '<div class="progress">'
                    + '<div title="{{drisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{drisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{drisk * 100}}%">'
                    + '<span class="sr-only">{{drisk * 100}}% Complete</span>'
                    + '</div>'
                    + '</div>'
                    + '<div class="inline">{{drisk * 100}}%</div>'
                    + '</div>'
                    + '<div class="risk-progressbar-holder">'
                    + '<div class="inline">TR</div>'
                    + '<div class="progress">'
                    + '<div title="{{trisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{trisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{(trisk * 100)}}%">'
                    + '<span class="sr-only">{{trisk * 100}}% Complete</span>'
                    + '</div>'
                    + '</div>'				
                    + '<div class="inline">{{trisk * 100}}%</div>'
                    + '</div>'
                    + '<div class="risk-progressbar-holder">'
                    + '<div class="inline">IR</div>'
                    + '<div class="progress">'
                    + '<div title="{{irisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{irisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{(irisk * 100)}}%">'
                    + '<span class="sr-only">{{irisk * 100}}% Complete</span>'
                    + '</div>'
                    + '</div>'				
                    + '<div class="inline">{{irisk * 100}}%</div>'
                    + '</div>'
                    + '</div>'
                    
                    + '</div>'
                    + '</div>'
                    + '',*/
        template : ''
                + '<div class="work-diary-case-list" ng-click="onClickWorkDiaryCard({index:index})">'
                + '<div class="row">'
                + '<div class="col-md-7">'
                + '<div class="work-diary-case-list-index">'
                + '<span>{{ index < 9 ? "0" + (index+1) : index+1}}</span>'
                + '</div>'
                + '<div class="work-diary-case-list-name">'
                + '<span title="{{name | uppercase}}">{{name | uppercase}}</span>'
                + '</div>'
                + '<div class="work-diary-case-list-others">'
                + '<span>TYPE: {{type | uppercase | limitTo:30}}</span>'
                + '</div>'
                + '<div class="work-diary-case-list-others">'
                + '<span>ASSIGNED BY: {{assignedby | uppercase | limitTo:30}}</span>'
                + '</div>'
                + '</div>'

                + '<div class="col-md-5">'
                + '<div class="work-diary-case-list-risk-graph">'
                + '<div class="c100 p{{(1 - (1 - drisk) * (1 - irisk) * (1 - trisk))*100 | number:0}} pink radial">'
                + '<span class="overlay">{{(1 - (1 - drisk) * (1 - irisk) * (1 - trisk))*100 | number:0}}%</span>'
                + '<div class="slice">'
                + '<div class="bar"></div>'
                + '<div class="fill"></div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                
                + '</div>'
                + '<div class="row">'
                + '<div class="col-md-4">'
                + '<div class="work-diary-case-list-dropdown">'
                + '<div class="work-diary-case-list-button-panel">'
                + '<button class="btn btn-green dropdown-toggle act-btn" type="button" ng-click="onClickActDropdown({event:$event, index:index})">ACT<span class="fa fa-angle-down icon"></span></button>'
                + '<div class="act-dropdown dropdown-submenu work_diary_act_dropdown_div" ng-hide="isHideActDropdown({index: index})">'
                
                + '<ul class="work_diary_act_dropdown_list">'
                + '<li ng-repeat="item in statusDataForEachCase"><a href="javascript:void(0)"; class="{{\'act_dropdown_\'+(item.name | uppercase)}}" ng-click="onClickActDropdownItem({event:$event, ev: \'act_dropdown_\'+(item.name | uppercase), index: index, seedid: seedid, currentstatus: currentstatus, selectedStatus: item.key})">{{item.name}}</a></li>'
                + '</ul>'
                
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="col-md-8">'
                + '<div class="work-diary-case-list-risk-text pull-right">'
                + '<span>CUMULATIVE RISK</span>'
                + '</div>'
                + '</div>'

                + '</div>'
                + '</div>'
                
                                    
                + '<div class="work-diary-case-list" ng-hide="isHideDetailSection({index:index})">'
                
                + '<div class="work-diary-case-list-description">'
                + '<p>{{purpose}}</p>'
                
                + '<div class="caseriskprofile">'
                + '<div class="risk-progressbar-holder">'
                + '<div class="inline">DR</div>'
                + '<div class="progress">'
                + '<div title="{{drisk * 100}}%" class="progress-bar progressdr" role="progressbar" aria-valuenow="{{drisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{drisk * 100}}%">'
                + '<span class="sr-only">{{drisk * 100}}% Complete</span>'
                + '</div>'
                + '</div>'
                + '<div class="inline">{{drisk * 100}}%</div>'
                + '</div>'
                + '<div class="risk-progressbar-holder">'
                + '<div class="inline">TR</div>'
                + '<div class="progress">'
                + '<div title="{{trisk * 100}}%" class="progress-bar progresstr" role="progressbar" aria-valuenow="{{trisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{(trisk * 100)}}%">'
                + '<span class="sr-only">{{trisk * 100}}% Complete</span>'
                + '</div>'
                + '</div>'				
                + '<div class="inline">{{trisk * 100}}%</div>'
                + '</div>'
                + '<div class="risk-progressbar-holder">'
                + '<div class="inline">IR</div>'
                + '<div class="progress">'
                + '<div title="{{irisk * 100}}%" class="progress-bar progresscr" role="progressbar" aria-valuenow="{{irisk * 100}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{(irisk * 100)}}%">'
                + '<span class="sr-only">{{irisk * 100}}% Complete</span>'
                + '</div>'
                + '</div>'				
                + '<div class="inline">{{irisk * 100}}%</div>'
                + '</div>'
                + '</div>'
                
                + '</div>'
                + '</div>'
                + '',
            link: function () {
               
            }
        };
    }