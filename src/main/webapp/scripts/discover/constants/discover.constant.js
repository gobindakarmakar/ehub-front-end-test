'use strict';
elementApp
	   .constant('DiscoverConst',{
		   recordsPerPage: 4,
		   statusesInfo : [
	    		{status:"Fresh", code:1},
		    	{status:"Submitted", code:2},
		    	{status:"Acknowledge", code:3},
		    	{status:"Accepted", code:4},
		    	{status:"Pause", code:5},
		    	{status:"Rejected", code:6},
		    	{status:"Focus", code:7},
		    	{status:"Resolved", code:8},
		    	{status:"Canceled", code:9}
	       ],
		   statusList: [
				{status:"Forward", code:null},
		    	{status:"Accepted", code:4},
		    	{status:"Pause", code:5},
		    	{status:"Rejected", code:6},
		    	{status:"Focus", code:7},
		    	{status:"Resolved", code:8},
		    	{status:"Canceled", code:9}
	      ],
	      focusedStatusList: [
				{status:"Forward", code:null},
				{status:"Acknowledge", code:3},
		    	{status:"Accepted", code:4},
		    	{status:"Pause", code:5},
		    	{status:"Rejected", code:6},
		    	{status:"Unfocus", code:7},
		    	{status:"Resolved", code:8},
		    	{status:"Canceled", code:9}
	      ],
	   })
	   .constant('GridsterConst',{
	   		isMobile: true,
	   		pushing: true,
	   		floating: true,
	   		swapping: true,
	   		rowHeight: 320,
	   		maxSizeY: 2,
	   		resizable: {
	   			enabled: true,
	   			handles: ['n', 'e', 's', 'w', 'se', 'sw']
	   		},
	   		draggable: {
	   			enabled: true,
	   			handle: '.widget-header'	   
	   		},
	   		margin: [10,10]
	   })
	   .constant('MyCaseDiaryConst',{
		   chartHeight: 230,
		   chartWidth: 800
	   })
	   .constant('TopLocationsConst',{
		   zoomOptions: {
			   minZoom: 1.0,
			   zoom: 1.5
		   }
	   })
	   .constant('HotTopicsConst',{
		   	speed : 0.1,
			slower : 0.1,
			height : 230,
			timer : 5,
			fontMultiplier : 15,
			isheader : false,
			hoverStyle : {
				border : 'none',
				color : '#0b2e6f'
			},
			mouseOutStyle : {
				border : '',
				color : ''
			},
			colors: ["#3990CF", "#1DBF6A", "#E1C433", "#E61874", "#E58B19"],
			ballSize: 50,
			colorObj: {
				persons      : '#9C6CC4',
				companies     : '#50B0A2',
				locations    : '#AFA960',
				phrases 	  : '#C2776A',
				organisation: '#5C94CE',
				industries  : '#5C94CE',
				country     : '#61B45F'
			}
	   })
	   .constant('YourCaseMetricsConst',{
		   header : "YOUR CASE METRICS",
		   height : 400 
	   })
	   .constant('EntitiesInFocusConst',{
		    chartHeight: 300
	   })
	   .constant('TimeScaleBubbleConst',{
		   header:" MY ENTITIES ",
           height: '250',
           isheader:false,
           xticks:5,
           margin:{top: 20, right: 0, bottom: 30, left: 30}      
	   }).constant('alertsDataConst', {
		   alerts1:[
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "Ashmore Investment Management"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "PSA Energy Holdings SPC"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "PETROSAUDI�SAUDI ARABIA (RIYADH)"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "PETROSAUDI�VENEZUELA"
			  }
		   ],
		   alerts2:[
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "Ashmore Investment Management"
			  },
			  { 
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "Transaction screening potential index hit reverted"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "PSA Energy Holdings SPC"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "an incoming transfer of 300 M USD was identified",
			   "label": "Transaction Alert"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Changes in Board",
			   "label": "Yukos  Services Pvt Ltd"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Changes in Board",
			   "label": "PETROSAUDI�SAUDI ARABIA (RIYADH)"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Changes in Board",
			   "label": "PETROSAUDI�VENEZUELA"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Email from Compliance to the RM with request for information",
			   "label": "Event"
			  },
			  { 
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "Vesta Petrolium  Investment Limited"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Incoming Transfer of 300M USD was identified",
			   "label": "Petro Saudi joint venture"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Email from the RM to Compliance  saying there is a business rationale",
			   "label": "Event"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Joined the group",
			   "label": "John Brown Hyderocarbons Ltd"
			  },
			  {
			   "type": 'NOTIFICATION',
			   "description": "Added as partner",
			   "label": "Saudi Makamin Oil & Gas Services"
			  }
		   ]
	   });