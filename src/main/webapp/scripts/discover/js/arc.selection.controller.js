'use strict';
angular.module('ehubApp')
	   .controller('ArcSelectionController', arcSelectionController);

		arcSelectionController.$inject = [
			'$scope', 
			'$uibModal'
		];
		
		function arcSelectionController(
				$scope, 
				$uibModal) {         
		$scope.arcSelection = {
				openNetworkChartModal: openNetworkChartModal
		};
		/*
	     * @purpose: open captured widget modal
	     * @created: 3 feb 2018
	     * @params: widgetId(string)
	     * @return: no
	     * @author: Anil
	    */
		function openNetworkChartModal(){
			var networkChartModal = $uibModal.open({
				templateUrl: 'scripts/discover/modal/views/arcselection.networkChart.html',
				size: 'lg',
				windowClass: 'custom-modal capture-modal'
			});
			networkChartModal.result.then(function(){
				
			}, function(){
			});
		}
	
		
}