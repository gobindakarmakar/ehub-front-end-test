'use strict';
angular.module('ehubApp')
	   .controller('MyCaseDiaryController', myCaseDiaryController);

		myCaseDiaryController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'DiscoverApiService',
			'HostPathService',
			'DTOptionsBuilder',
			'DTColumnDefBuilder',
			'$location',
			'$uibModal',
			'EHUB_API',
			'EHUB_FE_API',
			'Flash',
			'DiscoverConst',
			'MyCaseDiaryConst',
			'$timeout'
		];
		
		function myCaseDiaryController(
				$scope, 
				$rootScope,
				$state,
				DiscoverApiService,
				HostPathService,
				DTOptionsBuilder,
				DTColumnDefBuilder,
				$location,
				$uibModal,
				EHUB_API,
				EHUB_FE_API,
				Flash,
				DiscoverConst,
				MyCaseDiaryConst,
				$timeout) {
		$scope.hyperLinksNewTab = $rootScope.hyperLinksNewTab;
		$scope.token = $rootScope.ehubObject.token;
		$scope.toggleGraph = false;
		$scope.cases = [];
		/* my case diary table options */
		$scope.dtOptions = DTOptionsBuilder.newOptions()
					        .withDisplayLength(10)
					        .withOption('bLengthChange', false)
					        .withOption("bPaginate", false)
					        .withOption("bFilter", false)
					        .withOption("bInfo" , false)
					        .withOption('responsive', true)
							.withOption("destroy", true);
		/* my case diary table sorting of columns */
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(2).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
//            DTColumnDefBuilder.newColumnDef(4).notSortable(),
            DTColumnDefBuilder.newColumnDef(5).notSortable(),
            DTColumnDefBuilder.newColumnDef(6).notSortable()
	    ];
		$scope.statusList = DiscoverConst.statusList;
		
		var AllMyCasesParams = {
	      token: $scope.token,
	      orderIn: "desc",
	      orderBy:"createdOn"
		};
		var companyImage = 'assets/images/menu_icons/co.png';
		/**
		 * @purpose: open modal for filter case
	     * @created: 14 Dec 2017
	     * @params: no
	     * @return: null
	     * @author: prasanthi
		 */
		var openCasefiltersModal;
	  	$scope.OpenfilterCaseDataModal =function() {  		
	  		
	  		openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             //backdrop: 'static',
				 windowClass: 'custom-modal comments-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return false;
		                },
		                statusList:function(){
		                	return [			                		
							    	{status:"Accepted", code:4},
							    	{status:"Pause", code:5},
							    	{status:"Resolved", code:8},
							    	{status:"Canceled", code:9}
						    	];
		                },
		                keysTofilter:function(){
		                	return false;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return $scope.FilteredKey;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return false;
		                },
		                CaseDiarySelections : function(){
		                	return [
		                		{"MCDtype" : $scope.type},
		                		{"MCDorderby" : $scope.orderby},
		                		{"MCDorderin" : $scope.orderin},
		                		{"MCDstatus" : $scope.status}
		                		];
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
			                return $scope.namesList;	
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return true;
		                }
		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {	
	  			if(response){
	  				handleFiltersApplied(response);
	  			}
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
	     };
	  	//-----------------------------------------------------------------------
	  	/**
	  	 * Function to apply filters
	  	 */
	  	var searchCaseKeyword,filtersApplied;
	  	function handleFiltersApplied(filersData){
	  		$scope.type = filersData.filterApplied.type;
	  		$scope.FilteredKey = filersData.filterApplied.priority;
	  		$scope.orderby = filersData.filterApplied.orderBy;
	  		$scope.orderin = filersData.filterApplied.orderIn;
	  		
	  		if(filersData.filterApplied.recordsPerPage){
	  			$scope.pageNum = 1;
	  			$scope.recordsPerPage = filersData.filterApplied.recordsPerPage;
	  		}
	  		
	  		if(filersData.filterApplied.status && filersData.filterApplied.status.length > 0 ){
	  			$scope.StatusOption = filersData.filterApplied.status[0];
	  		}
	  		else{
	  			$scope.StatusOption = 0;
	  		}
	  		filtersApplied = jQuery.extend(true,{}, filersData.filterApplied);
	  		$scope.FilteredKeyword = "";
	  		if(filersData.caseSearchKeyword){
	  			$scope.FilteredKeyword = filersData.caseSearchKeyword;
	  			searchCaseKeyword = filersData.caseSearchKeyword;
	  			getSearchMyCases(jQuery.extend(true,{}, filersData.filterApplied),filersData.caseSearchKeyword);
	  		}else if(filersData.filterApplied){
	  			searchCaseKeyword = undefined;
	  			AllMyCasesParams = jQuery.extend(true,{}, filersData.filterApplied);
	  			loadMyCasesParams =  jQuery.extend(true,{}, filersData.filterApplied);
	  			loadMyCasesParams.recordsPerPage= $scope.recordsPerPage;
	  			loadMyCasesParams.pageNumber= $scope.pageNum;	  			    
	  			getAllMyCases(filersData.filterApplied);
	  		}
	  	}
	  	function getSearchMyCases(filters,keyword){
	  		filters.keyword= keyword;
//	  		$scope.myCaseDiaryPreloader = true;	
	  		delete filters.token;
	  		if(!filters.status || filters.status.length==0){
	  			filters.status=[4,5,8,9];
	  		}
			DiscoverApiService.caseListFullTextSearch({token:$rootScope.ehubObject.token},filters).then(function(response) {
				handleGetAllMyCasesData(response,filters,keyword);
			});
	  	}
	  	function loadsearchAllMyCases(filters,keyword){
	  		filters.keyword= keyword;
	  		filters.recordsPerPage= $scope.recordsPerPage;
	  		filters.pageNumber= $scope.pageNum;
	  		$scope.myCaseDiaryPreloader = true; 
	  		delete filters.token;
	  		if(!filters.status || filters.status.length==0){
	  			filters.status=[4,5,8,9];
	  		}
			DiscoverApiService.caseListFullTextSearch({token:$rootScope.ehubObject.token},filters).then(function(response) {
				handleLoadMyCasesData(response);
		    });
	  	}
		//-----------------------------------------------------------------------
		/*
	     * @purpose: get all my cases
	     * @created: 18 sep 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		function getAllMyCases(){
			if(searchCaseKeyword){
				getSearchMyCases(jQuery.extend(true,{}, filtersApplied),searchCaseKeyword);
				return;
			}
			$scope.myCaseDiaryPreloader = true;	
			delete AllMyCasesParams.token;
			if(!AllMyCasesParams.status || AllMyCasesParams.status.length==0){
				AllMyCasesParams.status=[4,5,8,9];
	  		}
			DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},AllMyCasesParams).then(function(response) {
//				
				/* For Total Count */
				var loadAllCaseParam = {
					      token: $scope.token,
					      orderIn: "desc",
					      orderBy:"createdOn",
						  recordsPerPage: response.data.paginationInformation.totalResults,
						  pageNumber: 1
						};
				loadAllCaseParam.status=[4,5,8,9];
				DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},loadAllCaseParam).then(function(results) {
					$scope.resposeS = results;
					$scope.namesList = [];
					angular.forEach($scope.resposeS.data.result,function(d){
						$scope.namesList.push(d.name);
					});
					$scope.namesList = _.uniq($scope.namesList);
					handleGetAllMyCasesData(results);
				});
		    });
		}
		
		//-----------------------------------------------------------------------
		/*
	     * @purpose:to handle data of get all my cases
	     * @created: 18 sep 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		var toolTipData = [];
		function handleGetAllMyCasesData(response,filters,keyword){
			$scope.resposeS = response;
//			$scope.filterS = filters;
//			$scope.keywordS = keyword;
			$scope.myCaseDiaryPreloader = false;
//			$scope.$apply();
	    	var caseChartList = [];   
	    	var statusesInfo = DiscoverConst.statusesInfo;
	    	angular.forEach(response.data.result, function( itemData ) {
	    		caseChartList.push({
    				'x': itemData.caseId,
    				'dr': itemData.directRisk !== null ? parseInt((itemData.directRisk * 100).toFixed(2)) : 0,
    				'tr': itemData.transactionalRisk !== null ? parseInt((itemData.transactionalRisk * 100).toFixed(2)) : 0,
    				'ir': itemData.indirectRisk !== null ? parseInt((itemData.indirectRisk * 100).toFixed(2)) : 0
    			});
	    		var date = new Date(itemData.modifiedOn);
	    		date = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
	    		toolTipData.push({
    				'CaseId': itemData.caseId,
    				'Case Id': itemData.caseId,
    				'name': itemData.name,
    				'priority': itemData.priority,
    				//'health': itemData.health,
    				'last updated': date,
    				'cumulative risk': (1 - (1 - itemData.directRisk) * (1 - itemData.indirectRisk) * (1 - itemData.transactionalRisk))*100,
    				'status': statusesInfo[itemData.currentStatus - 1].status,
    				'dr': itemData.directRisk !== null ? parseInt((itemData.directRisk * 100).toFixed(2)) : 0,
    				'tr': itemData.transactionalRisk !== null ? parseInt((itemData.transactionalRisk * 100).toFixed(2)) : 0,
    				'ir': itemData.indirectRisk !== null ? parseInt((itemData.indirectRisk * 100).toFixed(2)) : 0
    			});
	    	});  

	    	var options = {
	    	        container: "#chartOptions",
	    	        header: "",
	    	        data: caseChartList,
	    	        height: MyCaseDiaryConst.chartHeight,
	    	        width: MyCaseDiaryConst.chartWidth,
	    	        xtext: " ",
	    	        ytext: "risk ratio",
	    	        headerOptions: false,
	    	        txt: toolTipData
	    	    };
	    	if(response.data.result.length > 0){
		    	$timeout(function(){
		    		$scope.maxoptions = options;
		    		groupedColumChart(options);
		    	}, 0);
	    	}else{
	    		$scope.maxoptions = {};
	    		angular.element('#chartOptions').html('<p class="no-data">No Data Found</p>');
	    	}
	    	if(filters || keyword){
	    		loadsearchAllMyCases(filters,keyword);
	    	}else{
	    		loadMyCases(loadMyCasesParams);
	    	}
		}
		getAllMyCases();
		
		$scope.pageNum = 1;
		$scope.recordsPerPage = DiscoverConst.recordsPerPage;
		var loadMyCasesParams = {
	      token: $scope.token,
	      orderIn: "desc",
	      orderBy:"createdOn",
		  recordsPerPage: $scope.recordsPerPage,
		  pageNumber: $scope.pageNum
		};
//		var uploadedFilesLength;
	  /*
	   * @purpose: load my cases per page 4
	   * @created: 18 sep 2017
	   * @params: loadMyCasesParams(object)
	   * @return: no
	   * @author: swathi
	  */ 
		function loadMyCases(loadMyCasesParams){
			if(searchCaseKeyword){
				loadsearchAllMyCases(jQuery.extend(true,{}, filtersApplied),searchCaseKeyword);
				return;
			}
			$scope.myCaseDiaryPreloader = true; 
			delete loadMyCasesParams.token;
			if(!loadMyCasesParams.status || loadMyCasesParams.status.length==0){
				loadMyCasesParams.status=[4,5,8,9];
	  		}
			DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},loadMyCasesParams).then(function(response) {
//				$scope.resposeS = response;
				handleLoadMyCasesData(response);
				
		    });
		}
		/*
		   * @purpose: apply scroll
		   * @created: 18 sep 2017
		   * @params: 
		   * @return: no
		   * @author: swathi
		  */ 
		var startInterval;
		function applyScroll(){
			clearInterval(startInterval);
			$('#myCaseDiaryItems').mCustomScrollbar({
					axis : "y",
					theme : "minimal"
				});
		}
		/*
		   * @purpose: handle data for load my cases per page 4
		   * @created: 18 sep 2017
		   * @params: loadMyCasesParams(object)
		   * @return: no
		   * @author: swathi
		  */ 
		 var casesCount = 0, index = 0, caseStatusesArray = [];
		 //$scope.statusName = [];
		function handleLoadMyCasesData(response){
			$scope.cases = [];
			$scope.caseDiaryDataForMyCaseDiarySelectOptions = [];
			caseStatusesArray = [];
			var respDataLength = response.data.result.length;
	    	if(response.data.result.length > 0){
	    		angular.forEach(response.data.result, function(v, k){
	    			v.selectedStatus = v.currentStatus;
					v.IR = v.indirectRisk;
					v.DR = v.directRisk;
					v.TR = v.transactionalRisk;
					v.cumulativeRisk = (1 - (1 - v.directRisk) * (1 - v.indirectRisk) * (1 - v.transactionalRisk))*100;
					if(v.type == 'Corporate-KYC'){
						v.imageBank = companyImage;
		    		}
					if(response.data.result.length - 1 == k){
						$scope.myCaseDiaryPreloader = false;
					}
					$scope.cases.push(v);

					var params = {
						'token':$scope.token,
						'caseId':v.caseId,
						'currentStatus':v.currentStatus
					};
					
					index++;
					getStatuses(params, index,respDataLength);

					//Giving name for current status
					
					/*angular.forEach($scope.statusList,function(val,key){
						if(v.currentStatus === val.code)
						{
							$scope.statusName.push(val.status);
							console.log($scope.statusName);
						}
					});*/
				});
	    	}else{
	    		$scope.myCaseDiaryPreloader = false;
	    	}
//	    	var widthResp = $("#myCaseDiaryGridster").width();
	    	var heightResp = document.getElementById("myCaseDiaryGridster").clientHeight ? document.getElementById("myCaseDiaryGridster").clientHeight : MyCaseDiaryConst.chartHeight;
	    	
	    	if(heightResp > 320){
	    		heightResp = 530;
	    	}else{
	    		heightResp = 198;
	    	}
	    	$("#myCaseDiaryItems").css("height", heightResp);
    		
    		if($scope.recordsPerPage > 4){
    			
  				startInterval = setInterval(function(){
  					applyScroll();
  				}, 100);
  			}
	    	
			$scope.casesLength = response.data.paginationInformation.totalResults;
			
		}
			
		/*
	     * @purpose: get status options for each Status drop down
	     * @created: 04 Jul 2018
	     * @params: params(object), index(integer)
	     * @author: swathi
	    */
	   function getStatuses(params, index,responseData){
		DiscoverApiService.caseDropDownBasedOnStatus(params).then(function(response){
			casesCount++;
			var obj = response.data;
			if(obj.length > 0){
				angular.forEach(obj, function(v){
					v.caseId = params.caseId;
					v.casesCount = index;
				});
			}else{
				obj = [{
					'caseId': params.caseId,
					'casesCount': index	
				}];
			}
			caseStatusesArray.push(obj);

			if(responseData == casesCount){
				caseStatusesArray.sort(function(a, b){
					 return a[0].casesCount - b[0].casesCount; 
				});
				$scope.caseDiaryDataForMyCaseDiarySelectOptions = caseStatusesArray;
			}
		});
	}


	   /*
	     * @purpose: on page change
	     * @created: 18 sep 2017
	     * @params: page(number)
	     * @return: no
	     * @author: swathi
	    */ 
	   
	    $scope.pageChanged = function (page) {
	        $scope.pageNum = page;
//	        var params = {
//        		token: $scope.token,
//        		orderIn: "desc",
//        		recordsPerPage: $scope.recordsPerPage,
//        		pageNumber: $scope.pageNum,
//      	      orderBy:"createdOn"
//    		};
	        loadMyCasesParams.pageNumber = $scope.pageNum;
	        loadMyCases(loadMyCasesParams);
	    };
		/*
	     * @purpose: navigate to act page with clicked seed
	     * @created: 18 sep 2017
	     * @params: seedId(number)
	     * @return: no
	     * @author: swathi
	    */ 
		$scope.caseSelect = function(seedId, name, caseData){
			if(name){
				var url = EHUB_FE_API + 'entity/#!/company/' + name +"?qId="+seedId;
				
				if($scope.hyperLinksNewTab){
					window.open(url, '_blank');	
				}else{
					window.open(url, '_self');	
				}
			}
			if(caseData){
				if(caseData.isLeadGeneration){
					var url, workflowType = caseData.subType;
					if(workflowType == "General Cluster"){
						window.localStorage.setItem("cardHolderType", "General Card Holder");
						window.leadGenerationHidesStories = false;
						url = EHUB_FE_API + "leadGeneration/#!/cluster";
					}
					else if(workflowType == "Dual Card Holders"){
					    window.localStorage.setItem("cardHolderType", workflowType);
					    url = EHUB_FE_API + "leadGeneration/#!/dualCardClusterPage";
					}
					else if(workflowType == "Predicting Dual Card Holders"){
						window.localStorage.setItem("cardHolderType", workflowType);
						window.leadGenerationHidesStories = true;
						url = EHUB_FE_API + "leadGeneration/#!/cluster";
					}
					else if(workflowType == "Early Adaptors"){
						url = EHUB_FE_API + "leadGeneration/#!/earlyAdopters";
					}
					window.open(url, '_self');
				}else{
					$location.path('/act/' + seedId );
				}
			}
		};
		/*
	     * @purpose:to load decision scoring explanation
	     * @created: 18 sep 2017
	     * @params: seedId(number)
	     * @return: no
	     * @author: swathi
	    */ 
		$scope.loadDecisionScoring = function(caseId,caseName,riskScoreData){
			 var modalInstance = $uibModal.open({
		            templateUrl: 'scripts/common/modal/views/entityRiskScore.modal.html',
		            controller: 'EntityRiskScoreModalController',
					windowClass: 'custom-modal entity-risk-score-modal',
					resolve:{
						overviewRisks: function(){
		            		return '';
		            	},
		            	caseID:function(){
		            		return caseId;
		            	},
		            	caseName:function(){
		            		return caseName;
		            	},
		            	riskScoreData:function(){
		            		if(riskScoreData){
		            			return 	JSON.parse(riskScoreData).latest.entityRiskModel;
		            		}else{
		            			return 	null;
		            		}
		            		
		            	}
		            }
						
		        });

		        modalInstance.result.then(function(){
		        }, function(){
		        });
		};

		/*
	      * @purpose: disable case status options
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */ 
			$scope.respDataArray = [];
			//$scope.disableStatus = function(code, currentStatus) {
			$scope.disableStatus = function(code, caseDataArray) {
			var ok = true;
			
			/*if(code === 7){
				var ok = false;
			}
	        if (currentStatus === 2) {
	        	if(code === 3) {
					ok = false;
				}
	        } else if (currentStatus === 3) {
	            if(code === 4) {
	                ok = false;
	            } else if(code === 5) {
	                ok = false;
	            } else if(code === 6) {
	                ok = false;
	            }
	        } else if (currentStatus === 5) {
	            if(code === 4) {
	                ok = false;
	            } else if(code === 6) {
	                ok = false;
	            }
	        } else if(currentStatus === 4) {
	            if(code === 5) {
	                ok = false;
	            } else if(code === 8) {
	                ok = false;
	            } else if(code === 9) {
	                ok = false;
	            }
			}*/
				$scope.respDataArray = caseDataArray;
				angular.forEach($scope.respDataArray,function(val){
					
				if(val.key !=null || val.name == 'Forward')
					{	if(val.key==code)
						{ok= false;}
					}
				});
	        
	        return ok;
		};
	  	/*
	      * @purpose: update case status 
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */ 
	    $scope.updateStatus = function(caseId, currentStatus, selectedStatus) {
	    	$scope.tempSeedId = caseId;
	    	$scope.tempTargetStatus = selectedStatus;
	    	$scope.tempCurrentStatus = currentStatus;
	    	var apiUrl = EHUB_API, params = {
    			caseId: caseId,
	            token: $scope.token
			};
			
			var paramsForAll = {
				'token' : $scope.token,
				'caseId' : caseId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus
			};
			if(selectedStatus === 5 || selectedStatus === 6 || selectedStatus === 8)
			{
				commentsModal();
			}
			else if(selectedStatus === 7)
			{
				apiUrl += "investigation/markAsFocused";
				DiscoverApiService.updateCaseStatus(apiUrl, params).then(function(response) {
					HostPathService.FlashSuccessMessage('SUCCESSFUL FOCUSED CASE STATUS', response.data.responseMessage);
					$state.go('discover', {}, {reload:true});
				}, function() {
						HostPathService.FlashErrorMessage('ERROR FOCUSED CASE STATUS', 'Could not add case to focus area!');
						getAllMyCases();
				});
			}
			else if(selectedStatus === null)
			{
				forwardCaseModal();
			}
			else
			{
				DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
					$state.go('discover', {}, {reload:true});
				}, function() {
				});
			}



	    	/*if (currentStatus === 5) {
	            if(selectedStatus === 4) {
	                 /*From Paused to Accepted*/
	            	//apiUrl += "investigation/changeCurrentStatusPauseToAccepted";
	            	 /*calling api for all condition in general with apiUrl and params set*/
	               /* DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function(response) {
	                	$state.go('discover', {}, {reload:true});
	                }, function() {
	                });

	            } else if(selectedStatus === 6) {
	                 /*From Paused to Rejected*/
	            	/*commentsModal();
	            }else if(selectedStatus === 7){
		       		 apiUrl += "investigation/markAsFocused";
		   	    	 DiscoverApiService.updateCaseStatus(apiUrl, params).then(function(response) {
		   	    		HostPathService.FlashSuccessMessage('SUCCESSFUL FOCUSED CASE STATUS', response.data.responseMessage);
		   	    		$state.go('discover', {}, {reload:true});
		   	         }, function() {
		   	        	 HostPathService.FlashErrorMessage('ERROR FOCUSED CASE STATUS', 'Could not add case to focus area!');
		   	        	 getAllMyCases();
		   	         });
				}
				else if(selectedStatus === null)
				{
					forwardCaseModal()
				}
	        } else if(currentStatus === 4) {
	            if(selectedStatus === 5) {
	                 /*From Accepted to Paused*/
	            	/*commentsModal();
	            } else if(selectedStatus === 8) {
	                 /*From Accepted to Resolved*/
	            	/*commentsModal();
	            } else if(selectedStatus === 9) {
	                 /*From Accepted to Cancelled*/
	            	//apiUrl += "investigation/changeCurrentStatusAcceptedToCanceled";
	            	/*params.statusComment = '';
	            	  /*calling api for all condition in general with apiUrl and params set*/
	                /*DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function(response) {
	                	$state.go('discover', {}, {reload:true});
	                }, function() {
	                });
	            }else if(selectedStatus === 7){
		       		 apiUrl += "investigation/markAsFocused";
		   	    	 DiscoverApiService.updateCaseStatus(apiUrl, params).then(function(response) {
		   	    		HostPathService.FlashSuccessMessage('SUCCESSFUL FOCUSED CASE STATUS', response.data.responseMessage);
		   	    		$state.go('discover', {}, {reload:true});
		   	         }, function() {
		   	        	HostPathService.FlashErrorMessage('ERROR FOCUSED CASE STATUS', 'Could not add case to focus area!');
		   	        	getAllMyCases();
		   	         });
				}
				else if(selectedStatus === null)
				{
					forwardCaseModal()
				}
	        }else{
	        	if(selectedStatus === 7){
		       		 apiUrl += "investigation/markAsFocused";
		   	    	 DiscoverApiService.updateCaseStatus(apiUrl, params).then(function(response) {
		   	    		HostPathService.FlashSuccessMessage('SUCCESSFUL FOCUSED CASE STATUS', response.data.responseMessage);
		   	    		$state.go('discover', {}, {reload:true});
		   	         }, function() {
		   	        	HostPathService.FlashErrorMessage('ERROR FOCUSED CASE STATUS', 'Could not add case to focus area!');
		   	        	getAllMyCases();
		   	         });
	        	}
			};*/

		};
		
		/*
	     * @purpose: open forward case modal
	     * @created: 07 oct 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
	   function forwardCaseModal() {
		   var openForwardCaseModal = $uibModal.open({
			   templateUrl: 'scripts/discover/modal/views/forward.case.modal.html',
			   scope: $scope,
			   backdrop: 'static',
			   windowClass: 'custom-modal forwardcase-modal'
		   });

		   openForwardCaseModal.result.then(function (response) {
				if(response == "close"){
					angular.forEach($scope.cases, function(v){
						v.selectedStatus = v.currentStatus;
					});
				}
		   }, function () {
		   });
		   $scope.forwardCaseModalInstance = openForwardCaseModal;
		   return openForwardCaseModal.result;
	   }
	   
	  
	  /*
	   * @purpose: close forward modal
	   * @created: 07 oct 2017
	   * @params: no
	   * @return: null
	   * @author: swathi
	  */
	  $scope.closeForward = function() {
		$scope.tempSeedId = null;
		$scope.data.statusFwdComments = '';
		$scope.data.newUserId = null;
		$scope.forwardCaseModalInstance.close("close");
	 };

	 $scope.userList = [];
		var userListParams = {
			token: $scope.token
	    };
	 /* Get List of Users */
		
	 DiscoverApiService.getUserList(userListParams).then(function(response){
		$scope.usersLength = response.data.paginationInformation.totalResults;
		var data = {
			token: $scope.token,
			pageNumber: 1,
			recordsPerPage: $scope.usersLength 
		};
		DiscoverApiService.getUserList(data).then(function(response){
			$scope.userList = response.data.result;
		},function(){
		});
	},function(){
	});
	 /*
	      * @purpose: on change user order by option 
	      * @created: 16 oct 2017
	      * @params: option(string)
	      * @return: no
	      * @author: swathi
	     */
		$scope.getUserOrderBy = function(option){
			if(option){
				var data = {
	 				token: $scope.token,
	 				pageNumber: 1,
	 				recordsPerPage: $scope.usersLength ,
	 				orderBy: option
		 		};
				DiscoverApiService.getUserList(data).then(function(response){
		 			$scope.userList = response.data.result;
		 		},function(){
			 	});
			}
		};
		$scope.forwardLoader = false;
		$scope.data = {
		   statusFwdComments : ' ',
		   newUserId: null
		};
	
	 	/*
	      * @purpose: forward case to another user incoming tray
	      * @created: 07 oct 2017
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */
		$scope.forward = function forward() {
			
		   $scope.forwardLoader = true;
		   var seedId = $scope.tempSeedId;
		   var newUserId = $scope.data.newUserId;
		   var statusComment = $scope.data.statusFwdComments == undefined ? '' :  $scope.data.statusFwdComments;
		   var params = {
				   "caseId": seedId,
				   "token": $scope.token,
				   "newUserId": newUserId,
				   "statusComment": statusComment
			   };
		   DiscoverApiService.forwardCase(params).then(function() {
			   $scope.forwardLoader = false;
			   $scope.closeForward();
			   HostPathService.FlashSuccessMessage('SUCCESSFUL FORWADING THE CASE', 'Successfully forwarded the case!');
			   $state.go('discover', {}, {reload:true});
		   }, function(error) {
			   $scope.forwardLoader = false;
			   $scope.closeForward();
			   HostPathService.FlashErrorMessage('ERROR FORWARDING THE CASE', error.responseMessage || 'Failed to forward the case!');
			   getAllMyCases();
		   });
	   };

		/*Common API call for all case statuses */
		/*$scope.loadAllCaseStatusesWithCommonApi = function(caseId,statusFrom,statusTo)
		{
			var params = {
				'token' : $scope.token,
				'caseId' : caseId,
				'statusFrom' : statusFrom,
				'statusTo' :statusTo
			};
			DiscoverApiService.commonApiCallForAllCaseStatuses(params).then(function(response){
				$scope.allCaseStusesData = response.data;
				console.log('From common API');
				console.log($scope.allCaseStusesData);
			});
		};*/

		$scope.commentsLoader = false;
		$scope.data = {
		   statusComments : ''
		};
	 	/*
	      * @purpose: close comments modal
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.closeComments = function(processStatus) {
	 		$scope.data.statusComments = '';
	        $scope.tempSeedId = null;
	        $scope.tempCurrentStatus = '';
	        $scope.tempTargetStatus = null;
	        $scope.modalInstance.close(processStatus);
	     };
	     
	     /*
	      * @purpose: open comments modal
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	    var openCommentsModal;
	  	function commentsModal() {
	         openCommentsModal = $uibModal.open({
	             templateUrl: 'scripts/discover/modal/views/comments.modal.html',
	             scope: $scope,
	             backdrop: 'static',
				 windowClass: 'custom-modal comments-modal',
				 keyboard: false
	         });

	         openCommentsModal.result.then(function (response) {
	        	 if(response == "failure"){
	        		 angular.forEach($scope.cases, function(v){
	     	    		v.selectedStatus = v.currentStatus;
     				 });
	        	 }
	         }, function () {
	         });
	         $scope.modalInstance = openCommentsModal;
	         return openCommentsModal.result;
	     }
	  	/*
	      * @purpose: change case status to pause
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.pause = function () {
	    	$scope.commentsLoader = true;
			var comments = $scope.data.statusComments;
			var seedId = $scope.tempSeedId;
	 		var currentStatus = $scope.tempCurrentStatus;
			var selectedStatus = $scope.tempTargetStatus;
	 		//var apiPath = '';
	 		
	 		var today = new Date();
	 		var month = String(today.getMonth() + 1);
	 		var day = String(today.getDate());
	 		var year = String(today.getFullYear());
	 		
	 		if (month.length < 2) {month = '0' + month;}
	 		if (day.length < 2) {day = '0' + day;}
	 		
			/*var params = {
	        	"caseId": $scope.tempSeedId,
	        	"token": $scope.token,
	        	"statusComment": comments,
	        	"notificationDate": day + "-" + month + "-" + year
			};*/

			var paramsForAll = {
				'token' : $scope.token,
				'caseId' : seedId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus,
				"statusComment": comments,
	        	"notificationDate": day + "-" + month + "-" + year
			};

			DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
				$scope.commentsLoader = false;
				$scope.closeComments("success");
			   HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
				$state.go('discover', {}, {reload:true});
			 }, function() {
				  $scope.commentsLoader = false;
				  $scope.closeComments("failure");
				  getAllMyCases();
				  HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
			});

				 
			
	 		/*if($scope.tempCurrentStatus == 4) {
 				apiPath = EHUB_API + "investigation/changeCurrentStatusAcceptedToPaused",
 				DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	             	$scope.commentsLoader = false;
	             	$scope.closeComments("success");
					HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
	             	$state.go('discover', {}, {reload:true});
	      	    }, function() {
	      	    	 $scope.commentsLoader = false;
	      	    	 $scope.closeComments("failure");
	      	    	 getAllMyCases();
	      	    	 HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
	             });
	 		} else {
	 			apiPath = EHUB_API + "investigation/changeCurrentStatusAcknowledgedToPaused";
	             DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	             	 $scope.commentsLoader = false;
	             	 $scope.closeComments("success");
	             	 HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
	             	 $state.go('discover', {}, {reload:true});
	     	    }, function() {
	     	    	 $scope.commentsLoader = false;
	     	    	 $scope.closeComments("failure");
	     	    	 getAllMyCases();
	     	    	 HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
     	    	 });
	 		}*/
	 	};
	 	/*
	      * @purpose: change case status to resolve
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	 	 $scope.resolve = function() {
	 		$scope.commentsLoader = true;
			var seedId = $scope.tempSeedId;
			var currentStatus = $scope.tempCurrentStatus;
			var selectedStatus = $scope.tempTargetStatus;
	 		var comments = $scope.data.statusComments;
	 		var isResolved = false;
			 


			var paramsForAll = {
				'token' : $scope.token,
				'caseId' : seedId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus,
				"statusComment": comments
			};

			DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
				$scope.commentsLoader = false;
				$scope.closeComments("success");
				isResolved = true;
				$state.go('discover', {}, {reload:true});
			   HostPathService.FlashSuccessMessage('CASE STATUS UPDATED TO RESOLVED', 'Successfully changed the case status to Resolved!');
			}, function() {
				$scope.commentsLoader = false;
				$scope.closeComments("failure");
				getAllMyCases();
				HostPathService.FlashErrorMessage('ERROR UPDATE CASE STATUS', 'Failed to changed the case status to Resolved!');
			});

			 /*var apiPath = EHUB_API + "investigation/changeCurrentStatusAcceptedToResolved",
				 
			 params = {
		         	"caseId": seedId,
		         	"token": $scope.token,
		         	"statusComment": comments
		        };
	 		DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	 			$scope.commentsLoader = false;
	 			$scope.closeComments("success");
	 	    	isResolved = true;
	 	    	$state.go('discover', {}, {reload:true});
				HostPathService.FlashSuccessMessage('CASE STATUS UPDATED TO RESOLVED', 'Successfully changed the case status to Resolved!');
	 	    }, function() {
	 	    	$scope.commentsLoader = false;
	 	    	$scope.closeComments("failure");
	 	    	getAllMyCases();
	 	    	HostPathService.FlashErrorMessage('ERROR UPDATE CASE STATUS', 'Failed to changed the case status to Resolved!');
	 	    });*/
	     };
	     /*
	      * @purpose: change case status to reject
	      * @created: 18 sep 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.reject = function () {
	  		$scope.commentsLoader = true;
	 		var seedId = $scope.tempSeedId;
	 		var currentStatus = $scope.tempCurrentStatus;
			var comments = $scope.data.statusComments;
			var selectedStatus = $scope.tempTargetStatus;
	 		
	 		var today = new Date();
	 		var month = String(today.getMonth() + 1);
	 		var day = String(today.getDate());
	 		var year = String(today.getFullYear());
	 		
	 		if (month.length < 2) {month = '0' + month;}
	 		if (day.length < 2) {day = '0' + day;}
	 		/*var apiPath = '',
	 			params = {
	 	        	"caseId": seedId,
	 	        	"token": $scope.token,
	 	        	"statusComment": comments,
	 	        	"notificationDate": day + "-" + month + "-" + year
				 };
			*/	 
			var paramsForAll = {
					'token' : $scope.token,
					'caseId' : seedId,
					'statusFrom' : currentStatus,
					'statusTo' :selectedStatus,
					"statusComment": comments,
	 	        	"notificationDate": day + "-" + month + "-" + year
				};

			DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
				$scope.commentsLoader = false;
				$scope.closeComments("success");
				$state.go('discover', {}, {reload:true});
				HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
			}, function() {
				$scope.commentsLoader = false;
				$scope.closeComments("failure");
				getAllMyCases();
				HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
			});
			
	 		/*if($scope.tempCurrentStatus != 6) {
	 			if($scope.tempCurrentStatus == 5) {
	 				apiPath = EHUB_API + "investigation/changeCurrentStatusPauseToRejected";
	 	            DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	 	            	$scope.commentsLoader = false;
	 	            	$scope.closeComments("success");
	 	            	$state.go('discover', {}, {reload:true});
	 	            	HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
	 	    	    }, function() {
	 	    	    	$scope.commentsLoader = false;
	 	    	    	$scope.closeComments("failure");
	 	    	    	getAllMyCases();
	 	    	    	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
	 	            });
	 			} else if ($scope.tempCurrentStatus == 3) {
	 				apiPath = EHUB_API + "investigation/changeCurrentStatusAcknowledgedToRejected";
	 	            DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	 	            	$scope.commentsLoader = false;
	 	            	$scope.closeComments("success");
	 	            	getAllMyCases();
	 	            	HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
 	 	    	    }, function() {
	 	    	    	$scope.commentsLoader = false;
	 	    	    	$scope.closeComments("failure");
	 	    	    	getAllMyCases();
	 	    	    	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
 	 	            });
	 			}else{
	 				$scope.commentsLoader = false;
	 				$scope.closeComments("failure");
	 				getAllMyCases();
	 			 	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
				 }
	 		}
	 		else{
	 			$scope.commentsLoader = false;
	 			$scope.closeComments("failure");
	 			getAllMyCases();
	 		 	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
			 }*/
	 	};
	 	
		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function() {
			var response = $scope.resposeS;
			if(response){
			$scope.myCaseDiaryPreloader = false;
			$timeout(function(){
				
				$scope.$apply();
			}, 0);
	    	var caseChartList = [];    	
	    	angular.forEach(response.data.result, function( itemData ) {
	    		caseChartList.push({
    				'x': itemData.caseId,
    				'dr': itemData.directRisk !== null ? parseInt((itemData.directRisk * 100).toFixed(2)) : 0,
    				'tr': itemData.transactionalRisk !== null ? parseInt((itemData.transactionalRisk * 100).toFixed(2)) : 0,
    				'ir': itemData.indirectRisk !== null ? parseInt((itemData.indirectRisk * 100).toFixed(2)) : 0
    			});
	    	}); 
	    	var widthResp = $("#myCaseDiaryGridster").width();
	    	var heightResp = document.getElementById("myCaseDiaryGridster").clientHeight ? document.getElementById("myCaseDiaryGridster").clientHeight : MyCaseDiaryConst.chartHeight;
	    	
	    	if(heightResp > 320){
	    		heightResp = 530;
	    	}else{
	    		heightResp = 198;
	    	}
	    	$("#myCaseDiaryItems").css("height", heightResp);
	    	var options = {
	    	        container: "#chartOptions",
	    	        header: "",
	    	        data: caseChartList,
	    	        height: heightResp,
	    	        width: widthResp,
	    	        xtext:" ",
	    	        ytext:"Values",
	    	        txt: toolTipData,
	    	        headerOptions: false
	    	    };
	    	if(response.data.result.length > 0){
		    	$timeout(function(){
		    		$scope.maxoptions=options;
		    		groupedColumChart(options);
		    	}, 0);
	    	}else{
	    		angular.element('#chartOptions').html('<p class="no-data">No Data Found</p>');
	    	}

		}	
		});
		

		/*
		 * @purpose: open fullscreen modal
		 * @created: 3 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
				
			}, function(){
			});
			
		};
		
		/*
		 * @purpose: Convert the the string into json and return the riskValue
		 * @created: 21st Feb 2019	
		 * @author: varsha
		 */		
		$scope.getRiskScoreValue = function(data){
			if(data){
				var overrallScore;
				if(Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score'])>1){
					 overrallScore = Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score'])/100;
				}else{
					overrallScore = Math.abs(JSON.parse(data).latest.entityRiskModel['overall-score']);
				}
				return overrallScore*100;
			}else{
				return 0;
			}
			
		};

}
