'use strict';
angular.module('ehubApp')
	   .controller('DiscoverDashboardController', discoverDashboardController);

		discoverDashboardController.$inject = [
			'$scope', 
			'$rootScope',
			'$uibModal'
		];
		
		function discoverDashboardController(
				$scope, 
				$rootScope,
				$uibModal) {         
		$scope.discoverDashboard = {
			contentMenu: 'discoverMainMenu',
			contentMenu0: 'discoverSubMenu0',
			contentMenu1: 'discoverSubMenu1',
			contentMenu2: 'discoverSubMenu2',
			contentMenu3: 'discoverSubMenu3',
			contentMenu4: 'discoverSubMenu4',
			contentMenu5: 'discoverSubMenu5'
		};
		/*
	     * @purpose: open captured widget modal
	     * @created: 14 sep 2017
	     * @params: widgetId(string)
	     * @return: no
	     * @author: swathi
	    */
		$scope.mainCaptureDiv = function(widgetId, toggleGraphTopLocation){
			var openWidgetCaptureModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
				controller: 'WidgetCaptureModalController',
				size: 'lg',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					widgetId: function(){
						return widgetId;
					},
					canvasnew:function(){
						return '';
					},
					topLocMapView: function(){
						return toggleGraphTopLocation;
					}
				}
			});
			openWidgetCaptureModal.result.then(function(){
				
			}, function(){
			});
		};
		
		$(document).ready(function() {
			/*Custom Scroll Bar*/
			$('#caseFocusWidget').mCustomScrollbar({
				axis : "y",
				theme : "minimal"
			});
		});
		if($rootScope.isLimeServerModalOpened){
			$scope.$emit("LimeServerOnCloseEvent",$rootScope.isLimeServerModalOpened);
		}
		
}