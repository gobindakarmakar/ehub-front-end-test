'use strict';
angular.module('ehubApp')
	.controller('GridsterController', gridsterController);

gridsterController.$inject = [
	'$scope',
	'GridsterConst'
];

function gridsterController(
	$scope,
	GridsterConst) {
	$scope.gridsterConfiguration = GridsterConst;
	$scope.toggleGraph = false;

	/*
	 * @purpose: Defining scope variables
	 * @author: Ankit
	 */

	$scope.customItems = [{
			size: {
				x: 4,
				y: 1
			},
			position: [0, 0]
		},
		{
			size: {
				x: 2,
				y: 3
			},
			position: [0, 4]
		},
		{
			size: {
				x: 0,
				y: 0
			}
		},
		{
			size: {
				x: 0,
				y: 0
			}
		},
		{
			size: {
				x: 0,
				y: 0
			}
		},
		{
			size: {
				x: 0,
				y: 0
			}
		},
		{
			size: {
				x: 0,
				y: 0
			}
		}
	];


}