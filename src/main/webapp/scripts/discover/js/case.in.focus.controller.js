'use strict';
angular.module('ehubApp')
	   .controller('CaseInFocusController', caseInFocusController);

		caseInFocusController.$inject = [
			'$scope', 
			'$rootScope',
			'DiscoverApiService',
			'DiscoverConst',
			'DTOptionsBuilder',
			'DTColumnDefBuilder',
			'HostPathService',
			'UploadFileService',
			'$uibModal',
			'EHUB_API',
			'Flash',
			'$state'
		];
		
		function caseInFocusController(
				$scope, 
				$rootScope,
				DiscoverApiService,
				DiscoverConst,
				DTOptionsBuilder,
				DTColumnDefBuilder,
				HostPathService,
				UploadFileService,
				$uibModal,
				EHUB_API,
				Flash,
				$state) {
    
		$scope.token = $rootScope.ehubObject.token;
		$scope.toggleGraphcaseFocus = false;
		$scope.data = [];
		$scope.dtOptions1 = DTOptionsBuilder.newOptions()
					        .withOption('bLengthChange', false)
					        .withOption("bPaginate", false)
					        .withOption("bFilter", false)
	        	.withOption('responsive', true);
		
		/* Case in focus table sorting of columns */
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(1).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).notSortable()
	    ];
		var companyImage = 'assets/images/menu_icons/co.png';
		
		$scope.pageNum = 1;
		var loadCaseFocusParams = {			
			orderIn: "DESC",
			status:[7],
			recordsPerPage: DiscoverConst.recordsPerPage,
			pageNumber: $scope.pageNum
		};
		
	
		loadCaseFocus(loadCaseFocusParams);
		
		 /*
		   * @purpose: load cases in focus area per page 4
		   * @created: 15 sep 2017
		   * @params: params(object)
		   * @return: no
		   * @author: swathi
		  */ 
		var casesCount = 0, index = 0, caseStatusesArray = [];
		function loadCaseFocus(params){
			$scope.caseFocusPreloader = true;
			$scope.caseDiaryDataForCaseInFocusSelectOptions = [], caseStatusesArray = [];/*jshint ignore:line */

	    	DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},params).then(function(response) {
		    	$scope.data = response.data;
		    	$scope.caseFocusLength = response.data.paginationInformation.totalResults;
		    	
		    	if($scope.data.result.length > 0){
		    		angular.forEach($scope.data.result, function(v, k){
		    			v.selectedStatus = v.currentStatus;
			    		v.cumulativeRisk = (1 - (1 - v.directRisk) * (1 - v.indirectRisk) * (1 - v.transactionalRisk))*100;
			    		if(v.type == 'Corporate-KYC'){
			    			v.imageBank = companyImage;
			    		}
			    		if($scope.data.result.length - 1 == k){
			    			$scope.caseFocusPreloader = false;
						}
						
						var params = {
							'token':$scope.token,
							'caseId':v.caseId,
							'currentStatus':v.currentStatus
						};
						index++;
						getStatuses(params, index);
			    	});
		    	}else{
	    			$scope.caseFocusPreloader = false; 
	    		}
		    }, function() {
				$scope.caseFocusPreloader = false;      	
			});
		}
		/*
	     * @purpose: get status options for each Status drop down
	     * @created: 04 Jul 2018
	     * @params: params(object), index(integer)
	     * @author: swathi
	    */
		function getStatuses(params, index){
			DiscoverApiService.caseDropDownBasedOnStatus(params).then(function(response){
				casesCount++;
				var obj = response.data;
				if(obj.length > 0){
					angular.forEach(obj, function(v){
						v.caseId = params.caseId;
						v.casesCount = index;
					});
				}else{
					obj = [{
						'caseId': params.caseId,
						'casesCount': index	
					}];
				}
				caseStatusesArray.push(obj);
				
				if($scope.data.result.length == casesCount){
					caseStatusesArray.sort(function(a, b){
						 return a[0].casesCount - b[0].casesCount; 
					});
					$scope.caseDiaryDataForCaseInFocusSelectOptions = caseStatusesArray;
				}
			});
		}
		
	   /*
	     * @purpose: on page change
	     * @created: 15 sep 2017
	     * @params: page(number)
	     * @return: no
	     * @author: swathi
	    */ 
	    $scope.pageChanged = function (page) {
	        $scope.pageNum = page;
	        loadCaseFocusParams.recordsPerPage =  DiscoverConst.recordsPerPage;
	        loadCaseFocusParams.pageNumber=$scope.pageNum;
	        loadCaseFocus(loadCaseFocusParams);
	        casesCount = 0, index = 0;/*jshint ignore:line */
	    };
	    
		//$scope.statusList = DiscoverConst.focusedStatusList;
		$scope.focusStatusList = DiscoverConst.focusedStatusList;
		/*
	      * @purpose: disable case status options
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */ 
		//$scope.disableStatus = function(code, currentStatus) {
		$scope.disableStatus = function(code, caseDataArray) {
			var ok = true;


			/*if(code === 7){
				var ok = false;
			}
	        if (currentStatus === 2) { // if current status of Case whose option selected from my case diary is submitted  then Only code with 3 option is enabled to move the case to particular status under code 3
	        	if(code === 3) {
					ok = false;
				}
	        } else if (currentStatus === 3) { // if current status of Case whose option selected from my case diary is Acknowledge (ie code 3)  then case can be moved into 3 option enabled  with code Number 4, 5, 6 
	            if(code === 4) {
	                ok = false;
	            } else if(code === 5) {
	                ok = false;
	            } else if(code === 6) {
	                ok = false;
	            }
	        } else if (currentStatus === 5) { // if current status of Case whose option selected from my case diary is paused(ie code 5)   then case can be moved into 2 option enabled  with code Number 4, 6 
	            if(code === 4) {
	                ok = false;
	            } else if(code === 6) {
	                ok = false;
	            }
	        } else if(currentStatus === 4) {  // if current status of Case whose option selected from my case diary is Accepted(ie code 4)   then case can be moved into 3 option enabled  with code Number 5, 8, 9 
	            if(code === 5) {
	                ok = false;
	            } else if(code === 8) {
	                ok = false;
	            } else if(code === 9) {
	                ok = false;
	            }
			}*/
			
			$scope.respDataArray = caseDataArray;
				angular.forEach($scope.respDataArray,function(val){
				
				if(val.key !=null || val.name == 'Forward')
					if(val.key==code)/*jshint ignore:line */
					ok= false;/*jshint ignore:line */
			});
	        return ok;
		};
	  	/*
	      * @purpose: update case status 
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */ 
	    $scope.updateStatus = function(caseId, currentStatus, selectedStatus) {
	    	$scope.tempSeedId = caseId;
	    	$scope.tempTargetStatus = selectedStatus;
	    	$scope.tempCurrentStatus = currentStatus;
			var apiUrl = EHUB_API, params = {/*jshint ignore:line */
    			caseId: caseId,
	            token: $scope.token
			};
			
			var paramsForAll = {
				'token' : $scope.token,
				'caseId' : caseId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus
			};
			
			if(selectedStatus === 5 || selectedStatus === 6 || selectedStatus === 8)
			{
				commentsModal();
			}
			else if(selectedStatus === 7)
			{
				DiscoverApiService.removeCaseFromFocused(params).then(function() {	
					HostPathService.FlashSuccessMessage('SUCCESSFUL UNFOCUSED CASE STATUS', 'Successfully removed case from focus area!');
					$state.go('discover', {}, {reload:true});
				}, function(){
					HostPathService.FlashErrorMessage('ERROR UNFOCUSED CASE STATUS', 'Could not remove case from focus area!');
				});
			}
			else if(selectedStatus === null)
			{
				forwardCaseModal();
			}
			else
			{
				DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
					$state.go('discover', {}, {reload:true});
				}, function() {
				});
			}
	    };
		
		/*
	     * @purpose: open forward case modal
	     * @created: 07 oct 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
		function forwardCaseModal() {
			var openForwardCaseModal = $uibModal.open({
				templateUrl: 'scripts/discover/modal/views/forward.case.modal.html',
				scope: $scope,
				backdrop: 'static',
				windowClass: 'custom-modal forwardcase-modal'
			});

			openForwardCaseModal.result.then(function (response) {
				
				if(response == "close"){
					angular.forEach($scope.data.result, function(v){
						v.selectedStatus = v.currentStatus;
					 });
				}
			}, function () {
			});
			$scope.forwardCaseModalInstance = openForwardCaseModal;
			return openForwardCaseModal.result;
		}
		
		$scope.forwardLoader = false;
		$scope.data = {
		   statusFwdComments : '',
		   newUserId: null
		};
	   /*
		* @purpose: close forward modal
		* @created: 07 oct 2017
		* @params: no
		* @return: null
		* @author: swathi
	   */
	   $scope.closeForward = function() {
		$scope.tempSeedId = null;
		$scope.data.statusFwdComments = '';
		$scope.data.newUserId = null;
		$scope.tempCurrentStatus = '';
	    $scope.tempTargetStatus = null;
		$scope.forwardCaseModalInstance.close("close");
	  };

    	 $scope.commentsLoader = false;
	 	 $scope.data = {
			 statusComments : ''
		  };
		  

		  $scope.userList = [];
		var userListParams = {
			token: $scope.token
	    };
		  /* Get List of Users */
		
		DiscoverApiService.getUserList(userListParams).then(function(response){
			$scope.usersLength = response.data.paginationInformation.totalResults;
			var data = {
				token: $scope.token,
				pageNumber: 1,
				recordsPerPage: $scope.usersLength 
			};
			DiscoverApiService.getUserList(data).then(function(response){
				$scope.userList = response.data.result;
			},function(){
			});
		},function(){
		});
		  /*
	      * @purpose: on change user order by option 
	      * @created: 16 oct 2017
	      * @params: option(string)
	      * @return: no
	      * @author: swathi
	     */
		$scope.getUserOrderBy = function(option){
			if(option){
				var data = {
	 				token: $scope.token,
	 				pageNumber: 1,
	 				recordsPerPage: $scope.usersLength ,
	 				orderBy: option
		 		};
				DiscoverApiService.getUserList(data).then(function(response){
		 			$scope.userList = response.data.result;
		 		},function(){
			 	});
			}
		};
		/*
	      * @purpose: forward case to another user incoming tray
	      * @created: 07 oct 2017
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */
		$scope.forward = function forward() {
			$scope.forwardLoader = true;
		   var seedId = $scope.tempSeedId;
		   var newUserId = $scope.data.newUserId;
		   var statusComment = $scope.data.statusFwdComments == undefined ? '' :  $scope.data.statusFwdComments;
		   var params = {
				   "caseId": seedId,
				   "token": $scope.token,
				   "newUserId": newUserId,
				   "statusComment": statusComment
			   };
		   DiscoverApiService.forwardCase(params).then(function() {
			   $scope.forwardLoader = false;
			   $scope.closeForward();
			   HostPathService.FlashSuccessMessage('SUCCESSFUL FORWADING THE CASE', 'Successfully forwarded the case!');
			   $state.go('discover', {}, {reload:true});
		   }, function(error) {
			   $scope.forwardLoader = false;
			   $scope.closeForward();
			   HostPathService.FlashErrorMessage('ERROR FORWARDING THE CASE', error.responseMessage || 'Failed to forward the case!');
			   loadCaseFocus(loadCaseFocusParams);
		   });
	   };
	 	/*
	      * @purpose: close comments modal
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	 	$scope.closeComments = function(processStatus) {
	 		$scope.data.statusComments = '';
	        $scope.tempSeedId = null;
	        $scope.tempCurrentStatus = '';
	        $scope.tempTargetStatus = null;
	        $scope.modalInstance.close(processStatus);
	     };
	     
	     /*
	      * @purpose: open comments modal
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     var openCommentsModal;
		  	function commentsModal() {
		         openCommentsModal = $uibModal.open({
		             templateUrl: 'scripts/discover/modal/views/comments.modal.html',
		             scope: $scope,
		             backdrop: 'static',
					 windowClass: 'custom-modal comments-modal',
					 keyboard: false
		         });

		         openCommentsModal.result.then(function (response) {
		        	 if(response == "failure"){
		        		 angular.forEach($scope.data.result, function(v){
		     	    		v.selectedStatus = v.currentStatus;
	     				 });
		        	 }
		         }, function () {
		         });
		         $scope.modalInstance = openCommentsModal;
		         return openCommentsModal.result;
		     }
	  	/*
	      * @purpose: change case status to pause
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
		  	$scope.pause = function () {
		    	$scope.commentsLoader = true;
		 		var comments = $scope.data.statusComments;
				 //var apiPath = '';
				var seedId = $scope.tempSeedId;
				var currentStatus = $scope.tempCurrentStatus;
				var selectedStatus = $scope.tempTargetStatus;
		 		
		 		var today = new Date();
		 		var month = String(today.getMonth() + 1);
		 		var day = String(today.getDate());
		 		var year = String(today.getFullYear());
		 		
		 		if (month.length < 2) {month = '0' + month;}
		 		if (day.length < 2) {day = '0' + day;}
		 		
				/*var params = {
		        	"caseId": $scope.tempSeedId,
		        	"token": $scope.token,
		        	"statusComment": comments,
		        	"notificationDate": day + "-" + month + "-" + year
				};*/

				var paramsForAll = {
					'token' : $scope.token,
					'caseId' : seedId,
					'statusFrom' : currentStatus,
					'statusTo' :selectedStatus,
					"statusComment": comments,
		        	"notificationDate": day + "-" + month + "-" + year
				};

				DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
					$scope.commentsLoader = false;
					$scope.closeComments("success");
				   HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
					$state.go('discover', {}, {reload:true});
				 }, function() {
					  $scope.commentsLoader = false;
					  $scope.closeComments("failure");
					  getAllMyCases();/*jshint ignore:line */
					  HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
				});

		 		/*if($scope.tempCurrentStatus == 4) {
	 				apiPath = EHUB_API + "investigation/changeCurrentStatusAcceptedToPaused",
	 				DiscoverApiService.updateCaseStatus(apiPath, params).then(function(response) {
		             	$scope.commentsLoader = false;
		             	$scope.closeComments("success");
						HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
		             	$state.go('discover', {}, {reload:true});
		      	    }, function() {
		      	    	 $scope.commentsLoader = false;
		      	    	 $scope.closeComments("failure");
		      	    	 loadCaseFocus(loadCaseFocusParams);
		      	    	 HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
		             });
		 		} else {
		 			apiPath = EHUB_API + "investigation/changeCurrentStatusAcknowledgedToPaused";
		             DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
		             	 $scope.commentsLoader = false;
		             	 $scope.closeComments("success");
		             	 HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
		             	 $state.go('discover', {}, {reload:true});
		     	    }, function() {
		     	    	 $scope.commentsLoader = false;
		     	    	 $scope.closeComments("failure");
		     	    	loadCaseFocus(loadCaseFocusParams);
		     	    	 HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
	     	    	 });
				 }*/
		 	};
	 	/*
	      * @purpose: change case status to resolve
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
		 	$scope.resolve = function() {
		 		$scope.commentsLoader = true;
				var seedId = $scope.tempSeedId;
				var currentStatus = $scope.tempCurrentStatus;
				var selectedStatus = $scope.tempTargetStatus;
		 		var comments = $scope.data.statusComments;
		 		var isResolved = false;
				 
				 var paramsForAll = {
					'token' : $scope.token,
					'caseId' : seedId,
					'statusFrom' : currentStatus,
					'statusTo' :selectedStatus,
					"statusComment": comments
				};
				
				DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
					$scope.commentsLoader = false;
					$scope.closeComments("success");
					isResolved = true;
					$state.go('discover', {}, {reload:true});
				   HostPathService.FlashSuccessMessage('CASE STATUS UPDATED TO RESOLVED', 'Successfully changed the case status to Resolved!');
				}, function() {
					$scope.commentsLoader = false;
					$scope.closeComments("failure");
					getAllMyCases();/*jshint ignore:line */
					HostPathService.FlashErrorMessage('ERROR UPDATE CASE STATUS', 'Failed to changed the case status to Resolved!');
				});
				
				/*var apiPath = EHUB_API + "investigation/changeCurrentStatusAcceptedToResolved",
			 		params = {
			         	"caseId": seedId,
			         	"token": $scope.token,
			         	"statusComment": comments
			        };
		 		DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
		 			$scope.commentsLoader = false;
		 			$scope.closeComments("success");
		 	    	isResolved = true;
		 	    	$state.go('discover', {}, {reload:true});
					HostPathService.FlashSuccessMessage('CASE STATUS UPDATED TO RESOLVED', 'Successfully changed the case status to Resolved!');
		 	    }, function() {
		 	    	$scope.commentsLoader = false;
		 	    	$scope.closeComments("failure");
		 	    	loadCaseFocus(loadCaseFocusParams)();
		 	    	HostPathService.FlashErrorMessage('ERROR UPDATE CASE STATUS', 'Failed to changed the case status to Resolved!');
		 	    });*/
		     };
	     /*
	      * @purpose: change case status to reject
	      * @created: 10 jan 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.reject = function () {
	  		$scope.commentsLoader = true;
	 		var seedId = $scope.tempSeedId;
			 var currentStatus = $scope.tempCurrentStatus;
			 var selectedStatus = $scope.tempTargetStatus;
	 		var comments = $scope.data.statusComments;
	 		
	 		var today = new Date();
	 		var month = String(today.getMonth() + 1);
	 		var day = String(today.getDate());
	 		var year = String(today.getFullYear());
	 		
	 		if (month.length < 2) {month = '0' + month;}
	 		if (day.length < 2) {day = '0' + day;}
			 

			 var paramsForAll = {
				'token' : $scope.token,
				'caseId' : seedId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus,
				"statusComment": comments,
	 	        "notificationDate": day + "-" + month + "-" + year
			};

			DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
				$scope.commentsLoader = false;
				$scope.closeComments("success");
				$state.go('discover', {}, {reload:true});
				HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
			}, function() {
				$scope.commentsLoader = false;
				$scope.closeComments("failure");
				getAllMyCases();/*jshint ignore:line */
				HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
			});
			/*var apiPath = '',
	 			params = {
	 	        	"caseId": seedId,
	 	        	"token": $scope.token,
	 	        	"statusComment": comments,
	 	        	"notificationDate": day + "-" + month + "-" + year
	 	        };
	 		if($scope.tempCurrentStatus != 6) {
	 			if($scope.tempCurrentStatus == 5) { //change case status from pause to reject
	 				apiPath = EHUB_API + "investigation/changeCurrentStatusPauseToRejected";
	 	            DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	 	            	$scope.commentsLoader = false;
	 	            	$scope.closeComments("success");
	 	            	$state.go('discover', {}, {reload:true});
	 	            	HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
	 	    	    }, function() {
	 	    	    	$scope.commentsLoader = false;
	 	    	    	$scope.closeComments("failure");
	 	    	    	loadCaseFocus(loadCaseFocusParams)();
	 	    	    	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
	 	            });
	 			} else if ($scope.tempCurrentStatus == 3) { //change case status from Acknowledged to reject
	 				apiPath = EHUB_API + "investigation/changeCurrentStatusAcknowledgedToRejected";
	 	            DiscoverApiService.updateCaseStatus(apiPath, params).then(function() {
	 	            	$scope.commentsLoader = false;
	 	            	$scope.closeComments("success");
	 	            	loadCaseFocus(loadCaseFocusParams)();
	 	            	HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
 	 	    	    }, function() {
	 	    	    	$scope.commentsLoader = false;
	 	    	    	$scope.closeComments("failure");
	 	    	    	loadCaseFocus(loadCaseFocusParams)();
	 	    	    	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
 	 	            });
	 			}else{
	 				$scope.commentsLoader = false;
	 				$scope.closeComments("failure");
	 				loadCaseFocus(loadCaseFocusParams)();
	 			 	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
		 	    }
	 		}
	 		else{
	 			$scope.commentsLoader = false;
	 			$scope.closeComments("failure");
	 			loadCaseFocus(loadCaseFocusParams)();
	 		 	HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
			 }*/
	 	};
}