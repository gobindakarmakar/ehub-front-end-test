'use strict';
angular.module('ehubApp')
	.controller('EntitiesInFocusController', entitiesInFocusController);

entitiesInFocusController.$inject = [
	'$scope',
	'$rootScope',
	'DiscoverApiService',
	'DiscoverService',
	'EntitiesInFocusConst',
	'$uibModal'
];

function entitiesInFocusController(
	$scope,
	$rootScope,
	DiscoverApiService,
	DiscoverService,
	EntitiesInFocusConst,
	$uibModal) {

	$scope.token = $rootScope.ehubObject.token;
	$scope.toggleGraphdiscoverEntitiesInFocus = true;
	$scope.discoverEntitiesInFocusDatas = [];
	$scope.discoverEntitiesInFocusPreloader = true;
	$scope.responsiveData = {};
	var params = {
		token: $scope.token
	};
	var count = 40;
	var data = {
		'status': [4, 5, 8, 9]
	};
	var totalDataLength = 0;
	var actualinFocusData;
	var bubbleData = {};
	$scope.AllNames = [];
	var resultedArray = [];
	/*
	 * @purpose: open filter modal
	 * @created: 15 Dec 2017		
	 * @author: prasanthi
	 */
	var openFilterModalForEntitiesInFocusChart;
	var isApiLoaded = false;
	$scope.openFilterModalForEntitiesInFocusChart = function () {
		openFilterModalForEntitiesInFocusChart = $uibModal.open({
			templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
			scope: $scope,
			controller: 'filterCaseModalController',
			// backdrop: 'static',
			windowClass: 'custom-modal',
			resolve: {
				isOnlyKeyword: function () {
					return true;
				},
				statusList: function () {
					return false;
				},
				keysTofilter: function () {
					return $scope.keysTofilter;
				},
				FilteredKeyword: function () {
					return $scope.FilteredKeyword;
				},
				FilteredKey: function () {
					return $scope.FilteredKey;
				},
				RiskRatio: function () {
					return false;
				},
				onlySearch: function () {
					return false;
				},
				CaseDiarySelections: function () {
					return false;
				},
				StatusOption: function () {
					return $scope.StatusOption;
				},
				AllNames: function () {
					return $scope.AllNames;
				},
				isdateRequired: function () {
					return false;
				},
				itemsPerPage: function () {
					return false;
				}
			}
		});

		openFilterModalForEntitiesInFocusChart.result.then(function (response) {
			if (response) {
				handleFiltersApplied(response);
			}
		}, function () {});
		$scope.modalInstance = openFilterModalForEntitiesInFocusChart;
		return openFilterModalForEntitiesInFocusChart.result;
	};

	DiscoverApiService.getAllCaseSummaryByStatusList(params, data, count).then(function (response) {
		$scope.discoverEntitiesInFocusPreloader = false;
		resultedArray = response.data;
		delete resultedArray.caseId;
		if (resultedArray && response.data.status == undefined) {
			isApiLoaded = true;
			var actData = handleBubbleData(resultedArray);
			if (actData.children.length > 0) {
				angular.forEach(actData.children, function (d) {
					if (d.children.length > 0) {
						angular.forEach(d.children, function (key) {
							$scope.AllNames.push(key.name);
						});
					}
				});
				$scope.AllNames = _.uniq($scope.AllNames);
			}
			actualinFocusData = jQuery.extend(true, {}, resultedArray);
			$scope.keysTofilter = d3.keys(resultedArray);
			$scope.responsiveData = resultedArray;
			handleDataAndPlotChart(resultedArray);
		} else {
			angular.element('#discoverEntitiesInFocusBubbleChart').html('<p class="no-data">No Data Found</p>');
		}
	}, function (error) {
		$scope.discoverEntitiesInFocusPreloader = false;
		if (error.responseMessage) {
			angular.element('#discoverEntitiesInFocusBubbleChart').html('<p class="no-data">' + error.responseMessage + '</p>');
		}
	});
	/*
	 * @purpose: handle data and plot chart
	 * @created: 15 Dec 2017
	 * @params: response 	 
	 * @author: prasanthi
	 */
	
	function handleDataAndPlotChart(responseData) {
		$scope.discoverEntitiesInFocusDatas = [];
		var keys = Object.keys(responseData);
		var len = keys.length;
		totalDataLength = len;
		if (len > 0) {
			var heightL = document.getElementById('discoverEntitiesInFocusdiv').clientHeight ? document.getElementById('discoverEntitiesInFocusdiv').clientHeight : EntitiesInFocusConst.chartHeight;
			var options = {
				container: '#discoverEntitiesInFocusBubbleChart',
				header: "ENTITY IN FOCUS",
				uri: responseData,
				//		            height: EntitiesInFocusConst.chartHeight,
				height: heightL,
				isheader: false
			};
			bubbleData = handleBubbleData(responseData);
			options.data = bubbleData;
			angular.forEach(bubbleData.children, function (groupData) {
				angular.forEach(groupData.children, function (obj) {
					$scope.discoverEntitiesInFocusDatas.push({
						name: obj.name,
						size: obj.size,
						group: groupData.name
					});
				});
			});
			$scope.maxoptions = options;
			DiscoverService.setEntitiesInFocusBubblesChart(options);


			$(document).ready(function () {
				/*Custom Scroll Bar*/
				$('#entitiesInFocusWidget').mCustomScrollbar({
					axis: "y",
					theme: "minimal"
				});
			});

		} else {
			angular.element('#discoverEntitiesInFocusBubbleChart').html('<p class="no-data">No Data Found</p>');
		}
	}

	/*
	 * @purpose: to handle data according to format of Bubble Chart
	 * @created: 16 sep 2017
	 * @params: data(object)
	 * @return: finalData(object)
	 * @author: swathi
	 */
	function handleBubbleData(data) {
		var childrenData = [];
		angular.forEach(data, function (value, key) {
			var groupData = {
				name: key,
				children: []
			};
			angular.forEach(value, function (objSize, objName) {
				groupData.children.push({
					name: objName,
					size: objSize
				});
			});
			childrenData.push(groupData);
		});
		var finalData = {
			"children": childrenData
		};
		return finalData;
	}



	/*
	 * @purpose: handle/filter data (Click on Filter icon available on the click of three dots availble in right side of Entities in focus Div)
	 * @created: 15 Dec 2017
	 * @params: response of modal(object)		 
	 * @author: prasanthi
	 */
	function handleFiltersApplied(resp) {
		var filteredData = {};
		$scope.FilteredKeyword = '';
		var actualData = jQuery.extend(true, {}, actualinFocusData);

		if (resp.filterApplied.entity) {
			var keys = [resp.filterApplied.entity];
			$scope.FilteredKey = resp.filterApplied.entity;
		} else {
			var keys = d3.keys(actualData);
			$scope.FilteredKey = "ALL";
		}
		angular.forEach(keys, function (d) {
			var innerKeys = d3.keys(actualData[d]);
			var filtered = {};
			if (!resp.caseSearchKeyword) {
				filtered = actualData[d];
			} else {
				$.grep(innerKeys, function (v, i) {

					if (v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1) {
						filtered[v] = i;
					}
				});
				$scope.FilteredKeyword = resp.caseSearchKeyword;
			}
			if (d3.keys(filtered).length > 0) {
				filteredData[d] = filtered;
			}
		});
		$scope.responsiveData = filteredData;

		if (Object.keys(filteredData).length == 0) {
			$scope.maxoptions = {};
		}
		handleDataAndPlotChart(filteredData);

	}
	/* on resize of Div */
	$scope.$on('gridster-draggable-changed', function () {
		var heightResp = document.getElementById('discoverEntitiesInFocusdiv').clientHeight ? document.getElementById('discoverEntitiesInFocusdiv').clientHeight : 300;
		$("#discoverEntitiesInFocusdiv").find("tbody").css("height", heightResp);
		if (totalDataLength != 0 && isApiLoaded == true) {
			handleDataAndPlotChart($scope.responsiveData);
		}
	});

/* To resize charts plotted inside Div */
	$scope.resizeChart = function () {
		$scope.toggleGraphdiscoverEntitiesInFocus = true;
		if (totalDataLength != 0 && isApiLoaded == true) {
			handleDataAndPlotChart($scope.responsiveData);
		}
	};

	/*
	 * @purpose: open fullscreen modal
	 * @created: 3 Jan 2018		
	 * @author: varsha
	 */
	$scope.maximizeDiv = function (Chart) {
		var maximizeModal = $uibModal.open({
			templateUrl: 'scripts/common/modal/views/maximize.modal.html',
			controller: 'MaximizeModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal capture-modal',
			resolve: {
				Id: function () {
					return Chart;
				},
				ChartOptions: function () {
					return $scope.maxoptions;
				}
			}
		});
		maximizeModal.result.then(function () {

		}, function () {});

	};
}