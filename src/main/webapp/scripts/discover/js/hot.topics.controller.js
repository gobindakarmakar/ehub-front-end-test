'use strict';
angular.module('ehubApp')
	.controller('HotTopicsController', hotTopicsController);

hotTopicsController.$inject = [
	'$scope',
	'$rootScope',
	'DiscoverApiService',
	'DiscoverService',
	'EHUB_API',
	'HotTopicsConst',
	'$uibModal'
];

function hotTopicsController(
	$scope,
	$rootScope,
	DiscoverApiService,
	DiscoverService,
	EHUB_API,
	HotTopicsConst,
	$uibModal) {
	$scope.toggleGraphHotTopics = true;
	$scope.hotTopicsDatas = [];
	$scope.maxoptions = {};
	$scope.token = $rootScope.ehubObject.token;
	$scope.hotTopicsPreloader = true;
	var actualHotTopicsData;
	var totalDataLength;
	var params = {
		token: $scope.token
	};
	var count = 40;
	var data = {
		'status': [4, 5, 8, 9]
	};
	var isApiLoaded = false;
	var options = {
		container: "#tagcloud",
		header: "HOT TOPICS",
		uri: EHUB_API + 'workflow/es/cases/summary/' + count + '?token=' + $scope.token,
		speed: HotTopicsConst.speed,
		slower: HotTopicsConst.slower,
		height: HotTopicsConst.height,
		timer: HotTopicsConst.timer,
		fontMultiplier: HotTopicsConst.fontMultiplier,
		isheader: HotTopicsConst.isheader,
		hoverStyle: HotTopicsConst.hoverStyle,
		mouseOutStyle: HotTopicsConst.mouseOutStyle
	};
	var resultedArray = [];
	DiscoverApiService.getAllCaseSummaryByStatusList(params, data, count).then(function (response) {
		$scope.AllNames = [];
		resultedArray = response.data;
		if (resultedArray && response.data.status == undefined) {
			isApiLoaded = true;
			actualHotTopicsData = resultedArray;
			$scope.keysTofilter = d3.keys(resultedArray);
			var finalData = handleTagCloudData(resultedArray);
			options.data = finalData[0];
			options.data1 = finalData[1];
			$scope.hotTopicsDatas = finalData[1];
			if ($scope.hotTopicsDatas.length != 0) {
				$('#hotTopicsWidget').mCustomScrollbar({
					axis: "y",
					theme: "minimal"
				});
			}
			if (totalDataLength > 0) {
				angular.forEach($scope.hotTopicsDatas, function (d) {
					$scope.AllNames.push(d.name);
				});
				$scope.AllNames = _.uniq($scope.AllNames);
				$scope.maxoptions = options;
				DiscoverService.setHotTopicsCloudChart(options, 'discover');
			} else {
				angular.element('#tagcloud').html('<p class="no-data">No Data Found</p>');
			}
		} else {
			angular.element('#tagcloud').html('<p class="no-data">No Data Found</p>');
		}
		$scope.hotTopicsPreloader = false;
	}, function (error) {
		$scope.hotTopicsPreloader = false;
		if (error.responseMessage) {
			angular.element('#tagcloud').html('<p class="no-data">' + error.responseMessage + '</p>');
		}
	});
	/*
	 * @purpose: handle TagCloudData
	 * @created: 15 sep 2017
	 * @params: response(object)
	 * @returns: newData(object), newDataArray(object)
	 * @author: swathi
	 */
	function handleTagCloudData(response) {
		totalDataLength = 0;
		var newData = {};

		var newDataArray = [];
		angular.forEach(response, function (objVal, objKey) {
			var j = 0;
			var keys = Object.keys(objVal);
			var len = keys.length;
			totalDataLength = totalDataLength + len;
			angular.forEach(objVal, function (val, key) {
				if (j < 15) {
					newDataArray.push({
						name: key,
						count: val,
						group: objKey
					});
					j++;
					newData[key] = val;
				}
			});
		});
		return [newData, newDataArray];
	}
	/*
	 * @purpose: open filter modal
	 * @created: 15 Dec 2017		
	 * @author: prasanthi
	 */
	var openCasefiltersModal;
	$scope.openFilterModalForChart = function () {
		openCasefiltersModal = $uibModal.open({
			templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
			scope: $scope,
			controller: 'filterCaseModalController',
			//backdrop: 'static',
			windowClass: 'custom-modal',
			resolve: {
				isOnlyKeyword: function () {
					return true;
				},
				statusList: function () {
					return false;
				},
				keysTofilter: function () {
					return $scope.keysTofilter;
				},
				FilteredKeyword: function () {
					return $scope.FilteredKeyword;
				},
				FilteredKey: function () {
					return $scope.FilteredKey;
				},
				RiskRatio: function () {
					return false;
				},
				onlySearch: function () {
					return false;
				},
				CaseDiarySelections: function () {
					return false;
				},
				StatusOption: function () {
					return $scope.StatusOption;
				},
				AllNames: function () {
					return $scope.AllNames;
				},
				isdateRequired: function () {
					return false;
				},
				itemsPerPage: function () {
					return false;
				}
			}
		});

		openCasefiltersModal.result.then(function (response) {
			if (response) {
				handleFiltersApplied(response);
			}
		}, function () {});
		$scope.modalInstance = openCasefiltersModal;
		return openCasefiltersModal.result;
	};

	/*
	 * @purpose: handle/filter data
	 * @created: 15 Dec 2017
	 *  @params: response of modal(object)		 
	 * @author: prasanthi
	 */
	function handleFiltersApplied(resp) {

		var filteredData = {};
		$scope.FilteredKeyword = "";
		var actualData = jQuery.extend(true, {}, actualHotTopicsData);

		if (resp.filterApplied.entity) {
			var keys = [resp.filterApplied.entity];
			$scope.FilteredKey = resp.filterApplied.entity;
		} else {
			var keys = d3.keys(actualData);
			$scope.FilteredKey = "ALL";
		}
		angular.forEach(keys, function (d) {
			var innerKeys = d3.keys(actualData[d]);
			var filtered = {};
			if (!resp.caseSearchKeyword) {
				filtered = actualData[d];
			} else {
				$.grep(innerKeys, function (v, i) {

					if (v.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1) {
						filtered[v] = i;
					}
				});
				$scope.FilteredKeyword = resp.caseSearchKeyword;
			}
			filteredData[d] = filtered;
		});
		$(options.container).empty();
		var finalData = handleTagCloudData(filteredData);
		options.data = finalData[0];
		options.data1 = finalData[1];
		$scope.hotTopicsDatas = finalData[1];

		if (totalDataLength > 0 && Object.keys(filteredData).length > 0) {
			DiscoverService.setHotTopicsCloudChart(options, 'discover');
			$scope.maxoptions = options;
		} else {
			angular.element('#tagcloud').html('<p class="no-data">No Data Found</p>');
			$scope.maxoptions = {};
		}
	}

	/* on resize of Div */
	$scope.$on('gridster-draggable-changed', function () {
		if (totalDataLength) {
			if (totalDataLength > 0 && isApiLoaded == true) {
				DiscoverService.setHotTopicsCloudChart($scope.maxoptions);
			} else {
				angular.element('#tagcloud').html('<p class="no-data">Data Not Found</p>');
			}
		}
		var height = document.getElementById('hottopicsdiv').clientHeight ? document.getElementById('hottopicsdiv').clientHeight : 300;
		$("#hottopicsdiv").find("tbody").css("height", height);
	});
	/*
	 * @purpose: open fullscreen modal
	 * @created: 3 Jan 2018		
	 * @author: varsha
	 */
	$scope.maximizeDiv = function (Chart) {
		var maximizeModal = $uibModal.open({
			templateUrl: 'scripts/common/modal/views/maximize.modal.html',
			controller: 'MaximizeModalController',
			size: 'lg',
			backdrop: 'static',
			windowClass: 'custom-modal capture-modal',
			resolve: {
				Id: function () {
					return Chart;
				},
				ChartOptions: function () {
					return $scope.maxoptions;
				}
			}
		});
		maximizeModal.result.then(function () {

		}, function () {});

	};

	$scope.resizeHotTopics = function () {
		$scope.toggleGraphHotTopics = true;
		if (totalDataLength) {
			if (totalDataLength > 0 && isApiLoaded == true) {
				DiscoverService.setHotTopicsCloudChart($scope.maxoptions);
			} else {
				angular.element('#tagcloud').html('<p class="no-data">Data Not Found</p>');
			}
		}
	};
}