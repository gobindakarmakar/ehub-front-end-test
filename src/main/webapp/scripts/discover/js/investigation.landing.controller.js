'use strict';
angular.module('ehubApp')
       .controller('InvestigationLandingController', investigationLandingController);

        investigationLandingController.$inject = [
            '$scope', 
            '$rootScope',
            '$state',
            'ActRiskRatioConst',
            'InvestigationLandingService',
            'HostPathService',
            '$stateParams',
            'UploadFileService',
            'ActApiService',
            '$timeout',
            'TransactionAnalysisApiService',
            '$uibModal'
        ];
        
        function investigationLandingController(
                $scope, 
                $rootScope,
                $state,
                ActRiskRatioConst,
                InvestigationLandingService,
                HostPathService,
                $stateParams,
                UploadFileService,
                ActApiService,
                $timeout,
                TransactionAnalysisApiService,
                $uibModal) {
            $scope.imgPath = "assets/images/dummy-logo.png";
            $scope.entityName ="PetroSaudi international Ltd";
            $scope.scenario ="LARGE AMOUNT INTERNATIONAL TRANSFER TO OFFSHORE JURISDICTION";
            $scope.scenarioDate ="05/01/2010";
            $scope.transactionDetails=[];
            var taxanomyData;
            var journeyAndDecisionData;
            var clustersData;
            var CompareData;
            var transactionPetro=[{
                "custom-text2": "FE-CC-84-CE",
                "date-bene": "2010-01-05T00:00:00.000Z",
                "currency-bene": "USD",
                "date": "2010-01-05T00:00:00.000Z",
                "party-identifier": "C0-12-4A-F6",
                "normalized-name-bene": "PetroSaudi",
                "name-org": "1MDB",
                "normalized-name-org": "1MDB",
                "currency-org": "USD",
                "country-org": " Malaysia",
                "tx-id-bene": "22536449981",
                "tx-id-org": "22536449981",
                "data-source": "DEP",
                "channel": "Inernational Transfer",
                "type": "7754",
                "debit-credit": "C",
                "name-bene": "PetroSaudi",
                "currency": "USD",
                "product-type": "EFT-OTHER",
                "id": "22653121283",
                "amount-activity-org": "1000000000",
                "amount-activity-bene": "1000000000",
                "country-bene": " Seychelles",
                "date-org": "2010-01-05T00:00:00.000Z",
                "custom-text4": "C0-99-6E-F3",
                "amount-base": 1000000000,
                "amount-activity": 1000000000
            },{
                "custom-text2": "FE-CC-84-CE",
                "date-bene": "2010-02-05T00:00:00.000Z",
                "currency-bene": "USD",
                "date": "2010-02-05T00:00:00.000Z",
                "party-identifier": "C0-12-4A-F6",
                "normalized-name-bene": "Good Star",
                "name-org": "PetroSaudi",
                "normalized-name-org": "PetroSaudi",
                "currency-org": "USD",
                "country-org": " Seychelles",
                "tx-id-bene": "22536449981",
                "tx-id-org": "22536449981",
                "data-source": "DEP",
                "channel": "Wired Transaction",
                "type": "7754",
                "debit-credit": "D",
                "name-bene": "Good Star",
                "currency": "USD",
                "product-type": "EFT-OTHER",
                "id": "22653121284",
                "amount-activity-org": "700000000",
                "amount-activity-bene": "700000000",
                "country-bene": " Seychelles",
                "date-org": "2010-02-05T00:00:00.000Z",
                "custom-text4": "C0-99-6E-F3",
                "amount-base": 700000000,
                "amount-activity": 700000000
            },{
                "custom-text2": "FE-CC-84-CE",
                "date-bene": "2010-12-12T00:00:00.000Z",
                "currency-bene": "USD",
                "date": "2010-12-12T00:00:00.000Z",
                "party-identifier": "C0-12-4A-F6",
                "normalized-name-bene": "Totality LTD",
                "name-org": "Good Star",
                "normalized-name-org": "Good Star",
                "currency-org": "USD",
                "country-org": " Seychelles",
                "tx-id-bene": "22536449981",
                "tx-id-org": "22536449981",
                "data-source": "DEP",
                "channel": "Wired Transaction",
                "type": "7754",
                "debit-credit": "D",
                "name-bene": "Totality LTD",
                "currency": "USD",
                "product-type": "EFT-OTHER",
                "id": "22653121285",
                "amount-activity-org": "5000000",
                "amount-activity-bene": "5000000",
                "country-bene": " Seychelles",
                "date-org": "2010-12-12T00:00:00.000Z",
                "custom-text4": "C0-99-6E-F3",
                "amount-base": 5000000,
                "amount-activity": 5000000
            },{
                "custom-text2": "FE-CC-84-CE",
                "date-bene": "2010-12-13T00:00:00.000Z",
                "currency-bene": "USD",
                "date": "2010-12-13T00:00:00.000Z",
                "party-identifier": "C0-12-4A-F6",
                "normalized-name-bene": "Jasmine LOO Ai Swan",
                "name-org": "Good Star",
                "normalized-name-org": "Good Star",
                "currency-org": "USD",
                "country-org": " Seychelles",
                "tx-id-bene": "22536449981",
                "tx-id-org": "22536449981",
                "data-source": "DEP",
                "channel": "Wired Transaction",
                "type": "7754",
                "debit-credit": "D",
                "name-bene": "Jasmine LOO Ai Swan",
                "currency": "USD",
                "product-type": "EFT-OTHER",
                "id": "22653121286",
                "amount-activity-org": "5000000",
                "amount-activity-bene": "5000000",
                "country-bene": " Seychelles",
                "date-org": "2010-12-13T00:00:00.000Z",
                "custom-text4": "C0-99-6E-F3",
                "amount-base": 5000000,
                "amount-activity": 5000000
            }];
            $scope.InvestConsole = true;
            //code for number formatting
            Number.prototype.formatAmt = function(decPlaces, thouSeparator, decSeparator) {
                var n = this,
                decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
                decSeparator = decSeparator == undefined ? "." : decSeparator,
                thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
                sign = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
                return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
            };

            function getFormattedAmount(amt) {
                var formattedAmt = amt;
                return (formattedAmt ? Number(formattedAmt).formatAmt() : "");
            }
                //code for number formatting ends here
            /**
             * Function to load alert details
             */
            $scope.getAlertDetails =function(queryString){
                if(queryString.toLowerCase().indexOf("petrosaudi")>-1 ){
                    $scope.scenario ="LARGE AMOUNT INTERNATIONAL TRANSFER TO OFFSHORE JURISDICTION";
                    $scope.scenarioDate ="05/01/2010";
                }else{
                     var params = {
                                queryString: queryString                                
                            };
                            var token = {
                                token: $rootScope.ehubObject.token
                            };
                            TransactionAnalysisApiService.getAlertDetailsById(params, token).then(function(response){
                                $scope.scenario = response.data.scenario;
                                $scope.scenarioDate =new Date(response.data.date);
                            }, function(){
                                
                            });
                }
            };
            /**
             * Function to load  Tx details
             */
            $scope.getTxDetails = function (queryString) {
                
                if(queryString.toLowerCase().indexOf("petrosaudi")>-1 ){
                
                     $scope.transactionDetails = transactionPetro;
                    $scope.transactionDetails.map(function (d) {
                        d.txype = d.channel ? d.channel : 'N/A';
                        d.txtype2 = d['product-type'];
                        d.purpose1 = d['custom-text2'];
                        d.purpose2 = d['custom-text4'];
                        d.amt = d['amount-base'];
                        if (d['debit-credit'] == "C") {
                            d.DirectoinCls = "input-tranx";
                            d.inputOut = "input";
                            if (d.currency != d['currency-bene']) {
                                d.amt = parseInt(d['amount-activity-bene']);
                            }
                            d.counterParty = d['normalized-name-bene'] ? d['normalized-name-bene'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');
                        } else {
                            d.DirectoinCls = "output-tranx";
                            d.inputOut = "outward";

                            if (d.currency != d['currency-org']) {
                                d.amt = parseInt(d['amount-activity-org']);
                            }
                            d.counterParty = d['normalized-name-org'] ? d['normalized-name-org'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');

                        }
                        d.namebene = d['name-bene'] ? d['name-bene'] : 'N/A';
                        d.currencybene = d['currency-bene'] ? d['currency-bene'] : 'N/A';
                        d.countrybene = d['country-bene'] ? d['country-bene'] : 'N/A';
                        d.activity_amount_bene = d['amount-activity-bene'] ? parseFloat(d['amount-activity-bene']) : 'N/A';
                        d.nameorg = d['name-org'] ? d['name-org'] : 'N/A';
                        d.countryorg = d['country-org'] ? d['country-org'] : 'N/A';
                        d.activity_amount_org = d['amount-activity-org'] ? parseFloat(d['amount-activity-org']) : 'N/A';
                        d.currencyorg = d['currency-org'] ? d['currency-org'] : 'N/A';
                    });
                    CreateCharsData($scope.transactionDetails);
                    
                    return;
                }
                var filteredQueryString = '';

                if ($scope.taminamoutFilter) {
                    filteredQueryString = filteredQueryString + "&tx-amount-from=" + $scope.taminamoutFilter;
                }
                // start code for get locations selected
                var locationsArr = [];
                $(".TAlocationClassActive").each(function () {
                    locationsArr.push($(this).find("a").text());
                });
                if (locationsArr.length > 0) {
                    filteredQueryString = filteredQueryString + "&locations=" + locationsArr.join(",");
                }
                var params = {
                    queryString: queryString,
                    filteredQueryString: filteredQueryString
                };
                var token = {
                    token: $rootScope.ehubObject.token
                };
                TransactionAnalysisApiService.getTransactionsListForSelectedEntites(params, token).then(function(response){
                    var r = response.data.listTxDtos;/*jshint ignore:line */
                        $scope.InvestConsole = false;
                        $scope.transactionDetails = response.data.listTxDtos;
                        $scope.transactionDetails.map(function (d) {
                            d.txype = d.channel ? d.channel : 'N/A';
                            d.txtype2 = d['product-type'];
                            d.purpose1 = d['custom-text2'];
                            d.purpose2 = d['custom-text4'];
                            d.amt = d['amount-base'];
                            if (d['debit-credit'] == "D") {
                                d.DirectoinCls = "output-tranx";
                                d.inputOut = "input";
                                if (d.currency != d['currency-bene']) {
                                    d.amt = parseInt(d['amount-activity-bene']);
                                }
                                d.counterParty = d['normalized-name-bene'] ? d['normalized-name-bene'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');
                            } else {
                                d.DirectoinCls = "input-tranx";
                                d.inputOut = "outward";

                                if (d.currency != d['currency-org']) {
                                    d.amt = parseInt(d['amount-activity-org']);
                                }
                                d.counterParty = d['normalized-name-org'] ? d['normalized-name-org'] : (d['party-identifier'] ? d['party-identifier'] : 'N/A');

                            }
                            d.namebene = d['name-bene'] ? d['name-bene'] : 'N/A';
                            d.currencybene = d['currency-bene'] ? d['currency-bene'] : 'N/A';
                            d.countrybene = d['country-bene'] ? d['country-bene'] : 'N/A';
                            d.activity_amount_bene = d['amount-activity-bene'] ? parseFloat(d['amount-activity-bene']) : 'N/A';
                            d.nameorg = d['name-org'] ? d['name-org'] : 'N/A';
                            d.countryorg = d['country-org'] ? d['country-org'] : 'N/A';
                            d.activity_amount_org = d['amount-activity-org'] ? parseFloat(d['amount-activity-org']) : 'N/A';
                            d.currencyorg = d['currency-org'] ? d['currency-org'] : 'N/A';
                        });
                        CreateCharsData($scope.transactionDetails);
                   

                }, function(){
                    
                });
            };
            function getBasicInfo(keyword) {
                $scope.entityType =  getParameterByName("Etype")?getParameterByName("Etype"):"Company";
                $scope.founded_year ='N/A';
                 var data = {
                            "fetchers": ["1013", "1006", "1008", "1021"],
                            "keyword": keyword,
                            "searchType": getParameterByName("Etype")?getParameterByName("Etype"):"Company",
                            "lightWeight": true,
                            "saveGraph": true
                        };
                if(keyword.toLowerCase().indexOf("petrosaudi")>-1 ){
                    $scope.entityDetails={};
                    $scope.entityDetails.entityType="Company";
                    $scope.entityDetails.properties={};
                    $scope.entityDetails.properties.founded_year="28/03/2005";  
                    $scope.founded_year = $scope.entityDetails.properties.founded_year;
                    $scope.imgPath ="assets/images/activity/petrosaudi_logo.png";
                    $scope.entityDetails.properties.details="Petro Saudi International is a privately owned oil exploration and production company, with its main offices in the United Kingdom, Saudi Arabia and Switzerland. Founded in 2005, it is paving the way for the future of energy industry standards through compelling multinational projects on the ground and in the marketplace.";
                    
                    return;
                }
                var token = {
                    token: $rootScope.ehubObject.token
                };
                TransactionAnalysisApiService.searchResolve(JSON.stringify(data), token).then(function(response){
//                  var response = response.data;
                    var foundData = false;
                    if (response !== void 0) {
                        var resolvedResponse = [], resolvedName = '', parentIndex = -1, childIndex = -1;

                        if(response.data.resolved.length > 0  && response.data.resolved[0].entities[0].name)  
                             {resolvedResponse = response.data.resolved;}
                         else if(response.data.sortedResults.length > 0){
                                resolvedName = response.data.sortedResults[0].name;
                                response.data.sortedResults[0].fetcher === 'wikipedia.com' ? parentIndex = 0 : 
                                    response.data.sortedResults[0].fetcher === 'bloomberg.com' ? parentIndex = 1 : 
                                    response.data.sortedResults[0].fetcher === 'company.linkedin.com' ? parentIndex = 2 : parentIndex = 3;/*jshint ignore:line */
                                childIndex = response.data.sortedResults[0].index;
                                resolvedResponse.push(response.data.results[parentIndex]);
                            } else 
                             {resolvedResponse = response.data.results;}
                        resolvedResponse.forEach(function (entity) {
                            if (entity.status) {
                                entity.entities.forEach(function (entityID) {
                                    if (entityID.name !== '' && entityID.name !== void 0 && !foundData) {
                                        $scope.entityDetails = entityID;
//                                      if(entityID.entityID){
//                                          getRisk(entityID.entityID,token);
//                                      }
                                        $scope.founded_year = $scope.entityDetails.properties.founded_year?$scope.entityDetails.properties.founded_year:'N/A';
                                        foundData = true;
                                       
                                         }
                                });
                            }
                        });
                        
                        var dataForLogo ={"limit":1,"fetchers":["1016"],"keyword":$scope.entityDetails.name?$scope.entityDetails.name:$scope.entityName,"searchType":$scope.entityDetails.entityType,"lightWeight":true};
                      getLogo(dataForLogo,token);
//                        var dataForgraph ={"limit":1,"fetchers":["1008", "1006", "27"],"keyword":$scope.entityDetails.name?$scope.entityDetails.name:$scope.entityName,"searchType":$scope.entityDetails.entityType,"lightWeight":true}
                        var dataForgraph = {"fetchers":["1008","1006","27"],"keyword":$scope.entityDetails.name?$scope.entityDetails.name:$scope.entityName,"searchType":$scope.entityDetails.entityType,"lightWeight":true,"limit":1,"iterations":1,"alias":"investigationOverview","create_new_graph":false,"requires_expansion":true,"entity_resolution":true,"expansion_fetchers_lightweight":true,"expansion_fetchers":["2","2001","1005","1002","1003","3001","1007","29","2003","1009","1010","1011","1012","3002","3003","10","11","12","13","46","16","17","1021"]};
                        getLogo(dataForgraph,token,"graph");
                    }
                }, function(){
                    $('#vla').prepend('<div class="server-error">The server is busy, please try after sometime.</div>');
                    $('.custom-spinner').hide();
                });

            }
            /**
             * Function to get Risk
             */
            function getRisk(id,token,data){
                   TransactionAnalysisApiService.getRisk(id, token).then(function(response){
                       var arr =_.find(response.data.vertices, function(num){ return num.name.toLowerCase()== data.keyword.toLowerCase(); });
                   if(arr && arr.ir && arr.tr && arr.dr){
                       calculateRandomRisk(arr);
                        $scope.cumulativeRisk = (1 - (1 - DR) * (1 - IR) * (1 - TR))*100;
                   }
                   }, function(){
                        
                    });
             }
            /**
             * Function to get logo
             */
            function getLogo(data,token,isGraph){
                   TransactionAnalysisApiService.searchLogo(JSON.stringify(data), token,isGraph).then(function(response){
                       if(!isGraph && response.data.results[0].entities[0].properties.url )
                            {$scope.imgPath  = (response.data.results[0].entities[0].properties.url);}
                        if(isGraph){
                            getRisk(response.data.caseId,token,data);
                        }
                   }, function(){
                        
                    });
             }
            
                $scope.showLanding = true; 
                $scope.loadLanding =function(){
                    $timeout(function(){
                        $scope.showLanding = true;
                        $(".investigationsLaning a").click();
                    },0);
                };
                $scope.investigationTabClick = function(tabOption){
                    if(tabOption === "Investigation")
                        {$scope.showLanding = false;}
                    else if(tabOption === "InvestigationLanding")
                        {$scope.showLanding = true;} 
                }; 
                var IR, DR, TR;
                function calculateRandomRisk(caseObj){
//                  HostPathService.setRandomRisk(caseName);
                    $scope.scaledNumArr =[caseObj.ir,caseObj.dr,caseObj.tr];
                    IR = ($scope.scaledNumArr[0]/100);
                    DR = ($scope.scaledNumArr[1]/100);
                    TR = ($scope.scaledNumArr[2]/100);
                }
                
//              var uploadedFilesLength;
//              if($stateParams.caseId != undefined && $stateParams.caseId != '' && $stateParams.caseId != null){
//                  var caseDocsparams = {
//                      caseId: $stateParams.caseId,
//                      token: $rootScope.ehubObject.token
//                  };
//                  ActApiService.getCaseDetails(caseDocsparams).then(function(response){
//                      calculateRandomRisk(response.data.name);
//                  }, function(error){
//                      
//                  });
//                  UploadFileService.getCaseDocuments(caseDocsparams).then(function(response){
//                      uploadedFilesLength = response.data.result.length;
//                      if(uploadedFilesLength > 0){
//                          if(uploadedFilesLength >= 3){
//                              $scope.cumulativeRisk = "46";
//                              if(uploadedFilesLength >= 4)
//                                  $scope.cumulativeRisk = "72";
//                          }else{
//                              $scope.cumulativeRisk = "67";
//                          }
//                      }else{
//                          $scope.cumulativeRisk = "67";
//                      }
//                  }, function(error){
//                      
//                  });
//              }else{
//                  calculateRandomRisk(getParameterByName("entity")?getParameterByName("entity"):"petrosaudi international ltd");
//                  $scope.cumulativeRisk = (1 - (1 - DR) * (1 - IR) * (1 - TR))*100;
//              }
                /**
                 * Click on apply button
                 */
                $(".apply").on("click", function () {
                    loadInvestCharts($(this).parent().parent().siblings("h5").text());
                });
                
                $scope.modalClose = function(){
                    $("#investigationInfoWrapper").modal('hide');
                };
                
                function loadInvestCharts(text, fromtaglist) {
                    var isexist = false;
                    $.each($(".tags-list").find("li"), function () {
                        if (text == $(this).text()) {
                            isexist = true;
                            return false;
                        }
                    });
                    if (!fromtaglist && isexist) {
                        return false;
                    }
                    if (!fromtaglist) {
                        $(".tags-list").find("li").removeClass("active");
                        $(".tags-list").append('<li class="active tag_list_li txt_' + (text.split(" ").join("_")) + '"><a href="javascript:void(0);">' + text + '<i class="fa fa-times _close tag_list_li_close"></i></a></li>');
                    } else {
                        $(".tags-list").find("li").removeClass("active");
                        $(".tags-list").find("li.txt_" + (text.split(" ").join("_"))).addClass("active");
                    }
                /*-------- load comparison chart if selected element is comparison analysis ----*/
                    if (text == "Comparison Analysis") {
                        var options = {
                            container: "#chartContentDiv",
                            header: "COMPARISION ANALYSIS",
                            uri: "vendor/data/groupedColumn.json",
                            height: 475,
                            ytext: "Sum of transactions currency ($)",
                            xtext: "TRX Type",
                            isHeaer:false
                        };
                        InvestigationLandingService.groupedColumn(options,CompareData);
                    }
                /*--------load journey and decision chart if the element is journey and decision ----*/
                    if (text == "Journey and Decision") {
                        var options = {
                            header: "JOURNEY AND DECISION", 
                            Uri: "vendor/data/sank.json",
                            container: "#chartContentDiv",
                            height: 500,
                            isHeaer:false
                        };
                        InvestigationLandingService.loadsankey(options,journeyAndDecisionData);
                    }
                /*--------load Dynamic and Temporal Analysis chart if the element is journey and decision ----*/
                    if (text == "Dynamic and Temporal Analysis") {
                        InvestigationLandingService.plotdynamictemporal();
                    }
                /*-------------------------- load taxonomy chart ------------------------------------*/
                    if (text == "Taxonomy") {
                        var options = {
                            height: 500,
                            container: '#chartContentDiv', 
                            header: "Taxonomy",
                            uri: "vendor/data/bubbleChart.json",
                            isheader:false
                        };
                        InvestigationLandingService.chartBubble(options,taxanomyData);
                    }
               /*------------------------------------ load taxonomy chart ----------------------------------*/
                    if (text == "Cluster Analysis") {
                        var options = {
                            header: "Cluster ANALYSIS", 
                            Uri: "vendor/data/ClusteringBubbles.json", 
                            container: "#chartContentDiv",
                            height: 500,
                            isheader:false
                        };
                        InvestigationLandingService.ClusterBubble(options,clustersData);
                    }
                /*------------------------- load self emerged  chart -------------------------------*/
                    if (text == "Self emerged concepts") {
                        var options = {
                            container: "#chartContentDiv", 
                            "header": "SELF EMERGED CONCEPTS", 
                            "uri": "vendor/data/BubbleHierarchyInvestigation.json", 
                            "height": 500,
                            isHeaer:false
                        };
                        BubbleHierarchyChart(options);
                    }
                }
                function allowDrop(ev) {/*jshint ignore:line */
                    ev.preventDefault();
                }
                
                function drag(ev) {/*jshint ignore:line */
                    ev.dataTransfer.setData("text", $(ev.target).text());
                }
                
                function drop(ev) {/*jshint ignore:line */
                    ev.preventDefault();
                    var text = ev.dataTransfer.getData("text");
                    loadInvestCharts(text);
                }
                /**
                 * Click on tag list li
                 * 
                 */
                $("body").on("click", ".tag_list_li", function () {
                    loadInvestCharts($(this).text(), "fromtaglist");
                });
                /**
                 * Click on tag list li close
                 * 
                 */
                $("body").on("click", ".tag_list_li_close", function (e) {
                    e.stopPropagation();
                    var isactive = $(this).parent().parent().hasClass("active");
                    $(this).parent().parent().remove();
                    if (isactive) {
                        $("#chartContentDiv").empty();
                    }
                });
                
                /**
                 * Click on temporal submenu
                 */
                $("body").on("click", ".temporalsubmenu", function () {
                    $(".temporalsubmenu").css({"background-color": "#11B099"});
                    $(".temporalsubmenu").find("h5").css({"color": "#fff"});
                    $(this).css({"background-color": "#3890CF"});
                    $(this).find("h5").css({"color": "#fff"});
                    $("#temporalChartDiv").empty();
                    var url = "vendor/data/temporalData/data" + ($(this).parent().index() + 1) + ".json";
                    var options = {
                        height: 500,
                        container: '#temporalChartDiv', 
                        header: "DYNAMIC AND TEMPORAL ANALYSIS", 
                        uri: url,
                        isHeaer:false
                   };
                    chartTemporal(options);
                });
                
                /**
                 *Function to call a function to plot Bubble chart and to call function on window resize
                 */
                function chartTemporal(options){
                    loadtemporalChart(options);
                /*--------------------- responsiveness --------------------------*/
                    $(window).on("resize", function () {
                        if ($(options.container).find("svg").length != 0) {
                            $(options.container).empty();
                            new temporalChart(options);/*jshint ignore:line */
                        }
                    });
                    /**
                     *Function to load data to plot Bubble chart 
                     */
                    function loadtemporalChart(options) {
                        d3.json(options.uri, function (error, data) {
                            options.data = data;
                            new temporalChart(options);/*jshint ignore:line */
                        });
                    }
                }
                //code for loading chart from transaction intelligence starts here
                //get params by name function starts here
                function getParameterByName(name, url) {
                    if (!url) {
                        url = window.location.href;
                    }
                    name = name.replace(/[\[\]]/g, "\\$&");
                    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                            results = regex.exec(url);
                    if (!results)
                        {return null;}
                    if (!results[2])
                        {return '';}
                    return decodeURIComponent(results[2].replace(/\+/g, " "));
                }
                
                    
                    
                    if(getParameterByName("entity")){
                        $scope.entityName = getParameterByName("entity");                       
                    }
                    if(getParameterByName("q")){
                        $scope.showLanding = false;
                        $(".investigationDetails a").click();
                        
                    }
                    $scope.getTxDetails(getParameterByName("eId")?getParameterByName("eId"):$scope.entityName);
                    $scope.getAlertDetails(getParameterByName("id"));
                    
                    getBasicInfo($scope.entityName);
                    
                //code for loading chart from transaction intelligence ends here
                    function CreateCharsData(dataActual){
                        var linksArr=[];
                        var dataJourneyAndDecision={
                                nodes:[],
                                links:[]
                        };
                        var nodesObj ={},nodesCount=1;
                        var dataTaxonamy ={
                                "accounts": {},
                                "phrases": {},
                                "locations": {}
                            };
                        //clusters Data here
                        var dataClusters = [];
                        var dataCompare =[],beneArr=[];//compare analysis data starts here                      
                        var nested_data = d3.nest().key(function(d){return d.channel;}).entries(dataActual);
                        
                        angular.forEach(nested_data,function(d){
                            var dataComparechild={};
                            dataComparechild.model=d.key;
                            var childrenArray=[];
                            angular.forEach(d.values,function(innerval){
                                childrenArray.push({
                                    "name": innerval['name-org']?innerval['name-org']:innerval['name-bene'],
                                    "group":d.key,
                                    "size": innerval['amount-base'],
                                    "txt":"<div>"+( innerval['name-org']?innerval['name-org']:innerval['name-bene'])+"</div><div>Amount: "+getFormattedAmount(innerval['amount-base'])+"</div>"
                                    
                                });
                                beneArr.push(innerval['name-bene']);
                                dataComparechild[innerval['name-bene']] =innerval['amount-base'];
                            });
                            
                            dataClusters.push({
                                 "label": d.key,
                                  "value": d.values.length,
                                  "children": childrenArray,
                                  "txt":"<div>"+d.key+": "+(d.values.length)+"</div>"
                            });
                        
                            dataCompare.push(dataComparechild);
                        });
                        
                        clustersData =dataClusters;
                        //clusters Data ends here
                        angular.forEach(dataCompare,function(d){
                            angular.forEach(beneArr,function(d1){
                                if(!d[d1]){
                                    d[d1]=0;
                                }
                            });
                            
                        });
                        CompareData =dataCompare;
                        dataActual.sort(function(a,b){
                            return b['amount-base'] -a['amount-base'];
                        });
                        //compare analysis ends here
                        angular.forEach(dataActual,function(d){
                            //journey and decision Data here
                            if(d['name-org'] && !nodesObj[d['name-org']]){
                                dataJourneyAndDecision.nodes.push({
                                    "name":d['name-org'],
                                    "id": nodesCount-1
                                });
                                nodesObj[d['name-org']] = nodesCount;//to start node id from zero
                                nodesCount++;
                            }
                            if(d['name-bene'] && !nodesObj[d['name-bene']]){
                                dataJourneyAndDecision.nodes.push({
                                    "name":d['name-bene'],
                                    "id": nodesCount-1
                                });
                                nodesObj[d['name-bene']] = nodesCount;//to start node id from zero
                                nodesCount++;
                            }
                            
                            if(nodesObj[d['name-org']] &&  nodesObj[d['name-bene']]){
                                if(nodesObj[d['name-org']]-1 != nodesObj[d['name-bene']]-1 && $.inArray(((nodesObj[d['name-bene']]-1)+"---"+(nodesObj[d['name-org']]-1)),linksArr) ==-1){
                                    linksArr.push((nodesObj[d['name-org']]-1)+"---"+(nodesObj[d['name-bene']]-1));
                                    dataJourneyAndDecision.links.push({
                                        "source": nodesObj[d['name-org']]-1,
                                        "target": nodesObj[d['name-bene']]-1,
                                        "value": d['amount-base'],
                                        "label": d.channel,
                                        "txt":"<div>"+d.channel+"</div><div>From: "+d['name-org']+" TO: "+d['name-bene']+"</div><div>AMOUNT: "+getFormattedAmount(d['amount-base'])+"</div>"
                                            
                                    });
                                }
                            }
                            //end of journey and decision Data here
                            //data for taxanomy here
                            if(d['name-org'])
                                {dataTaxonamy.accounts[d['name-org']] = dataTaxonamy.accounts[d['name-org']]?(dataTaxonamy.accounts[d['name-org']]+1):1;}
                            if(d['name-bene'])
                                {dataTaxonamy.accounts[d['name-bene']] = dataTaxonamy.accounts[d['name-bene']]?(dataTaxonamy.accounts[d['name-bene']]+1):1;}
                            if(d['country-bene'])
                                {dataTaxonamy.locations[d['country-bene']] = dataTaxonamy.locations[d['country-bene']]?(dataTaxonamy.locations[d['country-bene']]+1):1;}
                            if(d['country-org'])
                                {dataTaxonamy.locations[d['country-org']] = dataTaxonamy.locations[d['country-org']]?(dataTaxonamy.locations[d['country-org']]+1):1;}
                            if(d['product-type'])
                                {dataTaxonamy.phrases[d['product-type']] = dataTaxonamy.phrases[d['product-type']]?(dataTaxonamy.phrases[d['product-type']]+1):1;}
                            if(d['party-identifier'])
                                {dataTaxonamy.phrases[d['party-identifier']] = dataTaxonamy.phrases[d['party-identifier']]?(dataTaxonamy.phrases[d['party-identifier']]+1):1;}
//                          if(d['amount-base'])
//                              dataTaxonamy.phrases[d['amount-base']] = dataTaxonamy.phrases[d['amount-base']]?(dataTaxonamy.phrases[d['amount-base']]+1):1;
                            if(d.currency)
                                {dataTaxonamy.phrases[d.currency] = dataTaxonamy.phrases[d.currency]?(dataTaxonamy.phrases[d.currency]+1):1;}
                            if(d['dataTaxonamy-source'])
                                {dataTaxonamy.phrases[d['dataTaxonamy-source']] = dataTaxonamy.phrases[d['dataTaxonamy-source']]?(dataTaxonamy.phrases[d['dataTaxonamy-source']]+1):1;}
                            if(d.date)
                                {dataTaxonamy.phrases[new Date(d.date).getFullYear()] = dataTaxonamy.phrases[new Date(d.date).getFullYear()]?(dataTaxonamy.phrases[new Date(d.date).getFullYear()]+1):1;}
                            if(d.channel)
                                {dataTaxonamy.phrases[d.channel] = dataTaxonamy.phrases[d.channel]?(dataTaxonamy.phrases[d.channel]+1):1;}
                                //end of data for taxanomy here                                                                     
                        });
                        taxanomyData =dataTaxonamy;
                        journeyAndDecisionData = dataJourneyAndDecision;
                        if(getParameterByName("q")){
                            $scope.showLanding = false;
                            $(".investigationDetails a").click();
                            setTimeout(function(){
                                loadInvestCharts(decodeURIComponent(getParameterByName("q")));
                            },0);
                        }
                    }
                    
                    
            $scope.captureImage=function(id){
                //$('#chartContentDiv').css('width','1000px');
                var openWidgetCaptureModal = $uibModal.open({/*jshint ignore:line */
                    templateUrl: 'scripts/common/modal/views/widget.capture.modal.html',
                    controller: 'WidgetCaptureModalController',
                    size: 'lg',
                    backdrop: 'static',
                    windowClass: 'custom-modal capture-modal',
                    resolve: {
                        widgetId: function(){
                            return id;
                        },
                        canvasnew:function(){
                            return '';
                        },
                        topLocMapView: function(){
                            return false;
                        }
                    }
                });
            
            };
                    
        }