'use strict';
angular.module('ehubApp')
	   .controller('YourCaseMetricsController', yourCaseMetricsController);

		yourCaseMetricsController.$inject = [
			'$scope', 
			'$rootScope',
			'DiscoverApiService',
			'YourCaseMetricsConst',
			'$uibModal',
			'$timeout'
		];
		
		function yourCaseMetricsController(
				$scope, 
				$rootScope,
				DiscoverApiService,
				YourCaseMetricsConst,
				$uibModal,
				$timeout) {
		$scope.token = $rootScope.ehubObject.token;
		$scope.toggleGraphCaseMetrics = true;
		$scope.yourCaseMetricsPreloader = true;
		var isApiLoaded = false;
		var actualcaseMetricsData;
		var actualcaseMetricsData1;
		var params = {
	    	token: $scope.token
	    };
		
		
		/*
		 * @purpose: open filter modal
		 * @created: 02 Jan 2018		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.OpenfilterCaseDataModal =function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             //backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return [
		                		{status:"Submitted", code:2},
		                		{status : "Acknowledge",code :3},
		                		{status:"Accepted", code:4},
		                		{status:"Pause", code:5},
		                		{status:"Rejected", code:6},
		                		{status:"Resolved", code:8},
		                		{status:"Canceled", code:9}
		                		];
		                },
		                keysTofilter:function(){
		                	return false;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return false;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return false;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
			                return $scope.namesList;	
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {	
	  			if(response){
					  handleFiltersApplied(response);
				  }
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 15 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
		
			var filteredData=[];
			var SearchName;
			$scope.FilteredKeyword = "";
			$scope.StatusOption = 0;
			var actualData = jQuery.extend(true,{}, actualcaseMetricsData);

			
			if(resp.filterApplied.status && resp.filterApplied.status.length > 0 ){
				var keys =resp.filterApplied.status[0];
				$scope.StatusOption = resp.filterApplied.status[0];
				if(resp.caseSearchKeyword){
					SearchName = resp.caseSearchKeyword;
					$scope.FilteredKeyword = SearchName;
				}else{
					SearchName = "noName";
				}
				angular.forEach(actualData.data.result,function(d){				
						if((d.name.toLowerCase().indexOf(SearchName.toLowerCase()) > -1 || SearchName == "noName") && d.currentStatus == keys){
							filteredData.push(d);
						}
				});
			}else if(resp.caseSearchKeyword){
				$scope.FilteredKeyword = resp.caseSearchKeyword;
				var SearchName = resp.caseSearchKeyword;
				angular.forEach(actualData.data.result,function(d){				
					if(d.name.toLowerCase().indexOf(SearchName.toLowerCase()) > -1){
						filteredData.push(d);
					}
				});
			}else{
				filteredData = actualData.data.result;
			}
			var optionsData = getFilterDatastatus(filteredData);
			var keyOptions = {
					"data" : optionsData
			};
			actualData.data.result = filteredData;  
			fetchData(actualData,keyOptions);

		}

		
		
		/*-----------------------Swathi's Functionality----------------------------*/
		var data = {"status": [3, 4, 5],"addRiskScore":false};
		function getAllCasesData(){
			DiscoverApiService.getAllCases(params, data).then(function(response) {
		    	var totalCasesCount = response.data.paginationInformation.totalResults;
		    	params = {
	    			token: $scope.token,
	    			//pageNumber: 1,
					//recordsPerPage: totalCasesCount
				};
				data = {
					"status": [3, 4, 5],
					"pageNumber":1,
					"recordsPerPage": totalCasesCount,
					"addRiskScore":false
				};
		    	DiscoverApiService.getAllCases(params, data).then(function(caseListResponse) {
		    		actualcaseMetricsData = caseListResponse;
		    		$scope.namesList = [];
		    		angular.forEach(actualcaseMetricsData.data.result,function(d){
		    			$scope.namesList.push(d.name);
		    		});
		    		$scope.namesList = _.uniq($scope.namesList);
		    		params = {
	    		    	token: $scope.token,
	    		    	currentOnly: true
					};
	    	    	DiscoverApiService.analystMetrix(params).then(function(metrixResponse) {
	    	    		isApiLoaded = true;
	    	    		actualcaseMetricsData1 = metrixResponse;
	    	    		fetchData(caseListResponse, metrixResponse);
	    	    		$scope.yourCaseMetricsPreloader = false;
	    		    	
	    			},function() {
	    				$scope.yourCaseMetricsPreloader = false;       		
	    			});
				},function() {
					$scope.yourCaseMetricsPreloader = false;       		
				});
			},function() {
				$scope.yourCaseMetricsPreloader = false;
			});
		}
		getAllCasesData();
		
		$rootScope.$on("caseAcknowledged", function(){
			getAllCasesData();
			return false;
		});
		$scope.cases = [];
		function fetchData(response, metrixResponse){
			$scope.cases = response.data.result;
	    	$scope.ongoing = [];
	    	$scope.delayed = [];
	    	$scope.today = new Date();
	    	
	    	angular.forEach($scope.cases, function(caseObj) {
	    		switch(caseObj.currentStatus) {
	    			case 5:
	    				$scope.delayed.push(caseObj);
	    				break;	
	    			case 4:
		    	    	$scope.ongoing.push(caseObj);
		    	        break;
	    			case 3:
		    	    	$scope.ongoing.push(caseObj);
		    	        break;
		    	    default:
		    	       
		    	}
	    	});
	    	if($scope.delayed.length > 3 || $scope.ongoing.length > 3){
		    	$(document).ready(function() {
					/*Custom Scroll Bar*/
					$('#yourCaseMetricsWidget').mCustomScrollbar({
						axis : "y",
						theme : "minimal"
					});
				});
	    	}
	    	var options = {
				container : "#caseMetricsGraph",
				header : YourCaseMetricsConst.header,
				uri : metrixResponse.data,
				showtext:false,
				txt:"Case Status",
				height : 200
			};
	    	$scope.errorMsg = true;
    		angular.forEach(options.uri, function(i) {
	    		if(i > 0){
	    			$scope.errorMsg = false;
		    	}
    		});
    		if($scope.errorMsg == false){
    			setCaseMetricsPieChart(options);
    		}else{
    			$scope.yourCaseMetricsData = [];
    			angular.element('#caseMetricsGraph').html('<p class="no-data">No Data Found</p>');
    		}
		}
		
		
		/*
		 * @purpose: setting pie chart for case metrics grid 
		 * @created: 15 sep 2017
		 * @params: options(object) 
		 * @returns: no 
		 * @author: swathi
		 */
		function setCaseMetricsPieChart(options){
			
			loadpieChart(options);

			/*responsiveness*/
			$(window).on("resize", function() {
				$timeout(function(){
					if ($(options.container).find("svg").length != 0) {
						$(options.container).empty();
						new pieChart(options);
					}
				}, 0);
			});

			/* on resize of Div */
			$scope.$on('gridster-draggable-changed', function() {
				if(isApiLoaded == true){
				ResizeChart();
			}
			});
			// resize chart as per dragged height in div
			function ResizeChart(){
				$timeout(function(){
					if ($(options.container).find("svg").length != 0) {
						$(options.container).empty();
						if(document.getElementById('yourCaseMetricsdiv').clientHeight < 320){
							options.height = 200;
						}else{
							options.height = 480;
						}
						new pieChart(options);
					}
				}, 0);
			}
			
			$scope.OntoggleGraph = function(){
				$scope.toggleGraphCaseMetrics = true;
				ResizeChart();
			};
			
			function loadpieChart(options) {
				var pieData = [];
				angular.forEach(options.uri, function(i, d) {
					if((d == 'fresh' || d == 'focus') || i == 0){
						
					}else{
						pieData.push({
							key : d,
							value : i
						});
					}
				});
				options.data = pieData;
				$scope.yourCaseMetricsData = pieData;
				$timeout(function(){
					 new pieChart(options);
				}, 0);
			}
		}
		
		function getFilterDatastatus(filteredData){
			var SubmittedC = 0;
    		var AcknowledgeC = 0;
    		var AcceptedC=0;
    		var PauseC = 0;
    		var RejectedC = 0;
    		var ResolvedC = 0;
    		var CanceledC=0;
			angular.forEach(filteredData, function(i) {
				switch(i.currentStatus){
				case 2:
					SubmittedC++;
					break;
				case 3:
					AcknowledgeC++;
					break;
				case 4:
					AcceptedC++;
					break;
				case 5:
					PauseC++;
					break;
				case 6:
					RejectedC++;
					break;
				case 8:
					ResolvedC++;
					break;
				case 9:
					CanceledC++;
					break;
				}
			});
			var key = {
					"submitted":SubmittedC,
	        		"acknowledge":AcknowledgeC,
	        		"accepted":AcceptedC,
	        		"pause":PauseC,
	        		"rejected": RejectedC,
	        		"resolved":ResolvedC,
	        		"canceled":CanceledC
			};
			return key;
		}
		
}