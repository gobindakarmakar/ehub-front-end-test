'use strict';
angular.module('ehubApp')
	   .controller('MyEntitiesController', myEntitiesController);

		myEntitiesController.$inject = [
			'$scope', 
			'$rootScope',
			'DiscoverService',
			'$uibModal',
			'TimeScaleBubbleConst',
			'DiscoverApiService'
		];
		
		function myEntitiesController(
				$scope, 
				$rootScope,
				DiscoverService,
				$uibModal,
				TimeScaleBubbleConst,
				DiscoverApiService) {
			
		$scope.token = $rootScope.ehubObject.token;
		$scope.toggleGraphmyEntities = true;
		$scope.myentitiesDatas = [];
		$scope.maxoptions = {};
		$scope.myEntitiesPreloader = true;
		var actualEntityData;
		var data = [];
		
		var AllMyCasesParams = {
			token: $scope.token,
			orderIn: "desc",
			orderBy:"createdOn",
			addRiskScore:false
		};
		/*
		 * @purpose: get my cases
		 * @created: 24 jan 2018
		 * @params: null
		 * @returns: no 
		 * @author: swathi
		 */
		var isApiLoaded = false;
		function getMyCases(){
			DiscoverApiService.getAllCases({token: $scope.token}, AllMyCasesParams).then(function(response) {
				isApiLoaded = true;
				var loadAllCaseParam = {
					token: $scope.token,
					orderIn: "desc",
					orderBy: "createdOn",
					recordsPerPage: response.data.paginationInformation.totalResults,
					pageNumber: 1,
					status: [4,5,8,9],
					addRiskScore:false
				};
				if(response.data.paginationInformation.totalResults != 0){
					DiscoverApiService.getAllCases({token: $scope.token}, loadAllCaseParam).then(function(results) {
						if(results.data.paginationInformation.totalResults != 0){
							angular.forEach(results.data.result, function(v, k){
								loadEntities(v.caseId, k, results.data.result.length - 1);
							});
						}else{
							$scope.myEntitiesPreloader = false;
							angular.element('#myEntitiesChart').html('<p class="no-data">No data found</p>');
						}
					}, function(){
				    	$scope.myEntitiesPreloader = false;
				    });
				}else{
					$scope.myEntitiesPreloader = false;
					angular.element('#myEntitiesChart').html('<p class="no-data">No data found</p>');
				}
		    }, function(){
		    	$scope.myEntitiesPreloader = false;
		    });
		}
		getMyCases();
		
		
		/*
		 * @purpose: get my entities
		 * @created: 24 jan 2018
		 * @params: null
		 * @returns: no 
		 * @author: swathi
		 */
		function loadEntities(caseId, caseKeys, caseLength){
			$scope.AllNames = [];
			var params = {
				token: $scope.token
			};
			DiscoverApiService.getMyEntities(params, caseId).then(function(response){
				$scope.myEntitiesPreloader = false;
				angular.forEach(response.data, function(v){
					v.date =  new Date(v.date).toLocaleDateString();
					data.push(v);
				});
				if(caseKeys == caseLength && data.length != 0){
			    	angular.forEach(data,function(d){
			    		$scope.AllNames.push(d.entityName);
			    	});
			    	$scope.AllNames = _.uniq($scope.AllNames);
			    	$scope.myentitiesDatas = data;
			    	if($scope.myentitiesDatas.length != 0){
						$('#myEntitiesWidget').mCustomScrollbar({
							axis : "y",
							theme : "minimal"
						});
					}
			    	actualEntityData = data;
			    	$scope.maxoptions = data;
			    	ResizeGraph();
//					$scope.myEntitiesPreloader = false;
				}else if(caseKeys == caseLength && data.length == 0){
					$scope.maxoptions = {};
					angular.element('#myEntitiesChart').html('<p class="no-data">No data found</p>');
					$scope.myEntitiesPreloader = false;
				}
			}, function(error){
		    	$scope.myEntitiesPreloader = false;
		    	if(error.responseMessage){
		    		angular.element('#myEntitiesChart').html('<p class="no-data">'+ error.responseMessage +'</p>');
		    	}
		    });
		}
	    
		 /*
		 * @purpose: open filter modal
		 * @created: 15 Dec 2017		
		 * @author: prasanthi
		 */
		var openCasefiltersModal;
		$scope.openFilterModalForChart =function(){
			openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	             backdrop: 'static',
				 windowClass: 'custom-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return true;
		                },
		                statusList:function(){
		                	return false;
		                },
		                keysTofilter:function(){
		                	return $scope.keysTofilter;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return $scope.FilteredKey;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return true;
		                },
		                CaseDiarySelections : function(){
		                	return false;
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
			                return $scope.AllNames;	
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {	  			
	  			handleFiltersApplied(response);
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
		};
		
		/*
		 * @purpose: handle/filter data
		 * @created: 15 Dec 2017
		 *  @params: response of modal(object)		 
		 * @author: prasanthi
		 */
		function handleFiltersApplied(resp){
			var filteredData={};/*jshint ignore:line */
			var actualData = jQuery.extend(true,{}, actualEntityData);
			var filtered= [];
			if(resp.caseSearchKeyword){
				$scope.FilteredKeyword = resp.caseSearchKeyword;
				angular.forEach(actualData,function(i){
					var name = i.entityName ? i.entityName : "noName" ;
					if(name != "noName"){
						if(name.toLowerCase().indexOf(resp.caseSearchKeyword.toLowerCase()) > -1){
							filtered.push(i);
						}
					}
				});
				$scope.myentitiesDatas = filtered;
				if(filtered.length > 0){
					$scope.maxoptions = filtered;
					DiscoverService.setMyEntitiesTimeScaleBubbleChart(filtered);				
				}else{
					angular.element('#myEntitiesChart').html('<p class="no-data">No Data Found</p>');
					$scope.maxoptions = {};
				}
			}else{
				$scope.FilteredKeyword = "";
				if(Object.keys(actualData).length > 0){
					angular.forEach(actualData,function(i){
						var name = i.entityName ? i.entityName : "noName" ;
						if(name != "noName"){
							filtered.push(i);
						}
					});
					$scope.myentitiesDatas = filtered;
					$scope.maxoptions = filtered;
					DiscoverService.setMyEntitiesTimeScaleBubbleChart(filtered);	
				}else{
					angular.element('#myEntitiesChart').html('<p class="no-data">No Data Found</p>');
					$scope.maxoptions = {};
				}
			}
		}
		
		/*
		 * @purpose: open fullscreen modal
		 * @created: 3 Jan 2018		
		 * @author: varsha
		 */		
		$scope.maximizeDiv=function(Chart){
			var maximizeModal = $uibModal.open({
				templateUrl: 'scripts/common/modal/views/maximize.modal.html',
				controller: 'MaximizeModalController',
				size: 'lg',
				backdrop: 'static',
				windowClass: 'custom-modal capture-modal',
				resolve: {
					Id: function(){
						return Chart;
					},
				    ChartOptions:function(){
				    	return $scope.maxoptions;
				   }
				}
			});
			maximizeModal.result.then(function(){
				
			}, function(){
			});
			
		};
	    
		/* on resize of Div */
		$scope.$on('gridster-draggable-changed', function(e, ui, $widget) {/*jshint ignore:line */
			if(isApiLoaded == true){
			ResizeGraph();
		}
		});
		
		$scope.openChartGraph = function(){
			$scope.toggleGraphmyEntities = true;
			if(isApiLoaded == true){
				ResizeGraph();
			}
		};
		
		function ResizeGraph(){
			var timescalebubbleData = $scope.maxoptions;
			var widthResp = $('#myEntitiesChart').width();/*jshint ignore:line */
			var heightResp = document.getElementById('myEntitiesdiv').clientHeight;
			if(timescalebubbleData.length > 0){
			
			if(heightResp > 320){
	    		heightResp = 530;
	    	}else{
	    		heightResp = 230;
	    	}
			$("#myEntitiesdiv").find("tbody").css("height",heightResp);
			$('#myEntitiesChart').empty();
			setTimeout(function(){
	               var exampleChart = new timescalebubbleChart({/*jshint ignore:line */
	                    container: '#myEntitiesChart',
	                    data: timescalebubbleData,
	                    header: TimeScaleBubbleConst.header,
	                    height: heightResp,
	                    isheader: TimeScaleBubbleConst.isheader,
	                    xticks: TimeScaleBubbleConst.xticks,
	                    margin: TimeScaleBubbleConst.margin                        
		            },0);
		         });
			}
		}

}