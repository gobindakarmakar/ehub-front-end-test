'use strict';
angular.module('ehubApp')
	   .controller('TopLocationsController', topLocationsController);

		topLocationsController.$inject = [
			'$scope',
			'$rootScope',
			'DiscoverService', 
			'DiscoverApiService',
			'TopLocationsConst'
		];
	    function topLocationsController(
	    		$scope,
	    		$rootScope,
	    		DiscoverService,
	    		DiscoverApiService,
	    		TopLocationsConst
	    		){
	    	
	    	$scope.toggleGraphTopLocation = true;
			$scope.worldDatas = [];
			$scope.locationsData = [];
			$scope.topLocationsPreloader = true;
			$scope.token = $rootScope.ehubObject.token;
			var markersData = [],/*jshint ignore:line */
				params = {
			    	token: $scope.token
				};
			var zoomOptions = TopLocationsConst.zoomOptions;
			var count = 40;
			var data = {
				'status': [4, 5, 8, 9]	
			};
			/*
		     * @purpose: get list of locations
		     * @created: 26 sep 2017
		     * @params: params(object)
		     * @return: no
		     * @author: swathi
		    */
			DiscoverApiService.getAllCaseSummaryByStatusList(params, data, count).then(function(response) {
				$scope.topLocationsPreloader = false;
				if(response.data && response.data.status == undefined){
					 $scope.locationsData = response.data.locations;
					 $scope.totalLocation = Object.keys(response.data.locations).length;
					 $('#topLocationWidget').mCustomScrollbar({
						axis : "y",
						theme : "minimal"
					 });
				}else{
				    $scope.locationsData = [];
					$scope.totalLocation = 0;
				}
				var timeoutCount = $scope.totalLocation * 1000;
				var isGeocoder;
				try {
					   new google.maps.Geocoder();

					   isGeocoder = true;
					}
					catch(err) {
						isGeocoder = false;
					}
					if(isGeocoder){
						var leafletMap= DiscoverService.setWorldMap($scope.locationsData, zoomOptions);
					}else{
						angular.element('#mapLeaflet').html('<p class="no-data">'+ 'Failed to get Data from Server' +'</p>');
					}
				setTimeout(function(){
					$scope.topLocationsPreloader = false;
				},timeoutCount);
			    $rootScope.leafletMap = leafletMap;
			}, 
			function(error) {
				$scope.topLocationsPreloader = false;			
				if(error.responseMessage){
					angular.element('#mapLeaflet').html('<p class="no-data">'+ error.responseMessage +'</p>');
				}
			});
			
			/*
		     * @purpose: get all cases count
		     * @created: 03 jan 2018
		     * @params: params(object), data(object)
		     * @return: no
		     * @author: swathi
		    */
		   data = {
				'status': [4, 5, 8, 9],
				'addRiskScore':false
			};
			DiscoverApiService.getAllCases(params, data).then(function(response) {
				$scope.CountCases = response.data.paginationInformation.totalResults;
			});
			
			$scope.$on('gridster-draggable-changed', function() {
				var height = document.getElementById('topLocationsdiv').clientHeight ? document.getElementById('topLocationsdiv').clientHeight : 600;
				var widthresp = $("#topLocationsdiv").width();
				if(height > 610){
					height = 830;
				}else{
					height = 570;
				}
				$("#topLocationsdiv").find("tbody").css("height", height);
				$("#topLocationsdiv").find("tbody").css("width", widthresp);
			});
	    }