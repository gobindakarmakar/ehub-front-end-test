'use strict';
angular.module('ehubApp')
	   .controller('YourWorkDiaryController', yourWorkDiaryController);

		yourWorkDiaryController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'DiscoverApiService',
			'HostPathService',
			'$uibModal',
			'EHUB_API',
			'Flash',
			'$timeout'
		];
		
		function yourWorkDiaryController(
				$scope, 
				$rootScope,
				$state,
				DiscoverApiService,
				HostPathService,
				$uibModal,
				EHUB_API,
				Flash,/*jshint ignore:line */
				$timeout
			) {
		/* Discover Utilities Panel - Work Diary */
		$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount = 0;
		$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList = [];
		$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = -1;
		$scope.pageNumber = 1;
		$scope.token = $rootScope.ehubObject.token;
		$scope.statusDataForEachCase = [];
		var statusDataForCases = [];
		
		var casesData = {
		   "orderIn": "desc", 
		   "orderBy": "createdOn",
		   "status":[2,3],//only Acknoledged and Submitted cases status
		   "recordsPerPage": 10,
		   "pageNumber": $scope.pageNumber
		};		

		/*
	     * @purpose: load cases
	     * @created: 07 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
		$scope.pageNumber = 1;
		var casesCount = 0, index = 0;
		function loadCases(params){
			if(searchCaseKeyword){
				loadCasesWithSearch(params,searchCaseKeyword);
				return;
			}
			$scope.yourWorkDairyPreloader = true;
			DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},params).then(function (response) {
				$scope.yourWorkDairyPreloader = false;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount = response.data.paginationInformation.count;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList = response.data.result;
				
				if($scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount > 0){
					$scope.dashboardUtilitiesPanelDiscoverOnClickWorkDiaryDropDown();
					$timeout(function(){
						$scope.workDairyToggle = true;
					},0);
				}
				var loadAllCaseParam = {
					"orderIn": "desc", 
					"orderBy": "createdOn",
					"status":[2,3],//only Acknoledged and Submitted cases status
				    "recordsPerPage": response.data.paginationInformation.totalResults,
					"pageNumber": 1
				};
				DiscoverApiService.getAllCases({token:$rootScope.ehubObject.token},loadAllCaseParam).then(function(results) {
					$scope.resposeS = results;
					$scope.AllNames = [];
					angular.forEach($scope.resposeS.data.result,function(d){
						$scope.AllNames.push(d.name);
						var params = {
							'token':$scope.token,
							'caseId':d.caseId,
							'currentStatus':d.currentStatus
						};
						index++;
						getStatuses(params, index);
					});
					$scope.AllNames = _.uniq($scope.AllNames);
				});
					
				$scope.allCasesCount = response.data.paginationInformation.totalResults;
			}, function () {
				$scope.yourWorkDairyPreloader = false;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount = 0;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList = [];
			});
		}
		loadCases(casesData);
		
		/*
	     * @purpose: get status options for each ACT drop down
	     * @created: 04 Jul 2018
	     * @params: params(object), index(integer)
	     * @author: swathi
	    */
		function getStatuses(params, index){
			DiscoverApiService.caseDropDownBasedOnStatus(params).then(function(response){
				casesCount++;
				var obj = response.data;
				if(obj.length > 0){
					angular.forEach(obj, function(v){
						v.caseId = params.caseId;
						v.casesCount = index;
					});
				}
				statusDataForCases.push(obj);
				if($scope.resposeS.data.result.length == casesCount){
					 statusDataForCases.sort(function(a, b){
						 if(a.length>0 && b.length>0){
							return a[0].casesCount - b[0].casesCount;
						 }
					 });
					  // Slice up the viewed cases
					 var begin = (($scope.pageNumber - 1) * 10),
             		 end = begin + 10;
					 $scope.statusDataForEachCase = statusDataForCases.slice(begin, end);
				}
			});
		}
		/*
	     * @purpose: load cases with search 
	     * @created: 20 dec 2017
	     * @params: params(object), searchCaseKeyword (string)
	     * @return: success, error functions
	     * @author: prasanthi
	    */
		function loadCasesWithSearch(params,searchCaseKeyword){	
			params.keyword = searchCaseKeyword;
			$scope.yourWorkDairyPreloader = true;
			DiscoverApiService.caseListFullTextSearch({token:$rootScope.ehubObject.token},params).then(function (response) {
				$scope.yourWorkDairyPreloader = false;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount = response.data.paginationInformation.count;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList = response.data.result;
				$scope.allCasesCount = response.data.paginationInformation.totalResults;
			}, function () {
				$scope.yourWorkDairyPreloader = false;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseCount = 0;
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList = [];
			});
		}
		/* Work Diary dropdown toggle */
		$scope.dashboardUtilitiesPanelDiscoverOnClickWorkDiaryDropDown = function (){
			$scope.workDairyToggle = !$scope.workDairyToggle;
			if($scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown != -1 && $scope.workDairyToggle == false){
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = -1;
			}
		};
		
	   /*
	     * @purpose: on page change
	     * @created: 07 oct 2017
	     * @params: page(number)
	     * @return: no
	     * @author: swathi
	    */ 
	    $scope.pageChanged = function (page) {
			$timeout(function(){
				$scope.workDairyToggle = true;
			},0);
	        $scope.pageNumber = page;
			casesData.pageNumber =$scope.pageNumber;
			$scope.statusDataForEachCase = [];
			statusDataForCases = [];
			casesCount = 0;
			index = 0;	
			loadCases(casesData);
	    };
	     /* get All cases summary  */
	     function initServicesData() {/*jshint ignore:line */
	 		var params = {
 				"token": $scope.token
	 		};
	 		var count = 40;
	 		var data = {
 				'status': [4, 5, 8, 9]	
 			};
	 		DiscoverApiService.getAllCaseSummaryByStatusList(params, data, count).then(function (response) {
	 			$rootScope.dashboardUtilitiesPanelCaseSummary = response.data;
	 		}, function () {
	 			$rootScope.dashboardUtilitiesPanelCaseSummary = {};
	 		});
	 	}
	 	/*
	     * @purpose: update case status submitted to acknowledged
	     * @created: 07 oct 2017
	     * @params: params(object)
	     * @return: success, error functions
	     * @author: swathi
	    */
	 	function acknowledged(index, seedId, currentStatus, selectedStatus){
	 		$scope.yourWorkDairyPreloader = true;
	 		var params = {
 				'token' : $scope.token,// for each login user generated token
				'caseId' : seedId,// case ID for case created.
				'statusFrom' : currentStatus,
				'statusTo' : selectedStatus
			};
			var paramvalues = {
				'token': $scope.token,// for each login user generated token
				'caseId': seedId,// case ID for case created.
				'currentStatus': currentStatus
			};
	    	DiscoverApiService.commonApiCallForAllCaseStatuses(params).then(function() {
	    		$scope.dashboardUtilitiesPanelDiscoverWorkDiaryCaseList[index].currentStatus = selectedStatus; 
	    		$scope.yourWorkDairyPreloader = false;
				$rootScope.$emit("caseAcknowledged", true);
				
				paramvalues.currentStatus = selectedStatus;
				DiscoverApiService.caseDropDownBasedOnStatus(paramvalues).then(function(response){
					$scope.statusDataForEachCase[index] = response.data;
				});
		    }, 
		    function() {
		    	$scope.yourWorkDairyPreloader = false;
			});
	 	}
		 
		


	 	/* Toggle detailed section on click of work diary card */
	 	$scope.showDetailedSection = -1;
	 	$scope.discoverClickWorkDiaryCard = function(index){
	 		$scope.workDairyToggle = false;
	 		if ($scope.showDetailedSection === index) {
				index = -1;
			}
			$scope.showDetailedSection = index;
	 	};
	 	
	 	$scope.discoverIsHideDetailSection = function(index){
	 		return (index !== $scope.showDetailedSection);
	 	};
	 	
	 	/* Work diary Act dropDown click function */
	 	$scope.discoverOnClickWorkDiaryActDropDown = function discoverOnClickWorkDiaryActDropDown(event, index) {
	 		event.stopPropagation();
	 		$scope.workDairyToggle = false;
	 		
	 		$scope.workDairyToggle = !$scope.workDairyToggle;
			if($scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown != -1 && $scope.workDairyToggle == false){
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = -1;
			}
			
			if ($scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown === index) {
				index = -1;
			}
			$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = index;
			
		};
		/* Hide Work diary Act dropDown*/
	 	$scope.discoverIsHideWorkDiaryActDropDown = function discoverIsHideWorkDiaryActDropDown(index) {
			return (index !== $scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown);
		};
		/* Work diary Act dropDown menu items click */
		var caseSelectedStatus;
	 	$scope.discoverOnClickWorkDiaryActDropDownItem = function discoverWorkDiaryActDropDownItemClick($event, event, index, seedid, currentstatus, selectedStatus) {
	 		$event.stopPropagation();
	 		$scope.workDairyToggle = false;
	 		
	 		$scope.workDairyToggle = !$scope.workDairyToggle;
			if($scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown != -1 && $scope.workDairyToggle == false){
				$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = -1;
			}
			
			$scope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownShown = -1;
			 var className = event;
			 // checking which option is selected from act dropdown so that under that category case will moved and event here give current option selected
	 		if (className.indexOf("act_dropdown_ACKNOWLEDGE") > -1) {
				acknowledged(index, seedid, currentstatus, selectedStatus);
			}
			else if (className.indexOf("act_dropdown_ACCEPT") > -1) {
				accept(seedid, currentstatus, selectedStatus);
			}
			else if (className.indexOf("act_dropdown_PAUSE") > -1) {
				caseSelectedStatus = selectedStatus;
				$scope.tempSeedId = seedid;
				$scope.tempCurrentStatus = currentstatus;
			    $scope.tempTargetStatus = 5;
			    commentsModal();
			}
			else if (className.indexOf("act_dropdown_REJECT") > -1) {
				caseSelectedStatus = selectedStatus;
				$scope.tempSeedId = seedid;
				$scope.tempCurrentStatus = currentstatus;
			    $scope.tempTargetStatus = 6;
			    commentsModal();
			}
			else if (className.indexOf("act_dropdown_FORWARD") > -1) {
				$scope.tempSeedId = seedid;
				forwardCaseModal();
			}
			else if (className.indexOf("act_dropdown_FOCUS") > -1) {
				$scope.tempSeedId = seedid;
				focus();
			}
			else if (className.indexOf("act_dropdown_RESOLVED") > -1) {
				caseSelectedStatus = selectedStatus;
				$scope.tempSeedId = seedid;
				$scope.tempCurrentStatus = currentstatus;
			    $scope.tempTargetStatus = 8;
			    commentsModal();
			}
			else if (className.indexOf("act_dropdown_CANCELED") > -1) {
				$scope.tempSeedId = seedid;
				cancel(seedid, currentstatus, selectedStatus);
			}
			else {
				/* $rootScope.dashboardUtilitiesPanelDiscoverWorkDiaryActDropDownDialogMessage
				   = "Failed to changed case status! Reason: Unknown menu"; */
			}
	 	};
	 	/*
	     * @purpose: open comments modal
	     * @created: 07 oct 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
	 	var openCommentsModal;
	 	function commentsModal() {
	        openCommentsModal = $uibModal.open({
        		templateUrl: 'scripts/discover/modal/views/comments.modal.html',
        		scope: $scope,
             	backdrop: 'static',
			 	windowClass: 'custom-modal comments-modal'
	        });

	        openCommentsModal.result.then(function () {
	        }, function () {
	        });
	        $scope.modalInstance = openCommentsModal;
	        return openCommentsModal.result;
	    }
	 	/*
	     * @purpose: open forward case modal
	     * @created: 07 oct 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
	 	var openForwardCaseModal;/*jshint ignore:line */
	 	function forwardCaseModal() {
	 		var openForwardCaseModal = $uibModal.open({
        		templateUrl: 'scripts/discover/modal/views/forward.case.modal.html',
        		scope: $scope,
             	backdrop: 'static',
			 	windowClass: 'custom-modal forwardcase-modal'
	        });

	 		openForwardCaseModal.result.then(function () {
	        }, function () {
	        });
	        $scope.forwardCaseModalInstance = openForwardCaseModal;
	        return openForwardCaseModal.result;
	    }
	 	/*
	     * @purpose: open create case modal
	     * @created: 07 oct 2017
	     * @params: no
	     * @return: null
	     * @author: swathi
	    */
	 	$scope.displayCreate = function() {
	 		var openCreateCaseModal = $uibModal.open({
        		templateUrl: 'scripts/discover/modal/views/create.case.modal.html',
        		scope: $scope,
             	backdrop: 'static',
             	controller: 'CreateCaseModalController',
			 	windowClass: 'custom-modal createcase-modal'
	        });

	 		openCreateCaseModal.result.then(function () {
	        }, function () {
	        });
	    };
	 	
	 	 $scope.commentsLoader = false;
	 	 $scope.data = {
			 statusComments : ''
	 	 };
	 	/*
	      * @purpose: close comments modal
	      * @created: 07 oct 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.closeComments = function() {
	 		$scope.data.statusComments = '';
	        $scope.tempSeedId = null;
	        $scope.tempCurrentStatus = '';
	        $scope.tempTargetStatus = null;
	        $scope.modalInstance.close("close");
	     };
	 	
		 $scope.forwardLoader = false;
	 	 $scope.data = {
 			statusFwdComments : '',
 			newUserId: null
	 	 };
	 	/*
	      * @purpose: close forward modal
	      * @created: 07 oct 2017
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
	     $scope.closeForward = function() {
	        $scope.tempSeedId = null;
	        $scope.data.statusFwdComments = '';
	     	$scope.data.newUserId = null;
	     	$scope.forwardCaseModalInstance.close("close");
		};

		/*
	      * @purpose: update case status to accepted 
	      * @created: 07 oct 2017
	      * @params: seedId(integer), currentStatus(integer)
	      * @return: null
	      * @author: swathi
	     */
	 	function accept(seedId, currentStatus, selectedStatus) {
	 		var params = {
 				'token' : $scope.token,
				'caseId' : seedId,// Case ID for Each case created
				'statusFrom' : currentStatus,
				'statusTo' : selectedStatus
			};
			$scope.yourWorkDairyPreloader = true;
	    	DiscoverApiService.commonApiCallForAllCaseStatuses(params).then(function() {
	    		$scope.yourWorkDairyPreloader = false;
				HostPathService.FlashSuccessMessage('SUCCESSFUL ACCEPTING CASE STATUS', 'Successfully changed case status to Accepted!');
				$state.go('discover', {}, {reload:true});
    	    }, function() {
	    		$scope.yourWorkDairyPreloader = false;
	    		HostPathService.FlashErrorMessage('ERROR ACCEPTING CASE STATUS', 'Failed to changed case status to Accepted!');
    	    });
		}
	 	
	 	/*
	      * @purpose: update case status to pause 
	      * @created: 07 oct 2017
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */
	 	$scope.pause = function () {
	 		$scope.commentsLoader = true;
			var seedId = $scope.tempSeedId;
			var currentStatus = $scope.tempCurrentStatus;
			var comments = $scope.data.statusComments;
			
			var today = new Date();
			var month = String(today.getMonth() + 1);
			var day = String(today.getDate());
			var year = String(today.getFullYear());
			
			if (month.length < 2) {month = '0' + month;}
			if (day.length < 2) {day = '0' + day;}
			
			var params = {
	        	"caseId": seedId,// Case ID for Each case created
	        	"token": $scope.token,
	        	"statusFrom" : currentStatus,
				"statusTo" : caseSelectedStatus,
	        	"statusComment": comments,// comment added while rejecting case
	        	"notificationDate": day + "-" + month + "-" + year
			};
            DiscoverApiService.commonApiCallForAllCaseStatuses(params).then(function() {
            	$scope.commentsLoader = false;
            	$scope.closeComments();
                HostPathService.FlashSuccessMessage('SUCCESSFUL PAUSING CASE STATUS', 'Successfully changed case status to Paused!');
        		$state.go('discover', {}, {reload:true});
     	    }, function() {
     	    	$scope.commentsLoader = false;
     	    	$scope.closeComments();
    	    	HostPathService.FlashErrorMessage('ERROR PAUSING CASE STATUS', 'Failed to changed case status to Pause!');
     	    });
		};
	 	/*
	      * @purpose: update case status to reject 
	      * @created: 07 oct 2017
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */
	 	$scope.reject = function () {
	 		$scope.commentsLoader = true;
			var seedId = $scope.tempSeedId;
			var currentStatus = $scope.tempCurrentStatus;
			var comments = $scope.data.statusComments;
			
			var today = new Date();
			var month = String(today.getMonth() + 1);
			var day = String(today.getDate());
			var year = String(today.getFullYear());
			
			if (month.length < 2) {month = '0' + month;}
			if (day.length < 2) {day = '0' + day;}
			
			var params = {
				"caseId": seedId,// Case ID for Each case created
	        	"token": $scope.token,
	        	"statusFrom" : currentStatus,
				"statusTo" : caseSelectedStatus,
	        	"statusComment": comments,// comment added while rejecting case
	        	"notificationDate": day + "-" + month + "-" + year
	        };
            DiscoverApiService.commonApiCallForAllCaseStatuses(params).then(function() {
            	$scope.commentsLoader = false;
            	$scope.closeComments();
                HostPathService.FlashSuccessMessage('SUCCESSFUL DECLINING CASE STATUS', 'Successfully changed case status to Declined!');
                loadCases(casesData);
    	    }, function() {
    	    	$scope.commentsLoader = false;
    	    	$scope.closeComments();
                HostPathService.FlashErrorMessage('ERROR DECLINING CASE STATUS', 'Failed to changed case status to Declined!');
            });
		};

		/*
	      * @purpose: update case status to focus 
	      * @created: 05 July 2018
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */

		function focus() {
			var apiUrl = EHUB_API;
			var params = {
				caseId: $scope.tempSeedId,
				token: $scope.token
			};
			apiUrl += "investigation/markAsFocused";
			$scope.yourWorkDairyPreloader = true;
			DiscoverApiService.updateCaseStatus(apiUrl, params).then(function(response) {
				$scope.yourWorkDairyPreloader = false;
			   HostPathService.FlashSuccessMessage('SUCCESSFUL FOCUSED CASE STATUS', response.data.responseMessage);
			   $state.go('discover', {}, {reload:true});
			}, function() {
				$scope.yourWorkDairyPreloader = false;
				HostPathService.FlashErrorMessage('ERROR FOCUSED CASE STATUS', 'Could not add case to focus area!');
				getAllMyCases();/*jshint ignore:line */
			});
	   }

		/*
	      * @purpose: change case status to resolve
	      * @created: 05 July 2018
	      * @params: no
	      * @return: null
	      * @author: swathi
	     */
		$scope.resolve = function() {
			$scope.commentsLoader = true;
		   var seedId = $scope.tempSeedId;
		   var currentStatus = $scope.tempCurrentStatus;
		   var selectedStatus = $scope.tempTargetStatus;
			var comments = $scope.data.statusComments;
			var isResolved = false;

		   var paramsForAll = {
			   'token' : $scope.token,
			   'caseId' : seedId,
			   'statusFrom' : currentStatus,
			   'statusTo' :selectedStatus,
			   "statusComment": comments
		   };

		   DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
			   $scope.commentsLoader = false;
			   $scope.closeComments("success");
			   isResolved = true;
			   $state.go('discover', {}, {reload:true});
			  HostPathService.FlashSuccessMessage('CASE STATUS UPDATED TO RESOLVED', 'Successfully changed the case status to Resolved!');
		   }, function() {
			   $scope.commentsLoader = false;
			   $scope.closeComments("failure");
			   getAllMyCases();/*jshint ignore:line */
			   HostPathService.FlashErrorMessage('ERROR UPDATE CASE STATUS', 'Failed to changed the case status to Resolved!');
		});
	};

		/*
	      * @purpose: update case status to accepted 
	      * @created: 07 oct 2017
	      * @params: seedId(integer), currentStatus(integer)
	      * @return: null
	      * @author: swathi
	     */
		function cancel(seedId, currentStatus, selectedStatus) {
			
			var paramsForAll = {
				'token' : $scope.token,
				'caseId' : seedId,
				'statusFrom' : currentStatus,
				'statusTo' :selectedStatus
			};

		   $scope.yourWorkDairyPreloader = true;
		   DiscoverApiService.commonApiCallForAllCaseStatuses(paramsForAll).then(function() {
			$scope.yourWorkDairyPreloader = false;
			$state.go('discover', {}, {reload:true});
		}, function() {
		});
	}

	 	$scope.userList = [];
		var userListParams = {
			token: $scope.token
	    };
		 /* Get List of Users */
		
		DiscoverApiService.getUserList(userListParams).then(function(response){
	 		$scope.usersLength = response.data.paginationInformation.totalResults;
	 		var data = {
 				token: $scope.token,
 				pageNumber: 1,
 				recordsPerPage: $scope.usersLength 
	 		};
	 		DiscoverApiService.getUserList(data).then(function(response){
	 			$scope.userList = response.data.result;
	 		},function(){
		 	});
	 	},function(){
	 	});
		/*
	      * @purpose: on change user order by option 
	      * @created: 16 oct 2017
	      * @params: option(string)
	      * @return: no
	      * @author: swathi
	     */
		$scope.getUserOrderBy = function(option){
			if(option){
				var data = {
	 				token: $scope.token,
	 				pageNumber: 1,
	 				recordsPerPage: $scope.usersLength ,
	 				orderBy: option
		 		};
				DiscoverApiService.getUserList(data).then(function(response){
		 			$scope.userList = response.data.result;
		 		},function(){
			 	});
			}
		};
	 	/*
	      * @purpose: forward case to another user incoming tray
	      * @created: 07 oct 2017
	      * @params: none
	      * @return: no
	      * @author: swathi
	     */
	 	$scope.forward = function forward() {
	 		$scope.forwardLoader = true;
			var seedId = $scope.tempSeedId;
			var newUserId = $scope.data.newUserId;
			var statusComment = $scope.data.statusFwdComments;
			var params = {
					"caseId": seedId,
		        	"token": $scope.token,
		        	"newUserId": newUserId,
		        	"statusComment": statusComment
		        };
			DiscoverApiService.forwardCase(params).then(function() {
				$scope.forwardLoader = false;
				$scope.closeForward();
				HostPathService.FlashSuccessMessage('SUCCESSFUL FORWADING THE CASE', 'Successfully forwarded the case!');
				$state.go('discover', {}, {reload:true});
		    }, function(error) {
		    	$scope.forwardLoader = false;
		    	$scope.closeForward();
				HostPathService.FlashErrorMessage('ERROR FORWARDING THE CASE', error.responseMessage || 'Failed to forward the case!');
				loadCases(casesData);
	        });
		};
		//-----------------------------------------------------------------------
		/**
		 * @purpose: open modal for filter case
	     * @created: 14 Dec 2017
	     * @params: no
	     * @return: null
	     * @author: prasanthi
		 */
		var openCasefiltersModal;
	  	$scope.OpenfilterCaseDataModal =function() {		
	  		$scope.workDairyToggle = false;
	  		openCasefiltersModal = $uibModal.open({
	             templateUrl: 'scripts/common/modal/views/filter.case.modal.html',
	             scope: $scope,
		         controller: 'filterCaseModalController',
	            // backdrop: 'static',
				 windowClass: 'custom-modal comments-modal',
				 resolve: {
		            	isOnlyKeyword: function () {
		                  return false;
		                },
		                statusList:function(){
		                	return [
			                		
							    	{status:"Submitted", code:2},
							    	{status:"Acknowledge", code:3}
						    	];
		                },
		                keysTofilter:function(){
		                	return $scope.keysTofilter;
		                },
		                FilteredKeyword:function(){
		                	return $scope.FilteredKeyword;
		                },
		                FilteredKey : function(){
		                	return $scope.FilteredKey;
		                },
		                RiskRatio : function(){
		                	return false;
		                },
		                onlySearch : function(){
		                	return false;
		                },
		                CaseDiarySelections : function(){
		                	return [
		                		{"MCDtype" : $scope.type},
		                		{"MCDorderby" : $scope.orderby},
		                		{"MCDorderin" : $scope.orderin},
		                		{"MCDstatus" : $scope.status}
		                		];
		                },
		                StatusOption :function(){
		                	return $scope.StatusOption;
		                },
		                AllNames : function(){
		                	return $scope.AllNames;
		                },
		                isdateRequired: function(){
		                	return false;
		                },
		                itemsPerPage: function(){
		                	return false;
		                }
		              }
	         });

	  		openCasefiltersModal.result.then(function (response) {	 
	  			if(response)
	  				{handleFiltersApplied(response);}
	         }, function () {
	         });
	         $scope.modalInstance = openCasefiltersModal;
	         return openCasefiltersModal.result;
	     };
	  	//-----------------------------------------------------------------------
	  	/**
	  	 * Function to apply filters
	  	 */
	  	var searchCaseKeyword,filtersApplied;
	  	function handleFiltersApplied(filersData){
	  		$scope.type = filersData.filterApplied.type;
	  		$scope.FilteredKey = filersData.filterApplied.priority;
	  		$scope.orderby =filersData.filterApplied.orderBy;
	  		$scope.orderin = filersData.filterApplied.orderIn;
	  		if(filersData.filterApplied.status && filersData.filterApplied.status.length > 0 )
	  			{$scope.StatusOption = filersData.filterApplied.status[0];}
	  		else
	  			{$scope.StatusOption = 0;}
	  		
	  		filtersApplied = jQuery.extend(true,{}, filersData.filterApplied);
	  		$scope.FilteredKeyword = "";
	  		if(filersData.caseSearchKeyword){
	  			$scope.FilteredKeyword = filersData.caseSearchKeyword;
	  			searchCaseKeyword = filersData.caseSearchKeyword;
	  		}else if(filersData.filterApplied){
	  			searchCaseKeyword = undefined;	  			
	  		}
	  		casesData = jQuery.extend(true,{}, filersData.filterApplied);	
  			if(!casesData.status){
  				casesData.status = [2,3];
  			}
  			casesData.recordsPerPage= 10;
  			casesData.pageNumber= $scope.pageNum;	  			    
  			loadCases(casesData);
		  }
	  	
	  	
}