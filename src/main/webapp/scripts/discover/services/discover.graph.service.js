'use strict';
angular.module('ehubApp')
	   .service('DiscoverService', discoverService);

		discoverService.$inject = [
			'HostPathService',
			'HotTopicsConst',
			'TimeScaleBubbleConst',
			'$state',
			'$timeout'
		];
		
		function discoverService(
				HostPathService,
				HotTopicsConst,
				TimeScaleBubbleConst,
				$state,
				$timeout){
			
			/*
			 * @purpose: setting world chart for top locations grid
			 * @created: 14 sep 2017
			 * @params: locationsData(object) 
			 * @returns: no 
			 * @author: swathi
			 */
			/* jshint validthis: true */
			var markersLoaded;
			this.setWorldMap = function(locationsData, zoomOptions){
				var isGeocoder;
				try {
					   new google.maps.Geocoder();
					   isGeocoder = true;
					}
				catch(err) {
						isGeocoder = false;
					}
				if(isGeocoder){
				var keys = Object.keys(locationsData);
				var lastKey = keys[keys.length - 1];
				
				var map = L.map( 'map', {
			        center: [20.0, 5.0],
			        maxBounds: L.latLngBounds([-90, -180], [90, 180]),
			        minZoom: zoomOptions.minZoom,
			        zoom: zoomOptions.zoom
			    });
				
	
				$timeout(function(){
					L.tileLayer.grayscale('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/hybrid.day/{z}/{x}/{y}/{size}/{format}?app_id=31fuSI6p3gIEf9ePVvdC&app_code=a6Rg3ihvsn38pyglOgdDvg&lg={language}', {
						attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
						subdomains: '1234',
						mapID: 'newest',
						app_id: '31fuSI6p3gIEf9ePVvdC',
						app_code: 'a6Rg3ihvsn38pyglOgdDvg',
						base: 'aerial',
						maxZoom: 20,
						type: 'maptile',
						language: 'eng',
						format: 'png8',
						size: '256',
						bounds: [[-90, -180], [90, 180]],
						noWrap:true
//						continuousWorld: false
					}, L.layerGroup()).addTo( map );
						
					var count =0;
			    angular.forEach(locationsData, function(v, k){
			    	count++;
			    	setTimeout(function(){
			    		
				 	var markersData = {};
		        	var geocoder =  new google.maps.Geocoder();
		        	geocoder.geocode( { 'address': k}, function(results, status) {
			          if (status == google.maps.GeocoderStatus.OK) {
			        	  markersData = {
			            	"name": k,
			                "url": "https://en.wikipedia.org/wiki/" + k,
			                "lat": results[0].geometry.location.lat(),
			                "lng": results[0].geometry.location.lng()
			                };
			            L.marker( [markersData.lat, markersData.lng] )
				          .bindPopup( '<a href="' + markersData.url + '" target="_blank">' + markersData.name + '</a>' )
				          .addTo( map );
			          }else if (k == 'New York') {
			        	  markersData = {
			            	"name": k,
			                "url": "https://en.wikipedia.org/wiki/" + k,
			                "lat": 43.299429,
			                "lng": -74.217933
			                };
			            L.marker( [markersData.lat, markersData.lng] )
				          .bindPopup( '<a href="' + markersData.url + '" target="_blank">' + markersData.name + '</a>' )
				          .addTo( map );
			          } else {
			          }
		        	});
			    },1000*count);
			    	if(lastKey == k){
		        		markersLoaded = true;
		        	}
			    });
				
				}, 0);
				return map;
			}
			};
			this.getWorldMap = function(){
				return markersLoaded;
			};
			
			/*
			 * @purpose: setting cloud chart for hot topics grid 
			 * @created: 15 sep 2017
			 * @params: options(object)
			 * @returns: no
			 * @author: swathi
			 */
			this.setHotTopicsCloudChart = function(options){
				$timeout(function(){
					$("#tagcloud").empty();
					hotTopic(options);
				}, 0);
				
				/*responsiveness*/
		        $(window).on("resize",  function () {
		        	if($state.current.name == 'discover'){
		        		$("#tagcloud").empty();
		        		hotTopic(options);
		        	}
		        });
			};
			
			function hotTopic(options){
				var colorScale = d3.scaleOrdinal().range(HotTopicsConst.colors);
				var lis = "";
				angular.forEach(options.data1,function(d){
					d.color = HotTopicsConst.colorObj[d.group.toLowerCase()] ? HotTopicsConst.colorObj[d.group.toLowerCase()] : colorScale(d.group);
					d.size  =d.count;
					d.text = d.name;
					lis = lis+'<a class="hottopictag" href="javascript:void(0);" style="color:'+colorScale(d.group)+';line-height:21px">'+d.name+'</a>';                  
				});
				var width = $('#hottopicsdiv').width();
				var height = document.getElementById('hottopicsdiv').clientHeight;				
				var setTemp = width >= height ? height : width;
				var radius = setTemp/2;
//				$("#tagcloud").append(lis);
				cloudTag({width:width,height:height,data:options.data1,ballSize: radius - HotTopicsConst.ballSize, "id":"#tagcloud"});
			}
			/*
			 * @purpose: setting time scale bubbles chart for my entities grid 
			 * @created: 16 sep 2017 
			 * @params: data(object) 
			 * @returns: no 
			 * @author: swathi
			 */
			var timescalebubbleData = [];
			this.setMyEntitiesTimeScaleBubbleChart = function(data,container){
				if(!container){
					container='#myEntitiesChart';
				}
		         timescalebubbleData = data; 
		         setTimeout(function(){
	              new timescalebubbleChart({
	                    container:container,
	                    data: timescalebubbleData,
	                    header: TimeScaleBubbleConst.header,
	                    height: TimeScaleBubbleConst.height,
	                    isheader: TimeScaleBubbleConst.isheader,
	                    xticks: TimeScaleBubbleConst.xticks,
	                    margin: TimeScaleBubbleConst.margin                        
		            });
		         },0);
		         
		         /*responsiveness*/
			        $(window).on("resize",  function () {
			                $(data.container).empty();
			                setTimeout(function(){
			 	               new timescalebubbleChart({
			 	                    container:container,
			 	                    data: timescalebubbleData,
			 	                    header: TimeScaleBubbleConst.header,
			 	                    height: TimeScaleBubbleConst.height,
			 	                    isheader: TimeScaleBubbleConst.isheader,
			 	                    xticks: TimeScaleBubbleConst.xticks,
			 	                    margin: TimeScaleBubbleConst.margin                        
			 		            },0);
			 		         });
//			            }
			        });
			};
			/*
			 * @purpose: setting bubbles chart for entity in focus grid 
			 * @created: 16 sep 2017 
			 * @params: options(object) 
			 * @returns: no 
			 * @author: swathi
			 */
			this.setEntitiesInFocusBubblesChart = function(options){
				loadBubbleChart(options);
		        /*responsiveness*/
		        $(window).on("resize",  function () {
		            if($(options.container).find("svg").length != 0){
		                $(options.container).empty();
		                new bubbleChart(options);
		            }
		        });
		        /*Function to load data to plot Bubble chart*/
				 
		        function loadBubbleChart(options) {
		            $(options.container).siblings(".headerDiv").html(options.Header);
		            $timeout(function(){
		            	new bubbleChart(options);
		            }, 0);
		        } 
			};
		}