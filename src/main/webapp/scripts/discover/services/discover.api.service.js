'use strict';
elementApp
	   .factory('DiscoverApiService', discoverApiService);

		discoverApiService.$inject = [
			'$http',
			'EHUB_API',
			'$q'
		];
		
		function discoverApiService(
				$http,
				EHUB_API,
				$q){
	
			return {
				getAllCaseSummaryByLimit: getAllCaseSummaryByLimit,
				getAllCaseSummaryByStatusList: getAllCaseSummaryByStatusList,
				getAllCases:getAllCases,
				caseListFullTextSearch:caseListFullTextSearch,
				getAllMyCases: getAllMyCases,
				searchMyCases:searchMyCases,
				getAllCaseFocus: getAllCaseFocus,
				updateCaseStatus: updateCaseStatus,
				getAllIncomingTrayByUserId: getAllIncomingTrayByUserId,
				getUserList: getUserList,
				forwardCase: forwardCase,
				countCaseStatus: countCaseStatus,
				removeCaseFromFocused:removeCaseFromFocused,
				analystMetrix: analystMetrix,
				getMyEntities: getMyEntities,
				searchCompanyLogo: searchCompanyLogo,
				getRiskScoreData:getRiskScoreData,
				caseDropDownBasedOnStatus:caseDropDownBasedOnStatus,
				commonApiCallForAllCaseStatuses:commonApiCallForAllCaseStatuses
			};
			/*
		     * @purpose: get cases summary by limit
		     * @created: 19 jan 2018
		     * @params: token(object), count(integer)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllCaseSummaryByLimit(token, count){
				var apiUrl = EHUB_API + 'workflow/es/cases/summary/' + count;
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: token
		        });
		        return(request
		                .then(getAllCaseSummaryByLimitSuccess)
		                .catch(getAllCaseSummaryByLimitError));
		
		        /*getAllCaseSummaryByLimit error function*/
		        function getAllCaseSummaryByLimitError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getAllCaseSummaryByLimit success function*/
		        function getAllCaseSummaryByLimitSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get cases summary by status list
		     * @created: 23 feb 2018
		     * @params: params(object), data(object), limit
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllCaseSummaryByStatusList(params, data, limit){
				var apiUrl = EHUB_API + 'workflow/es/cases/casesSummary/' + limit;
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params,
		            data: data
		        });
		        return(request
		                .then(getAllCaseSummaryByStatusListSuccess)
		                .catch(getAllCaseSummaryByStatusListError));
		
		        /*getAllCaseSummaryByStatusList error function*/
		        function getAllCaseSummaryByStatusListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		       /*getAllCaseSummaryByStatusList success function*/
		        function getAllCaseSummaryByStatusListSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of all cases
		     * @created: 15 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllCases(params, data){
				var apiUrl = EHUB_API + 'investigation/caseList';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data: data,
		            params: params
		        });
		        return(request
		                .then(getAllCasesSuccess)
		                .catch(getAllCasesError));

		        /*getAllMyCases error function*/
		        function getAllCasesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getAllMyCases success function*/
		        function getAllCasesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: search list of all cases
		     * @created: 15 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function caseListFullTextSearch(params,data){
				var apiUrl = EHUB_API + 'investigation/caseListFullTextSearch';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data:data,
		            params: params
		        });
		        return(request
		                .then(caseListFullTextSearchSuccess)
		                .catch(caseListFullTextSearchError));

		        /*caseListFullTextSearch error function*/
		        function caseListFullTextSearchError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*caseListFullTextSearch success function*/
		        function caseListFullTextSearchSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of my cases
		     * @created: 15 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllMyCases(params){
				var apiUrl = EHUB_API + 'investigation/getMyCases';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getAllMyCasesSuccess)
		                .catch(getAllMyCasesError));

		        /*getAllMyCases error function*/
		        function getAllMyCasesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getAllMyCases success function*/
		        function getAllMyCasesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of my cases
		     * @created: 15 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function searchMyCases(params){
				var apiUrl = EHUB_API + 'investigation/searchMyCases';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(searchMyCasesSuccess)
		                .catch(searchMyCasesError));

		        /*searchMyCases error function*/
		        function searchMyCasesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*searchMyCases success function*/
		        function searchMyCasesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of my case focus
		     * @created: 15 sep 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllCaseFocus(params){
				var apiUrl = EHUB_API + 'investigation/getAllFocusedCases';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getAllCaseFocusSuccess)
		                .catch(getAllCaseFocusError));

		        /*getAllCaseFocus error function*/
		        function getAllCaseFocusError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		        /*getAllCaseFocus success function*/
		        function getAllCaseFocusSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: update cases status
		     * @created: 18 sep 2017
		     * @params: apiPath(string), params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function updateCaseStatus(apiPath, params){
				var apiUrl = apiPath;
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(updateCaseStatusSuccess)
		                .catch(updateCaseStatusError));

		        /*updateCaseStatus error function*/
		        function updateCaseStatusError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*updateCaseStatus success function*/
		        function updateCaseStatusSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of incoming tray by user id
		     * @created: 07 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getAllIncomingTrayByUserId(params){		
				var apiUrl = EHUB_API + 'investigation/incomingTray';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getAllIncomingTrayByUserIdSuccess)
		                .catch(getAllIncomingTrayByUserIdError));

		        /* getAllIncomingTrayByUserId error function */
		        function getAllIncomingTrayByUserIdError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }

		        /* getAllIncomingTrayByUserId success function */
		        function getAllIncomingTrayByUserIdSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get list of Users
		     * @created: 07 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getUserList(params){
				var apiUrl = EHUB_API + 'user/getUserListing';
				var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getUserListSuccess)
		                .catch(getUserListError));

		        /* getlistofUsers error function */
		        function getUserListError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		             /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }

		         /* getlistofUsers success function */
		        function getUserListSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: forward case
		     * @created: 07 oct 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function forwardCase(params){		
				var apiUrl = EHUB_API + 'investigation/forwardCase';
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(forwardCaseSuccess)
		                .catch(forwardCaseError));

		        /* forwardCase error function */
		        function forwardCaseError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }
		        /* forwardCase success function */
		        function forwardCaseSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: count case status
		     * @created: 18 sep 2017
		     * @params: apiPath(string), params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function countCaseStatus(){
				return {
					"paused":1,
					"submitted":82,
					"acknowledge":3,
					"rejected":1,
					"accepted":4,
					"focus":1
				};		
			}
			
			/*
		     * @purpose: removeCaseFromFocused
		     * @created: 20 dec 2017
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: prasanthi
		    */
			function removeCaseFromFocused(params){		
				var apiUrl = EHUB_API + 'investigation/removeCaseFromFocused';
		        var request = $http({
		            method: "DELETE",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(removeCaseFromFocusedSuccess)
		                .catch(removeCaseFromFocusedError));

		        /* removeCaseFromFocused error function */
		        function removeCaseFromFocusedError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }
		        /* removeCaseFromFocused success function */
		        function removeCaseFromFocusedSuccess(response) {
		            return(response);
		        }
			}
			
			/*
		     * @purpose: analyst Metrix 
		     * @created: 06 jan 2018
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function analystMetrix(params){		
				var apiUrl = EHUB_API + 'investigation/analystMetrics';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(analystMetrixSuccess)
		                .catch(analystMetrixError));

		        /* analystMetrix error function */
		        function analystMetrixError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		           /* Otherwise, use expected error message. */
		            return($q.reject(response.data.message));
		        }
		        /* analystMetrix success function */
		        function analystMetrixSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: get my Entities
		     * @created: 22 jan 2018
		     * @params: token(object), caseId(integer)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function getMyEntities(token, caseId){
				var apiUrl = EHUB_API + 'workflow/graph/case/' + caseId +'/entities';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: token
		        });
		        return(request
		                .then(getMyEntitiesSuccess)
		                .catch(getMyEntitiesError));

		        /* getMyEntities error function */
		        function getMyEntitiesError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /* Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		        /* getMyEntities success function */
		        function getMyEntitiesSuccess(response) {
		            return(response);
		        }
			}
			/*
		     * @purpose: search Company Logo
		     * @created: 30 jan 2018
		     * @params: token(object), data(object)
		     * @return: success, error functions
		     * @author: swathi
		    */
			function searchCompanyLogo(data, token){
				var apiUrl = EHUB_API + 'search/getData?token='+ token + '&searchFlag=search';
			      
		        var request = $http({
		            method: "POST",
		            url: apiUrl,
		            data: data
		        });
		        return(request
		                .then(searchCompanyLogoSuccess)
		                .catch(searchCompanyLogoError));
		
		        /*searchCompanyLogo error function*/
		        function searchCompanyLogoError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }
		
		        /*searchCompanyLogo success function*/
		        function searchCompanyLogoSuccess(response) {
		            return(response);
		        }
			}
			
			
			
			/*
		     * @purpose: get riskScore
		     * @created: 20th Feb
		     * @params: params(object)
		     * @return: success, error functions
		     * @author: Varsha
		    */
			function getRiskScoreData(params){
				var apiUrl = 'https://4947kpl4ib.execute-api.eu-west-1.amazonaws.com/dev/risk/score/%7Bentity-id%7D';
		        var request = $http({
		            method: "GET",
		            url: apiUrl,
		            params: params
		        });
		        return(request
		                .then(getRiskScoreDataSuccess)
		                .catch(getRiskScoreDataError));

		        /*getRiskScoreData error function*/
		        function getRiskScoreDataError(response) {
		            if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
		            }
		            /*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
		        }

		       /*getRiskScoreData success function*/
		        function getRiskScoreDataSuccess(response) {
		            return(response);
		        }
			}
		

			
		  function caseDropDownBasedOnStatus(params){
			var apiUrl = EHUB_API + 'investigation/getTransitions';
			  var request = $http({
				  method : 'GET',
				  url : apiUrl,
				  params : params
			  });
			  return (request
							.then(caseDropDownBasedOnStatusSuccess)
							.catch(caseDropDownBasedOnStatusError));

				/*caseDropDownBasedOnStatus error function */
				function caseDropDownBasedOnStatusError(response){
					if (!angular.isObject(response.data) || !response.data.message) {
		                return($q.reject(response.data));
					}
					/*Otherwise, use expected error message.*/
		            return($q.reject(response.data.message));
				}

				/*caseDropDownBasedOnStatus success function*/
		        function caseDropDownBasedOnStatusSuccess(response) {
		            return(response);
		        }
		  }

		  /* Common API for all case statuses*/
		  function commonApiCallForAllCaseStatuses(params){
			  var apiUrl = EHUB_API + 'investigation/changeCaseSeedStatus';
			  var request = $http({
				  method : 'POST',
				  url : apiUrl,
				  params : params
			  });
			  return (request
							.then(commonApiCallForAllCaseStatusesSuccess)
							.catch(commonApiCallForAllCaseStatusesError));

			/*commonApiCallForAllCaseStatusesError error function*/
			function commonApiCallForAllCaseStatusesError(response){
				if(!angular.isObject(response.data) || !response.data.message){
					return ($q.reject(response.data.message));
				}
				return($q.reject(response.data.message));
			}

			/*commonApiCallForAllCaseStatusesSuccess success function*/
			function commonApiCallForAllCaseStatusesSuccess(response){
				return response;
			}
		  }
		  
}
