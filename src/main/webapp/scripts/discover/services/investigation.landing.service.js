'use strict';
angular.module('ehubApp')
	   .service('InvestigationLandingService', investigationLandingService);

		investigationLandingService.$inject = [
			'ActRiskRatioConst'
			
		];
		
		function investigationLandingService(
				ActRiskRatioConst
				){
			
			var clusterbubbleData = [];
			/* Load groupedColumn chart for COMPARISION ANALYSIS section */
			 /* jshint validthis: true */
	        this.groupedColumn = function(options,data)
	        {
	            var columnData = [];
	            loadgroupedColumnChart(options,data);
	            $(window).on("resize", function () {
	            	 $("#chartContentDiv").empty();
	            	new groupedColumChart(options,data);
	            });
	            function loadgroupedColumnChart(options,data) {
	            	if(!data){
		                d3.json(options.uri, function (error, data) {
		                    columnData = handleData(data, "model", "", "bar");
		                    options.data = columnData;
		                     new groupedColumChart(options);
		                });
	            	}else{
	            		columnData = handleData(data, "model", "", "bar");
	                    options.data = columnData;
	                    new groupedColumChart(options);
	            	}
	            }
	            /**
	             *Function to handle data
	             */
	            function handleData(data) {
	                var str = JSON.stringify(data);
	                str = str.replace(/model/g, 'x');
	                var object = JSON.parse(str);
	                return object;
	            }
	        };
	        
        	/**
	        *Function to load data to plot ClusterBubble chart 
	        */
	        
	        this.loadsankey = function(options,data){
	        	$(options.container).siblings(".headerDiv").html(options.header);
	        	 $(window).on("resize",  function () {
		                if($(options.container).find("svg").length != 0){
	 		                $(options.container).empty();
	 		                var current_options = jQuery.extend(true, {}, options);
	 		                new sankey_chart(current_options);
		                }
		            });
	        	if(!data){
		        	d3.json(options.Uri, function (error, data) {
		                options.data = data;
		                 new sankey_chart(options);
		            });
	        	}else{
	        		  options.data = jQuery.extend(true, {}, data);	        		
	        		  var current_options = jQuery.extend(true, {}, options);
		             new sankey_chart(current_options);
	        	}
	        };
	        
	        /**
			 * Function to give options for dynamic and temporal charts
			 */
			this.plotdynamictemporal = function() {
			    var list = '<div class="temporal-list-div"><ul class="tool-list-wrapper list-inline" style="text-align: center">';
			    var accountArray = ["Account1", "Account2", "Account3", "Account4"];
			    $.each(accountArray, function (i, d) {
			        list = list + '<li><div class="clearfix btn temporalsubmenu" style="background-color: #11B099;padding: 10px; height: initial;"><h5  style="padding: 0px;" class=" text-uppercase">' + d + '</h5> </div></li>';
			    });
			    list = list + '</ul></div><div id="temporalChartDiv"></div>';
			    $("#chartContentDiv").empty();
			    $("#chartContentDiv").append(list);
			};
			
		    /**
	         *Function to call a function to plot Bubble chart and to call function on window resize
	         */
	        this.chartBubble = function(options,data)
	        {
	           
	            loadBubbleChart(options);
	            $(window).on("resize",  function () {
	                if($(options.container).find("svg").length != 0){
		                $(options.container).empty();
		                new bubbleChart(options);
	                }
	            });
	            /**
	             *Function to load data to plot Bubble chart 
	             */
	            function loadBubbleChart(options) {
	                $(options.container).siblings(".headerDiv").html(options.Header);
	                if(!data){
		                d3.json(options.uri, function (error, data) {
		                	var bubbleData = handleBubbleData(data);
		                    options.data = bubbleData;
		                    new bubbleChart(options);
		                });
	                }else{
	                	var bubbleData = handleBubbleData(data);
	                    options.data = bubbleData;
	                    new bubbleChart(options);
	                }
	            }
	            /**
	             *Function to handle data according to format of Bubble Chart library
	             */
	            function handleBubbleData(data) {
	                var keys = d3.keys(data);
	                var newData = [];
	                $.each(keys, function (i, d) {
	                    var newVlaues = [];
	                    var innerKeys = d3.keys(data[d]);
	                    $.each(innerKeys, function (i1, d1) {
	                        newVlaues.push({
	                            "name": d1,
	                            "size": data[d][d1]
	                        });
	                    });
	                    newData.push({
	                        "name": d,
	                        "children": newVlaues
	                    });
	                });
	                var finalData = {
	                    "children": newData
	                };
	                return finalData;
	            }
	        };
	        
	        /**
	    	 * Function to load ClusterBubble chart
	    	 */
		     this.ClusterBubble =  function(options,data) {    	
		        loadclusterBubbleChart(options,data);
		        /* responsiveness */
		        $(window).on("resize", function() {
		            $("#riskRatioBubbleChart").empty();
		            loadclusterBubbleChart(options,data);
		        });
		        /**
		    	 * Function to handle data according to format of ClusterBubble Chart
		    	 * library
		    	 */
		        function handleClusteredChartData(data) {
		            var lowRiskArray = [];
		            var mediumRiskArray = [];
		            var highRiskArray = [];
		            angular.forEach(data, function(d) {
		                var cumilativeRisk = getriskRation(d.direct, d.indirect, d.transactional);
		                if (cumilativeRisk > ActRiskRatioConst.highRiskVal) {
		                    highRiskArray.push({
		                        "name": d.name,
		                        "group": "HIGH",
		                        "size": cumilativeRisk
		                    });
		                } else if (cumilativeRisk > ActRiskRatioConst.mediumRiskVal) {
		                    mediumRiskArray.push({
		                        "name": d.name,
		                        "group": "MEDIUM",
		                        "size": cumilativeRisk
		                    });
		                } else {
		                    lowRiskArray.push({
		                        "name": d.name,
		                        "group": "LOW",
		                        "size": cumilativeRisk
		                    });
		                }
		            });
		            var finalData = [{
		                "label": "LOW",
		                "value": lowRiskArray.length,
		                "children": lowRiskArray
		            }, {
		                "label": "MEDIUM",
		                "value": mediumRiskArray.length,
		                "children": mediumRiskArray
		            }, {
		                "label": "HIGH",
		                "value": highRiskArray.length,
		                "children": highRiskArray
		            }];
		            return finalData;
		        }
		        /* Calculate risk ratio */
		        function getriskRation(direct, indirect, transactional) {
		            return (1 - [(1 - direct) * (1 - indirect) * (1 - transactional)]) * 100;
		        }
		        /**
		    	 * Function to load data to plot ClusterBubble chart
		    	 */
		        function loadclusterBubbleChart(options,data) {
		        	if(!data){
		        	if (options.Uri) {
		            	d3.json(options.Uri, function(error, data) {
		                    clusterbubbleData = handleClusteredChartData(data.body);
		                    options.data = clusterbubbleData;
		                    new clusterbubbleChart(options);

		            	});	
		            } else {
	    	        	clusterbubbleData = handleClusteredChartData(options.dataParam.body);
	    	            options.data = clusterbubbleData;
	    		     

	    	            new clusterbubbleChart(options);
		            }
		        	}else{
		        		clusterbubbleData =data;
		        		options.data = data;
	                     new clusterbubbleChart(options);
		        		
		        	}
		        }
		    };
			
		}