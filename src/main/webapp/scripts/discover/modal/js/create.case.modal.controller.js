'use strict';
angular.module('ehubApp')
	   .controller('CreateCaseModalController', createCaseModalController);

		createCaseModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModalInstance',
			'EnrichApiService',
			'HostPathService',
			'Flash',
			'EHUB_FE_API',
			'TopPanelApiService',
			'$uibModal'
		];
		
		function createCaseModalController(
			$scope, 
			$rootScope,
			$state,
			$uibModalInstance,
			EnrichApiService,
			HostPathService,
			Flash,
			EHUB_FE_API,
			TopPanelApiService,
			$uibModal) {  
			
			$scope.token = $rootScope.ehubObject.token;
			$scope.data = {};
			$scope.data.priority = "0";
			$scope.healthValues = [];
			/* healthValues ranges from 1 to 100 */
		    for (var i = 100; i >= 0; i--) { 
		    	$scope.healthValues.push(i);
		    } 
		    /*
		     * @purpose: close create case modal
		     * @created: 21 jul 2017
		     * @params: no
		     * @return: success, error functions
		     * @author: swathi
		    */
		    $scope.closeCreateCase = function() {
		    	$scope.data.caseName = '';
		    	$scope.data.description = '';
		    	$scope.data.priority = '';
//		    	$scope.data.health = '';
		    	$scope.data.jsonFile = null;
		    	document.getElementById("jsonFile").value = "";
		    	$uibModalInstance.dismiss('close');
		    };
		    /*
		     * @purpose: create case
		     * @created: 31 jan 2018
		     * @params: no
		     * @return: success, error functions
		     * @author: swathi
		    */
			$scope.createCase = function createCase() {

				var fileContents;
				var file = document.getElementById("jsonFile").files[0];
				
				var filename=file.name;
				var index = filename.lastIndexOf(".");
				var strsubstring = filename.substring(index, filename.length)?filename.substring(index, filename.length).toLowerCase():filename.substring(index, filename.length);
				
				if(file.size > 10240000){
					HostPathService.FlashErrorMessage('The file size exceeds the limit allowed (10MB)', file.name);
				}
				else if (strsubstring == '.pdf' || strsubstring == '.doc' || strsubstring == '.csv' || strsubstring == '.docx')
                {
					if (file) {
						$scope.createCaseLoader = true;
						var reader = new FileReader();
						reader.readAsText(file, "UTF-8");
						reader.onload = function (evt) {
							fileContents = evt.target.result;
							var jsonData = JSON.parse(EnrichApiService.CSV2JSON(fileContents));
							var params = {
								"token": $scope.token,
							};
							var data = {
								"name": $scope.data.caseName,
								"description": $scope.data.description,
								"remarks": jsonData[0].Remarks,
								"priority": $scope.data.priority,
//								"health": $scope.data.health,
								"type": jsonData[0]["Is Customer Individual Or Corporate"] + '-' + jsonData[0].Type,
								"directRisk": 0,
								"indirectRisk": 0,
								"transactionalRisk": 0
							};
							EnrichApiService.createNewCase(data, params).then(function(createCaseStatus) {
								if(createCaseStatus.status == '200'){   
								
									processDoc(file,createCaseStatus.data.id);
//								
								}else{
									HostPathService.FlashErrorMessage('ERROR UPLOADING CASE','Error');
								}
							}, function(error) {
								$scope.createCaseLoader = false;
								 
						        	  HostPathService.FlashErrorMessage('ERROR UPLOADING CASE',error.responseMessage);
										
						         
						  });
						};
					}
				}
				else
				{
					HostPathService.FlashErrorMessage("UNSUPORTED FILE FORMAT", "Invalid File Type, allowed file types are: csv, word, pdf");
					$scope.createCaseLoader = false;
				}
			};
		    function processDoc(file,caseId){
//					
					
					var params = {
		                "token": $rootScope.ehubObject.token,
		                "caseId":caseId
					};
		            var data = {
		                uploadFile: file
		            };
		            EnrichApiService.createCaseFromQuestioner(params, data).then(function(response) {
		            	if(response == 'Cancelled'){
		            		$scope.createCaseLoader = false;
		            		 HostPathService.FlashErrorMessage('Time limit exceed for uploading file.', '');
		            	}else{
		            		$scope.createCaseLoader = false;
					    	HostPathService.FlashSuccessMessage('QUESTIONNAIRE UPLOADED SUCCESSFULLY','Document has been uploaded');
					    	$scope.closeCreateCase();
					    	$state.go('discover', {}, {reload:true});
		            	}

					},function(e){
						$scope.createCaseLoader = false;
						
						if(e.responseMessage ==="Document is not matched with any template"){
							var ext = file.name.split('.').pop().toLowerCase();
							if ($.inArray(ext, ['pdf']) == -1) { //checking uploaded file type
								HostPathService.FlashErrorMessage("ERROR", "Document not matched with any template");
								$uibModalInstance.dismiss('close');
								$rootScope.docsLoader = false;
								return;
							}
							else
							{
								$uibModalInstance.dismiss('close');
								$scope.openModal(file,caseId);	
							}
			          }else{
			        	  HostPathService.FlashErrorMessage('ERROR UPLOADING CASE', e.responseMessage);
							$scope.closeCreateCase();
			          }
					});
				
             }
		    /*
			 * @purpose: to open modal popup To confirm to navigate to docparser page
			 * @created: 7th sep 2018
			 * @params: params
			 * @returns: data
			 * @author: prasanthi
			 */
			 $scope.openModal = function (data,caseId) {
				 data.caseId =caseId;
		         var dataModal = $uibModal.open({
					templateUrl: function(){
						if(window.location.hash.indexOf("#!/") < 0){
			        		return 'scripts/common/modal/views/confirmationForDocparserPage.modal.html';
						 } else{
			       		    return '../scripts/common/modal/views/confirmationForDocparserPage.modal.html';
						 }
						},
					controller: 'ConfirmationForDocparser',
					size: 'xs',
					windowClass: 'custom-modal update-entities-modal related-person-modal data-popup-wrapper',
					resolve: {
						data: function () {
							return data;
						}
					}
				});
				dataModal.result.then(function () {}, function () {});
		  };
		}
		