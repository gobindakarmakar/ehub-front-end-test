'use strict';
angular.module('ehubApp')
	   .controller('filterCaseModalController', filterCaseModalController);

filterCaseModalController.$inject = [
			'$scope', 
			'$rootScope',
			'$state',
			'$uibModalInstance',
			'EnrichApiService',
			'HostPathService',
			'Flash',
			'isOnlyKeyword',
			'statusList'
		];
		
function filterCaseModalController(
			$scope, 
			$rootScope,
			$state,
			$uibModalInstance,
			EnrichApiService,
			HostPathService,
			Flash,
			isOnlyKeyword,
			statusList) {  
			$scope.isOnlyKeyword=isOnlyKeyword;			
			$scope.statusList =statusList;
			$scope.token = $rootScope.ehubObject.token;
			$scope.seletedtype = "ALL";
			$scope.seletedOrderby = "createdOn";
			$scope.seletedOrderIn = "asc";
			$scope.seletedPriority = "ALL";
			$scope.seletedStatus = "ALL";
			$scope.seletedEntityType ="ALL";
			/*
		 	*@purpose: close social media articles modal
		 	*@created: 23 aug 2017
		 	*@params: none
		 	*@returns: none
		 	*@author: swathi
		 	*/
			
		 	$scope.closeModal = function(){
		 		$uibModalInstance.close('');
		 	};
		 	
		 	
		 	$scope.applyFilters =function(){
			 	 var filterApplied ={};
			 	
			 	filterApplied.token =$scope.token;
			 	 if($scope.seletedtype && $scope.seletedtype !="ALL"){
			 		filterApplied.type = $scope.seletedtype;
			 		
			 	 }
			 	if($scope.seletedPriority && $scope.seletedPriority !="ALL"){
			 		filterApplied.priority = $scope.seletedPriority;
			 		
			 	 }
			 	if($scope.seletedOrderby){
			 		filterApplied.orderBy= $scope.seletedOrderby;
			 	 }if($scope.seletedOrderIn ){
			 		filterApplied.orderIn = $scope.seletedOrderIn;
			 	 }
			 	 if($scope.seletedStatus && $scope.seletedStatus !="ALL"){
			 		filterApplied.status=[];
			 		filterApplied.status.push( parseInt($scope.seletedStatus));
//			 		 filterApplied = filterApplied+"&status="+$scope.seletedStatus
			 	 }
			 	 if(isOnlyKeyword && $scope.seletedEntityType != "ALL"){
			 		filterApplied.entity = $scope.seletedEntityType;
			 	 }
			 	var filterObj ={
			 			caseSearchKeyword : $scope.searchKeyword,
			 			filterApplied:filterApplied
			 	};				
			 	$uibModalInstance.close(filterObj);
		 	};
		 
		
		}
		