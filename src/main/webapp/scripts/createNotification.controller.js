	'use strict';
	elementApp
		   .controller('CreateNotificationController', createNotificationController);

	createNotificationController.$inject = [
				'$scope',
				'$rootScope',
				'$state', 
				'$window',
				'TopPanelApiService',
				'HostPathService',
				'$uibModal'
			];
		    function createNotificationController(
		    		$scope, 
		    		$rootScope, 
		    		$state, 
		    		$window,
		    		TopPanelApiService,
		    		HostPathService,
		    		$uibModal){
		    	
				$scope.token = $rootScope.ehubObject.token;
				var params = {
			    	token: $scope.token		    	
			    };

	//Notification panel starts---------------------------------------------------------------------------//
	    	
	    	$scope.notificationVariable = {
	    			userListNotification:[],
	    			notificationList:[],
	    			selectedUser:[],
	    			checkInCount:''
	    	}
	    	
	    	$scope.notificationFunctions = {
	    			shareNotificationToUser:shareNotificationToUser,
	    			sendNotification:sendNotification,
	    			removeUser:removeUser,
	    			openFilterModal:openFilterModal,
	    			applyFilters:applyFilters
	    	}
	    		
	    	addScroll('#showAllNoti','200px')
	    		
	    	   	
	    /*
			     * @purpose: get notification
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    var params = {};
	    		function getNotification(params){
					TopPanelApiService.getNotificationList(params).then(function(response) {
			            console.log(response)
			            $scope.notificationVariable.notificationList = response.data.result
			        }, function(error) {
			        });
				}
	    	
	    		getNotification();
	    		
	    		/*
			     * @purpose: post notification
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	
		    	function postNotificationData(data){
					TopPanelApiService.postNotification(data).then(function(response) {
			            console.log(response)
			            HostPathService.FlashSuccessMessage('SUCCESSFULLY SENT','');
			        }, function(error) {
			        });
				}
	    		
	    		   /* * @purpose: Get list of all user *
	    	     *   @created: 18th may 2018
	    	     *   @returns: no
	    	     *   @author: varsha */

		    	var userList = []
	    	    function allUserList() {
	    	        var obj = {
	    	            "token": $scope.token
	    	        };
	    	        TopPanelApiService.getUserListing(obj).then(function(response) {
	    	        	userList  = jQuery.extend(true, [], response.data.result);
	    	            $scope.notificationVariable.userListNotification = response.data.result
	    	        }, function(e) {
	    	            HostPathService.FlashErrorMessage('ERROR', e.responseMessage);
	    	        });
	    	    }
	    	    allUserList();
	    	    var data = {
		    			userId:[],
		    			body:''
		    	}
	    	    
	    	    
	    	    /*
			     * @purpose: share notification user
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function shareNotificationToUser($item, $model, $label){
	    	    	var userId = [];
	    	    	data.userId.push($item.userId)
	    	    	$scope.notiUserValue = ''
	    	    	var index=$scope.notificationVariable.userListNotification.map(function(x){ return x.userId; }).indexOf($item.userId);
	    	    	$scope.notificationVariable.userListNotification.splice(index,1);
	    	    	$scope.notificationVariable.selectedUser.push($item)
	    	    	console.log(data)
	    	    }
	    	    
	    	    /*
			     * @purpose: send notification
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function sendNotification(notiBody,notiSubject){
	    	    	if((notiBody == '' || notiBody ==undefined) || (notiSubject == '' || notiSubject == undefined)|| data.userId.length == 0 ){
	    	    		HostPathService.FlashErrorMessage('Please fill the required field','');
	    	    		return false;
	    	    		
	    	    	}else{
		    	    	data.body = notiBody;
		    	    	if(notiSubject){
		    	    		data.subject = notiSubject
		    	    	}else{
		    	    		data.subject = ' '
		    	    	}
		    	    	var notiData ={
				    			"subject" :data.subject,
				    			"body" : data.body,
				    			"recipients" : data.userId
				    	}
		    	    	postNotificationData(notiData)
		    	    	$scope.notificationVariable.selectedUser = [];
		    	    	$scope.userValue = ''
		    	    	$scope.notiBody = ''
		    	    	$scope.notiSubject = ''
		    	    	$scope.notificationVariable.userListNotification = userList 
	    	    	}
	    	    }
	    	    
	    	    /*
			     * @purpose: remove user
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function removeUser(index,user){
	                data.userId.splice(index,1)
	    	    	$scope.notificationVariable.selectedUser.splice(index,1)
	    	    	var indexNew = userList.map(function(x){ return x.userId; }).indexOf(user.userId);
	    	    	$scope.notificationVariable.userListNotification.push(userList[indexNew])
	    	    	console.log(data)
	    	    }
	    	    

				/*
			     * @purpose: open filter modal
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function openFilterModal(){
	    	    	    	if (window.location.hash.indexOf("#!/") >= 0) {
	    	    	    		var modalPath ='../scripts/common/modal/views/notification.filter.modal.html'
	    	    			}else{
	    	    				var modalPath ='scripts/common/modal/views/notification.filter.modal.html'
	    	    	        }
	    	    	    	$uibModal.open({
	    	    	    		templateUrl:modalPath ,
	    	    	    		size: 'md',
	    	    	    		scope: $scope,
	    	    	    		backdrop: 'static',
	    	    	    		windowClass: 'custom-modal sticky-prompt-modal',
	    	    	    	}).result.catch(function (res) {
	    	    	    		if ((res === 'cancel') || (res === 'escape key press') || (res === undefined)) {
	    	    				}else if(res){
	    	    					throw res;
	    	    				}else{
	    	    					
	    	    				}
	    	    	    	}, function() {
	    	    	    	});
	    	    	    	 setTimeout(function() {
	    	    	    	        //load datePicker
	    	    	    	        var start = moment().subtract(2, 'year');
	    	    	    	        var end = moment();
	    	    	    	        initializeDatePicker('notification', start, end)
	    	    	    	    },0)
	    	    	  

	    	    }
	    	    
	    		/*
			     * @purpose: get Notification Count
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function getNotificationCount(){
	    	    	TopPanelApiService.getNotificationCount().then(function(response) {
	    	    		console.log(response)
	    	    		$scope.notificationVariable.checkInCount = response.data.notificationCount;
	    	    		 getNotificationCheckIn();
	    	        }, function(e) {
	    	        });
	    	    }
	    	    getNotificationCount();
	    	    
	    	    /*
			     * @purpose: get Notification CheckIn
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	    
	    	    function getNotificationCheckIn(){
	    	    	TopPanelApiService.getNotificationCheckIn().then(function(response) {
	    	    		console.log(response)
	    	        }, function(e) {
	    	        });
	    	    }
	    	 
	    	    
	    	   /*
			     * @purpose: addScroll
			     * @created: 23rd may 2018
			     * @params: params(object
			     * @return: success, error functions
			     * @author: varsha
			    */
	    	  
	    	    function addScroll(element,height){
	    	    	$(document).ready(function() {
	        	        /*Custom Scroll Bar*/
	        	        $(element).mCustomScrollbar({
	        	            axis: "y",
	        	            theme: "minimal"
	        	        });
	        	        $(element).css("height", height);
	        	    });
	    	    }
	    	    
	    	    
	    	    
	    	    /*
	    	     * @purpose:load datePicker
	    	     * @created: 5 feb 2018
	    	     * @author: varsha
	    	     */

	    	    function initializeDatePicker(tab, start, end) {
	    	        var id = "#" + tab + '_reportrange'

	    	        function cb(start, end) {
	    	            $scope.Date = []
	    	            $scope.Date[0] = new Date(start._d).getDate() + "-" + ((new Date(start._d).getMonth()) + 1) + "-" + new Date(start._d).getFullYear()
	    	            $scope.Date[1] = new Date(end._d).getDate() + "-" + ((new Date(end._d).getMonth()) + 1) + "-" + new Date(end._d).getFullYear()

	    	            $(id + ' ' + 'span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	                    console.log($scope.Date)
	    	        }

	    	        $(id).daterangepicker({
	    	            startDate: start,
	    	            endDate: end,
	    	            ranges: {
	    	                'Today': [moment(), moment()],
	    	                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	    	                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	    	                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	    	                'This Month': [moment().startOf('month'), moment().endOf('month')],
	    	                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	    	            }
	    	        }, cb);

	    	        cb(start, end);

	    	    };
	    	    
	    	    
	    	    function applyFilters(){
	    	    	 params = {
	    	     		   'dateFrom':$scope.Date[0],
	    	     		   'dateTo':$scope.Date[1]
	    	     		}
	    	    	 getNotification(params)
	    	    	 console.log($scope.Date)
	    	     	
	    	    }

	    	    
	    	    $scope.today = function() {
	    	        $scope.dt = new Date();
	    	      };
	    	      $scope.today();

	    	      $scope.clear = function() {
	    	        $scope.dt = null;
	    	      };

	    	      
	    	   // Disable weekend selection
	    	      function disabled(data) {
	    	        var date = data.date,
	    	          mode = data.mode;
	    	        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	    	      }
	    	      
	    	      $scope.dateOptions = {
	    	        dateDisabled: disabled,
	    	        formatYear: 'yy',
	    	        maxDate: new Date(2020, 5, 22),
	    	        minDate: new Date(),
	    	        startingDay: 1
	    	      };
	    	      
	    	    $scope.popup1 = {
	    	      opened: false
	    	    };

	    	    $scope.open1 = function() {
	    	        $scope.popup1.opened = true;
	    	      };
    
	    	    
	   
	}