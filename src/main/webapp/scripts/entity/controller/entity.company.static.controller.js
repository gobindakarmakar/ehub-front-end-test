'use strict';
angular.module('ehubApp')
.controller('EntityCompanyStaticController', entityCompanyStaticController);
	entityCompanyStaticController.$inject = [
		'$scope',
		'$state',
		'$rootScope',
		'EntityGraphService',
		'entityCompanyStaticConst',
		'EnrichSearchGraph',
		'$timeout'
	];
	function entityCompanyStaticController(
			$scope, 
			$state, 
			$rootScope,
			EntityGraphService,
			entityCompanyStaticConst,
			EnrichSearchGraph,
			$timeout) {
		
		function nFormatter(num, digits) {
			  var si = [
			    { value: 1E18, symbol: "E" },
			    { value: 1E15, symbol: "P" },
			    { value: 1E12, symbol: "T" },
			    { value: 1E9,  symbol: "G" },
			    { value: 1E6,  symbol: "M" },
			    { value: 1E3,  symbol: "k" }
			  ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
			  for (i = 0; i < si.length; i++) {
			    if (num >= si[i].value) {
			      return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
			    }
			  }
			  return num.toFixed(digits).replace(rx, "$1");
			}
		
		$scope.loadLinkAnalysis =function(){
			var url = $state.href('linkAnalysis', {'q':'Munich Re'});
 			window.open(url, '_blank');
		}
		$scope.adversenewslist = entityCompanyStaticConst.adversenewslist;
		var searchText = "Munich Re Group"; 
		$scope.adversenewslist = entityCompanyStaticConst.adversenewslist;
		$scope.searchText = searchText.trim();
		var unscaledIR = (searchText + 'indirect').toUpperCase(),
			unscaledDR = (searchText + 'direct').toUpperCase(),
			unscaledTR = (searchText + 'transaction').toUpperCase();
		var searchTextRiskArray = [unscaledIR, unscaledDR, unscaledTR];
		var  risk = 0;
		$scope.scaledNumArr = [];
		
		var scale = d3.scaleBand().domain(searchTextRiskArray).range([40,80]);
		
		for( var j=0, length = searchTextRiskArray.length; j<length; j++){
			risk = (scale(searchTextRiskArray[j])) + searchText.length;
			if(risk > 80){
				risk = risk - searchText.length;
			}
			$scope.scaledNumArr.push(risk);
		};
		
		$scope.mainInfoTabType = 'Overview';
		$scope.associatedCompanies = entityCompanyStaticConst.associatedCompanies;
		 /*
		  * @purpose: onClickInfoTabs function
		  * @created: 28 oct 2017
		  * @params: type(string)
		  * @returns: no
		  * @author: swathi
		  */
		 $scope.onClickInfoTabs = function(type){
			$scope.mainInfoTabType = type;
			 var lineChartData = entityCompanyStaticConst.lineChartData,
		   		width = 300;
			 
			if(type == 'Leaderships'){
				/* calling network chart */
				var options = {
		            url: "vendor/data/leadership_new.json",
		            id: "networkGraphLeadersips"
		        };
			   $timeout(function(){
					$scope.loadDataAndPlotGraphForEntity(options); 
				},10);
			   
			   /* calling Overall Trend line chart */
			   var id1 = "#lineChartOverallTrend"; 
			   plotLineChart(entityCompanyStaticConst.overAllTrendData, id1, width, '200');
			   
			   /* calling Senior Management Trend line chart */
			   var id2 = "#lineChartSeniorManagementTrend"; 
			   plotLineChart(entityCompanyStaticConst.seniorManagementData, id2, width, '200');
			   
			   /* calling Career Opportunities Trend line chart */
			   var id3 = "#lineChartCareerOpportunitiesTrend"; 
			   plotLineChart(entityCompanyStaticConst.careerOpportunitiesData, id3, width, '200');
	
			   $timeout(function(){
			 //tag cloud variables initializations for organization
			    var tagCloudOrganizationOptions = {
			        header: "MY ENTITIES",
			        container: "#tagCloudTopInfluencers",
			        height: 200	,
//			        width: ($(window).width() - 321) / 2,
			        width: $("#tagCloudTopInfluencers").width(),
			        data: entityCompanyStaticConst.tagCloudTopInfluencersData,
			        margin: {
			        	bottom: 10,
			        	top: 10,
			        	right: 10,
			        	left: 10
			        },
			        domain: {
			        	x: 1,
			        	y: 100
			        }
			    }; 
			    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOrganizationOptions);
			   },10)
			}
			if(type == 'Financial Growth'){
				$timeout(function(){
					/* calling network chart */
					var options = {
			            url: "vendor/data/share_holders.json",
			            id: "networkGraphFinancialGrowth"
			        };
				   $timeout(function(){
						$scope.loadDataAndPlotGraphForEntity(options); 
					},10);
				   
				   /* calling Earning Trends line chart */
				  var id1 = "#lineChartEarningTrends"; 
				   plotLineChart(entityCompanyStaticConst.totalRevenuesData, id1, width, '200');
				   
				   /* calling Income statement line chart */
				  var id2 = "#lineChartIncomeStatement"; 
				   plotLineChart(entityCompanyStaticConst.incomeStatementData, id2, '280', '260');
				   
				   /* calling balance sheet line chart */
				  var id3 = "#lineChartBalanceSheet"; 
				   plotLineChart(entityCompanyStaticConst.balanceSheetData, id3, '', '260');
				   
				   /* calling cash flow line chart */
				  var id3 = "#lineChartCashFlow"; 
				   plotLineChart(entityCompanyStaticConst.cashFlowData, id3, '', '260');
				   
				   /* calling investor sentiments line chart */
				   plotLineChart(entityCompanyStaticConst.investorSentimentData, '#lineChartInvestor', width);
				   
				   /* calling financial growth line chart */
				  var id4 = "#lineChartFinancialGrowth"; 
				   plotLineChart(lineChartData, id4, '', '260');
				   
				   /* calling investment trends pie chart */
				   $scope.InitializeandPlotPie(entityCompanyStaticConst.shareHoldersTypeData, 'pieChartInvestment');
				   
				   /* calling Major Holders pie chart */
//				   $scope.InitializeandPlotPie(staticPieData, 'PieChartMajorHolders');
				   /*---------------------------------- timeline: by investments chart ------------------------------*/
					
					var newData = entityCompanyStaticConst.timelineFinancialData;
					newData.map(function(d){
						return d.time = new Date(d.time);
					})
					var timelineOptions = {
				        container: "#timeline_new_bs_entity_financial",
				        height: 50,
				        data: newData
					};
					loadtimeLineColumnChart(timelineOptions);
				}, 10);
			}
			if(type == 'Risk Alerts'){
				/* calling network chart */
//				var options = {
//		            url: "vendor/data/riskAlertsNetwork.json",
//		            id: "networkGraphRiskAlerts"
//		        };
//			   $timeout(function(){
//					$scope.loadDataAndPlotGraphForEntity(options); 					
//				},10);
			   
			   /* calling Risk ratio pie chart */
				var riskPieData = entityCompanyStaticConst.riskPieData;
			   $scope.InitializeandPlotPie(riskPieData, 'pieChartRiskRatio');
			   
			   var timelinebrushOptions = {
 				        container: "#riskyTimelineChart",
 				        height: 50,
 				        colors:["#46a2de"],
 				        data:["2017-05-15","2016-09-08","2017-10-20"],
 				        noBrush:true,
 				        brushFill:"#334551"
 					};
 			   $timeout(function(){
 				  createBrush(timelinebrushOptions);
 			   },10)
			}
			if(type == 'Latest News'){
				/* calling network chart */

				var options = {
		            url: "vendor/data/adverseNewsVLA.json",
		            id: "networkGraphLatestNews"
		        };
			   $timeout(function(){
					$scope.loadDataAndPlotGraphForEntity(options); 
				},10);

			   
			   

			   $("#tagCloudPopularTags").empty();
			   var tagCloudOrganizationOptions = {
				        header: "MY ENTITIES",
				        container: "#tagCloudPopularTags",
				        height: 220	,
				        width: 300,
				        data: [],
				        margin: {
				        	bottom: 10,
				        	top: 10,
				        	right: 10,
				        	left: 10
				        },
				        domain: {
				        	x: 1,
				        	y: 100
				        }
				    }; 
			   
			   var tagCloudTagsList = entityCompanyStaticConst.tagCloudPopularTagsList;
				
				tagCloudOrganizationOptions.data = tagCloudTagsList.splice(0, 29);
				tagCloudOrganizationOptions.domain.y = maxOrganizationDomainRangeValue;
			    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOrganizationOptions);
			    
			    
			    /*--------------------- world chart --------------------*/
			    var worldChartOptions2 = {
				    container :"#worldMapNewsSources",
				    uri1 : "vendor/data/worldCountries.json",
				    height:400,
				    markers: {}
				 };
			    findLatLong(entityCompanyStaticConst.staticCompanyLocations, worldChartOptions2);
			    var overallsentimentsdata = entityCompanyStaticConst.overallsentiments;
			   
		            	  var timelineOptions = {
		  				        container: ".overallSentimentChart-wrapper",
		  				        height: 50,
		  				        colors:["#a75dc2","#5D97C9","#399034"],
		  				        data:overallsentimentsdata
		  					};
		            	var setimentsPieData = d3.nest()		            	 
		            	  .key(function(d) { return d.type; })
		            	  .rollup(function(sentiments) { return sentiments.length; })
		            	  .entries(overallsentimentsdata);
		            	setimentsPieData.map(function(d){
		            		return d['doc_count'] = d.value;
		            	})
		            	
		  			   $timeout(function(){
		  				   loadtimeLineColumnChart(timelineOptions);
		  				 /* calling General Sentiment pie chart */
		  				   $scope.InitializeandPlotPie(setimentsPieData, 'pieChartGeneralSentiment');
		  			   },10)
		            
			  

			}
			if(type == 'Social Media'){
				/* calling network chart */
//				var options = {
//		            url: "vendor/data/entityNetwork.json",
//		            id: "networkGraphSocialMedia"
//		        };
//			   $timeout(function(){
//					$scope.loadDataAndPlotGraphForEntity(options); 
//				},10);
				
				/*--------------------- world chart --------------------*/
			    var worldChartOptions3 = {
				    container :"#twitter-locations-chart",
				    uri1 : "vendor/data/worldCountries.json",				  
				    height:400,				   
				    markers: {}
				 };
			    findLatLong(entityCompanyStaticConst.staticCompanyLocations, worldChartOptions3);
			   
			   /* calling followers trend line chart */
				   plotLineChart(entityCompanyStaticConst.trafficData, "#lineChartFollowersTrend", '320', '270');
				   
				   /* calling interaction ratio pie chart */
				   $scope.InitializeandPlotPie(entityCompanyStaticConst.interactionRatioData, 'pieChartInteractionRatio');
				   
				   /* calling sentiments pie chart */
//				   $scope.InitializeandPlotPie(staticPieData, 'pieChartSentiments');
				 
			  
			   var timelineOptions = {
				        container: "#activityFeedTimeline",
				        height: 50,
				        colors:["#35942E","#A15EBB","#46a2de"],
				        data: entityCompanyStaticConst.activityFeedTimeline
					};
			   $timeout(function(){
				   loadtimeLineColumnChart(timelineOptions);
				 //tag cloud variables initializations for twitter tag words
				    var tagCloudTwitterTagsOptions = {
				        header: "MY ENTITIES",
				        container: "#tagcloudCompanyTwitterTags",
				        height: 150,
				        width: ($("#tagcloudCompanyTwitterTags").width() ),
				        data: entityCompanyStaticConst.tagCloudTwitterData,
				        margin: {
				        	bottom: 10,
				        	top: 10,
				        	right: 10,
				        	left: 10
				        },
				        domain: {
				        	x: 1,
				        	y: 100
				        },
				        //  xticks:5,
				        // yticks:5,
				        //  xtext:""
				        //ytext:""
				    }
				    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudTwitterTagsOptions);
			   },10)
			   
			   $timeout(function(){   
				   	loadtwitterchart(); 
				   },1000);
			}
		 };
		 $scope.entitySearchResult = {
			 list: {
				 'company_locations': entityCompanyStaticConst.staticCompanyLocations
			 }
		 };
		 var worldChartOptions1 = {
		    container :"#companyexample5",
		    uri1 : "vendor/data/worldCountries.json",		   
		    height:400,		   
		    markers: {}
		 };
		 /*-------------------------world location chart----------------------------------*/
		 function World(options) {
		     
		     var worldmapId = options.container;
		     var worldmapuri1 = options.uri1;		    
		     loadWorldChart(options);   

		     /**
		      *Function to load data to plot WorldMap chart 
		      */
		     function loadWorldChart(options) {
		         var WorldData = [];
		         d3.json(options.uri1, function (error, data) {
		             WorldData = data;
		             options.data=[WorldData,[]];
		             
		            	 EntityGraphService.plotWorldLocationChart(options);	
		             
		         });
		        
		     }
		     /**
		      *Function to handle data according to format of WorldMap Chart library
		      */	
		     function handleMapData(data) {
		         var finalData = [];
		         $.each(data, function (i, d) {
		             finalData.push({
		                 name: i,
		                 population: d
		             });
		         });
		         return finalData;
		     }
		 }
		 var finalLocationList = [];
		 function findLatLong(locationsData, worldChartOptions){
		     angular.forEach(locationsData, function(v, k){	    	
				var markersData = {};
		    	markersData = {
		            	name: v,
		                mark: 'assets/images/redpin.png',
						source: 'companieslist.co.uk',
						title: 'companieslist.co.uk'
	        	  };
	        	  finalLocationList.push(markersData);
        	 });
		     worldChartOptions.markers = finalLocationList;
		     World(worldChartOptions);
		 }
		 findLatLong($scope.entitySearchResult.list['company_locations'], worldChartOptions1);
		 
		 /*---------------------------------------------- network chart ---------------------------------------------*/
		 var options = {
            url: "vendor/data/overview-network.json",
            id: "networkGraphOverview"
        };
	   $timeout(function(){
			$scope.loadDataAndPlotGraphForEntity(options); 
		},10);
	   /**
		 * Function to load data for network chart
		 */
		 $scope.loadDataAndPlotGraphForEntity = function(options){
			var chartData = entityCompanyStaticConst[options.id]
		   
           	  handleGraphData(chartData, options);
             
	        function handleGraphData(data, options) {
			      var finalData = {};
			      var edges = [];
			      var nodeIDS = [];
			      var nodes = [];

			      angular.forEach(data.vertices, function ( val,i) {
			          nodeIDS.push(val.id)
			           val.weight=10;
			          nodes.push({			        	 
			              data: val
			          });
			      });
			      angular.forEach(data.edges, function ( val,i) {
			          if ($.inArray(val.from, nodeIDS) != -1 && $.inArray(val.to, nodeIDS) != -1)
			              edges.push({
			                  data: {
			                      source: val.from,
			                      target: val.to,
			                      labelE: val.labelE,
			                      id: val.id
			                  }});
			      });
			      finalData.nodes = nodes;
			      finalData.edges = edges

			      loadEntityNetworkChart(finalData, options);
			 }
		}
		 /*----------------------------------------------Performance Line chart ---------------------------------------------*/
		 var lineChartData = entityCompanyStaticConst.lineChartData;
		 var id = "#performanceLineChart";
		 function plotLineChart(lineChartData, id, lineWidth, height){
			 
			 var lineData = [];
			 if(id == '#lineChartCashFlow'){
				 angular.forEach(lineChartData, function(v, k){
					 v.values.map(function(d){
							d.x = new Date(d.date);
							d.value = d.sum;
							return d.time = d.date;
						});
				 });
				 lineData = lineChartData;
			 }else{
				 lineChartData.map(function(d){
					d.x = new Date(d.date);
					d.value = d.sum;
					return d.time = d.date;
				});
				 lineData =[{
					"key": "",
					"values": lineChartData
				 }];
			 }
				 var options = {
				    container: id,
				    height: height != undefined ? height: '',
				    width: lineWidth != undefined ? lineWidth: '',
				    axisX: true,
				    axisY: true,
				    gridy: true ,
				    tickformat:"year",
				    data: lineData,
				    marginRight:20,
				    gridcolor:"#2e424b",
				    actualData: lineData[0].values,
				    ystartsFromZero: true
				 }; 
				 new InitializeandPlotLine(options);
		 }
		 plotLineChart(lineChartData, id, '', '280');	
			
			 /*----------------------------------------------Social media bubble chart ---------------------------------------------*/
			 	
				    var bubbleOptions = {
				        "container": "#socialBubbleChart",				       
				        "height": 380
				    };
				    bubbleOptions.data = jQuery.extend(true, [], entityCompanyStaticConst.socialBubbleChart);
		           
		            var exampleChart = new BubbleHierarchyChart(bubbleOptions);
		            $scope.socialMediaUpdatesArray = [];
		            
		            bubbleOptions.data ['children'].sort(function(a,b){
		            	return b.size - a.size;
		            });
		            bubbleOptions.data ['children'].map(function(d){
		            	return d.valueFormatted = nFormatter(d.size);
		            })
		            $scope.socialMediaUpdatesArray =   bubbleOptions.data ['children'].splice(0,3);
			 	
				

				

				/*---------------------------------- timeline: by investments chart ------------------------------*/
				
				var newData = entityCompanyStaticConst.timelineOverviewData;
				newData.map(function(d){
					return d.time = new Date(d.time);
				})
				var timelineOptions = {
				        container: "#timeline_new_bs_mip_map",
				        height: 50,
				        
				        data: newData
					};

				loadtimeLineColumnChart(timelineOptions);
				
				/*---------------------------------- Related entites tag cloud chart ------------------------------*/	
				//tag cloud variables initializations for organization
			    var tagCloudOrganizationOptions = {
			        header: "MY ENTITIES",
			        container: "#tagcloudCompanyOrganization",
			        height: 220	,
//			        width: ($(window).width() - 321) / 2,
			        width: 300,
			        data: [],
			        margin: {
			        	bottom: 10,
			        	top: 10,
			        	right: 10,
			        	left: 10
			        },
			        domain: {
			        	x: 1,
			        	y: 100
			        }
			    }; 
			    var tagCloudOrganizationNameList = [], 
			    maxOrganizationDomainRangeValue = 5;
			    tagCloudOrganizationNameList = entityCompanyStaticConst.tagCloudStaticOrganizationNameList;
		
				tagCloudOrganizationOptions.data = tagCloudOrganizationNameList.splice(0, 29);
				tagCloudOrganizationOptions.domain.y = maxOrganizationDomainRangeValue;
			    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudOrganizationOptions);
			    
			    var staticPieData = entityCompanyStaticConst.staticPieData;
			    $scope.InitializeandPlotPie = function(data, id) {
				    data.map(function (d) {
				        return d.value = d.doc_count;
				    });
				    var newData = [];
				    var keys = [];
				    angular.forEach(data, function(d, i){
				    	if(d.key && $.inArray(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim(),keys)== -1){
				    		newData.push(d);
				    		keys.push(d.key.toLowerCase().split("-").join(" ").split(" ").join("").trim());
				    	}
				    });
				    var maxval = d3.max(data,function(d){
			   	    	return d.value;
			   		 });
				    var sum = d3.sum(data,function(d){
			   	    	return d.value;
			   		 });
				    var colors = ["#5d96c8" , "#b753cd" , "#69ca6b" , "#69ca6b","#c1bd4f" , "#db3f8d" , "#669900"]
				    var options = {
				        container: "#" + id,
				        data: newData,
				        height:160,
				        colors:colors,
				        islegends:true,
				        islegendleft:true,
				        legendwidth:30,
				        istxt: parseInt((maxval/sum)*100)+"%",
				        legendmargintop:30
				    };
				    setTimeout(function () {
				        new reusablePie(options);
				    });
				};
				//---------------------------------------Network chart-----------------------------------------------------------//
				function loadtwitterchart(){
				var WebSocketTwitterUrl='http://104.197.164.75';
				var nodes = [], links = [], tweetList = [], retweetList = [], mentionList = [], tagCloudNameList = [], 
				locationList = [], finalLocationList = [], locationNames = [], randomGeneratedId = Math.floor((Math.random() * 10) + 19999),maxTwitterTagDomainRangeValue=5;;
				var socket = {};
				 $scope.liveFeed = {
							nodeList: []
						};
				
				function getWebSocketTwitterData(){
					var name='munich re'
					 var tweetObjectOneOrigin = {
						id: randomGeneratedId,
						name:name,
						screen_name:name,
						text:name,
						profile_image: '',
						createdOn: new Date(),
						type: 'main'
					};		
					 nodes.push(tweetObjectOneOrigin);
					socket = io.connect(WebSocketTwitterUrl, {'forceNew': true});
					
					socket.on('message', function(response){
						
						$scope.disableSearchButton = false;
					    $('.custom-spinner').css('display', 'none');
						var isString = false;
						if(response.indexOf('{') === -1){
							isString = true;
						} else {
							response = JSON.parse(response);
							getSourceTargetData(response);
						}
//						if(tagCloudNameList.length > 0){
//							console.log(tagCloudNameList,tagCloudNameList.length,"tagCloudNameListtagCloudNameList")
//						    tagCloudTwitterTagsOptions.data = tagCloudNameList;
//							tagCloudTwitterTagsOptions.domain.y = maxTwitterTagDomainRangeValue;
//							console.log(tagCloudTwitterTagsOptions,"tagCloudTwitterTagsOptionstagCloudTwitterTagsOptions")
//						    EnrichSearchGraph.plotTagCloudChartForLiveFeed(tagCloudTwitterTagsOptions);
//						    
//						}
					});
					
					socket.on('connect', function() {
						socket.send(name);
					});

				    socket.on('disconnect', function() {
					});
				 }
				 
				 
			 getWebSocketTwitterData();
				 
				 
				 function getSourceTargetData(tweetData){
					 var linkDataOrigin = {
						source: null,
						target: null
					 };
					 if(tweetList.indexOf(tweetData.user.screen_name) === -1) {
						 var tweetObjectOne = {
							id: tweetData.id,
							name: tweetData.user.name,
							screen_name: tweetData.user.screen_name,
							text: tweetData.text,
							profile_image: tweetData.user.profile_image_url,
							createdOn: new Date(tweetData.user.created_at),
							likeCounts: tweetData.user.favourites_count || 0,
							retweetCounts: tweetData.user.retweet_count || 0,
							replyCounts: tweetData.user.reply_count || 0,
							type: 'tweet'
						};
						 tagCloudNameList.splice(0, 0, {
							 text: tweetData.user.name,
							 size: tweetData.user.favourites_count || 5 
						 });
						if(tagCloudNameList.length === 21) {
							tagCloudNameList.pop();
						} 
						 if(tweetData.user.location !== '' && tweetData.user.location) {
							 locationList.push({
			     				name: tweetData.user.location,
			    				long: '',
			    				lat: '',
			    				mark: 'static/images/redpin.png'
			    			});
						 }
						 nodes.push(tweetObjectOne);
						 $timeout(function(){
							 $scope.entitySearchResult.tweetCounts++;
							 $scope.liveFeed.nodeList.push(tweetObjectOne);
						 },0);
						 tweetList.push(tweetData.user.screen_name);
						 linkDataOrigin.source = randomGeneratedId;
						 linkDataOrigin.target = tweetData.id;
						 links.push(linkDataOrigin);
					 }
					 if(tweetData.entities['user_mentions'].length > 0){
						 angular.forEach(tweetData.entities['user_mentions'], function(mention, mentionKey){
							if(mentionList.indexOf(mention.screen_name) === -1) {
								var mentionObject = {
									id: 'mention-' + mention.id,
									name: mention.name,
									screen_name: mention.screen_name,
									type: 'mention'
								};
								 $timeout(function(){
									 $scope.entitySearchResult.mentionCounts++;
								 },0);
								nodes.push(mentionObject);
							    mentionList.push(mention.screen_name);
							}
							links.push({
								source: tweetData.id,
								target: 'mention-' + mention.id
							});
						 });
					 }
					 var linkData = {
						source: null,
						target: null
					 };
					 linkData.source = tweetData.id;
					 if(_.isObject(tweetData.retweeted_status)){
						 if(retweetList.indexOf(tweetData.retweeted_status.user.screen_name) === -1) {
							var tweetObject = {
								id: tweetData.retweeted_status.id,
								name: tweetData.retweeted_status.user.name,
								screen_name: tweetData.retweeted_status.user.screen_name,
								text: tweetData.retweeted_status.text,
								profile_image: tweetData.retweeted_status.user.profile_image_url,
								createdOn: new Date(tweetData.retweeted_status.user.created_at),
								likeCounts: tweetData.retweeted_status.user.favourites_count || 0,
								retweetCounts: tweetData.retweeted_status.user.retweet_count || 0,
								replyCounts: tweetData.retweeted_status.user.reply_count || 0,
								type: 'retweet'
							};
							tagCloudNameList.splice(0, 0, {
								 text: tweetData.retweeted_status.user.name,
								 size: tweetData.retweeted_status.user.favourites_count || 5 
							});
							if(tagCloudNameList.length === 21) {
								tagCloudNameList.pop();
							} 
							 if(tweetData.retweeted_status.user.location !== '' && tweetData.retweeted_status.user.location){
								 locationList.push({
				     				name: tweetData.retweeted_status.user.location,
				    				long: '',
				    				lat: '',
				    				mark: 'static/images/redpin.png'
				    			 });
							 }
							nodes.push(tweetObject);
							 $timeout(function(){
								 $scope.entitySearchResult.reTweetCounts++;
								 $scope.liveFeed.nodeList.push(tweetObject);
							 },0);
							retweetList.push(tweetData.retweeted_status.user.screen_name);
						 }
						 linkData.target = tweetData.retweeted_status.id;
					 }
					 if(linkData.source && linkData.target)
						 links.push(linkData);
					 var twitterFinalData = {'links': links, 'nodes': nodes};
					var twitterFinalDataNew = jQuery.extend(true, [], twitterFinalData);
					 twitterPlotNetworkChart(twitterFinalDataNew);
				 }
				 
				//----------------------------Twitter Network Chart----------------------------------------
				 var tool_tip = $('body').append('<div class="CaseTimeLine_Chart_tooltip" style="z-index:2000;position: absolute; opacity: 1; pointer-events: none; visibility: visible;display:none"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
				 var legendtool = $('body').append('<div id="socialMedialLegend" class="legend"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
				 var width = $("#networkGraphSocialMedia").width(),
			     height = 310;
				 	
				 	  var svg = d3.select("#networkGraphSocialMedia").append("svg")
				 	  .attr("width", width)
				 	    .attr("height", height),
				       color = d3.scaleOrdinal(d3.schemeCategory10);
				 //build the arrow.
				 svg.append("svg:defs").selectAll("marker")
				       .data(["end"])      // Different link/path types can be defined here
				       .enter().append("svg:marker")    // This section adds in the arrows
				       .attr("id", String)
				       .attr("viewBox", "0 -5 10 10")
				       .attr("refX", 15)
				       .attr("refY", -1.5)
				       .attr("markerWidth", 6)
				       .attr("markerHeight", 6)
				       .attr("orient", "auto")
				       .append("svg:path")
				       .attr("d", "M0,-5L10,0L0,5")
				       .style("fill", "#666");

				 var a = {id: "Sarah"},
				       b = {id: "Alice"},
				       c = {id: "Eveie"},
				       d = {id: "Peter"},
				       nodesnew = [],
				        linksnew = [];
				 var simulation = d3.forceSimulation(nodesnew)
				       .force("charge", d3.forceManyBody().strength(-300))
				       .force("link", d3.forceLink(linksnew).distance(60))
				       .force("x", d3.forceX())
				       .force("y", d3.forceY())
				       .alphaTarget(1)
				       .on("tick", ticked);

				 var g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
				       link = g.append("g").attr("stroke", "#000").attr("stroke-width", 1.5).selectAll(".link"),
				       node = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");



				 function twitterPlotNetworkChart(data) {
				 	nodesnew = data.nodes;
				 	linksnew=[];
				 	
					data.links.forEach(function(d) {	
						linksnew.push({
				               "source":nodesnew[nodesnew.findIndex( (el) ,function(){ el.id == d.source})],
				               "target":nodesnew[nodesnew.findIndex( (el),function(){ el.id == d.target})]
				           });
					});
					restart();
				 }
				 restart();


				 var colornew;
				 var radius;
				 function restart() {
					 legend(nodesnew);
				   // Apply the general update pattern to the nodes.
				   node = node.data(nodesnew, function (d) {
				       return d.id;
				   });
				   node.exit().remove();
				   node = node.enter().append("circle")
				   .style("stroke","none").attr("fill", function (d) {
				      if(d.type=="main"){
						   colornew="#62747E"//blue
							}
						else if(d.type=="tweet"){
							colornew="#62747E"//yellow
						}
						else if(d.type=="mention"){
							colornew="#62747E"
						}
						else if(d.type=="retweet"){
							colornew="#62747E"//green
						}
				       return colornew;
				   }).attr("r",  function (d) {
					      if(d.type=="main"){
							   radius="18"//blue
								}
							else {
								radius="15"//green
							}
					       return radius;
					   })
					   .on("mouseover", function (d) {
						   var appendHTMLTooltip = '';
						   if(d.type === 'tweet' || d.type === 'retweet')
							   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="static/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p><p><i class="fa fa-retweat icon"></i>' +d.text + '</p><p class="progress-text"><i class="fa fa-wechat icon"></i><span>'+d.replyCounts+'</span><img  class="img-responsive icon" src="static/images/icon/arrow-right-square.png" alt="icon" /><span>'+d.retweetCounts+'</span><i class="fa fa-heart icon"></i><span>'+d.likeCounts+'</span></p></div>';
						   else
							   appendHTMLTooltip = '<div class="timeline-tooltip"><h3><img src="static/images/entity/briefcase.svg" class="img-responsive icon" />' +d.screen_name + '</h3><p><i class="fa fa-user-o icon"></i>' +d.name + '</p></div>';
					      $(".CaseTimeLine_Chart_tooltip").html(appendHTMLTooltip);
					      $(".CaseTimeLine_Chart_tooltip").css("visibility", "visible");
					      return $(".CaseTimeLine_Chart_tooltip").css("display", "block");
					   }).on("mousemove", function (event) {
						  var value= $(this).offset();
						 var top=value.top
						 var left=value.left+20
					      $(".CaseTimeLine_Chart_tooltip").css("top", top + "px")
					      return  $(".CaseTimeLine_Chart_tooltip").css("left", left+ "px");

					   }).on("mouseout", function () {
					     // $(this).css("opacity", 0.4);
					      //hide tool-tip
					      return $(".CaseTimeLine_Chart_tooltip").css("display", "none");
					   }).merge(node);

				   // Apply the general update pattern to the links.
				   link = link.data(linksnew, function (d) {
				       return d.source.id + "-" + d.target.id;
				   });
				   link.exit().remove();
				   link = link.enter().append("line").merge(link);
			link.style("fill"," none")
			.style("stroke"," #666")
					.style("stroke-width","1.5px;")
				   // Update and restart the simulation.
				   simulation.nodes(nodesnew);
				   simulation.force("link").links(linksnew);
				   simulation.alpha(1).restart();
				 }
				 

				 function legend(nodesnew)
				 {
					 $(".legend").empty()
					 var type=[];
					 nodesnew.forEach(function(d)
					 {
						 type.push(d.type) 
					 })
					 var outputArray = [];
					 for (var i = 0; i < type.length; i++)
					    {
					        if ((jQuery.inArray(type[i], outputArray)) == -1)
					        {
					            outputArray.push(type[i]);
					        }
					    }
					   var legendcolor;
					 outputArray.forEach(function(d){
						 if(d=="main"){
							 d="Search query"
								 legendcolor="#000080"//blue
								}
							else if(d=="tweet"){
								legendcolor="#FFD700"//yellow
							}
							else if(d=="mention"){
								legendcolor="#800000"
							}
							else if(d=="retweet"){
								legendcolor="#008000"//green
							}
							else {
								legendcolor="#000"//green
							}
						 $(".legend").append('<div style="display: inline-block;vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;text-transform: capitalize;margin: 0 10px 0 0">'+d+'</span><span style="margin: 0 10px 0 0;vertical-align: middle;border-radius: 50%;display: inline-block;width: 15px;height: 15px;color: #435561;font-size: 12px;line-height: 20px;text-align: center;padding: 1px 4px;border: 2px solid #435561;background-color: '+legendcolor+'"></span></div>')
					 })
					// $(".legend").html('<div>jvdjvjioio</div>');
					 return $(".legend").css("visibility", "hidden");
				 }

				 function ticked() {
				   node.attr("cx", function (d) {
				       return d.x;
				   })
				           .attr("cy", function (d) {
				               return d.y;
				           })

				   link.attr("x1", function (d) {
				       return d.source.x;
				   })
				           .attr("y1", function (d) {
				               return d.source.y;
				           })
				           .attr("x2", function (d) {
				               return d.target.x;
				           })
				           .attr("y2", function (d) {
				               return d.target.y;
				           }).attr("marker-end", "url(#end)");
				 }
				 
				 
					}
	}
	
	