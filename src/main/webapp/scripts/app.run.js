elementApp
		.run(['$rootScope', '$state', '$location', '$stateProvider', '$urlRouterProvider', '$stateParams', 'HostPathService','UploadFileService', 'KYC_QUESTIONNAIRE_PATH','POLICY_ENFORCEMENT_PATH',function($rootScope, $state, $location, $stateProvider, $urlRouterProvider, $stateParams, HostPathService, UploadFileService,KYC_QUESTIONNAIRE_PATH,POLICY_ENFORCEMENT_PATH) {
		$rootScope.$state = $state;
		$rootScope.topPanelPreview = false;
		$rootScope.start_url = $location.absUrl().split('#')[0];
		HostPathService.setHostPath($location.absUrl());
		$rootScope.rootPath = HostPathService.getHostPath();
		$rootScope.apiPath = $rootScope.rootPath + 'rest/';
		$rootScope.hyperLinksNewTab = true;
		$rootScope.qbServeyID = 531697; // default qb(Questionnaire) servey Id
		$rootScope.customerLogo = '';
		$rootScope.defaultModuleName = '';
		var userDetails = getAgainTokenBySession();
//		userDetails = window.localStorage.getItem('ehubObject') !== null ? JSON.parse(window.localStorage.getItem('ehubObject')) : userDetails; 
//		userDetails = window.localStorage.getItem('ehubObject') !== null ? JSON.parse(window.localStorage.getItem('ehubObject')) : userDetails; 
		$rootScope.ehubObject = userDetails;
		
		$rootScope.ehubObject = {
			token: userDetails !== undefined && userDetails !== null ? userDetails.token: '',
			email: userDetails !== undefined && userDetails !== null ? userDetails.email: '',
			lastLoginDate : userDetails !== undefined && userDetails !== null ? userDetails.lastLoginDate: '',
			fullName: userDetails !== undefined && userDetails !== null ? userDetails.fullName: '',
			userId: userDetails !== undefined && userDetails !== null ? userDetails.userId: '',
			adminUser:userDetails !== undefined && userDetails !== null ? userDetails.adminUser: '',
			analystUser:userDetails !== undefined && userDetails !== null ? userDetails.analystUser:''
		};
		if($rootScope.ehubObject !== null && $rootScope.ehubObject !== undefined){
        	if($rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== undefined)
        		window.localStorage.setItem('ehubObject', JSON.stringify($rootScope.ehubObject));
        }
		
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
				fromState, fromParams) {
			$(".Bubble_Chart_tooltip").css("display", "none");
			if($rootScope.ehubObject.fullName === "DataEntry User"){
				if(toState.name == 'dataCuration' || toState.name == 'addDataCuration'){
				}else{
					event.preventDefault();
				}
			}
		});
		/*
	     * @purpose: Get system settings
	     * @created: 10 Aug 2018
	     * @params: params
	     * @return: no
	     * @author: Prasanthi
	    */
		var url;
		function getSystemSettings(){		
				UploadFileService.getGeneralSettings().then(function(response){	
					if(response.data && response.data['General Settings']){
						var dropDownmenuItems = HostPathService.getdashboarDropDownMenuItems();
						angular.forEach(response.data['General Settings'],function(val,k){
							if(!val.options && val.name == "Open link in a new tab"){
								$rootScope.hyperLinksNewTab = val.selectedValue == "On"?true:false;
							}
							if(val.section.toLowerCase() === 'branding'){
								$rootScope.customerLogo = val.customerLogoImage;
							}
							if (val.options && val.name === "Default Module") {
								angular.forEach(dropDownmenuItems, function (contntVal) {
									angular.forEach(contntVal.content, function (contentValName) {
										angular.forEach(val.options, function (key) {
											if (key.attributeName && contentValName.name && (key.attributeName.toLowerCase() === contentValName.name.toLowerCase())) {
												contentValName["attributeId"]= key.attributeId;
												contentValName["systemSettingType"]= val.systemSettingType;
												if(key.selected && (key.selected == true)){
													$rootScope.defaultModuleName = key.attributeName.toLowerCase();
												contentValName["selected"] = true;
												}else{
													contentValName["selected"] = false;
												}
											}
										});
									});
								})
								HostPathService.setdashboarDropDownMenuItems(dropDownmenuItems);
							}
							if(val.options && val.options.length && val.name.toLowerCase() == 'questionnaries'){
								angular.forEach(val.options,function(v1,k1){
									if(v1.selected){
											$rootScope.qbServeyID = v1.attributeId;
									}
								});
							}
						});
						var dashboarDropDownMenuItems = jQuery.extend(true, [], HostPathService.getdashboarDropDownMenuItems());
						var alldashboardItems =dashboarDropDownMenuItems;
						if($rootScope.process && $rootScope.process['dashboardItems']){
							$rootScope.process['dashboardItems'] =  alldashboardItems.moveArray(0, 1).moveArray(2,3);
						}
							
					}
				},function(error){
					
				});
		}
		
			/*
	     * @purpose: Adding a class to body For input box Animation
	     * @created: 7th May 2019
	     * @params: no
	     * @return: no
	     * @author: Asheesh
	    */
	   var url;
	 	function addInputClassToBody(){		
			$('body').addClass('bst_custom_input');				
	   }
	   addInputClassToBody();
		/*
	     * @purpose: Get Submenu Data
	     * @created: 2 Feb 2018
	     * @params: params
	     * @return: no
	     * @author: prasanthi
	    */
		var url;
		function getSubmenu(){
			if(window.location.hash.indexOf("#!/") < 0)
	    		url = 'scripts/common/data/submenu.json';
	   		  else
	   			url = '../scripts/common/data/submenu.json';
				UploadFileService.getSubmenuData(url).then(function(response){	
					if($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)){
						angular.forEach(response.data.dashboarDropDownMenuItems,function(val,key){
							if(val.menu == "Manage"){
								angular.forEach(val.content,function(v,k){
									if(v.name == "System Settings" && $rootScope.ehubObject.adminUser){
										v.disabled ="no";
									}
									if(v.name == "Document Parsing"){
										v.disabled ="no";
									}
									if(v.name == "Source Management"  && $rootScope.ehubObject.adminUser){
										v.disabled ="no";
									}
									if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
										v.disabled ="yes";
									}
								});
							}
						});
					}
										
					HostPathService.setdashboardDisableBydomains(response.data.DisableBydomains);
					HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);					
					HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
				},function(error){
					HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
				});
		}
		getSubmenu();
		getSystemSettings();
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (toState.name == 'login' || toState.name == 'domain' || toState.name == 'register' || toState.name == 'forgotPassword' || toState.name == 'validateToken' || toState.name == '404') {
				$rootScope.topPanelPreview = false;
//				if(toState.name == 'domain' && ($rootScope.ehubObject.token === undefined || $rootScope.ehubObject.token === null || $rootScope.ehubObject.token === "")){
//					$window.location.href = '';
//				}
					$rootScope.stateName = 'Element by Blackswan';
			} else {
				if($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== "")
				{
					$rootScope.topPanelPreview = true;
					if(toState.name == 'discover' || toState.name == 'linkAnalysis' || toState.name == 'enrich' || toState.name == 'auditTrail' || toState.name == 'dataCuration')
					{
						$rootScope.stateName = (toState.name).charAt(0).toUpperCase() + (toState.name).substr(1);
					}
					else if(toState.name == 'actCase')
					{
						$rootScope.stateName = "Case "+toParams.caseId;
					}
					else if(toState.name == 'act')
					{
						$rootScope.stateName = 'act';
					}
					else
					{
						$rootScope.stateName = 'Element by Blackswan';
					}
				}
				else{
//					$window.location.href = '';
				}
//					
			}
			
		});
		
		$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
			if(error.authenticate == true){
				
			}
			else{
				
			}
		});
		
		$stateProvider.state('domain', {
			url : '/domain',
			templateUrl : function(){
				return $rootScope.start_url + 'domain'
			},
			controller : 'DomainController'
		}).state('404', {
			url : '/404',
			templateUrl : function(){
				return $rootScope.start_url + 'error'
			}
		}).state('process', {
			url: '/process',
			templateUrl: function(){
				return $rootScope.start_url + 'process'
			},
			controller : 'ProcessController'
		})
		.state('workspace', {
			url: '/workspace',
			templateUrl: function(){
				return $rootScope.start_url + 'workspace'
			},
			controller : 'WorkspaceController'
		}).state('discover', {
			url : '/discover',
			templateProvider: ['$stateParams', '$templateRequest',
			      function($stateParams, $templateRequest) 
			      {
				  return UploadFileService.getGeneralSettings()
	                .then(function(response){  
				        var tplName = $rootScope.start_url + '/discoverDashboard';
				        angular.forEach(response.data['General Settings'],function(val,key){
							if(val.name == "Dashboard View" && val.selectedValue !="default"){								
								tplName = $rootScope.start_url + '/discoverDashboardUnderwriting';
							}
						}) 
						return $templateRequest(tplName);
	                })
			      }
			 ],
//			templateUrl : function(){
//				return $rootScope.start_url + '/discoverDashboard'
//			},
			controller : 'DiscoverDashboardController'
		}).state('investigationLanding', {
			url : '/investigationConsole/:caseId?q&entity&eId&id&Etype',
			templateUrl : function(){
				return $rootScope.start_url + '/investigationLanding'
			},
			controller : 'InvestigationLandingController'
		}).state('transactionMonitoring', {
			url : '/transactionMonitoring',
			templateUrl : function(){
				return $rootScope.start_url + '/transactionMonitoring'
			},
			controller : 'TransactionMonitoringController'
		}).state('transactionMonitoringCase', {
			url : '/transactionMonitoring/:caseId',
			templateUrl : function(){
				return $rootScope.start_url + '/transactionMonitoring'
			},
			controller : 'TransactionMonitoringController'
		}).state('enrich', {
			url : '/enrich?onBoard',
			templateUrl : function(){
				return $rootScope.start_url + '/enrichDashboard'
			},
			controller : 'EnrichDashboardController'
		}).state('enrichCase', {
			url : '/enrich?caseId',
			templateUrl : function(){
				return $rootScope.start_url + '/enrichDashboard'
			},
			controller : 'EnrichDashboardController'
		}).state('enrichSearchItem', {
			url : '/enrich?searchItem',
			templateUrl : function(){
				return $rootScope.start_url + '/enrichDashboard'
			},
			controller : 'EnrichDashboardController'
		}).state('act', {
			url : '/act',
			templateProvider: ['$stateParams', '$templateRequest',
			      function($stateParams, $templateRequest) 
			      {
				  return UploadFileService.getGeneralSettings()
	                .then(function(response){  
				        var tplName = $rootScope.start_url + '/actDashboard';
				        angular.forEach(response.data['General Settings'],function(val,key){
							if(val.name == "Case View" && val.selectedValue !="default"){								
								tplName = $rootScope.start_url + '/actDashboardUnderwriting';
							}
						}) ;				        
						return $templateRequest(tplName);
	                })
			      }
			 ],
//			templateUrl : function(){
//				return $rootScope.start_url + '/actDashboard'
//			},
			controller: 'ActDashboardController'			
		}).state('actCase', {
			url : '/act/:caseId',
			templateProvider: ['$stateParams', '$templateRequest',
			      function($stateParams, $templateRequest) 
			      {
				  return UploadFileService.getGeneralSettings()
	                .then(function(response){  
				        var tplName = $rootScope.start_url + '/actDashboard';
				        angular.forEach(response.data['General Settings'],function(val,key){
							if(val.name == "Case View" && val.selectedValue !="default"){								
								tplName = $rootScope.start_url + '/actDashboardUnderwriting';
							}
						}) ;				        
						return $templateRequest(tplName);
	                })
			      }
			 ],
//			templateUrl : function(){
//				return $rootScope.start_url + '/actDashboard'
//			},
			controller: 'ActDashboardController'
		}).state('casePageLanding', {
			url : '/casePageLanding',
			templateUrl : function(){
				return $rootScope.start_url + '/casePageLanding'
			},
			controller : 'CasePageLandingController'
		}).state('auditTrail', {
			url : '/auditTrail',
			templateUrl : function(){
				return $rootScope.start_url + '/auditTrail'
			},
			controller : 'auditTrailController'
		}).state('userCaseLanding', {
			url : '/userCaseLanding',
			templateUrl : function(){
				return $rootScope.start_url + '/userCaseLanding'
			},
			//controller : 'userCaseLandingController'
		}).state('userCaseDetails', {
			url : '/userCaseDetails',
			templateUrl : function(){
				return $rootScope.start_url + '/userCaseDetails'
			},
			//controller : 'userCaseDetailsController'
		})
		.state('manage', {
			url : '/manage',
			templateUrl : function(){
				return $rootScope.start_url + '/manageDashboard'
			},
			controller : 'ManageDashboardController'
		}).state('social-live-feed', {
			url : '/live-feed/:searchText',
			templateUrl : function(){
				return $rootScope.start_url + '/live-feed'
			},
			controller: 'SocialLiveFeedContentController'
		}).state('userManagement', {
			url : '/userManagement',
			templateUrl : function(){
				return $rootScope.start_url + '/userManagement'
			},
			controller : 'UserManagementController'
		}).state('appManager', {
			url : '/appManager',
			templateUrl : function(){
				return $rootScope.start_url + '/appManager'
			},
			controller : 'AppManagerController'
		}).state('systemMonitoring', {
			url : '/systemMonitoring',
			templateUrl : function(){
				return $rootScope.start_url + '/systemMonitoring'
			},
			controller : 'SystemMonitoringController'
		}).state('dataManagement', {
			url : '/dataManagement',
			templateUrl : function(){
				return $rootScope.start_url + '/dataManagement'
			},
			controller : 'DataManagementController'
		}).state('generalSettings', {
			url : '/systemSettings',
			templateUrl : function(){
				return $rootScope.start_url + '/generalSettings'
			},
			controller : 'GeneralSettingsController'
		}).state('adverseNews', {
			url : '/adverseTransactions',
			templateUrl : function(){
				return $rootScope.start_url + '/adverseNews'
			},
			controller :'AdverseNewsController'
		}).state('screeningTesting', {
			url : '/screeningTesting',
			templateUrl : function(){
				return $rootScope.start_url + '/screeningTesting'
			},
			controller :'ScreeningTestingController'
		}).state('linkAnalysis', {
			url : '/linkAnalysis?q&p&type&caseId&identity',
			templateUrl : function(){
				return $rootScope.start_url + '/linkAnalysis'
			},
			controller :'LinkAnalysisController'
		}).state('users', {
			url : '/user',
			templateUrl : function(){
				return $rootScope.start_url + '/user'
			},
			controller: 'UserController'
		}).state('groups', {
			url : '/manageGroups',
			templateUrl : function(){
				return $rootScope.start_url + '/group'
			},
			controller: 'GroupController'
		}).state('manageDb', {
			url : '/manageDb',
			templateUrl : function(){
				return $rootScope.start_url + '/manageDb'
			},
			controller : 'ManageDbController'
		}).state('deployments', {
			url : '/deployments',
			templateUrl : function(){
				return $rootScope.start_url + '/deployments'
			},
			controller : 'DeploymentsController'
		}).state('jobs', {
			url : '/jobs',
			templateUrl : function(){
				return $rootScope.start_url + '/jobs'
			},
			controller : 'JobsController'
		}).state('administrations', {
			url : '/administrations',
			templateUrl : function(){
				return $rootScope.start_url + '/administrations'
			},
			controller : 'AdministrationsController'
		}).state('crystalBall', {
			url : '/crystalBall',
			templateUrl : function(){
				return $rootScope.start_url + '/crystalBall'
			},
			controller : 'CrystalBallController'
		}).state('activeProcess', {
			url : '/activeProcess',
			templateUrl : function(){
				return $rootScope.start_url + '/activeProcess'
			},
			controller: 'ActiveProcessController'
		}).state('suspendedProcess', {
			url : '/suspendedProcess',
			templateUrl : function(){
				return $rootScope.start_url + '/suspendedProcess'
			},
			controller : 'SuspendedProcessController'
		}).state('roles', {
			url : '/manageRoles',
			templateUrl : function(){
				return $rootScope.start_url + '/manageRoles'
			},
			//controller: 'RoleController'
		}).state('passwords', {
			url : '/managePasswords',
			templateUrl : function(){
				return $rootScope.start_url + '/managePasswords'
			},
			//controller: 'PasswordController'
		}).state('monitor', {
			url : '/manageMonitor',
			templateUrl : function(){
				return $rootScope.start_url + '/manageMonitor'
			},
			//controller: 'MonitorController'
		}).state('accessControls', {
			url : '/manageAccess',
			templateUrl : function(){
				return $rootScope.start_url + '/manageAccess'
			},
			//controller: 'AccessControlController'
		}).state('businessRules', {
			url : '/businessRules',
			templateUrl : function(){
				return $rootScope.start_url + '/businessRules'
			},
			//controller: 'BusinessRulesController'
		}).state('bigDataRules', {
			url : '/bigDataRules',
			templateUrl : function(){
				return $rootScope.start_url + '/bigDataRules'
			},
			//controller: 'BigDataRulesController'
		}).state('ontology', {
			url : '/ontology',
			templateUrl : function(){
				return $rootScope.start_url + '/ontology'
			},
			//controller: 'OntologyController'
		}).state('logics', {
			url : '/logics',
			templateUrl : function(){
				return $rootScope.start_url + '/logics'
			},
			//controller: 'LogicsController'
		}).state('myApps', {
			url : '/myApps',
			templateUrl : function(){
				return $rootScope.start_url + '/myApps'
			},
			//controller: 'MyAppsController'
		}).state('store', {
			url : '/store',
			templateUrl : function(){
				return $rootScope.start_url + '/store'
			},
			//controller: 'StoreController'
		}).state('appConfiguration', {
			url : '/appConfiguration',
			templateUrl : function(){
				return $rootScope.start_url + '/appConfiguration'
			},
			//controller: 'AppConfigurationController'
		}).state('licenseManager', {
			url : '/licenseManager',
			templateUrl : function(){
				return $rootScope.start_url + '/licenseManager'
			},
			//controller: 'LicenseManagerController'
		}).state('socialManager', {
			url : '/socialManager',
			templateUrl : function(){
				return $rootScope.start_url + '/socialManager'
			},
			//controller: 'SocialManagerController'
		}).state('myDataSources', {
			url : '/myDataSources',
			templateUrl : function(){
				return $rootScope.start_url + '/myDataSources'
			},
			//controller: 'MyDataSourcesController'
		}).state('addNewSources', {
			url : '/addNewSources',
			templateUrl : function(){
				return $rootScope.start_url + '/addNewSources'
			},
			//controller: 'AddNewSourcesController'
		}).state('manageSources', {
			url : '/manageSources',
			templateUrl : function(){
				return $rootScope.start_url + '/manageSources'
			},
			//controller: 'ManageSourcesController'
		}).state('exportImport', {
			url : '/exportImport',
			templateUrl : function(){
				return $rootScope.start_url + '/exportImport'
			},
			//controller: 'ExportImportController'
		}).state('events', {
			url : '/events',
			templateUrl : function(){
				return $rootScope.start_url + '/events'
			},
			//controller: 'EventsController'
		}).state('hosts', {
			url : '/hosts',
			templateUrl : function(){
				return $rootScope.start_url + '/hosts'
			},
//			controller: 'HostsController'
		}).state('response', {
			url : '/response',
			templateUrl : function(){
				return $rootScope.start_url + '/response'
			},
			//controller: 'ResponseController'
		}).state('fields', {
			url : '/fields',
			templateUrl : function(){
				return $rootScope.start_url + '/fields'
			},
			//controller: 'FieldsController'
		}).state('portalSettings', {
			url : '/portalSettings',
			templateUrl : function(){
				return $rootScope.start_url + '/portalSettings'
			},
			//controller: 'PortalSettingsController'
		}).state('serveSettings', {
			url : '/serveSettings',
			templateUrl : function(){
				return $rootScope.start_url + '/serveSettings'
			},
			//controller: 'ServeSettingsController'
		}).state('systemProperties', {
			url : '/systemProperties',
			templateUrl : function(){
				return $rootScope.start_url + '/systemProperties'
			},
			//controller: 'SystemPropertiesController'
		}).state('customFields', {
			url : '/customFields',
			templateUrl : function(){
				return $rootScope.start_url + '/customFields'
			},
			//controller: 'CustomFieldsController'
		}).state('portalInstances', {
			url : '/portalInstances',
			templateUrl : function(){
				return $rootScope.start_url + '/portalInstances'
			},
			//controller: 'PortalInstancesController'
		}).state('dataSource', {
			url : '/dataSource',
			templateUrl : function(){
				return $rootScope.start_url + '/dataSource'
			},
			controller : 'DataSourceController'
		}).state('tasks', {
			url : '/tasks',
			templateUrl : function(){
				return $rootScope.start_url + '/tasks'
			},
			controller : 'TasksController'
		}).state('processInstance', {
			url : '/processInstance',
			templateUrl : function(){
				return $rootScope.start_url + '/processInstance'
			},
			controller : 'ProcessInstanceController'
		}).state('reports', {
			url : '/reports',
			templateUrl : function(){
				return $rootScope.start_url + '/reports'
			},
			controller : 'ReportsController'
		}).state('deploymentProcess', {
			url : '/deploymentProcess',
			templateUrl : function(){
				return $rootScope.start_url + '/deploymentProcess'
			},
			controller : 'DeploymentProcessController'
		}).state('modelWorkspace', {
			url : '/modelWorkspace',
			templateUrl : function(){
				return $rootScope.start_url + '/modelWorkspace'
			},
			controller : 'ModelWorkspaceController'
		}).state('dataCuration', {
			url: '/dataCuration',
			templateUrl: function(){
				return $rootScope.start_url + '/dataCuration'
			},
			controller: 'StagingMipController'
		}).state('addDataCuration', {
			url: '/dataCuration/:identifier',
			templateUrl: function(){
				return $rootScope.start_url + '/addDataCuration'
			},
			controller: 'AddDataCurationController'
		}).state('data-find-data', {
			url : '/data-find-data/:queryText',
			templateUrl : function(){
				return $rootScope.start_url + '/data-find-data'
			},
			controller: 'DFDController'
		}).state('createNotification', {
			url : '/createNotification',
			templateUrl : function(){
				return $rootScope.start_url + '/createNotification'
			},
			controller: 'CreateNotificationController'
		}).state('createEvents', {
			url : '/createEvents',
			templateUrl : function(){
				return $rootScope.start_url + '/createEvents'
			},
			controller: 'CreateEventController'
		});
		
		$urlRouterProvider.when('', function(){
			 return "/domain";
			
		});
		$urlRouterProvider.otherwise(function($injector, $location){
			if($location.$$url == "/uploadDocuments/#!%2Flanding"){
				localStorage.setItem("domain", "INSURANCE");	
			}
			if($location.$$url == "/enrich/onBoard"){
				 $state.go('enrich', {onBoard: true});
			}else if($location.$$url =="/Questionnaire%20Builder"){
				window.location.href = KYC_QUESTIONNAIRE_PATH;
			}else if($location.$$url  == "/Policy%20Enforcement"){
				window.location.href = POLICY_ENFORCEMENT_PATH;
			}else if($location.$$url.indexOf("/#!")!= -1){				
				var url = window.location.href.split(window.location.hash)[0]+($location.$$url.split("/#!%2F").join("/#!/").slice(1))
				window.location.href = url;
			}else
			return '/404';
		});

			/*
		     * @purpose: Array index positioning 
		     * @created: 22 sep 2017
		     * @params: old_index(number), new_index(number)
		     * @return: array
		     * @author: Ankit
		    */  
			
		   Array.prototype.moveArray = function (old_index, new_index) {
			if (new_index >= this.length) {
				var k = new_index - this.length;
				while ((k--) + 1) {
					this.push(undefined);
				}
			}
			this.splice(new_index, 0, this.splice(old_index, 1)[0]);
			return this; // for testing purposes
		};
}]);