'use strict';

elementApp.config(['$stateProvider', '$provide', '$urlRouterProvider', 'EHUB_FE_API', '$locationProvider', function($stateProvider, $provide, $urlRouterProvider, EHUB_FE_API, $locationProvider){
	   window.appName = 'predict';
	   $stateProvider
		.state('predict', {
			url: '/landing',
			templateUrl: function(){
				return  EHUB_FE_API + appName + '/views/predictDashboard.jsp';
			},
			controller : 'PredictDashboardController'
		})
		.state('404', {
			url : '/404',
			templateUrl : function(){
				return EHUB_FE_API + 'error';
			}
		});
	   $urlRouterProvider.when('', '/landing');
	   $urlRouterProvider.otherwise(function(){
			return '/404';
		});
}]);