<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="bst_element" ng-app="ehubPredictApp">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="../assets/images/logo-new.png" />
		<title>Element by Blackswan</title>
		
		<!-------------------------------  Vendor Styles Starts  ------------------------------->
		<link href="./assets/css/predict-vendor.min.css" rel="stylesheet"/>
		<!-------------------------------  Vendor Styles Ends  --------------------------------->
		
		<!-------------------------------  Custom Styles Starts  ------------------------------->
		<link href="./assets/css/predict-styles.min.css" rel="stylesheet"/>
		<!-------------------------------  Custom Styles Ends  --------------------------------->
		<style>input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
			  -webkit-appearance: none; 
			  -moz-appearance: none;
			  appearance: none;
			  margin: 0;      
			}</style>
	</head>
	
	<body class="bst_element_body" ng-cloak>
	
		 <div ng-show="topPanelPreview">
		<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
		 </div>   
		<div ui-view></div>
		
		<!--  Chat Panel Wrapper Starts  -->
			<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
		<!--  Chat Panel Wrapper Ends  -->
		
		<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>
		
		<!--------------------------------  Common Vendor Js Starts  ------------------------------->
		<script src='../vendor/jquery/js/jquery.min.js'></script>
		<script src='../vendor/jquery/js/jquery-ui.js'></script>
		<script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
		<script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
		<script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
		<script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
		<script src='../vendor/jquery/js/bootstrap.min.js'></script>
		<script src="../vendor/jquery/js/jquery.orgchart.js"></script>
		<script src='../vendor/jquery/js/displacy-entity.min.js'></script>
	
		<script src='../vendor//angular/js/angular.min.js'></script>
		<script src='../vendor//angular/js/angular-ui-router.min.js'></script>
		<script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
		<script src='../vendor/angular/js/angular-sanitize.min.js'></script>
		<script src='../vendor//angular/js/html2canvas.js'></script>
		<script src='../vendor//angular/js/ng-file-upload.min.js'></script>
		<script src='../vendor//angular/js/angular-flash.min.js'></script>
		<script src="../vendor/angular/js/lodash.min.js"></script>
		<script src="../vendor/angular/js/FileSaver.min.js"></script>
		<script src="../vendor/jquery/js/angularjs-dropdown-multiselect.js"></script>
	
		<script src='../charts/d3.v3.min.js'></script>
		<script src='../charts/d3.v4.min.js'></script>
		<script src='../charts/d3v3.js'></script>
		<script src='../charts/d3js.js'></script>
	
		<script src='../scripts/VLA/js/cola.v3.min.js'></script>
		<script src='../scripts/VLA/js/cytoscape_2.7.12.js'></script>
		<script src='../scripts/VLA/js/cytoscape-cola.js'></script>
		<script src='../scripts/VLA/js/jquery.qtip.js'></script>
		<script src='../scripts/VLA/js/cytoscape-qtip.js'></script>
		<!--------------------------------  Common Vendor Js Ends    ------------------------------->
		
		<script src="../scripts/app.js"></script>
		<script src="./predict.app.js"></script>
		<script src="./predict.config.js"></script>
		<script src="./predict.run.js"></script>
	    
	    <!--------------------------------  Common Scripts Js Starts  ------------------------------>
<!-- 		<script src="../scripts/common-scripts.min.js"></script> -->
		<!--------------------------------  Common Scripts Js Ends   ------------------------------->		
		
		<script src='../scripts/common/constants/app.constant.js'></script>
		<script src='../scripts/common/constants/common.constant.js'></script>
		<script src='../scripts/discover/constants/discover.constant.js'></script>
		<script src='../scripts/act/constants/act.constant.js'></script>
	
		<script src='../scripts/common/services/shared.service.js'></script>
		<script src='../scripts/common/services/top.panel.api.service.js'></script>
		<script src='../scripts/common/services/upload.file.service.js'></script>
		<script src='../scripts/discover/services/discover.api.service.js'></script>
		<script src='../scripts/act/services/act.api.service.js'></script>
		<script src='../scripts/act/services/act.graph.service.js'></script>
		<script src="../scripts/common/services/common.service.js"></script>
		<script src='../scripts/common/services/riskScore.service.js'></script>
	
	
		<script src='../scripts/common/js/submenu.controller.js'></script>
		<script src='../scripts/common/js/top.panel.controller.js'></script>
		<script src='../scripts/common/js/advanced.search.controller.js'></script>
		<script src='../scripts/common/js/user.events.controller.js'></script>
		<script src='../scripts/common/js/my.clipboard.controller.js'></script>
		<script src="../scripts/common/js/notification.controller.js"></script>
		<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
		<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
		<script src="../scripts/common/js/chatbot.controller.js"></script>
		<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
		<script src="../scripts/common/modal/js/dataPopUp.modal.controller.js"></script>
		<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
	
	
		<script src='../scripts/common/js/entity.visualiser.js'></script>
	
		<script src='../scripts/common/modal/js/onboarding.modal.controller.js'></script>
		<script src='../scripts/common/modal/js/uploadquestionaire.controller.js'></script>
		<script src='../scripts/common/modal/js/upload.documents.modal.controller.js'></script>
		<script src='../scripts/common/modal/js/uploadKYCquestionaire.modal.controller.js'></script>
		<script src='../scripts/common/modal/js/idv.questionaire.controller.js'></script>
		<script src="../scripts/common/modal/js/create.event.controller.js"></script>
		<script src="../scripts/common/modal/js/participants.event.controller.js"></script>
	
		<script src='../scripts/enrich/services/enrich.api.service.js'></script>

		<!--------------------------------   Predict Dashboard Js Starts  ------------------------------->	
		<script src="./controllers/predict.dashboard.controller.js"></script>
		<!--------------------------------   Predict Dashboard Js Ends  ------------------------------->
			<script>
   $(document).ready(function () {
       /*--  Custom Scroll Bar  --*/
       $('.chat-bot-wrapper').mCustomScrollbar({
           axis: "y"
       });
   });
</script>
	</body>
</html>
