<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html ng-app="sourceManagementApp" class="bst_element">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../assets/images/logo-new.png" />
	<title>Source Management</title>

	<!-------------------------------  Vendor Styles Starts  ------------------------------->
	<link href="./assets/css/source-management-vendor.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../vendor/jasmine/jasmine.css">

	<!-- -----------------------------  Vendor Styles Ends  --------------------------------- -->

	<!---------------------------------  Custom Styles Starts  ------------------------------ -->
	<!-- <link href="./assets/css/source-management-styles.min.css" rel="stylesheet" type="text/css" /> -->
	<link href="../vendor/jquery/css/grid-table.min.css" rel="stylesheet" type="text/css" /><!-- 			<link href="../vendor/angular/css/ui-grid.min.css" rel="stylesheet" type="text/css" /> -->
	<link href="../assets/css/common-styles.min.css" rel="stylesheet" />
	<!-------------------------------  Custom Styles Ends  --------------------------------->
 <style>
	 .ui-grid-no-data{
	 	position: absolute;
	    top: 50%;
	    /* transform: translateY(-50%); */
	    opacity: 0.25;
	    font-size: 3em;
	    width: 100%;
	    text-align: center;
	    z-index: 1000;
	 }
	


	#source_management_mainDiv a.nav-link{
		text-transform:uppercase
	}
 </style>
	<!--<style>
		.ui-grid-top-panel,
		.ui-grid-header-viewport {
			overflow: visible !important;
		}
		.ui-grid-render-container-body{
		overflow: auto !important;
		}
		.ui-grid-viewport{
		overflow: visible !important;
		}
	</style> -->

</head>

<body ng-cloak class="custom-scroll-wrapper bst_element_body custom-modal-body dark-grey-dashboard">
	<flash-message class="main-flash-wrapper"></flash-message>
	<div ng-show="topPanelPreview">
		<%@include file="../WEB-INF/jsp/topPanel.jsp" %>
	</div>
	<div ui-view></div>
	<script src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBaYPqD7kVy4deebTXmjZpg75WKI378WZs"></script>
	<!--  Chat Panel Wrapper Starts  -->
	<%@include file="../WEB-INF/jsp/common/chatBot.jsp"%>
	<!--  Chat Panel Wrapper Ends  -->
	<!--==============================================================================
		*************************  Local Path Starts ********************************
		==============================================================================-->
	<!--------------------------------  Common Vendor Js Starts  ------------------------------->
	<script src='../vendor/jquery/js/jquery.min.js'></script>
	<script src='../vendor/jquery/js/jquery-ui.js'></script>
	<script src='../vendor/jquery/js/jquery.mThumbnailScroller.min.js'></script>
	<script src='../vendor/jquery/js/jquery.mCustomScrollbar.js'></script>
	<script src='../vendor/jquery/js/jquery.dataTables.min.js'></script>
	<script src='../vendor/jquery/js/dataTables.responsive.min.js'></script>
	<script src='../vendor/jquery/js/bootstrap.min.js'></script>

	<link href="../vendor/jquery/css/flag-icon.min.css" rel="stylesheet">

	<script src='../vendor//angular/js/angular.min.js'></script>
	<script src='../vendor//angular/js/angular-ui-router.min.js'></script>
	<script src='../vendor//angular/js/ui-bootstrap-tpls-2.5.0.min.js'></script>
	<script src='../vendor//angular/js/ng-file-upload.min.js'></script>


	<script src='../vendor/angular/js/angular-touch.js'></script>
	<script src='../vendor/angular/js/angular-animate.js'></script>
	<script src='../vendor/angular/js/angular-aria.js'></script>
	<script src="../vendor/angular/js/csv.js"></script>
	<script src="../vendor/angular/js/pdfmake.js"></script>
	<script src="../vendor/angular/js/vfs_fonts.js"></script>
	<script src="../vendor/angular/js/lodash.min.js"></script>
	<script src="../vendor/angular/js/jszip.min.js"></script>
	<script src="../vendor/angular/js/excel-builder.dist.js"></script>
	<script src='../vendor/angular/js/ui-grid.min.js'></script>

	<script src='../vendor//angular/js/angular-flash.min.js'></script>
	<script src='../vendor/angular/js/angular_multiselect.js'></script>

	<script src='../charts/d3.v3.min.js'></script>
	<script src='../charts/d3.v4.min.js'></script>
	<script src='../charts/d3v3.js'></script>
	<script src='../charts/d3js.js'></script>




	<!--------------------------------  isra Card Vendor Js Ends    ------------------------------->

	<script src="../scripts/app.js"></script>
	<script src="./sourceManagement.app.js"></script>
	<script src="./sourceManagement.config.js"></script>
	<script src="./sourceManagement.run.js"></script>


	<script src='../scripts/common/constants/app.constant.js'></script>
	<script src='../scripts/common/constants/common.constant.js'></script>
	<script src='../scripts/common/directives/common.directives.js'></script>
	<script src='../scripts/discover/constants/discover.constant.js'></script>
	<script src='../scripts/act/constants/act.constant.js'></script>
	<script src='../scripts/common/services/shared.service.js'></script>
	<script src='../scripts/common/services/top.panel.api.service.js'></script>
	<script src='../scripts/common/services/riskScore.service.js'></script>
	<script src='../scripts/common/services/upload.file.service.js'></script>
	<script src='../scripts/discover/services/discover.api.service.js'></script>
	<script src='../scripts/act/services/act.api.service.js'></script>
	<script src='../scripts/act/services/act.graph.service.js'></script>
	<script src="../scripts/common/services/common.service.js"></script>
	<script src="../scripts/common/js/chatbot.controller.js"></script>
	<script src="../scripts/common/modal/js/chatbot.modal.controller.js"></script>
	<script src='../scripts/common/js/top.panel.controller.js'></script>
	<script src='../scripts/common/js/advanced.search.controller.js'></script>
	<script src='../scripts/common/js/user.events.controller.js'></script>
	<script src='../scripts/common/js/my.clipboard.controller.js'></script>
	<script src="../scripts/common/js/notification.controller.js"></script>
	<script src="../scripts/common/modal/js/riskScore.modal.controller.js"></script>
	<script src='../scripts/common/modal/js/add.media.modal.controller.js'></script>
	<script src='../scripts/common/modal/js/widget.capture.modal.controller.js'></script>
	<script src="../scripts/common/modal/js/create.event.controller.js"></script>
	<script src="../scripts/common/modal/js/participants.event.controller.js"></script>
	<script src='../scripts/common/js/submenu.controller.js'></script>

	<!--------------------------------  Common Scripts Js Starts  ------------------------------>

	<!--------------------------------  Common Scripts Js Ends   ------------------------------->

	<!-------------------------------- Source Management Js Starts  ------------------------------>

	<script src="./services/sourceManagement.api.service.js"></script>
	<script src="./controllers/sourceManagement.controller.js"></script>

	<!--------------------------------  Source Management Scripts Js Ends   ------------------------------->

	<!-------------------------------- Source Management  modal Js Starts  ------------------------------>

	<script src="./modal/js/addNewSourceModal.controller.js"></script>
	<script src="./modal/js/editSourceModalSourceManagement.controller.js"></script>

	<!--------------------------------  Source Management modal Js Ends   ------------------------------->
	<!-- <script>
		(function ($) {
			$(window).on("click", function () {
				if ($(".multiselect-parent").hasClass("open")) {
					$(".ui-grid-top-panel").css({
						"overflow": "visible"
					});
					$(".ui-grid-header-viewport").css({
						"overflow": "visible"
					});
				}
				else{
					$(".ui-grid-top-panel").css({
						"overflow": "hidden"
					});
					$(".ui-grid-header-viewport").css({
						"overflow": "hidden"
					});
				}
			});
		})(jQuery);
	</script> -->
	
	<!--new minified Js start -->
	<!-- <script src="./assets/minifiedJs/sourceManagement.vendor.new.min.js"></script>

	<script src="./assets/minifiedJs/sourceManagement.intialize.new.min.js"></script>

	<script src="../scripts/common.scripts.new.min.js"></script>
	<script src="./assets/minifiedJs/sourceManagement.scripts.new.min.js"></script>
	<script src="./assets/minifiedJs/sourceManagement.moduleScripts.new.min.js"></script> -->
	<!--new minified Js end -->

	<script>
						(function ($) {
							$(window).on("load", function () {
								$(".multiselect-parent ul ,.ui-grid-menu-button .ui-grid-menu .ui-grid-menu-mid .ui-grid-menu-items").mCustomScrollbar({
									axis: "y",
									theme: "dark-3",
									autoHideScrollbar:"true",
								scrollbarPosition:"outside",
									advanced: {
										autoExpandHorizontalScroll: true //optional (remove or set to false for non-dynamic/static elements)
									}
								});
							});
						})(jQuery);
					</script>

</body>

</html>