'use strict';
angular.module('sourceManagementApp')
	.factory('SourceManagementAppAppApiService', sourceManagementAppAppApiService);

sourceManagementAppAppApiService.$inject = [
	'$http',
	'$q',
	'EHUB_API',
	'$rootScope'
];

function sourceManagementAppAppApiService(
	$http,
	$q,
	EHUB_API,
	$rootScope
) {
	return {
		saveGeneralSource: saveGeneralSource,
		getSources: getSources,
		updateSourceAPI: updateSourceAPI,
		getClassificationForSource: getClassificationForSource,
		addNewSourceAPI : addNewSourceAPI,
		getSourceIndustryList:getSourceIndustryList,
		getSourceDomainList:getSourceDomainList,
		getSourceMediaList:getSourceMediaList,
		getSourceJurisdictionList:getSourceJurisdictionList,
		getSourceCategories:getSourceCategories,
		saveSourceIndustryList:saveSourceIndustryList,
		saveSourceDomainList:saveSourceDomainList,
		saveSourceJurisdictionList:saveSourceJurisdictionList,
		saveSourceMediaList:saveSourceMediaList,
		searchDomainNewsData:searchDomainNewsData

	}

	function getClassificationForSource() {
		var apiUrl = EHUB_API + "classification/getClassifications?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
		});
		return (request
			.then(ClassificationForSourceSuccess)
			.catch(ClassificationForSourceError));

		/*ClassificationForSource error function*/
		function ClassificationForSourceError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*ClassificationForSource success function*/
		function ClassificationForSourceSuccess(response) {
			return (response);
		}
	}

	function saveGeneralSource() {
		var apiUrl = EHUB_API + "sourceManagement/saveGeneralSource?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'GET',
			url: apiUrl,
		});
		return (request
			.then(saveGeneralSourceSuccess)
			.catch(saveGeneralSourceError));

		/*saveGeneralSource error function*/
		function saveGeneralSourceError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*saveGeneralSource success function*/
		function saveGeneralSourceSuccess(response) {
			return (response);
		}
	}


	function getSources(classificationid,recordsPerPage,pageNumber,sourceFilterDto,orderBy,orderIn,subSlassificationId,visible) {
		if(!orderBy) orderBy = '';
		if(!orderIn) orderIn = '';
		if(!subSlassificationId) subSlassificationId = '';
		if(subSlassificationId) {orderBy = 'credibility';}
		if(visible == undefined || visible === '') {visible = '';}
		var apiUrl = EHUB_API + "sourceCredibility/getSources"  + '?recordsPerPage=' + recordsPerPage + '&pageNumber=' + pageNumber + '&classificationId=' + classificationid + '&orderBy=' + orderBy  + '&orderIn=' + orderIn + '&subSlassificationId=' + subSlassificationId + '&visible=' + visible + '&token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: sourceFilterDto ? sourceFilterDto : []
		});
		return (request
			.then(getSourcesSuccess)
			.catch(getSourcesError));

		/* getSources error function */
		function getSourcesError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* getSources success function */
		function getSourcesSuccess(response) {
			return (response);
		}
	}

	/* purpose: Add source
	* created: 4th jan 2019
	* author: karnakar
	*/


	function addNewSourceAPI(params) {
		var apiUrl = EHUB_API + "sourceCredibility/saveGeneralSource?token=" + $rootScope.ehubObject.token;
		var request = $http({
			method: 'POST',
			url: apiUrl,
			data:params
		});
		return (request
			.then(addNewSourceAPISuccess)
			.catch(addNewSourceAPIError));

		/*addNewSourceAPI error function*/
		function addNewSourceAPIError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			/*Otherwise, use expected error message.*/
			return ($q.reject(response.data.message));
		}

		/*addNewSourceAPI success function*/
		function addNewSourceAPISuccess(response) {
			return (response);
		}
	}

	/* purpose: Update source
	 * created: 2nd jan 2019
	 * params: params(object)
	 * return: success, error functions
	 * author: karnakar
	 */
	function updateSourceAPI(params) {
		var apiUrl = EHUB_API + "sourceCredibility/updateSource" + '?token=' + $rootScope.ehubObject.token;
		var request = $http({
			url: apiUrl,
			method: 'POST',
			data: params
		});
		return (request
			.then(updateSourceAPISuccess)
			.catch(updateSourceAPIError));

		/* updateSourceAPI error function */
		function updateSourceAPIError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* updateSourceAPI success function */
		function updateSourceAPISuccess(response) {
			return (response);
		}
	}

		/////////////////////========= Get source API's start for industry,domain,jurisdiction and media ============= /////////////////////
			
			/* 
			 * purpose: get source industry list details
			 * created: 3rd jan 2019
			 * author: karunakar
			 */

			function getSourceIndustryList(){
				var apiUrl = EHUB_API + "sourceIndustry/getSourceIndustry?token="+$rootScope.ehubObject.token;
				   var request = $http({
					   method:'GET' ,
					   url: apiUrl
				   });
				   return(request
						   .then(getSourceIndustryListSuccess)
						   .catch(getSourceIndustryListError));
		   
					/*getSourceIndustryList error function*/
				   function getSourceIndustryListError(response) {
					   if (!angular.isObject(response.data) || !response.data.message) {
						   return($q.reject(response.data));
					   }
					   /*Otherwise, use expected error message.*/
					   return($q.reject(response.data.message));
				   }
		   
					/*getSourceIndustryList success function*/
				   function getSourceIndustryListSuccess(response) {
					   return(response);
				   }
			}
			/* 
			 * purpose: get source domain list details
			 * created: 3rd jan 2019
			 * author: karunakar
			 */

			function getSourceDomainList(){
				var apiUrl = EHUB_API + "sourceDomain/getSourceDomain?token="+$rootScope.ehubObject.token;
				   var request = $http({
					   method:'GET' ,
					   url: apiUrl
				   });
				   return(request
						   .then(getSourceDomainListSuccess)
						   .catch(getSourceDomainListError));
		   
					/*getSourceDomainList error function*/
				   function getSourceDomainListError(response) {
					   if (!angular.isObject(response.data) || !response.data.message) {
						   return($q.reject(response.data));
					   }
					   /*Otherwise, use expected error message.*/
					   return($q.reject(response.data.message));
				   }
		   
					/*getSourceDomainList success function*/
				   function getSourceDomainListSuccess(response) {
					   return(response);
				   }
			   }
			   
			/* 
			 * purpose: get source jurisdiction list details
			 * created: 3rd jan 2019
			 * author: karunakar
			 */

			function getSourceJurisdictionList(){
				var apiUrl = EHUB_API + "sourceJurisdiction/getSourceJurisdiction?token="+$rootScope.ehubObject.token;
				   var request = $http({
					   method:'GET' ,
					   url: apiUrl
				   });
				   return(request
						   .then(getSourceJurisdictionListSuccess)
						   .catch(getSourceJurisdictionListError));
		   
					/*getSourceJurisdictionList error function*/
				   function getSourceJurisdictionListError(response) {
					   if (!angular.isObject(response.data) || !response.data.message) {
						   return($q.reject(response.data));
					   }
					   /*Otherwise, use expected error message.*/
					   return($q.reject(response.data.message));
				   }
		   
					/*getSourceJurisdictionList success function*/
				   function getSourceJurisdictionListSuccess(response) {
					   return(response);
				   }
			}
			   
			/* 
			 * purpose: get source media list details
			 * created: 3rd jan 2019
			 * author: karunakar
			 */

			function getSourceMediaList(){
				var apiUrl = EHUB_API + "sourceMedia/getSourceMedia?token="+$rootScope.ehubObject.token;
				   var request = $http({
					   method:'GET' ,
					   url: apiUrl
				   });
				   return(request
						   .then(getSourceMediaListSuccess)
						   .catch(getSourceMediaListError));
		   
					/*getSourceMediaList error function*/
				   function getSourceMediaListError(response) {
					   if (!angular.isObject(response.data) || !response.data.message) {
						   return($q.reject(response.data));
					   }
					   /*Otherwise, use expected error message.*/
					   return($q.reject(response.data.message));
				   }
		   
					/*getSourceMediaList success function*/
				   function getSourceMediaListSuccess(response) {
					   return(response);
				   }
			}
			  /* 
			 * purpose: get source category list details
			 * created: 3rd june 2019
			 * author:  Amritesh
			 */

			function getSourceCategories(){
				var apiUrl = EHUB_API + "sourceCredibility/getSourceCategories?token="+$rootScope.ehubObject.token;
				   var request = $http({
					   method:'GET' ,
					   url: apiUrl
				   });
				   return(request
						   .then(getSourceCategoriesSuccess)
						   .catch(getSourceCategoriesError));
		   
					/*getSourceCategories error function*/
				   function getSourceCategoriesError(response) {
					   if (!angular.isObject(response.data) || !response.data.message) {
						   return($q.reject(response.data));
					   }
					   /*Otherwise, use expected error message.*/
					   return($q.reject(response.data.message));
				   }
		   
					/*getSourceCategories success function*/
				   function getSourceCategoriesSuccess(response) {
					   return(response);
				   }
			} 
		/////////////////////========= Get source API's end for industry,domain,jurisdiction and media ============= /////////////////////

		/////////////////////========= save source API's start for industry,domain,jurisdiction and media ============= /////////////////////

			/* purpose: save Source Industry List
			* created: 3rd jan 2019
			* params: params(object)
			* return: success, error functions
			* author: karnakar
			*/
			function saveSourceIndustryList(params) {
				var apiUrl = EHUB_API + "sourceIndustry/saveSourceIndustry" + '?token=' + $rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data: params
				});
				return (request
					.then(saveSourceIndustrySuccess)
					.catch(saveSourceIndustryError));

				/* saveSourceIndustry error function */
				function saveSourceIndustryError(response) {
					if (!angular.isObject(response.data) || !response.data.message) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* saveSourceIndustry success function */
				function saveSourceIndustrySuccess(response) {
					return (response);
				}
			}

			/* purpose: save Source Domain List
			* created: 3rd jan 2019
			* params: params(object)
			* return: success, error functions
			* author: karnakar
			*/
			function saveSourceDomainList(params) {
				var apiUrl = EHUB_API + "sourceDomain/saveSourceDomain" + '?token=' + $rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data: params
				});
				return (request
					.then(saveSourceDomainSuccess)
					.catch(saveSourceDomainError));

				/* saveSourceDomain error function */
				function saveSourceDomainError(response) {
					if (!angular.isObject(response.data) || !response.data.message) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* saveSourceDomain success function */
				function saveSourceDomainSuccess(response) {
					return (response);
				}
			}
			/* purpose: save Source Industry List
			* created: 3rd jan 2019
			* params: params(object)
			* return: success, error functions
			* author: karnakar
			*/
			function saveSourceJurisdictionList(params) {
				var apiUrl = EHUB_API + "sourceJurisdiction/saveSourceJurisdiction" + '?token=' + $rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data: params
				});
				return (request
					.then(saveSourceJurisdictionSuccess)
					.catch(saveSourceJurisdictionError));

				/* saveSourceJurisdiction error function */
				function saveSourceJurisdictionError(response) {
					if (!angular.isObject(response.data) || !response.data.message) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* saveSourceJurisdiction success function */
				function saveSourceJurisdictionSuccess(response) {
					return (response);
				}
			}
			/* purpose: save Source Industry List
			* created: 3rd jan 2019
			* params: params(object)
			* return: success, error functions
			* author: karnakar
			*/
			function saveSourceMediaList(params) {
				var apiUrl = EHUB_API + "sourceMedia/saveSourceMedia" + '?token=' + $rootScope.ehubObject.token;
				var request = $http({
					url: apiUrl,
					method: 'POST',
					data: params
				});
				return (request
					.then(saveSourceMediaSuccess)
					.catch(saveSourceMediaError));

				/* saveSourceMedia error function */
				function saveSourceMediaError(response) {
					if (!angular.isObject(response.data) || !response.data.message) {
						return ($q.reject(response.data));
					}
					return ($q.reject(response.data.message));
				}
				/* saveSourceMedia success function */
				function saveSourceMediaSuccess(response) {
					return (response);
				}
			}
			   
		/////////////////////========= save source API's end for industry,domain,jurisdiction and media ============= /////////////////////

	 /* purpose: get news data
			* created: 10th June 2019
			* params: params(searchText)
			* return: success, error functions
			* author: Amarjith
			*/
	function searchDomainNewsData(searchText) {
		var apiUrl = EHUB_API + "sourceCredibility/getInfoFromDomains" + '?token=' + $rootScope.ehubObject.token + "&domain=" + searchText;
		// var apiUrl = "https://f6lqai1wta.execute-api.eu-west-1.amazonaws.com/Prod/v1/search-domain?count=10&domain="+searchText;
		var request = $http({
			url: apiUrl,
			method: 'GET'
		});
		return (request
			.then(searchDomainNewsDataSuccess)
			.catch(searchDomainNewsDataError));

		/* saveSourceMedia error function */
		function searchDomainNewsDataError(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				return ($q.reject(response.data));
			}
			return ($q.reject(response.data.message));
		}
		/* saveSourceMedia success function */
		function searchDomainNewsDataSuccess(response) {
			return (response);
		}
	}

}