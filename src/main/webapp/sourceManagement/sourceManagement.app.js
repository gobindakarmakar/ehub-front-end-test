'use strict';

var elementApp = angular.module('sourceManagementApp', ['ui.bootstrap', 'ui.router','ngFileUpload','ngFlash', 'ngTouch', 'ui.grid','ui.grid.pagination', 'ui.grid.exporter','ui.grid.autoResize',  'ui.grid.resizeColumns', 'ui.grid.selection', 'ui.grid.pinning','ui.grid.moveColumns','ui.grid.saveState','angularjs-dropdown-multiselect']);
var elementModule = elementApp;
