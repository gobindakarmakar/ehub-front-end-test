'use strict';
angular.module('sourceManagementApp')
	.controller('SourceManagementController', SourceManagementController);

SourceManagementController.$inject = [
	'$scope',
	'$rootScope',
	'$q',
	'uiGridConstants',
	'CommonService',
	'SourceManagementAppAppApiService',
	'$uibModal',
	'HostPathService',
	'$sce'
];


function SourceManagementController($scope, $rootScope,$q,uiGridConstants, CommonService, SourceManagementAppAppApiService, $uibModal,HostPathService, $sce) {
	$rootScope.mediaGridValue = '';
	var slidertrusted = {};
	window.currentMediaData={};
	var currentUpdatedObj ={};
	$scope.currentPageSize = 10;
	$scope.pageNo = 1;
	$scope.mainClassificationData = [];
	$scope.currentTab = '';
	$scope.btnSubmit = false;
	$scope.filtersValue ={};
	$scope.tabChangeFiltersVal ={};
	$scope.SortValues = {};
	$scope.hideShowDiv = {
		showAddNewIndustryAttributeSection : false,
		showAddNewDomainAttributeSection : false,
		showAddNewMediaAttributeSection : false,
		showAddNewJurisdictionAttributeSection : false
	};
	$scope.classificationsForAddSource = {};//for add source controller
	$scope.isPageNumChanged = false;
	var gridheaders = {};
	$rootScope.sliderObjectKeys = [];
	$scope.mediaLists = [];
	$scope.mediaListsforsingleselect = [];
	$scope.IndustryList =[];
	$scope.CategoriesList =[];
	$scope.DomainList =[];
	$scope.JurisdictionList =[];
	$scope.appliedFilters = [];
	var gridstaticheaders = [
		{
			'column': 'Source',
			'type': 'Text',
			'displayval': 'sourceDisplayName',
			'columnsortname': 'sourcedisplayname',
			'FilterDropdownData': [],
			'enableColumnMenu': false, 
			'enableHiding': false
		},
		{
			'column': 'Link',
			'type': 'Hyperlink',
			'displayval': 'sourceUrl',
			'columnsortname': 'sourceurl',
			'FilterDropdownData': [],
			'enableColumnMenu': true, 
			'enableHiding': true
		}, {
			'column': "domain",
			'type': "Multi-Select",
			'displayval': 'sourceDomain',
			'columnsortname': 'domain',
			'FilterDropdownData': [],
			'enableColumnMenu': true, 
			'enableHiding': true
		},
		{
			'column': "category",
			'type': "Multi-Select",
			'displayval': 'sourceCategory',
			'columnsortname': 'category',
			'FilterDropdownData': [],
			'enableColumnMenu': true, 
			'enableHiding': true
		},
		{
			'column': "industry",
			'type': "Multi-Select",
			'displayval': 'sourceIndustry',
			'columnsortname': 'industry',
			'FilterDropdownData': [],
			'enableColumnMenu': true, 
			'enableHiding': true

		},
		{
			'column': "jurisdiction",
			'type': "Multi-Select",
			'displayval': 'sourceJurisdiction',
			'columnsortname': 'jurisdiction',
			'FilterDropdownData': [],
			'enableColumnMenu': true, 
			'enableHiding': true
		}

	];
	$scope.mapMediaHash = {
			Doc: 'file-word-o text-dodger-blue2',
			Excel: 'file-excel-o text-dark-pastal-green',
			PPT: 'file-powerpoint-o text-coral-red',
			PDF: 'file-pdf-o text-coral-red',
			Movies: 'file-video-o text-deep-lilac',
			Audio: 'file-audio-o text-tealish-blue',
			Screenshot :'crop',
			All:"files-o"
	};
	
	
//	$scope.reverseMapMediaHash = _.invert($scope.mapMediaHash);
	/*@Purpose:Convert slider value for UI
	* return : null
	* @author: Varsha
	* @Date: 5th Jan 2019
	*/
	
	
	function getDecryptValueForRangeSlider(input){
		var mapHash = {
		    'NONE':0,
		    'LOW':1,
		    'MEDIUM':2,
		    'HIGH':3
		  };
			return mapHash[input];
	}
	
	/*@Purpose:Convert back slider value for api
	* return : null
	* @author: Varsha
	* @Date: 5th Jan 2019
	*/
	
	
	function getEncryptValueForRangeSlider(input){
		var mapHash = {
		    0:'NONE',
		    1:'LOW',
		    2:'MEDIUM',
		    3: 'HIGH'
		  };
			return mapHash[input];
	}
	
	/*@Purpose: Prepend the option label 'All' on the top from the lists
	* return : array
	* @param: dropdownvals <Array>
	* @author: Amarjith
	* @Date: 01 Feb 2019
	*/
	function unShiftAllfromList(dropdownvals){
		// unshift 'All' from the dropdown values list
		var find_label_allIndex = dropdownvals.findIndex(function(d){ return d.id.toLowerCase() == 'all'; });
		if(find_label_allIndex >=0){
			var label_allObject = dropdownvals[find_label_allIndex];
			dropdownvals = dropdownvals.filter(function(d){ return d.id.toLowerCase() != 'all'; });
			dropdownvals.unshift(label_allObject);
		}
		return  dropdownvals;
	}

	/*@Purpose: Calling Media Sources Api 
	* 
	* return : null
	* @author: Amarjith Kumar
	* @Date: 04th-Dec-2019
	*/
	function getListofSources(type) {
		
		if(type == 'All' || type=="media"){
			//get media list
			SourceManagementAppAppApiService.getSourceMediaList().then(function (response) {		
				angular.forEach(response.data,function(val){
					$scope.mediaLists.push({
						id: CommonService.jsUcfirst(val.mediaName).trim(),
	                    label: CommonService.jsUcfirst(val.mediaName).trim(),
	                    mediaId: val.mediaId,
	                    mediaName: CommonService.jsUcfirst(val.mediaName).trim()
					});
					$scope.mediaLists = $scope.mediaLists.sort(CommonService.compareStrings);
					$scope.mediaLists = unShiftAllfromList($scope.mediaLists);
					$scope.mediaListsforsingleselect.push(CommonService.jsUcfirst(val.mediaName));
				});
			}, function () {
			});
		}
		if(type == 'All' || type=="industry"){
			//get industry list
			SourceManagementAppAppApiService.getSourceIndustryList().then(function (response) {
				angular.forEach(response.data,function(val){
					$scope.IndustryList.push({
						id: CommonService.jsUcfirst(val.industryName).trim(),
	                    label: CommonService.jsUcfirst(val.industryName).trim(),
	                    industryId: val.industryId,
	                    industryName: CommonService.jsUcfirst(val.industryName).trim()
					});					
				});
				$scope.IndustryList = $scope.IndustryList.sort(CommonService.compareStrings);
				$scope.IndustryList = unShiftAllfromList($scope.IndustryList);
		    }, function () {
		    });
		}
		if(type == 'All' || type=="category"){
			//get industry list
			SourceManagementAppAppApiService.getSourceCategories().then(function (response) {
				angular.forEach(response.data,function(val){
					$scope.CategoriesList.push({
						id: CommonService.jsUcfirst(val.categoryName).trim(),
	                    label: CommonService.jsUcfirst(val.categoryName).trim(),
	                    categoryId: val.categoryId,
	                    categoryName: CommonService.jsUcfirst(val.categoryName).trim()
					});					
				});
				$scope.CategoriesList = $scope.CategoriesList.sort(CommonService.compareStrings);
				// $scope.CategoriesList = unShiftAllfromList($scope.CategoriesList);
		    }, function () {
		    });
		}
		if(type == 'All' || type=="domain"){
			//get domain list
		    SourceManagementAppAppApiService.getSourceDomainList().then(function (response) {
		    	angular.forEach(response.data,function(val){
					$scope.DomainList.push({
						id: CommonService.jsUcfirst(val.domainName).trim(),
	                    label: CommonService.jsUcfirst(val.domainName).trim(),
	                    domainId: val.domainId,
	                    domainName: CommonService.jsUcfirst(val.domainName).trim()
					});
				});
				$scope.DomainList = $scope.DomainList.sort(CommonService.compareStrings);	
				$scope.DomainList = unShiftAllfromList($scope.DomainList);
		    }, function () {
		    });
		}
		if(type == 'All' || type=="jurisdiction"){
			//get jurisdiction list
		    SourceManagementAppAppApiService.getSourceJurisdictionList().then(function (response) {
		    	angular.forEach(response.data,function(val){
					$scope.JurisdictionList.push({
						id: val.jurisdictionOriginalName,
	                    label: val.jurisdictionOriginalName,
	                    jurisdictionId: val.jurisdictionId,
						jurisdictionName: val.jurisdictionName,
						jurisdictionOriginalName:val.jurisdictionOriginalName,
						selected:val.selected
					});
				});
		    	$scope.JurisdictionList = unShiftAllfromList($scope.JurisdictionList);
				getClassifications();
		    }, function () {
			});
		}
	}
	getListofSources('All');

	/*@Purpose: saving source Api's 
	* return : params
	* @author: karnakar
	* @Date: 7th-Jan-2019
	*/

	$scope.saveSource = function(newAttribute,type){		
		if(type == "domain"){
			var params = {"domainName" : newAttribute};
			SourceManagementAppAppApiService.saveSourceDomainList(params).then(function () {
				    $scope.DomainList =[];
					$scope.hideShowDiv.showAddNewDomainAttributeSection = false;
					getListofSources('domain');
				}, function () {
			});
		}
		else if(type == "jurisdiction"){
			var params = {"jurisdictionOriginalName" : newAttribute};
			SourceManagementAppAppApiService.saveSourceJurisdictionList(params).then(function () {
				$scope.JurisdictionList =[];
				$scope.hideShowDiv.showAddNewJurisdictionAttributeSection = false;
				getListofSources('jurisdiction');
				}, function () {
			});
		}
		else if(type == "industry"){
			var params = {"industryName" : newAttribute};
			SourceManagementAppAppApiService.saveSourceIndustryList(params).then(function () {
				$scope.IndustryList =[];
				$scope.hideShowDiv.showAddNewIndustryAttributeSection = false;
				getListofSources('industry');
				}, function () {
			});
		}
		else if(type == "media"){
			var params = {"mediaName" : newAttribute};
			SourceManagementAppAppApiService.saveSourceMediaList(params).then(function () {
				$scope.mediaLists = [];
				$scope.mediaListsforsingleselect = [];
				$scope.hideShowDiv.showAddNewMediaAttributeSection = false;
				getListofSources('media');
				}, function () {
			});
		}
	};

	/*@Purpose: Calling Classification Api 
	* 
	* return : null
	* @author: Amarjith Kumar
	* @Date: 04th-Dec-2019
	*/
	var classificationList = [];
	function getClassifications() {
		SourceManagementAppAppApiService.getClassificationForSource().then(function (response) {			
			angular.forEach(response.data, function (val) {
				if(val.classifcationName == 'GENERAL'){
					$scope.getSources(val,$scope.currentPageSize,$scope.pageNo,false);
				}
				$scope.classificationsForAddSource = response.data;//for add soucre controller
				classificationList.push(val);
				$scope.fullpageloader = true;
			});	
		}, function () {
		});
	}

	/*@Purpose: Calling getSource Api based ClassificationId 
	* @param: val: Object
	* return : null
	* @author: Amarjith Kumar
	* @Date: 04th-Dec-2019
	*/
	var originalSourceData =[];
	$scope.mainClassificationData = [];
	$scope.getSources = function (val,recordPerPage,pageNum,isDataExists,sourceFilterDto,orderBy,orderIn,subSlassificationId,visible) {
		$scope.fullpageloader = true;
		$scope.isPageNumChanged = false;
		//call API to get the data
		SourceManagementAppAppApiService.getSources(val.classificationId,recordPerPage,pageNum,sourceFilterDto,orderBy,orderIn,subSlassificationId,visible).then(function (response) {
			if(response.data.result && _.isEmpty(response.data.result)){
				$scope.gridOptions.data = [];
				$scope.fullpageloader = false;
				return;
			}
			
			if(!isDataExists){
				originalSourceData.push(response.data) ;
				response.data.result.map(function(d){
					var classifiIndex  = d.classifications.findIndex(function(d){return d.classificationId == val.classificationId;});
					d.currentClassification  = d.classifications[classifiIndex];
				});
				$scope.mainClassificationData.push({
					'title': val.classifcationName,
					'key': val.classifcationName,
					'classificationId': val.classificationId,
					'subClassifications': val.subClassifications,
					'values':response.data.result,
					'paginationOptions' : {
						paginationSizes: [],
						pageNum: $scope.pageNo,
						pageSize: $scope.currentPageSize,
						totalSourceCount: originalSourceData[0].paginationInformation.totalResults
					},
					'hiddenColumns':[],
					'initialColumnOrder':null,
					'columnOrders':{},
					'state':{}
//					'hideStatusDto': {
						//commented EL-3745 ticket
//					    classificationId: 2451780,
//					    hideCategory: null,
//					    hideDomain: false,
//					    hideIndustry: false,
//					    hideJurisdiction: false,
//					    hideLink: false,
//					    sourceId: 2473273,
//					    sourcesHideStatusId: 2473278,
//					    visible: true
//					  }
				});

				classificationList.map(function(val){
					if(val.classifcationName != 'GENERAL' && val.classifcationName!= null){
						$scope.mainClassificationData.push({
							'title': val.classifcationName,
							'key': val.classifcationName,
							'classificationId': val.classificationId,
							'subClassifications': val.subClassifications,
							'values':[],
							'paginationOptions' : {
								paginationSizes: [],
								pageNum: $scope.pageNo,
								pageSize: $scope.currentPageSize,
								totalSourceCount: 0
							},
							'hiddenColumns':[],
							'initialColumnOrder':null,
							'columnOrders':{},
							'state':{}
//							'hideStatusDto': {
//							    classificationId: 2451780,
//							    hideCategory: null,
//							    hideDomain: false,
//							    hideIndustry: false,
//							    hideJurisdiction: false,
//							    hideLink: false,
//							    sourceId: 2473273,
//							    sourcesHideStatusId: 2473278,
//							    visible: true
//							  }
						});
					}
					
				});
				
				//load data for first classification 				
					$scope.currentTab = $scope.mainClassificationData[0].title;
					$scope.currentTabIndex = 0;
					$scope.tabIndexValue = 0;
					$scope.totalSourceCount = originalSourceData[$scope.tabIndexValue].paginationInformation.totalResults;
					$scope.mainClassificationData[0].paginationOptions.paginationSizes = [$scope.mainClassificationData[0].paginationOptions.pageSize, $scope.mainClassificationData[0].paginationOptions.pageSize * 2, $scope.mainClassificationData[0].paginationOptions.pageSize * 3];
					creatGridData($scope.mainClassificationData[0]);				
					
			}else{
				var tabIndexFound = originalSourceData.findIndex(function(d){return d.result[0].classifications[0].classifcationName.toLowerCase() === $scope.currentTab.toLowerCase();});
				if(tabIndexFound < 0){
					originalSourceData.push(response.data);
				}else{
					originalSourceData[tabIndexFound] = response.data;
				}				
				$scope.currentTabIndex = originalSourceData.map(function(d){
					return d.result[0].classifications[0].classifcationName.toLowerCase();
				}).indexOf($scope.currentTab.toLowerCase());
				$scope.mainClassificationData[$scope.tabIndexValue].values = [];
				$scope.totalSourceCount = originalSourceData[$scope.currentTabIndex].paginationInformation.totalResults;
				$scope.mainClassificationData[$scope.tabIndexValue].values =response.data.result;
				$scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.totalSourceCount = $scope.totalSourceCount;
				creatGridData($scope.mainClassificationData[$scope.tabIndexValue]);
				
			}
			 $scope.fullpageloader = false;

		}, function () {
			$scope.fullpageloader = false;
			//show error 
			$scope.mainClassificationData.push({
				'title': val.classifcationName,
				'key': val.classifcationName,
				'classificationId': val.classificationId,
				'subClassifications': val.subClassifications,
				'hideStatusDto':null,
				'initialColumnOrder':null,
				'columnOrders':{},
				'hiddenColumns':[],
				'state':{},
				'values':[]
			});	
		});
	};
	

	/*@Purpose: Update Sliders value
	* return : null
	* @author: Varsha
	* @Date: 5th Jan 2019
	*/
	
	$rootScope.setSliderDataForSource = function(data,sliderName){
		var index = originalSourceData[$scope.currentTabIndex].result.map(function(d){
			return d.sourceId;
		}).indexOf(data.entity.sourceId);
		var subClassificationId = data.entity[sliderName+'_value'].subClassificationId;
		var dataTochange = jQuery.extend(true,{},originalSourceData[$scope.currentTabIndex].result[index]);
		if(dataTochange   && dataTochange.classifications && dataTochange.classifications[0]){
			var indexClassification = dataTochange.classifications[0].subClassifications.map(function(d){
				return d.subClassificationId;
			}).indexOf(subClassificationId);
			
		
		for(var i=0;i<Object.keys(dataTochange).length;i++){
			if(Object.keys(dataTochange)[i] != 'classifications' &&  Array.isArray(dataTochange[Object.keys(dataTochange)[i]])){
				dataTochange[Object.keys(dataTochange)[i]] =[];
			}
		}
		$scope.mainClassificationData[$scope.tabIndexValue].values[index].classifications[0].subClassifications[indexClassification].subClassificationCredibility = getEncryptValueForRangeSlider(data.entity[sliderName]) ;
		dataTochange.classifications[0].subClassifications[indexClassification].subClassificationCredibility = getEncryptValueForRangeSlider(data.entity[sliderName]) ;
		dataTochange.classifications[0].subClassifications.map(function(d){
			d.dataAttributes = [];
		});
		
		if($scope.mainClassificationData[$scope.tabIndexValue].values[index].classifications[0].subClassifications[indexClassification].dataAttributes.length>0){
			$scope.mainClassificationData[$scope.tabIndexValue].values[index].classifications[0].subClassifications[indexClassification].dataAttributes.map(function(d){
				d.credibilityValue = getEncryptValueForRangeSlider(data.entity[sliderName]) ;
			});
		}
			
	
		delete dataTochange.currentClassification;
		$scope.updateSource(dataTochange);

		}
		
	};
	
	/*@Purpose: Update visibility value
	* return : null
	* @author: Varsha
	* @Date: 5th Jan 2019
	*/	
	
	var visibleStatus = false;
	var visibleIndex;
	var visibleResultValue;
	$rootScope.updateVisibity = function(key,data){
		visibleStatus = true;
		var index = originalSourceData[$scope.currentTabIndex].result.map(function(d){
			return d.sourceId;
		}).indexOf(data.entity.sourceId);
		visibleIndex = index;
		var dataTochange = jQuery.extend(true,{},originalSourceData[$scope.currentTabIndex].result[index]);
		for(var i=0;i<Object.keys(dataTochange).length;i++){
			if(Object.keys(dataTochange)[i] != 'classifications' &&  Array.isArray(dataTochange[Object.keys(dataTochange)[i]])){
				dataTochange[Object.keys(dataTochange)[i]] =[];
			}
		}
		dataTochange.classifications[0].subClassifications.map(function(d){
			d.dataAttributes = [];
		});
		visibleResultValue = !data.entity.Visible;
		delete dataTochange.currentClassification;
		dataTochange.classifications[0].hideStatusDto.visible = !data.entity.Visible;
		$scope.updateSource(dataTochange);
	

	};
	
	function changeDataForVisibilty(status){
		if(status){
			$scope.mainClassificationData[$scope.tabIndexValue].values[visibleIndex].classifications[0].hideStatusDto.visible = visibleResultValue;
			$scope.gridApi.core.refresh();
		}
	}
	
	
	/*@Purpose: function to create grid data  
	* 
	* return : null
	* @author: Amarjith Kumar
	* @Date: 28th-Dec-2018
	*/
	function creatGridData(val) {
	//	if (gridheaders[val.key]) {
			gridheaders[val.key] = {
				headers: [],
				data: []
			};
	//	};
		$scope.uniqueIndustries = [];
		$scope.uniqueDomains = [];
		$scope.uniqJurisdiction = [];
		var uniqdynamicHeadersArr = [];
		var dynamicHeaders = [];
		val.values.map(function (value) {
			var gridData = {};
			value.sourceIndustry.map(function (v1) {
				$scope.uniqueIndustries.push(v1);
			});
			value.sourceDomain.map(function (v1) {
				$scope.uniqueDomains.push(v1);
			});
			value.sourceJurisdiction.map(function (v1) {
				$scope.uniqJurisdiction.push(v1);
			});
			$scope.uniqueIndustries = _.compact(_.uniq($scope.uniqueIndustries));
			$scope.uniqueDomains = _.compact(_.uniq($scope.uniqueDomains));
			$scope.uniqJurisdiction = _.compact(_.uniq($scope.uniqJurisdiction));
			angular.forEach(gridstaticheaders, function (v) {
				if (v.column == "jurisdiction" || v.column == 'industry' || v.column == 'domain') {
					var mappedData = $.map(value[v.displayval ? v.displayval : v.column], function (d) {
						if(v.column == 'domain'){
							return d.domainName;
						}else if(v.column == 'jurisdiction'){
							return d.jurisdictionOriginalName;
						}else if(v.column == 'industry'){
							return d.industryName;
						} 
					});
					gridData[v.column] = mappedData.join(", ");
				} else if (v.column == 'Link') {
					gridData[v.column] = value.sourceName;
					gridData.sourceLink = value[v.displayval ? v.displayval : v.column];
					gridData.sourceLinkName = value[v.displayval ? v.displayval : v.column];
					if(gridData.sourceLink && !gridData.sourceLink.match(/^[a-zA-Z]+:\/\//)){
						if(gridData.sourceLink){
							gridData.sourceLink = window.location.protocol + '//' + gridData.sourceLink;
						}						
					}
				} else if(v.column == 'category'){
					gridData[v.column] =  value[v.column]; 
				} else if (v.column == 'Source') {
					gridData[v.column] =  value.sourceDisplayName;
					gridData.sourceId = value.sourceId;
					gridData.sourceMedia = value.sourceMedia;
				} else {
					gridData[v.column] = value[v.displayval ? v.displayval : v.column];
				}
			});
			gridData.Classification = value.classifications;
			value.classifications[0].subClassifications.map(function (v1,key) {
				gridData[v1.subClassifcationName.split(' ').join('')+'_value'] = v1;
				// checking if all data attributes contains same value
    			var allEqual =  gridData[v1.subClassifcationName.split(' ').join('')+'_value'].dataAttributes.every(  v => v.credibilityValue === gridData[v1.subClassifcationName.split(' ').join('')+'_value'].dataAttributes[0].credibilityValue);
    			if(allEqual){
    				gridData[v1.subClassifcationName.split(' ').join('')+'_isDisabled'] = false; //getDecryptValueForRangeSlider(gridData[v1.subClassifcationName.split(' ').join('')+'_value'].dataAttributes[0].credibilityValue);
    			}else{
    				gridData[v1.subClassifcationName.split(' ').join('')+'_isDisabled'] = true;
    			}
				if (uniqdynamicHeadersArr.indexOf(v1.subClassifcationName.toLowerCase()) == -1) {
					uniqdynamicHeadersArr.push(v1.subClassifcationName.toLowerCase());
					dynamicHeaders.push({
						columnFieldName: v1.subClassifcationName.split(' ').join(''),
						columnHeaderName: v1.subClassifcationName, //.split(' ').join(''),
						columnType: 'Slider',
						slider: true,
						editable: false,
						enableFilter: true,
						width: 250,
						headerClass: '',
						cellClass: '',
						subclassifications:"subclassifications"+(key+1),
						subclassificationId : v1.subClassificationId,
						FilterDropdownData:['ALL','NONE','LOW','MEDIUM','HIGH'],
						enableColumnMenu: true, 
						enableHiding : true
					});
				}
			});
			gridheaders[val.key].data.push(gridData);
		});

		dynamicHeaders.push({
			columnFieldName: "media",
			columnHeaderName: "Media",
			columnsortname: 'media',
			columnType: "media",
			slider: false,
			editable: false,
			enableFilter: true,
			width: 150,
			headerClass: '',
			cellClass: '',
			media:true,
			FilterDropdownData: $scope.mediaListsforsingleselect,
			enableColumnMenu: true, 
			enableHiding: true
		});
		dynamicHeaders.push({
			columnFieldName: "Visible",
			columnHeaderName: "Visible",
			columnType: "visible",
			columnsortname: 'visible',
			slider: false,
			visible: true,
			editable: false,
			enableFilter: true,
			width: 150,
			headerClass: '',
			cellClass: '',
			FilterDropdownData: ['visible', 'Non Visible', 'All'],
			enableColumnMenu: false, 
			enableHiding: false
		});

		dynamicHeaders.push({
			columnFieldName: "edit",
			columnHeaderName: "Edit",
			columnType: "edit",
			slider: false,
			visible: true,
			editable: false,
			enableFilter: true,
			width:140,
			headerClass: '',
			cellClass: '',
			FilterDropdownData: [],
			enableColumnMenu: false, 
			enableHiding: false
		});
		
		var hideStatusDto = jQuery.extend(true, {}, val.hideStatusDto);
	    angular.forEach(gridstaticheaders, function (v) {
			var dropdownvals = v.FilterDropdownData;
			var enableColumn = false;
			if (v.column == 'industry') {
				enableColumn = !hideStatusDto.hideIndustry;
				dropdownvals = $.map($scope.IndustryList, function (d) {
					return d.label;
				});
			} else if (v.column == 'domain') {
				enableColumn = !hideStatusDto.hideDomain;
				dropdownvals = $.map($scope.DomainList, function (d) {
					return d.label;
				});
			} else if (v.column == 'jurisdiction') {
				enableColumn = !hideStatusDto.hideJurisdiction;
				dropdownvals = $.map($scope.JurisdictionList, function (d) {
					return d.label;
				});
			} else if (v.column == 'Link') {
				enableColumn = !hideStatusDto.hideLink;
			} else if (v.column == 'category') {
				enableColumn = !hideStatusDto.hideCategory;
				dropdownvals = $.map($scope.CategoriesList, function (d) {
					return d.label;
				});
			} else{
				enableColumn = true;
			}
			dropdownvals = dropdownvals.sort();
			
			if(dropdownvals && dropdownvals.length > 0){
				// unshift 'All' from the dropdown values list
				var find_label_allIndex = dropdownvals.findIndex(function(d){ return d.toLowerCase() == 'all'; });
				if(find_label_allIndex >=0){
					var label_allObject = dropdownvals[find_label_allIndex];
					dropdownvals = dropdownvals.filter(function(d){ return d.toLowerCase() != 'all'; });
					dropdownvals.unshift(label_allObject);
				}else{
					// if All is not exist in the dropdown value list
					dropdownvals.unshift('All');
				}
			}
			
			
			gridheaders[val.key].headers.push({
				columnFieldName: v.column,
				enableColumnMenu:v.enableColumnMenu,
				enableHiding: v.enableHiding,
				columnSortName:v.columnsortname.toLowerCase(),
				columnHeaderName: v.column,
				columnType: v.type,
				enableColumn: enableColumn,
				slider: false,
				editable: false,
				enableFilter: true,
				width: 200,
				headerClass: '',
				cellClass: '',
				FilterDropdownData: dropdownvals
			});

		});
		gridheaders[val.key].headers = gridheaders[val.key].headers.concat(dynamicHeaders);
		angular.forEach(gridheaders[val.key].headers,function(headerval){
			var enableColumn = true;
			if(val.hiddenColumns.indexOf(headerval.columnFieldName) >=0){
				enableColumn = false;
			}
			headerval.enableColumn = enableColumn;
		});

		if (val.key == $scope.currentTab) {
			populateGridTable();
		}
	}

	var sampleAddTogridTable = {};
	var currentData = [];
	function populateGridTable() {
		// console.log($scope.totalSourceCount );
		// $scope.gridOptions.totalItems = $scope.totalSourceCount;   
		
		$scope.totalPage = Math.ceil($scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.totalSourceCount / $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.pageSize);  
		$scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.paginationSizes = [$scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.pageSize, $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.pageSize * 2, $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.pageSize * 3];
		var gridOptions = {
			totalItems : $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.totalSourceCount,
			exporterMenuPdf: false,
			paginationPageSizes: $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.paginationSizes,
			paginationPageSize: $scope.mainClassificationData[$scope.tabIndexValue].paginationOptions.pageSize,
			exporterMenuExcel: false,
			columnDefs: gridheaders[$scope.currentTab].headers
		};
		currentData = gridheaders[$scope.currentTab];
		manipulateDataForUIgrid(gridheaders[$scope.currentTab]);

		var gridData = gridheaders[$scope.currentTab].data;
		sampleAddTogridTable = gridData[0];
		CommonService.createGridTable($scope, gridOptions, gridData, uiGridConstants);
		setTimeout(function(){
			$( "#media" ).change(function(d) {
				  window.mediaFilter(this,$(d.target.selectedOptions[0]).attr('id').split('_')[1]);
				});
			$('.ui-grid-pager-container').find('input').attr('disabled',true);
			$('.ui-grid-top-panel input[type=text]').attr('placeholder','Search');
			
		},0);
		
		
		
	}

	
	/*@Purpose: Manipulation of data before creating UI
	* return : null
	* @author: Varsha
	* @Date: 5th Jan 2019
	*/
	
	function manipulateDataForUIgrid(data){
		 data.data.map(function(dat){
			 data.headers.map(function(d){
					  if(dat.hasOwnProperty(d.columnFieldName+'_value') && d.slider){
						  if($rootScope.sliderObjectKeys.indexOf(d.columnFieldName.split(' ').join('')) ==-1){
							  $rootScope.sliderObjectKeys.push(d.columnFieldName.split(' ').join(''));
						  }
						// checking if all data attributes contains same value
						  if(dat[d.columnFieldName+'_value'].dataAttributes && dat[d.columnFieldName+'_value'].dataAttributes.length > 0){
				    			var allEqual =  dat[d.columnFieldName+'_value'].dataAttributes.every(  v => v.credibilityValue === dat[d.columnFieldName+'_value'].dataAttributes[0].credibilityValue);
				    			if(allEqual){
									  if(dat[d.columnFieldName+'_value'].dataAttributes.length > 0) {
										  dat[d.columnFieldName.split(' ').join('')]= getDecryptValueForRangeSlider(dat[d.columnFieldName+'_value'].dataAttributes[0].credibilityValue);
									  }
				    			}else{
									  dat[d.columnFieldName.split(' ').join('')]= getDecryptValueForRangeSlider('NONE');
				    			}
						  }else{
							  dat[d.columnFieldName.split(' ').join('')] = getDecryptValueForRangeSlider(dat[d.columnFieldName+'_value'].subClassificationCredibility);
						  }
					  }
					  else if(d.slider){
						 dat[d.columnFieldName.split(' ').join('')]= 0;
					  }else if(d.visible){
						  dat[d.columnFieldName] = dat.Classification[0].hideStatusDto.visible;
					  }
					  else if(d.media){
						  var mediaOption = [];
						  $scope.mediaListsforsingleselect.map(function(d,k){
							 mediaOption.push({
								 'name':d,
								 'value':k
							 });
						  });
						  dat.mediaListsforsingleselect= mediaOption;
					  }
				  	
				  });
		  });
		
	}

	/*@Purpose: update source
	* @author: karunakar
	* @Date: 2nd-Jan-2019
	*/
	$scope.updateSource = function (updatedData,calledFrom,colname) {
		var param = updatedData;
		if($scope.btnSubmit){
			$scope.fullpageloader = true;
		}
		SourceManagementAppAppApiService.updateSourceAPI(param).then(function (response) {
			if($scope.currRowIndex != undefined){
				updateRowData(updatedData,calledFrom,colname);
			}
			$(".custom-data-table-wrapper").click();
			if(response.data && visibleStatus){
				visibleStatus = false;
				$rootScope.updateVisibityAccordingToStatus(response.data);
				changeDataForVisibilty(true);
			}
			$scope.fullpageloader = false;
			if($scope.btnSubmit){
				HostPathService.FlashSuccessMessage("SUCCESS", 'Source updated successfully');
			}
		}, function (err) {	
			$(".custom-data-table-wrapper").click();
			$scope.fullpageloader = false;
			HostPathService.FlashErrorMessage("ERROR",err.responseMessage);
		});
	};

	function updateRowData(updatedData,calledFrom,colname){
		var curr_mainindex = $scope.mainClassificationData.findIndex(function(d){return d.key == $scope.currentTab ;});
		var curr_mainData = $scope.mainClassificationData[curr_mainindex];
		
		var findrowIndex = $scope.gridApi.grid.rows.findIndex(function(d){ return d.uid == $scope.currRowIndex.uid; });

		
		var rowdata = $scope.gridApi.grid.rows[findrowIndex].entity;
		var source_index = curr_mainData.values.findIndex(function(d){return d.sourceId == rowdata.sourceId ;});
		var main_sourceData = curr_mainData.values[source_index];
        rowdata.Source = updatedData.sourceDisplayName;
        rowdata.Link = updatedData.sourceName;
        main_sourceData.sourceDisplayName = rowdata.Source;
        main_sourceData.sourceName = updatedData.sourceName;
        main_sourceData.sourceUrl = updatedData.sourceUrl;
		rowdata.sourceLink = updatedData.sourceUrl;
		rowdata.category = updatedData.category;
        if (updatedData.sourceDomain && updatedData.sourceDomain.length > 0) {
            rowdata.domain = updatedData.sourceDomain.map(function (d) { return d.domainName; }).join(", ");
            main_sourceData.sourceDomain = updatedData.sourceDomain;
        }
        if (updatedData.sourceIndustry && updatedData.sourceIndustry.length > 0) {
            rowdata.industry = updatedData.sourceIndustry.map(function (d) { return d.industryName ; }).join(", ");
            main_sourceData.sourceIndustry = updatedData.sourceIndustry;
        }
        if (updatedData.sourceJurisdiction && updatedData.sourceJurisdiction.length > 0) {
            rowdata.jurisdiction = updatedData.sourceJurisdiction.map(function (d) { return d.jurisdictionOriginalName ;}).join(", ");
            main_sourceData.sourceJurisdiction = updatedData.sourceJurisdiction;
        }
        if (updatedData.sourceMedia && updatedData.sourceMedia.length > 0 ||calledFrom =="UpdateFromMedia") {
            rowdata.sourceMedia = updatedData.sourceMedia;
            main_sourceData.sourceMedia = rowdata.sourceMedia;
        }
        if(updatedData.classifications && updatedData.classifications.length > 0){
	        var find_index = updatedData.classifications.findIndex(function(d){return d.classifcationName == $scope.currentTab;});
	        var classfication_data = updatedData.classifications[find_index];
	       //updating data Attributes values
	        if(classfication_data.subClassifications && classfication_data.subClassifications.length > 0){
		        angular.forEach(classfication_data.subClassifications,function(val){
		        	var mainClassIndex = main_sourceData.classifications[find_index].subClassifications.findIndex(function(d){return d.subClassificationId === val.subClassificationId;});
		        	main_sourceData.classifications[find_index].subClassifications[mainClassIndex].subClassificationCredibility = val.subClassificationCredibility;
		        	
		        	
		        	if(val.dataAttributes && val.dataAttributes.length > 0){
		        		var subClassificationdata = rowdata[val.subClassifcationName.split(" ").join("")+"_value"];   
		        		angular.forEach(subClassificationdata.dataAttributes,function(d){
		        			angular.forEach(val.dataAttributes,function(val1){
		            			if(val1.attributeId === d.attributeId){
		            				d.credibilityValue = val1.credibilityValue;
		            				rowdata[colname+'_isDisabled'] = true;
		            			}
		        			});       			
		
		        		});
		        		if(subClassificationdata.dataAttributes && subClassificationdata.dataAttributes.length > 0){
		        		   // checking if all data attributes contains same value
			    			var allEqual =  subClassificationdata.dataAttributes.every(  v => v.credibilityValue === subClassificationdata.dataAttributes[0].credibilityValue);
			    			if(allEqual){
			    				rowdata[colname] = $scope.mapMultiSlidert(subClassificationdata.dataAttributes[0].credibilityValue.toLowerCase());
			    				rowdata[colname+'_isDisabled'] = false;
			    			}else{
			    				rowdata[colname] = $scope.mapMultiSlidert('none');
			    				rowdata[colname+'_isDisabled'] = true;
			    			}
		        		}
		        	}
		        });
	        }
        }
	}
	
	
	
	
	/*@Purpose: Show popup for add new source
	* @author: karunakar
	* @Date: 28th-Dec-2018
	*/
	$scope.addSource = function (data,classificationData) {

	$scope.fullpageloader = true;
	$scope.btnSubmit = true;
		sampleAddTogridTable.Classification = data.classifications;
		//var changedDta = setDataToAddSource(data)
		var addData = [];
		addData.push(data);
		//$rootScope.addToGridTable(addData);
		SourceManagementAppAppApiService.addNewSourceAPI(data).then(function (response) {
			//$scope.getSources(classification,$scope.paginationOptions.pageSize,$scope.paginationOptions.pageNum,true);
			$scope.filtersValue = {};
			var index = $scope.mainClassificationData.map(function(d){
				return d.title.toLowerCase();
			}).indexOf(classificationData.classifcationName.toLowerCase());
			$scope.mainClassificationData[index].values = response.data;
			originalSourceData[$scope.tabIndexValue].result = response.data;
			if(classificationData.classifcationName.toLowerCase() == $scope.currentTab.toLowerCase()){
				$scope.totalSourceCount = $scope.mainClassificationData[index].values.length;
				creatGridData($scope.mainClassificationData[index]);
			}
			$scope.fullpageloader = false;
			$scope['filterSourceValue_'+$scope.currentTab] = '';
			setTimeout(function(){
			   $(".ui-grid-filter-input").val("");
			},100);
			
			HostPathService.FlashSuccessMessage("SUCCESS", 'Source added successfully');
		}, function (err) {
			$scope.fullpageloader = false;
			HostPathService.FlashErrorMessage("ERROR",err.responseMessage);
		});
	};
	
	/*@Purpose: Show popup for add new source
	* @author: karunakar
	* @Date: 28th-Dec-2018
	*/
	$scope.openModalForAddSource = function () {
		$scope.openAddSourceModal();
	};


	/**
	* Function to load screening testing modal
	*/
	$scope.openAddSourceModal = function () {
		var addNewSourceModalInstance = $uibModal.open({
			templateUrl: './modal/views/addSourceModalSourceManagement.html',
			controller: 'AddNewSourceModalController',
			scope: $scope,
			backdrop:'static',
			windowClass: 'custom-modal modal-md bst_modal'
		});

		addNewSourceModalInstance.result.then(function () {
		}, function () {
		});
	};
	
	var toAdd = localStorage.getItem('toAdd');
	if(toAdd == 'yes'){
		$scope.openAddSourceModal();
		localStorage.setItem('toAdd', 'no');
	}

	/*@Purpose: Show popup for edit source
	* @author: Amarjith
	* @Date: 02 jan-2018
	*/
	$scope.openModalForEditSource = function (col_val,rowindex,col) {
		$scope.currentRowVal = col_val;
		$scope.currentRowVal.classifications = $scope.currentTab;
		$scope.currRowIndex = col;
		$scope.openEditSourceModal();
	};


	/**
	* Function to load screening testing modal
	*/
	$scope.openEditSourceModal = function () {
		var editNewSourceModalInstance = $uibModal.open({
			templateUrl: './modal/views/editSourceModalSourceManagement.html',
			controller: 'EditNewSourceModalController',
			scope: $scope,
			backdrop:'static',
			windowClass: 'custom-modal modal-md bst_modal'
		});

		editNewSourceModalInstance.result.then(function () {
		}, function () {
		});
	};
	
	/*@Purpose: to change the tab
	* @author: varsha
	* @Date: 04 jan-2018
	*/
	$scope.tabOnchange = function(index,title){
		$scope.filtersValue = {};
		$scope.currentTab = title;
		$scope.tabIndexValue = index;
		$scope.fullpageloader = true;
		if($scope.mainClassificationData[index].values.length > 0){
			$scope.currentTabIndex = originalSourceData.map(function(d){
				return d.result[0].classifications[0].classifcationName.toLowerCase();
			}).indexOf(title.toLowerCase());
			$scope.totalSourceCount = $scope.mainClassificationData[index].values.length;
			creatGridData($scope.mainClassificationData[index]);
			 $scope.fullpageloader = false;
		}else{
			$scope.getSources($scope.mainClassificationData[index],$scope.currentPageSize,$scope.pageNo,true);
		}
	};
	
	window.currentMediaData={};
	/*@Purpose: To update the current data attributes of the source when the user clicks on save button
	 * @author: Amarjith Kumar
	 * @ return : null
	 * @Date: 7th Jan 2019
	 */
	window.updateSourceDataAttr = function(){
		var vm =$scope;		
		var rowdata = jQuery.extend(true, [], vm.currentDataAttr.currentrowdata);
		var colname = vm.currentDataAttr.fieldname;
		var sourcename = vm.currentDataAttr.sourcename.split(" ").join("");
		var rowentity = jQuery.extend(true, {}, vm.currentDataAttr.rowenttity);
		var subClassificationId = vm.currentDataAttr.subClassificationId;
		var data = [];
		angular.forEach(rowdata,function(val){
			var currentCrediablityvalue = parseInt(document.getElementById(colname+"_"+val.sourceAttributeName.split(" ").join("")+sourcename+"_value").value);
			if (vm.getDecryptValueForRangeSlider(currentCrediablityvalue) != val.credibilityValue ){
				val.credibilityValue = vm.getDecryptValueForRangeSlider(currentCrediablityvalue);
				data.push(val);
			}
		});
		  var currIndex = vm.mainClassificationData.findIndex(function (d) { return d.key === vm.currentTab; });
	       
		  var filtermaindata = jQuery.extend(true,{}, vm.mainClassificationData[currIndex]);
	      var filtermaindata_resultIndex = filtermaindata.values.findIndex(function (d) { return d.sourceId === rowentity.sourceId; });
	      var filtermaindata_results = jQuery.extend(true,{},filtermaindata.values[filtermaindata_resultIndex]);
	      delete filtermaindata_results.currentClassification;
          filtermaindata_results.sourceIndustry = [];
    
          filtermaindata_results.sourceJurisdiction = [];
          filtermaindata_results.sourceDomain = [];
          filtermaindata_results.sourceMedia = [];
          var classificationsdata = filtermaindata_results.classifications[filtermaindata_results.classifications.findIndex(function(d){return d.classifcationName == vm.currentTab;})];
	        angular.forEach(classificationsdata.subClassifications, function (val) {
	        	if(val.subClassificationId == subClassificationId){
	        		val.dataAttributes = data;
	        	}else{
	        		val.dataAttributes = [];
	        	}
	        });

          var updatedData = jQuery.extend( true,{}, filtermaindata_results);
		  window[colname+rowentity.sourceId+'_isOpen'] = false;  
		  vm.btnSubmit = true;            
	      vm.updateSource(updatedData,'',colname);

	};
	/*
	 * @purpose: Generate UI grid Table
	 * @created: 4th JAN 2019
	 * @params:  mediadata :DATA Array
	 * @return: checkMediaLists:DATA Array
	 * @author: Amarjith
	 * 
	*/
	function getcheckedMediaData(mediadata){
		var checkMediaLists = [];
		if(mediadata.findIndex(function(d){return d.mediaName == 'All';}) != -1){
			$scope.mediaLists.map(function(value){
					checkMediaLists.push({
						mediaId: value.mediaId,
						mediaName: value.mediaName,
						checked: true
					});
			});
			return checkMediaLists;
		}
		$scope.mediaLists.map(function(value){
			if(value.mediaName == 'All'){
				checkMediaLists.push({
					mediaId: value.mediaId,
					mediaName: value.mediaName,
					checked: false
				});
			}else{
				checkMediaLists.push({
					mediaId: value.mediaId,
					mediaName: value.mediaName,
					checked:(mediadata.findIndex(function(d){return d.mediaName == value.mediaName;}) != -1 )? true : false
				});
			}
		});
		
		return checkMediaLists;
	}
	/*
	 * @purpose: Generate UI grid Table
	 * @created: 4th JAN 2019
	 * @params:  row:Object, col: Object, rowindex:number
	 * @return: no
	 * @author: Amarjith
	 * 
	*/
	$scope.getTemplate= function(row, columnName) {
		var currentdataAttrdata = jQuery.extend(true, [], row.entity[columnName.field+'_value'].dataAttributes);
		var sourcename = row.entity.Source;
		$scope.currRowIndex = row;
		$scope.currentDataAttr = {
				currentrowdata  : jQuery.extend(true, [], currentdataAttrdata),
				fieldname : columnName.field,
				sourcename : sourcename,
				rowenttity :  row.entity,
				subClassificationId : row.entity[columnName.field+'_value'].subClassificationId
		};
		var template = '<div class="custom-panel-wrapper border-0 mar-b0 bg-transparent panel"><div class="panel-header roboto-bold f-18 text-cream d-flex ai-c"><h4 class="panel-title d-flex ai-c" ><span class="mxw-150 text-capitalize pad-r10 text-overflow">'+sourcename+' </span><span>|</span><span class="roboto-light mar-l5 text-cream">'+columnName.displayName+'</span></h4><span class=" text-cream f-12 c-pointer roboto-regular mar-autol" onClick="window.reset(\''+ columnName.field+'\',\''+sourcename.split(" ").join("") + '\')"> Reset as Default</span></div> <div class="panel-body d-flex ai-c pad-0">';
		currentdataAttrdata.forEach(function (d) {
		d.credibilityNumberValue =  getDecryptValueForRangeSlider(d.credibilityValue)	; // 
		 template = template +'<div class="range-bar-wrapper d-flex ai-c"><p class="roboto-regular text-cream f-14 mar-b0 width-50 text-overflow mar-r10">'+d.sourceAttributeName+' :'+'</p><div class = " custom-range-wrapper entity-range-bar d-flex ai-c"><p class="roboto-regular text-cream f-12 mar-b0 text-overflow mar-r10" id='+columnName.field+'_'+d.sourceAttributeName.split(" ").join("")+sourcename.split(" ").join("")+'_text'+'>'+d.credibilityValue+'</p><input type="range" oninput="updateDataAttr(\''+ columnName.field+'\',\''+d.sourceAttributeName.split(" ").join("")+'\',\''+sourcename.split(" ").join("") + '\')" value='+d.credibilityNumberValue+' name="range" min="0" max="3"  class="" id="'+columnName.field+'_'+d.sourceAttributeName.split(" ").join("")+sourcename.split(" ").join("")+'_value"'+' /></div></div>';
		});
		template = template + '</div><div class="buttons-wrapper d-flex ai-c"><a onClick="cancelSourceDataAttr()" class="btn bg-transparent text-uppercase text-dodger-blue border-0">Cancel</a><a class="btn bg-transparent text-uppercase text-dodger-blue border-0 pad-r0" onClick="updateSourceDataAttr()">Save</a></div></div></div>';
		
		return slidertrusted[template] || (slidertrusted[template] = $sce.trustAsHtml(template)); 
	};
	var trusted = {};		
	$scope.getMediaTemplate=function(row) {
		var mediaData = getcheckedMediaData(row.entity.sourceMedia);
		var sourcename = row.entity.Source;
        $scope.currRowIndex = row;
		window.currentMediaData = {
				"mediaList":mediaData,
				"data":row.entity
		};
		var template = '<div class="custom-panel-wrapper border-0 mar-b0 bg-transparent panel"><div class="panel-header roboto-bold f-18 text-cream d-flex ai-c"><h4 class="panel-title d-flex ai-c" ><span class="mxw-150 text-capitalize company_name p-rel pad-r10 text-overflow">'+sourcename+' </span><span class="roboto-light mar-l5 text-cream"> Media</span></h4><span class=" text-cream f-12 roboto-regular c-pointer mar-autol" onClick="window.resetMedia(\''+ row.entity.sourceId+'\',\''+row.entity.Classification[0].classificationId + '\')"> Reset as Default </span></div> <div class="panel-body d-flex ai-c media pad-0">';
		mediaData.forEach(function (d) {
			template = template + '<div class="switch-wrapper d-flex ai-c"><p class="roboto-regular text-cream f-14 mar-b0 width-85 text-overflow mar-r10"><i class="fa fa-'+$scope.mapMediaHash[d.mediaName]+' f-12 mar-r5"></i>' + d.mediaName + ' :' + '</p><div class="inner-switch-wrapper"><label class = " switch pull-right custom-switch-wrapper bst-switch-wrapper d-flex ai-c"><input onClick="window.updateMedia(\'' + d.mediaName + '\')" type="checkbox" '+ (d.checked ? "checked" : "") +' value='+d.checked+' class="custom-switch-checkbox Check_'+d.mediaName+'_source'+row.entity.sourceId+'_class'+row.entity.Classification[0].classificationId+'"><span class="slider round" ><span></label></div></div>';
		});
		template = template + '</div><div class="buttons-wrapper d-flex ai-c"><a class="btn bg-transparent text-uppercase text-dodger-blue border-0" onClick="cancelSourceDataAttr()">Cancel</a><a class="btn bg-transparent text-uppercase text-dodger-blue border-0 pad-r0"  onClick="saveMedia()">Save</a></div></div></div></div>';
		return trusted[template] || (trusted[template] = $sce.trustAsHtml(template)); 
	};
	/*@Purpose: To reset value of Slider Attribute popover
	 * @Params :row:Object, col: Object
	 * @author: karunakar
	 * @ return : null
	 * @Date: 21th Jan 2019
	 */
	window.reset = function(col,row){
		$scope.currentDataAttr.currentrowdata.forEach(function (d) {
			d.sourceAttributeName=d.sourceAttributeName.replace(/ +/g, "");
			d.credibilityNumberValue = getDecryptValueForRangeSlider(d.credibilityValue);
			document.getElementById(col+"_"+d.sourceAttributeName+row+"_value").value =d.credibilityNumberValue;
			document.getElementById(col+"_"+d.sourceAttributeName+row+"_text").innerHTML = d.credibilityValue;
		});
	};
	/*@Purpose: To reset value of media popover
	 * @Params :sourceId: number,classificationId:number
	 * @author: karunakar
	 * @ return : null
	 * @Date: 20th Jan 2019
	 */
	window.resetMedia = function(sourceId,classificationId){
		var fetchMediaData = getcheckedMediaData(window.currentMediaData.data.sourceMedia);
		fetchMediaData.forEach(function (d) {
			document.getElementsByClassName('Check_'+d.mediaName+'_source'+sourceId+'_class'+classificationId)[0].checked = d.checked;
		});
	};
	/*@Purpose: To update the sliders value on slide
	 * @Params : col : current column name, d: current slider label name, sourcename : current source column data
	 * @author: Amarjith Kumar
	 * @ return : null
	 * @Date: 7th Jan 2019
	 */
	window.updateDataAttr = function(col,d,sourcename){
		var vm =$scope;	
		var value = parseInt(document.getElementById(col+"_"+d+sourcename+"_value").value);
		//var text = document.getElementById(col+"_"+d+sourcename+"_text").innerHTML;
		document.getElementById(col+"_"+d+sourcename+"_text").innerHTML = vm.getDecryptValueForRangeSlider(value);
	};
	/*@Purpose: To cancel the current data attributes of the source when click on cancel button
	 * @author: Amarjith Kumar
	 * @ return : null
	 * @Date: 7th Jan 2019
	 */
	window.cancelSourceDataAttr = function(){
		$(".custom-data-table-wrapper").click();
	};

	/*@Purpose: To update the media popover value
	 * @Params : currentVal: string
	 * @author: Karunakar
	 * @ return : null
	 * @Date: 17th Jan 2019
	 */
	window.updateMedia = function(currentVal) {
	    var className = '',
	        status = true,
	        updatedMediaArray = [],
	        checkedClass,
	        allTrue,
            markTrue,
	        disable;
	    setTimeout(function() {
	        var checkUncheckProperty = $('.Check_' + currentVal + '_source' + window.currentMediaData.data.sourceId + '_class' + window.currentMediaData.data.Classification[0].classificationId).prop('checked');
	        $('.panel-body.media.ai-c.pad-0').find('input[type=checkbox]').each(function() {
	            var sList = this.checked ? "checked" : "notchecked";
	            if (sList == "checked") {
	                checkedClass = $(this).attr("class").split(' ')[1].split('_')[1];
	                window.currentMediaData.mediaList.forEach(function(d) {
	                    if (d.mediaName == checkedClass && checkedClass != 'All') {
	                        delete d.checked;
	                        updatedMediaArray.push(d);
	                    }
	                });
	            }
	            if($('.'+$(this).attr("class").split(' ')[1]).prop('checked')){
                	$('.panel-body.media.ai-c.pad-0').find('input[type=checkbox]').each(function() {
                		if ($(this).attr("class").split(' ')[1].split('_')[1] == 'All') {
        	                markTrue = $(this).attr("class").split(' ')[1];
        	            }
                		if ($(this).attr("class").split(' ')[1].split('_')[1] != 'All') {
                			 if($('.'+$(this).attr("class").split(' ')[1]).prop('checked')){
                				 allTrue= true;
                			 } else {
                				 allTrue= false;
                				 return false;
                			 }
                		}
                	});
                }
	            if (sList == "notchecked" && currentVal != 'All') {
	                status = false;

	            } else if ($(this).attr("class").split(' ')[1].split('_')[1] == 'All') {
	                className = $(this).attr("class").split(' ')[1];
	              
	            } else if (currentVal == 'All') {
	                if (!checkUncheckProperty) {
	                    status = false;
	                    disable = $(this).attr("class").split(' ')[1];
	                    updatedMediaArray=[];
	                    $('.' + disable).prop('checked', false);
	                } else if (checkUncheckProperty) {
	                    status = true;
	                    disable = $(this).attr("class").split(' ')[1];
	                    $('.' + disable).prop('checked', true);
	                    window.currentMediaData.mediaList.forEach(function(d) {
		                    if (d.mediaName == 'All') {
		                        delete d.checked;
		                        updatedMediaArray=[d];
		                    }
		                });
	                }
	            }
	        });
	        if (!status) {
	            if (className != '') {
	                $('.' + className).prop('checked', false);
	            }
	        } else if (status) {
	            $('.' + className).prop('checked', true);
	        }
	        if(allTrue){
	        	$('.' + markTrue).prop('checked', true);
	        	window.currentMediaData.mediaList.forEach(function(d) {
	                    if (d.mediaName == 'All') {
	                        delete d.checked;
	                        updatedMediaArray=[d];
	                    }
	                });
	        }
	        var tabIndex = originalSourceData.map(function(d) {
	            return d.result[0].classifications["0"].classifcationName;
	        }).indexOf(window.currentMediaData.data.Classification["0"].classifcationName);
	        var index = originalSourceData[tabIndex].result.map(function(d) {
	            return d.sourceId;
	        }).indexOf(window.currentMediaData.data.sourceId);
	        if (index != -1) {
	           currentUpdatedObj = jQuery.extend(true, {}, originalSourceData[tabIndex].result[index]);
	            currentUpdatedObj.classifications["0"].subClassifications.map(function(val) {
	            	val.dataAttributes = [];
	            	return val.dataAttributes;
	            });
	            currentUpdatedObj.sourceIndustry = [];
	            currentUpdatedObj.sourceJurisdiction = [];
	            currentUpdatedObj.sourceDomain = [];
	            currentUpdatedObj.sourceMedia = updatedMediaArray;
	            delete currentUpdatedObj.currentClassification;
	        }
	        			
	    }, 0);
	};
	/*@Purpose: To savethe media popover value
	 * @Params :no
	 * @author: Amarjit
	 * @ return : null
	 * @Date: 7th Jan 2019
	 */
	window.saveMedia = function() {
		$scope.btnSubmit = true;
		if(Object.keys(currentUpdatedObj).length >0){
		$scope.updateSource(currentUpdatedObj,"UpdateFromMedia");
		currentUpdatedObj = {};
		}
		$(".custom-data-table-wrapper").click();
	};
	//---------------------------------------------------------------------------------------------------	    

	
}