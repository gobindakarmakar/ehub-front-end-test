<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--  SubMenu Starts  -->
<%@include file="../../WEB-INF/jsp/common/submenu.jsp"%>
<!--  SubMenu Ends  -->
<style>
	.iconClass{
    font-size: 24px;
    transform: translate(-50%,-50%);
     margin-left: 50%;
    margin-top: 10%;
}

.centerAlign .c-pointer{
justify-content: center;
}
.centerAlign{
    display: flex;
    justify-content: center;
}
.pointerNone{
pointer-events: none;
}
.text-capitalize{
text-transform: capitalize !important;
}
</style>
<div id= "source_management_mainDiv" class="source_management dashboard-wrapper sm-dashboard-wrapper">
	<div class="bst-uib-tab-wrapper pad-0 source-tabs-wrapper">
		<uib-tabset active="active">
			<uib-tab index="$index" heading={{tabs.title}} ng-repeat="(k,tabs) in mainClassificationData track by $index" ng-click="tabOnchange($index,tabs.title)">
				<div class="content-wrapper">
					<div class="left-col">
						<div class="panel-body pad-0">
							<div class=" custom-data-table-wrapper ">
								<div class="top-table-content-wrapper pad-b5  p-rel d-flex ai-c">
									
									<p class="text-cream d-ib mar-b0 mar-x15 roboto-regular f-14"> Prioritize information from a source for each category
										({{totalSourceCount}} sources).</p>
								
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</uib-tab>
		</uib-tabset>
		<div ng-if="gridOptions" ui-grid="gridOptions" ui-grid-pagination ui-grid-exporter ui-grid-resize-columns ui-grid-move-columns ui-grid-save-state class="myGrid "> 
			<div class=" leadGenErrorDiv text-center no-data-wrapper" ng-show="!gridOptions.data.length"> <span class="no-data-wrapper">Data Not Found</span></div>
		</div>

	</div>
	
	
	 <div class="custom-spinner full-page-spinner" ng-show="fullpageloader">
                <i class="fa fa-spinner fa-spin fa-3x"></i>
     </div>
</div>

<script>
	// (function ($) {
	// 	$(window).on("load", function () {
	// 		setTimeout(function () {

	// 			$(".ui-grid-render-container-body").mCustomScrollbar({
	// 				axis: "yx",
	// 				theme: "dark-3",
	// 				autoHideScrollbar: "true",
	// 				scrollbarPosition: "outside",
	// 				advanced: {
	// 					autoExpandHorizontalScroll: true //optional (remove or set to false for non-dynamic/static elements)
	// 				}
	// 			});
	// 		}, 3000);
	// 	});
	// })(jQuery);
</script>