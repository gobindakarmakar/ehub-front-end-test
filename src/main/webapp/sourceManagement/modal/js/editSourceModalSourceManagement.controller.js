'use strict';
angular.module('sourceManagementApp')
    .controller('EditNewSourceModalController', editNewSourceModalController);

editNewSourceModalController.$inject = [
    '$scope',
    '$uibModalInstance',
    'SourceManagementAppAppApiService'
];

function editNewSourceModalController(
    $scope,
    $uibModalInstance,
    SourceManagementAppAppApiService
) {
    var currntrow_obj = $scope.$parent.currentRowVal;
    var filter_all = $scope.$parent.JurisdictionList.filter(function(d){return (d.jurisdictionName.toLowerCase() == 'all')});
		var filter_without_all = $scope.$parent.JurisdictionList.filter(function(d){return (d.jurisdictionName.toLowerCase() !== 'all')});
		filter_without_all.sort((a,b) => (a.jurisdictionName > b.jurisdictionName) ? 1 : ((b.jurisdictionName > a.jurisdictionName) ? -1 : 0)); 
		$scope.$parent.JurisdictionList = filter_all.concat(filter_without_all);
    // to store previous values.
    $scope.prevObjData = {
        sourceDisplayName : '',
        sourceName : '',
        sourceUrl : '',
        category : '',
        sourceDomain:[],
        sourceIndustry:[],
        sourceJurisdiction:[],
        sourceMedia:[]
    }

    /*@Purpose: formatting the checked data as per multiselect data format from the ui grid table
	* @param: val<Strings(comma separated)>, valuelist<String>
	* return : Array of Objects ([{}])
	* @author: Amarjith Kumar
	* @Date: 17th-Jan-2019
	*/
    $scope.formatSourceListsCheckedData = function (val,valuelist) {
        var arrdata = [];
        var data = val.split(",");
        if(val == "All" && valuelist == "JurisdictionList"){
            data = $scope.$parent[valuelist].filter(function(d){return d.id;});
            angular.forEach(data, function (val, key) {
                arrdata.push({
                    id: val.id.trim(),
                    label : val.label.trim(),
                    jurisdictionName : val.jurisdictionName.trim()
                });
            });
        } else if(val != "All" && valuelist == "JurisdictionList"){
           angular.forEach(data,function(da){
            angular.forEach($scope.$parent[valuelist],function(d){
                if(da.trim() === d.jurisdictionOriginalName.trim()){
                    arrdata.push({
                        id: d.id.trim(),
                        label : d.label.trim(),
                        jurisdictionName : d.jurisdictionName.trim()
                    });
                }
                });  
            });
        }else if(val == "All" && valuelist != "JurisdictionList"){
            data = $scope.$parent[valuelist].map(function(d){return d.id.trim();});
            angular.forEach(data, function (val, key) {
                arrdata.push({
                    id: val.trim(),
                    label : val.trim()
                });
            });
        }else{
            angular.forEach(data, function (val, key) {
                arrdata.push({
                    id: val.trim(),
                    label : val.trim()
                });
            });
        }
       
        return arrdata;
    }

    $scope.editSourceData = {
        sourceDisplayName: currntrow_obj.Source,
        sourcename: currntrow_obj.Link,
        sourceId: currntrow_obj.sourceId,
        sourcelink: currntrow_obj.sourceLinkName,
        Classification: currntrow_obj.Classification ? currntrow_obj.Classification : '',
        domain: currntrow_obj.domain ? $scope.formatSourceListsCheckedData(currntrow_obj.domain, "DomainList") : [],
        industry: currntrow_obj.industry ? $scope.formatSourceListsCheckedData(currntrow_obj.industry,"IndustryList") : [],
        jurisdiction: currntrow_obj.jurisdiction ? $scope.formatSourceListsCheckedData(currntrow_obj.jurisdiction,"JurisdictionList") : [],
        media: currntrow_obj.sourceMedia ? $scope.formatSourceListsCheckedData(currntrow_obj.sourceMedia.map(function(d){return d.mediaName}).toString(),"mediaLists") : [],
        editRole:currntrow_obj.category
    };
    $scope.editSourceData.media = $scope.editSourceData.media.filter(function(d){return (d.id !== '')});
    $scope.prevObjData.sourceDisplayName = $scope.editSourceData.sourceDisplayName;
    $scope.prevObjData.sourceName  = $scope.editSourceData.sourcename;
    $scope.prevObjData.category  = $scope.editSourceData.editRole;
    $scope.prevObjData.sourceUrl = $scope.editSourceData.sourcelink;
    $scope.prevObjData.sourceDomain = jQuery.extend(true,[],$scope.editSourceData.domain);
    $scope.prevObjData.sourceIndustry = jQuery.extend(true,[],$scope.editSourceData.industry);
    $scope.prevObjData.sourceJurisdiction = jQuery.extend(true,[],$scope.editSourceData.jurisdiction);
    $scope.prevObjData.sourceMedia = jQuery.extend(true,[],$scope.editSourceData.media);

    $scope.source = {
        name: {
            required: true,
            minlength: 5,
            maxlength: 25
        }
    };

    $scope.closeeditSourceModal = function () {
        $uibModalInstance.close('close');
    };

    $scope.removeParticularItem = function (item,selectedList,List,removeFrom) {
       $scope.editSourceData[removeFrom]= selectDeSelectItems(item,selectedList,$scope[List],'onItemDeselect');
    };
    /*@Purpose: formattting the data for Juridiction,Industry, domain and media as per API structure
	* @param: data<Array>,originalDataLists<Array>,keyid<String>,keyname<String>
	* return : Array of Objects ([{}])
	* @author: Amarjith Kumar
	* @Date: 17th-Jan-2019
	*/
    $scope.formatSourceArray = function(data,originalDataLists,keyid,keyname){
        var arraySourceData = [], curr_data = [];
        if (data.length == 1) {
            curr_data = data.filter(function (val, key) {
                return val.id != originalDataLists[key].id;
            });
        } else if (data.length > 1) {
            curr_data = data;
        } 
        if(curr_data.length > 0){
            angular.forEach(curr_data, function (val, key) {
                var filterLists = originalDataLists.filter(function (d, i) { return d.id === val.id });
                angular.forEach(filterLists, function (val1, key1) {
                    if(keyname === 'jurisdictionOriginalName'){
                        arraySourceData.push({
                            [keyid]: val1[keyid],
                            [keyname]: val1[keyname],
                            'jurisdictionName': val1['jurisdictionName']
                         });
                    }else{
                      arraySourceData.push({
                         [keyid]: val1[keyid],
                         [keyname]: val1[keyname]
                      });
                    }
                });
            });
        }
        return arraySourceData;
    }

    /*@Purpose: formattting the data as per updatesource api structure
	* @param: null
	* return : null
	* @author: Amarjith Kumar
	* @Date: 05th-Jan-2019
	*/
    $scope.updatetoformatdata = function () {
    	
        var mediaLists = $scope.$parent.mediaLists;
        var IndustryList = $scope.$parent.IndustryList;
        var DomainList = $scope.$parent.DomainList;
        var JurisdictionList = $scope.$parent.JurisdictionList;
        var currIndex = $scope.$parent.mainClassificationData.findIndex(function (d) { return d.key === currntrow_obj.classifications })
        var filtermaindata = jQuery.extend(true, {}, $scope.$parent.mainClassificationData[currIndex]);  
        var filtermaindata_resultIndex = filtermaindata.values.findIndex(function (d) { return d.sourceId === currntrow_obj.sourceId });
        var filtermaindata_results = jQuery.extend(true, {}, filtermaindata.values[filtermaindata_resultIndex]); 
        
        delete filtermaindata_results['currentClassification'];
        filtermaindata_results.sourceDisplayName = $scope.editSourceData.sourceDisplayName;
        filtermaindata_results.sourceName = $scope.editSourceData.sourcename;
        filtermaindata_results.sourceUrl = $scope.editSourceData.sourcelink;
        filtermaindata_results.category = $scope.editSourceData.editRole;      
        filtermaindata_results.sourceIndustry = $scope.formatSourceArray($scope.editSourceData.industry,IndustryList,'industryId','industryName');
        filtermaindata_results.sourceJurisdiction = $scope.formatSourceArray($scope.editSourceData.jurisdiction,JurisdictionList,'jurisdictionId','jurisdictionOriginalName');
        filtermaindata_results.sourceDomain = $scope.formatSourceArray($scope.editSourceData.domain,DomainList,'domainId','domainName');
        filtermaindata_results.sourceMedia = $scope.formatSourceArray($scope.editSourceData.media,mediaLists,'mediaId','mediaName');
        
        var result_sourceDisplayName = ($scope.editSourceData.sourceDisplayName == $scope.prevObjData.sourceDisplayName);
        var result_sourceName = ($scope.editSourceData.sourceName == $scope.prevObjData.sourceName);
        var result_sourceUrl = ($scope.editSourceData.sourcelink == $scope.prevObjData.sourceUrl);
        var result_category = ($scope.editSourceData.editRole == $scope.prevObjData.category);
         // comparing the previous source data with changed source data source dropdowns
        var result_sourceDomain = compareArrayObjects($scope.prevObjData.sourceDomain,$scope.editSourceData.domain);
        var result_sourceIndustry = compareArrayObjects($scope.prevObjData.sourceIndustry,$scope.editSourceData.industry);
        var result_sourceJurisdiction = compareArrayObjects($scope.prevObjData.sourceJurisdiction,$scope.editSourceData.jurisdiction);
        var result_sourceMedia = compareArrayObjects($scope.prevObjData.sourceMedia,$scope.editSourceData.media);

        // doing empty when nothing selected or deselected from  source dropdowns
        if(result_sourceDomain) {filtermaindata_results.sourceDomain = [];}
        if(result_sourceIndustry) {filtermaindata_results.sourceIndustry = [];}
        if(result_sourceJurisdiction) {filtermaindata_results.sourceJurisdiction = [];}
        if(result_sourceMedia) {filtermaindata_results.sourceMedia = [];}
        
        //if all is selected in jurisdiction , industry , domain or media only select ALL data from the lists
        var industryIndex = filtermaindata_results.sourceIndustry.findIndex(function(d){return d.industryName.toLowerCase() === 'all'}) ;
        var domainIndex = filtermaindata_results.sourceDomain.findIndex(function(d){return d.domainName.toLowerCase() === 'all'}) 
        var jurisdictionIndex = filtermaindata_results.sourceJurisdiction.findIndex(function(d){return d.jurisdictionName.toLowerCase() === 'all'}) 
        var mediaIndex = filtermaindata_results.sourceMedia.findIndex(function(d){return d.mediaName.toLowerCase() === 'all'}) 

        if(industryIndex >= 0){
        	filtermaindata_results.sourceIndustry = filtermaindata_results.sourceIndustry.filter(function(d,i){return i == industryIndex});
        }
        
		if(domainIndex >= 0){
			filtermaindata_results.sourceDomain = filtermaindata_results.sourceDomain.filter(function(d,i){return i == domainIndex});  	
		}
		        
		if(jurisdictionIndex >= 0){
			filtermaindata_results.sourceJurisdiction = filtermaindata_results.sourceJurisdiction.filter(function(d,i){return i == jurisdictionIndex});
		}
		
		if(mediaIndex >= 0){
			filtermaindata_results.sourceMedia = filtermaindata_results.sourceMedia.filter(function(d,i){return i == mediaIndex});
		}
     
        

        var findSubClass_index = filtermaindata_results.classifications.findIndex(function(d){ return(d.classifcationName == $scope.$parent.currentTab)});
        angular.forEach(filtermaindata_results.classifications[findSubClass_index].subClassifications, function (val, key) {
            val.dataAttributes = [];
        });

        var updatedData = Object.assign({}, filtermaindata_results);
        if(result_sourceDisplayName && result_sourceUrl && result_sourceDomain && result_sourceIndustry && result_sourceJurisdiction && result_sourceMedia && result_category){

        }else{
            $scope.$parent.btnSubmit = true;
            $scope.$parent.updateSource(updatedData);
        }
        $scope.closeeditSourceModal();
    };

    /*@Purpose: comparing the two array of source object data
	* @param: array1<Array>, array2<Array>
	* return : boolean
	* @author: Amarjith Kumar
	* @Date: 28th-Jan-2019
	*/

    function compareArrayObjects (array1, array2){
        var mapArr1 = array1.map(function(d){return d.label;});
        var mapArr2 = array2.map(function(d){return d.label;});
        if(_.isEqual(mapArr1, mapArr2)){
            return true;
        }else{
            return false;
        }
    }

    /*@Purpose: Select , de-select multiselect dropdown
	* @param: item<Object>, data<Array>, dataLists<Array>, selectionName<String>
	* return : null
	* @author: Amarjith Kumar
	* @Date: 17th-Jan-2019
	*/
    function selectDeSelectItems(item, data, dataLists, selectionName) {
        if (selectionName == 'onItemSelect') {
            if (item.id == 'All') {
                data = [];
                dataLists.map(function(d) {
                    data.push({
                        'id': d.id
                    })
                    setMultiSelectOptions(data, dataLists);
                })
            } else {
                var difference = diff_In_list(data, dataLists);
                if (difference.length == 1) {
                    if (difference[0].id == "All") {
                        data.push({
                            'id': difference[0].id
                        })
                        setMultiSelectOptions(data, dataLists);
                    }
                }
                setMultiSelectOptions(data, dataLists);
            }
        } else if (selectionName == 'onItemDeselect') {
            if (item.id == "All") {
                data = [];
                setMultiSelectOptions(data, dataLists);
            } else {
                data.map(function(e, i) {
                    if (e.id == item.id || e.id == "All") {
                        data.splice(i, 1);
                        setMultiSelectOptions(data, dataLists);
                    }
                });
            }
        } else if (selectionName == 'onSelectAll') {

            // below commented for selectALL on key enter

            // dataLists.map(function(d) {
            //     data.push({
            //         'id': d.id
            //     })
            //     setMultiSelectOptions(data, dataLists);
            // });
        }
        return data;
    }



    //getting industry values from multiselect box

    $scope.selectIndustryListForEdit = {
    	    onItemDeselect: function(item) {
                $scope.editSourceData.industry =  selectDeSelectItems(item,$scope.editSourceData.industry,$scope.IndustryList,'onItemDeselect');
    	    },
    	    onItemSelect: function(item,onEnterTabKey) {
                if(onEnterTabKey){
                    $scope.editSourceData.industry.push(item);
                }
    	        $scope.editSourceData.industry = selectDeSelectItems(item,$scope.editSourceData.industry,$scope.IndustryList,'onItemSelect');
    	    },
    	    onSelectAll: function (item) {
                $scope.editSourceData.industry = selectDeSelectItems(item,$scope.editSourceData.industry,$scope.IndustryList,'onSelectAll');    		    	
    		}
        };
   
        


    //getting jurisdictionList values from multiselect box
    $scope.selectJurisdictionListForEdit = {
    		onItemDeselect: function (item) {
                $scope.editSourceData.jurisdiction = selectDeSelectItems(item,$scope.editSourceData.jurisdiction,$scope.JurisdictionList,'onItemDeselect');
            },
            onItemSelect: function(item,onEnterTabKey) {
                if(onEnterTabKey){
                    $scope.editSourceData.jurisdiction.push(item);
                }
                $scope.editSourceData.jurisdiction = selectDeSelectItems(item,$scope.editSourceData.jurisdiction,$scope.JurisdictionList,'onItemSelect');
            },
            onSelectAll: function (item) {
                $scope.editSourceData.jurisdiction = selectDeSelectItems(item,$scope.editSourceData.jurisdiction,$scope.JurisdictionList,'onSelectAll');                  
            }       
    };

    //getting domain list values from multiselect box
    $scope.selectDomainListForEdit = {
    		onItemDeselect: function (item) {
                $scope.editSourceData.domain =  selectDeSelectItems(item,$scope.editSourceData.domain,$scope.DomainList,'onItemDeselect'); 
            },
            onItemSelect: function(item,onEnterTabKey) {
                if(onEnterTabKey){
                    $scope.editSourceData.domain.push(item);
                }
                $scope.editSourceData.domain = selectDeSelectItems(item,$scope.editSourceData.domain,$scope.DomainList,'onItemSelect'); 
            },
            onSelectAll: function (item) {
                $scope.editSourceData.domain = selectDeSelectItems(item,$scope.editSourceData.domain,$scope.DomainList,'onSelectAll');                     
            }
    };

    //getting media list values from multiselect box
    $scope.selectMediaListForEdit = {
    	    onItemDeselect: function(item) {
                $scope.editSourceData.media = selectDeSelectItems(item,$scope.editSourceData.media,$scope.mediaLists,'onItemDeselect'); 
    	    },
    	    onItemSelect: function(item,onEnterTabKey) {
                if(onEnterTabKey){
                    $scope.editSourceData.media.push(item);
                }
    	        $scope.editSourceData.media = selectDeSelectItems(item,$scope.editSourceData.media,$scope.mediaLists,'onItemSelect'); 
    	    },
            onSelectAll: function (item) {
                $scope.editSourceData.media = selectDeSelectItems(item,$scope.editSourceData.media,$scope.mediaLists,'onSelectAll');                 
            }
    	};

    //finding difference selected list and All list for particular domain.
    function diff_In_list(selectedList,list){	
    	var bIds = {},checkDiff=[];
    	selectedList.forEach(function(obj){
    	    bIds[obj.id] = obj;
    	});
    	// pushing all elements which in list, unless in selectedList
    	list.filter(function(obj){
    	    if( !(obj.id in bIds)){
    	    	checkDiff.push(obj)
    	    };
    	});
    	return checkDiff;
    }
    //binding selected items
    function setMultiSelectOptions(selectedList,list){	
        selectedList.forEach(function(selectedData) {
            var data = list.find(function(item) {
                if(item.id && selectedData.id){
                    return item.id == selectedData.id;
                }
            });
            if (data != null) {
                angular.copy(JSON.parse(JSON.stringify( data )), selectedData);
            }
        });
    }

    $scope.multiselectorSettingsForEdit = {
        scrollableHeight: '100px',
        scrollable: true,
        displayProp: 'label',
        //idProperty: 'id',
        enableSearch: true,
        showSelectAll: true,
        keyboardControls: true,
        //closeOnBlur: false,
        showCheckAll: false,
        showUncheckAll:false
        //checkBoxes: false,
        //smartButtonMaxItems: 2
    };

    	/*
		@purpose: function to get news data on search of text string if the classification is selected NEWS
		@params : searchText : type(string)
		@data: 10th June 2019
		@author: Amarjith Kumar
		*/
		$scope.newsSourceResults= [];
		$scope.listSelected = false;
		$scope.getNewsOnSearchSource = function(searchText){
			$scope.listSelected = false;
			SourceManagementAppAppApiService.searchDomainNewsData(searchText).then(function(response){
				$scope.newsSourceResults= response.data.result;
			}).catch(function(err){
				console.log(err);
			})
		}

		/*
		@purpose: function to set news source on click of source list if the classification is selected NEWS
		@params : value : type(string)
		@data: 10th June 2019
		@author: Amarjith Kumar
		*/
		$scope.setSelectedNewsSource = function(value){
			$scope.listSelected = true;
			$scope.newsSourceResults = [];
            $scope.editSourceData.sourceDisplayName = value;
            $scope.editSourceData.sourcename = value;
            $scope.editSourceData.sourcelink = value;
		}
}
