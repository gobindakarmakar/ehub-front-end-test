'use strict';
angular.module('sourceManagementApp')
		.controller('AddNewSourceModalController', addNewSourceModalController);

addNewSourceModalController.$inject = [
	'$scope',
	'$http',
	'$timeout',
	'$rootScope',	 
	'$uibModalInstance',
	'SourceManagementAppAppApiService'
];

function addNewSourceModalController(
	$scope,
	$http,
	$timeout,
	$rootScope,	 
	$uibModalInstance,
	SourceManagementAppAppApiService
	){
		$scope.customFilter = '';
		$scope.addSourceData = {};
		$scope.addSourceDataForTable = {};
		var filter_all = $scope.$parent.JurisdictionList.filter(function(d){return (d.jurisdictionName.toLowerCase() == 'all')});
		var filter_without_all = $scope.$parent.JurisdictionList.filter(function(d){return (d.jurisdictionName.toLowerCase() !== 'all')});
		filter_without_all.sort((a,b) => (a.jurisdictionName > b.jurisdictionName) ? 1 : ((b.jurisdictionName > a.jurisdictionName) ? -1 : 0)); 
		$scope.$parent.JurisdictionList = filter_all.concat(filter_without_all);
		$scope.selectedLists = {
			selectedDomainList: setDefaultValue($scope.DomainList,"General"),
			selectedIndustryList: [],//setDefaultValue($scope.IndustryList,"All"),
			selectedJurisdictionList: [],
			selectedMediaList: []
		};

		function setDefaultValue(list,type){
			var data = [];
			angular.forEach(list,function(val){
				if(val.id === type){
					data.push(val);
				}
			});
			return data;
		}

		$scope.extraSettingsForMutliSelect = {
			scrollableHeight: '100px',
			scrollable: true,
			displayProp: 'label',
			idProperty: 'id',
			enableSearch: true, 
			clearSearchOnClose:true,
			showSelectAll: true, 
			keyboardControls: true,
			//closeOnBlur: false,
			showCheckAll: false,
			showUncheckAll:false
			//checkBoxes: false,
			//smartButtonMaxItems: 2
		};

		 $scope.removeParticularItem = function (item,selectedList,List,removeFrom) {
		       $scope.selectedLists[removeFrom]= selectDeSelectItems(item,selectedList,$scope[List],'onItemDeselect');
		 };

    /*@Purpose: Select , de-select multiselect dropdown
	* @param: item<Object>, data<Array>, dataLists<Array>, selectionName<String>, currentDropdownName<String>
	* return : null
	* @author: Amarjith Kumar
	* @Date: 17th-Jan-2019
	*/
    function selectDeSelectItems(item, data, dataLists, selectionName) {
        if (selectionName == 'onItemSelect') {
            if (item.id == 'All') {
                data = [];
                dataLists.map(function(d) {
                    data.push({
                        'id': d.id
                    });
                    setMultiSelectOptions(data, dataLists);
                });
            } else {
                var difference = diff_In_list(data, dataLists);
                if (difference.length == 1) {
                    if (difference[0].id == "All") {
                        data.push({
                            'id': difference[0].id
                        });
                        setMultiSelectOptions(data, dataLists);
                    }
                }
                setMultiSelectOptions(data, dataLists);
            }
        } else if (selectionName == 'onItemDeselect') {
            if (item.id == "All") {
                data = [];
                setMultiSelectOptions(data, dataLists);
            } else {
                data.map(function(e, i) {
                    if (e.id == item.id || e.id == "All") {
                        data.splice(i, 1);
                        setMultiSelectOptions(data, dataLists);
                    }
                });
            }
        } else if (selectionName == 'onSelectAll') {
			// On key enter Call saveSource function to add attribute in the lists
			var type = $(".bst_input_group_auto_update").attr("id");
			if(type != 'media' && type != 'jurisdiction'){
				$scope.$parent.saveSource($scope.newAttribute,type);
			}
			// data = getSelectItemonAddAttribute(currentDropdownName);

			// below commented for selectALL on key enter

            // data.map(function(d) {
            //     data.push({
            //         'id': d.id
            //     });
            //     setMultiSelectOptions(data, dataLists);
            // });
        }
        return data;
	}

	// function getSelectItemonAddAttribute(currentDropdownName){
	// 	var data = [];
	// 	if(currentDropdownName == "domain"){
	// 		var item = {id:$("#domainId .dropdown-menu a.option").text().trim()};
	// 		var items = [item];
	// 		items.map(function(d) {
    //             data.push({
    //                 'id': d.id
    //             });
    //             setMultiSelectOptions(data, $scope.DomainList);
	// 		});
	// 		return data;
	// 	}else if(currentDropdownName == "jurisdiction"){
	// 		var item = {id:$("#jurisdictionId .dropdown-menu a.option").text().trim()};
	// 		var items = [item];
	// 		items.map(function(d) {
    //             data.push({
    //                 'id': d.id
    //             });
    //             setMultiSelectOptions(data, $scope.JurisdictionList);
	// 		});
	// 		return data;
	// 	}else if(currentDropdownName == "industry"){
	// 		var item = {id:$("#industryId .dropdown-menu a.option").text().trim()};
	// 		var items = [item];
	// 		items.map(function(d) {
    //             data.push({
    //                 'id': d.id
    //             });
    //             setMultiSelectOptions(data, $scope.IndustryList);
	// 		});
	// 		return data;
	// 	}else if(currentDropdownName == "media"){
	// 		var item = {id:$("#mediaId .dropdown-menu a.option").text().trim()};
	// 		var items = [item];
	// 		items.map(function(d) {
    //             data.push({
    //                 'id': d.id
    //             });
    //             setMultiSelectOptions(data, $scope.selectedMediaList);
	// 		});
	// 		return data;
	// 	}
	// }

		//getting industry values from multiselect box

		$scope.selectIndustryList = {
	    	    onItemDeselect: function(item) {
					$scope.selectedLists.selectedIndustryList =  selectDeSelectItems(item,$scope.selectedLists.selectedIndustryList,$scope.IndustryList,'onItemDeselect','industry');
	    	    },
	    	    onItemSelect: function(item,onEnterTabKey) {
					if(onEnterTabKey){
						$scope.selectedLists.selectedIndustryList.push(item);
					}
	    	        $scope.selectedLists.selectedIndustryList =  selectDeSelectItems(item,$scope.selectedLists.selectedIndustryList,$scope.IndustryList,'onItemSelect','industry');
	    	    },
	    	    onSelectAll: function (item,selectedModel,itemNotfound) {
					if(itemNotfound){
						$scope.selectedLists.selectedIndustryList = selectedModel;
					}
					$scope.selectedLists.selectedIndustryList =  selectDeSelectItems(item,$scope.selectedLists.selectedIndustryList,$scope.IndustryList,'onSelectAll','industry');
				}
		};	

		//getting jurisdictionList values from multiselect box
		$scope.selectJurisdictionList = {
				onItemDeselect: function(item) {
						$scope.selectedLists.selectedJurisdictionList =  selectDeSelectItems(item,$scope.selectedLists.selectedJurisdictionList,$scope.JurisdictionList,'onItemDeselect','jurisdiction');
				},
				onItemSelect: function(item,onEnterTabKey) {
					if(onEnterTabKey){
						$scope.selectedLists.selectedJurisdictionList.push(item);
					}
					$scope.selectedLists.selectedJurisdictionList =  selectDeSelectItems(item,$scope.selectedLists.selectedJurisdictionList,$scope.JurisdictionList,'onItemSelect','jurisdiction');
				},
				onSelectAll: function (item) {
					$scope.selectedLists.selectedJurisdictionList =  selectDeSelectItems(item,$scope.selectedLists.selectedJurisdictionList,$scope.JurisdictionList,'onSelectAll','jurisdiction');
				}
//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedJurisdictionList,$scope.JurisdictionList);}
		};

		//getting domain list values from multiselect box
		$scope.selectDomainList = {
				 	onItemDeselect: function(item) {
						$scope.selectedLists.selectedDomainList =  selectDeSelectItems(item,$scope.selectedLists.selectedDomainList,$scope.DomainList,'onItemDeselect','domain');
		    	    },
		    	    onItemSelect: function(item,onEnterTabKey) {
						if(onEnterTabKey){
							$scope.selectedLists.selectedDomainList.push(item);
						}
						$scope.selectedLists.selectedDomainList =  selectDeSelectItems(item,$scope.selectedLists.selectedDomainList,$scope.DomainList,'onItemSelect','domain');
		    	    },
					onSelectAll: function (item,selectedModel,itemNotfound) {
						if(itemNotfound){
							$scope.selectedLists.selectedDomainList = selectedModel;
						}
		    	    	$scope.selectedLists.selectedDomainList =  selectDeSelectItems(item,$scope.selectedLists.selectedDomainList,$scope.DomainList,'onSelectAll','domain');
					}
//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedDomainList,$scope.DomainList);}
		};

		//getting media list values from multiselect box
		$scope.selectMediaList = {
				    onItemDeselect: function(item) {
						$scope.selectedLists.selectedMediaList =  selectDeSelectItems(item,$scope.selectedLists.selectedMediaList,$scope.mediaLists,'onItemDeselect','media');
		    	    },
		    	    onItemSelect: function(item,onEnterTabKey) {
						if(onEnterTabKey){
							$scope.selectedLists.selectedMediaList.push(item);
						}
		    	        $scope.selectedLists.selectedMediaList =  selectDeSelectItems(item,$scope.selectedLists.selectedMediaList,$scope.mediaLists,'onItemSelect','media');
		    	    },
		    	    onSelectAll: function (item) {
		    	    	$scope.selectedLists.selectedMediaList =  selectDeSelectItems(item,$scope.selectedLists.selectedMediaList,$scope.mediaLists,'onSelectAll','media');
					}
//			onSelectionChanged: function(){ return setMultiSelectOptions($scope.selectedLists.selectedMediaList,$scope.mediaLists);}
		};
		   //finding difference selected list and All list for particular domain.
	    function diff_In_list(selectedList,list){	
	    	var bIds = {},checkDiff=[];
	    	selectedList.forEach(function(obj){
	    	    bIds[obj.id] = obj;
	    	});
	    	// pushing all elements which in list, unless in selectedList
	    	list.filter(function(obj){
	    	    if( !(obj.id in bIds)){
	    	    	checkDiff.push(obj);
	    	    }
	    	});
	    	return checkDiff;
	    }
		//binding select items
		function setMultiSelectOptions(selectedList,list){
			selectedList.forEach(function(selectedData) {
				var data = list.find(function(item) {
					if(item.id && selectedData.id){
						return item.id == selectedData.id;
					}
				});
				if (data != null) {
					angular.copy(JSON.parse(JSON.stringify( data )), selectedData);
				}
			});
		}


		//adding source to the table
		$scope.addSourceTotableFromModal = function(){
			var classificationArr = {}; 
			if($scope.classificationsForAddSource.length > 0){
				angular.forEach($scope.classificationsForAddSource,function(val){ //$scope.classificationsForAddSource is from controller
					if(val.classifcationName && (val.classifcationName == $scope.addSourceData.classification) ){
						classificationArr = val;
					}			
				});	
			}

			$scope.addSourceDataForTable =  {
			"sourceName": $scope.addSourceData.sourcename ? $scope.addSourceData.sourcename : "",
			"sourceUrl": $scope.addSourceData.sourcelink ? $scope.addSourceData.sourcelink : "",
			"sourceDisplayName": $scope.addSourceData.sourcename ? $scope.addSourceData.sourcename : "",
			"entityId": "",
			"sourceType": "",
			"sourceDomain":setListsInData($scope.selectedLists.selectedDomainList,"domain"),
			"sourceIndustry": setListsInData($scope.selectedLists.selectedIndustryList,"industry"),
			"sourceJurisdiction": setListsInData($scope.selectedLists.selectedJurisdictionList,"jurisdiction"),
			"classifications": [classificationArr],
			"sourceMedia": setListsInData($scope.selectedLists.selectedMediaList,"media"),
			"category":$scope.addSourceData.addRole ? $scope.addSourceData.addRole : ""
		};		
		   $scope.closeAddSourceModal();
			$timeout(function(){
				$scope.addSource($scope.addSourceDataForTable,classificationArr);
			},1000);
		};
		//setting list items for API
		function setListsInData(selectedList,type){
			var data = [];
			var lable_all_found = false;
			angular.forEach(selectedList,function(val){
				if(type == 'domain'){
					if(val.label && val.label.toLowerCase() == 'all'){
						lable_all_found = true;
						data = [{domainId : val.domainId,domainName:val.domainName}];
					}
					if(!lable_all_found) {data.push({domainId : val.domainId,domainName:val.domainName});}
				}else if(type == 'industry'){
					if(val.label && val.label.toLowerCase() == 'all'){
						lable_all_found = true;
						data = [{industryId : val.industryId,industryName:val.industryName}];
					}
					if(!lable_all_found) {data.push({industryId : val.industryId,industryName:val.industryName});}
				}else if(type == 'jurisdiction'){
					if(val.label && val.label.toLowerCase() == 'all'){
						lable_all_found = true;
						data = [{jurisdictionId : val.jurisdictionId,jurisdictionName:val.jurisdictionName,jurisdictionOriginalName:val.jurisdictionOriginalName,selected:val.selected}];
					}
					if(!lable_all_found) {data.push({jurisdictionId : val.jurisdictionId,jurisdictionName:val.jurisdictionName,jurisdictionOriginalName:val.jurisdictionOriginalName,selected:val.selected});}
				}else if(type == 'media'){
					if(val.label && val.label.toLowerCase() == 'all'){
						lable_all_found = true;
						data = [{mediaId : val.mediaId,mediaName:val.mediaName}];
					}
					if(!lable_all_found) {data.push({mediaId : val.mediaId,mediaName:val.mediaName});}
				}
			});
			lable_all_found = false;
			return data;
		}
		
		$scope.closeAddSourceModal = function(){
			$uibModalInstance.close('close');
		};

		//Getting options and search results from angular_multiselect.js
		window.getValuesFromSearchMultiselector = function(options,searchResults){
			var results = options.filter(function(value) {
					return value.label.toLowerCase().indexOf(searchResults.toLowerCase()) >= 0;
			});
	
			$scope.newAttribute = searchResults;
			if(searchResults.length>0){
			$scope.hideShowDiv.showAddNewDomainAttributeSection = (results.length == 0 && $scope.typeOfSelect == "domain") ? true : false;
			$scope.hideShowDiv.showAddNewIndustryAttributeSection = (results.length == 0 && $scope.typeOfSelect == "industry") ? true : false;
			$scope.hideShowDiv.showAddNewMediaAttributeSection = (results.length == 0 && $scope.typeOfSelect == "media") ? true : false;
			$scope.hideShowDiv.showAddNewJurisdictionAttributeSection = (results.length == 0 && $scope.typeOfSelect == "jurisdiction") ? true : false;
			}
		};

		$scope.closeAddAttributeModal = function(attributeFieldName){
			if(attributeFieldName == 'industry'){
				$scope.hideShowDiv.showAddNewIndustryAttributeSection = false;
			}else if(attributeFieldName == 'media'){
				$scope.hideShowDiv.showAddNewMediaAttributeSection = false;
			}else if(attributeFieldName == 'domain'){
				$scope.hideShowDiv.showAddNewDomainAttributeSection = false;
			}else if(attributeFieldName == 'jurisdiction'){
				$scope.hideShowDiv.showAddNewJurisdictionAttributeSection = false;
			}
			if(attributeFieldName){
				$("#"+attributeFieldName).hide();
			}

		};
	
		$(document).ready(function(){
			$("#industryId.multiselect-wrapper button").click(function(){
				$scope.typeOfSelect = "industry";
			});
			$("#mediaId.multiselect-wrapper button").click(function(){
				$scope.typeOfSelect = "media";
			});
			$("#domainId.multiselect-wrapper button").click(function(){
				$scope.typeOfSelect = "domain";
			});
			$("#jurisdictionId.multiselect-wrapper button").click(function(){
				$scope.typeOfSelect = "jurisdiction";
			});

			$(window).on("click",function(){
				var idname = $(".bst_input_group_auto_update").attr("id");

				$scope.closeAddAttributeModal(idname);
			});
		});

		/*
		@purpose: function to get news data on search of text string if the classification is selected NEWS
		@params : searchText : type(string)
		@data: 10th June 2019
		@author: Amarjith Kumar
		*/
		$scope.newsSourceResults= [];
		$scope.listSelected = false;
		$scope.getNewsOnSearchSource = function(searchText){
			$scope.listSelected = false;
			SourceManagementAppAppApiService.searchDomainNewsData(searchText).then(function(response){
				$scope.newsSourceResults= response.data.result;
			}).catch(function(err){
				console.log(err);
			})
		}

		/*
		@purpose: function to set news source on click of source list if the classification is selected NEWS
		@params : value : type(string)
		@data: 10th June 2019
		@author: Amarjith Kumar
		*/
		$scope.setSelectedNewsSource = function(value){
			$scope.listSelected = true;
			$scope.newsSourceResults = [];
			$scope.addSourceData.sourcename = value;
			$scope.addSourceData.sourceDisplayName = value;
			$scope.addSourceData.sourcelink = value;
		}
} 