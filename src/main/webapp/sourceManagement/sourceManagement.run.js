angular.module('sourceManagementApp')
.run(['$rootScope', 'UploadFileService', 'HostPathService','$location', '$state','EHUB_FE_API','$stateProvider', '$urlRouterProvider', function($rootScope, UploadFileService,  HostPathService, $location, $state,EHUB_FE_API,$stateProvider, $urlRouterProvider){
			var userDetails = getAgainTokenBySession();
			$rootScope.ehubObject = userDetails;
			$rootScope.$state = $state;
			 window.appName = 'sourceManagement';
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
					fromState, fromParams) {
				$(".Bubble_Chart_tooltip").css("display", "none");
				if(userDetails.fullName === "DataEntry User"){
					if(toState.name == 'dataCuration' || toState.name == 'addDataCuration'){
					}else{
						$location.path('/404');
					}
				}
			});
			
			/*
		     * @purpose: Get Submenu Data
		     * @created: 2 Feb 2018
		     * @params: params
		     * @return: no
		     * @author: Amritesh
		    */
			var url;
			function getSubmenu(){
				if(window.location.hash.indexOf("#!/") < 0 && window.location.hash.indexOf("#") >= 0)
					url = 'scripts/common/data/submenu.json';					
		   		else if(window.location.hash === ""){
		   			url = '../scripts/common/data/submenu.json';
		   		}else
		   			url = '../scripts/common/data/submenu.json';
				
					UploadFileService.getSubmenuData(url).then(function(response){
						if($rootScope.ehubObject && ($rootScope.ehubObject.adminUser || $rootScope.ehubObject.analystUser)){
							angular.forEach(response.data.dashboarDropDownMenuItems,function(val,key){
								if(val.menu == "Manage"){
									angular.forEach(val.content,function(v,k){
										if(v.name == "System Settings" && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Document Parsing"){
											v.disabled ="no";
										}
										if(v.name == "Source Management"  && $rootScope.ehubObject.adminUser){
											v.disabled ="no";
										}
										if(v.name == "Orchestration"  && !$rootScope.ehubObject.adminUser){
											v.disabled ="yes";
										}
									});
								}
							});
						}
						HostPathService.setdashboardDisableBydomains(response.data.DisableBydomains);						
						HostPathService.setdashboarDropDownMenuItems(response.data.dashboarDropDownMenuItems);
						HostPathService.setdashboardname($location.absUrl(), response.data.dashboarDropDownMenuItems); /* Setting dashboard name*/
					},function(error){
						HostPathService.FlashErrorMessage('ERROR',error.responseMessage);
					});
			}
			getSubmenu();
			$rootScope.stateForIsra = "";
			$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
				$rootScope.stateForIsra = toState.name;
				if (toState.name == '404' || toState.name == 'domain') {
					$rootScope.topPanelPreview = false;
					if(toState.name == 'domain' && ($rootScope.ehubObject.token === undefined || $rootScope.ehubObject.token === null || $rootScope.ehubObject.token === "")){
						$window.location.href = '';
					}
				} else {
					if($rootScope.ehubObject.token !== undefined && $rootScope.ehubObject.token !== null && $rootScope.ehubObject.token !== "")
						$rootScope.topPanelPreview = true;
					else
						$window.location.href = '';
				}
			});
			
			   $stateProvider
				.state('sourceManagement', {
					url: '/sourceManagement',
					templateUrl: function(){
						return  EHUB_FE_API + appName + '/views/sourceManagement.jsp';
					},
					controller : 'SourceManagementController'
				}).state('sourceManagement_new', {
					url: '/sourceManagement_new',
					templateUrl: function(){
						return  EHUB_FE_API + appName + '/views/sourceManagement_new.jsp';
					},
					controller : 'sourceManagementController_new'
				}).state('404', {
					url : '/404',
					templateUrl : function(){
						return EHUB_FE_API + 'error';
					}
				});
			   $urlRouterProvider.when('', '/sourceManagement');
			   $urlRouterProvider.otherwise(function(){
					return '/404';
				});
			  
		}]);