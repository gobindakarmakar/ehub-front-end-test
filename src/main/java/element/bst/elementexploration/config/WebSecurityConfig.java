package element.bst.elementexploration.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import element.bst.elementexploration.security.CustomAuthenticationProvider;
import element.bst.elementexploration.security.CustomAuthenticationSuccessHandler;

/**
 * 
 * @author Amit Patel
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


	@Autowired
	private CustomAuthenticationProvider authProvider;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.authenticationProvider(authProvider);
	}

	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {

		return new CustomAuthenticationSuccessHandler();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().formLogin().loginPage("/login").failureUrl("/login?login_error=true")
				.successHandler(authenticationSuccessHandler())
				.loginProcessingUrl("/login").and().authorizeRequests().antMatchers("/login", "/assets/**", "/vendor/**", "/scripts/**").permitAll()
				.anyRequest().authenticated().and().logout().deleteCookies("JSESSIONID").invalidateHttpSession(true)
				.logoutSuccessUrl("/login").logoutUrl("/logout");
		http.headers().frameOptions().disable();
	}
}
