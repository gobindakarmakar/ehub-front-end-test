package element.bst.elementexploration.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import element.bst.elementexploration.security.domain.CurrentUser;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler{

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		HttpSession session = request.getSession(true);
		CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
		session.setAttribute("userInfo", currentUser.getUserInfo());
		//if(!currentUser.getUserInfo().isDataEntryUser())
			redirectStrategy.sendRedirect(request, response, currentUser.getUserInfo().getDefaultUrl()!=null && !currentUser.getUserInfo().getDefaultUrl().isEmpty() ? currentUser.getUserInfo().getDefaultUrl() : "/");
		//else
			//redirectStrategy.sendRedirect(request, response, "#/dataCuration");//Add page
	}

}
