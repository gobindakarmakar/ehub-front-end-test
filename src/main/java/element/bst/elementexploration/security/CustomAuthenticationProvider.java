package element.bst.elementexploration.security;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import element.bst.elementexploration.security.domain.CurrentUser;
import element.bst.elementexploration.security.domain.UserInfo;

@Component("authProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private HttpServletRequest request;
	
	@Value("${element.api.url}")
    private String ELEMENT_LOGIN_URL;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		String url=null;
		try {
			url = new String(
					request.getScheme() + "://" + InetAddress.getLocalHost().getHostAddress() + ":" + request.getLocalPort() + "/");
		} catch (UnknownHostException unknownException) {
			unknownException.printStackTrace();
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("CALLER-IP", request.getRemoteAddr());
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("emailAddress", name);
		map.add("password", password);
		UserInfo userInfo = null;
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		try{
			userInfo = restTemplate.postForObject(ELEMENT_LOGIN_URL, request, UserInfo.class);
		}catch (Exception e) {
			e.printStackTrace();
		}

		if (userInfo != null) {
			UserDetails userDetails = new CurrentUser(name, password, userInfo);
			return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
