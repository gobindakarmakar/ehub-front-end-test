package element.bst.elementexploration.security.domain;

import java.io.Serializable;

/**
 * 
 * @author Amit Patel
 *
 */
public class UserInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email;

	private String token;

	private Long userId;

	private String fullName;

	private String lastLoginDate;

	private String lang;

	private boolean dataEntryUser;

	private boolean isAdminUser = false;

	private String defaultUrl;
	
	private boolean isAnalystUser = false;


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isDataEntryUser() {
		return dataEntryUser;
	}

	public void setDataEntryUser(boolean dataEntryUser) {
		this.dataEntryUser = dataEntryUser;
	}

	public boolean isAdminUser() {
		return isAdminUser;
	}

	public void setAdminUser(boolean isAdminUser) {
		this.isAdminUser = isAdminUser;
	}

	public String getDefaultUrl() {
		return defaultUrl;
	}

	public void setDefaultUrl(String defaultUrl) {
		this.defaultUrl = defaultUrl;
	}

	public boolean isAnalystUser() {
		return isAnalystUser;
	}

	public void setAnalystUser(boolean isAnalystUser) {
		this.isAnalystUser = isAnalystUser;
	}

}
