package element.bst.elementexploration.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import element.bst.elementexploration.security.domain.CurrentUser;
import element.bst.elementexploration.security.domain.UserInfo;

@Controller
public class StaticRouteController {

	@Autowired
	HttpServletRequest httpServletRequest;

	@GetMapping("/login")
	public String loginPage() {
		return "login";
	}
		
	@GetMapping("/")
	public String app(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "index";
	}
	
	@GetMapping("/domain")
	public String domain(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "domain";
	}
	@GetMapping("/process")
	public String process(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "process";
	}
	@GetMapping("/workspace")
	public String workspace(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "workspace";
	}
	@GetMapping("/discoverDashboard")
	public String discoverDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "discoverDashboard";
	}
	@GetMapping("/discoverDashboardUnderwriting")
	public String discoverDashboardUnderwriting(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "discoverDashboardUnderwriting";
	}
	@GetMapping("/transactionMonitoring")
	public String transactionMonitoring(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "transactionMonitoring";
	}
	@GetMapping("/investigationLanding")
	public String investigationLanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "investigationLanding";
	}
	@GetMapping("/predictDashboard")
	public String predictDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "predictDashboard";
	}
	@GetMapping("/enrichDashboard")
	public String enrich(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "enrichDashboard";
	}
	@GetMapping("/actDashboard")
	public String actDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "actDashboard";
	}
	@GetMapping("/actDashboardUnderwriting")
	public String actDashboardUnderwriting(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "actDashboardUnderwriting";
	}
	@GetMapping("/casePageLanding")
	public String casePageLanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "casePageLanding";
	}
	@GetMapping("/alertsDashboard")
	public String alertsDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "alertsDashboard";
	}
	@GetMapping("/auditTrail")
	public String auditTrail(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "auditTrail";
	}
	@GetMapping("/entityPerson")
	public String entityPage(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "entityPage";
	}
	@GetMapping("/entityPersonNew")
	public String entityPersonNew(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "entityPersonNew";
	}
	@GetMapping("/entityCompany")
	public String entityCompany(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "entityCompany";
	}
	@GetMapping("/entityCompanyNew")
	public String entityCompanyNew(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "entityCompanyNew";
	}
	@RequestMapping(value = "/live-feed", method = RequestMethod.GET)
	public String socialLiveFeedContent(HttpServletRequest request, Model model, HttpServletResponse response) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "socialLiveFeedContent";
	}
	@GetMapping("/manageDashboard")
	public String manageDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageDashboard";
	}
	@GetMapping("/userManagement")
	public String userManagement(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "userManagement";
	}
	@GetMapping("/workflow")
	public String workflow(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "workflow";
	}
	@GetMapping("/appManager")
	public String appManager(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "appManager";
	}
	@GetMapping("/systemMonitoring")
	public String systemMonitoring(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "systemMonitoring";
	}
	@GetMapping("/dataManagement")
	public String dataManagement(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "dataManagement";
	}
	@GetMapping("/generalSettings")
	public String generalSettings(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "generalSettings";
	}
	@GetMapping("/adverseNews")
	public String adverseNews(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "adverseNews";
	}
	@GetMapping("/screeningTesting")
	public String screeningTesting(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "screeningTesting";
	}
	@GetMapping("/linkAnalysis")
	public String linkAnalysis(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "linkAnalysis";
    }
	@GetMapping("/user")
	public String user(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "user";
	}
	@GetMapping("/group")
	public String group(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "group";
	}
	@GetMapping("/error")
	public String error(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "error";
	}
	@GetMapping("/manageDb")
	public String manageDb(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageDb";
	}
	@GetMapping("/deployments")
	public String deployments(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "deployments";
	}
	@GetMapping("/jobs")
	public String jobs(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "jobs";
	}
	@GetMapping("/administrations")
	public String administrations(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "administrations";
	}
	@GetMapping("/crystalBall")
	public String crystalBall(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "crystalBall";
	}
	@GetMapping("/activeProcess")
	public String activeProcess(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "activeProcess";
	}
	@GetMapping("/suspendedProcess")
	public String suspendedProcess(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "suspendedProcess";
	}
	
	@GetMapping("/manageRoles")
	public String manageRoles(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageRoles";
	}
	
	@GetMapping("/managePasswords")
	public String managePasswords(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "managePasswords";
	}
	
	@GetMapping("/manageMonitor")
	public String manageMonitor(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageMonitor";
	}
	
	@GetMapping("/manageAccess")
	public String manageAccess(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageAccess";
	}
	
	@GetMapping("/businessRules")
	public String businessRules(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "businessRules";
	}
	
	@GetMapping("/bigDataRules")
	public String bigDataRules(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "bigDataRules";
	}
	@GetMapping("/ontology")
	public String ontology(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "ontology";
	}
	@GetMapping("/logics")
	public String logics(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "logics";
	}
	@GetMapping("/myApps")
	public String myApps(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "myApps";
	}
	@GetMapping("/store")
	public String store(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "store";
	}
	@GetMapping("/appConfiguration")
	public String appConfiguration(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "appConfiguration";
	}
	
	@GetMapping("/licenseManager")
	public String licenseManager(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "licenseManager";
	}
	@GetMapping("/socialManager")
	public String socialManager(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "socialManager";
	}
	
	@GetMapping("/myDataSources")
	public String myDataSources(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "myDataSources";
	}
	@GetMapping("/addNewSources")
	public String addNewSources(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "addNewSources";
	}
	
	@GetMapping("/manageSources")
	public String manageSources(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "manageSources";
	}
	@GetMapping("/exportImport")
	public String exportImport(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "exportImport";
	}
	@GetMapping("/events")
	public String events(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "events";
	}
	@GetMapping("/hosts")
	public String hosts(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "hosts";
	}
	@GetMapping("/response")
	public String response(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "response";
	}
	@GetMapping("/fields")
	public String fields(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "fields";
	}
	@GetMapping("/portalSettings")
	public String portalSettings(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "portalSettings";
	}
	@GetMapping("/serveSettings")
	public String serveSettings(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "serveSettings";
	}
	@GetMapping("/systemProperties")
	public String systemProperties(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "systemProperties";
	}
	@GetMapping("/customFields")
	public String customFields(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "customFields";
	}
	@GetMapping("/portalInstances")
	public String portalInstances(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "portalInstances";
	}
	@GetMapping("/dataSource")
	public String dataSource(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "dataSource";
	}
	@GetMapping("/tasks")
	public String tasks(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "tasks";
	}
	@GetMapping("/processInstance")
	public String processInstance(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "processInstance";
	}
	@GetMapping("/reports")
	public String reports(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "reports";
	}
	@GetMapping("/deploymentProcess")
	public String deploymentProcess(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "deploymentProcess";
	}
	@GetMapping("/modelWorkspace")
	public String modelWorkspace(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "modelWorkspace";
	}
	@GetMapping("/mipLanding")
	public String mipLanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "mipLanding";
	}
	@GetMapping("/mip")
	public String mip(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "mip";
	}
	@GetMapping("/entityCompanyStatic")
	public String entityCompanyStatic(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "entityCompanyStatic";
	}
	@GetMapping("/transactionAnalysisLanding")
	public String transactionAnalysisLanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "transactionAnalysisLanding";
	}
	@GetMapping("/transactionAnalysis")
	public String transactionAnalysis(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "transactionAnalysis";
	}
	@GetMapping("/dataCuration")
	public String dataCuration(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "dataCuration";
	}
	@GetMapping("/addDataCuration")
	public String addDataCuration(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "addDataCuration";
	}
	@GetMapping("/data-find-data")
	public String dfdDashboard(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "dfdDashboard";
	}	
	@GetMapping("/israCardlanding")
	public String israCardlanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "israCardlanding";
	}
	@GetMapping("/arcSelection")
	public String arcSelection(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "arcSelection";
	}
	@GetMapping("/segmentSelection")
	public String segmentSelection(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "segmentSelection";
	}
	@GetMapping("/israClusters")
	public String israClusters(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "israClusters";
	}
	@GetMapping("/earlyAdopters")
	public String earlyAdopters(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "earlyAdopters";
	}
	@GetMapping("/customerUsage")
	public String customerUsage(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "customerUsage";
	}
	@GetMapping("/israCardDetail")
	public String israCardDetail(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "israCardDetail";
	}
	@GetMapping("/userCaseLanding")
	public String userCaseLanding(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "userCaseLanding";
	}
	@GetMapping("/userCaseDetails")
	public String userCaseDetails(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "userCaseDetails";
	}
	@GetMapping("/users")
	public String users(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "users";
	}
	@GetMapping("/groups")
	public String groups(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "groups";
	}
	@GetMapping("/personal")
	public String personal(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "personal";
	}
	@GetMapping("/createNotification")
	public String createNotification(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "createNotification";
	}
	@GetMapping("/createEvents")
	public String createEvents(HttpServletRequest request, Model model) {
		model.addAttribute("firstName", getFirstName(getCurrentUserInfo().getFullName()));
		model.addAttribute("fullName", getCurrentUserInfo().getFullName());
		return "createEvents";
	}
	private UserInfo getCurrentUserInfo(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
		return currentUser.getUserInfo();
	}
	
	private String getFirstName(String fullName){
		String[] splited = fullName.split("\\s+");
		return splited[0];
	}
}